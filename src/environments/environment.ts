export const environment = {
  production: false,
  keycloak: {
    issuer: 'http://192.168.2.105:8081/',
    realm: 'sso',
    client: 'crm-frontend',
    relationShip: 'crm-service',
    realmManagement: 'realm-management',
  },

  url_endpoint_admin: 'http://dev-crmcore-api.evotek.vn/crm01-admin',
  url_endpoint_sale: 'http://dev-crmcore-api.evotek.vn/crm12-sale',
  url_endpoint_workflow: 'http://dev-crmcore-api.evotek.vn/crm11-workflow',
  url_endpoint_category: 'http://dev-crmcore-api.evotek.vn/crm02-category',
  url_endpoint_activity: 'http://dev-crmcore-api.evotek.vn/crm10-activity',
  url_endpoint_rm: 'http://dev-crmcore-api.evotek.vn/crm05-rmprofile',
  url_endpoint_customer: 'http://dev-crmcore-api.evotek.vn/crm08-customer',
  url_endpoint_rm_relationship: 'http://dev-crmcore-api.evotek.vn/crm09-rm-relationship',
  url_endpoint_kpi: 'http://dev-crmcore-api.evotek.vn/crm15-kpi',
  url_endpoint_integration: 'http://dev-crmcore-api.evotek.vn/crm16-integration',
  url_endpoint_ticket: 'http://dev-crmcore-api.evotek.vn/crm14-services-management',
  url_endpoint_report: 'http://dev-crmcore-api.evotek.vn/crm18-report',
  url_endpoint_customer_sme: 'http://dev-crmcore-api.evotek.vn/crm19-customer-org',

  url_socket_endpoint_sale: 'https://gateway.crm.lptech.dev/sale-service/websocket',
  url_socket_endpoint_workflow: 'https://gateway.crm.lptech.dev/workflow-service/websocket',
  url_socket_endpoint_category: 'https://gateway.crm.lptech.dev/category-service/websocket',
  url_socket_endpoint_activity: 'https://gateway.crm.lptech.dev/activity-service/websocket',

  socket_endpoint_rocketchat: 'wss://chat.crm.lptech.dev/websocket',
  url_endpoint_rocketchat: 'https://chat.crm.lptech.dev',
  url_endpoint_embedded: 'http://dev-crmcore-api.evotek.vn',

};
