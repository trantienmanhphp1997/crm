import { Utils } from 'src/app/core/utils/utils';
import { FormControl, FormGroup } from '@angular/forms';
import { Roles, Scopes } from './common-constants';
import { HttpErrorResponse } from '@angular/common/http';
import * as _ from 'lodash';

export function getRole(roles: string[]): string {
  if (roles) {
    if (roles.indexOf(Roles.TL) !== -1) {
      return Roles.TL;
    } else if (roles.indexOf(Roles.SeRM) !== -1) {
      return Roles.SeRM;
    } else if (roles.indexOf(Roles.RM) !== -1) {
      return Roles.RM;
    } else if (roles.indexOf(Roles.RGM) !== -1) {
      return Roles.RGM;
    } else if (roles.indexOf(Roles.RDM) !== -1) {
      return Roles.RDM;
    } else if (roles.indexOf(Roles.OP) !== -1) {
      return Roles.OP;
    } else if (roles.indexOf(Roles.ED) !== -1) {
      return Roles.ED;
    } else if (roles.indexOf(Roles.BODS) !== -1) {
      return Roles.BODS;
    } else if (roles.indexOf(Roles.BOD) !== -1) {
      return Roles.BOD;
    } else if (roles.indexOf(Roles.BM) !== -1) {
      return Roles.BM;
    } else if (roles.indexOf(Roles.BDM) !== -1) {
      return Roles.BDM;
    } else if (roles.indexOf(Roles.ADM) !== -1) {
      return Roles.ADM;
    }
  }
  return null;
}

export function generateAvatar(name: string, isUser?: boolean) {
  const nameArr = name?.trim().split(' ');
  if (nameArr.length > 1) {
    if (isUser) {
      return nameArr[nameArr.length - 2] + ' ' + nameArr[nameArr.length - 1];
    } else {
      return nameArr[0] + ' ' + nameArr[1];
    }
  }
  return name;
}
export function buildRSQL(objSearch, query: string) {
  if (!query) {
    query = '';
  }
  if (objSearch) {
    const condition = objSearch.condition === 'and' ? ';' : ',';
    objSearch.rules.forEach((rule) => {
      if (rule.condition) {
        query += '(';
        query = buildRSQL(rule, query);
        query += ')';
        query += condition;
      } else {
        if (rule.value && rule.value.toString().length > 0) {
          query += rule.field;
          switch (rule.operator) {
            case '=':
              query += '==';
              query += rule.value;
              break;
            case 'like':
              query += '==';
              query += '*' + rule.value + '*';
              break;
            case 'in':
              query += '=in=';
              query += '(' + rule.value + ')';
              break;
            case 'out':
              query += '=out=';
              query += '(' + rule.value + ')';
              break;
            default:
              query += rule.operator;
              query += rule.value;
              break;
          }
          query = query + condition;
        }
      }
    });
  }
  query = query && query.length > 0 ? query.substr(0, query.length - 1) : query;
  return query;
}

export function buildBaseQuery(objSearch: any, query: string) {
  if (!query) {
    query = '';
  }
  if (objSearch) {
    Object.keys(objSearch).forEach((key) => {
      if (objSearch[key] && objSearch[key].toString().trim().length > 0) {
        query += key;
        query += '==';
        query += objSearch[key];
        query += ';';
      }
    });
  }
  return query.length > 0 ? query.substr(0, query.length - 1) : query;
}

export function sortTaskKanban(array: any[]) {
  return array.sort((a, b) => {
    if (!b.index) {
      return -1;
    } else if (b.index && a.index && a.index !== b.index) {
      return a.index - b.index; // asc
    }
    if (!b.timeIndex) {
      return -1;
    } else if (b.timeIndex && a.timeIndex && a.timeIndex !== b.timeIndex) {
      return b.timeIndex - a.timeIndex; // asc
    }
  });
}

export function validateAllFormFields(formGroup: FormGroup) {
  Object.keys(formGroup.controls).forEach((field) => {
    const control = formGroup.get(field);
    if (control instanceof FormControl) {
      control.markAsTouched({ onlySelf: true });
    } else if (control instanceof FormGroup) {
      validateAllFormFields(control);
    }
  });
}

export function cleanDataForm(formGroup: FormGroup) {
  const form = formGroup;
  Object.keys(form.controls).forEach((field) => {
    const control = form.get(field);
    if (control instanceof FormControl && typeof control.value === 'string') {
      control.setValue(Utils.trimNullToEmpty(control.value), { emitEvent: false });
    } else if (control instanceof FormGroup) {
      cleanDataForm(control);
    }
  });
  return form.getRawValue();
}

export function cleanData(data: any) {
  Object.keys(data).forEach((key) => {
    if (_.isString(data[key]) || _.isNull(data[key]) || _.isUndefined(data[key])) {
      data[key] = _.trim(data[key]);
    } else if (_.isArray(data[key])) {
      const array = data[key];
      for (let index = 0; index < array?.length; index++) {
        array[index] = _.trim(array[index]);
      }
    } else if (_.isObject(data[key])) {
      cleanData(data[key]);
    }
  });
}

export function checkScopeForScreen(scope, objFunction) {
  if (!objFunction) {
    return false;
  }
  switch (scope) {
    case Scopes.CREATE:
      return objFunction.create;
    case Scopes.UPDATE:
      return objFunction.update;
    case Scopes.VIEW:
      return objFunction.view;
    case Scopes.DELETE:
      return objFunction.delete;
    case Scopes.EXPORT:
      return objFunction.export;
    case Scopes.IMPORT:
      return objFunction.import;
    default:
      return false;
  }
}

export function percentage(value: number, total: number) {
  if (value === 0 && total === 0) {
    return value.toFixed(2);
  }
  return ((100 * value) / total)?.toFixed(2);
}

export function percentagev2(value: number, total: number) {
  if (total === 0)
    return "0.00";

  if (value === 0 && total !== 0) {
    return "0.00";
  }

  const data = (Math.ceil((value/total) * 10000)/100)?.toFixed(2)

  return data;
}

export function checkRespondSuccess(respond) {
  return !(respond instanceof HttpErrorResponse);
}

export function clearLocalStorage(user: any, removeSessionImg = false): void {
  // prepare all key will live forever
  const posterStatus = getLocalPosterList(user);
  const date = new Date();
  const currentDate =  (date.getFullYear() + '' + (date.getMonth() > 8 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1)) + '' + (date.getDate() > 9 ? date.getDate() : '0' + date.getDate()));
  const keyCountLoginView = user?.username + '_' + currentDate + '_VIEW';
 // localStorage.clear();
  for (let i = 0; i < localStorage.length; i++){
    if(!localStorage.key(i).includes(currentDate)){
      localStorage.removeItem(localStorage.key(i));
    }
  }
  localStorage.setItem(keyCountLoginView,'1');
  // restore all live key
  posterStatus.filter(item => removeSessionImg ? !item.key.includes('session_poster') : true).forEach(poster => {
    localStorage.setItem(poster.key, poster.value);
  })
}

export function getLocalPosterList(user): any[] {
  const posterKey = {
    session: 'session_poster',
    live: 'poster'
  }

  const getId = (key: string) => {
    let result = '';
    if (key.includes(posterKey.session)) {
      result = key.split(`_${posterKey.session}`)[0];
    } else if (key.includes(posterKey.live)) {
      result = key.split(`_${posterKey.live}`)[0];
    }
    return result;
  }

  return Object.keys(localStorage).filter(key => key.includes(`poster_${user.username}`)).map(key => {
    return {key, value: localStorage.getItem(key), id: getId(key)}
  })
}

export function logoutKeycloak(){
  // Call api logout
  const keycloakLogoutUrl = 'http://192.168.2.105:8081/realms/sso/protocol/openid-connect/logout';
  const postLogoutRedirectUri = 'http://dev-crmcore-api.evotek.vn/';
  const clientId = 'crm-frontend';
  window.location.href = `${keycloakLogoutUrl}?post_logout_redirect_uri=${encodeURIComponent(postLogoutRedirectUri)}&client_id=${clientId}`;
}
