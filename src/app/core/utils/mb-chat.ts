export let srcFrameMbChat = '';

export function createGroupChat11(idRoomChat: string, isGroup? : boolean) {
  let path = 'chat/';
  if (isGroup) {
    path = 'broadcast/'
  }
  if (!srcFrameMbChat) {
    const el = document.getElementById('frame').outerHTML;
    srcFrameMbChat = el.slice(el.indexOf('https://'), el.lastIndexOf('"><'));
  }
  document.getElementById('frame').setAttribute('src', srcFrameMbChat.replace('/widget?token','/widget/' + path + idRoomChat + '?token'));

  if (document.getElementsByClassName('chat-widget--bubble -hide').length > 0) {
    const element: HTMLElement
      = document?.getElementById('widget-chat')?.getElementsByClassName('chat-widget--button')[0] as HTMLElement;
    element.click();
  }
}



