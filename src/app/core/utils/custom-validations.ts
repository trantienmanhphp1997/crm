import { AbstractControl, ValidationErrors } from '@angular/forms';

const regexPhoneMobileVN = new RegExp(/84|0[35789]([0-9]{8})/g);
const regexEmail = new RegExp(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g);
const regexOnlyNumber = new RegExp(/^[0-9]*$/g);
const regexCode = new RegExp(/^[0-9A-Z-_]*$/g);
const regexCodeKpi = new RegExp(/^[a-zA-Z0-9_]*$/g);
// ([1-9]|0(?=,[1-9])) -> Bat dau boi 1-9 hoac 0 nhung theo sau boi , va 1-9
const regexRate = new RegExp(/^([1-9]|0(?=,[1-9]))(\d*)(,\d)?$/g);

// 2 number decimal
const regexStandardKpi = new RegExp(/^(\d+)(,\d{1,2})?$/g);
const regexValueByTitle = new RegExp(/^(\d+)(,\d{1,2})?$/g);
const valueFactor = new RegExp(/^(\d+)(,\d{1,2})?$/g);
const regexRmTime = new RegExp(/^(\d+)(,\d{1,2})?$/g);
const taxCode = new RegExp(/^[0-9A-Z-/]*$/g);
const haveNormalAndSpecialChar = new RegExp(/^[a-zA-Z0-9!@#$%^&*~`()_+\-=\[\]{};':"\\|,.<>\/?]+$/);

export class CustomValidators {
  static required(control: AbstractControl): ValidationErrors | null {
    if (
      (!control.value && control.value !== 0 && control.value !== false) ||
      control.value?.toString()?.trim()?.length === 0
    ) {
      return { cusRequired: true };
    }
    return null;
  }

  static code(control: AbstractControl): ValidationErrors | null {
    regexCode.lastIndex = 0;
    if (control.value && !regexCode.test(control.value?.toString())) {
      return { code: true };
    }
    return null;
  }

  static email(control: AbstractControl): ValidationErrors | null {
    regexEmail.lastIndex = 0;
    if (control.value && !regexEmail.test(control.value?.toString())) {
      return { email: true };
    }
    return null;
  }

  static codeKpi(control: AbstractControl): ValidationErrors | null {
    regexCodeKpi.lastIndex = 0;
    if (control.value && !regexCodeKpi.test(control.value?.toString())) {
      return { code: true };
    }
    return null;
  }

  static valueFactor(control: AbstractControl): ValidationErrors | null {
    valueFactor.lastIndex = 0;
    let factor = parseFloat(control.value?.toString()?.replace(',', '.'));
    let validFactor = factor > 0;
    if (control.value && (!valueFactor.test(control.value?.toString()) || !validFactor)) {
      return { value: true };
    }
    return null;
  }

  static rate(control: AbstractControl): ValidationErrors | null {
    regexRate.lastIndex = 0;
    let rate = parseFloat(control.value?.toString()?.replace(',', '.'));
    let validRate = rate > 0 && rate <= 100;
    if (control.value && (!regexRate.test(control.value?.toString()) || !validRate)) {
      return { value: true };
    }
    return null;
  }

  static standardKpi(control: AbstractControl): ValidationErrors | null {
    regexStandardKpi.lastIndex = 0;
    let standardKpi = parseFloat(control.value?.toString()?.replace(',', '.'));
    let validStandardKpi = standardKpi >= 0;
    if (control.value && (!regexStandardKpi.test(control.value?.toString()) || !validStandardKpi)) {
      return { value: true };
    }
    return null;
  }

  static assignKpi(control: AbstractControl): ValidationErrors | null {
    regexStandardKpi.lastIndex = 0;
    let assignKpi = parseFloat(control.value?.toString()?.replace(',', '.'));
    let validAssignKpi = assignKpi >= 0;
    if (control.value && (!regexStandardKpi.test(control.value?.toString()) || !validAssignKpi)) {
      return { value: true };
    }
    return null;
  }

  static valueRmTime(control: AbstractControl): ValidationErrors | null {
    regexRmTime.lastIndex = 0;
    let timeRate = parseFloat(control.value?.toString()?.replace(',', '.'));
    let validTimeRate = timeRate > 0 && timeRate <= 1;
    if (control.value && (!regexRmTime.test(control.value?.toString()) || !validTimeRate)) {
      return { value: true };
    }
    return null;
  }

  static valueByTitle(control: AbstractControl): ValidationErrors | null {
    regexValueByTitle.lastIndex = 0;
    let valueByTitle = parseFloat(control.value?.toString()?.replace(',', '.'));
    let validValueByTitle = valueByTitle >= 0;
    if (control.value && (!regexValueByTitle.test(control.value?.toString()) || !validValueByTitle)) {
      return { value: true };
    }
    return null;
  }

  static noSpace(control: AbstractControl): ValidationErrors | null {
    if (control.value && control.value.toString().indexOf(' ') >= 0) {
      return { noSpace: true };
    }
    return null;
  }

  static phoneMobileVN(control: AbstractControl): ValidationErrors | null {
    regexPhoneMobileVN.lastIndex = 0;
    if (control.value && !regexPhoneMobileVN.test(control.value.toString())) {
      return { phoneMobile: true };
    }
    return null;
  }

  static onlyNumber(control: AbstractControl): ValidationErrors | null {
    regexOnlyNumber.lastIndex = 0;
    if (control.value && !regexOnlyNumber.test(control.value.toString().trim())) {
      return { onlyNumber: true };
    }
    return null;
  }

  static taxCode(control: AbstractControl): ValidationErrors | null {
    taxCode.lastIndex = 0;
    if (control.value && !taxCode.test(control.value.toString())) {
      return { taxCode: true };
    }
    return null;
  }

  static onlyNumberAndDecimal(control: AbstractControl): ValidationErrors | null {
    if (isNaN(control.value)) {
      return { onlyNumber: true };
    }
    
    return null;
  }

  static onlyNormalAndSpecialChar(control: AbstractControl): ValidationErrors | null {
    if (control.value && !haveNormalAndSpecialChar.test(control.value.toString().trim())) {
      return { onlyNormalAndSpecialChar: true }
    }

    return null;
  }

  static onlyInteger(control: AbstractControl): ValidationErrors | null {
    regexOnlyNumber.lastIndex = 0;
    
    if (!!control.value && Number(control.value) === 0) {
      return { onlyInteger: true };
    }
    return null;
  }
}
