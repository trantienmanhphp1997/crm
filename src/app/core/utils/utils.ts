import { Observable } from 'rxjs';
import _ from 'lodash';
import { formatDate } from '@angular/common';
export class Utils {
  private static async preresolve(promises, value, key?: string) {
    if (key === 'callback' || key === 'socket' || key === 'listener') {
      promises.push(value);
      return;
    }
    if (value instanceof Function) {
      this.preresolve(promises, value());
    } else if (value instanceof Array) {
      const inpromises = [];
      value.forEach((v) => {
        this.preresolve(inpromises, v);
      });
      promises.push(Promise.all(inpromises));
    } else if (value instanceof Observable) {
      promises.push(value.toPromise());
    } else if (value instanceof Promise) {
      promises.push(value);
    } else if (value instanceof Object) {
      promises.push(this.resolve(value));
    } else {
      promises.push(value);
    }
  }

  static async resolve(value: Object) {
    const keys = [];
    const promises = [];
    for (const i in value) {
      if (value.hasOwnProperty(i)) {
        keys.push(i);
        this.preresolve(promises, value[i], i);
      }
    }
    const values = await Promise.all(promises);
    const raw = {};
    keys.forEach((key: string, index: number) => {
      raw[key] = values[index];
    });
    return raw;
  }

  // static isEmailValid(email: string): boolean {
  //   const regex = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.+[A-Z]{2,4}/gim;
  //   return regex.test(email);
  // }

  static isNotNull(obj: any): boolean {
    return !this.isNull(obj);
  }

  static isNull(obj: any): boolean {
    return obj === undefined || obj === null || obj === {};
  }

  static isEmpty(obj: any): boolean {
    return _.isEmpty(obj);
  }

  static isNotEmpty(obj: any): boolean {
    return !this.isEmpty(obj);
  }

  static isFunction(obj: any): boolean {
    return this.isNotNull(obj) && obj instanceof Function;
  }

  static isNotFunction(obj: any): boolean {
    return !this.isFunction(obj);
  }

  static isStringEmpty(obj: string): boolean {
    return this.isNull(obj) || obj === '';
  }

  static isStringNotEmpty(obj: string): boolean {
    return !this.isStringEmpty(obj);
  }

  static isArrayEmpty(obj: any[]): boolean {
    return this.isNull(obj) || obj.length === 0;
  }

  static isArrayNotEmpty(obj: any[]): boolean {
    return !Utils.isArrayEmpty(obj);
  }

  static isHtmlNotEmpty(text: string): boolean {
    return !Utils.isHtmlEmpty(text);
  }

  static subString(text: string) {
    if(typeof text != 'string'){
      return;
    }
    return text?.substr(0, 1) || '';
  }

  static isHtmlEmpty(text: string): boolean {
    if (Utils.isNull(text)) {
      return true;
    }
    text = text.replace(/(<([^>]+)>)/gi, '');
    text = text.trim();
    return Utils.isStringEmpty(text);
  }

  static number(obj: any, defaultValue?: number) {
    if (Utils.isNull(obj) || Utils.isStringEmpty(obj) || isNaN(obj)) {
      return defaultValue;
    }
    return Number(obj);
  }

  static shorten(text: string, numOfCharacters: number = 300): string {
    if (Utils.isStringNotEmpty(text)) {
      text = text.replace(/(<([^>]+)>)/gi, '');
      text = text.replace(/\s+/g, ' ');
      if (text.length > numOfCharacters) {
        text = text.substr(0, numOfCharacters) + '...';
      }
    }
    return text;
  }

  // static isMatch(query: string, text: string): boolean {
  //   const terms = this.parseToEnglish(query)
  //     .toLowerCase()
  //     .trim()
  //     .split(/\s+|\+/g);
  //   for (let i = 0; i < terms.length; i++) {
  //     const index = text.indexOf(terms[i]);
  //     if (index < 0) {
  //       return false;
  //     }
  //     if (index > 0 && text.charAt(index - 1) !== ' ') {
  //       return false;
  //     }
  //   }
  //   return true;
  // }

  // static cleanUpHtml(html: string): string {
  //   if (Utils.isNull(html)) {
  //     return '';
  //   }
  //   return html.replace(/<\/*span.*?>/gi, '');
  // }

  static stripHtmlTags(text: string): string {
    return text ? text.replace(/\<.*?\>/gi, '') : undefined;
  }

  static parseToEnglish(text: string): string {
    if (Utils.isNull(text)) {
      return '';
    }
    let value = text.trim().toLowerCase();
    value = value.replace(/[áàảãạâấầẩẫậăắằẳẵặ]/gi, 'a');
    value = value.replace(/[éèẻẽẹêếềểễệ]/gi, 'e');
    value = value.replace(/[iíìỉĩị]/gi, 'i');
    value = value.replace(/[óòỏõọơớờởỡợôốồổỗộ]/gi, 'o');
    value = value.replace(/[úùủũụưứừửữự]/gi, 'u');
    value = value.replace(/[yýỳỷỹỵ]/gi, 'y');
    value = value.replace(/[đ]/gi, 'd');
    return value;
  }

  static trim(text: string) {
    return text.trim();
  }

  static trimToNull(value: string) {
    if (this.isNull(value)) {
      return null;
    } else {
      return value.trim();
    }
  }

  static trimNullToEmpty(value: string) {
    if (this.isNull(value)) {
      return '';
    } else {
      return value.trim();
    }
  }

  static numberWithCommas(x) {
    return x?.toString().replace(/\B(?=(\d\d\d)+(?!\d))/g, ',');
  }

  static numberWithCommasAndRound(x, fractionDigits) {
    const options = {
      minimumFractionDigits: fractionDigits,
      maximumFractionDigits: fractionDigits
    };
    return Number(x).toLocaleString('en', options);
  }

  static parseCode(value: string) {
    if (Utils.isStringEmpty(value)) {
      return '';
    }
    const i = value.lastIndexOf('-');
    if (i >= 0) {
      return value.substr(i + 1);
    }
    return value;
  }

  static truncate(value: string, limit = 30, completeWords = false, ellipsis = '...') {
    if (value.length < limit) return `${value.substr(0, limit)}`;

    if (completeWords) {
      limit = value.substr(0, limit).lastIndexOf(' ');
    }
    return `${value.substr(0, limit)}${ellipsis}`;
  }

  static getParams(params) {
    const data = {};
    Object.keys(params).forEach((key) => (data[key] = params[key]));
    return data;
  }
  static convertUTCTimeToTimeStamp12(date) {
    const hour = date.getHours() === 0 ? 12 : date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
    const min = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
    const ampm = date.getHours() < 12 ? 'AM' : 'PM';
    const time = hour + ':' + min + ' ' + ampm;
    return time;
  }
  static getLastDayInMonth(month: string): string {
    const currentDate = new Date();
    const currentMonth = formatDate(currentDate, 'yyyyMM', 'en-US');
    if (month === currentMonth) {
      const yesterday = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - 1);
      return formatDate(yesterday, 'yyyyMMdd', 'en-US');
    } else {
      const lastDayOfMonth = new Date(parseInt(month.substring(0, 4)), parseInt(month.substring(4)), 0);
      return formatDate(lastDayOfMonth, 'yyyyMMdd', 'en-US');
    }
  }
}
