export enum Actions {
  CREATE,
  UPDATE,
  VIEW,
  DELETE,
  SWITCH,
  CHECKBOX,
  LOGOUT,
  CHOOSE_ITEM,
}

export enum ProcessType {
  LEAD = 'LEAD',
  OPPORTUNITY = 'OPPORTUNITY',
}

export enum TaskType {
  LEAD = 'LEAD',
  OPPORTUNITY = 'OPPORTUNITY',
  CAMPAIGN = 'CAMPAIGN',
  ACCOUNT = 'ACCOUNT',
  CONTACT = 'CONTACT',
  CUSTOMER = 'CUSTOMER',
  PG_KH = 'PG_KH',
  TODO = 'TD',
}
export enum TodoType {
  PARENT = 'PARENT',
  CHILD = 'CHILD',
}

export enum CalendarType {
  CALENDAR = 'CALENDAR',
  TODO = 'TD',
  ACTIVITY = 'HD',
}

export enum StatusTask {
  IMPORT_PENDING = 'IMPORT_PENDING',
  IMPORT_SUCCESS = 'IMPORT_SUCCESS',
  WAITING_APPROVE = 'WAITING_APPROVE',
  WAITING_ASSIGN = 'WAITING_ASSIGN',
  ASSIGN_BRANCH = 'ASSIGN_BRANCH',
  ASSIGN_PGD = 'ASSIGN_PGD',
  ASSIGN_TEAM = 'ASSIGN_TEAM',
  ASSIGN_RM = 'ASSIGN_RM',
  ABORT = 'ABORT',
  STARTED = 'STARTED',
  COMPLETED = 'COMPLETED',
  DONE = 'DONE',
  PENDING = 'PENDING',
  NEW = 'NEW',
  REJECTED = 'REJECTED',
  OPEN = 'OPEN',
}

export enum StatusLead {
  NEW = 'NEW',
  CONTACTED = 'CONTACTED',
  WORKING = 'WORKING',
  UNQUALIFIED = 'UNQUALIFIED',
  QUALIFIED = 'QUALIFIED',
  CONVERTED = 'CONVERTED',
}

export const leadType = {
  assigned: 'ASSIGN',
  followUp: 'FOLLOWUP',
  support: 'SUPPORT',
};

export enum StatusTaskOpportunity {
  NEW = 'NEW',
  MEETING = 'MEETING',
  PRESENTATION = 'PRESENTATION',
  PROPOSAL = 'PROPOSAL',
  NEGOTIATION = 'NEGOTIATION',
  SENT_APPROVE = 'SENT_APPROVE',
  WAITING_APPROVE = 'WAITING_APPROVE',
  LOST = 'LOST',
  WON = 'WON',
  REJECT = 'REJECT',
}

export enum Roles {
  TL = 'TL',
  SeRM = 'SeRM',
  RM = 'RM',
  RGM = 'RGM',
  RDM = 'RDM',
  OP = 'OP',
  ED = 'ED',
  BODS = 'BODS',
  BOD = 'BOD',
  BM = 'BM',
  BDM = 'BDM',
  ADM = 'ADM',
}

export enum ServiceType {
  SALE,
  WORKFLOW,
  CATEGORY,
  ACTIVITY,
}

export enum SocketClientState {
  ATTEMPTING,
  CONNECTED,
}

export const FieldType = {
  textbox: 'textbox',
  textarea: 'textarea',
  date: 'date',
  checkbox: 'checkbox',
  radio: 'radio',
  select: 'select',
};

export enum FieldStyle {
  BLOCK,
  INLINE,
}

export enum DateType {
  DATE,
  HAS_HOUR,
}

export const DescriptionTypeTask = {
  campaign: 'Chiến dịch',
  lead: 'Khách hàng',
  task: 'Việc cần làm',
};

export const DescriptionStatusTask = {
  success: 'Import thành công',
  pending: 'Đang xử lý',
  abort: 'Import lỗi',
  assign: 'Chờ phân công',
  started: 'Cần thực hiện',
  completed: 'Đã thực hiện',
  done: 'Đã hoàn thành',
};

export enum CampaignStatus {
  InProgress = 'IN_PROGRESS',
  Activated = 'ACTIVATED',
  WaitingConfirm = 'WAITING_CONFIRM',
  Approved = 'APPROVED'
  // Expired = 'EXPIRED',
}
export enum ReductionProposalStatus {
  InProgress = 'IN_PROGRESS',
  Activated = 'ACTIVATED',
  ACCEPTED = 'ACCEPTED',
  WAITING_CONFIRM = 'WAITING_CONFIRM',
  CONFIRM = 'CONFIRM',
  REJECT = 'REJECT',
  MANAGER_REJECT = 'MANAGER_REJECT',
  CANCEL = 'CANCEL',
  APPROVED = 'APPROVED',
  CBQL_REFUSED_CONFIRM = 'CBQL_REFUSED_CONFIRM'
}

export const productStatus = {
  active: {
    description: 'Active',
    code: '1',
  },
  inActive: {
    description: 'InActive',
    code: '0',
  },
};

export const ProgressBarClass = ['progress--blue', 'progress--yellow', 'progress--red', 'progress--green'];

export enum RoomType {
  Channel = 'c',
  Group = 'p',
  Direct = 'd',
}

export enum RoomPath {
  Channel = 'channel',
  Group = 'group',
  Direct = 'direct',
}

export enum ActivityType {
  Call = 'CALL',
  Meeting = 'MEETING',
  SMS = 'SMS',
  Email = 'EMAIL',
  Chat = 'CHAT',
}

export const dataLaneLead = [
  { id: 'new', name: 'Chưa liên hệ', index: 0, isDisabled: false, isFilter: false },
  { id: 'waiting', name: 'Đang liên hệ', index: 1, isDisabled: false, isFilter: false },
  { id: 'unqualified', name: 'Không có nhu cầu', index: 2, isDisabled: false, isFilter: false },
  { id: 'qualified', name: 'Có nhu cầu', index: 3, isDisabled: true, isFilter: false },
];

export const gender = {
  male: {
    code: 'MALE',
    name: 'Nam',
  },
  female: {
    code: 'FEMALE',
    name: 'Nữ',
  },
};

export const maxInt32 = 2147483647;
export const defaultExportExcel = 200000;
export const typeExcel = [
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  'application/vnd.ms-excel',
  'text/csv',
];

export const typeDoc = [
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  'application/msword'
];

export const componentDashboards = {
  leadStatistic: {
    component: 'leadStatistic',
    cols: 2,
    rows: 1,
    image: 'lead_report.jpg',
    type: 'lead',
  },
  activityCallSuccess: {
    component: 'activityCallSuccess',
    cols: 1,
    rows: 2,
    image: 'activity_call_report.jpg',
    type: 'lead',
  },
  opportunityWonReport: {
    component: 'opportunityWonReport',
    cols: 1,
    rows: 2,
    image: 'opportunity_report.jpg',
    type: 'lead',
  },
  activityWeeklyReport: {
    component: 'activityWeeklyReport',
    cols: 2,
    rows: 3,
    image: 'activity_chart_report.jpg',
    type: 'lead',
  },
  activityUserRank: {
    component: 'activityUserRank',
    cols: 2,
    rows: 3,
    image: 'team_rate_report.jpg',
    type: 'lead',
  },
};

export enum HttpRespondCode {
  EXIST_CODE = '001',
}

export enum Scopes {
  CREATE = 'CREATE',
  UPDATE = 'UPDATE',
  DELETE = 'DELETE',
  VIEW = 'VIEW',
  IMPORT = 'CREATE_BY_FILE',
  DOWNLOAD_TEMPLATE = 'DOWNLOAD_TEMPLATE',
  EXPORT = 'EXPORT',
  LOG = 'VIEW_HISTORY',
  APPROVE = 'APPROVE',
  ASSIGN_KVH = 'ASSIGN_KVH',
  UNASSIGN_KVH = 'UNASSIGN_KVH',
  RM_GRANT = 'RM_GRANT',
  UNLOCKUSER = 'UNLOCKUSER',
  RESTORE = 'RESTORE',
  REVENUE_SHARE = 'REVENUE_SHARE',
  ONLY_OPN = 'ONLY_OPN',
  HO = 'HO_PERMISSION',
  UPDATE_SME = 'UPDATE_SME'
}

export enum FunctionCode {
  KPI_ASSIGNMENT_RM_DETAIL = 'KPI_ASSIGNMENT_RM_DETAIL',
  KPI_ASSIGNMENT_CBQL_DETAIL = 'KPI_ASSIGNMENT_CBQL_DETAIL',
  KPI_STANDARD_CBQL_DETAIL = 'KPI_STANDARD_CBQL_DETAIL',
  VIEW_KPI_STANDARD_CBQL = 'VIEW_KPI_STANDARD_CBQL',
  KPI_STANDARD_CBQL = 'KPI_STANDARD_CBQL',
  SALEKIT_APPROVE = 'SALEKIT_APPROVE',
  SALEKIT_INFO_DECLARE = 'SALEKIT_INFO_DECLARE',
  KPI_STANDARD_RM = 'KPI_STANDARD_RM',
  KPI_CATEGORY_BY_TITLE = 'KPI_CATEGORY_BY_TITLE',
  KPI_ITEM = 'KPI_ITEM',
  KPI_FACTOR = 'KPI_FACTOR',
  KPI_FACTOR_AREA = 'KPI_FACTOR_AREA',
  KPI_FACTOR_BRANCHTIME = 'KPI_FACTOR_BRANCHTIME',
  KPI_TIME_FACTOR = 'KPI_TIME_FACTOR',
  ADMIN_APP = 'ADMIN_APP',
  ADMIN_SESSIONS = 'ADMIN_SESSIONS',
  ADMIN_COMMON = 'ADMIN_COMMON',
  ADMIN_BRANCH = 'ADMIN_BRANCH',
  ADMIN_BLOCK = 'ADMIN_BLOCK',
  ADMIN_FUNCTION = 'ADMIN_FUNCTION',
  ADMIN_SCOPES = 'ADMIN_SCOPES',
  ADMIN_USER = 'ADMIN_USER',
  ADMIN_ROLE = 'ADMIN_ROLE',
  ADMIN_USER_ROLE = 'ADMIN_USER_ROLE',
  ADMIN_PERMISSION = 'ADMIN_PERMISSION',
  ADMIN_LOG = 'ADMIN_LOG',
  ADMIN_DATA_DOMAIN = 'ADMIN_DATA_DOMAIN',
  ADMIN_PERMISSION_ROLE = 'ADMIN_PERMISSION_ROLE',
  RM_MANAGER = 'RM_MANAGER',
  RM_BLOCK = 'RM_BLOCK',
  RM_TITLE = 'RM_TITLE',
  RM_LEVEL = 'RM_LEVEL',
  RM_GROUP = 'RM_GROUP',
  HUDDLE_GROUP = 'GROUP_HUDDLE',
  CUSTOMER_360_MANAGER = 'KH_MANAGER',
  CUSTOMER_360_OPERATION_BLOCK_MANAGER = 'KH_OPERATION_BLOCK_MANAGER',
  CUSTOMER_360_ASSIGNMENT = 'CUSTOMER_ASSIGNMENT_NORMAL',
  CUSTOMER_360_ASSIGNMENT_APPROVED = 'CUSTOMER_360_ASSIGNMENT_APPROVED',
  CUSTOMER_360_CONFIRM_ASSIGN = 'CONFIRM_ASSIGN_RM',
  RM_FLUCTUATE = 'RM_FLUCTUATE',
  TASK = 'TASKS',
  CALENDAR = 'CALENDAR',
  CALENDARSME = 'CALENDARSME',
  SALES_TRACKING_REPORT = 'SALES_TRACKING_REPORT',
  SALES_MONEY_TRANSFER = 'MANAGER_BLOCK_TTR_TTQT',
  LEAD = 'LEAD',
  DASHBOARD_CUSTOMER = 'DASHBOARD_CUSTOMER',
  DASHBOARD_CUSTOMER2 = 'DASHBOARD_CUSTOMER_2',
  PRODUCT = 'PRODUCT',
  PROCESS_MANAGEMENT = 'PROCESS_MANAGEMENT',
  EXPORT_DATA = 'EXPORT_DATA',
  NOTE = 'NOTE',
  KPIS_MANAGEMENT = 'KPIS_MANAGEMENT',
  CAMPAIGN = 'CAMPAIGN',
  CAMPAIGN_RM = 'CAMPAIGN_RM',
  CAMPAIGN_MAPPING = 'CAMPAIGN_MAPPING',
  CAMPAIGN_TYPE = 'CAMPAIGN_TYPE',
  GROUP_RM_UNASSIGN = 'GROUP_RM_UNASSIGN',
  KPI_CBQL = 'KPI_CBQL',
  REPORT_KPI_INDIV = 'REPORT_KPI_INDIV',
  REPORT_KPI_NHS = 'REPORT_KPI_NHS',
  KPI_RM = 'KPI_RM',
  DEVICE_MANAGEMENT = 'DEVICE_MANAGEMENT',
  MIGRATE = 'MIGRATE',
  CUSTOMER_360_ASSIGNMENT_IMPORT = 'CUSTOMER_360_ASSIGNMENT_IMPORT',
  REPORT_KPI_PL = 'REPORT_KPI_PL',
  TOI_REPORT = 'TOI_REPORT',
  REPORT_MOBILIZATION = 'REPORT_MOBILIZATION',
  REPORT_MOBILIZATION_V2 = 'REPORT_MOBILIZATION_V2',
  REPORT_CREDIT = 'REPORT_CREDIT',
  REPORT_CREDIT_V2 = 'REPORT_CREDIT_V2',
  MARKETING = 'TEMPLATES',
  TICKETS = 'TICKET_MANAGEMENT',
  REPORT_PT_KH = 'REPORT_PT_KH',
  SALEKIT_CATEGORY_DECLARE = 'SALEKIT_CATEGORY_DECLARE',
  SALEKIT_CHARACTERISTIC_DECLARE = 'SALEKIT_CHARACTERISTIC_DECLARE',
  KPI_ASSIGNMENT_RM = 'KPI_ASSIGNMENT_RM',
  KPI_ASSIGNMENT_CBQL = 'KPI_ASSIGNMENT_CBQL',
  KPI_ASSIGNMENT_RECEIVE = 'KPI_ASSIGNMENT_RECEIVE',
  REVENUE_SHARE = 'REVENUE_SHARE',
  GROUP_ARM = 'GROUP_ARM',
  KPI_RM_GROUP_RELATIONSHIP = 'KPI_RM_GROUP_RELATIONSHIP',
  KPI_STANDARD_CBQL_BRANCH = 'KPI_STANDARD_CBQL_BRANCH',
  OPPORTUNITY = 'OPPORTUNITY',
  DASHBOARD = 'DASHBOARD',
  ASSIGN_LEAD = 'ASSIGN_LEAD',
  OPPORTUNITY_TARGET = 'OPPORTUNITY_TARGET',
  OPPORTUNITY_SALE = 'OPPORTUNITY_SALE',
  CONFIG_AP = 'SETUP_AP',
  REPORT_MAP_KH = 'REPORT_BANDOKHACHHANG',
  REPORT_MAP_KH_V2 = 'REPORT_BANDOKHACHHANG_V2',
  REPORT_NSLD_SME = 'REPORT_NSLD_SME',
  REPORT_TTTM = 'REPORT_TTTM',
  REPORT_TTTM_V2 = 'REPORT_TTTM_V2',
  SALE_SUGGEST_LIST = 'SALE_SUGGEST_LIST',
  REPORT_BAOLANH = 'REPORT_BAOLANH',
  ACCOUNT_PLANNING = 'ACCOUNT_PLANNING',
  APPROVED_PLAN = 'APPROVED_PLAN',
  REPORT_BUSINESS_BRANCH = 'REPORT_BUSINESS_BRANCH',
  SELLING_OPPORTUNITY_INDIV = 'SELLING_OPPORTUNITY_INDIV',
  CUSTOMER_360_ASSIGN_BY_PRODUCT = 'CUSTOMER_360_ASSIGN_BY_PRODUCT',
  WARNING_KPI = 'WARNING_KPI',
  GROUP_ARM_UNASSIGN = 'GROUP_ARM_UNASSIGN',
  QUERY_MBAL = 'QUERY_MBAL',
  COACHING = 'COACHING',
  COACHING_SUGGEST = 'COACHING_SUGGEST',
  WARNING_AP = 'WARNING_AP',
  REPORT_AP = 'REPORT_AP',
  ADDRESS = 'ADDRESS',
  INFO_BASIC = 'INFO_BASIC',
  PLAN_AFTER_COACHING = 'PLAN_AFTER_COACHING',
  COACHING_SUGGEST_RESULT_IMPORT = 'COACHING_SUGGEST_RESULT_IMPORT',
  MICROLENDING = 'MICROLENDING',
  CUSTOMER_360_ASSIGN_LD_RM = 'CUSTOMER_360_ASSIGN_LD_RM',
  BIDDING_LIST = 'BIDDING_LIST',
  LIMIT_CUSTOMER = 'LIMIT_CUSTOMER',
  SUPPORT_RM = 'SUPPORT_RM',
  SUPPORT_RM_HOTLINE = 'SUPPORT_RM_HOTLINE',
  IMAGE_MANAGER = 'IMAGE_MANAGER',
  UPLOAD_IMAGE = 'UPLOAD_IMAGE',
  ASSIGN_CUSTOMER_BY_RM_TTTM = 'ASSIGN_CUSTOMER_BY_RM_TTTM',
  LEAD_KHCN = 'LEAD_KHCN',
  APP_CHAT = 'APP_CHAT',
  WARNING = 'WARNING',
  REPORT_ASSIGN_EXCEPTIONAL = 'REPORT_ASSIGN_EXCEPTIONAL',
  REPORT_BIZ_CUSTOMER = 'REPORT_BIZ_CUSTOMER',
  REPORT_CUSTOMER_SALES = 'REPORT_CUSTOMER_SALES',
  REPORT_GUARANTEE_V2 = 'REPORT_GUARANTEE_V2',
  REPORT_APP = 'REPORT_APP',
  EXTENSIONS = 'EXTENSIONS',
  SALEKIT = 'SALEKIT',
  REDUCTION_PROPOSAL = 'REDUCTION_PROPOSAL',
  TARGET_SALE_CONFIG = 'TARGET_SALE_CONFIG',
  NEWS = 'NEWS',
  REPORT_DELETE_GAP = 'REPORT_DELETE_GAP',
  DASHBOARD_MANAGE_CARD = 'DASHBOARD_MANAGE_CARD',
  APP_ACTIVE_CHART_DETAIL = 'APP_ACTIVE_CHART_DETAIL',
  BIRTHDAY_WARNING = 'BIRTHDAY_WARNING',
  REPORT_PL = 'REPORT_PL',
  REPORT_TOI_V2 = 'REPORT_TOI_V2',
  KPI_STANDARD_CBQL_BRANCH_V2 = 'KPI_STANDARD_CBQL_BRANCH_V2',
  REPORT_CUSTOMER_INACTIVE = 'REPORT_CUSTOMER_INACTIVE',
  CHART_CAMPAIGN_RESULT_DETAIL = 'CHART_CAMPAIGN_RESULT_DETAIL',
  CHART_CAMPAIGN_STAGE_DETAIL = 'CHART_CAMPAIGN_STAGE_DETAIL',
  CASH_FLOW_CUSTOMER = 'CASH_FLOW_CUSTOMER',
  DEBT_CLAIM = 'DEBT_CLAIM',
  SALE_OPPORTUNITY_APP = 'SALE_OPPORTUNITY_APP',
  RATING_FUNCTION_REPORT = 'RATING_FUNCTION_REPORT',
  REPORT_SPDV_SME = 'REPORT_SPDV_SME',
  SYSTEM_REPORT = 'SYSTEM_REPORT',
  CUSTOMER_MANAGEMENT = 'CUSTOMER_MANAGEMENT',
  CUSTOMER_DETAIL='CUSTOMER_DETAIL'
}

export const functionUri = {
  access_denied: '/access-denied',
  page_not_pound: '/page-not-pound',
  dashboard: '/dashboard',
  admin_user: '/system/users-config',
  admin_sessions: '/system/users-sessions',
  admin_app: '/system/applications-config',
  admin_common: '/system/common-category',
  admin_branch: '/system/branches-category',
  admin_block: '/system/blocks-category',
  admin_function: '/system/resources-category',
  admin_scopes: '/system/manipulation-category',
  admin_role: '/system/role-category',
  admin_user_role: '/system/user-role-config',
  admin_log: '/system/log-history',
  admin_permission: '/system/permission-config',
  admin_data_domain: '/system/user-permission-config',
  process_management: '/system/process-management',
  rm_360_manager: '/rm360/rm-manager',
  rm_block: '/rm360/rm-block',
  rm_title: '/rm360/title-category',
  rm_level: '/rm360/level-category',
  rm_group: '/rm360/rm-group',
  customer_360_manager: '/customer360/customer-manager',
  customer_360_operation_block_manager: '/customer360/operation-block',
  customer_360_assignment: '/customer360/customer-assignment',
  customer_360_assignment_approved: '/customer360/customer-assignment-approve',
  customer_360_dashboard: '/customer360/dashboard',
  customer_360_dashboard2: '/customer360/dashboard2',
  rm_fluctuate: '/rm360/rm-fluctuate',
  task: '/tasks',
  calendar: '/calendar',
  lead_management: '/lead/lead-management',
  lead_assign: '/lead/lead-assignment',
  product: '/products',
  campaign: '/campaigns/campaign-management',
  campaign_detail: '/campaigns/campaign-management/detail',
  campaign_rm: '/campaigns/campaign-rm',
  device_rm: 'rm360/device',
  tickets_manager: 'tickets/ticket-management',
  kpi_config: '/kpis/kpi-management/kpi-config/',
  rm_group_arm: '/rm360/arm',
  revenue_share: '/customer360/revenue-sharing',
  sale_target: '/sale-manager/sale-target-list',
  sale_transfer: '/sale-manager/sale-transfer-list',
  sale_opportunity: 'sale-manager/sale-manager-list',
  sale_suggest: '/sale-manager/sale-suggest-list',
  report_business_branch: '/report-management/report-business-branch',
  selling_opp_indiv: '/sale-manager/selling-opp-indiv',
  customer_360_assign_by_product: '/customer360/assign-by-product',
  query_mbal: '/report-management/query-mbal',
  reduction_proposal: '/sale-manager/reduction-proposal',
  reduction_fee_exception: '/sale-manager/fee-exception',
  reduction_interest_exception: '/sale-manager/interest-exception',
};

export const LEVEL_TTTM = ['RM_TTTM_LV2', 'RM_TTTM', 'RM_TTTM_LV3'];

export enum AgeGroup {
  AGE_LESS_18 = 'AGE_LESS_18',
  AGE_BETWEEN_18_25 = 'AGE_BETWEEN_18_25',
  AGE_BETWEEN_26_35 = 'AGE_BETWEEN_26_35',
  AGE_BETWEEN_36_50 = 'AGE_BETWEEN_36_50',
  AGE_BETWEEN_51_60 = 'AGE_BETWEEN_51_60',
  AGE_BETWEEN_OVER_60 = 'AGE_BETWEEN_OVER_60',
}

export enum RespondCodeError {
  CDRM002 = 'CDRM002',
  CDRM003 = 'CDRM003',
  LVRM001 = 'LVRM001',
  LVRM002 = 'LVRM002',
  CRM6192 = 'err.api.errorcode.CRM6192',
}

export enum SourceOp {
  SMARTBANK = 'SMARTBANK',
  CAMPAIGN = 'CAMPAIGN'
}

export enum ConfirmType {
  Success = 5,
  Warning = 2,
  Confirm = 3,
  CusError = 4,
}

export enum PriorityTaskOrCalendar {
  High = 'HIGH',
  Medium = 'MEDIUM',
  Low = 'LOW',
}

export enum RmType {
  RM = 'RM',
  aRM = 'aRM',
}

export enum Format {
  CusDate = 'dd/MM/yyyy',
  DateUp = 'DD/MM/YYYY',
  DateTime = 'HH:mm dd/MM/yyyy',
  DateTimeUp = 'HH:mm DD/MM/YYYY',
}

export enum CustomerType {
  INDIV = 'INDIV',
  NHS = 'NHS',
  COR = 'COR',
  SME = 'SME',
  CIB = 'CIB',
}

export enum ChartJsType {
  Pie = 'pie',
  Bar = 'bar',
  Line = 'line',
  Doughnut = 'doughnut',
}

export enum EChartType {
  Pie = 'pie',
  Bar = 'bar',
  Line = 'line',
  Doughnut = 'doughnut',
}

export enum ScreenType {
  Create = 'CREATE',
  Update = 'UPDATE',
  Detail = 'DETAIL',
}

export enum ScreenName {
  ActivityHistory = 'ActivityHistory',
}

export enum ServiceProductType {
  Credit = 'Credit',
  Mobilization = 'Mobilization',
  Card = 'Card',
  Digital = 'Digital',
  Group = 'Group',
}

export enum CreditProductGroup {
  Housing = '1_Housing',
  HousingProject = '2_Housing_Project',
  Stock = '3_Stock',
  Car = '4_Car',
  LetterValue = '5_Letter_Value',
  Coll = '6_Consumer_Coll',
  TDKC_TSBD = '7_Consumer_Non_Coll',
  SXKD = '8_Consumer_Bussiness',
  Salary = '9_Salary',
  Overdraft = '10_Overdraff',
  Other = '11_Others',
}

export enum MobilizationChart {
  CasaTD = '100',
  CasaBQ = 'CASA_BQ',
  TKCKHTD = '101',
}

export enum DigitalChart {
  EMB = '141',
  Sms = '142',
  App = '143',
}

export enum ComprodChart {
  MBAL = '171',
  SPDT = '172',
}

export enum CardChart {
  ActivePlus = 'ACTIVEPLUS',
  Debit = 'DEBIT',
  Jcb = 'JCB',
  VisaCredit = 'VISA_CREDIT',
  VisaBank = 'VISA_BANK_PLUS',
  VisaVin = 'VISA_VIN_GRP',
  // VisaVlin = '',
  // VisaViettel = '',
  // ActivePlus = '',
  // Debit = '',
}

export const rolesDefault = ['offline_access', 'uma_authorization'];

export const CommonCategory = {
  MAX_RM_SEARCH_DETAIL_CAMPAIGN: 'MAX_RM_SEARCH_DETAIL_CAMPAIGN',
  DB_SCHEMA: 'DB_SCHEMA',
  CAMPAIGN_TYPE: 'CAMPAIGN_TYPE',
  CAMPAIGN_MONTH_CONFIG_BEFORE: 'CAMPAIGN_MONTH_CONFIG_BEFORE',
  CAMPAIGN_FORM: 'CAMPAIGN_FORM',
  CONFIG_ACTIVITY_RESULT: 'CONFIG_ACTIVITY_RESULT',
  CONFIG_TYPE_ACTIVITY: 'CONFIG_TYPE_ACTIVITY',
  CONFIG_BACK_DATE: 'CONFIG_BACK_DATE',
  STATUS_CUSTOMER_CONFIG: 'STATUS_CUSTOMER_CONFIG',
  SEGMENT_CUSTOMER_CONFIG: 'SEGMENT_CUSTOMER_CONFIG',
  CONFIG_EXPORT_CUSTOMER_LIMIT_COUNT: 'CONFIG_EXPORT_CUSTOMER_LIMIT_COUNT',
  CUSTOMER_OBJECT_CONFIG: 'CUSTOMER_OBJECT_CONFIG',
  KH_PHONE_NO_CONFIG: 'KH_PHONE_NO_CONFIG',
  KH_EMAIL_CONFIG: 'KH_EMAIL_CONFIG',
  ACCOUNT_GROUP: 'ACCOUNT_GROUP',
  QUICKSEARCH_TYPE: 'QUICKSEARCH_TYPE',
  EMPLOYEE_CHANGE: 'EMPLOYEE_CHANGE',
  KPI_SYS_ORG_TYPE: 'KPI_SYS_ORG_TYPE',
  KPI_LOCATION: 'KPI_LOCATION',
  ACTIVITY_NEXT_TYPE: 'ACTIVITY_NEXT_TYPE',
  ASSET_TYPE: 'ASSET_TYPE',
  ASSET_GROUP: 'ASSET_GROUP',
  AGE_GROUP: 'AGE_GROUP',
  PRIVATE_PRIORITY_CONFIG: 'PRIVATE_PRIORITY_CONFIG',
  EXT_CAMPAIGNS_CHANNELS: 'EXT_CAMPAIGNS_CHANNELS',
  CAMPAIGN_DIVISION: 'CAMPAIGN_DIVISION',
  SOURCE_DATA: 'SOURCE_DATA',
  KPI_YEARS: 'KPI_YEARS',
  SPDV_THE: 'CARDTYPE_SPDV_CONFIG',
  PROCESS_MANAGEMENT_STATUS: 'PROCESS_MANAGEMENT_STATUS',
  APP_MANAGEMENT: 'APP_MANAGEMENT',
  REALM_ROLE_MANAGEMENT: 'REALM_ROLE_MANAGEMENT',
  EXPORT_CUSTOMER: 'EXPORT_CUSTOMER',
  IMPORT_LEAD: 'IMPORT_LEAD',
  CURRENCY_UNIT_REPORTS: 'CURRENCY_UNIT_REPORTS',
  REPORTS_YEARS: 'REPORTS_YEARS',
  BAOCAO_PL_RM_ROLEADMIN: 'BAOCAO_PL_RM_ROLEADMIN',
  TERM_TYPE: 'TERM_TYPE ',
  CURRENCY_TYPE: 'CURRENCY_TYPE',
  REPORTS_MAX_LENGTH_RM: 'REPORTS_MAX_LENGTH_RM',
  DASHBOARD_LIST: 'DASHBOARD_LIST',
  SALEKIT_APPROVE_STATUS: 'SALEKIT_APPROVE_STATUS',
  KHOI_PRIORITY: 'KHOI_PRIORITY',
  //f-marketing
  SMART_CHAT_MARKET_TYPE: 'SMART_CHAT_MARKET_TYPE',
  SMART_CHAT_MARKET_STATUS: 'SMART_CHAT_MARKET_STATUS',
  SMART_CHAT_MARKET_CATEGORY: 'SMART_CHAT_MARKET_CATEGORY',
  SMART_CHAT_MARKET_CUSTOMER_CATEGORY: 'SMART_CHAT_MARKET_CUSTOMER_CATEGORY',
  //f-marketing
  SHARE_REVENUE_LINE: 'SHARE_REVENUE_LINE',
  SHARE_ITEM: 'SHARE_ITEM',
  ACC_REVENUE_SHARING_ST: 'ACC_REVENUE_SHARING_ST',
  SHARE_REVENUE_ROLE_INPUT: 'SHARE_REVENUE_ROLE_INPUT',
  MAX_RM_REPORT_CREDIT: 'MAX_RM',
  MAX_RM_REPORT_CUSTOMER: 'BAOCAO_PTKH',
  SHARE_REVENUE_ROLE_APPROVE_EMAIL: 'SHARE_REVENUE_ROLE_APPROVE_EMAIL',
  MAX_CUSTOMER_REPORT: 'MAX_CUSTOMER',
  LEAD_STATUS: 'LEAD_STATUS',
  LEAD_LEVEL: 'LEAD_LEVEL',
  LEAD_FILTER: 'LEAD_FILTER',
  LEAD_SOURCE: 'LEAD_SOURCE',
  LEAD_TYPE: 'LEAD_TYPE',
  LIMIT_CUSTOMER_KHTN: 'LIMIT_CUSTOMER_KHTN',
  LEAD_ORG_TYPE: 'LEAD_ORG_TYPE',
  OPP_DIVISION: 'OPP_DIVISION',
  OPP_STATUS: 'OPP_STATUS',
  OPPORTUNITY_STATUS: 'OPPORTUNITY_STATUS',
  DASHBOARD_MANAGERMENT: 'DASHBOARD_MANAGERMENT',
  TARGET_STATUS: 'TARGET_STATUS',
  REVENUE_CUSTOMER_SEGMENT: 'REVENUE_CUSTOMER_SEGMENT',
  SALE_STATUS: 'SALE_STATUS',
  AP_KPI_UNIT: 'AP_KPI_UNIT',
  AP_TARGET: 'AP_TARGET',
  AP_TARGET_LEVEL: 'CONFIG_TARGET_LEVEL_AP',
  DASHBOARD_BUSINESS_COLOR: 'DASHBOARD_BUSINESS_COLOR',
  CONFIG_COLOR_TINDUNG_TD: 'TINDUNG_TD',
  CONFIG_COLOR_HUYDONGVON_TD: 'HUYDONGVON_TD',
  // sale-suggest
  SEGMENT_SUGGEST_SELLING_PRODUCT: 'SEGMENT_SUGGEST_SELLING_PRODUCT',
  CUSTOMER_TYPE_SUGGEST_SELLING_PRODUCT: 'CUSTOMER_TYPE_SUGGEST_SELLING_PRODUCT',
  AGE_SUGGEST_SELLING_PRODUCT: 'AGE_SUGGEST_SELLING_PRODUCT',
  CUSTOMER_USED_PRODUCT_SUGGEST_SELLING_PRODUCT: 'CUSTOMER_USED_PRODUCT_SUGGEST_SELLING_PRODUCT',
  CUSTOMER_NOT_USED_PRODUCT_SUGGEST_SELLING_PRODUCT: 'CUSTOMER_NOT_USED_PRODUCT_SUGGEST_SELLING_PRODUCT',
  AP_STATUS: 'AP_STATUS',
  SECTOR_SUGGEST_SELLING_PRODUCT: 'SECTOR_SUGGEST_SELLING_PRODUCT',
  LIMIT_IMPORT_CUSTOMER_TO_CAMPAIGN: 'LIMIT_IMPORT_CUSTOMER_TO_CAMPAIGN',
  CATEGORY_KPI: 'CATEGORY_KPI',
  //CALENDAR
  CALENDAR_CONFIG: 'CALENDAR_CONFIG',
  CALENDAR_ALERT: 'CALENDAR_ALERT',
  MAX_IMPORT_AP: 'MAX_IMPORT_AP',
  RM_GROUP_TITLE: 'RM_GROUP_TITLE',
  //PRODUCT
  PRODUCT_DELIVERED: 'PRODUCT_DELIVERED',
  PRODUCT_DELIVERED_NOT_ASSIGN: 'PRODUCT_DELIVERED_NOT_ASSIGN',
  MAX_BRANCH: 'MAX_BRANCH',
  MAX_RM: 'MAX_RM',
  MAX_CUSTOMER: 'MAX_CUSTOMER',
  CONTACT_POSITION: 'CONTACT_POSITION',
  CUSTOMERS_ASSIGNMENT_EXCEPTION_REASON: 'CUSTOMERS_ASSIGNMENT_EXCEPTION_REASON',
  BANNER_CONFIG: 'BANNER_CONFIG',
  CUSTOMERS_ASSIGNMENT_RM_TTTM_PRODUCTS: 'CUSTOMERS_ASSIGNMENT_RM_TTTM_PRODUCTS',
  SHARE_REVENUE: 'SHARE_REVENUE',
  POTENTIAL_RESOURCE: 'POTENTIAL_RESOURCE',
  POTENTIAL_INCOME_PER_MONTH: 'POTENTIAL_INCOME_PER_MONTH',
  POTENTIAL_LEVEL: 'POTENTIAL_LEVEL',
  // QL HẠN MỨC
  LIMIT_TYPE: 'LIMIT_TYPE',
  MINING_RATE_LIMIT: 'MINING_RATE_LIMIT',
  DUE_DATE_WARNING: 'DUE_DATE_WARNING',
  WARNING_TYPE_LIST: 'WARNING_TYPE_LIST',
  // tableau
  TABLEAU_MAX_RM_REPORT: 'TABLEAU_MAX_RM_REPORT',
  TABLEAU_MAX_BRANCH_REPORT: 'TABLEAU_MAX_BRANCH_REPORT',
  TABLEAU_MAX_CUSTOMER_REPORT: 'TABLEAU_MAX_CUSTOMER_REPORT',
  CONFIG_REPORT_TARGET_TYPE: 'CONFIG_REPORT_TARGET_TYPE',
  CONFIG_REPORT_CHANNEL: 'CONFIG_REPORT_CHANNEL',
  REGIONAL_DIRECTOR: 'REGIONAL_DIRECTOR',
  CONFIG_REPORT_CUSTOMER_SALES_TYPE: 'CONFIG_REPORT_CUSTOMER_SALES_TYPE',
  CAMPAIGN_APPROACHING_RESULT: 'CAMPAIGN_APPROACHING_RESULT',
  HINT_NOTE_CAMPAIGN: 'HINT_NOTE_CAMPAIGN',
  CAMPAIGN_SME_EDITABLE_ROLES: 'CAMPAIGN_SME_EDITABLE_ROLES',
  // Báo cáo app
  APP_DAILY: 'APP_DAILY',
  DEVELOPMENT_CHANNEL: 'DEVELOPMENT_CHANNEL',
  MAX_RM_REPORT_APP: 'MAX_RM_REPORT_APP',
  MAX_BRANCH_REPORT_APP: 'MAX_BRANCH_REPORT_APP',
  // Reduction proposal
  REDUCTION_PROPOSAL_STATUS: 'REDUCTION_PROPOSAL_STATUS',
  TARGET_SALE_CONFIG_PRODUCT: 'TARGET_SALE_CONFIG_PRODUCT',
  WARNING_TYPE_LIST_INDIV: 'WARNING_TYPE_LIST_INDIV',
  DUE_STATUS_LIST_INDIV: 'DUE_STATUS_LIST_INDIV',
  DUE_DATE_WARNING_INDIV: 'DUE_DATE_WARNING_INDIV',
  GROUP_DEBT: 'GROUP_DEBT',
  NEWS_FEATURE_CONFIG: 'NEWS_FEATURE_CONFIG',
  NEWS_NUMBER_CONFIG: 'NEWS_NUMBER_CONFIG',
  S3_REPORT_CONFIG: 'S3_REPORT_CONFIG',
  REPORT_CREDIT_INDIV_LIMIT_DAY: 'REPORT_CREDIT_INDIV_LIMIT_DAY',
  EVENT_NEWS_CHANNEL: 'EVENT_NEWS_CHANNEL',
  APPLICATION_CHANNEL: 'APPLICATION_CHANNEL',
  BIDDING_STATUS: 'BIDDING_STATUS',
  CUSTOMER_BIDDING_STATUS: 'CUSTOMER_BIDDING_STATUS',
  CUSTOMER_BIDDING_TYPE: 'CUSTOMER_BIDDING_TYPE',
  REDUCTION_PROPOSAL_AUTH_CONFIRM: 'REDUCTION_PROPOSAL_AUTH_CONFIRM',
  DASHBOARD_CUSTOMER_STATUS: 'DASHBOARD_CUSTOMER_STATUS',
  //Bao cao KPi
  TIME_LIFE_EXCEL_URL_CONFIG: 'TIME_LIFE_EXCEL_URL_CONFIG',
  EXPORT_KPI_KHCN_LIMIT: 'EXPORT_KPI_KHCN_LIMIT',
  CONFIG_PRODUCT_LIST_LEVEL_2: 'CONFIG_PRODUCT_LIST_LEVEL_2',
  CUSTOMER_INACTIVE_CUSTOMER_TYPE: 'CUSTOMER_INACTIVE_CUSTOMER_TYPE',
  LIST_BATCH: 'LIST_BATCH',
  HDV_INDIV: 'HDV_INDIV',
  DUNO_INDIV: 'DUNO_INDIV',
  BUSINESS_SME_DEBT_GROUP: 'BUSINESS_SME_DEBT_GROUP',
  BUSINESS_SME_PERIOD: 'BUSINESS_SME_PERIOD',
  BUSINESS_SME_PRODUCT: 'BUSINESS_SME_PRODUCT',
  BUSINESS_SME_PERIOD_HDV: 'BUSINESS_SME_PERIOD_HDV',
  BUSINESS_SME_PRODUCT_GUARANTEE:'BUSINESS_SME_PRODUCT_GUARANTEE',
  SETUP_INTEREST_STATUS: 'SETUP_INTEREST_STATUS',
  REDUCTION_EXCEPTION_STATUS: 'REDUCTION_EXCEPTION_STATUS',
  REASON_EXCEPTION_REDUCTION: 'REASON_EXCEPTION_REDUCTION',
  FILTER_STATUS_SALE_OPPORTUNITY: 'FILTER_STATUS_SALE_OPPORTUNITY',
  SOURCE_SALE_OPPORTUNITY: 'SOURCE_SALE_OPPORTUNITY',
  SALE_OPPORTUNITY_SOURCE: 'SALE_OPPORTUNITY_SOURCE',
  STATUS_SALE_OPPORTUNITY: 'STATUS_SALE_OPPORTUNITY',
  SURVEY_QUESTION: 'SURVEY_QUESTION'
};

export const ConfigBackDate = {
  BACK_DATE_ACTIVITY: 'BACK_DATE_ACTIVITY',
};

export const TimeShowNotify = 10000;

export const LeadProsessName = 'app_crm_lead_process';


export const ChannelResource = 'ROLE,CRM';

export enum ProductLevel {
  Lvl0 = '0',
  Lvl1 = '1',
  Lvl2 = '2',
  Lvl3 = '3',
  Lvl4 = '4',
  Lvl5 = '5',
}

export enum CampaignProcessType {
  HO = 'HO',
  MB247 = 'MB247',
  MANAGER = 'MANAGER',
  RM = 'RM',
}

export const BRANCH_HO = 'VN0010001';

export enum ConstantEnum {
  LIST_FUNCTION = 'LIST_FUNCTION',
  LIST_FUNCTION_USER = 'LIST_FUNCTION_USER',
}

export enum CampaignSize {
  TOAN_HANG = 'TOAN_HANG',
  CHI_NHANH = 'CHI_NHANH',
}

export enum KyRM {
  MONTH = '0',
  DAY = '1',
}

export enum CustomerImportType {
  KH360 = 'KH360',
  TN = 'TN',
}

export enum SessionKey {
  CUSTOMER_TARGETS = 'CUSTOMER_TARGETS',
  DIVISION_OF_RM = 'DIVISION_OF_RM',
  USER_INFO = 'USER_INFO',
  TOKEN = 'TOKEN',
  PRODUCT_DETAIL_CUSTOMER = 'PRODUCT_DETAIL_CUSTOMER',
  CURRENT_LINK_ACTIVE = 'CURRENT_LINK_ACTIVE',
  MENU = 'MENU',
  SESSION_STATE = 'SESSION_STATE',
  IS_PERMISSION = 'IS_PERMISSION',
  LIMIT_EXPORT = 'LIMIT_EXPORT',
  COMMON_DATA_CUSTOMER_DETAILS = 'COMMON_DATA_CUSTOMER_DETAILS',
  PL_REPORTS = 'PL_REPORTS',
  TOI_REPORTS = 'TOI_REPORTS',
  TIME_REFRESH_TOKEN = 'TIME_REFRESH_TOKEN',
  REPORT_MOBILIZATION = 'REPORT_MOBILIZATION',
  REPORT_MOBILIZATION_V2 = 'REPORT_MOBILIZATION_V2',
  REPORT_APP = 'REPORT_APP',
  REPORT_CREDIT = 'REPORT_CREDIT',
  REPORT_PT_KH = 'REPORT_PT_KH',
  LIST_CUSTOMER_OF_RM = 'LIST_CUSTOMER_OF_RM',
  LIST_LEAD_OF_RM = 'LIST_LEAD_OF_RM',
  SHARE_REVENUE_ROLE_INPUT = 'SHARE_REVENUE_ROLE_INPUT',
  LIST_INDUSTRY = 'LIST_INDUSTRY',
  KHOI_PRIORITY = 'KHOI_PRIORITY',
  LAST_TIME_ACTION = 'LAST_TIME_ACTION',
  CUS_REPORT_FINANCE = 'CUS_REPORT_FINANCE',
  PROCESS_MANAGEMENT = 'PROCESS_MANAGEMENT',
  LIST_BRANCH = 'LIST_BRANCH',
  COMMON_DATA_CAMPAIGN_DETAILS = 'COMMON_DATA_CAMPAIGN_DETAILS',
}

export enum ExportKey {
  Customer = 'EX_CUSTOMER',
}

export enum ComponentType {
  SinglePage,
  Modal,
}

export enum CodeApp {
  MOBILE = 'APP',
}

export enum RequestMethod {
  Get = 'GET',
  Post = 'POST',
  Put = 'PUT',
  Delete = 'DELETE',
}

export enum NotifyType {
  Success = 'success',
  CusError = 'error',
  Info = 'info',
  Warning = 'warn',
}

export enum Division {
  INDIV = 'INDIV',
  CIB = 'CIB',
  DVC = 'DVC',
  SME = 'SME',
  FI = 'FI',
}

export enum SaleKit {
  SALE_KIT = 'SALE_KIT',
}

export enum TypeFieldProduct {
  STRING = 'STRING',
  CURRENCY = 'CURRENCY',
  DATE = 'DATE',
}

export enum StatusWork {
  NTS = 'NTS',
}

export const CryptoJSKey = '379C286AB22326D5B611DCB24ECC5';

export const ListCustomerType = [
  { code: 1, displayName: 'Khách hàng quản lý' },
  { code: 2, displayName: 'Khách hàng chốt sale' },
];

export const ListStatusKpiReport = [
  { code: 'PENDING', name: 'Đang xử lý' },
  { code: 'SUCCESS', name: 'Chưa tải' },
  { code: 'FAIL', name: 'File lỗi' },
  { code: 'DOWNLOADED', name: 'Đã tải' }
];
export const ListParamKpiReport = [
  { code: 'GROUP_ID', name: 'Sản phẩm' },
  { code: 'RM_CODE', name: 'Mã RM' },
  { code: 'DATE_FROM', name: 'Thời điểm báo cáo: từ ngày' },
  { code: 'DATE_TO', name: 'đến ngày' }
]
export const DashboardType = {
  OTHER_INDIV_BUSINESS: 'BUSINESS_DASHBOARD',
  OTHER_INDIV_CUSTOMER: 'CUSTOMER_DASHBOARD',
  OTHER_INDIV_REVENUE: 'REVENUE_DASHBOARD',
  OTHER_INDIV_KPI: 'KPI_DASHBOARD',
  INDIV_BUSINESS: 'DASHBOARD_BUSSINESS',
  INDIV_CUSTOMER: 'DASHBOARD_CUSTOMER',
  INDIV_CARD: 'DASHBOARD_CARD',
  INDIV_CAMPAIGN: 'DASHBOARD_CAMPAIGN',
};

export const DashboardOtherIndiv = {
  DU_NO: 'TINDUNG_TD',
  TOP10_DU_NO: 'TOP10_DUNO',
  HDV: 'HUYDONGVON_TD',
  TOP10_HDV: 'TOP10_HĐV',
  DTTTRR: 'DTTTRR',
  TOP10_TRR: 'TOP10_TRR',
  DTTSRR: 'DTTSRR',
  TOP10_SRR: 'TOP10_SRR',
  KPI_TOTAL_POINT: 'TOTAL_POINT',
  KPI_TARGETS: 'KPI_TARGETS',
  DTTSRR_TBT: "DTTSRR_TBT",
  DTTTRR_TBT: "DTTTRR_TBT"
};

export const RoleUser = ['0_ADMIN_HT', '0_ADMIN_KHOI'];

export const DATE_TYPES = {
  DAY: 'day',
  MONTH: 'month',
};

export const ListEventTypeCalendar = [{ code: '', name: 'Tất cả hoạt động', listSuggest: [] }];

export const sector = [
  { code: 0, name: 'Không lặp lại' },
  { code: 1, name: 'Mỗi ngày' },
  { code: 2, name: 'Mỗi tuần' },
  { code: 3, name: 'Mỗi tháng' },
];

export const ListFrequencyCalendar = [
  { code: 0, name: 'Không lặp lại' },
  { code: 1, name: 'Mỗi ngày' },
  { code: 2, name: 'Mỗi tuần' },
  { code: 3, name: 'Mỗi tháng' },
];

export const ARM_STATUS_LIST = [
  { value: -1, name: 'Tất cả' },
  { value: 1, name: 'Hiệu lực' },
  { value: 0, name: 'Không hiệu lực' },
];

export const RM_GROUP_STATUS_LIST = [
  { value: -1, name: 'Tất cả' },
  { value: 1, name: 'Hiệu lực' },
  { value: 0, name: 'Không hiệu lực' },
];

export const ADS_IMAGE_STATUS_LIST = [
  { value: true, name: 'Có hiệu lực' },
  { value: false, name: 'Không hiệu lực' }
]

export const ADS_CHANNEL = [
  { value: 'WEB', name: 'Web' },
  { value: 'APP', name: 'App' }
]
// too much data, use this config when export
export const EXPORT_LIMIT = {
  size: 2147483647,
  page: 0,
};

export const LIST_STATUS_CUSTOMER_APPROVE = [
  { code: 0, text: 'DRAFT' },
  { code: 1, text: 'GUI PHE DUYET' },
  { code: 2, text: 'PHE DUYET CAP CHI NHANH' },
  { code: 3, text: 'PHE DUYET CAP HOI SO' },
  { code: 4, text: 'TRA PHE DUYET' },
  { code: 5, text: 'CHINH SUA KE HOACHs' },
];

export enum AlertCalendarType {
  FIRST = '15 phút nữa',
  SECOND = '10 phút nữa',
  OUTDATED = 'Đã quá hạn'
};

export enum AdvertisementConfigType {
  BANNER = 'banner',
  POSTER = 'poster'
}

export enum ActionType {
  CREATE = 'CREATE',
  UPDATE = 'UPDATE',
}

export enum FormType {
  CREATE = 'CREATE',
  UPDATE = 'UPDATE',
}

// ALL STATUS MUST USE REASON
export const REASON_STATUS = ['REFUSE', 'APPROVE', 'END'];

export const DUE_STATUS_LIST = [
  { value: null, name: 'Tất cả' },
  { value: '1', name: 'Quá hạn' },
  { value: '2', name: 'Đến hạn' },
  { value: '3', name: 'Sắp đến hạn' }
]

export const WARNING_TYPE = {
  GUARANTEE: { code: 'GUARANTEE', name: 'Bảo lãnh', class: 'blue-violet-tag' },
  CREDIT: { code: 'CREDIT', name: 'Tín dụng', class: 'purple-tag' },
  WARNING_SAVING: { code: 'WARNING_SAVING', name: 'Tiết kiệm', class: 'light-sky-blue-tag' },
  INTERNATIONAL_PAYMENT: { code: 'INTERNATIONAL_PAYMENT', name: 'TTTM', class: 'orange-tag' },
  REINSTATEMENT_INSURANCE: { code: 'REINSTATEMENT_INSURANCE', name: 'Tái tục bảo hiểm', class: 'red-tag' },
  ASSET_VALUATION: { code: 'ASSET_VALUATION', name: 'Định giá lại tài sản', class: 'brown-tag' },
}

// export const DUE_DATE_LIST = [
//   { value: '', name: 'Tất cả' },
//   { value: '1to3', name: '1-3 ngày' },
//   { value: '3to5', name: '3-5 ngày' },
//   { value: '5to7', name: '5-7 ngày' },
//   { value: 'after7', name: 'Trên 7 ngày' },
// ]
export const CAMPAIGN_SCOPES = {
  TOAN_HANG: 'TOAN_HANG',
  CHI_NHANH: 'CHI_NHANH'
}

export const FILE_TYPES = {
  FILE: 'FILE',
  IMAGE: 'IMAGE'
}

export const TARGET_PICK_BY = {
  SINGLE: 'SINGLE', // đơn lẻ
  MULTI: 'MULTI' // lô
}

export enum TYPE_MESSAGE_EMBEDED {
  KH_360 = 'KH_360',
}

export const ORDER_CHART = [
  { id: 1, value: 'Tăng' },
  { id: 2, value: 'Giảm' },
  { id: 3, value: 'Số dư' }
]

export const DAY_MONTH = [
  { id: 1, value: 'Ngày' },
  { id: 2, value: 'Tháng'}
]

export const RECORD = [
  { id: 1, value: 10 },
  { id: 2, value: 20 },
  { id: 3, value: 30 },
  { id: 4, value: 40 },
  { id: 5, value: 50 }
]

export const REPORT_ID = {
  CASA: '01',
  DEPO: '02',
  CARD: '03'
}

export enum TYPE_MESSAGE_NEXTGEN {
  KH_360 = 'KH_360',
}
