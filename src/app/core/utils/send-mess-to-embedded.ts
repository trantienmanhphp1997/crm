import { environment } from "src/environments/environment";

export function sendMessToEmbedded(typeMessage, dataMessage) {
  const iframe = (document.getElementById('iframeId') as HTMLIFrameElement).contentWindow;
  const data = {
    type: typeMessage,
    data: dataMessage
  }
  iframe.postMessage(data, environment.url_endpoint_embedded);
}
