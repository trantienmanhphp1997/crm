export interface CategoryChildrenModel {
  id: string;
  code: string;
  name: string;
  commonCategoryCode: string;
  orderNum: number;
  value: string;
  description?: string;
  isActive?: boolean;
  isDefault?: boolean;
}
