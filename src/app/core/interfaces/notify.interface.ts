export interface NotifyModel {
  id: string;
  title: string;
  time: string;
  avatar: {
    style: {
      color: string;
      [k: string]: string;
    };
    label: string;
  };
  isReaded: boolean;
}
