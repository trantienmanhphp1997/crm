export interface Pageable<T> {
  totalPages: number;
  totalElements: number;
  currentPage: number;
  size: number;
  rows: T[];
}
