export interface Action {
  type: number;
  icon: string;
  isDropdown: boolean;
  textTranslate: string;
}
