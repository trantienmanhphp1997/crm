export interface IS3Report {
    reportCode: string,
    reportParams: Object[],
    rsId?: string,
    scope?: string,
    type?: any
}