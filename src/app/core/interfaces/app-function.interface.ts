export interface AppFunction {
  rsId: string;
  rsCode: string;
  rsName: string;
  rsUri: string;
  scopes: string[];
  create: boolean;
  update: boolean;
  view: boolean;
  delete: boolean;
  import: boolean;
  export: boolean;
  revenueShare: boolean;
  log: boolean;
  approve: boolean;
  rmGrant: boolean;
  assignKVH: boolean;
  unAssignKVH: boolean;
  unLogUser: boolean;
  restore: boolean;
  onlyOPN: boolean;
  HO: boolean;
  updateSME: boolean;
}
