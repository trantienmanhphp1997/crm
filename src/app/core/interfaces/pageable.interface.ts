export interface Pageable {
  totalPages?: number;
  totalElements: number;
  currentPage: number;
  size?: number;
  prevPage?: boolean;
  nextPage?: boolean;
  offset?: number;
}
