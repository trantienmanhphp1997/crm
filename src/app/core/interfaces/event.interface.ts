export interface EventAction {
  type: number;
  data: any;
}
