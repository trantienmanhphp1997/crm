export interface LogModel {
  id?: string;
  type?: string;
  objectName?: string;
  correlation?: string;
  method?: string;
  scope?: string;
  uri?: string;
  body?: any;
  bodyResponse?: any;
}
