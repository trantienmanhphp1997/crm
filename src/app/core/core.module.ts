import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { AppComponent } from './containers/app.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { CustomHttpInterceptor } from './interceptors/custom-http.interceptor';
import { GlobalErrorHandler } from './services/global-error-handler';
import { RouterModule } from '@angular/router';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LayoutComponent } from './containers/layout/layout.component';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ToastModule } from 'primeng/toast';
import { SharedModule } from '../shared/shared.module';
import { AppConfigComponent } from './components/app-config/app-config.component';
import { CalendarModule } from 'primeng/calendar';
import { CheckboxModule } from 'primeng/checkbox';
import { NotifyMessageComponent } from './components/notify-message/notify-message.component';
import { EncrDecrService } from './services/encr-decr.service';
import { BadgeModule } from 'primeng/badge';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { AvatarModule } from 'primeng/avatar';
import {CalendarAlertComponent} from './components/calendar-alert/calendar-alert.component';
import { PosterComponent } from './components/poster/poster.component';
import { SurveyFunctionComponent } from './components/survey-function/survey-function.component';
import { DropdownModule } from 'primeng/dropdown';
import { RatingModule } from 'primeng/rating';
import { ModalConfirmComponent } from './components/survey-function/modal-confirm/modal-confirm.component';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
@NgModule({
  declarations: [AppComponent, NavbarComponent, LayoutComponent, NotifyMessageComponent, AppConfigComponent, CalendarAlertComponent, PosterComponent, SurveyFunctionComponent,ModalConfirmComponent],
  imports: [
    CommonModule,
    DragDropModule,
    RouterModule,
    BrowserModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    TranslateModule,
    NgxDatatableModule,
    ToastModule,
    SharedModule,
    CalendarModule,
    CheckboxModule,
    BadgeModule,
    InfiniteScrollModule,
    AvatarModule,
    DropdownModule,
    RatingModule,
    InputTextareaModule
  ],
  providers: [
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CustomHttpInterceptor,
      multi: true,
    },
    Title,
    EncrDecrService,
  ],
})
export class CoreModule {}
