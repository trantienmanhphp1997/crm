import { HttpCancelService } from '../services/http-cancel.service';
import { Router } from '@angular/router';
import {
  HttpClient,
  HttpHandler,
  HttpHeaderResponse,
  HttpInterceptor,
  HttpProgressEvent,
  HttpRequest,
  HttpResponse,
  HttpSentEvent,
  HttpUserEvent,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { takeUntil, catchError, tap } from 'rxjs/operators';
import { v4 as uuIdv4 } from 'uuid';
import { environment } from 'src/environments/environment';
import { KeycloakEventType, KeycloakService } from 'keycloak-angular';
import { LogService } from 'src/app/pages/system/services/log.service';
import * as _ from 'lodash';
import { LogModel } from '../interfaces/log-model.interface';
import { FunctionCode, RequestMethod, Scopes, SessionKey } from '../utils/common-constants';
import { SessionService } from '../services/session.service';
import { clearLocalStorage, logoutKeycloak } from '../utils/function';

const urlLogs: LogModel[] = [
  {
    uri: `${environment.url_endpoint_rm}/employees`,
    method: RequestMethod.Post,
    scope: Scopes.CREATE,
    objectName: FunctionCode.RM_MANAGER,
  },
  {
    uri: `${environment.url_endpoint_rm}/groups`,
    method: RequestMethod.Post,
    scope: Scopes.CREATE,
    objectName: FunctionCode.RM_GROUP,
  },
  {
    uri: `${environment.url_endpoint_rm}/groups`,
    method: RequestMethod.Put,
    scope: Scopes.UPDATE,
    objectName: FunctionCode.RM_GROUP,
  },
  {
    uri: `${environment.url_endpoint_rm}/groups`,
    method: RequestMethod.Delete,
    scope: Scopes.DELETE,
    objectName: FunctionCode.RM_GROUP,
  },
  {
    uri: `${environment.url_endpoint_sale}/campaigns`,
    method: RequestMethod.Post,
    scope: Scopes.CREATE,
    objectName: FunctionCode.CAMPAIGN,
  },
  {
    uri: `${environment.url_endpoint_sale}/campaigns`,
    method: RequestMethod.Put,
    scope: Scopes.UPDATE,
    objectName: FunctionCode.CAMPAIGN,
  },
  {
    uri: `${environment.url_endpoint_category}/commons`,
    method: RequestMethod.Post,
    scope: Scopes.CREATE,
    objectName: FunctionCode.ADMIN_COMMON,
  },
  {
    uri: `${environment.url_endpoint_category}/commons`,
    method: RequestMethod.Put,
    scope: Scopes.UPDATE,
    objectName: FunctionCode.ADMIN_COMMON,
  },
  {
    uri: `${environment.url_endpoint_category}/commons`,
    method: RequestMethod.Delete,
    scope: Scopes.DELETE,
    objectName: FunctionCode.ADMIN_COMMON,
  },
  {
    uri: `${environment.url_endpoint_category}/decentralization`,
    method: RequestMethod.Post,
    scope: Scopes.CREATE,
    objectName: FunctionCode.ADMIN_APP,
  },
  {
    uri: `${environment.url_endpoint_category}/decentralization`,
    method: RequestMethod.Put,
    scope: Scopes.UPDATE,
    objectName: FunctionCode.ADMIN_APP,
  },
  {
    uri: `${environment.url_endpoint_category}/decentralization`,
    method: RequestMethod.Delete,
    scope: Scopes.DELETE,
    objectName: FunctionCode.ADMIN_APP,
  },
  {
    uri: `${environment.url_endpoint_category}/branches`,
    method: RequestMethod.Put,
    scope: Scopes.UPDATE,
    objectName: FunctionCode.ADMIN_BRANCH,
  },
  {
    uri: `${environment.url_endpoint_category}/blocks`,
    method: RequestMethod.Post,
    scope: Scopes.CREATE,
    objectName: FunctionCode.ADMIN_BLOCK,
  },
  {
    uri: `${environment.url_endpoint_category}/blocks`,
    method: RequestMethod.Put,
    scope: Scopes.UPDATE,
    objectName: FunctionCode.ADMIN_BLOCK,
  },
  {
    uri: `${environment.url_endpoint_category}/blocks`,
    method: RequestMethod.Delete,
    scope: Scopes.DELETE,
    objectName: FunctionCode.ADMIN_BLOCK,
  },
  {
    uri: `${environment.url_endpoint_category}/function`,
    method: RequestMethod.Post,
    scope: Scopes.CREATE,
    objectName: FunctionCode.ADMIN_FUNCTION,
  },
  {
    uri: `${environment.url_endpoint_category}/function`,
    method: RequestMethod.Put,
    scope: Scopes.UPDATE,
    objectName: FunctionCode.ADMIN_FUNCTION,
  },
  {
    uri: `${environment.url_endpoint_category}/function`,
    method: RequestMethod.Delete,
    scope: Scopes.DELETE,
    objectName: FunctionCode.ADMIN_FUNCTION,
  },
  {
    uri: `${environment.url_endpoint_category}/roles`,
    method: RequestMethod.Post,
    scope: Scopes.CREATE,
    objectName: FunctionCode.ADMIN_ROLE,
  },
  {
    uri: `${environment.url_endpoint_category}/roles`,
    method: RequestMethod.Put,
    scope: Scopes.UPDATE,
    objectName: FunctionCode.ADMIN_ROLE,
  },
  {
    uri: `${environment.url_endpoint_category}/roles`,
    method: RequestMethod.Delete,
    scope: Scopes.DELETE,
    objectName: FunctionCode.ADMIN_ROLE,
  },
  {
    uri: `${environment.url_endpoint_category}/scopes`,
    method: RequestMethod.Post,
    scope: Scopes.CREATE,
    objectName: FunctionCode.ADMIN_SCOPES,
  },
  {
    uri: `${environment.url_endpoint_category}/scopes`,
    method: RequestMethod.Put,
    scope: Scopes.UPDATE,
    objectName: FunctionCode.ADMIN_SCOPES,
  },
  {
    uri: `${environment.url_endpoint_category}/scopes`,
    method: RequestMethod.Delete,
    scope: Scopes.DELETE,
    objectName: FunctionCode.ADMIN_SCOPES,
  },
  {
    uri: `${environment.keycloak.issuer}admin/realms/${environment.keycloak.realm}/clients/${localStorage.getItem(
      'RELATIONSHIP_CLIENT'
    )}/authz/resource-server/permission`,
    method: RequestMethod.Post,
    scope: Scopes.CREATE,
    objectName: FunctionCode.ADMIN_PERMISSION,
  },
  {
    uri: `${environment.keycloak.issuer}admin/realms/${environment.keycloak.realm}/clients/${localStorage.getItem(
      'RELATIONSHIP_CLIENT'
    )}/authz/resource-server/permission`,
    method: RequestMethod.Put,
    scope: Scopes.UPDATE,
    objectName: FunctionCode.ADMIN_PERMISSION,
  },
  {
    uri: `${environment.keycloak.issuer}admin/realms/${environment.keycloak.realm}/clients/${localStorage.getItem(
      'RELATIONSHIP_CLIENT'
    )}/authz/resource-server/permission`,
    method: RequestMethod.Delete,
    scope: Scopes.DELETE,
    objectName: FunctionCode.ADMIN_PERMISSION,
  },
  {
    uri: `${environment.url_endpoint_rm}/employees/username`,
    method: RequestMethod.Post,
    scope: Scopes.UPDATE,
    objectName: FunctionCode.ADMIN_USER_ROLE,
  },
  {
    uri: `${environment.url_endpoint_category}/users-permissions`,
    method: RequestMethod.Post,
    scope: Scopes.UPDATE,
    objectName: FunctionCode.ADMIN_DATA_DOMAIN,
  },
  {
    uri: `${environment.url_endpoint_customer}/product-process`,
    method: RequestMethod.Post,
    scope: Scopes.CREATE,
    objectName: FunctionCode.CAMPAIGN_MAPPING,
  },
  {
    uri: `${environment.url_endpoint_category}/title-groups`,
    method: RequestMethod.Post,
    scope: Scopes.CREATE,
    objectName: FunctionCode.RM_TITLE,
  },
  {
    uri: `${environment.url_endpoint_category}/title-groups`,
    method: RequestMethod.Put,
    scope: Scopes.UPDATE,
    objectName: FunctionCode.RM_TITLE,
  },
  {
    uri: `${environment.url_endpoint_category}/title-groups`,
    method: RequestMethod.Delete,
    scope: Scopes.DELETE,
    objectName: FunctionCode.RM_TITLE,
  },
  {
    uri: `${environment.url_endpoint_category}/levels`,
    method: RequestMethod.Post,
    scope: Scopes.CREATE,
    objectName: FunctionCode.RM_LEVEL,
  },
  {
    uri: `${environment.url_endpoint_category}/levels`,
    method: RequestMethod.Put,
    scope: Scopes.UPDATE,
    objectName: FunctionCode.RM_LEVEL,
  },
  {
    uri: `${environment.url_endpoint_category}/levels`,
    method: RequestMethod.Post,
    scope: Scopes.DELETE,
    objectName: FunctionCode.RM_LEVEL,
  },
  {
    uri: `${environment.url_endpoint_category}/device/update`,
    method: RequestMethod.Post,
    scope: Scopes.UPDATE,
    objectName: FunctionCode.DEVICE_MANAGEMENT,
  },
  {
    uri: `${environment.url_endpoint_category}/device/delete`,
    method: RequestMethod.Post,
    scope: Scopes.DELETE,
    objectName: FunctionCode.DEVICE_MANAGEMENT,
  },
  {
    uri: `${environment.url_endpoint_integration}/processes/save`,
    method: RequestMethod.Post,
    scope: Scopes.CREATE,
    objectName: FunctionCode.PROCESS_MANAGEMENT,
  },
  {
    uri: `${environment.url_endpoint_integration}/processes/start-process`,
    method: RequestMethod.Post,
    scope: Scopes.UPDATE,
    objectName: FunctionCode.PROCESS_MANAGEMENT,
  },
  {
    uri: `${environment.url_endpoint_integration}/processes/stop-process`,
    method: RequestMethod.Post,
    scope: Scopes.UPDATE,
    objectName: FunctionCode.PROCESS_MANAGEMENT,
  },
  {
    uri: `${environment.url_endpoint_integration}/processes`,
    method: RequestMethod.Put,
    scope: Scopes.UPDATE,
    objectName: FunctionCode.PROCESS_MANAGEMENT,
  },
  {
    uri: `${environment.url_endpoint_integration}/processes`,
    method: RequestMethod.Delete,
    scope: Scopes.DELETE,
    objectName: FunctionCode.PROCESS_MANAGEMENT,
  },
  {
    uri: `${environment.url_endpoint_rm_relationship}/fluctuations`,
    method: RequestMethod.Post,
    scope: Scopes.CREATE,
    objectName: FunctionCode.RM_FLUCTUATE,
  },
  {
    uri: `${environment.url_endpoint_rm_relationship}/fluctuations`,
    method: RequestMethod.Put,
    scope: Scopes.UPDATE,
    objectName: FunctionCode.RM_FLUCTUATE,
  },
  {
    uri: `${environment.url_endpoint_rm_relationship}/fluctuations/approve`,
    method: RequestMethod.Put,
    scope: Scopes.APPROVE,
    objectName: FunctionCode.RM_FLUCTUATE,
  },
  {
    uri: `${environment.url_endpoint_rm_relationship}/fluctuations/reject`,
    method: RequestMethod.Put,
    scope: Scopes.APPROVE,
    objectName: FunctionCode.RM_FLUCTUATE,
  },
  {
    uri: `${environment.url_endpoint_rm_relationship}/fluctuations/restoreManual`,
    method: RequestMethod.Post,
    scope: Scopes.RESTORE,
    objectName: FunctionCode.RM_FLUCTUATE,
  },
  {
    uri: `${environment.url_endpoint_rm_relationship}/fluctuations`,
    method: RequestMethod.Delete,
    scope: Scopes.DELETE,
    objectName: FunctionCode.RM_FLUCTUATE,
  },
  {
    uri: `${environment.url_endpoint_activity}/events/findAll/cbql`,
    method: RequestMethod.Post,
    scope: Scopes.VIEW,
    objectName: FunctionCode.CALENDARSME,
  },
];

const urlNotCheck = `${environment.url_endpoint_admin}/notify/list-all-notification`;

@Injectable()
export class CustomHttpInterceptor implements HttpInterceptor {
  isCheck = false;
  currUser = null;
  constructor(
    private router: Router,
    private httpCancelService: HttpCancelService,
    private keyCloakService: KeycloakService,
    private log: LogService,
    private http: HttpClient,
    private sessionService: SessionService
  ) {
    this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
    this.keyCloakService.keycloakEvents$.subscribe((res) => {
      if (res?.type === KeycloakEventType.OnAuthRefreshSuccess) {
        localStorage.setItem(SessionKey.LAST_TIME_ACTION, new Date().valueOf().toString());
      }
      if (res?.type === KeycloakEventType.OnTokenExpired) {
        this.keyCloakService.updateToken().then(value => {
          if (value) {
            this.keyCloakService.getToken().then((token) => {
              this.sessionService.setSessionData(SessionKey.TOKEN, token);
              this.onUpdateTokenEmbed();
            });
          }
        });
      }
    });
    const timeoutSession = 15 * 60 * 1000;
    setInterval(() => {
      const now = new Date().valueOf();
      const lastTime = localStorage.getItem(SessionKey.LAST_TIME_ACTION);
      if (!document.hidden && lastTime && now - +lastTime > timeoutSession) {

        if (!this.currUser) {
          this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
        }
        clearLocalStorage(this.currUser);
        logoutKeycloak();
        // this.keyCloakService.logout().then();
      }
    }, 3000);
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
    if (this.keyCloakService.isTokenExpired()) {
      clearLocalStorage(this.currUser);
      logoutKeycloak();
      // this.keyCloakService.logout();
      return;
    }
    const clientMessageId = uuIdv4();
    const item = _.find(urlLogs, (i) => req.url?.includes(i.uri) && req.method === i.method);
    if (item) {
      const data: LogModel = {
        uri: req.url,
        correlation: clientMessageId,
        method: req.method,
        objectName: item.objectName,
        body: req.body ? JSON.stringify(req.body) : null,
        scope: item.scope,
      };
      this.log.createLog(data).subscribe(() => {});
    }

    if (!req.url.includes(urlNotCheck)) {
      localStorage.setItem(SessionKey.LAST_TIME_ACTION, new Date().valueOf().toString());
    }

    const modifiedReq = req.url.startsWith(environment.keycloak.issuer)
      ? req.clone()
      : req.clone({
          headers: req.headers.set('clientMessageId', clientMessageId),
        });
    return next.handle(modifiedReq).pipe(
      catchError((e) => {
        this.checkSession(e);
        throw e;
      }),
      takeUntil(this.httpCancelService.onCancelPendingRequests())
    );
  }

  checkSession(err) {
    if (err?.status === 403 && !this.isCheck) {
      this.isCheck = !this.isCheck;
      const data2 = new URLSearchParams();
      data2.set('audience', environment.keycloak.relationShip);
      data2.set('response_mode', 'permissions');
      data2.set('grant_type', 'urn:ietf:params:oauth:grant-type:uma-ticket');
      this.http
        .post(`${environment.keycloak.issuer}realms/${environment.keycloak.realm}/protocol/openid-connect/token`, data2.toString(), {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
        })
        .subscribe(
          () => {
            this.isCheck = !this.isCheck;
          },
          (e) => {
            if (e?.status === 401 && e?.error?.error_description === 'Invalid bearer token') {
              logoutKeycloak();
              // this.keyCloakService.logout().then();
            }
          }
        );
    }
  }

  onUpdateTokenEmbed() {
    const domain = environment.url_endpoint_embedded;
    const iframe = (document.getElementById('iframeId') as HTMLIFrameElement).contentWindow;
    iframe.postMessage(this.sessionService.getSessionData(SessionKey.TOKEN), domain)
  }
}
