import { saveAs } from 'file-saver';
import { retryWhen, delay, take, catchError, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CommunicateService } from './communicate.service';

@Injectable({
  providedIn: 'root',
})
export class FileService {
  private baseUrl = `${environment.url_endpoint_rm}/files`;
  constructor(private http: HttpClient, private communicateService: CommunicateService) {}

  uploadFile(data): Observable<any> {
    return this.http.post(`${this.baseUrl}/upload`, data, { responseType: 'text' });
  }

  downloadFile(fileId: string, fileName: string, key?: string): Observable<boolean> {
    let count = 0;
    const rep = 60;
    return new Observable<boolean>((observer) => {
      this.http
        .get(`${this.baseUrl}/download/${fileId}`, {
          responseType: 'arraybuffer',
        })
        .pipe(
          retryWhen((e) => {
            return e
              .pipe(
                delay(5000),
                take(rep),
                tap(() => {
                  count++;
                  if (count === rep) {
                    observer.next(false);
                  }
                })
              )
              .pipe(catchError(() => of(false)));
          })
        )
        .subscribe((res) => {
          if (res) {
            saveAs(
              new Blob([res], {
                type: 'application/octet-stream',
              }),
              fileName
            );
            observer.next(true);
          } else {
            observer.next(false);
          }
          if (key) {
            this.communicateService.request({ name: 'export-completed', key });
          }
        });
    });
  }
}
