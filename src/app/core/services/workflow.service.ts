import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface ParamsLane {
  type: string;
  nameProcess: string;
}

@Injectable({
  providedIn: 'root',
})
export class WorkflowService {
  private baseUrl = environment.url_endpoint_workflow;
  constructor(private http: HttpClient) {}

  findAllProcess(): Observable<any> {
    return this.http.get(`${this.baseUrl}/lanes`);
  }

  getLanes(params: ParamsLane): Observable<any> {
    const data: any = {};
    Object.keys(params).forEach((key) => (data[key] = params[key]));
    return this.http.get(`${this.baseUrl}/lanes`, { params: data });
  }

  processOpportunity(opportunityId: string, status: string, product?: string): Observable<any> {
    return this.http.post(`${this.baseUrl}/tasks/complete-task`, {
      payloadType: 'CompleteTaskPayload',
      variables: { opportunityId: opportunityId, [status]: true, product },
    });
  }

  processQualify(leadId, status): Observable<any> {
    return this.http.put(`${environment.url_endpoint_sale}/leads/changeStatusLead/${leadId}?status=${status}`, {});
  }

  convertOpportunity(leadId): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/leads/convert?leadId=${leadId}`, {});
  }
}
