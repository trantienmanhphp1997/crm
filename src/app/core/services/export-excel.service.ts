import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as XLSXCus from 'xlsx-js-style'

import {JSON2SheetOpts} from 'xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable({
  providedIn: 'root',
})
export class ExportExcelService {
  opt:  JSON2SheetOpts;
  constructor() {}

  public exportAsExcelFile(json: any[], excelFileName: string, usedDate: boolean = true): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    // const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName, usedDate);
  }

  private saveAsExcelFile(buffer: any, fileName: string, usedDate: boolean = true): void {
    const data: Blob = new Blob([buffer], {
      type: 'application/octet-stream',
    });
    fileName = usedDate ? `${fileName}_${new Date().getTime()}${EXCEL_EXTENSION}` : `${fileName}${EXCEL_EXTENSION}`
    FileSaver.saveAs(data, fileName);
  }
  public exportExcelCustom(json: any[], excelFileName: string, usedDate: boolean = true): void {
    const Heading1 = [
        "STT",
        "Khách hàng",
        "",
        "",
        ""
    ];
    const Heading2 = [
        "",
        "Doanh số",
        "Doanh số thực tế",
        "Đơn vị",
        "",
    ];
    json = [
      {
        'STT': '1',
        'Doanh số': 12334,
        'Doanh số thực tế': 124324,
        'Đơn vị': 'Tỉ đồng'
      }
    ];
    const worksheet = XLSX.utils.json_to_sheet([]);
    const merge = [
      { s: { r: 0, c: 0 }, e: { r: 1, c: 0 } },{ s: { r: 0, c: 1 }, e: { r: 0, c: 3 } },
    ];
    worksheet['!merges'] = merge;
    XLSX.utils.sheet_add_aoa(worksheet, [Heading1, Heading2]);
    XLSX.utils.sheet_add_json(worksheet, json, {
      skipHeader: true,
      origin: -1,
    });
    const workbook: XLSX.WorkBook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    // const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName, usedDate);
  }

  public exportExcelKpi(json: any[], fromDate, toDate, excelFileName: string, usedDate: boolean = true){
    const Heading1 = [
      "",
      "",
      "",
      "",
      "",
      "",
      "BÁO CÁO ĐIỂM KPIs",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
    ];
    const Heading2 = [
      "",
      "",
      "",
      "",
      "",
      "",
      "Từ ngày: " + fromDate,
      "",
      "Đến ngày: " + toDate,
      "",
      "",
      "",
      "",
      "",
      "",
    ];
    const Headling3 = [
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
    ];
    const Headling4 = [
      "",
      "",
      "RM bán chính",
      "RM bán chéo",
      "Hệ số bán chéo",
      "Hệ số cũ/mới",
      "Khách hàng",
      "",
      "Sản phẩm/Dịch vụ",
      "",
      "",
      "Doanh số",
      "",
      "",
      "",
    ];
    const Headling5 = [
      "STT",
      "Ngày",
      "",
      "",
      "",
      "",
      "Code KH",
      "Tên KH",
      "Sản phẩm cấp 1",
      "Sản phẩm cấp 2",
      "Sản phẩm cấp 3",
      "Doanh số",
      "Doanh số thực tế",
      "Đơn vị",
      "Loại tiền",
    ];
    const worksheet = XLSXCus.utils.json_to_sheet([]);
    const merge = [
      { s: { r: 0, c: 6 }, e: { r: 0, c: 9 } },
      { s: { r: 1, c: 6 }, e: { r: 1, c: 7 } },
      { s: { r: 1, c: 8 }, e: { r: 1, c: 9 } },
      { s: { r: 5, c: 0 }, e: { r: 5, c: 1 } },
      { s: { r: 5, c: 2 }, e: { r: 6, c: 2 } },
      { s: { r: 5, c: 3 }, e: { r: 6, c: 3 } },
      { s: { r: 5, c: 4 }, e: { r: 6, c: 4 } },
      { s: { r: 5, c: 5 }, e: { r: 6, c: 5 } },
      { s: { r: 5, c: 6 }, e: { r: 5, c: 7 } },
      { s: { r: 5, c: 8 }, e: { r: 5, c: 10 } },
      { s: { r: 5, c: 11 }, e: { r: 5, c: 13 } },
    ];
    worksheet['!merges'] = merge;
    const wscols = [
      {width: 6},
      {width: 12},
      {width: 20},
      {width: 20},
      {width: 15},
      {width: 15},
      {width: 30},
      {width: 30},
      {width: 30},
      {width: 30},
      {width: 30},
      {width: 30},
      {width: 30},
      {width: 10},
      {width: 10}
    ];
    worksheet['!cols'] = wscols;
    XLSXCus.utils.sheet_add_aoa(worksheet, [Heading1, Heading2, Headling3,Headling3,Headling3,Headling4,Headling5]);
    XLSXCus.utils.sheet_add_json(worksheet, json, {
      skipHeader: true,
      origin: -1,
    });
    const cellRef1 = XLSXCus.utils.encode_cell({ r: 0, c: 6 });
    const colors = ['FDE9D9','DCE6F1','F2DCDB','DDD9C4']
    for(let j = 5; j < 7; j++){
      for(let i = 0; i < 15; i++){
        let color = colors[0];
        if(i <=5){
          color = colors[0];
        }
        else if(i > 5 && i < 8){
          color = colors[1];
        }
        else if( i < 11){
          color = colors[2];
        }
        else if(i >= 11 ){
          color = colors[3];
        }
        const cellRef =  XLSXCus.utils.encode_cell({ r: j, c: i});
        worksheet[cellRef].s = {
          alignment: { horizontal: 'center', vertical: 'center' },
          font: { bold: true},
          fill: { fgColor: { rgb: color } },
          border: {top: {style: 'thin',color: 'fff'},
            bottom: {style: 'thin',color: 'fff'},
            left: {style: 'thin',color: 'fff'},
            right: {style: 'thin',color: 'fff'} }
        };
      }
    }
    // Add center alignment to every cell
    worksheet[cellRef1].s = {
      alignment: { horizontal: 'center' },
      font: { bold: true, sz: 16 },
    };

    const workbook: XLSXCus.WorkBook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSXCus.write(workbook, { bookType: 'xlsx', type: 'array' });
    // const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName, usedDate);
  }
}
