import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Injectable } from '@angular/core';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { ConfirmType } from '../utils/common-constants';

@Injectable({
  providedIn: 'root',
})
export class ConfirmService {
  constructor(private modalService: NgbModal) {}

  success(message: string | string[], isCopy?: boolean): Promise<any> {
    return this.openModal(ConfirmType.Success, message, isCopy);
  }

  error(message: string | string[], isCopy?: boolean): Promise<boolean> {
    return this.openModal(ConfirmType.CusError, message, isCopy);
  }

  confirm(message?: string | string[], isCopy?: boolean): Promise<any> {
    return this.openModal(ConfirmType.Confirm, message, isCopy);
  }

  warn(message: string | string[], isCopy?: boolean): Promise<any> {
    return this.openModal(ConfirmType.Warning, message, isCopy);
  }

  openModal(type: number, message?: string | string[], isCopy?: boolean): Promise<any> {
    return new Promise((resolve) => {
      const modal = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      modal.componentInstance.message = message;
      modal.componentInstance.type = type;
      modal.componentInstance.isCopy = isCopy;
      modal.result
        .then((result) => {
          resolve(result);
        })
        .catch(() => {
          resolve(false);
        });
    });
  }
}
