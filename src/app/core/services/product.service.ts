import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  baseUrl: string = environment.url_endpoint_customer;
  constructor(private http: HttpClient) {}

  findAll(): Observable<any> {
    return this.http.get(`${this.baseUrl}/products/findAll`);
  }

  getProductByLevel(level): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer}/products?level=${level}`);
  }

  getProductTreeSale(levelId: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer}/products/getProductTreeSale?lvl1=${levelId}`);
  }

  getProcessByProductAndBlock(productId, blockCode): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_customer}/product-process?productId=${productId}&blockCode=${blockCode}`
    );
  }

  getAllWarningIndiv(params: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/products/product-warning`, params);
  }

  getProductDetail(productId): Observable<any>{
    return this.http.get(`${environment.url_endpoint_sale}/sale-kit/product-detail?productId=${productId}`);
  }
}
