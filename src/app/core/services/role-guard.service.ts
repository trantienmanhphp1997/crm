import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, UrlTree } from '@angular/router';
import { KeycloakService, KeycloakAuthGuard } from 'keycloak-angular';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AppFunction } from '../interfaces/app-function.interface';
import { functionUri, Scopes, SessionKey } from '../utils/common-constants';
import { SessionService } from './session.service';
import * as _ from 'lodash';
import { catchError } from 'rxjs/operators';
import { global } from '@angular/compiler/src/util';

@Injectable({
  providedIn: 'root',
})
export class RoleGuardService extends KeycloakAuthGuard implements CanActivateChild {
  isRedirectTo = true;
  listData = [];
  user: any;
  constructor(
    protected readonly router: Router,
    protected readonly keycloak: KeycloakService,
    private sessionService: SessionService,
    private http: HttpClient
  ) {
    super(router, keycloak);
    this.user = this.sessionService.getSessionData(SessionKey.USER_INFO);
  }

  isAccessAllowed(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    return new Promise(async (resolve) => {
      if (!this.authenticated) {
        localStorage.clear();
        await this.keycloakAngular.login();
        return;
      }
      const objFunction: AppFunction = this.sessionService.getSessionData(`FUNCTION_${route?.data?.code}`);
      if (objFunction) {
        localStorage.setItem('FUNCTION_SCOPE', route?.data?.scope || Scopes.VIEW);
        const currCode = localStorage.getItem('FUNCTION_CODE');
        const listObjLog = JSON.parse(localStorage.getItem('LIST_FUNCTION_LOG'));
        if (currCode !== objFunction.rsCode) {
          this.listData = [];
          const time = new Date().valueOf();
          const oldUserTracingId = _.chain(listObjLog)
            .find((i) => i.functionCode === objFunction.rsCode)
            .value()?.userTracingId;
          if (!oldUserTracingId) {
            this.listData.push({
              userName: this.user?.fullName,
              userId: this.user?.username,
              functionCode: objFunction.rsCode,
              functionName: objFunction.rsName,
              appCode: 'CRM_WEB',
              startTime: time,
            });
          }
          const currUserTracingId =
            _.chain(listObjLog)
              .find((i) => i.functionCode === currCode)
              .value()?.userTracingId || 0;
          if (currUserTracingId !== 0) {
            this.listData.push({ userTracingId: currUserTracingId, endTime: time });
          }
          localStorage.setItem('FUNCTION_CODE', objFunction.rsCode);
          this.save();
        }
        resolve(true);
      } else {
        await this.router.navigateByUrl(functionUri.dashboard);
        resolve(false);
      }
    });
  }

  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    const objFunction: AppFunction = this.sessionService.getSessionData(`FUNCTION_${childRoute?.data?.code}`);
    if ((objFunction && !childRoute?.data?.scope) || objFunction?.scopes?.includes(childRoute?.data?.scope)) {
      localStorage.setItem('FUNCTION_SCOPE', childRoute?.data?.scope || Scopes.VIEW);
      const currCode = localStorage.getItem('FUNCTION_CODE');
      const listObjLog = JSON.parse(localStorage.getItem('LIST_FUNCTION_LOG'));
      if (currCode !== objFunction.rsCode) {
        this.listData = [];
        const time = new Date().valueOf();
        const oldUserTracingId = _.chain(listObjLog)
          .find((i) => i.functionCode === objFunction.rsCode)
          .value()?.userTracingId;
        if (!oldUserTracingId) {
          this.listData.push({
            userName: this.user?.fullName,
            userId: this.user?.username,
            functionCode: objFunction.rsCode,
            functionName: objFunction.rsName,
            appCode: 'CRM_WEB',
            startTime: time,
          });
        }
        const currUserTracingId =
          _.chain(listObjLog)
            .find((i) => i.functionCode === currCode)
            .value()?.userTracingId || 0;
        if (currUserTracingId !== 0) {
          this.listData.push({ userTracingId: currUserTracingId, endTime: time });
        }
        localStorage.setItem('FUNCTION_CODE', objFunction.rsCode);
        this.save();
      }
      return true;
    }
    this.router.navigateByUrl(functionUri.dashboard);
    return false;
  }

  save() {
    this.http
      .post(`${environment.url_endpoint_category}/device/save-user-tracing`, this.listData)
      .pipe(catchError(() => of(undefined)))
      .subscribe((res: any) => {
        if (res) {
          let listObjLog = JSON.parse(localStorage.getItem('LIST_FUNCTION_LOG')) || [];
          listObjLog = [...listObjLog, ...res];
          listObjLog = _.unionBy(listObjLog, 'functionCode');
          const strList = JSON.stringify(listObjLog);
          localStorage.setItem('LIST_FUNCTION_LOG', strList);
        }
      });
  }
}
