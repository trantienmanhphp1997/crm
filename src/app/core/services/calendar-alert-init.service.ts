import {Injectable, OnInit} from '@angular/core';
import {CalendarAlertModel, CalendarAlertService} from '../components/calendar-alert/calendar-alert.service';
import {AlertCalendarType, CommonCategory, FunctionCode, NotifyType, SessionKey} from '../utils/common-constants';
import {Utils} from '../utils/utils';
import {CalendarService} from '../../pages/calendar-sme/services/calendar.service';
import {SessionService} from './session.service';
import {TranslateService} from '@ngx-translate/core';
import {CategoryService} from '../../pages/system/services/category.service';
import _ from 'lodash';

@Injectable({
  providedIn: 'root',
})
export class CalendarAlertInitService {

  rsId: string;
  currUser: any;
  message: any;
  listConfigTime:any = [];
  TIME_PUSH_DUE_DATE: number;
  TIME_PUSH_REPEAT_DUE_DATE: number;
  TIME_PUSH_OUT_OF_DATE: number;
  NUMBER_PUSH_OUT_OF_DATE: number;

  constructor(
    private calendarAlertService: CalendarAlertService,
    private calendarService: CalendarService,
    private sessionService: SessionService,
    private categoryService :CategoryService,
    private translate: TranslateService,
  ) {
    this.translate.get('dayInWeek').subscribe((res) => {
      this.message = res;
    });
    this.runIntervalAlert();
    this.getConfigCommon();
  }

  getCalendarOfTheDay() {
    this.rsId = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CALENDARSME}`)?.rsId;
    this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
    const start = new Date((new Date()).setHours(0,0,0,0)).toISOString();
    const end = new Date((new Date()).setHours(23,59,59,0)).toISOString();
    const params = {status:0,rsId:this.rsId,hrsCode:[this.currUser?.hrsCode],code:''
      ,scope:'VIEW',startDate:start,endDate:end,pageNumber:0,pageSize:3}
    this.calendarService.getAllEventsForCBQL(params).subscribe(value => {
      if (value.content[0]?.listEvent) {
        const data = this.handleListAlert(value.content[0]?.listEvent);
        this.calculateTime(data);
      }
    },() => {
    });
  }

  runIntervalAlert() {
    const interval = setInterval(() => {
      const data = this.sessionService.getSessionData('CALENDAR_ALERT');
      this.calculateTime(data);
    }, 60000);
  }

  handleListAlert(data) {
    const listAlert = data?.filter(job => {
      // lấy lịch có startDate hoặc endDate là ngày hiện tại và không lấy lịch đã tắt thông báo
      if ((this.isSameDay(new Date(job.start), new Date())
        || this.isSameDay(new Date(job.end), new Date()))
        && (job.isPushDueDate || job.isPushOutOfDate)) {
          job.isSeVeralDay = true;
          job.start = new Date(new Date(job.start).setSeconds(0)).toJSON();
          job.end = new Date(new Date(job.end).setSeconds(0)).toJSON();
          // chỉ lấy lịch có trạng thái New
          return job.status === 'NEW' || job.status === 'EXPIRE';
      }
    });
    this.sessionService.setSessionData('CALENDAR_ALERT', listAlert);
    return listAlert;
  }

  calculateTime(data) {
    if (data) {
      const now = new Date();
      data.forEach(item => {
        const d = new Date(item.start).valueOf() - now.valueOf();
        const c = now.valueOf() - new Date(item.end).valueOf();
        if (d > 0) {
          const diff = Math.abs(d);
          const minutes = Math.floor((diff/1000)/60);
          if (( minutes ===  this.TIME_PUSH_DUE_DATE
            || minutes ===  this.TIME_PUSH_REPEAT_DUE_DATE) && item.isPushDueDate) {
          // if ((minutes === 15 || minutes === 14) && item.isPushDueDate) {
          // if (minutes > 0) {
            const timeString = this.handleStringTime(item);
            // tslint:disable-next-line:no-shadowed-variable
            const notify: CalendarAlertModel =  {
              id : 'id',
              idCalendar: item.id,
              idCalendarRoot: item.idRoot,
              title:'[' + item?.name + ']: ' + item?.description,
              time: timeString,
              timeRemaining: minutes + ' phút nữa',
              startDisplay: item.startDisplay,
              startTime: item.start,
              endTime: item.end,
              isNotAction: minutes ===  this.TIME_PUSH_DUE_DATE ? 1 : 2,
              titleNoti:null
            }
            this.calendarAlertService.show(notify);
          }
        }
        if (c > 0) {
          const diff = Math.abs(c);
          const minutes = Math.floor((diff/1000)/60);
          console.log('--minutes:', minutes)
          if ((minutes === this.TIME_PUSH_OUT_OF_DATE
            || (minutes === this.TIME_PUSH_OUT_OF_DATE * (this.NUMBER_PUSH_OUT_OF_DATE) && this.NUMBER_PUSH_OUT_OF_DATE > 1)
            || (minutes === this.TIME_PUSH_OUT_OF_DATE * (this.NUMBER_PUSH_OUT_OF_DATE - 1) && this.NUMBER_PUSH_OUT_OF_DATE > 2)
            || (minutes === this.TIME_PUSH_OUT_OF_DATE * (this.NUMBER_PUSH_OUT_OF_DATE - 2) && this.NUMBER_PUSH_OUT_OF_DATE > 3)
          ) && item.isPushOutOfDate) {
          // if ((minutes === 1 || minutes === 2 || minutes === 3) && item.isPushOutOfDate) {
          // if ((minutes >0) && item.isPushOutOfDate) {
            const timeString = this.handleStringTime(item);
            // tslint:disable-next-line:no-shadowed-variable
            const notify: CalendarAlertModel =  {
              id : 'id',
              idCalendar: item.id,
              idCalendarRoot: item.idRoot,
              title:'[' + item?.name + ']: ' + item?.description,
              time: timeString,
              timeRemaining: AlertCalendarType.OUTDATED,
              startDisplay: item.startDisplay,
              startTime: item.start,
              endTime: item.end,
              isNotAction: minutes === this.TIME_PUSH_OUT_OF_DATE * this.NUMBER_PUSH_OUT_OF_DATE ? 2 : 1,
              titleNoti:null
            }
            this.calendarAlertService.show(notify);
          }
        }
      })
    }
  }

  isSameDay(d1, d2) {
    return d1.getFullYear() === d2.getFullYear() &&
      d1.getMonth() === d2.getMonth() &&
      d1.getDate() === d2.getDate();
  }

  handleStringTime(item) {
    const startDateString = new Date(item.start).getUTCDate()
      +'/'+ (new Date(item.start).getUTCMonth()+1) +'/'+  new Date(item.start).getUTCFullYear();
    if (this.isSameDay(new Date(item.start), new Date(item.end))) {

      return  Utils.convertUTCTimeToTimeStamp12(new Date(item.start)) + ' - '
        +  Utils.convertUTCTimeToTimeStamp12(new Date(item.end))
        + ' ' + this.message[new Date(item.start).getDay().toString()] +', '+ startDateString
    } else {
      const endDateString = new Date(item.end).getUTCDate()
        +'/'+ (new Date(item.start).getUTCMonth() + 1) +'/'+  new Date(item.start).getUTCFullYear();
      return  Utils.convertUTCTimeToTimeStamp12(new Date(item.start))

        + ' ' + this.message[new Date(item.start).getDay().toString()] +', '+ startDateString
        + ' - '
        +  Utils.convertUTCTimeToTimeStamp12(new Date(item.end))
        + ' ' + this.message[new Date(item.end).getDay().toString()] +', '+ endDateString
    }
  }

  getConfigCommon() {
    this.categoryService.getCommonCategory(CommonCategory.CALENDAR_ALERT).subscribe((value => {
      if (value.content) {
       value.content.forEach(item => {
         this.listConfigTime.push({code: item.code, value: parseInt(item.value)})
         this.handleTime();
       })
      }
    }));
  }

  handleTime() {
    this.TIME_PUSH_DUE_DATE = _.get(
      _.find(this.listConfigTime, (i) => i.code === 'TIME_PUSH_DUE_DATE'),
      'value'
    );
    this.TIME_PUSH_REPEAT_DUE_DATE = _.get(
      _.find(this.listConfigTime, (i) => i.code === 'TIME_PUSH_REPEAT_DUE_DATE'),
      'value'
    );
    this.TIME_PUSH_OUT_OF_DATE = _.get(
      _.find(this.listConfigTime, (i) => i.code === 'TIME_PUSH_OUT_OF_DATE'),
      'value'
    );
    this.NUMBER_PUSH_OUT_OF_DATE = _.get(
      _.find(this.listConfigTime, (i) => i.code === 'NUMBER_PUSH_OUT_OF_DATE'),
      'value'
    );
  }
}
