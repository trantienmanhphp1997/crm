import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RmProfileService {
  baseUrl = `${environment.url_endpoint_rm}/employees`;
  constructor(private http: HttpClient) {}

  getByUsername(username: string, params?): Observable<any> {
    return this.http.get(`${this.baseUrl}/username/${username}`, { params });
  }

  getDetailRm(hrsCode: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/${hrsCode}`);
  }

  getRolesByUser(userName: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/username/${userName}/roles`);
  }

  updateRolesByUser(userName: string, data: string[]): Observable<any> {
    return this.http.post(`${this.baseUrl}/username/${userName}/roles`, data);
  }

  getDivisionOfCurrentUser(): Observable<any> {
    return this.http.get(`${this.baseUrl}/divisionOfCurrentUser`);
  }
}
