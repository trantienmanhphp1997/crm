import { maxInt32 } from 'src/app/core/utils/common-constants';
import { HttpWrapper } from './../apis/http-wapper';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root',
})
export class CommonCategoryService extends HttpWrapper {
	constructor(http: HttpClient) {
		super(http, `${environment.url_endpoint_category}/activities`);
	}

	getCommonCategory(parentCode, code?): Observable<any> {
		return this.http.get(`${environment.url_endpoint_category}/commons`, {
			params: {
				page: '0',
				size: maxInt32.toString(),
				commonCategoryCode: parentCode,
				code: code || '',
			},
		});
	}

	getCommonCategoryByListCode(commonCategoryCodeList: string[]): Observable<any> {
		return this.http.post(`${environment.url_endpoint_category}/commons/findByCommonCategoryCode`, commonCategoryCodeList);
	}


	getBeforeDay(): Observable<any> {
		return this.http.get(`${environment.url_endpoint_category}/holidays/getBeforeDay`);
	}

	getDayWorkingByDay(data): Observable<any> {
		return this.http.post(`${environment.url_endpoint_category}/holidays/getDayWorkingByDay`, data);
	}
}
