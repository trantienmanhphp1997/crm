import { Injectable } from '@angular/core';
import {
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate,
  UrlTree,
  NavigationEnd,
} from '@angular/router';
import { KeycloakService } from 'keycloak-angular';
import {
  ChannelResource,
  CommonCategory,
  FunctionCode,
  functionUri,
  maxInt32,
  Scopes,
  SessionKey
} from '../utils/common-constants';
import { SessionService } from './session.service';
import * as _ from 'lodash';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CustomKeycloakService } from 'src/app/pages/system/services/custom-keycloak.service';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { RmProfileService } from './rm-profile.service';
import { AppFunction } from '../interfaces/app-function.interface';
import { CommonCategoryService } from './common-category.service';
import { CalendarService } from '../../pages/calendar-sme/services/calendar.service';
import { CalendarAlertService } from '../components/calendar-alert/calendar-alert.service';
import { CalendarAlertInitService } from './calendar-alert-init.service';

@Injectable({
  providedIn: 'root',
})
export class AppInitService implements CanActivate {
  objFunction: AppFunction;
  isLoaded = false;

  constructor(
    protected readonly router: Router,
    private sessionService: SessionService,
    private keycloakService: KeycloakService,
    private calendarAlertService: CalendarAlertService,
    private customKeycloakService: CustomKeycloakService,
    private calendarService: CalendarService,
    private categoryService: CategoryService,
    private rmApi: RmProfileService,
    private commonService: CommonCategoryService,
    private calendarAlertInitService: CalendarAlertInitService,
  ) {
    let permission = false;
    this.router.events.subscribe((res) => {
      permission = this.sessionService.getSessionData(SessionKey.IS_PERMISSION);
      if (res instanceof NavigationEnd && res.url === functionUri.access_denied && permission) {
        this.router.navigateByUrl(functionUri.dashboard);
      }
    });
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    return new Promise(async (resolve) => {
      const division = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM);
      if (division) {
        this.isLoaded = true;
        resolve(true);
      } else {
        forkJoin([
          this.customKeycloakService.getFunctionByUser().pipe(catchError(() => of(undefined))),
          this.categoryService
            .searchResourceCategory({ page: 0, size: maxInt32, decentralizationCode: ChannelResource  })
            .pipe(catchError(() => of(undefined))),
          this.rmApi.getDivisionOfCurrentUser().pipe(catchError(() => of([]))),
          this.commonService
            .getCommonCategory(CommonCategory.SHARE_REVENUE_ROLE_INPUT)
            .pipe(catchError(() => of(undefined))),
          this.rmApi
            .getByUsername(this.keycloakService.getUsername(), { externalData: true })
            .pipe(catchError(() => of(undefined))),
          this.commonService.getCommonCategory(CommonCategory.KHOI_PRIORITY).pipe(catchError(() => of(undefined))),
        ]).subscribe(
          ([
            functionsOfUser,
            listResource,
            listDivisionOfRm,
            shareRevenueInputConfig,
            itemRm,
            divisionPriorityConfig,
          ]) => {
            this.saveLogLogin(itemRm);
            this.sessionService.setSessionData(SessionKey.KHOI_PRIORITY, _.get(divisionPriorityConfig, 'content', []));
            const listDivision = _.map(_.uniqBy(listDivisionOfRm, 'blockCode'), (item) => {
              return {
                titleCode: item?.titleGroupCode,
                titleId: item?.titleGroupId,
                levelCode: item?.levelRMCode,
                levelId: item?.levelRMId,
                code: item?.blockCode,
                name: item?.blockName,
                order:
                  _.findIndex(divisionPriorityConfig?.content, (i) => i.code === item.blockCode) === -1
                    ? 99
                    : _.findIndex(divisionPriorityConfig?.content, (i) => i.code === item.blockCode),
              };
            });
            const listShareRevenueInputConfig = _.get(shareRevenueInputConfig, 'content', []);
            for (const itemConfig of listShareRevenueInputConfig) {
              const itemDivision = _.find(listDivision, (d) => d.code === itemConfig?.code?.replace('TITLE_', ''));
              if (itemDivision && itemConfig?.value?.includes(itemDivision?.titleCode)) {
                const functionItem = _.find(
                  _.get(listResource, 'content'),
                  (i) => i.code === FunctionCode.REVENUE_SHARE
                );
                functionsOfUser?.push({ rsid: functionItem.id, scopes: [Scopes.VIEW] });
                break;
              }
            }
            this.sessionService.setSessionData(SessionKey.DIVISION_OF_RM, _.orderBy(listDivision, ['order']));
            this.sessionService.setSessionData(SessionKey.SHARE_REVENUE_ROLE_INPUT, listShareRevenueInputConfig);
            let listFunction = [];
            const listMenu = [];
            let isPermissionView = false;
            if (functionsOfUser?.length > 0) {
              for (const itemOfUser of functionsOfUser) {
                const item = listResource?.content?.find((itemFunction) => {
                  return itemFunction.id === itemOfUser?.rsid;
                });
                if (!isPermissionView && item && itemOfUser?.scopes?.includes(Scopes.VIEW)) {
                  isPermissionView = true;
                }
                if (item && itemOfUser?.scopes && itemOfUser?.scopes?.indexOf(Scopes.VIEW) > -1) {
                  this.objFunction = {
                    create: itemOfUser.scopes?.indexOf(Scopes.CREATE) > -1,
                    update: itemOfUser.scopes?.indexOf(Scopes.UPDATE) > -1,
                    view: itemOfUser.scopes?.indexOf(Scopes.VIEW) > -1,
                    delete: itemOfUser.scopes?.indexOf(Scopes.DELETE) > -1,
                    import: itemOfUser.scopes?.indexOf(Scopes.IMPORT) > -1,
                    export: itemOfUser.scopes?.indexOf(Scopes.VIEW) > -1,
                    log: itemOfUser.scopes?.indexOf(Scopes.LOG) > -1,
                    approve: itemOfUser.scopes?.indexOf(Scopes.APPROVE) > -1,
                    rmGrant: itemOfUser.scopes?.indexOf(Scopes.RM_GRANT) > -1,
                    assignKVH: itemOfUser.scopes?.indexOf(Scopes.ASSIGN_KVH) > -1,
                    unAssignKVH: itemOfUser.scopes?.indexOf(Scopes.UNASSIGN_KVH) > -1,
                    unLogUser: itemOfUser.scopes?.indexOf(Scopes.UNLOCKUSER) > -1,
                    restore: itemOfUser.scopes?.indexOf(Scopes.RESTORE) > -1,
                    revenueShare: itemOfUser.scopes?.indexOf(Scopes.REVENUE_SHARE) > -1,
                    onlyOPN: itemOfUser.scopes?.indexOf(Scopes.ONLY_OPN) > -1,
                    rsId: itemOfUser?.rsid,
                    rsUri: item?.uri,
                    rsCode: item?.code,
                    rsName: item?.name,
                    scopes: itemOfUser?.scopes,
                    HO: itemOfUser.scopes?.indexOf(Scopes.HO) > -1,
                    updateSME: itemOfUser.scopes?.indexOf(Scopes.UPDATE_SME) > -1,
                  };
                  this.sessionService.setSessionData(`FUNCTION_${item.code}`, this.objFunction);
                  listFunction.push(item);
                  this.findFunctionParent(item, listResource.content, listFunction);
                }
              }
              this.sessionService.setSessionData(SessionKey.IS_PERMISSION, isPermissionView);
              listFunction.sort((a, b) => {
                if (a.indexNumber === 0) {
                  a.indexNumber = null;
                }
                if (b.indexNumber === 0) {
                  b.indexNumber = null;
                }
                if (!b.indexNumber || !a.indexNumber) {
                  return -1;
                } else if (b.indexNumber && a.indexNumber && a.indexNumber !== b.indexNumber) {
                  return a.indexNumber - b.indexNumber; // asc
                }
              });
              listFunction = [...new Set(listFunction)];
              for (const item of listFunction) {
                if (!item.parentId) {
                  listMenu.push(item);
                }
              }
              this.getMenuChildren(listMenu, listFunction);
              this.sessionService.setSessionData(SessionKey.MENU, listMenu);
            }

            this.sessionService.setSessionData(SessionKey.USER_INFO, itemRm);

            this.calendarAlertInitService.getCalendarOfTheDay();
            // this.apmService.apm.setUserContext({
            //   username: itemRm.username,
            // });
            this.isLoaded = true;
            resolve(true);
            if (!isPermissionView) {
              this.router.navigateByUrl(functionUri.access_denied);
              return;
            }
          }
        );
      }
    });
  }

  findFunctionParent(item, listFunction, listResult) {
    if (item.parentId) {
      const itemFunction = listFunction.find((obj) => {
        return obj.id === item.parentId;
      });
      if (
        itemFunction &&
        listResult.findIndex((n) => {
          return n.id === itemFunction.id;
        }) === -1
      ) {
        listResult.push(itemFunction);
        this.findFunctionParent(itemFunction, listFunction, listResult);
      }
    }
  }

  getMenuChildren(listParent, listFunction) {
    for (const item of listParent) {
      item.subMenu = listFunction.filter((obj) => {
        return obj.parentId && obj.parentId === item.id;
      });
      this.getMenuChildren(item.subMenu, listFunction);
    }
  }

  saveLogLogin(item) {
    const isLogIn = localStorage.getItem('LOGIN');
    if (isLogIn !== 'LOGIN') {
      this.checkCountLogin(item);
      const listData = [];
      listData.push({
        userName: item?.fullName,
        userId: item?.username,
        functionCode: 'LOGIN',
        functionName: 'Đăng nhập',
        appCode: 'CRM_WEB',
        startTime: new Date().valueOf(),
      });
      listData.push({
        userName: item?.fullName,
        userId: item?.username,
        functionCode: FunctionCode.DASHBOARD,
        functionName: FunctionCode.DASHBOARD,
        appCode: 'CRM_WEB',
        startTime: new Date().valueOf(),
      });

      this.categoryService.saveTracing(listData)
        .subscribe((res: any) => {
          if (res) {
            // let listObjLog = JSON.parse(localStorage.getItem('LIST_FUNCTION_LOG')) || [];
            // listObjLog = [...listObjLog, ...res];
            // listObjLog = _.unionBy(listObjLog, 'functionCode');
            // const strList = JSON.stringify(listObjLog);
            // localStorage.setItem('LIST_FUNCTION_LOG', strList);
            localStorage.setItem('LOGIN', 'LOGIN');
          }
        });
    }
    else{
      this.checkCountLogin(item);
    }
  }
  checkCountLogin(item){
    const keyCountLogin = item?.username + '_' + this.convertDateToString();
    const keyCountLoginView = item?.username + '_' + this.convertDateToString() + '_VIEW';
    let value = localStorage.getItem(keyCountLogin);
    const checkView = localStorage.getItem(keyCountLoginView); //1-hien thi, 0-khong hien thi
    if(value && checkView == '1'){
      value = (Number(value) + 1) + '';
    }
    else if(!value){
      value = '1';
      localStorage.setItem(keyCountLoginView, '1'); //1-hien thi, 0-khong hien thi
    }
    console.log('key: ',keyCountLogin, ' , value: ',value);
    console.log('view check: ',localStorage.getItem(keyCountLoginView));
    localStorage.setItem(keyCountLogin, value);
  }
  convertDateToString() {
    const date = new Date();
    return (date.getFullYear() + '' + (date.getMonth() > 8 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1)) + '' + (date.getDate() > 9 ? date.getDate() : '0' + date.getDate()));
  }
}
