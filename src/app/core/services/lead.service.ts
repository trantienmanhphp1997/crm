import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LeadService {
  constructor(private http: HttpClient) {}

  getSidebars(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/leads/getSlideBarData`, { params });
  }

  getLanes(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_workflow}/lanes`, { params });
  }

  searchLeadPreviews(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/lead-previews/findLeadPreviewsDuplicate`, { params });
  }

  mergeLeadPreviews(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/lead-previews/merge`, data);
  }

  searchLeads(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm_relationship}/graph-relationship/leads/findAllByUser`, params);
  }

  getCustomersIsDuplicateByPhone(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm_relationship}/graph-relationship/customers/findAllByPhone`, {
      params,
    });
  }

  searchActivities(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_activity}/call-data`, { params });
  }

  getLeadById(id): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/leads/${id}`);
  }

  create(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/leads`, data, { responseType: 'text' });
  }

  update(data): Observable<any> {
    return this.http.put(`${environment.url_endpoint_sale}/leads/${data.id}`, data, { responseType: 'text' });
  }

  changeStatus(id, status): Observable<any> {
    return this.http.put(`${environment.url_endpoint_sale}/leads/changeStatusLead/${id}?status=${status}`, {});
  }

  convertLead(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/leads/convert`, data);
  }

  getActivityByLead(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_activity}/activities`, { params });
  }

  creatActivity(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/activities`, data, { responseType: 'text' });
  }

  getBranchByRegionCode(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/branches`, { params });
  }

  getUserByBranchCode(branchCode): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/users?briefRepresentation=0&branches=${branchCode}`);
  }

  getLogHistory(leadId) {
    return this.http.get(`${environment.url_endpoint_sale}/tracking?id=${leadId}`);
  }

  reAssignForUser(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/leads/reassign`, data);
  }

  reAssignForBranch(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_workflow}/processes/reassign`, data);
  }

  reStore(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/leads/restore`, data);
  }

  getCampaignByExpired(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/campaigns`, { params });
  }

  getInsightByLeadId(id): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/insights/findByCif?cif=${id}`);
  }

  getDetailLead(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/leads/detailLead`, { params });
  }

  searchLeadKHCN(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sale-potential/search`, params);
  }

  exportFileLeadKHCN(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sale-potential/export`, params, { responseType: 'text' });
  }

  createLeadKHCN(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sale-potential/create`, data);
  }

  updateLeadKHCN(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sale-potential/update`, data);
  }

  deleteLeadKHCN(code: string): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_sale}/sale-potential/delete/${code}`, { responseType: 'text' });
  }

  getLeadByCode(code: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/sale-potential/${code}`);
  }

  getRmByBranch(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sale-potential/findRmLead`, params);
  }

  searchKHCampaignByIdCard(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sale-potential/searchKH`, data);
  }

  createCampaignLead(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sale-potential/create-campaign-lead`, params);
  }
  searchLeadOnboarding(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/leads/find-lead-onboarding-biz`, data);
  }
  exportFileLeadOnboarding(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/leads/exportCustomerOB`, params, { responseType: 'text' });
  }
}
