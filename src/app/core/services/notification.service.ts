import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  baseUrl = environment.url_endpoint_admin;

  constructor(private http: HttpClient) {}

  findAll(offset: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/notify/list-all-notification?offset=${offset}`);
  }

  markAsRead(id): Observable<any> {
    return this.http.put(`${this.baseUrl}/notify/update-read-notification/${id}`, {});
  }

  markAllAsRead(): Observable<any> {
    return this.http.put(`${this.baseUrl}/notify/update-all-read-notification`, {});
  }

  delete(id): Observable<any> {
    return this.http.put(`${this.baseUrl}/notify/update-deleted-notification/${id}`, {});
  }
  getNotiLimit(): Observable<any> {
    return this.http.get(`${this.baseUrl}/notify/get-notify-limit`);
    // return this.http.get(`http://localhost:10169/crm01-admin/notify/get-notify-limit`);
  }
}
