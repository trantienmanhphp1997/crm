import { HttpWrapper } from './../apis/http-wapper';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ActivityService extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_activity}/activities`);
  }

  search(params): Observable<any> {
    return this.http.post(`${this.url}/findAll`, params);
  }

  create(data): Observable<any> {
    return this.http.post(`${this.url}`, data, { responseType: 'text' });
  }

  update(data): Observable<any> {
    return this.http.put(`${this.url}`, data, { responseType: 'text' });
  }

  record(data): Observable<any> {
    return this.http.post(`${this.url}/approveActivity`, data, { responseType: 'text' });
  }
}
