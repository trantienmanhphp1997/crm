/* tslint:disable:prefer-const */
import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})

export class EncrDecrService {
  constructor() { }
  base64Key = CryptoJS.enc.Hex.parse('0123456789abcdef0123456789abcdef')
  // The set method is use for encrypt the value.
  set(value) {
    const key = CryptoJS.enc.Utf8.parse(this.base64Key);
    const iv = CryptoJS.enc.Utf8.parse(this.base64Key);
    const encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(value.toString()), key,
      {
        keySize: 128 / 8,
        iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
      });

    return encodeURIComponent(encrypted.toString().replace('/[A-Z0-9._%+-]+@[A-Z0-9.-]+\.+[A-Z]{2,4}/g', ''));
  }

  // The get method is use for decrypt the value.
  get(value) {
    const key = CryptoJS.enc.Utf8.parse(this.base64Key);
    const iv = CryptoJS.enc.Utf8.parse(this.base64Key);
    const decrypted = CryptoJS.AES.decrypt(decodeURIComponent(value), key, {
      keySize: 128 / 8,
      iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7
    });
    return decrypted.toString(CryptoJS.enc.Utf8);
  }

}
