import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class MbeeChatService {

  baseUrl = environment.url_endpoint_customer;

  constructor(private http: HttpClient) {}

  generatePlugin(rsId): Observable<any> {
    return this.http.get(`${this.baseUrl}/mbee-chat-integration/generate-plugin?rsId=${rsId}&scope=VIEW`, {responseType: 'text'});
  }

  createGroupChat(customerId) {
    return this.http.post<any>(`${this.baseUrl}/mbee-chat-integration/group?customerCode=${customerId}`, {});
  }

}
