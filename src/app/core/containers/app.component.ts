import { CommonCategory, defaultExportExcel, functionUri, SessionKey } from 'src/app/core/utils/common-constants';
import { Subscription, of } from 'rxjs';
import { CommunicateService } from 'src/app/core/services/communicate.service';
import { OnInit, OnDestroy, HostBinding, AfterViewInit, ViewChild, ViewChildren, Inject } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { Component } from '@angular/core';
import { global } from '@angular/compiler/src/util';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { GridsterComponent } from 'angular-gridster2';
import { SessionService } from '../services/session.service';
import { AppFunction } from '../interfaces/app-function.interface';
import { CommonCategoryService } from '../services/common-category.service';
import * as _ from 'lodash';
import {CalendarAlertService} from "../components/calendar-alert/calendar-alert.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy, AfterViewInit {
  @HostBinding('class.app__root') appRoot = true;
  title = 'crm-nextgen';
  isLoaded = false;
  isLoading = false;
  isFireWorks = false;
  objFunction: AppFunction;
  subscription: Subscription;
  @ViewChild(FullCalendarComponent) calendar: FullCalendarComponent;
  @ViewChildren(GridsterComponent, { read: true }) gridster: GridsterComponent;
  clickListener: any;
  displayBanner = true;

  constructor(
    private keycloakService: KeycloakService,
    private communicateService: CommunicateService,
    private translate: TranslateService,
    private router: Router,
    private sessionService: SessionService,
    private commonService: CommonCategoryService,
    private calendarAlertService: CalendarAlertService
  ) {
    this.subscription = this.communicateService.request$.subscribe((req) => {
      if (req && req.name === 'FireWorks') {
        this.showFireWorks();
      }
    });
    this.calendarAlertService.isLoadingSubject.subscribe(status => {
      this.isLoading = status;
    })
  }

  ngOnInit() {
    console.log('version 1.5');
    global.userConfig = {
      pageSize: 10,
      isShowBanner: true,
    };
    this.keycloakService.getToken().then((token) => {
      this.sessionService.setSessionData(SessionKey.TOKEN, token);
      this.isLoaded = true;
    });
    this.translate
      .get('messageTable')
      .pipe(catchError(() => of(undefined)))
      .subscribe((messageTable) => {
        global.messageTable = messageTable;
      });
    this.commonService.getCommonCategory(CommonCategory.CONFIG_EXPORT_CUSTOMER_LIMIT_COUNT).subscribe((res) => {
      const maxExportExcel = +(
        res?.content?.find((item) => item.code === CommonCategory.EXPORT_CUSTOMER)?.value || defaultExportExcel
      );
      this.sessionService.setSessionData(SessionKey.LIMIT_EXPORT, maxExportExcel);
    });
  }

  ngAfterViewInit() {
    this.clickListener = (event) => {
      if (this.isClickToggleSidebar(event?.target) && this.router.url.split('/')[1] !== 'system') {
        if (document.querySelector('.show-sidebar')) {
          document.getElementsByTagName('html').item(0).className = '';
        } else {
          document.getElementsByTagName('html').item(0).className = 'show-sidebar';
        }
      }
    };
    document.addEventListener('click', this.clickListener);
  }

  private isClickToggleSidebar(el) {
    return (
      (!_.isEmpty(el?.className) && el?.className?.includes('btn-toggle-sidebar')) ||
      (!_.isEmpty(el?.parentElement?.className) && el?.parentElement?.className?.includes('btn-toggle-sidebar'))
    );
  }

  showFireWorks() {
    this.isFireWorks = true;
    setTimeout(() => {
      this.isFireWorks = false;
    }, 5000);
  }

  hiddenFireWorks() {
    this.isFireWorks = false;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
