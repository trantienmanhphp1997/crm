import { global } from '@angular/compiler/src/util';
import { ChangeDetectorRef, Component, HostBinding, Injector, OnInit } from '@angular/core';
import { LayoutService } from '../../services/layout.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { MbeeChatService } from '../../services/mbee-chat.service';
import { Division, FunctionCode, SessionKey } from '../../utils/common-constants';
import { SessionService } from '../../services/session.service';
import { AppFunction } from '../../interfaces/app-function.interface';
import { MatDialog } from '@angular/material/dialog';
import { PosterComponent } from '../../components/poster/poster.component';
import { map } from 'lodash';
import {PopupImageDTO, UploadImgService} from 'src/app/pages/system/services/upload-img.service';
import { forkJoin } from 'rxjs';
import { getLocalPosterList } from '../../utils/function';

class ImageSnippet {
  constructor(
    public src: string,
    public file: any
  ) { }
}
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit {
  @HostBinding('class.app__layout') appLayout = true;
  isShowBanner = true;
  isShowChat = false;
  objFunction: AppFunction;
  objFunctionMBChat: AppFunction;
  listDivision = [];
  commonData = {
    divisions: []
  }
  prefixImg = 'data:image/png;base64,';
  bannerList = [];
  poster = null;
  currUser = null;

  constructor(
    private layoutServive: LayoutService,
    private mbeeChatService: MbeeChatService,
    private http: HttpClient,
    private cdk: ChangeDetectorRef,
    protected sessionService: SessionService,
    public dialog: MatDialog,
    private uploadImageService: UploadImgService,
  ) {
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
    this.objFunctionMBChat = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.APP_CHAT}`);
  }

  ngOnInit(): void {
    this.layoutServive.bannerObs.subscribe(status => {
      this.isShowBanner = status;
      this.cdk.detectChanges();
    });
    this.getDivisionOfUser();
    this.embedMbChat();
  }

  ngAfterViewInit(): void {
    // show banner, poster
    this.getPopupData();
    this.getBannerData();

  }

  closeBanner() {
    this.layoutServive.bannerObs.next(!this.isShowBanner);
  }

  getDivisionOfUser(): void {
    this.commonData.divisions = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM);
    this.listDivision = map(this.commonData.divisions, (item) => {
      return { code: item.code, name: `${item.code} - ${item.name}` };
    });

  }

  getBannerData(): void {
    const imageFromDivision = this.listDivision.map(division => division.code);
    forkJoin(
      ...imageFromDivision.map(item => this.uploadImageService.getImgFromDivisions({ divisionCodes: item }))
    ).subscribe((imgs) => {
      imgs = imgs.filter(item => item);
      imgs.forEach(divisionImgList => {
        divisionImgList.forEach(item => {
          if (item.type === 'banner') {
            this.bannerList.push(`${this.prefixImg}${item.image}`);
          }
        });
      });
    })
  }
  // AnhTT comment doi logic xu ly popup
  // getPopupData(){
  //   const keyCountLogin = this.currUser?.username + '_' + this.convertDateToString();
  //   const keyCountLoginView = keyCountLogin + '_VIEW';
  //   const checkView = localStorage.getItem(keyCountLoginView);
  //   if(checkView == '1'){
  //     const countLogin : number = Number(localStorage.getItem(keyCountLogin)) - 1;
  //     const paramSearchPoster = {
  //       numberOfDisplay: countLogin,
  //       divisionCode: '',
  //       channel:['WEB']
  //     }
  //     this.uploadImageService.findPoster(paramSearchPoster).pipe().subscribe((poster) => {
  //       if (poster) {
  //         //  const index = Math.floor(Math.random() * (posterList.length));
  //         this.poster = `${this.prefixImg}${poster?.image}`;
  //         //   this.openPoster({ uuidImage: posterList[index].uuidImage })
  //         this.openPoster(poster);
  //       }
  //     });
  //   }
  // }
  getPopupData(){
    const keyCountLogin = this.currUser?.username + '_' + this.convertDateToString();
    const keyPopupImages = this.currUser?.username + '_' + this.convertDateToString() + '_POPUP_POSTER';
    const keyCountLoginView = keyCountLogin + '_VIEW';
    const checkView = localStorage.getItem(keyCountLoginView);
    let imageStorage: PopupImageDTO[] = JSON.parse(localStorage.getItem(keyPopupImages));
    if(checkView === '1' ){
      const paramSearchPoster = {
        numberOfDisplay: 0,
        divisionCode: '',
        channel:['WEB']
      }
      this.uploadImageService.findPoster(paramSearchPoster).pipe().subscribe((poster: PopupImageDTO[]) => {
        let imagePopup: PopupImageDTO;
        if (poster) {
          localStorage.setItem(keyCountLoginView,'0');
          if (imageStorage) {
            poster.forEach(item => {
              if(imagePopup) return;
              const imageInStorage: PopupImageDTO[] = imageStorage.filter(i => i.uuidImage === item.uuidImage);
              if (!imageInStorage || imageInStorage.length === 0 || !item.numberOfDisplay || !imageInStorage[0].countNumberShow || item.numberOfDisplay > imageInStorage[0].countNumberShow) {
                imagePopup = item;
              }
            })
          } else {
            imagePopup = poster[0];
          }
          if(!imagePopup){
            return;
          }
          if (imagePopup?.image) {
            this.poster = `${this.prefixImg}${imagePopup?.image}`;
            this.openPoster(imagePopup);
          } else {
            this.uploadImageService.getImagBase64(imagePopup.uuidImage).pipe().subscribe(img => {
              this.poster = `${this.prefixImg}${img?.base64Data}`;
              imagePopup.image = img?.base64Data;
              this.openPoster(imagePopup);
            })
          }
          // save localStorage
          const imageInStorageSave: PopupImageDTO[] = imageStorage?.filter(i => i.uuidImage === imagePopup.uuidImage);
          if(imageInStorageSave && imageInStorageSave.length>0){
            imageStorage.forEach(i=>{
              if(i.uuidImage === imagePopup.uuidImage) {
                i.countNumberShow = i.countNumberShow ? i.countNumberShow + 1 : 1;
                i.image = '';
              }
            })
          }else {
            imagePopup.countNumberShow = imagePopup.countNumberShow ? imagePopup.countNumberShow + 1 : 1;
            if (imageStorage) {
              imageStorage.push(imagePopup)
            } else {
              imageStorage = [imagePopup];
            }
          }
          localStorage.setItem(keyPopupImages, JSON.stringify(imageStorage));
          //----------------------
        }
      });
    }
  }
  convertDateToString() {
    const date = new Date();
    return (date.getFullYear() + '' + (date.getMonth() > 8 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1)) + '' + (date.getDate() > 9 ? date.getDate() : '0' + date.getDate()));
  }
  // checkCountLogin(item){
  //   const keyCountLogin = item?.username + '_' + this.convertDateToString();
  //   const keyCountLoginView = item?.username + '_' + this.convertDateToString() + '_VIEW';
  //   const keyCheckOff = item?.username + '_' + this.convertDateToString() + '_CHECKOFF';
  //   let value = localStorage.getItem(keyCountLogin);
  //   const checkView = localStorage.getItem(keyCountLoginView); //1-hien thi, 0-khong hien thi
  //   const checkOff = localStorage.getItem(keyCheckOff);//0-chua off, 1 - da off
  //   if(value && checkView == '1' && checkOff == '1'){
  //     value = (Number(value) + 1) + '';
  //   }
  //   else if(!value){
  //     value = '1';
  //     localStorage.setItem(keyCountLoginView, '1'); //1-hien thi, 0-khong hien thi
  //     localStorage.setItem(keyCountLoginView, '0');
  //   }
  //   console.log('key: ',keyCountLogin, ' , value: ',value);
  //   console.log('view check: ',localStorage.getItem(keyCountLoginView));
  //   localStorage.setItem(keyCountLogin, value);
  // }
  openPoster(poster): void {
    // const sessionKey = `${uuidImage}_session_poster_${this.currUser.username}`;
    // const liveKey = `${uuidImage}_poster_${this.currUser.username}`;
    // const currentKey = Object.keys(localStorage).find(key => key === sessionKey || key === liveKey);
    // const posterStatus = currentKey ? localStorage.getItem(currentKey) : 'show';
    //
    // if (posterStatus === 'hide') return;

    if(!poster){
      return;
    }
    console.log('poster: ', poster);
    console.log('iamge: ', this.poster);
    const dialogRef = this.dialog.open(PosterComponent, {
      panelClass: 'poster',
      disableClose: true,
      data: {
        image: this.poster,
        poster,
        username: this.currUser.username
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      const keyCountLoginView = this.currUser?.username + '_' + this.convertDateToString() + '_VIEW';
      localStorage.setItem(keyCountLoginView,'0');
      console.log(`Dialog result: ${result}`);
    });


  }

  embedMbChat() {
    this.isShowChat = this.objFunctionMBChat?.scopes.includes('VIEW');
    if (this.isShowChat) {
      this.mbeeChatService.generatePlugin(this.objFunctionMBChat.rsId)
        .subscribe(data => {
          if (data) {
            const scr = document.createElement('script');
            scr.innerText = data;
            scr.type = 'text/javascript';
            document.body.appendChild(scr);
          }
        });
    }
  }
}
