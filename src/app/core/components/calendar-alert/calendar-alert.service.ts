import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import { filter } from 'rxjs/operators';

export interface CalendarAlertModel {
  id: string;
  idCalendar: string;
  idCalendarRoot: string;
  title: string;
  time: string;
  timeRemaining: string;
  startDisplay: string;
  startTime: string;
  endTime: string;
  isNotAction: number;
  titleNoti:string
}

@Injectable({
  providedIn: 'root',
})
export class CalendarAlertService  {
  private subject = new Subject<CalendarAlertModel>();
  private defaultId = 'default-calendar';
  isLoadingSubject = new BehaviorSubject(false);

  onShow(id = this.defaultId): Observable<CalendarAlertModel> {
    return this.subject.asObservable().pipe(filter((x) => x && x.id === id));
  }

  show(notify: CalendarAlertModel) {
    notify.id = this.defaultId;
    this.subject.next(notify);
  }
}
