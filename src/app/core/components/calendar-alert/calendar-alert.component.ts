import {Component, Injector, Input, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {MessageService} from 'primeng/api';
import {AlertCalendarType, CommonCategory, FunctionCode, ListEventTypeCalendar,} from '../../utils/common-constants';
import {Subscription} from 'rxjs';
import {CalendarAlertModel, CalendarAlertService} from './calendar-alert.service';
import {Router} from '@angular/router';
import {CalendarService} from '../../../pages/calendar-sme/services/calendar.service';
import {SessionService} from '../../services/session.service';
import {finalize} from 'rxjs/operators';
import {CalendarEditModalComponent} from '../../../pages/calendar-sme/components/calendar-add-modal/calendar-edit-modal/calendar-edit-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CategoryService} from '../../../pages/system/services/category.service';
import {BaseComponent} from '../base.component';
import {ConfirmCalendarDialogComponent} from '../../../pages/calendar-sme/components/confirm-calendar-dialog/confirm-calendar-dialog.component';
import { NotificationService } from '../../services/notification.service';
@Component({
  selector: 'app-calendar-alert',
  templateUrl: './calendar-alert.component.html',
  styleUrls: ['./calendar-alert.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MessageService],
})
export class CalendarAlertComponent extends BaseComponent implements OnInit, OnDestroy  {
  @Input() id = 'default-calendar';
  @Input() fade = true;

  listEventType = ListEventTypeCalendar;
  isLoading = false;
  isCal = false;
  dataFake: any;
  isUpdateAll = false;
  data: {};
  startTimeDisplay: string;
  AlertCalendarType = AlertCalendarType;
  alertSubscription: Subscription;
  routeSubscription: Subscription;
  notifyType : any;
  title="Lời nhắc lịch";
  constructor(
    injector: Injector,
    // private router: Router,
    private message: MessageService,
    // private sessionService: SessionService,
    private categoryService :CategoryService,
    // private modalService: NgbModal,
    private service: CalendarAlertService,
    private calendarService: CalendarService,
    private notificationService: NotificationService

  ) {
    super(injector);
    this.alertSubscription = this.service.onShow(this.id).subscribe((notify) => {
      this.changeParams(notify);
      this.show(notify);
    });
  }
  ngOnInit() {
    this.categoryService.getCommonCategory(CommonCategory.CALENDAR_CONFIG).subscribe((value => {
      if (value.content) {
        Object.keys(value.content).forEach((key) => {
          this.listEventType.push({code:value.content[key].code, name:value.content[key].value
            , listSuggest: JSON.parse(value.content[key].description)})
        });
      }
    }));
  }

  ngOnDestroy() {
    this.alertSubscription.unsubscribe();
    this.routeSubscription?.unsubscribe();
  }

  changeParams(notify) {
    this.startTimeDisplay = notify.startDisplay;
  }

  show(notify: CalendarAlertModel) {
    if(notify.idCalendarRoot?.includes('NOTIFICATION_LIMIT_RESIGN') 
        || notify.idCalendarRoot?.includes('NOTIFICATION_LIMIT_LOW_MINING')
        || notify.idCalendarRoot?.includes('NOTIFICATION_LIMIT_NOT_ACTIVE')
        || notify.idCalendarRoot?.includes('NOTIFICATION_CAMPAIGN_APPROACH_CUSTOMER')
        || notify.idCalendarRoot?.includes('NOTIFICATION_CAMPAIGN_NEW_CUSTOMER')
        || notify.idCalendarRoot?.includes('NOTIFICATION_CAMPAIGN_NOT_ASSIGN_CUSTOMER')){
      this.isCal = true;  
      this.message.add({
        sticky: true,
        severity: 'success',
        summary: this.getLimitLength(notify.title,140),
        detail: notify.time,
        contentStyleClass: notify.titleNoti,
        styleClass: notify.endTime,
        id: notify.idCalendar,
        icon: notify.idCalendarRoot,
        data: notify.timeRemaining,
        life: notify.isNotAction,
      }); 
    }else{
      this.message.add({
        sticky: true,
        severity: 'success',
        summary: this.getLimitLength(notify.title,140),
        detail: notify.time,
        contentStyleClass: notify.startTime,
        styleClass: notify.endTime,
        id: notify.idCalendar,
        icon: notify.idCalendarRoot,
        data: notify.timeRemaining,
        life: notify.isNotAction,
      });
    }
    
  }

  getLimitLength(str?: string, length = 20, noWhiteSpace = 10): string {
    // nếu string không có khoảng trắng thì sẽ lấy length
    if (!!str && str.indexOf(' ') === -1) {
      return str.length <= length
        ? str
        : str.slice(0, noWhiteSpace) + '...';
    }
    return !!str
      ? str.length <= length
        ? str
        : str.slice(0, length) + '...'
      : '';
  }

  clear(el, data, repeat) {
    if(data?.icon?.includes('NOTIFICATION_LIMIT_RESIGN') 
    || data?.icon?.includes('NOTIFICATION_LIMIT_LOW_MINING')
    || data?.icon?.includes('NOTIFICATION_LIMIT_NOT_ACTIVE')
    || data?.icon?.includes('NOTIFICATION_CAMPAIGN_APPROACH_CUSTOMER')
    || data?.icon?.includes('NOTIFICATION_CAMPAIGN_NEW_CUSTOMER')
    || data?.icon?.includes('NOTIFICATION_CAMPAIGN_NOT_ASSIGN_CUSTOMER')){
      
      this.notificationService.markAsRead(data?.id).subscribe(() => {  
         
      });
    }
    el?.parentElement?.parentElement?.parentElement?.parentElement?.getElementsByClassName('p-toast-icon-close')[0]?.click();
    if (!repeat) {
      // call API không thông báo lại;
      const params = {
        id: data.id,
        idRoot: data.icon,
        isPushDueDate: data.data === AlertCalendarType.OUTDATED ? 1 : 0,
        isPushOutOfDate: data.data === AlertCalendarType.OUTDATED ? 0 : 1,
        start: data.contentStyleClass
      }
      this.calendarService.updateAlertCalendar(params).subscribe((res: any) => {
        if (res) {
          const listAlert = this.sessionService.getSessionData('CALENDAR_ALERT');
          listAlert.forEach(item => {
            if ((item.id != null && item.id === data.id) || (item.idRoot != null && item.idRoot === data.icon)) {
              item.isPushDueDate = data.data === AlertCalendarType.OUTDATED ? 1 : 0;
              item.isPushOutOfDate = data.data === AlertCalendarType.OUTDATED ? 0 : 1;
            }
          });
          // this.sessionService.setSessionData('CALENDAR_ALERT', newListAlert);
        }
        });
    }
  }

  goDetailCalendar(el,message) {

    if(message?.icon?.includes('NOTIFICATION_LIMIT_RESIGN') 
    || message?.icon?.includes('NOTIFICATION_LIMIT_LOW_MINING')
    || message?.icon?.includes('NOTIFICATION_LIMIT_NOT_ACTIVE')){
      let item = {};
      let listValue = message.icon.split('----')
      item["notificationType"] = listValue[0];
      item["user"] = listValue[1];
      item["time"] = new Date(message.detail);
      this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
        this.router.navigate(['/sale-manager/limit-customer'], {
          skipLocationChange: true,
          queryParams: {
            itemNoti: JSON.stringify(item)
          },
        }))
      this.notificationService.markAsRead(message?.id).subscribe(() => {  
        
      });
      el?.parentElement?.getElementsByClassName('p-toast-icon-close')[0]?.click();
    }else if(message?.icon?.includes('NOTIFICATION_CAMPAIGN_APPROACH_CUSTOMER')
    || message?.icon?.includes('NOTIFICATION_CAMPAIGN_NEW_CUSTOMER')
    || message?.icon?.includes('NOTIFICATION_CAMPAIGN_NOT_ASSIGN_CUSTOMER')){
      let listValue = message.icon.split('----')
      let campaignId = listValue[1];
      this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
        this.router.navigate(['/campaigns/campaign-management', 'detail-sme', campaignId], {
          skipLocationChange: true,
          queryParams: {
            tabIndex: 2
          }
      }));
      this.notificationService.markAsRead(message?.id).subscribe(() => {  
        
      });
      el?.parentElement?.getElementsByClassName('p-toast-icon-close')[0]?.click();
    }else{
      const obj = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CALENDARSME}`);
      // this.isLoading = true;
      if (message.id) {
        this.service.isLoadingSubject.next(true);
        // const param = {rsId:this.objFunction.rsId, scope:'VIEW' };
        const param = {rsId:obj?.rsId, scope:'VIEW' };
        this.calendarService.getDetailEvents(message.id, param).pipe(
          finalize(() => {
            el?.parentElement?.getElementsByClassName('p-toast-icon-close')[0]?.click();
            this.service.isLoadingSubject.next(false);
          })
        ).subscribe(value => {
          if (value) {
            this.dataFake = value;
            this.dataFake.listUser = [{
              hrsCodeAssigns: value?.userHrsCode,
              name:value?.userCode + ' - ' + value?.fullNameUser,
              status: value?.status,
              reason: value?.reason,
            },...value.listEventAssigns.map(item => {
              return {
                hrsCodeAssigns: item?.userHrsCode,
                name:item?.userCode + ' - ' + item?.fullNameUser,
                status: item?.status,
                reason: item?.reason,
              };
            })];
            this.dataFake.start = new Date( this.dataFake.start);
            this.dataFake.end = new Date( this.dataFake.end);

            const modal = this.modalService.open(CalendarEditModalComponent, { windowClass: 'create-calendar-modal' });
            // this.isLoading = false;
            modal.componentInstance.isDetailEvent = true;
            modal.componentInstance.data = this.dataFake;
            modal.componentInstance.isUpdateAll = true;
            modal.componentInstance.listEventType = this.listEventType;
            modal.result
              .then((result) => {
                if (!result.isDelete) {
                  if (result) {
                    result.value.status = this.updateStatus(result.value.status);
                    this.data = result.value;
                    this.calendarService.updateSme(this.data).subscribe(
                      (item) => {
                        this.messageService.success(this.notificationMessage.success);
                      },
                      () => {
                        this.messageService.error(this.notificationMessage.error);
                        // this.onChangeLoading.emit(false);
                      }
                    );
                  }
                } else {
                }
              })
          }
        },() => {
          this.messageService.error('Dữ liệu không tồn tại');
        });
      } else {
        this.openDialogConfirm(el,message,obj?.rsId);
      }
    }
  }

  updateStatus(status: string) {
    if (status !== 'DONE' && status !== 'WAIT' && status !== 'REJECT') {
      status = 'NEW';
    }
    return status;
  }

  openDialogConfirm(el,message,rsId) {
    // phần này là cải tiến lịch chọn update 1 hay tất cả lịch, tạm thời pending
    // let value = '';
    // this.translate
    //   .get('calendar.chooseDialog')
    //   .subscribe((data) => {
    //     value = data
    //   });
    // const confirm = this.modalService.open(ConfirmCalendarDialogComponent, { windowClass: 'confirm-dialog' });
    // confirm.result
    //   .then((confirmed: number) => {
    //     if (confirmed !== 0) {
    //       if (confirmed === 2) {
    //         this.isUpdateAll = true;
    //       }
    //       this.getDetailEventLoop(el,message,rsId);
    //     }
    //   })
    //   .catch(() => {});

    this.isUpdateAll = true;
    this.getDetailEventLoop(el,message,rsId);
  }

  getDetailEventLoop(el,message,rsIds) {
    this.service.isLoadingSubject.next(true);
    const param = {rsId: rsIds, scope:'VIEW', idRoot: message.icon, startDate: message.contentStyleClass
      , endDate: message.styleClass };
    this.calendarService.getDetailEventsLoop(param).pipe(
      finalize(() => {
        el?.parentElement?.getElementsByClassName('p-toast-icon-close')[0]?.click();
        this.service.isLoadingSubject.next(false);
      })
    ).subscribe(value => {
      if (value) {
        this.dataFake = value;
        this.dataFake.listUser = [{
          hrsCodeAssigns: value?.userHrsCode,
          name:value?.userCode + ' - ' + value?.fullNameUser,
          status: value?.status,
          reason: value?.reason,
        },...value.listEventAssigns.map(item => {
          return {
            hrsCodeAssigns: item?.userHrsCode,
            name:item?.userCode + ' - ' + item?.fullNameUser,
            status: item?.status,
            reason: item?.reason,
          };
        })];
        this.dataFake.start = new Date( this.dataFake.start);
        this.dataFake.end = new Date( this.dataFake.end);

        const modal = this.modalService.open(CalendarEditModalComponent, { windowClass: 'create-calendar-modal' });
        modal.componentInstance.isDetailEvent = true;
        modal.componentInstance.data = this.dataFake;
        modal.componentInstance.isUpdateAll = this.isUpdateAll;
        modal.componentInstance.listEventType = this.listEventType;
        modal.result
          .then((result) => {
            if (!result.isDelete) {
              if (result) {
                result.value.status = this.updateStatus(result.value.status);
                this.data = result.value;
                if (this.isUpdateAll) {
                  this.calendarService.updateSme(this.data).subscribe(
                    (item) => {
                      this.messageService.success(this.notificationMessage.success);
                    },
                    () => {
                      this.messageService.error(this.notificationMessage.error);
                    }
                  );
                } else {
                  this.calendarService.updateSmeOneRecord(this.data).subscribe(
                    (item) => {
                      this.messageService.success(this.notificationMessage.success);
                    },
                    () => {
                      this.messageService.error(this.notificationMessage.error);
                    }
                  );
                }
              }
            } else {
            }
          })
      }
    },() => {
      this.messageService.error(this.notificationMessage.error);
    });
  }
}
