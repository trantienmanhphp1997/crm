import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { NotifyType } from '../../utils/common-constants';

export interface NotifyMessageModel {
  id: string;
  type: NotifyType;
  message: string;
  autoClose: boolean;
}

@Injectable({
  providedIn: 'root',
})
export class NotifyMessageService {
  private subject = new Subject<NotifyMessageModel>();
  private defaultId = 'default-notify';
  private options: any = {
    autoClose: true,
    keepAfterRouteChange: true,
  };

  onShow(id = this.defaultId): Observable<NotifyMessageModel> {
    return this.subject.asObservable().pipe(filter((x) => x && x.id === id));
  }

  // convenience methods
  success(message: string) {
    this.show({ ...this.options, type: NotifyType.Success, message });
  }

  error(message: string) {
    this.show({ ...this.options, type: NotifyType.CusError, message });
  }

  info(message: string) {
    this.show({ ...this.options, type: NotifyType.Info, message });
  }

  warn(message: string) {
    this.show({ ...this.options, type: NotifyType.Warning, message });
  }

  show(notify: NotifyMessageModel) {
    notify.id = notify.id || this.defaultId;
    this.subject.next(notify);
  }

  // clear(id = this.defaultId) {
  //   this.subject.next(id);
  // }
}
