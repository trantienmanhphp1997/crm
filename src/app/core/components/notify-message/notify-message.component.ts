import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { NotifyMessageModel, NotifyMessageService } from './notify-message.service';
import { Router } from '@angular/router';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'primeng/api';
import { TimeShowNotify } from '../../utils/common-constants';

@Component({
  selector: 'notify-message',
  templateUrl: './notify-message.component.html',
  styleUrls: ['./notify-message.component.scss'],
  animations: [
    trigger('flyInOut', [
      state('in', style({ transform: 'translateX(0)' })),
      transition('void => *', [style({ transform: 'translateX(100%)' }), animate(200)]),
      transition('* => void', [animate(200, style({ transform: 'translateX(100%)' }))]),
    ]),
  ],
  providers: [MessageService],
})
export class NotifyMessageComponent implements OnInit, OnDestroy {
  @Input() id = 'default-notify';
  @Input() fade = true;

  alerts: NotifyMessageModel[] = [];
  alertSubscription: Subscription;
  routeSubscription: Subscription;
  message: any;

  constructor(
    private router: Router,
    private service: NotifyMessageService,
    private translate: TranslateService,
    private messageService: MessageService
  ) {
    this.translate.get('messages').subscribe((res) => {
      this.message = res;
    });
    this.alertSubscription = this.service.onShow(this.id).subscribe((notify) => {
      this.show(notify);
    });
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.alertSubscription.unsubscribe();
    this.routeSubscription?.unsubscribe();
  }

  removeAlert(alert: NotifyMessageModel) {
    if (!this.alerts.includes(alert)) {
      return;
    }
    this.alerts = this.alerts.filter((x) => x !== alert);
  }

  show(notify: NotifyMessageModel) {
    this.messageService.add({
      sticky: !notify.autoClose,
      severity: notify.type,
      summary: this.message[notify.type],
      detail: notify.message,
      life: TimeShowNotify,
      id: notify.id,
    });
  }
}
