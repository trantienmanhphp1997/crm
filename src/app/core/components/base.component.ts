import { retry } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { CommunicateService } from 'src/app/core/services/communicate.service';
import { TranslateService } from '@ngx-translate/core';
import { Injector, HostBinding, OnDestroy, ViewChild, ChangeDetectorRef, Component } from '@angular/core';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder } from '@angular/forms';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { global } from '@angular/compiler/src/util';
import { NotifyMessageService } from './notify-message/notify-message.service';
import { ConfirmService } from '../services/confirm.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { CommonCategory, defaultExportExcel, ScreenType, SessionKey } from '../utils/common-constants';
import { SessionService } from '../services/session.service';
import { AppFunction } from '../interfaces/app-function.interface';
import { CommonCategoryService } from '../services/common-category.service';
import {CalendarAlertService} from './calendar-alert/calendar-alert.service';

@Component({
  template: `<ng-content></ng-content>`,
})
export class BaseComponent implements OnDestroy {
	public prop: any;
	public fields: any;
	public labelTitle: any;
	public notificationMessage: any;
	public confirmMessage: any;
	public screenType = {
		create: ScreenType.Create,
		update: ScreenType.Update,
		detail: ScreenType.Detail,
	};
	public messages = global.messageTable;
	public objFunction: AppFunction;
	public objFunctionCampaignRM: AppFunction;
	public maxExportExcel = defaultExportExcel;
	public isLoading: boolean;
	public currUser: any;
	public isDownLoading = false;
	public now = new Date();
	protected configModal: NgbModalConfig;
	protected translate: TranslateService;
	protected messageService: NotifyMessageService;
	protected modalService: NgbModal;
	protected confirmService: ConfirmService;
	protected fb: FormBuilder;
	protected router: Router;
	protected route: ActivatedRoute;
	protected location: Location;
	protected communicateService: CommunicateService;
	protected sessionService: SessionService;
	protected ref: ChangeDetectorRef;
	protected commonService: CommonCategoryService;
	protected state: any;
    protected calendarAlertService: CalendarAlertService;
	subscription: Subscription;
	subscriptions: Subscription[] = [];
	@ViewChild('table') table: DatatableComponent;

  @HostBinding('class.app__right-content') appRightContent = true;

  constructor(private injector: Injector) {
    this.init();
    this.configModal.backdrop = 'static';
    this.configModal.keyboard = false;
    this.translate.get(['fields', 'notificationMessage', 'title', 'confirmMessage']).subscribe((result) => {
      this.notificationMessage = result.notificationMessage;
      this.fields = result.fields;
      this.labelTitle = result.title;
      this.confirmMessage = result.confirmMessage;
    });
    this.state = this.router.getCurrentNavigation()?.extras?.state;
    this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
    this.subscriptions.push(
      this.communicateService.request$.subscribe((req) => {
        if (req?.name === 'export-completed' && req?.key) {
          this.isDownLoading = false;
          this.sessionService.setSessionData(req?.key, false);
        }
      })
    );
    this.maxExportExcel = +(this.sessionService.getSessionData(SessionKey.LIMIT_EXPORT) || defaultExportExcel);
  }

  init() {
    this.configModal = this.injector.get(NgbModalConfig);
    this.translate = this.injector.get(TranslateService);
    this.messageService = this.injector.get(NotifyMessageService);
    this.calendarAlertService = this.injector.get(CalendarAlertService);
    this.modalService = this.injector.get(NgbModal);
    this.fb = this.injector.get(FormBuilder);
    this.router = this.injector.get(Router);
    this.route = this.injector.get(ActivatedRoute);
    this.location = this.injector.get(Location);
    this.confirmService = this.injector.get(ConfirmService);
    this.communicateService = this.injector.get(CommunicateService);
    this.sessionService = this.injector.get(SessionService);
    this.ref = this.injector.get(ChangeDetectorRef);
    this.commonService = this.injector.get(CommonCategoryService);
  }

  back() {
    this.isLoading = true;
    if (global?.previousUrl) {
      this.router.navigateByUrl(global.previousUrl, { state: this.prop ? this.prop : this.state });
    } else {
      this.location.back();
    }
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
    this.subscriptions?.forEach((sub) => {
      sub.unsubscribe();
    });
  }
}
