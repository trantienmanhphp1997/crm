import {Component, OnDestroy, OnInit, ElementRef, ViewEncapsulation, Injector} from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import * as moment from 'moment';
import {BehaviorSubject, of, Subscription} from 'rxjs';
import { TaskService } from 'src/app/pages/tasks/services/tasks.service';
import { AppConfig } from '../../interfaces/appconfig.interface';
import { AppConfigService } from '../../services/appconfig.service';
import {
  CommonCategory,
  FunctionCode,
  maxInt32,
  PriorityTaskOrCalendar,
  SessionKey,
  functionUri,
  StatusTask
} from '../../utils/common-constants';
import * as _ from 'lodash';
import { NotificationService } from '../../services/notification.service';
import { NotifyModel } from '../../interfaces/notify.interface';
import {catchError, finalize} from 'rxjs/operators';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {LeadModalComponent} from '../../../pages/lead/components';
import {ConfirmCalendarDialogComponent} from '../../../pages/calendar-sme/components/confirm-calendar-dialog/confirm-calendar-dialog.component';
import {CalendarEditModalComponent} from '../../../pages/calendar-sme/components/calendar-add-modal/calendar-edit-modal/calendar-edit-modal.component';
import {CalendarService} from '../../../pages/calendar-sme/services/calendar.service';
import {NotifyMessageService} from '../notify-message/notify-message.service';
import {CategoryService} from '../../../pages/system/services/category.service';
import {CalendarAlertModel, CalendarAlertService} from '../calendar-alert/calendar-alert.service';
import {SessionService} from '../../services/session.service';
import { nullSafeIsEquivalent } from '@angular/compiler/src/output/output_ast';
import {ProductService} from '../../services/product.service';
@Component({
  selector: 'app-config',
  templateUrl: './app-config.component.html',
  styleUrls: ['./app-config.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppConfigComponent implements OnInit, OnDestroy {
  active: boolean;
  isDrag = false;
  scale = 14;
  scales: number[] = [12, 13, 14, 15, 16];

  outsideClickListener: any;

  isUpdateAll = false;
  dataFake: any;
  data: {};
  listEventType = [];
  id: '';

  config: AppConfig;
  date = moment();
  title: string;
  listTask = [];
  listTaskTerm = [];
  statusTask = StatusTask;
  subscription: Subscription;

  countUnReadNotify = 0;
  listNotify: NotifyModel[] = [];
  offset = 0;
  countNotify = 0;
  interval: any;
  type = 'task';
  isLoading = false;
  objFunctionCalendar: any;

  constructor(
    private el: ElementRef,
    private configService: AppConfigService,
    private modalService: NgbModal,
    private router: Router,
    private taskService: TaskService,
    private messageService: NotifyMessageService,
    private calendarService: CalendarService,
    private  categoryService :CategoryService,
    private calendarAlertService: CalendarAlertService,
    private notificationService: NotificationService,
    private sessionService: SessionService,
    private productService: ProductService,
  ) {}

  ngOnInit() {
    const params: any = {
      size: maxInt32,
      page: 0,
      search: '',
    };

    this.taskService.searchTaskAssignUp(params).subscribe((listTask) => {
      this.listTaskTerm = _.chain(_.get(listTask, 'content')).value();
      this.getTasksByDate();
    });
    this.title = this.date.locale('vi').format('dddd, LL');
    this.config = this.configService.config;
    this.subscription = this.configService.configUpdate$.subscribe((config) => {
      this.config = config;
      if (this.config.theme === 'nano') this.scale = 12;
      else this.scale = 14;

      this.applyScale();
    });
    this.intervalData();
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.active = false;
      }
    });

    if (this.config.theme === 'nano') this.scale = 12;

    this.categoryService.getCommonCategory(CommonCategory.CALENDAR_CONFIG).subscribe((value => {
      if (value.content) {
        Object.keys(value.content).forEach((key) => {
          this.listEventType.push({code:value.content[key].code, name:value.content[key].value
            , listSuggest: JSON.parse(value.content[key].description)})
        });
      }
    }))
    this.notificationService.getNotiLimit().subscribe((value => {
      if (value) {
        value.forEach(element => {
          this.alertNotifyLimit(element)
        });
      }
    }))
    // this.alertCalendarInvite(notify);
  }

  getTasksByDate() {
    this.listTask = _.chain(this.listTaskTerm)
      .filter(
        (item) =>
          item.refStatus !== StatusTask.DONE &&
          moment().isSameOrAfter(moment(item.createdAt)) &&
          moment(item.dueDate).isAfter(moment(this.date))
      )
      .value();
  }

  toggleConfigurator(event: Event) {
    if (this.isDrag) {
      return;
    }
    this.active = !this.active;
    event.preventDefault();

    if (this.active) this.bindOutsideClickListener();
    else this.unbindOutsideClickListener();
  }

  openConfigurator(event: Event, type) {
    this.saveLogNotification();
    this.listNotify.forEach((notify:any) => {
      if ((notify.notificationType === 'NOTIFICATION_ONBOARDING_BIZ' || notify.notificationType === 'NOTIFICATION_ONBOARDING_BIZ_KHTN') && !notify.title.includes('Bạn còn 5 phút')) {
        notify.title = this.calculateRemainingTime(notify);
      }
    });
    if (this.isDrag) {
      return;
    }
    this.active = true;
    event.preventDefault();
    this.type = type;
    if (this.active) this.bindOutsideClickListener();
    else this.unbindOutsideClickListener();
  }

  hideConfigurator(event) {
    this.active = false;
    this.unbindOutsideClickListener();
    event.preventDefault();
  }

  changeTheme(event: Event, theme: string, dark: boolean) {
    const themeElement = document.getElementById('theme-link');
    themeElement.setAttribute('href', themeElement.getAttribute('href').replace(this.config.theme, theme));
    this.configService.updateConfig({ ...this.config, ...{ theme, dark } });
    event.preventDefault();
  }

  onRippleChange() {
    this.configService.updateConfig(this.config);
  }

  bindOutsideClickListener() {
    if (!this.outsideClickListener) {
      this.outsideClickListener = (event) => {
        if (this.active && this.isOutsideClicked(event)) {
          this.active = false;
        }
      };
      document.addEventListener('click', this.outsideClickListener);
    }
  }

  unbindOutsideClickListener() {
    if (this.outsideClickListener) {
      document.removeEventListener('click', this.outsideClickListener);
      this.outsideClickListener = null;
    }
  }

  isOutsideClicked(event) {
    return !(this.el.nativeElement.isSameNode(event.target) || this.el.nativeElement.contains(event.target));
  }

  decrementScale() {
    this.scale--;
    this.applyScale();
  }

  incrementScale() {
    this.scale++;
    this.applyScale();
  }

  applyScale() {
    document.documentElement.style.fontSize = this.scale + 'px';
  }

  generateClassTask(item) {
    if (item.level === PriorityTaskOrCalendar.High) {
      return 'text-red';
    } else if (item.level === PriorityTaskOrCalendar.Medium) {
      return 'text-yellow';
    } else if (item.level === PriorityTaskOrCalendar.Low) {
      return 'text-green';
    }
  }

  selectedDate(e) {
    this.date = moment(e);
    this.title = this.date.locale('vi').format('dddd, LL');
    this.getTasksByDate();
  }

  startDrag(e) {
    this.isDrag = true;
  }

  endDrag(e) {
    const timer = setTimeout(() => {
      this.isDrag = false;
      clearTimeout(timer);
    }, 500);
  }

  intervalData() {
    this.notificationService
      .findAll(0)
      .pipe(catchError(() => of(undefined)))
      .subscribe((data) => {
        if (data) {
          this.countUnReadNotify = +(data?.countUnreadNotification || 0);
          data?.notifications?.reverse().forEach((item) => {
            if (_.findIndex(this.listNotify, (i) => i.id === item.id) === -1) {
              const notify = {
                id: item.id,
                title: item.content,
                time: item.sendDate,
                user: item.user,
                avatar: {
                  style: {
                    ['background-color']: item.notificationDescriptionDTO?.color,
                    color: item.notificationDescriptionDTO?.textColor,
                  },
                  label: item.notificationDescriptionDTO?.description,
                },
                isReaded: item.read,
                notificationType: item.notificationType,
                campaignId: item.campaignId,
                jsonParameter: item.jsonParameter
              };
              if (notify.notificationType === 'NOTIFICATION_ONBOARDING_BIZ'
                  || notify.notificationType === 'NOTIFICATION_ONBOARDING_BIZ_KHTN') {
                if (!notify.title.includes('Bạn còn 5 phút')) {
                  notify.title = this.calculateRemainingTime(notify);
                }
                this.getCustomerCode(notify);
              }
              if (notify.notificationType === 'NOTIFICATION_CALENDAR_ASSIGN_LOOP'
                || notify.notificationType === 'NOTIFICATION_CALENDAR_ASSIGN'
                || notify.notificationType === 'NOTIFICATION_CALENDAR_ASSIGN_REPLY'
                || notify.notificationType === 'NOTIFICATION_CALENDAR_ASSIGN_REPLY_LOOP') {
                this.getInfoCalendar(notify);
              }
              this.listNotify.unshift(notify);
            }
          });
          this.countNotify = this.listNotify?.length || 0;
          const timeDelay = 60000;
          const timer = setTimeout(() => {
            this.intervalData();
            clearTimeout(timer);
          }, timeDelay);
        }
      });
  }

  getInfoCalendar(notify) {
    const data = notify.title.slice(notify.title.indexOf('idCalendar:'));
    notify.idCalendar = notify.notificationType === ('NOTIFICATION_CALENDAR_ASSIGN_LOOP' || 'NOTIFICATION_CALENDAR_ASSIGN_REPLY_LOOP')
      ? data.slice(11, data.indexOf('startTime')-1) : data.slice(11);
    notify.startDate = data.slice(data.indexOf('startTime') + 10, data.toString().indexOf('endTime') - 1);
    notify.endDate = data.slice(data.indexOf('endTime') +8 );
    notify.title = notify.title.slice(0,notify.title.indexOf('idCalendar:'));
    // nếu là thông báo được mời thì hiện popup
    if (notify.notificationType === 'NOTIFICATION_CALENDAR_ASSIGN' ||  notify.notificationType === 'NOTIFICATION_CALENDAR_ASSIGN_LOOP') {
      this.alertCalendarInvite(notify);
    }
  }
  getCustomerCode(notify) {
    notify.code = notify.title.substring(notify.title.indexOf('http://') + 7,notify.title.indexOf('-'));
    notify.title = notify.title.replace(notify.title.substring(notify.title.indexOf('http://'),notify.title.indexOf('-') + 1), '')  ;
  }

  calculateRemainingTime(notify) {
    const diff = Math.abs((new Date()).valueOf() - new Date(notify.time).valueOf());
    const minutes = Math.floor((diff/1000)/60);
    if (minutes < 30 ) {
     return notify.title.substring(0,notify.title.lastIndexOf(':') + 1) + ' ' + (29-minutes) + ' phút.';
    } else {
      return notify.title.substring(0,notify.title.lastIndexOf(':') + 1)  + ' 0 phút.';
    }
  }

  markAllAsRead() {
    if (this.countUnReadNotify > 0) {
      this.notificationService.markAllAsRead().subscribe(() => {
        this.countUnReadNotify = 0;
        this.listNotify?.forEach((item) => {
          item.isReaded = true;
        });
      });
    }
  }

  markAsRead(item) {
    if (!item.isReaded) {
      this.notificationService.markAsRead(item?.id).subscribe(() => {
        this.countUnReadNotify -= 1;
        item.isReaded = true;
        this.navigationByNotificationType(item);
      });
    } else {
      this.navigationByNotificationType(item);
    }
  }

  navigationByNotificationType(item) {
    this.objFunctionCalendar = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CALENDARSME}`);
    if (item.notificationType === 'NOTIFICATION_ASSIGN_LEAD_GROUP'
      || item.notificationType === 'NOTIFICATION_ASSIGN_CUSTOMER_GROUP'
      || item.notificationType === 'NOTIFICATION_UNASSIGN_LEAD_GROUP'
      || item.notificationType === 'NOTIFICATION_PREPARE_TO_EXPIRE_LEAD_GROUP'
      || item.notificationType === 'NOTIFICATION_EXPIRE_LEAD_GROUP'
      || item.notificationType === 'NOTIFICATION_UNASSIGN_CUSTOMER_GROUP') {
      const formSearch = {
        id: item.id,
        notiType: item.notificationType,
      };
      const modal = this.modalService.open(LeadModalComponent, { windowClass: 'list__lead-modal' });
      // modal.componentInstance.config = config;
      modal.componentInstance.dataSearch = formSearch;
      modal.componentInstance.notificationType = item.notificationType;
      // modal.componentInstance.listHrsCode = [this.form.controls.hrsCode.value];
      modal.result
        .then((res) => {});
    }
    if (item.notificationType === 'NOTIFICATION_ONBOARDING_BIZ') {
      this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
        this.router.navigate(['/customer360/customer-manager', 'detail', 'sme', item.code], {
          skipLocationChange: true,
          queryParams: {
            manageRM: item.manageRM,
            showBtnRevenueShare: false,
          },
        }))
    } else if (item.notificationType === 'NOTIFICATION_CALENDAR_ASSIGN_LOOP' || item.notificationType === 'NOTIFICATION_CALENDAR_ASSIGN_REPLY_LOOP') {
        this.openDialogConfirm(item, this.objFunctionCalendar.rsId);
    } else if (item.notificationType === 'NOTIFICATION_CALENDAR_ASSIGN' || item.notificationType === 'NOTIFICATION_CALENDAR_ASSIGN_REPLY') {
        this.getDetailEvent(item, this.objFunctionCalendar.rsId);
    } else if ( item.notificationType === 'NOTIFICATION_LIMIT_RESIGN' || item.notificationType === 'NOTIFICATION_LIMIT_NOT_ACTIVE' || item.notificationType === 'NOTIFICATION_LIMIT_LOW_MINING'){
      this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
        this.router.navigate(['/sale-manager/limit-customer'], {
          skipLocationChange: true,
          queryParams: {
            itemNoti: JSON.stringify(item)
          },
        }))
    } else if (item.notificationType === 'NOTIFICATION_ONBOARDING_BIZ_KHTN') {
      this.router.navigate(['/lead/lead-management', 'detail', item.code], {
        skipLocationChange: true,
        queryParams: {
          customerType: '1'
        }});

    } else if (item.notificationType === 'NOTI_NEW_CAMPAIGN') {
      this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
      this.router.navigate(['/campaigns/campaign-management', 'detail-sme', item.campaignId], {
        skipLocationChange: true,
      }));
    } else if (item.notificationType === 'NOTIFICATION_CAMPAIGN_APPROACH_CUSTOMER' ||
              item.notificationType === 'NOTIFICATION_CAMPAIGN_NEW_CUSTOMER' ||
              item.notificationType === 'NOTIFICATION_CAMPAIGN_NOT_ASSIGN_CUSTOMER') {
      this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
        this.router.navigate(['/campaigns/campaign-management', 'detail-sme', item.campaignId], {
          skipLocationChange: true,
          queryParams: {
            tabIndex: 2
          }
      }));
    }
    else if(item.notificationType === 'NOTIFICATION_RM_NOT_ASSIGN_GROUP'){
      this.router.navigateByUrl('/',{skipLocationChange: true}).then(() =>
        this.router.navigate(['/rm360/rm-group'], {
          skipLocationChange: true,
        }));
    }
    else if(item.notificationType === 'CAMPAIGN_ASSIGN_NOTIFICATION_CBQL' || item.notificationType === 'CAMPAIGN_EXPIRE_NOTIFICATION_CBQL'){
      this.router.navigateByUrl('/',{skipLocationChange: true}).then(() =>
        this.router.navigate(['/campaigns/campaign-management/detail/'+ item.campaignId], {
          skipLocationChange: true,
        }));
    }
    // dieu huong salekit
    else if(item?.jsonParameter){
      const dataSearch = JSON.parse(item.jsonParameter);
      const data = {
        compareProductId: dataSearch?.saleKitProductId,
        categoryId: dataSearch?.saleKitCategoryId,
        productId: dataSearch?.saleKitProductId,
        salekitType: dataSearch?.saleKitType,
        block: dataSearch?.division
      }
      let checkExists = 1;
      const objFunctionSalekit = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.SALEKIT}`);
      if(!objFunctionSalekit){
        this.messageService.warn('Bạn không có quyền truy cập vào salekit này!');
        return;
      }
      this.productService
        .getProductDetail(dataSearch?.saleKitProductId).pipe()
        .subscribe((result: any) => {
          console.log('result: ', result);
          if(result?.id){
             if(result?.effectiveDateTo){
               const currentDate = new Date().getTime();
               const effectDateTo = moment(result?.effectiveDateTo, 'DD/MM/YYYY').toDate().getTime();
               if(effectDateTo < currentDate){
                 checkExists = 0;
               }
             }
             if(!result?.isActive){
               checkExists = 0;
             }
          }
          else{
            checkExists = 0;
          }
          if(checkExists === 0){
            this.messageService.warn('Salekit không tồn tại!');
            return;
          }
          this.sessionService.setSessionData(FunctionCode.EXTENSIONS, data);
          this.router.navigateByUrl('/',{skipLocationChange: true}).then(() =>
            this.router.navigate(['/extensions/salekit'], {
              skipLocationChange: true,
            }));
        });
    }
  }
  onScroll(e) {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    const offset = this.countNotify > 0 ? this.countNotify - 1 : 0;
    this.notificationService
      .findAll(offset)
      .pipe(catchError(() => of(undefined)))
      .subscribe(
        (data) => {
          if (data) {
            this.countUnReadNotify = +(data?.countUnreadNotification || 0);
            data?.notifications?.forEach((item) => {
              const notify = {
                id: item.id,
                title: item.content,
                time: item.sendDate,
                avatar: {
                  style: {
                    ['background-color']: item.notificationDescriptionDTO?.color,
                    color: item.notificationDescriptionDTO?.textColor,
                  },
                  label: item.notificationDescriptionDTO?.description,
                },
                isReaded: item.read,
                notificationType: item.notificationType,
              };
              this.listNotify.push(notify);
            });
            this.countNotify = this.listNotify?.length || 0;
            this.isLoading = false;
          }
        },
        () => {
          this.isLoading = false;
        }
      );
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  openDialogConfirm(item, rsId) {
    // phần này là cải tiến lịch chọn update 1 hay tất cả lịch, tạm thời pending
    // const confirm = this.modalService.open(ConfirmCalendarDialogComponent, { windowClass: 'confirm-dialog' });
    // confirm.result
    //   .then((confirmed: number) => {
    //     if (confirmed !== 0) {
    //       if (confirmed === 2) {
    //         this.isUpdateAll = true;
    //       }
    //       this.getDetailEventLoop(item);
    //     }
    //   })
    //   .catch(() => {});

    this.isUpdateAll = true;
    this.getDetailEventLoop(item, rsId);
  }

  getDetailEventLoop(item, rsIdCalendar) {
    const param = {rsId: rsIdCalendar, scope:'VIEW', idRoot: item.idCalendar
      , startDate: item.startDate, endDate: item.endDate };
    this.calendarAlertService.isLoadingSubject.next(true);
    this.calendarService.getDetailEventsLoop(param).subscribe(value => {
      if (value) {
        this.calendarAlertService.isLoadingSubject.next(false);
        this.dataFake = value;
        this.dataFake.listUser = [{
          hrsCodeAssigns: value?.userHrsCode,
          name:value?.userCode + ' - ' + value?.fullNameUser,
          status: value?.status,
          reason: value?.reason,
        },...value.listEventAssigns.map(i => {
          return {
            hrsCodeAssigns: i?.userHrsCode,
            name:i?.userCode + ' - ' + i?.fullNameUser,
            status: i?.status,
            reason: i?.reason,
          };
        })];
        this.dataFake.start = new Date( this.dataFake.start);
        this.dataFake.end = new Date( this.dataFake.end);
        const isOriginalRecord = (this.dataFake.idEventAssign === '' || this.dataFake.idEventAssign === undefined);
        const isPersonalRecord = false;
        const modal = this.modalService.open(CalendarEditModalComponent, { windowClass: 'create-calendar-modal' });
        modal.componentInstance.isDetailEvent = true;
        modal.componentInstance.data = this.dataFake;
        modal.componentInstance.isUpdateAll = this.isUpdateAll;
        modal.componentInstance.isOriginalRecord = isOriginalRecord;
        modal.componentInstance.isPersonalRecord = isPersonalRecord;
        modal.componentInstance.listEventType = this.listEventType;
        modal.result
          .then((result) => {
            this.isLoading = true;
            if (!result.isDelete) {
              if (result) {
                result.value.status = this.updateStatus(result.value.status);
                this.data = result.value;
                if (this.isUpdateAll) {
                  this.calendarService.updateSme(this.data).subscribe(
                    (i) => {
                      this.messageService.success('Thực hiện thành công');
                    },
                    () => {
                      this.messageService.error('error');
                    }
                  );
                } else {
                  this.calendarService.updateSmeOneRecord(this.data).subscribe(
                    (i) => {
                      this.messageService.success('Thực hiện thành công');
                    },
                    () => {
                      this.messageService.error('error');
                    }
                  );
                }
              }
            } else {
            }
          })
      }
    },() => {
      this.calendarAlertService.isLoadingSubject.next(false);
      if (item.notificationType === 'NOTIFICATION_CALENDAR_ASSIGN_REPLY' || item.notificationType === 'NOTIFICATION_CALENDAR_ASSIGN_REPLY_LOOP') {
        this.messageService.error('Bạn không có quyền xem lịch này!');
      } else {
        this.messageService.error('Bản ghi không tồn tại');
      }
    });
  }

  updateStatus(status: string) {
    if (status !== 'DONE' && status !== 'WAIT' && status !== 'REJECT') {
      status = 'NEW';
    }
    return status;
  }

  getDetailEvent(item, rsIdCalendar) {
    const param = {rsId: rsIdCalendar, scope:'VIEW' };
    this.calendarAlertService.isLoadingSubject.next(true);
    this.calendarService.getDetailEvents(item.idCalendar, param).subscribe(value => {
      if (value) {
        this.calendarAlertService.isLoadingSubject.next(false);
        this.dataFake = value;
        this.dataFake.listUser = [{
          hrsCodeAssigns: value?.userHrsCode,
          name:value?.userCode + ' - ' + value?.fullNameUser,
          status: value?.status,
          reason: value?.reason,
        },...value.listEventAssigns.map(i => {
          return {
            hrsCodeAssigns: i?.userHrsCode,
            name:i?.userCode + ' - ' + i?.fullNameUser,
            status: i?.status,
            reason: i?.reason,
          };
        })];
        this.dataFake.start = new Date( this.dataFake.start);
        this.dataFake.end = new Date( this.dataFake.end);
        const isOriginalRecord = this.dataFake.idEventAssign === undefined ? true : false;
        const isPersonalRecord = (isOriginalRecord === true && this.dataFake.listUser.length === 1);
        const modal = this.modalService.open(CalendarEditModalComponent, { windowClass: 'create-calendar-modal' });
        modal.componentInstance.isDetailEvent = true;
        // modal.componentInstance.data = this.job;
        modal.componentInstance.data = this.dataFake;
        modal.componentInstance.isOriginalRecord = isOriginalRecord;
        modal.componentInstance.isUpdateAll = true;
        modal.componentInstance.isPersonalRecord = isPersonalRecord;
        modal.componentInstance.listEventType = this.listEventType;
        modal.result
          .then((result) => {
            this.isLoading = true;
            if (!result.isDelete) {
              if (result) {
                result.value.status = this.updateStatus(result.value.status);
                this.data = result.value;
                this.calendarService.updateSme(this.data).subscribe(
                  (i) => {
                    this.messageService.success('Thực hiện thành công');

                    // update alert calendar
                    // this.calendarAlertInitService.getCalendarOfTheDay();
                  },
                  () => {
                    this.messageService.error('Có lỗi xảy ra');
                  }
                );
              }
            } else {
              // update alert calendar
              // this.calendarAlertInitService.getCalendarOfTheDay();
            }
          })
      }
    },() => {
      this.calendarAlertService.isLoadingSubject.next(false);
      if (item.notificationType === 'NOTIFICATION_CALENDAR_ASSIGN_REPLY' || item.notificationType === 'NOTIFICATION_CALENDAR_ASSIGN_REPLY_LOOP') {
        this.messageService.error('Bạn không có quyền xem lịch này!');
      } else {
        this.messageService.error('Bản ghi không tồn tại');
      }
    });
    // this.dataFake = this.fakeDataDetail;
  }

  // popup lịch được mời
  alertCalendarInvite(notify) {
    const now = new Date();
    const d = now.valueOf() - new Date(notify.time).setSeconds(0).valueOf() ;
    if (d > 0) {
      const diff = Math.abs(d);
      const minutes = Math.floor((diff/1000)/60);
      if (minutes <= 1) {
        // tslint:disable-next-line:no-shadowed-variable
        if (notify.notificationType === 'NOTIFICATION_CALENDAR_ASSIGN_LOOP') {
          const notifyAlert: CalendarAlertModel =  {
            id : 'id',
            idCalendar: '',
            idCalendarRoot: notify.idCalendar,
            title: notify.title,
            time: '',
            timeRemaining: '',
            startDisplay: '',
            startTime: notify.startDate,
            endTime: notify.endDate,
            isNotAction: 2,
            titleNoti:null
          }
          this.calendarAlertService.show(notifyAlert);
        } else {
          const notifyAlert: CalendarAlertModel =  {
            id : 'id',
            idCalendar: notify.idCalendar,
            idCalendarRoot: '',
            title: notify.title,
            time: '',
            timeRemaining: '',
            startDisplay: '',
            startTime: notify.startDate,
            endTime: notify.endDate,
            isNotAction: 2,
            titleNoti:null
          }
          this.calendarAlertService.show(notifyAlert);
        }
      }
    }
  }
  saveLogNotification() {
    const currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
    const isSavedLog = localStorage.getItem('NOTIFICATION');
    if (isSavedLog !== 'NOTIFICATION') {
      const listData = [];
      listData.push({
        userName: currUser?.userName,
        userId: currUser?.userId,
        functionCode: 'NOTIFICATION',
        functionName: 'Chuông thông báo',
        appCode: 'CRM_WEB',
        startTime: new Date().valueOf(),
      });
      this.categoryService.saveTracing(listData)
        .subscribe((res: any) => {
          if (res) {
            localStorage.setItem('NOTIFICATION', 'NOTIFICATION');
          }
        });
    }
  }
  alertNotifyLimit(notify) {
    const notifyAlert: CalendarAlertModel =  {
      id: "id",
      idCalendar: notify.id,
      idCalendarRoot: notify.notificationType.concat('----',notify.campaignId),
      title: notify.content,
      time: notify.sendDate,
      timeRemaining: null,
      startDisplay: null,
      startTime: null,
      endTime: null,
      isNotAction: 2,
      titleNoti:notify.title
    }
    this.calendarAlertService.show(notifyAlert);
  }
}
