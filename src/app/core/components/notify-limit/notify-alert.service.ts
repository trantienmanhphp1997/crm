import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import { filter } from 'rxjs/operators';
export interface NotifyLimitModel {
    id: string;
    type: string;
    title: string;
    content: string;
    time: string;
    message: string;
    autoClose: boolean;
}
@Injectable({
    providedIn: 'root',
})
export class NotifyAlertService {
  private subject = new Subject<NotifyLimitModel>();
  private defaultId = 'default-noti-limit';
  isLoadingSubject = new BehaviorSubject(false);

  onShow(id = this.defaultId): Observable<NotifyLimitModel> {
    return this.subject.asObservable().pipe(filter((x) => x && x.id === id));
  }

  show(notify: NotifyLimitModel) {
    notify.id = this.defaultId;
    this.subject.next(notify);
  }
}