import { Component, Injector, Input, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { MessageService } from 'primeng/api';
import { BaseComponent } from '../base.component';
import { Subscription } from 'rxjs';
import { NotifyLimitModel, NotifyAlertService } from './notify-alert.service';
@Component({
    selector: 'app-notify-alert',
    templateUrl: './notify-alert.component.html',
    styleUrls: ['./notify-alert.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [MessageService],
})
export class NotifyAlertComponent extends BaseComponent implements OnInit, OnDestroy {
    @Input() id = 'default-noti-limit';
    @Input() fade = true;
    alertSubscription: Subscription;
    routeSubscription: Subscription;
    title = "Cảnh báo hết hạn"
    constructor(
        injector: Injector,
        private message: MessageService,
        private service: NotifyAlertService,
    ) {
        super(injector);
        console.log("Contructor here")
        this.alertSubscription = this.service.onShow(this.id).subscribe((notify) => {
            this.show(notify);
        });
    }
    ngOnInit() {
        console.log("HERE")
    }
    ngOnDestroy() {
        this.alertSubscription.unsubscribe();
        this.routeSubscription?.unsubscribe();
    }
    show(notify: NotifyLimitModel) {
        console.log("HERE")
        this.message.add({
            sticky: true,
            severity: 'success',
            summary: this.getLimitLength(notify.title, 140),
            detail: notify.time,
            // contentStyleClass: notify.startTime,
            // styleClass: notify.endTime,
            // id: notify.id,
            // icon: notify.idCalendarRoot,
            // data: notify.timeRemaining,
            // life: notify.isNotAction,
        });
    }
    getLimitLength(str?: string, length = 20, noWhiteSpace = 10): string {
        // nếu string không có khoảng trắng thì sẽ lấy length
        if (!!str && str.indexOf(' ') === -1) {
            return str.length <= length
                ? str
                : str.slice(0, noWhiteSpace) + '...';
        }
        return !!str
            ? str.length <= length
                ? str
                : str.slice(0, length) + '...'
         
                : '';
    }
    goDetaiNotify(abc,def){
        console.log('Detail');
    }
}