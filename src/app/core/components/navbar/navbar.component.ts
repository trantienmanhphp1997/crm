import { KeycloakService } from 'keycloak-angular';
import { Component, Input, OnInit } from '@angular/core';
import { ActivationEnd, NavigationEnd, Router } from '@angular/router';
import { HttpCancelService } from '../../services/http-cancel.service';
import { SessionService } from '../../services/session.service';
import { functionUri, SessionKey } from '../../utils/common-constants';
import * as _ from 'lodash';
import { clearLocalStorage, logoutKeycloak } from '../../utils/function';
declare const $: any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  listMenu: any[];
  @Input() isSubMenu: boolean;
  @Input() subMenu: any[];
  currUser: any;
  name: string;
  isPermission: boolean;
  isRun = false;

  links = {
    SUPPORT_RM_HOTLINE: 'https://hotronghiepvu.web.com.vn',
    REPORT_REGION_SMART_CHANEL: 'https://smartchannel.web.com.vn/auth/login'
  }

  constructor(
    private router: Router,
    private httpCancelService: HttpCancelService,
    private keyCloakService: KeycloakService,
    private sessionService: SessionService
  ) {
    this.router.events.subscribe((event) => {
      if (event instanceof ActivationEnd) {
        this.httpCancelService.cancelPendingRequests();
      }
      if (!this.subMenu && this.router?.url && !this.isRun) {
        const currentUrl = this.sessionService.getSessionData(SessionKey.CURRENT_LINK_ACTIVE);
        this.isRun = true;
        const timer = setTimeout(() => {
          this.activeUrl(currentUrl);
          clearTimeout(timer);
        }, 100);
      }
      if (event instanceof NavigationEnd) {
        this.activeMenuLevel1()
      }
    });
    this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
    this.getName();
  }

  ngOnInit() {
    if (!this.subMenu) {
      this.listMenu = this.sessionService.getSessionData(SessionKey.MENU);

      this.isPermission = this.sessionService.getSessionData(SessionKey.IS_PERMISSION);
      if (this.router?.url) {
        this.isRun = true;
        const timer = setTimeout(() => {
          this.activeUrl();
          clearTimeout(timer);
        }, 300);
      }
    }
  }

  activeUrl(currentUrl?: string) {
    const arrayEl = document.getElementById('menu')?.getElementsByTagName('a');
    for (let index = 0; index < arrayEl?.length; index++) {
      const el = arrayEl[index];
      const url = el?.href?.replace(document.location.origin, '');
      const currUrl = this.sessionService.getSessionData(SessionKey.CURRENT_LINK_ACTIVE);
      if (!_.isEmpty(url) && this.router?.url?.includes(url)) {
        this.activeParentLink(el.parentElement, currentUrl);
        this.sessionService.setSessionData(SessionKey.CURRENT_LINK_ACTIVE, url);
        break;
      }
    }
  }

  generateIcon(item) {
    if (item.iconUri) {
      return item.iconUri;
    } else {
      return 'las la-cogs';
    }
  }

  onMouseOver(el: HTMLElement, isLv1?: boolean) {
    const subMenu: HTMLElement = el?.querySelector('ul.sub-nav');
    if (subMenu) {
      const offsetTop = isLv1 ? el.getBoundingClientRect().top - 0 : el.offsetTop + 20;
      subMenu.style.transform = `translate3d(${el.offsetWidth}px, ${offsetTop}px, 0px)`;
    }
  }

  viewProfile() {
    const hrsCode = this.sessionService.getSessionData(SessionKey.USER_INFO)?.hrsCode;
    if (hrsCode && this.isPermission) {
      this.router
        .navigateByUrl(`${functionUri.rm_360_manager}/detail/${hrsCode}`, { skipLocationChange: true })
        .then(() => {});
    }
  }

  logout() {
    clearLocalStorage(this.currUser, true);
    this.keyCloakService.clearToken();
    logoutKeycloak();
  }

  getName() {
    const arr: string[] = _.split(this.currUser?.fullName, ' ');
    const lastName = _.last(arr) || '';
    const lastIndex = _.lastIndexOf(arr, lastName);
    if (arr.length > 2) {
      this.name = `${lastName} `;
      arr?.forEach((strName, i) => {
        if (i < lastIndex) {
          this.name += strName?.substr(0, 1)?.toUpperCase();
        }
      });
    } else {
      this.name = `${lastName}`;
    }
  }

  routerActiveClass(itemActive: HTMLElement, url?: string) {
    const currLink = this.sessionService.getSessionData(SessionKey.CURRENT_LINK_ACTIVE);
    if (url !== currLink) {
      this.activeParentLink(itemActive.parentElement, currLink);
      this.sessionService.setSessionData(SessionKey.CURRENT_LINK_ACTIVE, url);
    }
  }

  activeParentLink(el: HTMLElement, urlInActive?: string) {
    if (urlInActive) {
      const arrayEl = document.getElementById('menu').getElementsByTagName('a');
      for (let index = 0; index < arrayEl.length; index++) {
        const elInActive = arrayEl[index];
        if (elInActive.href && elInActive.href.replace(document.location.origin, '') === urlInActive) {
          this.inActiveParentLink(elInActive);
          break;
        }
      }
    }
    if (
      el?.tagName === 'APP-NAVBAR' &&
      el?.previousElementSibling?.className.includes('nav-link') &&
      !el?.previousElementSibling?.className.includes('active')
    ) {
      el.previousElementSibling.className += ' active';
    }
    if (!(el?.tagName === 'UL' && el?.id === 'menu') && el?.parentElement) {
      this.activeParentLink(el.parentElement);
    }
    this.isRun = false;
  }

  inActiveParentLink(el: HTMLElement) {
    if (el?.tagName === 'APP-NAVBAR' && el?.previousElementSibling?.className.includes('nav-link')) {
      el.previousElementSibling.className = el.previousElementSibling.className?.replace('active', '')?.trim();
    }
    if (!(el?.tagName === 'UL' && el?.id === 'menu') && el?.parentElement) {
      this.inActiveParentLink(el.parentElement);
    }
  }

  viewDevice() {
    if (this.isPermission) {
      this.router.navigateByUrl(`${functionUri.device_rm}`).then(() => {});
    }
  }

  openTab(event: Event, code: string): void {
    event.stopPropagation();

    const link = this.links[code];

    window.open(link);
  }

  activeMenuLevel1(){
    setTimeout(()=> {
      $('a.nav-link').each(function(){
        if ($(this).attr('href'))
        {
          $(this).removeClass('active')
        }
      })
      if (this.router.url === '/dashboard')
      {
        $('a.nav-link').each(function(){
          $(this).removeClass('active')
        })
      }
      const currentUrl = this.router.url === '/dashboard' ? `/customer360${this.router.url}` : this.router.url
      $(`a[href="${currentUrl}"].nav-link`).addClass('active')
    }, 300)
  }

}
