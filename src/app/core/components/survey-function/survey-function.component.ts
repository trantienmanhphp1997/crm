import { Component, OnInit, ElementRef} from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import {
  CommonCategory,
  SessionKey,
} from '../../utils/common-constants';
import * as _ from 'lodash';
import { catchError, filter } from 'rxjs/operators';
import { CategoryService } from '../../../pages/system/services/category.service';
import { SessionService } from '../../services/session.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonCategoryService } from '../../services/common-category.service';
import { MatDialog } from '@angular/material/dialog';
import { of } from 'rxjs';
import { ModalConfirmComponent } from './modal-confirm/modal-confirm.component';
import { NotifyMessageService } from '../notify-message/notify-message.service';

@Component({
  selector: 'app-survey-function',
  templateUrl: './survey-function.component.html',
  styleUrls: ['./survey-function.component.scss']
})
export class SurveyFunctionComponent implements OnInit {
  active: boolean;
  outsideClickListener: any;
  form: FormGroup;
  listFunctions = [];
  listProblem = [];
  listQuestion = [];
  description = [
    'Chưa hài lòng', 'Cần cải thiện thêm', 'Hài lòng', 'Rất hài lòng', 'Vượt mong đợi'
  ];
  currUser: any;
  isLoading = false;
  currentFunction: any;
  clickSend = false;
  isDrag = false;
  constructor(
    private fb: FormBuilder,
    private el: ElementRef,
    private sessionService: SessionService,
    private router: Router,
    protected commonService: CommonCategoryService,
    private categoryService: CategoryService,
    public dialog: MatDialog,
    protected messageService: NotifyMessageService,

  ) {
    this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
    this.form = fb.group({
      function: [null, Validators.required],
      stars: [null, Validators.required],
      opinion: null,
      another: null,
    });
    this.getlistMenu(this.sessionService.getSessionData(SessionKey.MENU));
    console.log(this.listFunctions);
    this.listFunctions = this.listFunctions.filter(item => item.uri != '/');
    this.listFunctions.unshift({
      name: 'Đánh giá chung cho cả hệ thống',
      id: 'ALL'
    });
    this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((res: any) => {
      setTimeout(() => {
        if (res?.url) {
          const findFunction = this.listFunctions.find(item => res.url == item.uri);
          if (findFunction?.id) {
            this.currentFunction = findFunction?.id;
            this.form.controls.function.setValue(findFunction?.id);
          } else {
            // dashboard
            this.form.controls.function.setValue("2920a460-9c38-4f18-bf34-f8f6c93cb616");
            this.currentFunction = "2920a460-9c38-4f18-bf34-f8f6c93cb616";
          }
        }
      }, 400);
    });
    this.commonService.getCommonCategory(CommonCategory.SURVEY_QUESTION).pipe(catchError(() => of(undefined))).subscribe(res => {
      this.listQuestion = res?.content;
    });
    this.form.controls.stars.valueChanges.subscribe(val => {
      if(val < 3) {
        this.form.controls.opinion.setValidators([Validators.required]);
        if(this.listProblem.includes('Khác')){
          this.form.controls.another.setValidators([Validators.required]);
        }
      } else {
        this.form.controls.opinion.clearValidators();
        this.form.controls.another.clearValidators();
      }
      this.form.controls.opinion.updateValueAndValidity();
      this.form.controls.another.updateValueAndValidity();
    })
  }

  startDrag(ev){
    this.isDrag = true;
  }

  endDrag(ev){
    setTimeout(() => {
      this.isDrag = false;
    }, 200);
  }


  getlistMenu(menu = []) {
    if (menu.length) {
      menu.forEach(element => {
        if (!element.subMenu.length) {
          this.listFunctions.push(element);
        } else {
          this.getlistMenu(element.subMenu);
        }
      });
    }
  }

  ngOnInit() {
  }

  openConfigurator(event) {
    if(this.isDrag) {
      return;
    }
    this.active = !this.active;
    if (this.active) this.bindOutsideClickListener();
    else this.unbindOutsideClickListener();
  }

  hideConfigurator(event) {
    this.active = false;
    this.reset();
    this.unbindOutsideClickListener();
  }

  bindOutsideClickListener() {
    if (!this.outsideClickListener) {
      this.outsideClickListener = (event) => {
        if (this.active && this.isOutsideClicked(event)) {
          this.active = false;
          this.reset();
        }
      };
      document.addEventListener('click', this.outsideClickListener);
    }
  }

  unbindOutsideClickListener() {
    if (this.outsideClickListener) {
      document.removeEventListener('click', this.outsideClickListener);
      this.outsideClickListener = null;
    }
  }

  isOutsideClicked(event) {
    return !(this.el.nativeElement.isSameNode(event.target) || this.el.nativeElement.contains(event.target));
  }

  send() {
    this.clickSend = true;
    const valueForm = this.form.value;
    this.form.markAllAsTouched();
    const body = {
      rsId: valueForm?.function,
      rating: valueForm.stars,
      reason: [...this.listProblem, valueForm?.another?.trim?.()].filter(item => item != 'Khác' && item).join(', '),
      channel: 'WEB',
      suggestion: valueForm?.opinion?.trim?.(),
    };
    if(this.form.invalid) {
      return;
    }
    if(valueForm.stars < 3 && !this.listProblem.length) {
      return;
    }
    this.isLoading = true;
    this.categoryService.ratingFeature(body).subscribe(res => {
      this.isLoading = false;
      this.hideConfigurator(null);
      const dialog = this.dialog.open(ModalConfirmComponent, {
        width: '500px',
      });
      dialog.afterClosed().subscribe(res => {
        if(res) {
          this.openConfigurator(null);
        }
      });
    }, err => {
      this.isLoading = false;
      this.messageService.error('Có lỗi xảy ra, Vui lòng thử lại sau!');
    });
  }

  reset() {
    this.form.reset();
    this.form.controls.function.setValue(this.currentFunction);
    this.listProblem = [];
    this.clickSend = false;
  }

  getDescription(star) {
    return this.description[+star - 1] + '!';
  }

  check() {
    return this.listProblem.find(item => item == 'Khác');
  }

  onChangeProblem(ev, name){
    console.log(ev, name);
    if(name == 'Khác' && ev.checked) {
      this.form.controls.another.setValidators(Validators.required);
    } 
    if(name == 'Khác' && !ev.checked) {
      this.form.controls.another.clearValidators();
    }
    this.form.controls.another.updateValueAndValidity();
  }
}
