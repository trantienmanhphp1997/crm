import { Component, Inject, Injector, OnInit } from '@angular/core';
import { SessionKey } from '../../utils/common-constants';
import { BaseComponent } from '../base.component';
import { orderBy, map } from 'lodash';
import { UploadImgService } from 'src/app/pages/system/services/upload-img.service';
import { forkJoin } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';


@Component({
  selector: 'app-poster',
  templateUrl: './poster.component.html',
  styleUrls: ['./poster.component.scss']
})
export class PosterComponent extends BaseComponent implements OnInit {

  selectedImg = null;

  notShowAgain = new FormControl(false);

  sessionKey = 'session_poster';
  liveKey = 'poster';
  key = {
    session: 'session_poster',
    live: 'poster'
  }

  constructor(
    injector: Injector,
    public dialogRef: MatDialogRef<PosterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private uploadImageService: UploadImgService,
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.pickImg();
  }

  close(): void {
    this.checkOffPostup();
    this.dialogRef.close();
  }

  pickImg(): void {
    this.selectedImg = this.data.image;
  }
  hidePoster(type: string): void {
    //xu li call api
 //  localStorage.setItem(this.getKey(type), 'hide');
    this.checkOffPostup();
    if(type === 'live'){
      const param = {
        id: this.data.poster?.id
      }
      this.uploadImageService.skipViewPoster(param).subscribe((res) => {
      });
      this.close();
    }
    else{
      this.close();
    }
  }

  getKey(type): string {
    const {uuidImage, username} = this.data;
    return [uuidImage, this.key[type], username].join('_');
  }
  checkOffPostup(){
    const keyCheckOff = this.currUser?.username + '_' + this.convertDateToString() + '_CHECKOFF';
    localStorage.setItem(keyCheckOff,'1');
  }
  convertDateToString() {
    const date = new Date();
    return (date.getFullYear() + '' + (date.getMonth() > 8 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1)) + '' + (date.getDate() > 9 ? date.getDate() : '0' + date.getDate()));
  }
}
