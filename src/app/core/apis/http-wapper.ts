import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

export class HttpWrapper {
  url: string;
  constructor(public http: HttpClient, public baseURL: string) {
    this.url = baseURL;
  }

  fetch(params): Observable<any> {
    return this.http.get(`${this.baseURL}`, { params });
  }

  get(href = '', params: any = {}) {
    return this.http.get(this.baseURL + `/${href}`, {
      params,
    });
  }

  post(href = '', data: any, params: any = {}): Observable<any> {
    return this.http.post(this.baseURL + `/${href}`, data, {
      params,
    });
  }

  create(data): Observable<any> {
    return this.http.post(`${this.baseURL}`, data);
  }
  createNewRm(data): Observable<any> {
    return this.http.post(`${this.baseURL}/createRMProfile`, data);
  }

  update(data): Observable<any> {
    return this.http.put(`${this.baseURL}`, data);
  }

  delete(id): Observable<any> {
    return this.http.delete(`${this.baseURL}/${id}`);
  }

  getByCode(code): Observable<any> {
    return this.http.get(`${this.baseURL}/${code}`);
  }

  synchronizedBranches(): Observable<any> {
    return this.http.post(`${this.baseURL}/synchronize`, {});
  }

  createFile(href = '', obj: any, type: any): Observable<any> {
    return this.http.post(`${this.baseURL}/${href}`, obj, type);
  }

  postFile(href = '', data: any): Observable<any> {
    return this.http.post(this.baseURL + `/${href}`, data, {
      responseType: 'text'
    });
  }

  file(href = '', data: any, type = 'pdf') {
    return this.http
      .post(`${this.baseURL}/${href}`, data, {
        responseType: 'arraybuffer',
        observe: 'response',
      })
      .toPromise()
      .then((response: HttpResponse<ArrayBuffer>) => {
        return new Blob([response.body], { type: type || 'application/octet-stream' });
      });
  }

  getByCodeAndCampaignId(code, params): Observable<any> {
    return this.http.get(`${this.baseURL}/${code}`, { params });
  }

  deleteByCodeAndCampaignId(code, params): Observable<any> {
    return this.http.delete(`${this.baseURL}/${code}`, { params });
  }
}
