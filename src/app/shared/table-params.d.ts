declare interface TableParams {
    size: number,
    page: number,
    name: string,
    code: string,
    sort: string,
  }