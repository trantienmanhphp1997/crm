import { HostBinding, Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ColumnMode } from '@swimlane/ngx-datatable';
import { TranslateService } from '@ngx-translate/core';
import { HttpWrapper } from '../core/apis/http-wapper';
import { NotifyMessageService } from '../core/components/notify-message/notify-message.service';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { global } from '@angular/compiler/src/util';

import _ from 'lodash';

export class BaseTableComponent<TWrapper extends HttpWrapper> {
  models: Array<any>; // models
  count: number; // total
  offset: number; // page
  // limit: number = global.userConfig.pageSize || 10// quantity
  limit: number = global.userConfig.pageSize || 10; // quantity
  mode: string = ColumnMode.force;
  public currentVisible: number = 5;
  isLoading: boolean = true;

  params: TableParams = {
    size: global.userConfig.pageSize,
    page: 0,
    name: '',
    code: '',
    sort: 'code,asc',
  };

  protected router: Router;
  protected route: ActivatedRoute;
  protected modal: NgbModal;
  protected translate: TranslateService;
  protected messageService: NotifyMessageService;
  protected message: any;
  @HostBinding('class.app__right-content') appRightContent = true;

  constructor(private injector: Injector, protected api: TWrapper) {
    this.isLoading = true;
    this.init();
    this.route.data.subscribe((data) => {
      this.prepare(data);
      this.isLoading = false;
    });
    this.route.paramMap.subscribe((map) => {
      this.params.page = +map.get('p');
    });
    this.isLoading = false;
  }

  // showSideBar() {}

  init() {
    //
    this.router = this.injector.get(Router);
    this.route = this.injector.get(ActivatedRoute);
    this.modal = this.injector.get(NgbModal);
    this.translate = this.injector.get(TranslateService);
    this.messageService = this.injector.get(NotifyMessageService);

    this.params.size = 10;
    // TODO: rename text
    this.translate.get('notificationMessage').subscribe((message) => {
      this.message = message;
    });
  }

  paging($event) {
    const { offset } = $event;
    this.params.page = offset;
    this.reload();
  }

  sorting($event) {
    const property = _.get($event, 'column.prop');
    const direction = _.get($event, 'newValue');
    this.params.sort = `${property},${direction}`;
    this.reload();
  }

  prepare(data: any) {
    this.models = _.get(data, 'fetch.content');
    this.count = _.get(data, 'fetch.totalElements');
    this.offset = _.get(data, 'fetch.number');
  }

  parameters() {
    return {
      q: this.params.size === 10 ? undefined : this.params.size, // quantity
      p: this.params.page === 0 ? undefined : this.params.page, // page (from 0)
      n:
        this.params.name === null || this.params.name === undefined || this.params.name === ''
          ? undefined
          : this.params.name, // name
      c:
        this.params.code === null || this.params.code === undefined || this.params.code === ''
          ? undefined
          : this.params.code, // code
      s:
        this.params.sort === null || this.params.sort === undefined || this.params.sort === 'code,asc'
          ? undefined
          : this.params.sort, // sorting
    };
  }

  reload() {
    const params = this.parameters();
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: params,
      queryParamsHandling: 'merge',
    });
  }

  delete(value: any) {
    const id = this.parse_code(value);
    const confirm = this.modal.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.result.then((response) => {
      if (response) {
        this.api.delete(id).subscribe(
          () => {
            this.messageService.success(_.get(this.message, 'success'));
            this.reload();
            this.isLoading = false;
          },
          (res) => {
            this.messageService.error(_.get(this.message, `${_.get(res, 'error.code')}`));
            this.isLoading = false;
          }
        );
      }
    });
  }

  parse_code(value: any) {
    return _.get(value, 'id');
  }

  get_value(model: any, key: string) {
    return _.get(model, key);
  }
}
