import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatNumber'
})
export class FormatNumberPipe implements PipeTransform {

  transform(value: any, unit?: any, toFixed?: any): any {
    if (value == 0) {
      return '0';
    }
    if (!value || value === 'NaN' || value === '--') {
      return '--';
    }
    // có bao nhiêu số thập phân
    if (toFixed && +value) {
      value = +value;
      value = value.toFixed(toFixed);
    }
    let formated = value?.toString();
    let first = '';
    let last = '';
    if (formated.includes('.')) {
      first = formated.substr(0, formated.indexOf('.'));
      last = formated.substr(formated.indexOf('.'));
    } else {
      first = formated;
    }
    if (Number(first)) {
      first = Number(first).toLocaleString('en-US', {
        style: 'currency',
        currency: 'VND',
      });
      first = first.trim().replace('₫', '');
      formated = first + last;
    }
    // thêm đơn vị
    if (unit) {
      formated = formated + `${unit}`;
    }
    return formated;

  }

}
