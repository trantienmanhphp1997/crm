import { KeycloakService } from 'keycloak-angular';
import { Component, OnInit, HostBinding } from '@angular/core';
import { logoutKeycloak } from '../../../core/utils/function';

@Component({
  selector: 'app-access-denied',
  templateUrl: './access-denied.component.html',
  styleUrls: ['./access-denied.component.scss'],
})
export class AccessDeniedComponent implements OnInit {
  @HostBinding('class.app__right-content') appRightContent = true;
  constructor(private keycloakService: KeycloakService) {}

  ngOnInit(): void {}

  logout() {
    // this.keycloakService.logout();
    logoutKeycloak();
  }
}
