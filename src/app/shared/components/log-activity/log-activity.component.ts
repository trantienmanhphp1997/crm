import { trigger, state, style, transition, animate, AUTO_STYLE } from '@angular/animations';
import { Component, OnInit, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommunicateService } from 'src/app/core/services/communicate.service';

@Component({
  selector: 'app-log-activity',
  templateUrl: './log-activity.component.html',
  styleUrls: ['./log-activity.component.scss'],
  animations: [
    trigger('collapse', [
      state(
        'false',
        style({ height: AUTO_STYLE, visibility: AUTO_STYLE, paddingTop: '0.5rem', borderTop: '1px solid' })
      ),
      state('true', style({ height: '0', visibility: 'hidden' })),
      transition('false => true', animate(150 + 'ms ease-in')),
      transition('true => false', animate(150 + 'ms ease-out')),
    ]),
  ],
})
export class LogActivityComponent implements OnInit, OnDestroy, OnChanges {
  data: any;
  activityCalls = [];
  subscription: Subscription;
  constructor(private communicateService: CommunicateService) {
    this.subscription = this.communicateService.request$.subscribe((req) => {
      // if (req?.name === 'customer-left-view') {
      //   this.paramActivity.parentCustomerId = Utils.trimNullToEmpty(req.data.parentCustomerId);
      //   this.paramActivity.parentType = Utils.trimNullToEmpty(req.data.parentType);
      //   this.paramActivity.activityType = Utils.trimNullToEmpty(req.data.activityType);
      //   this.paramActivity.parentId = Utils.trimNullToEmpty(req.data.parentId);
      //   if (this.timeOutId) {
      //     clearTimeout(this.timeOutId);
      //   }
      // }
    });
  }

  ngOnInit(): void {}

  ngOnChanges(change: SimpleChanges) {
    if (change?.listActivity?.currentValue) {
      this.activityCalls = change?.listActivity?.currentValue;
    }
  }

  collapsedActivity(i: number) {
    this.activityCalls[i].isCollapsed = this.activityCalls[i].isCollapsed ? false : true;
  }

  identify(index, item) {
    return item ? item.id : undefined;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
