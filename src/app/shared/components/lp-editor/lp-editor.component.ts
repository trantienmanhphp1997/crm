import { Component, Input, OnChanges, OnDestroy, OnInit, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
// import { Editor, toDoc } from 'ngx-editor';
// import { toHTML } from 'ngx-editor';
import * as _ from 'lodash';

@Component({
  selector: 'lp-editor',
  templateUrl: './lp-editor.component.html',
  styleUrls: ['./lp-editor.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'NgxEditor__Wrapper',
  },
})
export class LpEditorComponent implements OnInit, OnDestroy, OnChanges {
  constructor() {}
  editor: any;
  entity: any;
  @Input() model: string;
  @Input() disabled: boolean;
  @Input() type: string;
  @Output() modelChange = new EventEmitter();

  ngOnInit(): void {
    // this.editor = new Editor();
    // this.entity = toDoc(this.model);
  }

  ngOnChanges() {
    // this.entity = toDoc(_.trim(this.model));
  }

  ngOnDestroy(): void {
    // this.editor.destroy();
  }

  onFocus() {
    // this.entity = toDoc(toHTML(this.entity));
    // const html = toHTML(this.entity);
    // this.modelChange.emit(html !== '<p></p>' && html !== '<p> </p>' ? html : '');
  }
}
