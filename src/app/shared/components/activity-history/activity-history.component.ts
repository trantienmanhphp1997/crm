import { global } from '@angular/compiler/src/util';
import { Component, OnInit, Injector, Input, ViewChild, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import * as moment from 'moment';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { ActivityService } from 'src/app/core/services/activity.service';
import {
  CommonCategory,
  ConfigBackDate,
  FunctionCode,
  maxInt32,
  Scopes,
  ScreenName,
  ScreenType,
} from 'src/app/core/utils/common-constants';
import { CampaignsService } from 'src/app/pages/campaigns/services/campaigns.service';
import { ActivityActionComponent } from 'src/app/shared/components/activity-action/activity-action.component';

@Component({
  selector: 'app-activity-history',
  templateUrl: './activity-history.component.html',
  styleUrls: ['./activity-history.component.scss']
})
export class ActivityHistoryComponent extends BaseComponent implements OnInit {
  listData = [];
  listActivityType = [];
  listActivityResult = [];
  listCampaign = [];
  pageable: Pageable;
  @Input() parent: any;
  @Input() customerCode: string;
  @Input() listBranchCode: string[];
  params = {
    page: 0,
    size: global?.userConfig?.pageSize,
    parentId: '',
    rsId: '',
    scope: Scopes.VIEW,
    activityType: '',
    dateStart: '',
    campaignId: '',
  };
  prevParams: any;
  backHours: number;
  isFisrt = true;
  @ViewChild('tableActivity') tableActivity: DatatableComponent;

  constructor(injector: Injector, private activityService: ActivityService, private campaignService: CampaignsService, private modalActive: NgbActiveModal,) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.subscriptions.push(
      this.communicateService.request$.subscribe((req) => {
        if (req?.name === 'refreshActivityList') {
          this.search(false);
        }
      })
    );
  }

  ngOnInit(): void {
    const paramsCampaign = {
      page: 0,
      size: maxInt32,
    };
    this.prevParams = this.params;
    this.params.parentId = this.parent.parentId;
    this.params.rsId = this.objFunction?.rsId;
    this.commonService
      .getCommonCategory(CommonCategory.CONFIG_ACTIVITY_RESULT)
      .pipe(catchError((e) => of(undefined)))
      .subscribe((listData) => {
        this.listActivityResult = listData?.content || [];
      });
    this.commonService
      .getCommonCategory(CommonCategory.CONFIG_TYPE_ACTIVITY)
      .pipe(catchError((e) => of(undefined)))
      .subscribe((listData) => {
        this.listActivityType = listData?.content || [];
        this.listActivityType.unshift({ code: '', name: this.fields.all });
      });
    this.commonService
      .getCommonCategory(CommonCategory.CONFIG_BACK_DATE, ConfigBackDate.BACK_DATE_ACTIVITY)
      .pipe(catchError((e) => of(undefined)))
      .subscribe((backDate) => {
        this.backHours = +(backDate?.content[0]?.value * 24) || 0;
      });
    this.campaignService.search(paramsCampaign).subscribe((listCampaign) => {
      this.listCampaign = listCampaign?.content || [];
      this.listCampaign.forEach((item) => {
        item.displayName = item.code + ' - ' + item.name;
      });
      this.listCampaign.unshift({ id: '', displayName: this.fields.all });
      this.params.campaignId = this.listCampaign[0]?.id;
    });
  }

  search(isSearch?: boolean) {
    this.isLoading = true;
    let params: any;
    if (isSearch) {
      this.params.page = 0;
      params = this.params;
    } else {
      params = this.prevParams;
      params.page = this.params.page;
    }
    this.activityService.search(params).subscribe((listData) => {
      this.prevParams = params;
      this.listData = listData?.content || [];
      this.pageable = {
        totalElements: listData?.totalElements,
        totalPages: listData?.totalPages,
        currentPage: listData?.number,
        size: global.userConfig.pageSize,
      };
      if (this.isFisrt) {
        const timer = setTimeout(() => {
          if (
            (document.getElementById('customer__activity-list')?.querySelector('.table__view') as HTMLElement) &&
            this.listData.length > 0
          ) {
            const rowHeight =
              (
                document
                  .getElementById('customer__activity-list')
                  ?.querySelector('.datatable-row-center.datatable-row-group') as HTMLElement
              )?.clientHeight || 0;
            const height =
              this.tableActivity?.headerHeight +
              this.tableActivity?.footerHeight +
              this.tableActivity?.rows?.length * rowHeight;
            (
              document.getElementById('customer__activity-list')?.querySelector('.table__view') as HTMLElement
            ).style.height = `${height + 25}px`;
          }
          this.tableActivity?.recalculate();
          clearTimeout(timer);
        }, 100);
      }
      this.isFisrt = false;
      this.isLoading = false;
    });
  }

  setPage(pageInfo) {
    this.params.page = pageInfo.offset;
    this.search(false);
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      this.openActivityModal(event.row, ScreenType.Detail, ScreenName.ActivityHistory);
    }
  }

  edit(item) {
    this.openActivityModal(item, ScreenType.Update, ScreenName.ActivityHistory);
  }

  openActivityModal(item: any, type: string, screen: string) {
    this.activityService
      .getByCode(item.id)
      .pipe(catchError((e) => undefined))
      .subscribe((data) => {
        const activityModal = this.modalService.open(ActivityActionComponent, {
          windowClass: 'create-activity-modal',
          scrollable: true,
        });
        activityModal.componentInstance.type = type;
        activityModal.componentInstance.data = data;
        activityModal.componentInstance.fromScreen = screen;
        activityModal.result.then((result) => {
          if (result === ScreenName.ActivityHistory) {
            this.closeModal();
          }
        }, (reason) => {
          console.log(reason);
        })
      });
  }

  delete(item) {
    this.confirmService.confirm().then((res) => {
      if (res) {
        this.isLoading = true;
        this.activityService.delete(item.id).subscribe(
          () => {
            this.isLoading = false;
            this.search(false);
            this.messageService.success(this.notificationMessage.success);
          },
          () => {
            this.isLoading = false;
            this.messageService.error(this.notificationMessage.error);
          }
        );
      }
    });
  }

  getActivityTypeName(value) {
    return (
      this.listActivityType?.find((type) => {
        return type.code === value;
      })?.name || ''
    );
  }

  getActivityResultName(value) {
    return (
      this.listActivityResult?.find((type) => {
        return type.code === value;
      })?.name || ''
    );
  }

  isDelete(row) {
    return (
      this.objFunction?.delete &&
      row.createdBy === this.currUser?.username &&
      moment(row.dateStart)
        .add(+this.backHours, 'hour')
        .isSameOrAfter(moment().startOf('day'))
    );
  }

  isUpdate(row) {
    return (
      this.objFunction?.update &&
      row.createdBy === this.currUser?.username &&
      moment(row.dateStart)
        .add(+this.backHours, 'hour')
        .isSameOrAfter(moment().startOf('day'))
    );
  }

  getCampaign(campaignId) {
    return this.listCampaign?.find((item) => item.id === campaignId)?.name;
  }

  closeModal() {
    this.modalActive.close(false);
  }
}

