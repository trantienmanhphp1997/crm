import { Component, Input, Output, EventEmitter, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ChooseBranchesModalComponent } from '../../../pages/system/components/choose-branches-modal/choose-branches-modal.component';
import { Utils } from '../../../core/utils/utils';
import { TranslateService } from '@ngx-translate/core';
import _ from 'lodash';

@Component({
  selector: 'lp-branches',
  templateUrl: './lp-branches.component.html',
  styleUrls: ['./lp-branches.component.scss'],
})
export class LpBranchesComponent implements OnInit, OnChanges {
  constructor(private modalService: NgbModal, private translate: TranslateService) { }

  @Input() isForm: boolean;
  @Input() disabled: boolean;
  @Input() model: string[];
  @Input() listBranch: any[];
  @Output() modelChange = new EventEmitter();

  fields: any;
  display_name: string;

  ngOnInit() {
    this.translate.get('fields').subscribe((res) => {
      this.fields = res;
      if (!this.isForm) {
        this.display_name =
          _.size(this.model) === 0 || _.size(this.model) === _.size(this.listBranch)
            ? _.get(this.fields, 'all')
            : _.join(this.model, ', ');
      } else {
        this.display_name =
          _.size(this.model) > 0 && _.size(this.listBranch) > 0 && _.size(this.model) === _.size(this.listBranch)
            ? _.get(this.fields, 'all')
            : _.join(this.model, ', ');
      }
    });
  }

  ngOnChanges(change: SimpleChanges) {
    if (!this.isForm) {
      this.display_name =
        _.size(this.model) === 0 || _.size(this.model) === _.size(this.listBranch)
          ? this.fields?.all
          : this.model?.join(', ');
    } else {
      this.display_name =
        _.size(this.model) > 0 && _.size(this.listBranch) > 0 && _.size(this.model) === _.size(this.listBranch)
          ? this.fields?.all
          : this.model?.join(', ');
    }
  }

  chooseBranch() {
    const modal = this.modalService.open(ChooseBranchesModalComponent, { windowClass: 'tree__branches-modal' });
    modal.componentInstance.listBranchOld = this.model || [];
    modal.componentInstance.listBranch = this.listBranch;
    modal.result
      .then((res) => {
        if (res) {
          const branchCodes = res?.map((i) => i.code) || [];
          this.model = res;

          if (!this.isForm) {
            this.display_name =
              _.size(branchCodes) === 0 || _.size(branchCodes) === _.size(this.listBranch)
                ? this.fields?.all
                : branchCodes?.join(', ');
          } else {
            this.display_name =
              _.size(branchCodes) > 0 && _.size(this.listBranch) > 0 && _.size(branchCodes) === _.size(this.listBranch)
                ? this.fields?.all
                : branchCodes?.join(', ');
          }

          this.modelChange.emit(branchCodes);
        }
      })
      .catch(() => { });
  }
}
