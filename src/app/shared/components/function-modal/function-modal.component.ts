import { maxInt32 } from 'src/app/core/utils/common-constants';
import { forkJoin } from 'rxjs';
import { Component, HostBinding, OnInit, ViewChildren, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ColumnMode, DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-function-modal',
  templateUrl: './function-modal.component.html',
  styleUrls: ['./function-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class FunctionModalComponent implements OnInit {
  @HostBinding('class') elementClass = 'app-function-modal';
  isLoading = false;
  functionId: string;
  selected = [];
  ColumnMode = ColumnMode;
  SelectionType = SelectionType;
  listTemp = [];
  listApp = [];
  textSearch = '';
  listFunction = [];
  listData: any;
  @ViewChildren('table') table: DatatableComponent;

  constructor(
    public activeModal: NgbActiveModal,
    private categoryService: CategoryService,
    private translate: TranslateService
  ) {}

  ngOnInit(): void {
    this.isLoading = true;
    if (!this.listData) {
      forkJoin([
        this.categoryService.searchResourceCategory({ page: 0, size: maxInt32 }),
        this.categoryService.searchAppCategory({ page: 0, size: maxInt32 }),
      ]).subscribe(
        ([listData, listApp]) => {
          this.isLoading = false;
          this.listApp = listApp.content;
          this.getData(listData.content);
        },
        () => {
          this.isLoading = false;
        }
      );
    } else {
      this.categoryService.searchAppCategory({ page: 0, size: maxInt32 }).subscribe(
        (listApp) => {
          this.listApp = listApp.content;
          this.getData(this.listData);
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
    }
  }

  getData(list) {
    this.listFunction = [];
    for (const item of list) {
      item.treeStatus = 'expanded';
    }
    list.sort((a, b) => {
      if (a.code.toLowerCase() < b.code.toLowerCase()) {
        return -1;
      }
      if (a.code.toLowerCase() > b.code.toLowerCase()) {
        return 1;
      }
      return 0;
    });
    this.listTemp = list;
    if (this.functionId) {
      const index = list.findIndex((item) => {
        return item.id === this.functionId;
      });
      this.selected = [list[index]];
    }
    this.listFunction = list;
  }

  updateFilter() {
    const val = this.textSearch.trim().toLowerCase();
    this.listFunction = [];

    // filter our data
    const temp = this.listTemp.filter(function (d) {
      return d.code.toLowerCase().indexOf(val) !== -1 || d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.listFunction = temp;
  }

  choose() {
    this.activeModal.close(this.selected);
  }

  onTreeAction(event) {
    const row = event.row;
    if (row.treeStatus === 'collapsed') {
      row.treeStatus = 'expanded';
    } else {
      row.treeStatus = 'collapsed';
    }
    this.listFunction = [...this.listFunction];
  }

  generateAppName(type) {
    if (this.listApp && this.listApp.length > 0) {
      for (const app of this.listApp) {
        if (type === app.id) {
          return app.name;
        }
      }
    }
    return '';
  }

  closeModal() {
    this.activeModal.close(false);
  }
}
