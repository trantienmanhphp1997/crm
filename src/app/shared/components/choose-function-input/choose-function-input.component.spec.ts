import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseFunctionInputComponent } from './choose-function-input.component';

describe('ChooseFunctionInputComponent', () => {
  let component: ChooseFunctionInputComponent;
  let fixture: ComponentFixture<ChooseFunctionInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseFunctionInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseFunctionInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
