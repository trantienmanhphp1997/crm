import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FunctionModalComponent } from '../function-modal/function-modal.component';

@Component({
  selector: 'app-choose-function-input',
  templateUrl: './choose-function-input.component.html',
  styleUrls: ['./choose-function-input.component.scss'],
})
export class ChooseFunctionInputComponent implements OnInit, OnChanges {
  value = '';
  functionId: string;
  selected = [];
  @Input() listFunction: any[];
  @Input() model: any;
  @Output() modelChange = new EventEmitter<any>();
  constructor(private modalService: NgbModal) {}

  ngOnInit(): void {
    if (this.listFunction && this.model) {
      const item = this.listFunction.find((obj) => {
        return obj.id === this.model;
      });
      if (item) {
        this.value = item.name;
        this.functionId = item.id;
      }
    }
  }

  ngOnChanges(simpleChange: SimpleChanges) {
    this.ngOnInit();
  }

  search() {
    const modal = this.modalService.open(FunctionModalComponent, { windowClass: 'tree__function-modal' });

    modal.componentInstance.functionId = this.functionId;
    modal.componentInstance.listData = this.listFunction;
    modal.result
      .then((res) => {
        if (res) {
          if (res.length > 0) {
            this.functionId = res[0].id;
            this.value = res[0].name;
          } else {
            this.functionId = '';
            this.value = '';
          }
          this.modelChange.emit(this.functionId);
        }
      })
      .catch(() => {});
  }

  clearData() {
    this.selected = [];
    this.value = '';
    this.functionId = '';
    this.modelChange.emit(this.functionId);
  }
}
