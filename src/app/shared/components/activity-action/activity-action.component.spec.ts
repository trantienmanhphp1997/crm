import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityActionComponent } from './activity-action.component';

describe('ActivityActionComponent', () => {
  let component: ActivityActionComponent;
  let fixture: ComponentFixture<ActivityActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ActivityActionComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
