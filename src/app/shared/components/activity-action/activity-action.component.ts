import { forkJoin, of, Observable } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, Injector, AfterViewInit, Input } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import {
  ActivityType,
  CommonCategory,
  ConfigBackDate,
  FunctionCode,
  functionUri,
  ScreenName,
  ScreenType,
  TaskType,
} from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { ActivityService } from 'src/app/core/services/activity.service';
import * as moment from 'moment';
import { CustomerDetailApi } from 'src/app/pages/customer-360/apis';
import { Utils } from 'src/app/core/utils/utils';
import * as _ from 'lodash';
import {SaleManagerApi} from "../../../pages/sale-manager/api";

@Component({
  selector: 'activity-action',
  templateUrl: './activity-action.component.html',
  styleUrls: ['./activity-action.component.scss'],
})
export class ActivityActionComponent extends BaseComponent implements OnInit, AfterViewInit {
  @Input() parent: any;
  @Input() isActionHeader: boolean;
  @Input() type = ScreenType.Create;
  @Input() data: any;
  @Input() id: string;
  @Input() fromScreen: string;
  dataForm: any;
  titleHeader: string;
  today = new Date();
  form = this.fb.group({
    id: [''],
    parentId: [''],
    parentName: [''],
    parentType: [''],
    activityType: ['', CustomValidators.required],
    dateStart: [this.today, CustomValidators.required],
    activityResult: ['', CustomValidators.required],
    location: [''],
    note: [''],
    isFutureActivity: [''],
    futureActivity: this.fb.group({
      id: [''],
      activityType: [''],
      dateStart: [''],
      futureActivityType: [''],
      location: [''],
      note: [''],
    }),
    productCode: ''
  });
  nextActivityMinDate = this.today;
  activityMinDate = this.today;
  activityMaxDate = new Date();
  listType = [];
  listNextTask = [];
  listResult = [];
  isShowLocation = false;
  isShowLocationFuture = false;
  isValidator = false;
  itemCustomer: any;
  backHours = 0;
  listProduct: any;

  constructor(
    injector: Injector,
    private modalActive: NgbActiveModal,
    private activityService: ActivityService,
    private customerDetailApi: CustomerDetailApi,
    private saleManagerApi: SaleManagerApi,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    if (!this.type || this.type === ScreenType.Create) {
      this.titleHeader = 'title.createActivity';
      this.form.controls.futureActivity.disable();
    } else {
      this.titleHeader = 'title.detailActivity';
    }
    forkJoin([
      this.saleManagerApi.getProductByLevel().pipe(catchError((e) => of(undefined))),
      this.commonService
        .getCommonCategory(CommonCategory.CONFIG_ACTIVITY_RESULT)
        .pipe(catchError((e) => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.CONFIG_TYPE_ACTIVITY).pipe(catchError((e) => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.ACTIVITY_NEXT_TYPE).pipe(catchError((e) => of(undefined))),
      this.commonService
        .getCommonCategory(CommonCategory.CONFIG_BACK_DATE, ConfigBackDate.BACK_DATE_ACTIVITY)
        .pipe(catchError((e) => of(undefined))),
      this.customerDetailApi.get(this.data?.parentId).pipe(catchError((e) => of(undefined))),
    ]).subscribe(([listProduct, listResult, listType, listNextTask, backDate, itemCustomer]) => {
      this.listProduct = listProduct?.map((item) => {
        return { code: item.idProductTree, displayName: item.idProductTree + ' - ' + item.description };
      });
      this.itemCustomer = itemCustomer;
      listResult?.content?.forEach((item) => {
        if (item.code === item.value) {
          item.children = listResult?.content?.filter((itemChild) => {
            return itemChild.value === item.code && itemChild.code !== itemChild.value;
          });
          if (item.children.length === 0) {
            delete item.children;
          }
          this.listResult.push(item);
        }
      });
      this.listType = listType?.content || [];
      this.listNextTask = listNextTask?.content || [];
      this.backHours = (backDate?.content[0]?.value || 0) * 24;
      this.activityMinDate = new Date(moment().add(-this.backHours, 'hour').startOf('day').valueOf());
      if (this.dataForm) {
        this.form.patchValue(this.dataForm);
      }
      this.isLoading = false;
      this.handleMapData();
    });
  }

  ngAfterViewInit() {
    this.form.controls.activityResult.valueChanges.subscribe((value) => {
      if (value === 'KHAC') {
        this.form.controls.note.setValidators(CustomValidators.required);
      } else {
        this.form.controls.note.clearValidators();
      }
      this.form.controls.note.updateValueAndValidity({ emitEvent: false });
    });
    this.form.controls.dateStart.valueChanges.subscribe((value) => {
      if (moment(value).isAfter(moment())) {
        this.form?.controls?.activityResult?.clearValidators();
      } else {
        this.form?.controls?.activityResult?.setValidators(CustomValidators.required);
      }
      this.form?.controls?.activityResult?.updateValueAndValidity({ emitEvent: false });
    });
    this.form.valueChanges.subscribe((data) => {
      this.isShowLocation = data?.activityType === ActivityType.Meeting;
      this.isShowLocationFuture = data?.futureActivity?.activityType === ActivityType.Meeting;
      if (data?.isFutureActivity) {
        this.form?.get('futureActivity.activityType')?.setValidators(CustomValidators.required);
        this.form?.get('futureActivity.dateStart')?.setValidators(CustomValidators.required);
        this.form?.get('futureActivity.futureActivityType')?.setValidators(CustomValidators.required);
      } else {
        this.form?.get('futureActivity.activityType')?.clearValidators();
        this.form?.get('futureActivity.dateStart')?.clearValidators();
        this.form?.get('futureActivity.futureActivityType')?.clearValidators();
      }
      this.form?.controls?.futureActivity?.updateValueAndValidity({ emitEvent: false });
    });
  }

  handleMapData() {
    if (this.type === ScreenType.Update) {
      this.form.controls.dateStart.disable();
      this.form.controls.activityType.disable();
      this.form.controls.activityResult.disable();
      this.form.controls.note.disable();
      this.form.controls.location.disable();
      this.form.controls.productCode.disable();
      // this.activityMaxDate = null;
      if (this.data?.isFutureActivity) {
        this.form.controls.isFutureActivity.disable();
      }
    }

    if (this.data) {
      this.parent = {
        parentId: this.data?.parentId,
        parentName: this.data?.parentName,
        parentType: this.data?.parentType,
      };
    }
    if (this.isActionHeader) {
      if (this.id === this.data?.id && this.data?.dateStart) {
        this.dataForm = this.data;
      } else {
        this.dataForm = this.data?.futureActivity;
      }
      this.dataForm.dateStart = new Date(this.dataForm?.dateStart);
    } else {
      if (this.data?.dateStart) {
        this.data.dateStart = new Date(this.data.dateStart);
        if (
          moment(this.data.dateStart)
            .add(+this.backHours, 'hour')
            .isAfter(moment().startOf('day'))
        ) {
          this.form.controls.activityResult.enable();
          this.form.controls.note.enable();
          this.form.controls.location.enable();
          this.form.controls.activityType.enable();
          this.form.controls.dateStart.enable();
          this.form.controls.productCode.enable();
        }
        if (!this.data?.isFutureActivity) {
          this.form.controls.futureActivity.disable();
        }
        if (this.data.futureActivity?.dateStart) {
          this.data.futureActivity.dateStart = new Date(this.data.futureActivity?.dateStart);
          if (this.type !== ScreenType.Detail) {
            this.form.controls.futureActivity.enable();
          }
          if (
            moment(this.data.futureActivity.dateStart)
              .add(+this.backHours, 'hour')
              .isBefore(moment().startOf('day'))
          ) {
            this.form.controls.futureActivity.disable();
          }
        }
        this.dataForm = this.data;
      }
    }
    if (this.dataForm) {
      this.form.patchValue(this.dataForm);
      if (this.form.controls.productCode.value) {
        this.form.controls.productCode.setValue(+this.form.controls.productCode.value);
      }
    }
    if (this.type === ScreenType.Detail) {
      this.form.disable();
    }
  }

  save() {
    if (this.isLoading) {
      return;
    }
    this.isValidator = true;
    if (this.form.valid) {
      this.isLoading = true;
      const data = cleanDataForm(this.form);
      if (this.parent) {
        data.parentId = this.parent?.parentId;
        data.parentName = this.parent?.parentName;
        data.parentType = this.parent?.parentType;
      }
      let api: Observable<any>;
      if (this.type === ScreenType.Create) {
        api = this.activityService.create(data);
      } else {
        data.futureActivityId = this.data?.futureActivityId;
        api = this.activityService.update(data);
      }
      api.subscribe(
        () => {
          this.isLoading = false;
          this.communicateService.request({ name: 'refreshActivityList' });
          this.messageService.success(this.notificationMessage.success);
          this.modalActive.close(true);
        },
        () => {
          this.isLoading = false;
          this.messageService.error(this.notificationMessage.error);
        }
      );
    } else {
      validateAllFormFields(this.form);
    }
  }

  isCreateNextActivity(e) {
    if (e?.checked) {
      this.form.controls.futureActivity.enable();
    } else {
      this.form.controls.futureActivity.reset();
      this.form.controls.futureActivity.disable();
    }
  }

  closeModal() {
    this.modalActive.close(false);
  }

  delete() {
    const params = {
      campaignId: this.data?.campaignId
    }
    this.confirmService.confirm().then((res) => {
      if (res) {
        this.isLoading = true;
        this.activityService.deleteByCodeAndCampaignId(this.dataForm?.id, params).subscribe(
          () => {
            this.isLoading = false;
            this.modalActive.close(true);
            this.messageService.success(this.notificationMessage.success);
          },
          () => {
            this.isLoading = false;
            this.messageService.error(this.notificationMessage.error);
          }
        );
      }
    });
  }

  isDelete() {
    return (
      this.type === ScreenType.Detail &&
      this.dataForm?.dateStart &&
      this.dataForm?.createdBy === this.currUser?.username &&
      moment(this.dataForm?.dateStart)
        .add(+this.backHours, 'hour')
        .isSameOrAfter(moment().startOf('day'))
    );
  }

  isUpdate() {
    return (
      this.objFunction?.update &&
      this.dataForm?.dateStart &&
      this.dataForm?.createdBy === this.currUser?.username &&
      moment(this.dataForm?.dateStart)
        .add(+this.backHours, 'hour')
        .isSameOrAfter(moment().startOf('day'))
    );
  }

  showFormUpdate() {
    this.isActionHeader = false;
    this.type = ScreenType.Update;
    // this.activityMaxDate = null;
    this.handleMapData();
  }

  detailParent() {
    if (this.parent.parentType === TaskType.CUSTOMER && Utils.trimNullToEmpty(this.parent.parentId) !== '') {
      if (!_.isEmpty(_.trim(this.itemCustomer?.systemInfo?.accountType)) && !_.isEmpty(_.trim(this.parent?.parentId))) {
        if (this.fromScreen === ScreenName.ActivityHistory) {
          this.modalActive.close(ScreenName.ActivityHistory);
        } else {
          this.modalActive.close(false);
        }
        const customerType = _.get(this.itemCustomer, 'otherInfo.khoiNHS')
          ? _.get(this.itemCustomer, 'otherInfo.khoiNHS')
          : _.get(this.itemCustomer, 'systemInfo.accountType');
        this.router.navigateByUrl(
          `${functionUri.customer_360_manager}/detail/${_.toLower(customerType)}/${this.parent?.parentId}`,
          {
            skipLocationChange: true,
          }
        );
      }
    }
  }

  setMaxDate() {
    this.activityMaxDate = new Date();
  }
}
