import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'editor-primeng',
  templateUrl: './editor-primeng.component.html',
  styleUrls: ['./editor-primeng.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'PEditor__Wrapper',
  },
})
export class EditorPrimengComponent implements OnInit {
  constructor() {}
  @Input() model: string;
  @Input() disabled: boolean;
  @Output() modelChange = new EventEmitter();

  ngOnInit(): void {
  }

  onFocus(event) {
    // this.modelChange.emit(this.model !== '<p></p>' && this.model !== '<p> </p>' ? this.model : '');
    if(event?.htmlValue || event.textValue !== undefined) {
      this.modelChange.emit(event?.htmlValue);
    }
  }
}
