import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportLanesComponent } from './report-lanes.component';

describe('ReportLanesComponent', () => {
  let component: ReportLanesComponent;
  let fixture: ComponentFixture<ReportLanesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportLanesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportLanesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
