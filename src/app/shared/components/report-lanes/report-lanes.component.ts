import { Subscription } from 'rxjs';
import { Component, OnInit, OnChanges, ChangeDetectionStrategy, Input, SimpleChanges } from '@angular/core';
import { CommunicateService } from 'src/app/core/services/communicate.service';
import { ProgressBarClass } from 'src/app/core/utils/common-constants';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-report-lanes',
  templateUrl: './report-lanes.component.html',
  styleUrls: ['./report-lanes.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReportLanesComponent implements OnInit, OnChanges {
  @Input() data: any;
  lanes: any[] = [];
  title = '';
  subscription: Subscription;

  constructor(private communicateService: CommunicateService, private translate: TranslateService) {
    this.subscription = this.communicateService.respond$.subscribe((res) => {});
  }

  ngOnInit(): void {
    this.translate.get('lefBar.statisticalLanes').subscribe((res) => {
      if (res) {
        this.title = res;
      }
    });
  }

  ngOnChanges(change: SimpleChanges) {
    if (change?.lanes?.currentValue) {
      this.lanes = change?.lanes?.currentValue;
    }
    if (change?.title?.currentValue) {
      this.title = change?.title?.currentValue;
    }
  }

  generateClassProgressBar(lane) {
    return ProgressBarClass[lane.index];
  }

  identify(index, item) {
    return item?.id;
  }
}
