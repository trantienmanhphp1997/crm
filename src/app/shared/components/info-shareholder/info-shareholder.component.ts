import { Component, OnInit, Injector, Input, OnChanges, SimpleChanges } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { IInfoContact } from '../../../pages/sale-manager/model/info-basic.interface';
import { AddressApi } from '../../../pages/sale-manager/api/address.api';
import {DatePipe, DecimalPipe} from '@angular/common';
import _ from 'lodash';
@Component({
    selector: 'app-info-shareholder',
    templateUrl: './info-shareholder.component.html',
    styleUrls: ['./info-shareholder.component.scss'],
    providers: [DatePipe, DecimalPipe],
  })

  export class InfoShareholderComponent extends BaseComponent implements OnInit{
    @Input() taxCode: string;
    public isLoading = false;
    listData  = [];
    tabIndex = 0;
    item:IInfoContact;

    columns = [
        {
            name: 'Tên cổ đông',
            prop: 'tenTvCd',
            sortable: false,
            draggable: false,
            width: 240,
        },
        {
            name: 'Số GTTT/DKKD',
            prop: 'soChungThuc',
            sortable: false,
            draggable: false,
            width: 50,
        },
        {
            name: 'Địa chỉ',
            prop: 'diaChi',
            sortable: false,
            draggable: false,
            width: 300,
        },
        {
          name: 'Ngày sinh/ngày thành lập',
          prop: 'ngaysinhThanhlap',
          sortable: false,
          draggable: false,
          width: 100,
        },
        {
          name: 'Tỷ lệ góp vốn',
          prop: 'tyLePhantram',
          sortable: false,
          draggable: false,
          width: 50,
        },
        {
          name: 'Giá trị góp vốn (VNĐ)',
          prop: 'vonGop',
          sortable: false,
          draggable: false,
          width: 50,
        },

    ];

    constructor(
        private api: AddressApi,
        private datePipe: DatePipe,
        private decimalPipe: DecimalPipe,
        injector: Injector) {
        super(injector);
        this.isLoading = true;
    }
    ngOnInit(): void {
        this.getListContact();
    }
  getListContact(){
        this.api.getShareholderInfoByTaxCode(this.taxCode).subscribe(value => {
            this.listData = value;
            this.isLoading = false;
            this.listData?.forEach(item => {
              item.ngaysinhThanhlap = this.datePipe.transform(item.ngaysinhThanhlap, 'dd/MM/yyyy');
              item.tyLePhantram = item.tyLePhantram + '%';
              item.vonGop = this.decimalPipe.transform(item.vonGop);
            })
        }, (e) => {
            this.isLoading = false;
            this.messageService.error(e?.error?.description);
        })
    }

  onTabChanged($event) {
    this.tabIndex = _.get($event, 'index');
  }
}
