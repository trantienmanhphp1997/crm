import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ConfirmType } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ConfirmDialogComponent implements OnInit {
  message: string | string[];
  type: number;
  confirmType = ConfirmType;
  title: string;
  isConfirm = true;
  disableConfirm = false;
  isChoose = false;
  isCopy: boolean;
  background = 'background-blue';
  listMessage: string[] = [];
  content = '';
  color = 'light-blue';
  closeTitle = 'btnAction.cancel';
  continueTitle = 'btnAction.continue';
  constructor(private modal: NgbActiveModal) {}

  ngOnInit(): void {
    if (this.message && this.message instanceof Array) {
      for (const mes of this.message) {
        this.listMessage.push(mes);
      }
    } else {
      const mes: any = this.message;
      this.listMessage.push(mes);
    }
    this.isConfirm = this.type === ConfirmType.Confirm || !this.type;
    if (this.type === ConfirmType.CusError) {
      this.background = 'background-red';
      this.color = 'red';
    } else if (this.type === ConfirmType.Warning) {
      this.background = 'background-yellow';
      this.color = 'yellow';
    } else {
      this.background = 'background-blue';
    }
  }

  close(event: boolean) {

    this.modal.close(event);
  }

  copy() {
    const el = document.createElement('textarea');
    el.value = document.getElementById('content').textContent;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    this.modal.close(false);
  }
}
