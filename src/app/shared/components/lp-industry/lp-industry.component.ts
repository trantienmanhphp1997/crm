import { Component, Input, Output, ViewChild, EventEmitter, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ColumnMode, DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { IndustryModalComponent } from '../../../pages/system/components/industry-modal/industry-modal.component';
import { Utils } from '../../../core/utils/utils';
import { CategoryService } from '../../../pages/system/services/category.service';
import { SalesService } from '../../../pages/system/services/sales.service';
import _ from 'lodash';
import { SessionService } from 'src/app/core/services/session.service';
import { SessionKey } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'lp-industry',
  templateUrl: './lp-industry.component.html',
  styleUrls: ['./lp-industry.component.scss'],
})
export class LpIndustryComponent implements OnInit, OnChanges {
  constructor(
    private modalService: NgbModal,
    private service: CategoryService,
    private salesService: SalesService,
    private sessionService: SessionService
  ) {}
  listIndustry: Array<any>;
  ColumnMode = ColumnMode;
  SelectionType = SelectionType;
  public currentVisible = 5;
  notificationMessage: any;

  listIndustryTemp: Array<any>;
  @Input() model: any;
  @Input() typeCustomer: any;
  @Output() modelChange = new EventEmitter();
  fields: any;
  @ViewChild('tableBranch') public tableBranch: DatatableComponent;
  display_name: string;
  itemSelected: any;

  ngOnInit(): void {
    
    if(this.typeCustomer === 'KHTN'){
      const list = this.sessionService.getSessionData(SessionKey.LIST_INDUSTRY);
      console.log("typeCustomer : ",this.typeCustomer)
      if (_.isArray(list)) {
        this.listIndustryTemp = list;
        const itemSelected = _.find(this.listIndustryTemp, (item) => item.key === this.model);
        if (itemSelected) {
          this.display_name = `${itemSelected.key} - ${itemSelected.value}`;
        }
      } else {
      this.salesService.getAllIndustry().subscribe(
        (res: Array<any>) => {
          let listData = []; 
          if(res){
            res.forEach((e) => {
              let item = {
                "key":e.id,
                "value":e.loanPurpose,
                "parent":e.loai
              }
              listData.push(item);
            })
          }
          this.listIndustry = listData;
          if (Utils.isArrayNotEmpty(listData)) {
            this.listIndustryTemp = [...listData];
          } else {
            this.listIndustryTemp = [];
          }
          this.sessionService.setSessionData(SessionKey.LIST_INDUSTRY, this.listIndustryTemp);
          this.itemSelected = _.find(this.listIndustryTemp, (item) => item.key === this.model);
          if (this.itemSelected) {
            this.display_name = `${this.itemSelected.key} - ${this.itemSelected.value}`;
          }
        },
        () => {
          this.listIndustry = [];
          this.listIndustryTemp = [];
        }
      );
      }
    }else{
      this.service.getIndustries({}).subscribe(
        (res: Array<any>) => {
          this.listIndustry = res;
          if (Utils.isArrayNotEmpty(res)) {
            this.listIndustryTemp = [...res];
          } else {
            this.listIndustryTemp = [];
          }
          // this.sessionService.setSessionData(SessionKey.LIST_INDUSTRY, this.listIndustryTemp);
          this.itemSelected = _.find(this.listIndustryTemp, (item) => item.key === this.model);
          if (this.itemSelected) {
            this.display_name = `${this.itemSelected.key} - ${this.itemSelected.value}`;
          }
        },
        () => {
          this.listIndustry = [];
          this.listIndustryTemp = [];
        }
      );
      
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    this.itemSelected = _.find(this.listIndustryTemp, (item) => item.key === this.model);
    if (this.itemSelected) {
      this.display_name = `${this.itemSelected.key} - ${this.itemSelected.value}`;
    }
  }

  chooseIndustry() {
    const modal = this.modalService.open(IndustryModalComponent, { windowClass: 'tree__branch-modal' });
    modal.componentInstance.listBranchOld = _.filter(this.listIndustryTemp, (x) => this.model === x.key);
    modal.componentInstance.typeCustomer = this.typeCustomer;
    modal.result
      .then((res) => {
        if (res) {
          this.itemSelected = res[0];
          this.display_name = `${_.get(this.itemSelected, 'key')} - ${_.get(this.itemSelected, 'value')}`;
          this.modelChange.emit(_.get(this.itemSelected, 'key'));
        }
      })
      .catch(() => {});
  }

  delete() {
    this.model = '';
    this.display_name = '';
    this.modelChange.emit('');
    this.itemSelected = null;
  }
}
