import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataTablePagerComponent as SuperDataTablePagerComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-pager-ngx-datatable',
  templateUrl: './pager-ngx-datatable.component.html',
  styleUrls: ['./pager-ngx-datatable.component.scss'],
})
export class PagerNgxDatatableComponent extends SuperDataTablePagerComponent {
  @Input() quickPage = true;
  @Input() pagerLeftArrowIcon = 'las la-angle-left';
  @Input() pagerRightArrowIcon = 'las la-angle-right';
  @Input() pagerPreviousIcon = 'las la-step-backward';
  @Input() pagerNextIcon = 'las la-step-forward';
  @Input() hasDropdown = false;

  @Input()
  set offset(val: number) {
    this._offset = val || 1;
    this.pages = this.calcPages();
  }

  get offset(): number {
    return this._offset;
  }

  @Input()
  set size(val: number) {
    this._size = val;
    this.pageSize = val;
    this.pages = this.calcPages();
  }

  get size(): number {
    return this._size;
  }

  @Input()
  set count(val: number) {
    this._count = val;
    this.pages = this.calcPages();
  }

  get count(): number {
    return this._count;
  }

  @Input()
  set page(val: number) {
    this._page = val === 0 ? 1 : val;
    this.pages = this.calcPages();
  }

  get page(): number {
    return this._page;
  }

  get totalPages(): number {
    const count = this.size < 1 ? 1 : Math.ceil(this.count / this.size);
    return Math.max(count || 0, 1);
  }

  @Output() change: EventEmitter<any> = new EventEmitter();
  @Output() eventChangePageSize: EventEmitter<any> = new EventEmitter();

  public _visiblePagesCount: number = 5;

  @Input()
  set visiblePagesCount(val: number) {
    this._visiblePagesCount = val;
    this.pages = this.calcPages();
  }

  get visiblePagesCount(): number {
    return this._visiblePagesCount;
  }

  _count: number = 0;
  _page: number = 1;
  _size: number = 0;
  _offset: number = 0;
  pages: any;

  listPageSize = [
    { code: 10, displayName: "10 bản ghi"},
    { code: 50, displayName: "50 bản ghi"},
    { code: 100, displayName: "100 bản ghi"},
    { code: 200, displayName: "200 bản ghi"},
  ];
  pageSize = 10;

  onChangePage(event) {
    this.pageSize = event.value;
    this.eventChangePageSize.emit(this.pageSize);
  }

  canPrevious(): boolean {
    return this.page > 1;
  }

  canNext(): boolean {
    return this.page < this.totalPages;
  }

  prevPage(): void {
    this.selectPage(this.page - 1);
  }

  nextPage(): void {
    this.selectPage(this.page + 1);
  }

  selectPage(page: number): void {
    if (page > 0 && page <= this.totalPages && page !== this.page) {
      this.page = page;

      this.change.emit({
        page,
      });
    }
  }

  calcPages(page?: number): any[] {
    const pages = [];
    let startPage = 1;
    let endPage = this.totalPages;
    const maxSize = this.visiblePagesCount;
    const isMaxSized = maxSize < this.totalPages;

    page = page || this.page;

    if (isMaxSized) {
      startPage = page - Math.floor(maxSize / 2);
      endPage = page + Math.floor(maxSize / 2);

      if (startPage < 1) {
        startPage = 1;
        endPage = Math.min(startPage + maxSize - 1, this.totalPages);
      } else if (endPage > this.totalPages) {
        startPage = Math.max(this.totalPages - maxSize + 1, 1);
        endPage = this.totalPages;
      }
    }
    for (let num = startPage; num <= endPage; num++) {
      pages.push({
        number: num,
        text: <string>(<any>num),
      });
    }
    return pages;
  }
}
