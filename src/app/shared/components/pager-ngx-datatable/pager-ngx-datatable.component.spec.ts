import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagerNgxDatatableComponent } from './pager-ngx-datatable.component';

describe('PagerNgxDatatableComponent', () => {
  let component: PagerNgxDatatableComponent;
  let fixture: ComponentFixture<PagerNgxDatatableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagerNgxDatatableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagerNgxDatatableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
