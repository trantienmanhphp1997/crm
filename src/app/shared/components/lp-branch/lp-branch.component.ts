import { Component, Input, Output, ViewChild, EventEmitter, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ColumnMode, DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { BranchModalComponent } from '../../../pages/system/components/branch-modal/branch-modal.component';
import { RmService } from '../../../pages/rm/services/rm.service';
import { TranslateService } from '@ngx-translate/core';
import _ from 'lodash';

@Component({
  selector: 'lp-branch',
  templateUrl: './lp-branch.component.html',
  styleUrls: ['./lp-branch.component.scss'],
})
export class LpBranchComponent implements OnInit {
  constructor(private modalService: NgbModal, private service: RmService, private translate: TranslateService) {}
  pageableUser: Pageable;
  pageablePermission: Pageable;
  selectedUser = [];
  selectedPermission = [];
  ColumnMode = ColumnMode;
  SelectionType = SelectionType;
  public currentVisible: number = 5;
  notificationMessage: any;
  branchSearch = '';
  @Input() listBranch: Array<any>;
  @Input('model') branchCode: string;
  @Input() disabled: boolean;
  @Output() modelChange = new EventEmitter();
  fields: any;
  itemFocusUser: any;
  itemFocusPermission: any;
  @ViewChild('tableBranch') public tableBranch: DatatableComponent;
  display_name: string;

  ngOnInit() {
    const itemBranch = _.chain(this.listBranch)
      .find((item) => item.code === this.branchCode)
      .value();
    if (itemBranch) {
      this.display_name = `${_.get(itemBranch, 'code') || ''} - ${_.get(itemBranch, 'name') || ''}`;
    }
  }

  chooseBranch() {
    const modal = this.modalService.open(BranchModalComponent, { windowClass: 'tree__branch-modal' });
    if (this.branchCode) {
      modal.componentInstance.listBranchOld = _.chain(this.listBranch)
        .filter((x) => this.branchCode === x.code)
        .value();
    } else {
      modal.componentInstance.listBranchOld = [];
    }
    modal.componentInstance.listBranch = this.listBranch;
    modal.result
      .then((res) => {
        if (res) {
          const branchCodes = [];
          res.forEach((branch) => {
            branchCodes.push(branch.code);
          });
          const data: any = {};
          data.branches = branchCodes;
          if (_.get(res, '[0]')) {
            this.display_name = `${_.get(res, '[0].code')} - ${_.get(res, '[0].name')}`;
          }
          this.modelChange.emit(_.get(res, '[0].code'));
        }
      })
      .catch(() => {});
  }
}
