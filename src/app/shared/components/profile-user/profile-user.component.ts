import { environment } from 'src/environments/environment';
import { KeycloakService } from 'keycloak-angular';
import { Component, OnInit, OnDestroy, ViewEncapsulation, HostBinding } from '@angular/core';
import { getRole} from 'src/app/core/utils/function';
import { global } from '@angular/compiler/src/util';
import qs from 'qs';
import * as _ from 'lodash';
import { NotificationService } from 'src/app/core/services/notification.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { SessionService } from 'src/app/core/services/session.service';
import { SessionKey } from 'src/app/core/utils/common-constants';
import { NotifyModel } from 'src/app/core/interfaces/notify.interface';

@Component({
  selector: 'app-profile-user',
  templateUrl: './profile-user.component.html',
  styleUrls: ['./profile-user.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ProfileUserComponent implements OnInit, OnDestroy {
  @HostBinding('class') hostClass = 'app-profile-user';
  fullName: string;
  role: string;
  subTitle: string;
  countUnReadNotify: number = 0;
  listNotify: NotifyModel[] = [];
  offset = 0;
  countNotify: number = 0;
  interval: any;
  isLoading = false;
  currUser: any;

  constructor(
    private keycloakService: KeycloakService,
    private notificationService: NotificationService,
    private session: SessionService
  ) {
    this.currUser = this.session.getSessionData(SessionKey.USER_INFO);
    this.role = getRole(global.roles);
    this.subTitle = this.currUser?.branch || '';
    this.fullName = `${this.currUser?.fullName || ''} - ${this.currUser?.code || ''}`;
  }

  ngOnInit(): void {
    // this.intervalData();
  }

  intervalData() {
    this.notificationService
      .findAll(0)
      .pipe(catchError(() => of(undefined)))
      .subscribe((data) => {
        if (data) {
          this.countUnReadNotify = +(data?.countUnreadNotification || 0);
          data?.notifications?.reverse().forEach((item) => {
            if (_.findIndex(this.listNotify, (i) => i.id === item.id) === -1) {
              const notify = {
                id: item.id,
                title: item.content,
                time: item.sendDate,
                avatar: {
                  style: {
                    ['background-color']: item.notificationDescriptionDTO?.color,
                    color: item.notificationDescriptionDTO?.textColor,
                  },
                  label: item.notificationDescriptionDTO?.description,
                },
                isReaded: item.read,
              };
              this.listNotify.unshift(notify);
            }
          });
          this.countNotify = this.listNotify?.length || 0;
          const timer = setTimeout(() => {
            this.intervalData();
            clearTimeout(timer);
          }, 6000);
        }
      });
  }

  viewProfile() {
    const params = {
      referrer: 'security-admin-console',
      referrer_uri: environment.keycloak.issuer + 'admin/crm/console/#/forbidden',
    };
    window.open(environment.keycloak.issuer + 'realms/crm/account?' + qs.stringify(params), '_blank');
  }

  markAllAsRead() {
    if (this.countUnReadNotify > 0) {
      this.notificationService.markAllAsRead().subscribe(() => {
        this.countUnReadNotify = 0;
        this.listNotify?.forEach((item) => {
          item.isReaded = true;
        });
      });
    }
  }

  markAsRead(item) {
    if (!item.isReaded) {
      this.notificationService.markAsRead(item?.id).subscribe(() => {
        this.countUnReadNotify -= 1;
        item.isReaded = true;
      });
    }
  }

  onScroll(e) {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    const offset = this.countNotify > 0 ? this.countNotify - 1 : 0;
    this.notificationService
      .findAll(offset)
      .pipe(catchError(() => of(undefined)))
      .subscribe(
        (data) => {
          if (data) {
            this.countUnReadNotify = +(data?.countUnreadNotification || 0);
            data?.notifications?.forEach((item) => {
              const notify = {
                id: item.id,
                title: item.content,
                time: item.sendDate,
                avatar: {
                  style: {
                    ['background-color']: item.notificationDescriptionDTO?.color,
                    color: item.notificationDescriptionDTO?.textColor,
                  },
                  label: item.notificationDescriptionDTO?.description,
                },
                isReaded: item.read,
              };
              this.listNotify.push(notify);
            });
            this.countNotify = this.listNotify?.length || 0;
            this.isLoading = false;
          }
        },
        () => {
          this.isLoading = false;
        }
      );
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }
}
