import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MatTabsModule } from '@angular/material/tabs';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { CalendarModule } from 'primeng/calendar';
import {
  ConfirmDialogComponent,
  ProfileUserComponent,
  PagerNgxDatatableComponent,
  LpBranchesComponent,
  ChooseFunctionInputComponent,
  FunctionModalComponent,
  LpBranchComponent,
  PageNotFoundComponent,
  AccessDeniedComponent,
  LpIndustryComponent,
  LogActivityComponent,
  ReportLanesComponent,
  ActivityActionComponent,
  LoadingComponent,
  LpEditorComponent,
  ActivityHistoryComponent,
  BranchesByRegionComponent,
} from './components';
import { DIRECTIVES } from './directives';
import { DropdownModule } from 'primeng/dropdown';
import { CheckboxModule } from 'primeng/checkbox';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { CascadeSelectModule } from 'primeng/cascadeselect';
import { BadgeModule } from 'primeng/badge';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { AvatarModule } from 'primeng/avatar';
// import { NgxEditorModule } from 'ngx-editor';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { FileUploadModule } from 'primeng/fileupload';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { MatDialogModule } from '@angular/material/dialog';
import {EditorPrimengComponent} from './components/editor-primeng/editor-primeng.component';
import {EditorModule} from 'primeng/editor';
import { InfoShareholderComponent } from './components/info-shareholder/info-shareholder.component';
import { FormatNumberPipe } from './pipe/format-number.pipe';

const COMPONENTS = [
  ConfirmDialogComponent,
  ProfileUserComponent,
  PagerNgxDatatableComponent,
  LpBranchesComponent,
  ChooseFunctionInputComponent,
  FunctionModalComponent,
  LpBranchComponent,
  PageNotFoundComponent,
  AccessDeniedComponent,
  LpIndustryComponent,
  LogActivityComponent,
  ReportLanesComponent,
  ActivityActionComponent,
  LoadingComponent,
  LpEditorComponent,
  ActivityHistoryComponent,
  EditorPrimengComponent,
  InfoShareholderComponent,
  BranchesByRegionComponent,
  FormatNumberPipe
];

@NgModule({
  declarations: [...COMPONENTS, ...DIRECTIVES],
  imports: [
    CommonModule,
    AvatarModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule,
    NgbModule,
    NgxDatatableModule,
    MatTabsModule,
    MatProgressBarModule,
    CalendarModule,
    DropdownModule,
    CheckboxModule,
    InputTextareaModule,
    InputTextModule,
    CascadeSelectModule,
    // NgxEditorModule,
    BadgeModule,
    OverlayPanelModule,
    NgxExtendedPdfViewerModule,
    FileUploadModule,
    InfiniteScrollModule,
    MatDialogModule,
    EditorModule,
  ],
  exports: [...COMPONENTS, ...DIRECTIVES],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class SharedModule {}
