import { DatatableComponent } from '@swimlane/ngx-datatable';
import {
  Directive,
  Renderer2,
  ElementRef,
  AfterViewInit,
  AfterViewChecked,
  HostListener,
  OnDestroy,
} from '@angular/core';
import * as _ from 'lodash';

/**
 * Directive to add an empty row to `ngx-datatable components that do not have any data.
 * This fixes a bug with ngx-datatable in which empty tables do not have a scrollable header.
 */
@Directive({
  selector: '[ngxDataTable]',
})
export class NgxDataTableDirective implements AfterViewInit, AfterViewChecked, OnDestroy {
  private datatableBodyEl: HTMLElement;
  private datatableHeaderEl: HTMLElement;
  private datatableEmptyRowEl: HTMLElement;
  private datatableRowCenterEl: HTMLElement;
  private datatableHeaderRowCenterEl: HTMLElement;
  private clickListener: any;

  constructor(private renderer: Renderer2, private hostElRef: ElementRef, private component: DatatableComponent) {}

  @HostListener('window:resize')
  onResize() {
    this.component?.recalculate();
  }

  public ngAfterViewInit() {
    this.setUpElementReferences();
    this.clickListener = (event) => {
      if (this.isClickToggleSidebar(event?.target)) {
        const timer = setTimeout(() => {
          this.component?.recalculate();
          clearTimeout(timer);
        }, 200);
      } else {
        this.component?.recalculate();
      }
    };
    document.addEventListener('click', this.clickListener);
  }

  public ngAfterViewChecked() {
    if (this.shouldApplyHack && this.datatableHeaderRowCenterEl) {
      this.datatableHeaderRowCenterEl.style.transform = 'translate3d(0px, 0px, 0px)';
    }
  }

  private isClickToggleSidebar(el) {
    return (
      (!_.isEmpty(el?.className) && el?.className?.includes('btn-toggle-sidebar')) ||
      (!_.isEmpty(el?.parentElement?.className) && el?.parentElement?.className?.includes('btn-toggle-sidebar'))
    );
  }

  private setUpElementReferences() {
    const hostEl = this.hostElRef.nativeElement;
    this.datatableBodyEl = hostEl.getElementsByClassName('datatable-body')[0];
    this.datatableHeaderEl = hostEl.getElementsByClassName('datatable-header')[0];
    this.datatableEmptyRowEl = hostEl.getElementsByClassName('empty-row')[0];
    this.datatableRowCenterEl = hostEl.getElementsByClassName('datatable-row-center')[0];
    this.datatableHeaderRowCenterEl = this.datatableHeaderEl?.querySelector('.datatable-row-center');
  }

  private get shouldApplyHack(): boolean {
    const hostEl = this.hostElRef.nativeElement;
    this.datatableEmptyRowEl = hostEl.getElementsByClassName('empty-row')[0];
    return !!this.datatableEmptyRowEl;
  }

  ngOnDestroy() {
    document.removeEventListener('click', this.clickListener);
  }
}
