import { Directive, ElementRef, HostListener } from "@angular/core";

@Directive({
    selector: '[trimInput]',
})
export class TrimInputDirective {
    lastValue: string;

    constructor(public ref: ElementRef) { }

    @HostListener('input', ['$event']) onInput($event) {
        $event.target.value = $event.target.value.trim();
    }
}