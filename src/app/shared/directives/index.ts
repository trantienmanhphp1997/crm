import { AdDirective } from './dynamic-component.directive';
import { LowercaseInputDirective } from './lowercase-input.directive';
import { NgxModalDraggableDirective } from './modal-drag.directive';
import { NgxDataTableDirective } from './ngx-datatable.directive';
import { TrimInputDirective } from './trim-input.directive';
import { UppercaseInputDirective } from './uppercase-input.directive';

export const DIRECTIVES = [
  AdDirective,
  UppercaseInputDirective,
  LowercaseInputDirective,
  NgxDataTableDirective,
  NgxModalDraggableDirective,
  TrimInputDirective
];
