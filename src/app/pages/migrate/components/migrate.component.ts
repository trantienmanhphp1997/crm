import { saveAs } from 'file-saver';
import { Component, OnInit, HostBinding, ViewEncapsulation } from '@angular/core';
import { ConfirmService } from 'src/app/core/services/confirm.service';
import { MigrateService } from '../migrate.service';
import { CustomKeycloakService } from '../../system/services/custom-keycloak.service';
import { CommonCategoryService } from 'src/app/core/services/common-category.service';
import { CommonCategory, SessionKey } from 'src/app/core/utils/common-constants';
import * as _ from 'lodash';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { FileService } from 'src/app/core/services/file.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-migrate',
  templateUrl: './migrate.component.html',
  styleUrls: ['./migrate.component.scss'],
  providers: [MigrateService],
  encapsulation: ViewEncapsulation.None,
})
export class MigrateComponent implements OnInit {
  @HostBinding('class.app__right-content') appRightContent = true;
  isLoading = true;
  count = 5000;
  size = 5000;
  page = 0;
  isUpload: boolean;
  fileId: string;

  constructor(
    private service: MigrateService,
    private confirmService: ConfirmService,
    private customKeycloakService: CustomKeycloakService,
    private commonService: CommonCategoryService,
    private fileService: FileService,
    public datepipe: DatePipe
  ) {}

  ngOnInit(): void {
    this.isLoading = false;
  }

  action1(event, fileUpload) {
    if (this.isUpload) {
      return;
    }
    this.isLoading = true;
    const formData = new FormData();
    formData.append('file', event.files[0]);
    this.service.migrateAPI_1(formData).subscribe(
      (res) => {
        fileUpload.clear();
        this.isUpload = true;
        saveAs(
          new Blob([res], {
            type: 'application/octet-stream',
          }),
          'rm-error.xlsx'
        );
        this.confirmService.success('Thực hiện thành công');
        this.isLoading = false;
      },
      () => {
        this.confirmService.error('Thực hiện không thành công');
        this.isLoading = false;
      }
    );
  }

  action2() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_2().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action3() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_3().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action4() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_4().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action5() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_5().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action6() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_6().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action7() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_7().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action8() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_8().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action9(event, fileUpload) {
    if (this.isUpload) {
      return;
    }
    this.isLoading = true;
    const formData = new FormData();
    formData.append('file', event.files[0]);
    this.service.migrateAPI_9(formData).subscribe(
      (res) => {
        fileUpload.clear();
        this.isUpload = true;
        this.confirmService.success('Thực hiện thành công');
        this.isLoading = false;
      },
      () => {
        this.confirmService.error('Thực hiện không thành công');
        this.isLoading = false;
      }
    );
  }

  action10(event, fileUpload) {
    if (this.isUpload) {
      return;
    }
    this.isLoading = true;
    const formData = new FormData();
    formData.append('file', event.files[0]);
    this.service.migrateAPI_10(formData).subscribe(
      (res) => {
        fileUpload.clear();
        this.isUpload = true;
        this.confirmService.success('Thực hiện thành công');
        this.isLoading = false;
      },
      () => {
        this.confirmService.error('Thực hiện không thành công');
        this.isLoading = false;
      }
    );
  }

  action11() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_11().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action12() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_12().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action13() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_13().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action14() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        forkJoin([
          this.customKeycloakService.getRolesRealmManagement().pipe(catchError(() => of(undefined))),
          this.commonService
            .getCommonCategory(CommonCategory.REALM_ROLE_MANAGEMENT)
            .pipe(catchError(() => of(undefined))),
        ]).subscribe(([clientRoles, realmRoles]) => {
          if (realmRoles?.content) {
            const count = realmRoles?.content?.length || 0;
            let countLoop = 0;
            realmRoles?.content?.forEach((item) => {
              const roles = item?.value?.split(',') || [];
              const apis = [];
              roles.forEach((roleName) => {
                apis.push(this.customKeycloakService.getUserByRole(roleName).pipe(catchError(() => of(undefined))));
              });
              forkJoin(apis).subscribe((results: any[]) => {
                const apisAddClientRole = [];
                results?.forEach((users) => {
                  users?.forEach((user) => {
                    const itemClientRole = _.find(
                      clientRoles,
                      (i) => i?.name?.toLowerCase() === item?.code?.toLowerCase()
                    );
                    if (itemClientRole) {
                      apisAddClientRole.push(
                        this.customKeycloakService
                          .createRolesRealmManagementByUserId(user.id, [itemClientRole])
                          .pipe(catchError(() => of(undefined)))
                      );
                    }
                  });
                });
                forkJoin(apisAddClientRole).subscribe(() => {
                  countLoop += 1;
                  if (countLoop === count) {
                    this.isLoading = false;
                    this.confirmService.success('Thực hiện thành công');
                  }
                });
              });
            });
          } else {
            this.isLoading = false;
            this.confirmService.success('Thực hiện thành công');
          }
        });
      }
    });
  }

  action15() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_15().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action16() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_16().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action17() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_17().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action18(event, fileUpload) {
    if (this.isUpload) {
      return;
    }
    this.isLoading = true;
    const formData = new FormData();
    formData.append('file', event.files[0]);
    this.service.migrateAPI_18(formData).subscribe(
      (res) => {
        fileUpload.clear();
        this.isUpload = true;
        // saveAs(
        //   new Blob([res], {
        //     type: 'application/octet-stream',
        //   }),
        //   'rm-error.xlsx'
        // );
        this.confirmService.success('Thực hiện thành công');
        this.isLoading = false;
      },
      () => {
        this.confirmService.error('Thực hiện không thành công');
        this.isLoading = false;
      }
    );
  }

  action19() {
    this.isLoading = true;
    this.fileService.downloadFile(this.fileId, 'OPN-data-assgin-report.xlsx').subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.confirmService.error('Thực hiện không thành công');
      }
    });
  }

  action20() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_20().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action21() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_21().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action22() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        const interval = setInterval(() => {
          localStorage.setItem(SessionKey.LAST_TIME_ACTION, new Date().valueOf().toString());
        }, 5000);
        this.isLoading = true;
        this.service.migrateAPI_22().subscribe(
          () => {
            clearInterval(interval);
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            clearInterval(interval);
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action23() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_23().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action24() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_24().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action25() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_25().subscribe(
          (fileId) => {
            if (!_.isEmpty(fileId)) {
              this.fileService
                .downloadFile(fileId, `Vai-tro-cho-nguoi-dung-co-khoi-khac-indiv.xlsx`)
                .pipe(catchError((e) => of(false)))
                .subscribe((res) => {
                  this.isLoading = false;
                  if (!res) {
                    this.confirmService.error('Thực hiện không thành công');
                  }
                });
            } else {
              this.confirmService.error('Thực hiện không thành công');
              this.isLoading = false;
            }
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action26(event, fileUpload) {
    if (this.isUpload) {
      return;
    }
    this.isLoading = true;
    const formData = new FormData();
    formData.append('file', event.files[0]);
    this.service.migrateAPI_26(formData).subscribe(
      (res) => {
        fileUpload.clear();
        this.isUpload = true;
        this.confirmService.success('Thực hiện thành công');
        this.isLoading = false;
      },
      () => {
        this.confirmService.error('Thực hiện không thành công');
        this.isLoading = false;
      }
    );
  }

  action27(event, fileUpload) {
    if (this.isUpload) {
      return;
    }
    this.isLoading = true;
    const formData = new FormData();
    formData.append('file', event.files[0]);
    this.service.migrateAPI_27(formData).subscribe(
      (res) => {
        fileUpload.clear();
        this.isUpload = true;
        this.confirmService.success('Thực hiện thành công');
        this.isLoading = false;
      },
      () => {
        this.confirmService.error('Thực hiện không thành công');
        this.isLoading = false;
      }
    );
  }

  action28() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_28().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action29(type: string) {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_29(type).subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action30(event, fileUpload) {
    if (this.isUpload) {
      return;
    }
    this.isLoading = true;
    const formData = new FormData();
    formData.append('file', event.files[0]);
    this.service.migrateAPI_30(formData).subscribe(
      (res) => {
        fileUpload.clear();
        this.isUpload = true;
        this.confirmService.success('Thực hiện thành công');
        this.isLoading = false;
      },
      () => {
        this.confirmService.error('Thực hiện không thành công');
        this.isLoading = false;
      }
    );
  }

  action31() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_31().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action32() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_32().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action33(event, fileUpload) {
    if (this.isUpload) {
      return;
    }
    this.isLoading = true;
    const formData = new FormData();
    formData.append('file', event.files[0]);
    this.service.migrateAPI_33(formData).subscribe(
      (res) => {
        fileUpload.clear();
        this.isUpload = true;
        this.confirmService.success('Thực hiện thành công');
        this.isLoading = false;
      },
      () => {
        this.confirmService.error('Thực hiện không thành công');
        this.isLoading = false;
      }
    );
  }

  action34() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_34().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action35() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_35().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action36() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_36().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action37(event, fileUpload) {
    if (this.isUpload) {
      return;
    }
    this.isLoading = true;
    const formData = new FormData();
    formData.append('file', event.files[0]);
    this.service.migrateAPI_37(formData).subscribe(
      (res) => {
        fileUpload.clear();
        this.isUpload = true;
        this.confirmService.success('Thực hiện thành công');
        this.isLoading = false;
      },
      (e) => {
        this.confirmService.error('Thực hiện không thành công');
        this.isLoading = false;
      }
    );
  }

  // action38() {
  //   this.confirmService.confirmInput().then((res) => {
  //     if (res.event) {
  //       this.isLoading = true;
  //       let etl = this.datepipe.transform(res.etlDate, 'yyyy-MM-dd');
  //       this.service.migrateAPI_38(res.messageId, etl).subscribe(
  //         () => {
  //           this.confirmService.success('Thực hiện thành công');
  //           this.isLoading = false;
  //         },
  //         () => {
  //           this.confirmService.error('Thực hiện không thành công');
  //           this.isLoading = false;
  //         }
  //       );
  //     }
  //   });
  // }

  action39() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_39().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action40(event, fileUpload) {
    if (this.isUpload) {
      return;
    }
    this.isLoading = true;
    const formData = new FormData();
    formData.append('file', event.files[0]);
    this.service.migrateAPI_40(formData).subscribe(
      (res) => {
        fileUpload.clear();
        this.isUpload = true;
        this.confirmService.success('Thực hiện thành công');
        this.isLoading = false;
      },
      (e) => {
        this.confirmService.error('Thực hiện không thành công');
        this.isLoading = false;
      }
    );
  }

  action41(type: string) {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_41(type).subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action42() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_42().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action43(migrateFull?: boolean) {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_43(String(migrateFull)).subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action44(migrateFull?: boolean) {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_44(String(migrateFull)).subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action45(migrateFull?: boolean) {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_45(String(migrateFull)).subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action46(migrateFull?: boolean) {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_46(String(migrateFull)).subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action47(migrateFull?: boolean) {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_47(String(migrateFull)).subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }

  action48() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_48().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }
  action49() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_49().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }
  action50() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.migrateAPI_50().subscribe(
          () => {
            this.confirmService.success('Thực hiện thành công');
            this.isLoading = false;
          },
          () => {
            this.confirmService.error('Thực hiện không thành công');
            this.isLoading = false;
          }
        );
      }
    });
  }
}
