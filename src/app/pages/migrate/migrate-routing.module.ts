import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGuardService } from 'src/app/core/services/role-guard.service';
import { FunctionCode } from 'src/app/core/utils/common-constants';
import { MigrateComponent } from './components/migrate.component';

const routes: Routes = [
  // {
  //   path: '',
  //   component: CalendarLeftViewComponent,
  //   outlet: 'app-left-content',
  // },
  {
    path: '',
    component: MigrateComponent,
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.MIGRATE,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MigrateRoutingModule {}
