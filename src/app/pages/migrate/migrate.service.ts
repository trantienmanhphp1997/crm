import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class MigrateService {
  constructor(private http: HttpClient) {}

  migrateAPI_1(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/import-rm/import`, data, { responseType: 'arraybuffer' });
  }
  migrateAPI_2(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/add-role`);
  }
  migrateAPI_3(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/user-role`);
  }
  migrateAPI_4(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer}/operation-division`);
  }
  migrateAPI_5(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/title-category`);
  }
  migrateAPI_6(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/assign-rm-block`);
  }
  migrateAPI_7(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/group-rm`);
  }
  migrateAPI_8(params?): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer}/assign-customer`, { params });
  }
  migrateAPI_9(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/permissions/createPermission`, data);
  }
  migrateAPI_10(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/permissions/addPolicy`, data);
  }
  migrateAPI_11(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/add-userpermission`);
  }
  migrateAPI_12(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_workflow}/processes/updateSaleProcess`);
  }

  migrateAPI_13(): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/employees/sync/syncRmCore`, {});
  }

  migrateAPI_15(): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_workflow}/lanes/sysProcess?type=LEAD&nameProcess=app_crm_lead_process`
    );
  }

  migrateAPI_16(): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_workflow}/lanes/sysProcess?type=OPPORTUNITY&nameProcess=app_crm_sale_process_case_1`
    );
  }

  migrateAPI_17(): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/customers/delete-auto-account-update-assignment`, {});
  }

  migrateAPI_18(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/customers/upload-assign-core`, data);
  }

  migrateAPI_20(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm_relationship}/fluctuations/migrateData`);
  }

  migrateAPI_21(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm_relationship}/fluctuations/migrateDivisionData`);
  }

  migrateAPI_22(): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/customers-assignment/migrate-assignment`, {});
  }

  migrateAPI_23(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm_relationship}/graph-relationship/migrate-employee-id`);
  }

  migrateAPI_24(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm_relationship}/fluctuations/migrate-title-id-level-id`);
  }

  migrateAPI_25(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/user-role/corp`, {
      responseType: 'text',
    });
  }

  migrateAPI_26(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/employees/import-emp-corp-migrate`, data);
  }

  migrateAPI_27(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/permissions/import-permissions-by-role`, data);
  }

  migrateAPI_28(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/eat-group-migrate?action=DELETE_RM_MIGRATE`);
  }

  migrateAPI_29(key: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/eat-group-migrate?action=${key}`);
  }

  migrateAPI_30(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/leads/import-lead-migrate`, data, {
      responseType: 'text',
    });
  }

  migrateAPI_31(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer}/customers-assignment/corp-migrate?action=IMPORT_TEMP`);
  }

  migrateAPI_32(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer}/customers-assignment/corp-migrate?action=INSERT`);
  }

  migrateAPI_33(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/opportunities_onboarding/import-data-migrate`, data);
  }

  migrateAPI_34(): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm_relationship}/fluctuations/employee-change-migrate`, {});
  }

  migrateAPI_35(): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_sale}/opportunities_onboarding/delete-data-migrate`);
  }

  migrateAPI_36(): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/customers-assignment/migrate-unassignment`, {});
  }

  migrateAPI_37(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sale-management/unassign-file`, data, {
      responseType: 'text',
    });
  }

  migrateAPI_38(messageId, etlDate): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_customer}/customers/retry-synchronized-customer-t24?messageId=${messageId}&etlDate=${etlDate}`
    );
  }

  migrateAPI_39(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/eat-group-migrate-cib`);
  }

  migrateAPI_40(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/customers-assignment/migrate/CIB`, data, {
      responseType: 'text',
    });
  }

  migrateAPI_41(key: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/eat-group-migrate?action=${key}`);
  }

  migrateAPI_42(): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/account-plan/migrate-ap-warning`, {});
  }

  migrateAPI_43(migrateFull: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/migrate/leads?migrateFull=${migrateFull}`);
  }

  migrateAPI_44(migrateFull: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/migrate/campaign?migrateFull=${migrateFull}`);
  }

  migrateAPI_45(migrateFull: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/migrate/lead-preview?migrateFull=${migrateFull}`);
  }

  migrateAPI_46(migrateFull: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/migrate/lead-preview-tmp?migrateFull=${migrateFull}`);
  }

  migrateAPI_47(migrateFull: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/migrate/opportunities?migrateFull=${migrateFull}`);
  }

  migrateAPI_48(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/roles/add-role-client-from-policy`);
  }
  migrateAPI_49(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/update-client-role-user`);
  }
  migrateAPI_50(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/employees/delete-realm-role-user`);
  }
}
