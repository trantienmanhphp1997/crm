import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MigrateRoutingModule } from './migrate-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FileUploadModule } from 'primeng/fileupload';
import { MigrateComponent } from './components/migrate.component';
import { InputTextModule } from 'primeng/inputtext';
import { TranslateModule } from '@ngx-translate/core';
import { CalendarModule } from 'primeng/calendar';

@NgModule({
  declarations: [MigrateComponent],
  imports: [
    CommonModule,
    TranslateModule,
    ReactiveFormsModule,
    FormsModule,
    MigrateRoutingModule,
    FileUploadModule,
    SharedModule,
    InputTextModule,
    CalendarModule,
  ],
  providers: [DatePipe],
})
export class MigrateModule {}
