import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';
import { Observable, Subject } from 'rxjs';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root',
})
export class KpiReportApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_kpi}/kpi/report`);
  }

  loadFile(href: string, type?: string) {
    const url = `${environment.url_endpoint_rm}/files`;
    return this.http
      .get(url + `/${href}`, {
        responseType: 'arraybuffer',
        observe: 'response',
      })
      .toPromise()
      .then((response: HttpResponse<ArrayBuffer>) => {
        return new Blob([response.body], { type: type || 'application/octet-stream' });
      });
  }

  reportTOI(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/ors-report/toi`, params, { responseType: 'text' });
  }

  reportPL(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/ors-report/pl`, params, { responseType: 'text' });
  }

  reportMobilization(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/ors-report/hdv`, params, { responseType: 'text' });
  }

  reportCredit(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/ors-report/td`, params, { responseType: 'text' });
  }

  getTableauReport(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_report}/report/getTableauReport`, params, { responseType: 'text' });
  }

  reportCustomerDevelop(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/sales-report/ptkh`, params, { responseType: 'text' });
  }

  reportAccessLog(fileId: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_activity}/bpm-report-access-logs?requestId=${fileId}`, {
      headers: { 'Content-Type': 'application/json' },
    });
  }

  reportMapCustomer(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_report}/dashboard/bdkh`, params, { responseType: 'text' });
  }

  checkIdReportSuccess(fileId: string, respondSource: Subject<any>) {
    const timeDelay = 2500;
    let countInterval = 0;
    const interval = setInterval(() => {
      countInterval++;
      this.reportAccessLog(fileId).subscribe((res) => {
        if (!_.isEmpty(res.error)) {
          respondSource.next({ error: res.error });
          clearInterval(interval);
        } else if (!_.isEmpty(res.requestId)) {
          respondSource.next({ fileId: res.requestId });
          clearInterval(interval);
        }
      });
      if (countInterval > 144) {
        respondSource.next({ error: 'error' });
        clearInterval(interval);
      }
    }, timeDelay);
  }

  reportNsldSmeCib(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/ors-report/kpi-sme-cib`, params, { responseType: 'text' });
  }

  reportManagementTTTM(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/ors-report/admin-tttm`, params, { responseType: 'text' });
  }

  reportGuaranteeMoment(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_report}/dashboard/baolanh`, params, { responseType: 'text' });
  }

  reportGuaranteeAvagate(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/ors-report/guarantee`, params, { responseType: 'text' });
  }

  reportAp(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/ors-report/ap`, params, { responseType: 'text' });
  }

  reportS3(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_report}/report/getS3Report`, params);
  }
  reportExportS3(params): Observable<any>{
    return this.http.post(`${environment.url_endpoint_report}/report/export-excel-kpi-rm-indiv`, params);
  }
  reportGetListFileExport(params) : Observable<any>{
    return this.http.get(`${environment.url_endpoint_report}/report/get-file-export-async`,{params});
  }
  reportUpdateStatusFile(params) : Observable<any>{
    return this.http.put(`${environment.url_endpoint_report}/report/export-excel-async`, params);
  }

  exportCustomerReport(params) : Observable<any>{
    console.log("starting export report");
    return this.http.post(`${environment.url_endpoint_report}/report/export-kpi-report-file`, params, { responseType: 'blob' });
  }

  getMaxDateDKI() : Observable<any>{
    console.log("starting export report");
    return this.http.get(`${environment.url_endpoint_report}/report/get-max-date-kpi`);
  }

  searchKpiReport(params): Observable<any>{
    return this.http.post(`${environment.url_endpoint_report}/report/search-kpi-report`, params);
  }
}
export class KpiCustomerDTO {
  STT?: number;
  NGAY: string;
  RM_BANCHINH: string;
  RM_BANCHEO: string;
  HESO_BANCHEO: string;
  HESO_CUMOI: string;
  CODE_KH: string;
  TEN_KH: string;
  SP1: string;
  SP2: string;
  SP3: string;
  DOANHSO: number;
  DOANHSO_TT: number;
  DONVI: string;
  CCY: string;
  GROUP_ID: string;
}
