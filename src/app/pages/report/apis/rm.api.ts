import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';
import { Observable, of } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class RmApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_rm}/employees`);
  }

  checkRmAssign(hrsCode): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_rm}/employees/check-not-allowed-to-choose-to-assign-rm/${hrsCode}`
    );
  }

  checkRmNTSNotAssign(hrsCode): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm_relationship}/fluctuations/checkRmNTSNotAssign/${hrsCode}`);
  }

  getRmAssignOrFollow(isAssignee): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/getAssigneeToFollower/${isAssignee}`);
  }

  createFileExcel(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/employees/exportsRm`, params, { responseType: 'text' });
  }

  dowloadTemplate(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/template`, { responseType: "text" });
  }

  importFile(param): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/employees/import`, param);
  }

  checkImportSuccess(fileId): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/checkProcessImport?fileId=${fileId}`);
  }

  searchDataError(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/findDataError`, { params })
  }

  searchDataSuccess(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/findDataSuccess`, { params });
  }

}
