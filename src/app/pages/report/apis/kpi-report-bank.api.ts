import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';
import { catchError, delay, retryWhen, take } from 'rxjs/operators';
import { Observable, of, Subject } from 'rxjs';
import * as _ from 'lodash';

@Injectable()
export class KpiReportBankApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_kpi}/kpi/report-nhs`);
  }
  loadFile(href: string, type?: string) {
    let url = `${environment.url_endpoint_rm}/files`;
    return this.http
      .get(url + `/${href}`, {
        responseType: 'arraybuffer',
        observe: 'response',
      })
      .toPromise()
      .then((response: HttpResponse<ArrayBuffer>) => {
        return new Blob([response.body], { type: type || 'application/octet-stream' });
      });
  }

  reportAccessLog(fileId: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_activity}/bpm-report-access-logs?requestId=${fileId}`, {
      headers: { 'Content-Type': 'application/json' },
    });
  }

  checkIdReportSuccess(fileId: string, respondSource: Subject<any>) {
    const timeDelay = 2500;
    let countInterval = 0;
    const interval = setInterval(() => {
      countInterval++;
      this.reportAccessLog(fileId).subscribe((res) => {
        if (!_.isEmpty(res.error)) {
          respondSource.next({ error: res.error });
          clearInterval(interval);
        } else if (!_.isEmpty(res.requestId)) {
          respondSource.next({ fileId: res.requestId });
          clearInterval(interval);
        }
      });
      if (countInterval > 144) {
        respondSource.next({ error: 'error' });
        clearInterval(interval);
      }
    }, timeDelay);
  }
}
