import { KpiReportApi } from './kpi-report.api';
import { CategoryApi } from './category.api';
import { KpiProductApi } from './kpi-product.api';
import { KpiReportBankApi } from './kpi-report-bank.api';
import { RmApi } from './rm.api';
import { BlockApi } from './block.api';

export { KpiReportApi } from './kpi-report.api';
export { CategoryApi } from './category.api';
export { KpiProductApi } from './kpi-product.api';
export { KpiReportBankApi } from './kpi-report-bank.api';
export { RmApi } from './rm.api';
export { BlockApi } from './block.api';

export const APIS = [
  KpiReportApi,
  CategoryApi,
  KpiProductApi,
  KpiReportBankApi,
  RmApi,
  BlockApi
];