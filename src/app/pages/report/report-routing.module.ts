import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGuardService } from 'src/app/core/services/role-guard.service';
import { FunctionCode, Scopes, ScreenType } from 'src/app/core/utils/common-constants';
import {
  KpiIndivReportComponent,
  BlockBankListComponent,
  KpiPlReportComponent,
  ReportMobilizationComponent,
  ReportCreditComponent,
  ReportCustomerDevelopComponent,
  ReportToiViewComponent,
  ReportMapCustomerComponent,
  ReportNsldSmeComponent,
  ReportCustomerManagementTTTMComponent,
  ReportGuaranteeComponent,
  ReportAPComponent,
  // ReportBusinessBranchComponent,
  QueryMbalComponent,
  QueryMbalDetailComponent,
  ReportCreditV2Component
} from './components';
import { BizCustomerComponent } from './components/biz-customer/biz-customer.component';
import { ReportMobilizationV2Component } from './components/report-mobilization-v2/report-mobilization.component';
import { ReportCustomerSaleComponent } from './components/report-customer-sales/report-customer-sales.component';
import { ReportGuaranteeV2 } from './components/report-guarantee-v2/app-report-guarantee.component';
import { ReportAppComponent } from "./components/report-app/report-app.component";
import { ReportCustomerManagementTttmV2 } from './components/report-customer-management-tttm-v2/report-customer-management-tttm-v2';

import { ReportGapComponent } from './components/report-gap/report-gap.component';
import { ReportCustomerInactiveComponent } from './components/customer-inactive/customer-inactive.component';
import { ReportToiV2Component } from './components/report-toi-v2/report-toi-v2.component';
import { ReportPLComponent } from './components/report-pl/report-pl.component';
import { ReportMapCustomerV2Component } from './components/report-map-customer-v2/report-map-customer-v2.component';
import { ReportRatingComponent } from './components/report-rating/report-rating.component';
const routes: Routes = [
  {
    path: 'kpi-indiv-report',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_KPI_INDIV,
    },
    component: KpiIndivReportComponent,
  },
  {
    path: 'block-bank-report',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_KPI_NHS,
    },
    component: BlockBankListComponent,
  },
  {
    path: 'kpi-pl-report',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_KPI_PL,
    },
    component: KpiPlReportComponent,
  },
  {
    path: 'report-toi-v2',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_TOI_V2,
    },
    component: ReportToiV2Component,
  },
  {
    path: 'report-toi',
    canActivate: [RoleGuardService],
    data: {
    code: FunctionCode.TOI_REPORT,
    },
    component: ReportToiViewComponent,
    },
  {
    path: 'report-mobilization',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_MOBILIZATION,
    },
    component: ReportMobilizationComponent,
  },
  {
    path: 'report-mobilization-v2',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_MOBILIZATION_V2,
    },
    component: ReportMobilizationV2Component,
  },
  {
    path: 'report-credit',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_CREDIT,
    },
    component: ReportCreditComponent,
  },
  {
    path: 'report-credit-v2',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_CREDIT_V2,
    },
    component: ReportCreditV2Component,
  },
  {
    path: 'report-customer-develop',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_PT_KH,
    },
    component: ReportCustomerDevelopComponent,
  },
  {
    path: 'map-customer',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_MAP_KH,
    },
    component: ReportMapCustomerComponent,
  },
  {
    path: 'map-customer-v2',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_MAP_KH_V2,
    },
    component: ReportMapCustomerV2Component,
  },
  {
    path: 'nsld-sme',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_NSLD_SME,
    },
    component: ReportNsldSmeComponent,
  },
  {
    path: 'delete-gap',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_DELETE_GAP,
    },
    component: ReportGapComponent,
  },
  {
    path: 'customer-management-tttm',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_TTTM,
    },
    component: ReportCustomerManagementTTTMComponent,
  },
  {
    path: 'customer-management-tttm-v2',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_TTTM_V2,
    },
    component: ReportCustomerManagementTttmV2,
  },
  {
    path: 'report-guarantee',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_BAOLANH,
    },
    component: ReportGuaranteeComponent,
  },
  // {
  //   path: 'report-business-branch',
  //   canActivate: [RoleGuardService],
  //   data: {
  //     code: FunctionCode.REPORT_BUSINESS_BRANCH,
  //     scope: Scopes.VIEW,
  //   },
  //   component: ReportBusinessBranchComponent,
  // },
  {
    path: 'query-mbal',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.QUERY_MBAL,
    },
    children: [
      {
        path: '',
        component: QueryMbalComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
      {
        path: 'detail',
        component: QueryMbalDetailComponent,
        data: {
          type: ScreenType.Detail,
        },
      },
    ],
  },
  {
    path: 'report-ap',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_AP,
    },
    component: ReportAPComponent,
  },
  {
    path: 'query-mbal',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.QUERY_MBAL,
    },
    children: [
      {
        path: '',
        component: QueryMbalComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
      {
        path: 'detail',
        component: QueryMbalDetailComponent,
        data: {
          type: ScreenType.Detail,
        },
      },
    ],
  },
  {
    path: 'biz-customer',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_BIZ_CUSTOMER,
    },
    component: BizCustomerComponent,
  },
  {
    path: 'report-customer-sales',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_CUSTOMER_SALES,
    },
    component: ReportCustomerSaleComponent,
  },
  {
    path: 'report-guarantee-v2',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_GUARANTEE_V2,
    },
    component: ReportGuaranteeV2,
  },
  {
    path: 'report-app',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_APP,
    },
    component: ReportAppComponent,
  },
  {
    path: 'report-customer-inactive',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_CUSTOMER_INACTIVE,
    },
    component: ReportCustomerInactiveComponent,
  },
  {
    path: 'report-pl',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.REPORT_PL,
    },
    component: ReportPLComponent,
  },
  {
    path: 'report-rating',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.RATING_FUNCTION_REPORT,
    },
    component: ReportRatingComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportRoutingModule { }
