import { MatTabsModule } from '@angular/material/tabs';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportRoutingModule } from './report-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { TranslateModule } from '@ngx-translate/core';
import { APIS } from './apis';
import { COMPONENTS, KpiReportBranchTableComponent } from './components';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxSelectModule } from 'ngx-select-ex';
import { RmModule } from '../rm/rm.module';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { NgxEchartsModule } from 'ngx-echarts';
import * as echarts from 'echarts';
import { GridsterModule } from 'angular-gridster2';
import { DynamicModule } from 'ng-dynamic-component';
import { AccordionModule } from 'primeng/accordion';
import { TabViewModule } from 'primeng/tabview';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { RadioButtonModule } from 'primeng/radiobutton';
import { KpiReportModalComponent } from './dialogs';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { CheckboxModule } from 'primeng/checkbox';
import { MultiSelectModule } from 'primeng/multiselect';
import { InputNumberModule } from 'primeng/inputnumber';
import { ReportGapComponent } from './components/report-gap/report-gap.component';
import { ReportMapCustomerV2Component } from './components/report-map-customer-v2/report-map-customer-v2.component';
import {KpiIndivReportListComponent} from "./dialogs/kpi-indiv-report-list/kpi-indiv-report-list.component";
import { ReportRatingComponent } from './components/report-rating/report-rating.component';

const DIALOGS = [KpiReportModalComponent,KpiIndivReportListComponent];

@NgModule({
  declarations: [...COMPONENTS, ...DIALOGS, ReportGapComponent, ReportMapCustomerV2Component, ReportRatingComponent],
  imports: [
    CommonModule,
    SharedModule,
    NgbModule,
    DragDropModule,
    ReactiveFormsModule,
    FormsModule,
    InfiniteScrollModule,
    TranslateModule,
    ReportRoutingModule,
    NgxDatatableModule,
    NgxSelectModule,
    RmModule,
    MatProgressBarModule,
    MatTabsModule,
    NgxEchartsModule.forRoot({
      echarts,
    }),
    GridsterModule,
    DynamicModule,
    AccordionModule,
    TabViewModule,
    ButtonModule,
    InputTextModule,
    DropdownModule,
    CalendarModule,
    RadioButtonModule,
    NgxExtendedPdfViewerModule,
    CheckboxModule,
    MultiSelectModule,
    InputNumberModule,
  ],
  providers: [...APIS],
  exports: [KpiReportModalComponent, KpiReportBranchTableComponent, KpiIndivReportListComponent],
  entryComponents: [...COMPONENTS, ...DIALOGS],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class ReportModule {}
