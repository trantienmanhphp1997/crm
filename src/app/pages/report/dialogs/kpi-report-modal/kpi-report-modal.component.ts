import { Component, HostBinding, Injector, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import _ from 'lodash';
import { pdfDefaultOptions } from 'ngx-extended-pdf-viewer';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { saveAs } from 'file-saver';
import { catchError, delay, retryWhen, take } from 'rxjs/operators';
import { of, Subject } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { NotifyMessageService } from '../../../../core/components/notify-message/notify-message.service';
import { Utils } from 'src/app/core/utils/utils';
import { BaseComponent } from 'src/app/core/components/base.component';
import { KpiReportApi } from '../../apis';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-kpi-report-modal',
  templateUrl: './kpi-report-modal.component.html',
  styleUrls: ['./kpi-report-modal.component.scss'],
})
export class KpiReportModalComponent extends BaseComponent implements OnInit {

  @HostBinding('class.app-kpi-report-modal') reportModal = true;
  constructor(
    injector: Injector,
    private modal: NgbActiveModal,
    private http: HttpClient,
    private kpiReportApi: KpiReportApi,
    private sanitizer: DomSanitizer
  ) {
    super(injector);
    pdfDefaultOptions.assetsFolder = 'bleeding-edge';
  }

  @Input() data: any;
  @Input() model: any;
  @Input() title = 'Báo cáo KPIs KHCN';
  @Input() baseUrl: any;
  @Input() extendUrl = '';
  @Input() filename = 'bao-cao-kpi';
  @Input() excelData: any;
  @Input() excelType: string;
  url: string;
  isLoading: boolean;
  urlMain: string;
  @Input() embeddedReport: string;
  @Input() reportType: number;
  @Input() s3ReportData: string[];
  safeEmbeddedReport: SafeUrl;

  ngOnInit(): void {
    if (this.reportType == 1){
      // Tableau report
      this.safeEmbeddedReport = this.sanitizer.bypassSecurityTrustResourceUrl(this.embeddedReport);
    }else if (this.reportType == 2){
      // S3 report
    }else{
      // default
      this.reportType = 0;
      this.url = URL.createObjectURL(this.data);
      if (_.isEmpty(this.extendUrl)) {
        this.urlMain = `${this.baseUrl}`;
      } else {
        this.urlMain = `${this.baseUrl}/${this.extendUrl}`;
      }
    }
  }

  saveFile(type: string = 'pdf') {
    this.isLoading = true;
    if (type === 'pdf' && Utils.isNotNull(this.data)) {
      saveAs(this.data, `${this.filename}.${type}`);
      this.isLoading = false;
      return;
    }
    
    // change excel type
    let attributeFormat = type;
    if (type === 'xlsx' && this.excelType === 'xls') {
      attributeFormat = '';
      type  = 'xls';
    }

    _.set(this.model, 'attributeFormat', attributeFormat);
    this.http
      .post(this.urlMain, this.model, {
        responseType: 'text',
      })
      .toPromise()
      .then(
        (response: string) => {
          const respondSource = new Subject<any>();
          this.kpiReportApi.checkIdReportSuccess(response, respondSource);
          respondSource.asObservable().subscribe((res) => {
            if (res?.error === 'error') {
              this.messageService.error(this.notificationMessage.E001);
            } else if (res?.fileId) {
              this.loadFile(res?.fileId, type);
            } else {
              this.messageService.error(this.notificationMessage.messageOrsReport);
            }
            this.isLoading = false;
            respondSource.unsubscribe();
          });
        },
        () => {
          this.isLoading = false;
        }
      );
  }

  loadFile(fileId: string, type: string = 'pdf') {
    const url = `${environment.url_endpoint_rm}/files/download/${fileId}`;
    this.http
      .get(`${url}`, {
        responseType: 'arraybuffer',
        observe: 'response',
      })
      .toPromise()
      .then(
        (response: HttpResponse<ArrayBuffer>) => {
          if (response) {
            let blob = new Blob([response.body], { type: type || 'application/octet-stream' });
            saveAs(blob, `${this.filename}.${type}`);
          }
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
  }

  close() {
    this.modal.close();
  }
}
