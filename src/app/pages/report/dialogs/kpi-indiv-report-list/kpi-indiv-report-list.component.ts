import {BaseComponent} from '../../../../core/components/base.component';
import {Component, Injector, OnInit, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Pageable} from '../../../../core/interfaces/pageable.interface';
import {KpiReportApi} from '../../apis';
import _ from 'lodash';
import {global} from "@angular/compiler/src/util";
import {Utils} from "../../../../core/utils/utils";
import {ExportExcelService} from "../../../../core/services/export-excel.service";
import {KpiCustomerDTO} from "../../apis/kpi-report.api";

@Component({
  selector: 'app-kpi-indiv-report-list',
  templateUrl: 'kpi-indiv-report-list.component.html',
  styleUrls: ['kpi-indiv-report-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class KpiIndivReportListComponent extends BaseComponent implements OnInit{
  pageable: Pageable;
  listData: KpiCustomerDTO[];
  fromDate = '';
  toDate = '';
  listCurrentData = [];
  listExportExcel = [];
  limit = global.userConfig.pageSize;
  constructor(injector: Injector,
              private modalActive: NgbActiveModal,
              private api: ExportExcelService,
              ) {
    super(injector);
  }


  ngOnInit(): void {
    console.log('fromDate: ', this.fromDate);
    console.log('toDate: ', this.toDate);
    _.forEach(this.listData, (item, index) => {
      const data = {
        STT: index + 1,
        NGAY: item.NGAY,
        RM_BANCHINH: item.RM_BANCHINH,
        RM_BANCHEO: item.RM_BANCHEO,
        HESO_BANCHEO: Number(item.HESO_BANCHEO),
        HESO_CUMOI: Number(item.HESO_CUMOI),
        CODE_KH: item.CODE_KH,
        TEN_KH: item.TEN_KH,
        SP1: item.SP1,
        SP2: item.SP2,
        SP3: item.SP3,
        DOANHSO: Number(item.DOANHSO.toFixed(4)),
        DOANHSO_TT: Number(item.DOANHSO_TT.toFixed(4)),
        DONVI: item.DONVI,
        CCY: item.CCY
      }
      this.listExportExcel.push(data);
      item.DOANHSO =  Utils.numberWithCommasAndRound(item.DOANHSO,4);
      item.DOANHSO_TT = Utils.numberWithCommasAndRound(item.DOANHSO_TT,4);
    });
    console.log('data: ', this.listData);
    this.loadPage(0,10);

  }
  setPage(page){
    this.loadPage(page.offset,this.limit);
  }
  loadPage(page, limit){
    const totalElement = this.listData.length;
    this.pageable = {
      totalElements: totalElement || 0,
      totalPages: totalElement/limit,
      currentPage: page,
      size: limit
    }
    const start = page*limit;
    const end = Math.min(page*limit + limit,totalElement);
    this.listCurrentData = this.listData.slice(start,end);
  }
  getValue(row, key) {
    return _.get(row, key);
  }
  closeModal(){
    this.modalActive.close();
  }
  handleChangePageSize(event){
    console.log('limit: ', this.limit);
    this.limit = event;
    this.loadPage(0, this.limit);
  }
  export(){
    this.api.exportExcelKpi(this.listExportExcel,this.fromDate, this.toDate,'bao-cao-kpi-chi tiet-theo-KH',true);
  }
}
