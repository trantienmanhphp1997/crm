import { Component, HostBinding, OnInit, ChangeDetectorRef, Injector } from '@angular/core';
import { CommonCategory, Division, FunctionCode, maxInt32, Scopes, TaskType } from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import _ from 'lodash';
import { AppFunction } from 'src/app/core/interfaces/app-function.interface';
import { SessionService } from 'src/app/core/services/session.service';
import { KpiReportApi, CategoryApi, KpiProductApi } from '../../apis';
import { KpiReportModalComponent } from '../../dialogs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { catchError, delay, finalize, retryWhen, take, tap } from 'rxjs/operators';
import { NotifyMessageService } from '../../../../core/components/notify-message/notify-message.service';
import { RmApi } from '../../../rm/apis';
import { Utils } from 'src/app/core/utils/utils';
import * as moment from 'moment';
import { SessionKey } from 'src/app/core/utils/common-constants';
import { BaseComponent } from 'src/app/core/components/base.component';
import { KpiIndivReportListComponent } from '../../dialogs/kpi-indiv-report-list/kpi-indiv-report-list.component';
import { formatDate } from '@angular/common';
import { Pageable } from '../../../../core/interfaces/pageable.interface';
import { global } from '@angular/compiler/src/util';
import { TableColumn } from '@swimlane/ngx-datatable';
import { CustomerApi } from '../../../customer-360/apis';
import { CustomerModalComponent } from '../../../customer-360/components';
import {KpiCustomerDTO} from "../../apis/kpi-report.api";

@Component({
  selector: 'app-kpi-indiv-report',
  templateUrl: './kpi-indiv-report.component.html',
  styleUrls: ['./kpi-indiv-report.component.scss']
})
export class KpiIndivReportComponent extends BaseComponent implements OnInit {
  REPORT_CODE_RM_CBQL = 'KPI04';
  NON_TERM_CAPITAL_FUNDING = 'NON_TERM_CAPITAL_FUNDING';

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private api: KpiReportApi,
    private rmApi: RmApi,
    private categoryApi: CategoryApi,
    private productApi: KpiProductApi,
    private customerApi: CustomerApi
  ) {
    super(injector);
    this.block = 'INDIV - Khách hàng cá nhân';

  }
  productTypeOpt = [
    {
      code: 'II1',
      name: 'Huy động vốn không kỳ hạn'
    },
    {
      code: 'OTHER',
      name: 'Các sản phẩm khác'
    }
    ];
  isLoading: boolean;
  blocks: Array<any>;
  model: any = {
    kpiReportType: 'CN',
    kpiDetailReportType: 'TH',
    productType: 'II1',
    minDate: null,
    maxDate: new Date(),
    currentMonth: ''
  };
  reportTypes: Array<any>;
  reportDetails: Array<any>;
  products: Array<any>;
  productList: Array<any>;
  branches: Array<any>;
  obj: AppFunction;
  branchCodes: Array<any>;
  block: string;
  rms: Array<any>;
  managers: Array<any>;
  productCode = '';
  message: string;
  rmCodes: Array<any> = [];
  managerCodes: Array<any> = [];
  date: Date;
  profile: any;
  rmCode: string;
  maxDateDefault = new Date();
  maxRm = 50;
  rsId = '';
  maxDate = '';
  capitalFundingCode : any;
  prevEvent = [];

  columns: TableColumn[];
  listDataTableCustomer = [];
  termData = [];
  pageableCustomer: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize
  };
  paramsTableCustomer = {
    page: 0,
    size: global?.userConfig?.pageSize
  };
  allRM = false;
  textSearch: string;
  pageable: Pageable;
  listData: KpiCustomerDTO[];
  reportCodeS3 = 'KPI04_MONTHLY_REPORT_RM';
  intervalDownloadFileS3 = null;
  limit = {
    dayReport: 90,
    monthReport: -4
  }
  @HostBinding('class.app__right-content') appRightContent = true;

  ngOnInit(): void {
    this.isLoading = true;
    this.model.maxDate = moment().add(-1, 'day').toDate();
    this.model.minDate = moment().add(-1,'day').add(this.limit.monthReport, 'month').toDate();
    this.model.currentMonth = this.model.maxDate;
    this.obj = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.profile = this.sessionService.getSessionData(SessionKey.USER_INFO);
    this.rmCode = _.get(this.profile, 'code');
    _.set(this.model, 'rsId', _.get(this.obj, 'rsId'));
    _.set(this.model, 'scope', 'VIEW');

    this.rsId = _.get(this.obj, 'rsId');

    forkJoin([
      this.categoryService.getBranchesOfUser(this.obj?.rsId, Scopes.VIEW).pipe(catchError((e) => of(undefined))),
      this.commonService.getCommonCategoryByListCode([CommonCategory.EXPORT_KPI_KHCN_LIMIT]).pipe(catchError((e) => of(undefined))),
      this.productApi.get('indiv').pipe(catchError((e) => of(undefined))),
    ]).subscribe(([branches, limitRm, _productList]) => {
      this.maxRm = limitRm[0]?.value || 50;
      this.branches = branches;
      if (Utils.isArrayNotEmpty(_productList)) {
        _.remove(_productList, (i) => i.groupId === 'II1');
        const prds = _.map(_productList, (x) => ({ ...x, value_to_search: `${x.groupId} - ${x.groupName}` }));
        this.productList = [
          ...prds,
        ];
        if (this.productList.length > 0) {
          this.productList.splice(0,0,{
            groupId: '',
            value_to_search: this.fields.all
          });
          this.productCode = this.productList[0].groupId;
        }
      }


      this.isLoading = false;
    });

    this.reportTypes = [
      {
        code: 'CN',
        name: 'Chi nhánh'
      },
      {
        code: 'RM',
        name: 'RM'
      },
      {
        code: 'CBQL',
        name: 'CBQL'
      }
    ];
    this.reportDetails = [
      {
        code: 'TH',
        name: 'Tổng hợp'
      }
    ];
    this.init_date();

    this.columns = [
      {
        name: this.fields.customerCode,
        prop: 'code'
      },
      {
        name: this.fields.customerName,
        prop: 'name'
      }
    ];

  }

  convertProductStringToList(productString: string): string[] {
    return productString.split(',').map((product) => product.trim());
  }

  init_date() {
    this.date = moment().add('day', -1).toDate();
    _.set(this.model, 'startDate', new Date(moment(this.date).format('YYYY-MM-DD')));
    _.set(this.model, 'endDate', new Date(moment(this.date).format('YYYY-MM-DD')));
  }

  reportTypeChange($event) {
    this.model.kpiDetailReportType = null;
    switch ($event) {
      case 'CN':
        this.reportDetails = [
          {
            code: 'TH',
            name: 'Tổng hợp'
          }
        ];
        _.set(this.model, 'kpiDetailReportType', 'TH');
        const branches = _.filter(this.branchCodes, (x) => Utils.isStringNotEmpty(x.code));
        _.set(
          this.model,
          'branchCodes',
          _.map(branches, (x) => x.code)
        );
        _.set(this.model, 'rmCodes', null);
        break;
      case 'CBQL':
        this.reportDetails = [
          {
            code: 'RM',
            name: 'RM'
          },
          {
            code: 'KH',
            name: 'Khách hàng (Tự doanh)'
          }
        ];
        _.set(this.model, 'kpiDetailReportType', 'RM');
        const managerCodes = _.filter(this.managerCodes, (x) => Utils.isStringNotEmpty(x.rmCode));
        _.set(
          this.model,
          'rmCodes',
          _.map(managerCodes, (x) => x.rmCode)
        );
        _.set(this.model, 'branchCodes', null);
        break;
      case 'RM':
        this.reportDetails = [
          {
            code: 'TH',
            name: 'Tổng hợp'
          },
          {
            code: 'KH',
            name: 'Khách hàng'
          }
        ];
        _.set(this.model, 'kpiDetailReportType', 'KH');
        const rmCodes = _.filter(this.rmCodes, (x) => Utils.isStringNotEmpty(x.rmCode));
        _.set(
          this.model,
          'rmCodes',
          _.map(rmCodes, (x) => x.rmCode)
        );
        _.set(this.model, 'branchCodes', null);
        break;
    }
  }

  validation() {
    if (_.get(this.model, 'kpiDetailReportType') === 'KH') {
      if (Utils.isStringEmpty(this.productCode) && _.size(this.rmCodes) > 1) {
        this.messageService.error('Vui lòng chọn tối đa 1 RM nếu chọn tất cả sản phẩm');
        return false;
      }
    }
    return true;
  }

  showSideBar() {
    const timer = setTimeout(() => {
    }, 300);
  }

  view() {
    if(this.model.kpiDetailReportType === 'KH'
      && (this.model.kpiReportType === 'CBQL' || this.model.kpiReportType === 'RM')
      &&  (this.model.productType === 'II1' ||  this.productCode !== '')){
      const validView = this.validateReport();
      if(!validView){
        this.isLoading = false;
        return;
      }
      this.viewReportCustomer();
      return;
    }
    this.isLoading = true;
    _.set(this.model, 'attributeFormat', 'pdf');
    if (Utils.isStringNotEmpty(this.productCode)) {
      _.set(this.model, 'productCodes', [`${this.productCode}`]);
    } else {
      _.set(this.model, 'productCodes', []);
    }
    const valid = this.validate(_.get(this.model, 'kpiReportType'));
    if (!valid) {
      // this.messageService.error(this.message);
      this.isLoading = false;
      return;
    }


    this.model.startDate = new Date(moment(_.get(this.model, 'startDate')).format('YYYY-MM-DD'));
    this.model.endDate = new Date(moment(_.get(this.model, 'endDate')).format('YYYY-MM-DD'));
    const url = _.get(this.api, 'baseURL');
    this.api.postFile('', this.model).subscribe(
      (response: any) => {
        const respondSource = new Subject<any>();
        this.api.checkIdReportSuccess(response, respondSource);
        respondSource.asObservable().subscribe((res) => {
          if (res?.error === 'error') {
            this.messageService.error(this.notificationMessage.E001);
          } else if (res?.fileId) {
            this.loadFile(res?.fileId, url);
          } else {
            this.messageService.error(this.notificationMessage.messageOrsReport);
          }
          this.isLoading = false;
          respondSource.unsubscribe();
        });
      },
      () => {
        this.messageService.error('Có lỗi xảy ra khi tạo yêu cầu xem báo cáo');
        this.isLoading = false;
      }
    );
  }
  validateReport() {
    const valid = this.validate(_.get(this.model, 'kpiReportType'));
    if (!valid) {
      return false;
    }
    let listRmCode = '';
    let listTitle = this.rmCodes;

    if (_.get(this.model, 'kpiReportType') === 'CBQL') {
      listTitle = this.managerCodes;
    }

    listTitle.map((x) => {
      // listRmCode += x.rmCode + ',';
      if (!Utils.isStringNotEmpty(listRmCode)) {
        listRmCode += x.rmCode;
      } else {
        listRmCode += ',' + x.rmCode;
      }
    });
    if (this.model.kpiDetailReportType === 'KH') {
      //  const isFundingSelected = this.productCode === this.capitalFundingCode.value;
      const isFundingSelected = this.model.productType === 'II1';
      if (isFundingSelected && !this.validateCustomerCode()) {
        return false;
      }
    }
    return true;
  }
  viewReportCustomer(){

    const params =  {
      DATE_FROM: formatDate(this.model.startDate, 'yyyyMMdd', 'en'),
      DATE_TO: formatDate(this.model.endDate, 'yyyyMMdd', 'en'),
      RM_CODE: this.model.kpiReportType === 'CBQL' ? this.managerCodes[0].rmCode : this.rmCodes[0].rmCode,
      GROUP_ID: [this.model.productType === 'II1' ? 'II1' : this.productCode],
      RS_ID: this.rsId,
      CODE_KH: this.model.productType === 'II1' ? this.termData[0].customerCode : null
    };
    this.isLoading = true;
    this.api.searchKpiReport(params).pipe().subscribe((data: KpiCustomerDTO[]) => {
      this.listData = data;
      this.isLoading = false;
      if(_.isEmpty(this.listData)){
        this.messageService.warn(_.get(this.notificationMessage, 'report_empty'));
        return;
      }
      const modal = this.modalService.open(KpiIndivReportListComponent, { windowClass: 'kpi-report-list-modal' });
      const fromDate = formatDate(this.model.startDate, 'dd/MM/yyyy', 'en');
      const toDate = formatDate(this.model.endDate, 'dd/MM/yyyy', 'en');
      modal.componentInstance.fromDate = fromDate;
      modal.componentInstance.toDate = toDate;
      modal.componentInstance.listData = this.listData;
      modal.result
        .then((res) => {
          if (res) {

          } else {
          }
        })
        .catch(() => {
        });

    }, (err) => {
      this.messageService.error(this.notificationMessage.error);
      this.isLoading = false;
    })
  }
  loadFile(fileId: string, url: string) {
    this.api.loadFile(`download/${fileId}`, 'pdf').then(
      (response) => {
        const blob = new Blob([response], { type: 'application/octet-stream' });
        const modal = this.modalService.open(KpiReportModalComponent, { windowClass: 'tree__report-modal' });
        modal.componentInstance.data = blob;
        modal.componentInstance.model = this.model;
        modal.componentInstance.baseUrl = url;
        modal.componentInstance.extendUrl = '';
        this.isLoading = false;
      },
      () => {
        this.messageService.error('Không thể tải file');
        this.isLoading = false;
      }
    );
  }

  validate(value) {
    switch (value) {
      case 'CN':

        return this.validateBranchCode();
      case 'CBQL':
        return this.validateCBQLCode();
      case 'RM':

        return this.validateRmCode();
    }
  }


  onModelChange($event) {
    this.branchCodes = $event;
    this.model.branchCodes = _.map($event, (x) => x.code);
  }

  onStartDateChange($event) {
    if (Utils.isNull(_.get(this.model, 'startDate'))) {
      _.set(this.model, 'startDate', new Date(moment().format('YYYY-MM-DD')));
    }
    this.ref.detectChanges();
  }

  closeStartDate($event) {
    const startDate: moment.Moment = moment(_.get(this.model, 'startDate'));
    const currentDate: moment.Moment = moment();
    const allowableStartDate: moment.Moment = currentDate.clone().subtract(this.limit.dayReport, 'days');

    const smonth = startDate.month();
    const year = startDate.year();



    if (startDate.isBefore(allowableStartDate)) {
      this.messageService.warn('Chỉ cho phép chọn thời điểm trong vòng 90 ngày gần nhất!');
      _.set(this.model, 'startDate', new Date());
      _.set(this.model, 'endDate', new Date());

      return;
    }

    _.set(this.model, 'endDate', new Date(year, smonth + 1, 0));
    this.ref.detectChanges();
  }

  onEndDateChange($event) {

    // const _maxDate = moment(this.maxDate)

    if (Utils.isNull(_.get(this.model, 'endDate'))) {
      _.set(this.model, 'endDate', new Date(moment(this.date).format('YYYY-MM-DD')));
    }
    this.ref.detectChanges();
  }

  closeEvent($event) {
    const smonth = moment(_.get(this.model, 'startDate')).month();
    const year = moment(_.get(this.model, 'startDate')).year();
    const emonth = moment(_.get(this.model, 'endDate')).month();
    if (emonth !== smonth) {
      this.messageService.warn('Bắt đầu từ ngày và đến ngày phải cùng 1 tháng');
      _.set(this.model, 'endDate', new Date(year, smonth + 1, 0));
    }
    this.ref.detectChanges();
  }

  onEndDateChangeValidate($event) {
    const smonth = moment(_.get(this.model, 'startDate')).month();
    const year = moment(_.get(this.model, 'startDate')).year();
    const emonth = moment(_.get(this.model, 'endDate')).month();
    if (emonth !== smonth) {
      this.messageService.warn('Bắt đầu từ ngày và đến ngày phải cùng 1 tháng');
      _.set(this.model, 'endDate', new Date(year, smonth + 1, 0));
      this.ref.detectChanges();
    }
  }

  onRmModelChange($event, type: string) {
    this.model.rmCodes = _.map($event, (x) => Utils.isStringNotEmpty(x.rmCode) && x.rmCode);
    if ($event.length < this.prevEvent.length) {
      this.ref.detectChanges();
      return;
    }
    this.prevEvent = [...$event];
    if (type === 'RM') {
      this.rmCodes = $event;
      this.validateRmCode();
    } else if (type === 'CBQL') {
      this.managerCodes = $event;
      this.validateCBQLCode();
    }

    this.ref.detectChanges();
  }

  export() {
    const valid = this.validate(_.get(this.model, 'kpiReportType'));
    if (!valid) {
      this.isLoading = false;
      return;
    }

    let listRmCode = '';
    let listTitle = this.rmCodes;

    if (_.get(this.model, 'kpiReportType') === 'CBQL') {
      listTitle = this.managerCodes;
    }

    listTitle.map((x) => {
      // listRmCode += x.rmCode + ',';
      if (!Utils.isStringNotEmpty(listRmCode)) {
        listRmCode += x.rmCode;
      } else {
        listRmCode += ',' + x.rmCode;
      }
    });
    if (this.model.kpiDetailReportType === 'KH' && this.model.productType === 'OTHER' && this.productCode === '') {
      const month = formatDate(this.model.currentMonth, 'yyyyMM', 'en');
      console.log('currentMonth: ',month);
     const params = {
        reportCode: this.reportCodeS3,
        reportParams: [
          {
            name: 'prd_id',
            value: Utils.getLastDayInMonth(month)
          },
          {
            name: 'field01',
            value: this.model.kpiReportType === 'CBQL' ? this.managerCodes[0].rmCode : this.rmCodes[0].rmCode
          }
        ],
        rsId: this.rsId,
        scope: Scopes.VIEW

      };
      console.log(params);
      this.isLoading = true;
      this.api.reportS3(params).pipe(
        finalize(() => {
          this.isLoading = false;
        })
      ).pipe(
        finalize(() => {
          this.isLoading = false;
        })
      ).subscribe(urls => {
        if (urls.length > 0) {
          const download = (urls) => {
            let url = urls.pop();
            window.open(url, '_self');

            if (!urls.length && this.intervalDownloadFileS3) {
              clearInterval(this.intervalDownloadFileS3);
            }
          }

          this.intervalDownloadFileS3 = setInterval(download, 500, urls);
        } else {
          this.messageService.warn(_.get(this.notificationMessage, 'report_empty'));
        }
      }, () => {
        this.messageService.error(this.notificationMessage.E001);
      });
    }
  }

  saveByteStreamToFile(byteStream: any, fileName: string): void {
    const blob = new Blob([byteStream], { type: 'application/vnd.ms-excel' });
    const url = URL.createObjectURL(blob);

    const link = document.createElement('a');
    link.href = url;
    link.download = fileName;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }


  viewListFile() {
    const form = this.modalService.open(KpiIndivReportListComponent, {
      windowClass: 'kpi-indiv-report-list',
      scrollable: true
    });
    form.componentInstance.listProduct = this.products;
    form.result.then((res) => {
      console.log('success');
    }).catch(() => {
    });
  }

  isReportBy() {
    // return this.form.get('reportBy').value === value;
    return true;
  }

  setPageCustomer(pageInfo) {
    this.paramsTableCustomer.page = pageInfo.offset;
    this.mapData();
  }

  mapData() {
    const total = this.termData.length;
    this.pageableCustomer.totalElements = total;
    this.pageableCustomer.totalPages = Math.floor(total / this.pageableCustomer.size);
    this.pageableCustomer.currentPage = this.paramsTableCustomer.page;
    const start = this.paramsTableCustomer.page * this.paramsTableCustomer.size;
    this.listDataTableCustomer = this.termData?.slice(start, start + this.paramsTableCustomer.size);
  }

  removeRecord(item) {
    _.remove(this.termData, (i) => i.code === item.code);
    _.remove(this.listDataTableCustomer, (i) => i.code === item.code);
    if (this.listDataTableCustomer.length === 0 && this.pageableCustomer.currentPage > 0) {
      this.paramsTableCustomer.page -= 1;
    }
    this.listDataTableCustomer = [...this.listDataTableCustomer];
    this.allRM = false;
    this.mapData();
  }


  searchCustomer() {
    const formSearch = {
      customerType: {
        value: Division.INDIV,
        disabled: true
      }
    };
    const modal = this.modalService.open(CustomerModalComponent, { windowClass: 'list__customer360-modal' });
    modal.componentInstance.listSelectedOld = this.termData;
    modal.componentInstance.dataSearch = formSearch;
    modal.result
      .then((res) => {
        if (res) {
          console.log('res: ',res);
          this.termData =
            res.listSelected?.map((item) => {
              return {
                code: item.customerCode,
                customerCode: item.customerCode,
                name: item.customerName ? item.customerName : item.name
              };
            }) || [];
            this.mapData();
            this.validateCustomerCode();
        }
      })
      .catch(() => {
      });
  }

  add() {
    this.textSearch = _.trim(this.textSearch);
    if (this.textSearch.length === 0) {
      this.isLoading = false;
      return;
    }
    if (_.findIndex(this.termData, (i) => i.code?.toUpperCase() === this.textSearch?.toUpperCase()) === -1) {
      this.isLoading = true;
      const params = {
        customerCode: this.textSearch,
        customerType: Division.INDIV,
        rsId: this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`)?.rsId,
        scope: Scopes.VIEW,
        pageNumber: 0,
        pageSize: global?.userConfig?.pageSize
        // isCheckGroupCBQL: this.form.get('division').value !== Division.INDIV,
      };
      this.isLoading = true;
      let api: Observable<any>;
      api = this.customerApi.search(params);
      api.subscribe(
        (data) => {
          if (data?.length > 0) {
            this.termData?.push({
              code: data[0]?.customerCode,
              name: data[0]?.customerName,
              customerCode: data[0]?.customerCode
            });
            this.mapData();
          } else {
            this.messageService.warn(_.get(this.notificationMessage, 'customerNotExist'));
          }
          this.isLoading = false;
        },
        (e) => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    }
  }

  removeListCustomer() {
    this.termData = [];
    this.paramsTableCustomer.page = 0;
    this.allRM = false;
    this.mapData();
  }

  validateRmCode() : boolean {
    if (this.rmCodes.length === 0) {
      this.messageService.error("Vui lòng chọn RM");
      return false
    }

    if (this.model.kpiDetailReportType === "KH") {
      if(this.rmCodes.length > 1) {
        this.messageService.error("Chỉ được phép chọn 1 RM");
        return false;
      }
    } else {
      if(this.rmCodes.length > 50) {
        this.messageService.error('Chỉ được chọn tối đa ' + this.maxRm + ' RM');
        return false;
      }
    }
    return true;
  }

  validateCBQLCode() : boolean {
    if (this.managerCodes.length === 0) {
      this.messageService.error("Vui lòng chọn RM");
      return false
    }

    if (this.model.kpiDetailReportType === "KH") {
      if(this.managerCodes.length > 1) {
        this.messageService.error("Chỉ được phép chọn 1 RM");
        return false;
      }
    } else {
      if(this.managerCodes.length > 50) {
        this.messageService.error('Chỉ được chọn tối đa ' + this.maxRm + ' RM');
        return false;
      }
    }
    return true;
  }


  validateCustomerCode() : boolean {
    if (this.model.kpiDetailReportType === 'KH') {
      if(this.termData.length > 1) {
        this.messageService.error("Chỉ được phép chọn 1 KH");
        return false;
      } else if (this.termData.length === 0) {
        this.messageService.error("Vui lòng chọn Khách hàng");
        return false
      }
    }
    return true;
  }

  validateBranchCode() {
    if (Utils.isArrayEmpty(_.get(this.model, 'branchCodes'))) {
      this.messageService.warn("Vui lòng chọn đơn vị");
      return false;
    }
    return true;
  }
}
