import { formatDate } from '@angular/common';
import { forkJoin, of } from 'rxjs';
import { AfterViewInit, Component, Injector, OnInit } from '@angular/core';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { TableColumn } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import {
  BRANCH_HO,
  CommonCategory,
  Division,
  FunctionCode,
  maxInt32,
  Scopes,
  SessionKey
} from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as moment from 'moment';
import { catchError, finalize } from 'rxjs/operators';
import { CustomerApi } from 'src/app/pages/customer-360/apis';
import { KpiReportApi } from '../../apis';
import { KpiReportModalComponent } from '../../dialogs';
import { RmApi } from 'src/app/pages/rm/apis';
import { RmModalComponent } from 'src/app/pages/rm/components/rm-modal/rm-modal.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { CustomerModalComponent } from 'src/app/pages/customer-360/components';
import { DashboardService } from 'src/app/pages/dashboard/services/dashboard.service';
import { validateAllFormFields } from 'src/app/core/utils/function';
import {
  ChooseBranchesModalComponent
} from '../../../system/components/choose-branches-modal/choose-branches-modal.component';

@Component({
  selector: 'app-report-customer-management-tttm-v2',
  templateUrl: './report-customer-management-tttm-v2.component.html',
  styleUrls: ['./report-customer-management-tttm-v2.scss']
})
export class ReportCustomerManagementTttmV2 extends BaseComponent implements OnInit, AfterViewInit {
  commonData = {
    isRm: true,
    isRmTttm: true,
    isRegionDirector: false,
    listBranch: [],
    regionList: [],
    listYears: [],
    listDivision: []
  };
  textSearch: string;
  paramsTable = {
    page: 0,
    size: global?.userConfig?.pageSize
  };
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize
  };

  form = this.fb.group({
    momentReport: [new Date(moment().add(-1, 'day').toDate()), CustomValidators.required],
    period: 'monthly',
    reportBy: 'customer',
    downloadReport: 'HTML',
    listOption: 'ALL',
    branchCodeLv1: [[]],
    branchCode: [''],
    regionCode: '',
    page: 1,
    division: ''
  });
  allRM = false;
  dataTerm = {
    rm: {
      pageable: { ...this.pageable },
      term: []
    },
    rm_tttm: {
      pageable: { ...this.pageable },
      term: []
    },
    branch: {
      pageable: { ...this.pageable },
      term: []
    },
    customer: {
      pageable: { ...this.pageable },
      term: []
    }
  };
  tableType: string;
  termData = [];
  listDataTable = [];
  columns: TableColumn[];
  fileName = 'bao-cao-quan-tri-KH-TTTM';
  countRm: any;
  countBranchMax: number;
  countCustomerMax: number;
  regions = {};
  allBranchLv2 = [];
  allBranchLv1 = [];
  listBranchTerm = [];
  branchLv1List = [];
  branchLv2List = [];
  lenghtBranchesOfUser: number;
  isHO = false;
  objFunctionReport: any;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private customerApi: CustomerApi,
    private rmApi: RmApi,
    private kpiReportApi: KpiReportApi,
    private dashboardService: DashboardService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.objFunctionReport = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REPORT_TTTM_V2}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.tableType = this.form.get('reportBy').value;
    this.columns = [
      {
        name: this.fields.rmCode,
        prop: 'code'
      },
      {
        name: this.fields.rmName,
        prop: 'name'
      }
    ];

    this.isHO = this.currUser?.branch === BRANCH_HO;
    const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
    this.commonData.listDivision = divisionOfUser
      ?.map((item) => {
        if (item.code === Division.SME || item.code === Division.CIB) {
          return { code: item.code, name: `${item.code || ''} - ${item.name || ''}` };
        }
      })
      ?.filter((i) => i);

    this.form.controls.division.setValue('SME');
    forkJoin([
      this.categoryService.getBranchesOfUser(this.objFunctionReport?.rsId, Scopes.VIEW).pipe(catchError(() => of(undefined))),
      this.categoryService.getCommonCategory(CommonCategory.REPORTS_MAX_LENGTH_RM).pipe(catchError(() => of(undefined))),
      this.dashboardService.getBranchByDomain({ rsId: this.objFunctionReport?.rsId, scope: Scopes.VIEW }),
      this.rmApi.findRMInfo(this.currUser.hrsCode).pipe(catchError(() => of(undefined))),
    ]).pipe(finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(([branchesOfUser, configReport, branchesByDomain, rmInfo]) => {
      this.countRm =
        _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.TABLEAU_MAX_RM_REPORT)?.value ||
        0;
      this.countBranchMax =
        _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.TABLEAU_MAX_BRANCH_REPORT)?.value ||
        0;
      this.countCustomerMax =
        _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.TABLEAU_MAX_CUSTOMER_REPORT)?.value || 0;

      this.commonData.listBranch = branchesOfUser || [];
      this.commonData.isRm = _.isEmpty(branchesOfUser);
      this.lenghtBranchesOfUser = branchesOfUser.length;
      this.branchLv2List = branchesOfUser;
      this.commonData.isRmTttm = rmInfo?.hrisEmployee?.title?.toUpperCase().includes('TÀI TRỢ THƯƠNG MẠI');
      this.commonData.isRegionDirector = this.objFunctionReport.scopes.includes('VIEW_REGION_REPORT');

      this.initBranch(branchesByDomain);
      this.setBranchOfUser();
    });

    this.textSearch = this.currUser.code;
    if (this.currUser.code) {
      this.add();
    }
  }

  ngAfterViewInit() {
    if (!_.find(this.commonData.listDivision, (item) => item.code === this.form.get('division').value)) {
      this.form.get('division').setValue(_.head(this.commonData.listDivision)?.code);
    }
    this.setValueChanges();
  }

  search() {
    if (this.form.controls.listOption.value === 'rm') {
      const formSearch = {
        crmIsActive: { value: 'true', disabled: true },
        rmBlock: {
          value: this.form.controls.division.value,
          disabled: true
        },
        isReportByRm: this.commonData.isRm,
        manager: true,
        rm: true
      };
      const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
      modal.componentInstance.dataSearch = formSearch;
      modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
      modal.result
        .then((res) => {
          if (res) {
            const listRMSelect = res.listSelected?.map((item) => {
              return {
                hrsCode: item?.hrisEmployee?.employeeId,
                code: item?.t24Employee?.employeeCode,
                name: item?.hrisEmployee?.fullName
              };
            });
            this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
            this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);
            this.mapData();
          }
        })
        .catch(() => {
        });
    } else if (this.form.controls.listOption.value === 'customer') {
      const formSearch = {
        customerType: {
          value: this.form.controls.division.value,
          disabled: true
        }
      };
      const modal = this.modalService.open(CustomerModalComponent, { windowClass: 'list__customer360-modal' });
      modal.componentInstance.listSelectedOld = this.termData;
      modal.componentInstance.dataSearch = formSearch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData =
              res.listSelected?.map((item) => {
                return {
                  code: item.customerCode,
                  customerCode: item.customerCode,
                  name: item.customerName ? item.customerName : item.name
                };
              }) || [];
            this.mapData();
          }
        })
        .catch(() => {
        });
    } else if (this.form.controls.listOption.value === 'branch') {
      const modal = this.modalService.open(ChooseBranchesModalComponent, { windowClass: 'tree__branches-modal' });
      modal.componentInstance.listBranchOld = _.map(this.termData, (x) => x.code) || [];
      modal.componentInstance.listBranch = this.commonData.listBranch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData = res;
            this.mapData();
          }
        })
        .catch(() => {
        });
    } else if (this.form.controls.listOption.value === 'rm_tttm') {
      const formSearch = {
        crmIsActive: { value: 'true', disabled: true },
        rmBlock: {
          value: this.form.controls.division.value,
          disabled: true
        },
        productGroup: 'TTTM',
        scope: Scopes.VIEW,
        rsId: this.objFunction?.rsId
        // isReportByRm: this.commonData.isRm,
        // manager: true,
        // rm: true
      };
      const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
      modal.componentInstance.dataSearch = formSearch;
      modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
      modal.componentInstance.isRmSale = true;
      modal.result
        .then((res) => {
          if (res) {
            const listRMSelect = res.listSelected?.map((item) => {
              return {
                hrsCode: item?.hrisEmployee?.employeeId,
                code: item?.t24Employee?.employeeCode,
                name: item?.hrisEmployee?.fullName
              };
            });
            this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
            this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);
            this.mapData();
          }
        })
        .catch(() => {
        });
    }
  }

  add() {
    if (this.hidePickupList) {
      return;
    }

    this.textSearch = _.trim(this.textSearch);
    if (this.textSearch.length === 0) {
      this.isLoading = false;
      return;
    }
    let option = this.form.controls.listOption.value;
    if (_.findIndex(this.termData, (i) => i.code?.toUpperCase() === this.textSearch?.toUpperCase()) === -1) {
      this.isLoading = true;
      if (option === 'rm') {
        const params = {
          crmIsActive: true,
          scope: Scopes.VIEW,
          rmBlock: this.form.controls.division.value,
          rsId: this.objFunction?.rsId,
          employeeCode: this.textSearch,
          page: 0,
          size: maxInt32,
          isReportByRm: this.commonData.isRm,
          rm: true,
          manager: true
        };
        this.isLoading = true;
        this.rmApi.post('findAll', params).subscribe(
          (data) => {
            const itemResult = _.find(
              _.get(data, 'content'),
              (item) => item?.t24Employee?.employeeCode?.toUpperCase() === this.textSearch?.toUpperCase()
            );
            if (itemResult) {
              this.termData?.push({
                code: itemResult?.t24Employee?.employeeCode,
                name: itemResult?.hrisEmployee?.fullName,
                hrsCode: itemResult?.hrisEmployee?.employeeId
              });
              this.termData = _.uniqBy(this.termData, (i) => i.hrsCode);
              this.mapData();
            } else if (!itemResult && !this.allRM) {
              this.messageService.warn(_.get(this.notificationMessage, 'rmNotExistOrNotBranh'));
            }
            this.isLoading = false;
          },
          () => {
            this.messageService.error(this.notificationMessage.E001);
            this.isLoading = false;
          }
        );
      } else if (option === 'customer') {
        const params = {
          customerCode: this.textSearch,
          customerType: this.form.controls.division.value,
          rsId: this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`)?.rsId,
          scope: Scopes.VIEW,
          pageNumber: 0,
          pageSize: global?.userConfig?.pageSize
        };
        this.customerApi.searchSme(params).pipe(
          finalize(() => {
            this.isLoading = false;
          })
        ).subscribe((data) => {
          if (data?.length > 0) {
            this.termData?.push({
              code: data[0]?.customerCode,
              name: data[0]?.customerName,
              customerCode: data[0]?.customerCode
            });
            this.mapData();
          } else {
            this.messageService.warn(_.get(this.notificationMessage, 'customerNotExist'));
          }
        }, () => {
          this.messageService.error(this.notificationMessage.E001);
        });
      } else if (option === 'branch') {
        const itemResult = _.find(
          this.commonData.listBranch,
          (item) => item.code?.toUpperCase() === this.textSearch?.toUpperCase()
        );
        if (itemResult) {
          this.termData?.push(itemResult);
          this.mapData();
        } else {
          this.messageService.warn(_.get(this.notificationMessage, 'branchNotExistOrNotInBranch'));
        }
        this.isLoading = false;
      } else if (option === 'rm_tttm') {
        const params = {
          crmIsActive: true,
          scope: Scopes.VIEW,
          rmBlock: this.form.controls.division.value,
          productGroup: 'TTTM',
          rsId: this.objFunction?.rsId,
          employeeCode: this.textSearch,
          page: 0,
          size: maxInt32
          // isReportByRm: this.commonData.isRm,
          // rm: true,
          // manager: true
        };
        this.isLoading = true;
        this.rmApi.getRMSale(params).subscribe(
          (data) => {
            const itemResult = _.find(
              _.get(data, 'content'),
              (item) => item?.t24Employee?.employeeCode?.toUpperCase() === this.textSearch?.toUpperCase()
            );
            if (itemResult) {
              this.termData?.push({
                code: itemResult?.t24Employee?.employeeCode,
                name: itemResult?.hrisEmployee?.fullName,
                hrsCode: itemResult?.hrisEmployee?.employeeId
              });
              this.termData = _.uniqBy(this.termData, (i) => i.hrsCode);
              this.mapData();
            } else if (!itemResult && !this.allRM) {
              this.messageService.warn(_.get(this.notificationMessage, 'rmTttmNotExistOrNotBranh'));
            }
            this.isLoading = false;
          },
          () => {
            this.messageService.error(this.notificationMessage.E001);
            this.isLoading = false;
          }
        );
      }
    } else {
      const messageReport = option.toString() + 'IsExist';
      this.messageService.warn(_.get(this.notificationMessage, messageReport));
      this.isLoading = false;
      return;
    }
  }

  setPage(pageInfo) {
    this.paramsTable.page = pageInfo.offset;
    this.mapData();
  }

  mapData() {
    const total = this.termData.length;
    this.pageable.totalElements = total;
    this.pageable.totalPages = Math.floor(total / this.pageable.size);
    this.pageable.currentPage = this.paramsTable.page;
    const start = this.paramsTable.page * this.paramsTable.size;
    this.listDataTable = this.termData?.slice(start, start + this.paramsTable.size);
  }

  removeRecord(item) {
    _.remove(this.termData, (i) => i.code === item.code);
    _.remove(this.listDataTable, (i) => i.code === item.code);
    if (this.listDataTable.length === 0 && this.pageable.currentPage > 0) {
      this.paramsTable.page -= 1;
    }
    this.listDataTable = [...this.listDataTable];
    this.allRM = false;
    this.mapData();
  }

  viewReport() {
    if (this.isLoading) {
      return;
    }

    if (!this.form.valid) {
      validateAllFormFields(this.form);
      return;
    }

    let option = this.form.controls.listOption.value;
    const formData = this.form.getRawValue();
    const { momentReport, reportBy, period } = formData;
    let codes = this.termData.map(item => item.code).join(';');
    codes = option === 'ALL' ? 'ALL' : codes;

    // this.validateTermList(reportBy);
    const isTermDataEmpty = _.isEmpty(this.termData);
    const isOptionAll = option === 'ALL';
    if (!isOptionAll) {
      if (option === 'branch') {
        if (isTermDataEmpty && reportBy != 'branch') {
          this.messageService.warn(_.get(this.notificationMessage, 'branchValidation'));
          return;
        }
        if (this.termData.length > this.countBranchMax && reportBy != 'branch') {
          this.translate.get('notificationMessage.countBranchMax', { number: this.countBranchMax }).subscribe((res) => {
            this.messageService.warn(res);
          });
          return;
        }
        if (this.commonData.isRm) {
          this.messageService.warn(_.get(this.notificationMessage, 'branchValidation'));
          return;
        }
      } else if (['rm', 'rm_tttm'].includes(option)) {
        if (isTermDataEmpty) {
          this.messageService.warn(_.get(this.notificationMessage, 'rmValidation'));
          return;
        }
        if (this.termData.length > this.countRm) {
          this.translate.get('notificationMessage.countRmMax', { number: this.countRm }).subscribe((res) => {
            this.messageService.warn(res);
          });
          return;
        }
      } else if (option === 'customer') {
        if (isTermDataEmpty) {
          this.messageService.warn(_.get(this.notificationMessage, 'customerValidation'));
          return;
        }
        if (this.termData.length > this.countCustomerMax) {
          this.translate.get('notificationMessage.countCustomerLimit', { number: this.countCustomerMax }).subscribe((res) => {
            this.messageService.warn(res);
          });
          return;
        }
      }
    }

    // set report param
    let reportCode;
    let reportParams = [
      { name: 'p_lob', value: this.form.controls.division.value },
      { name: 'type_of_time', value: period === 'monthly' ? 'MTD' : period === 'yearly' ? 'YTD' : ''},
      { name: 'p_time', value: formatDate(momentReport, 'dd/MM/yyyy', 'en') }
    ];

    switch (reportBy) {
      case 'customer':
        reportCode = 'REPORT_TTTM_CUSTOMER';
        reportParams.push(...[
          { name: 'p_rm_code', value: this.commonData.isRm && !this.commonData.isRmTttm ? this.currUser.code : option === 'rm' ? codes : 'ALL' },
          { name: 'p_br_code_lv1', value: 'ALL' },
          { name: 'p_br_code_lv2', value: option === 'branch' ? codes : 'ALL' },
          { name: 'p_customer_id', value: option === 'customer' ? codes : 'ALL' },
          { name: 'p_rm_tf_code', value: this.commonData.isRmTttm ? this.currUser.code : option === 'rm_tttm' ? codes : 'ALL' },
          { name: 'p_rgon', value: option === 'region' ? this.form.controls.regionCode.value : 'ALL' }
        ]);
        break;
      case 'branch':
        reportCode = 'REPORT_TTTM_BRANCH';
        reportParams.push(...[
          { name: 'p_rm_code', value: 'ALL' },
          { name: 'p_rgon', value: 'ALL' },
          { name: 'p_br_code_lv1', value: formData.branchCodeLv1 },
          { name: 'p_br_code_lv2', value: formData.branchCode }
        ]);
        break;
      default:
        break;
    }

    let params = {
      reportCode: reportCode,
      reportParams: reportParams,
      rsId: this.objFunctionReport?.rsId,
      scope: Scopes.VIEW
    };

    this.isLoading = true;
    this.kpiReportApi.getTableauReport(params).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(embeddedLink => {
      if (embeddedLink.length > 0) {
        this.loadReport(embeddedLink, params);
      } else {
        this.messageService.error(this.notificationMessage.E001);
      }
    });
  }

  loadReport(embeddedLink: string, paramSearch: any) {
    const modal = this.modalService.open(KpiReportModalComponent, { windowClass: 'tree__report-modal' });
    modal.componentInstance.embeddedReport = embeddedLink;
    modal.componentInstance.data = null;
    modal.componentInstance.model = paramSearch;
    modal.componentInstance.baseUrl = null;
    modal.componentInstance.filename = this.fileName;
    modal.componentInstance.title = 'Báo cáo quản trị KH TTTM';
    modal.componentInstance.extendUrl = '';
    modal.componentInstance.reportType = 1;
  }

  removeList() {
    this.termData = [];
    this.paramsTable.page = 0;
    this.allRM = false;
    this.mapData();
  }

  initBranch(branchesByDomain) {
    // region, branch lv1 , branch lv2
    const regionObj = _.groupBy(branchesByDomain, 'region');
    const regionList = Object.keys(regionObj).map(key => ({ locationCode: key, locationName: key }));

    Object.keys(regionObj).forEach(key => {
      const branchLv1 = _.groupBy(regionObj[key], 'parentCode');
      const branchLv1List = Object.keys(branchLv1).map(branchKey => {
        return {
          code: branchLv1[branchKey][0].parentCode,
          name: branchLv1[branchKey][0].parentName,
          displayName: `${branchLv1[branchKey][0].parentCode} - ${branchLv1[branchKey][0].parentName}`,
          khuVucM: key,
          branch_lv2: branchLv1[branchKey].map(branch => {
            return {
              code: branch.code,
              name: branch.name,
              displayName: `${branch.code} - ${branch.name}`,
              khuVucM: key,
              parentCode: branchLv1[branchKey][0].parentCode
            };
          })
        };
      });
      if (!this.regions[key]) {
        this.regions[key] = {
          branch_lv1: branchLv1List
        };
      }
    });

    // all branch lv
    Object.keys(this.regions).forEach(key => {
      this.regions[key].branch_lv1.forEach(item => {
        this.allBranchLv2.push(...item.branch_lv2);
      });
    });

    Object.keys(this.regions).forEach(key => {
      this.regions[key].branch_lv1.forEach(item => {
        const branchLv1 = this.allBranchLv1.find(branch => branch.code === item.code);
        if (!branchLv1) this.allBranchLv1.push(item);
      });
    });

    this.listBranchTerm = this.allBranchLv2 || [];
    this.sessionService.setSessionData(SessionKey.LIST_BRANCH, this.listBranchTerm);

    this.commonData.listBranch = _.cloneDeep(this.listBranchTerm);
    if (!_.find(this.commonData.listBranch, (item) => item.code === this.currUser?.branch)) {
      this.commonData.listBranch.push({
        code: this.currUser?.branch,
        displayName: `${this.currUser?.branch} - ${this.currUser?.branchName}`
      });
    }

    // set all region
    if (this.isHO) {
      this.commonData.regionList = regionList;
    } else {
      regionList?.forEach((item) => {
        if (
          this.commonData.listBranch?.findIndex((i) => i.khuVucM === item?.locationCode) !== -1 &&
          this.commonData.regionList.findIndex((r) => r.locationCode === item?.locationCode) === -1
        ) {
          this.commonData.regionList.push(item);
        }
      });
    }

    // sort region list
    this.commonData.regionList = _.orderBy(this.commonData.regionList, [region => region.locationName], ['asc']);
    if (this.commonData.regionList.length > 1) {
      let allRegion = '';
      // this.commonData.regionList?.map(item => {
      //   if (item.locationCode !== undefined && item.locationCode !== null && item.locationCode !== 'undefined') {
      //     allRegion += item.locationCode + ';';
      //   }
      // })
      allRegion = this.commonData.regionList.map(item => item.locationCode).join(';');
      this.commonData.regionList.unshift(
        {
          locationCode: allRegion,
          locationName: 'Tất cả'
        }
      );
    }

    if (this.commonData.regionList.length) {
      this.form.controls.regionCode.setValue(this.commonData.regionList[0].locationCode);
    }
    console.log(this.commonData.regionList);
  }

  // set default branch lv1, lv2
  setBranchOfUser() {
    this.setBranchLv1List();
  }

  setBranchLv1List(options: any = {}): void {
    const { branchCode } = this.form.getRawValue();
    const { setLv2List = true, resetCode = false } = options;
    let branchLv1List = [];

    if (resetCode) {
      this.form.controls.branchCode.setValue('', { emitEvent: false });
      this.form.controls.branchCodeLv1.setValue('', { emitEvent: false });
    }

    if (!branchLv1List.length) {
      this.branchLv1List = _.cloneDeep(this.allBranchLv1);
    } else {
      this.branchLv1List = branchLv1List;
    }

    // sort alphabe
    this.branchLv1List = _.orderBy(this.branchLv1List, [branch => branch.displayName], ['asc']);

    const haveAllOption = this.branchLv1List.find(branch => branch.code === '');
    if (this.branchLv1List.length > 1 && !haveAllOption) {
      this.branchLv1List.unshift({ code: 'ALL', displayName: 'Tất cả' });
    }

    let value = _.head(this.branchLv1List)?.code;

    // follow branch lv2
    if (branchCode && !setLv2List) {
      const branchLv2 = this.allBranchLv2.find(branch => branch.code === branchCode);

      if (branchLv2?.parentCode) value = branchLv2?.parentCode;
    }

    this.form.controls.branchCodeLv1.setValue(value, { emitEvent: false });

    if (setLv2List) {
      this.setBranchLv2List();
    }

  }

  setBranchLv2List(options: any = {}): void {
    const { branchCodeLv1 } = this.form.getRawValue();
    let { onChange = false, alreadyPick = false } = options;

    if (branchCodeLv1 && branchCodeLv1 !== 'ALL') {
      this.branchLv2List = this.allBranchLv2.filter(branch => branch.parentCode === branchCodeLv1);
    } else {
      this.branchLv2List = this.allBranchLv2;
    }

    this.branchLv2List = _.orderBy(this.branchLv2List, [branch => branch.displayName], ['asc']);

    this.setOptionAllToBrLv2();

    if (!onChange && !alreadyPick) {
      this.form.controls.branchCode.setValue(_.head(this.branchLv2List)?.code, { emitEvent: false });
    }
  }

  setOptionAllToBrLv2(): void {
    const haveAllOption = this.branchLv2List.find(branch => branch.code === '');
    if (this.branchLv2List.length > 1 && !haveAllOption) {
      this.branchLv2List.unshift({ code: 'ALL', displayName: 'Tất cả' });
    }
  }

  setValueChanges(): void {
    this.form.controls.branchCodeLv1.valueChanges.subscribe(() => {
      if (this.isLoading) {
        return;
      }
      this.setBranchLv2List();

    });

    this.form.controls.reportBy.valueChanges.subscribe(() => {
      this.initPickupList(this.form.controls.listOption.value);
    });

    this.form.controls.listOption.valueChanges.subscribe((value) => {
      this.initPickupList(value);
    });

    this.form.controls.division.valueChanges.subscribe((value) => {
      this.removeAll();
    });
  }

  initPickupList(value: string): void {
    this.allRM = false;
    this.paramsTable.page = 0;
    this.dataTerm[this.tableType] = {
      pageable: { ...this.pageable },
      term: [...this.termData]
    };
    this.textSearch = '';
    if (value === 'rm') {
      this.columns[0].name = this.fields.rmCode;
      this.columns[1].name = this.fields.rmName;
      this.termData = [...this.dataTerm.rm.term];
      this.pageable = { ...this.dataTerm.rm.pageable };
      if (this.commonData.isRm) {
        this.textSearch = this.currUser.code;
        if (this.termData.length === 0 && this.currUser.code) {
          this.add();
        }
      } else {
        this.textSearch = '';
      }
    } else if (value === 'customer') {
      this.columns[0].name = this.fields.customerCode;
      this.columns[1].name = this.fields.customerName;
      this.termData = [...this.dataTerm.customer.term];
      this.pageable = { ...this.dataTerm.customer.pageable };
    } else if (value === 'branch') {
      this.columns[0].name = this.fields.branchCode;
      this.columns[1].name = this.fields.branchName;
      this.termData = [...this.dataTerm.branch.term];
      this.pageable = { ...this.dataTerm.branch.pageable };
    } else if (value === 'rm_tttm') {
      this.columns[0].name = this.fields.rmCode;
      this.columns[1].name = this.fields.rmName;
      this.termData = [...this.dataTerm.rm_tttm.term];
      this.pageable = { ...this.dataTerm.rm_tttm.pageable };
      if (this.commonData.isRm) {
        this.textSearch = this.currUser.code;
        if (this.termData.length === 0 && this.currUser.code) {
          this.add();
        }
      } else {
        this.textSearch = '';
      }
    }
    this.tableType = value;
    this.mapData();
  }

  removeAll() {
    this.dataTerm.rm.term = [];
    this.dataTerm.branch.term = [];
    this.dataTerm.customer.term = [];
    this.termData = [];
    this.paramsTable.page = 0;
    this.allRM = false;
    this.mapData();
  }

  get hidePickupList(): boolean {
    return (
        this.form.controls.reportBy.value === 'customer' &&
        ['ALL', 'region'].includes(this.form.controls.listOption.value)
      )
      ||
      this.form.controls.reportBy.value === 'branch';
  }

  showButtonOptionRegion(): boolean {
    return this.form.controls.reportBy.value === 'customer'
      && this.form.controls.listOption.value === 'region'
      && this.commonData.isRegionDirector;
  }

}
