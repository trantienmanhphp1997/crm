import { formatDate } from '@angular/common';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { Component, OnInit, Injector, AfterViewInit } from '@angular/core';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { TableColumn } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import {
  BRANCH_HO,
  CommonCategory,
  Division,
  FunctionCode,
  maxInt32,
  Scopes,
  SessionKey,
} from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as moment from 'moment';
import { catchError } from 'rxjs/operators';
import { CustomerApi } from 'src/app/pages/customer-360/apis';
import { KpiReportApi } from '../../apis';
import { KpiReportModalComponent } from '../../dialogs';
import { RmApi } from 'src/app/pages/rm/apis';
import { RmModalComponent } from 'src/app/pages/rm/components/rm-modal/rm-modal.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { saveAs } from 'file-saver';
import { environment } from 'src/environments/environment';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { CustomerModalComponent } from 'src/app/pages/customer-360/components';
import { HttpClient } from '@angular/common/http';
import { CategoryRegionApi } from '../../apis/category-region.api';
import { RmProfileService } from '../../../../core/services/rm-profile.service';

@Component({
  selector: 'app-report-mobilization-v2',
  templateUrl: './report-mobilization.component.html',
  styleUrls: ['./report-mobilization.component.scss'],
})
export class ReportMobilizationV2Component extends BaseComponent implements OnInit, AfterViewInit {
  commonData = {
    listDivision: [],
    listCurrencyUnit: [],
    listTerm: [],
    listCurrencyType: [],
    minDate: null,
    minDateMonth: null,
    maxDate: new Date(),
    listQuarterly: [
      {
        label: 'Quý 1',
        value: 1,
      },
      {
        label: 'Quý 2',
        value: 2,
      },
      {
        label: 'Quý 3',
        value: 3,
      },
      {
        label: 'Quý 4',
        value: 4,
      },
    ],
    listYears: [],
    listYears_Year: [],
    listYearsCompare: [],
    listYears_YearCompare: [],
    listQuarterlyCompare: [],
    isRm: true,
    regionList: [],
    isHO: false,
    isRegionDirector: false,
    branches: [],
  };
  textSearch: string;
  paramsTable = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };
  form = this.fb.group({
    division: '',
    currencyType: '',
    currencyUnit: '',
    reportType: 'moment',
    momentReport: [new Date(moment().add(-1, 'day').toDate()), CustomValidators.required],
    momentCompare: [new Date(moment().add(-2, 'day').toDate()), CustomValidators.required],
    period: 'daily',
    reportPeriodFrom: [new Date(moment().add(-1, 'months').toDate())],
    reportPeriodTo: '',
    comparePeriodFrom: [new Date(moment().add(-2, 'months').toDate())],
    comparePeriodTo: null,
    reportBy: 'rm',
    reportTypeDetail: 'rm',

    reportPeriodQuarterly: '',
    reportPeriodYear: '',

    comparePeriodQuarterly: '',
    comparePeriodYear: '',
    downloadReport: 'HTML',
    term: '',
    areaCode: '',
    listOption: 'ALL',
  });
  allRM = false;
  dataTerm = {
    rm: {
      pageable: { ...this.pageable },
      term: [],
    },
    customer: {
      pageable: { ...this.pageable },
      term: [],
    },
  };
  tableType: string;
  termData = [];
  listDataTable = [];
  columns: TableColumn[];
  maxDate = new Date();
  fileName = 'bao-cao-huy-dong-von';
  countRm: number;
  countTablueRm: number;
  countCustomerMax: number;
  countTablueCustomerMax: number;
  countBranch: number;
  countTablueBranch: number;
  maxMonth: any;
  branches: Array<any>;
  branchCodes: Array<any>;
  intervalDownloadFileS3 = null;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private customerApi: CustomerApi,
    private rmApi: RmApi,
    private kpiReportApi: KpiReportApi,
    private http: HttpClient,
    private regionApi: CategoryRegionApi,
    private rmProfileService: RmProfileService,

  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);

    this.isLoading = true;
  }

  ngOnInit(): void {
    this.commonData.isHO = this.currUser?.branch === BRANCH_HO;
    this.tableType = this.form.get('reportBy').value;
    this.columns = [
      {
        name: this.fields.rmCode,
        prop: 'code',
      },
      {
        name: this.fields.rmName,
        prop: 'name',
      },
    ];
    this.form.get('reportPeriodQuarterly').setValue(_.head(this.commonData.listQuarterly)?.value);
    this.form.get('comparePeriodQuarterly').setValue(_.last(this.commonData.listQuarterly)?.value);
    this.prop = this.sessionService.getSessionData(SessionKey.REPORT_MOBILIZATION_V2);
    if (!this.prop) {
      const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
      this.commonData.listDivision =
        divisionOfUser?.map((item) => {
          return { code: item.code, name: `${item.code || ''} - ${item.name || ''}` };
        }) || [];
      this.form.get('division').setValue(_.head(this.commonData.listDivision)?.code);
      forkJoin([
        this.commonService
          .getCommonCategory(CommonCategory.CURRENCY_UNIT_REPORTS)
          .pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.REPORTS_YEARS).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.TERM_TYPE).pipe(catchError(() => of(undefined))),
        this.categoryService
          .getBranchesOfUser(this.objFunction.rsId, Scopes.VIEW)
          .pipe(catchError(() => of(undefined))),
        this.categoryService.getCommonCategory(CommonCategory.CURRENCY_TYPE).pipe(catchError(() => of(undefined))),
        this.categoryService
          .getCommonCategory(CommonCategory.REPORTS_MAX_LENGTH_RM)
          .pipe(catchError(() => of(undefined))),
        this.regionApi
          .getLocation()
          .pipe(catchError(() => of(undefined))),
        this.rmProfileService
          .getRolesByUser(this.currUser.username)
          .pipe(catchError(() => of(undefined))),
        this.categoryService
          .getCommonCategory(CommonCategory.REGIONAL_DIRECTOR)
          .pipe(catchError(() => of(undefined))),
      ]).subscribe(([currencyUnits, configYear, term, branchesOfUser, listCurrencyType, configReport, regionList, roleList, regionDirectorTitle]) => {
        if (this.commonData.isHO) {
          this.commonData.isRegionDirector = true;
        } else {
          _.forEach(roleList, i => {
            if (_.find(regionDirectorTitle.content, (item) => item.value === i.name)) {
              this.commonData.isRegionDirector = true;
              return;
            }
          });
        }
        this.commonData.branches = branchesOfUser;
        this.initRegionList(regionList);
        this.countTablueBranch =
          _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.TABLEAU_MAX_BRANCH_REPORT)?.value ||
          0;
        this.countTablueRm =
          _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.TABLEAU_MAX_RM_REPORT)?.value ||
          0;
        this.countTablueCustomerMax =
          _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.TABLEAU_MAX_CUSTOMER_REPORT)?.value ||
          0;
        this.countBranch =
          _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.MAX_BRANCH)?.value ||
          0;
        this.countRm =
          _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.MAX_RM_REPORT_CREDIT)?.value ||
          0;
        this.countCustomerMax =
          _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.MAX_CUSTOMER_REPORT)?.value ||
          0;
        this.commonData.listCurrencyType = _.get(listCurrencyType, 'content')
          ?.map((itemValue) => {
            return {
              code: itemValue.value,
              label: `${itemValue.name || ''}`,
            };
          });
        this.form.get('currencyType').setValue(_.head(this.commonData.listCurrencyType)?.code);
        this.commonData.isRm = _.isEmpty(branchesOfUser);
        if (!this.commonData.isRm && this.commonData.isRegionDirector) {
          this.form.controls.reportBy.setValue('area');
        }
        this.commonData.listCurrencyUnit = _.orderBy(_.get(currencyUnits, 'content'), ['orderNum'], ['asc', 'desc']);
        this.commonData.listCurrencyUnit =
          _.get(currencyUnits, 'content')?.map((item) => {
            return { code: item.value, name: item.name, isDefault: item.isDefault };
          }) || [];
        this.commonData.listTerm = _.get(term, 'content')?.map((item) => {
          return {
            code: item.value,
            label: `${item.name || ''}`,
          };
        });
        this.commonData.listTerm = _.orderBy(this.commonData.listTerm, ['label'], ['desc', 'asc']);
        this.form.get('term').setValue(_.head(this.commonData.listTerm)?.code);

        const yearConfig = _.find(_.get(configYear, 'content'), (item) => item.code === 'HDV')?.value || 1;
        const yearConfig_Year = _.find(_.get(configYear, 'content'), (item) => item.code === 'HDV_YEAR')?.value || 1;
        const yearConfig_Daily = _.find(_.get(configYear, 'content'), (item) => item.code === 'HDV_DAILY')?.value || 44;
        const year = moment().year();
        for (let i = 0; i < yearConfig; i++) {
          this.commonData.listYears.push({
            value: year - i,
            label: year - i,
          });
        }
        for (let i = 0; i < yearConfig_Year; i++) {
          this.commonData.listYears_Year.push({
            value: year - i,
            label: year - i,
          });
        }
        this.maxMonth = _.find(_.get(configYear, 'content'), (item) => item.code === 'PTKH_MONTH_OPTION')?.value || 1;
        this.commonData.minDate = new Date(
          moment()
            .add(-(yearConfig - 1), 'year')
            .startOf('year')
            .valueOf()
        );
        this.commonData.minDateMonth = new Date(
          moment()
            .add(-(yearConfig - 1), 'year')
            .startOf('year')
            .valueOf()
        );
        this.form.get('reportPeriodYear').setValue(_.head(this.commonData.listYears)?.value);
        this.form.get('comparePeriodYear').setValue(_.head(this.commonData.listYears)?.value - 1);
        this.sessionService.setSessionData(SessionKey.REPORT_MOBILIZATION_V2, this.commonData);
        this.form.get('currencyUnit').setValue(_.head(this.commonData.listCurrencyUnit)?.code);
        this.commonData.listCurrencyUnit.forEach((item) => {
          if (item.isDefault) {
            this.form.get('currencyUnit').setValue(item?.code);
          }
        });
        this.isLoading = false;
        if (this.currUser.code && this.commonData.isRm && !this.commonData.isRegionDirector) {
          this.textSearch = this.currUser.code;
          this.add();
        }
      });
    } else {
      this.commonData = this.prop;
      const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
      this.commonData.listDivision =
        divisionOfUser?.map((item) => {
          return { code: item.code, name: `${item.code || ''} - ${item.name || ''}` };
        }) || [];
      this.form.get('division').setValue(_.head(this.commonData.listDivision)?.code);

      this.form.get('currencyUnit').setValue(_.head(this.commonData.listCurrencyUnit)?.code);
      this.commonData.listCurrencyUnit.forEach((item) => {
        if (item.isDefault) {
          this.form.get('currencyUnit').setValue(item?.code);
        }
      });
      this.form.get('currencyType').setValue(_.head(this.commonData.listCurrencyType)?.code);
      this.form.get('term').setValue(_.head(this.commonData.listTerm)?.code);
      this.isLoading = false;
      if (this.currUser.code && this.commonData.isRm && !this.commonData.isRegionDirector) {
        this.textSearch = this.currUser.code;
        this.add();
      }
    }
  }

  ngAfterViewInit() {
    this.form.controls.reportBy.valueChanges.subscribe((value) => {
      // if (this.tableType !== value) {
      this.allRM = false;
      this.paramsTable.page = 0;
      this.dataTerm[this.tableType] = {
        pageable: { ...this.pageable },
        term: [...this.termData],
      };
      this.textSearch = '';
      if (value === 'rm' || ((value === 'customer' || value === 'contract') && this.isDownloadReport('EXCEL'))) {
        this.columns[0].name = this.fields.rmCode;
        this.columns[1].name = this.fields.rmName;
        this.termData = [...this.dataTerm.rm.term];
        this.pageable = { ...this.dataTerm.rm.pageable };
        if (this.commonData.isRm) {
          this.textSearch = this.currUser.code;
          if (this.termData.length === 0 && this.currUser.code) {
            this.add();
          }
        } else {
          this.textSearch = '';
        }
      } else if ((value === 'customer' || value === 'contract') && this.isDownloadReport('HTML')) {
        this.columns[0].name = this.fields.customerCode;
        this.columns[1].name = this.fields.customerName;
        this.termData = [...this.dataTerm.customer.term];
        this.pageable = { ...this.dataTerm.customer.pageable };
      }
      if (!_.find(this.commonData.listDivision, (item) => item.code === this.form.get('division').value)) {
        this.form.get('division').setValue(_.head(this.commonData.listDivision)?.code);
      }
      this.tableType = value;
      this.mapData();
      // }
      // this.form.controls.reportTypeDetail.setValue('customer');
    });

    this.form.controls.reportType.valueChanges.subscribe((value) => {
      this.form.get('period').setValue('monthly');
      this.form.get('momentCompare').setValue(new Date(moment().add(-2, 'day').toDate()));
    });

    // this.form.controls.reportTypeDetail.valueChanges.subscribe((value) => {
    //   if (value === 'contract') {
    //     this.form.get('momentCompare').setValue(null);
    //     this.form.get('comparePeriodFrom').setValue(null);
    //     this.form.get('comparePeriodTo').setValue(null);
    //     this.form.get('comparePeriodYear').setValue(null);
    //     this.form.get('comparePeriodQuarterly').setValue(null);
    //   }
    // });

    this.form.controls.period.valueChanges.subscribe((value) => {
      this.form.get('comparePeriodFrom').setValue(new Date(moment().add(-2, 'months').toDate()));
      this.form.get('comparePeriodTo').setValue(null);
      this.commonData.listYearsCompare = this.commonData.listYears;
      this.commonData.listYears_YearCompare = this.commonData.listYears_Year;
      this.commonData.listQuarterlyCompare = this.commonData.listQuarterly;
      this.form.get('comparePeriodQuarterly').setValue(_.last(this.commonData.listQuarterly)?.value);
      this.form.get('comparePeriodYear').setValue(_.head(this.commonData.listYears)?.value - 1);
      if (value === 'monthly') {
        this.form.get('reportPeriodFrom').setValue(new Date(moment().add(-1, 'months').toDate()));
      }
      if (this.form.controls.reportType.value === 'moment') {
        this.form.controls.momentReport.setValue(moment().add(-1, value === 'daily' ? 'day' : 'months').toDate());
        this.form.controls.momentCompare.setValue(moment().add(-2, value === 'daily' ? 'day' : 'months').toDate());
      }
    });

    this.form.controls.division.valueChanges.subscribe(() => {
      this.removeList();
    });

    this.form.controls.downloadReport.valueChanges.subscribe((value => {
      if (value === 'EXCEL') {
        this.form.get('reportBy').setValue('customer');
      } else {
        if ((this.isReportBy('customer') || this.isReportBy('contract')) && this.isDownloadReport('HTML')) {
          this.columns[0].name = this.fields.customerCode;
          this.columns[1].name = this.fields.customerName;
          this.termData = [...this.dataTerm.customer.term];
          this.pageable = { ...this.dataTerm.customer.pageable };
        }
      }
    }));

    this.form.controls.division.valueChanges.subscribe((value => {
      if (this.isReportBy('contract') && value === 'INDIV') {
        this.form.get('reportBy').setValue('customer');
      }
    }));
  }

  search() {
    if (this.columns[0].name === this.fields.rmCode) {
      const formSearch = {
        crmIsActive: { value: 'true', disabled: true },
        rmBlock: {
          value: this.form.get('division').value,
          disabled: true,
        },
        // isNHSReport: this.form.get('division').value === Division.INDIV,
        // isReportByRm: this.commonData.isRm,
        // manager: true,
        // rm: true,
      };
      const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
      modal.componentInstance.dataSearch = formSearch;
      modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
      modal.result
        .then((res) => {
          if (res) {
            const listRMSelect = res.listSelected?.map((item) => {
              return {
                hrsCode: item?.hrisEmployee?.employeeId,
                code: item?.t24Employee?.employeeCode,
                name: item?.hrisEmployee?.fullName,
              };
            });
            this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
            this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);
            this.mapData();
          }
        })
        .catch(() => { });
    } else if (this.columns[0].name === this.fields.customerCode) {
      const formSearch = {
        customerType: {
          value: this.form.get('division').value,
          disabled: true,
        },
        isCheckGroupCBQL: { value: this.form.get('division').value !== Division.INDIV },
      };
      const modal = this.modalService.open(CustomerModalComponent, { windowClass: 'list__customer360-modal' });
      modal.componentInstance.listSelectedOld = this.termData;
      modal.componentInstance.dataSearch = formSearch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData =
              res.listSelected?.map((item) => {
                return {
                  code: item.customerCode,
                  customerCode: item.customerCode,
                  name: item.customerName ? item.customerName : item.name,
                };
              }) || [];
            this.mapData();
          }
        })
        .catch(() => { });
    }
  }

  add() {
    this.textSearch = _.trim(this.textSearch);
    if (this.textSearch.length === 0) {
      this.isLoading = false;
      return;
    }
    if (_.findIndex(this.termData, (i) => i.code?.toUpperCase() === this.textSearch?.toUpperCase()) === -1) {
      if (this.columns[0].name === this.fields.rmCode) {
        const params = {
          crmIsActive: true,
          scope: Scopes.VIEW,
          rmBlock: this.form.get('division').value,
          rsId: this.objFunction?.rsId,
          employeeCode: this.textSearch,
          page: 0,
          size: maxInt32,
          // isNHSReport: this.form.get('division').value === Division.INDIV,
          // isReportByRm: this.commonData.isRm,
          // rm: true,
          // manager: true,
        };
        this.isLoading = true;
        this.rmApi.post('findAll', params).subscribe(
          (data) => {
            const itemResult = _.find(
              _.get(data, 'content'),
              (item) => item?.t24Employee?.employeeCode?.toUpperCase() === this.textSearch?.toUpperCase()
            );
            if (itemResult) {
              this.termData?.push({
                code: itemResult?.t24Employee?.employeeCode,
                name: itemResult?.hrisEmployee?.fullName,
                hrsCode: itemResult?.hrisEmployee?.employeeId,
              });
              this.termData = _.uniqBy(this.termData, (i) => i.hrsCode);
              this.mapData();
            } else if (!itemResult && !this.allRM) {
              this.messageService.warn(_.get(this.notificationMessage, 'rmNotExistOrNotBranh'));
            }
            this.isLoading = false;
          },
          (e) => {
            this.messageService.error(this.notificationMessage.E001);
            this.isLoading = false;
          }
        );
      } else if (this.columns[0].name === this.fields.customerCode) {
        const params = {
          customerCode: this.textSearch,
          customerType: this.form.get('division').value,
          rsId: this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`)?.rsId,
          scope: Scopes.VIEW,
          pageNumber: 0,
          pageSize: global?.userConfig?.pageSize,
          isCheckGroupCBQL: this.form.get('division').value !== Division.INDIV,
        };
        this.isLoading = true;
        let api: Observable<any>;
        if (this.form.get('division').value !== Division.INDIV) {
          api = this.customerApi.searchSme(params);
        } else {
          api = this.customerApi.search(params);
        }
        api.subscribe(
          (data) => {
            if (data?.length > 0) {
              this.termData?.push({
                code: data[0]?.customerCode,
                name: data[0]?.customerName,
                customerCode: data[0]?.customerCode,
              });
              this.mapData();
            } else {
              this.messageService.warn(_.get(this.notificationMessage, 'customerNotExist'));
            }
            this.isLoading = false;
          },
          (e) => {
            this.messageService.error(this.notificationMessage.E001);
            this.isLoading = false;
          }
        );
      }
    } else {
      const messageReport = this.form.get('reportBy').value.toString() + 'IsExist';
      this.messageService.warn(_.get(this.notificationMessage, messageReport));
      this.isLoading = false;
      return;
    }
  }

  setPage(pageInfo) {
    this.paramsTable.page = pageInfo.offset;
    this.mapData();
  }

  mapData() {
    const total = this.termData.length;
    this.pageable.totalElements = total;
    this.pageable.totalPages = Math.floor(total / this.pageable.size);
    this.pageable.currentPage = this.paramsTable.page;
    const start = this.paramsTable.page * this.paramsTable.size;
    this.listDataTable = this.termData?.slice(start, start + this.paramsTable.size);
  }

  removeRecord(item) {
    _.remove(this.termData, (i) => i.code === item.code);
    _.remove(this.listDataTable, (i) => i.code === item.code);
    if (this.listDataTable.length === 0 && this.pageable.currentPage > 0) {
      this.paramsTable.page -= 1;
    }
    this.listDataTable = [...this.listDataTable];
    this.allRM = false;
    this.mapData();
  }

  viewReport() {
    if (this.isLoading) {
      return;
    }
    if (this.isDownloadReport('HTML')) {
      this.callApiTablue();
      return;
    } else if (this.isDownloadReport('EXCEL')) {
      this.callApiS3();
      return;
    }
  }

  loadFile(pdfId: string, paramSearch: any) {
    if (this.form.get('downloadReport').value === 'EXCEL') {
      forkJoin([this.kpiReportApi.loadFile(`download/${pdfId}`)]).subscribe(
        ([blobExcel]) => {
          saveAs(blobExcel, `${this.fileName}.xlsx`);
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      this.kpiReportApi.loadFile(`download/${pdfId}`).then(
        (blobPDF) => {
          const url = `${environment.url_endpoint_kpi}/ors-report/hdv`;
          if (this.form.get('downloadReport').value === 'HTML') {
            const modal = this.modalService.open(KpiReportModalComponent, { windowClass: 'tree__report-modal' });
            modal.componentInstance.data = blobPDF;
            modal.componentInstance.model = paramSearch;
            modal.componentInstance.baseUrl = url;
            modal.componentInstance.filename = this.fileName;
            modal.componentInstance.title = 'Báo cáo huy động vốn';
            modal.componentInstance.extendUrl = '';
          } else if (this.form.get('downloadReport').value === 'PDF') {
            saveAs(blobPDF, `${this.fileName}.pdf`);
          }
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    }
  }

  removeList() {
    this.termData = [];
    this.paramsTable.page = 0;
    this.allRM = false;
    this.mapData();
  }

  isShowCheckboxAllRm() {
    return (
      this.currUser?.branch !== BRANCH_HO &&
      !this.commonData?.isRm &&
      this.form.get('division')?.value !== Division.INDIV &&
      this.form.get('reportBy')?.value === 'rm'
    );
  }

  loadReport(embeddedLink: string, paramSearch: any) {
    const modal = this.modalService.open(KpiReportModalComponent, { windowClass: 'tree__report-modal' });
    modal.componentInstance.embeddedReport = embeddedLink;
    modal.componentInstance.data = null;
    modal.componentInstance.model = paramSearch;
    modal.componentInstance.baseUrl = null;
    modal.componentInstance.filename = this.fileName;
    modal.componentInstance.title = 'Báo cáo huy động vốn';
    modal.componentInstance.extendUrl = '';
    modal.componentInstance.reportType = 1;
  }

  callApiTablue() {
    let { momentReport, momentCompare, period } = this.form.getRawValue();
    const startDateReport = this.form.get('reportPeriodFrom').value;
    const startDateCompare = this.form.get('comparePeriodFrom').value;

    const currencyT = this.commonData.listCurrencyType.filter((i) => i.code === this.form.get('currencyType').value && i.code);
    const currencyU = this.commonData.listCurrencyUnit.filter((i) => i.code === this.form.get('currencyUnit').value && i.code);
    const division = this.form.get('division').value;
    const reportCode = this.getReportCodeTablue();
    let paramFr = '';
    let valueParamFr = '';
    let paramTo = '';
    let valueParamTo = '';
    let valueBr2 = '';
    let paramsTablue = {};
    let valueRgon = '';
    let valueRm = '';
    if (this.isReportBy('area')) {
      if (_.isEmpty(this.form.controls.areaCode.value)) {
        valueRgon = 'ALL';
      } else {
        this.form.controls.areaCode.value.filter((i, index) => {
          if (index === this.form.controls.areaCode.value.length - 1) {
            valueRgon += i;
          } else {
            valueRgon += i + ';';
          }
        });
      }
      if (this.isReportType('moment')) {
        if (!this.form.valid) {
          return;
        }
        if (period === 'monthly') {
          momentReport = moment(momentReport).endOf('month');
          momentCompare = moment(momentCompare).endOf('month');
        }
        paramFr = 'p_fr_date';
        valueParamFr = this.form.get('momentReport').value ? formatDate(momentReport, 'yyyyMMdd', 'en') : '';
        paramTo = 'p_to_date';
        valueParamTo = this.form.get('momentCompare').value ? formatDate(momentCompare, 'yyyyMMdd', 'en') : '';
        if (valueParamFr === valueParamTo) {
          this.messageService.warn(this.notificationMessage.momentCompareNoThanMomentReport);
          return;
        }
      } else if (this.form.get('period').value === 'yearly') {
        if (
          !_.isNull(this.form.get('comparePeriodYear').value) &&
          this.form.get('reportPeriodYear').value === this.form.get('comparePeriodYear').value
        ) {
          this.messageService.warn(this.notificationMessage.compareFromReportFrom);
          return;
        }
        paramFr = 'p_fr_y';
        valueParamFr = this.form.get('reportPeriodYear').value ? this.form.get('reportPeriodYear').value : '';
        paramTo = 'p_to_y';
        valueParamTo = this.form.get('comparePeriodYear').value ? this.form.get('comparePeriodYear').value : '';
      } else if (this.form.get('period').value === 'quarterly') {
        const reportPeriodQuarterly = this.form.get('reportPeriodQuarterly').value;
        const reportPeriodYear = this.form.get('reportPeriodYear').value;
        const comparePeriodQuarterly = this.form.get('comparePeriodQuarterly').value;
        const comparePeriodYear = this.form.get('comparePeriodYear').value;

        if (_.isNull(comparePeriodQuarterly) && !_.isNull(comparePeriodYear)) {
          this.messageService.warn(this.notificationMessage.quarterlyValidation);
          return;
        } else if (!_.isNull(comparePeriodQuarterly) && _.isNull(comparePeriodYear)) {
          this.messageService.warn(this.notificationMessage.yearValidation);
          return;
        } else if (reportPeriodQuarterly === comparePeriodQuarterly && reportPeriodYear === comparePeriodYear) {
          this.messageService.warn(this.notificationMessage.compareFromReportFrom);
          return;
        }
        paramFr = 'p_fr_q';
        valueParamFr = 'Q' + reportPeriodQuarterly + '-' + reportPeriodYear;
        paramTo = 'p_to_q';
        valueParamTo = _.isNull(comparePeriodYear) ? '' : 'Q' + comparePeriodQuarterly + '-' + comparePeriodYear;
      } else if (this.form.get('period').value === 'monthly') {
        if (
          !_.isNull(startDateCompare) &&
          formatDate(startDateReport, 'MM/yyyy', 'en') === formatDate(startDateCompare, 'MM/yyyy', 'en')
        ) {
          this.messageService.warn(this.notificationMessage.compareFromReportFrom);
          return;
        }
        paramFr = 'p_fr_m';
        valueParamFr = this.form.get('reportPeriodFrom').value
          ? formatDate(this.form.get('reportPeriodFrom').value, 'MM/yyyy', 'en') : '';
        paramTo = 'p_to_m';
        valueParamTo = this.form.get('comparePeriodFrom').value ? formatDate(this.form.get('comparePeriodFrom').value, 'MM/yyyy', 'en') : '';
        // if (valueParamFr.charAt(0) === '0') {
        //   valueParamFr = valueParamFr.substring(1);
        // }
        // if (valueParamTo.charAt(0) === '0') {
        //   valueParamTo = valueParamTo.substring(1);
        // }
      }

      paramsTablue = {
        scope: 'VIEW',
        rsId: this.objFunction.rsId,
        reportCode,
        reportParams: [
          {
            name: paramFr,
            value: valueParamFr
          },
          {
            name: paramTo,
            value: valueParamTo
          },
          {
            name: 'p_lob',
            value: division !== 'INDIV' ? division : division + ';NHS'
          },
          {
            name: 'p_ccy_code',
            value: currencyT.length > 0 ? currencyT[0].code : 'VND'
          },
          {
            name: 'p_unit',
            value: currencyU[0].code
          },
          {
            name: 'p_rgon',
            value: valueRgon
          }
        ]
      };
    } else if (this.isReportBy('branch')) {
      if (this.isListOptionAll()) {
        valueBr2 = 'ALL';
      } else {
        if (_.isEmpty(this.branchCodes)) {
          this.messageService.warn('Vui lòng chọn chi nhánh');
          return;
        }

        if (this.branchCodes.length > this.countTablueBranch) {
          this.translate.get('notificationMessage.countBranchMaxINDIV', { number: this.countTablueBranch }).subscribe((res) => {
            this.messageService.warn(res);
          });
          return;
        }
        this.branchCodes.forEach((i, index) => {
          if (index === this.branchCodes.length - 1) {
            valueBr2 += i.code;
          } else {
            valueBr2 += i.code + ';';
          }
        });
      }
      if (this.isReportType('moment')) {
        if (period === 'monthly') {
          momentReport = moment(momentReport).endOf('month');
          momentCompare = moment(momentCompare).endOf('month');
        }
        paramFr = 'p_fr_date';
        valueParamFr = this.form.get('momentReport').value ? formatDate(momentReport, 'yyyyMMdd', 'en') : '';
        paramTo = 'p_to_date';
        valueParamTo = this.form.get('momentCompare').value ? formatDate(momentCompare, 'yyyyMMdd', 'en') : '';

        if (valueParamFr === valueParamTo) {
          this.messageService.warn(this.notificationMessage.momentCompareNoThanMomentReport);
          return;
        }
      } else if (this.form.get('period').value === 'yearly') {
        if (
          !_.isNull(this.form.get('comparePeriodYear').value) &&
          this.form.get('reportPeriodYear').value === this.form.get('comparePeriodYear').value
        ) {
          this.messageService.warn(this.notificationMessage.compareFromReportFrom);
          return;
        }
        paramFr = 'p_fr_y';
        valueParamFr = this.form.get('reportPeriodYear').value ? this.form.get('reportPeriodYear').value : '';
        paramTo = 'p_to_y';
        valueParamTo = this.form.get('comparePeriodYear').value ? this.form.get('comparePeriodYear').value : '';
      } else if (this.form.get('period').value === 'quarterly') {
        const reportPeriodQuarterly = this.form.get('reportPeriodQuarterly').value;
        const reportPeriodYear = this.form.get('reportPeriodYear').value;
        const comparePeriodQuarterly = this.form.get('comparePeriodQuarterly').value;
        const comparePeriodYear = this.form.get('comparePeriodYear').value;

        if (_.isNull(comparePeriodQuarterly) && !_.isNull(comparePeriodYear)) {
          this.messageService.warn(this.notificationMessage.quarterlyValidation);
          return;
        } else if (!_.isNull(comparePeriodQuarterly) && _.isNull(comparePeriodYear)) {
          this.messageService.warn(this.notificationMessage.yearValidation);
          return;
        } else if (reportPeriodQuarterly === comparePeriodQuarterly && reportPeriodYear === comparePeriodYear) {
          this.messageService.warn(this.notificationMessage.compareFromReportFrom);
          return;
        }
        paramFr = 'p_fr_q';
        valueParamFr = 'Q' + reportPeriodQuarterly + '-' + reportPeriodYear;
        paramTo = 'p_to_q';
        valueParamTo = _.isNull(comparePeriodYear) ? '' : 'Q' + comparePeriodQuarterly + '-' + comparePeriodYear;
      } else if (this.form.get('period').value === 'monthly') {
        if (
          !_.isNull(startDateCompare) &&
          formatDate(startDateReport, 'MM/yyyy', 'en') === formatDate(startDateCompare, 'MM/yyyy', 'en')
        ) {
          this.messageService.warn(this.notificationMessage.compareFromReportFrom);
          return;
        }
        paramFr = 'p_fr_m';
        // tslint:disable-next-line:max-line-length
        valueParamFr = this.form.get('reportPeriodFrom').value ? formatDate(this.form.get('reportPeriodFrom').value, 'MM/yyyy', 'en') : '';
        paramTo = 'p_to_m';
        valueParamTo = this.form.get('comparePeriodFrom').value
          ? formatDate(this.form.get('comparePeriodFrom').value, 'MM/yyyy', 'en') : '';

        // if (valueParamFr.charAt(0) === '0') {
        //   valueParamFr = valueParamFr.substring(1)
        // }
        // if (valueParamTo.charAt(0) === '0') {
        //   valueParamTo = valueParamTo.substring(1)
        // }
      }

      paramsTablue = {
        reportCode,
        scope: 'VIEW',
        rsId: this.objFunction.rsId,
        reportParams: [
          {
            name: paramFr,
            value: valueParamFr
          },
          {
            name: paramTo,
            value: valueParamTo
          },
          {
            name: 'p_lob',
            value: division !== 'INDIV' ? division : division + ';NHS'
          },
          {
            name: 'p_ccy_code',
            value: currencyT.length > 0 ? currencyT[0].code : 'VND'
          },
          {
            name: 'p_unit',
            value: currencyU[0].code
          },
          {
            name: 'p_br_code_lv2',
            value: valueBr2
          },
        ]
      };
    } else if (this.isReportBy('rm')) {
      if (this.isListOptionAll()) {
        valueRm = 'ALL'
      } else {
        if (_.isEmpty(this.termData)) {
          this.messageService.warn('Vui lòng chọn RM');
          return;
        }

        if (this.termData.length > this.countTablueRm) {
          this.translate.get('notificationMessage.countRmMax', { number: this.countTablueRm }).subscribe((res) => {
            this.messageService.warn(res);
          });
          return;
        }
        this.termData.forEach((i, index) => {
          if (index === this.termData.length - 1) {
            valueRm += i.code
          } else {
            valueRm += i.code + ';';
          }
        });
      }

      if (this.isReportType('moment')) {
        if (period === 'monthly') {
          momentReport = moment(momentReport).endOf('month');
          momentCompare = moment(momentCompare).endOf('month');
        }
        paramFr = 'p_fr_date';
        valueParamFr = this.form.get('momentReport').value ? formatDate(momentReport, 'yyyyMMdd', 'en') : '';
        paramTo = 'p_to_date';
        valueParamTo = this.form.get('momentCompare').value ? formatDate(momentCompare, 'yyyyMMdd', 'en') : '';
        // valueBr1 = _.chain(this.branches).filter()
        if (valueParamFr === valueParamTo) {
          this.messageService.warn(this.notificationMessage.momentCompareNoThanMomentReport);
          return;
        }
      } else if (this.form.get('period').value === 'yearly') {
        if (
          !_.isNull(this.form.get('comparePeriodYear').value) &&
          this.form.get('reportPeriodYear').value === this.form.get('comparePeriodYear').value
        ) {
          this.messageService.warn(this.notificationMessage.compareFromReportFrom);
          return;
        }
        paramFr = 'p_fr_y';
        valueParamFr = this.form.get('reportPeriodYear').value ? this.form.get('reportPeriodYear').value : '';
        paramTo = 'p_to_y';
        valueParamTo = this.form.get('comparePeriodYear').value ? this.form.get('comparePeriodYear').value : '';
      } else if (this.form.get('period').value === 'quarterly') {
        const reportPeriodQuarterly = this.form.get('reportPeriodQuarterly').value;
        const reportPeriodYear = this.form.get('reportPeriodYear').value;
        const comparePeriodQuarterly = this.form.get('comparePeriodQuarterly').value;
        const comparePeriodYear = this.form.get('comparePeriodYear').value;

        if (_.isNull(comparePeriodQuarterly) && !_.isNull(comparePeriodYear)) {
          this.messageService.warn(this.notificationMessage.quarterlyValidation);
          return;
        } else if (!_.isNull(comparePeriodQuarterly) && _.isNull(comparePeriodYear)) {
          this.messageService.warn(this.notificationMessage.yearValidation);
          return;
        } else if (reportPeriodQuarterly === comparePeriodQuarterly && reportPeriodYear === comparePeriodYear) {
          this.messageService.warn(this.notificationMessage.compareFromReportFrom);
          return;
        }
        paramFr = 'p_fr_q';
        valueParamFr = 'Q' + reportPeriodQuarterly + '-' + reportPeriodYear;
        paramTo = 'p_to_q';
        valueParamTo = _.isNull(comparePeriodYear) ? '' : 'Q' + comparePeriodQuarterly + '-' + comparePeriodYear;
      } else if (this.form.get('period').value === 'monthly') {
        if (
          !_.isNull(startDateCompare) &&
          formatDate(startDateReport, 'MM/yyyy', 'en') === formatDate(startDateCompare, 'MM/yyyy', 'en')
        ) {
          this.messageService.warn(this.notificationMessage.compareFromReportFrom);
          return;
        }
        paramFr = 'p_fr_m';
        // tslint:disable-next-line:max-line-length
        valueParamFr = this.form.get('reportPeriodFrom').value ? formatDate(this.form.get('reportPeriodFrom').value, 'MM/yyyy', 'en') : '';
        paramTo = 'p_to_m';
        valueParamTo = this.form.get('comparePeriodFrom').value
          ? formatDate(this.form.get('comparePeriodFrom').value, 'MM/yyyy', 'en') : '';
        // if (valueParamFr.charAt(0) === '0') {
        //   valueParamFr = valueParamFr.substring(1)
        // }
        // if (valueParamTo.charAt(0) === '0') {
        //   valueParamTo = valueParamTo.substring(1)
        // }
      }

      paramsTablue = {
        scope: 'VIEW',
        rsId: this.objFunction.rsId,
        reportCode,
        reportParams: [
          {
            name: paramFr,
            value: valueParamFr
          },
          {
            name: paramTo,
            value: valueParamTo
          },
          {
            name: 'p_lob',
            value: division !== 'INDIV' ? division : division + ';NHS'
          },
          {
            name: 'p_ccy_code',
            value: currencyT.length > 0 ? currencyT[0].code : 'VND'
          },
          {
            name: 'p_unit',
            value: currencyU[0].code
          },
          {
            name: 'p_rm_code',
            value: this.commonData.isRm ? this.currUser.code : valueRm
          },
          {
            name: 'p_br_code_lv2',
            value: this.commonData.isRm ? this.currUser?.branch : ''
          },
        ]
      };
    } else if (this.isReportBy('contract') || this.isReportBy('customer')) {
      if (_.isEmpty(this.termData)) {
        this.messageService.warn('Vui lòng chọn Khách hàng');
        return;
      }

      if (this.termData.length > this.countTablueCustomerMax) {
        this.translate.get('notificationMessage.countCustomerMaxINDIV', { number: this.countTablueCustomerMax }).subscribe((res) => {
          this.messageService.warn(res);
        });
        return;
      }
      this.termData.forEach((i, index) => {
        if (index === this.termData.length - 1) {
          valueRm += i.code;
        } else {
          valueRm += i.code + ';';
        }
      });
      if (this.isReportType('moment')) {
        if (period === 'monthly') {
          momentReport = moment(momentReport).endOf('month');
        }
        paramFr = 'p_fr_date';
        valueParamFr = this.form.get('momentReport').value ? formatDate(momentReport, 'yyyyMMdd', 'en') : '';
      } else if (this.form.get('period').value === 'yearly') {
        paramFr = 'p_fr_y';
        valueParamFr = this.form.get('reportPeriodYear').value ? this.form.get('reportPeriodYear').value : '';
      } else if (this.form.get('period').value === 'quarterly') {
        const reportPeriodQuarterly = this.form.get('reportPeriodQuarterly').value;
        const reportPeriodYear = this.form.get('reportPeriodYear').value;
        paramFr = 'p_fr_q';
        valueParamFr = 'Q' + reportPeriodQuarterly + '-' + reportPeriodYear;
      } else if (this.form.get('period').value === 'monthly') {
        paramFr = 'p_fr_m';
        valueParamFr = this.form.get('reportPeriodFrom').value
          ? formatDate(this.form.get('reportPeriodFrom').value, 'MM/yyyy', 'en') : '';
        // if (valueParamFr.charAt(0) === '0') {
        //   valueParamFr = valueParamFr.substring(1)
        // }
      }

      paramsTablue = {
        scope: 'VIEW',
        rsId: this.objFunction.rsId,
        reportCode,
        reportParams: [
          {
            name: paramFr,
            value: valueParamFr
          },
          {
            name: 'p_lob',
            value: division !== 'INDIV' ? division : division + ';NHS'
          },
          {
            name: 'p_ccy_code',
            value: currencyT.length > 0 ? currencyT[0].code : 'VND'
          },
          {
            name: 'p_rm_code',
            value: this.commonData.isRm ? this.currUser.code : 'ALL'
          },
          {
            name: 'p_br_code_lv2',
            value: this.commonData.isRm ? this.currUser.branch : ''
          },
          {
            name: 'p_customer_id',
            // value: 'ALL'
            value: valueRm
          },
          {
            name: 'p_unit',
            value: currencyU[0].code
          },
        ]
      };
    }

    this.isLoading = true;
    forkJoin([this.kpiReportApi.getTableauReport(paramsTablue)]).subscribe(
      ([embeddedLink]) => {
        if (embeddedLink.length > 0) {
          this.loadReport(embeddedLink, paramsTablue);
        } else {
          this.messageService.error(this.notificationMessage.E001);
        }
        this.isLoading = false;
      },
      () => {
        this.messageService.error(this.notificationMessage.E001);
        this.isLoading = false;
      }
    );
  }

  callApiS3() {
    const reportCode = this.getReportCodeS3();
    let prdId = '';
    let branches = '';
    let rm = '';

    // validate
    if (this.isReportTypeDetail('branch')) {
      if (_.isEmpty(this.branchCodes)) {
        this.messageService.warn('Vui lòng chọn chi nhánh');
        return;
      }
      this.branchCodes.forEach(i => {
        branches += i.code + ',';
      });
    } else {
      if (_.isEmpty(this.listDataTable)) {
        this.messageService.warn('Vui lòng chọn RM');
        return;
      }
      // if (this.listDataTable.length > this.countRm) {
      //   this.translate.get('notificationMessage.countRmMax', { number: this.countRm }).subscribe((res) => {
      //     this.messageService.warn(res);
      //   });
      //   return;
      // }
      this.termData.forEach((i, index) => {
        if (index === this.termData.length - 1) {
          rm += i.code
        } else {
          rm += i.code + ',';
        }
      });
    }


    // Tạo params
    if (this.isReportType('moment')) {
      let { momentReport, momentCompare, period } = this.form.getRawValue();
      if (period === 'monthly') {
        momentReport = moment(momentReport).endOf('month');
        momentCompare = moment(momentCompare).endOf('month');
      }
      prdId = this.form.get('momentReport').value ? formatDate(momentReport, 'yyyyMMdd', 'en') : '';
    } else if (this.form.get('period').value === 'yearly') {
      const y = (this.form.get('reportPeriodYear').value);
      const lastDayOfYear = new Date(y, 11, 31);
      prdId = formatDate(lastDayOfYear, 'yyyyMMdd', 'en');
    } else if (this.form.get('period').value === 'quarterly') {
      const q = this.form.get('reportPeriodQuarterly').value;
      const y = this.form.get('reportPeriodYear').value;
      switch (q) {
        case 1:
          prdId = y + '0331';
          break;
        case 2:
          prdId = y + '0630';
          break;
        case 3:
          prdId = y + '0930';
          break;
        case 4:
          prdId = y + '1231';
          break;
      }
    } else if (this.form.get('period').value === 'monthly') {
      const y = (this.form.get('reportPeriodFrom').value).getFullYear();
      const m = (this.form.get('reportPeriodFrom').value).getMonth();
      const lastDayOfMonth = new Date(y, m + 1, 0);
      prdId = formatDate(lastDayOfMonth, 'yyyyMMdd', 'en');
    }

    const paramsS3 = {
      reportCode,
      reportParams: [
        {
          name: 'prd_id',
          value: prdId
        },
        {
          name: 'field01',
          value: this.isReportTypeDetail('branch') ? branches : rm
        },
        {
          name: 'field02',
          value: this.isReportTypeDetail('branch') ? (this.form.controls.division.value === 'INDIV' ? 'INDIV,NHS' : this.form.controls.division.value) : 'ALL'
          // value: 'ALL'
        }],
      rsId: this.objFunction.rsId,
      scope: 'VIEW'
    }
    this.isLoading = true;
    this.kpiReportApi.reportS3(paramsS3).subscribe(urls => {
      this.isLoading = false;
      if (!_.isEmpty(urls)) {
        const download = (urls) => {
          const url = urls.pop();
          window.open(url, '_self');

          if (!urls.length && this.intervalDownloadFileS3) {
            clearInterval(this.intervalDownloadFileS3);
          }
        }

        this.intervalDownloadFileS3 = setInterval(download, 1000, urls);

      } else {
        this.confirmService.warn('Không có file dữ liệu');
      }
    }, error => {
      this.isLoading = false;
      this.messageService.error(this.notificationMessage.error);
    });
  }

  onModelChange($event) {
    this.branchCodes = $event;
  }

  isReportBy(value: string) {
    return this.form.get('reportBy').value === value;
  }

  isReportTypeDetail(value: string) {
    return this.form.get('reportTypeDetail').value === value;
  }

  isReportType(value: string) {
    return this.form.get('reportType').value === value;
  }

  isDownloadReport(value: string) {
    return this.form.get('downloadReport').value === value;
  }

  isListOptionAll() {
    return this.form.get('listOption').value === 'ALL';
  }

  getReportCodeS3() {
    let reportCode = 'HDV';
    switch (this.form.get('reportBy').value) {
      case 'customer':
        reportCode += '_KH';
        break;
      case 'contract':
        reportCode += '_HD';
        break;
    }

    if (this.form.get('reportType').value === 'moment') {
      reportCode += '_DAILY';
    } else {
      switch (this.form.get('period').value) {
        case 'monthly':
          reportCode += '_MONTH';
          break;
        case 'quarterly':
          reportCode += '_QUARTER';
          break;
        case 'yearly':
          reportCode += '_YEAR';
          break;
      }
    }

    switch (this.form.get('reportTypeDetail').value) {
      case 'branch':
        reportCode += '_BRANCH_CODE';
        break;
      case 'rm':
        reportCode += '_RM';
        break;
    }

    return reportCode;
  }

  getReportCodeTablue() {
    let reportCode = 'HDVBQ';
    switch (this.form.get('reportBy').value) {
      case 'rm':
        reportCode += '_RM';
        break;
      case 'area':
        reportCode += '_AREA';
        break;
      case 'branch':
        reportCode += '_BRANCH';
        break;
      case 'contract':
        reportCode += '_HD'
        break;
      case 'customer':
        reportCode += '_CUSTOMER'
    }

    if (this.form.get('reportType').value === 'moment') {
      reportCode += '_DAILY';
    } else {
      switch (this.form.get('period').value) {
        case 'monthly':
          reportCode += '_MONTH';
          break;
        case 'quarterly':
          reportCode += '_QUARTER';
          break;
        case 'yearly':
          reportCode += '_YEAR';
          break;
      }
    }

    return reportCode;
  }

  initRegionList(list: any[]): void {
    this.commonData.regionList = list;
    if (this.commonData.isHO) {
      return;
    } else {
      const areaCodes = [];
      this.commonData?.regionList?.forEach((item) => {
        if (
          this.commonData.branches.findIndex((i) => i.khuVucM === item?.locationCode) !== -1
        ) {
          areaCodes.push(item);
        }
      });
      this.commonData.regionList = areaCodes;
    }
  }
}
