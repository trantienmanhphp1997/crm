import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportGapComponent } from './report-gap.component';

describe('ReportGapComponent', () => {
  let component: ReportGapComponent;
  let fixture: ComponentFixture<ReportGapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportGapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportGapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
