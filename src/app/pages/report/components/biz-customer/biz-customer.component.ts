import { formatDate } from '@angular/common';
import { forkJoin, Observable, of } from 'rxjs';
import { Component, OnInit, Injector, AfterViewInit } from '@angular/core';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { TableColumn } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import {
  BRANCH_HO,
  CommonCategory,
  Division,
  FunctionCode,
  maxInt32,
  Scopes,
  SessionKey,
} from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as moment from 'moment';
import { catchError, finalize } from 'rxjs/operators';
import { CustomerApi } from 'src/app/pages/customer-360/apis';
import { KpiReportApi } from '../../apis';
import { KpiReportModalComponent } from '../../dialogs';
import { BranchApi, RmApi } from 'src/app/pages/rm/apis';
import { RmModalComponent } from 'src/app/pages/rm/components/rm-modal/rm-modal.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { CustomerModalComponent } from 'src/app/pages/customer-360/components';
import { CategoryRegionApi } from '../../apis/category-region.api';
import { DashboardService } from 'src/app/pages/dashboard/services/dashboard.service';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { LocationComponent } from 'src/app/pages/lead/components/location/location.component';
import { CustomerLeadService } from 'src/app/pages/lead/service/customer-lead.service';
import {
  ChooseBranchesModalComponent
} from '../../../system/components/choose-branches-modal/choose-branches-modal.component';
import { Utils } from '../../../../core/utils/utils';

@Component({
  selector: 'app-biz-customer',
  templateUrl: './biz-customer.component.html',
  styleUrls: ['./biz-customer.component.scss']
})
export class BizCustomerComponent extends BaseComponent implements OnInit, AfterViewInit {
  commonData = {
    minDate: null,
    maxDate: new Date(),
    isRm: true,
    isRegionDirector: false,
    listBranch: [],
    regionList: [],
    targetList: [],
    channelList: [],
    listRegionDirectorTitle: [],
    listYears: [],
    listBranchs: [],
  };
  textSearch: string;
  paramsTable = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };

  form = this.fb.group({
    momentReport: [new Date(moment().add(-1, 'day').toDate()), CustomValidators.required],
    momentReportMonth: [new Date(moment().add(-1, 'month').toDate()), CustomValidators.required],
    period: 'daily',
    reportBy: 'rm',
    downloadReport: 'HTML',
    listOption: 'ALL',
    target: [''],
    channel: [''],
    branchCodeLv1: [[]],
    branchCode: [''],
    regionCode: '',
    reportYear: '',
    page: 1,
  });
  allRM = false;
  dataTerm = {
    rm: {
      pageable: { ...this.pageable },
      term: [],
    },
    branch: {
      pageable: { ...this.pageable },
      term: [],
    },
    customer: {
      pageable: { ...this.pageable },
      term: [],
    }
  };
  tableType: string;
  termData = [];
  listDataTable = [];
  columns: TableColumn[];
  maxDate = new Date();
  fileName = 'bao-cao-KH-Biz';
  countRm: any;
  countBranchMax: number;
  countCustomerMax: number;
  maxMonth: any;
  regions = {};
  allBranchLv2 = [];
  allBranchLv1 = [];
  listBranchTerm = [];
  branchLv1List = [];
  branchLv2List = [];
  lenghtBranchesOfUser: number;
  defaultDivision = Division.SME;
  isHO = false;
  objFunctionReport: any;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private customerApi: CustomerApi,
    private rmApi: RmApi,
    private branchApi: BranchApi,
    private kpiReportApi: KpiReportApi,
    private regionApi: CategoryRegionApi,
    private dashboardService: DashboardService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.objFunctionReport = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REPORT_BIZ_CUSTOMER}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.tableType = this.form.get('reportBy').value;
    this.columns = [
      {
        name: this.fields.rmCode,
        prop: 'code',
      },
      {
        name: this.fields.rmName,
        prop: 'name',
      },
    ];

    this.isHO = this.currUser?.branch === BRANCH_HO;

    forkJoin([
      this.categoryService
        .getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined))),

      this.categoryService
        .getCommonCategory(CommonCategory.REPORTS_MAX_LENGTH_RM)
        .pipe(catchError(() => of(undefined))),

      this.dashboardService.getBranchByDomain({
        rsId: this.objFunction?.rsId,
        scope: Scopes.VIEW
      }),

      this.categoryService
        .getCommonCategory(CommonCategory.CONFIG_REPORT_TARGET_TYPE)
        .pipe(catchError(() => of(undefined))),

      this.categoryService
        .getCommonCategory(CommonCategory.CONFIG_REPORT_CHANNEL)
        .pipe(catchError(() => of(undefined))),

      this.commonService.getCommonCategory(CommonCategory.REPORTS_YEARS).pipe(catchError(() => of(undefined))),

    ]).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(([branchesOfUser, configReport, branchesByDomain, targetType, channel, configYear]) => {

      this.countRm =
        _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.TABLEAU_MAX_RM_REPORT)?.value ||
        0;
      this.countBranchMax =
        _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.TABLEAU_MAX_BRANCH_REPORT)?.value ||
        0;
      this.countCustomerMax =
        _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.TABLEAU_MAX_CUSTOMER_REPORT)?.value || 0;


      this.commonData.listBranch = branchesOfUser || [];
      this.commonData.isRm = _.isEmpty(branchesOfUser);
      this.lenghtBranchesOfUser = branchesOfUser.length;
      this.branchLv2List = branchesOfUser;

      this.commonData.targetList = targetType.content?.map(item => ({ value: item.value, name: item.name })) || [];
      this.commonData.channelList = channel.content?.map(item => ({ value: item.value, name: item.name })) || [];
      this.commonData.isRegionDirector = this.objFunctionReport.scopes.includes('VIEW_HISTORY');

      this.form.controls.channel.setValue(this.commonData.channelList[0].value);
      this.form.controls.target.setValue(this.commonData.targetList[0].value);
      const yearConfig = _.find(_.get(configYear, 'content'), (item) => item.code === 'NEW_CUSTOMER')?.value || 5;
      const year = moment().year();
      for (let i = 0; i < yearConfig; i++) {
        this.commonData.listYears.push({
          value: year - i,
          label: year - i,
        });
      }
      this.form.get('reportYear').setValue(_.head(this.commonData.listYears)?.value);
      this.initBranch(branchesByDomain);
      this.setBranchOfUser();
    });

    this.textSearch = this.currUser.code;
    if (this.currUser.code) {
      this.add();
    }
  }

  ngAfterViewInit() {
    this.setValueChanges();
  }

  search() {
    let option = this.getOption;
    if (option === 'rm') {
      const formSearch = {
        crmIsActive: { value: 'true', disabled: true },
        rmBlock: {
          value: this.defaultDivision,
          disabled: true,
        },
        isReportByRm: this.commonData.isRm,
        manager: true,
        rm: true,
      };
      const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
      modal.componentInstance.dataSearch = formSearch;
      modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
      modal.result
        .then((res) => {
          if (res) {
            const listRMSelect = res.listSelected?.map((item) => {
              return {
                hrsCode: item?.hrisEmployee?.employeeId,
                code: item?.t24Employee?.employeeCode,
                name: item?.hrisEmployee?.fullName,
              };
            });
            this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
            this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);
            this.mapData();
          }
        })
        .catch(() => { });
    } else if (option === 'customer') {
      const formSearch = {
        customerType: {
          value: this.defaultDivision,
          disabled: true,
        },
      };
      const modal = this.modalService.open(CustomerModalComponent, { windowClass: 'list__customer360-modal' });
      modal.componentInstance.listSelectedOld = this.termData;
      modal.componentInstance.dataSearch = formSearch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData =
              res.listSelected?.map((item) => {
                return {
                  code: item.customerCode,
                  customerCode: item.customerCode,
                  name: item.customerName ? item.customerName : item.name,
                };
              }) || [];
            this.mapData();
          }
        })
        .catch(() => { });
    } else if (option === 'branch') {
      const modal = this.modalService.open(ChooseBranchesModalComponent, { windowClass: 'tree__branches-modal' });
      modal.componentInstance.listBranchOld = _.map(this.termData, (x) => x.code) || [];
      modal.componentInstance.listBranch = this.commonData.listBranch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData = res;
            this.mapData();
          }
        })
        .catch(() => {
        });
    }
  }

  add() {
    if (this.hidePickupList) {
      return;
    }

    this.textSearch = _.trim(this.textSearch);
    if (this.textSearch.length === 0) {
      this.isLoading = false;
      return;
    }
    let option = this.getOption;
    if (_.findIndex(this.termData, (i) => i.code?.toUpperCase() === this.textSearch?.toUpperCase()) === -1) {
      this.isLoading = true;
      if (option === 'rm') {
        const params = {
          crmIsActive: true,
          scope: Scopes.VIEW,
          rmBlock: this.defaultDivision,
          rsId: this.objFunction?.rsId,
          employeeCode: this.textSearch,
          page: 0,
          size: maxInt32,
          isReportByRm: this.commonData.isRm,
          rm: true,
          manager: true,
        };
        this.isLoading = true;
        this.rmApi.post('findAll', params).subscribe(
          (data) => {
            const itemResult = _.find(
              _.get(data, 'content'),
              (item) => item?.t24Employee?.employeeCode?.toUpperCase() === this.textSearch?.toUpperCase()
            );
            if (itemResult) {
              this.termData?.push({
                code: itemResult?.t24Employee?.employeeCode,
                name: itemResult?.hrisEmployee?.fullName,
                hrsCode: itemResult?.hrisEmployee?.employeeId,
              });
              this.termData = _.uniqBy(this.termData, (i) => i.hrsCode);
              this.mapData();
            } else if (!itemResult && !this.allRM) {
              this.messageService.warn(_.get(this.notificationMessage, 'rmNotExistOrNotBranh'));
            }
            this.isLoading = false;
          },
          () => {
            this.messageService.error(this.notificationMessage.E001);
            this.isLoading = false;
          }
        );
      } else if (option === 'customer') {
        const params = {
          customerCode: this.textSearch,
          customerType: this.defaultDivision,
          rsId: this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`)?.rsId,
          scope: Scopes.VIEW,
          pageNumber: 0,
          pageSize: global?.userConfig?.pageSize,
        };

        this.customerApi.searchSme(params).pipe(
          finalize(() => {
            this.isLoading = false;
          })
        ).subscribe((data) => {
          if (data?.length > 0) {
            this.termData?.push({
              code: data[0]?.customerCode,
              name: data[0]?.customerName,
              customerCode: data[0]?.customerCode,
            });
            this.mapData();
          } else {
            this.messageService.warn(_.get(this.notificationMessage, 'customerNotExist'));
          }
        }, () => {
          this.messageService.error(this.notificationMessage.E001);
        });
      } else if (option === 'branch') {
        const itemResult = _.find(
          this.commonData.listBranch,
          (item) => item.code?.toUpperCase() === this.textSearch?.toUpperCase()
        );
        if (itemResult) {
          this.termData?.push(itemResult);
          this.mapData();
        } else {
          this.messageService.warn(_.get(this.notificationMessage, 'branchNotExistOrNotInBranch'));
        }
        this.isLoading = false;
      }
    } else {
      const messageReport = option.toString() + 'IsExist';
      this.messageService.warn(_.get(this.notificationMessage, messageReport));
      this.isLoading = false;
      return;
    }
  }

  setPage(pageInfo) {
    this.paramsTable.page = pageInfo.offset;
    this.mapData();
  }

  mapData() {
    const total = this.termData.length;
    this.pageable.totalElements = total;
    this.pageable.totalPages = Math.floor(total / this.pageable.size);
    this.pageable.currentPage = this.paramsTable.page;
    const start = this.paramsTable.page * this.paramsTable.size;
    this.listDataTable = this.termData?.slice(start, start + this.paramsTable.size);
  }

  removeRecord(item) {
    _.remove(this.termData, (i) => i.code === item.code);
    _.remove(this.listDataTable, (i) => i.code === item.code);
    if (this.listDataTable.length === 0 && this.pageable.currentPage > 0) {
      this.paramsTable.page -= 1;
    }
    this.listDataTable = [...this.listDataTable];
    this.allRM = false;
    this.mapData();
  }

  viewReport() {
    if (this.isLoading) {
      return;
    }

    if (!this.form.valid) {
      validateAllFormFields(this.form);
      return;
    }

    const formData = this.form.getRawValue();
    const { momentReportMonth, momentReport, reportBy, target, period, reportYear } = formData;
    const isNEW_BIZ_LOAN = target == 'NEW_BIZ_LOAN';
    let codes = this.termData.map(item => item.code).join(';');
    codes = this.form.get('listOption').value === 'ALL' ? 'ALL' : codes;

    // this.validateTermList(reportBy);
    const isTermDataEmpty = _.isEmpty(this.termData);
    const isOptionAll = this.form.get('listOption').value === 'ALL';
    let option = this.getOption;
    if (!isOptionAll) {
      if (option === 'branch') {
        if (isTermDataEmpty && reportBy != 'branch') {
          this.messageService.warn(_.get(this.notificationMessage, 'branchValidation'));
          return;
        }
        if (this.termData.length > this.countBranchMax && reportBy != 'branch') {
          this.translate.get('notificationMessage.countBranchMax', { number: this.countBranchMax }).subscribe((res) => {
            this.messageService.warn(res);
          });
          return;
        }
        if (this.commonData.isRm) {
          this.messageService.warn(_.get(this.notificationMessage, 'branchValidation'));
          return;
        }
      } else if (option === 'rm') {
        if (isTermDataEmpty && !isOptionAll) {
          this.messageService.warn(_.get(this.notificationMessage, 'rmValidation'));
          return;
        }
        if (this.termData.length > this.countRm) {
          this.translate.get('notificationMessage.countRmMax', { number: this.countRm }).subscribe((res) => {
            this.messageService.warn(res);
          });
          return;
        }
      } else if (option === 'customer') {
        if (isTermDataEmpty && !isOptionAll) {
          this.messageService.warn(_.get(this.notificationMessage, 'customerValidation'));
          return;
        }
        if (this.termData.length > this.countCustomerMax) {
          this.translate.get('notificationMessage.countCustomerLimit', { number: this.countCustomerMax }).subscribe((res) => {
            this.messageService.warn(res);
          });
          return;
        }
      }
    }

    const base = [
      { name: "p_lob", value: this.defaultDivision }
    ];

    let params = {
      reportCode: '',
      reportParams: [
        ...base
      ],
      rsId: this.objFunction?.rsId,
      scope: Scopes.VIEW
    };

    const setWithReportBy = (targetCode: string) => {
      const isNewCustomer = targetCode === 'NEW_BIZ_NEW_CODE';
      let p_time = '';
      let type_of_time = '';
      let option = this.getOption;
      if (isNewCustomer) {
        switch (period) {
          case 'daily':
          type_of_time = 'Chọn ngày';
          p_time = formatDate(momentReport, 'dd/MM/yyyy', 'en')
          break;
          case 'monthly':
            type_of_time = 'Chọn tháng';
            p_time = formatDate(momentReportMonth, 'MM/yyyy', 'en')
          break;
          case 'yearly':
            type_of_time = 'Chọn năm';
            p_time = reportYear
          break;
        }
      }

      let list = [];
      switch (reportBy) {
        case 'rm':
          params.reportCode = `${targetCode}_RM`;
          if (option === 'branch') {
            list = [
              { name: "p_rm_code", value: this.commonData.isRm ? this.currUser.code : 'ALL' },
              { name: 'p_br_code_lv2', value: codes }
            ];
          } else {
          list = [
            { name: "p_rm_code", value: this.commonData.isRm ? this.currUser.code : codes },
            { name: 'p_br_code_lv2', value: 'ALL' }
          ];
          }
          if (!isNEW_BIZ_LOAN) {
            list.push(...[
              { name: "p_rgon", value: 'ALL' },
              { name: 'p_br_code_lv1', value: 'ALL' },
            ]);
          }
          break;
        case 'customer':
          params.reportCode = `${targetCode}_CUSTOMER`;
          if (option === 'branch') {
            list = [
              { name: "p_rm_code", value: this.commonData.isRm ? this.currUser.code : 'ALL' },
              { name: 'p_br_code_lv2', value: codes },
              { name: "p_customer_id", value: 'ALL' }
            ];
          } else if (option === 'rm') {
            list = [
              { name: "p_rm_code", value: this.commonData.isRm ? this.currUser.code : codes },
              { name: 'p_br_code_lv2', value: 'ALL' },
              { name: "p_customer_id", value: 'ALL' }
            ];
          } else {
          list = [
            { name: "p_rm_code", value: this.commonData.isRm ? this.currUser.code : 'ALL' },
            { name: 'p_br_code_lv2', value: 'ALL' },
            { name: "p_customer_id", value: codes }
          ];
          }
          if (!isNEW_BIZ_LOAN) {
            list.push(...[
              { name: 'p_br_code_lv1', value: 'ALL' },
              { name: "p_rgon", value: 'ALL' },
            ]);
          }
          break;
        case 'branch':
          params.reportCode = `${targetCode}_BRANCH`;
          list = [
            { name: 'p_br_code_lv1', value: formData.branchCodeLv1 },
            { name: 'p_br_code_lv2', value: formData.branchCode },
            { name: "p_rm_code", value: 'ALL' },
          ];
          if (!isNEW_BIZ_LOAN) {
            list.push(...[
              { name: "p_rgon", value: 'ALL' },
            ]);
          }
          break;
        case 'region':
          params.reportCode = `${targetCode}_REGION`;
          list = [
            { name: 'p_rgon', value: formData.regionCode }
          ];
          if (!isNewCustomer) {
            list.push({name: "p_rm_code", value: 'ALL'});
          }
          break;
        default:
          break;
      }
      if (isNewCustomer) {
        list.push({name: "p_time", value: p_time});
        list.push({name: "type_of_time", value: type_of_time});
      }
      params.reportParams = [...params.reportParams, ...list];

    }
    const bizList: any[] = [
      { name: 'p_fr_date', value: formatDate(momentReport, 'yyyyMMdd', 'en') },
    ]
    switch (target) {
      case 'NEW_BIZ_NEW_CODE':
        setWithReportBy(target);
      break;
      case 'NEW_BIZ':
        setWithReportBy(target);

        if (reportBy === 'customer') {
          bizList.push({ name: 'p_page', value: formData.page });
        }
        params.reportParams.push(...bizList);
        break;
      case 'NEW_BIZ_NEW_PAYROLL':
        setWithReportBy(target);

        params.reportParams.push(...[
          { name: 'p_month', value: formatDate(momentReportMonth, 'MM/yyyy', 'en') },
          { name: 'p_chanel', value: formData.channel }
        ]);

        break;
      case 'NEW_BIZ_LOAN':
        setWithReportBy(target);
        params.reportParams.push(...bizList);
        break;
      default:
        break;
    }

    this.isLoading = true;
    this.kpiReportApi.getTableauReport(params).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(embeddedLink => {
      if (embeddedLink.length > 0) {
        this.loadReport(embeddedLink, params);
      } else {
        this.messageService.error(this.notificationMessage.E001);
      }
    });

  }

  loadReport(embeddedLink: string, paramSearch: any) {
    let title = 'Báo cáo ' + this.commonData.targetList.filter(item => {return item.value === this.form.controls.target.value})[0].name.toLowerCase();

    const modal = this.modalService.open(KpiReportModalComponent, { windowClass: 'tree__report-modal' });
    modal.componentInstance.embeddedReport = embeddedLink;
    modal.componentInstance.data = null;
    modal.componentInstance.model = paramSearch;
    modal.componentInstance.baseUrl = null;
    modal.componentInstance.filename = this.fileName;
    modal.componentInstance.title = title;
    modal.componentInstance.extendUrl = '';
    modal.componentInstance.reportType = 1;
  }

  removeList() {
    this.termData = [];
    this.paramsTable.page = 0;
    this.allRM = false;
    this.mapData();
  }



  initBranch(branchesByDomain) {
    // region, branch lv1 , branch lv2
    const regionObj = _.groupBy(branchesByDomain, 'region');
    const regionList = Object.keys(regionObj).map(key => ({ locationCode: key, locationName: key }));

    Object.keys(regionObj).forEach(key => {
      const branchLv1 = _.groupBy(regionObj[key], 'parentCode');
      const branchLv1List = Object.keys(branchLv1).map(branchKey => {
        return {
          code: branchLv1[branchKey][0].parentCode,
          name: branchLv1[branchKey][0].parentName,
          displayName: `${branchLv1[branchKey][0].parentCode} - ${branchLv1[branchKey][0].parentName}`,
          khuVucM: key,
          branch_lv2: branchLv1[branchKey].map(branch => {
            return { code: branch.code, name: branch.name, displayName: `${branch.code} - ${branch.name}`, khuVucM: key, parentCode: branchLv1[branchKey][0].parentCode }
          })
        }
      })
      if (!this.regions[key]) {
        this.regions[key] = {
          branch_lv1: branchLv1List
        }
      }
    });

    // all branch lv
    Object.keys(this.regions).forEach(key => {
      this.regions[key].branch_lv1.forEach(item => {
        this.allBranchLv2.push(...item.branch_lv2);
      });
    });

    Object.keys(this.regions).forEach(key => {
      this.regions[key].branch_lv1.forEach(item => {
        const branchLv1 = this.allBranchLv1.find(branch => branch.code === item.code);
        if (!branchLv1) this.allBranchLv1.push(item);
      });
    });

    this.listBranchTerm = this.allBranchLv2 || [];
    this.sessionService.setSessionData(SessionKey.LIST_BRANCH, this.listBranchTerm);

    this.commonData.listBranch = _.cloneDeep(this.listBranchTerm);
    if (!_.find(this.commonData.listBranch, (item) => item.code === this.currUser?.branch)) {
      this.commonData.listBranch.push({
        code: this.currUser?.branch,
        displayName: `${this.currUser?.branch} - ${this.currUser?.branchName}`,
      });
    }

    // set all region
    if (this.isHO) {
      this.commonData.regionList = regionList;
    } else {
      regionList?.forEach((item) => {
        if (
          this.commonData.listBranch?.findIndex((i) => i.khuVucM === item?.locationCode) !== -1 &&
          this.commonData.regionList.findIndex((r) => r.locationCode === item?.locationCode) === -1
        ) {
          this.commonData.regionList.push(item);
        }
      });
    }

    // sort region list
    this.commonData.regionList = _.orderBy(this.commonData.regionList, [region => region.locationName], ['asc']);
    if (this.commonData.regionList.length > 1) {
      let allRegion = '';
      this.commonData.regionList?.map(item => {
        if(item.locationCode !== undefined && item.locationCode !== null && item.locationCode !== 'undefined'){
          allRegion += item.locationCode + ';';
        }
      })
      this.commonData.regionList.unshift(
        {
          locationCode: allRegion,
          locationName: "Tất cả"
        }
      )
    }

    if (this.commonData.regionList.length) {
      this.form.controls.regionCode.setValue(this.commonData.regionList[0].locationCode);
    }

  }

  // set default branch lv1, lv2
  setBranchOfUser() {
    this.setBranchLv1List();
  }

  setBranchLv1List(options: any = {}): void {
    const { branchCode } = this.form.getRawValue();
    const { setLv2List = true, resetCode = false } = options;
    let branchLv1List = [];

    if (resetCode) {
      this.form.controls.branchCode.setValue('', { emitEvent: false });
      this.form.controls.branchCodeLv1.setValue('', { emitEvent: false });
    }

    if (!branchLv1List.length) {
      this.branchLv1List = _.cloneDeep(this.allBranchLv1);
    } else {
      this.branchLv1List = branchLv1List;
    }

    // sort alphabe
    this.branchLv1List = _.orderBy(this.branchLv1List, [branch => branch.displayName], ['asc']);

    const haveAllOption = this.branchLv1List.find(branch => branch.code === '');
    if (this.branchLv1List.length > 1 && !haveAllOption) {
      this.branchLv1List.unshift({ code: 'ALL', displayName: 'Tất cả' });
    }

    let value = _.head(this.branchLv1List)?.code;

    // follow branch lv2
    if (branchCode && !setLv2List) {
      const branchLv2 = this.allBranchLv2.find(branch => branch.code === branchCode);

      if (branchLv2?.parentCode) value = branchLv2?.parentCode;
    }

    this.form.controls.branchCodeLv1.setValue(value, { emitEvent: false });

    if (setLv2List) {
      this.setBranchLv2List();
    }

  }

  setBranchLv2List(options: any = {}): void {
    const { branchCodeLv1 } = this.form.getRawValue();
    let { onChange = false, alreadyPick = false } = options;

    if (branchCodeLv1 && branchCodeLv1 !== 'ALL') {
      this.branchLv2List = this.allBranchLv2.filter(branch => branch.parentCode === branchCodeLv1);
    } else {
      this.branchLv2List = this.allBranchLv2;
    }

    // this.commonData.listBranch = _.cloneDeep(branchLv2List);

    this.branchLv2List = _.orderBy(this.branchLv2List, [branch => branch.displayName], ['asc']);

    this.setOptionAllToBrLv2();

    if (!onChange && !alreadyPick) {
      this.form.controls.branchCode.setValue(_.head(this.branchLv2List)?.code, { emitEvent: false });
    }
  }

  setOptionAllToBrLv2(): void {
    const haveAllOption = this.branchLv2List.find(branch => branch.code === '');
    if (this.branchLv2List.length > 1 && !haveAllOption) {
      this.branchLv2List.unshift({ code: 'ALL', displayName: 'Tất cả' });
    }
  }

  setValueChanges(): void {
    this.form.controls.branchCodeLv1.valueChanges.subscribe(() => {
      if (this.isLoading) {
        return;
      }
      this.setBranchLv2List();

    })

    this.form.controls.reportBy.valueChanges.subscribe((value) => {
        this.initPickupList(this.getOption);
    });

    this.form.controls.target.valueChanges.subscribe(() => {
      if (!this.showMonth) this.form.controls.period.setValue('daily');
      if (!this.showDay) this.form.controls.period.setValue('monthly');
    })

    this.form.controls.listOption.valueChanges.subscribe(() => {
      this.initPickupList(this.getOption);
    });
  }

  initPickupList(value: string): void {
    this.allRM = false;
    this.paramsTable.page = 0;
    this.dataTerm[this.tableType] = {
      pageable: { ...this.pageable },
      term: [...this.termData],
    };
    this.textSearch = '';
    if (value === 'rm') {
      this.columns[0].name = this.fields.rmCode;
      this.columns[1].name = this.fields.rmName;
      this.termData = [...this.dataTerm.rm.term];
      this.pageable = { ...this.dataTerm.rm.pageable };
      if (this.commonData.isRm) {
        this.textSearch = this.currUser.code;
        if (this.termData.length === 0 && this.currUser.code) {
          this.add();
        }
      } else {
        this.textSearch = '';
      }
    } else if (value === 'customer') {
      this.columns[0].name = this.fields.customerCode;
      this.columns[1].name = this.fields.customerName;
      this.termData = [...this.dataTerm.customer.term];
      this.pageable = { ...this.dataTerm.customer.pageable };
    } else if (value === 'branch') {
      this.columns[0].name = this.fields.branchCode;
      this.columns[1].name = this.fields.branchName;
      this.termData = [...this.dataTerm.branch.term];
      this.pageable = { ...this.dataTerm.branch.pageable };
    }
    this.tableType = value;
    this.mapData();
  }

  get showYear(): boolean {
    return ['NEW_BIZ_NEW_CODE'].includes(this.form.controls.target.value);
  }

  get showMonth(): boolean {
    return !['NEW_BIZ', 'NEW_BIZ_LOAN'].includes(this.form.controls.target.value);
  }

  get showDay(): boolean {
    return !['NEW_BIZ_NEW_PAYROLL'].includes(this.form.controls.target.value);
  }

  get hidePickupList(): boolean {
    return (
      this.form.get('listOption').value === 'ALL' &&
      (this.form.get('reportBy').value === 'rm' || this.form.get('reportBy').value === 'customer')
    )
      ||
      ['branch', 'region'].includes(this.form.get('reportBy').value);
  }

  get dateView(): string {
    return this.form.controls.period.value === 'monthly' ? 'month' : 'date';
  }

  get isPayroll(): boolean {
    return this.form.controls.target.value === 'NEW_BIZ_NEW_PAYROLL';
  }

  get isBiz(): boolean {
    return this.form.controls.target.value === 'NEW_BIZ' && this.form?.get('reportBy').value === 'customer';
  }

  get showListOptions(): boolean {
    return ['rm', 'customer'].includes(this.form?.get('reportBy').value);
  }

  get choosenTitle(): string {
    switch (this.form?.controls?.reportBy.value) {
      case 'rm':
        return 'fields.selectRM';
      case 'customer':
        return 'fields.selectCustomer';
      case 'branch':
        return 'fields.choose_branch';
      default:
        return '';
    }
  }

  get choosenTitle2(): string {
    switch (this.form?.controls?.reportBy.value) {
      case 'rm':
        return 'fields.choose_branch';
      case 'customer':
        return 'fields.selectRM';
      default:
        return '';
    }
  }

  get getOption(): string {
    let side = this.form.get('reportBy').value;
    let option = this.form.get('listOption').value;
    let result = '';
    switch (side) {
      case 'rm':
        if (option === 'choose_rm2') {
          result = 'branch';
        } else {
          result = 'rm';
        }
        break;
      case 'branch':
        result = 'branch';
        break;
      case 'customer':
        if (option === 'choose_branch') {
          result = 'branch';
        } else if (option === 'choose_rm2') {
          result = 'rm';
        } else {
          result = 'customer';
        }
        break;
      case 'region':
        result = 'region';
        break;
    }
    return result;
  }

  get showButtonOptionRm(): boolean {
    let checkByCustomer = this.form.get('reportBy').value === 'customer' && !this.commonData.isRm;
    let checkByRm = this.form.get('reportBy').value === 'rm' && this.lenghtBranchesOfUser > 1;
    return checkByRm || checkByCustomer;
  }

  get showButtonOptionBranch(): boolean {
    return this.form.get('reportBy').value === 'customer' && this.lenghtBranchesOfUser > 1;
  }

}
