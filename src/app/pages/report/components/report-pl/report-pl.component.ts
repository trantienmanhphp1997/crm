import { formatDate } from '@angular/common';
import { forkJoin, Observable, of } from 'rxjs';
import { Component, OnInit, Injector, AfterViewInit } from '@angular/core';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { TableColumn } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import {
  BRANCH_HO,
  CommonCategory,
  Division,
  FunctionCode,
  maxInt32,
  Scopes,
  SessionKey,
} from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as moment from 'moment';
import { catchError, finalize } from 'rxjs/operators';
import { CustomerApi } from 'src/app/pages/customer-360/apis';
import { KpiReportApi } from '../../apis';
import { KpiReportModalComponent } from '../../dialogs';
import { RmApi } from 'src/app/pages/rm/apis';
import { RmModalComponent } from 'src/app/pages/rm/components/rm-modal/rm-modal.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { CustomerModalComponent } from 'src/app/pages/customer-360/components';
import { BranchApi } from 'src/app/pages/rm/apis';
import { ChooseBranchesModalComponent } from 'src/app/pages/system/components/choose-branches-modal/choose-branches-modal.component';
import { CategoryRegionApi } from '../../apis/category-region.api';
import { IS3Report } from 'src/app/core/interfaces/report.interface';

import { DashboardService } from 'src/app/pages/dashboard/services/dashboard.service';

@Component({
  selector: 'app-report-pl',
  templateUrl: './report-pl.component.html',
  styleUrls: ['./report-pl.component.scss'],
})
export class ReportPLComponent extends BaseComponent implements OnInit, AfterViewInit {
  commonData = {
    listDivision: [],
    listBranchs: [],
    minDate:  new Date(moment().add(-2, 'month').startOf('month').toDate()),
    minDateMonth: new Date(moment().add(-12, 'month').toDate()),
    maxDate: new Date(),
    listYears: [],
    listYearsCompare: [],
    listQuarterlyCompare: [],
    isRm: true,
    isRegionDirector: false,
    listBranch: [],
    isExportFile: false
  };
  textSearch: string;
  paramsTable = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };
  form = this.fb.group({
    division: '',
    period: 'monthly',
    reportBy: 'customer',
    listOption: 'ALL',
    regionCode: '',
    fromDate: [new Date(moment().startOf('month').toDate()), CustomValidators.required],
    toDate: [new Date(moment().add(-1, 'days').toDate())],
    month: [new Date(moment().add(-1, 'months').toDate())]
  });
  allRM = false;
  dataTerm = {
    rm: {
      pageable: { ...this.pageable },
      term: [],
    },
    branch: {
      pageable: { ...this.pageable },
      term: [],
    },
    customer: {
      pageable: { ...this.pageable },
      term: [],
    }
  };
  tableType: string;
  termData = [];
  listDataTable = [];
  columns: TableColumn[];
  fileName = 'bao-cao-pl';
  countRm: any;
  countBranchMax: number;
  countCustomerMax: number;
  regions = {};
  isHO = false;
  regionList = [];
  intervalDownloadFileS3 = null;
  objFunctionRMManager: any;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private customerApi: CustomerApi,
    private branchApi: BranchApi,
    private rmApi: RmApi,
    private kpiReportApi: KpiReportApi,
    private regionApi: CategoryRegionApi,
    private dashboardService: DashboardService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REPORT_PL}`);
    this.objFunctionRMManager = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    console.log('minDate: ', this.commonData.minDate);
    console.log('maxDate: ', this.commonData.minDateMonth);
    this.tableType = this.form.get('reportBy').value;

    this.isHO = this.currUser?.branch === BRANCH_HO;
    this.commonData.isExportFile = this.objFunction.scopes.includes('EXPORT_FILE');

    this.columns = [
      {
        name: this.fields.rmCode,
        prop: 'code',
      },
      {
        name: this.fields.rmName,
        prop: 'name',
      },
    ];

    const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
    this.commonData.listDivision =
      divisionOfUser?.map((item) => {
        return { code: item.code, name: `${item.code || ''} - ${item.name || ''}` };
      }) || [];

    this.form.get('division').setValue(_.head(this.commonData.listDivision)?.code);

    forkJoin([
      this.categoryService
        .getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined))),
      this.categoryService
        .getCommonCategory(CommonCategory.REPORTS_MAX_LENGTH_RM)
        .pipe(catchError(() => of(undefined))),

      this.dashboardService.getBranchByDomain({
        rsId: this.objFunction?.rsId,
        scope: Scopes.VIEW
      }),
    ]).subscribe(([branchesOfUser, configReport, branchesByDomain]) => {

      this.countRm =
        _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.TABLEAU_MAX_RM_REPORT)?.value ||
        0;
      this.countBranchMax =
        _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.TABLEAU_MAX_BRANCH_REPORT)?.value ||
        0;
      this.countCustomerMax =
        _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.TABLEAU_MAX_CUSTOMER_REPORT)?.value || 0;

      this.commonData.listBranch = branchesOfUser as any || [];
      this.commonData.isRm = _.isEmpty(branchesOfUser);
      this.commonData.isRegionDirector = this.objFunction.scopes.includes('VIEW_REGION_REPORT');
      if (this.unchooseAll()) {
        this.form.controls.listOption.setValue(this.form.controls.reportBy.value);
      }

      // set region list
      this.initRegionList(branchesByDomain);

      this.isLoading = false;
    });

  }

  ngAfterViewInit() {
    this.form.controls.division.valueChanges.subscribe(() => {
      this.form.controls.reportBy.setValue('customer');
      this.removeAll();
    });

    this.form.controls.reportBy.valueChanges.subscribe((value) => {
      this.unchooseAll() ?
        this.form.controls.listOption.setValue(value) :
        this.form.controls.listOption.setValue('ALL');
    });

    this.form.controls.listOption.valueChanges.subscribe((value) => {
      if (value != 'ALL') {
        this.initPickupList(value);
      }
    });
  }

  initPickupList(side: string): void {
    this.allRM = false;
    this.paramsTable.page = 0;
    this.dataTerm[this.tableType] = {
      pageable: { ...this.pageable },
      term: [...this.termData],
    };
    this.textSearch = '';

    switch (side) {
      case 'rm':
        this.columns[0].name = this.fields.rmCode;
        this.columns[1].name = this.fields.rmName;
        this.termData = [...this.dataTerm.rm.term];
        this.pageable = { ...this.dataTerm.rm.pageable };
        break;
      case 'branch':
        this.columns[0].name = this.fields.branchCode;
        this.columns[1].name = this.fields.branchName;
        this.termData = [...this.dataTerm.branch.term];
        this.pageable = { ...this.dataTerm.branch.pageable };
        break;
      case 'customer':
        this.columns[0].name = this.fields.customerCode;
        this.columns[1].name = this.fields.customerName;
        this.termData = [...this.dataTerm.customer.term];
        this.pageable = { ...this.dataTerm.customer.pageable };
        break;
    }
    this.tableType = side;
    this.mapData();
  }

  search() {
    if (this.form.controls.listOption.value === 'rm') {
      const formSearch = {
        crmIsActive: { value: 'true', disabled: true },
        rmBlock: {
          value: this.form.controls.division.value,
          disabled: true
        },
        isReportByRm: this.commonData.isRm,
        manager: true,
        rm: true
      };
      const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
      modal.componentInstance.dataSearch = formSearch;
      modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
      modal.result
        .then((res) => {
          if (res) {
            const listRMSelect = res.listSelected?.map((item) => {
              return {
                hrsCode: item?.hrisEmployee?.employeeId,
                code: item?.t24Employee?.employeeCode,
                name: item?.hrisEmployee?.fullName
              };
            });
            this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
            this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);
            this.mapData();
          }
        })
        .catch(() => {
        });
    } else if (this.form.controls.listOption.value === 'customer') {
      const formSearch = {
        customerType: {
          value: this.form.controls.division.value,
          disabled: true
        }
      };
      const modal = this.modalService.open(CustomerModalComponent, { windowClass: 'list__customer360-modal' });
      modal.componentInstance.listSelectedOld = this.termData;
      modal.componentInstance.dataSearch = formSearch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData =
              res.listSelected?.map((item) => {
                return {
                  code: item.customerCode,
                  customerCode: item.customerCode,
                  name: item.customerName ? item.customerName : item.name
                };
              }) || [];
            this.mapData();
          }
        })
        .catch(() => {
        });
    } else if (this.form.controls.listOption.value === 'branch') {
      const modal = this.modalService.open(ChooseBranchesModalComponent, { windowClass: 'tree__branches-modal' });
      modal.componentInstance.listBranchOld = _.map(this.termData, (x) => x.code) || [];
      modal.componentInstance.listBranch = this.commonData.listBranch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData = res;
            this.mapData();
          }
        })
        .catch(() => {
        });
    }
  }

  add() {
    if (this.hidePickupList) {
      return;
    }

    this.textSearch = _.trim(this.textSearch);
    if (this.textSearch.length === 0) {
      this.isLoading = false;
      return;
    }
    let option = this.form.controls.listOption.value;
    if (_.findIndex(this.termData, (i) => i.code?.toUpperCase() === this.textSearch?.toUpperCase()) === -1) {
      this.isLoading = true;
      if (option === 'rm') {
        const params = {
          crmIsActive: true,
          scope: Scopes.VIEW,
          rmBlock: this.form.controls.division.value,
          rsId: this.objFunctionRMManager?.rsId,
          employeeCode: this.textSearch,
          page: 0,
          size: maxInt32,
          isReportByRm: this.commonData.isRm,
          rm: true,
          manager: true
        };
        this.isLoading = true;
        this.rmApi.post('findAll', params).subscribe(
          (data) => {
            const itemResult = _.find(
              _.get(data, 'content'),
              (item) => item?.t24Employee?.employeeCode?.toUpperCase() === this.textSearch?.toUpperCase()
            );
            if (itemResult) {
              this.termData?.push({
                code: itemResult?.t24Employee?.employeeCode,
                name: itemResult?.hrisEmployee?.fullName,
                hrsCode: itemResult?.hrisEmployee?.employeeId
              });
              this.termData = _.uniqBy(this.termData, (i) => i.hrsCode);
              this.mapData();
            } else if (!itemResult && !this.allRM) {
              this.messageService.warn(_.get(this.notificationMessage, 'rmNotExistOrNotBranh'));
            }
            this.isLoading = false;
          },
          () => {
            this.messageService.error(this.notificationMessage.E001);
            this.isLoading = false;
          }
        );
      } else if (option === 'customer') {
        const params = {
          customerCode: this.textSearch,
          customerType: this.form.controls.division.value,
          rsId: this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`)?.rsId,
          scope: Scopes.VIEW,
          pageNumber: 0,
          pageSize: global?.userConfig?.pageSize,
          customerTypeSale: 1
        };
        let apiSearch = Division.INDIV == this.form.controls.division.value ? this.customerApi.search(params) : this.customerApi.searchSme(params);
        apiSearch.pipe(
          finalize(() => {
            this.isLoading = false;
          })
        ).subscribe((data) => {
          if (data?.length > 0) {
            this.termData?.push({
              code: data[0]?.customerCode,
              name: data[0]?.customerName,
              customerCode: data[0]?.customerCode
            });
            this.mapData();
          } else {
            this.messageService.warn(_.get(this.notificationMessage, 'customerNotExist'));
          }
        }, () => {
          this.messageService.error(this.notificationMessage.E001);
        });
      } else if (option === 'branch') {
        const itemResult = _.find(
          this.commonData.listBranch,
          (item) => item.code?.toUpperCase() === this.textSearch?.toUpperCase()
        );
        if (itemResult) {
          this.termData?.push(itemResult);
          this.mapData();
        } else {
          this.messageService.warn(_.get(this.notificationMessage, 'branchNotExistOrNotInBranch'));
        }
        this.isLoading = false;
      }
    } else {
      const messageReport = option.toString() + 'IsExist';
      this.messageService.warn(_.get(this.notificationMessage, messageReport));
      this.isLoading = false;
      return;
    }
  }

  setPage(pageInfo) {
    this.paramsTable.page = pageInfo.offset;
    this.mapData();
  }

  mapData() {
    const total = this.termData.length;
    this.pageable.totalElements = total;
    this.pageable.totalPages = Math.floor(total / this.pageable.size);
    this.pageable.currentPage = this.paramsTable.page;
    const start = this.paramsTable.page * this.paramsTable.size;
    this.listDataTable = this.termData?.slice(start, start + this.paramsTable.size);
  }

  removeRecord(item) {
    _.remove(this.termData, (i) => i.code === item.code);
    _.remove(this.listDataTable, (i) => i.code === item.code);
    if (this.listDataTable.length === 0 && this.pageable.currentPage > 0) {
      this.paramsTable.page -= 1;
    }
    this.listDataTable = [...this.listDataTable];
    this.allRM = false;
    this.mapData();
  }

  exportReport() {
    const formData = this.form.getRawValue();
    const { division, period, reportBy, fromDate, toDate, month } = formData;

    let reportParams = [];
    if (period === 'daily') {
      reportParams.push({ name: 'reportDivision', value: division });
      reportParams.push({ name: 'fromDate', value: formatDate(fromDate, 'yyyyMMdd', 'en') });
      reportParams.push({ name: 'toDate', value: formatDate(toDate, 'yyyyMMdd', 'en') });
    } else if (period === 'monthly') {
      reportParams.push({ name: 'reportDivision', value: division });
      reportParams.push({ name: 'reportMonth', value: formatDate(month, 'yyyyMM', 'en') });
    }

    let s3Params: IS3Report = {
      reportCode: 'REPORT_PL_' + reportBy.toUpperCase() + '_' + period.toUpperCase(),
      reportParams: reportParams,
      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW,
      type: 1
    }

    this.kpiReportApi.reportS3(s3Params).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(urls => {
      if (urls.length > 0) {
        const download = (urls) => {
          let url = urls.pop();
          window.open(url, '_self');

          if (!urls.length && this.intervalDownloadFileS3) {
            clearInterval(this.intervalDownloadFileS3);
          }
        }

        this.intervalDownloadFileS3 = setInterval(download, 500, urls);
      } else {
        this.messageService.warn('Không có file dữ liệu. Vui lòng thử lại sau');
      }
    }, () => {
      this.messageService.error(this.notificationMessage.E001);
    });
  }

  viewReport() {
    if (this.isLoading) {
      return;
    }

    if(this.validDateToDate()) {
      this.messageService.warn('Chỉ được phép xem báo cáo trong vòng 3 tháng.');
      return;
    }

    if (this.isReportExport()) {
      this.exportReport();
      return;
    }

    if (!this.form.valid) {
      validateAllFormFields(this.form);
      return;
    }

    const formData = this.form.getRawValue();
    const { division, period, reportBy, listOption, regionCode, fromDate, toDate, month } = formData;
    let codes = this.termData.map(item => item.code).join(';');
    codes = listOption === 'ALL' ? 'ALL' : codes;

    const isTermDataEmpty = _.isEmpty(this.termData);
    const isOptionAll = listOption === 'ALL';
    if (!isOptionAll) {
      if (listOption === 'branch') {
        if (isTermDataEmpty) {
          this.messageService.warn(_.get(this.notificationMessage, 'branchValidation'));
          return;
        }
        if (this.termData.length > this.countBranchMax) {
          this.translate.get('notificationMessage.countBranchMax', { number: this.countBranchMax }).subscribe((res) => {
            this.messageService.warn(res);
          });
          return;
        }
        if (this.commonData.isRm) {
          this.messageService.warn(_.get(this.notificationMessage, 'branchValidation'));
          return;
        }
      } else if (listOption === 'rm') {
        if (isTermDataEmpty) {
          this.messageService.warn(_.get(this.notificationMessage, 'rmValidation'));
          return;
        }
        if (this.termData.length > this.countRm) {
          this.translate.get('notificationMessage.countRmMax', { number: this.countRm }).subscribe((res) => {
            this.messageService.warn(res);
          });
          return;
        }
      } else if (listOption === 'customer') {
        if (isTermDataEmpty) {
          this.messageService.warn(_.get(this.notificationMessage, 'customerValidation'));
          return;
        }
        if (this.termData.length > this.countCustomerMax) {
          this.translate.get('notificationMessage.countCustomerLimit', { number: this.countCustomerMax }).subscribe((res) => {
            this.messageService.warn(res);
          });
          return;
        }
      }
    }

    // set report param
    let reportParams = [
      { name: 'p_lob', value: division },
      { name: 'p_rgon', value: listOption === 'region' ? regionCode : 'ALL' },
      { name: 'p_br_code_lv1', value: 'ALL' },
      { name: 'p_br_code_lv2', value: listOption === 'branch' ? codes : 'ALL' }
    ];
    if (period === 'daily') {
      reportParams.push(...[
        { name: 'p_fr_date', value: formatDate(fromDate, 'yyyyMMdd', 'en') },
        { name: 'p_to_date', value: formatDate(toDate, 'yyyyMMdd', 'en') }
      ]);
    } else if (period === 'monthly') {
      reportParams.push(
        { name: 'p_fr_m', value: formatDate(month, 'yyyyMM', 'en') }
      );
    }

    switch (reportBy) {
      case 'rm':
        reportParams.push(
          { name: 'p_rm_code', value: this.commonData.isRm ? this.currUser.code : listOption === 'rm' ? codes : 'ALL' }
        );
        break;
      case 'customer':
        reportParams.push(...[
          { name: 'p_rm_code', value: this.commonData.isRm ? this.currUser.code : listOption === 'rm' ? codes : 'ALL' },
          { name: 'p_customer_id', value: listOption === 'customer' ? codes : 'ALL' }
        ]);
        break;
      case 'branch':
        break;
      default:
        break;
    }
    // --- end set report param

    let params = {
      reportCode: 'REPORT_PL_' + reportBy.toUpperCase() + '_' + period.toUpperCase(),
      reportParams: reportParams,
      rsId: this.objFunction?.rsId,
      scope: Scopes.VIEW
    };

    this.isLoading = true;
    this.kpiReportApi.getTableauReport(params).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(embeddedLink => {
      if (embeddedLink.length > 0) {
        this.loadReport(embeddedLink, params);
      } else {
        this.messageService.error(this.notificationMessage.E001);
      }
    });
  }

  loadReport(embeddedLink: string, paramSearch: any) {
    const modal = this.modalService.open(KpiReportModalComponent, { windowClass: 'tree__report-modal' });
    modal.componentInstance.embeddedReport = embeddedLink;
    modal.componentInstance.data = null;
    modal.componentInstance.model = paramSearch;
    modal.componentInstance.baseUrl = null;
    modal.componentInstance.filename = this.fileName;
    modal.componentInstance.title = 'Báo cáo PL';
    modal.componentInstance.extendUrl = '';
    modal.componentInstance.reportType = 1;
  }

  removeList() {
    this.termData = [];
    this.paramsTable.page = 0;
    this.allRM = false;
    this.mapData();
  }

  removeAll() {
    this.dataTerm.rm.term = [];
    this.dataTerm.branch.term = [];
    this.dataTerm.customer.term = [];
    this.termData = [];
    this.paramsTable.page = 0;
    this.allRM = false;
    this.mapData();
  }

  initRegionList(branchesByDomain: any): void {
    const regionObj = _.groupBy(branchesByDomain, 'region');
    const regionList = Object.keys(regionObj).map(key => ({ locationCode: key, locationName: key }));

    Object.keys(regionObj).forEach(key => {
      const branchLv1 = _.groupBy(regionObj[key], 'parentCode');
      const branchLv1List = Object.keys(branchLv1).map(branchKey => {
        return {
          code: branchLv1[branchKey][0].parentCode,
          name: branchLv1[branchKey][0].parentName,
          displayName: `${branchLv1[branchKey][0].parentCode} - ${branchLv1[branchKey][0].parentName}`,
          khuVucM: key,
          branch_lv2: branchLv1[branchKey].map(branch => {
            return { code: branch.code, name: branch.name, displayName: `${branch.code} - ${branch.name}`, khuVucM: key, parentCode: branchLv1[branchKey][0].parentCode }
          })
        }
      })
      if (!this.regions[key]) {
        this.regions[key] = {
          branch_lv1: branchLv1List
        }
      }
    });

    if (this.isHO) {
      this.regionList = regionList;
    } else {
      regionList?.forEach((item) => {
        if (
          this.commonData.listBranch?.findIndex((i) => i.khuVucM === item?.locationCode) !== -1 &&
          this.regionList.findIndex((r) => r.locationCode === item?.locationCode) === -1
        ) {
          this.regionList.push(item);
        }
      });
    }

    // sort region list
    this.regionList = _.orderBy(this.regionList, [region => region.locationName], ['asc']);
    if (this.regionList.length > 1) {
      let allRegion = '';
      this.regionList?.map(item => {
        if (item.locationCode !== undefined && item.locationCode !== null && item.locationCode !== 'undefined') {
          allRegion += item.locationCode + ';';
        }
      });

      this.regionList.unshift(
        {
          locationCode: allRegion,
          locationName: "Tất cả"
        }
      )
    }

    if (this.regionList.length) {
      this.form.controls.regionCode.setValue(this.regionList[0].locationCode);
    }
    console.log(this.regionList);
  }

  getReportParams(params): any {
    return Object.keys(params).map(key => ({ name: key, value: params[key] }));
  }

  get hidePickupList(): boolean {
    return ['ALL', 'region'].includes(this.form.controls.listOption.value);
  }

  choosedDate() {
    if (this.form.get('toDate').value == null || moment(new Date()).diff(moment(this.form.get('toDate').value), 'month') > 3) {
      this.messageService.warn("Chỉ xem báo cáo trong 3 tháng gần nhất kể từ tháng hiện tại");
      this.form.get('toDate').setValue(new Date());
      this.setFromDate(this.form.get('toDate').value);
    } else {
      this.setFromDate(this.form.get('toDate').value);
    }
    this.ref.detectChanges();
  }
  setFromDate(toDate: Date) {
    const month = moment(toDate).month();
    const year = moment(toDate).year();
    this.form.get('fromDate').setValue(new Date(year, month, 1));
  }

  unchooseAll(): boolean {
    return this.form.controls.division.value === Division.INDIV
      && ['customer', 'rm'].includes(this.form.controls.reportBy.value)
      && this.commonData.isRegionDirector;
  }

  isReportExport(): boolean {
    return this.form.controls.division.value !== Division.INDIV
      && ['customer', 'rm'].includes(this.form.controls.reportBy.value)
      && this.form.controls.listOption.value == 'ALL'
      && this.commonData.isExportFile;
  }

  get choosenTitleViewReport(): string {
    return this.isReportExport() ? 'Xuất file' : 'labels.reportShow';
  }

  validDateToDate(): boolean {
    const fromDate = moment(this.form.get('fromDate').value);
    const toDate = moment(this.form.get('toDate').value);
    return toDate.diff(fromDate, 'days') > 92;
  }

}
