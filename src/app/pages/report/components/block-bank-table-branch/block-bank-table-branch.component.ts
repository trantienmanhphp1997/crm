import { global } from '@angular/compiler/src/util';
import { Component, EventEmitter, HostBinding, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { Scopes } from 'src/app/core/utils/common-constants';
import * as _ from 'lodash';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ChooseBranchesModalComponent } from 'src/app/pages/system/components/choose-branches-modal/choose-branches-modal.component';
import { Utils } from 'src/app/core/utils/utils';
import { BaseComponent } from 'src/app/core/components/base.component';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-block-bank-table-branch',
  templateUrl: './block-bank-table-branch.component.html',
  styleUrls: ['./block-bank-table-branch.component.scss'],
})
export class BlockBankTableBranchComponent extends BaseComponent implements OnInit {
  branches: [];
  pageable: Pageable;
  limit = global.userConfig.pageSize;
  messages: any;
  params = {
    pageSize: this.limit,
    pageNumber: 0,
    scope: Scopes.VIEW,
    rsId: '',
  };
  listBranchTable = [];
  listBranchChoose = [];
  @HostBinding('class.app__right-content') appRightContent = true;
  @Input() listBranch: Array<any>;
  search_value: string;
  @Output() modelChange = new EventEmitter();
  @ViewChild('table') table: DatatableComponent;
  @Input() listOldBranch: Array<any>;

  constructor(injector: Injector, modalService: NgbModal) {
    super(injector);
    this.messages = global.messageTable;
  }

  ngOnInit(): void {
    if (Utils.isArrayNotEmpty(this.listOldBranch)) {
      this.listBranchChoose = this.listOldBranch;
      this.search(true);
    }
  }

  search(isSearch: boolean) {
    if (isSearch) {
      this.params.pageNumber = 0;
    }
    this.listBranchTable = this.listBranchChoose.slice(
      this.params.pageNumber * this.params.pageSize,
      this.params.pageNumber * this.params.pageSize + this.params.pageSize
    );
    this.pageable = {
      totalElements: this.listBranchChoose.length,
      totalPages: this.listBranchChoose.length / this.params.pageSize,
      currentPage: this.params.pageNumber,
      size: this.params.pageSize,
    };
  }

  getValue(row, key) {
    return _.get(row, key);
  }

  setPage(pageInfo) {
    this.params.pageNumber = pageInfo.offset;
    this.search(false);
  }

  deleteRow(row: any) {
    this.listBranchChoose = this.listBranchChoose.filter((item) => {
      return item.code != row.code;
    });
    this.modelChange.emit(this.listBranchChoose);
    this.search(false);
  }

  selectBranch() {
    const modal = this.modalService.open(ChooseBranchesModalComponent, { windowClass: 'tree__branches-modal' });
    modal.componentInstance.listBranchOld = this.listBranchTable.map((item) => item.code) || [];
    modal.componentInstance.listBranch = this.listBranch;
    modal.result
      .then((res) => {
        if (res) {
          if (res.length > 10) {
            this.messageService.warn(this.notificationMessage.countBranch);
            return;
          }
          this.listBranchChoose =
            res?.map((item) => {
              return {
                code: item.code,
                name: item.name,
              };
            }) || [];
          this.modelChange.emit(this.listBranchChoose);
          this.search(true);
        }
      })
      .catch(() => {});
  }

  addBranch() {
    let valueSearch = [];
    if (Utils.isStringEmpty(this.search_value)) {
      return;
    }

    let countvalue = this.listBranchTable.filter((item) => {
      return item?.code?.toLowerCase() === this.search_value.trim().toLowerCase();
    });
    if (countvalue.length > 0) {
      this.messageService.warn(this.notificationMessage.branchExistTable);

      return;
    }
    valueSearch = this.listBranch.filter((item) => {
      return item?.code?.toLowerCase() === this.search_value?.trim().toLowerCase();
    });
    if (valueSearch.length > 0) {
      valueSearch =
        valueSearch.map((item) => {
          return {
            code: item.code,
            name: item.name,
          };
        }) || [];
      this.listBranchChoose.push(valueSearch[0]);
      this.modelChange.emit(this.listBranchChoose);
      this.search(true);
    } else {
      this.messageService.warn(this.notificationMessage.branchNotExist);
    }
  }

  removeList() {
    this.listBranchChoose = [];
    this.modelChange.emit(this.listBranchChoose);
    this.search(false);
  }
}
