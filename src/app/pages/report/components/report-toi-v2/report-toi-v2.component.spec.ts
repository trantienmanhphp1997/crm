/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ReportToiV2Component } from './report-toi-v2.component';

describe('ReportToiV2Component', () => {
  let component: ReportToiV2Component;
  let fixture: ComponentFixture<ReportToiV2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportToiV2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportToiV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
