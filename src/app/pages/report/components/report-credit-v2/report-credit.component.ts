import { formatDate } from '@angular/common';
import { forkJoin, Observable, of } from 'rxjs';
import { Component, OnInit, Injector, AfterViewInit } from '@angular/core';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { TableColumn } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import {
  BRANCH_HO,
  CommonCategory,
  Division,
  FunctionCode,
  maxInt32,
  Scopes,
  SessionKey,
} from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as moment from 'moment';
import { catchError, finalize } from 'rxjs/operators';
import { CustomerApi } from 'src/app/pages/customer-360/apis';
import { KpiReportApi } from '../../apis';
import { KpiReportModalComponent } from '../../dialogs';
import { RmApi } from 'src/app/pages/rm/apis';
import { RmModalComponent } from 'src/app/pages/rm/components/rm-modal/rm-modal.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { CustomerModalComponent } from 'src/app/pages/customer-360/components';
import { BranchApi } from 'src/app/pages/rm/apis';
import { ChooseBranchesModalComponent } from 'src/app/pages/system/components/choose-branches-modal/choose-branches-modal.component';
import { CategoryRegionApi } from '../../apis/category-region.api';
import { IS3Report } from 'src/app/core/interfaces/report.interface';

import { DashboardService } from 'src/app/pages/dashboard/services/dashboard.service';

@Component({
  selector: 'app-report-credit-v2',
  templateUrl: './report-credit.component.html',
  styleUrls: ['./report-credit.component.scss'],
})
export class ReportCreditV2Component extends BaseComponent implements OnInit, AfterViewInit {
  commonData = {
    listDivision: [],
    listBranchs: [],
    listCurrencyUnit: [],
    listCurrencyType: [],
    minDate: null,
    maxDate: new Date(),
    listQuarterly: [
      {
        label: 'Quý 1',
        value: 1,
      },
      {
        label: 'Quý 2',
        value: 2,
      },
      {
        label: 'Quý 3',
        value: 3,
      },
      {
        label: 'Quý 4',
        value: 4,
      },
    ],
    listYears: [],
    listYearsCompare: [],
    listQuarterlyCompare: [],
    isRm: true,
    isRegionDirector: false,
    listRegionDirectorTitle: [],
    listBranch: [],
  };
  textSearch: string;
  paramsTable = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };
  form = this.fb.group({
    division: '',
    currencyType: '',
    currencyUnit: '',
    reportType: 'moment',
    momentReport: [new Date(moment().add(-1, 'day').toDate()), CustomValidators.required],
    momentCompare: [new Date(moment().add(-2, 'day').toDate()), CustomValidators.required],
    period: 'daily',
    reportPeriodFrom: [new Date(moment().add(-1, 'months').toDate())],
    reportPeriodTo: [new Date(moment().add(-2, 'months').toDate())],
    comparePeriodFrom: [new Date(moment().add(-2, 'month').toDate()), [CustomValidators.required]],
    comparePeriodTo: null,
    reportBy: 'rm',
    formatReport: 'HTML',
    reportTypeDetail: 'customer',
    reportPeriodQuarterly: '',
    reportPeriodYear: '',
    comparePeriodQuarterly: '',
    comparePeriodYear: '',
    downloadReport: 'HTML',
    distributeBy: 'loanTerm',
    listOption: 'ALL',
    regionCode: '',
    customerReportType: 'sync',
    groupBy: 'rm'
  });
  allRM = false;
  dataTerm = {
    rm: {
      pageable: { ...this.pageable },
      term: [],
    },
    branch: {
      pageable: { ...this.pageable },
      term: [],
    },
    customer: {
      pageable: { ...this.pageable },
      term: [],
    }
  };
  tableType: string;
  termData = [];
  listDataTable = [];
  columns: TableColumn[];
  maxDate = new Date();
  fileName = 'bao-cao-tin-dung';
  countRm: any;
  countBranchMax: number;
  maxMonth: any;
  countCustomerMax: number;
  maxDay: any;
  // month start from 0
  quarterly = {
    'Q1': moment().set({ month: 2 }).endOf('month'),
    'Q2': moment().set({ month: 5 }).endOf('month'),
    'Q3': moment().set({ month: 8 }).endOf('month'),
    'Q4': moment().set({ month: 11 }).endOf('month')
  }
  intervalDownloadFileS3 = null;
  regions = {};
  isHO = false;
  regionList = [];
  highPrioritySort = ['SME', 'CIB', 'INDIV', 'FI', 'DVC'];
  objFunctionRMManager: any;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private customerApi: CustomerApi,
    private branchApi: BranchApi,
    private rmApi: RmApi,
    private kpiReportApi: KpiReportApi,
    private regionApi: CategoryRegionApi,
    private dashboardService: DashboardService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REPORT_CREDIT_V2}`);
    this.objFunctionRMManager = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.tableType = this.form.get('reportBy').value;

    this.isHO = this.currUser?.branch === BRANCH_HO;

    this.columns = [
      {
        name: this.fields.rmCode,
        prop: 'code',
      },
      {
        name: this.fields.rmName,
        prop: 'name',
      },
    ];
    this.form.get('reportPeriodQuarterly').setValue(_.head(this.commonData.listQuarterly)?.value);

    const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
    this.commonData.listDivision =
      divisionOfUser?.map((item) => {
        return { code: item.code, name: `${item.code || ''} - ${item.name || ''}` };
      }) || [];

    this.sortDivision();

    this.form.get('division').setValue(_.head(this.commonData.listDivision)?.code);

    forkJoin([
      this.commonService
        .getCommonCategory(CommonCategory.CURRENCY_UNIT_REPORTS)
        .pipe(catchError(() => of(undefined))),

      this.commonService.getCommonCategory(CommonCategory.REPORTS_YEARS).pipe(catchError(() => of(undefined))),

      this.categoryService
        .getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined))),

      this.categoryService.getCommonCategory(CommonCategory.CURRENCY_TYPE).pipe(catchError(() => of(undefined))),

      this.categoryService
        .getCommonCategory(CommonCategory.REPORTS_MAX_LENGTH_RM)
        .pipe(catchError(() => of(undefined))),

      this.dashboardService.getBranchByDomain({
        rsId: this.objFunction?.rsId,
        scope: Scopes.VIEW
      }),
      // S3_REPORT_CONFIG
      this.categoryService.getCommonCategory(CommonCategory.S3_REPORT_CONFIG).pipe(catchError(() => of(undefined))),
    ]).subscribe(([currencyUnits, configYear, branchesOfUser, listCurrencyType, configReport, branchesByDomain, s3Config]) => {

      this.countRm =
        _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.TABLEAU_MAX_RM_REPORT)?.value ||
        0;
      this.countBranchMax =
        _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.TABLEAU_MAX_BRANCH_REPORT)?.value ||
        0;
      this.countCustomerMax =
        _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.TABLEAU_MAX_CUSTOMER_REPORT)?.value || 0;
      const getMaxday = _.find(_.get(s3Config, 'content'), (item) => item.code === CommonCategory.REPORT_CREDIT_INDIV_LIMIT_DAY)?.value || 45;
      const day = moment().subtract(getMaxday, 'days').calendar();
      this.maxDay = new Date(day);
      console.log(this.maxDay);

      this.commonData.listCurrencyType = _.get(listCurrencyType, 'content')
        ?.map((itemValue) => {
          return {
            code: itemValue.value,
            label: `${itemValue.name || ''}`,
          };
        });
      this.form.get('currencyType').setValue(_.head(this.commonData.listCurrencyType)?.code);

      this.commonData.listBranch = branchesOfUser as any || [];
      this.commonData.isRm = _.isEmpty(branchesOfUser);
      this.commonData.listCurrencyUnit = _.orderBy(_.get(currencyUnits, 'content'), ['orderNum'], ['asc', 'desc']);
      this.commonData.listCurrencyUnit =
        _.get(currencyUnits, 'content')?.map((item) => {
          return { code: item.value, name: item.name, isDefault: item.isDefault };
        }) || [];
      const yearConfig = _.find(_.get(configYear, 'content'), (item) => item.code === 'TD')?.value || 1;
      const year = moment().year();
      for (let i = 0; i < yearConfig; i++) {
        this.commonData.listYears.push({
          value: year - i,
          label: year - i,
        });
      }
      this.maxMonth = _.find(_.get(configYear, 'content'), (item) => item.code === 'PTKH_MONTH_OPTION')?.value || 1;
      this.commonData.minDate = new Date(
        moment()
          .add(-(yearConfig - 1), 'year')
          .startOf('year')
          .valueOf()
      );
      this.form.get('reportPeriodYear').setValue(_.head(this.commonData.listYears)?.value);
      this.sessionService.setSessionData(SessionKey.REPORT_CREDIT, this.commonData);
      this.form.get('currencyUnit').setValue(_.head(this.commonData.listCurrencyUnit)?.code);
      this.commonData.listCurrencyUnit.forEach((item) => {
        if (item.isDefault) {
          this.form.get('currencyUnit').setValue(item?.code);
        }
      });
      this.checkRegionDirector(this.form.get('division').value);
      // set region list
      this.initRegionList(branchesByDomain);

      this.isLoading = false;
    });

    this.textSearch = this.currUser.code;
    if (this.currUser.code) {
      this.add();
    }
  }

  ngAfterViewInit() {
    this.form.controls.reportBy.valueChanges.subscribe((value) => {
        this.initPickupList(this.getOption);
    });

    this.form.controls.reportType.valueChanges.subscribe((value) => {
      if(value === 'moment'){
        this.form.get('period').setValue('daily');
      }else{
        this.form.get('period').setValue('monthly');
      }
      this.form.get('distributeBy').setValue('loanTerm');
    });

    this.form.controls.period.valueChanges.subscribe((value) => {
      this.commonData.listYearsCompare = this.commonData.listYears;
      this.commonData.listQuarterlyCompare = this.commonData.listQuarterly;
      if( this.form.controls.reportType.value === 'moment'){
        this.form.controls.momentReport.setValue(moment().add(-1, value === 'daily' ? 'day' : 'months').toDate());
        this.form.controls.momentCompare.setValue(moment().add(-2, value === 'daily' ? 'day' : 'months').toDate());
      }
    });

    this.form.controls.division.valueChanges.subscribe(value => {

      this.form.get('listOption').setValue('ALL');
      // reset pickupList in contract indiv mode
      this.form.controls.reportBy.setValue('customer');
      if (value === Division.INDIV) {
        // this.form.get('groupBy').setValue('rm');
        // this.initPickupList('rm');
        this.form.get('listOption').setValue('choose_rm');

      }
      this.checkRegionDirector(value);
      this.removeList();
      if (value == 'INDIV') {
      }
    });
    this.form.controls.customerReportType.valueChanges.subscribe(value => {
      if (this.useS3API) {
        this.form.get('groupBy').setValue('rm');
      } else {
        this.form.get('groupBy').setValue('customer');
      }

    });

    this.form.controls.groupBy.valueChanges.subscribe(value => {
      this.initPickupList(this.getOption);
      this.removeAll();
    });

    this.form.controls.formatReport.valueChanges.subscribe(res => {
      if (res == 'HTML') {
        this.initPickupList('customer');
        return;
      }
      this.form.controls.groupBy.setValue('rm');
    });
    this.form.controls.listOption.valueChanges.subscribe(res => {
      this.initPickupList(this.getOption);
    });
  }

  checkRegionDirector(division: string) {
    this.commonData.isRegionDirector = this.objFunction.scopes.includes('VIEW_HISTORY');
  }

  initPickupList(side: string): void {
    this.allRM = false;
    this.paramsTable.page = 0;
    this.dataTerm[this.tableType] = {
      pageable: { ...this.pageable },
      term: [...this.termData],
    };
    this.textSearch = '';

    switch (side) {
      case 'rm':

        this.columns[0].name = this.fields.rmCode;
        this.columns[1].name = this.fields.rmName;
        this.termData = [...this.dataTerm.rm.term];
        this.pageable = { ...this.dataTerm.rm.pageable };
        if (this.commonData.isRm) {
          this.textSearch = this.currUser.code;
          if (this.termData.length === 0 && this.currUser.code) {
            this.add();
          }
        } else {
          this.textSearch = '';
        }
        break;
      case 'branch':
        this.columns[0].name = this.fields.branchCode;
        this.columns[1].name = this.fields.branchName;
        this.termData = [...this.dataTerm.branch.term];
        this.pageable = { ...this.dataTerm.branch.pageable };
        break;
      case 'customer':
        this.columns[0].name = this.fields.customerCode;
        this.columns[1].name = this.fields.customerName;
        this.termData = [...this.dataTerm.customer.term];
        this.pageable = { ...this.dataTerm.customer.pageable };
        if (this.form.controls.formatReport.value == 'ECXEL') {
          this.form.controls.groupBy.setValue('rm');
        }
        break;

    }

    if (!_.find(this.commonData.listDivision, (item) => item.code === this.form.get('division').value)) {
      this.form.get('division').setValue(_.head(this.commonData.listDivision)?.code);
    }
    this.tableType = side;
    this.mapData();
  }

  search() {
    // let value = this.form.get('reportBy').value;
    let value = this.getOption;

    if (this.useS3API) {
      value = this.form.get('groupBy').value
    }

    if (value === 'rm') {
      const formSearch = {
        crmIsActive: { value: 'true', disabled: true },
        rmBlock: {
          value: this.form.get('division').value,
          disabled: true,
        },
        isNHSReport: this.form.get('division').value === Division.INDIV,
        isReportByRm: this.commonData.isRm,
        manager: true,
        rm: true,
      };
      const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
      modal.componentInstance.dataSearch = formSearch;
      modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
      modal.result
        .then((res) => {
          if (res) {
            const listRMSelect = res.listSelected?.map((item) => {
              return {
                hrsCode: item?.hrisEmployee?.employeeId,
                code: item?.t24Employee?.employeeCode,
                name: item?.hrisEmployee?.fullName,
              };
            });
            this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
            this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);
            this.mapData();
          }
        })
        .catch(() => { });
    } else if (value === 'branch') {
      const modal = this.modalService.open(ChooseBranchesModalComponent, { windowClass: 'tree__branches-modal' });
      modal.componentInstance.listBranchOld = _.map(this.termData, (x) => x.code) || [];
      modal.componentInstance.listBranch = this.commonData.listBranch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData = res;
            this.mapData();
          }
        })
        .catch(() => { });
    } else if (value === 'customer') {
      const formSearch = {
        customerType: {
          value: this.form.get('division').value,
          disabled: true,
        },
      };
      const modal = this.modalService.open(CustomerModalComponent, { windowClass: 'list__customer360-modal' });
      modal.componentInstance.listSelectedOld = this.termData;
      modal.componentInstance.dataSearch = formSearch;
      modal.result
        .then((res) => {
          // if (res.listSelected.length >= 200) {
          //   this.messageService.warn('Chỉ được chọn tối đa 200 RM');
          //   return;
          // }
          if (res) {
            this.termData =
              res.listSelected?.map((item) => {
                return {
                  code: item.customerCode,
                  customerCode: item.customerCode,
                  name: item.customerName ? item.customerName : item.name,
                };
              }) || [];
            this.mapData();
          }
        })
        .catch(() => { });
    }
  }

  add() {
    // option ALL - block auto add
    if (this.hidePickupList && !this.useS3API) {
      return;
    }

    this.textSearch = _.trim(this.textSearch);
    if (this.textSearch.length === 0) {
      this.isLoading = false;
      return;
    }
    // let side = this.form.get('reportBy').value;
    let side = this.getOption;
    if (this.form.controls.formatReport.value == 'EXCEL') {
      side = this.form.get('groupBy').value
    }
    if (_.findIndex(this.termData, (i) => i.code?.toUpperCase() === this.textSearch?.toUpperCase()) === -1) {
      this.isLoading = true;
      if (side === 'rm') {
        const params = {
          crmIsActive: true,
          scope: Scopes.VIEW,
          rmBlock: this.form.get('division').value,
          rsId: this.objFunctionRMManager?.rsId,
          employeeCode: this.textSearch,
          page: 0,
          size: maxInt32,
          isNHSReport: this.form.get('division').value === Division.INDIV,
          isReportByRm: this.commonData.isRm,
          rm: true,
          manager: true,
        };
        this.isLoading = true;
        this.rmApi.post('findAll', params).subscribe(
          (data) => {
            const itemResult = _.find(
              _.get(data, 'content'),
              (item) => item?.t24Employee?.employeeCode?.toUpperCase() === this.textSearch?.toUpperCase()
            );
            if (itemResult) {
              this.termData?.push({
                code: itemResult?.t24Employee?.employeeCode,
                name: itemResult?.hrisEmployee?.fullName,
                hrsCode: itemResult?.hrisEmployee?.employeeId,
              });
              this.termData = _.uniqBy(this.termData, (i) => i.hrsCode);
              this.mapData();
            } else if (!itemResult && !this.allRM) {
              this.messageService.warn(_.get(this.notificationMessage, 'rmNotExistOrNotBranh'));
            }
            this.isLoading = false;
          },
          () => {
            this.messageService.error(this.notificationMessage.E001);
            this.isLoading = false;
          }
        );
      } else if (side === 'branch') {
        const itemResult = _.find(
          this.commonData.listBranch,
          (item) => item.code?.toUpperCase() === this.textSearch?.toUpperCase()
        );
        if (itemResult) {
          this.termData?.push(itemResult);
          this.mapData();
        } else {
          this.messageService.warn(_.get(this.notificationMessage, 'branchNotExistOrNotInBranch'));
        }
        this.isLoading = false;
      } else if (side === 'customer') {
        const params = {
          customerCode: this.textSearch,
          customerType: this.form.get('division').value,
          rsId: this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`)?.rsId,
          scope: Scopes.VIEW,
          pageNumber: 0,
          pageSize: global?.userConfig?.pageSize,
        };

        let api: Observable<any>;

        // if (this.form.get('division').value === Division.SME) {
        api = this.customerApi.searchSme(params);
        // } else {
        // api = this.customerApi.search(params);
        // }

        api.pipe(
          finalize(() => {
            this.isLoading = false;
          })
        ).subscribe((data) => {
          if (data?.length > 0) {
            this.termData?.push({
              code: data[0]?.customerCode,
              name: data[0]?.customerName,
              customerCode: data[0]?.customerCode,
            });
            this.mapData();
          } else {
            this.messageService.warn(_.get(this.notificationMessage, 'customerNotExist'));
          }
        }, () => {
          this.messageService.error(this.notificationMessage.E001);
        });
      }
    } else {
      const messageReport = side.toString() + 'IsExist';
      this.messageService.warn(_.get(this.notificationMessage, messageReport));
      this.isLoading = false;
      return;
    }
  }

  setPage(pageInfo) {
    this.paramsTable.page = pageInfo.offset;
    this.mapData();
  }

  mapData() {
    const total = this.termData.length;
    this.pageable.totalElements = total;
    this.pageable.totalPages = Math.floor(total / this.pageable.size);
    this.pageable.currentPage = this.paramsTable.page;
    const start = this.paramsTable.page * this.paramsTable.size;
    this.listDataTable = this.termData?.slice(start, start + this.paramsTable.size);
  }

  removeRecord(item) {
    _.remove(this.termData, (i) => i.code === item.code);
    _.remove(this.listDataTable, (i) => i.code === item.code);
    if (this.listDataTable.length === 0 && this.pageable.currentPage > 0) {
      this.paramsTable.page -= 1;
    }
    this.listDataTable = [...this.listDataTable];
    this.allRM = false;
    this.mapData();
  }

  viewReport() {
    if (this.isLoading) {
      return;
    }

    let params = {
      reportCode: '',
      reportParams: [],
      rsId: this.objFunction?.rsId,
      scope: Scopes.VIEW
    };

    let s3Params: IS3Report = {
      reportCode: '',
      reportParams: [],
      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW
    }

    const formData = this.form.getRawValue();
    // let currencyU = this.commonData.listCurrencyUnit.filter((i) => i.code === this.form.get('currencyUnit').value && i.code);
    // let currencyUnit = currencyU.length > 0 ? currencyU[0].name : 'Đồng';

    // let currencyT = this.commonData.listCurrencyType.filter((i) => i.code === this.form.get('currencyType').value && i.code);
    // let currencyType = currencyT.length > 0 ? currencyT[0].code : 'VND';

    let codes = '';
    let codeList = ''; // raw
    this.termData?.map((i) => { codes += i.code + ';' });
    codeList = codes.substring(0, codes.length - 1);
    codes = this.form.get('listOption').value === 'ALL' ? 'ALL' : codeList;
    let { momentReport, momentCompare, currencyType, currencyUnit , period} = formData;
    let startDateReport = this.form.get('reportPeriodFrom').value;
    let startDateCompare = this.form.get('comparePeriodFrom').value;
    let allDivision: String = '';
    let option = this.getOption;

    if (_.isEmpty(this.form.get('division').value)) {
      this.commonData.listDivision.forEach((item) => {
        if (!_.isEmpty(item.code)) {
          allDivision += item.code + ';';
        }
      });
      allDivision = allDivision.substring(0, allDivision.length - 1);
    } else {
      allDivision = this.form.get('division').value;
      allDivision = allDivision === Division.INDIV ? allDivision+=';NHS': allDivision;
    }

    let reportBy = this.form.get('reportBy').value;

    const key = this.isContract ? 'CONTRACT' : reportBy.toString().toUpperCase();

    const getCustomerParams = (reportParams, option) => {
      let customerIdList = codes;
      let rmCodeList = this.commonData.isRm ? this.currUser.code : 'ALL';

      // set value for s3API if report contract, else default tableau
      if (this.useS3API) {
        if (formData.groupBy === 'rm') {
          customerIdList = 'ALL';
          rmCodeList = codeList;
        } else {
          customerIdList = codeList;
          rmCodeList = 'ALL';
        }
      }

      if (option === 'branch') {
        reportParams.p_customer_id = 'ALL';
        reportParams.p_br_code_lv2 = codes;
        reportParams.p_rm_code = 'ALL';
      } else if (option === 'rm') {
        reportParams.p_customer_id = 'ALL';
        reportParams.p_br_code_lv2 = 'ALL';
        reportParams.p_rm_code = codes;
      } else {
      reportParams.p_customer_id = customerIdList;
      reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : 'ALL';
      reportParams.p_rm_code = rmCodeList;
      }
      return reportParams;
    }

    const formatParams = (reportParams) => {
      s3Params = this.getS3ReportParams(params, reportParams, s3Params);
      params.reportParams = this.getReportParams(reportParams);
    }

    // thời điểm
    if (this.form.get('reportType').value === 'moment') {
      if (this.form.valid) {
        if (reportBy.toString().toUpperCase() !== 'CUSTOMER' && formatDate(momentReport, 'dd/MM/yyyy', 'en') === formatDate(momentCompare, 'dd/MM/yyyy', 'en')) {
          this.messageService.warn(this.notificationMessage.momentCompareNoThanMomentReport);
          return;
        }
        if (period === 'monthly') {
          momentReport = moment(momentReport).endOf('month');
          momentCompare = moment(momentCompare).endOf('month');
        } 
        momentReport = formatDate(momentReport, 'yyyyMMdd', 'en');
        momentCompare = _.isNull(momentCompare) ? '' : formatDate(momentCompare, 'yyyyMMdd', 'en');
      } else {
        validateAllFormFields(this.form);
        return;
      }

      // tableau payload
      params.reportCode = `TINDUNG_TD_${key}`;

      let reportParams: any = {
        p_fr_date: momentReport,
        p_lob: allDivision,
        p_ccy_code: currencyType,
        p_unit: currencyUnit,

      };

      switch (reportBy) {
        case 'rm':
          if (option === 'branch') {
            reportParams.p_rm_code = "ALL";
            reportParams.p_br_code_lv2 = codes;
          } else {
          reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : codes;
          reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : 'ALL';
          }
          reportParams.p_to_date = momentCompare;
          break;
        case 'branch':
          reportParams.p_br_code_lv2 = codes;
          reportParams.p_to_date = momentCompare;
          break;
        case 'customer':
          reportParams = getCustomerParams(reportParams, option);
          break;
        case 'region':
          reportParams.p_to_date = momentCompare;
          reportParams.p_rgon = this.form.get('regionCode').value;
          break;
      }

      formatParams(reportParams);

    } else { // bình quân
      if (this.form.get('period').value === 'monthly') {
        // tháng
        if (this.form.valid) {
          if (
            reportBy.toString().toUpperCase() !== 'CUSTOMER' &&
            !_.isNull(startDateCompare) &&
            formatDate(startDateReport, 'MM/yyyy', 'en') === formatDate(startDateCompare, 'MM/yyyy', 'en')
          ) {
            this.messageService.warn(this.notificationMessage.compareFromReportFrom);
            return;
          }

          startDateReport = formatDate(startDateReport, 'MM/yyyy', 'en');
          startDateCompare = _.isNull(startDateCompare) ? '' : formatDate(startDateCompare, 'MM/yyyy', 'en');
          // tableau payload
          params.reportCode = `TINDUNG_BQ_${key}_MONTHLY`;

          let reportParams: any = {
            p_fr_m: startDateReport,
            p_lob: allDivision,
            p_ccy_code: currencyType,
            p_unit: currencyUnit,
          };

          switch (reportBy) {
            case 'rm':
              if (option === 'branch') {
                reportParams.p_rm_code = "ALL";
                reportParams.p_br_code_lv2 = codes;
              } else {
              reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : codes;
              reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : 'ALL';
              }
              reportParams.p_to_m = startDateCompare;
              break;
            case 'branch':
              reportParams.p_br_code_lv2 = codes;
              reportParams.p_to_m = startDateCompare;
              break;
            case 'customer':
              reportParams = getCustomerParams(reportParams, option);
              break;
            case 'region':
              reportParams.p_rgon = this.form.get('regionCode').value;
              reportParams.p_to_m = startDateCompare;
              break;
          }

          formatParams(reportParams);
        } else {
          validateAllFormFields(this.form);
          return;
        }
      } else if (this.form.get('period').value === 'quarterly') { // quý

        const reportPeriodQuarterly = this.form.get('reportPeriodQuarterly').value;
        const reportPeriodYear = this.form.get('reportPeriodYear').value;
        const comparePeriodQuarterly = this.form.get('comparePeriodQuarterly').value;
        const comparePeriodYear = this.form.get('comparePeriodYear').value;
        if (reportBy.toString().toUpperCase() !== 'CUSTOMER' && _.isNull(comparePeriodQuarterly) && !_.isNull(comparePeriodYear)) {
          this.messageService.warn(this.notificationMessage.quarterlyValidation);
          return;
        } else if (reportBy.toString().toUpperCase() !== 'CUSTOMER' && !_.isNull(comparePeriodQuarterly) && _.isNull(comparePeriodYear)) {
          this.messageService.warn(this.notificationMessage.yearValidation);
          return;
        } else if (reportBy.toString().toUpperCase() !== 'CUSTOMER' && reportPeriodQuarterly === comparePeriodQuarterly && reportPeriodYear === comparePeriodYear) {
          this.messageService.warn(this.notificationMessage.compareFromReportFrom);
          return;
        }

        startDateReport = 'Q' + reportPeriodQuarterly + '-' + reportPeriodYear;
        startDateCompare = _.isNull(comparePeriodYear) ? '' : 'Q' + comparePeriodQuarterly + '-' + comparePeriodYear;
        // tableau payload
        params.reportCode = `TINDUNG_BQ_${key}_QUATERLY`;
        let reportParams: any = {
          p_fr_q: startDateReport,
          p_lob: allDivision,
          p_ccy_code: currencyType,
          p_unit: currencyUnit,
        };

        switch (reportBy) {
          case 'rm':
            if (option === 'branch') {
              reportParams.p_rm_code = "ALL";
              reportParams.p_br_code_lv2 = codes;
            } else {
            reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : codes;
            reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : 'ALL';
            }
            reportParams.p_to_q = startDateCompare;
            break;
          case 'branch':
            reportParams.p_br_code_lv2 = codes;
            reportParams.p_to_q = startDateCompare;
            break;
          case 'customer':
            reportParams = getCustomerParams(reportParams, option);

            break;
          case 'region':
            reportParams.p_to_q = startDateCompare;
            reportParams.p_rgon = this.form.get('regionCode').value;
            break;
        }

        formatParams(reportParams);
      } else if (this.form.get('period').value === 'yearly') { // năm

        if (
          reportBy.toString().toUpperCase() !== 'CUSTOMER' &&
          !_.isNull(this.form.get('comparePeriodYear').value) &&
          this.form.get('reportPeriodYear').value === this.form.get('comparePeriodYear').value
        ) {
          this.messageService.warn(this.notificationMessage.compareFromReportFrom);
          return;
        }

        startDateReport = this.form.get('reportPeriodYear').value;
        startDateCompare = this.form.get('comparePeriodYear').value ? this.form.get('comparePeriodYear').value : '';
        // tableau payload

        params.reportCode = `TINDUNG_BQ_${key}_YEARLY`;

        const reportParams: any = {
          p_fr_y: startDateReport,
          p_lob: allDivision,
          p_ccy_code: currencyType,
          p_unit: currencyUnit,
        };

        switch (reportBy) {
          case 'rm':
            if (option === 'branch') {
              reportParams.p_rm_code = "ALL";
              reportParams.p_br_code_lv2 = codes;
            } else {
            reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : codes;
            reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : 'ALL';
            }
            reportParams.p_to_y = startDateCompare;
            break;
          case 'branch':
            reportParams.p_br_code_lv2 = codes;
            reportParams.p_to_y = startDateCompare;
            break;
          case 'customer':
            if (option === 'branch') {
              reportParams.p_customer_id = 'ALL';
              reportParams.p_br_code_lv2 = codes;
              reportParams.p_rm_code = 'ALL';
            } else if (option === 'rm') {
            reportParams.p_customer_id = 'ALL';
            reportParams.p_br_code_lv2 = 'ALL';
            reportParams.p_rm_code = codes;
            } else {
            reportParams.p_customer_id = codes;
            reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : 'ALL';
            reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : this.form.controls.groupBy.value == 'rm' ? codeList : 'All';
            }
            break;
          case 'region':
            reportParams.p_rgon = this.form.get('regionCode').value;
            reportParams.p_to_y = startDateCompare;
            break;
        }

        formatParams(reportParams);
      }

    }

    // validate term

    const isTermDataEmpty = _.isEmpty(this.termData);
    // s3API wil have no option all
    const isOptionAll = this.form.get('listOption').value === 'ALL' && !this.useS3API;
    const side = this.useS3API ? formData.groupBy : option;
    if (this.form.get('listOption').value !== 'ALL') {
      if (isTermDataEmpty && !isOptionAll && side === 'rm') {
        this.messageService.warn(_.get(this.notificationMessage, 'rmValidation'));
        return;
      } else if (isTermDataEmpty && !isOptionAll && side === 'branch') {
        this.messageService.warn(_.get(this.notificationMessage, 'branchValidation'));
        return;
      } else if (this.termData.length > this.countRm && side === 'rm') {
        this.translate.get('notificationMessage.countRmMax', { number: this.countRm }).subscribe((res) => {
          this.messageService.warn(res);
        });
        return;
      } else if (this.termData.length > this.countBranchMax && side === 'branch') {
        this.translate.get('notificationMessage.countBranchMax', { number: this.countBranchMax }).subscribe((res) => {
          this.messageService.warn(res);
        });
        return;
      } else if (side === 'customer') {
        if (isTermDataEmpty && !isOptionAll) {
          this.messageService.warn(_.get(this.notificationMessage, 'customerValidation'));
          return;
        }
        if (this.termData.length > this.countCustomerMax) {
          this.translate.get('notificationMessage.countCustomerLimit', { number: this.countCustomerMax }).subscribe((res) => {
            this.messageService.warn(res);
          });
          return;
        }
      }
    }

    this.getLink(params, s3Params);

  }

  // report have 2 type to view
  getLink(params, s3Params: IS3Report): void {
    const formData = this.form.getRawValue();
    const { division, reportBy } = this.form.getRawValue();
    this.isLoading = true;

    if (division === Division.INDIV && this.form.controls.formatReport.value == 'EXCEL') { // indiv - s3 api
      // EXCEL
      if (reportBy === 'customer') {
        s3Params.reportCode = `${params.reportCode}_${formData.groupBy.toString().toUpperCase()}`;
      }

      s3Params.rsId = this.objFunction.rsId;
      s3Params.scope = Scopes.VIEW;
      this.kpiReportApi.reportS3(s3Params).pipe(
        finalize(() => {
          this.isLoading = false;
        })
      ).pipe(
        finalize(() => {
          this.isLoading = false;
        })
      ).subscribe(urls => {
        if (urls.length > 0) {
          const download = (urls) => {
            let url = urls.pop();
            window.open(url, '_self');

            if (!urls.length && this.intervalDownloadFileS3) {
              clearInterval(this.intervalDownloadFileS3);
            }
          }

          this.intervalDownloadFileS3 = setInterval(download, 500, urls);
        } else {
          this.messageService.warn(_.get(this.notificationMessage, 'report_empty'));
        }
      }, () => {
        this.messageService.error(this.notificationMessage.E001);
      });
    } else { // other division use tableau
      this.kpiReportApi.getTableauReport(params).pipe(
        finalize(() => {
          this.isLoading = false;
        })
      ).subscribe(embeddedLink => {
        if (embeddedLink.length > 0) {
          this.loadReport(embeddedLink, params);
        } else {
          this.messageService.error(this.notificationMessage.E001);
        }
      },
        () => {
          this.messageService.error(this.notificationMessage.E001);
        }
      );
    }

  }

  loadReport(embeddedLink: string, paramSearch: any) {
    const modal = this.modalService.open(KpiReportModalComponent, { windowClass: 'tree__report-modal' });
    modal.componentInstance.embeddedReport = embeddedLink;
    modal.componentInstance.data = null;
    modal.componentInstance.model = paramSearch;
    modal.componentInstance.baseUrl = null;
    modal.componentInstance.filename = this.fileName;
    modal.componentInstance.title = 'Báo cáo tín dụng';
    modal.componentInstance.extendUrl = '';
    modal.componentInstance.reportType = 1;
  }

  removeList() {
    this.termData = [];
    this.paramsTable.page = 0;
    this.allRM = false;
    this.mapData();
  }

  removeAll() {
    this.dataTerm.rm.term = [];
    this.dataTerm.branch.term = [];
    this.dataTerm.customer.term = [];
    this.termData = [];
    this.paramsTable.page = 0;
    this.allRM = false;
    this.mapData();
  }

  onChangeCheckAllRM(event) {
    if (event.checked) {
      this.textSearch = '';
      const params = {
        crmIsActive: true,
        scope: Scopes.VIEW,
        rmBlock: this.form.get('division').value,
        rsId: this.objFunction?.rsId,
        employeeCode: this.textSearch,
        page: 0,
        size: maxInt32,
        isNHSReport: this.form.get('division').value === Division.INDIV,
        isReportByRm: this.commonData.isRm,
        rm: true,
        manager: true,
      };
      this.isLoading = true;
      this.rmApi.post('findAll', params).subscribe(
        (data) => {
          this.termData = _.map(_.get(data, 'content'), (item) => {
            return {
              code: item?.t24Employee?.employeeCode,
              name: item?.hrisEmployee?.fullName,
              hrsCode: item?.hrisEmployee?.employeeId,
            };
          });
          this.termData = _.uniqBy(this.termData, (i) => i.hrsCode);
          this.paramsTable.page = 0;
          this.mapData();
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      this.termData = [];
      this.paramsTable.page = 0;
      this.mapData();
    }
  }

  isShowCheckboxAllRm() {
    return (
      this.currUser?.branch !== BRANCH_HO &&
      !this.commonData?.isRm &&
      this.form.get('division')?.value !== Division.INDIV &&
      this.form.get('reportBy')?.value === 'rm'
    );
  }

  initRegionList(branchesByDomain: any): void {
    const regionObj = _.groupBy(branchesByDomain, 'region');
    const regionList = Object.keys(regionObj).map(key => ({ locationCode: key, locationName: key }));

    Object.keys(regionObj).forEach(key => {
      const branchLv1 = _.groupBy(regionObj[key], 'parentCode');
      const branchLv1List = Object.keys(branchLv1).map(branchKey => {
        return {
          code: branchLv1[branchKey][0].parentCode,
          name: branchLv1[branchKey][0].parentName,
          displayName: `${branchLv1[branchKey][0].parentCode} - ${branchLv1[branchKey][0].parentName}`,
          khuVucM: key,
          branch_lv2: branchLv1[branchKey].map(branch => {
            return { code: branch.code, name: branch.name, displayName: `${branch.code} - ${branch.name}`, khuVucM: key, parentCode: branchLv1[branchKey][0].parentCode }
          })
        }
      })
      if (!this.regions[key]) {
        this.regions[key] = {
          branch_lv1: branchLv1List
        }
      }
    });

    if (this.isHO) {
      this.regionList = regionList;
    } else {
      regionList?.forEach((item) => {
        if (
          this.commonData.listBranch?.findIndex((i) => i.khuVucM === item?.locationCode) !== -1 &&
          this.regionList.findIndex((r) => r.locationCode === item?.locationCode) === -1
        ) {
          this.regionList.push(item);
        }
      });
    }

    // sort region list
    this.regionList = _.orderBy(this.regionList, [region => region.locationName], ['asc']);
    if (this.regionList.length > 1) {
      let allRegion = '';
      this.regionList?.map(item => {
        if (item.locationCode !== undefined && item.locationCode !== null && item.locationCode !== 'undefined') {
          allRegion += item.locationCode + ';';
        }
      });

      this.regionList.unshift(
        {
          locationCode: allRegion,
          locationName: "Tất cả"
        }
      )
    }

    if (this.regionList.length) {
      this.form.controls.regionCode.setValue(this.regionList[0].locationCode);
    }

  }

  getReportParams(params): any {
    return Object.keys(params).map(key => ({ name: key, value: params[key] }));
  }

  getS3ReportParams(params: any, reportParams, s3Params: IS3Report): IS3Report {
    if (!this.useS3API) return s3Params;
    let s3ReportParams = [];
    s3ReportParams.push({ name: 'field01', value: this.form.get('groupBy').value === 'rm' ? reportParams.p_rm_code.replaceAll(';', ',') : reportParams.p_customer_id.replaceAll(';', ',') });
    s3ReportParams.push({ name: 'field02', value: this.form.get('groupBy').value === 'rm' ? 'ALL' : reportParams.p_lob });

    const dateFormat = 'yyyyMMDD';
    // console.log(params, moment(this.form.get('momentReport').value).endOf('month').format(dateFormat));

    let prdId = '';
    if (this.form.get('reportType').value === 'moment') {
      prdId = reportParams.p_fr_date;
    } else {
      if (this.form.get('period').value === 'monthly') {
        prdId = moment(this.form.get('reportPeriodFrom').value).endOf('month').format(dateFormat);
      } else if (this.form.get('period').value === 'quarterly') {
        const [qName, year] = reportParams.p_fr_q.split('-');
        prdId = this.quarterly[qName].set({ year }).format(dateFormat);
      } else if (this.form.get('period').value === 'yearly') {
        prdId = moment().set({ year: reportParams.p_fr_y }).endOf('year').format(dateFormat);
      }
    }
    s3ReportParams.push({ name: 'prd_id', value: prdId });
    s3Params.reportParams = s3ReportParams;
    return s3Params;
  }

  sortDivision(): void {
    const currentDisivionList: any[] = _.cloneDeep(this.commonData.listDivision);
    const result = [];
    this.highPrioritySort.forEach(code => {
      const index = currentDisivionList.findIndex(item => item.code === code);
      if (index > -1) {
        result.push(currentDisivionList[index]);
        currentDisivionList.splice(index, 1);
      }
    });

    result.push(...currentDisivionList);
    this.commonData.listDivision = result;
  }

  get showListOptions(): boolean {
    return ['rm', 'customer', 'branch'].includes(this.form?.get('reportBy').value);
  }

  get showButtonOptionRm(): boolean {
    let checkByRm = this.form.get('reportBy').value === 'customer' && !this.commonData.isRm;
    let checkByCustomer = this.form.get('reportBy').value === 'rm' && this.commonData.listBranch.length > 1;
    return checkByRm || checkByCustomer;
  }

  get showButtonOptionBranch(): boolean {
    return this.form.get('reportBy').value === 'customer' && this.commonData.listBranch.length > 1;
  }

  get choosenTitle(): string {
    switch (this.form?.controls?.reportBy.value) {
      case 'rm':
        return 'fields.selectRM';
      case 'customer':
        return 'fields.selectCustomer';
      case 'branch':
        return 'fields.choose_branch';
      default:
        return '';
    }
  }

  get choosenTitle2(): string {
    switch (this.form?.controls?.reportBy.value) {
      case 'rm':
        return 'fields.choose_branch';
      case 'customer':
        return 'fields.selectRM';
      default:
        return '';
    }
  }

  get getOption(): string {
    let side = this.form.get('reportBy').value;
    let option = this.form.get('listOption').value;
    let result = '';
    switch (side) {
      case 'rm':
        if (option === 'choose_rm2') {
          result = 'branch';
        } else {
          result = 'rm';
        }
        break;
      case 'branch':
        result = 'branch';
        break;
      case 'customer':
        if (option === 'choose_branch') {
          result = 'branch';
        } else if (option === 'choose_rm2') {
          result = 'rm';
        } else {
          result = 'customer';
        }
        break;
      case 'region':
        result = 'region';
        break;
    }
    return result;
  }

  get hidePickupList(): boolean {
    return (this.form.get('listOption').value === 'ALL' && ['rm', 'customer', 'branch'].includes(this.form.get('reportBy').value))
      ||
      ['region'].includes(this.form.get('reportBy').value);
  }

  get showGroupBy(): boolean {
    return this.form?.get('reportBy').value === 'customer' && this.form?.get('division').value === Division.INDIV && this.form.controls.formatReport.value == 'EXCEL'
  }

  get isContract(): boolean {
    return this.form?.get('customerReportType').value === 'contract' && this.form?.get('reportBy').value === 'customer'
  }

  // support s3 api
  // if customer, contract = contract, else = reportby
  get useS3API(): boolean {
    return this.form?.get('reportBy').value === 'customer' && this.form.controls.formatReport.value == 'EXCEL' && this.form?.get('division').value === Division.INDIV;
  }
}
