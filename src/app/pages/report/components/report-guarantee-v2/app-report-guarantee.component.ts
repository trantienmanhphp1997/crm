import { formatDate } from '@angular/common';
import { forkJoin, Observable, of } from 'rxjs';
import { Component, OnInit, Injector, AfterViewInit } from '@angular/core';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { TableColumn } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import {
  BRANCH_HO,
  CommonCategory,
  Division,
  FunctionCode,
  maxInt32,
  Scopes,
  SessionKey,
} from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as moment from 'moment';
import { catchError, finalize } from 'rxjs/operators';
import { CustomerApi } from 'src/app/pages/customer-360/apis';
import { KpiReportApi } from '../../apis';
import { KpiReportModalComponent } from '../../dialogs';
import { RmApi } from 'src/app/pages/rm/apis';
import { RmModalComponent } from 'src/app/pages/rm/components/rm-modal/rm-modal.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { CustomerModalComponent } from 'src/app/pages/customer-360/components';
import { BranchApi } from 'src/app/pages/rm/apis';
import { Utils } from 'src/app/core/utils/utils';
import { ChooseBranchesModalComponent } from 'src/app/pages/system/components/choose-branches-modal/choose-branches-modal.component';
import { CategoryRegionApi } from '../../apis/category-region.api';
import { DashboardService } from 'src/app/pages/dashboard/services/dashboard.service';

@Component({
  selector: 'app-report-guarantee-v2',
  templateUrl: './app-report-guarantee.component.html',
  styleUrls: ['./app-report-guarantee.component.scss'],
})
export class ReportGuaranteeV2 extends BaseComponent implements OnInit, AfterViewInit {
  commonData = {
    listDivision: [],
    listBranchs: [],
    minDate: null,
    maxDate: new Date(moment().add(-1, 'day').toDate()),
    listQuarterly: [
      {
        label: 'Quý 1',
        value: 1,
      },
      {
        label: 'Quý 2',
        value: 2,
      },
      {
        label: 'Quý 3',
        value: 3,
      },
      {
        label: 'Quý 4',
        value: 4,
      },
    ],
    listYears: [],
    listYearsCompare: [],
    listQuarterlyCompare: [],
    isRm: true,
    listBranch: [],
    regionList: [],
    targetList: [],
    isRegionDirector: false,
    listRegionDirectorTitle: [],
    maxPeriodByYear : 5,
    maxPeriodByMonth : 2,
    listMonth: [],
    allBranchLv2: [],
    allBranchLv1: [],
    listBranchTerm: [],
    isHO: false,
    branchLv1List: [],
    branchLv2List: []
  };
  textSearch: string;
  lenghtBranchesOfUser: number;
  paramsTable = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };
  form = this.fb.group({
    target: [''],
    division: '',
    currencyType: '',
    currencyUnit: '',
    reportType: 'moment',
    momentReport: [new Date(moment().add(-1, 'day').toDate()), CustomValidators.required],
    momentCompare: [new Date(moment().add(-2, 'day').toDate()), CustomValidators.required],
    period: 'monthly',
    reportPeriodFrom: [new Date(moment().add(-1, 'months').toDate())],
    reportPeriodTo: [new Date(moment().add(-2, 'months').toDate())],
    comparePeriodFrom: [new Date(moment().add(-2, 'months').toDate())],
    comparePeriodTo: null,
    reportBy: 'rm',
    reportTypeDetail: 'customer',
    reportPeriodQuarterly: '',
    reportPeriodMonth: '',
    reportPeriodYear: '',

    comparePeriodQuarterly: '',
    comparePeriodYear: '',
    downloadReport: 'HTML',
    distributeBy: 'loanTerm',
    listOption: 'ALL',
    regionCode: '',
    customerReportType: 'sync',
    groupBy: 'rm',
    branchCodeLv1: [[]],
    branchCode: [''],
    branchLv1List: []
  });
  allRM = false;
  dataTerm = {
    rm: {
      pageable: { ...this.pageable },
      term: [],
    },
    branch: {
      pageable: { ...this.pageable },
      term: [],
    },
    customer: {
      pageable: { ...this.pageable },
      term: [],
    }
  };
  tableType: string;
  termData = [];
  listDataTable = [];
  columns: TableColumn[];
  maxDate = new Date(moment().add(-1, 'day').toDate());
  fileName = 'bao-cao-bao-lanh';
  countRm: any;
  countBranchMax: number;
  maxMonth: any;
  countCustomerMax: number;
  // month start from 0
  quarterly = {
    'Q1': moment().set({ month: 2 }).endOf('month'),
    'Q2': moment().set({ month: 5 }).endOf('month'),
    'Q3': moment().set({ month: 8 }).endOf('month'),
    'Q4': moment().set({ month: 11 }).endOf('month')
  }
  intervalDownloadFileS3 = null;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private customerApi: CustomerApi,
    private branchApi: BranchApi,
    private rmApi: RmApi,
    private kpiReportApi: KpiReportApi,
    private regionApi: CategoryRegionApi,
    private dashboardService: DashboardService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REPORT_GUARANTEE_V2}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.removeList();
    this.tableType = this.form.get('reportBy').value;
    this.commonData.isHO = this.currUser?.branch === BRANCH_HO;
    this.columns = [
      {
        name: this.fields.rmCode,
        prop: 'code',
      },
      {
        name: this.fields.rmName,
        prop: 'name',
      },
    ];
    const skipDividion = [Division.INDIV, Division.FI, Division.DVC];
    const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
    this.commonData.listDivision =
      divisionOfUser?.filter((item) => {return !skipDividion.includes(item.code)})?.map((item) => {
        return { code: item.code, name: `${item.code || ''} - ${item.name || ''}` };
      }) || [];
    this.form.get('division').setValue(_.head(this.commonData.listDivision)?.code);

    forkJoin([
      this.commonService.getCommonCategory(CommonCategory.REPORTS_YEARS).pipe(catchError(() => of(undefined))),
      this.categoryService
        .getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined))),
      this.categoryService
        .getCommonCategory(CommonCategory.REPORTS_MAX_LENGTH_RM)
        .pipe(catchError(() => of(undefined))),
        this.dashboardService.getBranchByDomain({
          rsId: this.objFunction?.rsId,
          scope: Scopes.VIEW
        }),
      this.categoryService
        .getCommonCategory(CommonCategory.REGIONAL_DIRECTOR)
        .pipe(catchError(() => of(undefined))),

    ]).subscribe(([ configYear, branchesOfUser, configReport, branchesByDomain, regionDirectorTitle]) => {


      this.countRm =
        _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.TABLEAU_MAX_RM_REPORT)?.value ||
        0;
      this.countBranchMax =
        _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.TABLEAU_MAX_BRANCH_REPORT)?.value ||
        0;
      this.countCustomerMax =
        _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.TABLEAU_MAX_CUSTOMER_REPORT)?.value || 0;



      this.commonData.listBranch = branchesOfUser || [];
      this.lenghtBranchesOfUser = branchesOfUser.length;
      this.commonData.branchLv2List = branchesOfUser;
      this.commonData.isRm = _.isEmpty(branchesOfUser);

      const yearConfig = _.find(_.get(configYear, 'content'), (item) => item.code === 'HDV')?.value || 1;
      const year = moment().year();
      for (let i = 0; i < yearConfig; i++) {
        this.commonData.listYears.push({
          value: year - i,
          label: year - i,
        });
      }
      this.maxMonth = _.find(_.get(configYear, 'content'), (item) => item.code === 'PTKH_MONTH_OPTION')?.value || 1;
      this.commonData.minDate = new Date(
        moment()
          .add(-(yearConfig - 1), 'year')
          .startOf('year')
          .valueOf()
      );

      this.commonData.listYearsCompare =  this.commonData.listYears;
      this.form.get('reportPeriodYear').setValue(_.head(this.commonData.listYears)?.value);
      if(this.commonData.listYears.length > 1 ){
        this.form.get('comparePeriodYear').setValue(this.commonData.listYears[1]?.value);
      }
      this.form.get('reportPeriodQuarterly').setValue(_.head(this.commonData.listQuarterly)?.value);
      this.commonData.listQuarterlyCompare = this.commonData.listQuarterly;
      this.form.get('comparePeriodQuarterly').setValue(_.head(this.commonData.listQuarterlyCompare)?.value);

      this.sessionService.setSessionData(SessionKey.REPORT_CREDIT, this.commonData);
      this.commonData.listRegionDirectorTitle = regionDirectorTitle.content?.map(item => ({ value: item.value, code: item.code })) || [];

      // set region list
      const title = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM)?.filter(f => f.code === Division.SME).map(item => item.titleCode);
      console.log(this.commonData.listRegionDirectorTitle);
      console.log(this.commonData.listRegionDirectorTitle?.filter(f => title.includes(f?.code)));
      this.commonData.isRegionDirector = this.objFunction.scopes.includes('VIEW_HISTORY');

      for (let i = 1; i <= 12; i++) {
        this.commonData.listMonth.push(
          {
            label: 'Tháng ' + i,
            value: i
          }
        );
      }

      this.initBranch(branchesByDomain);
      this.setBranchOfUser();
      this.textSearch = this.currUser.code;
      if (this.commonData.isRm && this.currUser.code) {
        this.add();
      }
      this.isLoading = false;
    });
  }

  ngAfterViewInit() {
    this.form.controls.reportBy.valueChanges.subscribe((value) => {
        this.initPickupList(this.getOption);
    });

    this.form.controls.period.valueChanges.subscribe((value) => {
      const year = moment().year();
      switch(value){
        case 'PS Tháng':
          this.form.get('reportPeriodMonth').setValue(moment().month());
          this.commonData.listYears = [];
          for (let i = 0; i < this.commonData.maxPeriodByMonth; i++) {
            this.commonData.listYears.push({
              value: year - i,
              label: year - i,
            });
          }
          this.form.get('reportPeriodYear').setValue(_.head(this.commonData.listYears)?.value);

        break;
        case 'PS Năm':
          this.commonData.listYears = [];
          for (let i = 0; i < this.commonData.maxPeriodByYear; i++) {
            this.commonData.listYears.push({
              value: year - i,
              label: year - i,
            });
          }
          this.form.get('reportPeriodYear').setValue(_.head(this.commonData.listYears)?.value);
          break;
      }


    });

    this.form.controls.division.valueChanges.subscribe(value => {
      this.form.get('listOption').setValue('ALL');
      this.removeList();
    });
    this.form.controls.customerReportType.valueChanges.subscribe(value => {
      // console.log('customerReportType', value);
      this.form.get('groupBy').setValue('rm');
    });

    this.form.controls.groupBy.valueChanges.subscribe(value => {
      this.initPickupList(this.getOption);
      this.removeAll();
    });

    this.form.controls.listOption.valueChanges.subscribe(res => {
      this.initPickupList(this.getOption);
    });

    this.form.controls.branchCodeLv1.valueChanges.subscribe(() => {
      if (this.isLoading) {
        return;
      }
      this.setBranchLv2List();

    })
  }

  initPickupList(side: string): void {
    this.allRM = false;
    this.paramsTable.page = 0;
    this.dataTerm[this.tableType] = {
      pageable: { ...this.pageable },
      term: [...this.termData],
    };
    this.textSearch = '';

    switch (side) {
      case 'rm':

        this.columns[0].name = this.fields.rmCode;
        this.columns[1].name = this.fields.rmName;
        this.termData = [...this.dataTerm.rm.term];
        this.pageable = { ...this.dataTerm.rm.pageable };
        if (this.commonData.isRm) {
          this.textSearch = this.currUser.code;
          if (this.termData.length === 0 && this.currUser.code) {
            this.add();
          }
        } else {
          this.textSearch = '';
        }
        break;
      case 'branch':
        this.columns[0].name = this.fields.branchCode;
        this.columns[1].name = this.fields.branchName;
        this.termData = [...this.dataTerm.branch.term];
        this.pageable = { ...this.dataTerm.branch.pageable };
        break;
      case 'customer':
        this.columns[0].name = this.fields.customerCode;
        this.columns[1].name = this.fields.customerName;
        this.termData = [...this.dataTerm.customer.term];
        this.pageable = { ...this.dataTerm.customer.pageable };
        break;

    }

    if (!_.find(this.commonData.listDivision, (item) => item.code === this.form.get('division').value)) {
      this.form.get('division').setValue(_.head(this.commonData.listDivision)?.code);
    }
    this.tableType = side;
    this.mapData();
  }

  search() {
    let option = this.getOption;
    if (option === 'rm') {
      const formSearch = {
        crmIsActive: { value: 'true', disabled: true },
        rmBlock: {
          value: this.form.get('division').value,
          disabled: true,
        },
        isNHSReport: this.form.get('division').value === Division.INDIV,
        isReportByRm: this.commonData.isRm,
        manager: true,
        rm: true,
      };
      const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
      modal.componentInstance.dataSearch = formSearch;
      modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
      modal.result
        .then((res) => {
          if (res) {
            const listRMSelect = res.listSelected?.map((item) => {
              return {
                hrsCode: item?.hrisEmployee?.employeeId,
                code: item?.t24Employee?.employeeCode,
                name: item?.hrisEmployee?.fullName,
              };
            });
            this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
            this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);
            this.mapData();
          }
        })
        .catch(() => { });
    } else if (option === 'branch') {
      const modal = this.modalService.open(ChooseBranchesModalComponent, { windowClass: 'tree__branches-modal' });
      modal.componentInstance.listBranchOld = _.map(this.termData, (x) => x.code) || [];
      modal.componentInstance.listBranch = this.commonData.listBranch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData = res;
            this.mapData();
          }
        })
        .catch(() => { });
    } else if (option === 'customer') {
      const formSearch = {
        customerType: {
          value: this.form.get('division').value,
          disabled: true,
        },
      };
      const modal = this.modalService.open(CustomerModalComponent, { windowClass: 'list__customer360-modal' });
      modal.componentInstance.listSelectedOld = this.termData;
      modal.componentInstance.dataSearch = formSearch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData =
              res.listSelected?.map((item) => {
                return {
                  code: item.customerCode,
                  customerCode: item.customerCode,
                  name: item.customerName ? item.customerName : item.name,
                };
              }) || [];
            this.mapData();
          }
        })
        .catch(() => { });
    }
  }

  add() {
    this.textSearch = _.trim(this.textSearch);
    if (this.textSearch.length === 0) {
      this.isLoading = false;
      return;
    }
    let option = this.getOption;
    if (_.findIndex(this.termData, (i) => i.code?.toUpperCase() === this.textSearch?.toUpperCase()) === -1) {
      this.isLoading = true;
      if (option === 'rm') {
        const params = {
          crmIsActive: true,
          scope: Scopes.VIEW,
          rmBlock: this.form.get('division').value,
          rsId: this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`)?.rsId,
          employeeCode: this.textSearch,
          page: 0,
          size: maxInt32,
          isNHSReport: this.form.get('division').value === Division.INDIV,
          isReportByRm: this.commonData.isRm,
          rm: true,
          manager: true,
        };
        this.isLoading = true;
        this.rmApi.post('findAll', params).subscribe(
          (data) => {
            const itemResult = _.find(
              _.get(data, 'content'),
              (item) => item?.t24Employee?.employeeCode?.toUpperCase() === this.textSearch?.toUpperCase()
            );
            if (itemResult) {
              this.termData?.push({
                code: itemResult?.t24Employee?.employeeCode,
                name: itemResult?.hrisEmployee?.fullName,
                hrsCode: itemResult?.hrisEmployee?.employeeId,
              });
              this.termData = _.uniqBy(this.termData, (i) => i.hrsCode);
              this.mapData();
            } else if (!itemResult && !this.allRM) {
              this.messageService.warn(_.get(this.notificationMessage, 'rmNotExistOrNotBranh'));
            }
            this.isLoading = false;
          },
          () => {
            this.messageService.error(this.notificationMessage.E001);
            this.isLoading = false;
          }
        );
      } else if (option === 'branch') {
        const itemResult = _.find(
          this.commonData.listBranch,
          (item) => item.code?.toUpperCase() === this.textSearch?.toUpperCase()
        );
        if (itemResult) {
          this.termData?.push(itemResult);
          this.mapData();
        } else {
          this.messageService.warn(_.get(this.notificationMessage, 'branchNotExistOrNotInBranch'));
        }
        this.isLoading = false;
      } else if (option === 'customer') {
        const params = {
          customerCode: this.textSearch,
          customerType: this.form.get('division').value,
          rsId: this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`)?.rsId,
          scope: Scopes.VIEW,
          pageNumber: 0,
          pageSize: global?.userConfig?.pageSize,
        };

        let api: Observable<any>;

        // if (this.form.get('division').value === Division.SME) {
          api = this.customerApi.searchSme(params);
        // } else {
          // api = this.customerApi.search(params);
        // }

        api.pipe(
          finalize(() => {
            this.isLoading = false;
          })
        ).subscribe((data) => {
          if (data?.length > 0) {
            this.termData?.push({
              code: data[0]?.customerCode,
              name: data[0]?.customerName,
              customerCode: data[0]?.customerCode,
            });
            this.mapData();
          } else {
            this.messageService.warn(_.get(this.notificationMessage, 'customerNotExist'));
          }
        }, () => {
          this.messageService.error(this.notificationMessage.E001);
        });
      }
    } else {
      const messageReport = option.toString() + 'IsExist';
      this.messageService.warn(_.get(this.notificationMessage, messageReport));
      this.isLoading = false;
      return;
    }
  }

  setPage(pageInfo) {
    this.paramsTable.page = pageInfo.offset;
    this.mapData();
  }

  mapData() {
    const total = this.termData.length;
    this.pageable.totalElements = total;
    this.pageable.totalPages = Math.floor(total / this.pageable.size);
    this.pageable.currentPage = this.paramsTable.page;
    const start = this.paramsTable.page * this.paramsTable.size;
    this.listDataTable = this.termData?.slice(start, start + this.paramsTable.size);
  }

  removeRecord(item) {
    _.remove(this.termData, (i) => i.code === item.code);
    _.remove(this.listDataTable, (i) => i.code === item.code);
    if (this.listDataTable.length === 0 && this.pageable.currentPage > 0) {
      this.paramsTable.page -= 1;
    }
    this.listDataTable = [...this.listDataTable];
    this.allRM = false;
    this.mapData();
  }

  viewReport() {
    if (this.isLoading) {
      return;
    }

    // form data
    const formData = this.form.getRawValue();
    let { reportType, momentReport, momentCompare, period, reportPeriodYear, reportPeriodMonth } = formData;
    const target = 'REPORT_GUARANTEE';
    // variable
    let allDivision = '';
    let codeList = '';
    let option = this.getOption.toUpperCase();
    const reportBy = this.form.get('reportBy').value;
    let isReportByCustomer = reportBy === 'customer';
    const direction =  isReportByCustomer && this.form?.get('customerReportType').value === 'contract'  ? 'CONTRACT' : reportBy.toString().toUpperCase();

    // codeList
    this.termData?.map((i) => { codeList += i.code + ';' });
    codeList = codeList.substring(0, codeList.length - 1);
    codeList = this.form.get('listOption').value === 'ALL' ? 'ALL' : codeList;
    // division
    if (_.isEmpty(this.form.get('division').value)) {
      this.commonData.listDivision.forEach((item) => {
        if (!_.isEmpty(item.code)) {
          allDivision += item.code + ';';
        }
      });
      allDivision = allDivision.substring(0, allDivision.length - 1);
    } else {
      allDivision = this.form.get('division').value;
    }

    let reportParams: any = {
      p_lob: allDivision,
      p_rgon: 'ALL'
    };

    if (!this.form.valid) {
      validateAllFormFields(this.form);
      return;
    }
    // validate term
    const isTermDataEmpty = codeList !== 'ALL' && _.isEmpty(this.termData);
    if (isTermDataEmpty && option === 'RM') {
      this.messageService.warn(_.get(this.notificationMessage, 'rmValidation'));
      return;
    } else if (isTermDataEmpty && option === 'BRANCH' && reportBy != 'branch') {
      this.messageService.warn(_.get(this.notificationMessage, 'branchValidation'));
      return;
    } else if (this.termData.length > this.countRm && option === 'RM') {
      this.translate.get('notificationMessage.countRmMax', { number: this.countRm }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    } else if (this.termData.length > this.countBranchMax && option === 'BRANCH' && reportBy != 'branch') {
      this.translate.get('notificationMessage.countBranchMax', { number: this.countBranchMax }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    } else if (option === 'CUSTOMER') {
      if (isTermDataEmpty) {
        this.messageService.warn(_.get(this.notificationMessage, 'customerValidation'));
        return;
      }
      if (this.termData.length > this.countCustomerMax) {
        this.translate.get('notificationMessage.countCustomerLimit', { number: this.countCustomerMax }).subscribe((res) => {
          this.messageService.warn(res);
        });
        return;
      }
      }
    let rpCode;
    // thời điểm
    if (this.form.get('reportType').value === 'moment') {

      // if (formatDate(momentReport, 'dd/MM/yyyy', 'en') === formatDate(momentCompare, 'dd/MM/yyyy', 'en')) {
      //   this.messageService.warn(this.notificationMessage.momentCompareNoThanMomentReport);
      //   return;
      // }

      rpCode = `${target}_TD_${direction}`;
      reportParams.p_fr_date = formatDate(momentReport, 'yyyyMMdd', 'en');

      switch (direction) {
        case 'RM':
          if (option === 'BRANCH') {
            reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : 'ALL';
            reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : codeList;
            reportParams.p_br_code_lv1 = 'ALL';
          } else {
          reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : codeList;
          reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : 'ALL';
          reportParams.p_br_code_lv1 = 'ALL';
          }
          reportParams.p_to_date = formatDate(momentCompare, 'yyyyMMdd', 'en');
          break;
        case 'BRANCH':
          reportParams.p_br_code_lv2 = formData.branchCode;
          reportParams.p_br_code_lv1 = formData.branchCodeLv1;
          reportParams.p_to_date = formatDate(momentCompare, 'yyyyMMdd', 'en');
          break;
        case 'CUSTOMER':
          if (option === 'BRANCH') {
            reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : 'ALL';
            reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : codeList;
            reportParams.p_customer_id = 'ALL';
            reportParams.p_br_code_lv1 = 'ALL';
          } else if (option === 'RM') {
            reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : codeList;
            reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : 'ALL';
            reportParams.p_customer_id = 'ALL';
            reportParams.p_br_code_lv1 = 'ALL';
          } else {
          reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : 'ALL';
          reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : 'ALL';
          reportParams.p_customer_id = codeList;
          reportParams.p_br_code_lv1 = 'ALL';
          }
          break;
        case 'CONTRACT':
          if (option === 'BRANCH') {
            reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : 'ALL';
            reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : codeList;
            reportParams.p_customer_id = 'ALL';
            reportParams.p_br_code_lv1 = 'ALL';
          } else if (option === 'RM') {
            reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : codeList;
            reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : 'ALL';
            reportParams.p_customer_id = 'ALL';
            reportParams.p_br_code_lv1 = 'ALL';
          } else {
          reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : 'ALL';
          reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : 'ALL';
          reportParams.p_customer_id = codeList;
          reportParams.p_br_code_lv1 = 'ALL';
          }
        break;
        case 'REGION':
          reportParams.p_rgon = this.form.get('regionCode').value;
          reportParams.p_to_date = formatDate(momentCompare, 'yyyyMMdd', 'en');
          break;
      }
    } else {
      // Bình quân
      rpCode = `GUARANTEE_BQ_${direction}`;
      let p_year, p_year_2, p_time, p_time_2;
      let startDateReport = this.form.get('reportPeriodFrom').value;
      let startDateCompare = this.form.get('comparePeriodFrom').value;
      if (this.form.get('period').value === 'monthly') {
        if (!isReportByCustomer) {
          if (!_.isNull(startDateCompare) &&
            formatDate(startDateReport, 'MM/yyyy', 'en') === formatDate(startDateCompare, 'MM/yyyy', 'en')) {
            this.messageService.warn(this.notificationMessage.compareFromReportFrom);
            return;
          }
          p_time_2 = `T${Number(formatDate(startDateCompare, 'MM', 'en'))}`;
          p_year_2 = formatDate(startDateCompare, 'yyyy', 'en');
        }
        p_time = `T${Number(formatDate(startDateReport, 'MM', 'en'))}`;
        p_year = formatDate(startDateReport, 'yyyy', 'en');
      } else if (this.form.get('period').value === 'quarterly') {
        const reportPeriodQuarterly = this.form.get('reportPeriodQuarterly').value;
        const reportPeriodYear = this.form.get('reportPeriodYear').value;
        const comparePeriodQuarterly = this.form.get('comparePeriodQuarterly').value;
        const comparePeriodYear = this.form.get('comparePeriodYear').value;

        if (!isReportByCustomer) {
          if (_.isNull(comparePeriodQuarterly) && !_.isNull(comparePeriodYear)) {
            this.messageService.warn(this.notificationMessage.quarterlyValidation);
            return;
          } else if (!_.isNull(comparePeriodQuarterly) && _.isNull(comparePeriodYear)) {
            this.messageService.warn(this.notificationMessage.yearValidation);
            return;
          } else if (reportPeriodQuarterly === comparePeriodQuarterly && reportPeriodYear === comparePeriodYear) {
            this.messageService.warn(this.notificationMessage.compareFromReportFrom);
            return;
          }
          p_time_2 = `Q${comparePeriodQuarterly}`;
          p_year_2 = comparePeriodYear + '';
        }
        p_time = `Q${reportPeriodQuarterly}`;
        p_year = reportPeriodYear + '';
      } else if (this.form.get('period').value === 'yearly') {
        if (!isReportByCustomer) {
          if (!_.isNull(this.form.get('comparePeriodYear').value) &&
            this.form.get('reportPeriodYear').value === this.form.get('comparePeriodYear').value) {
            this.messageService.warn(this.notificationMessage.compareFromReportFrom);
            return;
          }
          p_time_2 = `YEAR`;
          p_year_2 = this.form.get('comparePeriodYear').value + '';
        }
        p_time = `YEAR`;
        p_year = this.form.get('reportPeriodYear').value + '';
      }
      reportParams.p_year = p_year;
      reportParams.p_time = p_time;
      if (!isReportByCustomer) {
        reportParams.p_year_2 = p_year_2;
        reportParams.p_time_2 = p_time_2;
      }
      switch (direction) {
        case 'RM':
          if (option === 'BRANCH') {
            reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : 'ALL';
            reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : codeList;
            reportParams.p_br_code_lv1 = 'ALL';
          } else {
          reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : codeList;
          reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : 'ALL';
          reportParams.p_br_code_lv1 = 'ALL';
          }
          reportParams.p_to_date = formatDate(momentCompare, 'yyyyMMdd', 'en');
          break;
        case 'BRANCH':
          reportParams.p_br_code_lv2 = formData.branchCode;
          reportParams.p_br_code_lv1 = formData.branchCodeLv1;
          break;
        case 'CUSTOMER':
          if (option === 'BRANCH') {
            reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : 'ALL';
            reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : codeList;
            reportParams.p_customer_id = 'ALL';
            reportParams.p_br_code_lv1 = 'ALL';
          } else if (option === 'RM') {
            reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : codeList;
            reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : 'ALL';
            reportParams.p_customer_id = 'ALL';
            reportParams.p_br_code_lv1 = 'ALL';
          } else {
          reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : 'ALL';
          reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : 'ALL';
          reportParams.p_customer_id = codeList;
          reportParams.p_br_code_lv1 = 'ALL';
          }
          break;
        case 'CONTRACT':
          if (option === 'BRANCH') {
            reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : 'ALL';
            reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : codeList;
            reportParams.p_customer_id = 'ALL';
            reportParams.p_br_code_lv1 = 'ALL';
          } else if (option === 'RM') {
            reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : codeList;
            reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : 'ALL';
            reportParams.p_customer_id = 'ALL';
            reportParams.p_br_code_lv1 = 'ALL';
          } else {
          reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : 'ALL';
          reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : 'ALL';
          reportParams.p_customer_id = codeList;
          reportParams.p_br_code_lv1 = 'ALL';
          }
        break;
        case 'REGION':
          reportParams.p_rgon = this.form.get('regionCode').value;
          break;
      }
    }


    // tableau params construct
    let params = {
      reportCode: rpCode,
      reportParams: this.formatParams(reportParams),
      rsId: this.objFunction?.rsId,
      scope: Scopes.VIEW
    };
    this.getLink(params);
  }

  formatParams(params): any {
    return Object.keys(params).map(key => ({ name: key, value: params[key] }));
  }

  getLink(params): void {
    this.isLoading = true;
      this.kpiReportApi.getTableauReport(params).pipe(
        finalize(() => {
          this.isLoading = false;
        })
      ).subscribe(embeddedLink => {
        if (embeddedLink.length > 0) {
          this.loadReport(embeddedLink, params);
        } else {
          this.messageService.error(this.notificationMessage.E001);
        }
      },
        () => {
          this.messageService.error(this.notificationMessage.E001);
        }
      );

  }

  loadReport(embeddedLink: string, paramSearch: any) {
    const title = 'Báo cáo bảo lãnh';
    const modal = this.modalService.open(KpiReportModalComponent, { windowClass: 'tree__report-modal' });
    modal.componentInstance.embeddedReport = embeddedLink;
    modal.componentInstance.data = null;
    modal.componentInstance.model = paramSearch;
    modal.componentInstance.baseUrl = null;
    modal.componentInstance.filename = this.fileName;
    modal.componentInstance.title = title;
    modal.componentInstance.extendUrl = '';
    modal.componentInstance.reportType = 1;
  }

  removeList() {
    this.termData = [];
    this.paramsTable.page = 0;
    this.allRM = false;
    this.mapData();
  }

  removeAll() {
    this.dataTerm.rm.term = [];
    this.dataTerm.branch.term = [];
    this.dataTerm.customer.term = [];
    this.termData = [];
    this.paramsTable.page = 0;
    this.allRM = false;
    this.mapData();
  }

  onChangeCheckAllRM(event) {
    if (event.checked) {
      this.textSearch = '';
      const params = {
        crmIsActive: true,
        scope: Scopes.VIEW,
        rmBlock: this.form.get('division').value,
        rsId: this.objFunction?.rsId,
        employeeCode: this.textSearch,
        page: 0,
        size: maxInt32,
        isNHSReport: this.form.get('division').value === Division.INDIV,
        isReportByRm: this.commonData.isRm,
        rm: true,
        manager: true,
      };
      this.isLoading = true;
      this.rmApi.post('findAll', params).subscribe(
        (data) => {
          this.termData = _.map(_.get(data, 'content'), (item) => {
            return {
              code: item?.t24Employee?.employeeCode,
              name: item?.hrisEmployee?.fullName,
              hrsCode: item?.hrisEmployee?.employeeId,
            };
          });
          this.termData = _.uniqBy(this.termData, (i) => i.hrsCode);
          this.paramsTable.page = 0;
          this.mapData();
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      this.termData = [];
      this.paramsTable.page = 0;
      this.mapData();
    }
  }

  isShowCheckboxAllRm() {
    return (
      this.currUser?.branch !== BRANCH_HO &&
      !this.commonData?.isRm &&
      this.form.get('division')?.value !== Division.INDIV &&
      this.form.get('reportBy')?.value === 'rm'
    );
  }


  initBranch(branchesByDomain) {
    // region, branch lv1 , branch lv2
    const regionObj = _.groupBy(branchesByDomain, 'region');
    const regionList = Object.keys(regionObj).map(key => ({ locationCode: key, locationName: key }));

    Object.keys(regionObj).forEach(key => {
      const branchLv1 = _.groupBy(regionObj[key], 'parentCode');
      const branchLv1List = Object.keys(branchLv1).map(branchKey => {
        return {
          code: branchLv1[branchKey][0].parentCode,
          name: branchLv1[branchKey][0].parentName,
          displayName: `${branchLv1[branchKey][0].parentCode} - ${branchLv1[branchKey][0].parentName}`,
          khuVucM: key,
          branch_lv2: branchLv1[branchKey].map(branch => {
            return { code: branch.code, name: branch.name, displayName: `${branch.code} - ${branch.name}`, khuVucM: key, parentCode: branchLv1[branchKey][0].parentCode }
          })
        }
      })
      if (!this.commonData.regionList[key]) {
        this.commonData.regionList[key] = {
          branch_lv1: branchLv1List
        }
      }
    });

    // all branch lv
    Object.keys(this.commonData.regionList).forEach(key => {
      this.commonData.regionList[key].branch_lv1.forEach(item => {
        this.commonData.allBranchLv2.push(...item.branch_lv2);
      });
    });

    Object.keys(this.commonData.regionList).forEach(key => {
      this.commonData.regionList[key].branch_lv1.forEach(item => {
        const branchLv1 = this.commonData.allBranchLv1.find(branch => branch.code === item.code);
        if (!branchLv1) this.commonData.allBranchLv1.push(item);
      });
    });

    this.commonData.listBranchTerm = this.commonData.allBranchLv2 || [];

    this.commonData.listBranch = _.cloneDeep(this.commonData.listBranchTerm);
    if (!_.find(this.commonData.listBranch, (item) => item.code === this.currUser?.branch)) {
      this.commonData.listBranch.push({
        code: this.currUser?.branch,
        displayName: `${this.currUser?.branch} - ${this.currUser?.branchName}`,
      });
    }

    // set all region
    if (this.commonData.isHO) {
      this.commonData.regionList = regionList;
    } else {
      regionList?.forEach((item) => {
        if (
          this.commonData.listBranch?.findIndex((i) => i.khuVucM === item?.locationCode) !== -1 &&
          this.commonData.regionList.findIndex((r) => r.locationCode === item?.locationCode) === -1
        ) {
          this.commonData.regionList.push(item);
        }
      });
    }

    // sort region list
    this.commonData.regionList = _.orderBy(this.commonData.regionList, [region => region.locationName], ['asc']);
    if (this.commonData.regionList.length > 1) {
      let allRegion = '';
      this.commonData.regionList?.map(item => {
        if(item.locationCode !== undefined && item.locationCode !== null && item.locationCode !== 'undefined'){
          allRegion += item.locationCode + ';';
        }
      })
      this.commonData.regionList.unshift(
        {
          locationCode: allRegion,
          locationName: "Tất cả"
        }
      )
    }

    if (this.commonData.regionList.length) {
      this.form.controls.regionCode.setValue(this.commonData.regionList[0].locationCode);
    }

  }

  // set default branch lv1, lv2
  setBranchOfUser() {
    this.setBranchLv1List();
  }

  setBranchLv1List(options: any = {}): void {
    const { branchCode } = this.form.getRawValue();
    const { setLv2List = true, resetCode = false } = options;
    let branchLv1List = [];

    if (resetCode) {
      this.form.controls.branchCode.setValue('', { emitEvent: false });
      this.form.controls.branchCodeLv1.setValue('', { emitEvent: false });
    }

    if (!branchLv1List.length) {
      this.commonData.branchLv1List = _.cloneDeep(this.commonData.allBranchLv1);
    } else {
      this.commonData.branchLv1List = branchLv1List;
    }

    // sort alphabe
    this.commonData.branchLv1List = _.orderBy(this.commonData.branchLv1List, [branch => branch.displayName], ['asc']);

    const haveAllOption = this.commonData.branchLv1List.find(branch => branch.code === '');
    if (this.commonData.branchLv1List.length > 1 && !haveAllOption) {
      this.commonData.branchLv1List.unshift({ code: 'ALL', displayName: 'Tất cả' });
    }

    let value = _.head(this.commonData.branchLv1List)?.code;

    // follow branch lv2
    if (branchCode && !setLv2List) {
      const branchLv2 = this.commonData.allBranchLv2.find(branch => branch.code === branchCode);

      if (branchLv2?.parentCode) value = branchLv2?.parentCode;
    }

    this.form.controls.branchCodeLv1.setValue(value, { emitEvent: false });

    if (setLv2List) {
      this.setBranchLv2List();
    }

  }

  setBranchLv2List(options: any = {}): void {
    const { branchCodeLv1 } = this.form.getRawValue();
    let { onChange = false, alreadyPick = false } = options;

    if (branchCodeLv1 && branchCodeLv1 !== 'ALL') {
      this.commonData.branchLv2List = this.commonData.allBranchLv2.filter(branch => branch.parentCode === branchCodeLv1);
    } else {
      this.commonData.branchLv2List = this.commonData.allBranchLv2;
    }

    // this.commonData.listBranch = _.cloneDeep(this.commonData.branchLv2List);

    this.commonData.branchLv2List = _.orderBy(this.commonData.branchLv2List, [branch => branch.displayName], ['asc']);

    this.setOptionAllToBrLv2();

    if (!onChange && !alreadyPick) {
      this.form.controls.branchCode.setValue(_.head(this.commonData.branchLv2List)?.code, { emitEvent: false });
    }
  }

  setOptionAllToBrLv2(): void {
    const haveAllOption = this.commonData.branchLv2List.find(branch => branch.code === '');
    if (this.commonData.branchLv2List.length > 1 && !haveAllOption) {
      this.commonData.branchLv2List.unshift({ code: 'ALL', displayName: 'Tất cả' });
    }
  }

  get showListOptions(): boolean {
    return ['rm', 'customer'].includes(this.form?.get('reportBy').value);
  }

  get choosenTitle(): string {
    switch (this.form?.controls?.reportBy.value) {
      case 'rm':
        return 'fields.selectRM';
      case 'customer':
        return 'fields.selectCustomer';
      case 'branch':
        return 'fields.choose_branch';
      default:
        return '';
    }
  }

  get choosenTitle2(): string {
    switch (this.form?.controls?.reportBy.value) {
      case 'rm':
        return 'fields.choose_branch';
      case 'customer':
        return 'fields.selectRM';
      default:
        return '';
    }
  }

  get getOption(): string {
    let side = this.form.get('reportBy').value;
    let option = this.form.get('listOption').value;
    let result = '';
    switch (side) {
      case 'rm':
        if (option === 'choose_rm2') {
          result = 'branch';
        } else {
          result = 'rm';
        }
        break;
      case 'branch':
        result = 'branch';
        break;
      case 'customer':
        if (option === 'choose_branch') {
          result = 'branch';
        } else if (option === 'choose_rm2') {
          result = 'rm';
        } else {
          result = 'customer';
        }
        break;
      case 'region':
        result = 'region';
        break;
    }
    return result;
  }

  get showButtonOptionRm(): boolean {
    let checkByCustomer = this.form.get('reportBy').value === 'customer' && !this.commonData.isRm;
    let checkByRm = this.form.get('reportBy').value === 'rm' && this.lenghtBranchesOfUser > 1;
    return checkByRm || checkByCustomer;
  }

  get showButtonOptionBranch(): boolean {
    return this.form.get('reportBy').value === 'customer' && this.lenghtBranchesOfUser > 1;
  }

  get hidePickupList(): boolean {
    return (
      this.form.get('listOption').value === 'ALL' &&
      ['rm', 'customer'].includes(this.form.get('reportBy').value)
    )
      ||
      ['region', 'branch'].includes(this.form.get('reportBy').value);
  }

}
