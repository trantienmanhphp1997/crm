import { KpiIndivReportComponent } from './kpi-indiv-report/kpi-indiv-report.component';
import { KpiReportRmTableComponent } from './kpi-report-rm-table/kpi-report-rm-table.component';
import { KpiReportBranchTableComponent } from './kpi-report-branch-table/kpi-report-branch-table.component';
import { BlockBankListComponent } from './block-bank-list/block-bank-list.component';
import { BlockBankTableBranchComponent } from './block-bank-table-branch/block-bank-table-branch.component';
import { BlockBankTableRmComponent } from './block-bank-table-rm/block-bank-table-rm.component';
import { KpiPlReportComponent } from './kpi-pl-report/kpi-pl-report.component';
import { ReportToiViewComponent } from './report-toi-view/report-toi-view.component';
import { ReportMobilizationComponent } from './report-mobilization/report-mobilization.component';
import { ReportCreditComponent } from './report-credit/report-credit.component';
import { ReportCustomerDevelopComponent } from './report-customer-develop/report-customer-develop.component';
import { ReportMapCustomerComponent } from './report-map-customer/report-map-customer.component';
import { ReportNsldSmeComponent } from './report-nsld-sme/report-nsld-sme.component';
import { ReportCustomerManagementTTTMComponent } from './report-customer-management-tttm/report-customer-management-tttm.component';
import { ReportGuaranteeComponent } from './report-guarantee/report-guarantee.component';
import { ReportAPComponent } from './report-account-planning/report-ap.component';
// import { ReportBusinessBranchComponent } from './report-business-branch/report-business-branch.component';
import { QueryMbalComponent } from './query-mbal/query-mbal.component';
import { QueryMbalDetailComponent } from './query-mbal-detail/query-mbal-detail.component';
import { QueryMbalDetailItemComponent } from './query-mbal-detail-item/query-mbal-detail-item.component';
import { ReportCreditV2Component } from './report-credit-v2/report-credit.component';
import { BizCustomerComponent } from './biz-customer/biz-customer.component';
import { ReportMobilizationV2Component } from './report-mobilization-v2/report-mobilization.component';
import { ReportGuaranteeV2 } from './report-guarantee-v2/app-report-guarantee.component';
import { ReportCustomerSaleComponent } from './report-customer-sales/report-customer-sales.component';
import {ReportAppComponent} from './report-app/report-app.component';
import { ReportCustomerManagementTttmV2 } from './report-customer-management-tttm-v2/report-customer-management-tttm-v2';
import { ReportPLComponent } from './report-pl/report-pl.component';
import { ReportToiV2Component } from './report-toi-v2/report-toi-v2.component';
import { ReportCustomerInactiveComponent } from './customer-inactive/customer-inactive.component';
export { KpiIndivReportComponent } from './kpi-indiv-report/kpi-indiv-report.component';
export { KpiReportRmTableComponent } from './kpi-report-rm-table/kpi-report-rm-table.component';
export { KpiReportBranchTableComponent } from './kpi-report-branch-table/kpi-report-branch-table.component';
export { BlockBankListComponent } from './block-bank-list/block-bank-list.component';
export { BlockBankTableBranchComponent } from './block-bank-table-branch/block-bank-table-branch.component';
export { BlockBankTableRmComponent } from './block-bank-table-rm/block-bank-table-rm.component';
export { KpiPlReportComponent } from './kpi-pl-report/kpi-pl-report.component';
export { ReportToiViewComponent } from './report-toi-view/report-toi-view.component';
export { ReportMobilizationComponent } from './report-mobilization/report-mobilization.component';
export { ReportCreditComponent } from './report-credit/report-credit.component';
export { ReportCustomerDevelopComponent } from './report-customer-develop/report-customer-develop.component';
export { ReportMapCustomerComponent } from './report-map-customer/report-map-customer.component';
export { ReportNsldSmeComponent } from './report-nsld-sme/report-nsld-sme.component';
export { ReportCustomerManagementTTTMComponent } from './report-customer-management-tttm/report-customer-management-tttm.component';
export { ReportGuaranteeComponent } from './report-guarantee/report-guarantee.component';
export { ReportAPComponent } from './report-account-planning/report-ap.component';
// export { ReportBusinessBranchComponent } from './report-business-branch/report-business-branch.component';
export { QueryMbalComponent } from './query-mbal/query-mbal.component';
export { QueryMbalDetailComponent } from './query-mbal-detail/query-mbal-detail.component';
export { QueryMbalDetailItemComponent } from './query-mbal-detail-item/query-mbal-detail-item.component';
export { ReportCreditV2Component } from './report-credit-v2/report-credit.component';

export const COMPONENTS = [
  KpiIndivReportComponent,
  KpiReportRmTableComponent,
  KpiReportBranchTableComponent,
  BlockBankListComponent,
  BlockBankTableBranchComponent,
  BlockBankTableRmComponent,
  KpiPlReportComponent,
  ReportToiViewComponent,
  ReportMobilizationComponent,
  ReportMobilizationV2Component,
  ReportCreditComponent,
  ReportCustomerDevelopComponent,
  ReportMapCustomerComponent,
  ReportNsldSmeComponent,
  ReportCustomerManagementTTTMComponent,
  ReportCustomerManagementTttmV2,
  ReportGuaranteeComponent,
  ReportAPComponent,
  // ReportBusinessBranchComponent,
  QueryMbalComponent,
  QueryMbalDetailComponent,
  QueryMbalDetailItemComponent,
  ReportCreditV2Component,
  BizCustomerComponent,
  ReportCustomerSaleComponent,
  ReportGuaranteeV2,
  ReportAppComponent,
  ReportPLComponent,
  ReportToiV2Component,
  ReportCustomerInactiveComponent
];
