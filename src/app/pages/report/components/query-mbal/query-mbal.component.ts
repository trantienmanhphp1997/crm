import { global } from '@angular/compiler/src/util';
import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import _ from 'lodash';
import { forkJoin, of } from 'rxjs';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { FileService } from 'src/app/core/services/file.service';
import {
  FunctionCode,
  Scopes,
} from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { cleanDataForm } from 'src/app/core/utils/function';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { CustomerApi } from 'src/app/pages/customer-360/apis/customer.api';
import { RmModalComponent } from 'src/app/pages/rm/components/rm-modal/rm-modal.component';
import { ActivationStart } from '@angular/router';

@Component({
  selector: 'app-query-mbal',
  templateUrl: './query-mbal.component.html',
  styleUrls: ['./query-mbal.component.scss'],
})
export class QueryMbalComponent extends BaseComponent implements OnInit {
  isLoading = false;
  @ViewChild('table') table: DatatableComponent;
  limit = global.userConfig.pageSize;
  pageable: Pageable;
  params: any = {
    pageNumber: 0,
    pageSize: global?.userConfig?.pageSize,
    rsId: '',
    scope: Scopes.VIEW,
  };

  propDetail: any;

  listData = [];

  listStatus = [
    { code: '1', displayName: 'Active' },
    { code: '2', displayName: 'Lapse' },
    { code: '3', displayName: 'Cancel in free look' },
  ];
  selectedStatus: any[] = [];
  listChannel = [
    { code: '1', displayName: 'Cá nhân' },
    { code: '2', displayName: 'Doanh nghiệp' },
  ];
  selectedChannel: any[] = [];

  listDivision = [{
    code: 'INDIV',
    displayName: 'INDIV - Khách hàng cá nhân'
  }];
  customerType = '';

  facilities: Array<any>;
  facilityCode: Array<string>;
  employeeCode: Array<string>;
  employeeCodeParams: Array<string>;
  termData = [];

  rangeDate = 90;
  now = new Date();
  commonData = {
    listStatus: [],
    minDate: this.setMinDate(),
    maxDate: this.now,
    minDateTo: this.setMinDate(),
    maxDateTo: this.now,
  };
  formSearch = this.fb.group({
    queryBy: 'rm',
    alPolicyHolderId: '',
    alPolicyHolderName: '',
    alPolicyNumber: '',
    dateFrom: [this.setMinDate(), CustomValidators.required],
    dateTo: [this.now, CustomValidators.required],
    rsId: '',
    scope: Scopes.VIEW,
  });

  textFooter = '';

  prevParams: any;

  total: number = 0;
  isOpenMore = true;

  dSnapshotDt = '';
  totalAllRM = 0;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private customerApi: CustomerApi,
    private fileService: FileService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.propDetail = this.router.getCurrentNavigation()?.extras?.state;
  }

  ngOnInit() {
    this.customerType = this.listDivision[0].code;
    this.initData();
    this.pageable = {
      totalElements: 0,
      totalPages: 1,
      currentPage: this.params.pageNumber,
      size: this.limit,
    };
    // this.employeeCode = _.get(this.fields, 'all');
  }

  initData() {
    this.employeeCode = _.get(this.fields, 'all');
    this.prop = this.sessionService.getSessionData(FunctionCode.QUERY_MBAL);

    this.router.events.subscribe((e) => {
      if (e instanceof ActivationStart && e.snapshot?.data?.code !== FunctionCode.QUERY_MBAL) {
        this.prop = null;
        this.sessionService.setSessionData(FunctionCode.QUERY_MBAL, null);
      }
    });
    
    if (this.prop) {
      // console.log(this.prop);
      this.prevParams = { ...this.prop?.prevParams };
      this.formSearch.controls.queryBy.setValue(this.prevParams.queryBy);
      this.formSearch.controls.alPolicyHolderId.setValue(this.prevParams.alPolicyHolderId);
      this.formSearch.controls.alPolicyHolderName.setValue(this.prevParams.alPolicyHolderName);
      this.formSearch.controls.alPolicyNumber.setValue(this.prevParams.alPolicyNumber);
      this.formSearch.controls.dateFrom.setValue(new Date(this.prevParams.dateFrom));
      this.formSearch.controls.dateTo.setValue(new Date(this.prevParams.dateTo));

      this.selectedStatus = this.prop?.prevParams?.listStatus || this.prop?.selectedStatus || [];
      this.selectedChannel = this.prop?.prevParams?.listSaleChannel || this.prop?.selectedChannel || [];
      this.facilities = this.prop?.listBranch || [];
      this.facilityCode = this.prop?.facilityCode || null;
      this.employeeCode = this.prop?.employeeCode || [];
      this.employeeCodeParams = this.prop?.employeeCodeParams || [];
      this.params.pageSize = this.prevParams.pageSize;
      this.params.pageNumber = this.prevParams.pageNumber;
      this.search(false, true);
    } else {
      this.setDefaultComboBox();
      this.isLoading = true;
      forkJoin([
        this.categoryService
          .getBranchesOfUser(this.objFunction.rsId, Scopes.VIEW)
          .pipe(catchError(() => of(undefined))),
      ]).subscribe(
        ([listBranch]) => {
          this.facilities = listBranch || [];
          this.search(true);
        },
        (e) => {
          console.log(e);
          this.messageService.error('Thực hiện không thành công');
          this.isLoading = false;
        }
      );
    }
  }

  setDefaultComboBox() {
    this.selectedChannel = [];
    this.selectedStatus = [];
    this.listStatus.map((item) => this.selectedStatus.push(item.code));
    this.listChannel.map((item) => this.selectedChannel.push(item.code));
  }

  resetTextBox() {
    this.formSearch.controls.alPolicyHolderId.setValue('');
    this.formSearch.controls.alPolicyHolderName.setValue('');
    this.formSearch.controls.alPolicyNumber.setValue('');
  }

  setMinDate() {
    let today = new Date();
    let firstDate = new Date();
    firstDate.setDate(today.getDate() - this.rangeDate);
    return firstDate;
  }

  setTextFooter(date: string) {
    this.translate.get('queryMBAL.textFooter', { date }).subscribe((text) => {
      this.textFooter = text;
    });
  }

  changeBranch(event) {
    this.facilityCode = event;
    this.removeRMSelected();
  }

  searchRM() {
    const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
    modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
    modal.componentInstance.branchCodes = this.facilityCode;
    modal.componentInstance.block = this.customerType;
    modal.result
      .then((res) => {
        if (res) {
          this.employeeCode = [];
          this.employeeCodeParams = [];
          const listRMSelect = res.listSelected?.map((item) => {
            this.employeeCode.push(item?.t24Employee?.employeeCode);
            this.employeeCodeParams.push(item?.hrisEmployee?.employeeId);
            return {
              hrsCode: item?.hrisEmployee?.employeeId,
              code: item?.t24Employee?.employeeCode,
              name: item?.hrisEmployee?.fullName,
            };
          });
          this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
          this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);

          this.translate.get('fields').subscribe((res) => {
            this.fields = res;
            const codes = this.employeeCode;
            const codeParams = this.employeeCodeParams;
            const countCode = (codes) => codes.reduce((a, b) => ({ ...a, [b]: (a[b] || 0) + 1 }), {});
            const countCodeParams = (codeParams) => codeParams.reduce((a, b) => ({ ...a, [b]: (a[b] || 0) + 1 }), {});
  
            this.employeeCode = Object.keys(countCode(codes));
            this.employeeCodeParams = Object.keys(countCodeParams(codeParams));
            this.employeeCode =
              _.size(this.employeeCode) === 0 ||
              (_.size(this.employeeCode) === 1 && this.employeeCode[0] === _.get(this.fields, 'all'))
                ? _.get(this.fields, 'all')
                : _.join(this.employeeCode, ', ');
          });

          if (this.employeeCode.length === 0) {
            this.removeRMSelected();
          }
        } else {
          this.removeRMSelected();
        }
      })
      .catch(() => {});
  }

  removeRMSelected() {
    this.employeeCode = _.get(this.fields, 'all');
    this.employeeCodeParams = null;
    this.termData = [];
  }

  removeBranchSelected() {
    this.facilityCode = null;
    this.removeRMSelected();
  }

  checkEmployeeCode() {
    let show = false;
    this.translate.get('fields').subscribe((res) => {
      this.fields = res;
      if (this.employeeCode === _.get(this.fields, 'all')) {
        show = false;
      } else {
        show = true;
      }
    });
    return show;
  }

  setPreviewParam() {
    this.prop = {};
    this.prop = {
      ...this.prop,
      selectedStatus: this.selectedStatus,
      selectedChannel: this.selectedChannel,
      listBranch: this.facilities,
      facilityCode: this.facilityCode,
      employeeCode: this.employeeCode,
      employeeCodeParams: this.employeeCodeParams,
    };
    this.sessionService.setSessionData(FunctionCode.QUERY_MBAL, this.prop);
  }

  ngAfterViewInit() {
    this.formSearch.controls.queryBy.valueChanges.subscribe((value) => {
      if (value === 'rm') {
        this.resetTextBox();
      }
      this.resetAdvanceForm();
    });

    this.formSearch.controls.dateFrom.valueChanges.subscribe((value) => {
      this.commonData.minDateTo = value;
    });
    
    this.formSearch.controls.dateTo.valueChanges.subscribe((value) => {
      this.commonData.maxDate = value;
    });
  }

  resetAdvanceForm() {
    this.setDefaultComboBox();
    this.removeBranchSelected();
    this.formSearch.controls.dateFrom.setValue(this.setMinDate());
    this.formSearch.controls.dateTo.setValue(this.now);
  }

  search(isSearch: boolean, hasPrev?: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.params.pageNumber = 0;
    }
    let params = {
      ...cleanDataForm(this.formSearch),
      listStatus:
        this.selectedStatus.length == this.listStatus.length || this.selectedStatus.length === 0
          ? null
          : this.selectedStatus,
      listSaleChannel:
        this.selectedChannel.length == this.listChannel.length || this.selectedChannel.length === 0
          ? null
          : this.selectedChannel,
      listBranch: (_.isEmpty(this.facilityCode) || this.facilityCode === _.get(this.fields, 'all')) ? null : this.facilityCode,
      // listRM: this.employeeCode === _.get(this.fields, 'all') ? null : this.employeeCode,
      listRM: this.checkEmployeeCode() ? this.employeeCodeParams : null,
      pageNumber: this.params.pageNumber,
      pageSize: this.params.pageSize,
      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW,
    };
    params.dateFrom = this.convertDateToString(params.dateFrom);
    params.dateTo = this.convertDateToString(params.dateTo);

    if(hasPrev) {
      params.listRM = this.prevParams.listRM;
    }
    // console.log(this.facilityCode, this.employeeCode);
    this.setPreviewParam();
    // console.log(params);
    this.customerApi.insuranceLife(params).subscribe(
      (result) => {
        this.prevParams = params;
        this.prop.prevParams = params;

        let listDataTemp = result.content || [];
        this.dSnapshotDt = this.reFormatDateString(result.dsnapshotDt);
        this.totalAllRM = result.totalAllRM;
        if (listDataTemp.length > 0) {
          listDataTemp.forEach((item) => {
            if (item.branch && item.branchName) {
              item.branchDisplay = item.branch + ' - ' + item.branchName;
            } else {
              item.branchDisplay = '';
            }

            item.rmDisplay = '';
            const mbAgencyCode = item.mbAgencyCode ? item.mbAgencyCode : '';
            const mbAgencyName = item.mbAgencyName ? item.mbAgencyName : '';
            if (item.mbAgencyCode || item.mbAgencyName) {
              item.rmDisplay = mbAgencyCode + ' - ' + mbAgencyName;
            }
          });
        }

        if (this.dSnapshotDt) {
          this.setTextFooter(this.dSnapshotDt);
        }

        this.listData = listDataTemp;

        this.pageable = {
          totalElements: result.totalElements,
          totalPages: result.totalPages,
          currentPage: this.params.pageNumber,
          size: result.size,
        };
        this.isLoading = false;
      },
      (e) => {
        if (e.error.code) {
          this.messageService.error(e.error.description);
          this.isLoading = false;
          return;
        }
        this.messageService.error(this.notificationMessage.error);
        this.isLoading = false;
      }
    );
  }

  reFormatDateString(dateStr: string) {
    let result = '---';
    let arr = [];
    if (dateStr) {
      arr = dateStr.split('-');
      result = arr[2] + '-' + arr[1] + '-' + arr[0];
    }
    return result;
  }

  convertDateToString(dateString) {
    let date = new Date(dateString);
    return (
      date.getFullYear() +
      '-' +
      (date.getMonth() > 8 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1)) +
      '-' +
      (date.getDate() > 9 ? date.getDate() : '0' + date.getDate())
    );
  }

  openMore() {
    this.isOpenMore = !this.isOpenMore;
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.pageNumber = pageInfo.offset;
    this.search(false);
  }

  handleChangePageSize(event) {
    this.params.pageNumber = 0;
    this.params.pageSize = event;
    this.limit = event;
    this.search(false);
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const rowData = _.get(event, 'row');
      let paramDetail = this.setParamsDetail();
      paramDetail.mbReferalId = rowData?.mbReferalId || null;
      paramDetail.branch = rowData?.branch || null;
      if (this.formSearch.get('queryBy').value === 'kh') {
        paramDetail.alPolicyNumber = rowData?.alPolicyNumber || null;
      }

      this.router.navigate([this.router.url, 'detail'], {
        skipLocationChange: true,
        queryParams: {
          alPolicyHolderId: paramDetail.alPolicyHolderId || '',
          alPolicyHolderName: paramDetail.alPolicyHolderName || '',
          alPolicyNumber: paramDetail.alPolicyNumber || '',
          dateFrom: paramDetail.dateFrom || '',
          dateTo: paramDetail.dateTo || '',
          listBranch: paramDetail.listBranch || null,
          listSaleChannel: paramDetail.listSaleChannel || null,
          listStatus: paramDetail.listStatus || null,
          mbReferalId: paramDetail.mbReferalId || null,
          branch: paramDetail.branch || null,
          queryBy: paramDetail.queryBy || '',
          rsId: paramDetail.rsId || '',
          scope: paramDetail.scope || '',
        },
      });
    }
  }

  viewDetail(event) {
    const mbReferalId = _.get(event, 'mbReferalId');
    const branch = _.get(event, 'branch');
    let paramDetail = this.setParamsDetail();
    paramDetail.mbReferalId = mbReferalId || null;
    paramDetail.branch = branch || null;
    this.router.navigate([this.router.url, 'detail'], {
      skipLocationChange: true,
      queryParams: {
        alPolicyHolderId: paramDetail.alPolicyHolderId || '',
        alPolicyHolderName: paramDetail.alPolicyHolderName || '',
        alPolicyNumber: paramDetail.alPolicyNumber || '',
        dateFrom: paramDetail.dateFrom || '',
        dateTo: paramDetail.dateTo || '',
        listBranch: paramDetail.listBranch || null,
        listSaleChannel: paramDetail.listSaleChannel || null,
        listStatus: paramDetail.listStatus || null,
        mbReferalId: paramDetail.mbReferalId || null,
        branch: paramDetail.branch || null,
        queryBy: paramDetail.queryBy || '',
        rsId: paramDetail.rsId || '',
        scope: paramDetail.scope || '',
      },
    });
  }

  setParamsDetail() {
    let paramExport = {
      ...cleanDataForm(this.formSearch),
      listStatus: this.selectedStatus.length == this.listStatus.length ? null : this.selectedStatus,
      listSaleChannel: this.selectedChannel.length == this.listChannel.length ? null : this.selectedChannel,
      listBranch: this.facilityCode === _.get(this.fields, 'all') ? null : this.facilityCode,
      // listRM: this.employeeCode === _.get(this.fields, 'all') ? null : this.employeeCode,
      pageNumber: this.params.pageNumber,
      pageSize: this.params.pageSize,
      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW,
    };
    paramExport.dateFrom = this.convertDateToString(paramExport.dateFrom);
    paramExport.dateTo = this.convertDateToString(paramExport.dateTo);

    return paramExport;
  }

  exportFile() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+this.pageable?.totalElements <= 0) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.isLoading = true;
    let paramExport = {
      ...cleanDataForm(this.formSearch),
      listStatus: this.selectedStatus.length == this.listStatus.length ? null : this.selectedStatus,
      listSaleChannel: this.selectedChannel.length == this.listChannel.length ? null : this.selectedChannel,
      listBranch: this.facilityCode === _.get(this.fields, 'all') ? null : this.facilityCode,
      // listRM: this.employeeCode === _.get(this.fields, 'all') ? null : this.employeeCode,
      listRM: this.checkEmployeeCode() ? this.employeeCodeParams : null,
      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW,
    };
    paramExport.dateFrom = this.convertDateToString(paramExport.dateFrom);
    paramExport.dateTo = this.convertDateToString(paramExport.dateTo);
    // console.log(paramExport);
    this.customerApi.excelQueryMBAL(paramExport).subscribe(
      (res) => {
        if (res) {
          this.download(res);
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(_.get(this.notificationMessage, 'export_error'));
        this.isLoading = false;
      }
    );
  }

  download(fileId: string) {
    let todayExcel = new Date();
    let queryBy = this.formSearch.controls.queryBy.value === 'rm' ? 'RM' : 'HD';
    let titleExcel =
      'HDBH_theo_' +
      queryBy +
      '_' +
      todayExcel.getDate().toString() +
      (todayExcel.getMonth() + 1).toString() +
      todayExcel.getFullYear().toString() +
      '.xlsx';
    this.fileService.downloadFile(fileId, titleExcel).subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }
}