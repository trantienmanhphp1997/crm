import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QueryMbalComponent } from './query-mbal.component';

describe('QueryMbalComponent', () => {
  let component: QueryMbalComponent;
  let fixture: ComponentFixture<QueryMbalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueryMbalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueryMbalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
