import { forkJoin, of, Subject } from 'rxjs';
import { Component, OnInit, Injector, AfterViewInit } from '@angular/core';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { TableColumn } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import { CommonCategory, FunctionCode, maxInt32, Scopes, SessionKey } from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as moment from 'moment';
import { catchError } from 'rxjs/operators';
import { KpiReportApi } from '../../apis';
import { KpiReportModalComponent } from '../../dialogs';
import { RmApi } from 'src/app/pages/rm/apis';
import { RmModalComponent } from 'src/app/pages/rm/components/rm-modal/rm-modal.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { saveAs } from 'file-saver';
import { environment } from 'src/environments/environment';
import { formatDate } from '@angular/common';
import { validateAllFormFields } from 'src/app/core/utils/function';

@Component({
  selector: 'report-customer-develop',
  templateUrl: './report-customer-develop.component.html',
  styleUrls: ['./report-customer-develop.component.scss'],
})
export class ReportCustomerDevelopComponent extends BaseComponent implements OnInit, AfterViewInit {
  commonData = {
    listDivision: [],
    minDate: null,
    maxDate: new Date(),
    listYears: [],
    isRm: false,
  };
  textSearch: string;
  paramsTable = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };
  form = this.fb.group({
    division: '',
    reportFrom: [new Date(moment().add(-1, 'day').toDate()), CustomValidators.required],
    reportTo: [new Date(moment().add(-1, 'day').toDate()), CustomValidators.required],
    momentCompare: null,
    reportBy: 'rm',
    downloadReport: 'HTML',
    term: '',
  });
  dataTerm = {
    rm: {
      pageable: { ...this.pageable },
      term: [],
    },
  };
  tableType: string;
  termData = [];
  listDataTable = [];
  columns: TableColumn[];
  isSearch = false;
  fileName = 'bao-cao-phat-trien-khach-hang';
  countRm: any;
  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private rmApi: RmApi,
    private kpiReportApi: KpiReportApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.tableType = this.form.get('reportBy').value;
    this.columns = [
      {
        name: this.fields.rmCode,
        prop: 'code',
      },
      {
        name: this.fields.rmName,
        prop: 'name',
      },
    ];
    this.prop = this.sessionService.getSessionData(SessionKey.REPORT_PT_KH);
    if (!this.prop) {
      const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
      if (!_.isEmpty(divisionOfUser) && divisionOfUser?.length === 1) {
        divisionOfUser?.forEach((item) => {
          this.commonData.listDivision.push({ code: item.code, name: `${item.code || ''} - ${item.name || ''}` });
        });
        this.form.get('division').setValue(_.first(this.commonData.listDivision)?.code);
      } else {
        this.commonData.listDivision =
          divisionOfUser?.map((item) => {
            return { code: item.code, name: `${item.code || ''} - ${item.name || ''}` };
          }) || [];
        this.commonData.listDivision.unshift({ code: '', name: this.fields.all });
      }
      forkJoin([
        this.commonService.getCommonCategory(CommonCategory.REPORTS_YEARS).pipe(catchError(() => of(undefined))),
        this.categoryService
          .getBranchesOfUser(this.objFunction.rsId, Scopes.VIEW)
          .pipe(catchError(() => of(undefined))),
        this.categoryService
          .getCommonCategory(CommonCategory.REPORTS_MAX_LENGTH_RM, CommonCategory.MAX_RM_REPORT_CUSTOMER)
          .pipe(catchError(() => of(undefined))),
      ]).subscribe(([configYear, branchesOfUser, maxRm]) => {
        this.countRm = _.get(maxRm, 'content[0].value', 0);
        this.commonData.isRm = _.isEmpty(branchesOfUser);
        const yearConfig = _.find(_.get(configYear, 'content'), (item) => item.code === 'PTKH')?.value || 1;
        const year = moment().year();
        for (let i = 0; i < yearConfig; i++) {
          this.commonData.listYears.push({
            value: year - i,
            label: year - i,
          });
        }
        this.commonData.minDate = new Date(
          moment()
            .add(-(yearConfig - 1), 'year')
            .startOf('year')
            .valueOf()
        );
        this.sessionService.setSessionData(SessionKey.REPORT_PT_KH, this.commonData);
        this.isLoading = false;
      });
    } else {
      this.commonData = this.prop;
      this.form.get('division').setValue(_.first(this.commonData.listDivision)?.code);
      this.isLoading = false;
    }
    this.textSearch = this.currUser.code;
    if (this.currUser.code) {
      this.add();
    }
  }

  ngAfterViewInit() {
    this.form.controls.reportBy.valueChanges.subscribe((value) => {
      if (this.tableType !== value) {
        this.paramsTable.page = 0;
        this.dataTerm[this.tableType] = {
          pageable: { ...this.pageable },
          term: [...this.termData],
        };
        this.textSearch = '';
        this.columns[0].name = this.fields.rmCode;
        this.columns[1].name = this.fields.rmName;
        this.termData = [...this.dataTerm.rm.term];
        this.pageable = { ...this.dataTerm.rm.pageable };
        this.tableType = value;
        this.mapData();
      }
    });
    this.form.controls.division.valueChanges.subscribe(() => {
      this.removeList();
    });
  }

  search() {
    const formSearch = {
      crmIsActive: { value: 'true', disabled: true },
      rmBlock: {
        value: this.form.get('division').value,
        disabled: true,
      },
      isNHSReport: true,
      rm: true,
    };
    const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
    modal.componentInstance.dataSearch = formSearch;
    modal.componentInstance.rsId = this.objFunction?.rsId;
    modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
    modal.componentInstance.isManager = true;
    modal.result
      .then((res) => {
        if (res) {
          const listRMSelect = res.listSelected?.map((item) => {
            return {
              hrsCode: item?.hrisEmployee?.employeeId,
              code: item?.t24Employee?.employeeCode,
              name: item?.hrisEmployee?.fullName,
            };
          });
          this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
          this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);
          this.mapData();
        }
      })
      .catch(() => {});
  }

  add() {
    this.textSearch = _.trim(this.textSearch);
    if (this.textSearch.length === 0) {
      this.isSearch = false;
      return;
    }
    if (_.findIndex(this.termData, (i) => i.code?.toUpperCase() === this.textSearch?.toUpperCase()) === -1) {
      const params = {
        manager: true,
        rm: true,
        crmIsActive: true,
        scope: Scopes.VIEW,
        rmBlock: this.form.get('division').value,
        rsId: this.objFunction?.rsId,
        employeeCode: this.textSearch,
        page: 0,
        size: maxInt32,
        isNHSReport: true,
      };
      this.isSearch = true;
      this.rmApi
        .post('findAll', params)
        .pipe(catchError(() => of(undefined)))
        .subscribe((data) => {
          const itemResult = _.find(
            _.get(data, 'content'),
            (item) => item?.t24Employee?.employeeCode?.toUpperCase() === this.textSearch?.toUpperCase()
          );
          if (itemResult) {
            this.termData?.push({
              code: itemResult?.t24Employee?.employeeCode,
              name: itemResult?.hrisEmployee?.fullName,
              hrsCode: itemResult?.hrisEmployee?.employeeId,
            });
            this.mapData();
          } else {
            this.messageService.warn(_.get(this.notificationMessage, 'rmNotExistOrNotBranh'));
          }
          this.textSearch = '';
          this.isSearch = false;
        });
    } else {
      const messageReport = this.form.get('reportBy').value.toString() + 'IsExist';
      this.messageService.warn(_.get(this.notificationMessage, messageReport));
      this.isSearch = false;
      return;
    }
  }

  setPage(pageInfo) {
    this.paramsTable.page = pageInfo.offset;
    this.mapData();
  }

  mapData() {
    const total = this.termData.length;
    this.pageable.totalElements = total;
    this.pageable.totalPages = Math.floor(total / this.pageable.size);
    this.pageable.currentPage = this.paramsTable.page;
    const start = this.paramsTable.page * this.paramsTable.size;
    this.listDataTable = this.termData?.slice(start, start + this.paramsTable.size);
  }

  removeRecord(item) {
    _.remove(this.termData, (i) => i.code === item.code);
    _.remove(this.listDataTable, (i) => i.code === item.code);
    if (this.listDataTable.length === 0 && this.pageable.currentPage > 0) {
      this.paramsTable.page -= 1;
    }
    this.listDataTable = [...this.listDataTable];
    this.mapData();
  }

  viewReport() {
    if (this.form.valid) {
      let reportFrom = this.form.get('reportFrom').value;
      let reportTo = this.form.get('reportTo').value;
      let momentCompare = this.form.get('momentCompare').value;
      if (
        formatDate(reportTo, 'dd/MM/yyyy', 'en') === formatDate(momentCompare, 'dd/MM/yyyy', 'en') &&
        !_.isNull(momentCompare)
      ) {
        this.messageService.warn(this.notificationMessage.momentCompareNoThanMomentReport);
        return;
      }
      momentCompare = _.isNull(momentCompare) ? null : formatDate(momentCompare, 'dd/MM/yyyy', 'en');
      if (moment(reportTo).startOf('day').isBefore(moment(reportFrom).startOf('day'))) {
        this.messageService.warn(this.notificationMessage.startDateMoreThanEndDatePl);
        return;
      }

      reportFrom = formatDate(reportFrom, 'dd/MM/yyyy', 'en');
      reportTo = formatDate(reportTo, 'dd/MM/yyyy', 'en');
      if (_.isEmpty(this.termData)) {
        this.messageService.warn(_.get(this.notificationMessage, 'rmValidation'));
        return;
      } else {
        if (this.termData.length > this.countRm) {
          this.translate.get('notificationMessage.countRmMax', { number: this.countRm }).subscribe((res) => {
            this.messageService.warn(res);
          });
          return;
        }
      }
      const allDivision = [];
      if (_.isEmpty(this.form.get('division').value)) {
        this.commonData.listDivision.forEach((item) => {
          if (!_.isEmpty(item.code)) {
            allDivision.push(item.code);
          }
        });
      } else {
        allDivision.push(this.form.get('division').value);
        if (this.form.get('division').value === 'INDIV') {
          allDivision.push('NHS');
        }
      }
      const dataRm = this.termData?.map((i) => i.code);
      const params = {
        attributeFormat: 'pdf',
        data: dataRm,
        division: allDivision,
        reportFrom,
        reportTo,
        momentCompare,
        reportBy: this.form.get('reportBy').value,
        rsId: this.objFunction?.rsId,
        scope: Scopes.VIEW,
      };
      if (this.form.get('downloadReport').value === 'EXCEL') {
        params.attributeFormat = 'xlsx';
      }
      this.isLoading = true;
      forkJoin([this.kpiReportApi.reportCustomerDevelop(params)]).subscribe(
        ([pdfId]) => {
          const respondSource = new Subject<any>();
          this.kpiReportApi.checkIdReportSuccess(pdfId, respondSource);
          respondSource.asObservable().subscribe((res) => {
            if (res?.error === 'error') {
              this.messageService.error(this.notificationMessage.E001);
            } else if (res?.fileId) {
              this.loadFile(res?.fileId, params);
            } else {
              this.messageService.error(this.notificationMessage.messageOrsReport);
            }
            this.isLoading = false;
            respondSource.unsubscribe();
          });
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      validateAllFormFields(this.form);
    }
  }

  loadFile(pdfId: string, paramSearch: any) {
    if (this.form.get('downloadReport').value === 'EXCEL') {
      forkJoin([this.kpiReportApi.loadFile(`download/${pdfId}`)]).subscribe(
        ([blobExcel]) => {
          saveAs(blobExcel, `${this.fileName}.xlsx`);
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      this.kpiReportApi.loadFile(`download/${pdfId}`).then(
        (blobPDF) => {
          const url = `${environment.url_endpoint_kpi}/sales-report/ptkh`;
          if (this.form.get('downloadReport').value === 'HTML') {
            const modal = this.modalService.open(KpiReportModalComponent, { windowClass: 'tree__report-modal' });
            modal.componentInstance.data = blobPDF;
            modal.componentInstance.model = paramSearch;
            modal.componentInstance.baseUrl = url;
            modal.componentInstance.filename = this.fileName;
            modal.componentInstance.title = 'Báo cáo phát triển khách hàng';
            modal.componentInstance.extendUrl = '';
          } else if (this.form.get('downloadReport').value === 'PDF') {
            saveAs(blobPDF, `${this.fileName}.pdf`);
          }
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    }
  }

  removeList() {
    this.termData = [];
    this.paramsTable.page = 0;
    this.mapData();
  }
}
