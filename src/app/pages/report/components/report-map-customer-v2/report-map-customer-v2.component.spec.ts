import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportMapCustomerV2Component } from './report-map-customer-v2.component';

describe('ReportMapCustomerV2Component', () => {
  let component: ReportMapCustomerV2Component;
  let fixture: ComponentFixture<ReportMapCustomerV2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportMapCustomerV2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportMapCustomerV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
