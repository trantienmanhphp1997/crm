import { global } from '@angular/compiler/src/util';
import { AfterViewInit, Component, Injector, OnInit } from '@angular/core';
import { TableColumn } from '@swimlane/ngx-datatable';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';
import { formatDate } from '@angular/common';
import * as moment from 'moment';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';

import {
  BRANCH_HO,
  CommonCategory,
  Division,
  FunctionCode,
  maxInt32,
  Scopes
} from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { RmModalComponent } from 'src/app/pages/rm/components/rm-modal/rm-modal.component';
import { CustomerModalComponent } from 'src/app/pages/customer-360/components/customer-modal/customer-modal.component';
import { RmApi } from 'src/app/pages/rm/apis';
import { CustomerApi } from 'src/app/pages/customer-360/apis';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { KpiReportApi } from '../../apis';
import { KpiReportModalComponent } from '../../dialogs';
import {
  ChooseBranchesModalComponent
} from 'src/app/pages/system/components/choose-branches-modal/choose-branches-modal.component';
import { CustomValidators } from '../../../../core/utils/custom-validations';
import { DashboardService } from '../../../dashboard/services/dashboard.service';

@Component({
  selector: 'app-report-map-customer-v2',
  templateUrl: './report-map-customer-v2.component.html',
  styleUrls: ['./report-map-customer-v2.component.scss']
})
export class ReportMapCustomerV2Component extends BaseComponent implements OnInit, AfterViewInit {
  commonData = {
    listDivision: [],
    listBranchs: [],
    minDate: null,
    maxDate: new Date(),
    listQuarterly: [],
    listYears: [],
    listYearsCompare: [],
    listQuarterlyCompare: [],
    isRm: true,
    listBranch: [],
    listBranchv2: [],
    regionList: [],
    targetList: [],
    isRegionDirector: false,
    listRegionDirectorTitle: [],
    maxPeriodByYear: 5,
    maxPeriodByMonth: 2,
    listMonth: [],
    allBranchLv2: [],
    allBranchLv1: [],
    listBranchTerm: [],
    isHO: false,
    branchLv1List: [],
    listCurrencyUnit: []
  };
  textSearch: string;
  lengthBranchesOfUser: number;
  paramsTable = {
    page: 0,
    size: global?.userConfig?.pageSize
  };
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize
  };
  form = this.fb.group({
    target: [''],
    division: 'SME',
    currencyType: '',
    currencyUnit: '',
    reportType: 'moment',
    momentReport: [new Date(moment().add(-1, 'day').toDate()), CustomValidators.required],
    momentCompare: null,
    period: 'PS Ngày',
    reportPeriodFrom: [new Date(moment().add(-1, 'months').toDate())],
    reportPeriodTo: '',
    comparePeriodFrom: null,
    comparePeriodTo: null,
    reportBy: 'rm',
    reportTypeDetail: 'customer',

    reportPeriodMonth: [new Date(moment().add(-1, 'months').toDate())],
    reportPeriodYear: '',

    comparePeriodQuarterly: '',
    comparePeriodYear: '',
    downloadReport: 'HTML',
    distributeBy: 'loanTerm',
    listOption: 'ALL',
    regionCode: '',
    customerReportType: 'sync',
    groupBy: 'rm',
    branchCodeLv1: [[]],
    branchCode: [''],
    branchLv1List: []
  });
  allRM = false;
  dataTerm = {
    rm: {
      pageable: { ...this.pageable },
      term: []
    },
    branch: {
      pageable: { ...this.pageable },
      term: []
    },
    customer: {
      pageable: { ...this.pageable },
      term: []
    }
  };
  tableType: string;
  termData = [];
  listDataTable = [];
  columns: TableColumn[];
  maxDate = new Date();
  fileName = 'bao-cao-tin-dung';
  countRm: any;
  countBranchMax: number;
  maxMonth: any;
  countCustomerMax: number;
  objFunctionRMManager: any;
  intervalDownloadFileS3 = null;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private rmApi: RmApi,
    private customerApi: CustomerApi,
    private kpiReportApi: KpiReportApi,
    private dashboardService: DashboardService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REPORT_MAP_KH_V2}`);
    this.objFunctionRMManager = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.isLoading = true;
  }

  get showListOptions(): boolean {
    return ['rm', 'customer'].includes(this.form?.get('reportBy').value);
  }

  get choosenTitle(): string {
    switch (this.form?.controls?.reportBy.value) {
      case 'rm':
        return 'fields.selectRM';
      case 'customer':
        return 'fields.selectCustomer';
      case 'branch':
        return 'fields.choose_branch';
      default:
        return '';
    }
  }

  get hidePickupList(): boolean {
    return this.form.get('listOption').value === 'ALL' || ['region'].includes(this.form.get('listOption').value);
  }

  ngOnInit(): void {
    this.form.controls.reportBy.setValue('customer');
    this.removeList();
    this.tableType = this.form.get('reportBy').value;
    this.commonData.isHO = this.currUser?.branch === BRANCH_HO;
    this.columns = [
      {
        name: this.fields.rmCode,
        prop: 'code'
      },
      {
        name: this.fields.rmName,
        prop: 'name'
      }
    ];


    forkJoin([
      this.categoryService
        .getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined))),
      this.categoryService
        .getCommonCategory(CommonCategory.REPORTS_MAX_LENGTH_RM)
        .pipe(catchError(() => of(undefined))),
      this.commonService
        .getCommonCategory(CommonCategory.CURRENCY_UNIT_REPORTS)
        .pipe(catchError(() => of(undefined))),
      this.dashboardService.getBranchByDomain({
        rsId: this.objFunction?.rsId,
        scope: Scopes.VIEW
      })
    ]).subscribe(([branchesOfUser, configReport, currencyUnits, branchesByDomain]) => {

      this.countRm =
        _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.TABLEAU_MAX_RM_REPORT)?.value ||
        0;
      this.countBranchMax =
        _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.TABLEAU_MAX_BRANCH_REPORT)?.value ||
        0;
      this.countCustomerMax =
        _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.TABLEAU_MAX_CUSTOMER_REPORT)?.value || 0;
      this.commonData.listCurrencyUnit = _.orderBy(_.get(currencyUnits, 'content'), ['orderNum'], ['asc', 'desc']);
      this.commonData.listCurrencyUnit =
        _.get(currencyUnits, 'content')?.map((item) => {
          return { code: item.value, name: item.name, isDefault: item.isDefault };
        }) || [];
      this.form.get('currencyUnit').setValue(_.first(this.commonData.listCurrencyUnit)?.code);
      this.commonData.listCurrencyUnit.forEach((item) => {
        if (item.isDefault) {
          this.form.get('currencyUnit').setValue(item?.code);
        }
      });
      this.commonData.minDate = new Date(
        moment()
          .add(-12, 'months')
          .valueOf()
      );

      this.commonData.listBranch = branchesOfUser || [];
      this.commonData.isRm = _.isEmpty(branchesOfUser);
      this.lengthBranchesOfUser = branchesOfUser.length;

      // set region list
      this.commonData.isRegionDirector = this.objFunction.scopes.includes('VIEW_REGION_REPORT');

      this.initBranch(branchesByDomain);
      this.setBranchOfUser();
      this.textSearch = this.currUser.code;
      if (this.commonData.isRm && this.currUser.code) {
        this.add();
      }
      this.isLoading = false;
    });
  }

  ngAfterViewInit() {
    this.form.controls.listOption.valueChanges.subscribe((value) => {
      console.log(value);
      if (value === 'customer' || value === 'choose_customer') {
        this.initPickupList('customer');
        return;
      }
      if (value === 'rm' || value === 'choose_rm') {
        this.initPickupList('rm');
        return;
      }
      if (value === 'branch' || value === 'choose_branch') {
        this.initPickupList('branch');
        return;
      }
      if (this.tableType !== value) {
        this.initPickupList(value);
      }
    });
  }

  initPickupList(side: string): void {
    this.allRM = false;
    this.paramsTable.page = 0;
    this.dataTerm[this.tableType] = {
      pageable: { ...this.pageable },
      term: [...this.termData]
    };
    this.textSearch = '';

    switch (side) {
      case 'rm':
        this.columns[0].name = this.fields.rmCode;
        this.columns[1].name = this.fields.rmName;
        this.termData = [...this.dataTerm.rm.term];
        this.pageable = { ...this.dataTerm.rm.pageable };
        if (this.commonData.isRm) {
          this.textSearch = this.currUser.code;
          if (this.termData.length === 0 && this.currUser.code) {
            this.add();
          }
        } else {
          this.textSearch = '';
        }
        break;
      case 'branch':
        this.columns[0].name = this.fields.branchCode;
        this.columns[1].name = this.fields.branchName;
        this.termData = [...this.dataTerm.branch.term];
        this.pageable = { ...this.dataTerm.branch.pageable };
        break;
      case 'customer':
        this.columns[0].name = this.fields.customerCode;
        this.columns[1].name = this.fields.customerName;
        this.termData = [...this.dataTerm.customer.term];
        this.pageable = { ...this.dataTerm.customer.pageable };
        break;
    }
    this.tableType = side;
    this.mapData();
  }

  search() {
    if (this.form.get('listOption').value === 'rm' || (this.form.get('listOption').value === 'choose_rm')) {
      const formSearch = {
        crmIsActive: { value: 'true', disabled: true },
        rmBlock: {
          value: this.form.get('division').value,
          disabled: true
        },
        isNHSReport: this.form.get('division').value === Division.INDIV,
        isReportByRm: this.commonData.isRm,
        manager: true,
        rm: true
      };
      const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
      modal.componentInstance.dataSearch = formSearch;
      modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
      modal.result
        .then((res) => {
          if (res) {
            const listRMSelect = res.listSelected?.map((item) => {
              return {
                hrsCode: item?.hrisEmployee?.employeeId,
                code: item?.t24Employee?.employeeCode,
                name: item?.hrisEmployee?.fullName
              };
            });
            this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
            this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);
            this.mapData();
          }
        })
        .catch(() => {
        });
    } else if (this.form.get('listOption').value === 'branch') {
      const modal = this.modalService.open(ChooseBranchesModalComponent, { windowClass: 'tree__branches-modal' });
      modal.componentInstance.listBranchOld = _.map(this.termData, (x) => x.code) || [];
      modal.componentInstance.listBranch = this.commonData.listBranch;
      modal.result.then((res) => {
        if (res) {
          this.termData = res;
          this.mapData();
        }
      })
        .catch(() => {
        });
    } else if (this.form.get('listOption').value === 'customer') {
      const formSearch = {
        customerType: {
          value: this.form.get('division').value,
          disabled: true
        }
      };
      const modal = this.modalService.open(CustomerModalComponent, { windowClass: 'list__customer360-modal' });
      modal.componentInstance.listSelectedOld = this.termData;
      modal.componentInstance.dataSearch = formSearch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData =
              res.listSelected?.map((item) => {
                return {
                  code: item.customerCode,
                  customerCode: item.customerCode,
                  name: item.customerName ? item.customerName : item.name
                };
              }) || [];
            this.mapData();
          }
        })
        .catch(() => {
        });
    }
  }

  add() {
    console.log(this.form.get('listOption').value);
    this.textSearch = _.trim(this.textSearch);
    if (this.textSearch.length === 0) {
      this.isLoading = false;
      return;
    }
    if (_.findIndex(this.termData, (i) => i.code?.toUpperCase() === this.textSearch?.toUpperCase()) === -1) {
      this.isLoading = true;
      if (this.form.get('listOption').value === 'rm') {
        const params = {
          crmIsActive: true,
          scope: Scopes.VIEW,
          rmBlock: this.form.get('division').value,
          rsId: this.objFunctionRMManager?.rsId,
          employeeCode: this.textSearch,
          page: 0,
          size: maxInt32,
          isNHSReport: this.form.get('division').value === Division.INDIV,
          isReportByRm: this.commonData.isRm,
          rm: true,
          manager: true
        };
        this.isLoading = true;
        this.rmApi.post('findAll', params).subscribe(
          (data) => {
            const itemResult = _.find(
              _.get(data, 'content'),
              (item) => item?.t24Employee?.employeeCode?.toUpperCase() === this.textSearch?.toUpperCase()
            );
            if (itemResult) {
              this.termData?.push({
                code: itemResult?.t24Employee?.employeeCode,
                name: itemResult?.hrisEmployee?.fullName,
                hrsCode: itemResult?.hrisEmployee?.employeeId
              });
              this.termData = _.uniqBy(this.termData, (i) => i.hrsCode);
              this.mapData();
            } else if (!itemResult && !this.allRM) {
              this.messageService.warn(_.get(this.notificationMessage, 'rmNotExistOrNotBranh'));
            }
            this.isLoading = false;
          },
          () => {
            this.messageService.error(this.notificationMessage.E001);
            this.isLoading = false;
          }
        );
      } else if (this.form.get('listOption').value === 'branch') {
        const itemResult = _.find(
          this.commonData.listBranch,
          (item) => item.code?.toUpperCase() === this.textSearch?.toUpperCase()
        );
        if (itemResult) {
          this.termData?.push(itemResult);
          this.mapData();
        } else {
          this.messageService.warn(_.get(this.notificationMessage, 'branchNotExistOrNotInBranch'));
        }
        this.isLoading = false;
      } else if (this.form.get('listOption').value === 'customer') {
        const params = {
          customerCode: this.textSearch,
          customerType: this.form.get('division').value,
          rsId: this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`)?.rsId,
          scope: Scopes.VIEW,
          pageNumber: 0,
          pageSize: global?.userConfig?.pageSize
        };

        let api: Observable<any>;

        // if (this.form.get('division').value === Division.SME) {
        api = this.customerApi.searchSme(params);
        // } else {
        // api = this.customerApi.search(params);
        // }

        api.pipe(
          finalize(() => {
            this.isLoading = false;
          })
        ).subscribe((data) => {
          if (data?.length > 0) {
            this.termData?.push({
              code: data[0]?.customerCode,
              name: data[0]?.customerName,
              customerCode: data[0]?.customerCode
            });
            this.mapData();
          } else {
            this.messageService.warn(_.get(this.notificationMessage, 'customerNotExist'));
          }
        }, () => {
          this.messageService.error(this.notificationMessage.E001);
        });
      }
    } else {
      let messageReport = this.form.get('listOption').value.toString() + 'IsExist';
      if (this.form.get('listOption').value.toString() === 'choose_rm') {
        messageReport = this.form.get('reportBy').value.toString() + 'IsExist';
      }
      this.messageService.warn(_.get(this.notificationMessage, messageReport));
      this.isLoading = false;
      return;
    }
  }

  setPage(pageInfo) {
    this.paramsTable.page = pageInfo.offset;
    this.mapData();
  }

  mapData() {
    const total = this.termData.length;
    this.pageable.totalElements = total;
    this.pageable.totalPages = Math.floor(total / this.pageable.size);
    this.pageable.currentPage = this.paramsTable.page;
    const start = this.paramsTable.page * this.paramsTable.size;
    this.listDataTable = this.termData?.slice(start, start + this.paramsTable.size);
  }

  removeRecord(item) {
    _.remove(this.termData, (i) => i.code === item.code);
    _.remove(this.listDataTable, (i) => i.code === item.code);
    if (this.listDataTable.length === 0 && this.pageable.currentPage > 0) {
      this.paramsTable.page -= 1;
    }
    this.listDataTable = [...this.listDataTable];
    this.allRM = false;
    this.mapData();
  }

  viewReport() {
    if (this.isLoading) {
      return;
    }

    // form data
    const formData = this.form.getRawValue();
    const { momentReport, momentCompare, target, period, reportPeriodYear, reportPeriodMonth } = formData;

    // variable
    let codeList = '';
    let pDate = '';
    let tmpDate;
    const listOption = this.form.get('listOption').value;
    const selectAll = this.form.get('listOption').value === 'ALL';
    // codeList
    this.termData?.map((i) => {
      codeList += i.code + ';';
    });
    codeList = codeList.substring(0, codeList.length - 1);
    codeList = this.form.get('listOption').value === 'ALL' ? 'ALL' : codeList;
    // report time

    tmpDate = new Date(reportPeriodMonth.getFullYear(), reportPeriodMonth.getMonth() + 1, 0, 23, 59, 59);
    pDate = formatDate(tmpDate, 'yyyyMMdd', 'en');
    const reportParams: any = {
      p_fr_date: pDate
    };


    if (!this.form.valid) {
      validateAllFormFields(this.form);
      return;
    }
    const direction = listOption.toUpperCase();
    console.log(direction);
    switch (direction) {
      case 'CUSTOMER':
        reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : 'ALL';
        reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : 'ALL';
        reportParams.p_br_code_lv1 = 'ALL';
        reportParams.p_rgon = 'ALL';
        reportParams.p_customer_id = codeList;
        reportParams.p_unit = formData.currencyUnit;
        break;
      case 'RM':
        reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : codeList;
        reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : 'ALL';
        reportParams.p_br_code_lv1 = 'ALL';
        reportParams.p_rgon = 'ALL';
        reportParams.p_customer_id = 'ALL';
        reportParams.p_unit = formData.currencyUnit;
        break;
      case 'BRANCH':
        reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : 'ALL';
        reportParams.p_br_code_lv2 = codeList || '';
        reportParams.p_br_code_lv1 = 'ALL';
        reportParams.p_rgon = 'ALL';
        reportParams.p_customer_id = 'ALL';
        reportParams.p_unit = formData.currencyUnit;
        break;
      case 'REGION':
        reportParams.p_rm_code = 'ALL';
        reportParams.p_br_code_lv1 = 'ALL';
        reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : 'ALL';
        reportParams.p_rgon = this.form.get('regionCode').value;
        reportParams.p_customer_id = 'ALL';
        reportParams.p_unit = formData.currencyUnit;
        break;
      default:
        reportParams.p_rm_code = this.commonData.isRm ? this.currUser.code : 'ALL';
        reportParams.p_br_code_lv1 = 'ALL';
        reportParams.p_br_code_lv2 = this.commonData.isRm ? this.currUser.branch : 'ALL';
        reportParams.p_rgon = 'ALL';
        reportParams.p_customer_id = 'ALL';
        reportParams.p_unit = formData.currencyUnit;
        break;
    }

    // validate term
    const isTermDataEmpty = codeList !== 'ALL' && _.isEmpty(this.termData);
    if (isTermDataEmpty && (listOption.toUpperCase() === 'RM' || direction === 'RM' && listOption === 'choose_rm')) {
      this.messageService.warn(_.get(this.notificationMessage, 'rmValidation'));
      return;
    }
    if (isTermDataEmpty && (direction === 'BRANCH' || listOption.toUpperCase() === 'BRANCH')) {
      this.messageService.warn(_.get(this.notificationMessage, 'branchValidation'));
      return;
    }
    if (this.termData.length > this.countRm && (listOption.toUpperCase() === 'RM' || direction === 'RM' && listOption === 'choose_rm')) {
      this.translate.get('notificationMessage.countRmMax', { number: this.countRm }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    if (this.termData.length > this.countBranchMax && (direction === 'BRANCH' || listOption.toUpperCase() === 'BRANCH')) {
      this.translate.get('notificationMessage.countBranchMax', { number: this.countBranchMax }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    } else if (direction === 'CUSTOMER') {
      if (isTermDataEmpty) {
        this.messageService.warn(_.get(this.notificationMessage, 'customerValidation'));
        return;
      }
      if (this.termData.length > this.countCustomerMax) {
        this.translate.get('notificationMessage.countCustomerLimit', { number: this.countCustomerMax }).subscribe((res) => {
          this.messageService.warn(res);
        });
        return;
      }
    }

    // tableau params construct
    const reportCode = 'REPORT_MAP_CUSTOMER';
    const params = {
      reportCode,
      reportParams: this.formatParams(reportParams),
      rsId: this.objFunction?.rsId,
      scope: Scopes.VIEW
    };
    this.getLink(params);
  }

  formatParams(params): any {
    return Object.keys(params).map(key => ({ name: key, value: params[key] }));
  }

  getLink(params): void {
    this.isLoading = true;
    this.kpiReportApi.getTableauReport(params).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(embeddedLink => {
        if (embeddedLink.length > 0) {
          this.loadReport(embeddedLink, params);
        } else {
          this.messageService.error(this.notificationMessage.E001);
        }
      },
      () => {
        this.messageService.error(this.notificationMessage.E001);
      }
    );

  }

  loadReport(embeddedLink: string, paramSearch: any) {
    const { target } = this.form.getRawValue();
    const title = 'Báo cáo bản đồ khách hàng';
    const modal = this.modalService.open(KpiReportModalComponent, { windowClass: 'tree__report-modal' });
    modal.componentInstance.embeddedReport = embeddedLink;
    modal.componentInstance.data = null;
    modal.componentInstance.model = paramSearch;
    modal.componentInstance.baseUrl = null;
    modal.componentInstance.filename = this.fileName;
    modal.componentInstance.title = title;
    modal.componentInstance.extendUrl = '';
    modal.componentInstance.reportType = 1;
  }

  removeList() {
    this.termData = [];
    this.paramsTable.page = 0;
    this.allRM = false;
    this.mapData();
  }

  onChangeCheckAllRM(event) {
    if (event.checked) {
      this.textSearch = '';
      const params = {
        crmIsActive: true,
        scope: Scopes.VIEW,
        rmBlock: this.form.get('division').value,
        rsId: this.objFunction?.rsId,
        employeeCode: this.textSearch,
        page: 0,
        size: maxInt32,
        isNHSReport: this.form.get('division').value === Division.INDIV,
        isReportByRm: this.commonData.isRm,
        rm: true,
        manager: true
      };
      this.isLoading = true;
      this.rmApi.post('findAll', params).subscribe(
        (data) => {
          this.termData = _.map(_.get(data, 'content'), (item) => {
            return {
              code: item?.t24Employee?.employeeCode,
              name: item?.hrisEmployee?.fullName,
              hrsCode: item?.hrisEmployee?.employeeId
            };
          });
          this.termData = _.uniqBy(this.termData, (i) => i.hrsCode);
          this.paramsTable.page = 0;
          this.mapData();
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      this.termData = [];
      this.paramsTable.page = 0;
      this.mapData();
    }
  }

  isShowCheckboxAllRm() {
    return (
      this.currUser?.branch !== BRANCH_HO &&
      !this.commonData?.isRm &&
      this.form.get('division')?.value !== Division.INDIV &&
      this.form.get('reportBy')?.value === 'rm'
    );
  }

  initBranch(branchesByDomain) {
    // region, branch lv1 , branch lv2
    const regionObj = _.groupBy(branchesByDomain, 'region');
    const regionList = Object.keys(regionObj).map(key => ({ locationCode: key, locationName: key }));

    Object.keys(regionObj).forEach(key => {
      const branchLv1 = _.groupBy(regionObj[key], 'parentCode');
      const branchLv1List = Object.keys(branchLv1).map(branchKey => {
        return {
          code: branchLv1[branchKey][0].parentCode,
          name: branchLv1[branchKey][0].parentName,
          displayName: `${branchLv1[branchKey][0].parentCode} - ${branchLv1[branchKey][0].parentName}`,
          khuVucM: key,
          branch_lv2: branchLv1[branchKey].map(branch => {
            return {
              code: branch.code,
              name: branch.name,
              displayName: `${branch.code} - ${branch.name}`,
              khuVucM: key,
              parentCode: branchLv1[branchKey][0].parentCode
            };
          })
        };
      });
      if (!this.commonData.regionList[key]) {
        this.commonData.regionList[key] = {
          branch_lv1: branchLv1List
        };
      }
    });

    // all branch lv
    Object.keys(this.commonData.regionList).forEach(key => {
      this.commonData.regionList[key].branch_lv1.forEach(item => {
        this.commonData.allBranchLv2.push(...item.branch_lv2);
      });
    });

    Object.keys(this.commonData.regionList).forEach(key => {
      this.commonData.regionList[key].branch_lv1.forEach(item => {
        const branchLv1 = this.commonData.allBranchLv1.find(branch => branch.code === item.code);
        if (!branchLv1) this.commonData.allBranchLv1.push(item);
      });
    });

    this.commonData.listBranchTerm = this.commonData.allBranchLv2 || [];

    this.commonData.listBranch = _.cloneDeep(this.commonData.listBranchTerm);
    if (!_.find(this.commonData.listBranch, (item) => item.code === this.currUser?.branch)) {
      this.commonData.listBranch.push({
        code: this.currUser?.branch,
        displayName: `${this.currUser?.branch} - ${this.currUser?.branchName}`
      });
    }

    // set all region
    if (this.commonData.isHO) {
      this.commonData.regionList = regionList;
    } else {
      regionList?.forEach((item) => {
        if (
          this.commonData.listBranch?.findIndex((i) => i.khuVucM === item?.locationCode) !== -1 &&
          this.commonData.regionList.findIndex((r) => r.locationCode === item?.locationCode) === -1
        ) {
          this.commonData.regionList.push(item);
        }
      });
    }

    // sort region list
    this.commonData.regionList = _.orderBy(this.commonData.regionList, [region => region.locationName], ['asc']);
    if (this.commonData.regionList.length > 1) {
      let allRegion = '';
      this.commonData.regionList?.map(item => {
        if (item.locationCode !== undefined && item.locationCode !== null && item.locationCode !== 'undefined') {
          allRegion += item.locationCode + ';';
        }
      });
      allRegion = allRegion.substring(0, allRegion.length - 1);
      this.commonData.regionList.unshift(
        {
          locationCode: allRegion,
          locationName: 'Tất cả'
        }
      );
    }

    if (this.commonData.regionList.length) {
      this.form.controls.regionCode.setValue(this.commonData.regionList[0].locationCode);
    }

  }

  // set default branch lv1, lv2
  setBranchOfUser() {
    this.setBranchLv1List();
  }

  setBranchLv1List(options: any = {}): void {
    const { branchCode } = this.form.getRawValue();
    const { setLv2List = true, resetCode = false } = options;
    const branchLv1List = [];

    if (resetCode) {
      this.form.controls.branchCode.setValue('', { emitEvent: false });
      this.form.controls.branchCodeLv1.setValue('', { emitEvent: false });
    }

    if (!branchLv1List.length) {
      this.commonData.branchLv1List = _.cloneDeep(this.commonData.allBranchLv1);
    } else {
      this.commonData.branchLv1List = branchLv1List;
    }

    // sort alphabe
    this.commonData.branchLv1List = _.orderBy(this.commonData.branchLv1List, [branch => branch.displayName], ['asc']);

    const haveAllOption = this.commonData.branchLv1List.find(branch => branch.code === '');
    if (this.commonData.branchLv1List.length > 1 && !haveAllOption) {
      this.commonData.branchLv1List.unshift({ code: 'ALL', displayName: 'Tất cả' });
    }
    let value = _.head(this.commonData.branchLv1List)?.code;

    // follow branch lv2
    if (branchCode && !setLv2List) {
      const branchLv2 = this.commonData.allBranchLv2.find(branch => branch.code === branchCode);

      if (branchLv2?.parentCode) value = branchLv2?.parentCode;
    }

    this.form.controls.branchCodeLv1.setValue(value, { emitEvent: false });

    if (setLv2List) {
      this.setBranchLv2List();
    }

  }

  setBranchLv2List(options: any = {}): void {
    let branchLv2List = [];
    const { branchCodeLv1 } = this.form.getRawValue();
    const { onChange = false, alreadyPick = false } = options;

    if (branchCodeLv1 && branchCodeLv1 !== 'ALL') {
      branchLv2List = this.commonData.allBranchLv2.filter(branch => branch.parentCode === branchCodeLv1);
    } else {
      branchLv2List = this.commonData.allBranchLv2;
    }

    this.commonData.listBranchv2 = _.cloneDeep(branchLv2List);

    this.commonData.listBranchv2 = _.orderBy(this.commonData.listBranchv2, [branch => branch.displayName], ['asc']);

    this.setOptionAllToBrLv2();

    if (!onChange && !alreadyPick) {
      this.form.controls.branchCode.setValue(_.head(this.commonData.listBranchv2)?.code, { emitEvent: false });
    }
  }

  setOptionAllToBrLv2(): void {
    const haveAllOption = this.commonData.listBranchv2.find(branch => branch.code === '');
    if (this.commonData.listBranchv2.length > 1 && !haveAllOption) {
      this.commonData.listBranchv2 = _.cloneDeep(this.commonData.listBranchv2);
      this.commonData.listBranchv2.unshift({ code: 'ALL', displayName: 'Tất cả' });
    }
  }

}
