import { Component, EventEmitter, HostBinding, Input, ChangeDetectorRef, Output, ViewChild } from '@angular/core';
import { RmModalComponent } from '../../../rm/components/rm-modal/rm-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import _ from 'lodash';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { Utils } from 'src/app/core/utils/utils';
import { global } from '@angular/compiler/src/util';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NotifyMessageService } from '../../../../core/components/notify-message/notify-message.service';
import { TranslateService } from '@ngx-translate/core';
import { RmApi } from '../../apis';

@Component({
  selector: 'kpi-report-rm-table',
  templateUrl: './kpi-report-rm-table.component.html',
  styleUrls: ['./kpi-report-rm-table.component.scss'],
})
export class KpiReportRmTableComponent {
  constructor(
    private modalService: NgbModal,
    private messageService: NotifyMessageService,
    private ref: ChangeDetectorRef,
    private api: RmApi,
    private translate: TranslateService
  ) {
    this.messages = global.messageTable;
    this.translate.get(['fields', 'notificationMessage']).subscribe((result) => {
      this.notificationMessage = _.get(result, 'notificationMessage');
      this.fields = _.get(result, 'fields');
    });
  }
  notificationMessage: any;
  fields: any;
  listTemp: Array<any> = [];
  params = {
    page: 0,
    size: global.userConfig.pageSize,
    search: '',
  };
  @Input() values: Array<any>;
  @HostBinding('class.app__right-content') appRightContent = true;
  @Input() isManager: boolean;
  @Input() isRm: boolean;
  @Input() model: any;
  @Input() rsid: string;
  pageable: Pageable;
  isSearch = true;
  textSearch = '';
  prev_block: string;
  listRm: Array<any>;
  isLoading: boolean;
  messages: any;
  // models: Array<any>;
  @ViewChild('table') table: DatatableComponent;
  search_value: string;
  @Output() modelChange = new EventEmitter();
  @Input() title: string;
  @Input() rmCode: string;
  rmCodeTitle: string;
  rmNameTitle: string;
  param: any;

  ngOnInit(): void {
    if (Utils.isArrayNotEmpty(this.model)) {
      this.listTemp = this.model;
      this.calculatePageble(this.listTemp);
    }
    if (this.isManager) {
      this.rmCodeTitle = _.get(this.fields, 'managerCode');
      this.rmNameTitle = _.get(this.fields, 'managerName');
    }
    if (this.isRm) {
      this.rmCodeTitle = _.get(this.fields, 'rmCode');
      this.rmNameTitle = _.get(this.fields, 'rmName');
      if (Utils.isArrayEmpty(this.model)) {
        this.search_value = this.rmCode;
        if (Utils.isStringNotEmpty(this.rmCode)) {
          this.add();
        }
      }
    }
  }

  selectMembers() {
    const formSearch = {
      crmIsActive: { value: 'true', disabled: true },
      rmBlock: {
        value: 'INDIV',
        disabled: true,
      },
      rm: this.isRm,
    };
    const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
    console.log('listTemp: ', this.listTemp);
    modal.componentInstance.dataSearch = formSearch;
    modal.componentInstance.listHrsCode = _.map(this.listTemp, (x) => x.hrsCode);
    modal.componentInstance.isManager = this.isManager;
    modal.componentInstance.isRm = this.isRm;
    modal.result
      .then((res) => {
        if (res) {
          console.log('rm selected: ',res);
          const listSelected = _.map(_.get(res, 'listSelected'), (x) => {
            return {
              hrsCode: _.get(x, 'hrisEmployee.employeeId'),
              rmCode: _.get(x, 't24Employee.employeeCode'),
              rmFullName: _.get(x, 'hrisEmployee.fullName'),
              branchCode: _.get(x, 't24Employee.branchCode'),
              branchName: _.get(x, 't24Employee.branchName'),
            };
          });
          this.listTemp = _.uniqBy([...this.listTemp, ...listSelected], (i) => i.hrsCode);
          this.listTemp = _.remove(this.listTemp, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);
         // this.listTemp = [...listSelected];
          this.modelChange.emit(this.listTemp);
          this.search();
        }
      })
      .catch(() => {});
  }

  add() {
    this.isLoading = true;
    if (Utils.isStringNotEmpty(this.search_value)) {
      this.search_value = Utils.isStringNotEmpty(this.search_value) ? Utils.trim(this.search_value) : '';
      if (Utils.isNotNull(_.find(this.listTemp, (x) => x.rmCode.toLowerCase() === this.search_value.toLowerCase()))) {
        this.messageService.error(_.get(this.notificationMessage, 'rmCodeIsExist'));
        this.isLoading = false;
        return;
      }
      if (this.isRm) {
        this.param = {
          page: 0,
          size: 10,
          crmIsActive: true,
          scope: 'VIEW',
          rsId: this.rsid,
          rmBlock: 'INDIV',
          search: '',
          employeeCode: this.search_value,
          rm: true,
        };
      } else if (this.isManager) {
        this.param = {
          page: 0,
          size: 10,
          crmIsActive: true,
          scope: 'VIEW',
          rmBlock: 'INDIV',
          rsId: this.rsid,
          search: '',
          employeeCode: this.search_value,
          manager: true,
        };
      }
      this.api.post('findAll', this.param).subscribe(
        (response) => {
          let content = _.get(response, 'content');
          let rm = _.first(content);
          if (Utils.isNotNull(rm)) {
            this.listTemp.push({
              hrsCode: _.get(rm, 'hrisEmployee.employeeId'),
              rmCode: _.get(rm, 't24Employee.employeeCode'),
              rmFullName: _.get(rm, 'hrisEmployee.fullName'),
              branchCode: _.get(rm, 't24Employee.branchCode'),
              branchName: _.get(rm, 't24Employee.branchName'),
            });
            this.search();
            this.modelChange.emit(this.listTemp);
            this.search_value = '';
          } else {
            this.messageService.error(_.get(this.notificationMessage, 'rmCodeNotMatch'));
          }
          this.isLoading = false;
        },
        () => {
          this.messageService.error(_.get(this.notificationMessage, 'E001'));
          this.isLoading = false;
          return;
        }
      );
    } else {
      this.messageService.error(_.get(this.notificationMessage, 'rmCodeEmpty'));
      this.isLoading = false;
      return;
    }
    this.ref.detectChanges();
  }

  setPage(pageInfo) {
    this.params.page = pageInfo.offset;
    this.search();
  }

  deleteMember(row) {
    _.remove(this.listTemp, (x) => x.rmCode === _.get(row, 'rmCode'));
    this.search();
    this.modelChange.emit(this.listTemp);
  }

  getValue(row, key) {
    return _.get(row, key);
  }

  search() {
    let codes = _.map(this.listTemp, (x) => x.id);
    let lists = _.chain(this.listTemp)
      .filter((item) => codes.includes(item.id))
      .value();
    this.calculatePageble(lists);
  }

  validate(count) {
    if (count > 30) {
      if (this.isManager) {
        this.messageService.warn('Số lượng CBQL Không được vượt quá 30');
        return false;
      } else if (this.isRm) {
        this.messageService.warn('Số lượng RM Không được vượt quá 30');
        return false;
      }
    }
    return true;
  }

  deleteAll() {
    this.listTemp = [];
    this.calculatePageble(this.listTemp);
    this.modelChange.emit(this.listTemp);
  }

  calculatePageble(datas: Array<any>) {
    this.listRm = _.slice(
      datas,
      this.params.page * this.params.size,
      this.params.page * this.params.size + this.params.size
    );
    this.pageable = {
      totalElements: _.size(datas),
      totalPages: _.size(datas) / this.params.size,
      currentPage: _.get(this.params, 'page'),
      size: _.get(this.params, 'size'),
    };
  }
}
