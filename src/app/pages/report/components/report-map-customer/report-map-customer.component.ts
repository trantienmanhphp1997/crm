import { global } from '@angular/compiler/src/util';
import { AfterViewInit, ChangeDetectorRef, Component, Injector, OnInit } from '@angular/core';
import { TableColumn } from '@swimlane/ngx-datatable';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';
import { saveAs } from 'file-saver';
import { formatDate } from '@angular/common';
import * as moment from 'moment';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';

import {
  CommonCategory,
  Division,
  FunctionCode,
  maxInt32,
  Scopes,
  SessionKey,
} from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { RmModalComponent } from 'src/app/pages/rm/components/rm-modal/rm-modal.component';
import { CustomerModalComponent } from 'src/app/pages/customer-360/components/customer-modal/customer-modal.component';
import { RmApi } from 'src/app/pages/rm/apis';
import { CustomerApi } from 'src/app/pages/customer-360/apis';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { KpiReportApi } from '../../apis';
import { environment } from 'src/environments/environment';
import { KpiReportModalComponent } from '../../dialogs';
import { ChooseBranchesModalComponent } from 'src/app/pages/system/components/choose-branches-modal/choose-branches-modal.component';

@Component({
  templateUrl: './report-map-customer.component.html',
  styleUrls: ['./report-map-customer.component.scss'],
})
export class ReportMapCustomerComponent extends BaseComponent implements OnInit, AfterViewInit {
  countRm: any;
  countCustomerMax: any;
  countBranchMax = 0;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private rmApi: RmApi,
    private customerApi: CustomerApi,
    private kpiReportApi: KpiReportApi,
    private cdk: ChangeDetectorRef
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REPORT_MAP_KH}`);
  }
  form = this.fb.group({
    downloadReport: 'HTML',
    division: '',
    currencyUnit: '',
    momentReport: new Date(moment().year(), moment().month() - 1),
    reportBy: 'rm',
    attributeFormat: '',
  });
  textSearch = '';
  listDataTable = [];
  paramsTable = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };
  columns: TableColumn[];
  isSearch = false;
  commonData = {
    listDivision: [],
    minDate: null,
    maxDate: new Date(),
    isRm: false,
    listCurrencyUnit: [],
    listBranch: [],
  };
  dataTerm = {
    rm: {
      pageable: { ...this.pageable },
      term: [],
    },
    customer: {
      pageable: { ...this.pageable },
      term: [],
    },
    branch: {
      pageable: { ...this.pageable },
      term: [],
    },
  };
  termData = [];
  tableType: string;
  fileName = 'ban_do_khach_hang';

  reportByToPCase = {
    rm: 1,
    customer: 2,
    branch: 3,
  };
  showBranch = false;

  ngOnInit(): void {
    this.tableType = this.form.get('reportBy').value;
    this.columns = [
      {
        name: this.fields.rmCode,
        prop: 'code',
      },
      {
        name: this.fields.rmName,
        prop: 'name',
      },
    ];
    this.getData();
  }

  ngAfterViewInit() {
    this.form.controls.reportBy.valueChanges.subscribe((value) => {
      if (this.tableType !== value) {
        this.paramsTable.page = 0;
        this.dataTerm[this.tableType] = {
          pageable: { ...this.pageable },
          term: [...this.termData],
        };
        this.textSearch = '';
        if (value === 'rm') {
          this.columns[0].name = this.fields.rmCode;
          this.columns[1].name = this.fields.rmName;
          this.termData = [...this.dataTerm.rm.term];
          this.pageable = { ...this.dataTerm.rm.pageable };
          if (this.commonData.isRm) {
            this.textSearch = this.currUser.code;
            if (this.termData.length === 0 && this.currUser.code) {
              this.add();
            }
          } else {
            this.textSearch = '';
          }
        } else if (value === 'customer') {
          this.columns[0].name = this.fields.customerCode;
          this.columns[1].name = this.fields.customerName;
          this.termData = [...this.dataTerm.customer.term];
          this.pageable = { ...this.dataTerm.customer.pageable };
        } else if (value === 'branch') {
          this.columns[0].name = this.fields.branchCode;
          this.columns[1].name = this.fields.branchName;
          this.termData = [...this.dataTerm.branch.term];
          this.pageable = { ...this.dataTerm.branch.pageable };
        }
        if (!_.find(this.commonData.listDivision, (item) => item.code === this.form.get('division').value)) {
          this.form.get('division').setValue(_.head(this.commonData.listDivision)?.code);
        }
        this.tableType = value;
        this.mapData();
      }
    });
  }

  add() {
    this.textSearch = _.trim(this.textSearch);
    if (this.textSearch.length === 0) {
      this.isSearch = false;
      return;
    }

    const reportBy = this.form.get('reportBy').value;

    if (_.findIndex(this.termData, (i) => i.code?.toUpperCase() === this.textSearch?.toUpperCase()) === -1) {
      this.isLoading = true;
      if (reportBy === 'rm') {
        const params = {
          manager: true,
          rm: true,
          crmIsActive: true,
          scope: Scopes.VIEW,
          rmBlock: this.form.get('division').value,
          rsId: this.objFunction?.rsId,
          employeeCode: this.textSearch,
          page: 0,
          size: maxInt32,
        };
        this.rmApi
          .post('findAll', params)
          .pipe(
            finalize(() => {
              this.isLoading = false;
            })
          )
          .subscribe((data) => {
            const itemResult = _.find(
              _.get(data, 'content'),
              (item) => item?.t24Employee?.employeeCode?.toUpperCase() === this.textSearch?.toUpperCase()
            );
            if (itemResult) {
              this.termData?.push({
                code: itemResult?.t24Employee?.employeeCode,
                name: itemResult?.hrisEmployee?.fullName,
                hrsCode: itemResult?.hrisEmployee?.employeeId,
              });
              this.mapData();
            } else {
              this.messageService.warn(_.get(this.notificationMessage, 'rmNotExistOrNotBranh'));
            }
          }, (e) => {
            this.messageService.error(this.notificationMessage.E001);
          });
      } else if (reportBy === 'customer') {
        const params = {
          customerCode: this.textSearch,
          customerType: this.form.get('division').value,
          rsId: this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`)?.rsId,
          scope: Scopes.VIEW,
          pageNumber: 0,
          pageSize: global?.userConfig?.pageSize,
        };

        let api: Observable<any>;

        if (this.form.get('division').value === Division.SME) {
          api = this.customerApi.searchSme(params);
        } else {
          api = this.customerApi.search(params);
        }

        api.pipe(
          finalize(() => {
            this.isLoading = false;
          })
        ).subscribe((data) => {
          if (data?.length > 0) {
            this.termData?.push({
              code: data[0]?.customerCode,
              name: data[0]?.customerName,
              customerCode: data[0]?.customerCode,
            });
            this.mapData();
          } else {
            this.messageService.warn(_.get(this.notificationMessage, 'customerNotExist'));
          }
        }, () => {
          this.messageService.error(this.notificationMessage.E001);
        });
      } else if (reportBy === 'branch') {
        const itemResult = _.find(
          this.commonData.listBranch,
          (item) => item.code?.toUpperCase() === this.textSearch?.toUpperCase()
        );
        if (itemResult) {
          this.termData?.push(itemResult);
          this.mapData();
        } else {
          this.messageService.warn(_.get(this.notificationMessage, 'branchNotExist'));
        }
        this.isLoading = false;
      }
    } else {
      const messageReport = reportBy.toString() + 'IsExist';
      this.messageService.warn(_.get(this.notificationMessage, messageReport));
      return;
    }
  }

  search() {
    const reportType = this.form.get('reportBy').value;
    if (reportType === 'rm') {
      const formSearch = {
        crmIsActive: { value: 'true', disabled: true },
        rmBlock: {
          value: this.form.get('division').value,
          disabled: true,
        },
        rm: true,
        manager: true,
      };

      const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
      modal.componentInstance.dataSearch = formSearch;
      modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
      modal.componentInstance.isManager = true;
      modal.result
        .then((res) => {
          if (res) {
            const listRMSelect = res.listSelected?.map((item) => {
              return {
                hrsCode: item?.hrisEmployee?.employeeId,
                code: item?.t24Employee?.employeeCode,
                name: item?.hrisEmployee?.fullName,
              };
            });
            this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
            this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);
            this.mapData();
          }
        })
        .catch(() => { });
    } else if (reportType === 'customer') {
      const formSearch = {
        customerType: {
          value: this.form.get('division').value,
          disabled: true,
        },
      };

      const modal = this.modalService.open(CustomerModalComponent, { windowClass: 'list__customer360-modal' });
      modal.componentInstance.listSelectedOld = this.termData;
      modal.componentInstance.dataSearch = formSearch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData =
              res.listSelected?.map((item) => {
                return {
                  code: item.customerCode,
                  customerCode: item.customerCode,
                  name: item.customerName ? item.customerName : item.name,
                };
              }) || [];
            this.mapData();
          }
        })
        .catch(() => { });
    } else if (reportType === 'branch') {
      const modal = this.modalService.open(ChooseBranchesModalComponent, { windowClass: 'tree__branches-modal' });
      modal.componentInstance.listBranchOld = _.map(this.termData, (x) => x.code) || [];
      modal.componentInstance.listBranch = this.commonData.listBranch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData = res;
            this.mapData();
          }
        })
        .catch(() => { });
    }
  }

  setPage(pageInfo) {
    this.paramsTable.page = pageInfo.offset;
    this.mapData();
  }

  mapData() {
    const total = this.termData.length;
    this.pageable.totalElements = total;
    this.pageable.totalPages = Math.floor(total / this.pageable.size);
    this.pageable.currentPage = this.paramsTable.page;
    const start = this.paramsTable.page * this.paramsTable.size;
    this.listDataTable = this.termData?.slice(start, start + this.paramsTable.size);
  }

  removeRecord(item: any): void {
    _.remove(this.termData, (i) => i.code === item.code);
    _.remove(this.listDataTable, (i) => i.code === item.code);
    if (this.listDataTable.length === 0 && this.pageable.currentPage > 0) {
      this.paramsTable.page -= 1;
    }
    this.listDataTable = [...this.listDataTable];
    this.mapData();
  }

  viewReport() {
    let { momentReport, reportBy, currencyUnit, division } = this.form.getRawValue();

    if (this.form.valid) {
      if (formatDate(momentReport, 'yyyyMM', 'en') >= formatDate(moment().valueOf(), 'yyyyMM', 'en')) {
        this.messageService.warn(this.notificationMessage.momentReportNotLessDateNow);
        return;
      }
      momentReport = formatDate(momentReport, 'yyyyMM', 'en');
    } else {
      validateAllFormFields(this.form);
      return;
    }

    const isTermDataEmpty = _.isEmpty(this.termData);
    if (reportBy === 'branch') {
      if (this.termData.length > this.countBranchMax) {
        this.translate.get('notificationMessage.countBranchMax', { number: this.countBranchMax }).subscribe((res) => {
          this.messageService.warn(res);
        });
        return;
      }
      if (this.commonData.isRm) {
        this.messageService.warn(_.get(this.notificationMessage, 'branchValidation'));
        return;
      }
    } else if (reportBy === 'rm') {
      if (isTermDataEmpty) {
        this.messageService.warn(_.get(this.notificationMessage, 'rmValidation'));
        return;
      }
      if (this.termData.length > this.countRm) {
        this.translate.get('notificationMessage.countRmMax', { number: this.countRm }).subscribe((res) => {
          this.messageService.warn(res);
        });
        return;
      }
    } else if (reportBy === 'customer') {
      if (isTermDataEmpty) {
        this.messageService.warn(_.get(this.notificationMessage, 'customerValidation'));
        return;
      }
      if (this.termData.length > this.countCustomerMax) {
        this.translate.get('notificationMessage.countCustomerLimit', { number: this.countCustomerMax }).subscribe((res) => {
          this.messageService.warn(res);
        });
        return;
      }
    }
    
    let pcode = this.termData.map((row) => row.code).join(',');

    const params = {
      attributeFormat: 'pdf',
      division,
      currencyUnit,
      pcase: this.reportByToPCase[reportBy],
      pdatadate: moment(momentReport).format('yyyyMM'),
      pcode,
    };
    if (this.form.get('downloadReport').value === 'EXCEL') {
      params.attributeFormat = '';
    }
    this.isLoading = true;
    forkJoin([this.kpiReportApi.reportMapCustomer(params)]).subscribe(
      ([pdfId]) => {
        const respondSource = new Subject<any>();
        this.kpiReportApi.checkIdReportSuccess(pdfId, respondSource);
        respondSource.asObservable().subscribe((res) => {
          if (res?.error === 'error') {
            this.messageService.error(this.notificationMessage.E001);
          } else if (res?.fileId) {
            this.loadFile(res?.fileId, params);
          } else {
            this.messageService.error(this.notificationMessage.messageOrsReport);
          }
          this.isLoading = false;
          respondSource.unsubscribe();
        });
      },
      () => {
        this.messageService.error(this.notificationMessage.E001);
        this.isLoading = false;
      }
    );
  }

  loadFile(pdfId: string, paramSearch: any) {
    if (this.form.get('downloadReport').value === 'EXCEL') {
      forkJoin([this.kpiReportApi.loadFile(`download/${pdfId}`)]).subscribe(
        ([blobExcel]) => {
          saveAs(blobExcel, `${this.fileName}.xls`);
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      this.kpiReportApi.loadFile(`download/${pdfId}`).then(
        (blobPDF) => {
          const url = `${environment.url_endpoint_report}/dashboard/bdkh`;
          if (this.form.get('downloadReport').value === 'HTML') {
            const modal = this.modalService.open(KpiReportModalComponent, { windowClass: 'tree__report-modal' });
            modal.componentInstance.data = blobPDF;
            modal.componentInstance.model = paramSearch;
            modal.componentInstance.baseUrl = url;
            modal.componentInstance.filename = this.fileName;
            modal.componentInstance.title = 'Báo cáo bản đồ khách hàng SME';
            modal.componentInstance.extendUrl = '';
            modal.componentInstance.excelType = 'xls';
          } else if (this.form.get('downloadReport').value === 'PDF') {
            saveAs(blobPDF, `${this.fileName}.pdf`);
          }
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    }
  }

  removeList() {
    this.termData = [];
    this.paramsTable.page = 0;
    this.mapData();
  }

  getData() {
    this.isLoading = true;
    const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];

    this.commonData.listDivision =
      divisionOfUser?.filter(division => division?.code !== 'INDIV').map((item) => {
        return { code: item.code, name: `${item.code || ''} - ${item.name || ''}` };
      }) || [];
    this.form.get('division').setValue(_.head(this.commonData.listDivision)?.code);

    this.textSearch = this.currUser.code;

    forkJoin([
      this.commonService.getCommonCategory(CommonCategory.REPORTS_YEARS).pipe(catchError(() => of(undefined))),
      this.categoryService.getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.CURRENCY_UNIT_REPORTS).pipe(catchError(() => of(undefined))),
      this.categoryService
        .getCommonCategory(CommonCategory.REPORTS_MAX_LENGTH_RM)
        .pipe(catchError(() => of(undefined))),
    ]).pipe(
      finalize(() => {
        if (this.currUser.code) {
          this.add();
        } else {
          this.isLoading = false;
        }
      })
    ).subscribe(([configYear, branchesOfUser, currencyUnits, configReport]) => {
      this.commonData.listBranch = branchesOfUser || [];

      this.countRm =
        _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.MAX_RM_REPORT_CREDIT)?.value || 0;
      this.countCustomerMax =
        _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.MAX_CUSTOMER_REPORT)?.value || 0;
      this.countBranchMax = _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.MAX_BRANCH)?.value || 0;

      this.commonData.isRm = _.isEmpty(branchesOfUser);
      this.commonData.listCurrencyUnit = _.orderBy(_.get(currencyUnits, 'content'), ['orderNum'], ['asc', 'desc']);
      this.commonData.listCurrencyUnit =
        _.get(currencyUnits, 'content')?.map((item) => {
          return { code: item.value, name: item.name, isDefault: item.isDefault };
        }) || [];

      this.form.get('currencyUnit').setValue(_.first(this.commonData.listCurrencyUnit)?.code);
      this.commonData.listCurrencyUnit.forEach((item) => {
        if (item.isDefault) {
          this.form.get('currencyUnit').setValue(item?.code);
        }
      });

      const yearConfig = _.find(_.get(configYear, 'content'), (item) => item.code === 'HDV')?.value || 1;
      this.commonData.minDate = new Date(
        moment()
          .add(-(yearConfig - 1), 'year')
          .startOf('year')
          .valueOf()
      );

      this.showBranch = !this.commonData.isRm;
    });
  }

}
