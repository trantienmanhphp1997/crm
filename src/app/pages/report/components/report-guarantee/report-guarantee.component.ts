import { formatDate } from '@angular/common';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { Component, OnInit, Injector, AfterViewInit, ViewEncapsulation } from '@angular/core';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { TableColumn } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import {
  BRANCH_HO,
  CommonCategory,
  Division,
  FunctionCode,
  maxInt32,
  Scopes,
  SessionKey,
} from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as moment from 'moment';
import { catchError } from 'rxjs/operators';
import { CustomerApi } from 'src/app/pages/customer-360/apis';
import { KpiReportApi } from '../../apis';
import { KpiReportModalComponent } from '../../dialogs';
import { RmApi } from 'src/app/pages/rm/apis';
import { RmModalComponent } from 'src/app/pages/rm/components/rm-modal/rm-modal.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { saveAs } from 'file-saver';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { environment } from 'src/environments/environment';
import { CustomerModalComponent } from 'src/app/pages/customer-360/components';
import { ChooseBranchesModalComponent } from 'src/app/pages/system/components/choose-branches-modal/choose-branches-modal.component';

@Component({
  selector: 'app-report-guarantee',
  templateUrl: './report-guarantee.component.html',
  styleUrls: ['./report-guarantee.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ReportGuaranteeComponent extends BaseComponent implements OnInit, AfterViewInit {
  yearNow = new Date().getFullYear();
  commonData = {
    listDivision: [],
    maxDate: new Date(),
    listBranch: [],
    minMonth: new Date(moment().add(-14, 'months').toDate()),
    minDate: new Date(moment().add(-61, 'day').toDate()),
    listCurrencyType: [],
    listCurrencyUnit: [],
    //
    minDateReportPeriod: new Date(
      moment()
        .set({ year: this.yearNow - 1, month: 10 })
        .toDate()
    ),
    maxDateReportPeriod: new Date(moment().set({ year: this.yearNow, month: 11 }).toDate()),
    minComparePeriod: new Date(moment().add(-14, 'months').toDate()),

    listQuarterly: [
      {
        label: 'Quý 1',
        value: 1,
      },
      {
        label: 'Quý 2',
        value: 2,
      },
      {
        label: 'Quý 3',
        value: 3,
      },
      {
        label: 'Quý 4',
        value: 4,
      },
    ],
    listYears: [
      { value: this.yearNow, label: this.yearNow },
      { value: this.yearNow - 1, label: this.yearNow - 1 },
    ],
    listYearsQuarterly: [{ value: this.yearNow, label: this.yearNow }],
    listQuarterlyPrevious: [],
    isRm: true,
  };
  textSearch: string;
  paramsTable = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };
  form = this.fb.group({
    division: '',
    reportBy: 'RM',
    downloadReport: 'HTML',
    businessDate: [new Date(), CustomValidators.required],
    period: 'MONTH',
    startDate: [new Date(), CustomValidators.required],
    endDate: [new Date(), CustomValidators.required],
    month: [new Date()],

    currencyType: '',
    currencyUnit: '',
    reportType: 'moment',
    momentReport: [new Date(moment().add(-1, 'day').toDate()), CustomValidators.required],
    momentCompare: [],
    reportTypeDetail: 'synthesis',

    //Thêm
    monthReport: [new Date(moment().add(-1, 'months').toDate())],
    monthCompare: [new Date(moment().add(-1, 'months').toDate())],
    reportPeriodQuarterly: [''],
    reportPeriodYear: [''],
    comparePeriodQuarterly: [1],
    comparePeriodYear: [''],
  });
  dataTerm = {
    customer: {
      pageable: { ...this.pageable },
      term: [],
    },
    rm: {
      pageable: { ...this.pageable },
      term: [],
    },
  };
  tableType: string;
  termData = [];
  listDataTable = [];
  columns: TableColumn[];
  isSearch = false;
  fileName = 'Bao-cao-bao-lanh';
  countRm = 50;
  objFunctionCustomer: any;
  isIndiv = false;
  branch = [];
  allRM = false;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private customerApi: CustomerApi,
    private rmApi: RmApi,
    private kpiReportApi: KpiReportApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.objFunctionCustomer = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
  }

  ngOnInit(): void {
    this.isLoading = true;
    const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
    if (divisionOfUser.length === 1 && divisionOfUser[0].code === Division.INDIV) {
      this.isIndiv = true;
      return;
    }
    this.tableType = this.form.get('reportBy').value;
    this.columns = [
      {
        name: this.fields.rmCode,
        prop: 'code',
      },
      {
        name: this.fields.rmName,
        prop: 'name',
      },
    ];
    forkJoin([
      this.categoryService.getBranchesOfUser(this.objFunction.rsId, Scopes.VIEW).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.KHOI_PRIORITY).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.CURRENCY_UNIT_REPORTS).pipe(catchError(() => of(undefined))),
      this.categoryService.getCommonCategory(CommonCategory.CURRENCY_TYPE).pipe(catchError(() => of(undefined))),
    ]).subscribe(([branchesOfUser, divisionPriority, currencyUnits, listCurrencyType]) => {
      this.commonData.isRm = _.isEmpty(branchesOfUser);
      if (_.isEmpty(branchesOfUser)) {
        this.branch = [this.currUser.branch];
      } else if (this.currUser.branch === BRANCH_HO) {
        this.branch = [];
      } else {
        branchesOfUser.forEach((item) => {
          this.branch.push(item.code);
        });
      }
      // Loại tiền tệ
      this.commonData.listCurrencyUnit = _.orderBy(_.get(currencyUnits, 'content'), ['orderNum'], ['asc', 'desc']);
      this.commonData.listCurrencyUnit =
        _.get(currencyUnits, 'content')?.map((item) => {
          return { code: item.value, name: item.name, isDefault: item.isDefault };
        }) || [];
      this.commonData.listCurrencyUnit.forEach((item) => {
        if (item.isDefault) {
          this.form.get('currencyUnit').setValue(item?.code);
        }
      });

      //Đơn vị tiền tệ
      this.commonData.listCurrencyType = _.get(listCurrencyType, 'content')
        ?.filter((item) => {
          return item.code === 'ALL';
        })
        ?.map((itemValue) => {
          return {
            code: itemValue.value,
            label: `${itemValue.name || ''}`,
          };
        });
      this.form.get('currencyType').setValue(_.head(this.commonData.listCurrencyType)?.code);
      divisionPriority = divisionPriority?.content || [];
      divisionPriority = _.orderBy(divisionPriority, ['value']);
      let divisionConfig = divisionPriority?.map((item) => item.code);
      this.commonData.listDivision =
        divisionOfUser?.map((item) => {
          return {
            code: item.code,
            name: item.code + ' - ' + item.name,
            order:
              _.findIndex(divisionConfig, (value) => value === item.code) === -1
                ? 99
                : _.findIndex(divisionConfig, (value) => value === item.code),
          };
        }) || [];
      _.remove(this.commonData.listDivision, (i) => i.code === Division.INDIV);

      this.form.controls.division.setValue(_.first(this.commonData.listDivision)?.code);
      this.isLoading = false;
      this.onChangeYear({ value: new Date().getFullYear() });
    });
  }

  ngAfterViewInit() {
    this.form.controls.reportBy.valueChanges.subscribe((value) => {
      if (this.tableType !== value) {
        this.allRM = false;
        this.paramsTable.page = 0;
        this.dataTerm[this.tableType] = {
          pageable: { ...this.pageable },
          term: [...this.termData],
        };
        this.textSearch = '';
        if (value === 'KH') {
          this.columns[0].name = this.fields.customerCode;
          this.columns[1].name = this.fields.customerName;
          this.termData = [...this.dataTerm.customer.term];
          this.pageable = { ...this.dataTerm.customer.pageable };
        } else if (value === 'RM') {
          this.columns[0].name = this.fields.rmCode;
          this.columns[1].name = this.fields.rmName;
          this.termData = [...this.dataTerm.rm.term];
          this.pageable = { ...this.dataTerm.rm.pageable };
        }
        this.tableType = value;
        this.mapData();
      }
    });

    this.form.controls.reportBy.valueChanges.subscribe(() => {
      this.form.controls.reportTypeDetail.setValue('synthesis');
    });

    this.form.controls.division.valueChanges.subscribe(() => {
      this.removeList();
    });
    this.form.controls.period.valueChanges.subscribe((value) => {
      this.form.get('comparePeriodYear').setValue(this.yearNow);
      this.form.get('comparePeriodQuarterly').setValue(1);
      this.form.get('reportPeriodYear').setValue(this.yearNow);
      this.form.get('reportPeriodQuarterly').setValue(1);
    });
  }

  search() {
    const reportBy = this.form.get('reportBy').value;
    if (reportBy === 'KH') {
      const formSearch = {
        customerType: {
          value: this.form.get('division').value,
          disabled: true,
        },
      };
      const modal = this.modalService.open(CustomerModalComponent, { windowClass: 'list__customer360-modal' });
      modal.componentInstance.listSelectedOld = this.termData;
      modal.componentInstance.dataSearch = formSearch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData =
              res.listSelected?.map((item) => {
                return {
                  code: item.customerCode,
                  customerCode: item.customerCode,
                  name: item.customerName ? item.customerName : item.name,
                };
              }) || [];
            this.mapData();
          }
        })
        .catch(() => {});
    } else if (reportBy === 'RM') {
      const formSearch = {
        crmIsActive: { value: 'true', disabled: true },
        rmBlock: {
          value: this.form.get('division').value,
          disabled: true,
        },
        manager: true,
        rm: true,
        isReportByRm: false,
      };
      const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
      modal.componentInstance.dataSearch = formSearch;
      modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
      modal.result
        .then((res) => {
          if (res) {
            const listRMSelect = res.listSelected?.map((item) => {
              return {
                hrsCode: item?.hrisEmployee?.employeeId,
                code: item?.t24Employee?.employeeCode,
                name: item?.hrisEmployee?.fullName,
              };
            });
            this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
            this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);
            this.mapData();
          }
        })
        .catch(() => {});
    }
  }

  add() {
    const reportBy = this.form.get('reportBy').value;
    this.textSearch = _.trim(this.textSearch);
    if (this.textSearch.length === 0) {
      this.isSearch = false;
      return;
    }
    if (_.findIndex(this.termData, (i) => i.code?.toUpperCase() === this.textSearch?.toUpperCase()) === -1) {
      if (reportBy === 'KH') {
        const params = {
          customerCode: this.textSearch,
          customerType: this.form.get('division').value,
          rsId: this.objFunctionCustomer?.rsId,
          scope: Scopes.VIEW,
          pageNumber: 0,
          pageSize: global?.userConfig?.pageSize,
        };
        this.isSearch = true;
        let api: Observable<any>;
        if (this.form.get('division').value !== Division.INDIV) {
          api = this.customerApi.searchSme(params);
        } else {
          api = this.customerApi.search(params);
        }
        api.subscribe(
          (data) => {
            if (data?.length > 0) {
              this.termData?.push({
                code: data[0]?.customerCode,
                name: data[0]?.customerName,
                customerCode: data[0]?.customerCode,
              });
              this.messageService.success(_.get(this.notificationMessage, 'customerSuccess'));
              this.mapData();
            } else {
              this.messageService.warn(_.get(this.notificationMessage, 'customerNotExist'));
            }
            this.isSearch = false;
          },
          (e) => {
            this.messageService.error(this.notificationMessage.E001);
            this.isLoading = false;
            this.isSearch = false;
          }
        );
      } else if (reportBy === 'RM') {
        const params = {
          manager: true,
          rm: true,
          crmIsActive: true,
          scope: Scopes.VIEW,
          rmBlock: this.form.get('division').value,
          rsId: this.objFunction?.rsId,
          employeeCode: this.textSearch,
          page: 0,
          size: maxInt32,
          isReportByRm: false,
        };
        this.isSearch = true;
        this.rmApi.post('findAll', params).subscribe(
          (data) => {
            const itemResult = _.find(
              _.get(data, 'content'),
              (item) => item?.t24Employee?.employeeCode?.toUpperCase() === this.textSearch?.toUpperCase()
            );
            if (itemResult) {
              this.termData?.push({
                code: itemResult?.t24Employee?.employeeCode,
                name: itemResult?.hrisEmployee?.fullName,
                hrsCode: itemResult?.hrisEmployee?.employeeId,
              });
              this.mapData();
            } else if (!itemResult && !this.allRM) {
              this.messageService.warn(_.get(this.notificationMessage, 'rmNotExistOrNotBranh'));
            }
            this.isSearch = false;
          },
          (e) => {
            this.messageService.error(this.notificationMessage.E001);
            this.isLoading = false;
            this.isSearch = false;
          }
        );
      }
    } else {
      const messageReport = this.form.get('reportBy').value.toString() + 'IsExist';
      this.messageService.warn(_.get(this.notificationMessage, messageReport));
      this.isSearch = false;
      return;
    }
  }

  setPage(pageInfo) {
    if (!this.isLoading) {
      this.paramsTable.page = pageInfo.offset;
      this.mapData();
    }
  }

  mapData() {
    const total = this.termData.length;
    this.pageable.totalElements = total;
    this.pageable.totalPages = Math.floor(total / this.pageable.size);
    this.pageable.currentPage = this.paramsTable.page;
    const start = this.paramsTable.page * this.paramsTable.size;
    this.listDataTable = this.termData?.slice(start, start + this.paramsTable.size);
  }

  removeRecord(item) {
    _.remove(this.termData, (i) => i.code === item.code);
    _.remove(this.listDataTable, (i) => i.code === item.code);
    if (this.listDataTable.length === 0 && this.pageable.currentPage > 0) {
      this.paramsTable.page -= 1;
    }
    this.listDataTable = [...this.listDataTable];
    this.mapData();
  }

  viewReport() {
    const reportBy = this.form.get('reportBy').value;
    let momentReport = this.form.controls.momentReport.value;
    let momentCompare = this.form.controls.momentCompare.value;
    if (moment(momentReport).startOf('day').isAfter(moment(momentCompare).startOf('day')) && !_.isNull(momentCompare)) {
      this.messageService.warn(_.get(this.notificationMessage, 'momentReportThanMomentCompare'));
      return;
    } else if (
      formatDate(momentReport, 'dd/MM/yyyy', 'en') === formatDate(momentCompare, 'dd/MM/yyyy', 'en') &&
      !_.isNull(momentCompare)
    ) {
      this.messageService.warn(this.notificationMessage.momentCompareNoThanMomentReport);
      return;
    }

    if (this.isLoading || this.isSearch) {
      return;
    }
    if (this.form.valid) {
      if (_.isEmpty(this.termData) && reportBy === 'RM') {
        this.messageService.warn(_.get(this.notificationMessage, 'rmValidation'));
        return;
      } else if (_.isEmpty(this.termData) && reportBy === 'KH') {
        this.messageService.warn(_.get(this.notificationMessage, 'customerValidation'));
        return;
      }

      const dataTable = this.termData?.map((i) => i.code);
      let params: any;
      params = {
        attributeFormat: 'pdf',
        division: this.form.controls.division.value,
        reportBy: reportBy,
        data: dataTable,
        rsId: reportBy === 'RM' ? this.objFunction.rsId : this.objFunctionCustomer.rsId,
        scope: Scopes.VIEW,
        momentReport: formatDate(momentReport, 'dd/MM/yyyy', 'en'),
        momentCompare: !_.isNull(momentCompare) ? formatDate(momentCompare, 'dd/MM/yyyy', 'en') : '',
        currencyType: this.form.controls.currencyType.value,
        currencyUnit: this.form.controls.currencyUnit.value,
        reportTypeDetail: this.form.controls.reportTypeDetail.value,
        reportType: this.form.controls.reportType.value,
        branch: this.branch,
      };

      if (this.form.get('downloadReport').value === 'EXCEL') {
        params.attributeFormat = 'xlsx';
      }
      this.isLoading = true;
      let apiReport: Observable<any>;
      if (this.form.get('reportType').value === 'avagate') {
        //Thêm biến
        delete params.reportBy;
        delete params.momentReport;
        delete params.momentCompare;
        params.currencyUnitDisplay = this.commonData.listCurrencyUnit.filter(
          (item) => item.code === this.form.controls.currencyUnit.value
        )[0]?.name;
        params.reportAccording = reportBy;
        params.period = this.form.get('period').value;
        if (this.form.get('period').value === 'MONTH') {
          params.monthReport = formatDate(this.form.controls.monthReport.value, 'MM/yyyy', 'en');
          params.monthCompare = formatDate(this.form.controls.monthCompare.value, 'MM/yyyy', 'en');
        } else if (this.form.get('period').value === 'QUARTER') {
          params.quarter = this.form.controls.reportPeriodQuarterly.value;
          params.yearOfQuarter = this.form.controls.reportPeriodYear.value;
          params.quarterCompare = this.form.controls.comparePeriodQuarterly.value;
          params.yearOfQuarterCompare = this.form.controls.comparePeriodYear.value;
        } else {
          params.yearReport = this.form.controls.reportPeriodYear.value;
          params.yearCompare = this.form.controls.comparePeriodYear.value;
        }

        apiReport = this.kpiReportApi.reportGuaranteeAvagate(params);
      } else {
        apiReport = this.kpiReportApi.reportGuaranteeMoment(params);
      }

      forkJoin([apiReport]).subscribe(
        ([pdfId]) => {
          const respondSource = new Subject<any>();
          this.kpiReportApi.checkIdReportSuccess(pdfId, respondSource);
          respondSource.asObservable().subscribe((res) => {
            if (res?.error === 'error') {
              this.messageService.error(this.notificationMessage.E001);
            } else if (res?.fileId) {
              this.loadFile(res?.fileId, params);
            } else {
              this.messageService.error(this.notificationMessage.messageOrsReport);
            }
            this.isLoading = false;
            respondSource.unsubscribe();
          });
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      validateAllFormFields(this.form);
      return;
    }
  }

  loadFile(pdfId: string, paramSearch: any) {
    if (this.form.get('downloadReport').value === 'EXCEL') {
      forkJoin([this.kpiReportApi.loadFile(`download/${pdfId}`)]).subscribe(
        ([blobExcel]) => {
          saveAs(blobExcel, `${this.fileName}.xlsx`);
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      this.kpiReportApi.loadFile(`download/${pdfId}`).then(
        (blobPDF) => {
          let url = '';
          if (this.form.get('reportType').value === 'avagate') {
            url = `${environment.url_endpoint_kpi}/ors-report/guarantee`;
          } else {
            url = `${environment.url_endpoint_report}/dashboard/baolanh`;
          }

          if (this.form.get('downloadReport').value === 'HTML') {
            const modal = this.modalService.open(KpiReportModalComponent, { windowClass: 'tree__report-modal' });
            modal.componentInstance.data = blobPDF;
            modal.componentInstance.model = paramSearch;
            modal.componentInstance.baseUrl = url;
            modal.componentInstance.filename = this.fileName;
            modal.componentInstance.title = 'Báo cáo bảo lãnh';
            modal.componentInstance.extendUrl = '';
          } else if (this.form.get('downloadReport').value === 'PDF') {
            saveAs(blobPDF, `${this.fileName}.pdf`);
          }
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    }
  }

  removeList() {
    this.termData = [];
    this.paramsTable.page = 0;
    this.allRM = false;
    this.mapData();
  }

  checkQuarterly(value) {
    if (0 < value && value <= 3) {
      return 1;
    } else if (value > 3 && value <= 6) {
      return 2;
    } else if (value > 6 && value <= 9) {
      return 3;
    } else if (value > 9 && value <= 12) {
      return 4;
    }
  }

  onChangeYear(year) {
    this.commonData.listQuarterlyPrevious = [
      {
        label: 'Quý 1',
        value: 1,
      },
      {
        label: 'Quý 2',
        value: 2,
      },
      {
        label: 'Quý 3',
        value: 3,
      },
      {
        label: 'Quý 4',
        value: 4,
      },
    ];
    const monthNow = new Date().getMonth() + 1;
    const index = this.checkQuarterly(monthNow);
    if (year.value === new Date().getFullYear()) {
      _.remove(this.commonData.listQuarterlyPrevious, (item) => item.value > index);
      this.form.controls.comparePeriodQuarterly.setValue(_.first(this.commonData.listQuarterlyPrevious).value);
    } else if (year.value === new Date().getFullYear() - 1) {
      if (index > 1) {
        _.remove(this.commonData.listQuarterlyPrevious, (item) => item.value < index);
        this.form.controls.comparePeriodQuarterly.setValue(_.first(this.commonData.listQuarterlyPrevious).value);
      }
    } else {
      this.commonData.listQuarterlyPrevious = [];
      this.form.controls.comparePeriodQuarterly.setValue(null);
    }
  }
  isShowCheckboxAllRm() {
    return (
      this.currUser?.branch !== BRANCH_HO &&
      !this.commonData.isRm &&
      this.form.get('division').value !== Division.INDIV &&
      this.form.get('reportBy').value === 'RM'
    );
  }

  onChangeCheckAllRM(event) {
    if (event.checked) {
      this.textSearch = '';
      const params = {
        crmIsActive: true,
        scope: Scopes.VIEW,
        rmBlock: this.form.get('division').value,
        rsId: this.objFunction?.rsId,
        employeeCode: this.textSearch,
        page: 0,
        size: maxInt32,
        isReportByRm: false,
        rm: true,
        manager: true,
      };
      this.isLoading = true;
      this.rmApi.post('findAll', params).subscribe(
        (data) => {
          this.termData = _.map(_.get(data, 'content'), (item) => {
            return {
              code: item?.t24Employee?.employeeCode,
              name: item?.hrisEmployee?.fullName,
              hrsCode: item?.hrisEmployee?.employeeId,
            };
          });
          this.termData = _.uniqBy(this.termData, (i) => i.hrsCode);
          this.paramsTable.page = 0;
          this.mapData();
          this.isLoading = false;
        },
        (e) => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      this.termData = [];
      this.paramsTable.page = 0;
      this.mapData();
    }
  }
}
