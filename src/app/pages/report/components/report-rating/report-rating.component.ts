import { AfterViewInit, Component, Injector, OnInit } from '@angular/core';
import { FileService } from '../../../../core/services/file.service';
import { BaseComponent } from '../../../../core/components/base.component';
import { CategoryApi } from '../../apis';
import * as _ from 'lodash';
import {
  BRANCH_HO,
  CommonCategory,
  Division,
  FunctionCode, maxInt32,
  Scopes,
  SessionKey
} from '../../../../core/utils/common-constants';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import * as moment from 'moment/moment';
import { CustomValidators } from '../../../../core/utils/custom-validations';
import { global } from '@angular/compiler/src/util';
import { TableColumn } from '@swimlane/ngx-datatable';
import { Pageable } from '../../../../core/interfaces/pageable.interface';
import { RmModalComponent } from '../../../rm/components';
import {
  ChooseBranchesModalComponent
} from '../../../system/components/choose-branches-modal/choose-branches-modal.component';
import { CustomerModalComponent } from '../../../customer-360/components';
import { DashboardService } from '../../../dashboard/services/dashboard.service';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { formatDate } from '@angular/common';


@Component({
  selector: 'app-report-rating',
  templateUrl: './report-rating.component.html',
  styleUrls: ['./report-rating.component.scss']
})
export class ReportRatingComponent extends BaseComponent implements OnInit, AfterViewInit {
  isLoading = false;
  form = this.fb.group({
    blockCode: '',
    branchCodes: '',
    momentReportMonth: [new Date(moment().add(0, 'month').toDate()), CustomValidators.required],
    listOption: 'ALL'
  });
  commonData = {
    minDate: null,
    maxDate: new Date(),
    isRm: true,
    listBranch: [],
    listCustomerType: [],
    listDivision: [],

  };
  highPrioritySort = [Division.INDIV, Division.SME, Division.CIB, Division.FI];
  textSearch: string;
  termData = [];
  columns: TableColumn[];

  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };
  dataTerm = {
      pageable: { ...this.pageable },
      term: [],
  };
  listDataTable = [];
  listDataRequest = [];
  paramsTable = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };
  constructor(
    injector: Injector,
    private CategoryServiceApi: CategoryApi,
    private fileService: FileService,
    private dashboardService: DashboardService,
    private categoryService: CategoryService,


  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RATING_FUNCTION_REPORT}`);

  }

  ngOnInit(): void {
    const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
    this.commonData.listDivision =
      divisionOfUser?.map((item) => {
        return { code: item.code, displayName: `${item.code || ''} - ${item.name || ''}` };
      }) || [];
    this.sortDivision();
    if (this.commonData.listDivision.length > 0) {
      this.form.controls.blockCode.setValue(this.commonData.listDivision[0].code);
    }

    forkJoin([
      this.commonService.getCommonCategory(CommonCategory.REPORTS_YEARS).pipe(catchError(() => of(undefined))),
      this.categoryService
        .getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined))),
    ]).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(([configYear, branchesOfUser]) => {

      const yearConfig = _.find(_.get(configYear, 'content'), (item) => item.code === 'REPORT_SPDV')?.value || 2;
      this.commonData.minDate = new Date(
        moment()
          .add(-(yearConfig), 'month')
          .startOf('month')
          .valueOf()
      );
      this.commonData.listBranch = branchesOfUser as any || [];
    });
  }

  async export(){
    if(!this.form.controls.momentReportMonth?.value){
      this.messageService.warn('Vui lòng chọn tháng báo cáo');
      return;
    }
    if(_.isEmpty(this.listDataRequest) && this.form.controls.listOption.value == 'choose_branch'){
      this.messageService.warn(_.get(this.notificationMessage, 'branchValidation'));
      return;
    }
    this.isLoading = true;
    const month = formatDate(this.form.controls.momentReportMonth?.value, 'MM/yyyy', 'en')
    const body =
      {
        blockCode: this.form.controls.blockCode.value,
        branchCodes: this.listDataRequest.map(item => item.code),
        month,
      };
    const nameFile = 'Báo cáo đánh giá hài lòng người dùng-' + month.toString() + '.xlsx';
    this.categoryService.reportRating(body).subscribe(async value => {
      if (value) {
        await this.download(value, nameFile);
      }
      this.isLoading = false;

    }, (e) => {
      this.isLoading = false;
      if (!_.isEmpty(e?.error?.description)) {
        this.messageService.error(e.error.description);
      } else {
        this.messageService.error(this.notificationMessage.error);
      }
    })
  }


  download(fileId: any, name) {
    this.isLoading = true;
    this.fileService.downloadFile(fileId, name).subscribe((res) => {
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      };
      this.isLoading = false;
    }, err => {
      this.isLoading = false;
    });
  }

  ngAfterViewInit(): void {
  }

  sortDivision(): void {
    const currentDisivionList: any[] = _.cloneDeep(this.commonData.listDivision);
    const result = [];
    this.highPrioritySort.forEach(code => {
      const index = currentDisivionList.findIndex(item => item.code === code);
      if (index > -1) {
        result.push(currentDisivionList[index]);
        currentDisivionList.splice(index, 1);
      }
    });

    result.push(...currentDisivionList);
    this.commonData.listDivision = result;
  }

  search() {
      const modal = this.modalService.open(ChooseBranchesModalComponent, { windowClass: 'tree__branches-modal' });
      modal.componentInstance.listBranchOld = _.map(this.termData, (x) => x.code) || [];
      modal.componentInstance.listBranch = this.commonData.listBranch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData = res;
            this.mapData();
          }
        })
        .catch(() => { });
  }



  add() {
    this.textSearch = _.trim(this.textSearch);
    if (this.textSearch.length === 0) {
      this.isLoading = false;
      return;
    }
    if (_.findIndex(this.termData, (i) => i.code?.toUpperCase() === this.textSearch?.toUpperCase()) === -1) {
      this.isLoading = true;
        const itemResult = _.find(
          this.commonData.listBranch,
          (item) => item.code?.toUpperCase() === this.textSearch?.toUpperCase()
        );
        if (itemResult) {
          this.termData?.push(itemResult);
          this.mapData();
        } else {
          this.messageService.warn(_.get(this.notificationMessage, 'branchNotExistOrNotInBranch'));
        }
        this.isLoading = false;
    } else {
      this.messageService.warn(_.get(this.notificationMessage, 'branchExistTable'));
      this.isLoading = false;
      return;
    }
  }
  mapData() {
    const total = this.termData.length;
    this.pageable.totalElements = total;
    this.pageable.totalPages = Math.floor(total / this.pageable.size);
    this.pageable.currentPage = this.paramsTable.page;
    const start = this.paramsTable.page * this.paramsTable.size;
    this.listDataTable = this.termData?.slice(start, start + this.paramsTable.size);
    this.listDataRequest = this.termData?.slice(0, start + this.pageable.totalElements);
  }
  removeList() {
    this.termData = [];
    this.paramsTable.page = 0;
    this.mapData();
  }
  setPage(pageInfo) {
    this.paramsTable.page = pageInfo.offset;
    this.mapData();
  }

  removeRecord(item) {
    _.remove(this.termData, (i) => i.code === item.code);
    _.remove(this.listDataTable, (i) => i.code === item.code);
    if (this.listDataTable.length === 0 && this.pageable.currentPage > 0) {
      this.paramsTable.page -= 1;
    }
    this.listDataTable = [...this.listDataTable];
    this.mapData();
  }
  get hidePickupList(): boolean {
    return (this.form.get('listOption').value === 'ALL');
  }
}
