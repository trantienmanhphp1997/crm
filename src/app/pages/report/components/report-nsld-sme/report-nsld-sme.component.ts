import { formatDate } from '@angular/common';
import { forkJoin, of, Subject } from 'rxjs';
import { Component, OnInit, Injector, AfterViewInit, ViewEncapsulation } from '@angular/core';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { TableColumn } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import {
  CommonCategory,
  Division,
  FunctionCode,
  maxInt32,
  Scopes,
  SessionKey,
} from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as moment from 'moment';
import { catchError } from 'rxjs/operators';
import { CustomerApi } from 'src/app/pages/customer-360/apis';
import { KpiReportApi } from '../../apis';
import { KpiReportModalComponent } from '../../dialogs';
import { RmApi } from 'src/app/pages/rm/apis';
import { RmModalComponent } from 'src/app/pages/rm/components/rm-modal/rm-modal.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { saveAs } from 'file-saver';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { environment } from 'src/environments/environment';
import { CustomerModalComponent } from 'src/app/pages/customer-360/components';

@Component({
  selector: 'app-report-nsld-sme',
  templateUrl: './report-nsld-sme.component.html',
  styleUrls: ['./report-nsld-sme.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ReportNsldSmeComponent extends BaseComponent implements OnInit, AfterViewInit {
  commonData = {
    listDivision: [],
    maxDate: new Date(),
  };
  textSearch: string;
  paramsTable = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };
  form = this.fb.group({
    division: '',
    reportBy: 'rm',
    downloadReport: 'HTML',
    businessDate: [new Date(), CustomValidators.required],
  });
  dataTerm = {
    rm: {
      pageable: { ...this.pageable },
      term: [],
    },
    manager: {
      pageable: { ...this.pageable },
      term: [],
    },
  };
  tableType: string;
  termData = [];
  listDataTable = [];
  columns: TableColumn[];
  isSearch = false;
  fileName = 'bao-cao-nsld-sme/cib';
  countRm = 50;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private customerApi: CustomerApi,
    private rmApi: RmApi,
    private kpiReportApi: KpiReportApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
  }

  ngOnInit(): void {
    this.tableType = this.form.get('reportBy').value;
    this.columns = [
      {
        name: this.fields.rmCode,
        prop: 'code',
      },
      {
        name: this.fields.rmName,
        prop: 'name',
      },
    ];
    const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
    this.commonData.listDivision = divisionOfUser
      ?.map((item) => {
        if (item.code === Division.SME || item.code === Division.CIB) {
          return { code: item.code, name: `${item.code || ''} - ${item.name || ''}` };
        }
      })
      ?.filter((i) => i);

    this.form.controls.division.setValue('SME');
  }

  ngAfterViewInit() {
    this.form.controls.reportBy.valueChanges.subscribe((value) => {
      if (this.tableType !== value) {
        this.paramsTable.page = 0;
        this.dataTerm[this.tableType] = {
          pageable: { ...this.pageable },
          term: [...this.termData],
        };
        this.textSearch = '';
        if (value === 'rm') {
          this.columns[0].name = this.fields.rmCode;
          this.columns[1].name = this.fields.rmName;
          this.termData = [...this.dataTerm.rm.term];
          this.pageable = { ...this.dataTerm.rm.pageable };
        } else if (value === 'manager') {
          this.columns[0].name = this.fields.rmCode;
          this.columns[1].name = this.fields.rmName;
          this.termData = [...this.dataTerm.manager.term];
          this.pageable = { ...this.dataTerm.manager.pageable };
        }
        this.tableType = value;
        this.mapData();
      }
    });

    this.form.controls.division.valueChanges.subscribe(() => {
      this.removeList();
    });
  }

  search() {
    const formSearch = {
      crmIsActive: { value: 'true', disabled: true },
      rmBlock: {
        value: this.form.get('division').value,
        disabled: true,
      },
      rm: this.form.get('reportBy').value === 'rm',
      isReportByRm: this.form.get('reportBy').value === 'rm',
    };
    const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
    modal.componentInstance.dataSearch = formSearch;
    modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
    modal.componentInstance.isManager = this.form.get('reportBy').value !== 'rm';
    modal.result
      .then((res) => {
        if (res) {
          const listRMSelect = res.listSelected?.map((item) => {
            return {
              hrsCode: item?.hrisEmployee?.employeeId,
              code: item?.t24Employee?.employeeCode,
              name: item?.hrisEmployee?.fullName,
            };
          });
          this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
          this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);
          this.mapData();
        }
      })
      .catch(() => {});
  }

  add() {
    this.textSearch = _.trim(this.textSearch);
    if (this.textSearch.length === 0) {
      this.isSearch = false;
      return;
    }
    if (_.findIndex(this.termData, (i) => i.code?.toUpperCase() === this.textSearch?.toUpperCase()) === -1) {
      const params = {
        manager: this.form.get('reportBy').value === 'manager',
        rm: this.form.get('reportBy').value !== 'manager',
        crmIsActive: true,
        scope: Scopes.VIEW,
        rmBlock: this.form.get('division').value,
        rsId: this.objFunction?.rsId,
        employeeCode: this.textSearch,
        page: 0,
        size: maxInt32,
        isReportByRm: this.form.get('reportBy').value !== 'manager',
      };
      this.isSearch = true;
      this.rmApi.post('findAll', params).subscribe(
        (data) => {
          const itemResult = _.find(
            _.get(data, 'content'),
            (item) => item?.t24Employee?.employeeCode?.toUpperCase() === this.textSearch?.toUpperCase()
          );
          if (itemResult) {
            this.termData?.push({
              code: itemResult?.t24Employee?.employeeCode,
              name: itemResult?.hrisEmployee?.fullName,
              hrsCode: itemResult?.hrisEmployee?.employeeId,
            });
            this.mapData();
          } else {
            this.messageService.warn(_.get(this.notificationMessage, 'rmNotExistOrNotBranh'));
          }
          this.isSearch = false;
        },
        (e) => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
          this.isSearch = false;
        }
      );
    } else {
      const messageReport = this.form.get('reportBy').value.toString() + 'IsExist';
      this.messageService.warn(_.get(this.notificationMessage, messageReport));
      this.isSearch = false;
      return;
    }
  }

  setPage(pageInfo) {
    this.paramsTable.page = pageInfo.offset;
    this.mapData();
  }

  mapData() {
    const total = this.termData.length;
    this.pageable.totalElements = total;
    this.pageable.totalPages = Math.floor(total / this.pageable.size);
    this.pageable.currentPage = this.paramsTable.page;
    const start = this.paramsTable.page * this.paramsTable.size;
    this.listDataTable = this.termData?.slice(start, start + this.paramsTable.size);
  }

  removeRecord(item) {
    _.remove(this.termData, (i) => i.code === item.code);
    _.remove(this.listDataTable, (i) => i.code === item.code);
    if (this.listDataTable.length === 0 && this.pageable.currentPage > 0) {
      this.paramsTable.page -= 1;
    }
    this.listDataTable = [...this.listDataTable];
    this.mapData();
  }

  viewReport() {
    if (this.isLoading || this.isSearch) {
      return;
    }
    if (this.form.valid) {
      if (
        _.isEmpty(this.termData) &&
        (this.form.get('reportBy').value === 'rm' || this.form.get('reportBy').value === 'manager')
      ) {
        this.messageService.warn(_.get(this.notificationMessage, 'rmValidation'));
        return;
      } else if (this.termData.length > this.countRm) {
        this.translate.get('notificationMessage.countRmMax', { number: this.countRm }).subscribe((res) => {
          this.messageService.warn(res);
        });
        return;
      }

      const businessDate = formatDate(this.form.controls.businessDate.value, 'MM-yyyy', 'en');
      const dataRm = this.termData?.map((i) => i.code);
      const params = {
        attributeFormat: 'pdf',
        rmCodes: dataRm,
        blockCode: this.form.controls.division.value,
        businessDate: businessDate,
        isManager: this.form.get('reportBy').value === 'manager' ? true : false,
      };

      if (this.form.get('downloadReport').value === 'EXCEL') {
        params.attributeFormat = 'xlsx';
      }
      this.isLoading = true;
      forkJoin([this.kpiReportApi.reportNsldSmeCib(params)]).subscribe(
        ([pdfId]) => {
          const respondSource = new Subject<any>();
          this.kpiReportApi.checkIdReportSuccess(pdfId, respondSource);
          respondSource.asObservable().subscribe((res) => {
            if (res?.error === 'error') {
              this.messageService.error(this.notificationMessage.E001);
            } else if (res?.fileId) {
              this.loadFile(res?.fileId, params);
            } else {
              this.messageService.error(this.notificationMessage.messageOrsReport);
            }
            this.isLoading = false;
            respondSource.unsubscribe();
          });
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      validateAllFormFields(this.form);
      return;
    }
  }

  loadFile(pdfId: string, paramSearch: any) {
    if (this.form.get('downloadReport').value === 'EXCEL') {
      forkJoin([this.kpiReportApi.loadFile(`download/${pdfId}`)]).subscribe(
        ([blobExcel]) => {
          saveAs(blobExcel, `${this.fileName}.xlsx`);
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      this.kpiReportApi.loadFile(`download/${pdfId}`).then(
        (blobPDF) => {
          const url = `${environment.url_endpoint_kpi}/ors-report/kpi-sme-cib`;
          if (this.form.get('downloadReport').value === 'HTML') {
            const modal = this.modalService.open(KpiReportModalComponent, { windowClass: 'tree__report-modal' });
            modal.componentInstance.data = blobPDF;
            modal.componentInstance.model = paramSearch;
            modal.componentInstance.baseUrl = url;
            modal.componentInstance.filename = this.fileName;
            modal.componentInstance.title = this.fileName;
            modal.componentInstance.extendUrl = '';
          } else if (this.form.get('downloadReport').value === 'PDF') {
            saveAs(blobPDF, `${this.fileName}.pdf`);
          }
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    }
  }

  removeList() {
    this.termData = [];
    this.paramsTable.page = 0;
    this.mapData();
  }
}
