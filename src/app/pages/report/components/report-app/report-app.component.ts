import {formatDate} from '@angular/common';
import {forkJoin, Observable, of, Subject} from 'rxjs';
import {Component, OnInit, Injector, AfterViewInit} from '@angular/core';
import _ from 'lodash';
import {BaseComponent} from 'src/app/core/components/base.component';
import {Pageable} from 'src/app/core/interfaces/pageable.interface';
import {TableColumn} from '@swimlane/ngx-datatable';
import {global} from '@angular/compiler/src/util';
import {
  BRANCH_HO,
  CommonCategory,
  Division,
  FunctionCode,
  maxInt32,
  Scopes,
  SessionKey,
} from 'src/app/core/utils/common-constants';
import {CategoryService} from 'src/app/pages/system/services/category.service';
import * as moment from 'moment';
import {catchError} from 'rxjs/operators';
import {CustomerApi} from 'src/app/pages/customer-360/apis';
import {KpiReportApi} from '../../apis';
import {KpiReportModalComponent} from '../../dialogs';
import {RmApi} from 'src/app/pages/rm/apis';
import {RmModalComponent} from 'src/app/pages/rm/components/rm-modal/rm-modal.component';
import {CustomValidators} from 'src/app/core/utils/custom-validations';
import {saveAs} from 'file-saver';
import {environment} from 'src/environments/environment';
import {CustomerModalComponent} from 'src/app/pages/customer-360/components';
import {HttpClient} from '@angular/common/http';
import {CategoryRegionApi} from '../../apis/category-region.api';
import {RmProfileService} from '../../../../core/services/rm-profile.service';

@Component({
  selector: 'app-report-app',
  templateUrl: './report-app.component.html',
  styleUrls: ['./report-app.component.scss'],
})
export class ReportAppComponent extends BaseComponent implements OnInit, AfterViewInit {
  commonData = {
    listDivision: [],
    listTerm: [],
    minDate: null,
    maxDate: new Date(),
    isRm: true,
    isHO: false,
    listChannel: [],
    branches: [],
  };
  textSearch: string;
  paramsTable = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };
  form = this.fb.group({
    division: 'INDIV',
    currencyType: '',
    currencyUnit: '',
    momentReport: [new Date(moment().add(-1, 'day').toDate()), CustomValidators.required],
    period: 'monthly',
    reportPeriodFrom: [new Date(moment().add(-1, 'months').toDate())],
    reportPeriodTo: '',
    comparePeriodFrom: [new Date(moment().add(-2, 'months').toDate())],
    reportBy: 'rm',
    reportTypeDetail: 'rm',
    term: '',
    areaCode: '',
    listOptionRm: 'ALL',
    listOptionBr: 'ALL',
    p_channel: 'ALL',
  });
  allRM = false;
  dataTerm = {
    rm: {
      pageable: {...this.pageable},
      term: [],
    },
    customer: {
      pageable: {...this.pageable},
      term: [],
    },
  };
  termData = [];
  listDataTable = [];
  columns: TableColumn[];
  fileName = 'bao-cao-APP';
  countRm: number;
  countBranch: number;
  branchCodes: Array<any>;
  preBranchCodes: Array<any>;
  intervalDownloadFileS3 = null;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private customerApi: CustomerApi,
    private rmApi: RmApi,
    private kpiReportApi: KpiReportApi,
    private http: HttpClient,
    private regionApi: CategoryRegionApi,
    private rmProfileService: RmProfileService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);

    this.isLoading = true;
  }

  ngOnInit(): void {
    this.commonData.isHO = this.currUser?.branch === BRANCH_HO;
    this.columns = [
      {
        name: this.fields.rmCode,
        prop: 'code',
      },
      {
        name: this.fields.rmName,
        prop: 'name',
      },
    ];
    this.prop = this.sessionService.getSessionData(SessionKey.REPORT_APP);
    if (!this.prop) {
      const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
      this.commonData.listDivision =
        divisionOfUser?.map((item) => {
          return {code: item.code, name: `${item.code || ''} - ${item.name || ''}`};
        }) || [];
      forkJoin([
        this.commonService.getCommonCategory(CommonCategory.DEVELOPMENT_CHANNEL).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.REPORTS_YEARS).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.TERM_TYPE).pipe(catchError(() => of(undefined))),
        this.categoryService
          .getBranchesOfUser(this.objFunction.rsId, Scopes.VIEW)
          .pipe(catchError(() => of(undefined))),
        this.categoryService
          .getCommonCategory(CommonCategory.REPORTS_MAX_LENGTH_RM)
          .pipe(catchError(() => of(undefined))),
      ]).subscribe(([channel, configYear, term, branchesOfUser, configReport]) => {
        this.commonData.listChannel = _.get(channel, 'content')?.map((item) => {
          return {
            code: item.value,
            label: `${item.name || ''}`,
          };
        });
        this.commonData.listChannel.unshift({
          code: 'ALL',
          label: 'Tất cả'
        });
        this.commonData.branches = branchesOfUser;
        this.countRm =
          _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.MAX_RM_REPORT_APP)?.value ||
          0;
        this.countBranch =
          _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.MAX_BRANCH_REPORT_APP)?.value ||
          0;
        this.commonData.isRm = _.isEmpty(branchesOfUser);
        this.commonData.listTerm = _.get(term, 'content')?.map((item) => {
          return {
            code: item.value,
            label: `${item.name || ''}`,
          };
        });
        this.commonData.listTerm = _.orderBy(this.commonData.listTerm, ['label'], ['desc', 'asc']);
        this.form.get('term').setValue(_.head(this.commonData.listTerm)?.code);
        this.sessionService.setSessionData(SessionKey.REPORT_APP, this.commonData);
        this.isLoading = false;
        const yearConfigDaily = _.find(_.get(configYear, 'content'), (item) => item.code === 'APP_DAILY')?.value || 44;
        this.commonData.minDate = new Date(
          moment()
            .add(-(yearConfigDaily - 1), 'day')
            .startOf('day')
            .valueOf()
        );
        if (this.commonData.isRm) {
          this.handleRM();
        }
      });
    } else {
      this.commonData = this.prop;
      const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
      this.commonData.listDivision =
        divisionOfUser?.map((item) => {
          return {code: item.code, name: `${item.code || ''} - ${item.name || ''}`};
        }) || [];
      this.form.get('division').setValue(_.head(this.commonData.listDivision)?.code);
      this.form.get('term').setValue(_.head(this.commonData.listTerm)?.code);
      this.isLoading = false;
      if (this.commonData.isRm) {
        this.handleRM();
      }
    }
  }

  handleRM() {
    this.form.controls.listOptionRm.setValue('choose');
    if (this.currUser.code) {
      this.textSearch = this.currUser.code;
      this.add();
    }
  }
  ngAfterViewInit() {
    this.form.controls.listOptionBr.valueChanges.subscribe((value) => {
      this.branchCodes = [];
      this.preBranchCodes = [];
      if (this.termData.length > 0) {
        this.termData = [];
        this.mapData();
      }
    });
    this.form.controls.reportBy.valueChanges.subscribe((value) => {
        this.branchCodes = [];
        this.preBranchCodes = [];
        this.termData = [];
        this.mapData();
    });
    this.form.controls.reportTypeDetail.valueChanges.subscribe((value) => {
      this.branchCodes = [];
      this.preBranchCodes = [];
      this.termData = [];
      this.mapData();
    });

  }

  search() {
    let formSearch = {
      crmIsActive: {value: 'true', disabled: true},
      rmBlock: {
        value: this.form.get('division').value,
        disabled: true,
      },
    };
    if (this.isReportBy('rm') && this.form.controls.listOptionBr.value !== 'ALL') {
      if (!_.isEmpty(this.branchCodes)) {
        const branchCodes = {branchCodes: _.map(this.branchCodes, 'code')};
        formSearch = {...formSearch, ...branchCodes};
      } else {
        this.messageService.warn('Chọn chi nhánh trước!');
        return;
      }
    }
    const modal = this.modalService.open(RmModalComponent, {windowClass: 'list__rm-modal'});
    modal.componentInstance.dataSearch = formSearch;
    modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
    if (_.get(formSearch,'branchCodes')) {
      modal.componentInstance.branchCodes = _.map(this.branchCodes, 'code')
    }
    modal.result
      .then((res) => {
        if (res) {
          const listRMSelect = res.listSelected?.map((item) => {
            return {
              hrsCode: item?.hrisEmployee?.employeeId,
              code: item?.t24Employee?.employeeCode,
              name: item?.hrisEmployee?.fullName,
            };
          });
          this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
          this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);
          this.mapData();
        }
      })
      .catch(() => {
      });
  }

  add() {
    this.textSearch = _.trim(this.textSearch);
    if (this.textSearch.length === 0) {
      this.isLoading = false;
      return;
    }
    if (_.findIndex(this.termData, (i) => i.code?.toUpperCase() === this.textSearch?.toUpperCase()) === -1) {
      let params = {
        crmIsActive: true,
        scope: Scopes.VIEW,
        rmBlock: this.form.get('division').value,
        rsId: this.objFunction?.rsId,
        employeeCode: this.textSearch,
        page: 0,
        size: maxInt32,
      };
      if (this.isReportBy('rm') && this.form.controls.listOptionBr.value !== 'ALL') {
        if (this.branchCodes) {
          const branchCodes = {branchCodes: _.map(this.branchCodes, 'code')};
          params = {...params, ...branchCodes};
        } else {
          this.messageService.warn('Chọn chi nhánh trước!');
          return;
        }
      }
      this.isLoading = true;
      this.rmApi.post('findAll', params).subscribe(
        (data) => {
          const itemResult = _.find(
            _.get(data, 'content'),
            (item) => item?.t24Employee?.employeeCode?.toUpperCase() === this.textSearch?.toUpperCase()
          );
          if (itemResult) {
            this.termData?.push({
              code: itemResult?.t24Employee?.employeeCode,
              name: itemResult?.hrisEmployee?.fullName,
              hrsCode: itemResult?.hrisEmployee?.employeeId,
            });
            this.termData = _.uniqBy(this.termData, (i) => i.hrsCode);
            this.mapData();
          } else if (!itemResult && !this.allRM) {
            this.messageService.warn(_.get(this.notificationMessage, 'rmNotExistOrNotBranh'));
          }
          this.isLoading = false;
        },
        (e) => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      const messageReport = 'rmIsExist';
      this.messageService.warn(_.get(this.notificationMessage, messageReport));
      this.isLoading = false;
      return;
    }
  }

  setPage(pageInfo) {
    this.paramsTable.page = pageInfo.offset;
    this.mapData();
  }

  mapData() {
    const total = this.termData.length;
    this.pageable.totalElements = total;
    this.pageable.totalPages = Math.floor(total / this.pageable.size);
    this.pageable.currentPage = this.paramsTable.page;
    const start = this.paramsTable.page * this.paramsTable.size;
    this.listDataTable = this.termData?.slice(start, start + this.paramsTable.size);
  }

  removeRecord(item) {
    _.remove(this.termData, (i) => i.code === item.code);
    _.remove(this.listDataTable, (i) => i.code === item.code);
    if (this.listDataTable.length === 0 && this.pageable.currentPage > 0) {
      this.paramsTable.page -= 1;
    }
    this.listDataTable = [...this.listDataTable];
    this.allRM = false;
    this.mapData();
  }

  viewReport() {
    let valueBr2 = '';
    let valueRm = '';
    if (this.isLoading) {
      return;
    }
    if (this.isReportBy('rm')) {
      if (this.form.controls.listOptionBr.value === 'ALL') {
        valueBr2 = 'ALL';
      } else {
        if (_.isEmpty(this.branchCodes)) {
          this.messageService.warn('Vui lòng chọn chi nhánh');
          return;
        }
        if (this.branchCodes.length > this.countBranch) {
          this.translate.get('notificationMessage.countBranchMaxINDIV', { number: this.countBranch }).subscribe((res) => {
            this.messageService.warn(res);
          });
          return;
        }
        this.branchCodes.forEach((i, index) => {
          if (index === this.branchCodes.length - 1) {
            valueBr2 += i.code;
          } else {
            valueBr2 += i.code + ';';
          }
        });
      }

      if (this.form.controls.listOptionRm.value === 'ALL') {
        valueRm = 'ALL';
      } else {
        if (this.listDataTable.length > this.countRm) {
          this.translate.get('notificationMessage.countRmMax', {number: this.countRm}).subscribe((res) => {
            this.messageService.warn(res);
          });
          return;
        }

        if (_.isEmpty(this.termData)) {
          this.messageService.warn('Vui lòng chọn RM');
          return;
        }

        if (this.termData.length > this.countRm) {
          this.translate.get('notificationMessage.countRmMax', { number: this.countRm }).subscribe((res) => {
            this.messageService.warn(res);
          });
          return;
        }
        this.termData.forEach((i, index) => {
          if (index === this.termData.length - 1) {
            valueRm += i.code;
          } else {
            valueRm += i.code + ';';
          }
        });
      }
      this.callApiTablue(valueBr2, valueRm);
      return;
    } else if (this.isReportBy('customer')) {
      if (this.isReportTypeDetail('rm')) {
        if (_.isEmpty(this.termData)) {
          this.messageService.warn('Vui lòng chọn RM');
          return;
        }

        if (this.termData.length > this.countRm) {
          this.translate.get('notificationMessage.countRmMax', { number: this.countRm }).subscribe((res) => {
            this.messageService.warn(res);
          });
          return;
        }
        this.termData.forEach((i, index) => {
          if (index === this.termData.length - 1) {
            valueRm += i.code;
          } else {
            valueRm += i.code + ',';
          }
        });
      } else if (this.isReportTypeDetail('branch')) {
        if (_.isEmpty(this.branchCodes)) {
          this.messageService.warn('Vui lòng chọn chi nhánh');
          return;
        }
        this.branchCodes.forEach((i, index) => {
          if (index === this.branchCodes.length - 1) {
            valueBr2 += i.code;
          } else {
            valueBr2 += i.code + ',';
          }
        });
      }
      this.callApiS3(valueRm, valueBr2);
      return;
    }
  }

  loadFile(pdfId: string, paramSearch: any) {
    this.kpiReportApi.loadFile(`download/${pdfId}`).then(
      (blobPDF) => {
        const url = `${environment.url_endpoint_kpi}/ors-report/hdv`;
        const modal = this.modalService.open(KpiReportModalComponent, {windowClass: 'tree__report-modal'});
        modal.componentInstance.data = blobPDF;
        modal.componentInstance.model = paramSearch;
        modal.componentInstance.baseUrl = url;
        modal.componentInstance.filename = this.fileName;
        modal.componentInstance.title = 'Báo cáo APP';
        modal.componentInstance.extendUrl = '';
        this.isLoading = false;
      },
      () => {
        this.messageService.error(this.notificationMessage.E001);
        this.isLoading = false;
      }
    );
  }

  removeList() {
    this.termData = [];
    this.paramsTable.page = 0;
    this.allRM = false;
    this.mapData();
  }

  isShowCheckboxAllRm() {
    return (
      this.currUser?.branch !== BRANCH_HO &&
      !this.commonData?.isRm &&
      this.form.get('division')?.value !== Division.INDIV &&
      this.form.get('reportBy')?.value === 'rm'
    );
  }

  loadReport(embeddedLink: string, paramSearch: any) {
    const modal = this.modalService.open(KpiReportModalComponent, {windowClass: 'tree__report-modal'});
    modal.componentInstance.embeddedReport = embeddedLink;
    modal.componentInstance.data = null;
    modal.componentInstance.model = paramSearch;
    modal.componentInstance.baseUrl = null;
    modal.componentInstance.filename = this.fileName;
    modal.componentInstance.title = 'Báo cáo APP';
    modal.componentInstance.extendUrl = '';
    modal.componentInstance.reportType = 1;
  }

  callApiTablue(valueBr2, valueRm) {
    const paramsTablue =
    {
      scope: 'VIEW',
      rsId: this.objFunction.rsId,
      reportCode: 'BAOCAO_APP_RM',
      reportParams: [
      {
        name: 'p_fr_date',
        value: this.form.get('momentReport').value ? formatDate(this.form.get('momentReport').value, 'yyyyMMdd', 'en') : ''
      },
      {
        name: 'p_rm_code',
        value: valueRm
      },
      {
        name: 'p_br_code_lv2',
        value: valueBr2
      },
      {
        name: 'p_channel',
        value: this.form.get('p_channel').value
      }
    ]
    }

    this.isLoading = true;
    forkJoin([this.kpiReportApi.getTableauReport(paramsTablue)]).subscribe(
      ([embeddedLink]) => {
        if (embeddedLink.length > 0) {
          this.loadReport(embeddedLink, paramsTablue);
        } else {
          this.messageService.error(this.notificationMessage.E001);
        }
        this.isLoading = false;
      },
      () => {
        this.messageService.error(this.notificationMessage.E001);
        this.isLoading = false;
      }
    );
  }

  onModelChange($event) {
    if(_.xor(_.map($event, x => x.code), _.map(this.preBranchCodes, x => x.code), _.isEqual).length > 0) {
      this.termData = [];
      this.mapData();
    }
    this.preBranchCodes = Object.assign([], $event);
  }

  isReportBy(value: string) {
    return this.form.get('reportBy').value === value;
  }

  isReportTypeDetail(value: string) {
    return this.form.get('reportTypeDetail').value === value;
  }

  isDownloadReport(value: string) {
    return this.form.get('downloadReport').value === value;
  }


  getReportCodeTablue() {
    let reportCode = 'HDVBQ';
    switch (this.form.get('reportBy').value) {
      case 'rm':
        reportCode += '_RM';
        break;
      case 'area':
        reportCode += '_AREA';
        break;
      case 'branch':
        reportCode += '_BRANCH';
        break;
      case 'contract':
        reportCode += '_HD'
        break;
      case 'customer':
        reportCode += '_CUSTOMER'
    }
    switch (this.form.get('period').value) {
      case 'monthly':
        reportCode += '_MONTH';
        break;
      case 'quarterly':
        reportCode += '_QUARTER';
        break;
      case 'yearly':
        reportCode += '_YEAR';
        break;
    }

    return reportCode;
  }

  get showGroupBy(): boolean {
    return this.form?.get('reportBy').value === 'rm' &&
      this.form?.get('listOptionBr').value === 'choose' &&
      this.form?.get('listOptionRm').value === 'choose'
  }

  callApiS3(valueRm, valueBr) {
    const paramsS3 =
      {
        reportCode: _.isEmpty(valueRm) ? 'REPORT_APP_KH_BRANCH_CODE_C2' : 'REPORT_APP_KH_RM',
        reportParams: [
          {
            name: 'prd_id',
            value: this.form.get('momentReport').value ? formatDate(this.form.get('momentReport').value, 'yyyyMMdd', 'en') : ''
          },
          {
            name: 'field01',
            value: _.isEmpty(valueRm) ? valueBr : valueRm
          },
          {
            name: 'field02',
            value: 'ALL'
          }
        ],
        rsId: this.objFunction.rsId,
        scope: 'VIEW'
      }

    this.isLoading = true;
    this.kpiReportApi.reportS3(paramsS3).subscribe(urls => {
      this.isLoading = false;
      if (!_.isEmpty(urls)) {
        const download = (urls) => {
          const url = urls.pop();
          window.open(url, '_self');

          if (!urls.length && this.intervalDownloadFileS3) {
            clearInterval(this.intervalDownloadFileS3);
          }
        }

        this.intervalDownloadFileS3 = setInterval(download, 1000, urls);

      } else {
        this.confirmService.warn('Không có file dữ liệu');
      }
    },error => {
      this.isLoading = false;
      this.messageService.error(this.notificationMessage.error);
    });
  }

}
