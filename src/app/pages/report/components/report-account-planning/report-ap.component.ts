import { formatDate } from '@angular/common';
import { forkJoin, of, Subject } from 'rxjs';
import { Component, OnInit, Injector, AfterViewInit, ViewEncapsulation } from '@angular/core';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { TableColumn } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import { CommonCategory, FunctionCode, maxInt32, Scopes } from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as moment from 'moment';
import { catchError } from 'rxjs/operators';
import { CustomerApi } from 'src/app/pages/customer-360/apis';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { CustomerModalComponent } from 'src/app/pages/customer-360/components';
import { ChooseBranchesModalComponent } from 'src/app/pages/system/components/choose-branches-modal/choose-branches-modal.component';
import { RmModalComponent } from 'src/app/pages/rm/components/rm-modal/rm-modal.component';
import { KpiReportApi } from 'src/app/pages/report/apis/kpi-report.api';
import { KpiReportModalComponent } from 'src/app/pages/report/dialogs/kpi-report-modal/kpi-report-modal.component';
import { RmApi } from 'src/app/pages/rm/apis';
import { environment } from 'src/environments/environment';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-report-ap',
  templateUrl: './report-ap.component.html',
  styleUrls: ['./report-ap.component.scss'],
})
export class ReportAPComponent extends BaseComponent implements OnInit, AfterViewInit {
  commonData = {
    listDivision: [],
    // maxDate: new Date(),
    listBranch: [],
    minDate: new Date(moment().set('month', -13).toDate()),
    maxDate: new Date(moment().set('month', 11).toDate()),
  };
  textSearch: string;
  paramsTable = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };
  formSearch = this.fb.group({
    reportBy: 'RM',
    downloadReport: 'HTML',
    period: '1',
    momentPlan: [new Date()],
    fromDate: [new Date(), CustomValidators.required],
    yearPlan: '',
    monthPlan: '',
  });
  dataTerm = {
    branch: {
      pageable: { ...this.pageable },
      term: [],
    },
    customer: {
      pageable: { ...this.pageable },
      term: [],
    },
    rm: {
      pageable: { ...this.pageable },
      term: [],
    },
  };
  tableType: string;
  termData = [];
  listDataTable = [];
  columns: TableColumn[];
  isSearch = false;
  fileName = 'Bao-cao-ap';
  objFunctionCustomer: any;
  listYear = [
    {
      code: new Date().getFullYear(),
      name: 'Năm ' + new Date().getFullYear(),
    },
    {
      code: new Date().getFullYear() - 1,
      name: 'Năm ' + (new Date().getFullYear() - 1),
    },
  ];
  listMonth = [
    {
      code: 1,
      name: '6 tháng đầu năm',
    },
    {
      code: 2,
      name: '6 tháng cuối năm ',
    },
  ];

  isRm = false;
  listMaxData = [];

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private customerApi: CustomerApi,
    private rmApi: RmApi,
    private kpiReportApi: KpiReportApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.objFunctionCustomer = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.tableType = this.formSearch.get('reportBy').value;
    this.columns = [
      {
        name: this.fields.branchCode,
        prop: 'code',
      },
      {
        name: this.fields.branchName,
        prop: 'name',
      },
    ];
    forkJoin([
      this.categoryService.getBranchesOfUser(this.objFunction.rsId, Scopes.VIEW).pipe(catchError(() => of(undefined))),
      this.categoryService
        .getCommonCategory(CommonCategory.REPORTS_MAX_LENGTH_RM)
        .pipe(catchError(() => of(undefined))),
    ]).subscribe(([branchesOfUser, maxData]) => {
      this.listMaxData = [
        { code: 'rm', value: maxData?.content?.filter((i) => i.code === CommonCategory.MAX_RM)[0]?.value },
        { code: 'cn', value: maxData?.content?.filter((i) => i.code === CommonCategory.MAX_BRANCH)[0]?.value },
        { code: 'kh', value: maxData?.content?.filter((i) => i.code === CommonCategory.MAX_CUSTOMER)[0]?.value },
      ];

      console.log('234235345', this.listMaxData);

      this.commonData.listBranch = branchesOfUser || [];
      if (_.isEmpty(branchesOfUser)) {
        this.isRm = true;
        this.commonData.listBranch?.push({ code: this.currUser.branch, name: this.currUser.branchName });
      }

      this.isLoading = false;
    });
  }

  ngAfterViewInit() {
    this.formSearch.controls.reportBy.valueChanges.subscribe((value) => {
      if (this.tableType !== value) {
        this.paramsTable.page = 0;
        this.dataTerm[this.tableType] = {
          pageable: { ...this.pageable },
          term: [...this.termData],
        };
        this.textSearch = '';
        if (value === 'CN') {
          this.columns[0].name = this.fields.branchCode;
          this.columns[1].name = this.fields.branchName;
          this.termData = [...this.dataTerm.branch.term];
          this.pageable = { ...this.dataTerm.branch.pageable };
        } else if (value === 'KH') {
          this.columns[0].name = this.fields.customerCode;
          this.columns[1].name = this.fields.customerName;
          this.termData = [...this.dataTerm.customer.term];
          this.pageable = { ...this.dataTerm.customer.pageable };
        } else if (value === 'RM') {
          this.columns[0].name = this.fields.rmCode;
          this.columns[1].name = this.fields.rmName;
          this.termData = [...this.dataTerm.rm.term];
          this.pageable = { ...this.dataTerm.rm.pageable };
        }
        this.tableType = value;
        this.mapData();
      }
    });

    this.formSearch.controls.period.valueChanges.subscribe((value) => {
      if (+value === 1) {
        this.commonData.minDate = new Date(moment().set('month', -13).toDate());
        this.commonData.maxDate = new Date(moment().set('month', 11).toDate());
        this.formSearch.controls.momentPlan.setValue(new Date());
      } else if (+value === 2) {
        this.formSearch.controls.yearPlan.setValue(_.first(this.listYear).code);
        this.formSearch.controls.monthPlan.setValue(_.first(this.listMonth).code);
      } else {
        this.formSearch.controls.yearPlan.setValue(_.first(this.listYear).code);
      }
    });
  }

  search() {
    const reportBy = this.formSearch.get('reportBy').value;
    if (reportBy === 'CN') {
      const modal = this.modalService.open(ChooseBranchesModalComponent, { windowClass: 'tree__branches-modal' });
      modal.componentInstance.listBranchOld = _.map(this.termData, (x) => x.code) || [];
      modal.componentInstance.listBranch = this.commonData.listBranch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData = res;
            this.mapData();
          }
        })
        .catch(() => {});
    } else if (reportBy === 'KH') {
      const formSearch = {
        customerType: {
          value: 'CIB',
          disabled: true,
        },
      };
      const modal = this.modalService.open(CustomerModalComponent, { windowClass: 'list__customer360-modal' });
      modal.componentInstance.listSelectedOld = this.termData;
      modal.componentInstance.dataSearch = formSearch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData =
              res.listSelected?.map((item) => {
                return {
                  code: item.customerCode,
                  customerCode: item.customerCode,
                  name: item.customerName ? item.customerName : item.name,
                };
              }) || [];
            this.mapData();
          }
        })
        .catch(() => {});
    } else if (reportBy === 'RM') {
      const formSearch = {
        crmIsActive: { value: 'true', disabled: true },
        rmBlock: {
          value: 'CIB',
          disabled: true,
        },
        rm: true,
        manager: true,
        isReportByRm: false,
      };
      const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
      modal.componentInstance.dataSearch = formSearch;
      modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
      modal.result
        .then((res) => {
          if (res) {
            const listRMSelect = res.listSelected?.map((item) => {
              return {
                hrsCode: item?.hrisEmployee?.employeeId,
                code: item?.t24Employee?.employeeCode,
                name: item?.hrisEmployee?.fullName,
              };
            });
            this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
            this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);
            this.mapData();
          }
        })
        .catch(() => {});
    }
  }

  add() {
    const reportBy = this.formSearch.get('reportBy').value;
    this.textSearch = _.trim(this.textSearch);
    if (this.textSearch.length === 0) {
      this.isSearch = false;
      return;
    }
    if (_.findIndex(this.termData, (i) => i.code?.toUpperCase() === this.textSearch?.toUpperCase()) === -1) {
      if (reportBy === 'CN') {
        const itemResult = _.find(
          this.commonData.listBranch,
          (item) => item.code?.toUpperCase() === this.textSearch?.toUpperCase()
        );
        if (itemResult) {
          this.messageService.success(_.get(this.notificationMessage, 'branchSuccess'));
          this.termData?.push(itemResult);
          this.mapData();
        } else {
          this.messageService.warn('Chi nhánh tìm kiếm không tồn tại hoặc không thuộc miền dữ liệu được phân quyền');
        }
      } else if (reportBy === 'KH') {
        const params = {
          customerCode: this.textSearch,
          customerType: 'CIB',
          rsId: this.objFunctionCustomer?.rsId,
          scope: Scopes.VIEW,
          pageNumber: 0,
          pageSize: global?.userConfig?.pageSize,
        };
        this.isSearch = true;
        this.customerApi.search(params).subscribe(
          (data) => {
            if (data?.length > 0) {
              this.termData?.push({
                code: data[0]?.customerCode,
                name: data[0]?.customerName,
                customerCode: data[0]?.customerCode,
              });
              this.messageService.success(_.get(this.notificationMessage, 'customerSuccess'));
              this.mapData();
            } else {
              this.messageService.warn('Khách hàng không thuộc phạm vi quản lý');
            }
            this.isSearch = false;
          },
          (e) => {
            this.messageService.error(this.notificationMessage.E001);
            this.isLoading = false;
            this.isSearch = false;
          }
        );
      } else {
        const params = {
          rm: true,
          manager: true,
          crmIsActive: true,
          scope: Scopes.VIEW,
          rmBlock: 'CIB',
          rsId: this.objFunction?.rsId,
          employeeCode: this.textSearch,
          page: 0,
          size: maxInt32,
          isReportByRm: false,
        };
        this.isSearch = true;
        this.rmApi.post('findAll', params).subscribe(
          (data) => {
            const itemResult = _.find(
              _.get(data, 'content'),
              (item) => item?.t24Employee?.employeeCode?.toUpperCase() === this.textSearch?.toUpperCase()
            );
            if (itemResult) {
              this.termData?.push({
                code: itemResult?.t24Employee?.employeeCode,
                name: itemResult?.hrisEmployee?.fullName,
                hrsCode: itemResult?.hrisEmployee?.employeeId,
              });
              this.mapData();
            } else {
              this.messageService.warn(_.get(this.notificationMessage, 'rmNotExistOrNotBranh'));
            }
            this.isSearch = false;
          },
          (e) => {
            this.messageService.error(this.notificationMessage.E001);
            this.isLoading = false;
            this.isSearch = false;
          }
        );
      }
    } else {
      const messageReport = this.formSearch.get('reportBy').value.toString() + 'IsExist';
      this.messageService.warn(_.get(this.notificationMessage, messageReport));
      this.isSearch = false;
      return;
    }
  }

  setPage(pageInfo) {
    if (!this.isLoading) {
      this.paramsTable.page = pageInfo.offset;
      this.mapData();
    }
  }

  mapData() {
    const total = this.termData.length;
    this.pageable.totalElements = total;
    this.pageable.totalPages = Math.floor(total / this.pageable.size);
    this.pageable.currentPage = this.paramsTable.page;
    const start = this.paramsTable.page * this.paramsTable.size;
    this.listDataTable = this.termData?.slice(start, start + this.paramsTable.size);
  }

  removeRecord(item) {
    _.remove(this.termData, (i) => i.code === item.code);
    _.remove(this.listDataTable, (i) => i.code === item.code);
    if (this.listDataTable.length === 0 && this.pageable.currentPage > 0) {
      this.paramsTable.page -= 1;
    }
    this.listDataTable = [...this.listDataTable];
    this.mapData();
  }

  viewReport() {
    if (this.isLoading || this.isSearch) {
      return;
    }
    if (this.formSearch.valid) {
      if (_.isEmpty(this.termData)) {
        let textValidation = this.formSearch.get('reportBy').value.toLowerCase() + 'Validation';
        this.messageService.warn(_.get(this.notificationMessage, textValidation));
        return;
      } else {
        let maxData = this.listMaxData.filter((i) => i.code === this.formSearch.get('reportBy').value.toLowerCase())[0]
          .value;
        let textValidation = 'notificationMessage.' + this.formSearch.get('reportBy').value.toLowerCase() + 'CountMax';
        if (this.termData.length > maxData) {
          this.translate.get(textValidation, { number: maxData }).subscribe((res) => {
            this.messageService.warn(res);
          });
          return;
        }
      }
      const dataTable = this.termData?.map((i) => i.code);
      const params = {
        attributeFormat: 'pdf',
        reportType: this.formSearch.controls.reportBy.value,
        data: dataTable,
        period: +this.formSearch.controls.period.value,
        rsId: this.formSearch.get('reportBy').value === 'KH' ? this.objFunctionCustomer.rsId : this.objFunction.rsId,
        scope: Scopes.VIEW,
        momentPlan:
          this.formSearch.controls.period.value !== '3'
            ? formatDate(this.formSearch.controls.momentPlan.value, 'MM-yyyy', 'en')
            : this.formSearch.controls.yearPlan.value,
        fromDate: formatDate(this.formSearch.controls.fromDate.value, 'yyyy-mm-dd', 'en'),
      };

      if (this.formSearch.get('downloadReport').value === 'EXCEL') {
        params.attributeFormat = 'xlsx';
      }
      this.isLoading = true;
      forkJoin([this.kpiReportApi.reportAp(params)]).subscribe(
        ([pdfId]) => {
          const respondSource = new Subject<any>();
          this.kpiReportApi.checkIdReportSuccess(pdfId, respondSource);
          respondSource.asObservable().subscribe((res) => {
            if (res?.error === 'error') {
              this.messageService.error(this.notificationMessage.E001);
            } else if (res?.fileId) {
              this.loadFile(res?.fileId, params);
            } else {
              this.messageService.error(this.notificationMessage.messageOrsReport);
            }
            this.isLoading = false;
            respondSource.unsubscribe();
          });
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      validateAllFormFields(this.formSearch);
      return;
    }
  }

  loadFile(pdfId: string, paramSearch: any) {
    if (this.formSearch.get('downloadReport').value === 'EXCEL') {
      forkJoin([this.kpiReportApi.loadFile(`download/${pdfId}`)]).subscribe(
        ([blobExcel]) => {
          saveAs(blobExcel, `${this.fileName}.xlsx`);
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      this.kpiReportApi.loadFile(`download/${pdfId}`).then(
        (blobPDF) => {
          const url = `${environment.url_endpoint_kpi}/ors-report/ap`;
          if (this.formSearch.get('downloadReport').value === 'HTML') {
            const modal = this.modalService.open(KpiReportModalComponent, { windowClass: 'tree__report-modal' });
            modal.componentInstance.data = blobPDF;
            modal.componentInstance.model = paramSearch;
            modal.componentInstance.baseUrl = url;
            modal.componentInstance.filename = this.fileName;
            modal.componentInstance.title = this.fileName;
            modal.componentInstance.extendUrl = '';
          } else if (this.formSearch.get('downloadReport').value === 'PDF') {
            saveAs(blobPDF, `${this.fileName}.pdf`);
          }
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    }
  }

  removeList() {
    this.termData = [];
    this.paramsTable.page = 0;
    this.mapData();
  }
}
