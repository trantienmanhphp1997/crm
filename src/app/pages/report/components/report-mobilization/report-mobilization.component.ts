import { formatDate } from '@angular/common';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { Component, OnInit, Injector, AfterViewInit } from '@angular/core';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { TableColumn } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import {
  BRANCH_HO,
  CommonCategory,
  Division,
  FunctionCode,
  maxInt32,
  Scopes,
  SessionKey,
} from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as moment from 'moment';
import { catchError } from 'rxjs/operators';
import { CustomerApi } from 'src/app/pages/customer-360/apis';
import { KpiReportApi } from '../../apis';
import { KpiReportModalComponent } from '../../dialogs';
import { RmApi } from 'src/app/pages/rm/apis';
import { RmModalComponent } from 'src/app/pages/rm/components/rm-modal/rm-modal.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { saveAs } from 'file-saver';
import { environment } from 'src/environments/environment';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { CustomerModalComponent } from 'src/app/pages/customer-360/components';

@Component({
  selector: 'app-report-mobilization',
  templateUrl: './report-mobilization.component.html',
  styleUrls: ['./report-mobilization.component.scss'],
})
export class ReportMobilizationComponent extends BaseComponent implements OnInit, AfterViewInit {
  commonData = {
    listDivision: [],
    listCurrencyUnit: [],
    listTerm: [],
    listCurrencyType: [],
    minDate: null,
    maxDate: new Date(),
    listQuarterly: [
      {
        label: 'Quý 1',
        value: 1,
      },
      {
        label: 'Quý 2',
        value: 2,
      },
      {
        label: 'Quý 3',
        value: 3,
      },
      {
        label: 'Quý 4',
        value: 4,
      },
    ],
    listYears: [],
    listYearsCompare: [],
    listQuarterlyCompare: [],
    isRm: true,
  };
  textSearch: string;
  paramsTable = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };
  form = this.fb.group({
    division: '',
    currencyType: '',
    currencyUnit: '',
    reportType: 'moment',
    momentReport: [new Date(moment().add(-1, 'day').toDate()), CustomValidators.required],
    momentCompare: null,
    period: 'monthly',
    reportPeriodFrom: [new Date(moment().add(-1, 'months').toDate())],
    reportPeriodTo: '',
    comparePeriodFrom: null,
    comparePeriodTo: null,
    reportBy: 'rm',
    reportTypeDetail: 'customer',

    reportPeriodQuarterly: '',
    reportPeriodYear: '',

    comparePeriodQuarterly: '',
    comparePeriodYear: '',
    downloadReport: 'HTML',
    term: '',
  });
  allRM = false;
  dataTerm = {
    rm: {
      pageable: { ...this.pageable },
      term: [],
    },
    customer: {
      pageable: { ...this.pageable },
      term: [],
    },
  };
  tableType: string;
  termData = [];
  listDataTable = [];
  columns: TableColumn[];
  maxDate = new Date();
  fileName = 'bao-cao-huy-dong-von';
  countRm: number;
  countCustomerMax: number;
  maxMonth: any;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private customerApi: CustomerApi,
    private rmApi: RmApi,
    private kpiReportApi: KpiReportApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);

    this.isLoading = true;
  }

  ngOnInit(): void {
    this.tableType = this.form.get('reportBy').value;
    this.columns = [
      {
        name: this.fields.rmCode,
        prop: 'code',
      },
      {
        name: this.fields.rmName,
        prop: 'name',
      },
    ];
    this.form.get('reportPeriodQuarterly').setValue(_.head(this.commonData.listQuarterly)?.value);
    this.prop = this.sessionService.getSessionData(SessionKey.REPORT_MOBILIZATION);
    if (!this.prop) {
      const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
      this.commonData.listDivision =
        divisionOfUser?.map((item) => {
          return { code: item.code, name: `${item.code || ''} - ${item.name || ''}` };
        }) || [];
      this.form.get('division').setValue(_.head(this.commonData.listDivision)?.code);
      forkJoin([
        this.commonService
          .getCommonCategory(CommonCategory.CURRENCY_UNIT_REPORTS)
          .pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.REPORTS_YEARS).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.TERM_TYPE).pipe(catchError(() => of(undefined))),
        this.categoryService
          .getBranchesOfUser(this.objFunction.rsId, Scopes.VIEW)
          .pipe(catchError(() => of(undefined))),
        this.categoryService.getCommonCategory(CommonCategory.CURRENCY_TYPE).pipe(catchError(() => of(undefined))),
        this.categoryService
          .getCommonCategory(CommonCategory.REPORTS_MAX_LENGTH_RM)
          .pipe(catchError(() => of(undefined))),
      ]).subscribe(([currencyUnits, configYear, term, branchesOfUser, listCurrencyType, configReport]) => {
        this.countRm =
          _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.MAX_RM_REPORT_CREDIT)?.value ||
          0;
        this.countCustomerMax =
          _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.MAX_CUSTOMER_REPORT)?.value ||
          0;
        this.commonData.listCurrencyType = _.get(listCurrencyType, 'content')
          ?.filter((item) => {
            return item.code === 'ALL';
          })
          ?.map((itemValue) => {
            return {
              code: itemValue.value,
              label: `${itemValue.name || ''}`,
            };
          });
        this.form.get('currencyType').setValue(_.head(this.commonData.listCurrencyType)?.code);
        this.commonData.isRm = _.isEmpty(branchesOfUser);
        this.commonData.listCurrencyUnit = _.orderBy(_.get(currencyUnits, 'content'), ['orderNum'], ['asc', 'desc']);
        this.commonData.listCurrencyUnit =
          _.get(currencyUnits, 'content')?.map((item) => {
            return { code: item.value, name: item.name, isDefault: item.isDefault };
          }) || [];
        this.commonData.listTerm = _.get(term, 'content')?.map((item) => {
          return {
            code: item.value,
            label: `${item.name || ''}`,
          };
        });
        this.commonData.listTerm = _.orderBy(this.commonData.listTerm, ['label'], ['desc', 'asc']);
        this.form.get('term').setValue(_.head(this.commonData.listTerm)?.code);

        const yearConfig = _.find(_.get(configYear, 'content'), (item) => item.code === 'HDV')?.value || 1;
        const year = moment().year();
        for (let i = 0; i < yearConfig; i++) {
          this.commonData.listYears.push({
            value: year - i,
            label: year - i,
          });
        }
        this.maxMonth = _.find(_.get(configYear, 'content'), (item) => item.code === 'PTKH_MONTH_OPTION')?.value || 1;
        this.commonData.minDate = new Date(
          moment()
            .add(-(yearConfig - 1), 'year')
            .startOf('year')
            .valueOf()
        );
        this.form.get('reportPeriodYear').setValue(_.head(this.commonData.listYears)?.value);
        this.sessionService.setSessionData(SessionKey.REPORT_MOBILIZATION, this.commonData);
        this.form.get('currencyUnit').setValue(_.head(this.commonData.listCurrencyUnit)?.code);
        this.commonData.listCurrencyUnit.forEach((item) => {
          if (item.isDefault) {
            this.form.get('currencyUnit').setValue(item?.code);
          }
        });
        this.isLoading = false;
      });
    } else {
      this.commonData = this.prop;
      const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
      this.commonData.listDivision =
        divisionOfUser?.map((item) => {
          return { code: item.code, name: `${item.code || ''} - ${item.name || ''}` };
        }) || [];
      this.form.get('division').setValue(_.head(this.commonData.listDivision)?.code);

      this.form.get('currencyUnit').setValue(_.head(this.commonData.listCurrencyUnit)?.code);
      this.commonData.listCurrencyUnit.forEach((item) => {
        if (item.isDefault) {
          this.form.get('currencyUnit').setValue(item?.code);
        }
      });
      this.form.get('currencyType').setValue(_.head(this.commonData.listCurrencyType)?.code);
      this.form.get('term').setValue(_.head(this.commonData.listTerm)?.code);
      this.isLoading = false;
    }
    this.textSearch = this.currUser.code;
    if (this.currUser.code) {
      this.add();
    }
  }

  ngAfterViewInit() {
    this.form.controls.reportBy.valueChanges.subscribe((value) => {
      if (this.tableType !== value) {
        this.allRM = false;
        this.paramsTable.page = 0;
        this.dataTerm[this.tableType] = {
          pageable: { ...this.pageable },
          term: [...this.termData],
        };
        this.textSearch = '';
        if (value === 'rm') {
          this.columns[0].name = this.fields.rmCode;
          this.columns[1].name = this.fields.rmName;
          this.termData = [...this.dataTerm.rm.term];
          this.pageable = { ...this.dataTerm.rm.pageable };
          if (this.commonData.isRm) {
            this.textSearch = this.currUser.code;
            if (this.termData.length === 0 && this.currUser.code) {
              this.add();
            }
          } else {
            this.textSearch = '';
          }
        } else if (value === 'customer') {
          this.columns[0].name = this.fields.customerCode;
          this.columns[1].name = this.fields.customerName;
          this.termData = [...this.dataTerm.customer.term];
          this.pageable = { ...this.dataTerm.customer.pageable };
        }
        if (!_.find(this.commonData.listDivision, (item) => item.code === this.form.get('division').value)) {
          this.form.get('division').setValue(_.head(this.commonData.listDivision)?.code);
        }
        this.tableType = value;
        this.mapData();
      }
    });
    this.form.controls.reportType.valueChanges.subscribe((value) => {
      this.form.get('period').setValue('monthly');
      this.form.get('momentCompare').setValue(null);
    });
    this.form.controls.reportTypeDetail.valueChanges.subscribe((value) => {
      if (value === 'contract') {
        this.form.get('momentCompare').setValue(null);
        this.form.get('comparePeriodFrom').setValue(null);
        this.form.get('comparePeriodTo').setValue(null);
        this.form.get('comparePeriodYear').setValue(null);
        this.form.get('comparePeriodQuarterly').setValue(null);
      }
    });

    this.form.controls.period.valueChanges.subscribe((value) => {
      this.form.get('comparePeriodFrom').setValue(null);
      this.form.get('comparePeriodTo').setValue(null);
      this.commonData.listYearsCompare = this.commonData.listYears;
      this.commonData.listQuarterlyCompare = this.commonData.listQuarterly;
      this.form.get('comparePeriodQuarterly').setValue(null);
      this.form.get('comparePeriodYear').setValue(null);
      if (value === 'optional') {
        this.form.get('reportPeriodFrom').setValue(new Date(moment().set('month', 0).toDate()));
        this.form.get('reportPeriodTo').setValue(new Date(moment().set('month', 0).toDate()));
      } else if (value === 'monthly') {
        this.form.get('reportPeriodFrom').setValue(new Date(moment().add(-1, 'months').toDate()));
      }
    });

    this.form.controls.division.valueChanges.subscribe(() => {
      this.removeList();
    });
  }

  search() {
    if (this.form.get('reportBy').value === 'rm') {
      const formSearch = {
        crmIsActive: { value: 'true', disabled: true },
        rmBlock: {
          value: this.form.get('division').value,
          disabled: true,
        },
        isNHSReport: this.form.get('division').value === Division.INDIV,
        isReportByRm: this.commonData.isRm,
        manager: true,
        rm: true,
      };
      const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
      modal.componentInstance.dataSearch = formSearch;
      modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
      modal.result
        .then((res) => {
          if (res) {
            const listRMSelect = res.listSelected?.map((item) => {
              return {
                hrsCode: item?.hrisEmployee?.employeeId,
                code: item?.t24Employee?.employeeCode,
                name: item?.hrisEmployee?.fullName,
              };
            });
            this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
            this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);
            this.mapData();
          }
        })
        .catch(() => {});
    } else if (this.form.get('reportBy').value === 'customer') {
      const formSearch = {
        customerType: {
          value: this.form.get('division').value,
          disabled: true,
        },
        isCheckGroupCBQL: { value: this.form.get('division').value !== Division.INDIV },
      };
      const modal = this.modalService.open(CustomerModalComponent, { windowClass: 'list__customer360-modal' });
      modal.componentInstance.listSelectedOld = this.termData;
      modal.componentInstance.dataSearch = formSearch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData =
              res.listSelected?.map((item) => {
                return {
                  code: item.customerCode,
                  customerCode: item.customerCode,
                  name: item.customerName ? item.customerName : item.name,
                };
              }) || [];
            this.mapData();
          }
        })
        .catch(() => {});
    }
  }

  add() {
    this.textSearch = _.trim(this.textSearch);
    if (this.textSearch.length === 0) {
      this.isLoading = false;
      return;
    }
    if (_.findIndex(this.termData, (i) => i.code?.toUpperCase() === this.textSearch?.toUpperCase()) === -1) {
      if (this.form.get('reportBy').value === 'rm') {
        const params = {
          crmIsActive: true,
          scope: Scopes.VIEW,
          rmBlock: this.form.get('division').value,
          rsId: this.objFunction?.rsId,
          employeeCode: this.textSearch,
          page: 0,
          size: maxInt32,
          isNHSReport: this.form.get('division').value === Division.INDIV,
          isReportByRm: this.commonData.isRm,
          rm: true,
          manager: true,
        };
        this.isLoading = true;
        this.rmApi.post('findAll', params).subscribe(
          (data) => {
            const itemResult = _.find(
              _.get(data, 'content'),
              (item) => item?.t24Employee?.employeeCode?.toUpperCase() === this.textSearch?.toUpperCase()
            );
            if (itemResult) {
              this.termData?.push({
                code: itemResult?.t24Employee?.employeeCode,
                name: itemResult?.hrisEmployee?.fullName,
                hrsCode: itemResult?.hrisEmployee?.employeeId,
              });
              this.termData = _.uniqBy(this.termData, (i) => i.hrsCode);
              this.mapData();
            } else if (!itemResult && !this.allRM) {
              this.messageService.warn(_.get(this.notificationMessage, 'rmNotExistOrNotBranh'));
            }
            this.isLoading = false;
          },
          (e) => {
            this.messageService.error(this.notificationMessage.E001);
            this.isLoading = false;
          }
        );
      } else if (this.form.get('reportBy').value === 'customer') {
        const params = {
          customerCode: this.textSearch,
          customerType: this.form.get('division').value,
          rsId: this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`)?.rsId,
          scope: Scopes.VIEW,
          pageNumber: 0,
          pageSize: global?.userConfig?.pageSize,
          isCheckGroupCBQL: this.form.get('division').value !== Division.INDIV,
        };
        this.isLoading = true;
        let api: Observable<any>;
        if (this.form.get('division').value !== Division.INDIV) {
          api = this.customerApi.searchSme(params);
        } else {
          api = this.customerApi.search(params);
        }
        api.subscribe(
          (data) => {
            if (data?.length > 0) {
              this.termData?.push({
                code: data[0]?.customerCode,
                name: data[0]?.customerName,
                customerCode: data[0]?.customerCode,
              });
              this.mapData();
            } else {
              this.messageService.warn(_.get(this.notificationMessage, 'customerNotExist'));
            }
            this.isLoading = false;
          },
          (e) => {
            this.messageService.error(this.notificationMessage.E001);
            this.isLoading = false;
          }
        );
      }
    } else {
      const messageReport = this.form.get('reportBy').value.toString() + 'IsExist';
      this.messageService.warn(_.get(this.notificationMessage, messageReport));
      this.isLoading = false;
      return;
    }
  }

  setPage(pageInfo) {
    this.paramsTable.page = pageInfo.offset;
    this.mapData();
  }

  mapData() {
    const total = this.termData.length;
    this.pageable.totalElements = total;
    this.pageable.totalPages = Math.floor(total / this.pageable.size);
    this.pageable.currentPage = this.paramsTable.page;
    const start = this.paramsTable.page * this.paramsTable.size;
    this.listDataTable = this.termData?.slice(start, start + this.paramsTable.size);
  }

  removeRecord(item) {
    _.remove(this.termData, (i) => i.code === item.code);
    _.remove(this.listDataTable, (i) => i.code === item.code);
    if (this.listDataTable.length === 0 && this.pageable.currentPage > 0) {
      this.paramsTable.page -= 1;
    }
    this.listDataTable = [...this.listDataTable];
    this.allRM = false;
    this.mapData();
  }

  viewReport() {
    if (this.isLoading) {
      return;
    }
    let startDateReport = this.form.get('reportPeriodFrom').value;
    let endDateReport = this.form.get('reportPeriodTo').value;
    let startDateCompare = this.form.get('comparePeriodFrom').value;
    let endDateCompare = this.form.get('comparePeriodTo').value;
    let momentReport = this.form.get('momentReport').value;
    let momentCompare = this.form.get('momentCompare').value;

    if (this.form.get('reportType').value === 'moment') {
      if (this.form.valid) {
        if (formatDate(momentReport, 'dd/MM/yyyy', 'en') === formatDate(momentCompare, 'dd/MM/yyyy', 'en')) {
          this.messageService.warn(this.notificationMessage.momentCompareNoThanMomentReport);
          return;
        }
        momentReport = formatDate(momentReport, 'dd/MM/yyyy', 'en');
        momentCompare = _.isNull(momentCompare) ? null : formatDate(momentCompare, 'dd/MM/yyyy', 'en');
      } else {
        validateAllFormFields(this.form);
        return;
      }
    } else {
      if (this.form.get('period').value === 'monthly') {
        if (
          !_.isNull(startDateCompare) &&
          formatDate(startDateReport, 'MM/yyyy', 'en') === formatDate(startDateCompare, 'MM/yyyy', 'en')
        ) {
          this.messageService.warn(this.notificationMessage.compareFromReportFrom);
          return;
        }
        startDateReport = formatDate(startDateReport, 'MM/yyyy', 'en');
        startDateCompare = _.isNull(startDateCompare) ? null : formatDate(startDateCompare, 'MM/yyyy', 'en');
      } else if (this.form.get('period').value === 'quarterly') {
        const reportPeriodQuarterly = this.form.get('reportPeriodQuarterly').value;
        const reportPeriodYear = this.form.get('reportPeriodYear').value;
        const comparePeriodQuarterly = this.form.get('comparePeriodQuarterly').value;
        const comparePeriodYear = this.form.get('comparePeriodYear').value;

        if (_.isNull(comparePeriodQuarterly) && !_.isNull(comparePeriodYear)) {
          this.messageService.warn(this.notificationMessage.quarterlyValidation);
          return;
        } else if (!_.isNull(comparePeriodQuarterly) && _.isNull(comparePeriodYear)) {
          this.messageService.warn(this.notificationMessage.yearValidation);
          return;
        } else if (reportPeriodQuarterly === comparePeriodQuarterly && reportPeriodYear === comparePeriodYear) {
          this.messageService.warn(this.notificationMessage.compareFromReportFrom);
          return;
        }
        startDateReport = reportPeriodQuarterly + '/' + reportPeriodYear;
        startDateCompare = _.isNull(comparePeriodYear) ? null : comparePeriodQuarterly + '/' + comparePeriodYear;
      } else if (this.form.get('period').value === 'yearly') {
        if (
          !_.isNull(this.form.get('comparePeriodYear').value) &&
          this.form.get('reportPeriodYear').value === this.form.get('comparePeriodYear').value
        ) {
          this.messageService.warn(this.notificationMessage.compareFromReportFrom);
          return;
        }
        startDateReport = this.form.get('reportPeriodYear').value;
        startDateCompare = this.form.get('comparePeriodYear').value;
      } else if (this.form.get('period').value === 'optional') {
        if (moment(endDateReport).isBefore(moment(startDateReport))) {
          this.messageService.warn(this.notificationMessage.monthValidationMoment);
          return;
        }
        if (moment(endDateReport).diff(moment(startDateReport), 'months', true) > this.maxMonth) {
          this.messageService.warn(this.notificationMessage.maxMonthReport);
          return;
        }
        if (moment(endDateCompare).diff(moment(startDateCompare), 'months') > this.maxMonth) {
          this.messageService.warn(this.notificationMessage.maxMonthCompare);
          return;
        }
        if (
          (_.isNull(startDateCompare) && !_.isNull(endDateCompare)) ||
          (!_.isNull(startDateCompare) && _.isNull(endDateCompare))
        ) {
          this.messageService.warn(this.notificationMessage.compareFromValidation);
          return;
        } else if (!_.isNull(startDateCompare) && !_.isNull(endDateCompare)) {
          if (
            formatDate(startDateReport, 'MM/yyyy', 'en') === formatDate(startDateCompare, 'MM/yyyy', 'en') &&
            formatDate(endDateReport, 'MM/yyyy', 'en') === formatDate(endDateCompare, 'MM/yyyy', 'en')
          ) {
            this.messageService.warn(this.notificationMessage.compareFromReportFrom);
            return;
          }
        }
        startDateReport = formatDate(startDateReport, 'MM/yyyy', 'en');
        endDateReport = formatDate(endDateReport, 'MM/yyyy', 'en');
        if (!_.isNull(startDateCompare) && !_.isNull(endDateCompare)) {
          if (moment(endDateCompare).isBefore(moment(startDateCompare))) {
            this.messageService.warn(this.notificationMessage.monthValidationCompare);
            return;
          }
          startDateCompare = formatDate(startDateCompare, 'MM/yyyy', 'en');
          endDateCompare = formatDate(endDateCompare, 'MM/yyyy', 'en');
        }
      }
    }
    if (_.isEmpty(this.termData) && this.form.get('reportBy').value === 'rm') {
      this.messageService.warn(_.get(this.notificationMessage, 'rmValidation'));
      return;
    } else if (_.isEmpty(this.termData) && this.form.get('reportBy').value === 'customer') {
      this.messageService.warn(_.get(this.notificationMessage, 'customerValidation'));
      return;
    } else if (this.termData.length > this.countRm && this.form.get('reportBy').value === 'rm') {
      this.translate.get('notificationMessage.countRmMax', { number: this.countRm }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    } else if (this.termData.length > this.countCustomerMax && this.form.get('reportBy').value === 'customer') {
      this.translate.get('notificationMessage.countCustomer', { number: this.countCustomerMax }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    const allDivision = [];
    if (_.isEmpty(this.form.get('division').value)) {
      this.commonData.listDivision.forEach((item) => {
        if (!_.isEmpty(item.code)) {
          allDivision.push(item.code);
        }
      });
    } else {
      allDivision.push(this.form.get('division').value);
      if (this.form.get('division').value === 'INDIV') {
        allDivision.push('NHS');
      }
    }
    const listData = this.termData?.map((i) => i.code);
    const params = {
      attributeFormat: 'pdf',
      data: this.allRM ? [] : listData,
      division: allDivision,
      currencyType: this.form.get('currencyType').value,
      currencyUnit: this.form.get('currencyUnit').value,
      reportType: this.form.get('reportType').value,
      momentReport,
      momentCompare,
      period: this.form.get('period').value,

      reportPeriodFrom: startDateReport,
      reportPeriodTo: endDateReport,
      comparePeriodFrom: startDateCompare,
      comparePeriodTo: endDateCompare,
      reportBy: this.form.get('reportBy').value,
      reportTypeDetail: this.form.get('reportTypeDetail').value,
      term: this.form.get('term').value,
      rsId:
        this.form.get('reportBy').value === 'customer'
          ? this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`)?.rsId
          : this.objFunction?.rsId,
      scope: Scopes.VIEW,
      allRM: this.allRM,
    };
    if (this.form.get('downloadReport').value === 'EXCEL') {
      params.attributeFormat = 'xlsx';
    }
    this.isLoading = true;
    forkJoin([this.kpiReportApi.reportMobilization(params)]).subscribe(
      ([pdfId]) => {
        const respondSource = new Subject<any>();
        this.kpiReportApi.checkIdReportSuccess(pdfId, respondSource);
        respondSource.asObservable().subscribe((res) => {
          if (res?.error === 'error') {
            this.messageService.error(this.notificationMessage.E001);
          } else if (res?.fileId) {
            this.loadFile(res?.fileId, params);
          } else {
            this.messageService.error(this.notificationMessage.messageOrsReport);
          }
          this.isLoading = false;
          respondSource.unsubscribe();
        });
      },
      () => {
        this.messageService.error(this.notificationMessage.E001);
        this.isLoading = false;
      }
    );
  }

  loadFile(pdfId: string, paramSearch: any) {
    if (this.form.get('downloadReport').value === 'EXCEL') {
      forkJoin([this.kpiReportApi.loadFile(`download/${pdfId}`)]).subscribe(
        ([blobExcel]) => {
          saveAs(blobExcel, `${this.fileName}.xlsx`);
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      this.kpiReportApi.loadFile(`download/${pdfId}`).then(
        (blobPDF) => {
          const url = `${environment.url_endpoint_kpi}/ors-report/hdv`;
          if (this.form.get('downloadReport').value === 'HTML') {
            const modal = this.modalService.open(KpiReportModalComponent, { windowClass: 'tree__report-modal' });
            modal.componentInstance.data = blobPDF;
            modal.componentInstance.model = paramSearch;
            modal.componentInstance.baseUrl = url;
            modal.componentInstance.filename = this.fileName;
            modal.componentInstance.title = 'Báo cáo huy động vốn';
            modal.componentInstance.extendUrl = '';
          } else if (this.form.get('downloadReport').value === 'PDF') {
            saveAs(blobPDF, `${this.fileName}.pdf`);
          }
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    }
  }

  removeList() {
    this.termData = [];
    this.paramsTable.page = 0;
    this.allRM = false;
    this.mapData();
  }

  onChangeCheckAllRM(event) {
    if (event.checked) {
      this.textSearch = '';
      const params = {
        crmIsActive: true,
        scope: Scopes.VIEW,
        rmBlock: this.form.get('division').value,
        rsId: this.objFunction?.rsId,
        employeeCode: this.textSearch,
        page: 0,
        size: maxInt32,
        isNHSReport: this.form.get('division').value === Division.INDIV,
        isReportByRm: this.commonData.isRm,
        rm: true,
        manager: true,
      };
      this.isLoading = true;
      this.rmApi.post('findAll', params).subscribe(
        (data) => {
          this.termData = _.map(_.get(data, 'content'), (item) => {
            return {
              code: item?.t24Employee?.employeeCode,
              name: item?.hrisEmployee?.fullName,
              hrsCode: item?.hrisEmployee?.employeeId,
            };
          });
          this.termData = _.uniqBy(this.termData, (i) => i.hrsCode);
          this.paramsTable.page = 0;
          this.mapData();
          this.isLoading = false;
        },
        (e) => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      this.termData = [];
      this.paramsTable.page = 0;
      this.mapData();
    }
  }

  isShowCheckboxAllRm() {
    return (
      this.currUser?.branch !== BRANCH_HO &&
      !this.commonData?.isRm &&
      this.form.get('division')?.value !== Division.INDIV &&
      this.form.get('reportBy')?.value === 'rm'
    );
  }
}
