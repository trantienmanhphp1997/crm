import { global } from '@angular/compiler/src/util';
import { Component, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import {
  Scopes,
} from 'src/app/core/utils/common-constants';

@Component({
  selector: 'app-query-mbal-detail-item',
  templateUrl: './query-mbal-detail-item.component.html',
  styleUrls: ['./query-mbal-detail-item.component.scss']
})
export class QueryMbalDetailItemComponent extends BaseComponent implements OnInit {
  @Input("data") data: any;
  @Input("queryBy") queryBy: any;
  isLoading = false;
  @ViewChild('table') table: DatatableComponent;
  limit = global.userConfig.pageSize;
  pageable: Pageable;
  params: any = {
    pageNumber: 0,
    pageSize: global?.userConfig?.pageSize,
    rsId: '',
    scope: Scopes.VIEW,
  };

  dataTable: any;
  expandableAreasState = [];
  
  constructor(
    injector: Injector,
  ) { 
    super(injector);
  }

  ngOnInit(): void {
    this.dataTable = this.data.insuranceProductDTO;
    this.pageable = {
      totalElements: 0,
      totalPages: 1,
      currentPage: this.params.pageNumber,
      size: this.limit,
    };
  }

  changeDropdownArrowState(index: number) {
    this.expandableAreasState[index] = !this.expandableAreasState[index];
  }

  setPage(pageInfo) {
    // if (this.isLoading) {
    //   return;
    // }
    // this.params.pageNumber = pageInfo.offset;
    // this.search(false);
  }

  onActive(event) {
  }
}
