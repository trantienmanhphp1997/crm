import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QueryMbalDetailItemComponent } from './query-mbal-detail-item.component';

describe('QueryMbalDetailItemComponent', () => {
  let component: QueryMbalDetailItemComponent;
  let fixture: ComponentFixture<QueryMbalDetailItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueryMbalDetailItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueryMbalDetailItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
