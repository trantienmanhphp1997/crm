import { formatDate } from '@angular/common';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { Component, OnInit, Injector, AfterViewInit, ViewEncapsulation } from '@angular/core';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { TableColumn } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import {
  CommonCategory,
  Division,
  FunctionCode,
  maxInt32,
  Scopes,
  SessionKey,
} from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as moment from 'moment';
import { catchError } from 'rxjs/operators';
import { CustomerApi } from 'src/app/pages/customer-360/apis';
import { KpiReportApi } from '../../apis';
import { KpiReportModalComponent } from '../../dialogs';
import { RmApi } from 'src/app/pages/rm/apis';
import { RmModalComponent } from 'src/app/pages/rm/components/rm-modal/rm-modal.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { saveAs } from 'file-saver';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { environment } from 'src/environments/environment';
import { CustomerModalComponent } from 'src/app/pages/customer-360/components';
import { ChooseBranchesModalComponent } from 'src/app/pages/system/components/choose-branches-modal/choose-branches-modal.component';

@Component({
  selector: 'app-report-customer-management-tttm',
  templateUrl: './report-customer-management-tttm.component.html',
  styleUrls: ['./report-customer-management-tttm.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ReportCustomerManagementTTTMComponent extends BaseComponent implements OnInit, AfterViewInit {
  commonData = {
    listDivision: [],
    maxDate: new Date(),
    listBranch: [],
    minMonth: new Date(moment().add(-14, 'months').toDate()),
    minDate: new Date(moment().add(-90, 'day').toDate()),
  };
  textSearch: string;
  paramsTable = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };
  form = this.fb.group({
    division: '',
    reportBy: 'CN',
    downloadReport: 'HTML',
    businessDate: [new Date(), CustomValidators.required],
    period: 'DAY',
    startDate: [new Date(), CustomValidators.required],
    endDate: [new Date(), CustomValidators.required],
    month: [new Date()],
  });
  dataTerm = {
    branch: {
      pageable: { ...this.pageable },
      term: [],
    },
    customer: {
      pageable: { ...this.pageable },
      term: [],
    },
  };
  tableType: string;
  termData = [];
  listDataTable = [];
  columns: TableColumn[];
  isSearch = false;
  fileName = 'Bao-cao-quan-tri-khach-hang-tttm';
  countRm = 50;
  objFunctionCustomer: any;
  paramSearchDivison = {
    size: maxInt32,
    page: 0,
    name: '',
    code: '',
  };

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private customerApi: CustomerApi,
    private rmApi: RmApi,
    private kpiReportApi: KpiReportApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.objFunctionCustomer = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.tableType = this.form.get('reportBy').value;
    this.columns = [
      {
        name: this.fields.branchCode,
        prop: 'code',
      },
      {
        name: this.fields.branchName,
        prop: 'name',
      },
    ];
    forkJoin([
      this.categoryService.getBranchesOfUser(this.objFunction.rsId, Scopes.VIEW).pipe(catchError(() => of(undefined))),
      this.categoryService.searchBlocksCategory(this.paramSearchDivison).pipe(catchError(() => of(undefined))),
    ]).subscribe(([branchesOfUser, listDivision]) => {
      this.commonData.listBranch = branchesOfUser || [];
      if (_.isEmpty(branchesOfUser)) {
        this.commonData.listBranch?.push({ code: this.currUser.branch, name: this.currUser.branchName });
      }
      this.commonData.listDivision = listDivision.content?.map((item) => {
        return {
          code: item.code,
          name: item.code + ' - ' + item.name,
        };
      });
      this.form.controls.division.setValue(_.first(this.commonData.listDivision)?.code);
      if (this.commonData.listDivision?.length === 1) {
        this.form.controls.division.disable();
      }
      this.isLoading = false;
    });
  }

  ngAfterViewInit() {
    this.form.controls.reportBy.valueChanges.subscribe((value) => {
      if (this.tableType !== value) {
        this.paramsTable.page = 0;
        this.dataTerm[this.tableType] = {
          pageable: { ...this.pageable },
          term: [...this.termData],
        };
        this.textSearch = '';
        if (value === 'CN') {
          this.columns[0].name = this.fields.branchCode;
          this.columns[1].name = this.fields.branchName;
          this.termData = [...this.dataTerm.branch.term];
          this.pageable = { ...this.dataTerm.branch.pageable };
        } else if (value === 'KH') {
          this.columns[0].name = this.fields.customerCode;
          this.columns[1].name = this.fields.customerName;
          this.termData = [...this.dataTerm.customer.term];
          this.pageable = { ...this.dataTerm.customer.pageable };
        }
        this.tableType = value;
        this.mapData();
      }
    });

    this.form.controls.division.valueChanges.subscribe(() => {
      this.removeList();
    });
  }

  search() {
    const reportBy = this.form.get('reportBy').value;
    if (reportBy === 'CN') {
      const modal = this.modalService.open(ChooseBranchesModalComponent, { windowClass: 'tree__branches-modal' });
      modal.componentInstance.listBranchOld = _.map(this.termData, (x) => x.code) || [];
      modal.componentInstance.listBranch = this.commonData.listBranch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData = res;
            this.mapData();
          }
        })
        .catch(() => {});
    } else if (reportBy === 'KH') {
      const formSearch = {
        customerType: {
          value: this.form.get('division').value,
          disabled: true,
        },
      };
      const modal = this.modalService.open(CustomerModalComponent, { windowClass: 'list__customer360-modal' });
      modal.componentInstance.listSelectedOld = this.termData;
      modal.componentInstance.dataSearch = formSearch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData =
              res.listSelected?.map((item) => {
                return {
                  code: item.customerCode,
                  customerCode: item.customerCode,
                  name: item.customerName ? item.customerName : item.name,
                };
              }) || [];
            this.mapData();
          }
        })
        .catch(() => {});
    }
  }

  add() {
    const reportBy = this.form.get('reportBy').value;
    this.textSearch = _.trim(this.textSearch);
    if (this.textSearch.length === 0) {
      this.isSearch = false;
      return;
    }
    if (_.findIndex(this.termData, (i) => i.code?.toUpperCase() === this.textSearch?.toUpperCase()) === -1) {
      if (reportBy === 'CN') {
        const itemResult = _.find(
          this.commonData.listBranch,
          (item) => item.code?.toUpperCase() === this.textSearch?.toUpperCase()
        );
        if (itemResult) {
          this.messageService.success(_.get(this.notificationMessage, 'branchSuccess'));
          this.termData?.push(itemResult);
          this.mapData();
        } else {
          this.messageService.warn(_.get(this.notificationMessage, 'branchNotExist'));
        }
      } else if (reportBy === 'KH') {
        const params = {
          customerCode: this.textSearch,
          customerType: this.form.get('division').value,
          rsId: this.objFunctionCustomer?.rsId,
          scope: Scopes.VIEW,
          pageNumber: 0,
          pageSize: global?.userConfig?.pageSize,
        };
        this.isSearch = true;
        let api: Observable<any>;
        if (this.form.get('division').value !== Division.INDIV) {
          api = this.customerApi.searchSme(params);
        } else {
          api = this.customerApi.search(params);
        }
        api.subscribe(
          (data) => {
            if (data?.length > 0) {
              this.termData?.push({
                code: data[0]?.customerCode,
                name: data[0]?.customerName,
                customerCode: data[0]?.customerCode,
              });
              this.messageService.success(_.get(this.notificationMessage, 'customerSuccess'));
              this.mapData();
            } else {
              this.messageService.warn(_.get(this.notificationMessage, 'customerNotExist'));
            }
            this.isSearch = false;
          },
          (e) => {
            this.messageService.error(this.notificationMessage.E001);
            this.isLoading = false;
            this.isSearch = false;
          }
        );
      }
    } else {
      const messageReport = this.form.get('reportBy').value.toString() + 'IsExist';
      this.messageService.warn(_.get(this.notificationMessage, messageReport));
      this.isSearch = false;
      return;
    }
  }

  setPage(pageInfo) {
    if (!this.isLoading) {
      this.paramsTable.page = pageInfo.offset;
      this.mapData();
    }
  }

  mapData() {
    const total = this.termData.length;
    this.pageable.totalElements = total;
    this.pageable.totalPages = Math.floor(total / this.pageable.size);
    this.pageable.currentPage = this.paramsTable.page;
    const start = this.paramsTable.page * this.paramsTable.size;
    this.listDataTable = this.termData?.slice(start, start + this.paramsTable.size);
  }

  removeRecord(item) {
    _.remove(this.termData, (i) => i.code === item.code);
    _.remove(this.listDataTable, (i) => i.code === item.code);
    if (this.listDataTable.length === 0 && this.pageable.currentPage > 0) {
      this.paramsTable.page -= 1;
    }
    this.listDataTable = [...this.listDataTable];
    this.mapData();
  }

  viewReport() {
    let monthTTTM = moment().diff(moment(this.form.controls.month.value), 'months');
    let startDate = this.form.controls.startDate.value;
    let endDate = this.form.controls.endDate.value;

    if (
      this.form.controls.period.value === 'DAY' &&
      90 > moment(endDate).diff(moment(startDate), 'days') &&
      moment(endDate).diff(moment(startDate), 'days') > 30
    ) {
      this.messageService.warn('Chỉ cho phép xem dữ liệu trong khoảng thời gian 30 ngày với dữ liệu 90 ngày gần nhất');
      return;
    }
    if (this.form.controls.period.value === 'MONTH' && 13 > monthTTTM && monthTTTM > 3) {
      this.messageService.warn('Chỉ cho phép xem dữ liệu trong khoảng thời gian 3 tháng với dữ liệu 13 tháng gần nhất');
      return;
    }

    if (this.isLoading || this.isSearch) {
      return;
    }
    if (this.form.valid) {
      if (_.isEmpty(this.termData) && this.form.get('reportBy').value === 'CN') {
        this.messageService.warn(_.get(this.notificationMessage, 'branchValidation'));
        return;
      } else if (_.isEmpty(this.termData) && this.form.get('reportBy').value === 'KH') {
        this.messageService.warn(_.get(this.notificationMessage, 'customerValidation'));
        return;
      }

      const dataTable = this.termData?.map((i) => i.code);
      const params = {
        attributeFormat: 'pdf',
        division: this.form.controls.division.value,
        reportType: this.form.controls.reportBy.value,
        data: dataTable,
        period: this.form.controls.period.value,
        rsId: this.form.get('reportBy').value === 'CN' ? this.objFunction.rsId : this.objFunctionCustomer.rsId,
        scope: Scopes.VIEW,
        fromDate: formatDate(startDate, 'dd-MM-yyyy', 'en'),
        toDate: formatDate(endDate, 'dd-MM-yyyy', 'en'),
        monthReport: formatDate(this.form.controls.month.value, 'MM/yyyy', 'en'),
      };

      if (this.form.get('downloadReport').value === 'EXCEL') {
        params.attributeFormat = 'xlsx';
      }
      this.isLoading = true;
      forkJoin([this.kpiReportApi.reportManagementTTTM(params)]).subscribe(
        ([pdfId]) => {
          const respondSource = new Subject<any>();
          this.kpiReportApi.checkIdReportSuccess(pdfId, respondSource);
          respondSource.asObservable().subscribe((res) => {
            if (res?.error === 'error') {
              this.messageService.error(this.notificationMessage.E001);
            } else if (res?.fileId) {
              this.loadFile(res?.fileId, params);
            } else {
              this.messageService.error(this.notificationMessage.messageOrsReport);
            }
            this.isLoading = false;
            respondSource.unsubscribe();
          });
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      validateAllFormFields(this.form);
      return;
    }
  }

  loadFile(pdfId: string, paramSearch: any) {
    if (this.form.get('downloadReport').value === 'EXCEL') {
      forkJoin([this.kpiReportApi.loadFile(`download/${pdfId}`)]).subscribe(
        ([blobExcel]) => {
          saveAs(blobExcel, `${this.fileName}.xlsx`);
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      this.kpiReportApi.loadFile(`download/${pdfId}`).then(
        (blobPDF) => {
          const url = `${environment.url_endpoint_kpi}/ors-report/admin-tttm`;
          if (this.form.get('downloadReport').value === 'HTML') {
            const modal = this.modalService.open(KpiReportModalComponent, { windowClass: 'tree__report-modal' });
            modal.componentInstance.data = blobPDF;
            modal.componentInstance.model = paramSearch;
            modal.componentInstance.baseUrl = url;
            modal.componentInstance.filename = this.fileName;
            modal.componentInstance.title = 'Báo cáo quản trị Khách hàng TTTM';
            modal.componentInstance.extendUrl = '';
          } else if (this.form.get('downloadReport').value === 'PDF') {
            saveAs(blobPDF, `${this.fileName}.pdf`);
          }
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    }
  }

  removeList() {
    this.termData = [];
    this.paramsTable.page = 0;
    this.mapData();
  }
}
