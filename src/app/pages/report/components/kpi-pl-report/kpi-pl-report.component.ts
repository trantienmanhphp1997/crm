import { forkJoin, Observable, of, Subject } from 'rxjs';
import { Component, OnInit, Injector, AfterViewInit } from '@angular/core';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { TableColumn } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import { CommonCategory, Division, FunctionCode, Scopes, SessionKey } from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as moment from 'moment';
import { catchError } from 'rxjs/operators';
import { RmApi } from 'src/app/pages/rm/apis';
import { CustomerApi } from 'src/app/pages/customer-360/apis';
import { ChooseBranchesModalComponent } from 'src/app/pages/system/components/choose-branches-modal/choose-branches-modal.component';
import { RmModalComponent } from 'src/app/pages/rm/components';
import { CustomerModalComponent } from 'src/app/pages/customer-360/components';
import { KpiReportApi } from '../../apis';
import { KpiReportModalComponent } from '../../dialogs';
import { formatDate } from '@angular/common';
import { saveAs } from 'file-saver';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { maxInt32 } from 'src/app/core/utils/common-constants';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-kpi-pl-report',
  templateUrl: './kpi-pl-report.component.html',
  styleUrls: ['./kpi-pl-report.component.scss'],
})
export class KpiPlReportComponent extends BaseComponent implements OnInit, AfterViewInit {
  paramDTTTRR = [
    {
      period: 'DAILY',
      division: Division.SME,
      code: 'BAOCAO_PL_KH_FILTER_DAILY_SME',
      data: [],
    },
    {
      period: 'LUYKE',
      division: Division.INDIV,
      code: 'BAOCAO_PL_KH_FILTER_LUYKE_INDIV',
      data: [],
    },
    {
      period: 'LUYKE',
      division: Division.SME,
      code: 'BAOCAO_PL_KH_FILTER_LUYKE_SME',
      data: [],
    },
    {
      period: 'MONTHLY',
      division: Division.INDIV,
      code: 'BAOCAO_PL_KH_FILTER_MONTHLY_INDIV',
      data: [],
    },
    {
      period: 'MONTHLY',
      division: Division.SME,
      code: 'BAOCAO_PL_KH_FILTER_MONTHLY_SME',
      data: [],
    },
    {
      period: 'OTHER',
      division: 'OTHER',
      code: 'BAOCAO_PL_KH_FILTER_OTHER',
      data: [],
    },
  ];
  divisionConfig = [Division.CIB, Division.DVC, Division.INDIV, Division.SME];
  commonData = {
    listDivision: [],
    listCurrencyUnit: [],
    listDTTTRR: [],
    listBranch: [],
    minDate: null,
    maxDate: new Date(),
    maxDateDisable: new Date(),
    minDateMonth: null,
    isRm: false,
  };
  textSearch: string;
  paramsTable = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };
  form = this.fb.group({
    division: '',
    currencyUnit: '',
    reportType: 'branch',
    period: 'MONTHLY',
    startDate: [new Date(moment().startOf('month').toDate()), CustomValidators.required],
    endDate: [new Date(moment().add(-1, 'day').toDate()), CustomValidators.required],
    month: new Date(),
    dtttrr: '',
    downloadReport: 'HTML',
  });
  dataTerm = {
    branch: {
      pageable: { ...this.pageable },
      term: [],
    },
    rm: {
      pageable: { ...this.pageable },
      term: [],
    },
    customer: {
      pageable: { ...this.pageable },
      term: [],
    },
  };
  tableType: string;
  termData = [];
  listDataTable = [];
  columns: TableColumn[];
  isSearch = false;
  objFunctionCustomer: any;
  fileName = 'bao-cao-pl';
  objFunctionRM: any;
  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private rmApi: RmApi,
    private customerApi: CustomerApi,
    private kpiReportApi: KpiReportApi
  ) {
    super(injector);
    this.objFunctionCustomer = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.objFunctionRM = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.checkDayIsLastMonth();
    this.tableType = this.form.get('reportType').value;
    this.form.get('month').setValue(new Date(moment().year(), moment().month() - 1));
    this.columns = [
      {
        name: this.fields.branchCode,
        prop: 'code',
      },
      {
        name: this.fields.branchName,
        prop: 'name',
      },
    ];
    this.prop = this.sessionService.getSessionData(SessionKey.PL_REPORTS);
    if (!this.prop) {
      const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
      divisionOfUser?.forEach((item) => {
        if (this.divisionConfig?.includes(item.code)) {
          this.commonData.listDivision.push({ code: item.code, name: `${item.code || ''} - ${item.name || ''}` });
        }
      });
      const rsIdOfRm = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`)?.rsId;
      forkJoin([
        this.commonService
          .getCommonCategory(CommonCategory.CURRENCY_UNIT_REPORTS)
          .pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.REPORTS_YEARS).pipe(catchError(() => of(undefined))),
        this.categoryService.getBranchesOfUser(rsIdOfRm, Scopes.VIEW).pipe(catchError(() => of(undefined))),
      ]).subscribe(([currencyUnits, configYear, branchesOfUser]) => {
        this.commonData.listBranch = branchesOfUser || [];
        this.commonData.isRm = _.isEmpty(branchesOfUser);
        this.commonData.listCurrencyUnit = _.orderBy(_.get(currencyUnits, 'content'), ['orderNum'], ['asc', 'desc']);
        this.commonData.listCurrencyUnit =
          _.get(currencyUnits, 'content')?.map((item) => {
            return { code: item.value, name: item.name, isDefault: item.isDefault };
          }) || [];
        const yearConfig = _.find(_.get(configYear, 'content'), (item) => item.code === 'PL')?.value || 1;
        this.commonData.minDateMonth = new Date(
          moment()
            .add(-(yearConfig - 1), 'year')
            .startOf('year')
            .valueOf()
        );
        this.sessionService.setSessionData(SessionKey.PL_REPORTS, this.commonData);
        this.form.get('currencyUnit').setValue(_.first(this.commonData.listCurrencyUnit)?.code);
        this.commonData.listCurrencyUnit.forEach((item) => {
          if (item.isDefault) {
            this.form.get('currencyUnit').setValue(item?.code);
          }
        });
        this.form.get('division').setValue(_.first(this.commonData.listDivision)?.code);
        this.isLoading = false;
      });
    } else {
      this.commonData = this.prop;
      this.form.get('currencyUnit').setValue(_.first(this.commonData.listCurrencyUnit)?.code);
      this.commonData.listCurrencyUnit.forEach((item) => {
        if (item.isDefault) {
          this.form.get('currencyUnit').setValue(item?.code);
        }
      });
      this.form.get('division').setValue(_.first(this.commonData.listDivision)?.code);
      this.isLoading = false;
    }
  }

  ngAfterViewInit() {
    this.form.controls.reportType.valueChanges.subscribe((value) => {
      if (value !== 'customer' && this.form.get('period').value === 'LUYKE') {
        this.form.get('period').setValue('MONTHLY');
      }
      if (this.tableType !== value) {
        this.getDtttrrData();
        this.paramsTable.page = 0;
        this.dataTerm[this.tableType] = {
          pageable: { ...this.pageable },
          term: [...this.termData],
        };
        this.textSearch = '';
        if (value === 'branch') {
          this.columns[0].name = this.fields.branchCode;
          this.columns[1].name = this.fields.branchName;
          this.termData = [...this.dataTerm.branch.term];
          this.pageable = { ...this.dataTerm.branch.pageable };
        } else if (value === 'rm') {
          this.columns[0].name = this.fields.rmCode;
          this.columns[1].name = this.fields.rmName;
          this.termData = [...this.dataTerm.rm.term];
          this.pageable = { ...this.dataTerm.rm.pageable };
          if (this.commonData.isRm) {
            this.textSearch = this.currUser.code;
            if (this.termData.length === 0 && this.currUser.code) {
              this.add();
            }
          } else {
            this.textSearch = '';
          }
        } else if (value === 'customer') {
          this.columns[0].name = this.fields.customerCode;
          this.columns[1].name = this.fields.customerName;
          this.termData = [...this.dataTerm.customer.term];
          this.pageable = { ...this.dataTerm.customer.pageable };
        }
        this.tableType = value;
        this.mapData();
      }
    });
    this.form.controls.division.valueChanges.subscribe(() => {
      this.removeList();
      this.getDtttrrData();
    });
    this.form.controls.period.valueChanges.subscribe((value) => {
      this.getDtttrrData();
    });
  }

  checkDayIsLastMonth() {
    if (moment().date() === 1) {
      const dayNow = moment().toDate();
      this.form.get('startDate').setValue(new Date(dayNow.getFullYear(), dayNow.getMonth() - 1, 1));
      this.form.get('endDate').setValue(new Date(dayNow.getFullYear(), dayNow.getMonth(), 0));
    }
  }

  getDtttrrData() {
    if (this.form.get('reportType').value === 'customer') {
      const division = this.form.get('division').value;
      const period = this.form.get('period').value;
      const itemDTTTRR =
        _.find(this.paramDTTTRR, (i) => i.division === division && i.period === period) || _.last(this.paramDTTTRR);
      if (itemDTTTRR?.data?.length > 0) {
        this.commonData.listDTTTRR = itemDTTTRR.data;
        this.form.get('dtttrr').setValue(_.first(this.commonData.listDTTTRR)?.value);
      } else {
        this.commonService
          .getCommonCategory(itemDTTTRR.code)
          .pipe(catchError(() => of(undefined)))
          .subscribe((data) => {
            this.commonData.listDTTTRR = _.get(data, 'content') || [];
            itemDTTTRR.data = this.commonData.listDTTTRR;
            this.form.get('dtttrr').setValue(_.first(this.commonData.listDTTTRR)?.value);
          });
      }
    }
  }

  search() {
    const reportType = this.form.get('reportType').value;
    if (reportType === 'branch') {
      const modal = this.modalService.open(ChooseBranchesModalComponent, { windowClass: 'tree__branches-modal' });
      modal.componentInstance.listBranchOld = _.map(this.termData, (x) => x.code) || [];
      modal.componentInstance.listBranch = this.commonData.listBranch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData = res;
            this.mapData();
          }
        })
        .catch(() => {});
    } else if (reportType === 'rm') {
      const formSearch = {
        crmIsActive: { value: 'true', disabled: true },
        rmBlock: {
          value: this.form.get('division').value,
          disabled: true,
        },
        rm: true,
      };
      const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
      modal.componentInstance.dataSearch = formSearch;
      modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
      modal.componentInstance.isManager = true;
      modal.result
        .then((res) => {
          if (res) {
            const listRMSelect = res.listSelected?.map((item) => {
              return {
                hrsCode: item?.hrisEmployee?.employeeId,
                code: item?.t24Employee?.employeeCode,
                name: item?.hrisEmployee?.fullName,
              };
            });
            this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
            this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);
            this.mapData();
          }
        })
        .catch(() => {});
    } else if (reportType === 'customer') {
      const formSearch = {
        customerType: {
          value: this.form.get('division').value,
          disabled: true,
        },
      };
      const modal = this.modalService.open(CustomerModalComponent, { windowClass: 'list__customer360-modal' });
      modal.componentInstance.listSelectedOld = this.termData;
      modal.componentInstance.dataSearch = formSearch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData =
              res.listSelected?.map((item) => {
                return {
                  code: item.customerCode,
                  customerCode: item.customerCode,
                  name: item.customerName ? item.customerName : item.name,
                };
              }) || [];
            this.mapData();
          }
        })
        .catch(() => {});
    }
  }

  add() {
    const reportType = this.form.get('reportType').value;
    this.textSearch = _.trim(this.textSearch);
    if (this.textSearch.length === 0) {
      this.isSearch = false;
      return;
    }
    if (_.findIndex(this.termData, (i) => i.code?.toUpperCase() === this.textSearch?.toUpperCase()) === -1) {
      if (reportType === 'branch') {
        const itemResult = _.find(
          this.commonData.listBranch,
          (item) => item.code?.toUpperCase() === this.textSearch?.toUpperCase()
        );
        if (itemResult) {
          this.termData?.push(itemResult);
          this.mapData();
        } else {
          this.messageService.warn(_.get(this.notificationMessage, 'branchNotExist'));
        }
      } else if (reportType === 'rm') {
        const params = {
          manager: true,
          rm: true,
          crmIsActive: true,
          scope: Scopes.VIEW,
          rmBlock: this.form.get('division').value,
          rsId: this.objFunctionRM?.rsId,
          employeeCode: this.textSearch,
          page: 0,
          size: maxInt32,
        };
        this.isSearch = true;
        this.rmApi
          .post('findAll', params)
          .pipe(catchError(() => of(undefined)))
          .subscribe((data) => {
            const itemResult = _.find(
              _.get(data, 'content'),
              (item) => item?.t24Employee?.employeeCode?.toUpperCase() === this.textSearch?.toUpperCase()
            );
            if (itemResult) {
              this.termData?.push({
                code: itemResult?.t24Employee?.employeeCode,
                name: itemResult?.hrisEmployee?.fullName,
                hrsCode: itemResult?.hrisEmployee?.employeeId,
              });
              this.mapData();
            } else {
              this.messageService.warn(_.get(this.notificationMessage, 'rmNotExistOrNotBranh'));
            }
            this.isSearch = false;
          });
      } else if (reportType === 'customer') {
        const params = {
          customerCode: this.textSearch,
          customerType: this.form.get('division').value,
          rsId: this.objFunctionCustomer?.rsId,
          scope: Scopes.VIEW,
          pageNumber: 0,
          pageSize: global?.userConfig?.pageSize,
        };
        this.isSearch = true;
        let api: Observable<any>;
        if (this.form.get('division').value !== Division.INDIV) {
          api = this.customerApi.searchSme(params);
        } else {
          api = this.customerApi.search(params);
        }
        api.subscribe((data) => {
          if (data?.length > 0) {
            this.termData?.push({
              code: data[0]?.customerCode,
              name: data[0]?.customerName,
              customerCode: data[0]?.customerCode,
            });
            this.mapData();
          } else {
            this.messageService.warn(_.get(this.notificationMessage, 'customerNotExist'));
          }
          this.isSearch = false;
        });
      }
    } else {
      const messageReport = this.form.get('reportType').value.toString() + 'IsExist';
      this.messageService.warn(_.get(this.notificationMessage, messageReport));
      this.isSearch = false;
      return;
    }
  }

  setPage(pageInfo) {
    this.paramsTable.page = pageInfo.offset;
    this.mapData();
  }

  mapData() {
    const total = this.termData.length;
    this.pageable.totalElements = total;
    this.pageable.totalPages = Math.floor(total / this.pageable.size);
    this.pageable.currentPage = this.paramsTable.page;
    const start = this.paramsTable.page * this.paramsTable.size;
    this.listDataTable = this.termData?.slice(start, start + this.paramsTable.size);
  }

  removeRecord(item) {
    _.remove(this.termData, (i) => i.code === item.code);
    _.remove(this.listDataTable, (i) => i.code === item.code);
    if (this.listDataTable.length === 0 && this.pageable.currentPage > 0) {
      this.paramsTable.page -= 1;
    }
    this.listDataTable = [...this.listDataTable];
    this.mapData();
  }

  viewReport() {
    const dataForm = this.form.getRawValue();
    const start = moment(this.form.get('startDate').value);
    const end = moment(this.form.get('endDate').value);
    if (this.form.valid) {
      if (end.isBefore(start)) {
        this.messageService.warn(this.notificationMessage.startDateMoreThanEndDatePl);
        return;
      }
      if (end.isAfter(moment()) && dataForm.period === 'DAILY') {
        this.messageService.warn(this.notificationMessage.formDateMoreThanNow);
        return;
      }

      let reportTypeValue;
      if (dataForm.reportType === 'branch') {
        if (this.termData.length > 10) {
          this.messageService.warn(this.notificationMessage.countBranchPl);
          return;
        }
        if (this.commonData.isRm) {
          this.messageService.warn(_.get(this.notificationMessage, 'branchValidation'));
          return;
        }
        reportTypeValue = '1';
      } else if (dataForm.reportType === 'rm') {
        if (_.isEmpty(this.termData)) {
          this.messageService.warn(_.get(this.notificationMessage, 'rmValidation'));
          return;
        }
        if (this.termData.length > 100) {
          this.messageService.warn(this.notificationMessage.countRm);
          return;
        }
        reportTypeValue = '2';
      } else {
        reportTypeValue = '3';
        if (_.isEmpty(this.termData)) {
          this.messageService.warn(_.get(this.notificationMessage, 'customerValidation'));
          return;
        }
        if (this.termData.length > 100) {
          this.messageService.warn(this.notificationMessage.countCustomer);
          return;
        }
      }
      const customerCodes = this.termData?.map((i) => i.code);
      let periodValue;
      let startDate = '';
      let endDate = '';
      let monthReport = '';
      if (this.form.get('period').value === 'DAILY') {
        periodValue = '1';
        startDate = formatDate(this.form.get('startDate').value, 'dd/MM/yyyy', 'en');
        endDate = formatDate(this.form.get('endDate').value, 'dd/MM/yyyy', 'en');
      } else if (this.form.get('period').value === 'MONTHLY') {
        periodValue = '2';
        monthReport = formatDate(this.form.get('month').value, 'MM/yyyy', 'en');
      } else {
        monthReport = formatDate(this.form.get('month').value, 'MM/yyyy', 'en');
        periodValue = '3';
      }

      const params = {
        attributeFormat: 'pdf',
        blockCode: this.form.get('division').value,
        unitCurrency: this.form.get('currencyUnit').value,
        reportType: reportTypeValue,
        data: customerCodes,
        termReport: periodValue,
        dtttrrCommonValue: this.form.get('dtttrr').value,
        rsId: reportTypeValue === '3' ? this.objFunctionCustomer?.rsId : this.objFunctionRM?.rsId,
        scope: Scopes.VIEW,
        frDate: startDate,
        toDate: endDate,
        monthReport,
      };
      this.isLoading = true;
      if (this.form.get('downloadReport').value === 'EXCEL') {
        params.attributeFormat = 'xlsx';
      }

      forkJoin([this.kpiReportApi.reportPL(params)]).subscribe(
        ([pdfId]) => {
          const respondSource = new Subject<any>();
          this.kpiReportApi.checkIdReportSuccess(pdfId, respondSource);
          respondSource.asObservable().subscribe((res) => {
            if (res?.error === 'error') {
              this.messageService.error(this.notificationMessage.E001);
            } else if (res?.fileId) {
              this.loadFile(res?.fileId, params);
            } else {
              this.messageService.error(this.notificationMessage.messageOrsReport);
            }
            this.isLoading = false;
            respondSource.unsubscribe();
          });
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      validateAllFormFields(this.form);
    }
  }

  loadFile(pdfId: string, paramSearch: any) {
    if (this.form.get('downloadReport').value === 'EXCEL') {
      forkJoin([this.kpiReportApi.loadFile(`download/${pdfId}`)]).subscribe(
        ([blobExcel]) => {
          saveAs(blobExcel, `${this.fileName}.xlsx`);
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      this.kpiReportApi.loadFile(`download/${pdfId}`).then(
        (blobPDF) => {
          const url = `${environment.url_endpoint_kpi}/ors-report/pl`;
          if (this.form.get('downloadReport').value === 'HTML') {
            const modal = this.modalService.open(KpiReportModalComponent, { windowClass: 'tree__report-modal' });
            modal.componentInstance.data = blobPDF;
            modal.componentInstance.model = paramSearch;
            modal.componentInstance.baseUrl = url;
            modal.componentInstance.filename = this.fileName;
            modal.componentInstance.title = 'Báo cáo P/L';
            modal.componentInstance.extendUrl = '';
          } else if (this.form.get('downloadReport').value === 'PDF') {
            saveAs(blobPDF, `${this.fileName}.pdf`);
          }
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    }
  }

  removeList() {
    this.termData = [];
    this.paramsTable.page = 0;
    this.mapData();
  }

  closeStartDate($event) {
    this.commonData.minDate = new Date($event);
    const smonth = moment(this.form.get('startDate').value).month();
    const year = moment(this.form.get('startDate').value).year();
    this.form.get('endDate').setValue(new Date(year, smonth + 1, 0));
    this.form.get('startDate').setValue(new Date(year, smonth, 1));
    this.ref.detectChanges();
  }

  closeEvent($event) {
    if (!_.isNull(this.form.get('startDate').value)) {
      const countDay = moment(this.form.get('endDate').value).diff(moment(this.form.get('startDate').value), 'days');
      const startMonth = moment(this.form.get('startDate').value).month();
      const year = moment(this.form.get('startDate').value).year();
      const endMonth = moment(this.form.get('endDate').value).month();
      if (endMonth !== startMonth) {
        this.form.get('endDate').setValue(new Date(this.form.get('endDate').value));
        if (countDay > 31) {
          this.form.get('endDate').setValue(new Date(year, startMonth + 1, 0));
          this.messageService.warn(this.notificationMessage.noMoreThan31Days);
        } else if (countDay < 31) {
          this.form.get('endDate').setValue(new Date(year, startMonth + 1, 0));
          this.messageService.warn(this.notificationMessage.dayInMonth);
        }
      }
      this.ref.detectChanges();
    }
  }
}
