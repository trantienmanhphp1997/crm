import { global } from '@angular/compiler/src/util';
import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import _ from 'lodash';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { FileService } from 'src/app/core/services/file.service';
import {
  CommonCategory,
  Division,
  FunctionCode,
  maxInt32,
  Scopes
} from 'src/app/core/utils/common-constants';
import { CustomerApi } from 'src/app/pages/customer-360/apis/customer.api';

@Component({
  selector: 'app-query-mbal-detail',
  templateUrl: './query-mbal-detail.component.html',
  styleUrls: ['./query-mbal-detail.component.scss']
})
export class QueryMbalDetailComponent extends BaseComponent implements OnInit {
  isLoading = false;
  @ViewChild('table') table: DatatableComponent;
  @ViewChild('tableTarget') tableTarget: DatatableComponent;
  limit = global.userConfig.pageSize;
  pageable: Pageable;
  params: any = {
    pageNumber: 0,
    pageSize: global?.userConfig?.pageSize,
    rsId: '',
    scope: Scopes.VIEW,
  };
  objFunctionRm: any;

  accordianData = [];

  paramDetail = {
    alPolicyHolderId: '',
    alPolicyHolderName: '',
    alPolicyNumber: '',
    dateFrom: '',
    dateTo: '',
    listBranch: null,
    listSaleChannel: null,
    listStatus: null,
    mbReferalId: '',
    branch: '',
    queryBy: '',
    rsId: '',
    scope: '',
  };
  queryBy = '';

  textFooter = '';

  constructor(
    injector: Injector,
    private customerApi: CustomerApi,
    private fileService: FileService
  ) {
    super(injector);
    //get params from list page
    this.getParamsFromListPage();
  }
  
  ngOnInit() {
    this.pageable = {
      totalElements: 0,
      totalPages: 1,
      currentPage: this.params.pageNumber,
      size: this.limit,
    };
    this.getDetails();
  }

  getParamsFromListPage() {
    this.paramDetail.alPolicyHolderId = _.get(this.route.snapshot.queryParams, 'alPolicyHolderId');
    this.paramDetail.alPolicyHolderName = _.get(this.route.snapshot.queryParams, 'alPolicyHolderName');
    this.paramDetail.alPolicyNumber = _.get(this.route.snapshot.queryParams, 'alPolicyNumber');
    this.paramDetail.dateFrom = _.get(this.route.snapshot.queryParams, 'dateFrom');
    this.paramDetail.dateTo = _.get(this.route.snapshot.queryParams, 'dateTo');
    this.paramDetail.listBranch = _.get(this.route.snapshot.queryParams, 'listBranch');
    this.paramDetail.listSaleChannel = _.get(this.route.snapshot.queryParams, 'listSaleChannel');
    this.paramDetail.listStatus = _.get(this.route.snapshot.queryParams, 'listStatus');
    this.paramDetail.mbReferalId = _.get(this.route.snapshot.queryParams, 'mbReferalId');
    if (this.paramDetail.mbReferalId === 'null') {
      this.paramDetail.branch = _.get(this.route.snapshot.queryParams, 'branch');
    }
    this.paramDetail.queryBy = _.get(this.route.snapshot.queryParams, 'queryBy');
    this.queryBy = _.get(this.route.snapshot.queryParams, 'queryBy');
    this.paramDetail.rsId = _.get(this.route.snapshot.queryParams, 'rsId');
    this.paramDetail.scope = _.get(this.route.snapshot.queryParams, 'scope');
    console.log(this.paramDetail);
  }

  getDetails() {
    this.isLoading = true;
    this.customerApi.insuranceLife(this.paramDetail).subscribe((result) => {
      if (result) {
        result.content.forEach((item, index)=> {
          item.id = index;
          this.accordianData.push(item);
        });

        let dateStr = this.reFormatDateString(result.dsnapshotDt);
        if (dateStr) {
          this.setTextFooter(dateStr);
        }

        this.pageable = {
          totalElements: result.totalElements,
          totalPages: result.totalPages,
          currentPage: this.params.pageNumber,
          size: this.limit,
        };
      }
      // console.log(this.accordianData);
      this.isLoading = false;
    });
  }

  reFormatDateString(dateStr: string) {
    let result = '---';
    let arr = [];
    if (dateStr) {
      arr = dateStr.split('-');
      result = arr[2] + '-' + arr[1] + '-' + arr[0];
    }
    return result;
  }

  setTextFooter(date: string) {
    this.translate.get('queryMBAL.textFooter', { date }).subscribe((text) => {
      this.textFooter = text;
    });
  }

  ngAfterViewInit() {
    
  }

  convertDateToString(dateString) {
    let date = new Date(dateString);
    return (
      date.getFullYear() +
      '-' +
      (date.getMonth() > 8 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1)) +
      '-' +
      (date.getDate() > 9 ? date.getDate() : '0' + date.getDate())
    );
  }

  exportFile() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+this.pageable?.totalElements <= 0) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.isLoading = true;
    let paramExport = this.paramDetail;
    paramExport.dateFrom = this.convertDateToString(paramExport.dateFrom);
    paramExport.dateTo = this.convertDateToString(paramExport.dateTo);
    // console.log(paramExport);
    this.customerApi.excelDetailQueryMBAL(paramExport).subscribe(
      (res) => {
        if (res) {
          this.download(res);
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(_.get(this.notificationMessage, 'export_error'));
        this.isLoading = false;
      }
    );
  }

  download(fileId: string) {
    let todayExcel = new Date();
    let queryBy = this.queryBy === 'rm' ? 'RM' : 'HD';
    let titleExcel =
      'HDBH_chi_tiet_theo_' +
      queryBy +
      '_' +
      todayExcel.getDate().toString() +
      (todayExcel.getMonth() + 1).toString() +
      todayExcel.getFullYear().toString() +
      '.xlsx';
    this.fileService.downloadFile(fileId, titleExcel).subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }
}
