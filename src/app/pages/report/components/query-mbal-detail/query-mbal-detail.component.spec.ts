import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QueryMbalDetailComponent } from './query-mbal-detail.component';

describe('QueryMbalDetailComponent', () => {
  let component: QueryMbalDetailComponent;
  let fixture: ComponentFixture<QueryMbalDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueryMbalDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueryMbalDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
