import { forkJoin, of } from 'rxjs';
import { Component, OnInit, Injector, AfterViewInit } from '@angular/core';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { TableColumn } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import {
  BRANCH_HO,
  CommonCategory,
  Division,
  FunctionCode,
  Scopes,
  SessionKey,
} from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as moment from 'moment';
import { catchError, finalize } from 'rxjs/operators';
import { KpiReportApi } from '../../apis';
import { KpiReportModalComponent } from '../../dialogs';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { DashboardService } from 'src/app/pages/dashboard/services/dashboard.service';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { formatDate } from '@angular/common';
import { isInteger } from '@ng-bootstrap/ng-bootstrap/util/util';

@Component({
  selector: 'app-customer-inactive',
  templateUrl: './customer-inactive.component.html',
  styleUrls: ['./customer-inactive.component.scss']
})
export class ReportCustomerInactiveComponent extends BaseComponent implements OnInit {
  commonData = {
    minDate: null,
    maxDate: new Date(),
    isRm: true,
    listBranch: [],
    listCustomerType: [],
  };
  textSearch: string;
  paramsTable = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };

  form = this.fb.group({
    momentReportMonth: [new Date(moment().add(-1, 'month').toDate()), CustomValidators.required],
    period: 'monthly',
    reportBy: 'branch',
    branchCodes: ['ALL'],
    customerType: ''
  });
  allRM = false;
  termData = [];
  listDataTable = [];
  columns: TableColumn[];
  maxDate = new Date();
  fileName = '';
  countRm: any;
  countBranchMax: number;
  countCustomerMax: number;
  maxMonth: any;
  regions = {};
  allBranchLv2 = [];
  allBranchLv1 = [];
  listBranchTerm = [];
  branchLv1List = [];
  branchLv2List = [];
  lenghtBranchesOfUser: number;
  defaultDivision = Division.SME;
  isHO = false;
  objFunctionReport: any;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private kpiReportApi: KpiReportApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.objFunctionReport = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REPORT_CUSTOMER_INACTIVE}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.columns = [
      {
        name: this.fields.rmCode,
        prop: 'code',
      },
      {
        name: this.fields.rmName,
        prop: 'name',
      },
    ];

    this.isHO = this.currUser?.branch === BRANCH_HO;

    forkJoin([
      this.categoryService
        .searchBranches({page: 0, size: 2000})
        .pipe(catchError(() => of(undefined))),

      this.commonService.getCommonCategory(CommonCategory.REPORTS_YEARS).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.CUSTOMER_INACTIVE_CUSTOMER_TYPE).pipe(catchError(() => of(undefined))),

    ]).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(([branchs, configYear, customerType]) => {

      this.commonData.listBranch = _.get(branchs, 'content') || [];
      this.lenghtBranchesOfUser = branchs.length;

      const yearConfig = _.find(_.get(configYear, 'content'), (item) => item.code === 'LIMIT_YEAR')?.value || 2;
      this.commonData.minDate = new Date(
        moment()
          .add(-(yearConfig - 1), 'year')
          .startOf('year')
          .valueOf()
      );
      this.commonData.listCustomerType = _.get(customerType, 'content') || [];
      this.form.controls.customerType.setValue(_.head(this.commonData.listCustomerType)?.code);
      this.branchLv2List = this.commonData.listBranch;
      this.branchLv2List.unshift({ code: 'ALL', name: 'Tất cả' });
    });

    this.textSearch = this.currUser.code;
  }

  setPage(pageInfo) {
    this.paramsTable.page = pageInfo.offset;
    this.mapData();
  }

  mapData() {
    const total = this.termData.length;
    this.pageable.totalElements = total;
    this.pageable.totalPages = Math.floor(total / this.pageable.size);
    this.pageable.currentPage = this.paramsTable.page;
    const start = this.paramsTable.page * this.paramsTable.size;
    this.listDataTable = this.termData?.slice(start, start + this.paramsTable.size);
  }

  removeRecord(item) {
    _.remove(this.termData, (i) => i.code === item.code);
    _.remove(this.listDataTable, (i) => i.code === item.code);
    if (this.listDataTable.length === 0 && this.pageable.currentPage > 0) {
      this.paramsTable.page -= 1;
    }
    this.listDataTable = [...this.listDataTable];
    this.allRM = false;
    this.mapData();
  }

  viewReport() {
    if (this.isLoading) {
      return;
    }

    if (!this.form.valid) {
      validateAllFormFields(this.form);
      return;
    }

    const formData = this.form.getRawValue();
    const { momentReportMonth, customerType } = formData;

    const base = [
      { name: "p_rm_code", value: 'ALL' },
      { name: "p_customer_code", value: 'ALL' },
      { name: "p_rgon", value: 'ALL' },
      { name: "p_piority_sell", value: 'ALL' },
      { name: "p_fr_m", value: formatDate(momentReportMonth, 'MM/yyyy', 'en') },
      { name: "p_br_code_lv2", value:  formData.branchCodes },
      { name: "p_customer_seg", value: this.commonData.listCustomerType.find((item => item.code === customerType))?.value },
    ];

    let params = {
      reportCode: 'REPORT_CUSTOMER_INACTIVE',
      reportParams: [
        ...base
      ],
      rsId: this.objFunctionReport?.rsId,
      scope: Scopes.VIEW
    };

    this.isLoading = true;
    this.kpiReportApi.getTableauReport(params).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(embeddedLink => {
      if (embeddedLink.length > 0) {
        this.loadReport(embeddedLink, params);
      } else {
        this.messageService.error(this.notificationMessage.E001);
      }
    });

  }

  loadReport(embeddedLink: string, paramSearch: any) {
    let title = 'Danh sách KH Inactive khối CIB';

    const modal = this.modalService.open(KpiReportModalComponent, { windowClass: 'tree__report-modal' });
    modal.componentInstance.embeddedReport = embeddedLink;
    modal.componentInstance.data = null;
    modal.componentInstance.model = paramSearch;
    modal.componentInstance.baseUrl = null;
    modal.componentInstance.filename = this.fileName;
    modal.componentInstance.title = title;
    modal.componentInstance.extendUrl = '';
    modal.componentInstance.reportType = 1;
  }

  removeList() {
    this.termData = [];
    this.paramsTable.page = 0;
    this.allRM = false;
    this.mapData();
  }

}
