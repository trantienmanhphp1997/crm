import { global } from '@angular/compiler/src/util';
import { Component, EventEmitter, HostBinding, Injector, Input, OnInit, Output } from '@angular/core';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { FunctionCode, maxInt32, Scopes } from 'src/app/core/utils/common-constants';
import * as _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { RmModalComponent } from 'src/app/pages/rm/components/rm-modal/rm-modal.component';
import { Utils } from 'src/app/core/utils/utils';
import { RmApi } from 'src/app/pages/rm/apis';

@Component({
  selector: 'app-block-bank-table-rm',
  templateUrl: './block-bank-table-rm.component.html',
  styleUrls: ['./block-bank-table-rm.component.scss'],
})
export class BlockBankTableRmComponent extends BaseComponent implements OnInit {
  isLoading: boolean;
  pageable: Pageable;
  limit = global.userConfig.pageSize;
  messages: any;
  params = {
    pageSize: this.limit,
    pageNumber: 0,
    scope: Scopes.VIEW,
    rsId: '',
  };
  search_value: string;
  listRMTable = [];
  listRMChoose = [];
  @Output() modelChange = new EventEmitter();
  @HostBinding('class.app__right-content') appRightContent = true;
  @Input() listOldRm: Array<any>;
  @Input() customer: string;
  listRm: Array<any>;
  textSearch: string;
  rm: any;
  @Input() rmCode: string;
  countAddMember: number;

  constructor(injector: Injector, private api: RmApi) {
    super(injector);
    this.messages = global.messageTable;
    this.rm = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
  }

  ngOnInit(): void {
    this.search_value = this.rmCode ? this.rmCode : '';
    this.isLoading = true;
    // const rm_params_init = {
    //   page: 0,
    //   size: maxInt32,
    //   crmIsActive: true,
    //   scope: Scopes.VIEW,
    //   rsId: _.get(this.rm, 'rsId'),
    //   rmBlock: 'INDIV',
    //   employeeCode: this.search_value,
    //   isNHSReport: true,
    // };

    if (Utils.isArrayNotEmpty(this.listOldRm)) {
      this.isLoading = false;
      this.listRMChoose = this.listOldRm;
      this.search(true);
    } else {
      if (!Utils.isStringEmpty(this.search_value)) {
        this.countAddMember = 1;
        this.addMembers();
      } else {
        this.isLoading = false;
      }
    }
  }

  search(isSearch: boolean) {
    if (isSearch) {
      this.params.pageNumber = 0;
    }
    const offset = this.params.pageNumber * this.params.pageSize || 0;
    this.listRMTable = this.listRMChoose?.slice(offset, offset + this.params.pageSize);
    const total = this.listRMChoose?.length || 0;
    this.pageable = {
      totalElements: total,
      totalPages: Math.floor(total / this.params.pageSize),
      currentPage: this.params.pageNumber,
      size: this.params.pageSize,
    };
  }

  getValue(row, key) {
    return _.get(row, key);
  }

  addMembers() {
    let valueSearch = [];
    if (Utils.isStringEmpty(this.search_value)) {
      return;
    }
    const count = this.listRMChoose?.filter((item) => {
      return item?.rmCode?.toLowerCase() === this.search_value.trim().toLowerCase();
    });
    if (count.length > 0) {
      this.messageService.warn(this.notificationMessage.rmExistTable);
      return;
    }
    const rm_params = {
      manager: true,
      rm: true,
      page: 0,
      size: maxInt32,
      crmIsActive: true,
      scope: Scopes.VIEW,
      rsId: _.get(this.rm, 'rsId'),
      rmBlock: 'INDIV',
      search: this.search_value,
      isNHSReport: true,
    };
    this.isLoading = true;
    this.api.post('findAll', rm_params).subscribe(
      (response) => {
        this.listRm = _.get(response, 'content');
        valueSearch = this.listRm?.filter(
          (item) => item?.t24Employee?.employeeCode?.toUpperCase() === this.search_value?.trim()?.toUpperCase()
        );
        if (valueSearch.length > 0) {
          valueSearch =
            valueSearch.map((item) => {
              return {
                hrsCode: item?.hrisEmployee?.employeeId,
                rmCode: item?.t24Employee?.employeeCode,
                rmFullName: item?.hrisEmployee?.fullName,
              };
            }) || [];
          this.listRMChoose.push(valueSearch[0]);
          this.modelChange.emit(this.listRMChoose);
          this.search(true);
          this.isLoading = false;
        } else {
          this.isLoading = false;
          if (this.countAddMember === 1) {
            this.countAddMember = 0;
            this.search_value = '';
            return;
          }
          this.messageService.warn(this.notificationMessage.rmNotExist);
        }
      },
      () => {
        this.messageService.error(_.get(this.notificationMessage, 'E001'));
        this.isLoading = false;
        return;
      }
    );
  }

  selectMembers() {
    const formSearch = {
      crmIsActive: { value: 'true', disabled: true },
      isRmGroup: true,
      rmBlock: {
        value: 'INDIV',
        disabled: true,
      },
      isNHSReport: true,
      rm: true,
    };

    const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
    modal.componentInstance.dataSearch = formSearch;
    modal.componentInstance.listHrsCode = this.listRMChoose.map((item) => {
      return item.hrsCode;
    });
    modal.componentInstance.isManager = true;
    modal.result
      .then((res) => {
        if (res) {
          if (this.customer === 'TH') {
            if (res.listSelected.length > 50) {
              this.messageService.warn(this.notificationMessage.rmValidationMaxLengthTH);
              return;
            }
          } else {
            if (res.listSelected.length > 30) {
              this.messageService.warn(this.notificationMessage.rmValidationMaxLengthKH);
              return;
            }
          }
          const listRMSelect = res.listSelected?.map((item) => {
            return {
              hrsCode: item?.hrisEmployee?.employeeId,
              rmCode: item?.t24Employee?.employeeCode,
              rmFullName: item?.hrisEmployee?.fullName,
            };
          });
          this.listRMChoose = _.uniqBy([...this.listRMChoose, ...listRMSelect], (i) => i.hrsCode);
          this.listRMChoose = _.remove(this.listRMChoose, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);
          // this.listRMChoose = [...listRMSelect];
          this.modelChange.emit(this.listRMChoose);
          this.search(true);
        }
      })
      .catch(() => {});
  }
  setPage(pageInfo) {
    this.params.pageNumber = pageInfo.offset;
    this.search(false);
  }

  deleteRow(row: any) {
    this.listRMChoose = this.listRMChoose.filter((item) => {
      return item.rmCode != row.rmCode;
    });
    this.modelChange.emit(this.listRMChoose);
    if (this.listRMTable?.length === 1 && this.params.pageNumber > 0) {
      this.params.pageNumber -= 1;
    }
    this.search(false);
  }

  removeList() {
    this.listRMChoose = [];
    this.modelChange.emit(this.listRMChoose);
    this.search(false);
  }
}
