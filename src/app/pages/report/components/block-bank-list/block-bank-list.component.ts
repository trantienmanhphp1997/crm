import { Component, OnInit, Injector } from '@angular/core';
import { forkJoin, of, Subject } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import { FunctionCode, maxInt32, Scopes, SessionKey } from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import _ from 'lodash';
import * as moment from 'moment';
import { cleanDataForm } from 'src/app/core/utils/function';
import { Utils } from 'src/app/core/utils/utils';
import { RmApi } from '../../../rm/apis';
import { KpiProductApi, KpiReportBankApi } from '../../apis';
import { KpiReportModalComponent } from '../../dialogs';

@Component({
  selector: 'app-block-bank-list',
  templateUrl: './block-bank-list.component.html',
  styleUrls: ['./block-bank-list.component.scss'],
})
export class BlockBankListComponent extends BaseComponent implements OnInit {
  now = new Date();
  nowAfterDay = this.now.setDate(this.now.getDate() - 1);
  nowAfterMonth = new Date().setMonth(this.now.getMonth() - 1);
  form = this.fb.group({
    kpiReportType: ['CN'],
    kpiDetailReportType: ['TH'],
    productCodes: [''],
    startDate: [new Date(this.nowAfterDay)],
    endDate: [new Date(this.nowAfterDay)],
    dateMonth: [new Date(this.nowAfterMonth)],
    branchCodes: [''],
    rmCodes: [''],
    scope: Scopes.VIEW,
    rsId: '',
    attributeFormat: [''],
  });
  valueReport: string;
  hiddenProduct: boolean;
  branches: Array<any>;
  dateType: boolean;
  checkDateType = 1;
  branchCodes = [];
  block: string;
  dateCheck = new Date();
  listProduct = [];
  listRm = [];
  rm = [];
  listBranchOld: Array<any>;
  listRmOld: Array<any>;
  productCodes = [];
  isLoading: boolean;
  startDate: Date;
  endDate: Date;
  maxDateDefault = new Date();
  checkCustomer: string;
  listBranch: Array<any>;
  profile: any;
  rmCode: string;
  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private rmApi: RmApi,
    private productApi: KpiProductApi,
    private api: KpiReportBankApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
  }
  ngOnInit(): void {
    this.isLoading = true;
    this.form.controls.rsId.setValue(_.get(this.objFunction, 'rsId'));
    this.profile = this.sessionService.getSessionData(SessionKey.USER_INFO);
    this.rmCode = _.get(this.profile, 'code');
    this.dateCheck = new Date(this.nowAfterMonth);
    this.startDate = new Date(this.nowAfterDay);
    this.endDate = new Date(this.nowAfterDay);
    this.block = 'INDIV - Khách hàng cá nhân';
    this.dateType = true;
    this.valueReport = 'CN';
    this.hiddenProduct = false;
    forkJoin([
      this.categoryService
        .getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW)
        .pipe(catchError((e) => of(undefined))),
      this.productApi.get('nhs').pipe(catchError((e) => of(undefined))),
    ]).subscribe(([branches, products]) => {
      this.branches = branches;
      this.checkCustomer = 'TH';
      if (Utils.isArrayNotEmpty(products)) {
        this.listProduct = _.map(products, (item) => ({
          code: _.get(item, 'groupId'),
          value_to_search: `${_.get(item, 'groupId')} -${_.get(item, 'groupName')}`,
        }));
        this.listProduct.unshift({
          code: '',
          value_to_search: this.fields.all,
        });
      }
      this.isLoading = false;
    });
  }

  onClickReport(value: string) {
    this.hiddenProduct = false;
    if (value === 'RM') {
      this.hiddenProduct = true;
      this.form.controls.kpiDetailReportType.setValue('CT');
      this.checkCustomer = 'CT';
    } else {
      this.form.controls.kpiDetailReportType.setValue('TH');
      this.checkCustomer = 'TH';
    }
    this.valueReport = value;
  }

  onClickReportDetail(value: string) {
    this.checkCustomer = value;
    this.form.controls.kpiDetailReportType.setValue(value);
    this.hiddenProduct = false;
    if (value === 'CT' && this.valueReport === 'RM') {
      this.hiddenProduct = true;
    }
  }

  dateTypeChange(event) {
    if (event === '1') {
      this.checkDateType = 1;
    } else {
      this.checkDateType = 0;
    }
  }

  checkDateParamFile() {
    let startDate = moment(this.startDate).format('YYYY-MM-DD');
    let endDate = moment(this.endDate).format('YYYY-MM-DD');
    if (this.checkDateType === 0) {
      const y = this.form.controls.dateMonth.value.getFullYear();
      const m = this.form.controls.dateMonth.value.getMonth();
      const firstDay = new Date(y, m, 1);
      const lastDay = new Date(y, m + 1, 0);
      startDate = moment(firstDay).format('YYYY-MM-DD');
      endDate = moment(lastDay).format('YYYY-MM-DD');
    }
    this.form.controls.endDate.setValue(endDate);
    this.form.controls.startDate.setValue(startDate);
    return true;
  }

  view() {
    this.isLoading = true;
    if (!this.checkDateParamFile()) {
      return;
    }

    const paramSearch = cleanDataForm(this.form);
    if (paramSearch.kpiReportType === 'CN') {
      delete paramSearch.rmCodes;
      delete paramSearch.productCodes;
      if (Utils.isArrayEmpty(this.branchCodes)) {
        this.messageService.warn(this.notificationMessage.branchValidation);
        this.isLoading = false;
        return;
      } else {
        paramSearch.branchCodes = this.branchCodes;
      }
    } else {
      delete paramSearch.branchCodes;
      if (Utils.isArrayEmpty(this.listRm)) {
        this.messageService.warn(this.notificationMessage.rmValidation);
        this.isLoading = false;
        return;
      }
      paramSearch.rmCodes = this.listRm;
      this.productCodes = [];
      if (Utils.isStringEmpty(paramSearch.productCodes) && paramSearch.kpiDetailReportType === 'CT') {
        if (this.listRm.length > 1) {
          this.messageService.warn(this.notificationMessage.rmProductAll);
          this.isLoading = false;
          return;
        }
      } else {
        this.productCodes.push(paramSearch.productCodes);
      }
      paramSearch.productCodes = this.productCodes;
    }
    delete paramSearch.dateMonth;
    paramSearch.attributeFormat = 'pdf';
    const url = _.get(this.api, 'baseURL');
    this.translate.get('reportBank.list.title').subscribe((title) => {
      this.api.postFile('', paramSearch).subscribe(
        (response: any) => {
          const respondSource = new Subject<any>();
          this.api.checkIdReportSuccess(response, respondSource);
          respondSource.asObservable().subscribe((res) => {
            if (res?.error === 'error') {
              this.messageService.error(this.notificationMessage.E001);
            } else if (res?.fileId) {
              this.loadFile(res?.fileId, url, paramSearch, title);
            } else {
              this.messageService.error(this.notificationMessage.messageOrsReport);
            }
            this.isLoading = false;
            respondSource.unsubscribe();
          });
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    });
  }

  onModelChange($event) {
    this.listBranchOld = $event;
    this.branchCodes = _.map($event, (x) => x.code);
  }

  onModelChangeRm($event) {
    this.listRmOld = $event;
    this.listRm = _.map($event, (x) => Utils.isStringNotEmpty(x.rmCode) && x.rmCode);
  }

  onStartDateChange($event) {
    if (Utils.isNull(this.startDate)) {
      this.startDate = new Date(this.now);
    }
    this.ref.detectChanges();
  }

  closeStartDate($event) {
    const smonth = moment(this.startDate).month();
    const year = moment(this.startDate).year();
    this.endDate = new Date(year, smonth + 1, 0);
    this.ref.detectChanges();
  }

  onEndDateChange($event) {
    if (Utils.isNull(this.endDate)) {
      this.endDate = new Date(this.now);
    }
    this.ref.detectChanges();
  }

  closeEvent($event) {
    const smonth = moment(this.startDate).month();
    const year = moment(this.startDate).year();
    const emonth = moment(this.endDate).month();
    if (emonth !== smonth) {
      this.messageService.warn('Bắt đầu từ ngày và đến ngày phải cùng 1 tháng');
      this.endDate = new Date(year, smonth + 1, 0);
    }
    this.ref.detectChanges();
  }

  loadFile(fileId: string, url: string, paramSearch: any, textReport: string) {
    this.api.loadFile(`download/${fileId}`, 'pdf').then(
      (response) => {
        const blob = new Blob([response], { type: 'application/octet-stream' });
        const modal = this.modalService.open(KpiReportModalComponent, { windowClass: 'tree__report-modal' });
        modal.componentInstance.data = blob;
        modal.componentInstance.model = paramSearch;
        modal.componentInstance.title = textReport;
        modal.componentInstance.baseUrl = url;
        modal.componentInstance.extendUrl = '';
        modal.componentInstance.filename = 'bao-cao-nsld-nhs';
        this.isLoading = false;
      },
      () => {
        this.messageService.error(this.notificationMessage.E001);
        this.isLoading = false;
      }
    );
  }
}
