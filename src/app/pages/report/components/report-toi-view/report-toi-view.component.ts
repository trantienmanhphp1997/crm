import { formatDate } from '@angular/common';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { Component, OnInit, Injector } from '@angular/core';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { TableColumn } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import { CommonCategory, Division, FunctionCode, Scopes, SessionKey } from 'src/app/core/utils/common-constants';
import * as moment from 'moment';
import { catchError } from 'rxjs/operators';
import { CustomerApi } from 'src/app/pages/customer-360/apis';
import { CustomerModalComponent } from 'src/app/pages/customer-360/components';
import { KpiReportApi } from '../../apis';
import { KpiReportModalComponent } from '../../dialogs';
import { saveAs } from 'file-saver';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-report-toi-view',
  templateUrl: './report-toi-view.component.html',
  styleUrls: ['./report-toi-view.component.scss'],
})
export class ReportToiViewComponent extends BaseComponent implements OnInit {
  commonData = {
    listDivision: [],
    listCurrencyUnit: [],
    listDTTTRR: [],
    minDate: null,
  };
  paramDTTTRR = [
    {
      division: Division.SME,
      code: 'BAOCAO_TOI_KH_FILTER_MONTHLY_SME',
      data: [],
    },
    {
      division: Division.INDIV,
      code: 'BAOCAO_TOI_KH_FILTER_MONTHLY_INDIV',
      data: [],
    },
    {
      division: 'OTHER',
      code: 'BAOCAO_PL_KH_FILTER_OTHER',
      data: [],
    },
  ];
  divisionConfig = [Division.CIB, Division.INDIV, Division.SME];
  textSearch: string;
  paramsTable = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };
  form = this.fb.group({
    division: '',
    currencyUnit: '',
    reportType: 'customer',
    period: 'MONTHLY',
    month: new Date(),
    dtttrr: '',
    downloadReport: 'HTML',
  });
  termData = [];
  listDataTable = [];
  columns: TableColumn[];
  isSearch = false;
  maxDate = new Date(moment().year(), moment().month());
  fileName = 'bao-cao-toi';

  constructor(injector: Injector, private customerApi: CustomerApi, private kpiReportApi: KpiReportApi) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.form.get('month').setValue(new Date(moment().year(), moment().month() - 1));
    this.columns = [
      {
        name: this.fields.customerCode,
        prop: 'code',
      },
      {
        name: this.fields.customerName,
        prop: 'name',
      },
    ];
    this.form.controls.division.valueChanges.subscribe((value) => {
      const itemDTTTRR = _.find(this.paramDTTTRR, (i) => i.division === value) || _.last(this.paramDTTTRR);
      if (itemDTTTRR?.data?.length > 0) {
        this.commonData.listDTTTRR = itemDTTTRR.data;
        this.form.get('dtttrr').setValue(_.first(this.commonData.listDTTTRR)?.value);
      } else {
        this.commonService
          .getCommonCategory(itemDTTTRR.code)
          .pipe(catchError(() => of(undefined)))
          .subscribe((data) => {
            this.commonData.listDTTTRR = _.get(data, 'content') || [];
            itemDTTTRR.data = this.commonData.listDTTTRR;
            this.form.get('dtttrr').setValue(_.first(this.commonData.listDTTTRR)?.value);
          });
      }
    });
    this.prop = this.sessionService.getSessionData(SessionKey.TOI_REPORTS);
    if (!this.prop) {
      const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
      divisionOfUser?.forEach((item) => {
        if (this.divisionConfig?.includes(item.code)) {
          this.commonData.listDivision.push({ code: item.code, name: `${item.code || ''} - ${item.name || ''}` });
        }
      });
      forkJoin([
        this.commonService
          .getCommonCategory(CommonCategory.CURRENCY_UNIT_REPORTS)
          .pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.REPORTS_YEARS).pipe(catchError(() => of(undefined))),
      ]).subscribe(([currencyUnits, configYear]) => {
        this.commonData.listCurrencyUnit = _.orderBy(_.get(currencyUnits, 'content'), ['orderNum'], ['asc', 'desc']);
        this.commonData.listCurrencyUnit =
          _.get(currencyUnits, 'content')?.map((item) => {
            return { code: item.value, name: item.name, isDefault: item.isDefault };
          }) || [];
        const yearConfig = _.find(_.get(configYear, 'content'), (item) => item.code === 'TOI')?.value || 1;
        this.commonData.minDate = new Date(
          moment()
            .add(-(yearConfig - 1), 'year')
            .startOf('year')
            .valueOf()
        );
        this.sessionService.setSessionData(SessionKey.TOI_REPORTS, this.commonData);
        this.form.get('currencyUnit').setValue(_.first(this.commonData.listCurrencyUnit)?.code);
        this.commonData.listCurrencyUnit.forEach((item) => {
          if (item.isDefault) {
            this.form.get('currencyUnit').setValue(item?.code);
          }
        });
        this.form.get('division').setValue(_.first(this.commonData.listDivision)?.code);
        this.isLoading = false;
      });
    } else {
      this.commonData = this.prop;
      this.form.get('currencyUnit').setValue(_.first(this.commonData.listCurrencyUnit)?.code);
      this.commonData.listCurrencyUnit.forEach((item) => {
        if (item.isDefault) {
          this.form.get('currencyUnit').setValue(item?.code);
        }
      });
      this.form.get('division').setValue(_.first(this.commonData.listDivision)?.code);
      this.isLoading = false;
    }
    this.form.get('reportType').disable();
    this.form.get('period').disable();
  }

  search() {
    const formSearch = {
      customerType: {
        value: this.form.get('division').value,
        disabled: true,
      },
    };
    const modal = this.modalService.open(CustomerModalComponent, { windowClass: 'list__customer360-modal' });
    modal.componentInstance.listSelectedOld = this.termData;
    modal.componentInstance.dataSearch = formSearch;
    modal.result
      .then((res) => {
        if (res) {
          this.termData =
            res.listSelected?.map((item) => {
              return {
                code: item.customerCode,
                customerCode: item.customerCode,
                name: item.customerName ? item.customerName : item.name,
              };
            }) || [];
          this.mapData();
        }
      })
      .catch(() => {});
  }

  add() {
    this.textSearch = _.trim(this.textSearch);
    if (this.textSearch.length === 0) {
      this.isSearch = false;
      return;
    }
    if (_.findIndex(this.termData, (i) => i.code === this.textSearch) === -1) {
      const params = {
        customerCode: this.textSearch,
        customerType: this.form.get('division').value,
        rsId: this.objFunction?.rsId,
        scope: Scopes.VIEW,
        pageNumber: 0,
        pageSize: global?.userConfig?.pageSize,
        customerTypeSale: 1
      };
      this.isSearch = true;
      let api: Observable<any>;
      if (this.form.get('division').value !== Division.INDIV) {
        api = this.customerApi.searchSme(params);
      } else {
        api = this.customerApi.search(params);
      }
      api.subscribe((data) => {
        if (data?.length > 0) {
          this.termData?.push({
            code: data[0]?.customerCode,
            name: data[0]?.customerName,
            customerCode: data[0]?.customerCode,
          });
          this.mapData();
        } else {
          this.messageService.warn(_.get(this.notificationMessage, 'customerNotExist'));
        }
        this.isSearch = false;
      });
    } else {
      this.messageService.warn(this.notificationMessage.customerIsExist);
      this.isSearch = false;
      return;
    }
  }

  setPage(pageInfo) {
    this.paramsTable.page = pageInfo.offset;
    this.mapData();
  }

  mapData() {
    const total = this.termData.length;
    this.pageable.totalElements = total;
    this.pageable.totalPages = Math.floor(total / this.pageable.size);
    this.pageable.currentPage = this.paramsTable.page;
    const start = this.paramsTable.page * this.paramsTable.size;
    this.listDataTable = this.termData?.slice(start, start + this.paramsTable.size);
  }

  removeRecord(item) {
    _.remove(this.termData, (i) => i.code === item.code);
    _.remove(this.listDataTable, (i) => i.code === item.code);
    if (this.listDataTable.length === 0 && this.pageable.currentPage > 0) {
      this.paramsTable.page -= 1;
    }
    this.listDataTable = [...this.listDataTable];
    this.mapData();
  }

  viewReport() {
    if (_.isEmpty(this.termData)) {
      this.messageService.warn(this.notificationMessage.customerValidation);
      return;
    }
    const customerCodes = this.termData?.map((i) => i.code);
    if (customerCodes?.length > 100) {
      this.messageService.warn(this.notificationMessage.countCustomer);
      this.isSearch = false;
      return;
    }
    const params = {
      customerCodes,
      blockCode: this.form.get('division').value,
      unitCurrency: this.form.get('currencyUnit').value,
      businessDate: formatDate(this.form.get('month').value, 'MM/yyyy', 'en'),
      dtttrrCommonValue: this.form.get('dtttrr').value,
      rsId: this.objFunction?.rsId,
      scope: Scopes.VIEW,
      attributeFormat: 'pdf',
    };
    this.isLoading = true;
    if (this.form.get('downloadReport').value === 'EXCEL') {
      params.attributeFormat = 'xlsx';
    }
    forkJoin([this.kpiReportApi.reportTOI(params)]).subscribe(
      ([pdfId]) => {
        const respondSource = new Subject<any>();
        this.kpiReportApi.checkIdReportSuccess(pdfId, respondSource);
        respondSource.asObservable().subscribe((res) => {
          if (res?.error === 'error') {
            this.messageService.error(this.notificationMessage.E001);
          } else if (res?.fileId) {
            this.loadFile(res?.fileId, params);
          } else {
            this.messageService.error(this.notificationMessage.messageOrsReport);
          }
          this.isLoading = false;
          respondSource.unsubscribe();
        });
      },
      () => {
        this.messageService.error(this.notificationMessage.E001);
        this.isLoading = false;
      }
    );
  }

  loadFile(pdfId: string, paramSearch: any) {
    if (this.form.get('downloadReport').value === 'EXCEL') {
      forkJoin([this.kpiReportApi.loadFile(`download/${pdfId}`)]).subscribe(
        ([blobExcel]) => {
          saveAs(blobExcel, `${this.fileName}.xlsx`);
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      forkJoin([this.kpiReportApi.loadFile(`download/${pdfId}`)]).subscribe(
        ([blobPDF]) => {
          let url = `${environment.url_endpoint_kpi}/ors-report/toi`;
          if (this.form.get('downloadReport').value === 'HTML') {
            const modal = this.modalService.open(KpiReportModalComponent, { windowClass: 'tree__report-modal' });
            modal.componentInstance.data = blobPDF;
            modal.componentInstance.model = paramSearch;
            modal.componentInstance.baseUrl = url;
            modal.componentInstance.filename = this.fileName;
            modal.componentInstance.title = 'Báo cáo TOI';
            modal.componentInstance.extendUrl = '';
          } else if (this.form.get('downloadReport').value === 'PDF') {
            saveAs(blobPDF, `${this.fileName}.pdf`);
          }
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
    }
  }
  removeList() {
    this.termData = [];
    this.mapData();
  }
}
