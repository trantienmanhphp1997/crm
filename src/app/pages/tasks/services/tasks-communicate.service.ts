import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class TaskCommunicateService {
  private taskRequestSource = new Subject<any>();
  private taskRespondSource = new Subject<any>();

  // Observable string streams
  taskAnnounced$ = this.taskRequestSource.asObservable();
  taskConfirmed$ = this.taskRespondSource.asObservable();

  // Service message commands
  request(data: any) {
    this.taskRequestSource.next(data);
  }

  respond(data: any) {
    this.taskRespondSource.next(data);
  }
}
