import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TaskService {
  constructor(private http: HttpClient) {}

  search(params: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_workflow}/tasks`, { params });
  }

  searchTaskByUser(params: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm_relationship}/graph-relationship/getTasksByUser`, { params });
  }

  searchTaskFollowUp(params: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm_relationship}/graph-relationship/followsTask`, { params });
  }

  searchTaskAssignUp(params: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm_relationship}/graph-relationship/assignsTask/v2`, { params });
  }

  getTaskToDoById(id): Observable<any> {
    return this.http.get(`${environment.url_endpoint_activity}/to-do/v2/${id}`);
  }

  createTaskTodo(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/to-do`, data, { responseType: 'text' });
  }

  createTaskTodoV2(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/to-do/v2`, data);
  }

  updateTaskTodo(data): Observable<any> {
    return this.http.put(`${environment.url_endpoint_activity}/to-do/v2`, data);
  }

  deleteTaskTodo(id): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_activity}/to-do/v2/${id}`);
  }

  sendApproveTaskTodo(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/to-do/approve`, data);
  }

  completedTaskTodo(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/to-do/complete`, data);
  }

  rejectTaskTodo(id): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/to-do/reject/${id}`, {});
  }

  unFollowTaskTodo(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/to-do/unfollow`, data);
  }

  unFollowTaskTodoV2(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/to-do/unfollow/v2`, data);
  }

  createTaskActivity(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/activities`, data, { responseType: 'text' });
  }

  findTaskActivityTypeById(id): Observable<any> {
    return this.http.get(`${environment.url_endpoint_activity}/activities/${id}`);
  }

  findById(id): Observable<any> {
    return this.http.get(`${environment.url_endpoint_workflow}/tasks/${id}?includeTaskLocalVariables=true`);
  }

  getPerformamnce(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_workflow}/processes/getPerformance`, data);
  }

  getProcessesInstances(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_workflow}/processes/instances`, { params });
  }

  searchLeadPreviews(params: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/lead-previews`, { params });
  }

  approvedLeadPreviews(data: any): Observable<any> {
    return this.http.post(`${environment.url_endpoint_workflow}/processes/startProcessAssign`, data);
  }

  processAssign(data: any) {
    return this.http.post(`${environment.url_endpoint_workflow}/processes/complete-process-task`, data);
  }

  mergeLeadPreviews(data: any): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/lead-previews/merge`, data);
  }

  // onUpdate(): Observable<any> {
  //   return this.socketClient.on('/topic/process/update', ServiceType.WORKFLOW);
  // }
}
