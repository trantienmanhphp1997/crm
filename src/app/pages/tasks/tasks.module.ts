import { NgxSelectModule } from 'ngx-select-ex';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TasksRoutingModule } from './tasks-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { BadgeModule } from 'primeng/badge';
import { ChipModule } from 'primeng/chip';
import { AvatarModule } from 'primeng/avatar';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import {
  TasksLeftViewComponent,
  TasksViewComponent,
  LeadTaskComponent,
  LeadsDuplicateComponent,
  LeadsAssignComponent,
  DuplicateViewComponent,
  CreateTaskModalComponent,
  DetailTaskModalComponent,
  ChooseUserComponent,
  ListTasksComponent,
  DetailActivityComponent,
} from './components';

const COMPONENTS = [
  TasksLeftViewComponent,
  TasksViewComponent,
  LeadTaskComponent,
  LeadsDuplicateComponent,
  LeadsAssignComponent,
  DuplicateViewComponent,
  CreateTaskModalComponent,
  DetailTaskModalComponent,
  ChooseUserComponent,
  ListTasksComponent,
  DetailActivityComponent,
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [
    CommonModule,
    TasksRoutingModule,
    HttpClientModule,
    SharedModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule,
    NgxDatatableModule,
    NgxSelectModule,
    ButtonModule,
    RippleModule,
    BadgeModule,
    ChipModule,
    AvatarModule,
    CalendarModule,
    DropdownModule,
  ],
  exports: [CreateTaskModalComponent, ChooseUserComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class TasksViewModule {}
