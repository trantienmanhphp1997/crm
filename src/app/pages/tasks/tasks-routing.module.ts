import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TasksLeftViewComponent } from './components/tasks-left-view/tasks-left-view.component';
import { RoleGuardService } from 'src/app/core/services/role-guard.service';
import { FunctionCode } from 'src/app/core/utils/common-constants';
import { TasksViewComponent } from './components/tasks-view/tasks-view.component';

const routes: Routes = [
  {
    path: '',
    component: TasksLeftViewComponent,
    outlet: 'app-left-content',
  },
  { path: '', component: TasksViewComponent, canActivate: [RoleGuardService], data: { code: FunctionCode.TASK } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TasksRoutingModule {}
