import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-choose-user-assign',
  templateUrl: './choose-user.component.html',
  styleUrls: ['./choose-user.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ChooseUserComponent implements OnInit {
  data: any[];
  temp: any[];
  rmSearch = '';
  isLoading = true;

  constructor(private modal: NgbActiveModal) {}

  ngOnInit(): void {
    const timer = setTimeout(() => {
      this.temp = this.data;
      this.isLoading = false;
      clearTimeout(timer);
    }, 300);
  }

  chooseUser(user) {
    this.modal.close(user);
  }

  searchRM() {
    const val = this.rmSearch.trim().toLowerCase();
    // filter our data
    this.data = this.temp.filter(function (item) {
      return (
        (item?.code && item?.code?.toLowerCase().indexOf(val) !== -1) ||
        (item?.fullName && item?.fullName?.toLowerCase().indexOf(val) !== -1) ||
        !val
      );
    });
  }

  closeModal() {
    this.modal.close(false);
  }
}
