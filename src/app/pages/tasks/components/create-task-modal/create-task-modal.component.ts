import { forkJoin, of } from 'rxjs';
import { TaskService } from '../../services/tasks.service';
import { AfterViewInit, Component, HostBinding, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {Format, FunctionCode, maxInt32, PriorityTaskOrCalendar, Scopes} from 'src/app/core/utils/common-constants';
import { UserService } from 'src/app/pages/system/services/users.service';
import { ChooseUserComponent } from './choose-user.component';
import { global } from '@angular/compiler/src/util';
import { cleanDataForm, getRole } from 'src/app/core/utils/function';
import { BaseComponent } from 'src/app/core/components/base.component';
import { RmApi } from 'src/app/pages/rm/apis';
import { catchError } from 'rxjs/operators';
import {ConfirmDialogComponent} from "../../../../shared/components";
import {Utils} from "../../../../core/utils/utils";

@Component({
  selector: 'app-create-task-modal',
  templateUrl: './create-task-modal.component.html',
  styleUrls: ['./create-task-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CreateTaskModalComponent extends BaseComponent implements OnInit, AfterViewInit {
  @HostBinding('class.app-create-task') classCreateTask = true;
  data: any;
  today = new Date();
  checkLists = [];
  now = new Date();
  form = this.fb.group({
    subject: ['', Validators.required],
    dueDate: [this.now, Validators.required],
    createdAt: [this.now, Validators.required],
    description: [''],
    task: [''],
    level: [PriorityTaskOrCalendar.Medium],
    isSave: false,
    rsId: '',
    scope: Scopes.CREATE
  });
  listAssignee = [];
  listFollow = [];
  listUsersAssign = [];
  listUsersFollow = [];
  task = '';
  optCreateDate: any = {
    format: Format.DateTimeUp,
    minDate: this.now,
  };
  optDueDate: any = {
    format: Format.DateTimeUp,
  };
  role: string;
  priority = {
    high: false,
    medium: true,
    low: false,
  };

  constructor(
    injector: Injector,
    private userService: UserService,
    private taskService: TaskService,
    private modalActive: NgbActiveModal,
    private rmApi: RmApi
  ) {
    super(injector);
    this.role = getRole(global.roles);
    this.isLoading = true;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.TASK}`);
  }

  ngOnInit(): void {
    this.listAssignee.push({
      code: this.currUser?.code,
      fullName: this.currUser?.fullName,
      hrsCode: this.currUser?.hrsCode,
    });

    const param = {
      rsId: this.objFunction?.rsId,
      scope: Scopes.VIEW,
    };
    forkJoin([
    //  this.rmApi.getRmAssignOrFollow(true).pipe(catchError(() => of(undefined))),

      this.rmApi.getRmAssign(param,true).pipe(catchError(() => of(undefined))),
      this.rmApi.getRmAssignOrFollow(false).pipe(catchError(() => of(undefined))),
    ]).subscribe(
      ([listAssign, listFollow]) => {
        this.listUsersAssign = listAssign;
        if (
          this.listUsersAssign?.findIndex((item) => {
            return item.hrsCode === this.currUser?.hrsCode;
          }) === -1
        ) {
          this.listUsersAssign?.push({
            code: this.currUser?.code,
            fullName: this.currUser?.fullName,
            hrsCode: this.currUser?.hrsCode,
          });
        }
        this.listUsersFollow = listFollow;
        this.isLoading = false;
      },
      () => {
        this.modalActive.close(false);
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.E001);
      }
    );
  }

  ngAfterViewInit() {
    this.form.controls.createdAt.valueChanges.subscribe((value) => {
      this.optDueDate.minDate = value;
      if (value.valueOf() > this.form.controls.dueDate.value.valueOf()) {
        this.form.controls.dueDate.setValue(value);
      }
    });
  }

  addUserAssignee() {
    const chooseModal = this.modalService.open(ChooseUserComponent, { windowClass: 'choose-user-modal' });
    chooseModal.componentInstance.data = this.listUsersAssign?.filter((item) => {
      return (
        this.listAssignee?.findIndex((itemAssign) => {
          return itemAssign.hrsCode === item.hrsCode;
        }) === -1
      );
    });
    chooseModal.result
      .then((result) => {
        if (result) {
          this.listAssignee.push(result);
        }
      })
      .catch(() => {});
  }

  addUserFollow() {
    const chooseModal = this.modalService.open(ChooseUserComponent, { windowClass: 'choose-user-modal' });
    chooseModal.componentInstance.data = this.listUsersFollow?.filter((item) => {
      return (
        this.listFollow?.findIndex((itemFollow) => {
          return itemFollow.hrsCode === item.hrsCode;
        }) === -1
      );
    });
    chooseModal.result
      .then((result) => {
        if (result) {
          this.listFollow.push(result);
        }
      })
      .catch(() => {});
  }

  confirmDialog() {
    if (this.form.valid && this.listAssignee.length > 0) {
      this.isLoading = true;
      const data = cleanDataForm(this.form);
      data.assign = [];
      this.listAssignee.forEach((item) => {
        data.assign.push(item.hrsCode);
      });
      data.followUp = [];
      this.listFollow.forEach((item) => {
        data.followUp.push(item.hrsCode);
      });
      data.refType = this.data.parentType;
      data.refId = this.data.parentId;
      data.refName = this.data.parentName;
      data.contents = this.checkLists;
      data.rsId = this.objFunction?.rsId;
      delete data.task;
      this.taskService.createTaskTodoV2(data).subscribe(
        (res) => {
          console.log('item: ',res);
          if(res?.code && res?.code === 'err.api.errorcode.235'){
            this.isLoading = false;
            const confirmSave = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
            confirmSave.componentInstance.message = res?.description ;
            confirmSave.componentInstance.title = 'Cảnh báo';
            confirmSave.result
              .then((confirmed: boolean) => {
                if (confirmed) {
                  data.isSave = true;
                  this.isLoading = true;
                  this.taskService.createTaskTodoV2(data).subscribe((resNew:any) => {
                    this.messageService.success(this.notificationMessage.success);
                    this.isLoading = false;
                    this.modalActive.close(true);
                  }, (er) => {
                    this.messageService.error(this.notificationMessage.error);
                    this.isLoading = false;
                  });
                }
              })
              .catch(() => {
                this.messageService.error(this.notificationMessage.error);
              });
          }
          else{
            this.messageService.success(this.notificationMessage.success);
            this.modalActive.close(true);
          }
        },
        () => {
          this.messageService.error(this.notificationMessage.error);
          this.isLoading = false;
        }
      );
    } else {
      this.validateAllFormFields(this.form);
    }
  }

  createNewCheckList() {
    if (Utils.trimNullToEmpty(this.form.get('task').value) !== '') {
      this.checkLists.push({ title: this.form.get('task').value, index: this.checkLists.length, checked: false });
      this.form.get('task').reset();
    }
  }

  handlePriority(value: number) {
    this.priority = {
      high: value === 1,
      medium: value === 2,
      low: value === 3,
    };
    if (value === 1) {
      this.form.controls.level.setValue(PriorityTaskOrCalendar.High);
    } else if (value === 2) {
      this.form.controls.level.setValue(PriorityTaskOrCalendar.Medium);
    } else if (value === 3) {
      this.form.controls.level.setValue(PriorityTaskOrCalendar.Low);
    }
  }

  removeItemAssign(item) {
    this.listAssignee = this.listAssignee.filter((user) => {
      return item.hrsCode !== user.hrsCode;
    });
  }

  removeItemFollow(item) {
    this.listFollow = this.listFollow.filter((user) => {
      return item.code !== user.code;
    });
  }

  removeTask(index) {
    this.checkLists.splice(index, 1);
  }

  closeModal() {
    this.modalActive.close(false);
  }

  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
