import { DuplicateViewComponent } from '../duplicate-view/duplicate-view.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, OnDestroy, ViewEncapsulation, HostBinding, Injector } from '@angular/core';
import { TaskService } from '../../../services/tasks.service';
import { Subscription } from 'rxjs';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { global } from '@angular/compiler/src/util';
import { LeadService } from 'src/app/core/services/lead.service';
import { maxInt32 } from 'src/app/core/utils/common-constants';

@Component({
  // selector: 'app-leads-duplicate',
  templateUrl: './leads-duplicate.component.html',
  styleUrls: ['./leads-duplicate.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LeadsDuplicateComponent extends BaseComponent implements OnInit, OnDestroy {
  @HostBinding('class.lead-duplicate-content') leadDuplicateContent = true;
  task: any;
  leadPreviews: any;
  pageable: Pageable;
  campaignData: any;
  subscription: Subscription;
  isLoading = false;
  fileId: string;
  isDetail = false;
  paramSearch = {
    page: 0,
    size: global.userConfig.pageSize,
    fileId: '',
  };

  constructor(
    injector: Injector,
    private taskService: TaskService,
    private leadService: LeadService,
    private modalActive: NgbActiveModal
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.paramSearch.fileId = this.fileId;
  }

  search() {
    this.isLoading = true;
    this.leadService.searchLeadPreviews(this.paramSearch).subscribe(
      (result) => {
        this.leadPreviews = result?.content;
        this.pageable = {
          totalElements: result?.totalElements,
          totalPages: result?.totalPages,
          currentPage: result?.number,
          size: this.paramSearch.size,
        };
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  showDuplicate(item: any) {
    const params = {
      page: 0,
      size: maxInt32,
      phoneNumber: item?.phoneNumber,
    };
    this.isLoading = true;
    this.leadService.getCustomersIsDuplicateByPhone(params).subscribe(
      (result) => {
        if (result) {
          const modalRef = this.modalService.open(DuplicateViewComponent, { windowClass: 'merge-modal' });
          modalRef.componentInstance.itemLeadPreview = item;
          modalRef.componentInstance.listCustomers = result?.content;
          modalRef.result
            .then((res) => {
              if (res) {
                this.search();
              }
            })
            .catch(() => {});
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  confirmDialog() {
    // if (this.leadPreviews && this.leadPreviews.length > 0) {
    //   return;
    // }
    this.modalService
      .open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' })
      .result.then((result) => {
        if (result) {
          this.approved();
        }
      })
      .catch(() => {});
  }

  closeModal() {
    this.modalActive.close();
  }

  approved() {
    this.isLoading = true;
    const data: any = {};
    data.taskId = this.task?.id;
    data.assignProcessType = this.task?.taskLocalVariables?.assignProcessType;
    data.processDefinitionKey = 'app_crm_assign_lead_process';
    data.campaignId = this.task?.taskLocalVariables?.campaignId;
    this.taskService.processAssign(data).subscribe(
      () => {
        this.isLoading = false;
        this.modalActive.close(true);
        this.messageService.success(this.notificationMessage.success);
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }

  setPage(pageInfo) {
    this.paramSearch.page = pageInfo.offset;
  }

  ngOnDestroy() {
    // this.subscription.unsubscribe();
  }

  replaceName(name: string) {
    const newName = name.trim();
    return newName.substr(newName.indexOf(']') + 1, newName.length);
  }
}
