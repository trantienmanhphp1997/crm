import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadsDuplicateComponent } from './leads-duplicate.component';

describe('LeadsDuplicateComponent', () => {
  let component: LeadsDuplicateComponent;
  let fixture: ComponentFixture<LeadsDuplicateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LeadsDuplicateComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadsDuplicateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
