import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, OnDestroy, ViewEncapsulation, HostBinding } from '@angular/core';
import { TaskService } from '../../../services/tasks.service';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';

@Component({
  selector: 'app-leads-assign',
  templateUrl: './leads-assign.component.html',
  styleUrls: ['./leads-assign.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LeadsAssignComponent implements OnInit, OnDestroy {
  @HostBinding('class.lead-assign-content') isClassHost = true;
  task: any;
  // form: FormGroup;
  total = 0;
  unAssign = 0;
  leadAssigns: any[];
  performances = [];
  isLoading = false;
  messages: any = {
    emptyMessage: '',
    totalMessage: '',
    selectedMessage: '',
  };

  constructor(
    // private fb: FormBuilder,
    private taskService: TaskService,
    private modal: NgbActiveModal,
    private modalService: NgbModal,
    private messageService: NotifyMessageService
  ) {}

  ngOnInit(): void {
    // let group: any = {};
    let leadAssign: any = {};
    let countAssigned = 0;
    this.leadAssigns = [];
    this.performances?.forEach((item) => {
      leadAssign = {};
      leadAssign.key = item.code;
      leadAssign.label = item.name;
      leadAssign.overValue = false;
      if (item.limit) {
        leadAssign.chossed = true;
        leadAssign.value = item.limit;
        countAssigned += item.limit;
      } else {
        leadAssign.chossed = false;
        leadAssign.value = 0;
      }
      leadAssign.leadRate = item.leadRate;
      this.leadAssigns.push(leadAssign);
    });
    this.total = this.task?.taskLocalVariables?.countLeadPreview || 0;
    this.unAssign = this.total - countAssigned;
  }

  confirmDialog() {
    if (this.unAssign > 0) {
      return;
    }
    this.modalService
      .open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' })
      .result.then((result) => {
        if (result) {
          this.save();
        }
      })
      .catch(() => {});
  }

  closeModal() {
    this.modal.close();
  }

  distributed(type: any) {
    if (type === 1) {
      let percent = 0;
      let countField = 0;
      this.leadAssigns.forEach((item) => {
        if (item.chose) {
          countField++;
        }
      });
      percent = countField <= this.total && this.total > 0 && countField > 0 ? Math.floor(this.total / countField) : 0;
      if (countField > 0) {
        this.leadAssigns.forEach((item, index) => {
          if (item.chose) {
            this.leadAssigns[index].value = percent;
          }
        });
      }
      this.unAssign = this.total - percent * countField;
    }
  }

  onCheckBoxChange(row) {
    row.chose = !row.chose;
    if (!row.chose && row.value) {
      this.unAssign = this.unAssign + row.value;
      row.value = 0;
    }
  }

  checkAll(event) {
    if (!event.target.checked) {
      this.leadAssigns.forEach((item, index) => {
        item.chose = false;
        this.unAssign = this.unAssign + this.leadAssigns[index].value;
        this.leadAssigns[index].value = 0;
      });
    } else {
      this.leadAssigns.forEach((item) => {
        item.chose = true;
      });
    }
  }

  setPercentField(row, isPlus) {
    if (!row.chose) {
      return;
    }

    if (isPlus && row.value < this.total && this.unAssign > 0) {
      row.value += 1;
      this.unAssign = this.unAssign - 1;
    }
    if (!isPlus && row.value > 0) {
      row.value -= 1;
      this.unAssign = this.unAssign + 1;
    }
  }

  save() {
    this.isLoading = true;
    const data: any = {};
    const dataAssign: any = {};
    data.taskId = this.task.id;
    data.processDefinitionKey = 'app_crm_assign_lead_process';
    this.leadAssigns.forEach((item) => {
      if (item.chose) {
        dataAssign[item.key] = item.value;
      }
    });
    data.dataAssign = dataAssign;

    this.taskService.processAssign(data).subscribe(
      () => {
        this.isLoading = false;
        this.modal.close(true);
        this.messageService.success('Thực hiện thành công!');
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  onKey(value, index: number) {
    if (value === '') {
      this.leadAssigns[index].value = 0;
    }
    let valueTotal = 0;
    this.leadAssigns.forEach((item) => {
      valueTotal += item.value;
    });
    if (valueTotal > this.total) {
      this.leadAssigns[index].overValue = true;
    } else {
      this.leadAssigns[index].overValue = false;
      this.unAssign = this.total - valueTotal;
    }
  }

  ngOnDestroy() {}

  replaceName(name: string) {
    const newName = name.trim();
    return newName.substr(newName.indexOf(']') + 1, newName.length);
  }
}
