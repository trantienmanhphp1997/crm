import { Component, OnInit, ViewEncapsulation, HostBinding, Injector } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseComponent } from 'src/app/core/components/base.component';
import { LeadService } from 'src/app/core/services/lead.service';

@Component({
  selector: 'app-duplicate-view',
  templateUrl: './duplicate-view.component.html',
  styleUrls: ['./duplicate-view.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DuplicateViewComponent extends BaseComponent implements OnInit {
  @HostBinding('class.merge-content') classMergeContent = true;
  itemLeadPreview: any;
  listCustomers = [];
  isLoading = false;
  customerCode: string;

  constructor(injector: Injector, private modal: NgbActiveModal, private leadService: LeadService) {
    super(injector);
  }

  ngOnInit(): void {
    this.listCustomers?.forEach((item) => {
      item.phoneDisplay = item?.phoneT24?.replace('[', '').replace(']', '');
    });
  }

  merge() {
    if (!this.customerCode) {
      return;
    }
    this.isLoading = true;
    const data = {
      leadPreviewId: this.itemLeadPreview?.id,
      customerCode: this.customerCode,
    };
    this.leadService.mergeLeadPreviews(data).subscribe(
      () => {
        this.modal.close(true);
        this.messageService.success(this.notificationMessage.success);
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }

  chooseCustomer(customerCode) {
    this.customerCode = customerCode;
  }

  closeModal() {
    this.modal.close(false);
  }
}
