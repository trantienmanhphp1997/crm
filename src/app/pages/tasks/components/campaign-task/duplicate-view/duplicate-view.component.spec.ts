import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DuplicateViewComponent } from './duplicate-view.component';

describe('DuplicateViewComponent', () => {
  let component: DuplicateViewComponent;
  let fixture: ComponentFixture<DuplicateViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DuplicateViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DuplicateViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
