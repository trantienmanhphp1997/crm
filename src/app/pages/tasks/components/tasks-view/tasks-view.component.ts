import { Component, Injector, OnInit } from '@angular/core';
import { TaskService } from '../../services/tasks.service';
import {
  TaskType,
  StatusTask,
  PriorityTaskOrCalendar,
  FunctionCode,
  Scopes,
  maxInt32,
  TodoType,
} from 'src/app/core/utils/common-constants';
import { DetailTaskModalComponent } from '../../components/detail-task-modal/detail-task-modal.component';
import { global } from '@angular/compiler/src/util';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { cleanDataForm } from 'src/app/core/utils/function';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Observable, forkJoin } from 'rxjs';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CreateTaskModalComponent } from '../../components/create-task-modal/create-task-modal.component';
import { LeadsDuplicateComponent } from '../../components/campaign-task/leads-duplicate/leads-duplicate.component';
import { CampaignsService } from 'src/app/pages/campaigns/services/campaigns.service';
import { LeadService } from 'src/app/core/services/lead.service';
import { LeadsAssignComponent } from '../../components/campaign-task/leads-assign/leads-assign.component';
import { RmApi } from 'src/app/pages/rm/apis';
import { Utils } from 'src/app/core/utils/utils';

@Component({
  selector: 'app-tasks-view',
  templateUrl: './tasks-view.component.html',
  styleUrls: ['./tasks-view.component.scss'],
})
export class TasksViewComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listData = [];
  listBlock = [];
  listSelected = [];
  limit = global.userConfig.pageSize;
  formSearch = this.fb.group({
    subject: [''],
    createdBy: [''],
    level: [''],
    status: [''],
    type: [''],
    assignee: [''],
    campaignId: [''],
    dueDate: [''],
  });
  prevParams: any;
  paramSearch: any = {
    size: this.limit,
    page: 0,
    search: '',
  };
  branchesCode: any;
  pageable: Pageable;
  isSearch = false;
  prop: any;
  isAdvance = true;
  rsId: string;
  scope: string;
  messages = global.messageTable;
  listBranches = [];
  searchType = false;
  checkAll = false;
  countCheckedOnPage = 0;
  isAssign = true;
  priority = [];
  listStatus = [];
  listTaskType = [];
  listCampaign = [];

  constructor(
    injector: Injector,
    private taskService: TaskService,
    private campaignService: CampaignsService,
    private leadService: LeadService,
    private rmApi: RmApi
  ) {
    super(injector);
    this.isLoading = true;
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.TASK}`);
    this.translate.get(['priority', 'task']).subscribe((res) => {
      if (res) {
        this.listStatus.push({ code: '', name: this.fields.all });
        this.listStatus.push({ code: StatusTask.NEW, name: res?.task?.status?.new });
        this.listStatus.push({ code: StatusTask.PENDING, name: res?.task?.status?.pending });
        this.listStatus.push({ code: StatusTask.DONE, name: res?.task?.status?.done });
        this.priority.push({ code: '', name: this.fields.all });
        this.priority.push({ code: PriorityTaskOrCalendar.High, name: res?.priority?.high });
        this.priority.push({ code: PriorityTaskOrCalendar.Medium, name: res?.priority?.medium });
        this.priority.push({ code: PriorityTaskOrCalendar.Low, name: res?.priority?.low });
        this.listTaskType.push({ code: '', name: this.fields.all });
        this.listTaskType.push({ code: TaskType.PG_KH, name: res?.task?.type?.pg_kh });
        this.listTaskType.push({ code: TaskType.TODO, name: res?.task?.type?.td });
      }
    });
  }

  ngOnInit(): void {
    const paramsCampaign = {
      page: 0,
      size: maxInt32,
      rsId: this.objFunction?.rsId,
      scope: Scopes.VIEW,
      // status: CampaignStatus.Activated,
    };
    this.campaignService.search(paramsCampaign).subscribe((listCampaign) => {
      this.listCampaign = listCampaign?.content || [];
      this.listCampaign.forEach((item) => {
        item.displayName = `${item.code} - ${item.name}`;
        item.value_to_search = _.size(item.displayName) > 40 ? Utils.truncate(item.displayName, 50) : item.displayName;
      });
      this.listCampaign.unshift({ id: '', displayName: this.fields.all, value_to_search: this.fields.all });
      this.formSearch.controls.campaignId.setValue(this.listCampaign[0]?.id);
    });
    this.search(true);
  }

  isShow() {
    this.isAdvance = !this.isAdvance;
  }

  switchType() {
    this.isAssign = !this.isAssign;
    if (this.searchType) {
      this.searchAdvance(true);
    } else {
      this.search(true);
    }
  }

  search(isSearch: boolean) {
    this.isLoading = true;
    let params: any;
    if (isSearch) {
      this.paramSearch.page = 0;
      this.paramSearch.search = this.paramSearch.search?.trim();
      params = this.paramSearch;
    } else {
      params = this.prevParams;
      params.page = this.paramSearch.page;
    }
    params.rsId = this.objFunction?.rsId;
    params.scope = Scopes.VIEW;
    this.branchesCode = undefined;
    this.searchType = false;
    let api: Observable<any>;
    if (this.isAssign) {
      api = this.taskService.searchTaskAssignUp(params);
    } else {
      api = this.taskService.searchTaskFollowUp(params);
    }
    api.subscribe(
      (result) => {
        if (result) {
          this.prevParams = params;
          this.countCheckedOnPage = 0;
          this.listData = _.get(result, 'content');
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: this.paramSearch.size,
          };
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  searchAdvance(isSearch: boolean) {
    this.isLoading = true;
    let params: any;
    if (isSearch) {
      this.paramSearch.page = 0;
      params = cleanDataForm(this.formSearch);
      params.page = this.paramSearch.page;
      params.size = this.paramSearch.size;
      if (moment(params.dueDate).isValid()) {
        params.dueDate = moment(params.dueDate).format('YYYY-MM-DD');
      } else {
        delete params.dueDate;
      }
    } else {
      params = this.prevParams;
      params.page = this.paramSearch.page;
    }
    params.rsId = this.objFunction?.rsId;
    params.scope = Scopes.VIEW;
    this.searchType = true;
    let api: Observable<any>;
    if (this.isAssign) {
      api = this.taskService.searchTaskAssignUp(params);
    } else {
      api = this.taskService.searchTaskFollowUp(params);
    }
    api.subscribe(
      (result) => {
        if (result) {
          this.prevParams = params;
          this.countCheckedOnPage = 0;
          this.listData = _.get(result, 'content');
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: this.paramSearch.size,
          };
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    if (!this.isLoading) {
      this.paramSearch.page = pageInfo.offset;
      if (this.searchType) {
        this.searchAdvance(false);
      } else {
        this.search(false);
      }
    }
  }

  create() {
    const data: any = {};
    // data.parentId = this.model.systemInfo.code;
    // data.parentName = this.model.customerInfo?.fullName;
    data.parentType = TaskType.TODO;
    const taskModal = this.modalService.open(CreateTaskModalComponent, {
      windowClass: 'create-task-modal',
      scrollable: true,
    });
    taskModal.componentInstance.data = data;
    taskModal.result
      .then((res) => {
        if (res) {
          if (this.searchType) {
            this.searchAdvance(false);
          } else {
            this.search(false);
          }
        }
      })
      .catch(() => {});
  }

  getRowClass(row) {
    return {
      'row-pending': row?.refStatus === StatusTask.IMPORT_PENDING,
    };
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      if (item.refStatus === StatusTask.IMPORT_PENDING || item.refStatus === StatusTask.ABORT) {
        return;
      }
      this.openDetail(item, true);
    }
  }

  edit(item) {
    this.openDetail(item);
  }

  openDetail(item, isDetail?: boolean) {
    if (item?.type === TaskType.TODO) {
      this.isLoading = true;
      const param = {
        rsId: this.objFunction?.rsId,
        scope: Scopes.VIEW
      }
      forkJoin([
        this.rmApi.getRmAssign(param,true),
        this.rmApi.getRmAssignOrFollow(false),
        this.taskService.getTaskToDoById(item.id),
      ]).subscribe(
        ([listAssign, listFollow, task]) => {
          if (task) {
            const modal = this.modalService.open(DetailTaskModalComponent, {
              windowClass: 'detail-task-modal',
              scrollable: true,
            });
            modal.componentInstance.isDetail = isDetail;
            modal.componentInstance.listUsersAssign = listAssign;
            modal.componentInstance.listUsersFollow = listFollow;
            modal.componentInstance.task = task;
            modal.componentInstance.listChildTodo = task?.listChildTodo;
        //    console.log('task goc: ',task);
            modal.result
              .then((res) => {
                if (res) {
                  if (this.searchType) {
                    this.searchAdvance(false);
                  } else {
                    this.search(false);
                  }
                }
              })
              .catch(() => {});
          }
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    }
    else if (item?.type === TaskType.PG_KH && item?.refStatus === StatusTask.IMPORT_SUCCESS) {
      this.isLoading = true;
      forkJoin([
        this.taskService.findById(item?.id),
        this.campaignService.getById(item?.campaignId),
        this.leadService.searchLeadPreviews({ fileId: item?.fileId, page: 0, size: global?.userConfig?.pageSize }),
      ]).subscribe(
        ([itemTask, itemCampaign, listLeadPreviews]) => {
          const pageable: Pageable = {
            totalElements: listLeadPreviews.totalElements,
            totalPages: listLeadPreviews.totalPages,
            currentPage: listLeadPreviews.number,
            size: this.paramSearch.size,
          };
          const modal = this.modalService.open(LeadsDuplicateComponent, { windowClass: 'lead-duplicate-modal' });
          modal.componentInstance.task = itemTask;
          modal.componentInstance.campaignData = itemCampaign;
          modal.componentInstance.leadPreviews = listLeadPreviews?.content;
          modal.componentInstance.pageable = pageable;
          modal.componentInstance.fileId = item?.fileId;
          modal.componentInstance.isDetail = isDetail;
          modal.result
            .then((res) => {
              if (res) {
                if (this.searchType) {
                  this.searchAdvance(false);
                } else {
                  this.search(false);
                }
              }
            })
            .catch(() => {});
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
          this.messageService.error(this.notificationMessage.E001);
        }
      );
    } else if (item?.type === TaskType.PG_KH && item?.refStatus === StatusTask.WAITING_ASSIGN) {
      this.isLoading = true;
      this.taskService.findById(item?.id).subscribe(
        (task) => {
          this.taskService
            .getPerformamnce({
              assignProcessType: task?.taskLocalVariables?.assignProcessType,
              campaignId: item.campaignId,
            })
            .subscribe(
              (performances) => {
                const modal = this.modalService.open(LeadsAssignComponent, { windowClass: 'leads-assign-modal' });
                modal.componentInstance.task = task;
                modal.componentInstance.performances = performances;
                modal.result
                  .then((res) => {
                    if (res) {
                      if (this.searchType) {
                        this.searchAdvance(false);
                      } else {
                        this.search(false);
                      }
                    }
                  })
                  .catch(() => {});
                this.isLoading = false;
              },
              () => {
                this.isLoading = false;
                this.messageService.error(this.notificationMessage.E001);
              }
            );
        },
        () => {
          this.isLoading = false;
          this.messageService.error(this.notificationMessage.E001);
        }
      );
    }
  }

  delete(item) {
    this.confirmService.confirm().then((res) => {
      if (res) {
        this.isLoading = true;
        this.taskService.deleteTaskTodo(item.id).subscribe(
          () => {
            if (this.searchType) {
              this.searchAdvance(false);
            } else {
              this.search(false);
            }
            this.messageService.success(this.notificationMessage.success);
          },
          (e) => {
            this.isLoading = false;
            if(e?.error?.code){
              this.confirmService.warn(e?.error.description);
            }
            else{
              this.messageService.error(this.notificationMessage.error);
            }
          }
        );
      }
    });
  }

  isShowUpdate(item) {
    if(item && item.type?.toUpperCase() === TaskType.TODO){
      const taskType = _.get(item,'typeTask') || '';
      const typeCompare = TodoType.PARENT;
      return (
        // item.type?.toUpperCase() === TaskType.TODO &&
        // item.refStatus === StatusTask.NEW &&
        this.isAssign
        && moment().isBefore(moment(item?.dueDate))
        && taskType !== typeCompare)
    }
    else{
      const listStatus = [StatusTask.ABORT, StatusTask.IMPORT_PENDING, StatusTask.DONE, StatusTask.COMPLETED];
      return listStatus.indexOf(item.refStatus) === -1 && this.isAssign;
    }
  }

  isShowDelete(item) {
    const taskType = _.get(item,'typeTask') || '';
   // console.log('item: ',taskType);
    return (
      // item.type?.toUpperCase() === TaskType.TODO &&
      // item.refStatus === StatusTask.NEW &&
       this.isAssign &&
      // moment().isBefore(moment(item?.createdAt))
      item.type?.toUpperCase() === TaskType.TODO
      && moment().isBefore(moment(item?.dueDate))
      && taskType !== TodoType.CHILD)
  }
}
