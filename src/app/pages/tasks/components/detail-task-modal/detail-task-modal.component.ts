import { AfterViewInit, Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {
  Format,
  functionUri,
  PriorityTaskOrCalendar,
  StatusTask,
  TaskType,
  TodoType
} from 'src/app/core/utils/common-constants';
import { TaskService } from '../../services/tasks.service';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CustomerApi, CustomerDetailApi } from 'src/app/pages/customer-360/apis';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import * as moment from 'moment';
import { ChooseUserComponent } from '../create-task-modal/choose-user.component';
import { cleanDataForm } from 'src/app/core/utils/function';
import { catchError } from 'rxjs/operators';
import {forkJoin, of} from 'rxjs';
import * as _ from 'lodash';
import {Pageable} from "../../../../core/interfaces/pageable.interface";
import {global} from "@angular/compiler/src/util";
import {LeadsDuplicateComponent} from "../campaign-task/leads-duplicate/leads-duplicate.component";
import {LeadsAssignComponent} from "../campaign-task/leads-assign/leads-assign.component";
import {ConfirmDialogComponent} from "../../../../shared/components";

@Component({
  selector: 'app-detail-task-modal',
  templateUrl: './detail-task-modal.component.html',
  styleUrls: ['./detail-task-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [CustomerApi],
})
export class DetailTaskModalComponent extends BaseComponent implements OnInit, AfterViewInit {
  task: any;
  isDetail: boolean;
  titleHeader = 'task.detailTask';
  iconHeader = 'las la-clipboard-check';
  taskType: string;
  checkLists = [];
  isLoading = false;
  now = new Date();
  form = this.fb.group({
    id: [''],
    subject: ['', CustomValidators.required],
    dueDate: [this.now, CustomValidators.required],
    createdAt: [this.now, CustomValidators.required],
    createdBy: [{ value: '', disabled: true }],
    description: [''],
    level: [''],
    status: [StatusTask.NEW],
  });
  labelAction: string;
  isDisabled = false;
  isDisablePriority = false;
  isFollowUp = false;
  isAssign = false;
  statusTask = StatusTask;
  priority = {
    high: false,
    medium: false,
    low: false,
  };
  listAssignee = [];
  listFollow = [];
  listUsersAssign = [];
  listUsersFollow = [];
  listChildTodo = [];
  listStatus = [
    { code: StatusTask.NEW, name: 'Chưa hoàn thành' },
    { code: StatusTask.DONE, name: 'Hoàn thành' }
  ];
  optCreateDate: any = {
    format: Format.DateTimeUp,
    minDate: this.now,
  };
  optDueDate: any = {
    format: Format.DateTimeUp,
    minDate: this.now,
  };
  isEdit = true;
  newCheckList = '';
  createdAtText: string;
  isInProgress = false;
  pageable: Pageable;
  constructor(
    injector: Injector,
    private taskService: TaskService,
    private modalActive: NgbActiveModal,
    private customerService: CustomerDetailApi
  ) {
    super(injector);
  }

  ngOnInit(): void {
    if(!this.isDetail){
      this.titleHeader = 'task.editTask';
    }
    if (this.task) {
      this.task.createdAt = new Date(this.task.createdAt);
      this.task.dueDate = new Date(this.task.dueDate);
      this.optDueDate.minDate = this.task.createdAt;
      this.listAssignee = this.task?.assign || [];
      this.listFollow = this.task?.followUp || [];
      this.listChildTodo = this.task?.listChildTodo || [];
    }
    this.form.patchValue(this.task);
    this.priority[this.task?.level?.toLowerCase()] = true;
    // if (
    //   this.listUsersAssign?.findIndex((item) => {
    //     return item.hrsCode === this.currUser?.hrsCode;
    //   }) === -1
    // ) {
    //   this.listUsersAssign.push({
    //     code: this.currUser?.code,
    //     fullName: this.currUser?.fullName,
    //     hrsCode: this.currUser?.hrsCode,
    //   });
    // }
    if (this.task.refType === TaskType.CUSTOMER) {
      this.taskType = this.labelTitle.customer;
    } else if (this.task.refType === TaskType.LEAD) {
      this.taskType = this.labelTitle.potentialCustomers;
    } else if (this.task.refType === TaskType.OPPORTUNITY) {
      this.taskType = this.labelTitle.opportunity;
    }
    // if (this.task.note) {
    //   this.form.get('note').setValue(this.task.note);
    // }
    if (this.task.contents) {
      this.checkLists = this.task.contents;
    }
    if (
      this.task?.followUp?.findIndex((item) => {
        return item.username === this.currUser?.username;
      }) > -1
    ) {
      this.isFollowUp = true;
    }
    if (
      this.task?.assign?.findIndex((item) => {
        return item.username === this.currUser?.username;
      }) > -1
    ) {
      this.isAssign = true;
    }
    this.isDisabled = this.task.status === StatusTask.PENDING || this.task.status === StatusTask.DONE;
    if (
      this.currUser?.username !== this.task?.createdBy ||
      this.isDetail ||
      moment(this.now).isAfter(moment(this.task?.createdAt)) ||
      this.task?.status !== StatusTask.NEW
    ) {
      this.form.disable();
      this.isEdit = false;
    }
    // if (
    //   !this.isDetail &&
    //   moment(this.now).isBefore(moment(this.task?.dueDate)) &&
    //   moment(this.now).isAfter(moment(this.task?.createdAt)) &&
    //   this.task?.status === StatusTask.NEW &&
    //   this.currUser?.username === this.task?.createdBy
    // ) {
    //   this.form.controls.subject.enable();
    //   this.form.controls.description.enable();
    //   this.form.controls.dueDate.enable();
    //   this.createdAtText = this.task.createdAt;
    //   this.isInProgress = true;
    // }
    // if (!this.isDetail && this.task?.status !== StatusTask.DONE) {
    //   this.form.controls.description.enable();
    // }
    //chinh sua o day
    const taskType = _.get(this.task,'typeTask') || '';
    if(!this.isDetail && moment().isBefore(moment(this.task?.dueDate))){
      if(taskType === ''){
          this.form.controls.createdAt.enable();
          this.form.controls.status.enable();
          this.form.controls.subject.enable();
          this.form.controls.description.enable();
          this.form.controls.dueDate.enable();
          this.createdAtText = this.task.createdAt;
          this.isInProgress = true;
      }
      else if(taskType === TodoType.CHILD){
      //  this.form.controls.level.disable();
        this.isDisablePriority = true;
        this.form.controls.status.enable();
        this.form.controls.description.enable();
      }
    }
    this.pageable = {
      totalElements: this.listChildTodo.length,
      totalPages: 1,
      currentPage: 0,
      size: this.listChildTodo.length,
    };

  }

  ngAfterViewInit() {
    this.form.controls.createdAt.valueChanges.subscribe((value) => {
      this.optDueDate.minDate = value;
      if (value.valueOf() > this.form.controls.dueDate.value.valueOf()) {
        this.form.controls.dueDate.setValue(value);
      }
    });
  }

  addUserAssignee() {
    if (!this.isEdit) {
      return;
    }
    const chooseModal = this.modalService.open(ChooseUserComponent, { windowClass: 'choose-user-modal' });
    chooseModal.componentInstance.data = this.listUsersAssign?.filter((item) => {
      return (
        this.listAssignee?.findIndex((itemAssign) => {
          return itemAssign.hrsCode === item.hrsCode;
        }) === -1
      );
    });
    chooseModal.result
      .then((result) => {
        if (result) {
          this.listAssignee?.push(result);
        }
      })
      .catch(() => {});
  }

  addUserFollow() {
    if (!this.isEdit && !this.isInProgress) {
      return;
    }
    const chooseModal: NgbModalRef = this.modalService.open(ChooseUserComponent, { windowClass: 'choose-user-modal' });
    chooseModal.componentInstance.data = this.listUsersFollow?.filter((item) => {
      return (
        this.listFollow?.findIndex((itemFollow) => {
          return itemFollow.hrsCode === item.hrsCode;
        }) === -1
      );
    });
    chooseModal.result
      .then((result) => {
        if (result) {
          this.listFollow.push(result);
        }
      })
      .catch(() => {});
  }

  handlePriority(value: number) {
    this.priority = {
      high: value === 1,
      medium: value === 2,
      low: value === 3,
    };
    if (value === 1) {
      this.form.controls.level.setValue(PriorityTaskOrCalendar.High);
    } else if (value === 2) {
      this.form.controls.level.setValue(PriorityTaskOrCalendar.Medium);
    } else if (value === 3) {
      this.form.controls.level.setValue(PriorityTaskOrCalendar.Low);
    }
  }

  removeItemAssign(item) {
    if (!this.isEdit && this.isInProgress) {
      return;
    }
    this.listAssignee = this.listAssignee?.filter((user) => {
      return item.hrsCode !== user.hrsCode;
    });
  }

  removeItemFollow(item) {
    // if (!this.isEdit && this.isInProgress) {
    //   return;
    // }
    if(!this.isDetail && this.isInProgress){
      this.listFollow = this.listFollow?.filter((user) => {
        return item.code !== user.code;
      });
    }
  }

  reject() {
    this.isLoading = true;
    this.taskService.rejectTaskTodo(this.task.id).subscribe(
      () => {
        this.messageService.success(this.notificationMessage.success);
        this.modalActive.close(true);
      },
      () => {
        this.messageService.error(this.notificationMessage.error);
        this.isLoading = false;
      }
    );
  }

  sendAprrove() {
    this.isLoading = true;
    const data: any = cleanDataForm(this.form);
    Object.keys(data).forEach((key) => {
      this.task[key] = data[key];
    });
    this.task.assign = [];
    this.listAssignee?.forEach((item) => {
      this.task.assign.push(item.hrsCode);
    });
    this.task.followUp = [];
    this.listFollow?.forEach((item) => {
      this.task.followUp.push(item.hrsCode);
    });
    this.task.contents = this.checkLists.map((item, index) => {
      return {
        title: item.title,
        checked: item.checked,
        index,
      };
    });
    this.taskService.sendApproveTaskTodo(this.task).subscribe(
      () => {
        this.messageService.success(this.notificationMessage.success);
        this.modalActive.close(true);
      },
      () => {
        this.messageService.error(this.notificationMessage.error);
        this.isLoading = false;
      }
    );
  }

  completed() {
    this.isLoading = true;
    const data: any = cleanDataForm(this.form);
    Object.keys(data)?.forEach((key) => {
      this.task[key] = data[key];
    });
    this.task.assign = [];
    this.listAssignee?.forEach((item) => {
      this.task.assign.push(item.hrsCode);
    });
    this.task.followUp = [];
    this.listFollow?.forEach((item) => {
      this.task.followUp.push(item.hrsCode);
    });
    this.task.contents = this.checkLists.map((item, index) => {
      return {
        title: item.title,
        checked: item.checked,
        index,
      };
    });
    this.taskService.completedTaskTodo(this.task).subscribe(
      () => {
        this.messageService.success(this.notificationMessage.success);
        this.modalActive.close(true);
      },
      () => {
        this.messageService.error(this.notificationMessage.error);
        this.isLoading = false;
      }
    );
  }

  unfollow() {
    if (!this.isFollowUp) {
      return;
    }
    this.isLoading = true;
    const data = {
      toDoId: this.task.id,
    };
    this.taskService.unFollowTaskTodoV2(data).subscribe(
      () => {
        this.isLoading = false;
        this.messageService.success(this.notificationMessage.success);
        this.modalActive.close(true);
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }

  save() {
      this.isLoading = true;
      const data: any = cleanDataForm(this.form);
      // if (!this.isInProgress && moment(this.now).isAfter(data.createdAt)) {
      //   this.messageService.error(this.notificationMessage.startDateNotLessDateNow);
      //   this.isLoading = false;
      //   return;
      // }

      Object.keys(data).forEach((key) => {
        this.task[key] = data[key];
      });
      this.task.contents = this.checkLists.map((item, index) => {
        return {
          title: item.title,
          checked: item.checked,
          index,
        };
      });
      this.task.assign = [];
      this.listAssignee?.forEach((item) => {
        this.task.assign.push(item.hrsCode);
      });
      this.task.followUp = [];
      this.listFollow?.forEach((item) => {
        this.task.followUp.push(item.hrsCode);
      });
      this.task.isSave = false;
      this.taskService.updateTaskTodo(this.task).subscribe(
        (res) => {

          if(res?.code && res?.code === 'err.api.errorcode.235'){
            this.isLoading = false;
            const confirmSave = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
            confirmSave.componentInstance.message = res?.description ;
            confirmSave.componentInstance.title = 'Cảnh báo';
            confirmSave.result
              .then((confirmed: boolean) => {
                if (confirmed) {
                  this.task.isSave = true;
                  this.isLoading = true;
                  this.taskService.updateTaskTodo(this.task).subscribe((resNew:any) => {
                    this.messageService.success(this.notificationMessage.success);
                    this.isLoading = false;
                    this.modalActive.close(true);
                  }, (er) => {
                    this.messageService.error(this.notificationMessage.error);
                    this.isLoading = false;
                  });
                }
              })
              .catch(() => {
                this.messageService.error(this.notificationMessage.error);
              });
          }
          else{
            this.messageService.success(this.notificationMessage.success);
            this.modalActive.close(true);
          }
        },
        () => {
          this.messageService.error(this.notificationMessage.error);
          this.isLoading = false;
        }
      );
  }

  changeCheckList(index) {
    if (!this.isDisabled) {
      this.checkLists[index].checked = !this.checkLists[index].checked;
    }
  }

  detail() {
    if (this.task.refType === TaskType.CUSTOMER) {
      this.isLoading = true;
      this.customerService
        .get(this.task.refId)
        .pipe(catchError(() => of(undefined)))
        .subscribe((item) => {
          const customerType = _.get(item, 'otherInfo.khoiNHS')
            ? _.get(item, 'otherInfo.khoiNHS')
            : _.get(item, 'systemInfo.accountType');
          this.router
            .navigateByUrl(
              `${functionUri.customer_360_manager}/detail/${_.lowerCase(customerType)}/${this.task.refId}`,
              {
                skipLocationChange: true,
              }
            )
            .then(() => {});
          this.modalActive.close();
        });
    } else if (this.task.refType === TaskType.LEAD) {
      // this.router.navigateByUrl(`${functionUri.lead}/detail/${this.task.refId}`).then(() => {});
      this.modalActive.close();
    } else if (this.task.parentType === TaskType.OPPORTUNITY) {
      this.isLoading = true;
      const data: any = {};
      data.taskId = this.task.id;
      let lane: any;
      const lanes = [];
      for (const index in this.task.lanes) {
        if (this.task.lanes[index]) {
          lane = {};
          lane.name = this.task.lanes[index].description;
          lane.id = this.task.lanes[index].lane;
          lane.isDisabled = parseInt(index, 10) === this.task.lanes.length - 1;
          lane.index = index;
          lane.nameProcessDefine = this.task.lanes[index].nameProcessDefine;
          lane.status = this.task.lanes[index].title;
          lanes.push(lane);
        }
      }
      data.lanes = lanes;
      data.widthLane = 100 / this.task.lanes.length + '%';
      data.isUpdate = true;
      // this.opportunityService.getOpportunityById(this.task.parentId).subscribe(
      //   (opportunity) => {
      //     data.opportunity = opportunity;
      //     const paramsActivity = {
      //       parentType: TaskType.OPPORTUNITY,
      //       parentId: this.task.parentId,
      //     };
      //     this.opportunityService.getActivityByOpportunity(paramsActivity).subscribe(
      //       (result) => {
      //         data.activitites = result.content;
      //         this.isLoading = false;
      //         const modalDetail = this.modalService.open(OpportunityDetailComponent, {
      //           windowClass: 'opportunity-detail',
      //           // scrollable: true,
      //         });
      //         modalDetail.componentInstance.data = data;
      //       },
      //       () => {
      //         this.isLoading = false;
      //       }
      //     );
      //   },
      //   () => {
      //     this.isLoading = false;
      //   }
      // );
    }
  }

  createNewCheckList() {
    if (this.newCheckList.trim() !== '') {
      this.checkLists.push({ title: this.newCheckList.trim(), index: this.checkLists.length, checked: false });
      this.newCheckList = '';
    }
  }

  removeTask(index) {
    this.checkLists.splice(index, 1);
  }

  closeModal() {
    this.modalActive.close(false);
  }

  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }
  getValue(value) {
    return value[0]?.fullName;
  }
  detailChildTodo(row){
    const modal = this.modalService.open(DetailTaskModalComponent, {
      windowClass: 'detail-task-modal',
      scrollable: true,
    });
    modal.componentInstance.isDetail = true;
    modal.componentInstance.listUsersAssign = row.assign;
    modal.componentInstance.listUsersFollow = row.followUp;
    modal.componentInstance.task = row;
    // console.log('row: ',row);
    // this.taskService.getTaskToDoById(row.id).subscribe(item => {
    //   console.log('item: ',item);
    //   const modal = this.modalService.open(DetailTaskModalComponent, {
    //     windowClass: 'detail-task-modal',
    //     scrollable: true,
    //   });
    //   modal.componentInstance.isDetail = true;
    //   modal.componentInstance.listUsersAssign = item.assign;
    //   modal.componentInstance.listUsersFollow = item.followUp;
    //   modal.componentInstance.task = item;
    // });
  }
}
