import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailTaskModalComponent } from './detail-task-modal.component';

describe('DetailTaskModalComponent', () => {
  let component: DetailTaskModalComponent;
  let fixture: ComponentFixture<DetailTaskModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailTaskModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailTaskModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
