import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadTaskComponent } from './lead-task.component';

describe('LeadTaskComponent', () => {
  let component: LeadTaskComponent;
  let fixture: ComponentFixture<LeadTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
