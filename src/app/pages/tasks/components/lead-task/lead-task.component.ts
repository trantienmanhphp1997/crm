import { Component, OnInit, Input } from '@angular/core';

@Component({
  templateUrl: './lead-task.component.html',
  styleUrls: ['./lead-task.component.scss'],
})
export class LeadTaskComponent implements OnInit {
  @Input() data: any;
  userName = 'Task';

  constructor() {}

  ngOnInit(): void {}
}
