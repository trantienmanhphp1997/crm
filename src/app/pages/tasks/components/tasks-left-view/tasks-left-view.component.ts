import { trigger, state, style, transition, AUTO_STYLE, animate } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/pages/dashboard/services/dashboard.service';
import { getRole } from 'src/app/core/utils/function';
import { global } from '@angular/compiler/src/util';
import { Roles } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'app-tasks-left-view',
  templateUrl: './tasks-left-view.component.html',
  styleUrls: ['./tasks-left-view.component.scss'],
  animations: [
    trigger('collapse', [
      state(
        'false',
        style({ height: AUTO_STYLE, visibility: AUTO_STYLE, paddingTop: '0.5rem', borderTop: '1px solid' })
      ),
      state('true', style({ height: '0', visibility: 'hidden' })),
      transition('false => true', animate(150 + 'ms ease-in')),
      transition('true => false', animate(150 + 'ms ease-out')),
    ]),
  ],
})
export class TasksLeftViewComponent implements OnInit {
  activityCalls: any[];
  role: string;
  branchCode: string;
  constructor(private dashboardService: DashboardService) {
    this.role = getRole(global.roles);
    // this.branchCode = global.user.attributes.branches ? global.user.attributes.branches[0] : '';
  }

  ngOnInit(): void {
    this.getData();
    // if (this.role !== Roles.RGM) {
    //   this.dashboardService.onUpdateData().subscribe((res) => {
    //     if (res && res.branchCode === this.branchCode) {
    //       this.getData();
    //     }
    //   });
    // } else {
    //   setInterval(() => {
    //     this.getData();
    //   }, 90000);
    // }
  }

  getData() {
    this.dashboardService.getActivityCallsByRole().subscribe((result) => {
      this.activityCalls = [];
      if (result) {
        // result.forEach((activity) => {
        //   activity.isCollapsed = true;
        //   this.activityCalls.push(activity);
        // });
      }
    });
  }

  collapsedActivity(i: number) {
    this.activityCalls[i].isCollapsed = !this.activityCalls[i].isCollapsed;
  }

  identify(index, item) {
    return item ? item.id : undefined;
  }
}
