import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TasksLeftViewComponent } from './tasks-left-view.component';

describe('TasksLeftViewComponent', () => {
  let component: TasksLeftViewComponent;
  let fixture: ComponentFixture<TasksLeftViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TasksLeftViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TasksLeftViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
