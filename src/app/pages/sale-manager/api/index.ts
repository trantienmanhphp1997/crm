import { SaleManagerApi } from './sale-manager.api';
import { SaleTargetApi } from './sale-target.api';
import { SaleTransferApi } from './sale-transfer.api';

export { SaleManagerApi } from './sale-manager.api';
export { SaleTargetApi } from './sale-target.api';
export { SaleTransferApi } from './sale-transfer.api';

export const APIS = [SaleManagerApi, SaleTargetApi, SaleTransferApi];
