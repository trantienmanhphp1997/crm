import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend } from '@angular/common/http';

import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SaleManagerApi {
  constructor(private http: HttpClient) {}

  searcOpportunity(param, pageNumber, pageSize): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_sale}/opportunities_onboarding/search?page=${pageNumber}&size=${pageSize}`,
      param
    );
  }

  getTitleDivisionBranch(param) {
    return this.http.post(`${environment.url_endpoint_sale}/opportunities_onboarding/getTitle`, param);
  }

  excelOpportunity(param): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/opportunities_onboarding/export`, param, {
      responseType: 'text',
    });
  }

  getOppProductRule(rmLevelCode, customerDivision, search, page, size): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_category}/oppProductRule/search?code=${rmLevelCode}&customerDivision=${customerDivision}&search=${search}&page=${page}&size=${size}`
    );
  }

  createOpportunity(param): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/opportunities_onboarding`, param);
  }

  detailOpportunity(opportunityCode): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_sale}/opportunities_onboarding/searchByCode?opportunityCode=${opportunityCode}`
    );
  }

  listProductOpportunity(opportunityId, page, size): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_sale}/opportunities_onboarding/searchOpportunityProducts?opportunityId=${opportunityId}&page=${page}&size=${size}`,
      {}
    );
  }

  getBranchByUser(param): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/branches/getBranchByUser`, param);
  }

  checkRuleUpdate(divisionCode, rsId, scope, opportunityId, isUpdate): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_sale}/opportunities_onboarding/check-rule-update?divisionCode=${divisionCode}&rsId=${rsId}&scope=${scope}&opportunityId=${opportunityId}&isUpdate=${isUpdate}`
    );
  }

  convertOpportunity(opportunityId, rsId, scope): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_sale}/opportunities_onboarding/convert?opportunityId=${opportunityId}&rsId=${rsId}&scope=${scope}`
    );
  }

  findRmByBranchAndLevelCode(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/employees/findRmByBranchAndLevelCode`, params);
  }

  updateOpportunity(param): Observable<any> {
    return this.http.put(`${environment.url_endpoint_sale}/opportunities_onboarding`, param);
  }

  customerDetailOpportunity(param): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/opportunity/${param}`);
  }

  checkUserManager(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/check-user-manager`);
  }
  getBiddingList(body, page, size): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/bidding/get-bidding-list?page=${page}&size=${size}`, body);
  }
  exportBiddingList(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/bidding/export-bidding-list`, body, { responseType: 'text' });
  }
  getLimitList(body, page, size): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/limit-customer/search-limit?page=${page}&size=${size}`, body);
    // return this.http.post(`http://localhost:10186/crm19-customer-org/limit-customer/search-limit?page=${page}&size=${size}`, body);
  }
  exportLimitList(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/limit-customer/exportExcel`, body, { responseType: 'text' });
  }

  getAChance(param): Observable<any> {
    return this.http.put(`${environment.url_endpoint_sale}/sale-opportunity/opportunity`, param, { responseType: 'text' });
  }

  getProductByLevel(id?, blockCode?): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_sale}/sale-opportunity/products?lvl1=${id || ''}&blockCode=${blockCode || ''}`
    );
  }

  getStatusProduct(product): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_sale}/sale-opportunity/status-product?code=${product}`
    );
  }

  getRMUBByBranches(branchCodes): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_sale}/sale-opportunity/employees?branchCodes=${branchCodes}`
    );
  }

  getOppDetailForEdit(code): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_sale}/sale-opportunity/opportunity?code=${code}`
    );
  }

  getPresenter(): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_sale}/sale-opportunity/presenter`
    );
  }

  getBranches(rsId, scope): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_sale}/sale-opportunity/branches?rsId=${rsId}&scope=${scope}`
    );
  }

  createOpportunityINDIV(param): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_sale}/sale-opportunity/create`,
      param
    );
  }

  updateOpportunityINDIV(param): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_sale}/sale-opportunity/update`,
      param
    );
  }

  searchListOpportunity(param): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_sale}/sale-opportunity/search`,
      param
    );
  }

  searchOpportunityDetailFirst(searchStr, searchType): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_sale}/sale-opportunity/opportunity-customer-detail?searchStr=${searchStr}&searchType=${searchType}`
    );
  }

  opportunityINDIVDetail(customerCode, code, rsId, scope): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_sale}/sale-opportunity/opportunity-detail?customerCode=${customerCode}&code=${code}&rsId=${rsId}&scope=${scope}`
    );
  }

  checkPermissionToKH360(param): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_sale}/sale-opportunity/check-permission-to-KH360`,
      param
    );
  }

  exportSaleOpp(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sale-opportunity/export`, body, { responseType: 'text' });
  }

  searchOpportunityDetailFirstLead(customerCode): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_sale}/sale-opportunity/opportunity-customer-khtn-detail?customerCode=${customerCode}`
    );
  }

  checkPermissionToKHTN(param): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_sale}/sale-opportunity/check-permission-to-KHTN`,
      param
    );
  }

  searchSMEListOpportunity(param): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_sale}/sale-opportunity/searchSme`,
      param
    );
  }

  detailSMEOpportunity(code, rsId, scope): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/sale-opportunity/detail/sme?code=${code}&rsId=${rsId}&scope=${scope}`);
  }

  getCustomerInfoOpportunity(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sale-opportunity/misa/search-customer-info`,body);
  }

  createCustomerInfoOpportunity(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sale-opportunity/create/sme`,body, { responseType: 'text' });
  }

  updateCustomerInfoOpportunity(body, code): Observable<any> {
    return this.http.put(`${environment.url_endpoint_sale}/sale-opportunity/update/sme/${code}`,body, { responseType: 'text' });
  }

  getListSmartBank(params):Observable<any>{
    return this.http.post(`${environment.url_endpoint_sale}/sale-opportunity/get-smart-bank`,params);
  }

  getAChanceSME(param): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sale-opportunity/take-chance`, param, { responseType: 'text' });
  }
}
