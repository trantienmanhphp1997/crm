import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AddressApi {
  constructor(private http: HttpClient) {}

  searchLead(body, page, size): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_customer_sme}/addresses/search?page=${page}&size=${size}`,
      body
    );
  }

  getAddressById(id): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/addresses/normal/${id}`);
  }

  getContactByTaxCode(taxCode): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/addresses/contact/${taxCode}`);
  }

  searchCustomer(body, page, size): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_customer_sme}/addresses/search/customer?page=${page}&size=${size}`,
      body
    );
  }

  getReportFinanceAddress(code: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer}/customers/finance-report?customerCode=${code}`);
  }
  getCollateralInformation(code: string): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_customer_sme}/customers/collateral-information2?identifiedNumber=${code}`
    );
  }
  getAddressCustomerById(id,rsId,scope): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/addresses/normal-customer/${id}?rsId=${rsId}&scope=${scope}`);
    // return this.http.get(`http://localhost:10186/crm19-customer-org/addresses/normal-customer/${id}?rsId=${rsId}&scope=${scope}`);
  }
  getReportFinanceByCustomerCode(code: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/addresses/finance-report?customerCode=${code}`);
  }

  getShareholderInfoByTaxCode(taxCode): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/addresses/shareholder-info/${taxCode}`);
  }
}
