import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SaleTargetApi {
  constructor(private http: HttpClient) {}

  searcTargetSale(param, pageNumber, pageSize): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_sale}/targets_onboarding/search?page=${pageNumber}&size=${pageSize}`,
      param
    );
  }

  excelTargetSale(param): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/targets_onboarding/export`, param, {
      responseType: 'text',
    });
  }

  detailTargetSale(targetCode): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/targets_onboarding/searchByCode?targetCode=${targetCode}`);
  }

  updateTargetSale(param): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/targets_onboarding/update`, param);
  }

  checkUpdateTargetSale(divisionCode, rsId, scope, targetId, isUpdate): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_sale}/targets_onboarding/check-rule-update?division=${divisionCode}&rsId=${rsId}&scope=${scope}&id=${targetId}&isUpdate=${isUpdate}`
    );
  }

  convertSale(targetId, rsId, scope): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_sale}/targets_onboarding/convert?id=${targetId}&rsId=${rsId}&scope=${scope}`
    );
  }
}
