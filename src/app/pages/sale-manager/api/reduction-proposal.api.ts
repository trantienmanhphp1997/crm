import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReductionProposalApi {

  info = new BehaviorSubject({
    customer: {},
    toi: {},
    authorization: [],
    files: []
  });

  constructor(private http: HttpClient) {
  }

  findExistingCustomer(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/customers/find-by-customer-code`, body);
  }

  findPotentialCustomer(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/leads/find-by-tax-code`, body);
  }


  deleteReductionProposal(id: string): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_customer_sme}/reduction-proposal/${id}`);
  }

  searchReductionProposal(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/reduction-proposal/list`, params);
  }

  confirmReduction(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/reduction-proposal/confirm`, body);
  }

  CBQLconfirmReduction(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/reduction-proposal/manager-confirm`, body);
  }

  getRedutionProposol(path) {
    return this.http.get(`${environment.url_endpoint_customer_sme}/reduction-proposal/${path}`);
  }

  getInterestRate(body) {
    return this.http.post(`${environment.url_endpoint_customer_sme}/reduction-proposal/view/interest/rate`, body);
  }

  save(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/reduction-proposal/create`, body);
  }

  getManagerReduction(codes: any[]): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/reduction-proposal/manager?codes=${codes}`);
  }

  detailReductionProposal(id: number): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/reduction-proposal/${id}`);
  }

  viewAuthorization(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/reduction-proposal/view/authorization`, body);
  }

  update(id: number, body): Observable<any> {
    return this.http.put(`${environment.url_endpoint_customer_sme}/reduction-proposal/${id}`, body);
  }

  previewReduction(href: string, type?: string, body?: any) {
    const url = `${environment.url_endpoint_customer_sme}/reduction-proposal`;
    return this.http
      .post(url + `/${href}`, body, {
        responseType: 'arraybuffer',
        observe: 'response',
      })
      .toPromise()
      .then((response: HttpResponse<ArrayBuffer>) => {
        return new Blob([response.body], { type: type || 'application/octet-stream' });
      });
  }

  getListAuthor(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/reduction-proposal/interest/viewAuthor`, body);
  }

  getScopeReduction(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/reduction-proposal/get/scope/reduction`, body);
  }

  exportScopeReduction(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/reduction-proposal/export/scope/reduction`,
      body,
      { responseType: 'text' }
    );
  }

  processProposal(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/reduction-proposal/process-proposal`, body);
  }

  getLDCode(customerCode) {
    return this.http.get(`${environment.url_endpoint_customer_sme}/reduction-proposal/get/LD?customerCode=${customerCode}`);
  }

  getToiExisting(customerCode): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/reduction-proposal/get/TOI?customerCode=${customerCode}`);
  }
  downloadFilesECM(EcmDocId): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/reduction-proposal/download?docId=${EcmDocId}`, {
      responseType: 'arraybuffer',
      observe: 'response',
    });
  }

  findExistingCustomerIndiv(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/customers/find-by-idcard`, body);
  }
  encryptEcm(param): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/reduction-proposal/encrypt-ecm?docId=${param}`, {
      responseType: 'text',
    });
  }

  getFees(body) {
    return this.http.post(`${environment.url_endpoint_customer_sme}/reduction-proposal/get/index/chargeCode`, body);
  }

  getCustomerCredit(body) {
    return this.http.post(`${environment.url_endpoint_customer_sme}/reduction-proposal/customerCredit`, body);
  }

  getListFeeAuthor(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/reduction-proposal/fee/viewAuthor`, body);
  }

  getMDCode(customerCode, taxCode) {
    return this.http.get(`${environment.url_endpoint_customer_sme}/reduction-proposal/get/MD?customerCode=${customerCode}&taxCode=${taxCode}`);
  }

  getIndivOptionCode(customerCode, idCard) {
    return this.http.get(`${environment.url_endpoint_customer_sme}/reduction-proposal/get/bpm/khcn?customerCode=${customerCode}&idCard=${idCard}`);
  }

  getKhdnPotentialCustomerSegment(taxCode) {
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/get-segment-KHDN?taxCode=${taxCode}`);
  }

  checkNormalCIB(customerCode) {
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/check-CIB-exception?customerCode=${customerCode}`, {
      responseType: 'text',
    });
  }

  // Setup l�i su?t SME
  getInterestRateDiscounted(body) {
    return this.http.post(`${environment.url_endpoint_customer_sme}/reduction-proposal/get/interest/reduction/info`, body);
  }

  saveSetup(body) {
    return this.http.post(`${environment.url_endpoint_customer_sme}/setup-interest-biz/create`, body, { responseType: 'text' });
  }

  signSetup(customerCode) {
    return this.http.put(`${environment.url_endpoint_customer_sme}/setup-interest-biz/proposal?customerCode=${customerCode}`, {});
  }

  setupDetail(customerCode) {
    return this.http.get(`${environment.url_endpoint_customer_sme}/setup-interest-biz/detail?customerCode=${customerCode}`, {});
  }

  updateSetup(body) {
    return this.http.put(`${environment.url_endpoint_customer_sme}/setup-interest-biz/update`, body);
  }

  censorshipSetup(body) {
    return this.http.post(`${environment.url_endpoint_customer_sme}/setup-interest-biz/manager-confirm`, body);
  }

  saveException(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/reduction-proposal/create-reduction-exception`, body);
  }

  getInfoAuthorization(userName: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/get-info-rm-by-user?userName=${userName}`);
  }

  confirmReductionException(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/reduction-proposal/confirm-reduction-exception`, body, {
      responseType: 'text',
    });
  }
}
