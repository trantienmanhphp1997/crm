import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { SessionService } from '../../../core/services/session.service';

@Injectable({
  providedIn: 'root'
})
export class CashFlowApi {

  constructor(private http: HttpClient, private sessionService: SessionService
  ) {
    // this.info.asObservable();
  }
  getListConstruction(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/cashflow-management/search-constructions-information`, body);
  }

  getConstructionGeneralInfo(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/cashflow-management/construction-general-info`, { params, responseType: 'text' });
  }

  getAllFT(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/cashflow-management/get-transaction-history`, { params });
  }

  countAllFT(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/cashflow-management/get-count-transaction-history`, { params, responseType: 'text' });
  }

  getAssignedFT(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/cashflow-management/ft-assigned`, body);
  }

  updateAssignFT(body): Observable<any> {
    return this.http.put(`${environment.url_endpoint_sale}/cashflow-management/update-assign`, body);
  }

  getAccountByCustomerCode(body): Observable<any>{
    return this.http.post(`${environment.url_endpoint_sale}/cashflow-management/current-account`, body);
  }

  countListFTByAccount(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/cashflow-management/get-count-transaction-history`, { params, responseType: 'text' });
  }

  getListFTByAccount(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/cashflow-management/get-transaction-history`, { params });
  }
  assignedCollateralToFT(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/cashflow-management/assign-collateral-to-ft`, body);
  }


  getListAssignedByCollateralCode(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/cashflow-management/get-list-assigned-by-collateral-code`, body);
  }

  deleteByFT(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/cashflow-management/delete-by-ft`, {params});
  }

  getAmountRemaining(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/cashflow-management/get-ft-remaining-amount`, { params, responseType: 'text' });
  }

  createValuationReport(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/cashflow-management/create-valuation-report`, body);
  }
}
