import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend } from '@angular/common/http';

import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SaleSuggestApi {
  constructor(private http: HttpClient) {}

  getSuggestSelling(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/suggest-selling-product/findAll`, params);
  }

  countSuggestSelling(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/suggest-selling-product/countAll`, params);
  }

  getSectorBySegment(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer}/suggest-selling-product/sectors-by-segments?segments=${params}`);
  }

  groupChat(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/suggest-selling-product/group-chat`, params);
  }

  checkUserRMUB(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/employees/check-user-role`, params);
  }

  addCustomerToCampaign(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/suggest-selling-product/campaign`, data);
  }
}
