import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SaleTransferApi {
  constructor(private http: HttpClient) {}

  searchTransferSale(param, pageNumber, pageSize): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_sale}/sales_onboarding/search?page=${pageNumber}&size=${pageSize}`,
      param
    );
  }

  excelTransferSale(param): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sales_onboarding/export`, param, {
      responseType: 'text',
    });
  }

  detailTransferSale(saleCode): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/sales_onboarding/searchByCode?saleCode=${saleCode}`);
  }

  updateSale(param): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sales_onboarding/update`, param);
  }

  checkUpdateSale(divisionCode, rsId, scope, saleId): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_sale}/sales_onboarding/check-rule-update?division=${divisionCode}&rsId=${rsId}&scope=${scope}&id=${saleId}`
    );
  }
}
