export class ReductionProposalDetail {
    blockCode: string;
    businessActivities: string;
    code: string;
    competitiveness: string;
    customerCode: string;
    customerName:string;
    leadCode: string;
    taxCode: string;
    monthApply: number;
    monthConfirm: number;
    planInformation: string;
    scopeApply: string;
    scopeType: number;
    toiConfirm: number;
    type: number;
    dtttrrConfirm: number;
    nimConfirm: number;
    nimExisting: number;
    reductionProposalTOIDTO: ReductionProposalTOIDTO;
    reductionProposalDetailsDTOList: ReductionProposalDetailsDTOList[];
    reductionProposalFileDTOList: ReductionProposalFileDTOList[];
    reductionProposalAuthorizationDTOList: ReductionProposalAuthorizationDTOList[];
    classification: string;
    segment: string;
    scoreValue: string;
    isNew: string;
    toiExisting: number;
    approvedAuthorization:string;
    financialSituation: string;
    dtttrrExisting: string;
    status: string;
    lastApprove: string;
    creditRanking: string;
    customerSegment: string;
    customerType: string;
    jsonRelationTCTD: string;
    jsonRelationMB: string;
    businessRegistrationNumber: string;
}

export class ReductionProposalTOIDTO {
    ckhBqNim: number;
    ckhBqSd: number;
    dunoBqNim: number;
    dunoBqSd: number;
    dunoTdhBqNim: number;
    dunoTdhBqSd: number;
    kkhBqNim: number;
    kkhBqSd: number;
    tblDsSd: number;
    tblNim: number;
    tfxDsSd: number;
    tfxMpqd: number;
    thuBancaDsSd: number;
    thuBanCaMpqd: number;
    thuKhacDsSd: number;
    thuKhacMpqd: number;
    tttqtDsSd: number;
    tttqtMpqd: number;
    loai_KH: string;
    toi: number;
    dtttrr: number;
}

export class ReductionProposalDetailsDTOList {
    id: number;
    interestAmplitude: number;
    interestMaxReduction: number;
    interestProposedAmplitude: number;
    interestProposedValue: number;
    interestReference: number;
    interestValue: number;
    maximumCost: number;
    maximumCostOffer: number;
    minimumCost: number;
    minimumCostOffer: number;
    ratioCost: number;
    ratioCostOffer: number;
    reductionRate: string;
    type: string;
    name: string;
}

export class ReductionProposalFileDTOList {
    fileId: string;
    fileName: string;
    type: string;
}


export class DetailView {
    customerCode: string;
    customerName: string;
    classification: string;
    segment: string;
    isKhdn: boolean;
    scoreValue: string;
    isNew: string;
    taxCode: string;
}

export class ReductionProposalAuthorizationDTOList {
    id: number;
    fullName: string;
    priority: number;
    reductionProposalCode: string;
    status: string;
    titleCategory: string;
    userName: string;
}
