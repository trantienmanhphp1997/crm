import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGuardService } from 'src/app/core/services/role-guard.service';
import { FunctionCode, Scopes, ScreenType } from 'src/app/core/utils/common-constants';
import { SaleManagerCreateComponent } from './components/sale-manager-create/sale-manager-create.component';
import { SaleManagerDetailComponent } from './components/sale-manager-detail/sale-manager-detail.component';
import { SaleManagerListComponent } from './components/sale-manager-list/sale-manager-list.component';
import { SaleSuggestListComponent } from './components/sale-suggest-list/sale-suggest-list.component';
import { SaleTargetListComponent } from './components/sale-target-list/sale-target-list.component';
import { SaleTargetUpdateComponent } from './components/sale-target-update/sale-target-update.component';
import { SaleTransferDetailComponent } from './components/sale-transfer-detail/sale-transfer-detail.component';
import { SaleTransferListComponent } from './components/sale-transfer-list/sale-transfer-list.component';
import {SaleTrackingReportComponent} from './components/sale-tracking-report/sale-tracking-report.component';
import { InfoBasicComponent} from './components/address/info-basic/info-basic.component';
import {AddressComponent} from './components/address/address.component';
import {BiddingListComponent} from './components/bidding-list/bidding-list.component';
import {SellingOppIndivDetailV2Component} from './components/selling-opp-indiv-detail-v2/selling-opp-indiv-detail.component';
import { SellingOppIndivComponent } from './components/selling-opp-indiv/selling-opp-indiv.component';
import { SellingOppIndivCreateComponent } from './components/selling-opp-indiv-create/selling-opp-indiv-create.component';
import { LimitCustomerComponent } from './components/limit-customer/limit-customer.component';
import { ReductionProposalListViewComponent } from './components/reduction-proposal/reduction-propsal-list-view/reduction-proposal-list-view.component';
import { CreateReductionProposalComponent } from './components/reduction-proposal/create/create.component';
import { DetailReductionProposalComponent } from './components/reduction-proposal/detail/detail.component';
import { UpdateReductionProposalComponent } from './components/reduction-proposal/update/update.component';
import {
  ManagerMoneyTransferComponent
} from './components/money-transfer/manager-money-transfer/manager-money-transfer.component';
import {
  PopupMoneyTransferComponent
} from './components/money-transfer/popup-money-transfer/popup-money-transfer.component';
import { ConstructionListViewComponent } from './components/cash-flow-management/construction-list-view/construction-list-view.component';
import { CreateConstructionComponent } from './components/cash-flow-management/create-construction/create-construction.component';
import { ConstructionInfoComponent } from './components/cash-flow-management/construction-info/construction-info.component';
import { CreateSetupProposalComponent } from './components/reduction-proposal/setup/create/create-setup.component';
import { UpdateSetupProposalComponent } from './components/reduction-proposal/setup/update/update-setup.component';
import { DetailSetupProposalComponent } from './components/reduction-proposal/setup/detail/detail-setup.component';
import { CashFlowCustomerListComponent } from  './components/cash-flow-customer-management/cash-flow-customer-list/cash-flow-customer-list.component'
import { CreateFeeExceptionComponent } from './components/fee-exception/create/create/create.component';

import {
  CreateExceptionComponent
} from './components/reduction-proposal/interest-exception/turning/create/create-exception/create-exception.component';
import {
  UpdateExceptionComponent
} from './components/reduction-proposal/interest-exception/turning/update/update-exception/update-exception.component';
import {
  DetailExceptionComponent
} from './components/reduction-proposal/interest-exception/turning/detail/detail-exception/detail-exception.component';
import { SellingOppSmeDetailComponent } from './components/selling-opp-sme-detail/selling-opp-sme-detail.component';
import { SellingOppSmeCreateComponent } from './components/selling-opp-sme-create/selling-opp-sme-create.component';

const routes: Routes = [
  {
    path: 'sale-manager-list',
    data: {
      code: FunctionCode.OPPORTUNITY,
    },
    canActivateChild: [RoleGuardService],
    children: [
      {
        path: '',
        component: SaleManagerListComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
      {
        path: 'create',
        component: SaleManagerCreateComponent,
        data: {
          scope: Scopes.CREATE,
        },
      },
      {
        path: 'update',
        component: SaleManagerDetailComponent,
        data: {
          type: ScreenType.Update,
        },
      },
      {
        path: 'detail',
        component: SaleManagerDetailComponent,
        data: {
          type: ScreenType.Detail,
        },
      },
    ],
  },

  {
    path: 'sale-target-list',
    data: {
      code: FunctionCode.OPPORTUNITY_TARGET,
    },
    canActivateChild: [RoleGuardService],
    children: [
      {
        path: '',
        component: SaleTargetListComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },

      {
        path: 'update',
        component: SaleTargetUpdateComponent,
        data: {
          scope: Scopes.UPDATE,
          type: ScreenType.Update,
        },
      },

      {
        path: 'detail',
        component: SaleTargetUpdateComponent,
        data: {
          scope: Scopes.VIEW,
          type: ScreenType.Detail,
        },
      },
    ],
  },

  {
    path: 'sale-transfer-list',
    data: {
      code: FunctionCode.OPPORTUNITY_SALE,
    },
    canActivateChild: [RoleGuardService],
    children: [
      {
        path: '',
        component: SaleTransferListComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },

      {
        path: 'update',
        component: SaleTransferDetailComponent,
        data: {
          scope: Scopes.UPDATE,
          type: ScreenType.Update,
        },
      },

      {
        path: 'detail',
        component: SaleTransferDetailComponent,
        data: {
          scope: Scopes.VIEW,
          type: ScreenType.Detail,
        },
      },
    ],
  },

  {
    path: 'address',
    data: {
      code: FunctionCode.ADDRESS,
    },
    canActivateChild: [RoleGuardService],
    children: [
      {
        path: '',
        component: AddressComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
      {
        path: 'detail/:code/:taxCode',
        component: InfoBasicComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
    ],
  },

  {
    path: 'sale-suggest-list',
    data: {
      code: FunctionCode.SALE_SUGGEST_LIST,
    },
    canActivateChild: [RoleGuardService],
    children: [
      {
        path: '',
        component: SaleSuggestListComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
    ],
  },
  {
    path: 'sale-tracking-report',
    data: {
      code: FunctionCode.SALES_TRACKING_REPORT,
    },
    canActivateChild: [RoleGuardService],
    children: [
      {
        path: '',
        component: SaleTrackingReportComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
    ],
  },
  {
    path: 'bidding-list',
    data: {
      code: FunctionCode.BIDDING_LIST,
    },
    canActivateChild: [RoleGuardService],
    children: [
      {
        path: '',
        component: BiddingListComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
    ],
  },
  {
    path: 'selling-opp-indiv',
    data: {
      code: FunctionCode.SELLING_OPPORTUNITY_INDIV,
    },
    canActivateChild: [RoleGuardService],
    children: [
      {
        path: '',
        component: SellingOppIndivComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
      {
        path: 'create',
        component: SellingOppIndivCreateComponent,
        data: {
          scope: Scopes.CREATE,
        },
      },
      {
        path: 'detail-v2',
        component: SellingOppIndivDetailV2Component,
        data: {
          type: ScreenType.Detail,
        },
      },
      {
        path: 'detail-sme',
        component: SellingOppSmeDetailComponent,
        data: {
          type: ScreenType.Detail,
        },
      },
    ],
  },
  {
    path: 'limit-customer',
    data: {
      code: FunctionCode.LIMIT_CUSTOMER,
    },
    canActivateChild: [RoleGuardService],
    children: [
      {
        path: '',
        component: LimitCustomerComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
    ],
  },
  {
    path: 'money-transfer',
    data: {
      code: FunctionCode.SALES_MONEY_TRANSFER,
    },
    canActivateChild: [RoleGuardService],
    children: [
      {
        path: '',
        component: ManagerMoneyTransferComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
      {
        path: 'detail',
        component: PopupMoneyTransferComponent,
        data: {
          type: ScreenType.Detail,
        },
      }
    ],
  },
  {
    path: 'reduction-proposal',
    data: {
      code: FunctionCode.REDUCTION_PROPOSAL,
    },
    canActivateChild: [RoleGuardService],
    children: [
      {
        path: '',
        component: ReductionProposalListViewComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
      {
        path: 'create',
        component: CreateReductionProposalComponent,
        data: {
          scope: Scopes.CREATE,
        },
      },
      {
        path: 'detail',
        component: DetailReductionProposalComponent,
        data: {
          type: ScreenType.Detail,
        },
      },
      {
        path: 'update',
        component: UpdateReductionProposalComponent,
        data: {
          type: ScreenType.Update,
        },
      },
      {
        path: 'create-setup',
        component: CreateSetupProposalComponent,
        data: {
          scope: Scopes.CREATE,
        },
      },
      {
        path: 'update-setup',
        component: UpdateSetupProposalComponent,
        data: {
          type: ScreenType.Update
        },
      },
      {
        path: 'detail-setup',
        component: DetailSetupProposalComponent,
        data: {
          type: ScreenType.Detail,
        },
      },
    ],
  },
  {
    path: 'debt-claims',
    data: {
      code: FunctionCode.DEBT_CLAIM,
    },
    canActivateChild: [RoleGuardService],
    children: [
      {
        path: '',
        component: ConstructionListViewComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
      {
        path: 'create',
        component: CreateConstructionComponent,
        data: {
          scope: Scopes.CREATE,
        },
      },
      {
        path: 'info',
        component: ConstructionInfoComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
    ],
  },
  {
    path: 'cash-flow-customer',
    data: {
      code: FunctionCode.CASH_FLOW_CUSTOMER,
    },
    canActivateChild: [RoleGuardService],
    children: [
      {
        path: '',
        component: CashFlowCustomerListComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
    ],
  },
  {
    path: 'fee-exception',
    children: [
      {
        path: 'create',
        component: CreateFeeExceptionComponent,
      },
    ],
  },
  {
    path: 'interest-exception',
    children: [
      {
        path: 'create',
        component: CreateExceptionComponent,
      },
      {
        path: 'update',
        component: UpdateExceptionComponent,
      },
      {
        path: 'detail',
        component: DetailExceptionComponent,
      },
    ],
  },
  {
    path: 'selling-opp-sme',
    children: [
      {
        path: 'create',
        component: SellingOppSmeCreateComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SaleManagerRoutingModule {}
