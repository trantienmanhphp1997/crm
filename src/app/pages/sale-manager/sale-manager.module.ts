import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { RadioButtonModule } from 'primeng/radiobutton';
import { CheckboxModule } from 'primeng/checkbox';
import { InputNumberModule } from 'primeng/inputnumber';
import { NgxSelectModule } from 'ngx-select-ex';
import { TableModule } from 'primeng/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MultiSelectModule } from 'primeng/multiselect';
import { SelectButtonModule} from 'primeng/selectbutton';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { BadgeModule } from 'primeng/badge';
import { ChipModule } from 'primeng/chip';
import { AvatarModule } from 'primeng/avatar';
import { TimelineModule } from 'primeng/timeline';
import { MenuModule } from 'primeng/menu';
import { SliderModule } from 'primeng/slider';
import { SaleManagerRoutingModule } from './sale-manager-routing.module';
import { SaleManagerListComponent } from './components/sale-manager-list/sale-manager-list.component';
import { SaleManagerCreateComponent } from './components/sale-manager-create/sale-manager-create.component';
import { ProductModalComponent } from './components/product-model/product-modal.component';
import { APIS } from './api';
import { SaleManagerDetailComponent } from './components/sale-manager-detail/sale-manager-detail.component';
import { SaleTargetListComponent } from './components/sale-target-list/sale-target-list.component';
import { SaleTargetUpdateComponent } from './components/sale-target-update/sale-target-update.component';
import { SaleTransferListComponent } from './components/sale-transfer-list/sale-transfer-list.component';
import { SaleTransferDetailComponent } from './components/sale-transfer-detail/sale-transfer-detail.component';
import { SaleSuggestListComponent } from './components/sale-suggest-list/sale-suggest-list.component';
import { FormatNumberToMillionPipe } from './pipes/format-number-to-million.pipe';
import {SaleTrackingReportComponent} from './components/sale-tracking-report/sale-tracking-report.component';
import {AddressComponent} from './components/address/address.component';
import { InfoBasicComponent } from './components/address/info-basic/info-basic.component';
import { InfoContactComponent } from './components/address/info-contact/info-contact.component';
import { InfoRelationshipOCIComponent } from './components/address/info-relationship-OCI/info-relationship-oci.component';
import { CustomerLoanBalanceComponent } from './components/customer-loan-balance/customer-loan-balance.component';
import { CustomerSmeCollateralComponent } from './components/customer-sme-collateral/customer-sme-collateral.component';
import { CustomerSmeObligationOtherComponent } from './components/customer-sme-obligation-other/customer-sme-obligation-other.component';
import { CustomerReportFinanceAddressComponent } from './components/address/info-OCI-MB/customer-report-finance-address/customer-report-finance-address.component';
import { CustomerCollateralAddressComponent } from './components/address/customer-collateral-address/customer-collateral-address.component'
import { InfoOCIMBComponent } from './components/address/info-OCI-MB/info-OCI-MB.component';
import { TooltipModule } from 'primeng/tooltip';
import { ModalConfirmCreateChatComponent } from './components/modal-confirm-create-chat/modal-confirm-create-chat.component';
import { TabViewModule } from 'primeng/tabview';
import { MatStepperModule } from '@angular/material/stepper';
import { BiddingListComponent } from './components/bidding-list/bidding-list.component';
import { LimitCustomerComponent } from './components/limit-customer/limit-customer.component';
import {MatMenuModule} from '@angular/material/menu';
import { SellingOppIndivComponent } from './components/selling-opp-indiv/selling-opp-indiv.component';
import {SellingOppIndivDetailV2Component} from './components/selling-opp-indiv-detail-v2/selling-opp-indiv-detail.component';
import {SellingOppIndivCreateEditModalComponent} from './components/selling-opp-indiv-create-edit-modal/selling-opp-indiv-create-edit-modal-component';
import {SellingOppIndivDetailItemV2Component} from './components/selling-opp-indiv-detail-item-v2/selling-opp-indiv-detail-item.component';
import { SellingOppIndivCreateComponent } from './components/selling-opp-indiv-create/selling-opp-indiv-create.component';
import {GroupMbchatModalComponent} from "./components/list-group-mbchat-modal/group-mbchat-modal.component";
import { CreateReductionProposalComponent } from './components/reduction-proposal/create/create.component';
import { ChoosePotentialModalComponent } from './components/reduction-proposal/dialog/choose-create-modal/choose-potential-modal.component';
import { CreateProposalComponent } from './components/reduction-proposal/create-proposal/create-proposal.component';
import { ToiConfirmModalComponent } from './components/reduction-proposal/dialog/toi-confirm-modal/toi-confirm-modal.component';
import { ChooseCustomerModalComponent } from './components/reduction-proposal/dialog/choose-customer-modal/choose-customer-modal.component';
import { InterestModalComponent } from './components/reduction-proposal/dialog/select-interest-modal/select-interest-modal.component';
import { FreeDiscountAnnexComponent } from './components/reduction-proposal/annex/annex.component';
import { OptionCodeModalComponent } from './components/reduction-proposal/dialog/select-optioncode-modal/select-optioncode-modal.component';
import { LdCodeModalComponent } from './components/reduction-proposal/dialog/select-ldcode-modal/select-ldcode-modal.component';
import { ConfirmAuthorizationModalComponent } from './components/reduction-proposal/dialog/confirm-authorization-modal/confirm-authorization-modal.component';
import { DetailReductionProposalComponent } from './components/reduction-proposal/detail/detail.component';
import { UpdateReductionProposalComponent } from './components/reduction-proposal/update/update.component';
import { WarningAuthorizationModalComponent } from './components/reduction-proposal/dialog/warning-authorization-modal/warning-authorization-modal.component';
import { ReductionProposalListViewComponent } from './components/reduction-proposal/reduction-propsal-list-view/reduction-proposal-list-view.component';
import { AuthorizationInformationComponent } from './components/reduction-proposal/authorization-information/authorization-information.component';
import { ManagerConfirmModalComponent } from './components/reduction-proposal/dialog/manager-confirm-modal/manager-confirm-modal.component';
import { PreviewModalComponent } from './components/reduction-proposal/dialog/preview-modal/preview-modal.component';
import { ConfirmReductionComponent } from './components/reduction-proposal/confirm-reduction/confirm-reduction.component';
import { TreeTableModule } from 'primeng/treetable';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { MatDialogModule } from '@angular/material/dialog';
import {MatCardModule} from '@angular/material/card';
import {MatExpansionModule} from '@angular/material/expansion';
import { ManagerMoneyTransferComponent } from './components/money-transfer/manager-money-transfer/manager-money-transfer.component';
import { PopupMoneyTransferComponent } from './components/money-transfer/popup-money-transfer/popup-money-transfer.component';
import { SelectMdModalComponent } from './components/reduction-proposal/dialog/select-md-modal/select-md-modal.component';
import { SelectFeeModalComponent } from './components/reduction-proposal/dialog/select-fee-modal/select-fee-modal.component';
import { ConstructionInfoComponent } from './components/cash-flow-management/construction-info/construction-info.component';
import { ConstructionGeneralComponent } from './components/cash-flow-management/construction-general/construction-general.component';
import { ConstructionCashflowTrackingComponent } from './components/cash-flow-management/construction-cashflow-tracking/construction-cashflow-tracking.component';
import { ConstructionListViewComponent } from './components/cash-flow-management/construction-list-view/construction-list-view.component';
import { ConfirmUnassignlModalComponent } from './components/cash-flow-management/confirm-unassign-modal/confirm-unassign-modal.component';
import { CreateConstructionComponent } from './components/cash-flow-management/create-construction/create-construction.component';
import { CreateSetupProposalComponent } from './components/reduction-proposal/setup/create/create-setup.component';
import { DeclareSetupProposalComponent } from './components/reduction-proposal/setup/declare/declare-setup.component';
import { SetupExistedModalComponent } from './components/reduction-proposal/setup/dialog/setup-existed-modal/setup-existed-modal.component';
import { DetailSetupProposalComponent } from './components/reduction-proposal/setup/detail/detail-setup.component';
import { UpdateSetupProposalComponent } from './components/reduction-proposal/setup/update/update-setup.component';
import { CashFlowCustomerListComponent } from './components/cash-flow-customer-management/cash-flow-customer-list/cash-flow-customer-list.component';
import { AssignCollateralAssetCodeModalComponent } from './components/cash-flow-customer-management/dialog/assign-collateral-asset-code-modal/assign-collateral-asset-code-modal.component'
import { CreateFeeExceptionComponent } from './components/fee-exception/create/create/create.component';
import {
  UpdateExceptionComponent
} from './components/reduction-proposal/interest-exception/turning/update/update-exception/update-exception.component';
import {
  DetailExceptionComponent
} from './components/reduction-proposal/interest-exception/turning/detail/detail-exception/detail-exception.component';
import {
  CreateExceptionComponent
} from './components/reduction-proposal/interest-exception/turning/create/create-exception/create-exception.component';
import { CreateValuationReportModalComponent } from './components/cash-flow-management/create-valuation-report-modal/create-valuation-report-modal.component';
import { SellingOppSmeDetailComponent } from './components/selling-opp-sme-detail/selling-opp-sme-detail.component';
import { ActivityLogModule } from '../customer-360/activity-log.module';
import { SellingOppSmeCreateComponent } from './components/selling-opp-sme-create/selling-opp-sme-create.component';
import { DialogConfirmComponent } from './components/selling-opp-indiv-create/dialog-confirm/dialog-confirm.component';


const DIALOGS = [PreviewModalComponent];

const COMPONENTS = [
  SaleManagerListComponent,
  SaleManagerCreateComponent,
  ProductModalComponent,
  SaleManagerDetailComponent,
  SaleTargetListComponent,
  SaleTargetUpdateComponent,
  SaleTransferListComponent,
  SaleTransferDetailComponent,
  SaleSuggestListComponent,
  FormatNumberToMillionPipe,
  SaleTrackingReportComponent,
  AddressComponent,
  InfoBasicComponent,
  InfoContactComponent,
  InfoRelationshipOCIComponent,
  CustomerLoanBalanceComponent,
  CustomerSmeCollateralComponent,
  CustomerSmeObligationOtherComponent,
  InfoOCIMBComponent,
  CustomerReportFinanceAddressComponent,
  CustomerCollateralAddressComponent,
  BiddingListComponent,
  LimitCustomerComponent,
  ModalConfirmCreateChatComponent,
  SellingOppIndivComponent,
  SellingOppIndivDetailV2Component,
  SellingOppIndivCreateEditModalComponent,
  SellingOppIndivDetailItemV2Component,
  SellingOppIndivCreateComponent,
  GroupMbchatModalComponent,
  CreateReductionProposalComponent,
  ChoosePotentialModalComponent,
  CreateProposalComponent,
  ToiConfirmModalComponent,
  ChooseCustomerModalComponent,
  InterestModalComponent,
  FreeDiscountAnnexComponent,
  OptionCodeModalComponent,
  LdCodeModalComponent,
  ConfirmAuthorizationModalComponent,
  DetailReductionProposalComponent,
  UpdateReductionProposalComponent,
  WarningAuthorizationModalComponent,
  ReductionProposalListViewComponent,
  AuthorizationInformationComponent,
  ManagerConfirmModalComponent,
  ConfirmReductionComponent,
  SelectMdModalComponent,
  SelectFeeModalComponent,
  ConstructionInfoComponent,
  ConstructionGeneralComponent,
  ConstructionCashflowTrackingComponent,
  ConstructionListViewComponent,
  CreateConstructionComponent,
  ConfirmUnassignlModalComponent,
  CreateValuationReportModalComponent,
  CreateSetupProposalComponent,
  DeclareSetupProposalComponent,
  SetupExistedModalComponent,
  DetailSetupProposalComponent,
  CashFlowCustomerListComponent,
  AssignCollateralAssetCodeModalComponent,
  UpdateSetupProposalComponent,
  CreateFeeExceptionComponent,
  SellingOppSmeCreateComponent,
  DialogConfirmComponent,
  SellingOppSmeDetailComponent
];

@NgModule({
  declarations: [...COMPONENTS, ...DIALOGS, ManagerMoneyTransferComponent, PopupMoneyTransferComponent, UpdateExceptionComponent, DetailExceptionComponent, CreateExceptionComponent],
  imports: [
    CommonModule,
    SaleManagerRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    // HttpClientModule,
    SharedModule,
    NgbModule,
    TranslateModule,
    NgxDatatableModule,
    InputTextModule,
    InputTextareaModule,
    DropdownModule,
    CalendarModule,
    RadioButtonModule,
    CheckboxModule,
    InputNumberModule,
    NgxSelectModule,
    TableModule,
    MatTabsModule,
    MultiSelectModule,
    AvatarModule,
    ButtonModule,
    RippleModule,
    BadgeModule,
    ChipModule,
    TimelineModule,
    MenuModule,
    SelectButtonModule,
    TabViewModule,
    MatStepperModule,
    TooltipModule,
    SliderModule,
    MatMenuModule,
    TreeTableModule,
    TableModule,
    MatCardModule,
    MatExpansionModule,
    NgxExtendedPdfViewerModule,
    MatDialogModule,
    ActivityLogModule
  ],
  providers: [...APIS],
  entryComponents: [...COMPONENTS],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class SaleManagerModule {}
