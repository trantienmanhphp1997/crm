import { Validators } from '@angular/forms';
import { Component, OnInit, Injector, ViewChildren } from '@angular/core';
import { Observable, forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CustomerApi, CustomerDetailApi } from 'src/app/pages/customer-360/apis';
import { ReductionProposalApi } from 'src/app/pages/sale-manager/api/reduction-proposal.api';
import * as moment from 'moment';
import { Utils } from 'src/app/core/utils/utils';
import * as _ from 'lodash';
import { ToiConfirmModalComponent } from '../../../reduction-proposal/dialog/toi-confirm-modal/toi-confirm-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { SelectionType } from '@swimlane/ngx-datatable';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SelectFeeModalComponent } from '../../../reduction-proposal/dialog/select-fee-modal/select-fee-modal.component';
import { OptionCodeModalComponent } from '../../../reduction-proposal/dialog/select-optioncode-modal/select-optioncode-modal.component';
import { LdCodeModalComponent } from '../../../reduction-proposal/dialog/select-ldcode-modal/select-ldcode-modal.component';
import { SelectMdModalComponent } from '../../../reduction-proposal/dialog/select-md-modal/select-md-modal.component';
import { FileService } from 'src/app/core/services/file.service';
import {
  CommonCategory,
  Division,
  FunctionCode,
  functionUri,
  Scopes,
  SessionKey
} from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { rejects } from 'assert';
@Component({
  selector: 'app-create-fee-exception',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateFeeExceptionComponent extends BaseComponent implements OnInit {
  @ViewChildren('errorMessage') errorMessage: any | HTMLElement;
  allScopeApplys = [
    { code: 'ALL', displayName: 'Tất cả các phương án', caseSuggest: 'ALL', typeNum: 0 },
    { code: 'OPTION_CODE', displayName: 'Mã phương án hạn mức', caseSuggest: 'ALL', typeNum: 1 },
    { code: 'LD_CODE', displayName: 'Mã LD', caseSuggest: 'ALL', typeNum: 2 },
    { code: 'MD_CODE', displayName: 'Mã MD', caseSuggest: 'FEE', typeNum: 4 },
    // { code: 'UNKNOWN', displayName: 'Chưa xác định mã phương án', caseSuggest: 'ALL', typeNum: 3 }
  ];

  scopeApplysKhTn = [
    { code: 'ALL', displayName: 'Tất cả các phương án', caseSuggest: 'ALL', typeNum: 0 },
    { code: 'OPTION_CODE', displayName: 'Mã phương án hạn mức', caseSuggest: 'ALL', typeNum: 1 },
    // { code: 'UNKNOWN', displayName: 'Chưa xác định mã phương án', caseSuggest: 'ALL', typeNum: 3 }
  ];

  effects = [];
  form = this.fb.group({
    scopeApply: null,
    monthApply: null,
    approvedDateMO: null,
    userName: null,
    commitment: true,
    toi: null,
    reason: 1,
    codeHTCNTT: null,
    moCode: [null, [Validators.required, Validators.pattern(/^[^!@#$%\^&*()+_{:?<>]+$/)]],
    fullName: null,
    titleCategory: null,
    segment: null
  });
  info: any;
  customer360Fn: any;
  stateRouter: any;
  isManage: boolean;
  dataTable = [];
  reason = [
    { code: 1, displayName: 'Hệ thống lỗi' },
    { code: 2, displayName: 'Tờ trình trước khi golive hệ thống' },
    { code: 3, displayName: 'Trình giảm theo nhóm khách hàng' },
    { code: 4, displayName: 'Các ngoại lệ khác theo đặc thù của khách hàng và ĐVKĐ (biểu thông thường không review/ trình giảm vượt mức tối đa,..)' },
    { code: 5, displayName: 'Ngoại lệ TOI của KH (KH không đủ TOI tối thiểu), phân khúc' },
  ]
  maxDate = new Date();
  maxSize = 20;
  listFile = [];
  clickSave: boolean;
  dataCommon = {};
  isSelectFee: boolean;
  defaultDate = moment().format('DD/MM/YYYY');
  modeView: boolean;
  params: any;
  isRm: boolean;
  customerHaveCode: boolean;
  isSegment: boolean;
  dtttrrDisplay: any;

  constructor(Injector: Injector,
    private customerDetailApi: CustomerDetailApi,
    private reductionProposalApi: ReductionProposalApi,
    private customerApi: CustomerApi,
    public dialog: MatDialog,
    private modal: NgbModal,
    private fileService: FileService,
    private categoryService: CategoryService,

  ) {
    super(Injector);
    for (let index = 1; index <= 12; index++) {
      this.effects.push(
        { displayName: index, code: index },
      )
    };
    this.customer360Fn = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.stateRouter = this.router.getCurrentNavigation().extras.state || {};
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REDUCTION_PROPOSAL}`);
    this.params = this.route.snapshot.queryParams;
    this.customerHaveCode = this.stateRouter.formSearch?.customerType != 0;
    console.log(this.stateRouter);
    console.log(this.params);
  }

  ngOnInit() {
    this.form.controls.scopeApply.valueChanges.subscribe(data => {
      this.dataTable = [];
    });
    this.modeView = this.params?.mode == 'details';
    if (this.params?.mode == 'details' || this.params?.mode == 'edit') {
      this.getDetails(this.params?.id);
      return;
    }
    this.getData('create');
    this.form.controls.scopeApply.setValue('ALL');
  }

  onDragOver(event): void {
    event.preventDefault();
  }

  onDropSuccess(event): void {
    const lengthFile = 1;
    const allows = ['.pdf'];
    event.preventDefault();
    if (this.listFile?.length + event.dataTransfer.files?.length > lengthFile) {
      this.messageService.error(`Chỉ được chọn tối đa ${lengthFile} file đính kèm}`);
      return;
    }
    let countMaxData = 0;
    let countNotAllow = 0;
    let errMessage = '';
    let maxSizeByte = this.maxSize * 1024 * 1024;
    for (let file of event.dataTransfer.files) {
      const name = file.name;
      if (file?.size <= maxSizeByte && allows.includes(name.substr(name.lastIndexOf('.')).toLowerCase())) {
        this.upload(file);
      } else if (file?.size > maxSizeByte) {
        countMaxData++;
      } else if (!allows.includes(name.substr(name.lastIndexOf('.')))) {
        countNotAllow++;
      }
    }
    if (countMaxData > 0) {
      errMessage = "Dung lượng file không được vượt quá 20MB";
    }
    if (countNotAllow > 0) {
      errMessage = "File không đúng định dạng cho phép (.pdf)";
    }
    if (errMessage?.length > 0) {
      this.messageService.warn(errMessage);
    }
  }

  changeFile(event) {
    const lengthFile = 1;
    const allows = ['.pdf'];
    if (this.listFile?.length + event.target.files?.length > lengthFile) {
      this.messageService.error(`Chỉ được chọn tối đa ${lengthFile} file đính kèm`);
      return;
    }
    let countMaxData = 0;
    let countNotAllow = 0;
    let errMessage = '';
    let maxSizeByte = this.maxSize * 1024 * 1024;
    for (let file of event.target.files) {
      const name = file.name;
      if (file?.size <= maxSizeByte && allows.includes(name.substr(name.lastIndexOf('.')).toLowerCase())) {
        this.upload(file);
      } else if (file?.size > maxSizeByte) {
        countMaxData++;
      } else if (!allows.includes(name.substr(name.lastIndexOf('.')))) {
        countNotAllow++;
      }
    }
    if (countMaxData > 0) {
      errMessage = "Dung lượng file không được vượt quá 20MB";
    }
    if (countNotAllow > 0) {
      errMessage = "File không đúng định dạng cho phép (.pdf)";
    }
    if (errMessage.length > 0) {
      this.messageService.warn(errMessage);
    }
  }

  openDownloadFilePopup(inputFile): void {
    inputFile.value = '';
    inputFile.click();
  }

  openFeeModal(type?: string, row?) {
    const config = {
      selectionType: SelectionType.multi
    };
    const modal = this.modal.open(SelectFeeModalComponent, { windowClass: 'list__interest-modal' });
    modal.componentInstance.config = config;
    modal.componentInstance.department = this.stateRouter?.formSearch?.blockCode || this.info?.blockCode;
    modal.componentInstance.isManage = this.isManage;
    modal.componentInstance.code = row?.code || '';
    modal.componentInstance.isKhdn = true;
    modal.componentInstance.customerCreditType = this.info?.customerType;
    modal.componentInstance.maxLength = type == 'option' ? 50 : null;
    modal.componentInstance.lastSelectedType = _.last(this.dataTable)?.chargeGroup || '';
    modal.componentInstance.listCode = type == 'fee' ?
      this.dataTable.map(item => { return item.productCode; })
      : this.dataTable?.filter(elm => elm?.code === row.code).map(elm => elm.productCode);
    modal.componentInstance.reload(0);
    modal.result.then((res) => {
      // const Data = new Set(res.listSelected);
      // const listSelected = [...Data];
      res.listSelected = _.unionBy(res.listSelected, 'productCode');
      if (type == 'option') {
        let list = res.listSelected?.map(item => {
          item.rateCost = item?.chgAmtFrom ? 0 : '--';
          item.rateMin = item?.minCharge ? 0 : '--';
          item.rateMax = item?.maxCharge ? 0 : '--';
          item.suggestCost = item?.chgAmtFrom;
          item.suggestMin = item?.minCharge;
          item.suggestMax = item?.maxCharge;
          item.suggestCostOrigin = item?.chgAmtFrom;
          item.suggestMinOrigin = item?.minCharge;
          item.suggestMaxOrigin = item?.maxCharge;
          item.isChooseFee = true;
          delete item.status;
          return item;
        });
        // giữ lại những cái productCode có trong listcode
        list = [...list, ...this.dataTable.filter(item => item.productCode && res.listCode.includes(item.productCode))];
        if (!list.length) {
          this.isSelectFee = false;
          this.dataTable = this.dataTable.filter((item: any) => {
            return item.code != row.code;
          });
          this.dataTable.push({ code: row.code, descriptionVn: 'Chọn biểu phí' })
          return;
        }
        this.isSelectFee = true;
        const code = list[0]?.code;

        this.dataTable = this.dataTable.filter((item: any) => {
          return item.code != code;
        });
        this.dataTable = [...this.dataTable, ...list];
        this.dataTable = _.unionBy(this.dataTable, 'productCode');
        return;
      }

      const listSelected = res.listSelected.filter?.(item => item.productCode).map(item => {
        item.rateCost = item?.chgAmtFrom ? 0 : '--';
        item.rateMin = item?.minCharge ? 0 : '--';
        item.rateMax = item?.maxCharge ? 0 : '--';
        item.suggestCost = item?.chgAmtFrom;
        item.suggestMin = item?.minCharge;
        item.suggestMax = item?.maxCharge;
        item.code = item.productCode;
        item.isChooseFee = true;
        delete item.status;
        return item; 
      });
      // giữ lại những cái productCode có trong listcode
      this.dataTable = this.dataTable.filter(item => res.listCode.includes(item.productCode));
      this.dataTable = [...this.dataTable, ...listSelected];
      this.dataTable = _.unionBy(this.dataTable, 'productCode');

      if (!this.dataTable.length) {
        this.isSelectFee = false;
        return;
      }
      this.isSelectFee = true;
    });
  }

  openOptionCodeModal() {
    const config = {
      selectionType: SelectionType.multi
    };
    const listCode = this.dataTable?.map((item) => item.optionCode || item.code);
    const modal = this.modalService.open(OptionCodeModalComponent, { windowClass: 'list__optioncode-modal' });
    modal.componentInstance.config = config;
    modal.componentInstance.customerCode = this.info?.customerCode;
    modal.componentInstance.businessRegistrationNumber = this.info?.businessRegistrationNumber || this.info?.taxCode;
    modal.componentInstance.idCard = this.info?.idCard;
    modal.componentInstance.type = 0;
    modal.componentInstance.blockCode = this.stateRouter?.formSearch?.blockCode || this.info?.blockCode;
    modal.componentInstance.listCode = _.cloneDeep(listCode);
    modal.result.then((res) => {
      if (res) {
        const listAdd = res.listSelected.filter?.(item => item.optionCode && !listCode.includes(item.optionCode)).map(item => {
          item.code = item.optionCode;
          return item;
        });
        this.dataTable = [...this.dataTable, ...listAdd];
      }
    }).catch(() => {
    });
  }

  openLdCodeModal() {
    this.isLoading = true;
    this.reductionProposalApi.getLDCode(this.info?.customerCode).pipe(catchError(() => of(undefined))).subscribe(res => {
      this.isLoading = false;
      const config = {
        selectionType: SelectionType.multi
      };
      const modal = this.modalService.open(LdCodeModalComponent, { windowClass: 'list__ldcode-modal' });
      const listCode = this.dataTable?.map((item) => item.ldCode || item.code);
      modal.componentInstance.config = config;
      modal.componentInstance.listLD = res;
      modal.componentInstance.listCode = _.cloneDeep(listCode);
      modal.componentInstance.customerCode = this.info?.customerCode;
      modal.componentInstance.reload();
      modal.result.then((res) => {
        if (res) {
          const listAdd = res.listSelected.filter?.(item => item.ldCode && !listCode.includes(item.ldCode)).map(item => {
            item.code = item.ldCode;
            return item;
          });
          this.dataTable = [...this.dataTable, ...listAdd];
        }
      }).catch(() => {
      });
    }, err => { this.isLoading = false });
  }

  openMdCodeModal() {
    this.isLoading = true;
    this.reductionProposalApi.getMDCode(this.info?.customerCode, this.info?.taxCode).pipe(catchError(() => of(undefined))).subscribe(res => {
      const config = {
        selectionType: SelectionType.multi
      };
      this.isLoading = false;
      const listCode = this.dataTable?.map((item) => item.ARRANGEMENT_NBR || item.code);
      const modal = this.modalService.open(SelectMdModalComponent, { windowClass: 'list__mdcode-modal' });
      modal.componentInstance.config = config;
      modal.componentInstance.listMD = res;
      modal.componentInstance.listCode = _.cloneDeep(listCode);
      modal.componentInstance.customerCode = this.info?.customerCode;
      modal.componentInstance.reload();
      modal.result.then((res) => {
        if (res) {
          const listAdd = res.listSelected.filter?.(item => item.ARRANGEMENT_NBR && !listCode.includes(item.ARRANGEMENT_NBR)).map(item => {
            item.code = item.ARRANGEMENT_NBR;
            return item;
          });
          this.dataTable = [...this.dataTable, ...listAdd];
        }
      }).catch(() => {
      });
    }, err => {
      this.isLoading = false;
    })
  }

  getData(type?) {
    if (type == 'getDetails') {
      this.customerHaveCode = this.stateRouter['formSearch']?.customerCode;
    }
    const body = {
      "customerCode": this.stateRouter?.formSearch?.customerCode?.trim?.(),
      "taxCode": this.stateRouter?.formSearch?.taxCode,
      "blockCode": this.stateRouter?.formSearch?.blockCode || this.info?.blockCode
    };
    const searchSmeBody = {
      pageNumber: 0,
      pageSize: 10,
      rsId: this.customer360Fn?.rsId,
      scope: 'VIEW',
      customerType: '',
      search: this.stateRouter?.formSearch?.customerCode?.trim?.(),
      searchFastType: 'CUSTOMER_CODE',
      manageType: 1,
      customerTypeSale: 1
    };
    const param = {
      customerCode: this.stateRouter?.formSearch?.customerCode?.trim?.(),
      businessRegistrationNumber: '',
      loanId: ''
    };
    const CustomerCredit = {
      cif: this.stateRouter?.formSearch?.customerCode?.trim?.(),
      taxId: '',
      nationalId: '',
      lob: this.stateRouter?.formSearch?.blockCode || this.info?.blockCode
    }
    this.isLoading = true;
    return new Promise(async (resolve, reject) => {
      this.categoryService.getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW).pipe(catchError((e) => of(undefined))).subscribe(res => {
        this.isRm = _.isEmpty(res);
      })
      if (this.customerHaveCode) {
        forkJoin([
          this.reductionProposalApi.findExistingCustomer(body).pipe(catchError(() => of(undefined))),
          this.customerDetailApi.getByCode(this.stateRouter?.formSearch?.customerCode).pipe(catchError(() => of(undefined))),
          this.customerApi.searchSme(searchSmeBody).pipe(catchError(() => of(undefined))),
          this.reductionProposalApi.getScopeReduction(param).pipe(catchError(() => of(undefined))),
          this.reductionProposalApi.getToiExisting(this.stateRouter?.formSearch?.customerCode).pipe(catchError(() => of(undefined))),
          this.reductionProposalApi.getCustomerCredit(CustomerCredit).pipe(catchError(() => of(undefined))),
        ]).subscribe(([findExistingCustomer, getByCode, searchSme, getScopeReduction, getToiExisting, getCustomerCredit]) => {
          const customerType = getCustomerCredit?.data?.customerType;
          this.info = { ...findExistingCustomer, ...getByCode, ...{ searchSme: searchSme[0] }, ...{ customerName: searchSme[0]?.customerName }, ...getScopeReduction, ...getToiExisting, ...{ customerType: customerType } };
          console.log(this.info);
          this.isManage = this.currUser.branch === this.info?.branchCode;
          this.isLoading = false;
          resolve(true);
        }, error => {
          reject(error);
        });
        return;
      }
      if (type == 'create') {
        this.stateRouter.findPotentialCustomer['customerName'] = this.stateRouter?.findPotentialCustomer?.fullName;
        this.info = { ...this.stateRouter.findPotentialCustomer };
      }
      const paramScopeReduction = {
        businessRegistrationNumber: type == 'create' ? this.info?.businessRegistrationNumber : this.stateRouter['formSearch']?.businessRegistrationNumber,
        loanId: ''
      };
      this.isManage = true;
      forkJoin([this.commonService.getCommonCategory(CommonCategory.REVENUE_CUSTOMER_SEGMENT).pipe(catchError(() => of(undefined))),
      this.reductionProposalApi.getScopeReduction(paramScopeReduction).pipe(catchError(() => of(undefined))),
      this.reductionProposalApi.getKhdnPotentialCustomerSegment(this.info?.taxCode || this.stateRouter['formSearch']?.taxCode).pipe(catchError(() => of(undefined))),
      this.reductionProposalApi.findPotentialCustomer(body).pipe(catchError(() => of(undefined)))
      ]).subscribe(([REVENUE_CUSTOMER_SEGMENT, getScopeReduction, getKhdnPotentialCustomerSegment, findPotentialCustomer]) => {
        this.isLoading = false;
        this.dataCommon['REVENUE_CUSTOMER_SEGMENT'] = REVENUE_CUSTOMER_SEGMENT.content;
        this.info = { ...this.info, ...getScopeReduction, ...getKhdnPotentialCustomerSegment, ...findPotentialCustomer, customerName: findPotentialCustomer?.fullName };
        if (getKhdnPotentialCustomerSegment?.data.length && getKhdnPotentialCustomerSegment?.data[0]?.cstSegStd) {
          this.form.controls.segment.setValue(getKhdnPotentialCustomerSegment?.data[0]?.cstSegStd);
          this.isSegment = true;
        } else {
          this.isSegment = false;
        }
        this.info.customerType = 'Phi tín dụng';
        console.log(this.info);
        resolve(true)
      });
    });

  }
  close() {
    this.router.navigateByUrl(functionUri.reduction_proposal, { state: { formSearch: this.state?.search } });
  }

  generateRelationshipTimeWithMB(item) {
    const date = _.get(item, 'customerInfo.openCodeDate');
    if (Utils.isStringNotEmpty(date)) {
      const totalMonth = moment().diff(moment(date, 'DD/MM/YYYY'), 'months');
      return totalMonth > 12
        ? `${Math.floor(totalMonth / 12)} ${_.get(this.fields, 'year')}`
        : `${totalMonth} ${_.get(this.fields, 'month')}`;
    } else {
      return `0 ${_.get(this.fields, 'year')}`;
    }
  }

  openPopupCalculateTOI(): void {
    const dialogRef = this.dialog.open(ToiConfirmModalComponent);
    dialogRef.componentInstance.type = 0;
    dialogRef.componentInstance.action = this.modeView ? null : 'default';
    dialogRef.componentInstance.blockCode = this.stateRouter?.formSearch?.blockCode || this.info?.blockCode;
    dialogRef.afterClosed().subscribe((res) => {
      if (res == 'closeModal') {
        return;
      }
      const info: any = this.reductionProposalApi?.info?.value;
      const toi = info?.toi;
      this.info.toiCalculate = toi;
      if (toi) {
        this.form.controls.toi.setValue(toi?.toi);
        this.dtttrrDisplay = toi?.dtttrr.toString().replaceAll?.(',', '.');
      }
    });
  }

  convertDtttrr() {
    if (this.dtttrrDisplay) {
      return (+this.dtttrrDisplay / 1000000).toFixed(2);
    }
    return null;
  }

  changeSuggestCost(ev, item) {
    const value = ev.value || item.suggestCost;
    // if(!value){
    //   item.suggestCost = _.cloneDeep(item.suggestCostOrigin);
    // }
    if (!item?.chgAmtFrom) {
      return item.rateCost = '--'
    }
    item.rateCost = ((item?.chgAmtFrom - value) * 100 / (item?.chgAmtFrom)).toFixed(2);
  }

  changeSuggestMin(ev, item) {
    const value = ev.value || item.suggestMin;
    if (!item?.minCharge) {
      return item.rateMin = '--'
    }
    item.rateMin = ((item?.minCharge - value) * 100 / (item?.minCharge)).toFixed(2);
  }

  changeSuggestMax(ev, item) {
    const value = ev.value || item.suggestMax;
    if (!item?.maxCharge) {
      return item.rateMax = '--'
    }
    item.rateMax = ((item?.maxCharge - value) * 100 / (item?.maxCharge)).toFixed(2);
  }

  removeItem(item) {
    this.dataTable = this.dataTable.filter(list => list.productCode != item.productCode || list.code != item.code);
  }

  upload(file: any) {
    this.isLoading = true;
    const formData = new FormData();
    formData.append('file', file);
    this.fileService.uploadFile(formData).subscribe(res => {
      const mapFile = {
        "fileId": res,
        "fileName": file.name,
        "type": '',
        "filePath": ""
      };
      this.listFile.push(mapFile);
      this.isLoading = false;
    }, err => {
      this.isLoading = false;
      this.messageService.error('Có lỗi xảy ra, vui lòng thử lại sau');
    });
  }

  download(file: any) {
    const title = file?.fileName;
    this.isLoading = true;
    this.fileService.downloadFile(file?.fileId, title).subscribe((res) => {
      if (!res) {
        this.messageService.error('Có lỗi xảy ra, vui lòng thử lại sau');
      };
      this.isLoading = false;
    }, err => {
      this.isLoading = false;
      this.messageService.error('Có lỗi xảy ra, vui lòng thử lại sau');
    });
  }

  removeFile(id) {
    this.listFile = this.listFile.filter(list => list.fileId != id);
  }

  async save(isConfirm?): Promise<any> {
    this.clickSave = true;
    const formValue = this.form.getRawValue();
    if (!formValue.commitment) {
      this.messageService.warn('Vui lòng xác nhận cam kết');
      return;
    }
    if (!this.checkFee() || !this.checkFile() || !this.checkRequired() || this.checkHTMLerror()) {
      return;
    }
    this.isLoading = true;
    let payload = {
      "blockCode": this.stateRouter?.formSearch?.blockCode || this.info?.blockCode,
      "businessActivities": '',
      "code": '',
      "competitiveness": '',
      "customerCode": this.info?.customerCode,
      "monthApply": formValue?.monthApply,
      "monthConfirm": 6,
      "moDocId": "",
      "nimConfirm": '',
      "nimExisting": '',
      "planInformation": '',
      "resultBusiness": '',
      "scopeApply": formValue?.scopeApply,
      "scopeType": this.allScopeApplys.find(item => item.code == formValue?.scopeApply)?.typeNum,
      "dtttrrConfirm": this.dtttrrDisplay,
      "toiConfirm": formValue?.toi,
      "taxCode": this.info?.taxCode,
      "leadCode": this.stateRouter?.findPotentialCustomer?.leadCode,
      "type": 4,
      "reductionProposalDetailsDTOList": this.dataTable.map(item => {
        return {
          'mdCode': formValue.scopeApply == 'MD_CODE' ? item.code : '',
          'planCode': formValue.scopeApply == 'OPTION_CODE' ? item.code : '',
          'ldCode': formValue.scopeApply == 'LD_CODE' ? item.code : '',
          "itemCode": item?.chargeCode,  // mã biểu lãi
          "itemName": item?.descriptionVn, // tên biểu lãi
          "interestUnit": item?.currency, // Đơn vị tiền tệ
          "interestType": item?.chargeType, // Loại phí
          "ratioCost": item?.chgAmtFrom, // Mức phí biểu theo quy định
          "minimumCost": item?.minCharge, // Mức phí tối thiểu theo quy định
          "maximumCost": item?.maxCharge, // Mức phí tối đa theo quy định
          "ratioCostOffer": item?.suggestCost, // Mức phí đề xuất
          "minimumCostOffer": item?.suggestMin, // Mức phí tối thiểu đề xuất
          "maximumCostOffer": item?.suggestMax, // Mức phí tối đa đề xuất
          "calcType": item?.calcType,
          "chargeUnit": item?.chargeUnit, //Đơn vị phí tối thiểu, tối đa
          "valueRefDoc": item?.valueRefDoc, // Đơn vị mức phí
          "chargeGroup": item?.chargeGroup, // Nhóm loại phí
          "reductionRate": Utils.isNotNull(item?.rateCost) ? Number(item?.rateCost) : null, // tỷ lệ giảm
          "type": 0
        };
      }),
      "reductionProposalDetailsApprovedDTOList": [],
      reductionProposalAuthorizationDTOList: [
        // {
        //   fullName: this.form.controls.fullName.value,
        //   userName: this.form.controls.authApproval.value,
        //   titleCategory: this.form.controls.titleCategory.value
        // }
      ],
      "reductionProposalTOIDTO": {
        "ckhBqNim": this.info?.toiCalculate?.coKyHanBqUnit || 0,
        "ckhBqSd": this.info?.toiCalculate?.coKyHanBq || 0,
        "dunoBqNim": this.info?.toiCalculate?.duNoNganHanBqUnit || 0,
        "dunoBqSd": this.info?.toiCalculate?.duNoNganHanBq || 0,
        "dunoTdhBqNim": this.info?.toiCalculate?.duNoTdhBqUnit || 0,
        "dunoTdhBqSd": this.info?.toiCalculate?.duNoTdhBq || 0,
        "kkhBqNim": this.info?.toiCalculate?.khongKyHanBqUnit || 0,
        "kkhBqSd": this.info?.toiCalculate?.khongKyHanBq || 0,
        "tblDsSd": this.info?.toiCalculate?.thuBl || 0,
        "tblNim": this.info?.toiCalculate?.thuBlUnit || 0,
        "tfxDsSd": this.info?.toiCalculate?.thuFx || 0,
        "tfxMpqd": this.info?.toiCalculate?.thuFxUnit || 0,
        "thuBancaDsSd": this.info?.toiCalculate?.thuBanca || 0,
        "thuBanCaMpqd": 1,
        "thuKhacDsSd": this.info?.toiCalculate?.thuKhac || 0,
        "thuKhacMpqd": 1,
        "tttqtDsSd": this.info?.toiCalculate?.thuTTQT || 0,
        "tttqtMpqd": this.info?.toiCalculate?.thuTTQTUnit || 0,
        "ckhBqHq": this.info?.toiCalculate?.ckhBqHq ? +this.info?.toiCalculate?.ckhBqHq.replace(',', '.') : 0,
        "dunoBqHq": 0,
        "dunoTdhBqHq": 0,
        "kkhBqHq": 0,
        "tblHq": 0,
        "tfxHq": 0,
        "thuBanCaHq": 0,
        "thuKhacHq": 0,
        "tttqtHq": 0,
        "dttck": 0,
        "tck": 0
      },
      "reductionProposalFileDTOList": this.listFile,
      "customerName": this.info?.customerName,
      "approverName": "",
      "approverTitle": "",
      "status": "",
      "createdDate": "",
      "rmCode": this.currUser?.rmCode,
      "branchCode": this.currUser?.branch,
      "branchCodeCustomer": this.info?.id ? this.info?.branchCodeCustomer : this.info?.branchCode,
      "financialSituation": '',
      "customersPolicyApplied": "",
      "proposalReason": "",
      "customerProductList": "",
      "toiExisting": this.info?.TOI,
      "dtttrrExisting": this.info?.DTTTRR,
      "approvedAuthorization": "",
      "customerType": this.info.LOAI_KH == 'Khach hang cu' ? 'OLD' : ' NEW',
      "customerSegment": this.info?.searchSme?.segmentCode || this.form.controls.segment.value,
      "businessRegistrationNumber": this.info?.businessRegistrationNumber,
      "classification": this.info?.searchSme?.classification,
      "creditRanking": this.info?.ketQuaXhtd,
      "jsonRelationTCTD": JSON.stringify([]),
      "yearsOfRelationship": '',
      "jsonRelationMB": JSON.stringify([]),
      "industryActivity": "",
      "businessRegistrationLocation": "",
      "startDate": "",
      "endDate": "",
      'customerStructure': this.info?.customerType, // Cơ cấu khách hàng
      'policyApplied': '', // Chính sách đang áp dụng với khách hàng
      'proposedBasis': '', // Cơ sở đề xuất (uy tín khách hàng, lợi ích mang lại...)
      'jsonDebtOther': JSON.stringify([]),
      'debtCaution': '', // Nợ chú ý trong vòng 12 tháng gần nhất
      'jsonImportExportSales': JSON.stringify([]),
      "identifiedIssueDate": '',
      "identifiedIssueArea": '',
      "job": '',
      'isRmManager': this.isManage,
      approvedDateMO: moment(formValue?.approvedDateMO, 'DD/MM/YYYY'), //Ngày duyệt tờ trình trên MO
      reason: formValue?.reason, // lý do
      codeHTCNTT: formValue?.codeHTCNTT?.trim?.(),
      moCode: formValue?.moCode?.trim?.(), // mã tờ trình MO
      commitment: formValue?.commitment, //cam kết
      userApproved: formValue?.userName?.trim?.(), // thẩm quyền phê duyêt
      fullNameApproved: this.form.controls.fullName.value?.trim?.(), // họ và tên
      roleApproved: this.form.controls.titleCategory.value?.trim?.(),// Chức danh phê duyệt
      id: this.params?.id,
    }
    if (this.params?.mode == 'edit') {
      return new Promise(async (resolve, reject) => {
        this.reductionProposalApi.saveException(payload).subscribe(res => {
          this.isLoading = false;
          if (isConfirm) {
            resolve(res);
            return;
          }
          this.messageService.success('Cập nhật thành công');
          this.router.navigate([functionUri.reduction_proposal], { state: { formSearch: this.state?.search } });
        }, err => {
          this.messageService.error('Có lỗi xảy ra, vui lòng thử lại sau');
          this.isLoading = false;
          reject(err);
        });
        return;
      })
    }
    return new Promise(async (resolve, reject) => {
      this.reductionProposalApi.saveException(payload).subscribe(res => {
        if (isConfirm) {
          resolve(res);
          return;
        }
        this.messageService.success('Thêm mới thành công');
        this.isLoading = false;
        this.router.navigate([functionUri.reduction_proposal], { state: { formSearch: this.state?.search } });
      }, err => {
        this.isLoading = false;
        this.messageService.error('Có lỗi xảy ra, vui lòng thử lại sau');
        reject(err);
      });
    });
  }

  checkFee() {
    const isChooseFee = this.dataTable.find(item => !item.isChooseFee);
    if (!this.dataTable.length || !this.isSelectFee || isChooseFee) {
      this.messageService.warn('Vui lòng chọn biểu phí');
      return false;
    }
    return true;
  }

  dateEffect() {
    if (!this.form.controls?.approvedDateMO?.value || !this.form.controls?.monthApply?.value) {
      return '';
    }
    const date = moment(this.form.controls?.approvedDateMO?.value, 'DD/MM/YYYY');
    const ennDateMonth = moment(this.form.controls?.approvedDateMO?.value, 'DD/MM/YYYY').endOf('month');
    if (date.format('DD/MM/YYYY') == ennDateMonth.format('DD/MM/YYYY')) {
      return moment(date.add(this.form.controls?.monthApply?.value, 'month').calendar('DD/MM/YYYY'), 'MM/DD/YYYY').endOf('month').format('DD/MM/YYYY');
    }
    return moment(date.add(this.form.controls?.monthApply?.value, 'month').calendar('DD/MM/YYYY'), 'MM/DD/YYYY').format('DD/MM/YYYY')
  }

  checkRequired() {
    const formValue = this.form.getRawValue();
    delete formValue.toi;
    delete formValue.segment;
    if (formValue.reason != 1) {
      delete formValue.codeHTCNTT;
    }
    return Object.values(formValue).every(x => x);
  }

  checkFile() {
    if (!this.listFile.length) {
      this.messageService.warn('Vui lòng chọn file !');
      return false;
    }
    return true;
  }

  getDetails(id) {
    if (!id) {
      return;
    }
    this.isLoading = true;
    this.reductionProposalApi.detailReductionProposal(id).subscribe(async res => {
      this.stateRouter['formSearch'] = res;
      console.log(res);
      await this.getData('getDetails');
      this.isLoading = false;
      this.info = { ...this.info, ...res };
      this.initTOI(res);
      this.isManage = this.currUser.branch === this.info?.branchCodeCustomer;
      res.approvedDateMO = moment(res.approvedDateMO).format('DD/MM/YYYY');
      res.moCode = res?.moDocId;
      res.segment = res?.customerSegment;
      res.toi = res?.toiConfirm,
        this.dtttrrDisplay = +res?.dtttrrConfirm;
      this.form.patchValue(res);
      this.form.patchValue(res?.reductionProposalAuthorizationDTOList[0] || {});
      this.listFile = res.reductionProposalFileDTOList;
      this.dataTable = res.reductionProposalDetailsDTOList.map?.(item => {
        const code = res?.scopeApply == 'OPTION_CODE' ? item?.planCode : res?.scopeApply == 'LD_CODE'
          ? item?.ldCode : res?.scopeApply == 'MD_CODE' ? item?.mdCode : '';
        const dataObj = {
          code,
          chargeCode: item?.itemCode,
          optionCode: item?.planCode,
          ldCode: item?.ldCode,
          mdCode: item?.mdCode,
          productCode: code + item?.itemCode + item?.interestUnit,
          descriptionVn: item?.itemName,
          calcType: item?.calcType,
          chgAmtFrom: item?.ratioCost, // Biểu phí: Tỉ lệ phí/ Mức phí
          minCharge: item?.minimumCost, // Biểu phí: Phí tối thiểu
          maxCharge: item?.maximumCost, // Biểu phí: Phí tối đa
          suggestCost: item?.ratioCostOffer, // Đề xuất: Tỉ lệ phí/Mức phí
          suggestMax: item?.maximumCostOffer, // Đề xuất: Phí tối đa
          suggestMin: item?.minimumCostOffer, // Đề xuất: Phí tối thiểu
          suggestCostOrigin: item?.ratioCostOffer, // Đề xuất: Tỉ lệ phí/Mức phí
          suggestMaxOrigin: item?.maximumCostOffer, // Đề xuất: Phí tối đa
          suggestMinOrigin: item?.minimumCostOffer, // Đề xuất: Phí tối thiểu
          valueRefDoc: item?.valueRefDoc, // Ghi chú mức biểu phí
          chargeUnit: item?.chargeUnit, // Đơn vị tính
          chargeType: item?.interestType, // Phân loại phí
          currency: item?.interestUnit, // Phân loại phí
          chargeGroup: item?.chargeGroup, // Nhóm loại phí
          rateCost: !item?.ratioCost || !item?.ratioCostOffer ? '--' : ((item?.ratioCost - item?.ratioCostOffer) * 100 / (item?.ratioCost)).toFixed(2),
          rateMin: !item?.minimumCost || !item?.minimumCostOffer ? '--' : ((item?.minimumCost - item?.minimumCostOffer) * 100 / (item?.minimumCost)).toFixed(2),
          rateMax: !item?.maximumCost || !item?.maximumCostOffer ? '--' : ((item?.maximumCost - item?.maximumCostOffer) * 100 / (item?.maximumCost)).toFixed(2),
          isChooseFee: true
        };
        return dataObj;
      });
      if (!this.dataTable.length) {
        this.isSelectFee = false;
        return;
      }
      this.isSelectFee = true;
    }, err => {
      this.isLoading = false;
    });
  }

  initTOI(dataDetails) {
    const toi = dataDetails?.reductionProposalTOIDTO;
    this.info.toi = {
      duNoNganHanBq: toi?.dunoBqSd,
      duNoNganHanBqUnit: toi?.dunoBqNim,
      duNoTdhBq: toi?.dunoTdhBqSd,
      duNoTdhBqUnit: toi?.dunoTdhBqNim,
      coKyHanBq: toi?.ckhBqSd,
      coKyHanBqUnit: toi?.ckhBqNim,
      khongKyHanBq: toi?.kkhBqSd,
      khongKyHanBqUnit: toi?.kkhBqNim,
      thuBl: toi?.tblDsSd,
      thuBlUnit: toi?.tblNim,
      thuTTQT: toi?.tttqtDsSd,
      thuTTQTUnit: toi?.tttqtMpqd,
      thuFx: toi?.tfxDsSd,
      thuFxUnit: toi?.tfxMpqd,
      thuBanca: toi?.thuBancaDsSd,
      thuKhac: toi?.thuKhacDsSd,
      dtttrr: dataDetails?.dtttrrConfirm,
      toi: dataDetails?.toiConfirm,
      // Hieu qua
      ckhBqHq: toi?.ckhBqHq,
      dunoBqHq: toi?.dunoBqHq,
      dunoTdhBqHq: toi?.dunoTdhBqHq,
      kkhBqHq: toi?.kkhBqHq,
      tblHq: toi?.tblHq,
      tfxHq: toi?.tfxHq,
      thuBanCaHq: toi?.thuBanCaHq,
      thuKhacHq: toi?.thuKhacHq,
      tttqtHq: toi?.tttqtHq
    };
    this.info.toiCalculate = this.info.toi;
    this.reductionProposalApi.info.next(this.info);
  }

  showErrControl(controlName) {
    return !this.form.controls[controlName].value && this.clickSave || !this.form.controls[controlName].value && this.form.controls[controlName].touched;
  }

  displayWithUnit(unit1, unit2?) {
    return unit1 && unit1.includes('%') ? '%' : unit2 ? '/' + unit2 : '';
  }

  getInfoAuthorization(event) {
    if (this.modeView) {
      return;
    }
    return new Promise(async (resolve, reject) => {
      const val = event?.target?.value || this.form.controls.userName.value;
      if (!val) {
        resolve(true)
        return;
      }
      this.isLoading = true;
      if (val) {
        return this.reductionProposalApi.getInfoAuthorization(val).subscribe((item) => {
          this.isLoading = false;
          if (item) {
            this.form.controls.fullName.setValue(item?.fullName);
            this.form.controls.titleCategory.setValue(item?.positionName);
            resolve(true)
            return;
          }
          this.form.controls.fullName.setValue(null);
          this.form.controls.titleCategory.setValue(null);
          this.form.markAllAsTouched();
          resolve(true)
        }, err => {
          this.isLoading = false;
          this.form.controls.fullName.setValue(null);
          this.form.controls.titleCategory.setValue(null);
          this.form.markAllAsTouched();
          reject(err);
        });
      }
    })
  }

  getSegment(value) {
    if (!this.dataCommon['REVENUE_CUSTOMER_SEGMENT']?.length) {
      return;
    }
    return this.dataCommon['REVENUE_CUSTOMER_SEGMENT'].find(item => item.description == value)?.name;
  }

  async confirm() {
    this.isLoading = true;
    const resSave = this.info?.id && this.params?.mode == 'details' ? this.info : await this.save(true);
    const body = {
      code: resSave?.code,
      type: 0,
      creditRanking: resSave?.creditRanking || this.info?.ketQuaXhtd,
      classification: resSave?.classification || this.info?.searchSme?.classification,
      customerType: resSave?.customerStructure || this.info?.LOAI_KH == 'Khach hang cu' ? 'OLD' : 'NEW',
      dtttrrExisting: resSave?.dtttrrExisting || this.info?.DTTTRR,
      customerName: this.info?.customerName,

    };
    if (!resSave?.code) {
      this.isLoading = false;
      return;
    }
    this.reductionProposalApi.confirmReductionException(body).subscribe(res => {
      console.log(res);
      this.messageService.success('Trình duyệt thành công');
      this.isLoading = false;
      this.router.navigate([functionUri.reduction_proposal], { state: { formSearch: this.state?.search } });
    }, err => {
      this.isLoading = false;
      this.router.navigate([functionUri.reduction_proposal], { state: { formSearch: this.state?.search } });
      this.messageService.error('Có lỗi xảy ra, vui lòng thử lại sau');
    });
  }

  async approval(status) {
    const resSave = this.info?.id ? this.info : await this.save(true);
    const body = {
      status: status,
      code: resSave?.code,
      type: 0
    };
    this.isLoading = true;
    this.reductionProposalApi.CBQLconfirmReduction(body).subscribe(res => {
      console.log(res);
      this.messageService.success(`${status == 1 ? 'TỪ CHỐI' : 'ĐỒNG Ý'} thành công`);
      this.isLoading = false;
      this.router.navigate([functionUri.reduction_proposal], { state: { formSearch: this.state?.search } });
    }, err => {
      this.isLoading = false;
      this.messageService.error('Có lỗi xảy ra, vui lòng thử lại sau');
      this.router.navigate([functionUri.reduction_proposal], { state: { formSearch: this.state?.search } });
    });
  }

  checkHTMLerror() {
    if (this.errorMessage?.length) {
      return true;
    }
    return false;
  }

}
