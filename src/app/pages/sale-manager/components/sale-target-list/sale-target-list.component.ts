import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { global } from '@angular/compiler/src/util';
import {
  CommonCategory,
  Scopes,
  maxInt32,
  FunctionCode,
  StatusWork,
  functionUri,
} from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { forkJoin, of } from 'rxjs';
import _ from 'lodash';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { FileService } from 'src/app/core/services/file.service';
import { RmBlockApi } from 'src/app/pages/rm/apis';
import { cleanDataForm } from 'src/app/core/utils/function';
import { SaleManagerApi, SaleTargetApi } from '../../api';

@Component({
  selector: 'sale-target-list-component',
  templateUrl: './sale-target-list.component.html',
  styleUrls: ['./sale-target-list.component.scss'],
})
export class SaleTargetListComponent extends BaseComponent implements OnInit {
  isLoading = false;
  @ViewChild('table') table: DatatableComponent;
  limit = global.userConfig.pageSize;
  pageable: Pageable;
  params: any = {
    pageNumber: 0,
    pageSize: global?.userConfig?.pageSize,
    rsId: '',
    scope: Scopes.VIEW,
  };
  propDetail: any;

  paramSearchDivision = {
    size: global.userConfig.pageSize,
    page: 0,
    name: '',
    code: '',
  };

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private saleManagerApi: SaleManagerApi,
    private fileService: FileService,
    private rmBlockApi: RmBlockApi,
    private saleTargetApi: SaleTargetApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.OPPORTUNITY_TARGET}`);
    this.propDetail = this.router.getCurrentNavigation()?.extras?.state;
  }

  formSearch = this.fb.group({
    customerCode: [''],
    customerName: [''],
    registrationNumber: [''],
    division: [''],
    targetBranchCode: [''],
    targetRMCode: [''],
    targetStatus: [''],
    opportunityCode: [''],
    targetCode: [''],
    rsId: '',
    scope: Scopes.VIEW,
  });

  listDivision = [];
  listData = [];

  listRmManager = [];
  listBranchesOpp = [];
  listStatusOpp = [];
  dataDivisionBranch: any = {};
  listBranch = [];
  lstLevelRmCode: any;
  prevParams: any;
  prop: any;
  allRm = true;
  permissionOpp = false;
  listRmManagerAll = [];
  listRuleView = [];
  allBranch = true;

  ngOnInit() {
    this.isLoading = true;
    this.prop = this.sessionService.getSessionData(FunctionCode.OPPORTUNITY_TARGET);
    if (this.prop && !_.isEmpty(this.propDetail)) {
      this.prevParams = { ...this.prop?.prevParams };
      this.formSearch.patchValue(this.prevParams);
      this.listDivision = this.prop?.listDivision || [];
      this.listBranchesOpp = this.prop?.listBranchesOpp || [];
      this.listRmManager = this.prop?.listRmManager || [];
      this.listStatusOpp = this.prop?.listStatusOpp || [];
      this.listRuleView = this.prop?.listRuleView;
      this.listRmManagerAll = this.prop?.listRmManagerAll || [];
      this.search(true);
    } else {
      forkJoin([
        this.rmBlockApi
          .fetch({ page: 0, size: maxInt32, hrsCode: this.currUser?.hrsCode, isActive: true })
          .pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.TARGET_STATUS).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.KHOI_PRIORITY).pipe(catchError(() => of(undefined))),
        this.categoryService.searchBlocksCategory(this.paramSearchDivision).pipe(catchError(() => of(undefined))),
      ]).subscribe(([divisionOfSystem, listStatusOpp, listConfigDivision, divisionAll]) => {
        // KHOI_PRIORITY
        listConfigDivision = listConfigDivision?.content || [];
        listConfigDivision = _.orderBy(listConfigDivision, ['value']);
        let divisionConfig = listConfigDivision?.map((item) => item.code);
        // LIST TITLE
        let listDivisioTitle =
          divisionOfSystem?.content?.map((item) => {
            return {
              divisionCode: item?.blockCode,
              levelRmCode: item?.levelRMCode === undefined ? '' : item?.levelRMCode,
              titleGroupId: item?.titleGroupId === undefined ? '' : item?.titleGroupId,
            };
          }) || [];

        // STATUS
        this.listStatusOpp = listStatusOpp?.content.map((item) => {
          return { code: item.code, displayName: item.name };
        });
        this.listStatusOpp.unshift({ code: '', displayName: this.fields.all });

        this.saleManagerApi.getTitleDivisionBranch(listDivisioTitle).subscribe(
          (res) => {
            if (!_.isEmpty(res)) {
              this.dataDivisionBranch = res;
              const divisionOfUser =
                divisionAll?.content?.map((item) => {
                  return {
                    code: item.code,
                    displayName: item.code + ' - ' + item.name,
                    order:
                      _.findIndex(divisionConfig, (value) => value === item.code) === -1
                        ? 99
                        : _.findIndex(divisionConfig, (value) => value === item.code),
                  };
                }) || [];
              let listDivision = this.dataDivisionBranch.map((item) => item.ruleDivision.division).toString();
              listDivision = _.unionBy(_.split(listDivision, ','));
              let listBranchConfig = this.dataDivisionBranch.map((item) => item.ruleDivision.branch).toString();
              listBranchConfig = _.unionBy(_.split(listBranchConfig, ','));
              this.lstLevelRmCode = this.dataDivisionBranch.map((item) => item.ruleDivision.ruleAssign).toString();
              this.lstLevelRmCode = _.unionBy(_.split(this.lstLevelRmCode, ','));
              let listRuleView = this.dataDivisionBranch.map((item) => item.ruleDivision.view).toString();
              this.listRuleView = _.unionBy(_.split(listRuleView, ','));
              if (listDivision.findIndex((item) => item === 'All') > -1) {
                this.listDivision = divisionOfUser;
              } else {
                divisionOfUser?.forEach((item) => {
                  if (!_.isEmpty(listDivision.find((i) => i === item.code))) {
                    this.listDivision.push(item);
                  }
                });
              }
              this.listDivision = _.orderBy(this.listDivision, ['order']);
              this.formSearch.controls.division.setValue(_.first(this.listDivision)?.code, { emitEvent: false });
              this.getBranchByUser(listBranchConfig);
              this.getRmByBranch(listBranchConfig, true);
              this.prop = {
                listDivision: this.listDivision,
                listStatusOpp: this.listStatusOpp,
                listRuleView: this.listRuleView,
              };
              this.sessionService.setSessionData(FunctionCode.OPPORTUNITY_TARGET, this.prop);
              // this.search(true);
            } else {
              this.isLoading = false;
              this.permissionOpp = true;
            }
          },
          (e) => {
            this.messageService.error(e.error.description);
            this.isLoading = false;
          }
        );
      });
    }
  }

  getBranchByUser(listBranchConfig) {
    const param = {
      listBranchCode: listBranchConfig,
      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW,
    };
    this.saleManagerApi.getBranchByUser(param).subscribe((res) => {
      this.listBranchesOpp = res.map((item) => {
        return {
          code: item.code,
          displayName: item.code + ' - ' + item.name,
        };
      });
      this.prop = { ...this.prop, listBranchesOpp: this.listBranchesOpp };
      this.sessionService.setSessionData(FunctionCode.OPPORTUNITY_TARGET, this.prop);
      this.listBranchesOpp.unshift({ code: '', displayName: this.fields.all });
    });
  }

  getRmByBranch(listBranchCode, isStart?) {
    this.formSearch.controls.targetRMCode.disable();
    const param = {
      lstBranchCode: listBranchCode,
      lstLevelRmCode: this.lstLevelRmCode,
      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW,
    };
    this.saleManagerApi.findRmByBranchAndLevelCode(param).subscribe((res) => {
      if (res) {
        this.listRmManagerAll = [
          ...res?.map((item) => {
            if (!_.isEmpty(item?.rmCode)) {
              return {
                code: item.rmCode,
                displayName: item.rmCode + ' - ' + item.rmName,
              };
            }
          }),
        ];
        this.listRmManagerAll = this.listRmManagerAll.filter((item) => !_.isEmpty(item));
        this.listRmManager = res?.map((item) => {
          if (!_.isEmpty(item?.rmCode) && item?.active && item?.statusWork !== StatusWork.NTS) {
            return {
              code: item.rmCode,
              displayName: item.rmCode + ' - ' + item.rmName,
            };
          }
        });
        this.listRmManager = this.listRmManager.filter((item) => !_.isEmpty(item));
        if (_.isEmpty(this.lstLevelRmCode[0])) {
          if (this.formSearch.controls.targetBranchCode.value === this.currUser.branch) {
            this.listRmManager.unshift({ code: '', displayName: this.fields.all });
            this.formSearch.controls.targetRMCode.setValue(_.first(this.listRmManager)?.code);
          } else {
            if (_.isEmpty(this.formSearch.controls.targetBranchCode.value)) {
              this.listRmManager.unshift({ code: '', displayName: this.fields.all });
              this.formSearch.controls.targetRMCode.setValue('');
            } else {
              this.listRmManager = [];
              this.formSearch.controls.targetRMCode.setValue('');
            }
          }
        } else {
          this.listRmManager.unshift({ code: '', displayName: this.fields.all });
          this.formSearch.controls.targetRMCode.setValue('');
        }
        this.formSearch.controls.targetRMCode.enable();
        this.prop = { ...this.prop, listRmManager: this.listRmManager, listRmManagerAll: this.listRmManagerAll };
        this.sessionService.setSessionData(FunctionCode.OPPORTUNITY_TARGET, this.prop);

        if (isStart) {
          this.search(true);
        }
      }
    });
  }

  ngAfterViewInit() {
    this.formSearch.controls.targetBranchCode.valueChanges.subscribe((value) => {
      let opportunityBranch = [];
      if (_.isEmpty(value)) {
        opportunityBranch.push('All');
      } else {
        opportunityBranch.push(value);
      }
      this.getRmByBranch(opportunityBranch);
    });
  }

  search(isSearch: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.params.pageNumber = 0;
    }
    const params = {
      ...cleanDataForm(this.formSearch),
      rsId: this.objFunction.rsId,
    };

    if (_.isEmpty(params.targetRMCode)) {
      this.allRm = true;
      let listRmManagerSearch = [...this.listRmManagerAll];
      params.listTargetRMCode = _.remove(listRmManagerSearch, (i) => !_.isEmpty(i?.code)).map((item) => item.code);
    } else {
      this.allRm = false;
      let listTargetRMCode = [];
      listTargetRMCode.push(params.targetRMCode);
      params.listTargetRMCode = listTargetRMCode;
    }
    delete params.targetRMCode;
    if (_.isEmpty(params.targetBranchCode)) {
      this.allBranch = true;
      let listBranchesOppSearch = [...this.listBranchesOpp];
      params.lstBranches = _.remove(listBranchesOppSearch, (i) => !_.isEmpty(i?.code)).map((item) => item.code);
    } else {
      this.allBranch = false;
      let listBranchesOpp = [];
      listBranchesOpp.push(params.targetBranchCode);
      params.lstBranches = listBranchesOpp;
    }
    delete params.targetBranchCode;
    params.allRm = this.allRm;
    params.allBranch = this.allBranch;
    params.listRuleView = this.listRuleView;

    this.saleTargetApi.searcTargetSale(params, this.params.pageNumber, this.params.pageSize).subscribe(
      (result) => {
        if (result) {
          this.prevParams = params;
          this.prop.prevParams = params;
          this.isLoading = false;
          this.listData = result.content || [];
          this.listData.forEach((item) => {
            item.statusTarget = this.listStatusOpp.filter((i) => i.code === item.targetStatus)[0]?.displayName || '';
            if (+item.targetStatus === 1) {
              item.icon = 'la-status-opportunity-green';
            } else if (+item.targetStatus === 2) {
              item.icon = 'la-status-opportunity-red';
            } else {
              item.icon = 'la-status-opportunity-violet';
            }
          });
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: this.limit,
          };
        }
      },
      (e) => {
        if (e.error.code) {
          this.messageService.error(e.error.description);
          this.isLoading = false;
          return;
        }
        this.messageService.error(this.notificationMessage.error);
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.pageNumber = pageInfo.offset;
    this.search(false);
  }

  transferSale(row) {
    this.isLoading = true;
    this.saleTargetApi.convertSale(row.id, this.objFunction?.rsId, Scopes.UPDATE).subscribe(
      (resConvert) => {
        this.isLoading = false;
        this.messageService.success(this.notificationMessage.success);
        this.router.navigateByUrl(functionUri.sale_transfer);
      },
      (e) => {
        this.isLoading = false;
        if (e.error.code) {
          this.messageService.error(e.error.description);
        }
      }
    );
  }

  editOpportunity(row) {
    this.isLoading = true;
    this.saleTargetApi
      .checkUpdateTargetSale(row.division, this.objFunction?.rsId, Scopes.UPDATE, row.id, true)
      .subscribe(
        (res) => {
          this.isLoading = false;
          this.router.navigate([this.router.url, 'update'], {
            skipLocationChange: true,
            queryParams: {
              opportunityCode: row.opportunityCode,
              divisionCode: row.division,
              customerCode: row.customerCode,
              targetCode: row.targetCode,
              targetId: row.id,
            },
            state: this.prevParams,
          });
        },
        (e) => {
          this.isLoading = false;
          if (e.error.code) {
            this.messageService.error(e.error.description);
          }
        }
      );
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      this.router.navigate([this.router.url, 'detail'], {
        skipLocationChange: true,
        queryParams: {
          opportunityCode: item.opportunityCode,
          divisionCode: item.division,
          customerCode: item.customerCode,
          targetCode: item.targetCode,
          targetId: item.id,
        },
        state: this.prevParams,
      });
    }
  }

  exportFile() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+this.pageable?.totalElements <= 0) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.isLoading = true;
    const paramExport = {
      ...this.formSearch.value,
    };
    paramExport.rsId = this.objFunction.rsId;
    paramExport.pageNumber = 0;
    paramExport.pageSize = maxInt32;

    if (_.isEmpty(paramExport.targetRMCode)) {
      this.allRm = true;
      let listRmManagerSearch = [...this.listRmManager];
      paramExport.listTargetRMCode = _.remove(listRmManagerSearch, (i) => !_.isEmpty(i?.code)).map((item) => item.code);
    } else {
      this.allRm = false;
      let listTargetRMCode = [];
      listTargetRMCode.push(paramExport.targetRMCode);
      paramExport.listTargetRMCode = listTargetRMCode;
    }
    delete paramExport.targetRMCode;
    if (_.isEmpty(paramExport.targetBranchCode)) {
      this.allBranch = true;
      let listBranchesOppSearch = [...this.listBranchesOpp];
      paramExport.lstBranches = _.remove(listBranchesOppSearch, (i) => !_.isEmpty(i?.code)).map((item) => item.code);
    } else {
      this.allBranch = false;
      let listBranchesOpp = [];
      listBranchesOpp.push(paramExport.targetBranchCode);
      paramExport.lstBranches = listBranchesOpp;
    }
    delete paramExport.targetBranchCode;
    paramExport.allBranch = this.allBranch;
    paramExport.allRm = this.allRm;
    paramExport.listRuleView = this.listRuleView;

    this.saleTargetApi.excelTargetSale(paramExport).subscribe(
      (res) => {
        if (res) {
          this.download(res);
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(_.get(this.notificationMessage, 'export_error'));
        this.isLoading = false;
      }
    );
  }

  download(fileId: string) {
    let todayExcel = new Date();
    let titleExcel =
      'muc-tieu-ban-' +
      this.currUser.username +
      '-' +
      todayExcel.getFullYear().toString() +
      (todayExcel.getMonth() + 1).toString() +
      todayExcel.getDate().toString() +
      '.xlsx';
    this.fileService.downloadFile(fileId, titleExcel).subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }
}
