import { Component, OnInit, Injector, Input } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { formatNumber } from '@angular/common';
import { IInfoContact } from '../../../model/info-basic.interface';
import { AddressApi } from '../../../api/address.api';
@Component({
    selector: 'app-info-contact',
    templateUrl: './info-contact.component.html',
    styleUrls: ['./info-contact.component.scss'],
  })

  export class InfoContactComponent extends BaseComponent implements OnInit{
    @Input() taxCode: string;
    public isLoading: boolean = false;
    isRMManagement : boolean = true;
    listData  = [];
    item:IInfoContact;

    columns = [
        {
            name: "Họ và tên",
            prop: "cstRepNm",
            sortable: false,
            draggable: false,
        },
        {
            name: "Chức danh",
            prop: "position",
            sortable: false,
            draggable: false,
        },
        {
            name: "Số GTTT",
            prop: "certificateId",
            sortable: false,
            draggable: false,
        },
        
    ];

    constructor(
        private api: AddressApi,
        injector: Injector) {
        super(injector);
        this.isLoading = true;
    }
    ngOnInit(): void {
        this.checkRMManagement();
        this.getListContact();
    }
    getListContact(){
        this.api.getContactByTaxCode(this.taxCode).subscribe(value => {
            this.listData = value;
            this.isLoading = false;
        }, (e) => {
            this.isLoading = false;
            this.messageService.error(e?.error?.description);
        })
    }

    checkRMManagement(){
        // Trường hợp nếu là RM quản lý KH hoặc user được phân quyền miền dữ liệu thì mới được xem 2 trường này
        if(this.isRMManagement){
            this.columns.push(
                {
                    name: "Số điện thoại",
                    prop: "mobile",
                    sortable: false,
                    draggable: false,
                });
            this.columns.push(
                {
                    name: "Email",
                    prop: "email",
                    sortable: false,
                    draggable: false,
                });
        }
        this.columns.push(
            {
                name: "Ngày sinh",
                prop: "dateOfBirth",
                sortable: false,
                draggable: false,
            });
        this.columns.push(
            {
                name: "Giới tính",
                prop: "gender",
                sortable: false,
                draggable: false,
            });
    }
}