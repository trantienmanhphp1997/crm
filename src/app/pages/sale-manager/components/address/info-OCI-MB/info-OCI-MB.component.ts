import { Component, OnInit, Injector, OnDestroy, ViewChild, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import _ from 'lodash';
import { forkJoin, of } from 'rxjs';
import { CustomerDetailSmeApi } from 'src/app/pages/customer-360/apis/customer.api';
import { CustomerApi, CustomerDetailApi } from 'src/app/pages/customer-360/apis';
import { catchError, retry } from 'rxjs/operators';
import * as moment from 'moment';
import { Utils } from 'src/app/core/utils/utils';
import { FileService } from 'src/app/core/services/file.service';
import { formatDate, formatNumber } from '@angular/common';
import { ExportExcelService } from 'src/app/core/services/export-excel.service';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { RevenueShareApi } from 'src/app/pages/customer-360/apis/revenue-share.api';
import {
    CommonCategory,
    CustomerType,
    FunctionCode,
    functionUri,
    maxInt32,
    Scopes,
    ScreenType,
    SessionKey,
    TaskType,
} from 'src/app/core/utils/common-constants';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ActivityActionComponent } from 'src/app/shared/components/activity-action/activity-action.component';
import { CreateTaskModalComponent } from 'src/app/pages/tasks/components/create-task-modal/create-task-modal.component';

@Component({
    selector: 'app-info-oci-mb',
    templateUrl: './info-OCI-MB.component.html',
    styleUrls: ['./info-OCI-MB.component.scss'],
})
export class InfoOCIMBComponent extends BaseComponent implements OnInit {
    constructor(
        injector: Injector,
        private customerApi: CustomerApi,
        private exportExcelService: ExportExcelService,
        private customerDetailApi: CustomerDetailApi,
        private customerDetailSmeApi: CustomerDetailSmeApi,
        private categoryService: CategoryService,
        private revenueShareApi: RevenueShareApi,
        private fileService: FileService
    ) {
        super(injector);
        this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
        this.isLoading = true;
        this.format = new Intl.NumberFormat();
        this.code = _.get(this.route.snapshot.params, 'code');
        this.rmManagerList = _.get(this.route.snapshot.queryParams, 'manageRM');
        this.showBtnRevenueShare = _.get(this.route.snapshot.queryParams, 'showBtnRevenueShare') === 'true';
    }
    branchCode: string;
    code: string;
    format: any;
    excel_fields: any;
    excel_title: any;
    titles: Array<any>;
    month: number;
    year: number;
    hyper_link: string;
    model: any = {};
    emodel: any;
    form = this.fb.group({});
    messageTable: any;
    param: any;
    is_show: boolean = false;
    is_edit: boolean;
    is_show_form: boolean;
    index: number = 0;
    phoneConfig = [];
    emailConfig = [];
    ageGroup: any;
    sector: any;
    tab_index: number = 0;
    risk_management_tab_index: number = 0;
    relationship_with_app_tab_index: number = 0;
    credit_finance_tab_index: number = 0;
    campaign_tab_index: number = 0;
    isLoading: boolean;
    title: string;

    rmManager: string;
    collaterals: Array<any>;
    data_collaterals: Array<any>;
    rows: Array<any> = [];
    search_value: string;
    list_collaterals: Array<any>;
    asset_types: Array<any>;
    asset_groups: Array<any>;
    bscores: Array<any>;
    list_bscores: Array<any>;
    loading_count: number = 0;
    collaterals_items: Array<any>;
    bscore_items: Array<any>;
    load_coast: boolean = false;
    load_bscore: boolean = false;
    load_collateral: boolean = false;
    is_show_bscore: boolean = true;
    listBranchCode = [];
    commonData: any;
    email: any;
    sms: any;
    phone: any;
    meetUp: any;

    legalRepresentatives: any;
    chiefAccountants: any;
    contactInfos: any;
    directors: any;

    isLegalRepresentatives: boolean;
    isChiefAccountants: boolean;
    isContactInfos: boolean;
    isDirectors: boolean;
    show_error: boolean = true;
    error_mes: string;
    isCreditCollection: boolean;
    isCapitalMobilization: boolean;
    isServiceCollection: boolean;

    previousYear: string;
    previousMonth: string;
    twoMonthAgo: string;
    cost: any;
    contact: any = {};
    costValues: any;
    creditInformation: any;
    first: boolean = true;
    rmManagerList: string;
    showBtnRevenueShare: boolean;
    registrationNumber: string;
    show_data_collateral: boolean;
    show_data_bidding: boolean;
    collateral: any;
    bidding: any;
    divisionProduct: any;
    segmentCustomer: string;
    // SonLQ
    listOpportunity = [];
    listCreditRatings = [];
    dataEarly = [];
    isPossibility: boolean;
    isInfulenceLevel: boolean;
    isEarlywarningLevel: boolean;
    show_error_riskManagement: boolean = true;
    pageable: Pageable;
    previousYearWarning: string;
    previousMonthWarning: string;
    twoMonthAgoWarning: string;
    isBusinessCustomer: boolean;
    isBusinessDebit: boolean;
    isBusinessMobilization: boolean;
    isBusinessGuarantee: boolean;
    isBusinessCommercialFinance: boolean;
    isBusinessServiceProducts: boolean;
    classification: string;
    @ViewChild('tableCreditRatings') public tableCreditRatings: DatatableComponent;
    @ViewChild('tablecollateral') public tablecollateral: DatatableComponent;
    @ViewChild('customerProduct') customerProduct: any;
    @ViewChild('customerActivity') customerActivity: any;

    ngOnInit() {
        this.commonData = this.sessionService.getSessionData(SessionKey.COMMON_DATA_CUSTOMER_DETAILS);
        if (!this.commonData) {
            forkJoin([
                this.translate.get(['customer360', 'collateralMessage']).pipe(catchError((e) => of(undefined))),
                this.commonService.getCommonCategory(CommonCategory.ASSET_TYPE).pipe(catchError(() => of(undefined))),
                this.commonService.getCommonCategory(CommonCategory.ASSET_GROUP).pipe(catchError(() => of(undefined))),
                this.commonService.getCommonCategory(CommonCategory.KH_PHONE_NO_CONFIG).pipe(catchError(() => of(undefined))),
                this.commonService.getCommonCategory(CommonCategory.KH_EMAIL_CONFIG).pipe(catchError(() => of(undefined))),
                this.commonService
                    .getCommonCategory(CommonCategory.PRIVATE_PRIORITY_CONFIG)
                    .pipe(catchError((e) => of(undefined))),
                this.commonService.getCommonCategory(CommonCategory.ACCOUNT_GROUP).pipe(catchError(() => of(undefined))),
                this.categoryService
                    .getBranchesOfUser(_.get(this.objFunction, 'rsId'), Scopes.VIEW)
                    .pipe(catchError(() => of(undefined))),
                this.categoryService.getIndustries({}).pipe(catchError(() => of([]))),
            ]).subscribe(
                ([
                    objTranslate,
                    asset_types,
                    asset_groups,
                    phoneConfig,
                    emailConfig,
                    sector,
                    accountGroup,
                    branchesOfUser,
                    listIndustry,
                ]) => {
                    this.commonData = {
                        asset_types: _.get(asset_types, 'content') || [],
                        asset_groups: _.get(asset_groups, 'content') || [],
                        phoneConfig: _.get(phoneConfig, 'content') || [],
                        emailConfig: _.get(emailConfig, 'content') || [],
                        sector: _.get(sector, 'content') || [],
                        accountGroup: _.get(accountGroup, 'content') || [],
                        objTranslate: objTranslate,
                        branchesOfUser: branchesOfUser,
                        listIndustry,
                    };
                    this.sessionService.setSessionData(SessionKey.COMMON_DATA_CUSTOMER_DETAILS, this.commonData);
                    this.mapData();
                    this.error_mes = _.get(this.excel_fields, 'empty_data');
                }
            );
        }

        forkJoin([
            this.customerDetailSmeApi.get(this.code).pipe(catchError(() => of(undefined))),
            this.customerApi.getRmManagerByCustomerCode(this.code).pipe(catchError(() => of(undefined))),
            this.customerDetailApi.get(`bscore/${this.code}`).pipe(catchError(() => of(undefined))),
            this.customerDetailApi.get(`collateral/${this.code}`).pipe(catchError(() => of(undefined))),
            this.customerDetailSmeApi.get(`sme/cost-collection/${this.code}`).pipe(
                retry(2),
                catchError(() => of(this.messageService.error(_.get(this.notificationMessage, 'loadCostCollectionFail'))))
            ),
            this.customerDetailSmeApi.getSegmentByCustomerCode(this.code).pipe(catchError(() => of(undefined))),
        ]).subscribe(
            ([itemCustomer, rmManager, bscores, collaterals, cost, segment]) => {
                this.segmentCustomer = segment;
                this.rmManager = rmManager;
                var date = moment().add(-1, 'month');
                this.year = date.year();
                this.model = itemCustomer;
                const itemIndustry = _.find(this.commonData.listIndustry, (i) => i.key === this.model.otherInfo.mbIndustry);
                this.model.otherInfo.mbIndustryDesc = itemIndustry
                    ? `${itemIndustry.key} - ${itemIndustry.value}`
                    : this.model.otherInfo.mbIndustry;
                this.divisionProduct = _.get(this.model, 'systemInfo.accountType');
                if (this.divisionProduct === CustomerType.NHS) {
                    this.divisionProduct = CustomerType.SME;
                }
                this.registrationNumber = _.get(this.model, 'customerInfo.registrationNumber');
                const paramCreditInfo = {
                    identifiedNumber: this.registrationNumber,
                    division: this.divisionProduct,
                };
                this.getCreditInformation(paramCreditInfo);
                this.convertRepresentative(_.get(this.model, 'representativeInfo'));
                if (Utils.isNotNull(_.get(this.model, 'customerInfo.customerGroup'))) {
                    this.model.customerInfo.customerGroupName = _.get(
                        _.find(
                            _.get(this.commonData, 'accountGroup'),
                            (i) => i.code === _.get(this.model, 'customerInfo.customerGroup')
                        ),
                        'name'
                    );
                }
                this.bscores = bscores || [];
                this.collaterals_items = collaterals || [];
                this.cost = cost || {};
                this.costValues = cost || {};
                this.show_error = this.checkCost();

                this.mapData();
                this.init_title();

                const req = {
                    name: 'customer-left-view',
                    data: {
                        parentType: TaskType.CUSTOMER,
                        parentId: _.get(this.model, 'systemInfo.code'),
                        view: ScreenType.Detail,
                    },
                };
                this.communicateService.request(req);
                this.error_mes = _.get(this.excel_fields, 'empty_data');
            },
            () => {
                this.isLoading = false;
                this.messageService.error(_.get(this.notificationMessage, 'E001'));
            }
        );
    }

    init_title() {
        this.previousYear = `${_.get(this.fields, 'year')} ${moment(_.get(this.cost, 'n1.transactionDate')).format(
            'YYYY'
        )}`;
        this.previousMonth = `${_.get(this.fields, 'year')} ${moment(_.get(this.cost, 't1.transactionDate')).format(
            'YYYY'
        )} (${_.get(this.fields, 'month')} ${moment(_.get(this.cost, 't1.transactionDate')).format('MM')})`;
        this.twoMonthAgo = `${_.get(this.fields, 'year')} ${moment(_.get(this.cost, 't2.transactionDate')).format(
            'YYYY'
        )} (${_.get(this.fields, 'month')} ${moment(_.get(this.cost, 't2.transactionDate')).format('MM')})`;
    }

    getCreditInformation(params) {
        this.customerApi.getCreditInformation2(params).subscribe(
            (response: any) => {
                this.creditInformation = response;
                this.isLoading = false;
            },
            (e) => {
                if (e?.error?.description) {
                    this.messageService.error(e?.error?.description);
                } else {
                    this.messageService.error(_.get(this.notificationMessage, 'E001'));
                }
                this.isLoading = false;
            }
        );
    }

    checkCost() {
        let n1 = _.omit(_.get(this.costValues, 'n1'), 'transactionDate');
        let t1 = _.omit(_.get(this.costValues, 't1'), 'transactionDate');
        let t2 = _.omit(_.get(this.costValues, 't2'), 'transactionDate');
        return Utils.isEmpty(n1) && Utils.isEmpty(t1) && Utils.isEmpty(t2);
    }

    mapData() {
        if (this.model && this.commonData) {
            this.excel_fields = _.get(this.commonData, 'objTranslate.customer360.fields');
            this.excel_title = _.get(this.commonData, 'objTranslate.customer360.title');
            this.messageTable = _.get(this.commonData, 'objTranslate.collateralMessage');
            this.asset_types = _.get(this.commonData, 'asset_types') || [];
            this.asset_groups = _.get(this.commonData, 'asset_groups') || [];
            this.phoneConfig = _.get(this.commonData, 'phoneConfig') || [];
            this.emailConfig = _.get(this.commonData, 'emailConfig') || [];
            this.ageGroup = _.get(this.commonData, 'ageGroup') || [];
            this.sector = _.get(this.commonData, 'sector') || [];
            this.listBranchCode = _.map(_.get(this.commonData, 'branchesOfUser'), (item) => item.code);
            this.title = `${_.get(this.excel_title, 'year')} ${this.year}`;
        }
    }

    getAssetType(code: string) {
        return (
            _.get(
                _.chain(this.asset_types)
                    .filter((x) => x.code === code)
                    .first()
                    .value(),
                'name'
            ) || ''
        );
    }

    getAssetGroup(code: string) {
        return (
            _.get(
                _.chain(this.asset_groups)
                    .filter((x) => x.code === code)
                    .first()
                    .value(),
                'name'
            ) || ''
        );
    }

    convert_collateral(data: Array<any>) {
        this.data_collaterals = [];
        data = _.isArray(data) ? data : [];
        const listParent = JSON.parse(JSON.stringify(_.uniqBy(data, 'cltId')));
        _.forEach(listParent, (item, index) => {
            item.newId = `parent_${index}`;
            item.order = index;
            item.matDt = '';
            item.percent = '';
            item.arId = '';
            item.balFcy = item.balFcy ? formatNumber(item.balFcy || 0, 'en', '0.0-2') : item.balFcy;
            item.description = _.isEmpty(item.description) ? '---' : item.description;
            item.treeStatus = 'expanded';
        });

        _.forEach(data, (item, index) => {
            item.newId = `children_${index}`;
            item.parentCode = _.find(listParent, (i) => i.cltId === item.cltId)?.newId;
            item.order = _.find(listParent, (i) => i.cltId === item.cltId)?.order;
            item.matDt = item.matDt ? formatDate(item.matDt, 'dd/MM/yyyy', 'en') : '---';
            item.balFcy = item.balFcy ? formatNumber(item.balFcy || 0, 'en', '0.0-2') : item.balFcy;
            item.percent = item.percent ? formatNumber(item.percent || 0, 'en', '0.0-2') : item.percent;
            item.treeStatus = 'disabled';
            item.description = '';
        });
        this.data_collaterals = _.orderBy([...listParent, ...data], 'order');
        this.rows = this.data_collaterals;
    }

    onTreeAction(event: any) {
        const row = event.row;
        if (row.treeStatus === 'collapsed') {
            row.treeStatus = 'expanded';
        } else {
            row.treeStatus = 'collapsed';
        }
        this.rows = [...this.rows];
    }

    showSideBar() {
        const timer = setTimeout(() => {
            this.customerProduct?.resizeWindow();
            clearTimeout(timer);
        }, 300);
    }

    exportFile() {
        const fieldLabels = this.fields;
        let data = [];
        let obj: any = {};
        this.isLoading = true;

        if (Utils.isArrayNotEmpty(this.rows)) {
            _.forEach(this.rows, (x) => {
                obj = {};
                obj[fieldLabels.assetCode] = _.get(x, 'cltId');
                obj[fieldLabels.assetGroup] = _.get(x, 'assetGroup');
                obj[fieldLabels.assetType] = _.get(x, 'assetType');
                obj[fieldLabels.value] = _.get(x, 'balFcy');
                obj[fieldLabels.currency] = _.get(x, 'currency');
                obj[fieldLabels.contractCode] = _.get(x, 'arId');
                obj[fieldLabels.guaranteedRate] = _.get(x, 'percent');
                obj[fieldLabels.dueDate] = _.get(x, 'matDt') !== '---' ? _.get(x, 'matDt') : '';
                obj[fieldLabels.description] = _.get(x, 'description');
                data.push(obj);
            });
            this.exportExcelService.exportAsExcelFile(data, `tsdb_${this.code}`);
            this.isLoading = false;
        } else {
            this.isLoading = false;
            this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
        }
        this.isLoading = false;
    }

    export() {
        if (!this.show_error) {
            this.isLoading = true;
            var datas: Array<any> = [];
            var values = this.convertCostToExcel();
            _.forEach(values, (value) => {
                const obj = {};
                obj[_.get(this.excel_title, 'target')] = _.get(value, 'name');
                obj[`${this.previousYear}`] = _.get(value, 'n1');
                obj[`${this.twoMonthAgo}`] = _.get(value, 't2');
                obj[`${this.previousMonth}`] = _.get(value, 't1');
                datas.push(obj);
            });
            this.exportExcelService.exportAsExcelFile(datas, `Thu thập chi phí_${this.code}`, false);
            this.isLoading = false;
        } else {
            this.messageService.warn(_.get(this.excel_fields, 'empty_data'));
        }
    }

    convertCostToExcel() {
        const values = [];
        values.push({
            name: `1. ${_.get(this.excel_fields, 'netRevenueBeforeRisk')}`,
            n1: Utils.isNotNull(_.get(this.cost, 'n1.dtttrrI'))
                ? formatNumber(_.get(this.cost, 'n1.dtttrrI'), 'en', '1.0-2')
                : null,
            t2: Utils.isNotNull(_.get(this.cost, 't2.dtttrrI'))
                ? formatNumber(_.get(this.cost, 't2.dtttrrI'), 'en', '1.0-2')
                : null,
            t1: Utils.isNotNull(_.get(this.cost, 't1.dtttrrI'))
                ? formatNumber(_.get(this.cost, 't1.dtttrrI'), 'en', '1.0-2')
                : null,
        });
        values.push({
            name: `2. ${_.get(this.excel_fields, 'netRevenueAfterRisk')}`,
            n1: Utils.isNotNull(_.get(this.cost, 'n1.dttsrrI'))
                ? formatNumber(_.get(this.cost, 'n1.dttsrrI'), 'en', '1.0-2')
                : null,
            t2: Utils.isNotNull(_.get(this.cost, 't2.dttsrrI'))
                ? formatNumber(_.get(this.cost, 't2.dttsrrI'), 'en', '1.0-2')
                : null,
            t1: Utils.isNotNull(_.get(this.cost, 't1.dttsrrI'))
                ? formatNumber(_.get(this.cost, 't1.dttsrrI'), 'en', '1.0-2')
                : null,
        });
        values.push({
            name: `3. ${_.get(this.excel_fields, 'toi')}`,
            n1: Utils.isNotNull(_.get(this.cost, 'n1.toi')) ? formatNumber(_.get(this.cost, 'n1.toi'), 'en', '1.0-2') : null,
            t2: Utils.isNotNull(_.get(this.cost, 't2.toi')) ? formatNumber(_.get(this.cost, 't2.toi'), 'en', '1.0-2') : null,
            t1: Utils.isNotNull(_.get(this.cost, 't1.toi')) ? formatNumber(_.get(this.cost, 't1.toi'), 'en', '1.0-2') : null,
        });
        values.push({ name: `4. ${_.get(this.excel_fields, 'creditCollection')}` });
        values.push({
            name: `----${_.get(this.excel_fields, 'creditInterestCollection')}`,
            n1: Utils.isNotNull(_.get(this.cost, 'n1.thulaiTindungA1'))
                ? formatNumber(_.get(this.cost, 'n1.thulaiTindungA1'), 'en', '1.0-2')
                : null,
            t2: Utils.isNotNull(_.get(this.cost, 't2.thulaiTindungA1'))
                ? formatNumber(_.get(this.cost, 't2.thulaiTindungA1'), 'en', '1.0-2')
                : null,
            t1: Utils.isNotNull(_.get(this.cost, 't1.thulaiTindungA1'))
                ? formatNumber(_.get(this.cost, 't1.thulaiTindungA1'), 'en', '1.0-2')
                : null,
        });
        values.push({
            name: `----${_.get(this.excel_fields, 'interestExpense')}`,
            n1: Utils.isNotNull(_.get(this.cost, 'n1.chiftpTindungB1'))
                ? formatNumber(_.get(this.cost, 'n1.chiftpTindungB1'), 'en', '1.0-2')
                : null,
            t2: Utils.isNotNull(_.get(this.cost, 't2.chiftpTindungB1'))
                ? formatNumber(_.get(this.cost, 't2.chiftpTindungB1'), 'en', '1.0-2')
                : null,
            t1: Utils.isNotNull(_.get(this.cost, 't1.chiftpTindungB1'))
                ? formatNumber(_.get(this.cost, 't1.chiftpTindungB1'), 'en', '1.0-2')
                : null,
        });
        values.push({
            name: `----${_.get(this.excel_fields, 'earningSimilarToProfit')}`,
            n1: Utils.isNotNull(_.get(this.cost, 'n1.thuLaiKhacA2'))
                ? formatNumber(_.get(this.cost, 'n1.thuLaiKhacA2'), 'en', '1.0-2')
                : null,
            t2: Utils.isNotNull(_.get(this.cost, 't2.thuLaiKhacA2'))
                ? formatNumber(_.get(this.cost, 't2.thuLaiKhacA2'), 'en', '1.0-2')
                : null,
            t1: Utils.isNotNull(_.get(this.cost, 't1.thuLaiKhacA2'))
                ? formatNumber(_.get(this.cost, 't1.thuLaiKhacA2'), 'en', '1.0-2')
                : null,
        });
        values.push({
            name: `----${_.get(this.excel_fields, 'bondIncome')}`,
            n1: Utils.isNotNull(_.get(this.cost, 'n1.thulaiTraiphieuA3'))
                ? formatNumber(_.get(this.cost, 'n1.thulaiTraiphieuA3'), 'en', '1.0-2')
                : null,
            t2: Utils.isNotNull(_.get(this.cost, 't2.thulaiTraiphieuA3'))
                ? formatNumber(_.get(this.cost, 't2.thulaiTraiphieuA3'), 'en', '1.0-2')
                : null,
            t1: Utils.isNotNull(_.get(this.cost, 't1.thulaiTraiphieuA3'))
                ? formatNumber(_.get(this.cost, 't1.thulaiTraiphieuA3'), 'en', '1.0-2')
                : null,
        });
        values.push({ name: `5. ${_.get(this.excel_fields, 'fundraisingCollection')}` });
        values.push({
            name: `----${_.get(this.excel_fields, 'profitMBVHDV')}`,
            n1: Utils.isNotNull(_.get(this.cost, 'n1.thuftpHuydongD1'))
                ? formatNumber(_.get(this.cost, 'n1.thuftpHuydongD1'), 'en', '1.0-2')
                : null,
            t2: Utils.isNotNull(_.get(this.cost, 't2.thuftpHuydongD1'))
                ? formatNumber(_.get(this.cost, 't2.thuftpHuydongD1'), 'en', '1.0-2')
                : null,
            t1: Utils.isNotNull(_.get(this.cost, 't1.thuftpHuydongD1'))
                ? formatNumber(_.get(this.cost, 't1.thuftpHuydongD1'), 'en', '1.0-2')
                : null,
        });
        values.push({
            name: `----${_.get(this.excel_fields, 'interestExpenseOnHDV')}`,
            n1: Utils.isNotNull(_.get(this.cost, 'n1.chilaiHuydongC1'))
                ? formatNumber(_.get(this.cost, 'n1.chilaiHuydongC1'), 'en', '1.0-2')
                : null,
            t2: Utils.isNotNull(_.get(this.cost, 't2.chilaiHuydongC1'))
                ? formatNumber(_.get(this.cost, 't2.chilaiHuydongC1'), 'en', '1.0-2')
                : null,
            t1: Utils.isNotNull(_.get(this.cost, 't1.chilaiHuydongC1'))
                ? formatNumber(_.get(this.cost, 't1.chilaiHuydongC1'), 'en', '1.0-2')
                : null,
        });
        values.push({ name: `6. ${_.get(this.excel_fields, 'serviceCollection')}` });
        values.push({
            name: `----${_.get(this.excel_fields, 'guaranteeCollection')}`,
            n1: Utils.isNotNull(_.get(this.cost, 'n1.dvBaolanhE1'))
                ? formatNumber(_.get(this.cost, 'n1.dvBaolanhE1'), 'en', '1.0-2')
                : null,
            t2: Utils.isNotNull(_.get(this.cost, 't2.dvBaolanhE1'))
                ? formatNumber(_.get(this.cost, 't2.dvBaolanhE1'), 'en', '1.0-2')
                : null,
            t1: Utils.isNotNull(_.get(this.cost, 't1.dvBaolanhE1'))
                ? formatNumber(_.get(this.cost, 't1.dvBaolanhE1'), 'en', '1.0-2')
                : null,
        });
        values.push({
            name: `----${_.get(this.excel_fields, 'collectionTTQT')}`,
            n1: Utils.isNotNull(_.get(this.cost, 'n1.dvTtqtE2'))
                ? formatNumber(_.get(this.cost, 'n1.dvTtqtE2'), 'en', '1.0-2')
                : null,
            t2: Utils.isNotNull(_.get(this.cost, 't2.dvTtqtE2'))
                ? formatNumber(_.get(this.cost, 't2.dvTtqtE2'), 'en', '1.0-2')
                : null,
            t1: Utils.isNotNull(_.get(this.cost, 't1.dvTtqtE2'))
                ? formatNumber(_.get(this.cost, 't1.dvTtqtE2'), 'en', '1.0-2')
                : null,
        });
        values.push({
            name: `----${_.get(this.excel_fields, 'otherCollection')}`,
            n1: Utils.isNotNull(_.get(this.cost, 'n1.thuKhac'))
                ? formatNumber(_.get(this.cost, 'n1.thuKhac'), 'en', '1.0-2')
                : null,
            t2: Utils.isNotNull(_.get(this.cost, 't2.thuKhac'))
                ? formatNumber(_.get(this.cost, 't2.thuKhac'), 'en', '1.0-2')
                : null,
            t1: Utils.isNotNull(_.get(this.cost, 't1.thuKhac'))
                ? formatNumber(_.get(this.cost, 't1.thuKhac'), 'en', '1.0-2')
                : null,
        });
        values.push({
            name: `7. ${_.get(this.excel_fields, 'collectionKDNT')}`,
            n1: Utils.isNotNull(_.get(this.cost, 'n1.fxNteE14'))
                ? formatNumber(_.get(this.cost, 'n1.fxNteE14'), 'en', '1.0-2')
                : null,
            t2: Utils.isNotNull(_.get(this.cost, 't2.fxNteE14'))
                ? formatNumber(_.get(this.cost, 't2.fxNteE14'), 'en', '1.0-2')
                : null,
            t1: Utils.isNotNull(_.get(this.cost, 't1.fxNteE14'))
                ? formatNumber(_.get(this.cost, 't1.fxNteE14'), 'en', '1.0-2')
                : null,
        });
        values.push({
            name: `8. ${_.get(this.excel_fields, 'unusualCollection')}`,
            n1: Utils.isNotNull(_.get(this.cost, 'n1.thuBatThuong'))
                ? formatNumber(_.get(this.cost, 'n1.thuBatThuong'), 'en', '1.0-2')
                : null,
            t2: Utils.isNotNull(_.get(this.cost, 't2.thuBatThuong'))
                ? formatNumber(_.get(this.cost, 't2.thuBatThuong'), 'en', '1.0-2')
                : null,
            t1: Utils.isNotNull(_.get(this.cost, 't1.thuBatThuong'))
                ? formatNumber(_.get(this.cost, 't1.thuBatThuong'), 'en', '1.0-2')
                : null,
        });
        return values;
    }

    edit_form() {
        this.is_edit = !this.is_edit;
        if (this.is_edit) {
            this.emodel = this.model;
        }
    }

    show() {
        this.is_show = true;
    }

    close() {
        this.is_show = false;
    }

    show_form() {
        this.is_show_form = !this.is_show_form;
    }

    collaps() {
        this.is_show = !this.is_show;
    }

    update() {
        this.is_edit = !this.is_edit;
    }

    cancel() {
        this.is_edit = !this.is_edit;
    }

    generateGender(item) {
        const gender: string = _.get(item, 'gender');
        if (gender) {
            return this.fields[gender.toLowerCase()];
        } else {
            return '';
        }
    }

    generateMarialStatus(item) {
        const status: string = _.get(item, 'marialStatus');
        if (status) {
            return this.fields[status.toLowerCase()];
        } else {
            return '';
        }
    }

    onTabChanged($event) {
        this.tab_index = _.get($event, 'index');
        if (this.tab_index === 2) {
            this.onRiskTabChanged({ index: 0 });
        }
        if (this.tab_index === 4) {
            this.onRelationshipWithMBChanged({ index: 0 });
        }
        if (this.tab_index === 5) {
            this.onCreditFinanceChanged({ index: 0 });
        }
        if (this.tab_index === 6) {
            this.getDataOpportunityOfCustomer();
        }
        if (this.tab_index === 7) {
            this.onCampaignChanged({ index: 0 });
        }
        this.close();
    }

    onRelationshipWithMBChanged($event) {
        this.relationship_with_app_tab_index = _.get($event, 'index');
        if (this.relationship_with_app_tab_index === 2 || this.relationship_with_app_tab_index === 0) {
            this.getDataEarlyWarningModel();
        }
    }

    onRiskTabChanged($event) {
        this.risk_management_tab_index = _.get($event, 'index');
        // if (this.risk_management_tab_index === 0) {
        //   if (!this.load_bscore) {
        //     this.convert_bscore(this.bscores);
        //   }
        // }
        if (this.risk_management_tab_index === 0) {
            this.isLoading = false;
            this.collaterals = _.map(this.collaterals_items, (x) => ({
                ...x,
                id: `${x.id}-${x.arId}`,
                assetType: this.getAssetType(x.cltType),
                dsnapShotDt: '',
                assetGroup: this.getAssetGroup(x.cltCd),
                value_to_search: `${Utils.parseToEnglish(x.cltId)} - ${Utils.parseToEnglish(
                    this.getAssetGroup(x.cltCd)
                )} - ${Utils.parseToEnglish(this.getAssetType(x.cltType))} - ${Utils.parseToEnglish(
                    x.balFcy
                )} - ${Utils.parseToEnglish(x.arId)} - ${Utils.parseToEnglish(x.currency)} - ${Utils.parseToEnglish(
                    x.description
                )} - ${this.format.format(x.balFcy)}`,
            }));
            this.list_collaterals = [...this.collaterals];
            this.convert_collateral(this.collaterals);
        }
        if (this.risk_management_tab_index === 1) {
            this.searchCreditRatings(true);
        }
        if (this.risk_management_tab_index === 2) {
            this.getDataEarlyWarningModel();
        }

        this.close();
    }

    search() {
        if (Utils.isStringNotEmpty(this.search_value)) {
            this.search_value = Utils.trim(this.search_value);
            let datas = this.list_collaterals.filter((item) => {
                return (
                    Utils.trimNullToEmpty(item.value_to_search).toLowerCase().indexOf(Utils.parseToEnglish(this.search_value)) !==
                    -1
                );
            });
            this.convert_collateral(datas);
        } else {
            this.convert_collateral(this.collaterals);
        }
    }

    createTaskTodo() {
        const data: any = {};
        data.parentId = _.get(this.model, 'systemInfo.code');
        data.parentName = _.get(this.model, 'customerInfo.fullName');
        data.parentType = TaskType.CUSTOMER;
        const taskModal = this.modalService.open(CreateTaskModalComponent, {
            windowClass: 'create-task-modal',
            scrollable: true,
        });
        taskModal.componentInstance.data = data;
    }

    createActivity() {
        const parent: any = {};
        parent.parentType = TaskType.CUSTOMER;
        parent.parentId = _.get(this.model, 'systemInfo.code');
        parent.parentName = _.get(this.model, 'customerInfo.fullName');
        const modal = this.modalService.open(ActivityActionComponent, {
            windowClass: 'create-activity-modal',
            scrollable: true,
        });
        modal.componentInstance.parent = parent;
        modal.componentInstance.type = ScreenType.Create;
        modal.componentInstance.isShowFuture = true;
    }

    showLoading(isLoading) {
        this.isLoading = isLoading;
        this.ref.detectChanges();
    }

    ngOnDestroy() {
        const req = {
            name: 'customer-left-view',
            data: {
                parentType: TaskType.CUSTOMER,
                parentCustomerId: '',
            },
        };
        this.communicateService.request(req);
    }

    convert_bscore(datas) {
        var values = _.chain(datas)
            .groupBy((x) => x.monthValue)
            .map((item) => item)
            .value();
        this.list_bscores = [];
        if (Utils.isArrayNotEmpty(values)) {
            _.forEach(values, (x) => {
                this.list_bscores.push({
                    monthValue: _.get(_.chain(x).first().value(), 'monthValue'),
                    month: moment(_.get(_.chain(x).first().value(), 'monthValue')).format('MM/YYYY'),
                    assets: _.map(x, (x) => x.pdNm),
                    customer_rating: _.get(_.chain(x).first().value(), 'cstLvlCurr'),
                    asset_ratings: _.map(x, (x) => x.pdLvl),
                    value_size: _.size(x),
                });
            });
            this.list_bscores = _.orderBy(this.list_bscores, ['monthValue'], ['desc', 'asc']);
            this.is_show_bscore = true;
        } else {
            this.is_show_bscore = false;
        }
    }

    getCustomerDetail(value: string, url: string, disabled: boolean = false) {
        this.customerDetailApi.get(`${url}`).subscribe(
            (response) => {
                if (!disabled) {
                    this.isLoading = false;
                }
                this[value] = response;
            },
            () => {
                this.isLoading = false;
                this[value] = {};
            }
        );
    }

    mailTo() {
        const url = `mailto:${_.get(this.model, 'customerInfo.emailDisplay')}`;
        window.location.href = url;
    }

    getValue(item: any, key: string) {
        return Utils.isStringEmpty(_.get(item, key)) ? '---' : _.get(item, key);
    }

    getNumber(item: any, key: string) {
        return _.get(item, key);
    }

    revenue() {
        this.isLoading = true;
        this.revenueShareApi.checkCutomerShare(this.code).subscribe(
            (res) => {
                if (!res) {
                    this.isLoading = false;
                    this.router.navigate([functionUri.revenue_share, 'create', this.code], {
                        queryParams: {
                            showBtnRevenueShare: this.showBtnRevenueShare,
                        },
                    });
                } else {
                    this.isLoading = false;
                    this.messageService.error(this.notificationMessage.customerNotShare);
                }
            },
            (e) => {
                this.messageService.error(_.get(this.notificationMessage, 'E001'));
                this.isLoading = false;
            }
        );
    }

    getTruncateValue(item, key, limit) {
        return Utils.isStringNotEmpty(_.get(item, key)) ? Utils.truncate(_.get(item, key), limit) : '---';
    }

    generateRelationshipTimeWithMB(item) {
        var date = _.get(item, 'customerInfo.openCodeDate');
        if (Utils.isStringNotEmpty(date)) {
            var totalMonth = moment().diff(moment(date, 'DD/MM/YYYY'), 'months');
            return totalMonth > 12
                ? `${Math.floor(totalMonth / 12)} ${_.get(this.fields, 'year')}`
                : `${totalMonth} ${_.get(this.fields, 'month')}`;
        } else {
            return `0 ${_.get(this.fields, 'year')}`;
        }
    }

    convertRepresentative(model) {
        if (Utils.isNull(model)) {
            this.legalRepresentatives = [{}];
            this.chiefAccountants = [{}];
            this.contactInfos = [{}];
            this.directors = [{}];
        } else {
            var representativeInfos = _.get(model, 'legalRepresentative');
            var accountants = _.get(model, 'chiefAccountant');
            var contacts = _.get(model, 'contactInfo');
            var directorInfos = _.get(model, 'director');
            this.legalRepresentatives = Utils.isArrayNotEmpty(representativeInfos)
                ? this.convertRepresentativeInfos(representativeInfos)
                : [{}];
            this.chiefAccountants = Utils.isArrayNotEmpty(accountants) ? this.convertAccountants(accountants) : [{}];
            this.contactInfos = Utils.isArrayNotEmpty(contacts) ? this.convertContactInfos(contacts) : [{}];
            this.directors = Utils.isArrayNotEmpty(directorInfos) ? this.convertDirectors(directorInfos) : [{}];
        }
    }

    convertRepresentativeInfos(representativeInfos: Array<any>) {
        var representatives = _.filter(
            representativeInfos,
            (x) =>
                Utils.isStringNotEmpty(_.get(x, 'repName')) ||
                Utils.isStringNotEmpty(_.get(x, 'repBirthDay')) ||
                Utils.isStringNotEmpty(_.get(x, 'repEmail')) ||
                Utils.isStringNotEmpty(_.get(x, 'repIdNum')) ||
                Utils.isStringNotEmpty(_.get(x, 'repPhone'))
        );
        return Utils.isArrayNotEmpty(representatives) ? representatives : [{}];
    }

    convertAccountants(accountants: Array<any>) {
        var accounts = _.filter(
            accountants,
            (x) =>
                Utils.isStringNotEmpty(_.get(x, 'chiefAccName')) ||
                Utils.isStringNotEmpty(_.get(x, 'chiefAccBirth')) ||
                Utils.isStringNotEmpty(_.get(x, 'chiefAccMail')) ||
                Utils.isStringNotEmpty(_.get(x, 'chiefAccIdNum')) ||
                Utils.isStringNotEmpty(_.get(x, 'chiefAccPhone'))
        );
        return Utils.isArrayNotEmpty(accounts) ? accounts : [{}];
    }

    convertContactInfos(contactInfos: Array<any>) {
        var contacts = _.filter(
            contactInfos,
            (x) =>
                Utils.isStringNotEmpty(_.get(x, 'traderName')) ||
                Utils.isStringNotEmpty(_.get(x, 'traderBirthDay')) ||
                Utils.isStringNotEmpty(_.get(x, 'traderEmail')) ||
                Utils.isStringNotEmpty(_.get(x, 'traderIdNum')) ||
                Utils.isStringNotEmpty(_.get(x, 'traderPhone'))
        );
        return Utils.isArrayNotEmpty(contacts) ? contacts : [{}];
    }

    convertDirectors(directorInfos: Array<any>) {
        var directors = _.filter(
            directorInfos,
            (x) =>
                Utils.isStringNotEmpty(_.get(x, 'directorName')) ||
                Utils.isStringNotEmpty(_.get(x, 'directorBirth')) ||
                Utils.isStringNotEmpty(_.get(x, 'directorMail')) ||
                Utils.isStringNotEmpty(_.get(x, 'directorIdNum')) ||
                Utils.isStringNotEmpty(_.get(x, 'directorPhone'))
        );
        return Utils.isArrayNotEmpty(directors) ? directors : [{}];
    }

    openLegalRepresentatives() {
        this.isLegalRepresentatives = !this.isLegalRepresentatives;
    }

    openChiefAccountants() {
        this.isChiefAccountants = !this.isChiefAccountants;
    }

    openContactInfos() {
        this.isContactInfos = !this.isContactInfos;
    }

    openDirectors() {
        this.isDirectors = !this.isDirectors;
    }

    openCreditCollection() {
        this.isCreditCollection = !this.isCreditCollection;
    }

    openCapitalMobilization() {
        this.isCapitalMobilization = !this.isCapitalMobilization;
    }

    onpenServiceCollection() {
        this.isServiceCollection = !this.isServiceCollection;
    }

    onCreditFinanceChanged($event) {
        this.credit_finance_tab_index = _.get($event, 'index');
    }

    onCampaignChanged($event) {
        this.campaign_tab_index = _.get($event, 'index');
    }

    getCostValue(data, key) {
        return Utils.isNull(_.get(data, key)) ? '---' : formatNumber(_.get(data, key), 'en', '1.0-2');
    }

    onLoadingChange($event) {
        this.isLoading = $event;
        this.ref.detectChanges();
    }

    onFirstChange($event) {
        this.first = $event;
        this.ref.detectChanges();
    }

    onShowDataChange($event) {
        this.show_data_collateral = $event;
        this.ref.detectChanges();
    }

    onShowDataBiddingChange($event) {
        this.show_data_bidding = $event;
        this.ref.detectChanges();
    }

    onCollateralChange($event) {
        this.collateral = $event;
        this.ref.detectChanges();
    }

    onBiddingChange($event) {
        this.bidding = $event;
        this.ref.detectChanges();
    }

    //SonLQ

    searchCreditRatings(isSearch) {
        this.isLoading = true;
        this.customerDetailSmeApi.getDataCreditRatings(this.code).subscribe(
            (res) => {
                if (!_.isEmpty(res)) {
                    this.isLoading = false;
                    let dataMapCreditRatings = res.map((item) => {
                        item.loanCodeDTOList.forEach((element) => {
                            element.loanId = item.loanId;
                        });
                        return item.loanCodeDTOList;
                    });
                    let listCreditRatings = [];
                    dataMapCreditRatings.forEach((item) => {
                        item.forEach((i) => {
                            listCreditRatings.push(i);
                        });
                    });
                    this.listCreditRatings = listCreditRatings || [];
                    this.listCreditRatings = _.orderBy(this.listCreditRatings, [(obj) => new Date(obj.scoreDate)], ['desc']);
                } else {
                    this.isLoading = false;
                }
            },
            (e) => {
                this.messageService.error(_.get(this.notificationMessage, 'E001'));
                this.isLoading = false;
            }
        );
    }

    exportFileCreditRatings() {
        if (!_.isEmpty(this.listCreditRatings)) {
            this.isLoading = true;
            this.customerDetailSmeApi.exportFileCreditRatings(this.code).subscribe(
                (res) => {
                    if (res) {
                        this.download(res, 'Xếp hạng tín dụng_');
                    } else {
                        this.messageService.error(_.get(this.notificationMessage, 'export_error'));
                        this.isLoading = false;
                    }
                },
                () => {
                    this.messageService.error(_.get(this.notificationMessage, 'export_error'));
                    this.isLoading = false;
                }
            );
        } else {
            this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
        }
    }

    download(fileId: string, title: string) {
        let titleExcel = title + this.code + '.xlsx';
        this.fileService.downloadFile(fileId, titleExcel).subscribe((res) => {
            this.isLoading = false;
            if (!res) {
                this.messageService.error(this.notificationMessage.error);
            }
        });
    }

    getDataOpportunityOfCustomer() {
        this.isLoading = true;
        forkJoin([
            this.customerDetailSmeApi.getOpportunityOfCustomer(0, maxInt32, this.code).pipe(catchError(() => of(undefined))),
            this.commonService.getCommonCategory(CommonCategory.OPP_STATUS).pipe(catchError(() => of(undefined))),
            this.commonService.getCommonCategory(CommonCategory.TARGET_STATUS).pipe(catchError(() => of(undefined))),
            this.commonService.getCommonCategory(CommonCategory.SALE_STATUS).pipe(catchError(() => of(undefined))),
        ]).subscribe(([listOpportunity, listStatusOpp, listStatusTarget, listStatusSale]) => {
            this.isLoading = false;
            this.listOpportunity = listOpportunity?.content || [];
            this.listOpportunity.forEach((item) => {
                item.textStatusOpp = listStatusOpp?.content?.filter((i) => i.code === item.opportunityStatus)[0]?.name || '';
                item.targetSaleList.forEach((i) => {
                    i.textTargetStatus = listStatusTarget?.content?.filter((a) => a.code === i.targetStatus)[0]?.name || '';
                    i.textSaleStatus = listStatusSale?.content?.filter((a) => a.code === i.saleStatus)[0]?.name || '';
                });
            });
        });
    }

    viewDetailOpp(rowData) {
        this.router.navigate([`${functionUri.sale_opportunity}`, 'detail'], {
            skipLocationChange: true,
            queryParams: {
                customerCode: this.code,
                divisionCodeOpp: rowData.divisionCode,
                opportunityCode: rowData.opportunityCode,
                opportunityId: rowData.id,
            },
        });
    }

    viewDetailTarget(rowOpp, rowTarget) {
        this.router.navigate([`${functionUri.sale_target}`, 'detail'], {
            skipLocationChange: true,
            queryParams: {
                customerCode: this.code,
                divisionCode: rowOpp.divisionCode,
                opportunityCode: rowOpp.opportunityCode,
                targetCode: rowTarget?.targetCode,
            },
        });
    }

    viewDetailSale(rowOpp, rowTarget) {
        this.router.navigate([`${functionUri.sale_transfer}`, 'detail'], {
            skipLocationChange: true,
            queryParams: {
                customerCode: this.code,
                divisionCode: rowOpp.divisionCode,
                opportunityCode: rowOpp.opportunityCode,
                saleCode: rowTarget?.saleCode,
                targetCode: rowTarget?.targetCode,
            },
        });
    }

    getDataEarlyWarningModel() {
        this.isLoading = true;
        forkJoin([
            this.customerDetailSmeApi.getClassification({ customerCode: this.code }).pipe(catchError(() => of(undefined))),
            this.customerDetailSmeApi.getDataEarlyWarningModel(this.code).pipe(catchError(() => of(undefined))),
        ]).subscribe(([classification, listEarlyWarning]) => {
            this.classification = _.isEmpty(classification) ? '' : classification;
            if (!_.isEmpty(listEarlyWarning)) {
                this.isLoading = false;
                this.dataEarly = listEarlyWarning;
                this.previousYearWarning = `${_.get(this.fields, 'year')} ${moment(
                    _.get(this.dataEarly, 'dataY1.dataDate')
                ).format('YYYY')}`;
                this.previousMonthWarning = `${_.get(this.fields, 'year')} ${moment(
                    _.get(this.dataEarly, 'dataT1.dataDate')
                ).format('YYYY')} (${_.get(this.fields, 'month')} ${moment(_.get(this.dataEarly, 'dataT1.dataDate')).format(
                    'MM'
                )})`;

                this.twoMonthAgoWarning = `${_.get(this.fields, 'year')} ${moment(
                    _.get(this.dataEarly, 'dataT2.dataDate')
                ).format('YYYY')} (${_.get(this.fields, 'month')} ${moment(_.get(this.dataEarly, 'dataT2.dataDate')).format(
                    'MM'
                )})`;
                if (
                    _.isEmpty(_.get(this.dataEarly, 'dataY1.dataDate')) ||
                    _.isEmpty(_.get(this.dataEarly, 'dataT2.dataDate'))
                ) {
                    let dataDateT1 = _.get(this.dataEarly, 'dataT1.dataDate').split('-');
                    if (+dataDateT1[1] === 1) {
                        this.twoMonthAgoWarning =
                            `${_.get(this.fields, 'year')} ` + (dataDateT1[0] - 1) + `(${_.get(this.fields, 'month')} 12)`;
                        this.previousYearWarning = `${_.get(this.fields, 'year')} ` + (dataDateT1[0] - 1);
                    } else {
                        this.twoMonthAgoWarning =
                            `${_.get(this.fields, 'year')} ` +
                            dataDateT1[0] +
                            `(${_.get(this.fields, 'month')} ` +
                            moment((dataDateT1[1] - 1).toString()).format('MM') +
                            ')';
                        this.previousYearWarning = `${_.get(this.fields, 'year')} ` + (dataDateT1[0] - 1);
                    }
                }
                this.show_error_riskManagement = false;
            } else {
                let dataDateT1 = +moment().format('MM');
                if (+moment().format('MM') === 1) {
                    this.previousYearWarning = `${_.get(this.fields, 'year')} ` + (+moment().format('yyyy') - 2);
                    this.previousMonthWarning =
                        `${_.get(this.fields, 'year')}` +
                        (+moment().format('YYYY') - 1) +
                        `(${_.get(this.fields, 'month')} ` +
                        '12)';
                    this.twoMonthAgoWarning =
                        `${_.get(this.fields, 'year')}` +
                        (+moment().format('YYYY') - 1) +
                        `(${_.get(this.fields, 'month')} ` +
                        '11)';
                } else {
                    this.previousYearWarning = `${_.get(this.fields, 'year')} ` + (+moment().format('yyyy') - 1);
                    this.previousMonthWarning =
                        `${_.get(this.fields, 'year')} ${moment().format('YYYY')} (${_.get(this.fields, 'month')} ` +
                        moment((dataDateT1 - 1).toString()).format('MM') +
                        ')';
                    this.twoMonthAgoWarning =
                        `${_.get(this.fields, 'year')} ${moment().format('YYYY')} (${_.get(this.fields, 'month')} ` +
                        moment((dataDateT1 - 2).toString()).format('MM') +
                        ')';
                }
                this.show_error_riskManagement = true;
                this.isLoading = false;
            }
        });
    }

    // Show_hidden_button_riskManagement
    openPossibility() {
        this.isPossibility = !this.isPossibility;
    }

    openInfulenceLevel() {
        this.isInfulenceLevel = !this.isInfulenceLevel;
    }

    openEarlywarningLevel() {
        this.isEarlywarningLevel = !this.isEarlywarningLevel;
    }

    // Show_hidden_button_businessSituation
    openBusinessCustomer() {
        this.isBusinessCustomer = !this.isBusinessCustomer;
    }

    openBusinessDebit() {
        this.isBusinessDebit = !this.isBusinessDebit;
    }

    openBusinessMobilization() {
        this.isBusinessMobilization = !this.isBusinessMobilization;
    }

    openBusinessGuarantee() {
        this.isBusinessGuarantee = !this.isBusinessGuarantee;
    }

    openBusinessCommercialFinance() {
        this.isBusinessCommercialFinance = !this.isBusinessCommercialFinance;
    }

    openBusinessServiceProducts() {
        this.isBusinessServiceProducts = !this.isBusinessServiceProducts;
    }

    toggleExpandGroup(group) {
        this.tableCreditRatings.groupExpansionDefault = false;
        this.tableCreditRatings.groupHeader.toggleExpandGroup(group);
        group.expanded = !group.expanded;
    }

    exportFileCustomerExploitation() {
        if (!_.isEmpty(this.dataEarly)) {
            this.isLoading = true;
            this.customerDetailSmeApi.exportFileCustomerExploitation(this.code).subscribe(
                (res) => {
                    if (res) {
                        this.download(res, 'Tình hình khai thác khách hàng_');
                    } else {
                        this.messageService.error(_.get(this.notificationMessage, 'export_error'));
                        this.isLoading = false;
                    }
                },
                () => {
                    this.messageService.error(_.get(this.notificationMessage, 'export_error'));
                    this.isLoading = false;
                }
            );
        } else {
            this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
        }
    }

  exportFileBusinessSituation() {
    this.isLoading = true;

    const currentDate = new Date();
// Lấy tháng t-1
    const monthT = new Date(currentDate.getFullYear(), currentDate.getMonth(), 0, 23, 59, 59);
    const t1 = formatDate(monthT, 'MM/yyyy', 'en');

    // Lấy tháng t-2
    const monthT1 = new Date(currentDate.getFullYear(), currentDate.getMonth() - 1, 0, 23, 59, 59);
    const t2 = formatDate(monthT1, 'MM/yyyy', 'en');


    // Lấy tháng cuối năm của năm t -1
    const monthT2 = new Date(currentDate.getFullYear() - 1, 11, 31);
    const yearT1 = formatDate(monthT2, 'MM/yyyy', 'en');

    const listYear = yearT1.concat(',').concat(t2).concat(',').concat(t1);
    this.customerDetailSmeApi.exportFileBusinessSituation(listYear, this.code, this.branchCode).subscribe(
      (res) => {
        if (res) {
          this.download(res, 'Tình hình kinh doanh_');
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(_.get(this.notificationMessage, 'export_error'));
        this.isLoading = false;
      }
    );
  }
    getValueCreditRatings(value) {
        return value ? value : '---';
    }
}
