import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import {
  FunctionCode,
} from 'src/app/core/utils/common-constants';
import {AddressApi} from '../../api/address.api';
import {cleanDataForm} from '../../../../core/utils/function';
import _ from 'lodash';

@Component({
  selector: 'address-component',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss'],
})
export class AddressComponent extends BaseComponent implements OnInit {
  isLoading = false;
  pageable: Pageable;
  prevParams: any;
  params: any = {
    page: 0,
    size: 10
  };
  customerType = 'LEAD';
  common = {
    typeStatus: [{
      code: 'LEAD',
      displayName: 'Khách hàng chưa có code tại MB'
    },{
      code: 'CUSTOMER',
      displayName: 'Khách hàng đã có code tại MB'
    }]
  }


  constructor(
    injector: Injector,
    private api: AddressApi,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADDRESS}`);
    this.prop = this.router.getCurrentNavigation()?.extras?.state;
  }
  formSearch = this.fb.group({
    taxCode: [''],
    customerName: [''],
    phone: [''],
    certificateId: [''],
  });

  listData = [];

  ngOnInit() {
    if (this.prop) {
      this.formSearch.patchValue(this.prop);
    }
    this.search(this.params.page);
  }

  search(page) {
    cleanDataForm(this.formSearch);
    this.isLoading = true;
    if (this.customerType === 'LEAD') {
      this.api.searchLead(this.formSearch.getRawValue(), page, this.params.size)
        .subscribe(value => {
          if (value.content) {
            this.listData = value.content;
            this.changeLastestResult( this.listData);
            this.pageable = {
              totalElements: value.totalElements,
              totalPages: value.totalPages,
              currentPage: value.pageable.pageNumber,
              size: value.pageable.pageSize,
            };
          }
          this.isLoading = false;
        }, (e) => {
          this.isLoading = false;
          this.messageService.error(e?.error?.description);
        })
    }
    else {
      this.api.searchCustomer(this.formSearch.getRawValue(), page, this.params.size)
        .subscribe(value => {
          if (value.content) {
            this.listData = value.content;
            this.changeLastestResult(this.listData);
            this.pageable = {
              totalElements: value.totalElements,
              totalPages: value.totalPages,
              currentPage: value.pageable.pageNumber,
              size: value.pageable.pageSize,
            };
          }
          this.isLoading = false;
        }, (e) => {
          this.isLoading = false;
          this.messageService.error(e?.error?.description);
        })
    }
    this.prevParams = this.formSearch.getRawValue();
  }

  changeLastestResult(listData) {
    listData.forEach(item => {
      if (item.lastestResult) {
        item.lastestResult = 'Đã tiếp cận';
      }  else {
        item.lastestResult = 'Chưa tiếp cận';
      }
    })
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.page = pageInfo.offset;
    this.search(this.params.page);
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      var id = _.isEmpty(_.trim(item.id))? item.customerCode : item.id;
      if (!_.isEmpty(_.trim(item.taxCode))) {
        this.router.navigate(['/sale-manager/address/', 'detail', id , item.taxCode], {
          skipLocationChange: true,
          queryParams: {
            taxCode: item.taxCode,
            code: item.id,
            customerType : this.customerType,
            branchCode: item.branchCode
          },
          state: this.prevParams
        });
      } else {
        this.router.navigate(['/sale-manager/address/', 'detail', id , ''], {
          skipLocationChange: true,
          queryParams: {
            taxCode: item.taxCode,
            code: item.id,
            customerType : this.customerType,
            branchCode: item.branchCode
          },
        });
      }
    }
  }
  action() {

  }

}
