import { Component, OnInit, Injector, Input, Output,EventEmitter, } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import _ from 'lodash';
import { AddressApi } from '../../../api/address.api';
import { Utils } from 'src/app/core/utils/utils';

@Component({
    selector: 'app-info-relationship-oci',
    templateUrl: './info-relationship-oci.component.html',
    styleUrls: ['./info-relationship-oci.component.scss'],
})
export class InfoRelationshipOCIComponent extends BaseComponent implements OnInit {
    @Input() model: any;
    @Input() taxCode: string;
    credit_finance_tab_index: number = 0;
    code: string;
    isLoading: boolean;
    first: boolean = true;
    public notificationMessage: any;
    show_data_collateral: boolean;
    collateral: any;
    data:any;
    creditInformation: any;
    divisionProduct: any;
    models:any;
    constructor(
        private api: AddressApi,
        injector: Injector) {
        super(injector);
    }

    ngOnInit() {
        this.creditInformation = this.model;
        if (this.first) {
            this.api.getCollateralInformation(this.taxCode).subscribe(
                (response) => {
                    this.data = response;
                },
                () => {
                    this.messageService.error(_.get(this.notificationMessage, 'E001'));
                }
            );
        } else {
            this.models = _.get(this.data, 'listCollateralBusiness');
        }
    }

    onCreditFinanceChanged($event) {
        this.credit_finance_tab_index = _.get($event, 'index');
    }

    onLoadingChange($event) {
        this.isLoading = $event;
        this.ref.detectChanges();
    }

    onFirstChange($event) {
        this.first = $event;
        this.ref.detectChanges();
    }

    onCollateralChange($event) {
        this.collateral = $event;
        this.ref.detectChanges();
    }

    onShowDataChange($event) {
        this.show_data_collateral = $event;
        this.ref.detectChanges();
    }
}