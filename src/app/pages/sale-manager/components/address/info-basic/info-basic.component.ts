import { forkJoin, of } from 'rxjs';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Component, Injector, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { IInfoBasic } from '../../../model/info-basic.interface';
import { AddressApi } from '../../../api/address.api';
import { CustomerApi, CustomerAssignmentApi } from 'src/app/pages/customer-360/apis';
import { CustomerDetailSmeApi } from 'src/app/pages/customer-360/apis/customer.api';
import _ from 'lodash';
import { FunctionCode,Scopes } from 'src/app/core/utils/common-constants';
import { Utils } from 'src/app/core/utils/utils';
import { catchError, retry } from 'rxjs/operators';
import { Location } from '@angular/common';
import {ActivityLogComponent} from 'src/app/pages/customer-360/components/activity-log/activity-log.component';
@Component({
  selector: 'app-info-basic',
  templateUrl: './info-basic.component.html',
  styleUrls: ['./info-basic.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class InfoBasicComponent extends BaseComponent implements OnInit, OnDestroy {
  @ViewChild(ActivityLogComponent) activityLog: ActivityLogComponent;
  columns = [
    {
      name: "Họ và tên",
      prop: "fullName",
      sortable: false,
      draggable: false,
    },
    {
      name: "Chức danh",
      prop: "position",
      sortable: false,
      draggable: false,
    },
    {
      name: "Ngày sinh",
      prop: "dateOfBirth",
      sortable: false,
      draggable: false,
    },
    {
      name: "Số điện thoại",
      prop: "phoneNumber",
      sortable: false,
      draggable: false,
    },
    {
      name: "Email",
      prop: "email",
      sortable: false,
      draggable: false,
    },

  ];

  tab_index: number = 0;
  model: any;
  customerInfo: any;
  systemInfo: any;
  businessInfo: any;
  otherInfo: any;
  registrationNumber: any;
  is_show: boolean = false;
  isCustomer: boolean = false;
  isLoading = true;
  rows: Array<any> = [];
  creditInformation: any;
  rmManager: string;
  segmentCustomer: string;
  contactInfos: any;
  customerTypeMerge: string;
  isLegalRepresentatives: boolean;
  isChiefAccountants: boolean;
  isContactInfos: boolean;
  isDirectors: boolean;
  isManager: boolean =  false;
  warningPermission :string = "";

  legalRepresentatives: any;
  chiefAccountants: any;
  directors: any;

  public code: string;
  public taxCode: string;
  public customerType: string;
  public info: IInfoBasic;

  constructor(
    private customerApi: CustomerApi,
    private customerDetailSmeApi: CustomerDetailSmeApi,
    private customerAssignmentApi: CustomerAssignmentApi,
    private api: AddressApi,
    private _location: Location,
    injector: Injector,
  ) {
    super(injector);
    this.code = _.get(this.route.snapshot.params, 'code');
    this.taxCode = _.get(this.route.snapshot.queryParams, 'taxCode');
    this.customerType = _.get(this.route.snapshot.queryParams, 'customerType');
  }

  ngOnInit() {
    this.isLoading = true;
    if (this.customerType === 'LEAD') {
      this.isCustomer = false;
      this.api.getAddressById(this.code).subscribe(value => {
        this.info = value;
        this.info.status = (this.info.status === undefined) ? 'Không có thông tin' : this.info.status;
        this.registrationNumber = value?.esbtCode;
        this.isLoading = false;
        const paramCreditInfo = {
          identifiedNumber: this.taxCode,
        };
        if (this.taxCode) {
          this.getCreditInformation(paramCreditInfo);
        }
      }, (e) => {
        this.isLoading = false;
        this.messageService.error(e?.error?.description);
      })
    } else {
      this.isCustomer = true;
      var rsId = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADDRESS}`)?.rsId;
      forkJoin([
        this.api.getAddressCustomerById(this.code, rsId, 'VIEW'),
        this.customerApi.getRmManagerByCustomerCode(this.code).pipe(catchError(() => of(undefined))),
        this.customerDetailSmeApi.getSegmentByCustomerCode(this.code).pipe(catchError(() => of(undefined))),
      ]).subscribe(
        ([itemCustomer, rmManager, segment]) => {
          this.model = itemCustomer;
          this.segmentCustomer = segment;
          this.rmManager = rmManager;
          var contacts = _.get(this.model, 'contactInfo');

          this.isManager = _.get(this.model, 'isManager');
          this.warningPermission = "Bạn không có quyền xem dữ liệu này !";
          var customerInfo = _.get(this.model, 'customerInfo');
          this.convertRepresentative(_.get(this.model, 'representativeInfo'));

          this.registrationNumber = customerInfo.registrationNumber;
          const paramCreditInfo = {
            identifiedNumber: this.registrationNumber,
          };
          this.getCreditInformation(paramCreditInfo);
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
          this.messageService.error(_.get(this.notificationMessage, 'E001'));
        }
      );
    }

  }

  convertRepresentative(model) {
    if (Utils.isNull(model)) {
      this.legalRepresentatives = [{}];
      this.chiefAccountants = [{}];
      this.contactInfos = [{}];
      this.directors = [{}];
    } else {
      var representativeInfos = _.get(model, 'legalRepresentative');
      var accountants = _.get(model, 'chiefAccountant');
      var contacts = _.get(model, 'contactInfo');
      var directorInfos = _.get(model, 'director');
      this.legalRepresentatives = Utils.isArrayNotEmpty(representativeInfos)
        ? this.convertRepresentativeInfos(representativeInfos)
        : [{}];
      this.chiefAccountants = Utils.isArrayNotEmpty(accountants) ? this.convertAccountants(accountants) : [{}];
      this.contactInfos = Utils.isArrayNotEmpty(contacts) ? this.convertContactInfos(contacts) : [{}];
      this.directors = Utils.isArrayNotEmpty(directorInfos) ? this.convertDirectors(directorInfos) : [{}];
    }
  }

  convertAccountants(accountants: Array<any>) {
    var accounts = _.filter(
      accountants,
      (x) =>
        Utils.isStringNotEmpty(_.get(x, 'chiefAccName')) ||
        Utils.isStringNotEmpty(_.get(x, 'chiefAccBirth')) ||
        Utils.isStringNotEmpty(_.get(x, 'chiefAccMail')) ||
        Utils.isStringNotEmpty(_.get(x, 'chiefAccIdNum')) ||
        Utils.isStringNotEmpty(_.get(x, 'chiefAccPhone'))
    );
    return Utils.isArrayNotEmpty(accounts) ? accounts : [{}];
  }

  convertRepresentativeInfos(representativeInfos: Array<any>) {
    var representatives = _.filter(
      representativeInfos,
      (x) =>
        Utils.isStringNotEmpty(_.get(x, 'repName')) ||
        Utils.isStringNotEmpty(_.get(x, 'repBirthDay')) ||
        Utils.isStringNotEmpty(_.get(x, 'repEmail')) ||
        Utils.isStringNotEmpty(_.get(x, 'repIdNum')) ||
        Utils.isStringNotEmpty(_.get(x, 'repPhone'))
    );
    return Utils.isArrayNotEmpty(representatives) ? representatives : [{}];
  }

  convertDirectors(directorInfos: Array<any>) {
    var directors = _.filter(
      directorInfos,
      (x) =>
        Utils.isStringNotEmpty(_.get(x, 'directorName')) ||
        Utils.isStringNotEmpty(_.get(x, 'directorBirth')) ||
        Utils.isStringNotEmpty(_.get(x, 'directorMail')) ||
        Utils.isStringNotEmpty(_.get(x, 'directorIdNum')) ||
        Utils.isStringNotEmpty(_.get(x, 'directorPhone'))
    );
    return Utils.isArrayNotEmpty(directors) ? directors : [{}];
  }

  onTabChanged($event) {
    this.tab_index = _.get($event, 'index');
    this.close();
  }

  convertContactInfos(contactInfos: Array<any>) {
    var contacts = _.filter(
      contactInfos,
      (x) =>
        Utils.isStringNotEmpty(_.get(x, 'traderName')) ||
        Utils.isStringNotEmpty(_.get(x, 'traderBirthDay')) ||
        Utils.isStringNotEmpty(_.get(x, 'traderEmail')) ||
        Utils.isStringNotEmpty(_.get(x, 'traderIdNum')) ||
        Utils.isStringNotEmpty(_.get(x, 'traderPhone'))
    );
    return Utils.isArrayNotEmpty(contacts) ? contacts : [{}];
  }

  getValue(item: any, key: string) {
    return Utils.isStringEmpty(_.get(item, key)) ? '---' : _.get(item, key);
  }

  getTruncateValue(item, key, limit) {
    return Utils.isStringNotEmpty(_.get(item, key)) ? Utils.truncate(_.get(item, key), limit) : '---';
  }

  getNumber(item: any, key: string) {
    return _.get(item, key);
  }

  onRiskTabChanged($event) {
    // this.risk_management_tab_index = _.get($event, 'index');
  }
  show() {
    this.is_show = true;
  }

  close() {
    this.is_show = false;
  }
  collaps() {
    this.is_show = !this.is_show;
  }
  getCreditInformation(paramCreditInfo) {
    this.customerApi.getCreditInformation2(paramCreditInfo).subscribe(
      (response: any) => {
        this.creditInformation = response;
      },
      (e) => {
        if (e?.error?.description) {
          this.messageService.error(e?.error?.description);
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'E001'));
        }
      }
    );
  }
  openContactInfos() {
    this.isContactInfos = !this.isContactInfos;
  }

  openChiefAccountants() {
    this.isChiefAccountants = !this.isChiefAccountants;
  }

  openDirectors() {
    this.isDirectors = !this.isDirectors;
  }

  openLegalRepresentatives() {
    this.isLegalRepresentatives = !this.isLegalRepresentatives;
  }

  sendMailToPerson(item: any, key: string) {
    if (!Utils.isStringEmpty(_.get(item, key))) {
      if (this.customerTypeMerge === 'sme') {
        const data = {
          type: 'EMAIL',
          customerCode: this.code,
          scope: Scopes.VIEW,
          rsId: this.objFunction.rsId
        }
        this.customerAssignmentApi.updateActivityLog(data).subscribe(value => {
          if (value) {
            this.activityLog.getListActivityLog();
          } else {
          }
        });
      }
      const url = `mailto:${_.get(item, key)}`;
      window.location.href = url;
    }

  }

  getClassStatus(status) {
    // if (status == null) {
    //   this.info.status = 'Không có thông tin';
    //   return 'status-gray';
    // }
    if (status === 'NNT đang hoạt động (đã được cấp GCN ĐKT)' || status === 'NNT đang hoạt động (được cấp thông báo MST)') {
      return 'status-green';
    } else if (status === 'NNT tạm nghỉ kinh doanh có thời hạn' || status === 'NNT đã chuyển sang tỉnh khác') {
      return 'status-yellow' ;
    } else if (status === 'NNT không hoạt động tại địa chỉ đã đăng ký'
        || status === 'NNT không hoạt động tại địa chỉ đăng ký'
        || status === 'Không có thông tin') {
      return 'status-gray';
    } else if (status === 'NNT ngừng hoạt động'
        || status === 'NNT ngừng hoạt động nhưng chưa hoàn thành thủ tục đóng MST'
        || status === 'NNT ngừng hoạt động và đã đóng MST'
        || status === 'NNT đã chuyển cơ quan thuế quản lý') {
      return 'status-red';
    }
  }
}
