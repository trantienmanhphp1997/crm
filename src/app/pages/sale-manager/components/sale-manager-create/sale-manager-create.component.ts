import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import {
  CommonCategory,
  Division,
  FunctionCode,
  maxInt32,
  Scopes,
  StatusWork,
} from 'src/app/core/utils/common-constants';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import _ from 'lodash';
import { Utils } from 'src/app/core/utils/utils';
import { global } from '@angular/compiler/src/util';
import { RmApi, RmBlockApi } from 'src/app/pages/rm/apis';
import { ProductModalComponent } from '../product-model/product-modal.component';
import { CustomerApi, CustomerDetailApi, CustomerDetailSmeApi } from 'src/app/pages/customer-360/apis/customer.api';
import { SaleManagerApi } from '../../api';
import * as moment from 'moment';

@Component({
  selector: 'sale-manager-create-component',
  templateUrl: './sale-manager-create.component.html',
  styleUrls: ['./sale-manager-create.component.scss'],
})
export class SaleManagerCreateComponent extends BaseComponent implements OnInit {
  isLoading = false;
  @ViewChild('table') table: DatatableComponent;
  @ViewChild('tableTarget') tableTarget: DatatableComponent;
  pageable: Pageable;
  listBranches = [];
  listDivision = [];
  listStatusShare = [];
  listRmManager = [];
  formCheckInfo = this.fb.group({
    customerCode: [''],
    divisionCode: [''],
  });

  formProduct = this.fb.group({
    opportunityBranch: [''],
    assignTo: [''],
  });
  objFunctionRm: any;
  limit = global.userConfig.pageSize;
  model: any = {};
  rmManager: any;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private rmApi: RmApi,
    private customerDetailApi: CustomerDetailApi,
    private customerApi: CustomerApi,
    private saleManagerApi: SaleManagerApi,
    private rmBlockApi: RmBlockApi,
    private customerDetailSmeApi: CustomerDetailSmeApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.OPPORTUNITY}`);
    this.objFunctionRm = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
  }

  params: any = {
    pageNumber: 0,
    pageSize: 10,
    rsId: '',
    scope: Scopes.VIEW,
  };
  is_show = false;
  showData = false;
  showMessage = false;
  dataDivisionBranch: any = {};
  listBranchesOpp = [];
  rmLevelCode = '';
  btnCheckCustomer = true;

  pageableProduct: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };

  paramsTableProduct = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };

  listDataProduct = [];
  dataProduct = [];

  paramSearchDivision = {
    size: global.userConfig.pageSize,
    page: 0,
    name: '',
    code: '',
  };
  lstLevelRmCode: any;
  listDivisioTitle: any;
  segmentCustomer: string;

  ngOnInit() {
    this.isLoading = true;
    forkJoin([
      this.categoryService
        .getBranchesOfUser(this.objFunctionRm?.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined))),
      this.rmBlockApi
        .fetch({ page: 0, size: maxInt32, hrsCode: this.currUser?.hrsCode, isActive: true })
        .pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.KHOI_PRIORITY).pipe(catchError(() => of(undefined))),
      this.categoryService.searchBlocksCategory(this.paramSearchDivision).pipe(catchError(() => of(undefined))),
    ]).subscribe(([branchesOfUser, divisionOfSystem, listConfigDivision, divisionAll]) => {
      // KHOI_PRIORITY
      listConfigDivision = listConfigDivision?.content || [];
      listConfigDivision = _.orderBy(listConfigDivision, ['value']);
      let divisionConfig = listConfigDivision?.map((item) => item.code);

      // LIST TITLE
      this.listDivisioTitle =
        divisionOfSystem?.content?.map((item) => {
          return {
            divisionCode: item?.blockCode,
            levelRmCode: item?.levelRMCode === undefined ? '' : item?.levelRMCode,
          };
        }) || [];

      this.saleManagerApi.getTitleDivisionBranch(this.listDivisioTitle).subscribe((res) => {
        if (res) {
          this.dataDivisionBranch = res;
          const divisionOfUser =
            divisionAll?.content?.map((item) => {
              return {
                code: item.code,
                displayName: item.code + ' - ' + item.name,
                order:
                  _.findIndex(divisionConfig, (value) => value === item.code) === -1
                    ? 99
                    : _.findIndex(divisionConfig, (value) => value === item.code),
              };
            }) || [];

          let listDivision = this.dataDivisionBranch.map((item) => item.ruleDivision.division).toString();
          listDivision = _.unionBy(_.split(listDivision, ','));
          let listBranchConfig = this.dataDivisionBranch.map((item) => item.ruleDivision.branch).toString();
          listBranchConfig = _.unionBy(_.split(listBranchConfig, ','));
          this.lstLevelRmCode = this.dataDivisionBranch.map((item) => item.ruleDivision.ruleAssign).toString();
          this.lstLevelRmCode = _.unionBy(_.split(this.lstLevelRmCode, ','));

          if (!_.isEmpty(listDivision[0])) {
            if (listDivision.findIndex((item) => item === 'All') > -1) {
              this.listDivision = divisionOfUser;
            } else {
              divisionOfUser?.forEach((item) => {
                if (!_.isEmpty(listDivision.find((i) => i === item.code))) {
                  this.listDivision.push(item);
                }
              });
            }
          } else {
            this.listDivision =
              divisionOfSystem?.content?.map((item) => {
                return {
                  code: item.blockCode,
                  displayName: item.blockCode + ' - ' + item.blockName,
                  order:
                    _.findIndex(divisionConfig, (value) => value === item.blockCode) === -1
                      ? 99
                      : _.findIndex(divisionConfig, (value) => value === item.blockCode),
                };
              }) || [];
          }

          this.listDivision = _.orderBy(this.listDivision, ['order']);
          this.formCheckInfo.controls.divisionCode.setValue(_.first(this.listDivision)?.code, { emitEvent: false });
          this.getBranchByUser(listBranchConfig, branchesOfUser);
          this.isLoading = false;
        }
      });
    });
  }

  getBranchByUser(listBranchConfig, branchesOfUser) {
    const param = {
      listBranchCode: listBranchConfig,
      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW,
    };
    this.saleManagerApi.getBranchByUser(param).subscribe((res) => {
      this.listBranchesOpp = res.map((item) => {
        return {
          code: item.code,
          displayName: item.code + ' - ' + item.name,
        };
      });
      this.formProduct.controls.opportunityBranch.setValue(_.first(this.listBranchesOpp)?.code, { emitEvent: false });
      this.getRmByBranch(this.formProduct.controls.opportunityBranch.value);
    });
  }

  getRmByBranch(branchCode) {
    let listBranchCode = [];
    listBranchCode.push(branchCode);
    this.formProduct.controls.assignTo.disable();
    const param = {
      lstBranchCode: listBranchCode,
      lstLevelRmCode: this.lstLevelRmCode,
      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW,
    };
    this.saleManagerApi.findRmByBranchAndLevelCode(param).subscribe((res) => {
      if (res) {
        this.listRmManager = res?.map((item) => {
          if (!_.isEmpty(item?.rmCode) && item?.active && item?.statusWork !== StatusWork.NTS) {
            return {
              code: item.rmCode,
              displayName: item.rmCode + ' - ' + item.rmName,
              name: item.rmName,
            };
          }
        });
        if (_.isEmpty(this.lstLevelRmCode[0])) {
          if (this.formProduct.controls.opportunityBranch.value === this.currUser.branch) {
            this.formProduct.controls.assignTo.setValue(_.first(this.listRmManager)?.code);
          } else {
            this.listRmManager = [];
            this.formProduct.controls.assignTo.setValue('');
          }
        } else {
          this.listRmManager = this.listRmManager.filter((item) => !_.isEmpty(item));
          this.formProduct.controls.assignTo.setValue(_.first(this.listRmManager)?.code);
        }
        this.formProduct.controls.assignTo.enable();
      }
    });
  }

  ngAfterViewInit() {
    this.formProduct.controls.opportunityBranch.valueChanges.subscribe((value) => {
      this.getRmByBranch(value);
    });

    this.formCheckInfo.controls.divisionCode.valueChanges.subscribe((value) => {
      this.btnCheckCustomer = true;
      this.showData = false;
    });
    this.formCheckInfo.controls.customerCode.valueChanges.subscribe((value) => {
      this.btnCheckCustomer = true;
      this.is_show = false;
      this.showData = false;
      this.model = [];
      this.dataProduct = [];
      this.mapDataProduct();
    });
  }

  onChangeEventType(event) {
    this.is_show = false;
    this.model = [];
    this.dataProduct = [];
    this.mapDataProduct();
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.pageNumber = pageInfo.offset;
    this.mapDataProduct();
  }

  addProduct() {
    this.rmLevelCode = this.listDivisioTitle.find(
      (item) => this.formCheckInfo.get('divisionCode').value === item.divisionCode
    )?.levelRmCode;
    const modalConfirm = this.modalService.open(ProductModalComponent, {
      windowClass: 'list__rm-modal',
    });
    modalConfirm.componentInstance.customerDivision = this.formCheckInfo.get('divisionCode').value;
    modalConfirm.componentInstance.rmLevelCode = this.rmLevelCode;
    modalConfirm.componentInstance.listSelectedOld = this.dataProduct;
    modalConfirm.result
      .then((res) => {
        if (res) {
          this.dataProduct = res.listSelected;
          if (this.dataProduct.length > 0) {
            this.is_show = true;
          } else {
            this.is_show = false;
          }
          this.mapDataProduct();
        }
      })
      .catch(() => {});
  }

  searchDetailCustomer() {
    if (!this.formCheckInfo.controls.customerCode.value) {
      this.messageService.error('Mã KH bắt buộc nhập');
      return;
    }
    this.isLoading = true;
    if (this.formCheckInfo.controls.divisionCode.value === Division.SME) {
      forkJoin([
        this.saleManagerApi
          .customerDetailOpportunity(this.formCheckInfo.controls.customerCode.value.trim())
          .pipe(catchError(() => of(undefined))),
        this.customerApi
          .getRmManagerByCustomerCode(this.formCheckInfo.controls.customerCode.value.trim())
          .pipe(catchError(() => of(undefined))),
        this.customerDetailSmeApi
          .getSegmentByCustomerCode(this.formCheckInfo.controls.customerCode.value.trim())
          .pipe(catchError(() => of(undefined))),
      ]).subscribe(([itemCustomer, rmManager, segment]) => {
        this.isLoading = false;
        this.segmentCustomer = segment;
        const divisionCustomer = _.get(itemCustomer, 'systemInfo.accountType', '');
        if (divisionCustomer === this.formCheckInfo.controls.divisionCode.value || divisionCustomer === 'NHS') {
          this.btnCheckCustomer = false;
          this.showData = true;
          this.showMessage = false;
          this.model = itemCustomer;
          this.rmManager = rmManager;
        } else {
          this.showMessage = true;
          this.showData = false;
          this.model = {};
        }
      });
    } else {
      forkJoin([
        this.saleManagerApi
          .customerDetailOpportunity(this.formCheckInfo.controls.customerCode.value.trim())
          .pipe(catchError(() => of(undefined))),
        this.customerApi
          .getRmManagerByCustomerCode(this.formCheckInfo.controls.customerCode.value.trim())
          .pipe(catchError(() => of(undefined))),
      ]).subscribe(([itemCustomer, rmManager]) => {
        this.isLoading = false;
        // this.segmentCustomer = segment;
        const divisionCustomer = _.get(itemCustomer, 'systemInfo.accountType', '');
        if (divisionCustomer !== this.formCheckInfo.controls.divisionCode.value) {
          this.showMessage = true;
          this.showData = false;
          this.model = {};
        } else {
          this.btnCheckCustomer = false;
          this.showData = true;
          this.showMessage = false;
          this.model = itemCustomer;
          this.rmManager = rmManager;
          this.segmentCustomer = itemCustomer?.customerInfo?.customerSegment;
        }
      });
    }
  }

  getTruncateValue(item, key, limit) {
    return Utils.isStringNotEmpty(_.get(item, key)) ? Utils.truncate(_.get(item, key), limit) : '---';
  }

  mapDataProduct() {
    const total = this.dataProduct.length;
    this.pageableProduct.totalElements = total;
    this.pageableProduct.totalPages = Math.floor(total / this.pageableProduct.size);
    this.pageableProduct.currentPage = this.paramsTableProduct.page;
    const start = this.paramsTableProduct.page * this.paramsTableProduct.size;
    this.listDataProduct = this.dataProduct?.slice(start, start + this.paramsTableProduct.size);
  }

  removeRecord(item) {
    _.remove(this.dataProduct, (i) => i.productCode === item.productCode);
    _.remove(this.listDataProduct, (i) => i.productCode === item.productCode);
    if (this.listDataProduct.length === 0 && this.pageableProduct.currentPage > 0) {
      this.paramsTableProduct.page -= 1;
    }
    this.listDataProduct = [...this.listDataProduct];
    if (this.dataProduct.length === 0) {
      this.is_show = false;
    }
    this.mapDataProduct();
  }
  createOpportunity() {
    if (_.isEmpty(this.formProduct.get('assignTo').value)) {
      this.messageService.warn('Chọn RM cơ hội');
      return;
    }
    this.isLoading = true;
    let assignToName = this.listRmManager.filter((item) => item.code === this.formProduct.get('assignTo').value)[0]
      .name;
    let productDTOList = this.dataProduct.map((item) => {
      return _.omit(item, ['customerDivision', 'rmLevelCode', 'isChecked']);
    });
    const registrationDate = this.model?.customerInfo?.registrationDate?.split('/');
    let registrationConvert;
    if (registrationDate) {
      registrationConvert = moment(new Date(registrationDate[2], registrationDate[1], registrationDate[0])).format(
        'YYYY-MM-DD'
      );
    }
    const opportunityBranchName = this.listBranchesOpp
      .find((item) => item.code === this.formProduct.get('opportunityBranch').value)
      .displayName?.split('-')[1]
      .trim();
    const paramCrate = {
      fullName: this.model?.customerInfo?.fullName || '',
      registrationDate: registrationConvert || '',
      nationality: this.model?.customerInfo?.nationality || '',
      mobileNumber: this.model?.customerInfo?.mobileNumber || '',
      registrationNumber: this.model?.customerInfo?.registrationNumber || '',
      employeesNo: this.model?.customerInfo?.employeesNo || '',
      customerCode: this.model?.systemInfo?.code || '',
      accountType: this.model?.systemInfo?.accountType || '',
      divisionCode: this.formCheckInfo.get('divisionCode').value,
      address: this.model?.businessInfo?.address || '',
      sectorName: this.model?.systemInfo?.sector || '',
      industryCode: this.model.customerInfo.industryCode || '',
      segment: this.segmentCustomer || '',
      branchCode: this.model?.systemInfo?.branchCode || '',
      rmCode: this.rmManager || '',
      opportunityBranch: this.formProduct.get('opportunityBranch').value,
      assignTo: this.formProduct.get('assignTo').value,
      productDTOList: productDTOList,
      assignToName: assignToName,
      opportunityBranchName: opportunityBranchName,
    };
    this.saleManagerApi.createOpportunity(paramCrate).subscribe(
      (result) => {
        this.isLoading = false;
        this.messageService.success(this.notificationMessage.success);
        this.is_show = false;
        this.back();
      },
      (error) => {
        this.messageService.error(this.notificationMessage.error);
        this.isLoading = false;
      }
    );
  }

  valiDateSpace(event) {
    this.formCheckInfo.controls.customerCode.setValue(event.target.value.trim());
  }
}
