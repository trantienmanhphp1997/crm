import { Component, OnInit, Injector, ViewEncapsulation, HostBinding, Input } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import {createGroupChat11} from '../../../../core/utils/mb-chat';
import {SaleSuggestApi} from '../../api/sale-suggest.api';
import {FunctionCode} from '../../../../core/utils/common-constants';

@Component({
  selector: 'app-group-mbchat-modal',
  templateUrl: './group-mbchat-modal.component.html',
  styleUrls: ['./group-mbchat-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class GroupMbchatModalComponent extends BaseComponent implements OnInit {
  @HostBinding('class.list__group-mbchat-content') appRightContent = true;
  isLoading = false;
  @Input() listData: any;
  @Input() data: any;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private modalActive: NgbActiveModal,
    private saleSuggestApi: SaleSuggestApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.APP_CHAT}`);

  }

  ngOnInit() {
  }

  closeModal() {
    this.modalActive.close(false);
  }

  onActive(event) {
    return
    // if (event.type === 'click') {
    //   event.cellElement.blur();
    //   const item = event.row;
    //   createGroupChat11(item?.groupId, true)
    // }
  }

  createGroupChatIgnoreFilter() {
    this.isLoading = true;
    this.data.isCheckFilter = false;
    this.saleSuggestApi.groupChat(this.data).subscribe(res => {
      if (res && res.status === 200) {
        if (this.objFunction) {
          this.messageService.success('Tạo nhóm chat thành công!');
          createGroupChat11(res?.data?.groupId, true);
        } else {
          this.messageService.success('Tạo nhóm chat thành công. Truy cập ứng dụng để gửi tin nhắn tới nhóm vừa tạo');
        }
        this.modalActive.close(false);
      }
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
      this.messageService.error(error?.error?.description);
    });
  }

  goGroupChat(item) {
    if (this.objFunction) {
      createGroupChat11(item?.groupId, true);
    } else {
      this.messageService.success('Truy cập ứng dụng App chat để gửi tin nhắn tới nhóm');
    }
  }
}
