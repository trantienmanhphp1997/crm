import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellingOppIndivComponent } from './selling-opp-indiv.component';

describe('SellingOppIndivComponent', () => {
  let component: SellingOppIndivComponent;
  let fixture: ComponentFixture<SellingOppIndivComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellingOppIndivComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellingOppIndivComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
