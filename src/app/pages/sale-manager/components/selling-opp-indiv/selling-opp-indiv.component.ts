import { AfterViewInit, EventEmitter } from '@angular/core';
import { ElementRef } from '@angular/core';
import { global, newArray } from '@angular/compiler/src/util';
import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import _ from 'lodash';
import { forkJoin, of } from 'rxjs';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import {
  ActionType,
  CommonCategory,
  ConfirmType,
  Division,
  FormType,
  FunctionCode,
  functionUri,
  maxInt32,
  ProductLevel,
  Scopes,
  SessionKey, SourceOp,
  StatusWork,
} from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { RmApi, RmBlockApi } from 'src/app/pages/rm/apis';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { SaleManagerApi } from '../../api';
import { cleanDataForm } from 'src/app/core/utils/function';
import { ProductService } from 'src/app/core/services/product.service';
import { ActivationStart } from '@angular/router';
import { Utils } from 'src/app/core/utils/utils';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { SellingOppIndivCreateEditModalComponent } from '../selling-opp-indiv-create-edit-modal/selling-opp-indiv-create-edit-modal-component';
import { FileService } from '../../../../core/services/file.service';
import { LayoutService } from 'src/app/core/services/layout.service';
@Component({
  selector: 'app-selling-opp-indiv',
  templateUrl: './selling-opp-indiv.component.html',
  styleUrls: ['./selling-opp-indiv.component.scss'],
})
export class SellingOppIndivComponent extends BaseComponent implements OnInit {
  isLoading = false;
  @ViewChild('table') table: DatatableComponent;
  limit = global.userConfig.pageSize;
  pageable: Pageable;
  params: any = {
    pageNumber: 0,
    pageSize: global?.userConfig?.pageSize,
    rsId: '',
    scope: Scopes.VIEW,
  };
  listData = [];
  chooseSmart = '';
  listSmartBank = [];
  listAllSmartBank = [];
  listDivision = [];
  listRmManager = [];
  listBranchesOpp = [];
  listBranchesSME = [];
  listStatusOpp = [];
  listProduct = [];
  listProductSelected = [];
  listCreatedBy = [];
  listOpportunityFilter = [{
    code: null,
    displayName: 'Tất cả'
  }, {
    code: 1,
    displayName: 'Cơ hội của tôi'
  }, {
    code: 2,
    displayName: 'Cơ hội có thể nhận'
  }];

  listOpportunityFilterSME = [{
    code: 0,
    displayName: 'Cơ hội của tôi'
  },
  {
    code: 1,
    displayName: 'Cơ hội có thể nhận'
  },
  {
    code: 2,
    displayName: 'Cơ hội tôi tạo'
  }];

  lstLevelRmCode: any;
  prevParams: any;

  dataDivisionBranch: any = {};

  total: number = 0;
  isOpenMore = true;

  model: any = {};

  paramSearchDivision = {
    size: global.userConfig.pageSize,
    page: 0,
    name: '',
    code: '',
  };
  permissionOpp = false;

  obj: any;
  scopes: Array<any>;

  customerCode: any;
  isAllChance = false;
  isAssigned = false;
  listProductChild = [];
  listAllSource = [];
  listSource = [

  ];
  listSourceSME = [
    { code: '', name: 'Tất cả' },
    { code: 'MISA', name: 'Misa' },
    { code: 'RM', name: 'Tự khai thác' },
    { code: 'KHAC', name: 'Nguồn khác' },
  ]
  listStatusOppSME = [];
  currentBlockCode;
  listBranchAssign = [];

  @ViewChild('customerName') customerName: ElementRef
  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private saleManagerApi: SaleManagerApi,
    private rmApi: RmApi,
    private productService: ProductService,
    private fileService: FileService,
    private layoutService: LayoutService
  ) {
    super(injector);
    // this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.SELLING_OPPORTUNITY_INDIV}`);
    // console.log(this.objFunction);
    // this.obj = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.scopes = _.get(this.objFunction, 'scopes');
    this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
    layoutService.bannerObs.next(false);
  }

  formSearch = this.fb.group({
    code: '',
    customerCode: '',
    customerName: '',
    national: '',
    customerTypeMerge: '',
    branchOpp: '',
    memberCode: '',
    status: '',
    productCode: '',
    productCodeLvl2: '',
    createdBy: '',
    rsId: '',
    scope: Scopes.VIEW,
    oppFilter: null,
    source: '',
    businessRegistration: null,
    atmId: null
  });

  ngOnInit() {
    this.initData();
    this.pageable = {
      totalElements: 0,
      totalPages: 1,
      currentPage: this.params.pageNumber,
      size: this.limit,
    };
    this.subscribeForm();
  }

  checkScopes(codes: Array<any>) {
    if (Utils.isArrayEmpty(codes)) return false;
    return Utils.isArrayNotEmpty(_.filter(this.scopes, (x) => codes.includes(x)));
  }

  initData() {
    this.prop = this.sessionService.getSessionData(FunctionCode.SELLING_OPPORTUNITY_INDIV);

    this.router.events.subscribe((e) => {
      if (e instanceof ActivationStart && e.snapshot?.data?.code !== FunctionCode.SELLING_OPPORTUNITY_INDIV) {
        this.prop = null;
        this.sessionService.setSessionData(FunctionCode.SELLING_OPPORTUNITY_INDIV, null);
      }
    });
    // console.log(this.prop);
    if (this.prop && this.prop?.prevParams?.customerTypeMerge == Division.INDIV) {
      this.prevParams = { ...this.prop?.prevParams };
      //this.formSearch.patchValue(this.prevParams);
      this.listDivision = this.prop?.listDivision || [];
      this.listBranchesOpp = this.prop?.listBranchesOpp || [];
      this.listRmManager = this.prop?.listRmManager || [];
      this.listStatusOpp = this.prop?.listStatusOpp || [];
      this.listProduct = this.prop?.listProduct || [];
      this.listCreatedBy = this.prop?.listCreatedBy || [];
      this.isAllChance = this.prop?.prevParams?.isAllChance || false;
      this.listProductChild = this.prop?.listProductChild || [];
      this.listAllSource = this.prop?.listAllSource || [];
      this.listAllSmartBank = this.prop?.listAllSmartBank || [];
      this.listSource = this.prop?.listSource || [];
      this.listSmartBank = this.prop?.listSmartBank || [];
      this.listProductSelected = [];
      this.formSearch.patchValue(this.prevParams);
      if (!this.formSearch.value.productCode) {
        this.formSearch.controls.status.disable();
      }
      else {
        this.getFilterStatus(this.formSearch.value.productCode);
      }
      if (!this.prevParams.productCode) {
        this.listProduct.map((item) => {
          this.listProductSelected.push(parseInt(item.idProductTree));
        });
        this.formSearch.controls.productCode.setValue(this.listProductSelected);
      } else {
        this.formSearch.controls.productCode.setValue(this.prevParams.productCode);
      }

      this.params.pageSize = this.prevParams.pageSize;
      this.params.pageNumber = this.prevParams.pageNumber;
      if (this.isAllChance) {
        if (this.prop?.prevParams?.isChance) {
          this.search(false, true);
          this.formSearch.controls.oppFilter.setValue(2)
        } else {
          this.search(false, true, true);
          this.formSearch.controls.oppFilter.setValue(1);
        }
      } else {
        this.search(false, false);
      }
    } else {
      this.isLoading = true;
      const param = {
        BRANCH_CODE_CAP2: '',
        rsId: this.objFunction?.rsId,
        scope: Scopes.VIEW
      };
      forkJoin([
        this.saleManagerApi.getProductByLevel().pipe(catchError((e) => of([]))),
        this.productService.getProductByLevel(ProductLevel.Lvl1).pipe(catchError(() => of([]))),
        this.categoryService.getCommonCategory(CommonCategory.STATUS_SALE_OPPORTUNITY).pipe(catchError((e) => of(undefined))),
        this.categoryService.getCommonCategory(CommonCategory.SOURCE_SALE_OPPORTUNITY).pipe(catchError((e) => of(undefined))),
        this.saleManagerApi.getListSmartBank(param).pipe(catchError((e) => of(undefined))),
        this.saleManagerApi.getBranches(this.objFunction.rsId, Scopes.VIEW).pipe(catchError((e) => of([]))),
        this.categoryService.getCommonCategory(CommonCategory.SALE_OPPORTUNITY_SOURCE).pipe(catchError((e) => of(undefined))),
        this.categoryService.getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW).pipe(catchError(() => of(undefined))),
        // this.saleManagerApi.getListSmartBank({BRANCH_CODE_CAP2: 'VN0010065'}).pipe(catchError((e) => of(undefined))),
      ]).subscribe(([listProductLvl3, listProductLvl1, STATUS_SALE_OPPORTUNITY, lstSource, lstSmartBank, getBranches, SALE_OPPORTUNITY_SOURCE, getBranchesOfUser]) => {
        this.listBranchesSME = getBranches.map(
          (item) => {
            return {
              code: item.code,
              displayName: item.code + ' - ' + item.name,
            };
          }
        );
        this.listSourceSME = SALE_OPPORTUNITY_SOURCE?.content;
        this.listSourceSME.unshift({ code: '', name: this.fields.all });
        this.listBranchesSME.unshift({ code: '', displayName: this.fields.all });
        console.log('lstSmartBank: ', lstSmartBank)
        this.listAllSmartBank = [...lstSmartBank];
        this.listSmartBank = [...lstSmartBank];

        if (_.isEmpty(this.listSmartBank)) {
          this.listSmartBank[0] = {
            ATM_ID: null, SMB_NM: this.fields.all
          }
        }
        else {
          this.listSmartBank.unshift({ ATM_ID: null, SMB_NM: this.fields.all });
        }
        console.log('listAllSmart: ', this.listAllSmartBank);
        if (!_.isEmpty(this.listSmartBank) && this.formSearch.value.source === SourceOp.SMARTBANK) {
          this.formSearch.controls.atmId.setValue(this.listSmartBank[0].ATM_ID);
        }
        // Product
        const listProductTemp = listProductLvl1?.map((item) => {
          const idProductTree = item.idProductTree ? item.idProductTree : '';
          const description = item.description ? item.description : '';
          return {
            idProductTree: parseInt(item.idProductTree),
            description: idProductTree + ' - ' + description,
          };
        });
        this.listProductChild = listProductLvl3?.map((item) => {
          return { code: item.idProductTree, name: `${item.idProductTree} - ${item.description}`, statusList: item.statusList };
        });
        this.listProduct = listProductTemp;
        this.listProduct.unshift({ idProductTree: '', description: this.fields.all });
        this.listProductChild.unshift({ code: '', name: this.fields.all });
        listProductTemp.map((item) => {
          this.listProductSelected.push(parseInt(item.idProductTree));
        });
        this.formSearch.controls.productCode.setValue(this.listProductSelected);

        // KHOI
        let listDivision = _.map(this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM), (item) => {
          return {
            titleCode: item.titleCode,
            levelCode: item.levelCode,
            code: item.code,
            displayName: `${item.code} - ${item.name}`,
          };
        });
        this.listDivision = listDivision;
        this.listAllSource = lstSource?.content || [];
        this.formSearch.controls.customerTypeMerge.setValue(listDivision[0].code || null);
        this.fillSourceByBlock(Division.INDIV);
        // BRANCH & RM
        this.getBranchByUser();
        this.getRmByBranch([], false);
        this.prop = {
          listDivision: this.listDivision,
          listStatusOpp: this.listStatusOpp,
          listProduct: this.listProduct,
          listProductChild: this.listProductChild,
          listAllSource: this.listAllSource,
          listAllSmartBank: this.listAllSmartBank,
          listSource: this.listSource,
          listSmartBank: this.listSmartBank
          // listCreatedBy: this.listCreatedBy,
        };
        this.sessionService.setSessionData(FunctionCode.SELLING_OPPORTUNITY_INDIV, this.prop);
        this.isLoading = false;
        this.listStatusOppSME = STATUS_SALE_OPPORTUNITY?.content;
        this.listStatusOppSME.unshift({ code: '', name: this.fields.all });
        this.formSearch.controls.productCode.setValue('');
        if (this.state?.formSearch) {
          this.formSearch.patchValue(this.state?.formSearch);
        }
        this.listBranchAssign = getBranchesOfUser?.map?.(item => item.code) || [];
      },
        (e) => {
          console.log(e);
          this.messageService.error(this.notificationMessage.error);
          this.isLoading = false;
        }
      );
    }
  }
  getBranchByUser() {
    this.listBranchesOpp = [];
    this.categoryService.getBranchesOfUser(this.objFunction.rsId, Scopes.VIEW).subscribe((res) => {
      this.listBranchesOpp = res.map(
        (item) => {
          return {
            code: item.code,
            displayName: item.code + ' - ' + item.name,
          };
        },
        (e) => {
          console.log(e);
          this.messageService.error(this.notificationMessage.error);
          this.isLoading = false;
        }
      );
      this.prop = { ...this.prop, listBranchesOpp: this.listBranchesOpp };
      this.sessionService.setSessionData(FunctionCode.SELLING_OPPORTUNITY_INDIV, this.prop);
      this.listBranchesOpp.unshift({ code: '', displayName: this.fields.all });
    });
  }

  getRmByBranch(listBranchCode, isStart?: boolean) {
    this.formSearch.controls.memberCode.disable();
    this.formSearch.controls.createdBy.disable();
    this.formSearch.controls.atmId.setValue(null);
    if (_.isEmpty(listBranchCode) || _.isEmpty(listBranchCode[0])) {
      this.listSmartBank = [...this.listAllSmartBank];
    }
    else {
      this.listSmartBank = this.listAllSmartBank.filter((i) => i.BRANCH_CODE_CAP2 === listBranchCode[0]);
    }
    //this.listSmartBank.unshift({ atmId: null, smbName: this.fields.all });
    if (_.isEmpty(this.listSmartBank)) {
      this.listSmartBank[0] = {
        ATM_ID: null, SMB_NM: this.fields.all
      }
    }
    else {
      this.listSmartBank.unshift({ ATM_ID: null, SMB_NM: this.fields.all });
    }
    if (!_.isEmpty(this.listSmartBank) && this.formSearch.value.source === SourceOp.SMARTBANK) {
      this.formSearch.controls.atmId.setValue(this.listSmartBank[0].ATM_ID);
    }
    const body = {
      page: 0,
      size: maxInt32,
      crmIsActive: true,
      branchCodes: listBranchCode?.filter((code) => !Utils.isStringEmpty(code)),
      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW,
    }
    if(this.formSearch.getRawValue()?.customerTypeMerge != Division.INDIV) {
      body['rmBlock'] = this.formSearch.getRawValue()?.customerTypeMerge;
    }
    forkJoin([ this.rmApi.post('findAll',body )]).subscribe(
        ([res]) => {

          // RM
          const listRm: any[] =
            res?.content?.map((item) => {
              return {
                code: item?.t24Employee?.employeeCode || '',
                displayName:
                  Utils.trimNullToEmpty(item?.t24Employee?.employeeCode) +
                  ' - ' +
                  Utils.trimNullToEmpty(item?.hrisEmployee?.fullName),
                hrsCode: item?.hrisEmployee?.employeeId
              };
            }) || [];
          this.listRmManager = listRm.filter((item) => {
            return item.code !== this.formSearch.get('memberCode').value?.split(' ')[0] && item.code;
          });
          this.listRmManager.unshift({ code: '',hrsCode: '', displayName: this.fields.all });
          this.formSearch.controls.memberCode.setValue(_.first(this.listRmManager)?.code);
          this.formSearch.controls.memberCode.enable();

          // PRESENTER
          const listPresenter: any[] =
            res?.content?.map((item) => {
              return {
                code: item?.hrisEmployee?.userName || '',
                fullName:
                  Utils.trimNullToEmpty(item?.t24Employee?.employeeCode) +
                  ' - ' +
                  Utils.trimNullToEmpty(item?.hrisEmployee?.fullName),
              };
            }) || [];
          this.listCreatedBy = listPresenter.filter((item) => {
            return item.code !== this.formSearch.get('createdBy').value?.split(' ')[0] && item.code;
          });
          this.listCreatedBy.unshift({ code: '', fullName: this.fields.all });
          this.formSearch.controls.createdBy.setValue(_.first(this.listCreatedBy)?.code);
          this.formSearch.controls.createdBy.enable();

          this.prop = { ...this.prop, listRmManager: this.listRmManager, listCreatedBy: this.listCreatedBy };
          this.sessionService.setSessionData(FunctionCode.SELLING_OPPORTUNITY_INDIV, this.prop);

          if (isStart) {
            this.search(true);
          } else {
            // this.isLoading = false;
          }
        },
        (e) => {
          console.log(e);
          this.messageService.error(this.notificationMessage.error);
          this.isLoading = false;
        }
      );
  }

  ngAfterViewInit() {

  }



  search(isSearch: boolean, isAllChance?: boolean, assigned?: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.params.pageNumber = 0;
    }
    if (this.formSearch.controls.customerTypeMerge.value === Division.INDIV && isSearch && !isAllChance && !assigned) {
      this.formSearch.controls.oppFilter.setValue(null);
    }
    let params = {
      ...cleanDataForm(this.formSearch),
      pageNumber: this.params.pageNumber,
      pageSize: this.params.pageSize,
      rsId: this.objFunction.rsId,
    };
    if (this.formSearch.controls.customerTypeMerge.value != Division.INDIV) {
      params['view'] = params.oppFilter;
    }

    params.productCode = null;
    const productCode = this.formSearch.controls.productCode.value || '';
    if (productCode.length !== this.listProduct.length) {
      params.productCode = productCode;
    }

    if (isAllChance && assigned) {
      this.isAllChance = true;
      this.resetForm();
      params = {
        isChance: false,
        pageNumber: this.params.pageNumber,
        pageSize: this.params.pageSize,
        rsId: this.objFunction.rsId,
        scope: 'VIEW',
        isAllChance: this.isAllChance,
      };
    } else if (isAllChance) {
      this.isAllChance = true;
      this.resetForm();
      params = {
        isChance: true,
        pageNumber: this.params.pageNumber,
        pageSize: this.params.pageSize,
        rsId: this.objFunction.rsId,
        scope: 'VIEW',
        isAllChance: this.isAllChance,
      };
    } else {
      this.isAllChance = false;
    }
    if (this.formSearch.controls.customerTypeMerge.value == Division.INDIV) {
      delete params.businessRegistration;
    }
    if (this.formSearch.controls.status.disabled) {
      this.formSearch.controls.status.setValue(null);
    }

    const rsIdApp = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.SALE_OPPORTUNITY_APP}`);
    this.formSearch.controls.customerTypeMerge.value != Division.INDIV ? params.rsId = rsIdApp?.rsId : null;
    let api = this.formSearch.controls.customerTypeMerge.value == Division.INDIV ? this.saleManagerApi.searchListOpportunity(params) : this.saleManagerApi.searchSMEListOpportunity(params);
    // console.log(params);
    api.subscribe(
      (result) => {
        if (result) {
          this.prevParams = params;
          this.prop.prevParams = params;
          let listDataTemp = result.content.forEach((item, index) => {
            item.customer = item.customerCode + ' - ' + item.customerName;
            if (item.colour) {
              item.colourDisplay = JSON.parse(item.colour);
            };
          });
          this.listData = result?.content;
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: this.params.pageSize,
          };
        }
        this.currentBlockCode = this.formSearch.controls.customerTypeMerge.value;
        this.sessionService.setSessionData(FunctionCode.SELLING_OPPORTUNITY_INDIV, this.prop);
        this.isLoading = false;
      },
      (e) => {
        console.log(e);
        this.messageService.error(this.notificationMessage.error);
        this.isLoading = false;
      }
    );
  }

  resetForm() {
    this.formSearch.controls.code.setValue('');
    this.formSearch.controls.customerCode.setValue('');
    this.formSearch.controls.customerName.setValue('');
    this.formSearch.controls.national.setValue('');
    //  this.formSearch.controls.customerTypeMerge.setValue(Division.INDIV);
    this.formSearch.controls.branchOpp.setValue(_.head(this.listBranchesOpp)?.code);
    this.formSearch.controls.memberCode.setValue(_.head(this.listRmManager)?.code);
    this.formSearch.controls.status.setValue(null);
    this.formSearch.controls.status.disable();
    this.formSearch.controls.source.setValue('');
    // this.formSearch.controls.status.setValue(_.head(this.listStatusOpp)?.code);
    let listProductTemp = [...this.listProduct];
    this.listProductSelected = [];
    listProductTemp.map((item) => {
      this.listProductSelected.push(parseInt(item.idProductTree));
    });
    this.formSearch.controls.productCode.setValue(this.listProductSelected);
    this.formSearch.controls.createdBy.setValue(_.head(this.listCreatedBy)?.code);
  }

  createOpp() {
    console.log(this.formSearch.getRawValue());
    this.router.navigate([this.router.url, 'create'], {
      skipLocationChange: true,
      queryParams: {
        url: this.router.url,
      },
      state: {
        formSearch: this.formSearch.getRawValue()
      }
    });
  }

  takeAllChance() {
    this.isAssigned = !this.isAssigned;
    if (this.isAssigned) {
      this.search(true, true, false);
    } else {
      this.search(true, true, true);
    }
  }

  takeAChance(row) {
    this.isLoading = true;
    if(this.currentBlockCode != Division.INDIV) {
      const param = {
        code: row.code,
        status: true,
      }
      this.saleManagerApi.getAChanceSME(param).subscribe(res => {
        this.messageService.success('Nhận cơ hội thành công');
        this.search(false);
      }, err => {
        this.isLoading = false;
        this.messageService.error('Thực hiện không thành công');
      })
      return;
    }
    const param = {
      code: row.code,
      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW,
    };
    this.saleManagerApi.getAChance(param).subscribe(
      (res) => {
        this.messageService.success(this.notificationMessage.success);
        if (this.isAllChance) {
          if (this.formSearch.controls.oppFilter.value === 2) {
            this.search(false, true);
          } else {
            this.search(false, true, true);
          }
        } else {
          this.search(false);
        }
      },
      (e) => {
        console.log(e);
        this.messageService.error('Thực hiện không thành công');
        this.isLoading = false;
      }
    );
  }

  edit(row) {
    if (this.currentBlockCode != Division.INDIV) {
      this.router.navigate(['sale-manager/selling-opp-sme', 'create'], {
        queryParams: {
          code: row?.code,
          mode: 'edit',
          customerCode: row?.customerCode,
          leadCode: row?.leadCode,
          blockCode: this.currentBlockCode,
          businessRegistrationNumber: row?.registrationNumber,
          searchQuickType: row?.customerType == 'KH360' || row?.customerType == 'KHHH' ? 'KHHH' : row?.customerType
        },
        state: {
          data: row,
          formSearch: this.formSearch.getRawValue()
        }
      });
      return;
    }
    this.isLoading = true;
    this.saleManagerApi.opportunityINDIVDetail(row.customerCode, '', this.objFunction.rsId, Scopes.VIEW).subscribe(value => {
      if (value) {
        this.isLoading = false;
        const data = _.find(value, item => item.saleOppId === row.saleOppId);
        const modal = this.modalService.open(SellingOppIndivCreateEditModalComponent, {
          windowClass: 'app-create-edit-selling',
          scrollable: true,
        });
        modal.componentInstance.customerCode = row.customerCode;
        modal.componentInstance.dataEdit = data;
        modal.componentInstance.isEditDisplay = true;
        modal.componentInstance.title = data?.code + ' - ' + data?.subProductName;
        modal.result
          .then((res) => {
            if (res) {
              this.isLoading = true;
              if (this.formSearch.controls.oppFilter.value === 1) {
                this.search(false, true, true);
              } else if (this.formSearch.controls.oppFilter.value === 2) {
                this.search(false, true, false);
              } else {
                this.search(false);
              }
            }
          })
      }
    });
  }

  openMore() {
    this.isOpenMore = !this.isOpenMore;
  }

  handleChangePageSize(event) {
    this.params.pageNumber = 0;
    this.params.pageSize = event;
    this.limit = event;
    if (this.isAllChance) {
      if (this.formSearch.controls.oppFilter.value === 2) {
        this.search(false, true);
      } else {
        this.search(false, true, true);
      }
    } else {
      this.search(false);
    }
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.pageNumber = pageInfo.offset;
    if (this.isAllChance) {
      if (this.formSearch.controls.oppFilter.value === 2) {
        this.search(false, true);
      } else {
        this.search(false, true, true);
      }
    } else {
      this.search(false);
    }
  }

  // onActive(event) {
  //   if (event.type === 'dblclick') {
  //     event.cellElement.blur();
  //     const item = _.get(event, 'row');
  //     this.router.navigate([this.router.url, 'detail'], {
  //       skipLocationChange: true,
  //       queryParams: {
  //         actionType: ActionType.UPDATE,
  //         formType: FormType.UPDATE,
  //         customerCode: item.customerCode,
  //         code: item.code,
  //         canEdit: item.updator && this.checkScopes(['UPDATE'])
  //       },
  //     });
  //   }
  // }

  onActive(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      let path = this.currentBlockCode === Division.INDIV ? 'detail-v2' : 'detail-sme';
      this.router.navigate([this.router.url, path], {
        skipLocationChange: false,
        queryParams: {
          customerCode: item.customerCode,
          saleOppId: item.saleOppId,
          code: item?.code
        },
        state: {
          formSearch: this.formSearch.getRawValue()
        }
      });
    }
  }

  navigate360Detail(item) {
    this.isLoading = true;
    const param = {
      customerCode: item.customerCode || item.leadCode,
      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW,
    };
    if (item.customerType === 'KH360') {
      this.saleManagerApi.checkPermissionToKH360(param).subscribe(
        (res) => {
          console.log(res);
          if (res.data) {
            localStorage.setItem('DETAIL_CUS_ID', JSON.stringify(item.customerCode));
            localStorage.setItem('BLOCK_CODE_OPP', JSON.stringify(this.currentBlockCode));
            const url = this.router.serializeUrl(
              this.router.createUrlTree([functionUri.customer_360_manager], {
                skipLocationChange: true,
              })
            );
            window.open(url, '_blank');
          } else {
            this.openModalInfo();
          }
          this.isLoading = false;
        },
        (e) => {
          console.log(e);
          this.messageService.error('Thực hiện không thành công');
          this.isLoading = false;
        }
      );
    }
    else {
      this.isLoading = false;
      if(this.currentBlockCode != Division.INDIV) {
        const url = this.router.serializeUrl(
          this.router.createUrlTree([functionUri.lead_management, 'detail', item?.leadCode], {
            skipLocationChange: true,
            queryParams: {
              customerType: '1'
            }
          })
        );
        window.open(url, '_blank');
        return;
      }
      this.saleManagerApi.checkPermissionToKHTN(param).subscribe(
        (res) => {
          console.log(res);
          if (res.data) {
            const url = this.router.serializeUrl(
              this.router.createUrlTree([functionUri.lead_management, 'detail', res.id], {
                skipLocationChange: true,
                queryParams: {
                  customerType: '0'
                }
              })
            );
            window.open(url, '_blank');
          } else {
            this.openModalInfo();
          }
          this.isLoading = false;
        },
        (e) => {
          console.log(e);
          this.messageService.error('Thực hiện không thành công');
          this.isLoading = false;
        }
      );
    }
  }

  openModalInfo() {
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.componentInstance.type = ConfirmType.CusError;
    confirm.componentInstance.message = 'Bạn không có quyền xem thông tin KH này';
    confirm.result
      .then((res) => {
        if (res) {
          console.log(res);
        }
      })
      .catch(() => { });
  }

  onChangeProduct(event) {

    if (this.isLoading || this.formSearch.controls.customerTypeMerge.value != 'INDIV') {
      return;
    }
    this.formSearch.controls.status.enable();
    this.formSearch.controls.status.setValue(null);
    this.getFilterStatus(event.value);
  }

  onChangeProductLvl2(event) {
    if (this.isLoading || this.formSearch.controls.customerTypeMerge.value != 'INDIV') {
      this.getProductChildren(event.value);
      this.formSearch.controls.productCode.setValue('', { emitEvent: false });
      this.formSearch.controls.status.setValue('');
      return;
    }
    this.listStatusOpp = [];
    this.formSearch.controls.productCode.setValue('');
    this.formSearch.controls.status.setValue(null);
    this.formSearch.controls.status.disable();
    this.getProductChildren(event.value);
  }

  getFilterStatus(product: string) {
    this.listStatusOpp = [];
    if (product) {
      this.listStatusOpp = _.find(this.listProductChild, item => item.code === product).statusList.map(item => {
        return {
          code: item?.status,
          displayName: item?.title
        }
      });
      this.listStatusOpp.unshift({ code: null, displayName: 'Tất cả' });
    }
  }

  changeOppFilter(event) {
    if (this.formSearch.controls.customerTypeMerge.value != Division.INDIV) {
      this.search(true, false, false);
      return;
    }
    this.listStatusOpp = [];
    if (event.value === null) {
      this.resetForm();
      this.search(true);
    } else if (event.value === 1) {
      this.search(true, true, true);
    } else if (event.value === 2) {
      this.search(true, true, false);
    }
  }

  getProductChildren(id) {

    this.saleManagerApi.getProductByLevel(id, this.formSearch.controls.customerTypeMerge.value).subscribe((list) => {
      this.listProductChild = list?.map((item) => {
        return { code: item.idProductTree, name: `${item.idProductTree} - ${item.description}`, statusList: item.statusList };
      });
      this.listProductChild.unshift({ code: '', name: this.fields.all });
    });
  }

  exportFile() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+(this.pageable?.totalElements || 0) === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    let params: any = {};
    if (this.formSearch.controls.oppFilter.value === null) {
      params = {
        ...cleanDataForm(this.formSearch),
        pageNumber: this.params.pageNumber,
        pageSize: this.params.pageSize,
        rsId: this.objFunction.rsId,
      };
      params.productCode = null;
      const productCode = this.formSearch.controls.productCode.value || '';
      if (productCode.length !== this.listProduct.length) {
        params.productCode = productCode;
      }
    } else if (this.formSearch.controls.oppFilter.value === 1) {
      params = {
        isChance: false,
        pageNumber: this.params.pageNumber,
        pageSize: this.params.pageSize,
        rsId: this.objFunction.rsId,
        scope: 'VIEW',
        isAllChance: this.isAllChance,
      };
    } else if (this.formSearch.controls.oppFilter.value === 2) {
      params = {
        isChance: true,
        pageNumber: this.params.pageNumber,
        pageSize: this.params.pageSize,
        rsId: this.objFunction.rsId,
        scope: 'VIEW',
        isAllChance: this.isAllChance,
      };
    }
    this.isLoading = true;
    this.saleManagerApi.exportSaleOpp(params).subscribe((fileId) => {
      if (!_.isEmpty(fileId)) {
        this.fileService
          .downloadFile(fileId, 'sale_opp.xlsx')
          .pipe(catchError((e) => of(false)))
          .subscribe((res) => {
            this.isLoading = false;
            if (!res) {
              this.messageService.error(this.notificationMessage.error);
            }
          });
      } else {
        this.messageService.error(this.notificationMessage?.export_error || '');
        this.isLoading = false;
      }
    }, (e) => {
      const error = JSON.parse(_.get(e, 'error'));

      if (e && e.error && error) {
        this.messageService.error(error.messages.vn);
      } else {
        this.messageService.error(this.notificationMessage?.export_error || '');
      }
      this.isLoading = false;
    });
  }
  fillSourceByBlock(block) {
    this.listSource = this.listAllSource.filter((i) => i.value.includes(block));
    if (this.listSource.length > 1) {
      this.listSource.unshift({ code: '', name: this.fields.all });
    }
    this.formSearch.controls.source.setValue(this.listSource[0]?.code);
  }

  subscribeForm() {
    this.formSearch.controls.branchOpp.valueChanges.subscribe((value) => {
      if (this.formSearch.controls.customerTypeMerge.value != Division.INDIV) {
        if(!this.listBranchAssign.length) {
          if(this.currUser?.branch != value && value){
            this.listCreatedBy = [];
            this.listRmManager = [];
          } else {
            this.getRmByBranch([], false);
          }
          return;
        }
        if (!value) {
          this.getRmByBranch(this.listBranchAssign, false);
          return;
        }
        if (this.listBranchAssign.includes(value)) {
          this.getRmByBranch([value], false);
        } else {
          this.listCreatedBy = [];
          this.listRmManager = [];
        }
        return;
      }
      let branchOpp = [];
      if (_.isEmpty(value)) {
        branchOpp.push('');
      } else {
        branchOpp.push(value);
      }
      this.getRmByBranch(branchOpp, false);
    });

    this.formSearch.controls.source.valueChanges.subscribe((value) => {
      //  this.listSmartBank = [];
      this.formSearch.controls.atmId.setValue(null);
      console.log('smartBank: ', this.listSmartBank);
      if (!_.isEmpty(this.listSmartBank) && this.formSearch.value.source === SourceOp.SMARTBANK) {
        this.formSearch.controls.atmId.setValue(this.listSmartBank[0].ATM_ID);
      }
    });
    this.formSearch.controls.customerTypeMerge.valueChanges.subscribe(value => {
      this.fillSourceByBlock(value);
      if (value === Division.INDIV) {
        this.formSearch.controls.oppFilter.setValue(null);
        this.formSearch.controls.branchOpp.setValue('');
      } else {
        this.formSearch.controls.oppFilter.setValue(0);
        this.formSearch.controls.branchOpp.setValue('');
      }
    });
    this.formSearch.controls.productCode.valueChanges.subscribe(value => {
      if (this.formSearch.controls.customerTypeMerge.value != Division.INDIV) {
        if (!value) {
          this.formSearch.controls.status.setValue('', { emitEvent: false });
        }
      }
    })
  }
}
