import { forkJoin, of } from 'rxjs';
import {AfterViewInit, Component, HostBinding, Injector, Input, OnInit, ViewEncapsulation} from '@angular/core';
import { Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {
  CommonCategory, Division,
  FunctionCode,
  Scopes, SourceOp
} from 'src/app/core/utils/common-constants';
import { cleanDataForm } from 'src/app/core/utils/function';
import { BaseComponent } from 'src/app/core/components/base.component';
import { catchError } from 'rxjs/operators';
import _ from 'lodash';
import {SaleManagerApi} from '../../api';
import {CategoryService} from '../../../system/services/category.service';
import {Utils} from '../../../../core/utils/utils';
import {ConfirmDialogComponent} from "../../../../shared/components";


@Component({
  selector: 'app-create-edit-selling-modal',
  templateUrl: './selling-opp-indiv-create-edit-modal.component.html',
  styleUrls: ['./selling-opp-indiv-create-edit-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SellingOppIndivCreateEditModalComponent extends BaseComponent implements OnInit, AfterViewInit {
  @HostBinding('class.app-create-edit-selling') classCreateTask = true;

  @Input() title?: any;
  @Input() customerCode?: any;
  @Input() dataEdit?: any;
  @Input() isEditDisplay = false;
  listSource = [];
  isFirstEdit = true;
  form = this.fb.group({
    saleOppId: null,
    productCode: ['', Validators.required],
    customerCode: '',
    branchCode: [this.currUser?.branch, Validators.required],
    memberHrsCode: [null],
    memberCode: [null],
    currency: ['VND', Validators.required],
    amount: [null, Validators.required],
    status: [null, Validators.required],
    guideHrsCode: [this.currUser?.hrsCode],
    guideCode: [this.currUser?.code],
    note: [''],
    source: '',
    atmId: null,
    smbName: ''
  });
  listProduct: any;
  listCurrencies = [{
    code: 'USD',
    name: 'USD'
  },{
    code: 'VND',
    name: 'VND'
  }];
  listBranches: any;
  listStatusOpp: any;
  listRmManager: any;
  listPresenter: any;
  oldForm: any;
  formChange = false;
  listSmartBank = [];
  listAllSmartBank = [];
  constructor(
    injector: Injector,
    private saleManagerApi: SaleManagerApi,
    private categoryService: CategoryService,
    private modalActive: NgbActiveModal,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.SELLING_OPPORTUNITY_INDIV}`);
    this.isLoading = true;
  }

  ngAfterViewInit() {
    this.form.get('memberHrsCode').valueChanges.subscribe((value) => {
      this.form.get('memberCode').setValue(_.chain(this.listRmManager)
                                                    .find((i) => i.memberHrsCode  === value)
                                                    .value()?.rmCode || null,
                                                    { emitEvent: false });
    });
    this.form.get('guideHrsCode').valueChanges.subscribe((value) => {
      this.form.get('guideCode').setValue(_.chain(this.listPresenter)
                                                    .find((i) => i.guideHrsCode  === value)
                                                    .value()?.code || null
                                                    , { emitEvent: false });
    });
    this.form.controls.source.valueChanges.subscribe((value => {
      if(value === SourceOp.SMARTBANK){
        // console.log('check required');
        // this.form.controls.atmId.setValidators(Validators.required);
        this.form.controls.atmId.setValue(this.listSmartBank[0].ATM_ID);
      }
      // else{
      //   console.log('bo check required');
      //   this.form.controls.atmId.clearValidators();
      // }
   //   this.form.controls.atmId.updateValueAndValidity({ emitEvent: false });
      console.log('form: ', this.form);
    }));


    this.form.valueChanges.subscribe((value) => {
      if (!this.isLoading) {
        this.checkFormChange(value);
      }
    });
  }

  ngOnInit(): void {
    console.log(this.dataEdit);
    const paramSearchSmart = {
      BRANCH_CODE_CAP2: 'ALL',
      rsId: this.objFunction?.rsId,
      scope: Scopes.VIEW
    };
    forkJoin([
      this.saleManagerApi.getProductByLevel().pipe(catchError((e) => of(undefined))),
      this.saleManagerApi.getBranches(this.objFunction?.rsId, Scopes.VIEW).pipe(catchError((e) => of(undefined))),
      this.categoryService.getCommonCategory(CommonCategory.CURRENCY_TYPE).pipe(catchError(() => of(undefined))),
      this.saleManagerApi.getPresenter(),
      this.categoryService.getCommonCategory(CommonCategory.SOURCE_SALE_OPPORTUNITY).pipe(catchError((e) => of(undefined))),
      this.saleManagerApi.getListSmartBank(paramSearchSmart).pipe(catchError((e) => of(undefined))),
    ]).subscribe(([listProduct, listBranches, listCurrencyType, listPresenter, listSource, listSmartBank]) => {
      // PRODUCT
      this.listProduct = listProduct?.map((item) => {
        return { code: item.idProductTree, displayName: item.idProductTree + ' - ' + item.description };
      });
      this.listAllSmartBank = listSmartBank;
      let currentBranch = this.currUser?.branch;
      if(this.isEditDisplay){
        currentBranch = this.dataEdit.branchCode;
      }
      this.listSmartBank = this.listAllSmartBank.filter((item) => item?.BRANCH_CODE_CAP2 === currentBranch);
      // BRANCHES
      const ltBranches = listBranches?.map((item) => {
        return {
          code: item.code,
          displayName: item.code + ' - ' + item.name,
        };
      });
      this.listBranches = ltBranches;
      this.listSource = listSource?.content || [];
      this.listSource = this.listSource.filter((i) => (i.value.includes(Division.INDIV) && i.code !== SourceOp.CAMPAIGN));
      if(!_.isEmpty(this.listSource)){
        this.form.controls.source.setValue(this.listSource[0].code);
      }
      // CURRENCY
      // this.listCurrencies = _.get(listCurrencyType, 'content');

      // PRESENTER
      this.listPresenter = listPresenter?.map((item, index) => {
        const code = item.code ? item.code : '';
        const fullName = item.fullName ? item.fullName : '';
        return {
          code: item.code,
          displayName: code + ' - ' + fullName,
          guideHrsCode: item.hrsCode,
        };
      });
      this.listPresenter = _.orderBy(this.listPresenter, ['code'], ['asc'])

      if (this.isEditDisplay) {
        this.form.patchValue({
          saleOppId: this.dataEdit.saleOppId,
          productCode: this.dataEdit.productCode,
          customerCode: this.dataEdit.customerCode,
          branchCode: this.dataEdit.branchCode,
          memberHrsCode: this.dataEdit.memberHrsCode,
          memberCode: this.dataEdit.memberCode,
          currency: this.dataEdit.currency,
          amount: this.dataEdit.amount,
          status: this.dataEdit.status,
          guideHrsCode: this.dataEdit.guideHrsCode,
          guideCode: this.dataEdit.guideCode,
          note: this.dataEdit.note,
          source: this.dataEdit.source,
          atmId: this.dataEdit?.atmId,
          smbName: this.dataEdit?.smbName,
        }, {emitEvent: false});
        this.getFilterStatus(this.dataEdit.productCode.toString());
        this.getRmManager(this.dataEdit?.branchCode?.split(' '));
      } else {
        this.getRmManager(this.currUser?.branch.split(' '));
      }
      const formValue = this.form.value;
      this.oldForm = { ...formValue };
      this.isLoading = false;
    });

  }

  confirmDialog() {
    cleanDataForm(this.form);
    this.form.controls.customerCode.setValue(this.customerCode);
    if(this.form.value.source === SourceOp.SMARTBANK){
      if(this.form.value.atmId){
        const smb = this.listSmartBank.filter((i) => i.ATM_ID === this.form.value.atmId)[0].SMB_NM || null;
        this.form.controls.smbName.setValue(smb);
      }
      else{
        this.form.controls.smbName.setValue(null);
      }
    }
    if(this.form.value.source !== SourceOp.SMARTBANK){
      this.form.controls.smbName.setValue(null);
      this.form.controls.atmId.setValue(null);
    }
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.componentInstance.message = 'Bạn có muốn lưu cơ hội bán?';
    confirm.result
      .then((res) => {
        if (res) {
          this.isLoading = true;
          if (this.isEditDisplay) {
            this.saleManagerApi.updateOpportunityINDIV(this.form.getRawValue()).subscribe(
              (resCreate) => {
                this.messageService.success(this.notificationMessage.success)
                this.modalActive.close(this.form.controls.saleOppId.value);
              },
              () => {
                // this.isLoading = false;
                this.messageService.error(this.notificationMessage.error);
              }
            );
          } else {
            this.saleManagerApi.createOpportunityINDIV(this.form.getRawValue()).subscribe(
              (resCreate) => {
                this.messageService.success(this.notificationMessage.success)
                this.modalActive.close(true);
              },
              () => {
                // this.isLoading = false;
                this.messageService.error(this.notificationMessage.error);
              }
            );
          }
        }
      })
      .catch(() => {});
  }

  closeModal() {
    if (this.formChange) {
      this.confirmChangeFormType();
    } else {
      this.modalActive.close(false);
    }
  }

  resetModal() {
    this.form.patchValue({
      saleOppId: null,
      productCode: '',
      customerCode: '',
      branchCode: this.currUser?.branch,
      memberHrsCode: null,
      memberCode: null,
      currency: 'VND',
      amount: null,
      status: null,
      guideHrsCode: this.currUser?.hrsCode,
      guideCode: this.currUser?.code || null,
      note: '',
      source: '',
      atmId: null,
      smbName: ''
    },{emitEvent: true});
    this.getRmManager(this.currUser?.branch.split(' '));
  }

  onChangeProduct(event) {
    if (this.isLoading) {
      return;
    }
    this.form.controls.status.setValue(null);
    this.getFilterStatus(event.value);
  }

  onChangeBranch(event) {
    if (this.isLoading) {
      return;
    }
    this.form.controls.memberCode.setValue(null);
    this.form.controls.memberHrsCode.setValue(null);
    this.form.controls.atmId.setValue(null);
    this.listSmartBank = this.listAllSmartBank.filter((item) => item.BRANCH_CODE_CAP2 === event.value);
    if(!_.isEmpty(this.listSmartBank) && this.form.value.source === SourceOp.SMARTBANK){
      this.form.controls.atmId.setValue(this.listSmartBank[0].ATM_ID);
    }
    this.getRmManager([event.value], true);
  }

  getFilterStatus(product: string) {
    this.listStatusOpp = [];
    this.saleManagerApi.getStatusProduct(product).subscribe(
      (listStatusOpp) => {
        // STATUS
        if (listStatusOpp?.length > 0) {
          this.listStatusOpp = listStatusOpp?.map((item) => {
            return { code: item?.status, displayName: item?.title };
          });
        }
      },
      (e) => {
        console.log(e);
        this.messageService.error('Thực hiện không thành công');
        this.isLoading = false;
      }
    );
  }

  getRmManager(listBranch, isChangeBranch?: boolean) {
    this.listRmManager = [];
    const branchCodes = listBranch?.filter((code) => !Utils.isStringEmpty(code));
    this.saleManagerApi.getRMUBByBranches(branchCodes).subscribe((res) => {
      // console.log(res);
      const listRm: any[] =
        res?.map((item, index) => {
          if (item?.code) {
            return {
              rmCode: item?.code,
              displayName: Utils.trimNullToEmpty(item?.code) + ' - ' + Utils.trimNullToEmpty(item?.fullName),
              memberHrsCode: item?.hrsCode,
            };
          }
        }) || [];
      this.listRmManager = _.remove(listRm, (item) => item !== undefined);
      if (!_.isEmpty(this.listRmManager) && this.isEditDisplay && isChangeBranch) {
        this.form.controls.memberHrsCode.setValue(_.first(this.listRmManager).memberHrsCode);
      }
      if (!this.isEditDisplay) {
        this.listRmManager.unshift({rmCode: null, memberHrsCode: null, displayName: 'Chưa gán' });
      }
    });
  }

  checkFormChange(value) {
    if (_.isEqual(this.oldForm, value)) {
      this.formChange = false;
    } else {
      this.formChange = true;
    }
  }

  confirmChangeFormType() {
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.componentInstance.message =
      'Cơ hội đang khai báo chưa được hoàn tất. Bạn chắc chắn muốn chuyển sang cơ hội khác?';
    confirm.result
      .then((res) => {
        if (res) {
          this.modalActive.close(false);
        }
      })
      .catch(() => {});
  }
  isValidForm(){
    console.log('form: ', this.form);
    if(this.form.value.source === SourceOp.SMARTBANK){
      return (this.form.valid && this.form.value.atmId != null)
    }
    else{
      return this.form.valid;
    }
  }
}
