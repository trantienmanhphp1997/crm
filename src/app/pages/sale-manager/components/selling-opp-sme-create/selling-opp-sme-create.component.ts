import { cloneDeep } from 'lodash';
import { Component, Injector, OnInit } from '@angular/core';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CommonCategory, FunctionCode, Scopes, maxInt32 } from 'src/app/core/utils/common-constants';
import { Utils } from 'src/app/core/utils/utils';
import { RmApi } from 'src/app/pages/rm/apis';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { SaleManagerApi } from '../../api';
import { LayoutService } from 'src/app/core/services/layout.service';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-selling-opp-sme-create',
  templateUrl: './selling-opp-sme-create.component.html',
  styleUrls: ['./selling-opp-sme-create.component.scss']
})
export class SellingOppSmeCreateComponent extends BaseComponent implements OnInit {


  listStatusOppSME = [];
  form = this.fb.group({
    status: 'NEW',
    source: 'RM',
    branchOpp: '',
    productCode: [null, Validators.required],
    memberHrsCode: [this.currUser?.hrsCode],
    currency: 'VND',
    description: '',
    amount: [null,Validators.required],
    guideHrsCode: [this.currUser?.hrsCode,  Validators.required],
    note: ''
  });
  listSource = [
    { code: 'MISA', name: 'Misa' },
    { code: 'RM', name: 'Tự khai thác' },
    { code: 'KHAC', name: 'Nguồn khác' },
  ];
  listBranchesOpp = [];
  listHandler = [];
  listProductChild = [];
  listCurrent = [
    { name: 'VND', code: 'VND' },
    { name: 'USD', code: 'USD' }
  ]
  listIntroduce = [];
  params: any;
  customerInfoOpportunity: any;
  detailSMEOpportunity: any;
  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private rmApi: RmApi,
    private saleManagerApi: SaleManagerApi,
    private layoutService: LayoutService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.SELLING_OPPORTUNITY_INDIV}`);
    this.subscribeForm();
    this.params = this.route.snapshot.queryParams;
    this.getData();
    console.log(this.currUser);
    layoutService.bannerObs.next(false);
  }

  ngOnInit() {

  }

  create() {
    if(this.params?.mode == 'edit') {
      this.form.controls.guideHrsCode.clearValidators();
      this.form.controls.guideHrsCode.updateValueAndValidity();
    }
    this.form.markAllAsTouched();
    if (this.form.invalid) {
      return;
    }
    const valueForm = this.form.getRawValue();
    const body = {
      "customerCode": this.customerInfoOpportunity?.customerType== 'KHHH' || this.customerInfoOpportunity?.customerType == 'KH360' ? this.customerInfoOpportunity?.customerCode : '',
      "customerType": this.customerInfoOpportunity?.customerType,
      "customerName": this?.customerInfoOpportunity?.name,
      "national": "",
      "blockCode": this.params?.blockCode,
      "branchOpp": valueForm?.branchOpp,
      "memberHrsCode": valueForm?.memberHrsCode,
      "productCode": valueForm?.productCode,
      "amount": valueForm?.amount,
      "currency": valueForm?.currency,
      "businessRegistrationNumber": this?.customerInfoOpportunity?.businessRegistrationNumber,
      "source": valueForm?.source,
      "status": valueForm?.status,
      "createdBy": "",
      "channel": "",
      "guideHrsCode": valueForm?.guideHrsCode,
      "filterStatus": "",
      "interestExpected": "",
      "misaCode": "",
      "description": valueForm?.description?.trim?.(),
      "note": valueForm?.note?.trim?.(),
      "contractCode": "",
      "updateBy": "",
      "leadCode": this.customerInfoOpportunity?.customerType == 'KHTN' ? this.customerInfoOpportunity?.leadCode : '',
      "address": this.customerInfoOpportunity?.address,
      "memberCode": '',
      "memberName": '',
      "guideCode": "",
      "memberRmCode": "",
      "guideRmCode": '',
      "fullName": "",
      "taxCode": "",
      rsId :this.objFunction.rsId,
      scope: Scopes.VIEW
    }
    this.isLoading = true;
    const api = this.params?.mode == 'edit' ? this.saleManagerApi.updateCustomerInfoOpportunity(body, this.params?.code) : this.saleManagerApi.createCustomerInfoOpportunity(body) ;
    api.pipe(catchError((e) => of(undefined))).subscribe(res => {
      this.isLoading = false;
      if(res) {
        this.messageService.success(this.params?.mode == 'edit' ? 'Chỉnh sửa thành công' : 'Thêm mới thành công');
        this.router.navigateByUrl('/sale-manager/selling-opp-indiv', {
          state : {
            formSearch : this.state?.formSearch
          }
        });
        return;
      }
      this.messageService.error(this.notificationMessage.error);
    }, err => {
      this.isLoading = false;
      this.messageService.error(this.notificationMessage.error);
    });
  }

  getData() {
    this.isLoading = true;
    const body = {
      code: this.params?.mode == 'edit' ? this.checkCode(this.params?.searchQuickType) : this.params?.customerCode,
      block: this.params?.blockCode,
      type: this.params?.searchQuickType
    }
    forkJoin([
      this.categoryService.getCommonCategory(CommonCategory.STATUS_SALE_OPPORTUNITY).pipe(catchError((e) => of(undefined))),
      this.saleManagerApi.getBranches(this.objFunction.rsId, Scopes.VIEW).pipe(catchError((e) => of([]))),
      this.saleManagerApi.getProductByLevel().pipe(catchError((e) => of([]))),
      this.saleManagerApi.getPresenter().pipe(catchError((e) => of(undefined))),
      this.saleManagerApi.getCustomerInfoOpportunity(body).pipe(catchError((e) => of(undefined))),
      this.params?.mode == 'edit' ? this.saleManagerApi.detailSMEOpportunity(this.params?.code, this.objFunction.rsId, Scopes.VIEW).pipe(catchError((e) => of(undefined))) : of(undefined)
    ]).subscribe(([STATUS_SALE_OPPORTUNITY, getBranchesOfUser, getProductByLevel, getPresenter, getCustomerInfoOpportunity, detailSMEOpportunity]) => {
      this.listStatusOppSME = STATUS_SALE_OPPORTUNITY?.content;
      this.listBranchesOpp = getBranchesOfUser.map(
        (item) => {
          return {
            code: item.code,
            displayName: item.code + ' - ' + item.name,
          };
        }
      );
      // this.listBranchesOpp.unshift({ code: '', displayName: this.fields.all });
      this.listProductChild = getProductByLevel?.map((item) => {
        return { code: item.idProductTree, name: `${item.idProductTree} - ${item.description}`, statusList: item.statusList };
      });
      // this.listProductChild.unshift({ code: '', name: this.fields.all });
      this.listIntroduce = getPresenter?.map?.(item => {
        item.displayName = `${item?.code ? item.code+' - ' : ''}${item.fullName}`;
        return item;
      }) || [];
      this.customerInfoOpportunity = getCustomerInfoOpportunity;
      if(this.params?.mode != 'edit') {
        this.form.controls.branchOpp.setValue(this.currUser.branch);
      }
      if(this.params?.mode == 'edit') {
        this.detailSMEOpportunity = detailSMEOpportunity;
        this.form.reset();
        this.form.controls.branchOpp.setValue(detailSMEOpportunity?.branchCode);
        this.form.patchValue(detailSMEOpportunity);
      }
      this.isLoading = false;
    });
  }

  checkCode(searchQuickType) {
    let code = ''
    switch (searchQuickType) {
      case 'KHHH':
        code = this.params?.customerCode;
        break;
      case 'KHTN':
        code = this.params?.leadCode;
        break;
      case 'DKKD':
        code = this.params?.businessRegistrationNumber;
        break;
      default:
        break;
    }
    return code;
  }

  getListHandler(branchCode, blockCode) {
    this.isLoading = true;
    this.rmApi.get('getRmByBranchAndBlock', {
      blockCode: blockCode,
      branchCode: branchCode
    })
      .subscribe(
        (res: any) => {
          if (res) {
            this.isLoading = false;
            this.listHandler = res?.map(item => {
              return {
                code: item.hrisCode,
                displayName: `${item?.rmCode ? item.rmCode+' - ' : ''}${item.rmName}`,
                name: item.rmName
              }
            });
            if(this.params?.mode != 'edit') {
              this.listHandler.unshift({
                code: '',
                displayName: 'Chưa gán',
              });
              if (!this.listHandler.find(item => item.code == this.currUser?.hrsCode)) {
                  this.form.controls.memberHrsCode.setValue('');
              } else {
                this.form.controls.memberHrsCode.setValue(this.currUser?.hrsCode);
              }
            };
            if(this.params?.mode == 'edit') {
              if(!this.listHandler.find(item => item.code ==this.detailSMEOpportunity?.memberHrsCode)) {
                this.form.controls.memberHrsCode.setValue(null);
              } else {
                this.form.controls.memberHrsCode.setValue(this.detailSMEOpportunity?.memberHrsCode);
              }
              this.form.controls.memberHrsCode.setValidators(Validators.required);
              this.form.controls.memberHrsCode.updateValueAndValidity();
            }
          }
        },
        (e) => {
          this.isLoading = false;
        }
      );
  }

  subscribeForm() {
    this.form.controls.branchOpp.valueChanges.subscribe(value => {
      if (value) {
        this.getListHandler(value, this.params?.blockCode);
      }
    });
  }

  close(){
    this.router.navigateByUrl('/sale-manager/selling-opp-indiv', {
      state : {
        formSearch : this.state?.formSearch
      }
    });
  }

  get isEdit(): boolean {
    if (this.params?.mode == 'edit' && (this.form.controls.status.value == 'WON' || this.form.controls.status.value == 'LOST')) {
      return true;
    } else {
      false;
    }
  }
}
