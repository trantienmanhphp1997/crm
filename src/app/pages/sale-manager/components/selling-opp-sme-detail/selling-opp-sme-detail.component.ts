import { forkJoin, of } from 'rxjs';
import { CustomerAssignmentApi, CustomerDetailApi } from 'src/app/pages/customer-360/apis';
import {
  CommonCategory,
  FunctionCode,
  Scopes,
  ScreenType,
  TaskType,
} from '../../../../core/utils/common-constants';
import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Utils } from '../../../../core/utils/utils';
import { ViewEncapsulation } from '@angular/core';
import { ActivityActionComponent } from 'src/app/shared/components/activity-action/activity-action.component';
import { Validators } from '@angular/forms';
import { SaleManagerApi } from '../../api';
import { CreateTaskModalComponent } from 'src/app/pages/tasks/components/create-task-modal/create-task-modal.component';
import { ActivityHistoryComponent } from '../../../../shared/components';
import { SellingOppIndivCreateEditModalComponent } from '../selling-opp-indiv-create-edit-modal/selling-opp-indiv-create-edit-modal-component';
import { global } from '@angular/compiler/src/util';
import * as moment from 'moment';
import { CommonCategoryService } from 'src/app/core/services/common-category.service';
import { catchError } from 'rxjs/operators';
import { ActivityLogComponent } from 'src/app/pages/customer-360/components';
import { CalendarAddModalComponent } from 'src/app/pages/calendar-sme/components/calendar-add-modal/calendar-add-modal.component';

@Component({
  selector: 'app-selling-opp-sme-detail',
  templateUrl: './selling-opp-sme-detail.component.html',
  styleUrls: ['./selling-opp-sme-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SellingOppSmeDetailComponent extends BaseComponent implements OnInit {

  @ViewChild(ActivityLogComponent) activityLog: ActivityLogComponent;
  code;
  detailInfo: any;
  customerCode: string;
  saleOppId: any;
  common = {
    filterStatus: [],
    sources: [],
    status: []
  }

  constructor(
    injector: Injector,
    private saleManagerApi: SaleManagerApi,
    private commonCategoryService: CommonCategoryService,
    private customerAssignmentApi: CustomerAssignmentApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.SALE_OPPORTUNITY_APP}`);
    this.code = _.get(this.route.snapshot.queryParams, 'code');
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.getDetailsForDisplay();
  }

  getDetailsForDisplay() {
    forkJoin([
      this.saleManagerApi.detailSMEOpportunity(this.code, this.objFunction?.rsId, Scopes.VIEW).pipe(catchError(() => of(undefined))),
      this.commonCategoryService.getCommonCategory(CommonCategory.FILTER_STATUS_SALE_OPPORTUNITY).pipe(catchError(() => of(undefined))),
      this.commonCategoryService.getCommonCategory(CommonCategory.SOURCE_SALE_OPPORTUNITY).pipe(catchError(() => of(undefined))),
      this.commonCategoryService.getCommonCategory(CommonCategory.STATUS_SALE_OPPORTUNITY).pipe(catchError(() => of(undefined)))
    ]).subscribe(
      ([oppDetail, listFilterStatus, sources, status]) => {
        this.common.filterStatus = listFilterStatus?.content || [];
        this.common.sources = sources?.content || [];
        this.common.status = status?.content || [];
        if (oppDetail) {
          this.detailInfo = oppDetail;
        } else {
          this.messageService.error(this.notificationMessage.error);
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }

  mailTo() {
    let mail = this.detailInfo?.customerInfo?.email;
    if (!mail) {
      this.messageService.error('Không có thông tin Email');
    } else {
      const data = {
        type: 'EMAIL',
        customerCode: this.code,
        scope: Scopes.VIEW,
        rsId: this.objFunction.rsId
      }
      this.customerAssignmentApi.updateActivityLog(data).subscribe(value => {
        if (value) {
          this.activityLog.getListActivityLog();
        } else {
        }
      });
      const url = `mailto:${mail}`;
      window.location.href = url;
    }
  }

  createActivity() {
    const parent: any = {};
    parent.parentType = TaskType.CUSTOMER;
    parent.parentId = this.customerCode;
    // parent.parentName = this.customerName;
    const modal = this.modalService.open(ActivityActionComponent, {
      windowClass: 'create-activity-modal',
      scrollable: true,
    });
    modal.componentInstance.parent = parent;
    modal.componentInstance.type = ScreenType.Create;
    modal.componentInstance.isShowFuture = true;
  }

  createTaskTodo() {
    const modal = this.modalService.open(CalendarAddModalComponent, { windowClass: 'create-calendar-modal' });
    if (this.detailInfo?.customerCode) {
      modal.componentInstance.customerCode = this.detailInfo?.customerCode;
      modal.componentInstance.customerName = this.detailInfo?.customerName;
    } else {
      modal.componentInstance.leadCode = this.detailInfo?.customerInfo?.leadCode;
      modal.componentInstance.leadName = this.detailInfo?.customerInfo?.name;
    }
    modal.componentInstance.isDetailLead = true;
    modal.result
      .then((res) => {
        if (res) {
          this.activityLog.getListActivityLog();
        } else {
        }
      })
      .catch(() => { });
  }

  historyAction() {
    const parent: any = {};
    parent.parentType = TaskType.CUSTOMER;
    parent.parentId = this.detailInfo.customerCode;
    // parent.parentName = row.customerName;
    const modal = this.modalService.open(ActivityHistoryComponent, {
      windowClass: 'create-activity-modal',
      scrollable: true,
    });
    modal.componentInstance.parent = parent;
    modal.componentInstance.type = ScreenType.Create;
    modal.componentInstance.isShowFuture = true;
  }

  back() {
    this.isLoading = true;
    if (global?.previousUrl) {
      this.router.navigateByUrl(global.previousUrl, { state: this.prop ? this.prop : this.state });
    } else {
      this.router.navigateByUrl('/sale-manager/selling-opp-indiv', { state: this.prop ? this.prop : this.state });
    }
  }

  displayCustomerType(val) {
    if (!val) {
      return '--';
    } else if (val === 'KH360') {
      return 'Khách hàng hiện hữu';
    } else {
      return 'Khách hàng tiềm năng';
    }
  }

  formatDate(val) {
    if (!val) {
      return '--';
    }
    return moment(val).format('HH:mm - DD/MM/YYYY');
  }

  getName(val) {
    if (!val) {
      return '--';
    }
    let found = this.common.status.find(item => item.code === val) || {};
    return found?.name || '--';
  }

  concatStr(str1, str2, defaultVal) {
    let result = '';
    if (str1) {
      result += str1;
    }
    if (str1 && str2) {
      result += ' - '
    }
    if (str2) {
      result += str2;
    }
    return result || defaultVal;
  }


}
