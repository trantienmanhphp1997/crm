import { BaseComponent } from 'src/app/core/components/base.component';
import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import * as moment from 'moment';
import { global } from '@angular/compiler/src/util';
import {
	CommonCategory,
	SessionKey,
	Scopes,
	maxInt32,
	FunctionCode,
} from 'src/app/core/utils/common-constants';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { AssignCollateralAssetCodeModalComponent } from '../dialog/assign-collateral-asset-code-modal/assign-collateral-asset-code-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppFunction } from 'src/app/core/interfaces/app-function.interface';
import { CashFlowApi } from '../../../api/cash-flow.api';
import { Utils } from 'src/app/core/utils/utils';
import * as _ from 'lodash';
import { forkJoin, of } from 'rxjs';
import { DatePipe } from '@angular/common';
import { CommonCategoryService } from 'src/app/core/services/common-category.service';
import { CustomerApi } from 'src/app/pages/customer-360/apis/customer.api';

@Component({
	selector: 'app-cash-flow-customer-list',
	templateUrl: './cash-flow-customer-list.component.html',
	styleUrls: ['./cash-flow-customer-list.component.scss'],
	providers: [NgbModal, DatePipe]
})
export class CashFlowCustomerListComponent extends BaseComponent implements OnInit {
	@ViewChild('table') table: DatatableComponent;
	isLoading: boolean = false;
	
	listStatus: any = [{
		"code": 0,
		"name": "Tất cả"
	}, {
		"code": 1,
		"name": "Đã gán"
	}];
	listAccount: any = [];
	listFTCrm: any = [];
	crrDate = new Date();
	minDate = null;
	params: any = {
		pageNumber: 0,
		pageSize: global?.userConfig?.pageSize,
		rsId: '',
		scope: Scopes.VIEW,
	};
	pageable: Pageable;
	formSearch = this.fb.group({
		customerCode: [''],
		taxCode: [''],
		accountNumber: [''],
		ftNumber: [''],
		status: [0],
		fromDate: new Date(moment().year(), moment().month(), moment().date() - 7),
		toDate: new Date()
	});
	listData: any;
	listBatch: any;
	listFullData: any;
	currUser: any;
	obj: AppFunction;

	constructor(
		injector: Injector,
		private cashFlowApi: CashFlowApi,
		private customerOrgApi: CustomerApi,
		private datePipe: DatePipe,
		private modal: NgbModal,
	) {
		super(injector);
		this.isLoading = true;
		this.obj = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
		this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
	}

	ngOnInit() {
		this.minDate =  this.return90DayAgo(new Date());
		this.commonService.getCommonCategory(CommonCategory.LIST_BATCH).subscribe((response) => {
			this.listBatch = response.content;
			this.isLoading = false;
		},
			() => {
				this.isLoading = false;
				this.messageService.error(this.notificationMessage.error);
			})
	}

	search(isSearch: boolean) {
		if(Utils.isEmpty(this.formSearch.get('accountNumber').value)){
      this.messageService.warn("Vui lòng nhập Mã khách hàng/mã số thuế và chọn STK!");
			return;
		}

		let status = this.formSearch.get('status').value;
		if (status == 0) {
			this.onChangeAccount(this.formSearch.get('accountNumber').value)
		} else {
			this.getFTCrm(0);
		}
	}
	searchAccountByCustomerCode(event) {
		if (!Utils.isEmpty(event)) {
			this.isLoading = true;
			let body = {
				"customerId": event?.trim(),
				"taxCode": null,
				"rsId": this.obj?.rsId,
				"scope": "VIEW"
			}
			this.getListAccount(body);
		} else {
			this.formSearch.controls.accountNumber.setValue("");
			this.listAccount = [];
			this.listData = [];
		}
	}
	searchAccountByTaxCode(event) { 
		if (!Utils.isEmpty(event)) {
			this.isLoading = true;
			let body = {
				"customerId": null,
				"taxCode": event?.trim(),
				"rsId": this.obj?.rsId,
				"scope": "VIEW"
			}
			this.getListAccount(body);
		} else {
			this.formSearch.controls.accountNumber.setValue("");
			this.listAccount = [];
			this.listData = [];
		}
	}
	onChangeAccount(event) {
		this.isLoading = true;
		let listAccStr = this.listAccount.map(item => item.accountNumber).join(',');
		let body = {
			"accountNo": event?.trim(),// "7447732143052"
			"ft": this.formSearch.get('ftNumber').value?.trim(),
			"startTime": this.formSearch.get('fromDate').value?.getTime(),
			"endTime": this.formSearch.get('toDate').value?.getTime(),
			"listAcc": listAccStr
		}
		let bodyCrm = {
			"accountNo": event?.trim(),//7447732143052 
			"ft": this.formSearch.get('ftNumber').value?.trim(),
			"fromDate": this.datePipe.transform(this.formSearch.get('fromDate').value, 'dd/MM/yyyy'),
			"toDate": this.datePipe.transform(this.formSearch.get('toDate').value, 'dd/MM/yyyy'),
			"status": "",
			"page": 0,
			"size": maxInt32
		}
		forkJoin([
			this.cashFlowApi.countListFTByAccount(body),
			this.cashFlowApi.getAssignedFT(bodyCrm),
		]).subscribe(([countFTDwh, listFTCrm]) => {
			if (countFTDwh > 1000) {
				this.messageService.error("Số lượng bản ghi lớn hơn 1000.Vui lòng chọn thời gian truy vấn ngắn hơn.");
				this.isLoading = false;
				return;
			}
			this.listFTCrm = listFTCrm.content;
			this.getListFT(body);
		},
			() => {
				this.isLoading = false;
				this.messageService.error(this.notificationMessage.error);
			});
	}
	getFTCrm(page) {
		this.isLoading = true;
		let bodyCrm = {
			"accountNo": this.formSearch.get('accountNumber').value?.trim(),//"7447732143052",
			"ft": this.formSearch.get('ftNumber').value?.trim(),
			"fromDate": this.datePipe.transform(this.formSearch.get('fromDate').value, 'dd/MM/yyyy'),
			"toDate": this.datePipe.transform(this.formSearch.get('toDate').value, 'dd/MM/yyyy'),
			"status": "",
			"page": page,
			"size": this.params?.pageSize
		}
		this.cashFlowApi.getAssignedFT(bodyCrm).subscribe((response) => {
			this.listData = this.convertFTCrm(response.content);
			this.pageable = {
				totalElements: response?.totalElements,
				size: this.params?.pageSize,
				currentPage: 0
			};
			this.ref.detectChanges();
			this.isLoading = false;
		},
			() => {
				this.isLoading = false;
				this.messageService.error(this.notificationMessage.error);
			})
	}

	getListFT(body) {
		this.cashFlowApi.getListFTByAccount(body).subscribe((response) => {
			let ftNumber = this.formSearch.get('ftNumber').value?.trim();
			if (Utils.isEmpty(ftNumber)) {
				this.listFullData = response;
			} else {
				this.listFullData = response.filter(item => item.refNo === ftNumber)
			}

			let listFT = this.listFullData.slice(0, this.params?.pageSize);
			this.listData = this.convertFT(listFT);
			this.pageable = {
				totalElements: this.listFullData.length,
				size: this.params?.pageSize,
				currentPage: 0
			};
			this.isLoading = false;
		},
			() => {
				this.isLoading = false;
				this.messageService.error(this.notificationMessage.error);
			})
	}
	getListAccount(body) {
		this.listAccount = [];
		this.listData = [];
		this.formSearch.controls.accountNumber.setValue("");
		this.pageable = {
			totalElements: 0,
			size: this.params?.pageSize,
			currentPage: 0
		};
		this.cashFlowApi.getAccountByCustomerCode(body).subscribe(
			(response) => {
				if (response && response?.status == 200) {
					this.isLoading = false;
					let data = response?.data;
					if (data.length > 0) {
						if (data[0]?.errorCode == '000') {
							this.listAccount = data[0]?.accountInfos;
						} else {
							this.messageService.error(data[0]?.errorDesc);
						}
					}
				} else {
					this.messageService.error(response?.error);
					this.isLoading = false;
				}
			},
			() => {
				this.isLoading = false;
				this.messageService.error(this.notificationMessage.error);
			})
	}
	assign(row) {
		if(row.moneyReturnDate && !this.isWithinThreemonth(row.moneyReturnDate,new Date())){
			this.messageService.error("Quá thời gian thay đổi dòng tiền với công trình");
			return;
		}
		let customerCode = this.formSearch.get('customerCode').value?.trim();
		let taxCode = this.formSearch.get('taxCode').value?.trim();
		this.isLoading = true;
		this.customerOrgApi.checkCustomerAssignRm(customerCode,taxCode).subscribe((response) => {
			this.isLoading = false;
      if(response == false){
        this.messageService.warn("Người dùng không thuộc chi nhánh mở code của khách hàng");
			}else{
				const modal = this.modal.open(AssignCollateralAssetCodeModalComponent, { windowClass: 'list__assign-collateral-asset-code-modal' });
				modal.componentInstance.listBatch = this.listBatch;
				modal.componentInstance.ftCode = row.ftNumber;
				modal.componentInstance.moneyReturnNote = row.moneyReturnNote;
				modal.componentInstance.moneyReturnValue = row.moneyReturnValue;
				modal.componentInstance.accountNumber = this.formSearch.get('accountNumber').value?.trim();
				modal.componentInstance.customerCode = customerCode;
				modal.componentInstance.taxCode = taxCode;
				modal.result
				.then((res) => {
					if (res) {
						this.search(true);
					}
				})
			}
			
		},() => {
      this.isLoading = false;
			this.messageService.error(this.notificationMessage.error);
    })
		
		// modal.componentInstance.reload();
	}

	changePage(pageInfo) {
		let status = this.formSearch.get('status').value;
		if (status == 0) {
			let firstItem = ((pageInfo?.page - 1) * this.params?.pageSize) + 1;
			let lastItem = (pageInfo?.page * this.params?.pageSize);
			let listFT = this.listFullData.slice(firstItem, lastItem);
			this.listData = this.convertFT(listFT);
		} else {
			this.getFTCrm(pageInfo?.page - 1)
		}

	}
	convertFT(listFT) {
		return _.map(listFT, (item) => {
			let returnDateStr = item.trxDt;
			let arrDate = returnDateStr.split(' ');
			let returnDate = null
			if (arrDate.length == 6) {
				returnDate = new Date(arrDate[2] + '-' + arrDate[1] + '-' + arrDate[5])
			}
			// -----
			let ftAssigned = _.find(this.listFTCrm, (i) => i.ft === item.refNo);
			// -----
			// let period = _.find(this.listBatch, (i) => i.code === ftAssigned?.period);
			return {
				ftNumber: item.refNo,
				moneyReturnDate: returnDate,
				moneyReturnValue: item.creditAmt,
				moneyReturnNote: item.dscp,
				tsdbCode: ftAssigned?.collateralCode || "",
				batch: ftAssigned?.period || "",
				paymentType: ftAssigned?.paymentType,
				status: ftAssigned ? "Đã gán MTS" : "Chưa gán MTS",
			};
		});
	}
	convertFTCrm(listFT) {
		return _.map(listFT, (item) => {
			return {
				ftNumber: item?.ft,
				moneyReturnDate: item?.transactionDate,
				moneyReturnValue: item?.amount,
				moneyReturnNote: item?.content,
				tsdbCode: item?.collateralCode,
				batch: item?.period,
				paymentType: item?.paymentType,
				status: "Đã gán MTS",
			};
		});
	}

	isWithinThreemonth(date1, date2) {
		date1 = new Date(date1);
		date2 = new Date(date2);
		const d1Plus3Month = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate() + 90);
		return date2 <= d1Plus3Month;
	}
	return90DayAgo(date2) {
		const d1 = new Date(date2);
		const d1Div3Month = new Date(d1.getFullYear(), d1.getMonth(), d1.getDate() - 90);
		return d1Div3Month;
	}
	convertPaymentType(value){
		if(value === 1)
			return "Thanh toán"
		else if(value === 0)  
 	 		return "Tạm ứng"
		else 
			return ""
	}
	convertPeriod(value){
		if(value){
			return "Đợt " + value;
		}else{
			return ""
		}
	} 
 } 
