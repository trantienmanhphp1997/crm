import {
  Component,
  OnInit,
  Injector,
  ViewChild,
  ViewEncapsulation,
  HostBinding,
  Input,
  ChangeDetectorRef,
  OnDestroy
} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormArray } from '@angular/forms';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { Utils } from 'src/app/core/utils/utils';
import _ from 'lodash';
import { CashFlowApi } from '../../../../api/cash-flow.api';
import {
	maxInt32,
  ConfirmType,
  SessionKey,
  FunctionCode
} from 'src/app/core/utils/common-constants';
import { forkJoin } from 'rxjs';
import { AppFunction } from 'src/app/core/interfaces/app-function.interface';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm } from 'src/app/core/utils/function';

@Component({
  selector: 'app-assign-collateral-asset-code-modal',
  templateUrl: './assign-collateral-asset-code-modal.component.html',
  styleUrls: ['./assign-collateral-asset-code-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AssignCollateralAssetCodeModalComponent extends BaseComponent implements OnInit, OnDestroy {
  @HostBinding('class.list__assign-collateral-asset-code-content') appRightContent = true;
  isLoading:boolean = false;
  pageable: Pageable;
  listData: any[];
  listCollateralAssigned: any[] = [];
  @Input() listBatch: any[];
  @Input() ftCode: any;
  @Input() moneyReturnValue: any;
  @Input() customerCode: any;
  @Input() taxCode: any;
  @Input() accountNumber: any;
  @Input() moneyReturnNote: any;
  formArrCollateralAsset: FormArray;
  formCollateralAsset = this.fb.group({
    arrayCollateralAsset: this.fb.array([

    ]),
  });
  haveNewRecord:number = 0;
  sumMoneyReturnInput: any = 0;
  obj: AppFunction;
  listCollateralAsset:any[];
  listPaymentType:any[] = [
    {
      "name":"Tạm ứng",
      "value": 0
    },{
      "name":"Thanh toán",
      "value":1
    }
  ]
  constructor(
    injector: Injector,
    private cashFlowApi: CashFlowApi,
    private modalActive: NgbActiveModal,
  ) {
    super(injector);
    this.isLoading = true;
    this.obj = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DEBT_CLAIM}`);
    this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
  }

  ngOnInit(){
    this.loadData();
  }
  loadData() {
    let body = {
        "maTaiSan": "",
        "tenCongTrinh": "",
        "tenKhachHang": "",
        "codeT24": this.customerCode,
        "mstDkkd": this.taxCode,
        "userKhoiTao": "",
        "branchCode": "",
        "pageSize": 50,
        "page": 0,
        "rsId": this.obj?.rsId,
        "scope": "VIEW"
    }
    let bodyCrm = {
			"accountNo": this.accountNumber,
			"ft": this.ftCode,
			"fromDate": null,
			"toDate": null,
			"status": "",
			"page": 0,
			"size": maxInt32
		}
    forkJoin([
			this.cashFlowApi.getListConstruction(body),
			this.cashFlowApi.getAssignedFT(bodyCrm),
		]).subscribe(([listConstruction,listFTCrm]) => {
      this.listCollateralAsset = listConstruction?.content
      this.mapData(listFTCrm?.content);
      this.isLoading = false;
      let totalPage = listConstruction?.totalPages;
      if(totalPage > 1){
        for(let i = 1; i<totalPage; i++){
          let body = {
            "maTaiSan": "",
            "tenCongTrinh": "",
            "tenKhachHang": "",
            "codeT24": this.customerCode,
            "mstDkkd": this.taxCode,
            "userKhoiTao": "",
            "branchCode": "",
            "pageSize": 50,
            "page": i,
            "rsId": this.obj?.rsId,
            "scope": "VIEW"
        }
          this.cashFlowApi.getListConstruction(body).subscribe((request) => {
            let listCollateralAssetNext = request?.content;
            this.listCollateralAsset.push(listCollateralAssetNext);
          })
        }
      }
      this.ref.detectChanges();
    },
		() => {
			this.isLoading = false;
			this.messageService.error(this.notificationMessage.error);
		});
    // this.pageable = {
		// 	totalElements: 10,
		// 	size: 10,
		// 	currentPage: 0
		// };
  }

  mapData(data){
    this.arrCollateralAsset().valueChanges.subscribe(() => {
      this.listData = [...this.arrCollateralAsset().value];
    })
    // if (data.length > 0) {
    let listCollateralCode = data?.map(item => item.collateralCode);
    this.cashFlowApi.getListAssignedByCollateralCode(listCollateralCode).subscribe((listAssigneds) =>{
      this.arrCollateralAsset().clear();
      _.forEach(data, (item) => {
        this.listCollateralAssigned.push(item.collateralCode);
        // let collateralInfo = _.find(this.listCollateralAsset,(x) => x.maTaiSan ==item.collateralCode);
        let listAssignedByCollCode = listAssigneds.filter(x => x.collateralCode === item.collateralCode);
        let totalAssigned = 0;
        if(listAssignedByCollCode?.length > 0){
          totalAssigned = _.reduce(
            _.map(listAssignedByCollCode, (x) => x.amountCollateral),
            (a, c) => {
              return a + c;
            }
          );
        }
        this.sumMoneyReturnInput = totalAssigned;
        
        const form = this.fb.group({
          id: null,
          collateralCode: [null, CustomValidators.required],
          productName: null,
          ft:this.ftCode,
          transactionDate:new Date(),
          amount: this.moneyReturnValue,
          amountContract: item?.amountContract,
          amountContractRemaining: item?.amountContract - totalAssigned,
          contractNumber: item?.contractNumber,
          content: this.moneyReturnNote,
          amountCollateral: ['', [CustomValidators.onlyNumber,CustomValidators.required]],
          period:[null, CustomValidators.required],
          paymentType:[null, CustomValidators.required],
          accountNo: this.accountNumber,
          type: 1,
          isAssign: true,
          error:null,
          count:null,
          isChange:false,
          createdBy:item?.createdBy,
        });
        // contact.birthday = contact?.birthday ? new Date(contact.birthday) : null;
        // contact.birthday = contact?.birthday ? new Date(contact.birthday) : null;
        form.patchValue(item);
        this.arrCollateralAsset().push(form);
      })
    },() => {
      this.messageService.error(this.notificationMessage.error);
      this.isLoading = false;
    })
  }
  onChange(rowIndex){
    let count = this.getForm(rowIndex).get('count').value;
    if(count >= 5){
      this.messageService.warn("Không được công trình gán với dòng tiền quá 5 lần");
      this.getForm(rowIndex).controls.isChange.setValue(false)
    }else{
      this.getForm(rowIndex).controls.isChange.setValue(true)
    }
  }
  allowDelete(row){
    return row?.createdBy == this.currUser?.username
  }
  update() {
    let checkError = true;
    this.formArrCollateralAsset = this.formCollateralAsset.get('arrayCollateralAsset') as FormArray;
    _.forEach(this.formArrCollateralAsset.controls, (item) => {
      item.markAllAsTouched();
      if(item?.value?.error){
        checkError = false;
      };
      
    });
    if(this.formCollateralAsset.valid && checkError){
      this.isLoading = true;
      const listCollateral = cleanDataForm(this.formCollateralAsset)?.arrayCollateralAsset;
      if(listCollateral <= 0){
        this.messageService.error("Danh sách công trình không có dữ liệu");
        this.isLoading = false;
      }
        
      this.cashFlowApi.assignedCollateralToFT(listCollateral).subscribe((reponse) =>{
        if(reponse.code == 200){
          this.messageService.success(reponse.message);
        }else if(reponse.code == 201){
          this.messageService.warn(reponse.message);
        }else{
          this.messageService.error(reponse.message);
        }
        this.modalActive.close(false);
        // this.loadData();
      },() => {
        this.messageService.error(this.notificationMessage.error);
        this.isLoading = false;
      })
    }
  }

  deleteRecord(rowIndex){
    if(this.getForm(rowIndex).get('id').value == null){
      var collateralCode = this.getForm(rowIndex).get('collateralCode').value;
      this.arrCollateralAsset().removeAt(rowIndex);
      this.listCollateralAssigned = _.find(this.listCollateralAssigned, (x) => x != collateralCode)
      return;
    }
    const messageSave = `Bạn có muốn xóa`;
    this.confirmService.openModal(ConfirmType.Confirm, messageSave).then((resConfirm) => {
      if (resConfirm) {
        this.isLoading = true;
        let params = {
          "collateralCode" : this.getForm(rowIndex).get('collateralCode').value,
          "ftCode": this.ftCode
        }
        this.cashFlowApi.deleteByFT(params).subscribe((response)=>{
          this.messageService.success(response?.message);
          this.arrCollateralAsset().removeAt(rowIndex);
          this.ref.detectChanges();
          this.isLoading = false;
        },()=>{
          this.messageService.error(this.notificationMessage.error);
          this.isLoading = false;
        })
      }
    });
  }

  onChangeCollateral(collateralCode,rowIndex){
    
    if(this.listCollateralAssigned != undefined && this.listCollateralAssigned != null &&  this.listCollateralAssigned.includes(collateralCode)){
      this.messageService.warn("Mã công trình " +collateralCode+ " đã tồn tại trong danh sách");
      this.getForm(rowIndex).controls.collateralCode.setValue("");
      this.listCollateralAssigned = []
      let formArrCollateralAssetTemp = this.formCollateralAsset.get('arrayCollateralAsset') as FormArray;
      _.forEach(formArrCollateralAssetTemp.controls, (item) => {
        this.listCollateralAssigned.push(item?.value?.collateralCode);
      });
      return;
    }
    let collateralInfo = _.find(this.listCollateralAsset, (x) => x.maTaiSan == collateralCode);
    let valueContract = 0;
    if(collateralInfo?.giaTriHopDongDieuChinhQuyDoi)
      valueContract = collateralInfo?.giaTriHopDongDieuChinhQuyDoi;
    else
      valueContract = collateralInfo?.giaTriHopDongBanDauQuyDoi;
    this.getForm(rowIndex).controls.productName.setValue(collateralInfo?.tenCongTrinh);
    this.getForm(rowIndex).controls.amountContract.setValue(valueContract);
    this.getForm(rowIndex).controls.contractNumber.setValue(collateralInfo?.soHsplHdmb);
    this.isLoading = true;
    let bodyCrm = {
			"accountNo": null,
			"collateralCode": collateralCode,
			"ft": null,
			"fromDate": null,
			"toDate": null,
			"status": "",
			"page": 0,
			"size": maxInt32
		}
    let totalAssigned = 0;
    let count = 0;
    this.cashFlowApi.getAssignedFT(bodyCrm).subscribe((reponse)=>{
      let data = reponse?.content;
      if (data.length > 0) {
        totalAssigned = _.reduce(
          _.map(data, (x) => x.amountCollateral),
          (a, c) => {
            return a + c;
          }
        );
        count = _.find(data,(x) => x.ft === this.ftCode)?.count;
      }
      let amountContractRemaining = valueContract - totalAssigned;
      this.getForm(rowIndex).controls.amountContractRemaining.setValue(amountContractRemaining);
      this.getForm(rowIndex).controls.amountCollateral.setValue(null);
      this.getForm(rowIndex).controls.count.setValue(count);
      this.getForm(rowIndex).controls.isChange.setValue(true);
      
      this.isLoading = false;
    },() => {
      this.messageService.error(this.notificationMessage.error);
      this.isLoading = false;
    })
    this.listCollateralAssigned = []
    let formArrCollateralAssetTemp = this.formCollateralAsset.get('arrayCollateralAsset') as FormArray;
    _.forEach(formArrCollateralAssetTemp.controls, (item) => {
      this.listCollateralAssigned.push(item?.value?.collateralCode);
    });
  }

  createNewRecord(){
    this.arrCollateralAsset().push(this.newArrayContactForm());
    this.haveNewRecord++;
    this.ref.detectChanges();
  }

  changeValue(rowIndex){
    let amountContractRemaining = this.getForm(rowIndex).controls.amountContractRemaining.value;
    let amountCollateral = this.getForm(rowIndex).get('amountCollateral').value;
    let count = this.getForm(rowIndex).get('count').value;
    if(count >= 5){
      this.messageService.warn("Không được sửa gán công trình với dòng tiền quá 5 lần");
      this.getForm(rowIndex).controls.isChange.setValue(false)
    }else{
      this.getForm(rowIndex).controls.isChange.setValue(true)
    }
    if(amountCollateral != null && amountCollateral != undefined && amountCollateral <= 0){
      this.getForm(rowIndex).controls.error.setValue("Giá trị tiền về phải > 0");
      return;
    }

    if(amountContractRemaining < amountCollateral){
      this.getForm(rowIndex).controls.error.setValue("Giá trị tiền về gắn với TSĐB phải <= của hợp đồng còn lại");
      return;
    }
    if(this.moneyReturnValue < amountCollateral){
      this.getForm(rowIndex).controls.error.setValue("Giá trị tiền về gắn với TSĐB phải <= trị tiền về");
      return;
    }
    let sum = 0;
    let formArrCollateralAssetTemp = this.formCollateralAsset.get('arrayCollateralAsset') as FormArray;
    _.forEach(formArrCollateralAssetTemp.controls, (item) => {
      sum += item?.value?.amountCollateral
    });
    if(this.moneyReturnValue < sum){
      this.getForm(rowIndex).controls.error.setValue("Tổng giá trị tiền về gắn với TSĐB phải <= giá trị tiền về");
      return;
    }else{
      for(let i = 0;i < formArrCollateralAssetTemp.value.length;i ++){
        this.getForm(i).controls.error.setValue(null);
      }
    }
    this.getForm(rowIndex).controls.error.setValue(null);
  }

  newArrayContactForm(): FormGroup {
    return this.fb.group({
      id: null,
      collateralCode: [null, CustomValidators.required],
      productName:null,
      ft: this.ftCode,
      transactionDate:new Date(),
      amount: this.moneyReturnValue,
      amountContract: null,
      amountContractRemaining: null,
      contractNumber: null,
      content: this.moneyReturnNote,
      amountCollateral:  ['', [CustomValidators.required]],
      period:[null, CustomValidators.required],
      paymentType:[null, CustomValidators.required],
      accountNo:this.accountNumber,
      type: 1,
      count: 0,
      isAssign: true,
      isChange: true,
      error:null,
      createdBy:this.currUser?.username
    });
  }

  getForm(i: number): FormGroup {
    return this.arrCollateralAsset().controls[i] as FormGroup;
  }

  getValue(item: any, key: string) {
    return Utils.isStringEmpty(_.get(item, key)) ? '' : _.get(item, key);
  }

  closeModal() {
    this.modalActive.close(false);
  }

  arrCollateralAsset(): FormArray {
    return this.formCollateralAsset.get('arrayCollateralAsset') as FormArray;
  }
}