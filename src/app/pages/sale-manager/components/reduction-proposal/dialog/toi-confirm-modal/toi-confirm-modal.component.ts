import { Component, HostBinding, Injector, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Division } from 'src/app/core/utils/common-constants';
import { Utils } from 'src/app/core/utils/utils';
import { ReductionProposalApi } from 'src/app/pages/sale-manager/api/reduction-proposal.api';
import { BaseComponent } from '../../../../../../core/components/base.component';

@Component({
  selector: 'app-toi-confirm-modal',
  templateUrl: './toi-confirm-modal.component.html',
  styleUrls: ['./toi-confirm-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ToiConfirmModalComponent extends BaseComponent implements OnInit {
  @HostBinding('toi-confirm-modal') classCreateCalendar = true;
  @Input() toiInfo;
  constructor(
    injector: Injector,
    public fb: FormBuilder,
    private reductionProposalApi: ReductionProposalApi,
    private dialogRef: MatDialogRef<ToiConfirmModalComponent>
  ) {
    super(injector);
    dialogRef.disableClose = true;
  }

  toiForm = this.fb.group({
    duNoNganHanBq: null,
    duNoNganHanBqUnit: null,
    duNoTdhBq: null,
    duNoTdhBqUnit: null,
    coKyHanBq: null,
    coKyHanBqUnit: null,
    khongKyHanBq: null,
    khongKyHanBqUnit: null,
    thuBl: null,
    thuBlUnit: null,
    thuTTQT: null,
    thuTTQTUnit: null,
    thuFx: null,
    thuFxUnit: null,
    thuBanca: null,
    thuKhac: null,
  });

  calculated = {
    duNoNganHanBq: null,
    duNoTdhBq: null,
    coKyHanBq: null,
    khongKyHanBq: null,
    thuBl: null,
    thuTTQT: null,
    thuFx: null,
    thuBanca: null,
    thuKhac: null,
    dtttrr: null,
    toi: null,
    // -- Hiệu quả
    duNoNganHanBqHq: null,
    duNoTdhBqHq: null,
    coKyHanBqHq: null,
    khongKyHanBqHq: null,
    thuBlHq: null,
    thuTTQTHq: null,
    thuFxHq: null
  }

  info: any;
  action = 'default';
  type;
  blockCode;


  ngOnInit(): void {
    this.info = this.reductionProposalApi?.info?.value;
    const toi = this.info?.toi || this.toiInfo;
    console.log(toi);

    if (toi) {
      this.toiForm.controls.duNoNganHanBq.setValue(toi?.duNoNganHanBq);
      this.toiForm.controls.duNoNganHanBqUnit.setValue(toi?.duNoNganHanBqUnit);
      this.calculated.duNoNganHanBqHq = this.multiplication(toi?.duNoNganHanBq, toi?.duNoNganHanBqUnit, true);
      this.toiForm.controls.duNoTdhBq.setValue(toi?.duNoTdhBq);
      this.toiForm.controls.duNoTdhBqUnit.setValue(toi?.duNoTdhBqUnit);
      this.calculated.duNoTdhBqHq = this.multiplication(toi?.duNoTdhBq, toi?.duNoTdhBqUnit, true);
      this.toiForm.controls.coKyHanBq.setValue(toi?.coKyHanBq);
      this.toiForm.controls.coKyHanBqUnit.setValue(toi?.coKyHanBqUnit);
      this.calculated.coKyHanBqHq = this.multiplication(toi?.coKyHanBq, toi?.coKyHanBqUnit, true);
      this.toiForm.controls.khongKyHanBq.setValue(toi?.khongKyHanBq);
      this.toiForm.controls.khongKyHanBqUnit.setValue(toi?.khongKyHanBqUnit);
      this.calculated.khongKyHanBqHq = this.multiplication(toi?.khongKyHanBq, toi?.khongKyHanBqUnit, true);
      this.toiForm.controls.thuBl.setValue(toi?.thuBl);
      this.toiForm.controls.thuBlUnit.setValue(toi?.thuBlUnit);
      this.calculated.thuBlHq = this.multiplication(toi?.thuBl, toi?.thuBlUnit, true);
      this.toiForm.controls.thuTTQT.setValue(toi?.thuTTQT);
      this.toiForm.controls.thuTTQTUnit.setValue(toi?.thuTTQTUnit);
      this.calculated.thuTTQTHq = this.multiplication(toi?.thuTTQT, toi?.thuTTQTUnit, true);
      this.toiForm.controls.thuFx.setValue(toi?.thuFx);
      this.toiForm.controls.thuFxUnit.setValue(toi?.thuFxUnit);
      this.calculated.thuFxHq = this.multiplication(toi?.thuFx, toi?.thuFxUnit, false);
      this.toiForm.controls.thuBanca.setValue(toi?.thuBanca);
      this.toiForm.controls.thuKhac.setValue(toi?.thuKhac);
      this.calculated.thuBanca = toi?.thuBanca;
      this.calculated.thuKhac = toi?.thuKhac;
      this.calculated.dtttrr = toi?.dtttrr;
      this.calculated.toi = toi?.toi;
      this.calculatedToi();
    }
  }

  ngAfterViewInit() {

    this.toiForm.controls.duNoNganHanBq.valueChanges.subscribe((value) => {
      const {
        duNoNganHanBqUnit
      } = this.toiForm.getRawValue();
      this.calculated.duNoNganHanBqHq = this.multiplication(value, duNoNganHanBqUnit, true);
      this.calculatedToi();
    });
    this.toiForm.controls.duNoNganHanBqUnit.valueChanges.subscribe((value) => {
      const {
        duNoNganHanBq } = this.toiForm.getRawValue();
      this.calculated.duNoNganHanBqHq = this.multiplication(duNoNganHanBq, value, true);
      this.calculatedToi();
    });
    this.toiForm.controls.duNoTdhBq.valueChanges.subscribe((value) => {
      const {
        duNoTdhBqUnit } = this.toiForm.getRawValue();
      this.calculated.duNoTdhBqHq = this.multiplication(value, duNoTdhBqUnit, true);
      this.calculatedToi();
    });
    this.toiForm.controls.duNoTdhBqUnit.valueChanges.subscribe((value) => {
      const {
        duNoTdhBq } = this.toiForm.getRawValue();
      this.calculated.duNoTdhBqHq = this.multiplication(duNoTdhBq, value, true);
      this.calculatedToi();
    });
    this.toiForm.controls.coKyHanBq.valueChanges.subscribe((value) => {
      const {
        coKyHanBqUnit } = this.toiForm.getRawValue();
      this.calculated.coKyHanBqHq = this.multiplication(value, coKyHanBqUnit, true);
      this.calculatedToi();
    });
    this.toiForm.controls.coKyHanBqUnit.valueChanges.subscribe((value) => {
      const {
        coKyHanBq } = this.toiForm.getRawValue();
      this.calculated.coKyHanBqHq = this.multiplication(coKyHanBq, value, true);
      this.calculatedToi();
    });
    this.toiForm.controls.khongKyHanBq.valueChanges.subscribe((value) => {
      const {
        khongKyHanBqUnit } = this.toiForm.getRawValue();
      this.calculated.khongKyHanBqHq = this.multiplication(value, khongKyHanBqUnit, true);
      this.calculatedToi();
    });
    this.toiForm.controls.khongKyHanBqUnit.valueChanges.subscribe((value) => {
      const {
        khongKyHanBq
      } = this.toiForm.getRawValue();
      this.calculated.khongKyHanBqHq = this.multiplication(khongKyHanBq, value, true);
      this.calculatedToi();
    });

    this.toiForm.controls.thuBl.valueChanges.subscribe((value) => {
      this.calculated.thuBlHq = this.multiplication(value, this.toiForm.controls.thuBlUnit.value, true);
      this.calculatedToi();
    });
    this.toiForm.controls.thuBlUnit.valueChanges.subscribe((value) => {
      this.calculated.thuBlHq = this.multiplication(value, this.toiForm.controls.thuBl.value, true);
      this.calculatedToi();
    });

    this.toiForm.controls.thuTTQT.valueChanges.subscribe((value) => {
      this.calculated.thuTTQTHq = this.multiplication(value, this.toiForm.controls.thuTTQTUnit.value, true);
      this.calculatedToi();
    });
    this.toiForm.controls.thuTTQTUnit.valueChanges.subscribe((value) => {
      this.calculated.thuTTQTHq = this.multiplication(value, this.toiForm.controls.thuTTQT.value, true);
      this.calculatedToi();
    });
    this.toiForm.controls.thuFx.valueChanges.subscribe((value) => {
      this.calculated.thuFxHq = this.multiplication(value, this.toiForm.controls.thuFxUnit.value, false);
      this.calculatedToi();
    });
    this.toiForm.controls.thuFxUnit.valueChanges.subscribe((value) => {
      this.calculated.thuFxHq = this.multiplication(value, this.toiForm.controls.thuFx.value, false);
      this.calculatedToi();
    });
    this.toiForm.controls.thuBanca.valueChanges.subscribe((value) => {
      this.calculated.thuBanca = value ? value.toString() : null;
      this.calculatedToi();
    });
    this.toiForm.controls.thuKhac.valueChanges.subscribe((value) => {
      this.calculated.thuKhac = value ? value.toString() : null;
      this.calculatedToi();
    });
  }

  multiplication(val1, val2, division100) {
    if (!Utils.isNull(val1) && !Utils.isNull(val2)) {
      if (division100) {
        val2 = Number(val2) / 100;
      }
      return Number((Number(val1) * val2).toFixed(2)).toLocaleString('fullwide', { useGrouping: false });
    }
  }

  calculatedToi() {

    let { duNoNganHanBq, duNoTdhBq } = this.toiForm.getRawValue();
    duNoNganHanBq = Utils.isNull(duNoNganHanBq) ? 0 : Number(duNoNganHanBq);
    duNoTdhBq = Utils.isNull(duNoTdhBq) ? 0 : Number(duNoTdhBq);
    this.calculated.dtttrr =
      Number(!this.calculated.duNoNganHanBqHq ? 0 : this.calculated.duNoNganHanBqHq.toString().replace(",", ".")) +
      Number(!this.calculated.duNoTdhBqHq ? 0 : this.calculated.duNoTdhBqHq.toString().replace(",", ".")) +
      Number(!this.calculated.coKyHanBqHq ? 0 : this.calculated.coKyHanBqHq.toString().replace(",", ".")) +
      Number(!this.calculated.khongKyHanBqHq ? 0 : this.calculated.khongKyHanBqHq.toString().replace(",", ".")) +
      Number(!this.calculated.thuBlHq ? 0 : this.calculated.thuBlHq.toString().replace(",", ".")) +
      Number(!this.calculated.thuTTQTHq ? 0 : this.calculated.thuTTQTHq.toString().replace(",", ".")) +
      Number(!this.calculated.thuFxHq ? 0 : this.calculated.thuFxHq.toString().replace(",", ".")) +
      Number(!this.calculated.thuBanca ? 0 : this.calculated.thuBanca.toString().replace(",", ".")) +
      Number(!this.calculated.thuKhac ? 0 : this.calculated.thuKhac.toString().replace(",", "."));
    if (duNoNganHanBq + duNoTdhBq > 0 && this.calculated.dtttrr > 0) {
      this.calculated.toi = (this.calculated.dtttrr / (duNoNganHanBq + duNoTdhBq) * 100).toFixed(2);
      this.calculated.dtttrr = Number((this.calculated.dtttrr).toFixed(2)).toLocaleString('fullwide', { useGrouping: false });
    } else {
      this.calculated.toi = null;
    }
  }

  choose(choose: boolean): void {
    if(this.calculated.toi > 100 && choose){
      this.messageService.warn('TOI cam kết phải nhỏ hơn hoặc bằng 100%');
      return;
    }
    if (choose) {
      const { duNoNganHanBq,
        duNoNganHanBqUnit,
        duNoTdhBq,
        duNoTdhBqUnit,
        coKyHanBq,
        coKyHanBqUnit,
        khongKyHanBq,
        khongKyHanBqUnit,
        thuBl,
        thuBlUnit,
        thuTTQT,
        thuTTQTUnit,
        thuFx,
        thuFxUnit,
        thuBanca,
        thuKhac
      } = this.toiForm.getRawValue();
      this.calculated.coKyHanBqHq;
      let toiChanged = this.info?.toi && this.calculated.toi !== this.info?.toi?.toi;
      if (toiChanged && (this.type === 1 || (this.type === 0 && this.blockCode !== Division.FI))) {
        console.log('TOI changed');
        this.info.authorization = [];
      }

      this.info.toi = {
        duNoNganHanBq,
        duNoNganHanBqUnit,
        duNoTdhBq,
        duNoTdhBqUnit,
        coKyHanBq,
        coKyHanBqUnit,
        khongKyHanBq,
        khongKyHanBqUnit,
        thuBl,
        thuBlUnit,
        thuTTQT,
        thuTTQTUnit,
        thuFx,
        thuFxUnit,
        thuBanca,
        thuKhac,
        'dtttrr': this.calculated.dtttrr,
        'toi': this.calculated.toi,
        ckhBqHq: this.calculated.coKyHanBqHq,
        dunoBqHq: this.calculated.duNoNganHanBqHq,
        dunoTdhBqHq: this.calculated.duNoTdhBqHq,
        kkhBqHq: this.calculated.khongKyHanBqHq,
        tblHq: this.calculated.thuBlHq,
        tfxHq: this.calculated.thuFxHq,
        thuBanCaHq: this.calculated.thuBanca,
        thuKhacHq: this.calculated.thuKhac,
        tttqtHq: this.calculated.thuTTQTHq,
        toiChanged
      };
      this.reductionProposalApi.info.next(this.info);
      this.dialogRef.close();
    }
    else {
      this.dialogRef.close('closeModal');
    }
  }

  closeModal() {
    this.info.toi.toiChanged = false;
    this.reductionProposalApi.info.next(this.info);
    this.dialogRef.close('closeModal');
  }

  formatCurrency(element) {
    if (!element || element === 'NaN') {
      return '';
    }
    let formated: string = element.toString();
    let first = '';
    let last = '';
    if (formated.includes('.')) {
      first = formated.substr(0, formated.indexOf('.'));
      last = formated.substr(formated.indexOf('.'));
    } else {
      first = formated;
    }
    if (Number(first)) {
      first = Number(first).toLocaleString('en-US', {
        style: 'currency',
        currency: 'VND',
      });
      first = first.substr(1, first.length);
      formated = first + last;
    }
    return formated;
  }


}
