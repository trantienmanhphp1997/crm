import { forkJoin, Observable } from 'rxjs';
import { Component, OnInit, Injector, ViewChild, ViewEncapsulation, HostBinding, Input, ChangeDetectorRef } from '@angular/core';
import { Validators } from '@angular/forms';
import { DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { global } from '@angular/compiler/src/util';
import { Division, FunctionCode, Scopes, SessionKey } from 'src/app/core/utils/common-constants';
// import { RmService } from '../../services';
import { Utils } from 'src/app/core/utils/utils';
import { cleanDataForm } from 'src/app/core/utils/function';
// import { BlockApi, RmApi } from '../../apis';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { RmService } from 'src/app/pages/rm/services';
import { BlockApi, RmApi } from 'src/app/pages/rm/apis';
import { ReductionProposalApi } from 'src/app/pages/sale-manager/api/reduction-proposal.api';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-select-fee-modal',
  templateUrl: './select-fee-modal.component.html',
  styleUrls: ['./select-fee-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class SelectFeeModalComponent extends BaseComponent implements OnInit {
  limit = global.userConfig.pageSize;
  isLoading = false;
  @ViewChild('tablefee') public tablerm: DatatableComponent;
  @HostBinding('class.list__interest-content') appRightContent = true;
  isDisableBranch = false;
  isDisableBlock = false;
  showSearchBasic = true;
  obj: any;
  branch_codes: Array<any> = [];
  search_form = this.fb.group({
    employeeCode: ['', Validators.maxLength(20)],
    employeeId: ['', Validators.maxLength(20)],
    fullName: ['', Validators.maxLength(120)],
    email: ['', Validators.maxLength(50)],
    mobile: ['', Validators.maxLength(30)],
    crmIsActive: ['true'],
    rmBlock: [''],
    productGroup: [''],
    divisionCode: [''],
  });
  search_value: string = '';
  searchFeeType: string;
  show: boolean;
  facilities: Array<any>;
  grades: Array<any>;
  facilityCode: Array<string>;
  prevSearch: string;
  // model = [];
  models = [];
  count: number; // total
  offset: number; // page
  params: any = {
    adjustmentPeriod: "",
    department: "",
    currency: "",
    productCode: "",
    period: "",
    rankingCredit: "",
    customerType: "",
    category: "",
    customerSegment: ""
  };
  searchType: boolean = false;
  body = {};
  checkAll = false;
  listSelected = [];
  listCode = [];
  listCodeOld = [];
  countCheckedOnPage = 0;
  @Input() config: any;
  showCheckBox = true;
  listInterest = [];
  selected = [];
  SelectionType = SelectionType;
  rawData: any[];
  response: any[];
  isFirstLoad = true;
  isManage;
  scoreValue = '';
  department = '';
  lastSelectedType = '';
  prevChargeGroup = '';
  feeTypes = [];
  isKhdn;
  types = [
    { code: '', name: 'Tất cả' },
    { code: 'CREDIT', name: 'Tín dụng' },
    { code: 'NON_CREDIT', name: 'Phi tín dụng' },
    { code: 'REMITTANCES', name: this.department === Division.SME ? 'Tài trợ thương mại' : 'Kiều hối' }
  ];
  code = '';
  customerCreditType;
  search_value_code = '';
  dataOrigin = [];
  maxLength = 50;
  constructor(
    injector: Injector,
    private reductionProposalApi: ReductionProposalApi,
    private modalActive: NgbActiveModal,
    private dtc: ChangeDetectorRef,
    // public dialogRef: MatDialogRef<any>
  ) {
    super(injector);
    this.obj = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REDUCTION_PROPOSAL}`);
    this.params.rsId = this.obj?.rsId;
  }

  ngOnInit() {
    console.log(window);
    console.log(window.innerWidth);

    this.params.rsId = _.get(this.obj, 'rsId');
    if (this.grades?.length > 0) {
      this.grades = [
        {
          id: '',
          code: '',
          value_to_search: _.get(this.fields, 'all'),
        },
        ...this.grades,
      ];
    }

    this.listCodeOld = [...this.listCode];
  }

  changeBranch(event) { }

  paging(event) {
  }

  ngAfterViewChecked() {
    //your code to update the model
    // this.dtc.detectChanges();
  }


  searchFE() {
    this.models = _.cloneDeep(this.response);
    let dataFind = [];
    if (this.search_value_code) {
      this.models = this.dataOrigin.filter(item => item.chargeCode == this.search_value_code);
      this.dataOrigin.forEach(item => {
        if (item.sumChargeCode == this.search_value_code) {
          this.models.push(item);
        }
      });
      this.models.forEach(item => {
        if (item.sumChargeCode) {
          this.dataOrigin.forEach(itemOrigin => {
            if (itemOrigin.sumChargeCode == item.sumChargeCode || itemOrigin.chargeCode == item.sumChargeCode) {
              if (itemOrigin.chargeCode != item.chargeCode) {
                this.models.push(itemOrigin);
              }
            }
          })
        }
      })
    }
    const setData = new Set(this.models);
    this.models = [...setData];

    dataFind = _.cloneDeep(this.models);
    if (this.search_value) {
      // tim con
      const dataParent = dataFind.filter(item => item.descriptionVn.toUpperCase().includes(this.search_value.toUpperCase()) && !item.sumChargeCode);
      if (dataParent.length) {
        this.models = dataParent;
        dataParent.forEach(item => {
          const data = dataFind.filter(itemOrigin => itemOrigin.sumChargeCode == item.chargeCode);
          this.models = [...this.models, ...data];
        });
      }
      // tim cha
      const dataChild = dataFind.filter(item => item.descriptionVn.toUpperCase().includes(this.search_value.toUpperCase()) && item.sumChargeCode);
      if (dataChild.length) {
        this.models = [];
        dataChild.forEach(item => {
          const data = dataFind.filter(itemOrigin => (itemOrigin.chargeCode == item.sumChargeCode || itemOrigin.sumChargeCode == item.sumChargeCode) && itemOrigin.currency == item.currency);
          this.models = [...this.models, ...data];
        });
      }
      if (!dataParent.length && !dataChild.length) {
        this.models = [];
      }
    }
    // if (this.search_value_code && this.search_value) {
    //   this.models = this.dataOrigin.filter(item => item.descriptionVn == this.search_value && item.chargeCode == this.search_value_code);
    //   return;
    // }
    const Data = new Set(this.models);
    this.models = [...Data];
    console.log(this.models);

  }

  reload() {
    if (this.isFirstLoad) {
      this.feeTypes = this.types.filter(item => {
        if (this.department === Division.FI) {
          return item.code !== 'REMITTANCES';
        }
        if (this.department === Division.SME && !Utils.isEmpty(this.lastSelectedType)) {
          if (this.lastSelectedType == 'REMITTANCES') {
            return item.code == this.lastSelectedType;
          }
          if (this.lastSelectedType == 'CREDIT' || this.lastSelectedType == 'NON_CREDIT') {
            return item.code == 'CREDIT' && this.isManage || item.code == 'NON_CREDIT';
          }

        }
        if (this.department === Division.INDIV && !Utils.isEmpty(this.lastSelectedType)) {
          return item.code == this.lastSelectedType;
        }
        if (Utils.isEmpty(item.code) && !this.isKhdn) {
          return false;
        }
        if (
          (item.code === 'CREDIT' && !this.isManage && this.isKhdn)
          || (item.code === '' && !this.isManage && this.isKhdn)
          || (item.code === '' && this.department === Division.SME)
        ) {
          return false;
        }
        if (item.code === 'REMITTANCES' && this.isKhdn && this.department !== Division.SME) {
          return false;
        }
        return true;
      }).map(item => {
        if (this.department === Division.SME && item.code === 'REMITTANCES') {
          item.name = 'Tài trợ thương mại';
        }
        return item;
      });

      let c;
      if (this.isKhdn && this.department !== Division.SME) {
        c = 0;
      } else if (!Utils.isEmpty(this.lastSelectedType) && [Division.SME, Division.INDIV]) {
        c = 1;
      }
      switch (c) {
        case 0:
          this.searchFeeType = _.head(this.feeTypes)?.code;
          break;
        case 1:
          this.searchFeeType = this.lastSelectedType;
          break;
        default:
          this.searchFeeType = 'REMITTANCES';
      }
      this.prevChargeGroup = this.searchFeeType;
    }
    this.feeTypeChange();
    this.isLoading = true;
    this.reductionProposalApi.getFees({
      department: this.department,
      descriptionVn: '',
      chargeGroup: this.searchFeeType,
      chargeCodes: []
    }).subscribe((res: any) => {
      this.isFirstLoad = false;
      // API bị lỗi sẽ trả về định dạng object
      if ((res?.data || []).length === 0) {
        this.response = [];
        this.models = [];
        this.count = 0;
      } else {
        this.rawData = res?.data;

        this.rawData = this.rawData
          .filter(item => {
            let chgAmtFrom = item?.chgAmtFrom || 0;
            let minCharge = item?.minCharge || 0;
            let maxCharge = item?.maxCharge || 0;
            let chargeType = item?.chargeType;
            return ['1', '2'].includes(chargeType) && ((this.isKhdn && Math.max(chgAmtFrom, minCharge, maxCharge) > 0) || (!this.isKhdn && chgAmtFrom > 0));
          }).map(item => {
            item.code = this.code;
            item.productCode = this.code + item?.chargeCode + item?.currency;
            return item;
          });
        this.response = this.rawData;
        this.models = this.response;
        this.dataOrigin = _.cloneDeep(this.response);
        this.count = this.response.length;
        this.offset = 0;
        this.countCheckedOnPage = 0;
        for (const item of this.models) {
          if (
            this.listCode.findIndex((code) => {
              return code === item?.productCode;
            }) > -1
          ) {
            this.selected.push(item);
            this.listSelected.push(item);
            this.countCheckedOnPage += 1;
          }
        }
        this.checkAll = this.models.length > 0 && this.countCheckedOnPage === this.models.length;
      }
      this.prevChargeGroup = this.searchFeeType;
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
    });
  }

  findChildFee(parent): Array<any> {
    return this.models.filter(item => item.sumChargeCode == parent.chargeCode && item.currency == parent.currency);
  }

  get findParentFee(): Array<any> {
    return this.models.filter(item => !item.sumChargeCode);
  }

  onCheckboxRmFn(item, isChecked, event) {
    if (isChecked) {
      // Chọn tối đa 30 khoảm mục phí
      if (this.maxLength && this.listCode.length > this.maxLength) {
        this.messageService.warn(`Chỉ được phép chọn tối đa ${this.maxLength} loại phí`);
        event.preventDefault();
        return;
      }
      this.selected.push({ ...item });
      this.listSelected.push({ ...item });
      this.listCode.push(item?.productCode);
      this.countCheckedOnPage += 1;
    } else {
      this.selected = this.selected.filter((itemSelected) => {
        return itemSelected.productCode !== item?.productCode;
      });
      this.listSelected = this.listSelected.filter((itemSelected) => {
        return itemSelected.productCode !== item?.productCode;
      });
      this.listCode = this.listCode.filter((hrsCode) => {
        return hrsCode !== item?.productCode;
      });
      this.countCheckedOnPage -= 1;
    }
    this.checkAll = this.countCheckedOnPage === this.models.length;
  }

  onCheckboxAllFn(isChecked, event) {
    this.checkAll = isChecked;
    if (!isChecked) {
      this.countCheckedOnPage = 0;
    } else {
      this.countCheckedOnPage = this.models.length;
      // Chọn tối đa 30 khoảm mục phí
      let adđitional = this.models.filter(elm => !this.listCode.includes(elm.productCode)).length;
      if (this.maxLength && (this.listCode.length + adđitional) > this.maxLength) {
        this.messageService.warn(`Chỉ được phép chọn tối đa ${this.maxLength} loại phí`);
        event.preventDefault();
        return;
      }
    }
    for (const item of this.models) {
      item.isChecked = isChecked;
      if (isChecked) {
        if (
          this.selected.findIndex((itemChoose) => {
            return itemChoose.productCode === item.productCode;
          }) === -1
        ) {
          this.selected.push({ ...item });
          this.listSelected.push({ ...item });
          this.listCode.push(item.productCode);
        }
      } else {
        this.selected = this.selected.filter((itemChoose) => {
          return itemChoose.productCode !== item.productCode;
        });
        this.listSelected = this.listSelected.filter((itemChoose) => {
          return itemChoose.productCode !== item.productCode;
        });
        this.listCode = this.listCode.filter((itemChoose) => {
          return itemChoose !== item.productCode;
        });
      }
    }
  }

  isChecked(item) {
    return (this.listCode.findIndex((hrsCode) => { return (hrsCode === item?.productCode); }) > -1);
  }

  choose() {
    const listRemove = this.listCodeOld?.filter(
      (codeOld) => this.listCode?.findIndex((code) => code === codeOld) === -1
    );
    if (this.searchFeeType == 'REMITTANCES') {
      const haveSelectRemittances = this.listSelected.find(item => item.chargeGroup == 'REMITTANCES');
      if(haveSelectRemittances){
        this.listSelected = this.listSelected.filter(item => item.chargeGroup == 'REMITTANCES');
      }
    }
    const creditGroup = ['CREDIT', 'NON_CREDIT'];
    if (creditGroup.includes(this.searchFeeType)) {
      const haveSelectCreditOrNonCredit = this.listSelected.filter(item => creditGroup.includes(item.chargeGroup));
      if(haveSelectCreditOrNonCredit?.length){
        this.listSelected = this.listSelected.filter(item => creditGroup.includes(item.chargeGroup));
      }
    }
    const data = {
      listSelected: this.listSelected,
      listRemove,
      listCode :this.listCode
    };
    this.modalActive.close(data);
    // this.dialogRef.close(data);
  }

  closeModal() {
    this.modalActive.close(false);
    // this.dialogRef.close(false);

  }

  handleChangePageSize(event) {
    console.log(event);
    this.params.page = 0;
    this.params.size = event;
    this.limit = event;
    // this.reload();
  }

  feeTypeChange() {
    // let creditGroup = ['CREDIT', 'NON_CREDIT'];
    // let changedCreditGroupSME = !(creditGroup.includes(this.searchFeeType) && creditGroup.includes(this.prevChargeGroup)) && this.department === Division.SME;
    // if ((!this.isKhdn || changedCreditGroupSME) && this.searchFeeType !== this.prevChargeGroup) {
    //   // reset all checked
    //   this.selected = [];
    //   this.listSelected = [];
    //   this.listCodeOld = [];
    // }
  }

  formatCurrency(element) {
    if (element == 0) {
      return '0';
    }
    if (!element || element === 'NaN' || element === '--') {
      return '';
    }
    let formated: string = element.toString();
    let first = '';
    let last = '';
    if (formated.includes('.')) {
      first = formated.substr(0, formated.indexOf('.'));
      last = formated.substr(formated.indexOf('.'));
    } else {
      first = formated;
    }
    if (Number(first)) {
      first = Number(first).toLocaleString('en-US', {
        style: 'currency',
        currency: 'VND',
      });
      first = first.substr(1, first.length);
      formated = first + last;
    }
    return formated;
  }
  trimSearchValue() {
    if (Utils.isNotNull(this.search_value)) {
      this.search_value = this.search_value.trim();
      this.searchFE();
    }
  }
}
