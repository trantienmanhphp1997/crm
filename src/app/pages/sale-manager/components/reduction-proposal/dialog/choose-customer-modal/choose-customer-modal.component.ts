import { Component, HostBinding, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';
import { Division, functionUri } from 'src/app/core/utils/common-constants';
import { ReductionProposalApi } from 'src/app/pages/sale-manager/api/reduction-proposal.api';

@Component({
  selector: 'app-choose-customer-modal',
  templateUrl: './choose-customer-modal.component.html',
  styleUrls: ['./choose-customer-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ChooseCustomerModalComponent implements OnInit {
  @HostBinding('choose-customer-modal') classCreateCalendar = true;
  constructor(
    private router: Router,
    private dialogRef: MatDialogRef<ChooseCustomerModalComponent>,
    private reductionProposal: ReductionProposalApi
  ) { }

  info: any;


  ngOnInit(): void {
  }

  choose(choose: boolean): void {
    console.log('choose: '+choose);

    this.info = this.reductionProposal.info.value;
    const customer = this.info?.customer;
    customer.existed = choose;
    this.reductionProposal.info.next({'customer': customer, toi: {}, authorization: [], files: []});
    this.router.navigate([functionUri.reduction_proposal, 'create'], {
      queryParams: {
        isExist: choose,
      },
    });
    this.dialogRef.close();
  }

  closeModal() {
    this.dialogRef.close();
  }
}
