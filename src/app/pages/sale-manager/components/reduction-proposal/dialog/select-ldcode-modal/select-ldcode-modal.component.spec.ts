import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LdCodeModalComponent } from './select-ldcode-modal.component';

describe('RmModalComponent', () => {
  let component: LdCodeModalComponent;
  let fixture: ComponentFixture<LdCodeModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LdCodeModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LdCodeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
