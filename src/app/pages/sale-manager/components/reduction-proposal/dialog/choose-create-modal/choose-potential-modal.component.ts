import { Component, HostBinding, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';
import { Division, functionUri } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'app-potential-modal',
  templateUrl: './choose-potential-modal.component.html',
  styleUrls: ['./choose-potential-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ChoosePotentialModalComponent implements OnInit {
  @HostBinding('choose-potential-modal') classCreateCalendar = true;
  constructor(
    private router: Router,
    private dialogRef: MatDialogRef<ChoosePotentialModalComponent>
  ) { }

  ngOnInit(): void {
  }

  choose(choose: boolean): void {
    if (choose) {
      const isKHDN = '1';
      const url = this.router.serializeUrl(this.router.createUrlTree([functionUri.lead_management, 'create'], {
        state: ({}),
        queryParams: {
          customerType: '1',
          haveInDiv: '1',
          haveSME: '1'
        }
      }));
      window.open(url, '_blank');
    }
    this.dialogRef.close();
  }
}
