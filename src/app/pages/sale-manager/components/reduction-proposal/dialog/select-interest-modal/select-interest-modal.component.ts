import {
  Component,
  OnInit,
  Injector,
  ViewChild,
  ViewEncapsulation,
  HostBinding,
  Input,
  ChangeDetectorRef,
  OnDestroy
} from '@angular/core';
import { Validators } from '@angular/forms';
import { DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { global } from '@angular/compiler/src/util';
import { Division, FunctionCode, Scopes, SessionKey } from 'src/app/core/utils/common-constants';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ReductionProposalApi } from 'src/app/pages/sale-manager/api/reduction-proposal.api';

@Component({
  selector: 'app-interest-modal',
  templateUrl: './select-interest-modal.component.html',
  styleUrls: ['./select-interest-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class InterestModalComponent extends BaseComponent implements OnInit, OnDestroy {
  limit = global.userConfig.pageSize;
  isLoading = false;
  @ViewChild('tablefee') public tablerm: DatatableComponent;
  @HostBinding('class.list__interest-content') appRightContent = true;
  isDisableBranch = false;
  isDisableBlock = false;
  showSearchBasic = true;
  obj: any;
  search_form = this.fb.group({
    employeeCode: ['', Validators.maxLength(20)],
    employeeId: ['', Validators.maxLength(20)],
    fullName: ['', Validators.maxLength(120)],
    email: ['', Validators.maxLength(50)],
    mobile: ['', Validators.maxLength(30)],
    crmIsActive: ['true'],
    rmBlock: [''],
    productGroup: [''],
    divisionCode: [''],
  });
  search_value: string;
  show: boolean;
  facilities: Array<any>;
  grades: Array<any>;
  customer_ranges: Array<any> = [];
  facilityCode: Array<string>;
  prevSearch: string;
  // model = [];
  models = [];
  count: number; // total
  offset: number; // page
  params: any = {
    adjustmentPeriod: '',
    department: '',
    currency: '',
    productCode: '',
    period: '',
    rankingCredit: '',
    customerType: '',
    category: '',
    customerSegment: ''
  };
  searchType = false;
  body = {};
  checkAll = false;
  listSelected = [];
  listCode = [];
  listCodeOld = [];
  countCheckedOnPage = 0;
  @Input() config: any;
  showCheckBox = true;
  listInterest = [];
  selected = [];
  SelectionType = SelectionType;
  rawData: any[];
  response: any[];
  isFirstLoad = true;
  customerSegment = '';
  scoreValue = '';
  customerObject: any;
  productType: any;
  productCode: any;
  blockCode: any;
  toi: any;
  customerCodeObject: any;




  constructor(
    injector: Injector,
    private reductionProposalApi: ReductionProposalApi,
    private modalActive: NgbActiveModal,
    private dtc: ChangeDetectorRef,
    // public dialogRef: MatDialogRef<any>
  ) {
    super(injector);
    this.obj = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REDUCTION_PROPOSAL}`);
    this.params.rsId = this.obj?.rsId;
  }

  ngOnInit() {
    this.customer_ranges.push({ code: '', name: this.fields.all });
    this.customer_ranges.push({ code: 'true', name: this.fields.effective });
    this.customer_ranges.push({ code: 'false', name: this.fields.expire });
    this.params.rsId = _.get(this.obj, 'rsId');
    if (this.grades?.length > 0) {
      this.grades = [
        {
          id: '',
          code: '',
          value_to_search: _.get(this.fields, 'all'),
        },
        ...this.grades,
      ];
    }

    this.listCodeOld = [...this.listCode];
  }

  changeBranch(event) { }

  paging(event) {
  }

  searchBasic() {
    this.searchType = false;
    // this.isLoading = true;
    if (this.search_value) {
      this.response = this.rawData.filter(item => {
        return item.name.toLocaleLowerCase().includes(this.search_value.trim().toLocaleLowerCase());
      });
    } else {
      this.response = this.rawData;
    }
    this.models = this.response;
    this.count = this.response.length;
    this.offset = 0;
    this.countCheckedOnPage = 0;
    for (const item of this.models) {
      if (
        this.listCode.findIndex((code) => {
          return code === item?.productCode;
        }) > -1
      ) {
        // this.selected.push(item);
        this.countCheckedOnPage += 1;
      }
    }
    this.checkAll = this.models.length > 0 && this.countCheckedOnPage === this.models.length;

  }

  reload() {
    this.isLoading = true;
    this.scoreValue = this.scoreValue ? this.scoreValue : 'BB';
    this.reductionProposalApi.getInterestRate({
      customerSegment: this.customerSegment,
      rankingCredit: this.scoreValue,
      department: this.blockCode,
      productCode: this.productCode,
      intType: this.productType,
      customerType: this.customerObject,
      // "toi": this.toi
    }).subscribe((res) => {
      this.isLoading = false;
      // API bị lỗi sẽ trả về định dạng object
      if (Object.prototype.toString.call(res) === '[object Object]' || (res as []).length === 0) {
        this.messageService.error('Không tìm thấy dữ liệu biểu lãi');
      } else {
        this.rawData = res as any[];

        this.rawData = this.rawData.map(item => {
          item.period = item?.periodTo
            ? item?.periodFrom + ' - ' + item?.periodTo
            : '> ' + item?.periodFrom;

          item.name = item?.productName;
          const productCode = item?.productCode;
          item.productCodeOrigin = productCode;
          item.productCode = item?.marginId;
          return item;
        });
        this.response = this.rawData;
        this.models = this.response;
        this.count = this.response.length;
        this.offset = 0;
        this.countCheckedOnPage = 0;
        for (const item of this.models) {
          if (
            this.listCode.findIndex((code) => {
              return code === item?.productCode;
            }) > -1
          ) {
            // this.selected.push(item);
            this.countCheckedOnPage += 1;
          }
        }
        this.checkAll = this.models.length > 0 && this.countCheckedOnPage === this.models.length;
      }
    }, error => {
      this.isLoading = false;
    });
  }

  onCheckboxRmFn(item, isChecked) {
    if (isChecked) {
      // Chọn tối đa 30 khoảm mục phí
      if (this.listCode.length > 14) {
        this.messageService.warn('Chỉ được phép chọn tối đa 15 biểu lãi');
        event.preventDefault();
        return;
      }
      this.selected.push(item);
      this.listCode.push(item?.productCode);
      this.countCheckedOnPage += 1;
    } else {
      this.selected = this.selected.filter((itemSelected) => {
        return itemSelected.productCode !== item?.productCode;
      });
      this.listCode = this.listCode.filter((hrsCode) => {
        return hrsCode !== item?.productCode;
      });
      this.countCheckedOnPage -= 1;
    }
    this.checkAll = this.countCheckedOnPage === this.models.length;
  }

  isChecked(item) {
    return (this.listCode.findIndex((hrsCode) => { return (hrsCode === item?.productCode || hrsCode === item?.optionCode) ; }) > -1);
  }

  getTitleException(){
    if(this.productCode === 40){
      return 'title.exceptionEndow';
    }
    if(this.productCode === 30){
      return 'title.exceptionAbility';
    }
    if(this.productCode === 10){
      return 'title.exceptionCib';
    }
  }


  choose() {

    const listRemove = this.listCodeOld?.filter(
      (codeOld) => this.listCode?.findIndex((code) => code === codeOld) === -1
    );
    const data = {
      listSelected: this.selected,
      listRemove,
      showOrHide: this.models.length === 0
    };

    this.modalActive.close(data);
    // this.dialogRef.close(data);
  }

  closeModal() {
    this.modalActive.close(false);
    // this.dialogRef.close(false);

  }

  handleChangePageSize(event) {
    console.log(event);
    this.params.page = 0;
    this.params.size = event;
    this.limit = event;
    // this.reload();
  }
  ngOnDestroy() {
    console.log('ngOnDestroy');
    // this.modalActive.dismiss();
  }
}
