import { Component, HostBinding, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ReductionProposalApi } from 'src/app/pages/sale-manager/api/reduction-proposal.api';
import { catchError } from 'rxjs/operators';
import { forkJoin, of } from 'rxjs';
import * as _ from 'lodash';
import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';
import { CategoryApi } from 'src/app/pages/report/apis';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { BRANCH_HO, CommonCategory, Division } from 'src/app/core/utils/common-constants';
import {BaseComponent} from 'src/app/core/components/base.component';

@Component({
  selector: 'app-potential-modal',
  templateUrl: './confirm-authorization-modal.component.html',
  styleUrls: ['./confirm-authorization-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ConfirmAuthorizationModalComponent extends BaseComponent implements OnInit {
  @HostBinding('confirm-authorization-modal') classCreateCalendar = true;
  constructor(
    injector: Injector,
    private reductionProposalApi: ReductionProposalApi,
    private dialogRef: MatDialogRef<ConfirmAuthorizationModalComponent>,
    protected messageService: NotifyMessageService,
    private  categoryService :CategoryService,
  ) {
    super(injector);
    dialogRef.disableClose = true;
  }

  data = [];
  selected = [];
  info: any = {};
  mapId = {};
  authors;
  maxPiority = 0;
  isRM = true;
  titles = {};
  divisionCode;
  requiredAll = false
  type;
  interestType;
  hasTTTM;
  disabledTTTM = false

  ngOnInit(): void {
    this.info = this.reductionProposalApi?.info?.value;
    this.selected = this.info?.authorization;
    this.requiredAll = [Division.FI, Division.INDIV].includes(this.divisionCode);
    this.data = [];
    let tmp = {};
    // Group user has same piority
    this.authors.data.map(item => {
      let piority = item?.orderSigning;
      let newObj:any = {
        fullName: item?.name,
        note: '',
        priority: piority,
        userName: item?.user,
        titleCategory: this.getTitle(piority),
        signedTime: '',
        status: '',
        typeSignature: '',
        moDocId: '',
        reductionProposalCode: '',
      };

      if(
          // Nếu user trình là CBQL tại chi nhánh thì làm mờ, không cho chọn cấp phê duyệt đầu tiên (CBQL trực tiếp của người khởi tạo)
        (piority == 0 && !this.isRM)
          // [Giảm phí](Ẩn không cho chọn đối với trường hợp chọn loại phí chỉ là TTTM)
        || (this.type === 0 && piority == 2 && !this.hasTTTM && [Division.SME].includes(this.divisionCode))
          // [Giảm phí-Khối FI]: Disabled 3 cấp phê duyệt đầu tiên trong trường hợp khởi tạo là user HO
        || (this.type === 0 && piority < 3 && this.divisionCode === Division.FI && this.currUser?.branch === BRANCH_HO)){
          newObj.disabled = true;
          this.disabledTTTM = this.type === 0 && this.divisionCode === Division.FI && this.currUser?.branch === BRANCH_HO;
        }

      if (tmp[piority] === undefined) {

        tmp[piority] = { childs: [newObj], num: newObj.priority, titleCategory: newObj?.titleCategory, disabled: newObj?.disabled };
      } else if (tmp[piority].childs !== undefined) {
        tmp[piority].childs.push(newObj);
      } else {
        tmp[piority].childs = [];
        tmp[piority].childs.push(newObj);
      }

    });
    // Sort piority
    let sortedKey = Object.keys(tmp).sort((a: any, b: any) => {
      return a.piority - b.piority;
    })
    // init data
    sortedKey.forEach(item => {
      this.maxPiority = Number(item);
      this.data.push(tmp[item]);
    });

    if (this.selected) {
      const selectedId = this.selected?.map(item => item.userName + item.priority);
      this.data = this.data.map(item => {
        if (item.childs) {
          item.childs.map(child => {
            if (selectedId.includes(child.userName + child.priority)) {
              child.isChecked = true;
              if (child?.id) {
                this.mapId[child?.priority] = child?.id;
              }
            }
          });
        } else {
          if (item?.id) {
            this.mapId[item?.priority] = item?.id;
          }
        }
        return item;
      });
    } else {
      this.selected = this.data;
    }
    // Tự động chọn nếu user cấp cao nhất chỉ có 1
    if (this.data[this.data.length - 1].childs.length === 1) {
      this.data[this.data.length - 1].childs[0].isChecked = true;
      this.select(this.data[this.data.length - 1].childs[0]);
    }
  }

  getTitle(piority: any) {
    console.log('this.titles: ', this.titles);
    return this.titles[piority] || '';
  }

  select(row): void {
    this.selected = this.selected.filter(item => item.priority !== row.priority);
    this.selected.push(row);
  }

  confirm(): void {
    let selectedMaxPiority = false;
    let selectedMinPiority = false;
    let selectedSecondPiority = false;
    let selectedTTTM = false;

    this.selected = this.selected.map(item => {
      if (this.mapId[item?.priority]) {
        item.id = this.mapId[item?.priority];
      }
      if (Number(item?.priority) === this.maxPiority) {
        selectedMaxPiority = true;
      }
      if (Number(item?.priority) === 0) {
        selectedMinPiority = true;
      }
      if (Number(item?.priority) === 1) {
        selectedSecondPiority = true;
      }
      if (Number(item?.priority) === 2) {
        selectedTTTM = true;
      }
      return item;
    });
    let hasError = false;
    if(!this.disabledTTTM && this.isRM && this.data.length !== this.selected.length){
      hasError = true;
    }
    let message = '';
    if([Division.SME, Division.CIB].includes(this.divisionCode)){
      if (!selectedMaxPiority) {
        message = 'Bắt buộc phải chọn người phê duyệt của cấp cuối cùng';
      }
      if (!selectedMinPiority && this.isRM) {
        message = 'Bắt buộc phải chọn cấp phê duyệt đầu tiên (cấp CBQL)';
      }

      if (!selectedSecondPiority && !this.isRM) {
        message = 'Bắt buộc phải chọn cấp phê duyệt thứ 2 (BLĐ chi nhánh)';
      }
      if(!selectedTTTM && this.type === 1 && this.interestType !== 1){
        message = 'Bắt buộc phải chọn cấp phòng PTKD';
      }

      if(this.type === 0 && this.hasTTTM && !selectedTTTM && this.maxPiority >= 2 && this.divisionCode === Division.SME){
        // Check voi bieu phi
        message = 'Bắt buộc phải chọn cấp '+ this.getTitle(2);

      }
    }else if (
      // Bắt buộc chọn tất cả đối với user là RM
      (!this.disabledTTTM && this.isRM && this.data.length !== this.selected.length)
      // Bắt buộc chọn trừ cấp đầu tiên đối với user là CBQL
      || (!this.disabledTTTM && !this.isRM && this.data.length -1 !== this.selected.length)
      // Bắt buộc chọn trừ 3 cấp đầu tiên đối với user HO khối FI
      || (this.disabledTTTM && this.data.length -3 !== this.selected.length)
      ){
      console.log(`this.data.length: ${this.data.length} !== this.selected.length: ${this.selected.length}`);
      message = 'Bắt buộc chọn tất cả các cấp phê duyệt';
    }

    if(message.length > 0){
      this.messageService.warn(message);
      return;
    }
    this.info['authorization'] = this.selected;
    this.info['authLength'] = this.data.length;
    this.reductionProposalApi.info.next(this.info);
    this.dialogRef.close();
  }

  closeModal(): void {
    this.dialogRef.close();
  }

}
