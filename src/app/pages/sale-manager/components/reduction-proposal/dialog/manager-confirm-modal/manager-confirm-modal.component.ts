import { AfterViewInit, Component, Inject, Injector, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { ReductionProposalApi } from '../../../../api/reduction-proposal.api';
import { NotifyMessageService } from '../../../../../../core/components/notify-message/notify-message.service';
import { BaseComponent } from '../../../../../../core/components/base.component';
import { Scopes } from '../../../../../../core/utils/common-constants';
import { global } from '@angular/compiler/src/util';

@Component({
  selector: 'app-manager-confirm-modal',
  templateUrl: './manager-confirm-modal.component.html',
  styleUrls: ['./manager-confirm-modal.component.scss']
})
export class ManagerConfirmModalComponent extends BaseComponent implements OnInit, AfterViewInit {
  isLoading = false;
  public notificationMessage: any;
  paramSearch = {
    size: global.userConfig.pageSize,
    page: 0,
    rsId: '',
    scope: Scopes.VIEW
  };
  code;
  protected messageService: NotifyMessageService;

  constructor(
    injector: Injector,
    public dialog: MatDialog,
    private reductionProposalApi: ReductionProposalApi,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) {
    super(injector);
    this.isLoading = true;
    this.paramSearch.rsId = this.objFunction?.rsId;
    this.code = data.code;
  }

  ngOnInit(): void {
  }


  ngAfterViewInit(): void {
  }

  managerConfirm(status: boolean) {
    const params: any = {};
      params.status = status ? 0 : 1;
      params.code = this.code;
    this.reductionProposalApi.CBQLconfirmReduction(params).subscribe(() => {
      this.messageService.success(this.notificationMessage.success);
    }, (e) => {
      this.isLoading = false;
      this.messageService.error(this.notificationMessage.error);
    });
  }
}
