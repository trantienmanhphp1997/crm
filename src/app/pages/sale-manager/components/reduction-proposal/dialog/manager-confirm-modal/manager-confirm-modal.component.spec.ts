import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerConfirmModalComponent } from './manager-confirm-modal.component';

describe('ManagerConfirmModalComponent', () => {
  let component: ManagerConfirmModalComponent;
  let fixture: ComponentFixture<ManagerConfirmModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerConfirmModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerConfirmModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
