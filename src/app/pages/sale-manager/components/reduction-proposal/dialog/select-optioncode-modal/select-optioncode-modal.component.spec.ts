import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionCodeModalComponent } from './select-optioncode-modal.component';

describe('RmModalComponent', () => {
  let component: OptionCodeModalComponent;
  let fixture: ComponentFixture<OptionCodeModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionCodeModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionCodeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
