import { forkJoin, Observable } from 'rxjs';
import { Component, OnInit, Injector, ViewChild, ViewEncapsulation, HostBinding, Input } from '@angular/core';
import { Validators } from '@angular/forms';
import { DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { global } from '@angular/compiler/src/util';
import { Division, FunctionCode, Scopes, SessionKey } from 'src/app/core/utils/common-constants';
// import { RmService } from '../../services';
import { Utils } from 'src/app/core/utils/utils';
import { cleanDataForm } from 'src/app/core/utils/function';
// import { BlockApi, RmApi } from '../../apis';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { RmService } from 'src/app/pages/rm/services';
import { BlockApi, RmApi } from 'src/app/pages/rm/apis';
import { ReductionProposalApi } from 'src/app/pages/sale-manager/api/reduction-proposal.api';
import { ReportCustomerDevelopComponent } from 'src/app/pages/report/components';

@Component({
  selector: 'app-optioncode-modal',
  templateUrl: './select-optioncode-modal.component.html',
  styleUrls: ['./select-optioncode-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class OptionCodeModalComponent extends BaseComponent implements OnInit {
  limit = global.userConfig.pageSize;
  isLoading = false;
  @ViewChild('tablefee') public tablerm: DatatableComponent;
  @HostBinding('class.list__optioncode-content') appRightContent = true;
  isDisableBranch = false;
  isDisableBlock = false;
  showSearchBasic = true;
  obj: any;
  branch_codes: Array<any> = [];
  search_form = this.fb.group({
    employeeCode: ['', Validators.maxLength(20)],
    employeeId: ['', Validators.maxLength(20)],
    fullName: ['', Validators.maxLength(120)],
    email: ['', Validators.maxLength(50)],
    mobile: ['', Validators.maxLength(30)],
    crmIsActive: ['true'],
    rmBlock: [''],
    productGroup: [''],
    divisionCode: [''],
  });
  search_value: string;
  search_date_from: Date;
  search_date_to: Date;
  show: boolean;
  facilities: Array<any>;
  grades: Array<any>;
  customer_ranges: Array<any> = [];
  facilityCode: Array<string>;
  prevSearch: string;
  // model = [];
  models = [];
  reduction = [];
  count: number; // total
  offset: number; // page
  params: any = {
    adjustmentPeriod: "",
    department: "",
    currency: "",
    optionCode: "",
    period: "",
    rankingCredit: "",
    customerType: "",
    category: "",
    customerSegment: ""
  };
  searchType: boolean = false;

  checkAll = false;
  listSelected = [];
  listCode = [];
  listCodeOld = [];
  countCheckedOnPage = 0;
  SelectionType = SelectionType;
  @Input() config: any;
  @Input() customerCode: any;
  @Input() businessRegistrationNumber: any;
  showCheckBox = true;
  listInterest = [];
  selected = [];
  type;
  idCard;
  blockCode;

  constructor(
    injector: Injector,
    private reductionProposalApi: ReductionProposalApi,
    private modalActive: NgbActiveModal
  ) {
    super(injector);
    this.obj = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REDUCTION_PROPOSAL}`);
    this.params.rsId = this.obj?.rsId;
  }

  ngOnInit() {
    this.isLoading = true;
    this.customer_ranges.push({ code: '', name: this.fields.all });
    this.customer_ranges.push({ code: 'true', name: this.fields.effective });
    this.customer_ranges.push({ code: 'false', name: this.fields.expire });
    this.params.rsId = _.get(this.obj, 'rsId');
    if (this.grades?.length > 0) {
      this.grades = [
        {
          id: '',
          code: '',
          value_to_search: _.get(this.fields, 'all'),
        },
        ...this.grades,
      ];
    }

    this.listCodeOld = [...this.listCode];
    this.reload();
    this.isLoading = false;

  }

  changeBranch(event) { }

  paging(event) {
    if (!this.isLoading) {
      this.params.page = _.get(event, 'page') ? _.get(event, 'page') - 1 : 0;
      this.reload();
    }
  }

  searchBasic() {
    this.searchType = false;
    let result = this.reduction;
    this.models = []
    if(this.search_value){
      result = result.filter((i) => i?.optionCode?.toLocaleLowerCase()?.includes(this.search_value.trim().toLocaleLowerCase()))
    }
    if(this.search_date_from){
      result = result.filter((i) => i?.createdDate >= this.search_date_from)
    }
    if(this.search_date_to){
      result = result.filter((i) => i?.createdDate <= this.search_date_to)
    }
    this.models = result;
  }


  reload() {
    this.isLoading = true;
    let param;
    if(this.type === 0 && this.blockCode == Division.INDIV){
      this.reductionProposalApi.getIndivOptionCode(this.customerCode, this.idCard).subscribe(
        (data: any) => {

          if(data?.status != 200){
            this.isLoading = false;
            return;
          }
          let listLoanInfoByLoanId = data?.data || [];
          let result = listLoanInfoByLoanId
          .filter(itm => itm?.loanId)
          .map(element => {
            element.optionCode = element?.loanId;
            return element;
          });
          this.models = result;
          this.reduction = result;
          this.count = result?.length || 0;
          this.offset = 0;
          this.countCheckedOnPage = 0;
          for (const item of this.models) {
            if (
              this.listCode.findIndex((code) => {
                return code === item.optionCode;
              }) > -1
            ) {
              // this.selected.push(item);
              this.countCheckedOnPage += 1;
            }
          }

        this.checkAll = this.models?.length > 0 && this.countCheckedOnPage === this.models?.length;
        this.isLoading = false;
      },() => {
        this.isLoading = false;
      });
    }else{
      param = {
        "customerCode": this.customerCode || "",
        "businessRegistrationNumber": this.businessRegistrationNumber || "",
      }
      this.reductionProposalApi.getScopeReduction(param).subscribe(
        (data) => {

          if(Utils.isEmpty(data)){
            this.isLoading = false;
            return;
          }
          let listLoanInfoByLoanId = data?.listLoanInfoByLoanId || [];
          let result =  [];
          listLoanInfoByLoanId?.filter(item => item?.loanId && item?.loaiPhuongAn === 'GOC').forEach(element => {
            var dateObject = null;
            if(element?.ngayTaoPhuongAn){
              var dateParts = element?.ngayTaoPhuongAn.split("/");
              dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
            }
            result.push({
              "optionCode": element?.loanId,
              "createdDate": dateObject,
              "optionType": element?.loaiPhuongAn,
              "listHanMuc": element?.listHanMuc,
            })
          });
          this.models = result;
          this.reduction = result;
          this.selected = result.filter(item => {
            return this.listCode.includes(item.optionCode);
          })
          this.count = result?.length || 0;
          this.offset = 0;
          this.countCheckedOnPage = 0;
          for (const item of this.models) {
            if (
              this.listCode.findIndex((code) => {
                return code === item.optionCode;
              }) > -1
            ) {
              // this.selected.push(item);
              this.countCheckedOnPage += 1;
            }
          }

        this.checkAll = this.models?.length > 0 && this.countCheckedOnPage === this.models?.length;
        this.isLoading = false;
      },() => {
        this.isLoading = false;
      });
    }
  }

  onCheckboxRmFn(item, isChecked) {
    if (isChecked) {
      this.selected.push(item);
      this.listCode.push(item.optionCode);
      this.countCheckedOnPage += 1;
    } else {
      this.selected = this.selected.filter((itemSelected) => {
        return itemSelected.optionCode !== item.optionCode;
      });
      this.listCode = this.listCode.filter((hrsCode) => {
        return hrsCode !== item.optionCode;
      });
      this.countCheckedOnPage -= 1;
    }
    this.checkAll = this.countCheckedOnPage === this.models.length;
  }

  onselectRadio(item) {
    this.selected = item;
  }

  isChecked(item) {
    return (
      this.listCode.findIndex((hrsCode) => {
        return hrsCode === item.optionCode;
      }) > -1
    );
  }

  choose() {
    const listRemove = this.listCodeOld?.filter(
      (codeOld) => this.listCode?.findIndex((code) => code === codeOld) === -1
    );
    const data = {
      listSelected: this.selected,
      listRemove,
    };
    this.modalActive.close(data);
  }

  closeModal() {
    this.modalActive.close(false);
  }

  handleChangePageSize(event) {
    this.params.page = 0;
    this.params.size = event;
    this.limit = event;
    this.reload();
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
  }
}
