import { forkJoin, Observable } from 'rxjs';
import { Component, OnInit, Injector, ViewChild, ViewEncapsulation, HostBinding, Input } from '@angular/core';
import { Validators } from '@angular/forms';
import { DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { global } from '@angular/compiler/src/util';
import { Division, FunctionCode, Scopes, SessionKey } from 'src/app/core/utils/common-constants';
// import { RmService } from '../../services';
import { Utils } from 'src/app/core/utils/utils';
import { cleanDataForm } from 'src/app/core/utils/function';
// import { BlockApi, RmApi } from '../../apis';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { RmService } from 'src/app/pages/rm/services';
import { BlockApi, RmApi } from 'src/app/pages/rm/apis';
import { ReductionProposalApi } from 'src/app/pages/sale-manager/api/reduction-proposal.api';
import * as moment from 'moment';

@Component({
  selector: 'app-select-md-modal',
  templateUrl: './select-md-modal.component.html',
  styleUrls: ['./select-md-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SelectMdModalComponent extends BaseComponent implements OnInit {
  limit = global.userConfig.pageSize;
  isLoading = false;
  @ViewChild('tablefee') public tablerm: DatatableComponent;
  @HostBinding('class.list__mdcode-content') appRightContent = true;
  isDisableBranch = false;
  isDisableBlock = false;
  showSearchBasic = true;
  obj: any;
  branch_codes: Array<any> = [];
  search_form = this.fb.group({
    employeeCode: ['', Validators.maxLength(20)],
    employeeId: ['', Validators.maxLength(20)],
    fullName: ['', Validators.maxLength(120)],
    email: ['', Validators.maxLength(50)],
    mobile: ['', Validators.maxLength(30)],
    crmIsActive: ['true'],
    rmBlock: [''],
    productGroup: [''],
    divisionCode: [''],
  });
  search_value: string;
  show: boolean;
  facilities: Array<any>;
  grades: Array<any>;
  customer_ranges: Array<any> = [];
  facilityCode: Array<string>;
  prevSearch: string;
  // model = [];
  models = [];
  count: number; // total
  offset: number; // page
  params: any;
  searchType: boolean = false;

  checkAll = false;
  listSelected = [];
  listCode = [];
  listCodeOld = [];
  countCheckedOnPage = 0;
  @Input() config: any;
  @Input() listMD: any;
  showCheckBox = true;
  listInterest = [];
  selected = [];

  customerCode;
  rawData = [];
  data = [];
  search_date_from: Date;
  search_date_to: Date;


  constructor(
    injector: Injector,
    private reductionProposalApi: ReductionProposalApi,
    private modalActive: NgbActiveModal
  ) {
    super(injector);
    this.obj = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REDUCTION_PROPOSAL}`);
  }

  ngOnInit() {
    this.isLoading = true;
    this.customer_ranges.push({ code: '', name: this.fields.all });
    this.customer_ranges.push({ code: 'true', name: this.fields.effective });
    this.customer_ranges.push({ code: 'false', name: this.fields.expire });
    if (this.grades?.length > 0) {
      this.grades = [
        {
          id: '',
          code: '',
          value_to_search: _.get(this.fields, 'all'),
        },
        ...this.grades,
      ];
    }

    this.listCodeOld = [...this.listCode];
    this.isLoading = false;

  }

  changeBranch(event) { }

  paging(event) {
  }

  searchBasic() {
    this.searchType = false;
    let result = this.rawData;
    this.models = []
    if (this.search_value) {
      result = result.filter((i) => i?.ARRANGEMENT_NBR?.toLocaleLowerCase()?.includes(this.search_value.trim().toLocaleLowerCase()))
    }
    if (this.search_date_from) {
      result = result.filter((i) => moment(i?.ARRANGEMENT_VALUE_DATE, "DD/MM/YYYY").toDate() >= this.search_date_from)
    }
    if (this.search_date_to) {
      result = result.filter((i) => moment(i?.ARRANGEMENT_VALUE_DATE, "DD/MM/YYYY").toDate() <= this.search_date_to)
    }
    this.models = result;
  }


  reload() {
    if (!this.listMD || Object.prototype.toString.call(this.listMD) === '[object Object]' || (this.listMD as any[]).length === 0) {
      this.messageService.error('Không tìm thấy dữ liệu MD');
    } else {
      this.rawData = this.listMD as any[];
      this.rawData = this.rawData.map(item => {
        item.ldCode = item?.ARRANGEMENT_NBR;
        return item;
      });
      this.data = this.rawData;
      this.models = this.data;
      this.selected = this.data.filter(item => { return this.listCode.includes(item.ldCode)});
      this.count = this.data.length;
      this.offset = 0;
      this.countCheckedOnPage = 0;
      for (const item of this.models) {
        if (
          this.listCode.findIndex((code) => {
            return code === item.ldCode;
          }) > -1
        ) {
          this.countCheckedOnPage += 1;
        }
      }
      this.checkAll = this.models.length > 0 && this.countCheckedOnPage === this.models.length;
    }
  }

  onCheckboxRmFn(item, isChecked) {
    if (isChecked) {
      this.selected.push(item);
      this.listCode.push(item.ARRANGEMENT_NBR);
      this.countCheckedOnPage += 1;
    } else {
      this.selected = this.selected.filter((itemSelected) => {
        return itemSelected.ldCode !== item.ldCode;
      });
      this.listCode = this.listCode.filter((hrsCode) => {
        return hrsCode !== item.ARRANGEMENT_NBR;
      });
      this.countCheckedOnPage -= 1;
    }
    this.checkAll = this.countCheckedOnPage === this.models.length;
  }

  isChecked(item) {
    return (
      this.listCode.findIndex((hrsCode) => {
        return hrsCode === item.ARRANGEMENT_NBR;
      }) > -1
    );
  }

  choose() {
    const listRemove = this.listCodeOld?.filter(
      (codeOld) => this.listCode?.findIndex((code) => code === codeOld) === -1
    );
    const data = {
      listSelected: this.selected,
      listRemove,
    };
    this.modalActive.close(data);
  }

  closeModal() {
    this.modalActive.close(false);
  }

  handleChangePageSize(event) {
    this.params.page = 0;
    this.params.size = event;
    this.limit = event;
    this.reload();
  }
}

