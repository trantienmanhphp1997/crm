import { Component, HostBinding, Injector, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import _ from 'lodash';
import { pdfDefaultOptions } from 'ngx-extended-pdf-viewer';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseComponent } from 'src/app/core/components/base.component';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-preview-modal',
  templateUrl: './preview-modal.component.html',
  styleUrls: ['./preview-modal.component.scss'],
})
export class PreviewModalComponent extends BaseComponent implements OnInit {
  @HostBinding('class.app-preview-modal') reportModal = true;
  @Input() data: any;
  title = "Preview file trình kí";
  url: string;
  constructor(
    injector: Injector,
    private modal: NgbActiveModal,
  ) {
    super(injector);
    pdfDefaultOptions.assetsFolder = 'bleeding-edge';
    this.isLoading = true;
  }


  ngOnInit(): void {
    if(this.data){
      this.url = URL.createObjectURL(this.data);
    }
    this.isLoading = false;
  }
  close() {
    this.modal.close();
  }
}