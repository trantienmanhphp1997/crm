import { Component, HostBinding, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-warning-authorization-modal',
  templateUrl: './warning-authorization-modal.component.html',
  styleUrls: ['./warning-authorization-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class WarningAuthorizationModalComponent implements OnInit {
  @HostBinding('warning-authorization-modal') classCreateCalendar = true;
  constructor(
    private dialogRef: MatDialogRef<WarningAuthorizationModalComponent>
  ) { 
    dialogRef.disableClose = true;
  }

  ngOnInit(): void {
  }

  openAuthModal(): void {
    this.dialogRef.close(true);
  }
}
