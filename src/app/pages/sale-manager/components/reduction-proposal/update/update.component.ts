import { AfterViewInit, Component, Injector, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Division, FunctionCode, functionUri, Scopes } from 'src/app/core/utils/common-constants';
import * as _ from 'lodash';
import { MatDialog } from '@angular/material/dialog';
import { ReductionProposalApi } from '../../../api/reduction-proposal.api';
import { CreateProposalComponent } from '../create-proposal/create-proposal.component';
import { forkJoin, of } from 'rxjs';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { ConfirmReductionComponent } from '../confirm-reduction/confirm-reduction.component';
import { FreeDiscountAnnexComponent } from '../annex/annex.component';
import { PreviewModalComponent } from '../dialog/preview-modal/preview-modal.component';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { CustomerApi } from 'src/app/pages/customer-360/apis';
import { CustomerDetailSmeApi } from 'src/app/pages/customer-360/apis/customer.api';
import { DatePipe } from '@angular/common';
import { Utils } from 'src/app/core/utils/utils';

@Component({
  selector: 'app-reduction-proposal-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss'],
  providers: [DatePipe]
})
export class UpdateReductionProposalComponent extends BaseComponent implements OnInit, OnChanges, AfterViewInit {

  @ViewChild(CreateProposalComponent) declareComponent: CreateProposalComponent;
  @ViewChild(ConfirmReductionComponent) confirmReductionComponent: ConfirmReductionComponent;
  @ViewChild(FreeDiscountAnnexComponent) freeDiscountAnnexComponent: FreeDiscountAnnexComponent;
  id;
  type: any;
  commonData = {
    divisions: []
  };
  customerCode = '';
  tabIndex = 0;
  customer = {
    existed: false,
    isKhdn: false,
    isNew: false,
    searchLabel: '',
    searchPlaceholder: '',
    searchLabel2: '',
    searchPlaceholder2: '',
    customerCode: 'code',
    customerName: 'name',
    taxCode: 'taxCode',
    segment: 'segment',
    relationship: 'relationship',
    dtttrr: 'dtttrr',
    piority: 'piority',
    classification: 'classification',
    toiCurrent: 'toiCurrent',
    nimCurrent: 'nimCurrent',
    scoreValue: 'scoreValue'
  };
  reductionProposalTranslate: any;
  info: any;
  isDeclareTab = false;
  isKhdn = false;
  existed = false;
  dataReduction: any;
  creditInformation: any;
  creditInformationAtMB: any;
  isRm: boolean;
  declareInfo = {
    searchStr: '',
    caseSuggest: 'FEE',
    scopeApply: 'ALL',
    toi: null,
    dtttrr: null,
    monthApply: null,
    monthConfirm: null,
    listData: [],
    branchCode: ''
  };
  updateInfo: any;
  reductionProposal: [];
  listLD: [];
  params;
  code: any;
  searchValue: any;
  productCode: any;

  constructor(
    injector: Injector,
    public dialog: MatDialog,
    private reductionProposalApi: ReductionProposalApi,
    private categoryService: CategoryService,
    private customerDetailSmeApi: CustomerDetailSmeApi,
    private datePipe: DatePipe,
    private customerApi: CustomerApi

  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REDUCTION_PROPOSAL}`);
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
    this.params = this.route.snapshot.queryParams;
    if (this.params) {
      this.id = this.params.reductionId;
      this.type = this.params.type;
    }
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.translate.get(['reductionProposal']).subscribe((result) => {
      this.reductionProposalTranslate = _.get(result, 'reductionProposal');
    });
    this.info = this.reductionProposalApi?.info?.value;
    this.info.type = Number(this.type);
    this.reductionProposalApi.info.next(this.info);
    this.searchValue = {
      customerCode: this.info?.customer?.customerCode,
      taxCode: this.info?.customer?.taxCode,
      idCard: '',
      blockCode: this.info?.customer?.customerTypeMerge,
      customerType: '',
      type: this.info.type
    };

    forkJoin([
      this.reductionProposalApi.detailReductionProposal(this.id),
      this.categoryService
        .getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined)))
    ]).subscribe(([reductionProposal, branchesOfUser]) => {

      this.freeDiscountAnnexComponent.form.patchValue(reductionProposal);
      // check RM is manage
      if (reductionProposal) {
        this.info.customer.isManage = this.checkBrand(reductionProposal?.branchCode);
      }
      this.isRm = _.isEmpty(branchesOfUser);
      this.updateInfo = reductionProposal;
      this.searchValue.blockCode = reductionProposal?.blockCode;
      this.searchValue.taxCode = reductionProposal?.taxCode;
      this.searchValue.type = reductionProposal?.type;
      this.freeDiscountAnnexComponent.loadDataByTaxCode(reductionProposal?.taxCode?.trim() || reductionProposal?.businessRegistrationNumber?.trim());
      this.freeDiscountAnnexComponent.getCreditInformation({
        identifiedNumber: reductionProposal?.businessRegistrationNumber
      });
      this.freeDiscountAnnexComponent.getDataCustomerRelation(reductionProposal?.customerCode, reductionProposal?.branchCodeCustomer);
    }, (e) => {
      this.isLoading = false;
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.code?.currentValue) {
      this.signSubmit();
    }
  }

  ngAfterViewInit() {

  }

  onTabChanged($event) {
    this.tabIndex = _.get($event, 'index');
  }

  backStep(): void {
    // check tabindex
    // if (this.tabIndex > 0) {
    //   this.isDeclareTab = false;
    //   this.tabIndex--;
    // } else {
    this.router.navigateByUrl(functionUri.reduction_proposal, { state: this.prop ? this.prop : this.state });
    // }
  }

  handlerDataReduction(data) {
    this.dataReduction = data?.customerInfo;
    this.reductionProposal = data?.reductionProposal?.listLoanInfoByLoanId;
    this.listLD = data?.listLD;
    const paramCreditInfo = {
      identifiedNumber: this.updateInfo?.businessRegistrationNumber
    };
    const customerCode = this.updateInfo?.customerCode;
    this.customerCode = customerCode;
    const api = [
      this.customerDetailSmeApi.getDataEarlyWarningModel(customerCode).pipe(catchError(() => of(undefined))),
    ];
    if(this.updateInfo?.blockCode != Division.INDIV) {
      api.push(this.customerApi.getCreditInformation(paramCreditInfo).pipe(catchError(() => of(undefined))));
    }
    forkJoin(api).subscribe(([creditInformation, creditInformationAtMB]) => {
      this.creditInformation = creditInformation;
      this.creditInformationAtMB = creditInformationAtMB;
      this.isLoading = false;
    }, (e) => {
      this.isLoading = false;
    });
  }

  confirm() {
    this.confirmReductionComponent.confirm();
  }

  saveFile() {
    return this.freeDiscountAnnexComponent.save();
  }

  update(backToList: boolean, type: string) {

    return new Promise<any>(async (resolve, reject) => {
      if (!this.isValid() || !this.freeDiscountAnnexComponent.compareDate(type)) {
        reject(false);
        return;
      }
      const fileList = await this.saveFile();
      const declare: any = this.info = this.reductionProposalApi?.info?.value;
      const annexValue = this.freeDiscountAnnexComponent ? this.freeDiscountAnnexComponent.form.getRawValue() : {};
      const proposalValue = this.declareComponent.form.getRawValue();
      const listData = this.declareComponent.listData;
      const selected = declare?.authorization ? declare?.authorization : [];
      // ---
      let reductionProposalDetailsDTOList = [];
      if (listData.length > 0) {
        reductionProposalDetailsDTOList = listData?.map(item => {
          item = item?.data;
          this.productCode = item?.codeInterest;
          return {
            planCode: proposalValue.scopeApply == 'OPTION_CODE' ? item.code : '',
            ldCode: proposalValue.scopeApply == 'LD_CODE' ? item.code : '',
            itemCode: item?.productCode,  // mã biểu lãi
            itemName: item?.name, // tên biểu lãi
            maximumCost: '',
            maximumCostOffer: '',
            minimumCost: '',
            minimumCostOffer: '',
            ratioCost: '',
            ratioCostOffer: '',
            type: proposalValue.caseSuggest == 'FEE' ? 0 : 1, // Loại---0 : Biểu phí, 1: Biểu lãi suất
            interestValue: item.loanRate, // Lãi suất cho vay
            interestUnit: item.currency, // Đơn vị tiền tệ
            interestType: item.name, // Biểu lãi suất
            interestReference: item.referenceInterestRate, // Lãi suất tham chiếu (%)
            interestProposedValue: item.loanSuggest, // Lãi suất cho vay đề xuất
            interestProposedAmplitude: item.marginSuggest, // Biên độ đề xuất (%)
            interestMaxReduction: item.maximumMarginReduction,// Mức giảm biên độ tối đa (%
            interestLoanPeriod: item.period, // Kỳ hạn vay
            interestAmplitude: item.baseMargin, // Biên độ (%)
            interestAdjustPeriod: item?.adjustmentPeriod, // Kỳ điểu chỉnh lãi suất
            reductionRate: item.reductPercent, // tỷ lệ giảm
            id: '',
            minInterestRate: item.minInterestRate,
            minInterestRateOffer: item.rateMinimum
          };
        });
      }
      // Mapping thông tin giảm phí
      let errorCount = 0;
      let lowerThanZero = 0;
      if (this.declareComponent.listFeeData.length > 0) {
        console.log('listFeeData : ', this.declareComponent.listFeeData);
        reductionProposalDetailsDTOList = this.declareComponent.listFeeData?.map(item => {
          item = item.data;
          const rateCost = item?.rateCost || 0;
          const rateMin = item?.rateMin || 0;
          const rateMax = item?.rateMax || 0;
          errorCount += rateCost === 0 && rateMin === 0 && rateMax === 0 ? 1 : 0;
          lowerThanZero += Math.min(rateCost, rateMin, rateMax) < 0 ? 1 : 0;


          return {
            planCode: proposalValue.scopeApply == 'OPTION_CODE' ? item.code : '',
            ldCode: proposalValue.scopeApply == 'LD_CODE' ? item.code : '',
            mdCode: proposalValue.scopeApply == 'MD_CODE' ? item.code : '',
            itemCode: item?.chargeCode,  // mã biểu lãi
            itemName: item?.descriptionVn, // tên biểu lãi
            interestUnit: item?.currency, // Đơn vị tiền tệ
            interestType: item?.chargeType, // Loại phí
            ratioCost: item?.chgAmtFrom || !isNaN(item?.chgAmtFrom) ? Number(item?.chgAmtFrom) : null, // Mức phí biểu theo quy định
            minimumCost: item?.minCharge || !isNaN(item?.minCharge) ? Number(item?.minCharge) : null, // Mức phí tối thiểu theo quy định
            maximumCost: item?.maxCharge || !isNaN(item?.maxCharge) ? Number(item?.maxCharge) : null, // Mức phí tối đa theo quy định
            ratioCostOffer: item?.suggestCost || !isNaN(item?.suggestCost) ? Number(item?.suggestCost.replaceAll(',', '')) : null, // Mức phí đề xuất
            minimumCostOffer: item?.suggestMin || !isNaN(item?.suggestMin) ? Number(item?.suggestMin.replaceAll(',', '')) : null, // Mức phí tối thiểu đề xuất
            maximumCostOffer: item?.suggestMax || !isNaN(item?.suggestMax) ? Number(item?.suggestMax.replaceAll(',', '')) : null, // Mức phí tối đa đề xuất
            calcType: item?.calcType,
            chargeUnit: item?.chargeUnit, // Đơn vị phí tối thiểu, tối đa
            valueRefDoc: item?.valueRefDoc, // Đơn vị mức phí
            reductionRate: item?.rateCost ? Number(item?.rateCost) : 0, // tỷ lệ giảm
            type: proposalValue.caseSuggest == 'FEE' ? 0 : 1, // Loại---0 : Biểu phí, 1: Biểu lãi suất
            chargeGroup: item?.chargeGroup,
            sumChargeCode: item?.sumChargeCode // Mã biểu phí của phí cha
          };
        });
      }

      if (errorCount > 0) {
        this.messageService.warn('Vui lòng nhập thông tin đề xuất giảm');
        reject(false);
        return;
      }

      if (lowerThanZero > 0) {
        this.messageService.warn('Tỉ lệ giảm phải lớn hơn hoặc bằng 0');
        reject(false);
        return;
      }

      let taxCode;
      if (this.updateInfo.blockCode === Division.INDIV && this.info.type === 0) {
        taxCode = declare?.customer?.idCard;
      } else {
        taxCode = declare?.customer?.taxCode;
      }

      // scopeApply
      let scopeApplyData = [];
      switch (proposalValue.scopeApply) {
        case 'LD_CODE':
          scopeApplyData = this.declareComponent.ldCodeData;
          break;
        case 'OPTION_CODE':
          scopeApplyData = this.declareComponent.optionCodeData;
          break;
        case 'MD_CODE':
          scopeApplyData = this.declareComponent.mdCodeData;
          break;
        default:
          if (this.info.type === 1) {
            scopeApplyData = this.declareComponent.interestData;
          } else {
            scopeApplyData = this.declareComponent.feeData;
          }
      }
      const scopeApply = JSON.stringify(scopeApplyData);
      // scopeType
      const scopeType = this.declareComponent.scopeApplys.find(i => i.code === proposalValue.scopeApply).typeNum;

      const reductionProposalAuthorizationDTOList = selected.map(item => {
        if (item && item.childs) {
          delete item.childs;
        }
        return item;
      });

      const payload = {
        blockCode: this.updateInfo?.blockCode,
        businessActivities: annexValue?.businessActivities?.trim(),
        code: this.updateInfo?.code,
        competitiveness: annexValue?.competitiveness?.trim(),
        customerCode: declare?.customer?.customerCode,
        dtttrrConfirm: proposalValue.dtttrr ? Number(proposalValue.dtttrr.toString().replace(',', '.')) : proposalValue.dtttrr,
        monthApply: proposalValue.monthApply,
        monthConfirm: proposalValue.monthConfirm,
        moDocId: '',
        nimConfirm: proposalValue.nimConfirm,
        nimExisting: proposalValue.nimCurrent,
        planInformation: annexValue.planInformation?.trim(), // Thông tin phương án
        resultBusiness: annexValue?.resultBusiness?.trim(),
        scopeApply,
        scopeType,
        toiConfirm: proposalValue.toi,
        taxCode,
        leadCode: declare?.customer?.leadCode,
        type: proposalValue.caseSuggest == 'FEE' ? 0 : 1,
        reductionProposalDetailsDTOList,
        reductionProposalDetailsApprovedDTOList: [],
        reductionProposalTOIDTO: {
          ckhBqNim: declare?.toi?.coKyHanBqUnit || 0,
          ckhBqSd: declare?.toi?.coKyHanBq || 0,
          dunoBqNim: declare?.toi?.duNoNganHanBqUnit || 0,
          dunoBqSd: declare?.toi?.duNoNganHanBq || 0,
          dunoTdhBqNim: declare?.toi?.duNoTdhBqUnit || 0,
          dunoTdhBqSd: declare?.toi?.duNoTdhBq || 0,
          kkhBqNim: declare?.toi?.khongKyHanBqUnit || 0,
          kkhBqSd: declare?.toi?.khongKyHanBq || 0,
          tblDsSd: declare?.toi?.thuBl || 0,
          tblNim: declare?.toi?.thuBlUnit || 0,
          tfxDsSd: declare?.toi?.thuFx || 0,
          tfxMpqd: declare?.toi?.thuFxUnit || 0,
          thuBancaDsSd: declare?.toi?.thuBanca || 0,
          thuBanCaMpqd: 1,
          thuKhacDsSd: declare?.toi?.thuKhac || 0,
          thuKhacMpqd: 1,
          tttqtDsSd: declare?.toi?.thuTTQT || 0,
          tttqtMpqd: declare?.toi?.thuTTQTUnit || 0,
          ckhBqHq: declare?.toi?.ckhBqHq || 0,
          dunoBqHq: 0,
          dunoTdhBqHq: 0,
          kkhBqHq: 0,
          tblHq: 0,
          tfxHq: 0,
          thuBanCaHq: 0,
          thuKhacHq: 0,
          tttqtHq: 0,
          dttck: 0,
          tck: 0
        },
        reductionProposalFileDTOList: fileList,
        reductionProposalAuthorizationDTOList,
        customerName: declare?.customer?.customerName,
        approverName: '',
        approverTitle: '',
        status: '',
        createdDate: '',
        rmCode: this.currUser?.rmCode,
        branchCode: this.currUser?.branch,
        branchCodeCustomer: this.info.customer?.branchCode,
        financialSituation: annexValue?.financialSituation?.trim(),
        customersPolicyApplied: '',
        proposalReason: '',
        customerProductList: '',
        toiExisting: declare?.customer?.toiCurrent,
        dtttrrExisting: declare?.customer?.dtttrr,
        approvedAuthorization: '',
        customerType: declare?.customer?.customerTypeCode,
        customerSegment: declare?.customer?.segmentCode,
        businessRegistrationNumber: declare?.customer?.businessRegistrationNumber,
        classification: declare?.customer?.classification,
        creditRanking: declare?.customer?.scoreValue,
        jsonRelationTCTD: JSON.stringify(this.freeDiscountAnnexComponent.creditInformation),
        yearsOfRelationship: declare?.customer?.mbRelationship,
        jsonRelationMB: JSON.stringify(this.freeDiscountAnnexComponent.relationshipHT),
        industryActivity: '',
        businessRegistrationLocation: '',
        startDate: '',
        endDate: '',
        customerStructure: declare?.customer?.customerCreditType, // Cơ cấu khách hàng
        policyApplied: annexValue.policyApplied, // Chính sách đang áp dụng với khách hàng
        proposedBasis: annexValue.proposedBasis, // Cơ sở đề xuất (uy tín khách hàng, lợi ích mang lại...)
        jsonDebtOther: JSON.stringify(this.updateInfo.blockCode === Division.INDIV ? this.freeDiscountAnnexComponent.duNoKHCN : this.freeDiscountAnnexComponent.camKetNgoaiBangData), // Thông tin dư nợ thẻ, trái phiếu và cam kết ngoại bảng
        debtCaution: this.freeDiscountAnnexComponent.noChuY ? 1 : 0, // Nợ chú ý trong vòng 12 tháng gần nhất
        jsonImportExportSales: JSON.stringify(this.freeDiscountAnnexComponent.doanhSoXNKData),
        identifiedIssueDate: this.declareComponent.info.customer?.businessInfo?.identifiedIssueDate,
        identifiedIssueArea: this.declareComponent.info.customer?.businessInfo?.identifiedIssueArea,
        job: this.declareComponent.info.customer?.jobInfo?.job,
        isRmManager: this.info.customer.isManage,
        interestType: this.info.type === 1 ? this.declareComponent?.form.controls.typeInterest.value : null,
        productCode: this.mappingPayload()
      };

      if (this.info.customer?.customerTypeMerge === Division.CIB
        && !this.info.customer.isManage
      ) {
        payload.toiExisting = 0;
        payload.dtttrrExisting = 0;
      }

      // ---
      this.isLoading = true;
      this.reductionProposalApi.update(this.updateInfo.id, payload).subscribe(
        () => {
          this.isLoading = false;
          this.messageService.success('Cập nhật thành công');
          if (backToList) {
            setTimeout(() => {
              this.router.navigateByUrl(functionUri.reduction_proposal, { state: this.prop ? this.prop : this.state });
            }, 2000);
          }
          resolve(true);
        },
        (e) => {
          if (e?.error) {
            this.messageService.error(e?.error?.description);
          } else {
            this.messageService.error(_.get(this.notificationMessage, 'error'));
          }
          this.isLoading = false;
          reject(false);
        });
    });
  }

  mappingPayload() {
    if(this.info.type === 1){
      return this.declareComponent?.form?.controls?.scopeApply?.value === 'LD_CODE' ? this.productCode :
        this.declareComponent?.typeInterest?.find(item => item.id === this.declareComponent?.form.controls.typeInterest.value)?.code
    }
    else {
      return;
    }
  }

  next() {
    if (!this.isValid()) {
      return;
    }
    this.update(false, 'update').then(() => {
      this.tabIndex++;
    });
  }

  previewPdf() {
    this.isLoading = true;
    const customerInfo = this.info.customer;
    const annexValue = this.freeDiscountAnnexComponent ? this.freeDiscountAnnexComponent.form.getRawValue() : {};
    const proposalValue = this.declareComponent.form.getRawValue();
    const declare: any = this.info = this.reductionProposalApi?.info?.value;
    const listData = this.declareComponent.listData || [];
    const listFeeData = this.declareComponent.listFeeData || [];
    let reductionProposalDetailsDTOList = [];
    const planDetailList = [];
    let ldDetailList = [];
    if (listData?.length > 0) {
      reductionProposalDetailsDTOList = listData?.map(item => {
        item = item?.data;
        return {
          minInterestRate: item?.minInterestRate,
          minInterestRateOffer: item?.rateMinimum,
          interestValue: item?.loanRate, // Lãi suất cho vay
          interestUnit: item?.currency, // Đơn vị tiền tệ
          interestType: item?.name, // Biểu lãi suất
          interestReference: item?.referenceInterestRate, // Lãi suất tham chiếu (%)
          interestProposedValue: item?.loanSuggest, // Lãi suất cho vay đề xuất
          interestProposedAmplitude: item?.marginSuggest, // Biên độ đề xuất (%)
          interestMaxReduction: item?.maximumMarginReduction,// Mức giảm biên độ tối đa (%)
          interestLoanPeriod: item?.period, // Kỳ hạn vay
          interestAmplitude: item?.baseMargin, // Biên độ (%)
          interestAdjustPeriod: item?.adjustmentPeriod, // Kỳ điểu chỉnh lãi suất
          reductionRate: item?.reductPercent, // tỷ lệ giảm
          planCode: proposalValue.scopeApply == 'OPTION_CODE' ? item.code : '',
          mdCode: proposalValue.scopeApply == 'MD_CODE' ? item.code : '',
          ldCode: proposalValue.scopeApply == 'LD_CODE' ? item.code : '',
          type: proposalValue.caseSuggest == 'FEE' ? 0 : 1 // Loại---0 : Biểu phí, 1: Biểu lãi suất
        };
      });
    }
    if (listFeeData?.length > 0) {
      reductionProposalDetailsDTOList = listFeeData?.map(item => {
        item = item?.data;
        return {
          mdCode: proposalValue.scopeApply == 'MD_CODE' ? item.code : '',
          planCode: proposalValue.scopeApply == 'OPTION_CODE' ? item.code : '',
          ldCode: proposalValue.scopeApply == 'LD_CODE' ? item.code : '',
          itemCode: item?.currency ? item?.productCode.replace(item?.currency, '') : item?.productCode,  // mã biểu lãi
          itemName: item?.descriptionVn, // tên biểu lãi
          interestUnit: item?.currency, // Đơn vị tiền tệ
          interestType: item?.chargeType, // Loại phí
          ratioCost: Utils.isNotNull(item?.chgAmtFrom) ? Number(item?.chgAmtFrom) : null, // Mức phí biểu theo quy định
          minimumCost: Utils.isNotNull(item?.minCharge) ? Number(item?.minCharge) : null, // Mức phí tối thiểu theo quy định
          maximumCost: Utils.isNotNull(item?.maxCharge) ? Number(item?.maxCharge) : null, // Mức phí tối đa theo quy định
          ratioCostOffer: Utils.isNotNull(item?.suggestCost) ? Number(item?.suggestCost.replaceAll(',', '')) : null, // Mức phí đề xuất
          minimumCostOffer: Utils.isNotNull(item?.suggestMin) ? Number(item?.suggestMin.replaceAll(',', '')) : null, // Mức phí tối thiểu đề xuất
          maximumCostOffer: Utils.isNotNull(item?.suggestMax) ? Number(item?.suggestMax.replaceAll(',', '')) : null, // Mức phí tối đa đề xuất
          calcType: item?.calcType,
          chargeUnit: item?.chargeUnit, // Đơn vị phí tối thiểu, tối đa
          valueRefDoc: item?.valueRefDoc, // Đơn vị mức phí
          reductionRate: Utils.isNotNull(item?.rateCost) ? Number(item?.rateCost) : null, // tỷ lệ giảm
          type: proposalValue.caseSuggest == 'FEE' ? 0 : 1
        };
      });
    }

    // Phu luc : Thong tin phuong an trinh dieu chinh

    listData?.forEach(item => {
      const dataDetail = _.find(this.reductionProposal, (e) => e.loanId === item?.data?.optionCode || e.loanId === item?.data?.code);
      if (dataDetail) {
        const detailMapping = {
          optionCode: dataDetail?.loanId,
          createdDate: dataDetail?.ngayTaoPhuongAn,
          optionType: dataDetail?.loaiPhuongAn,
          listHanMuc: dataDetail?.listHanMuc
        };
        planDetailList.push(detailMapping);
      }
    });

    // Phu luc : Danh sach LD Trinh dieu chinh
    const listLDCode = this.declareComponent?.ldCodeData?.map(item => {
      return item.ldCode || item.SOKU;
    });

    if (listLDCode) {
      ldDetailList = _.filter(
        this.listLD,
        (item) => listLDCode.includes(item.SOKU)
      );
    }

    let scopeApplyData = [];
    switch (proposalValue.scopeApply) {
      case 'LD_CODE':
        scopeApplyData = ldDetailList;
        break;
      case 'OPTION_CODE':
        scopeApplyData = this.declareComponent.optionCodeData;
        break;
      case 'MD_CODE':
        scopeApplyData = this.declareComponent.mdCodeData;
        break;
      default:
        if (this.info.type === 1) {
          scopeApplyData = this.declareComponent.interestData;
        } else {
          scopeApplyData = this.declareComponent.feeData;
        }
    }
    const scopeApply = JSON.stringify(scopeApplyData);
    // scopeType
    const scopeType = this.declareComponent.scopeApplys.find(i => i.code === proposalValue.scopeApply).typeNum;
    // author
    const selected = declare?.authorization ? declare?.authorization : [];
    const reductionProposalAuthorizationDTOList = selected.map(item => {
      if (item && item.childs) {
        delete item.childs;
      }
      return item;
    });

    const body = {
      interestType: this.declareComponent?.form.controls.typeInterest.value,
      blockCode: this.updateInfo?.blockCode,
      branchCode: this.currUser?.branch || '',
      businessActivities: annexValue?.businessActivities?.trim(),
      code: this.updateInfo?.code,
      competitiveness: annexValue?.competitiveness?.trim(),
      customerCode: customerInfo?.customerCode,
      customerName: customerInfo?.customerName,
      monthApply: proposalValue.monthApply,
      monthConfirm: proposalValue.monthConfirm,
      planInformation: annexValue.planInformation?.trim(),
      resultBusiness: annexValue?.resultBusiness?.trim(),
      scopeApply,
      scopeType,
      toiConfirm: proposalValue.toi,
      taxCode: declare?.customer?.taxCode ? declare?.customer?.taxCode : declare?.customer?.businessRegistrationNumber,
      leadCode: declare?.customer?.leadCode,
      type: proposalValue.caseSuggest == 'FEE' ? 0 : 1,
      dtttrrConfirm: proposalValue.dtttrr,
      nimConfirm: proposalValue.nimConfirm,
      nimExisting: proposalValue.nimCurrent,
      reductionProposalTOIDTO: {
        ckhBqNim: declare?.toi?.coKyHanBqUnit || 0,
        ckhBqSd: declare?.toi?.coKyHanBq || 0,
        dunoBqNim: declare?.toi?.duNoNganHanBqUnit || 0,
        dunoBqSd: declare?.toi?.duNoNganHanBq || 0,
        dunoTdhBqNim: declare?.toi?.duNoTdhBqUnit || 0,
        dunoTdhBqSd: declare?.toi?.duNoTdhBq || 0,
        kkhBqNim: declare?.toi?.khongKyHanBqUnit || 0,
        kkhBqSd: declare?.toi?.khongKyHanBq || 0,
        tblDsSd: declare?.toi?.thuBl || 0,
        tblNim: declare?.toi?.thuBlUnit || 0,
        tfxDsSd: declare?.toi?.thuFx || 0,
        tfxMpqd: declare?.toi?.thuFxUnit || 0,
        thuBancaDsSd: declare?.toi?.thuBanca || 0,
        thuBanCaMpqd: 1,
        thuKhacDsSd: declare?.toi?.thuKhac || 0,
        thuKhacMpqd: 1,
        tttqtDsSd: declare?.toi?.thuTTQT || 0,
        tttqtMpqd: declare?.toi?.thuTTQTUnit || 0,
      },
      customerType: declare?.customer?.customerTypeCode,
      reductionProposalDetailsDTOList,
      financialSituation: annexValue?.financialSituation?.trim(), // Tình hình tài chính (INDIV)
      customersPolicyApplied: 'Chính sách đang áp dụng với khách hàng (INDIV)', // Chính sách đang áp dụng với khách hàng (INDIV)
      proposalReason: 'Cơ sở đề xuất (INDIV)', // Cơ sở đề xuất (INDIV)
      customerProductList: 'Các sản phẩm khác khách hàng đang sử dụng (INDIV)', // Các sản phẩm khác khách hàng đang sử dụng (INDIV)
      toiExisting: declare?.customer?.toiCurrent,// declare?.customer?.toiCurrent? Number(Number(declare?.customer?.toiCurrent)* 100).toFixed(2):"", //TOI hien huu
      dtttrrExisting: declare?.customer?.dtttrr,
      approvedAuthorization: proposalValue?.lastApprove ? proposalValue?.lastApproveTitle + ' - ' + proposalValue?.lastApprove : '', // Thẩm quyền phê duyệt
      businessRegistrationNumber: customerInfo?.taxCode || customerInfo?.businessRegistrationNumber, // Số DKKD
      customerSegment: customerInfo?.segment, // Phân khúc
      classification: declare?.customer?.classification, // Đối tượng Khách hàng
      creditRanking: declare?.customer?.scoreValue || '', // Xếp hạng tín dụng

      jsonRelationTCTD: JSON.stringify(this.freeDiscountAnnexComponent.creditInformation),
      yearsOfRelationship: declare?.customer?.mbRelationship,
      jsonRelationMB: JSON.stringify(this.freeDiscountAnnexComponent.relationshipHT),
      customerStructure: declare?.customer?.customerCreditType == 'Phi tín dụng' ? 'NO_CREDIT' : 'CREDIT', // Cơ cấu khách hàng
      policyApplied: annexValue.policyApplied, // Chính sách đang áp dụng với khách hàng
      proposedBasis: annexValue.proposedBasis, // Cơ sở đề xuất (uy tín khách hàng, lợi ích mang lại...)
      jsonDebtOther: JSON.stringify(this.updateInfo.blockCode === Division.INDIV ? this.freeDiscountAnnexComponent.duNoKHCN : this.freeDiscountAnnexComponent.camKetNgoaiBangData), // Thông tin dư nợ thẻ, trái phiếu và cam kết ngoại bảng
      debtCaution: this.freeDiscountAnnexComponent.noChuY ? 1 : 0, // Nợ chú ý trong vòng 12 tháng gần nhất
      jsonImportExportSales: JSON.stringify(this.freeDiscountAnnexComponent.doanhSoXNKData),
      rmCode: this.currUser?.code,
      isRmManager: this.info?.customer?.isManage,
      identifiedIssueDate: this.declareComponent.info.customer?.businessInfo?.identifiedIssueDate,
      identifiedIssueArea: this.declareComponent.info.customer?.businessInfo?.identifiedIssueArea,
      job: this.declareComponent.info.customer?.jobInfo?.job,
      reductionProposalAuthorizationDTOList
    };
    this.reductionProposalApi.previewReduction(`preview`, 'pdf', body).then(
      (response) => {
        const blob = new Blob([response], { type: 'application/octet-stream' });
        const modal = this.modalService.open(PreviewModalComponent, { windowClass: 'tree__report-modal' });
        modal.componentInstance.data = blob;
        modal.componentInstance.extendUrl = '';
        this.isLoading = false;
      },
      () => {
        this.messageService.error('Không thể tải file');
        this.isLoading = false;
      }
    );
  }

  add2Param(value1, value2) {
    const val1 = value1 || 0;
    const val2 = value2 || 0;
    return val1 + val2;
  }

  multiplication(val1, val2, division100) {
    if (!Utils.isNull(val1) && !Utils.isNull(val2)) {
      if (division100) {
        val2 = Number(val2) / 100;
      }
      return Number((Number(val1) * val2));
    }
  }


  async signSubmit() {
    const info: any = this.reductionProposalApi.info.value;
    if (_.isEmpty(info.authorization)) {
      this.messageService.warn(this.notificationMessage.process_proposal_author_warning);
    } else {
      const checkAuthDataChange = await this.declareComponent.checkAuthDataChange(true);
      if (checkAuthDataChange) {
        return;
      }
      this.update(false, 'signSubmit').then(() => {
        this.isLoading = true;
        const params: any = {};
        params.code = this.updateInfo.code;
        params.customerType = info?.customer?.customerTypeCode;
        params.customerName = info?.customer?.customerName;
        params.toiExisting = info?.customer?.toiCurrent;
        params.dtttrrExisting = info?.customer?.dtttrr;
        params.yearsOfRelationship = info?.customer?.mbRelationship;
        params.businessRegistrationNumber = info?.customer?.businessRegistrationNumber;
        params.customerSegment = info?.customer?.segmentCode;
        params.classification = info?.customer?.classification;
        params.creditRanking = info?.customer?.scoreValue;
        params.jsonRelationTCTD = JSON.stringify(this.freeDiscountAnnexComponent.creditInformation);
        params.jsonRelationMB = JSON.stringify(this.freeDiscountAnnexComponent.relationshipHT);
        const customerCICDTOS = this.freeDiscountAnnexComponent.creditInformation?.customerCICDTOS;
        if (Utils.isNotNull(customerCICDTOS) && customerCICDTOS.length > 0) {
          params.dataDate = new Date(customerCICDTOS[0]?.dsnapShotDt);
        } else {
          params.dataDate = null;
        }
        this.reductionProposalApi.processProposal(params).subscribe((res) => {
          const description = res?.description;
          this.isLoading = false;
          if (res?.status === 200) {
            this.messageService.success(this.notificationMessage.process_proposal_success);
            this.isLoading = false;
            this.router.navigateByUrl(functionUri.reduction_proposal, { state: this.prop ? this.prop : this.state });
            setTimeout(() => {
              this.backStep();
            }, 5000);
          } else {
            this.messageService.error(description);
            this.isLoading = false;
          }
        }, () => {
          this.messageService.error(this.notificationMessage.process_proposal_error);
          this.isLoading = false;
        });
      }).catch(err => {
      });
    }
  }

  isValid() {
    let errCount = 0;
    let errMessage = '';
    // xóa điều kiện validate với KH cũ{
    const declare: any = this.info = this.reductionProposalApi?.info?.value;
    if (declare?.customer?.customerTypeCode === 'OLD') {
      this.declareComponent.form.get('dtttrr').clearValidators();
      this.declareComponent.form.get('toi').clearValidators();
      this.declareComponent.form.get('dtttrr').updateValueAndValidity({ emitEvent: false });
      this.declareComponent.form.get('toi').updateValueAndValidity({ emitEvent: false });
    }
    if (this.info.type != 0) {
      if (!this.declareComponent.form.valid) {
        validateAllFormFields(this.declareComponent.form);
        errCount++;
      }

      if (this.declareComponent.listData.length === 0) {
        errMessage += 'Chưa nhập thông tin giảm lãi suất. ';
        errCount++;
      } else {
        const fieldErr = [];
        this.declareComponent.listData.map(item => {
          if ((item.data?.reductPercent === null || Number(item.data?.reductPercent)) < 0 && !fieldErr.includes('reductPercent')) {
            errMessage += 'Tỉ lệ giảm phải lớn hơn hoặc bằng 0. ';
            fieldErr.push('reductPercent');
            errCount++;
          }
          if(item.data?.maximumMarginReduction){
            const baseMargin = item.data?.baseMargin || 0;
            const maximumMarginReduction = item.data?.maximumMarginReduction || 0;
            const margin = (baseMargin - maximumMarginReduction).toFixed(2) || 0;
            const marginSuggestl = item.data?.marginSuggest || 0;
            if (Number(marginSuggestl) < +margin && !fieldErr.includes('margin')) {
              errMessage += 'Biên độ đề xuất phải lớn hơn hoặc bằng mức (Biên độ theo biểu - Mức giảm biên tối đa) ';
              fieldErr.push('margin');
              errCount++;
            }
          }
          if (item.data?.marginSuggest === null && !fieldErr.includes('marginSuggest')) {
            errMessage += 'Nhập đầy đủ biên độ đề xuất. ';
            fieldErr.push('marginSuggest');
            errCount++;
          }
        });
      }
    } else {
      if (!this.declareComponent.form.valid) {
        validateAllFormFields(this.declareComponent.form);
        errCount++;
      }
      if (this.declareComponent.listFeeData.length === 0) {
        errMessage += 'Chưa nhập thông tin giảm phí.';
        errCount++;
      } else {

      }
    }
    if (Utils.isStringNotEmpty(errMessage)) {
      this.messageService.warn(errMessage);
    }
    return errCount === 0;
  }

  changeLoading(isLoading = false): void {
    // this.isLoading = isLoading;
    this.ref.detectChanges();
  }

  checkBrand(branchCode) {
    if (branchCode !== this.currUser.branch && this.info.customer?.existed) {
      if (this.info.type === 1) {
        this.messageService.warn('Khách hàng không thuộc chi nhánh quản lý');
      }
      return false;
    }
    return true;
  }
}

