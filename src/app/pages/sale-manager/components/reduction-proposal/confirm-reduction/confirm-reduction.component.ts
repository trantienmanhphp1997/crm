import { Location } from '@angular/common';
import { ChangeDetectorRef, Component, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { forkJoin, Subscription } from 'rxjs';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CalendarAlertService } from 'src/app/core/components/calendar-alert/calendar-alert.service';
import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';
import { AppFunction } from 'src/app/core/interfaces/app-function.interface';
import { CommonCategoryService } from 'src/app/core/services/common-category.service';
import { CommunicateService } from 'src/app/core/services/communicate.service';
import { ConfirmService } from 'src/app/core/services/confirm.service';
import { SessionService } from 'src/app/core/services/session.service';
import { CommonCategory, Division, ReductionProposalStatus, ScreenType } from 'src/app/core/utils/common-constants';
import { CustomerDetailSmeApi } from 'src/app/pages/customer-360/apis/customer.api';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { ReductionProposalApi } from '../../../api/reduction-proposal.api';
import { ToiConfirmModalComponent } from '../dialog/toi-confirm-modal/toi-confirm-modal.component';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ACTION } from './difine';
import { InterestModalComponent } from '../dialog/select-interest-modal/select-interest-modal.component';
import { OptionCodeModalComponent } from '../dialog/select-optioncode-modal/select-optioncode-modal.component';
import { Utils } from 'src/app/core/utils/utils';
import { TreeTable } from 'primeng/treetable';

@Component({
  selector: 'app-confirm-reduction',
  templateUrl: './confirm-reduction.component.html',
  styleUrls: ['./confirm-reduction.component.scss']
})

export class ConfirmReductionComponent extends BaseComponent implements OnInit {
  sales: any;
  rmAccepted: any;
  products: any;
  hasErr = 0;
  selectedMonth: any;
  month = [];
  data: any;
  isKhdn = true;
  redutionProposol: any = {
    dtttrrConfirm: '',
    monthConfirm: '',
    toiExisting: '',
    monthApply: '',
    reductionProposalDetailsDTOList: []
  };
  info: any;
  id: any;
  action: any;
  code: any;
  modelValue = {
    toi: '',
    dtttrr: ''
  }
  queryParamMap: {};
  isApproval: boolean;
  isRm: boolean;
  listSegment = [];
  rmConfirm: any;
  form = this.fb.group(
    {
      rmConfirm: ['', Validators.required]
    }
  );
  @ViewChild('treeTableInterest') treeTableInterest: TreeTable;
  optionCodeData: any;
  public prop: any;
  public fields: any;
  public labelTitle: any;
  public notificationMessage: any;
  public confirmMessage: any;
  public screenType: { create: ScreenType; update: ScreenType; detail: ScreenType; };
  public messages: any;
  public objFunction: AppFunction;
  public maxExportExcel: number;
  public isLoading: boolean;
  public currUser: any;
  public isDownLoading: boolean;
  public now: Date;
  protected configModal: NgbModalConfig;
  protected translate: TranslateService;
  protected confirmService: ConfirmService;
  protected location: Location;
  protected communicateService: CommunicateService;
  protected sessionService: SessionService;
  protected ref: ChangeDetectorRef;
  protected commonService: CommonCategoryService;
  protected state: any;
  protected calendarAlertService: CalendarAlertService;
  subscription: Subscription;
  subscriptions: Subscription[];
  table: DatatableComponent;
  appRightContent: boolean;
  existed: boolean;
  monthApproval: any;
  timeApproval: any;
  isManage: boolean =  true;
  displayConfirmInfo: boolean;
  reductionProposalDetailsDTOList : any;
  monthConfirm : any;
  dtttrrConfirmTrd : any;
  dtttrrConfirmApprovedTrd : any;
  nimConfirmApproved : any;
  nimExistingApproved : any;
  errorNimConfirm : any;
  errorMonthApproval : any;
  errorCalReduction : any;
  toiAlready = false;
  typeInterestDetail: any;
  rateInterestError: any;
  marginError: any;
  @Input() set dataReduction(data) {
    if (!data) {
      return;
    }
    if(data.monthConfirm){
      for (let i = 1;  i <= data.monthConfirm; i++) {
        this.month.push({ name: i.toString(), code: i });
      }
    }
    this.monthConfirm = data?.monthConfirmApproved?data?.monthConfirmApproved:data?.monthConfirm?data?.monthConfirm: 0
    this.data = data;


  }
  scopeApplys = [
    { code: '0', displayName: 'Tất cả các phương án' },
    { code: '1', displayName: 'Mã phương án hạn mức' },
    { code: '2', displayName: 'Mã LD' },
    { code: '4', displayName: 'Mã MD' },
    { code: '3', displayName: 'Chưa xác định mã phương án' },
  ];
  toiChangedApprove;
  constructor(
    injector: Injector,
    private reductionProposalApi: ReductionProposalApi,
    public dialog: MatDialog,
    private customerDetailSmeApi: CustomerDetailSmeApi,
    private reductionProposal: ReductionProposalApi,
    private categoryService: CategoryService,
    protected messageService: NotifyMessageService,
    private modal: NgbModal,
    private dtc: ChangeDetectorRef,
  ) {
    super(injector);
    this.selectedMonth = '1'
    this.queryParamMap = this.route.snapshot.queryParamMap;
    this.info = this.reductionProposal?.info?.value;
  }

  ngOnInit() {

    this.id = this.queryParamMap['params']['id'] || '';
    this.isApproval = this.queryParamMap['params']['action'] == ACTION.approved;
    this.action = this.queryParamMap['params']['action'];
    console.log(this.action);


    this.code = this.queryParamMap['params']['code'];
    this.isRm = this.queryParamMap['params']['isRm'];

    forkJoin([this.reductionProposalApi.getRedutionProposol(this.id), this.categoryService.getCommonCategory(CommonCategory.SEGMENT_CUSTOMER_CONFIG)]).subscribe(([redutionProposol, categorySegment]) => {
      if (categorySegment.content) {
        Object.keys(categorySegment.content).forEach((key) => {
          this.listSegment.push({ code: categorySegment.content[key].code, name: categorySegment.content[key].name })
        });
      }

      this.redutionProposol = redutionProposol;
      this.typeInterestDetail = this.redutionProposol?.interestType;
      this.nimConfirmApproved = this.redutionProposol?.nimConfirmApproved || this.redutionProposol?.nimConfirm;
      this.nimExistingApproved = this.redutionProposol?.nimExisting;
      this.existed = this.redutionProposol?.customerCode ? true : false;
      this.isManage = this.currUser?.branch === this.redutionProposol?.branchCodeCustomer;

      this.optionCodeData = redutionProposol['scopeApplyApproved'] ? JSON.parse(redutionProposol['scopeApplyApproved']) : '';
      if (this.redutionProposol.isConfirmRm) {
        this.form.controls['rmConfirm'].setValue(this.redutionProposol.isConfirmRm == 1);
      }
      const toi = this.redutionProposol.reductionProposalTOIDTO;
      this.initTOI(toi);
      this.isKhdn =  ['SME', 'CIB', 'FI', 'DVC'].includes(this.redutionProposol?.blockCode);
      if(this.redutionProposol?.reductionProposalDetailsApprovedDTOList?.length > 0){
        this.reductionProposalDetailsDTOList = this.redutionProposol.reductionProposalDetailsApprovedDTOList;
        this.reductionProposalDetailsDTOList?.forEach(element => {
          let rpd = this.redutionProposol?.reductionProposalDetailsDTOList?.find(i => i.itemCode === element.itemCode);
          element["mdCode"] = rpd?.mdCode;
          element["ldCode"] = rpd?.ldCode;
          element["planCode"] = rpd?.planCode;
          element["interestType"] = rpd?.interestType;
        });
      }
      else
        this.reductionProposalDetailsDTOList = this.redutionProposol.reductionProposalDetailsDTOList;
      this.reductionProposalDetailsDTOList.forEach(item => {
        if(!('interestApprovedAmplitude' in item)){
          item['interestApprovedAmplitude'] = null;
        }
      })
      if(this.redutionProposol?.dtttrrConfirm){
        this.dtttrrConfirmTrd = (Number(this.redutionProposol?.dtttrrConfirm) / 1000000).toFixed(2) || 0;
      }
      if(this.redutionProposol?.dtttrrConfirmApproved){
        this.dtttrrConfirmApprovedTrd = (Number(this.redutionProposol?.dtttrrConfirmApproved) / 1000000).toFixed(2) || 0;
      }

      this.displayConfirmInfo = this.redutionProposol?.blockCode === 'FI' || this.redutionProposol?.blockCode === 'CIB' || this.redutionProposol?.customerStructure !== 'Phi tín dụng';
      if (this.reductionProposalDetailsDTOList.length && this.action == ACTION.confirm) {

        this.reductionProposalDetailsDTOList.forEach(item => {
          item['interestApprovedAmplitude'] = _.cloneDeep(item.interestProposedAmplitude);
          item.reductPercent = 0;
          item.rateMinimum = 0;
          item.interestValue = (Number(item.interestReference) + Number(item.interestProposedAmplitude)).toFixed(2);

          item["ratioCostApproved"] = item.ratioCostApproved?item.ratioCostApproved: item.ratioCostOffer || null;
          item["minimumCostApproved"] =  item.minimumCostApproved?item.minimumCostApproved: item.minimumCostOffer || null;
          item["maximumCostApproved"] = item.maximumCostApproved?item.maximumCostApproved: item.maximumCostOffer || null;
          item["minInterestRateOfferApproved"] = item.minInterestRateOffer ? item.minInterestRateOffer : item.minInterestRateOfferApproved || null;


          if(Utils.isNotNull(item?.ratioCostApproved)){
            this.calculateReduction(item?.ratioCostApproved ,item, 'Cost');
          }
          if(Utils.isNotNull(item?.minimumCostApproved)){
            this.calculateReduction(item?.minimumCostApproved ,item, 'Min');
          }
          if(Utils.isNotNull(item?.maximumCostApproved)){
            this.calculateReduction(item?.maximumCostApproved ,item, 'Max');
          }
        });
      }

      if(this.reductionProposalDetailsDTOList.length && this.action != ACTION.confirm){
        this.reductionProposalDetailsDTOList.forEach(item => {
          if(Utils.isNotNull(item?.ratioCostApproved)){
            this.calculateReduction(item?.ratioCostApproved ,item, 'Cost');
          }
          if(Utils.isNotNull(item?.minimumCostApproved)){
            this.calculateReduction(item?.minimumCostApproved ,item, 'Min');
          }
          if(Utils.isNotNull(item?.maximumCostApproved)){
            this.calculateReduction(item?.maximumCostApproved ,item, 'Max');
          }
        });
      }
      if(this.action != ACTION.confirm){
        this.monthApproval = this.redutionProposol.monthApplyApproved || null;
        this.timeApproval = this.redutionProposol.endDateApproved?moment(this.redutionProposol.endDateApproved).format('DD/MM/YYYY') :'Không thời hạn';
      }else{
        if(this.redutionProposol.status == 'CBQL_REFUSED_CONFIRM' && !this.isKhdn && !this.redutionProposol.monthApplyApproved){
          this.monthApproval = this.redutionProposol.monthApplyApproved;
          this.timeApproval = 'Không thời hạn';
        }else{
          this.monthApproval = this.redutionProposol.monthApplyApproved?this.redutionProposol.monthApplyApproved:this.redutionProposol.monthApply || null ;
          this.timeApproval =  this.redutionProposol.endDateApproved ? moment(this.redutionProposol.endDateApproved).format('DD/MM/YYYY') : this.redutionProposol.endDate?moment(this.redutionProposol.endDate).format('DD/MM/YYYY'):'Không thời hạn';
        }
      }
      this.products = [
        { content: 'Phạm vi áp dụng', offer: this.showScopeType(this.redutionProposol.scopeType), scopeApproval: this.showScopeType(this.redutionProposol.scopeTypeApproved) },
        { content: 'Hiệu lực tờ trình', offer: this.redutionProposol.monthApply?`${this.redutionProposol.monthApply} tháng`:`Không thời hạn`, timeApproval: this.monthApproval},
        { content: 'Thời gian hết hạn tờ trình', offer: this.redutionProposol.monthApply?`${moment(this.redutionProposol.endDate).format('DD/MM/YYYY')}`:`Không thời hạn`, timeEndApproval: this.timeApproval}
      ];
      if (this.isApproval || !this.action) {
        this.redutionProposol.reductionProposalDetailsApprovedDTOList.forEach(item => {
          item.interestValue = (Number(item.interestReference) + Number(item.interestApprovedAmplitude)).toFixed(2);
          item.reductPercent = ((Number(item?.interestProposedAmplitude) - Number(item?.interestApprovedAmplitude)) / Number(item?.interestProposedAmplitude) * 100).toFixed(2);
          item.rateMinimum = ((Number(item?.minInterestRateOffer) - Number(item?.minInterestRateOfferApproved)) / Number(item?.minInterestRateOffer) * 100).toFixed(2);
        });
      }
      this.redutionProposol['segmentName'] = this.listSegment.find(item => item.code == this.redutionProposol.customerSegment)?.name;
    })
  }

  setTimeApproval(event) {
    if (!this.isKhdn && event.value == null) {
      this.products[2].timeEndApproval = "Không thời hạn";
      this.products[1].timeApproval = null;
      this.errorMonthApproval = null;
      return;
    }
    if(event.value != null && event.value !== "" && event.value <= 0){
      this.errorMonthApproval = "Hiệu lực tờ trình không được nhỏ hơn 0";
      return;
    }else{
      if(!event.value && this.isKhdn){
        this.errorMonthApproval = "Hiệu lực tờ trình không được để trống";
        return
      }
      this.errorMonthApproval = null;
    }

    const monthApply = this.redutionProposol.monthApply?this.redutionProposol.monthApply:0;
    const month = event.value - monthApply;
    let dateCalendar = null;
    if (month > 0) {
      dateCalendar = moment(this.redutionProposol.endDate).add(month, 'months').calendar();
    }
    if (month < 0) {
      dateCalendar = moment(this.redutionProposol.endDate).subtract(-month, 'months').calendar();
    }
    if (!month) {
      dateCalendar = moment(this.redutionProposol.endDate);
    }
    this.products[2].timeEndApproval = moment(dateCalendar).format('DD/MM/YYYY');
  }


  initTOI(toi) {
    if(this.toiAlready){
      this.info.toi = toi;
    }else{
      this.info.toi = {
        duNoNganHanBq: toi?.dunoBqSd,
        duNoNganHanBqUnit: toi?.dunoBqNim,
        duNoTdhBq: toi?.dunoTdhBqSd,
        duNoTdhBqUnit: toi?.dunoTdhBqNim,
        coKyHanBq: toi?.ckhBqSd,
        coKyHanBqUnit: toi?.ckhBqNim,
        khongKyHanBq: toi?.kkhBqSd,
        khongKyHanBqUnit: toi?.kkhBqNim,
        thuBl: toi?.tblDsSd,
        thuBlUnit: toi?.tblNim,
        thuTTQT: toi?.tttqtDsSd,
        thuTTQTUnit: toi?.tttqtMpqd,
        thuFx: toi?.tfxDsSd,
        thuFxUnit: toi?.tfxMpqd,
        thuBanca: toi?.thuBancaDsSd,
        thuKhac: toi?.thuKhacDsSd,
        'dtttrr': this.redutionProposol.status == ReductionProposalStatus.WAITING_CONFIRM ? this.redutionProposol?.dtttrrConfirm : this.redutionProposol.dtttrrConfirmApproved,
        'toi': this.redutionProposol.status == ReductionProposalStatus.WAITING_CONFIRM ? this.redutionProposol?.toiConfirm : this.redutionProposol.toiConfirmApproved,
        // Hieu qua
        ckhBqHq: toi?.ckhBqHq,
        dunoBqHq: toi?.dunoBqHq,
        dunoTdhBqHq: toi?.dunoTdhBqHq,
        kkhBqHq: toi?.kkhBqHq,
        tblHq: toi?.tblHq,
        tfxHq: toi?.tfxHq,
        thuBanCaHq: toi?.thuBanCaHq,
        thuKhacHq: toi?.thuKhacHq,
        tttqtHq: toi?.tttqtHq,
      };
    }
    this.reductionProposal.info.next(this.info);
  }

  showScopeType(scope) {
    let type = ''
    switch (scope) {
      case 0:
        type = 'Tất cả các phương án'
        break;
      case 1:
        type = 'Mã phương án hạn mức'
        break;
      case 2:
        type = 'Mã LD'
        break;
      case 3:
        type = 'Chưa xác định'
        break;
      case 4:
        type = 'Mã MD'
        break;
      default:
        type = scope;
        break;
    }
    return type;
  }

  confirm() {
    this.form.controls['rmConfirm'].markAllAsTouched();

    const declare: any = this.info = this.reductionProposalApi?.info?.value;
    let checkNull = true;
    let nimConfirmApprovedNumber = 0;
    let nimExistingApprovedNumber = 0;
    let errorMessage;
    if(!this.isKhdn){
      if(this.nimConfirmApproved){
        nimConfirmApprovedNumber = Number(this.nimConfirmApproved.toString().replace(',','.')) || 0;
      }else{
        if(!errorMessage){
          if(this.nimConfirmApproved != 0)
            errorMessage = "NIM cam kết không được để trống"
          else
            errorMessage = "NIM cam kết (%) phải lớn hơn 0"
        }
      }
      if(this.nimExistingApproved){
        nimExistingApprovedNumber = Number(this.nimExistingApproved.toString().replace(',','.')) || 0;
      }else{
        if(!errorMessage)
          errorMessage = "NIM hiện hữu không được để trống"
      }
      if(nimConfirmApprovedNumber <= 0){
        if(!errorMessage)
          errorMessage = "NIM cam kết (%) phải lớn hơn 0"
      }
      if(nimConfirmApprovedNumber > this.redutionProposol?.nimConfirm){
        if(!errorMessage)
          errorMessage = "Giá trị xác nhận <= giá trị đề xuất"
      }

    }
    if(this.errorMonthApproval){
      if(!errorMessage)
        errorMessage = this.errorMonthApproval;
    }
    if(this.errorCalReduction){
      if(!errorMessage)
        errorMessage = this.errorCalReduction;
    }
    console.log(this.redutionProposol.reductionProposalDetailsDTOList);

    const body = {
      code: this.code,
      reductionProposalDetailsDTOList: this.reductionProposalDetailsDTOList.map(item => {
        // item.interestProposedAmplitude = item.interestApprovalAmplitude ? item.interestApprovalAmplitude : item.interestProposedAmplitude;
        if(item.maximumCostApproved)
          item.maximumCostApproved = Number(item.maximumCostApproved?.toString().replaceAll(',', ''));
        if(item.minimumCostApproved)
          item.minimumCostApproved = Number(item.minimumCostApproved?.toString().replaceAll(',', ''));
        if(item.ratioCostApproved)
          item.ratioCostApproved = Number(item.ratioCostApproved?.toString().replaceAll(',', ''));
        if (checkNull && !this.onFocus(null, item)) {
          checkNull = false;
        }
        if(this.optionCodeData?.optionCode){
          item.planCode = this.optionCodeData?.optionCode;
        }
        return item;
      }),
      "reductionProposalTOIDTO": {
        "ckhBqNim": declare?.toi?.coKyHanBqUnit || 0,
        "ckhBqSd": declare?.toi?.coKyHanBq || 0,
        "dunoBqNim": declare?.toi?.duNoNganHanBqUnit || 0,
        "dunoBqSd": declare?.toi?.duNoNganHanBq || 0,
        "dunoTdhBqNim": declare?.toi?.duNoTdhBqUnit || 0,
        "dunoTdhBqSd": declare?.toi?.duNoTdhBq || 0,
        "kkhBqNim": declare?.toi?.khongKyHanBqUnit || 0,
        "kkhBqSd": declare?.toi?.khongKyHanBq || 0,
        "tblDsSd": declare?.toi?.thuBl || 0,
        "tblNim": declare?.toi?.thuBlUnit || 0,
        "tfxDsSd": declare?.toi?.thuFx || 0,
        "tfxMpqd": declare?.toi?.thuFxUnit || 0,
        "thuBancaDsSd": declare?.toi?.thuBanca || 0,
        "thuBanCaMpqd": 1,
        "thuKhacDsSd": declare?.toi?.thuKhac || 0,
        "thuKhacMpqd": 1,
        "tttqtDsSd": declare?.toi?.thuTTQT || 0,
        "tttqtMpqd": declare?.toi?.thuTTQTUnit || 0,
        "ckhBqHq": declare?.toi?.ckhBqHq || 0,
        "dunoBqHq": 0,
        "dunoTdhBqHq": 0,
        "kkhBqHq": 0,
        "tblHq": 0,
        "tfxHq": 0,
        "thuBanCaHq": 0,
        "thuKhacHq": 0,
        "tttqtHq": 0,
        "dttck": 0,
        "tck": 0
      },
      toiConfirm: this.modelValue.toi || this.redutionProposol.toiConfirm,
      dtttrrConfirm: this.modelValue.dtttrr || this.redutionProposol.dtttrrConfirm,
      endDate: this.products[2].timeEndApproval?.trim() == '' || this.products[2].timeEndApproval?.trim() ==  'Không thời hạn'? null : this.products[2].timeEndApproval ,// Checkhere
      monthConfirm: this.monthConfirm,
      monthApply: this.products[1].timeApproval,
      scopeType: this.redutionProposol.scopeType,
      isConfirmRm: this.form.controls['rmConfirm'].value ? 1 : 0,
      scopeApplyApproved: this.optionCodeData.optionCode ? JSON.stringify(this.optionCodeData) : '',
      type: this.redutionProposol.type,
      nimConfirmApproved: nimConfirmApprovedNumber,
      nimExisting: nimExistingApprovedNumber,
    };

    if (this.isKhdn && this.products[1].timeApproval > this.redutionProposol.monthApply) {
      if(!errorMessage)
        errorMessage = 'Hiệu lực tờ trình phê duyệt phải nhỏ hơn hoặc bằng hiệu lực tờ trình đề xuất';
    }
    if(this.redutionProposol.type == 0){
      if(!errorMessage)
        errorMessage = this.reductionProposalDetailsDTOList.find(x => x.errorCost !=  "" && x.errorCost != null && x.errorCost != undefined)?.errorCost
      if(!errorMessage)
        errorMessage = this.reductionProposalDetailsDTOList.find(x => x.errorMin !=  "" && x.errorMin != null && x.errorMin != undefined)?.errorMin
      if(!errorMessage)
        errorMessage = this.reductionProposalDetailsDTOList.find(x => x.errorMax !=  "" && x.errorMax != null && x.errorMax != undefined)?.errorMax

    }
    if (errorMessage) {
      this.messageService.warn(errorMessage);
      return;
    }
    if(this.rateInterestError){
      this.messageService.warn(this.rateInterestError);
      return;
    }
    if(this.marginError){
      this.messageService.warn(this.marginError);
      return;
    }

    if (!this.checkRequierd(body) || !checkNull) {
      return;
    }
    if (this.form.invalid) {
      return;
    }
    this.isLoading = true;
    this.reductionProposalApi.confirmReduction(body).subscribe(res => {
      this.messageService.success('Xác nhận thành công!');
      this.isLoading = false;
      this.router.navigateByUrl('sale-manager/reduction-proposal');

    }, err => {
      this.isLoading = false;
      this.messageService.error('Xác nhận thất bại!');

    });
  }

  onFocusTimeApproval(event) {
    const value = { value: this.products[1].timeApproval };
    this.setTimeApproval(value);
  }

  checkRequierd(body) {
    // let checkCIBHH =  this.redutionProposol?.customerCode && this.redutionProposol?.customerStructure != 'Phi tín dụng' && this.redutionProposol?.blockCode == 'CIB';
    if (this.redutionProposol.type == 1 && this.redutionProposol.customerType == 'NEW') {
      if (!body.toiConfirm) {
        this.messageService.error('Vui lòng tính TOI!');
        return false;
      }
      if (!body.dtttrrConfirm) {
        this.messageService.error('Vui lòng tính TOI!');
        return false;
      }
    }
    // Phí : Check TOI voi khoi FI va tin dụng

    else if (this.redutionProposol.type == 0 && this.isKhdn && this.displayConfirmInfo ) {
      if(this.redutionProposol?.blockCode !== 'FI' && this.redutionProposol?.blockCode !== 'CIB' ){
        if (!body.toiConfirm) {
          this.messageService.error('Vui lòng tính TOI!');
          return false;
        }
        if (!body.dtttrrConfirm) {
          this.messageService.error('Vui lòng tính TOI!');
          return false;
        }
      }
    }
    if (body.isConfirmRm == 0) {
      this.messageService.error('Vui lòng xác nhận chịu trách nhiệm với toàn bộ nội dung xác nhận khớp đúng với nội dung phê duyệt của cấp phê duyệt!');
      return false;
    }
    if (!body.scopeApplyApproved && this.redutionProposol.scopeType == 3) {
      this.messageService.error('Vui lòng chọn mã phương án!');
      return false;
    }
    if (this.isKhdn && !body.monthApply) {
      this.messageService.error('Vui lòng nhập hiệu lực tờ trình');
      return false;
    }
    return true;
  }

  CBQLconfirm(idea: boolean) {
    const params: any = {};
    params.status = idea ? 0 : 1;
    params.code = this.code;
    params.type = this.redutionProposol.type;
    this.isLoading = true;
    this.reductionProposalApi.CBQLconfirmReduction(params).subscribe(res => {
      this.isLoading = false;
      this.router.navigateByUrl('sale-manager/reduction-proposal');
      this.messageService.success(`${idea ? 'Đồng ý thành công!' : 'Từ chối thành công!'}`);
    }, err => {
      this.messageService.error('Cập nhật không thành công');
      this.isLoading = false;
      // this.router.navigateByUrl('/sale-manager/reduction-proposal');
    });
  }

  CaculateToi() {
    const dialogRef = this.dialog.open(ToiConfirmModalComponent);
    if(!this.toiAlready && (this.action != ACTION.confirm || this.redutionProposol?.reductionProposalTOIApprovedDTO)){
      this.initTOI(this.redutionProposol.reductionProposalTOIApprovedDTO);
    }
    if(this.toiAlready){
      this.initTOI(this.toiChangedApprove);
    }
    dialogRef.componentInstance.action = this.queryParamMap['params']['action'] || '';
    dialogRef.afterClosed().subscribe(data => {
      this.toiAlready = true;
      console.log(this.reductionProposalApi);
      this.info = this.reductionProposalApi?.info?.value;
      const toi = this.info?.toi;
      this.toiChangedApprove = {...toi};
      const convertDtttrr = toi?.dtttrr? Number(+toi?.dtttrr?.toString().replace(',', '.')) : 0;
      if (+toi.toi < +this.redutionProposol?.toiConfirm || +convertDtttrr < +this.redutionProposol?.dtttrrConfirm) {
        this.messageService.error('TOI không được nhỏ hơn TOI hiện tại!');
        this.initTOI(this.redutionProposol.reductionProposalTOIDTO);
        return;
      }
      if (toi) {
        this.modelValue.toi = toi.toi;
        this.modelValue.dtttrr = toi.dtttrr;
        this.redutionProposol.toiConfirmApproved = toi.toi;

        if(toi.dtttrr){
          this.dtttrrConfirmTrd = (Number(toi.dtttrr) / 1000000).toFixed(2) || 0;
          this.dtttrrConfirmApprovedTrd = this.dtttrrConfirmTrd;
        }
      }
    });
  }

  onFocus(event, data) {
    if (+data.interestApprovedAmplitude < +data.interestProposedAmplitude) {
      if(event){
        this.messageService.warn('Biên độ phải ≥ biên độ đề xuất!');
      }
      return false;
    }
    return true;
  }

  changeMarginSuggest(event, data): void {
    // console.log(event);
    const val = event.value;
    this.marginError = '';

    if(Number(val) > Number(data.interestAmplitude)){
      data.errorMessage = 'Biên độ phải ≤ biên độ quy định';
      this.messageService.warn('Biên độ phải ≤ biên độ quy định');
      this.marginError = data.errorMessage;
    }
    else if(Number(val) < Number(data.interestProposedAmplitude)){
      data.errorMessage = 'Biên độ phải ≥ biên độ đề xuất';
      this.messageService.warn('Biên độ phải ≥ biên độ đề xuất');
      this.marginError = data.errorMessage;
    }
    else if (+val < +data.interestProposedAmplitude) {
      // this.messageService.warn('Biên độ phê duyệt phải lớn hơn hoặc bằng biên độ đề xuất!');
      // data.interestApprovedAmplitude = 5;
      // console.log(data);
      return;
    }
    else if (val || val == 0) {
      if (Number(val) > 100) {
        data.errorMessage = 'Biên độ phải nhỏ hơn hoặc bằng 100';
      } else if (Number(val) < data?.interestProposedAmplitude) {
        data.errorMessage = 'Biên độ phải lớn hơn hoặc bằng mức giảm biên độ tối đa';
      } else {
        delete data.errorMessage;
      }
      data.interestValue = (Number(data.interestReference) + Number(val)).toFixed(2);
      data.reductPercent = ((Number(data.interestProposedAmplitude) - Number(val)) / Number(data.interestProposedAmplitude) * 100).toFixed(2);

    } else {
      delete data.errorMessage;
      data.interestValue = null;
      data.reductPercent = null;
    }
  }

  openOptionCodeModal(event) {
    const config = {
      selectionType: SelectionType.single
    };
    const modal = this.modal.open(OptionCodeModalComponent, { windowClass: 'list__optioncode-modal' });
    modal.componentInstance.config = config;
    modal.componentInstance.type = this.redutionProposol.type;
    modal.componentInstance.blockCode = this.redutionProposol.blockCode;
    modal.componentInstance.customerCode = this.info?.customer?.customerCode; //"";
    modal.componentInstance.businessRegistrationNumber = this.info?.businessRegistrationNumber || this.info?.taxCode; 
    modal.componentInstance.idCard = this.info?.customer?.idCard || this.info?.customer?.taxCode;
    modal.componentInstance.listCode = this.optionCodeData ? [this.optionCodeData['optionCode']] : [];
    modal.result.then((res) => {
      if (res) {
        this.optionCodeData = res.listSelected;
      }
      console.log(this.optionCodeData);

    }).catch(() => {

    });
  }


  formatCurrency(element) {
    if (element == 0) {
      return '0';
    }
    if (!element || element === 'NaN' || element === '--') {
      return '--';
    }
    let formated: string = element.toString();
    let first = '';
    let last = '';
    if (formated.includes('.')) {
      first = formated.substr(0, formated.indexOf('.'));
      last = formated.substr(formated.indexOf('.'));
    } else {
      first = formated;
    }
    if (Number(first)) {
      first = Number(first).toLocaleString('en-US', {
        style: 'currency',
        currency: 'VND',
      });
      first = first.substr(1, first.length);
      formated = first + last;
    }
    return formated;
  }

  convertToiCurrent(toi) {
    if (toi) {
      toi = Number(Number(toi) * 100).toFixed(2);
    }
    return toi;
  }

  convertDtttrr(dtttrr) {
    if (dtttrr) {
      dtttrr = Number(Number(dtttrr) / 1000000).toFixed(2);
    }
    return dtttrr;
  }

  calculateReduction(value, item, type): void {
    value = value?.toString();
    delete item['error' + type];
    this.errorCalReduction= "";
    if (Utils.isNotNull(value) && Utils.isNotEmpty(value)) {
      this.hasErr = 0;
      let hasErr2 = 0;

      let hasErrMax = 0;
      let val = Number(value?.replaceAll(',', ''));
      // format input
      let formated = this.formatCurrency(val);
      let check = value.match(/\.$/);
      if(check && !formated.includes('.')){
        formated += '.';
      }
      check = value.match(/0+$/);
      if(formated.includes('.') && check){
        formated += check[0];
      }
      check = value.match(/\.0+$/);
      if(!formated.includes('.') && check){
        formated += check[0];
      }
      switch (type) {
        case 'Cost':
          item.rateCost = item?.ratioCostOffer?val ? ((item?.ratioCostOffer - val) / item?.ratioCostOffer * 100).toFixed(2) : val === 0 ? 100 : null:0;
          if(val < item?.ratioCostOffer){
            this.hasErr++;
          }
          if(val > item?.ratioCost){
            hasErrMax++;
          }
          item.ratioCostApproved = formated;
          break;
        case 'Min':
          console.log("item?.maximumCostApproved : ",item?.maximumCostApproved)
          item.rateMin = item?.minimumCostOffer?val ? ((item?.minimumCostOffer - val) / item?.minimumCostOffer * 100).toFixed(2) : val === 0 ? 100 : null:0;
          if(val < item?.minimumCostOffer){
            this.hasErr++;
          }
          if(val > item?.minimumCost){
            hasErrMax++;
          }
          if(Utils.isNotNull(item?.maximumCostApproved) && val > Number(item?.maximumCostApproved?.toString().replaceAll(',', ''))){
            hasErr2++;
          }
          item.minimumCostApproved = formated;
          break;
        case 'Max':
          console.log("item?.minimumCostApproved : ",item?.minimumCostApproved)
          item.rateMax = item?.maximumCostOffer?val ? ((item?.maximumCostOffer - val) / item?.maximumCostOffer * 100).toFixed(2) : val === 0 ? 100 : null:0;
          if(val < item?.maximumCostOffer){
            this.hasErr++;
          }
          if(val > item?.maximumCost){
            hasErrMax++;
          }
          if(Utils.isNotNull(item?.minimumCostApproved) && val < Number(item?.minimumCostApproved?.toString().replaceAll(',', ''))){
            hasErr2++;
          }
          item.maximumCostApproved = formated;
          break;
      }
      if(this.hasErr){
        item['error' + type] = 'Giá trị phải lớn hơn hoặc bằng biểu đề xuất';
        this.errorCalReduction = 'Giá trị phải lớn hơn hoặc bằng biểu đề xuất';
      }else if(hasErrMax){
        item['error' + type] = 'Giá trị phải nhỏ hơn hoặc bằng biểu theo quy định';
        this.errorCalReduction = 'Giá trị phải nhỏ hơn hoặc bằng biểu theo quy định';
      }else if(hasErr2){
        let mess = 'Phí tối đa phê duyệt phải >= phí tối thiểu phê duyệt';
        item['error' + type] = mess;
        this.errorCalReduction = mess;
      }
      if(hasErr2 == 0){
        let mess = 'Phí tối đa phê duyệt phải >= phí tối thiểu phê duyệt';
        if(item['errorMax'] == mess)
          item['errorMax'] = null;
        if(item['errorMin'] == mess)
          item['errorMin'] = null;
      }
    }else{
      item['rate' + type] = null;

      switch (type) {
        case 'Cost':
          this.errorCalReduction = 'Mức phí phê duyệt giảm không được để trống';
          break;
        case 'Min':
          this.errorCalReduction = 'Phí tối thiểu không được để trống';
          break;
        case 'Max':
          this.errorCalReduction = 'Phí tối đa không được để trống';
          break;
      }
      item['error' + type] = this.errorCalReduction;

    }
    // for (let index = 0; index < this.reductionProposalDetailsDTOList.length; index++) {
    //   const element = this.reductionProposalDetailsDTOList[index];
    //   if (element.data.code === item.code && element.data?.productCode === item?.productCode) {
    //     this.reductionProposalDetailsDTOList[index].data = item;
    //   }
    // }
  }
  getValueNumber(){

  }

  validateNimConfirm(event){
    if(!this.nimConfirmApproved && this.nimConfirmApproved != 0)
      this.errorNimConfirm = "Trường này là bắt buộc"
    else if(this.nimConfirmApproved <= 0)
      this.errorNimConfirm = "NIM cam kết (%) phải lớn hơn 0"
    else if(this.nimConfirmApproved > this.redutionProposol?.nimConfirm){
      this.errorNimConfirm = "Giá trị xác nhận <= giá trị đề xuất"
    }else
      this.errorNimConfirm = null;
  }

  keyPress(event, rowData, type){
    let val = event.key;
    let checkVal;
    switch (type) {
      case 'Cost':
        checkVal = rowData.ratioCostApproved;
        break;
      case 'Min':
        checkVal = rowData.suggestMin;
        break;
      case 'Max':
        checkVal = rowData.suggestMax;
    }

    if ((isNaN(val) && !['.','Delete', 'Backspace','ArrowLeft','ArrowRight'].includes(val)) ||
      (val ==='.' && checkVal?.includes('.')))  {
      event.preventDefault();
    }
  }
  calculateReductionOnBlur(event, rowData, type){
    event.preventDefault();
    let val = event.target.value;
    let valNum = Number(val?.replaceAll(',',''));

    if(Utils.isEmpty(val)){
      setTimeout(() => {
        switch (type) {
          case 'Cost':
            rowData.ratioCostApproved = rowData?.suggestCost;
            break;
          case 'Min':
            rowData.suggestMin = rowData.minCharge;
            break;
          case 'Max':
            rowData.suggestMax = rowData.maxCharge;
        }
        rowData['rate' + type] = 0;
      }, 200);
    }else{
      switch (type) {
        case 'Cost':
          rowData.ratioCostApproved = this.formatCurrency(valNum);
          break;
        case 'Min':
          rowData.suggestMin = this.formatCurrency(valNum);
          break;
        case 'Max':
          rowData.suggestMax = this.formatCurrency(valNum);
      }
    }
  }

  checkAbilityInterest(){
    return (this.typeInterestDetail === 2 || this.typeInterestDetail === 4);
  }

  getValueMinimumRate(event, row) {
    console.log("row: ", row);
    const val = event.target.value;
    if (Number(val) < row.minInterestRateOffer) {
      row.rateMinimumError = 'Lãi suất tối thiểu phải ≥ lãi suất tối thiểu đề xuất';
      this.messageService.warn('Lãi suất tối thiểu phải ≥ lãi suất tối thiểu đề xuất');
      this.rateInterestError = row.rateMinimumError;
    }
    else if(Number(val) > Number(row?.minInterestRate)){
      row.rateMinimumError = 'Lãi suất tối thiểu phải ≤ lãi suất tối thiểu quy định';
      this.messageService.warn('Lãi suất tối thiểu phải ≤ lãi suất tối thiểu quy định');
      this.rateInterestError = row.rateMinimumError;
    }
    else if (Number(val) < 0) {
      row.rateMinimumError = 'Lãi suất tối thiểu phải lớn hơn 0 và nhỏ hơn hoặc bằng lãi suất tối thiểu theo biểu quy định';
      this.messageService.warn('Lãi suất tối thiểu phải lớn hơn 0 và nhỏ hơn hoặc bằng lãi suất tối thiểu theo biểu quy định)');
      this.rateInterestError = row.rateMinimumError;
    } else {
      delete row.rateMinimumError;
      this.rateInterestError = '';
    }
    row.rateMinimum = ((Number(row.minInterestRateOffer) - Number(val)) / Number(row.minInterestRateOffer) * 100).toFixed(2);
  }

  calculateInterest(event, row) {
    const val = event.target.value;
    if (Number(val) <= 0 || Number(val) > Number(row?.minInterestRate) || !val) {
      row.rateMinimum = row?.minInterestRate;
      row.rateInterestReduction = 0;
    }
    this.treeTableInterest.updateSerializedValue();
  }
}
