import {
  AfterContentChecked,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  HostListener,
  Injector,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import {
  CommonCategory,
  Division,
  FunctionCode,
  functionUri,
  Scopes,
  SessionKey
} from 'src/app/core/utils/common-constants';
import * as _ from 'lodash';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject, forkJoin, of } from 'rxjs';
import { ReductionProposalApi } from '../../../api/reduction-proposal.api';
import { CustomerApi, CustomerDetailApi, CustomerDetailSmeApi } from 'src/app/pages/customer-360/apis/customer.api';
import { catchError, finalize } from 'rxjs/operators';
import { ToiConfirmModalComponent } from '../dialog/toi-confirm-modal/toi-confirm-modal.component';
import { InterestModalComponent } from '../dialog/select-interest-modal/select-interest-modal.component';
import { SelectionType } from '@swimlane/ngx-datatable';
import { OptionCodeModalComponent } from '../dialog/select-optioncode-modal/select-optioncode-modal.component';
import { LdCodeModalComponent } from '../dialog/select-ldcode-modal/select-ldcode-modal.component';
import { SelectMdModalComponent } from '../dialog/select-md-modal/select-md-modal.component';
import {
  ConfirmAuthorizationModalComponent
} from '../dialog/confirm-authorization-modal/confirm-authorization-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { Utils } from 'src/app/core/utils/utils';
import * as moment from 'moment';
import {
  WarningAuthorizationModalComponent
} from '../dialog/warning-authorization-modal/warning-authorization-modal.component';
import { BranchApi } from 'src/app/pages/rm/apis';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { Validators } from '@angular/forms';
import { SelectFeeModalComponent } from '../dialog/select-fee-modal/select-fee-modal.component';
import { CreateReductionProposalComponent } from '../create/create.component';
import { TreeTable } from 'primeng/treetable';

@Component({
  selector: 'app-create-proposal',
  templateUrl: './create-proposal.component.html',
  styleUrls: ['./create-proposal.component.scss'],
  providers: [NgbModal]
})
export class CreateProposalComponent extends BaseComponent implements OnInit, AfterContentChecked {


  @ViewChild('treeTableInterest') treeTableInterest: TreeTable;
  @Output() changeLoading = new EventEmitter();
  @Output() dataReduction = new EventEmitter();
  @Input() declareInfo;
  subject = new BehaviorSubject(null);
  @Input() isUpdate = false;
  updateObj;
  form = this.fb.group({
    searchStr: '',
    caseSuggest: [null],
    scopeApply: 'ALL',
    toi: null,
    dtttrr: null,
    monthApply: null,
    monthConfirm: null,
    listData: [],
    authorization: [],
    lastApprove: '',
    lastApproveTitle: '',
    lastApproveFull: '',
    segment: '',
    dtttrrDisplay: null,
    nimCurrent: null,
    nimConfirm: null,
    typeInterest: 1
  });
  isShow: any;
  customerObject = [];
  customerSegment = [];
  tabIndex = 0;
  ngmodel = 5;
  parentLoading = false;
  reductionProposalTranslate: any;
  info: any;
  ketQuaXhtd: any;
  monthConfirms = [];
  months = [];
  caseSuggesst = [
    { code: 'FEE', displayName: 'Phương án miễn giảm phí' },
    { code: 'INTEREST', displayName: 'Phương án miễn giảm lãi' }
  ];
  allScopeApplys = [
    { code: 'ALL', displayName: 'Tất cả các phương án', caseSuggest: 'ALL', typeNum: 0 },
    { code: 'OPTION_CODE', displayName: 'Mã phương án hạn mức', caseSuggest: 'ALL', typeNum: 1 },
    { code: 'LD_CODE', displayName: 'Mã LD', caseSuggest: 'ALL', typeNum: 2 },
    { code: 'MD_CODE', displayName: 'Mã MD', caseSuggest: 'FEE', typeNum: 4 },
    { code: 'UNKNOWN', displayName: 'Chưa xác định mã phương án', caseSuggest: 'ALL', typeNum: 3 }
  ];
  AllTypeFee = [
    {
      id: 1,
      code: 10,
      displayName: 'Biểu lãi suất thông thường',
      name: 'Thông thường',
      caseSuggest: 'NORMAL',
      productType: 'TT',
      typeNum: 0,
      show: true
    },
    {
      id: 2,
      code: 40,
      displayName: 'Biểu lãi suất ưu đãi (AIRS)',
      name: 'Ưu đãi',
      caseSuggest: 'ENDOW',
      productType: 'UD',
      typeNum: 0,
      show: true
    },
    {
      id: 3,
      code: 40,
      displayName: 'Ngoại lệ ưu đãi',
      name: 'Ưu đãi',
      caseSuggest: 'EXCEPTION_ENDOW',
      productType: 'UD',
      typeNum: 0,
      show: false
    },
    {
      id: 4,
      code: 30,
      displayName: 'Biểu lãi suất linh hoạt',
      name: 'Linh hoạt',
      caseSuggest: 'ABILITY',
      productType: 'UD',
      typeNum: 0,
      show: true
    },
    {
      id: 5,
      code: 30,
      displayName: 'Ngoại lệ linh hoạt',
      name: 'Linh hoạt',
      caseSuggest: 'EXCEPTION_ABILITY',
      productType: 'UD',
      typeNum: 0,
      show: false
    },
    {
      id: 6, code: 10, value: 10, displayName: ' Biểu lãi suất thông thường CIB', name: 'Thông thường'
      , caseSuggest: 'NORMAL_CIB', productType: 'TT', typeNum: 0, show: true
    }
  ];
  confirmException = [
    {
      code: 1, name: 'TOI (%)', suggest: null, desc: '', placeholderPropose: 'Nhập TOI (%)'
      , placeholderBasic: 'Nhập nội dung', length: 15, maxDigi: 2, minDigi: 2, type: 3, errorMess: ''
    },
    {
      code: 2, name: 'Đối tượng KH', suggest: '', desc: '', optionalCustomer: this.customerObject
      , type: 1, placeholderPropose: 'Chọn đối tượng KH', placeholderBasic: 'Nhập nội dung', errorMess: ''
    },
    {
      code: 3, name: 'Phân khúc', suggest: '', desc: '', optionalCustomer: this.customerSegment
      , type: 1, placeholderPropose: 'Chọn phân khúc', placeholderBasic: 'Nhập nội dung', errorMess: ''
    },
    {
      code: 4, name: 'Tiêu chí khác', suggest: '', desc: '', placeholderPropose: 'Nhập tiêu chí khác'
      , placeholderBasic: 'Nhập nội dung', type: 2
    }
  ];
  resetTypeInterestCib = false;
  dumpTypeInterest: any;
  typeInterest = [];
  scopeApplys = [];
  listCreditRatings = [];
  customerTypeDisplay = '';
  customer360Fn: any;
  isDeclareTab = false;
  interestData = [];
  optionCodeData = [];
  ldCodeData = [];
  mdCodeData = [];
  listData = [];
  isLoaded = false;
  commonSegment = [];
  listLD = [];
  listMD = [];
  existed = true;
  divisionCodeOfUser: any[];
  isRM = true;
  isKhdn = true;
  blockCode = '';
  feeData = [];
  listFeeData = [];
  checkConfirmInfo = false;
  displayConfirmInfo = false;
  checkPotentialCustomerSegment = false;
  customer: any;
  viewTable = true;

  constructor(
    injector: Injector,
    public dialog: MatDialog,
    private reductionProposalApi: ReductionProposalApi,
    private customerApi: CustomerApi,
    private customerDetailSmeApi: CustomerDetailSmeApi,
    private customerDetailApi: CustomerDetailApi,
    private dtc: ChangeDetectorRef,
    private modal: NgbModal,
    private branchApi: BranchApi,
    private categoryService: CategoryService,
    private createReductionProposalComponent: CreateReductionProposalComponent
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REDUCTION_PROPOSAL}`);
    this.customer360Fn = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
  }

  @Input() set onLoaded(data) {
    if (data) {
      this.updateObj = data;
      this.loadData();
      console.log(data);

    }
  };

  @HostListener('keyup.enter')
  onEnter() {
    this.dtc.detectChanges();
  }

  ngOnInit(): void {
    const month = [];
    const monthConfirm = [];

    for (let index = 1; index < 13; index++) {
      const item = { code: index, displayName: index.toString() };
      if (index < 7) {
        monthConfirm.push(item);
      }
      month.push(item);
    }
    this.months = month;
    this.monthConfirms = monthConfirm;
    this.info = this.reductionProposalApi?.info?.value;
    this.customer = this.info.customer;
    // if (this.isUpdate) {
    //   this.loadData();
    // }
    const divisionOfUser = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
    this.divisionCodeOfUser = divisionOfUser?.map(item => {
      return item?.code;
    });

  }

  loadData(): void {
    this.customer = this.info.customer;
    this.translate.get(['reductionProposal']).subscribe((result) => {
      this.reductionProposalTranslate = _.get(result, 'reductionProposal');
    });

    this.info = this.reductionProposalApi?.info?.value;
    const customer = this.info.customer;
    let customerCode, caseSuggest, taxCode, idCard;
    if (this.isUpdate) {
      // Luồng cập nhật
      customerCode = this.updateObj?.customerCode;
      taxCode = this.updateObj?.taxCode;
      idCard = this.updateObj?.taxCode;
      this.info.customer.idCard = idCard;
      this.blockCode = this.updateObj?.blockCode;
      caseSuggest = this.updateObj?.type === 1 ? 'INTEREST' : 'FEE';
      this.isKhdn = ['SME', 'CIB', 'FI', 'DVC'].includes(this.blockCode);
      this.existed = !!this.updateObj?.customerCode;
      this.info.customer.existed = this.existed;
      this.info.type = this.updateObj?.type;

    } else {
      // Luồng tạo mới
      customerCode = customer?.customerCode;
      taxCode = customer?.taxCode;
      idCard = customer?.idCard;
      this.blockCode = this.info.customer.customerTypeMerge;
      caseSuggest = this.info.type === 1 ? 'INTEREST' : 'FEE';
      this.isKhdn = this.customer.isKhdn;
      this.existed = !!this.customer?.customerCode;

      if (customerCode) {
        this.checkBrand(this.form.controls.caseSuggest.value);
      }
    }
    if (caseSuggest === 'FEE') {
      if (this.blockCode === Division.FI) {
        this.monthConfirms = this.months;
      } else {
        this.monthConfirms = [{ code: 6, displayName: '6' }];
      }
    }
    this.info.customer.customerTypeMerge = this.blockCode;
    this.form.controls.caseSuggest.setValue(caseSuggest);
    this.scopeApplys = this.allScopeApplys.filter(item => {
      let res = item.caseSuggest === 'ALL' || item.caseSuggest === caseSuggest;
      if (
        (item.code === 'MD_CODE' && (!this.isKhdn || !this.info.customer.existed))
        || (item.code === 'LD_CODE' && !this.info.customer.existed)
        || ('ALL' !== item.code && this.blockCode === Division.FI)
        || (this.info.type === 1 && item.code === 'LD_CODE')
      ) {
        res = false;
      }
      return res;
    });

    this.subject.subscribe(res => {
      if (res) {
        if (this.existed) {
          // KH có code
          this.changeLoading.emit(true);
          const searchSmeBody = {
            pageNumber: 0,
            pageSize: 10,
            rsId: this.customer360Fn?.rsId,
            scope: 'VIEW',
            customerType: '',
            search: customerCode,
            searchFastType: 'CUSTOMER_CODE',
            manageType: 1,
            customerTypeSale: 1
          };
          const param = {
            customerCode: customerCode || '',
            businessRegistrationNumber: '',
            loanId: ''
          };
          this.customerApi.getByCode(customerCode);
          this.customerApi.search(customerCode);

          const forkJoinReq = [
            this.customerDetailSmeApi.getDataCreditRatings(customerCode).pipe(catchError(() => of(undefined))),
            this.customerDetailSmeApi.getClassification({ customerCode }).pipe(catchError(() => of(undefined))),
            this.reductionProposalApi.getToiExisting(customerCode).pipe(catchError(() => of(undefined))),
            this.reductionProposalApi.getScopeReduction(param).pipe(catchError(() => of(undefined))),
            this.reductionProposalApi.getLDCode(customerCode).pipe(catchError(() => of(undefined))),
            this.reductionProposalApi.getMDCode(customerCode, taxCode).pipe(catchError(() => of(undefined)))
          ];

          if (this.blockCode === Division.INDIV) {
            forkJoinReq.push(this.customerApi.search(searchSmeBody).pipe(catchError(() => of(undefined))));
            forkJoinReq.push(this.customerDetailApi.getByCode(customerCode).pipe(catchError(() => of(undefined))));
            forkJoinReq.push(this.customerDetailApi.getSegmentByCustomerCode(customerCode).pipe(catchError(() => of(undefined))));
          } else {
            forkJoinReq.push(this.customerApi.searchSme(searchSmeBody).pipe(catchError(() => of(undefined))));
            forkJoinReq.push(this.customerDetailSmeApi.get(customerCode).pipe(catchError(() => of(undefined))));
          }
          forkJoin(forkJoinReq).subscribe(([
                                             creditRatings,
                                             classificationResp,
                                             toi,
                                             reductionProposal,
                                             listLD,
                                             listMD,
                                             customerInfo,
                                             info,
                                             indivSegment]) => {
            this.info.customer.mbRelationship = this.generateRelationshipTimeWithMB(info);
            this.info.customer.piority = info?.systemInfo?.priorityName;
            this.info.customer.businessInfo = info?.businessInfo || {};
            this.info.customer.jobInfo = info?.jobInfo || {};
            this.listLD = listLD;
            this.listMD = listMD;
            if (!_.isEmpty(creditRatings)) {
              const dataMapCreditRatings = creditRatings.map((item) => {
                item.loanCodeDTOList.forEach((element) => {
                  element.loanId = item.loanId;
                });
                return item.loanCodeDTOList;
              });
              const listCreditRatings = [];
              dataMapCreditRatings.forEach((item) => {
                item.forEach((i) => {
                  listCreditRatings.push(i);
                });
              });
              this.listCreditRatings = listCreditRatings || [];
              this.listCreditRatings = _.orderBy(this.listCreditRatings, [(obj) => new Date(obj.scoreDate)], ['desc']);
            }
            // Xếp hạng tín dụng
            this.info.customer.scoreValue = reductionProposal?.ketQuaXhtd;

            if (!_.isEmpty(customerInfo)) {
              const cInfo = customerInfo.length ? customerInfo[0] : {};

              this.info.customer.classification = cInfo?.classification;
              this.info.customer.customerCode = cInfo?.customerCode;
              this.info.customer.customerName = cInfo?.customerName;
              this.info.customer.taxCode = cInfo?.taxCode;
              this.info.customer.segment = cInfo?.segmentCode ? cInfo?.segment : '';
              this.info.customer.segmentCode = cInfo?.segmentCode;
            }
            if (this.blockCode === Division.INDIV && this.info.type === 0 && indivSegment) {
              indivSegment = JSON.parse(indivSegment);
              this.info.customer.segment = indivSegment?.name;
              this.info.customer.segmentCode = indivSegment?.code;
            }
            const dataReturn = {
              customerInfo,
              reductionProposal,
              listLD,
              listMD
            };

            this.info.customer.classification = this.info.customer.classification ? this.info.customer.classification : classificationResp;

            if (this.isUpdate && this.updateObj) {
              this.customer.isManage = this.currUser.branch === info?.systemInfo?.branchCode;
              this.form.controls.nimCurrent.setValue(this.updateObj?.nimExisting);
              this.form.controls.nimConfirm.setValue(this.updateObj?.nimConfirm);
              this.info.customer.nimCurrent = this.updateObj?.toiExisting;
              this.info.customer.nimConfirm = this.updateObj?.dtttrrExisting;

              this.form.controls.dtttrr.setValue(this.updateObj?.dtttrrConfirm);
              this.form.controls.dtttrrDisplay.setValue(this.convertDtttrr(this.updateObj?.dtttrrConfirm));
              this.form.controls.toi.setValue(this.updateObj?.toiConfirm);
              if (this.updateObj?.monthApply) {
                this.form.controls.monthApply.setValue(+this.updateObj?.monthApply);
              }
              if (this.updateObj?.monthConfirm) {
                this.form.controls.monthConfirm.setValue(this.updateObj?.monthConfirm);
              }
              this.info.customer.toiCurrent = this.updateObj?.toiExisting;
              this.info.customer.dtttrr = this.updateObj?.dtttrrExisting;
              this.form.controls.caseSuggest.setValue(this.updateObj?.type === 1 ? 'INTEREST' : 'FEE');
              let scopeType;
              switch (this.updateObj?.scopeType) {
                case 0:
                  scopeType = 'ALL';
                  break;
                case 1:
                  scopeType = 'OPTION_CODE';
                  break;
                case 2:
                  scopeType = 'LD_CODE';
                  break;
                case 3:
                  scopeType = 'UNKNOWN';
                  break;
                case 4:
                  scopeType = 'MD_CODE';
                  break;
              }
              this.form.controls.scopeApply.setValue(scopeType);
              this.form.controls.typeInterest.setValue(this.updateObj?.interestType,{emitEvent: false});
              this.initTOI();
              this.initInterestData();
              this.initAuthData();
              this.initFile();
              const body = this.blockCode === Division.INDIV ? {
                customerCode: customerCode.trim(),
                idCard: '',
                blockCode: this.blockCode.trim()
              } : {
                customerCode: customerCode.trim(),
                taxCode: taxCode.trim(),
                blockCode: this.blockCode.trim()
              };
              const api = this.blockCode === Division.INDIV
                ? this.reductionProposalApi.findExistingCustomerIndiv(body)
                : this.reductionProposalApi.findExistingCustomer(body);
              api.pipe(
                finalize(() => {
                })
              ).subscribe((data) => {
                if (!data.code) {
                  this.info.customer.customerCode = data.customerCode;
                  this.info.customer.customerName = data.customerName;
                  this.info.customer.taxCode = data?.taxCode;
                  this.info.customer.idCard = data?.taxCode;
                  this.info.customer.isKhdn = this.isKhdn;
                  this.info.customer.customerTypeMerge = data?.customerTypeMerge;
                  this.info.customer.businessRegistrationNumber = data?.businessRegistrationNumber;
                  this.declareInfo.branchCode = data.branchCode;
                  this.info.customer.branchCode = data.branchCode;
                  this.info.customer.isManage = this.checkBrand(data?.branchCode);
                  this.reductionProposalApi.info.next(this.info);
                  this.dataReduction.emit(dataReturn);
                } else {
                  this.messageService.warn(_.get(this.notificationMessage, 'customerNotExist'));
                  this.dataReduction.emit(dataReturn);
                }
              }, (err) => {
                this.dataReduction.emit(dataReturn);
                this.messageService.warn(err?.error?.description);
              });
            } else {
              this.dataReduction.emit(dataReturn);
            }

            if (toi !== null && toi !== undefined && Object.keys(toi).length === 0) {
              // api ko trả về dữ liệu toi thì mặc định là KH mới
              toi = {
                LOAI_KH: 'Khach hang moi',
                TOI: toi?.TOI,
                DTTTRR: toi?.DTTTRR
              };
            }
            forkJoin([
              this.categoryService.getCommonCategory(CommonCategory.CUSTOMER_OBJECT_CONFIG),
              this.categoryService.getCommonCategory(CommonCategory.SEGMENT_CUSTOMER_CONFIG)])
              .subscribe(([customerObject, categorySegment]) => {
                if (customerObject.content) {
                  customerObject.content.forEach((item) => {
                    this.customerObject.push({ code: item.code, name: item.name });
                  });
                }
                if (categorySegment.content) {
                  const content = categorySegment.content.filter(obj => obj.value === 'SME');
                  content.forEach((item) => {
                    this.customerSegment.push({ code: item.code, name: item.name });
                  });
                }
              });
            this.typeInterest = this.AllTypeFee.filter(value => value.show);
            if (this.info.customer.blockCode === Division.CIB || this.updateObj?.blockCode === Division.CIB) {
              this.typeInterest = this.typeInterest.filter(value => value.show && value.id !== 6);
            }

            this.info.customer.customerType = toi?.LOAI_KH;
            this.info.customer.toiCurrent = toi?.TOI;
            this.info.customer.dtttrr = toi?.DTTTRR;
            // SME. Đối với KH tín dụng, nếu không trả gia trị TOI hiện hữu thì hiển thị TOI cam kết và nút tính TOI
            this.displayConfirmInfo = this.info.customer.customerCreditType !== 'Phi tín dụng'
            && this.blockCode === Division.SME && Utils.isNull(toi?.TOI) ? true : this.checkConfirmInfo;

            if (!Utils.isEmpty(toi) && toi?.LOAI_KH.toLocaleLowerCase() === 'khach hang cu') {
              this.info.customer.customerTypeDisplay = 'Khách hàng cũ';
              this.info.customer.customerTypeCode = 'OLD';
              this.info.customer.isNew = false;
            } else if (!Utils.isEmpty(toi) && toi?.LOAI_KH.toLocaleLowerCase() === 'khach hang moi') {
              this.info.customer.customerTypeDisplay = 'Khách hàng mới';
              this.info.customer.customerTypeCode = 'NEW';
              this.info.customer.isNew = true;
            }
            this.isLoaded = true;
            if (this.updateObj?.reductionProposalAuthorizationDTOList.length > 0) {
              this.checkAuthDataChange(false);
            }
            this.changeLoading.emit(false);
          }, (e) => {
            this.messageService.error(e.error.description);
            this.changeLoading.emit(false);
          });
        } else {
          // KH chưa có code
          this.scopeApplys = this.scopeApplys.filter(item => {
            return item.code !== 'LD_CODE';
          });
          const param = {
            businessRegistrationNumber: this.info.customer?.businessRegistrationNumber || '',
            loanId: ''
          };
          const forkJoinReq = [
            this.commonService
              .getCommonCategory(CommonCategory.REVENUE_CUSTOMER_SEGMENT)
              .pipe(catchError(() => of(undefined))),
            this.reductionProposalApi.getScopeReduction(param).pipe(catchError(() => of(undefined))),
            this.reductionProposalApi.getLDCode(customerCode).pipe(catchError(() => of(undefined))),
            this.reductionProposalApi.getMDCode(customerCode, taxCode).pipe(catchError(() => of(undefined)))
          ];
          const forkJoinResp = ['segment', 'reductionProposal', 'listLD', 'listMD'];
          this.checkPotentialCustomerSegment = ['SME', 'CIB'].includes(this.blockCode);
          if (this.checkPotentialCustomerSegment) {
            forkJoinReq.push(this.reductionProposalApi.getKhdnPotentialCustomerSegment(taxCode).pipe(catchError(() => of(undefined))));
            forkJoinResp.push('potentialCustomerSegment');
          }

          forkJoin(forkJoinReq).subscribe((forkJoinResp) => {
            const segment = forkJoinResp[0];
            const reductionProposal = forkJoinResp[1];
            const listLD = forkJoinResp[2];
            const listMD = forkJoinResp[3];

            // Xếp hạng tín dụng
            this.info.customer.scoreValue = reductionProposal?.ketQuaXhtd;
            const seg = _.get(segment, 'content') || [];
            this.commonSegment = seg.map(i => {
              return {
                name: i?.name,
                code: i?.description
              };
            });
            this.info.customer.customerTypeDisplay = 'Khách hàng mới';
            this.info.customer.customerTypeCode = 'NEW';
            this.info.customer.isNew = true;
            this.isLoaded = true;
            let segmentCode;
            if (this.checkPotentialCustomerSegment) {
              // ....
              const segmentResp = forkJoinResp[4];
              if (segmentResp?.status === 'OK' && !Utils.isEmpty(segmentResp?.data)) {
                segmentCode = _.head(segmentResp?.data)?.cstSegStd;
                this.info.customer.segment = this.commonSegment.find(i => {
                  return i?.code === segmentCode;
                })?.name;
                this.info.customer.segmentCode = segmentCode;
              } else {
                this.checkPotentialCustomerSegment = false;
              }
            }
            this.form.controls.segment.setValue(segmentCode);
            // SME. Đối với KH tín dụng, nếu không trả gia trị TOI hiện hữu thì hiển thị TOI cam kết và nút tính TOI
            this.displayConfirmInfo = this.checkConfirmInfo;
            if (this.isUpdate && this.updateObj) {
              let segmentCode;
              if (this.checkPotentialCustomerSegment) {
                // ....
                const segmentResp = forkJoinResp[4];
                if (segmentResp?.status === 'OK' && !Utils.isEmpty(segmentResp?.data)) {
                  segmentCode = _.head(segmentResp?.data)?.cstSegStd;
                  this.info.customer.segment = this.commonSegment.find(i => {
                    return i?.code === segmentCode;
                  })?.name;
                  this.info.customer.segmentCode = segmentCode;
                }
              } else {
                segmentCode = this.commonSegment.find(item => {
                  return item?.code === this.updateObj?.customerSegment;
                })?.code;
              }
              this.form.controls.segment.setValue(segmentCode);
              if (this.updateObj?.customerName) {
                this.info.customer.customerName = this.updateObj?.customerName;
              }
              if (this.updateObj?.taxCode) {
                this.info.customer.taxCode = this.updateObj?.taxCode;
              }
              if (this.updateObj?.businessRegistrationNumber) {
                this.info.customer.businessRegistrationNumber = this.updateObj?.businessRegistrationNumber;
              }
              const body = {
                customerCode: '',
                taxCode: this.updateObj?.taxCode.trim()
              };
              this.reductionProposalApi.findPotentialCustomer(body).pipe(
                finalize(() => {
                })
              ).subscribe((data) => {
                if (!data.code) {
                  this.info.customer.customerCode = '';
                  this.info.customer.customerName = data?.fullName;
                  this.info.customer.taxCode = data?.taxCode;
                  this.info.customer.businessRegistrationNumber = data?.businessRegistrationNumber;
                  this.info.customer.leadCode = data?.leadCode;
                  if (this.divisionCodeOfUser.includes('SME')) {
                    this.info.customer.customerTypeMerge = 'SME';
                  } else if (this.divisionCodeOfUser.length > 0) {
                    this.info.customer.customerTypeMerge = this.divisionCodeOfUser[0];
                  }
                  this.reductionProposalApi.info.next(this.info);
                } else {
                }
                if (this.updateObj?.reductionProposalAuthorizationDTOList.length > 0) {
                  this.checkAuthDataChange(false);
                }
              }, (e) => {
                this.messageService.warn(e?.error?.description);
              });
            }
            const dataReturn = {
              customerInfo: '',
              reductionProposal,
              listLD,
              listMD
            };
            this.dataReduction.emit(dataReturn);
            this.changeLoading.emit(false);
            this.reductionProposalApi.info.next(this.info);
          });
        }
        // init update data
        if (this.isUpdate && this.updateObj) {
          this.form.controls.nimCurrent.setValue(this.updateObj?.nimExisting);
          this.form.controls.nimConfirm.setValue(this.updateObj?.nimConfirm);
          this.info.customer.nimCurrent = this.updateObj?.toiExisting;
          this.info.customer.nimConfirm = this.updateObj?.dtttrrExisting;

          this.form.controls.dtttrr.setValue(this.updateObj?.dtttrrConfirm);
          this.form.controls.dtttrrDisplay.setValue(this.convertDtttrr(this.updateObj?.dtttrrConfirm));
          this.form.controls.toi.setValue(this.updateObj?.toiConfirm);
          if (this.updateObj?.monthApply) {
            this.form.controls.monthApply.setValue(+this.updateObj?.monthApply);
          }
          if (this.updateObj?.monthConfirm) {
            this.form.controls.monthConfirm.setValue(this.updateObj?.monthConfirm);
          }
          this.info.customer.toiCurrent = this.updateObj?.toiExisting;
          this.info.customer.dtttrr = this.updateObj?.dtttrrExisting;
          // SME. Đối với KH tín dụng, nếu không trả gia trị TOI hiện hữu thì hiển thị TOI cam kết và nút tính TOI
          this.displayConfirmInfo = this.info.customer.customerCreditType !== 'Phi tín dụng' && this.blockCode === Division.SME && Utils.isNull(this.updateObj?.toiExisting) ? true : this.checkConfirmInfo;
          this.form.controls.caseSuggest.setValue(this.updateObj?.type === 1 ? 'INTEREST' : 'FEE');
          if (!this.checkPotentialCustomerSegment) {
            const segmentCode = this.commonSegment.find(item => {
              return item?.name.toLowerCase() === this.updateObj?.customerSegment.toLowerCase();
            })?.code;
            this.form.controls.segment.setValue(segmentCode);
          }

          let scopeType;
          switch (this.updateObj?.scopeType) {
            case 0:
              scopeType = 'ALL';
              break;
            case 1:
              scopeType = 'OPTION_CODE';
              break;
            case 2:
              scopeType = 'LD_CODE';
              break;
            case 3:
              scopeType = 'UNKNOWN';
              break;
            case 4:
              scopeType = 'MD_CODE';
              break;
          }
          this.form.controls.scopeApply.setValue(scopeType);
          this.initTOI();
          this.initInterestData();
          this.initAuthData();
          if (this.updateObj?.reductionProposalAuthorizationDTOList.length > 0) {
            this.checkAuthDataChange(false);
          }
          this.initFile();
          this.reductionProposalApi.info.next(this.info);
          // this.changeLoading.emit(false);
        }
        // check isRM

        this.branchApi.getBranchesByFunction({
          rsId: this.objFunction.rsId,
          scope: Scopes.VIEW
        }).pipe(
          finalize(() => {
          })
        ).subscribe((data) => {
          const branchs = data ? data : [];
          this.isRM = branchs.length === 0;
        }, (e) => {
          this.messageService.warn(e?.error?.description);
        });
        if (!this.form.controls.scopeApply.value) {
          this.form.controls.scopeApply.setValue('ALL');
        }
      }
    });

    // Phí
    if (caseSuggest === 'FEE') {
      if (this.info.customer?.existed) {
        // KHHH
        this.reductionProposalApi.getCustomerCredit({
          cif: customerCode || '',
          taxId: '',
          nationalId: this.isKhdn ? taxCode || '' : '',
          lob: this.blockCode
        }).subscribe((customerCreditResp: any) => {
          if (customerCreditResp?.status === 200) {
            this.info.customer.customerCreditType = customerCreditResp?.data?.customerType;
            this.form.controls.monthConfirm.clearValidators();
            if (this.info.customer.customerCreditType !== 'Phi tín dụng' && this.isKhdn) {
              this.form.controls.monthConfirm.setValidators(CustomValidators.required);
            }
            if (this.info.customer.customerCreditType !== 'Phi tín dụng' && this.blockCode !== Division.FI) {
              this.form.controls.monthConfirm.setValue(6);
            }
            this.form.controls.monthConfirm.updateValueAndValidity();
            this.checkConfirmInfo = (this.blockCode !== Division.SME && this.info.customer.customerCreditType !== 'Phi tín dụng') || this.blockCode === Division.FI || this.blockCode === Division.CIB;
          } else {
            this.checkConfirmInfo = this.blockCode === Division.FI || this.blockCode === Division.CIB;
          }
          console.log('getCustomerCredit');
          this.customValidator();
          this.subject.next(1);
        }, (err) => {
          this.checkConfirmInfo = this.blockCode === Division.FI || this.blockCode === Division.CIB;
          this.customValidator();
          this.subject.next(1);
        });
      } else {
        // KHTN
        this.info.customer.customerCreditType = 'Phi tín dụng';
        this.checkConfirmInfo = this.blockCode === Division.FI || this.blockCode === Division.CIB;
        this.customValidator();
        this.subject.next(1);
      }
    } else {
      // Lãi
      this.customValidator();
      this.subject.next(1);
    }
  }

  initTOI() {
    const toi = this.updateObj.reductionProposalTOIDTO;
    this.info.toi = {
      duNoNganHanBq: toi?.dunoBqSd,
      duNoNganHanBqUnit: toi?.dunoBqNim,
      duNoTdhBq: toi?.dunoTdhBqSd,
      duNoTdhBqUnit: toi?.dunoTdhBqNim,
      coKyHanBq: toi?.ckhBqSd,
      coKyHanBqUnit: toi?.ckhBqNim,
      khongKyHanBq: toi?.kkhBqSd,
      khongKyHanBqUnit: toi?.kkhBqNim,
      thuBl: toi?.tblDsSd,
      thuBlUnit: toi?.tblNim,
      thuTTQT: toi?.tttqtDsSd,
      thuTTQTUnit: toi?.tttqtMpqd,
      thuFx: toi?.tfxDsSd,
      thuFxUnit: toi?.tfxMpqd,
      thuBanca: toi?.thuBancaDsSd,
      thuKhac: toi?.thuKhacDsSd,
      dtttrr: this.updateObj?.dtttrrConfirm,
      toi: this.updateObj?.toiConfirm,
      // Hieu qua
      ckhBqHq: toi?.ckhBqHq,
      dunoBqHq: toi?.dunoBqHq,
      dunoTdhBqHq: toi?.dunoTdhBqHq,
      kkhBqHq: toi?.kkhBqHq,
      tblHq: toi?.tblHq,
      tfxHq: toi?.tfxHq,
      thuBanCaHq: toi?.thuBanCaHq,
      thuKhacHq: toi?.thuKhacHq,
      tttqtHq: toi?.tttqtHq
    };
    this.reductionProposalApi.info.next(this.info);
  }

  initAuthData() {
    let lastAuth = '';
    let lastApproveTitle = '';
    let lastPiority = 0;
    const auth = this.updateObj.reductionProposalAuthorizationDTOList;
    this.info.authorization = auth?.map(item => {
      if (item?.priority > lastPiority) {
        lastAuth = item?.fullName;
        lastApproveTitle = item?.titleCategory;
        lastPiority = item?.priority;
      }
      return {
        fullName: item?.fullName,
        note: item?.note,
        priority: item?.priority,
        userName: item?.userName,
        titleCategory: item?.titleCategory,
        status: item?.status,
        typeSignature: item?.typeSignature,
        reductionProposalCode: item?.reductionProposalCode,
        id: item?.id
      };
    });
    this.form.controls.lastApprove.setValue(lastAuth);
    this.form.controls.lastApproveTitle.setValue(lastApproveTitle);
    this.form.controls.lastApproveFull.setValue(lastApproveTitle + ' - ' + lastAuth);

    this.reductionProposalApi.info.next(this.info);
  }

  checkAuthDataChange(isSignSubmit) {
    return new Promise((resolve, reject) => {
      const customer: any = this.info?.customer;
      let hasChanged = false;
      let maxSelectedPiority = -1;
      const { toi } = this.form.getRawValue();
      if (this.info.type === 1 && (!toi || toi === 0) && this.info.customer.isNew) {
        this.messageService.warn('Vui lòng tính TOI');
        return;
      }
      const listAuth = isSignSubmit ? this.reductionProposalApi.info.value.authorization : this.updateObj?.reductionProposalAuthorizationDTOList;
      listAuth.map(item => {
        if (item.priority > maxSelectedPiority) {
          maxSelectedPiority = item.priority;
        }
      });
      let reductPercent = 0;
      const maxReductPercentOfTypes = {};
      let required = 0;
      if (this.info.type !== 0) {
        // Validate chọn thẩm quyền đối với biểu lãi
        if (this.listData.length === 0) {
          this.messageService.warn('Vui lòng nhập thông tin giảm lãi suất');
          return;
        }
        this.listData.sort((a: any, b: any) => {
          return Number(b.data?.reductPercent) - Number(a.data?.reductPercent);
        });
        reductPercent = this.listData?.[0]?.data?.reductPercent;
        if (!reductPercent) {
          this.messageService.warn('Vui lòng nhập biên độ đề xuất');
          return;
        } else if (reductPercent < 0) {
          this.messageService.warn('Tỉ lệ giảm phải lớn hơn hoặc bằng 0');
          return;
        }
        maxReductPercentOfTypes['0'] = reductPercent;
      } else {
        // Validate chọn thẩm quyền đối với biểu phí
        if (this.listFeeData.length === 0) {
          this.messageService.warn('Vui lòng nhập thông tin giảm phí');
          return;
        }
        // Lấy tỉ lệ giảm lớn nhất nhóm theo phân loại phí
        this.listFeeData.forEach(item => {
          const chargeType = item.data?.chargeType;
          const rateCost = item.data?.rateCost || 0;
          const rateMin = item.data?.rateMin || 0;
          const rateMax = item.data?.rateMax || 0;
          const chargeTypeRate = maxReductPercentOfTypes[chargeType] || 0;
          const rate = Math.max(rateCost, rateMin, rateMax, chargeTypeRate);
          maxReductPercentOfTypes[chargeType] = rate;

          required += rateCost === 0 && rateMin === 0 && rateMax === 0 ? 1 : 0;
        });
        if (required) {
          this.messageService.warn('Vui lòng nhập thông tin đề xuất giảm');
          return;
        }
      }

      let toiCommit;
      if (this.info.type === 1) {
        if (this.info.customer.isNew) {
          toiCommit = toi;
        } else {
          toiCommit = this.convertToiCurrent(this.info.customer.toiCurrent);
        }
      } else {
        if (this.blockCode !== Division.INDIV) {
          toiCommit = toi ? toi : this.convertToiCurrent(this.info.customer.toiCurrent);
        }
      }


      let department = customer.customerTypeMerge;
      if (!department) {
        if (this.divisionCodeOfUser.includes('SME')) {
          department = 'SME';
        } else if (this.divisionCodeOfUser.length > 0) {
          department = this.divisionCodeOfUser[0];
        }
      }

      let customerSegment;
      let customerType;
      if (department === Division.SME) {
        customerSegment = department + this.customer?.segmentCode;
        customerType = customer?.classification;
      } else if (department === Division.INDIV) {
        customerSegment = 'KHCN';
        customerType = customer?.segmentCode;
      } else {
        customerSegment = department;
        customerType = customer?.classification;
      }
      const reqBody = {};
      const body = this.getBodyByDivision(customerType, customerSegment, department, reductPercent, toiCommit);

      const forkJoinReq = [this.categoryService.getCommonCategory(CommonCategory.REDUCTION_PROPOSAL_AUTH_CONFIRM)
        .pipe(catchError(() => of(undefined)))];
      const forkJoinResp = ['cate'];
      const types = Object.keys(maxReductPercentOfTypes);
      types.forEach(type => {
        if (this.info.type !== 0) {
          const typeInterest = this.typeInterest.find(item => item.id === this.form.controls.typeInterest.value)?.name;
          const block = this.form.controls.typeInterest.value === 6 ? 'CIB' : department;
          const bodyInterest = this.getBodyByDivisionInterest(customerType, customerSegment, department, reductPercent, toiCommit, typeInterest, block);
          forkJoinReq.push(this.reductionProposalApi.getListAuthor(bodyInterest).pipe(catchError(() => of(undefined))));
        } else {
          body.discountPercent = maxReductPercentOfTypes[type];
          body.typeOfFee = 'Loại ' + type;
          forkJoinReq.push(this.reductionProposalApi.getListFeeAuthor({ ...body }).pipe(catchError(() => of(undefined))));
        }
        forkJoinResp.push('authors' + type);
        reqBody[type] = { ...body };
      });

      forkJoin(forkJoinReq).subscribe((forkJoinResp) => {
        const category = forkJoinResp[0];
        let titles = {};
        if (category.content) {
          let normalCIB = '';
          if (this.form.controls.typeInterest.value === 6) {
            normalCIB = '-CIB';
          }
          const rawTitles = category.content?.find(item => item?.code === this.info.type + '-' + this.blockCode + normalCIB)
            ?.value || '{}';
          titles = JSON.parse(rawTitles);
        }
        let authors = forkJoinResp[1];
        if (types.length > 1) {
          // Tính số lượng cấp phê duyệt theo phân loại phí
          const checkAuth = {};
          for (let index = 1; index < forkJoinResp.length; index++) {
            const auth = forkJoinResp[index];
            if (auth?.status === 200) {
              auth?.data?.forEach(itm => {
                checkAuth[index] = Math.max(itm?.orderSigning, checkAuth[index] || 0);
              });
            }
          }
          // Lấy danh sách thẩm quyền có nhiều cấp phê duyệt nhất
          let maxOrderSigning = 0;
          Object.keys(checkAuth).map(idx => {
            const orderSigning = checkAuth[idx];
            if (maxOrderSigning < orderSigning) {
              maxOrderSigning = orderSigning;
              authors = forkJoinResp[idx];
            }
          });
        }

        if (authors?.status === 200) {
          const tmp = {};
          // Group user has same piority
          authors.data.map(item => {
            const piority = item?.orderSigning;
            const newObj = {
              fullName: item?.name,
              note: '',
              priority: piority,
              userName: item?.user,
              titleCategory: this.getTitle(piority),
              signedTime: '',
              status: '',
              typeSignature: '',
              moDocId: '',
              reductionProposalCode: ''
            };

            if (tmp[piority] === undefined) {
              tmp[piority] = { childs: [newObj], num: newObj.priority, titleCategory: newObj?.titleCategory };
            } else if (tmp[piority].childs !== undefined) {
              tmp[piority].childs.push(newObj);
            } else {
              tmp[piority].childs = [];
              tmp[piority].childs.push(newObj);
            }
          });

          const keys = Object.keys(tmp);
          if (keys.length - 1 != maxSelectedPiority) {
            hasChanged = true;
          } else {
            for (let index = 0; index < listAuth.length; index++) {
              const item = listAuth[index];
              const currentAuth: any[] = tmp[item.priority].childs;
              const found = currentAuth.find(obj => {
                return obj.userName === item.userName;
              });
              if (!found) {
                hasChanged = true;
                break;
              }
            }
          }
          if (hasChanged) {
            const dialogRef = this.dialog.open(WarningAuthorizationModalComponent, {
              width: 'auto',
              panelClass: 'justify-content-center'
            });
            dialogRef.afterClosed().subscribe((res) => {
              if (res) {
                this.clearAuthData();
                this.openAuthPopup();
              }
            });
          }
          resolve(hasChanged);
        }
      });
    });
  }

  getTitle(piority: any) {
    let title = '';
    switch (piority) {
      case 0:
        title = 'CBQL trực tiếp';
        break;
      case 1:
        title = 'BLĐ Chi nhánh';
        break;
      case 2:
        title = 'Giám đốc vùng';
        break;
      case 3:
        title = 'Phó giám đốc Khối';
        break;
      case 4:
        title = 'Giám đốc khối/TVBĐH/P.TGĐ';
        break;
      case 5:
        title = 'Tổng giám đốc';
        break;
    }
    return title;
  }

  initFile() {
    const files = this.updateObj.reductionProposalFileDTOList;
    const uploaded = [];
    files.map(item => {
      uploaded.push({
        fileName: item.fileName,
        fileId: item.fileId
      });
    });
    this.info.files = uploaded;
    this.reductionProposalApi.info.next(this.info);
  }


  initInterestData() {
    if (this.updateObj?.type === 1) {// Thông tin giảm lãi
      this.listData = this.updateObj.reductionProposalDetailsDTOList.map(item => {
        return {
          data: {
            code: this.updateObj?.scopeType === 1 ? item?.planCode : this.updateObj?.scopeType === 2 ? item?.ldCode : '',
            optionCode: item?.planCode,
            ldCode: item?.ldCode,
            productCode: item?.itemCode,
            name: item?.itemName,
            currency: item?.interestUnit,
            adjustmentPeriod: item?.interestAdjustPeriod,
            period: item?.interestLoanPeriod,
            referenceInterestRate: item?.interestReference,
            baseMargin: item?.interestAmplitude,
            maximumMarginReduction: item?.interestMaxReduction ? item?.interestMaxReduction : null,
            loanRate: item?.interestValue,
            marginSuggest: item?.interestProposedAmplitude,
            loanSuggest: item?.interestProposedValue,
            reductPercent: ((Number(item?.interestAmplitude) - Number(item?.interestProposedAmplitude)) / Number(item?.interestAmplitude) * 100).toFixed(2),
            minInterestRate: item?.minInterestRate,
            rateMinimum: item?.minInterestRateOffer,
            rateInterestReduction: ((Number(item?.minInterestRate) - Number(item?.minInterestRateOffer)) / Number(item?.minInterestRate) * 100).toFixed(2),
          }
        };
      });
    } else { // Thông tin giảm phí
      this.listFeeData = this.updateObj.reductionProposalDetailsDTOList.map(item => {
        const code = this.updateObj?.scopeType === 1 ? item?.planCode : this.updateObj?.scopeType === 2
          ? item?.ldCode : this.updateObj?.scopeType === 4 ? item?.mdCode : '';
        const dataObj = {
          code,
          chargeCode: item?.itemCode,
          optionCode: item?.planCode,
          ldCode: item?.ldCode,
          mdCode: item?.mdCode,
          productCode: code + item?.itemCode + item?.interestUnit,
          descriptionVn: item?.itemName,
          calcType: item?.calcType,
          chgAmtFrom: item?.ratioCost, // Biểu phí: Tỉ lệ phí/ Mức phí
          minCharge: item?.minimumCost, // Biểu phí: Phí tối thiểu
          maxCharge: item?.maximumCost, // Biểu phí: Phí tối đa
          suggestCost: item?.ratioCostOffer, // Đề xuất: Tỉ lệ phí/Mức phí
          suggestMax: item?.maximumCostOffer, // Đề xuất: Phí tối đa
          suggestMin: item?.minimumCostOffer, // Đề xuất: Phí tối thiểu
          valueRefDoc: item?.valueRefDoc, // Ghi chú mức biểu phí
          chargeUnit: item?.chargeUnit, // Đơn vị tính
          chargeType: item?.interestType, // Phân loại phí
          currency: item?.interestUnit, // Phân loại phí
          chargeGroup: item?.chargeGroup // Nhóm loại phí
        };
        this.feeData.push(dataObj);
        return {
          data: dataObj
        };
      });
      this.listFeeData.forEach(item => {
        item = item.data;
        this.calculateReduction(item?.suggestCost, item, 'Cost');
        this.calculateReduction(item?.suggestMin, item, 'Min');
        this.calculateReduction(item?.suggestMax, item, 'Max');
        return item;
      });
    }
    const scopeApplyJson = JSON.parse(this.updateObj?.scopeApply);
    switch (this.updateObj?.scopeType) {
      case 0:
        if (this.info.type === 1) {
          this.interestData = scopeApplyJson;
        } else {
          this.feeData = scopeApplyJson;
        }
        break;
      case 1:
        this.optionCodeData = scopeApplyJson;
        break;
      case 2:
        this.ldCodeData = scopeApplyJson;
        break;
      case 4:
        this.mdCodeData = scopeApplyJson;
        break;
    }
    this.info.customer.chargeGroup = _.head(this.listFeeData)?.data?.chargeGroup || '';
  }

  openPopupCalculateTOI(): void {
    const dialogRef = this.dialog.open(ToiConfirmModalComponent);
    dialogRef.componentInstance.type = this.info?.type;
    dialogRef.componentInstance.blockCode = this.blockCode;
    dialogRef.afterClosed().subscribe(() => {
      this.info = this.reductionProposalApi?.info?.value;
      const toi = this.info?.toi;
      if (toi) {
        this.form.controls.toi.setValue(toi?.toi);
        this.form.controls.dtttrr.setValue(toi?.dtttrr);
        this.form.controls.dtttrrDisplay.setValue(this.convertDtttrr(toi?.dtttrr));
      }
      if (toi?.toiChanged && (this.info.type === 1 || (this.info.type === 0 && this.blockCode !== Division.FI))) {
        this.clearAuthData();
      }
    });
  }


  ngAfterViewInit() {
    this.form.controls.scopeApply.valueChanges.subscribe(() => {
      this.form.controls.typeInterest.setValue(1);
      this.reset();
    });

    this.form.controls.caseSuggest.valueChanges.subscribe((value) => {
      this.checkBrand(value);
      this.reset();
    });

    this.form.controls.segment.valueChanges.subscribe((value) => {
      this.info.customer.segment = this.commonSegment.find(i => {
        return i?.code === value;
      })?.name;
      this.info.customer.segmentCode = value;
    });

    this.form.controls.typeInterest.valueChanges.subscribe((value) => {
      this.reset();
      if (value === 6) {
        this.checkNormalCibForSme();
      }
    });
    this.dtc.detectChanges();
  }

  reset() {
    this.listData = [];
    this.interestData = [];
    this.optionCodeData = [];
    this.ldCodeData = [];
    this.clearAuthData();
    this.listFeeData = [];
    this.feeData = [];
    this.isShow = false;
  }

  checkBrand(caseSuggest) {
    if (this.info.type === 1 && this.customer.existed) {
      if (this.declareInfo.branchCode != this.currUser.branch && !this.isUpdate && this.existed) {
        this.messageService.warn('Khách hàng không thuộc chi nhánh quản lý');
        setTimeout(() => {
          this.router.navigateByUrl(functionUri.reduction_proposal);
        }, 2000);
        return false;
      }
    }
    if (this.info.type === 0) {
      return this.declareInfo.branchCode === this.currUser.branch;
    }
    return true;
  }

  ngAfterContentChecked(): void {
    // Called after every check of the component's or directive's content.
    // Add 'implements AfterContentChecked' to the class.
    this.dtc.detectChanges();
  }

  onTabChanged() {

  }

  mapTable(type: string, interestItem) {
    const division = this.info.customer.customerTypeMerge;
    const segmentCode = this.info.customer.segmentCode;
    const scoreValue = this.info.customer.scoreValue;
    const rowData = [];
    const currentSize = this.listData.length;

    let existed;
    switch (type) {
      case 'interest':
        existed = this.listData.map(item => item?.data?.productCode);
        this.interestData?.filter((item) => !existed.includes(item?.productCode)).map((item) => {
          rowData.push(
            {
              data: {
                code: '',
                productCode: item?.productCode,
                name: item.name,
                currency: item.currency,
                adjustmentPeriod: item.adjustmentPeriod,
                period: item.period,
                referenceInterestRate: Number(item.referenceInterestRate).toFixed(2),
                baseMargin: Number(item.baseMargin).toFixed(2),
                maximumMarginReduction: item.maximumMarginReduction ? Number(item.maximumMarginReduction || 0).toFixed(2) : null,
                loanRate: (Number(item.baseMargin) + Number(item.referenceInterestRate)).toFixed(2),
                marginSuggest: Number(item.baseMargin).toFixed(2),
                loanSuggest: 0,
                reductPercent: 0,
                rateMinimum: item?.minInterestRate,
                rateInterestReduction: 0,
                minInterestRate: item?.minInterestRate
              }
            }
          );
        });
        this.listData = [...this.listData, ...rowData];
        break;
      case 'option':
        existed = this.listData.map(item => item?.data?.code);
        this.optionCodeData?.filter((item) => !existed.includes(item?.optionCode)).map((item) => {
          rowData.push(
            {
              data: {
                code: item.optionCode,
                productCode: null,
                name: null,
                currency: null,
                adjustmentPeriod: null,
                period: null,
                referenceInterestRate: null,
                baseMargin: null,
                maximumMarginReduction: null,
                loanRate: null,
                marginSuggest: null,
                loanSuggest: null,
                reductPercent: null,
                rateMinimum: null,
                rateInterestReduction: null,
                minInterestRate: item?.minInterestRate
              }
            }
          );
        });
        this.listData = [...this.listData, ...rowData];
        break;
      case 'ld':

        existed = this.listData.map(item => item?.data?.code);
        this.ldCodeData?.filter((item) => !existed.includes(item?.ldCode)).map((item) => {
          // Lấy thông tin biểu lãi dựa vào các chỉ số của LD


          this.reductionProposalApi.getInterestRate({
            adjustmentPeriod: item?.Y_TS_TDLS,
            currency: item?.NTE,
            customerSegment: division === 'CIB' ? division : division + segmentCode,
            period: item?.KYHANTHANG ? item?.KYHANTHANG?.toString().substr(0, item?.KYHANTHANG?.toString().length - 1) : '',
            rankingCredit: scoreValue ? scoreValue : 'BB',
            customerType: this.info.customer.classification
          }).subscribe((res) => {
            // API bị lỗi sẽ trả về định dạng object
            if (Object.prototype?.toString.call(res) === '[object Object]' || (res as []).length === 0) {
              this.messageService.error('Không tìm thấy dữ liệu biểu lãi');
            } else {
              const obj: any = res[0];
              this.listData = [...this.listData, ...[
                {
                  data: {
                    code: item.ldCode,
                    productCode: obj?.marginId,
                    name: 'LSTC cho vay',
                    currency: item?.NTE,
                    adjustmentPeriod: item?.Y_TS_TDLS,
                    period: item?.KYHANTHANG,
                    referenceInterestRate: item?.INTEREST_FIX,
                    baseMargin: item?.BIENDOLS,
                    maximumMarginReduction: obj?.maximumMarginReduction ? obj?.maximumMarginReduction : null,
                    loanRate: item?.LS,
                    marginSuggest: null,
                    loanSuggest: null,
                    reductPercent: null,
                    codeInterest: obj?.productCode
                  }
                }
              ]];
            }
          });
        });
      case 'option-interest':
        for (let index = 0; index < this.listData.length; index++) {
          const element = this.listData[index];
          if (interestItem?.code === element?.data?.code) {
            console.log(interestItem);
            this.listData[index] = {
              data: {
                code: interestItem?.code,
                productCode: interestItem?.productCode,
                name: interestItem.name,
                currency: interestItem.currency,
                adjustmentPeriod: interestItem.adjustmentPeriod,
                period: interestItem.period,
                referenceInterestRate: Number(interestItem.referenceInterestRate).toFixed(2),
                baseMargin: Number(interestItem.baseMargin).toFixed(2),
                maximumMarginReduction: interestItem?.maximumMarginReduction
                  ? Number(interestItem.maximumMarginReduction).toFixed(2) : null,
                loanRate: (Number(interestItem.baseMargin) + Number(interestItem.referenceInterestRate)).toFixed(2),
                marginSuggest: Number(interestItem.baseMargin).toFixed(2),
                loanSuggest: 0,
                reductPercent: 0,
                rateMinimum: interestItem?.minInterestRate,
                rateInterestReduction: 0,
                minInterestRate: interestItem?.minInterestRate
              }
            };
          }

        }
        break;
    }
    console.log(this.listData);
    if (currentSize !== this.listData.length) {
      this.clearAuthData();
    }
    this.form.controls.listData.setValue(this.listData);
    this.treeTableInterest.updateSerializedValue();
  }


  backStep(): void {
    // check tabindex
    if (this.isDeclareTab) {
      this.isDeclareTab = false;
    } else {
      this.router.navigateByUrl(functionUri.reduction_proposal);
    }
  }

  removeItem(data): void {
    const caseSuggest = this.form.controls.caseSuggest.value;
    const scopeApply = this.form.controls.scopeApply.value;
    if (caseSuggest === 'INTEREST') {
      if (scopeApply === 'ALL') {
        this.listData = this.listData?.filter(item => {
          return item?.data?.productCode !== data?.productCode;
        });
        this.interestData = this.interestData?.filter(item => {
          return item?.productCode !== data?.productCode;
        });
      } else if (scopeApply === 'UNKNOWN') {
        this.listData = this.listData?.filter(item => {
          return item?.data?.productCode !== data?.productCode;
        });
        this.interestData = this.interestData?.filter(item => {
          return item?.productCode !== data?.productCode;
        });
      } else {
        this.listData = this.listData?.filter(item => {
          return item?.data?.code !== data?.code;
        });
        this.interestData = this.interestData?.filter(item => {
          return item?.productCode !== data?.productCode;
        });
        if (scopeApply === 'OPTION_CODE') {
          this.optionCodeData = this.optionCodeData?.filter(item => {
            return item?.optionCode !== data?.code;
          });
        } else if (scopeApply === 'LD_CODE') {
          this.ldCodeData = this.ldCodeData?.filter(item => {
            return item?.ldCode !== data?.code;
          });
        }
      }
    } else if (caseSuggest === 'FEE') {
      if (['ALL', 'UNKNOWN'].includes(scopeApply)) {
        this.listFeeData = this.listFeeData?.filter(item => {
          return item?.data?.productCode !== data?.productCode;
        });
      } else {
        this.listFeeData = this.listFeeData?.filter(item => {
          if (item.data?.productCode) {
            return item?.data?.productCode !== data?.productCode;
          }
          return item?.data?.code !== data?.code;
        });
      }
      this.feeData = this.feeData?.filter(item => {
        return item?.productCode !== data?.productCode;
      });
      this.optionCodeData = this.optionCodeData?.filter(item => {
        return item?.optionCode !== data?.code;
      });
      this.ldCodeData = this.ldCodeData?.filter(item => {
        return item?.ldCode !== data?.code;
      });
      this.mdCodeData = this.mdCodeData?.filter(item => {
        return item?.ARRANGEMENT_NBR !== data?.code;
      });
    }
    this.clearAuthData();
  }

  changeMarginSuggest(event, data): void {
    const val = event.target.value;
    if (val) {
      data.loanSuggest = (Number(data.referenceInterestRate) + Number(val)).toFixed(2);
      data.reductPercent = ((Number(data.baseMargin) - Number(val)) / Number(data.baseMargin) * 100).toFixed(2);
      // check bieu uu dai linh hoat
      if ([2, 3, 4, 5].includes(this.form.controls.typeInterest.value) && Number(val) > data.baseMargin) {
        data.errorMessage = 'Biên độ đề xuất ≤ biên độ quy định';
        this.messageService.warn('Biên độ đề xuất ≤ biên độ quy định');
      } else if ([2, 3, 4, 5].includes(this.form.controls.typeInterest.value) && Number(val) < 0) {
        data.errorMessage = 'Biên độ đề xuất > 0';
        this.messageService.warn('Biên độ đề xuất > 0');

      } else {
        delete data.errorMessage;
      }
      if (data?.maximumMarginReduction) {
        const maximumMarginReduction = data?.maximumMarginReduction || 0;
        const baseMargin = data?.baseMargin || 0;
        const maximunMargin = (baseMargin - maximumMarginReduction).toFixed(2) || 0;
        if (Number(val) > 100) {
          data.errorMessage = 'Biên độ đề xuất phải nhỏ hơn hoặc bằng 100';
        } else if (Number(val) < +maximunMargin) {
          data.errorMessage = 'Biên độ đề xuất phải lớn hơn hoặc bằng mức (Biên độ theo biểu - Mức giảm biên tối đa)';
        } else {
          delete data.errorMessage;
        }
      }
      for (let index = 0; index < this.listData.length; index++) {
        const element = this.listData[index];
        if (element.data.code === data.code && element.data.productCode === data.productCode) {
          this.listData[index].data = data;
        }
      }
    } else {
      data.loanSuggest = null;
      data.reductPercent = null;
    }
    this.clearAuthData();
  }

  calculateMarginSuggest(event, row) {
    const val = event.target.value;
    if (Number(val) <= 0 || Number(val) > Number(row?.baseMargin) || !val) {
      row.marginSuggest = row?.baseMargin;
      row.loanSuggest = 0;
      row.reductPercent = 0;
    }
    this.treeTableInterest.updateSerializedValue();
  }

  openInterestModal(type: string, item) {
    // Get data
    let scoreValue = '', division = '';
    if (this.existed) {
      scoreValue = this.info.customer.scoreValue;
      division = this.info.customer.customerTypeMerge;
    } else {
      scoreValue = this.info.customer.scoreValue;
      if (this.divisionCodeOfUser.includes('SME')) {
        division = 'SME';
      } else if (this.divisionCodeOfUser.length > 0) {
        division = this.divisionCodeOfUser[0];
      }
    }
    const segmentCode = this.info.customer.segmentCode;
    if (!segmentCode && !this.existed) {
      this.messageService.warn('Vui lòng chọn phân khúc khách hàng');
      return;
    }
    const planType = this.form?.controls?.scopeApply?.value;
    const config = {
      selectionType: type == 'interest' && planType !== 'UNKNOWN' ? SelectionType.multi : SelectionType.single
    };
    const { toi } = this.form.getRawValue();
    if (this.info.type === 1 && (!toi || toi === 0) && this.info.customer.isNew) {
      this.messageService.warn('Vui lòng tính TOI');
      return;
    }
    if (this.isShow && this.form.controls.typeInterest.value !== 6 && this.form.controls.typeInterest.value !== 1) {
      if (this.validateConfirmException()) {
        return;
      }
    }
    let toiCommit;
    if (this.info.customer.isNew) {
      toiCommit = toi;
    } else {
      toiCommit = this.convertToiCurrent(this.info.customer.toiCurrent);
    }
    const modal = this.getExceptionOrNormal(config, division, segmentCode, toiCommit, scoreValue, type, item);
    modal.result
      .then((res) => {
        if (res) {
          this.isShow = res.showOrHide;
          if (this.isShow) {
            this.resetListTypeInterest();
          }
          if (res.listRemove.length > 0) {
            this.clearAuthData();
          }
          if (type === 'option') {
            if (res.listSelected.length > 0) {
              const selected = res.listSelected;
              item.productCode = selected[0]?.productCode;
              item.name = selected[0]?.name;
              item.currency = selected[0]?.currency;
              item.adjustmentPeriod = selected[0]?.adjustmentPeriod;
              item.period = selected[0]?.period;
              item.referenceInterestRate = Number(selected[0]?.referenceInterestRate).toFixed(2);
              item.baseMargin = Number(selected[0]?.baseMargin).toFixed(2);
              item.maximumMarginReduction = selected[0]?.maximumMarginReduction
                ? Number(selected[0]?.maximumMarginReduction).toFixed(2) : null;
              item.loanRate = (Number(selected[0]?.baseMargin) + Number(selected[0]?.referenceInterestRate)).toFixed(2);
              item.marginSuggest = null;
              item.loanSuggest = null;
              item.rateMinimum = null;
              item.rateInterestReduction = null;
              item.minInterestRate = selected[0]?.minInterestRate;
              this.mapTable('option-interest', item);
            }
          } else {
            if (planType === 'UNKNOWN') {
              this.interestData = _.uniqBy([...res.listSelected], (i) => i.productCode);
              this.listData = [];
              this.mapTable('interest', null);
            } else {
              this.interestData = _.uniqBy([...this.interestData, ...res.listSelected], (i) => i.productCode);
              this.interestData = _.remove(this.interestData, (i) => _.indexOf(res.listRemove, i.productCode) === -1);
              this.listData = _.remove(this.listData, (i) => _.indexOf(res.listRemove, i.data?.productCode) === -1);
              this.mapTable('interest', null);
            }
          }
        }
      })
      .catch(() => {
      });
  }


  validateConfirmException() {
    let checkValidate = false;
    if (this.confirmException[0].suggest === null) {
      this.confirmException[0].errorMess = 'Vui lòng nhập TOI';
      checkValidate = true;
    }
    if (this.confirmException[1].suggest === '') {
      this.confirmException[1].errorMess = 'Vui lòng chọn đối tượng KH';
      checkValidate = true;
    }
    if (this.confirmException[2].suggest === '') {
      this.confirmException[2].errorMess = 'Vui lòng chọn phân khúc';
      checkValidate = true;
    }
    return checkValidate;
  }

  resetListTypeInterest() {
    const id = this.form.controls.typeInterest.value;
    const idException = Number(id) + 1;
    if (this.isShow || this.resetTypeInterestCib) {
      if (this.dumpTypeInterest) {
        const dumpData = this.dumpTypeInterest.filter((item) => {
          return item.id !== id && !item.show;
        });
        const dumpTypeData = this.dumpTypeInterest.filter((item) => {
          return item.id !== id && item.show;
        });
        const data = this.AllTypeFee.filter((item) => {
          return item.id === idException && !item.show;
        });
        this.typeInterest = [...dumpTypeData, ...data, ...dumpData];
        if (this.info.customer.blockCode === Division.CIB) {
          this.typeInterest = this.typeInterest.filter(value => value.id !== 6);
        }
      } else {
        this.typeInterest = this.AllTypeFee.filter((item) => {
          return item.id !== id && item.show;
        });
        const data = this.AllTypeFee.filter((item) => {
          return item.id === idException && !item.show;
        });
        this.dumpTypeInterest = [...this.typeInterest, ...data];
        this.typeInterest = this.dumpTypeInterest;
        if (this.info.customer.blockCode === Division.CIB) {
          this.typeInterest = this.typeInterest.filter(value => value.id !== 6);
        }
      }
      this.form.controls.typeInterest.setValue(idException, {
        onlySelf: true,
        emitEvent: false
      });
    }
  }

  getExceptionOrNormal(config: any, division: any, segmentCode: any, toiCommit: any, scoreValue: any, type: any, item: any) {
    const modal = this.modal.open(InterestModalComponent, { windowClass: 'list__interest-modal' });
    if (this.isShow && this.form.controls.typeInterest.value !== 6 && this.form.controls.typeInterest.value !== 1) {
      modal.componentInstance.config = config;
      modal.componentInstance.customerObject = this.confirmException.find(value => value.code === 2)?.suggest; // Loại khách hàng
      modal.componentInstance.customerSegment = this.confirmException.find(value => value.code === 3)?.suggest; // Phân khúc
      modal.componentInstance.toi = this.confirmException.find(value => value.code === 1)?.suggest; // thông tin toi
      modal.componentInstance.scoreValue = scoreValue || 'BB'; // Xếp hạng tín dụng
      modal.componentInstance.productCode = this.info.customer.customerCode ? this.typeInterest.find(item => item.id === this.form.controls.typeInterest.value)?.code : 10; // product code sang MS
      modal.componentInstance.productType = this.info.customer.customerCode ? this.typeInterest.find(item => item.id === this.form.controls.typeInterest.value)?.productType : 'TT'; // product type sang MS
      modal.componentInstance.blockCode = this.form.controls.typeInterest.value === 6 ? 'CIB' : division; // Thông tin khối
      modal.componentInstance.reload();
      modal.componentInstance.listCode = type == 'interest' ? item.map(item => {
        return item.data.productCode;
      }) : [item?.productCode];
      return modal;
    } else {
      modal.componentInstance.config = config;
      modal.componentInstance.customerObject = this.info.customer.customerCode ? this.info.customer.classification : 'NORMAL'; // Loại khách hàng
      modal.componentInstance.customerSegment = division === 'CIB' ? division : division + segmentCode; // Phân khúc
      modal.componentInstance.toi = toiCommit; // thông tin toi
      modal.componentInstance.scoreValue = scoreValue || 'BB'; // Xếp hạng tín dụng
      modal.componentInstance.productCode = this.info.customer.customerCode ? this.typeInterest.find(item => item.id === this.form.controls.typeInterest.value)?.code : 10; // product code sang MS
      modal.componentInstance.productType = this.info.customer.customerCode ? this.typeInterest.find(item => item.id === this.form.controls.typeInterest.value)?.productType : 'TT'; // product type sang MSmodal.componentInstance.blockCode = this.form.controls.typeInterest.value === 6 ? 'CIB' : division; // Thông tin khối
      modal.componentInstance.reload();
      modal.componentInstance.listCode = type === 'interest' ? item.map(item => {
        return item.data.productCode;
      }) : [item?.productCode];
      return modal;
    }
  }

  checkNormalCibForSme() {
    this.changeLoading.emit(true);
    this.reductionProposalApi.checkNormalCIB(this.info.customer.customerCode).subscribe((data: any) => {
      this.changeLoading.emit(false);
      this.resetTypeInterestCib = data;
      if (data === 'true') {
        this.messageService.warn('Khách hàng không phải thông thường CIB');
        this.resetListTypeInterest();
        this.resetNormalCib();
      }
    }, (err) => {
      this.messageService.error(err.error.description);
      this.changeLoading.emit(false);
    });
  }

  resetNormalCib(){
    this.interestData = [];
    this.optionCodeData = [];
    this.ldCodeData = [];
    this.listData = [];
    this.listLD = [];
    this.treeTableInterest.updateSerializedValue();
  }

  openFeeModal(type: string, item) {
    // Get data
    let scoreValue = '', division = '';
    if (this.existed) {
      scoreValue = this.info.customer.scoreValue;
      division = this.info.customer.customerTypeMerge;
    } else {
      scoreValue = this.info.customer.scoreValue;
      if (this.divisionCodeOfUser.includes('SME')) {
        division = 'SME';
      } else if (this.divisionCodeOfUser.length > 0) {
        division = this.divisionCodeOfUser[0];
      }
    }
    const config = {
      selectionType: SelectionType.multi
    };

    const modal = this.modal.open(SelectFeeModalComponent, { windowClass: 'list__interest-modal' });
    modal.componentInstance.config = config;
    modal.componentInstance.department = this.blockCode;
    modal.componentInstance.isManage = this.customer?.isManage;
    modal.componentInstance.code = type != 'fee' ? item?.code : '';
    modal.componentInstance.isKhdn = this.isKhdn;
    modal.componentInstance.customerCreditType = this.info.customer.customerCreditType;
    modal.componentInstance.lastSelectedType = _.head(this.feeData)?.chargeGroup || '';
    modal.componentInstance.listCode =
      type == 'fee'
        ? item.map(item => {
          return item.data.productCode;
        })
        : this.feeData?.filter(elm => elm?.code === item.code).map(elm => elm.productCode);
    modal.componentInstance.reload(0);
    modal.result
      .then((res) => {
        if (res) {
          this.feeData = _.uniqBy([...this.feeData, ...res.listSelected], (i) => i.productCode);
          this.feeData = _.remove(this.feeData, (i) => _.indexOf(res.listRemove, i.productCode) === -1);
          this.listFeeData = _.remove(this.listFeeData, (i) => _.indexOf(res.listRemove, i.data?.productCode) === -1);
          const currentCodes = this.listFeeData.map(i => i?.data?.code);
          this.mdCodeData = this.mdCodeData
            .filter(i => currentCodes.includes(i?.ARRANGEMENT_NBR));// _.remove(this.mdCodeData, (i) => currentCodes.includes(i?.mdCode));
          this.ldCodeData = this.ldCodeData
            .filter(i => currentCodes.includes(i?.ldCode)); // _.remove(this.ldCodeData, (i) => currentCodes.includes(i?.ldCode));
          this.optionCodeData = this.optionCodeData.filter(i => currentCodes.includes(i?.optionCode));
          this.info.customer.chargeGroup = res.listSelected[0] ? res.listSelected[0].chargeGroup : null;
          // Xoa tham quyen khi co thay doi
          if (res.listRemove.length > 0) {
            this.clearAuthData();
          }
          if (type === 'option') {
            this.mapFeeTable('selected-fee');
          } else {
            // Chọn biểu phí cho Tất cả phương án, Không xác định
            this.mapFeeTable('fee');
          }
        }
      })
      .catch(() => {
      });
  }

  mapFeeTable(type: string) {
    const rowData = [];
    const currentSize = this.listFeeData.length;

    let existed;
    switch (type) {
      case 'fee':
        existed = this.listFeeData.map(item => item?.data?.productCode);
        this.feeData?.filter(item => !existed.includes(item?.productCode)).map(item => {
          item.rateCost = item?.chgAmtFrom ? 0 : null;
          item.rateMin = item?.minCharge ? 0 : null;
          item.rateMax = item?.maxCharge ? 0 : null;
          item.suggestCost = item?.chgAmtFrom ? this.formatCurrency(item?.chgAmtFrom) : item?.chgAmtFrom;
          item.suggestMin = item?.minCharge ? this.formatCurrency(item?.minCharge) : item?.minCharge;
          item.suggestMax = item?.maxCharge ? this.formatCurrency(item?.maxCharge) : item?.maxCharge;
          rowData.push(
            {
              data: item
            }
          );
        });
        this.listFeeData = [...this.listFeeData, ...rowData];
        break;
      case 'option':
        existed = this.listFeeData.map(item => item?.data?.code);
        this.optionCodeData?.filter((item) => !existed.includes(item?.optionCode)).map((item) => {
          rowData.push(
            {
              data: {
                code: item.optionCode
              }
            }
          );
        });
        this.listFeeData = [...this.listFeeData, ...rowData];
        break;
      case 'ld':
        existed = this.listFeeData.map(item => item?.data?.code);
        this.ldCodeData?.filter((item) => !existed.includes(item?.ldCode)).map((item) => {
          rowData.push(
            {
              data: {
                code: item.ldCode
              }
            }
          );
        });
        this.listFeeData = [...this.listFeeData, ...rowData];
        break;
      case 'md':
        existed = this.listFeeData.map(item => item?.data?.code);
        this.mdCodeData?.filter((item) => !existed.includes(item?.ARRANGEMENT_NBR)).map((item) => {
          rowData.push(
            {
              data: {
                code: item.ARRANGEMENT_NBR
              }
            }
          );
        });
        this.listFeeData = [...this.listFeeData, ...rowData];
        break;
      default:
        existed = this.listFeeData.map(item => item?.data?.productCode);
        const codes = this.feeData?.filter(item => !existed.includes(item?.productCode)).map(item => {
          item.rateCost = item?.chgAmtFrom ? 0 : null;
          item.rateMin = item?.minCharge ? 0 : null;
          item.rateMax = item?.maxCharge ? 0 : null;
          item.suggestCost = item?.chgAmtFrom ? this.formatCurrency(item?.chgAmtFrom) : item?.chgAmtFrom;
          item.suggestMin = item?.minCharge ? this.formatCurrency(item?.minCharge) : item?.minCharge;
          item.suggestMax = item?.maxCharge ? this.formatCurrency(item?.maxCharge) : item?.maxCharge;
          rowData.push(
            {
              data: item
            }
          );
          return item?.code;
        });
        this.listFeeData = this.listFeeData.filter((item: any) => {
          return !codes.includes(item.data?.code) || item.data?.productCode;
        });
        this.listFeeData = [...this.listFeeData, ...rowData];

    }

    console.log('listFeeData :', this.listFeeData);
    if (currentSize !== this.listFeeData.length) {
      this.clearAuthData();
    }
    this.form.controls.listFeeData.setValue(this.listFeeData);
  }

  calculateReduction(value, item, type): void {
    value = value?.toString();
    delete item['error' + type];
    if (Utils.isNotNull(value) && Utils.isNotEmpty(value)) {
      let hasErr = 0;
      let hasErr2 = 0;
      let errorMessage;

      const val = Number(value.replaceAll(',', ''));
      // format input
      let formated = this.formatCurrency(val);
      let check = value.match(/\.$/);
      if (check && !formated.includes('.')) {
        formated += '.';
      }
      check = value.match(/0+$/);
      if (formated.includes('.') && check) {
        formated += check[0];
      }
      check = value.match(/\.0+$/);
      if (!formated.includes('.') && check) {
        formated += check[0];
      }
      switch (type) {
        case 'Cost':
          item.rateCost = val ? Number(((item?.chgAmtFrom - val) / item?.chgAmtFrom * 100).toFixed(2)) : val === 0 ? 100 : null;
          if (val > item?.chgAmtFrom) {
            hasErr++;
          }
          item.suggestCost = formated;
          break;
        case 'Min':
          item.rateMin = val ? Number(((item?.minCharge - val) / item?.minCharge * 100).toFixed(2)) : val === 0 ? 100 : null;
          if (val > item?.minCharge) {
            hasErr++;
          }
          if (Utils.isNotNull(item?.suggestMax) && val > Number(item?.suggestMax?.toString().replaceAll(',', ''))) {
            hasErr2++;
            errorMessage = 'Phí tối thiểu đề xuất phải <= Phí tối đa đề xuất';
          }
          item.suggestMin = formated;
          break;
        case 'Max':
          item.rateMax = val ? Number(((item?.maxCharge - val) / item?.maxCharge * 100).toFixed(2)) : val === 0 ? 100 : null;
          if (val > item?.maxCharge) {
            hasErr++;
          }
          if (Utils.isNotNull(item?.suggestMin) && val < Number(item?.suggestMin?.toString().replaceAll(',', ''))) {
            hasErr2++;
            errorMessage = 'Phí tối đa đề xuất phải >= Phí tối thiểu đề xuất';
          }
          item.suggestMax = formated;
          break;
      }
      if (hasErr) {
        item['error' + type] = 'Giá trị phải nhỏ hơn hoặc bằng biểu theo quy định';
      } else if (hasErr2) {
        item['error' + type] = errorMessage;
      }
      if (hasErr2 == 0) {
        delete item.errorMax;
        delete item.errorMin;
      }
    }
    this.clearAuthData();
  }

  displayWithUnit(val, unit1, unit2) {
    let str = '--';
    const unit = unit1 && unit1.includes('%') ? '%' : unit2 ? '/' + unit2 : '';
    if (!Utils.isNull(val)) {
      str = this.formatCurrency(val) + unit;
    }
    return str;
  }

  calculateReductionOnBlur(event, rowData, type) {
    event.preventDefault();
    const val = event.target.value;
    const valNum = Number(val.replaceAll(',', ''));

    if (Utils.isEmpty(val)) {
      setTimeout(() => {
        switch (type) {
          case 'Cost':
            rowData.suggestCost = this.formatCurrency(rowData?.chgAmtFrom);
            break;
          case 'Min':
            rowData.suggestMin = this.formatCurrency(rowData?.minCharge);
            break;
          case 'Max':
            rowData.suggestMax = this.formatCurrency(rowData?.maxCharge);
        }
        rowData['rate' + type] = 0;
      }, 200);
    } else {
      switch (type) {
        case 'Cost':
          rowData.suggestCost = this.formatCurrency(valNum);
          break;
        case 'Min':
          rowData.suggestMin = this.formatCurrency(valNum);
          break;
        case 'Max':
          rowData.suggestMax = this.formatCurrency(valNum);
      }
    }
  }

  keyPress(event, rowData, type) {
    const val = event.key;
    let checkVal;
    switch (type) {
      case 'Cost':
        checkVal = rowData.suggestCost;
        break;
      case 'Min':
        checkVal = rowData.suggestMin;
        break;
      case 'Max':
        checkVal = rowData.suggestMax;
    }

    const fnKey = ['.', 'Delete', 'Backspace', 'ArrowLeft', 'ArrowRight'].includes(val);

    if (
      (isNaN(val) && !fnKey) ||
      (val === '.' && checkVal?.includes('.')) ||
      (checkVal.replaceAll(',', '').length >= 15 && !fnKey)
    ) {
      event.preventDefault();
    }
  }

  openOptionCodeModal() {
    const config = {
      selectionType: SelectionType.multi
    };
    
    const modal = this.modalService.open(OptionCodeModalComponent, { windowClass: 'list__optioncode-modal' });
    modal.componentInstance.config = config;
    modal.componentInstance.customerCode = this.info?.customer?.customerCode; // "";
    modal.componentInstance.businessRegistrationNumber = this.info?.businessRegistrationNumber || this.info?.taxCode; 
    modal.componentInstance.idCard = this.info?.customer?.idCard;
    modal.componentInstance.type = this.info?.type;
    modal.componentInstance.blockCode = this.blockCode;
    modal.componentInstance.listCode = this.optionCodeData?.map((item) => item.optionCode);
    modal.result
      .then((res) => {
        if (res) {
          if (res.listRemove.length > 0) {
            this.clearAuthData();
          }
          this.optionCodeData = _.uniqBy([...this.optionCodeData, ...res.listSelected], (i) => i.optionCode);
          this.optionCodeData = _.remove(this.optionCodeData, (i) => _.indexOf(res.listRemove, i.optionCode) === -1);
          if (this.info.type === 1) {
            // Lãi
            this.listData = _.remove(this.listData, (i) => _.indexOf(res.listRemove, i.data?.code) === -1);
            this.mapTable('option', null);
          } else {
            // Phí
            this.listFeeData = _.remove(this.listFeeData, (i) => _.indexOf(res.listRemove, i.data?.code) === -1);
            this.mapFeeTable('option');
          }
        }
      })
      .catch(() => {
      });
  }

  openLdCodeModal() {
    const config = {
      selectionType: SelectionType.multi
    };
    const modal = this.modalService.open(LdCodeModalComponent, { windowClass: 'list__ldcode-modal' });
    modal.componentInstance.config = config;
    modal.componentInstance.listLD = this.listLD;
    modal.componentInstance.listCode = this.ldCodeData?.map((item) => item.ldCode);
    modal.componentInstance.customerCode = this.customer?.customerCode;
    modal.componentInstance.reload();
    modal.result
      .then((res) => {
        if (res) {
          if (res.listRemove.length > 0) {
            this.clearAuthData();
          }
          this.ldCodeData = _.uniqBy([...this.ldCodeData, ...res.listSelected], (i) => i.ldCode);
          this.ldCodeData = _.remove(this.ldCodeData, (i) => _.indexOf(res.listRemove, i.ldCode) === -1);
          if (this.info.type === 1) {
            // Lãi
            this.listData = _.remove(this.listData, (i) => _.indexOf(res.listRemove, i.data?.code) === -1);
            this.mapTable('ld', null);
          } else {
            // Phí
            this.listFeeData = _.remove(this.listFeeData, (i) => _.indexOf(res.listRemove, i.data?.code) === -1);
            this.mapFeeTable('ld');
          }
        }
      })
      .catch(() => {
      });
  }

  openMdCodeModal() {
    const config = {
      selectionType: SelectionType.multi
    };
    const modal = this.modalService.open(SelectMdModalComponent, { windowClass: 'list__mdcode-modal' });
    modal.componentInstance.config = config;
    modal.componentInstance.listMD = this.listMD;
    modal.componentInstance.listCode = this.mdCodeData?.map((item) => item.ARRANGEMENT_NBR);
    modal.componentInstance.customerCode = this.customer?.customerCode;
    modal.componentInstance.reload();
    modal.result
      .then((res) => {
        if (res) {
          if (res.listRemove.length > 0) {
            this.clearAuthData();
          }
          this.mdCodeData = _.uniqBy([...this.mdCodeData, ...res.listSelected], (i) => i.ARRANGEMENT_NBR);
          this.mdCodeData = _.remove(this.mdCodeData, (i) => _.indexOf(res.listRemove, i.ARRANGEMENT_NBR) === -1);
          this.listData = _.remove(this.listData, (i) => _.indexOf(res.listRemove, i.data?.code) === -1);
          this.mapFeeTable('md');
        }
      })
      .catch(() => {
      });
  }


  // lấy thẩm quyền phê duyệt
  openAuthPopup() {
    const { toi } = this.form.getRawValue();
    if (this.info.type === 1 && (!toi || toi === 0) && this.info.customer.isNew) {
      this.messageService.warn('Vui lòng tính TOI');
      return;
    }
    let reductPercent = 0;
    const maxReductPercentOfTypes = {};
    let required = 0;
    let lowerThanZero = 0;
    let hasTTTM;

    if (this.info.type !== 0) {
      // Validate chọn thẩm quyền đối với biểu lãi
      if (this.listData.length === 0) {
        this.messageService.warn('Vui lòng nhập thông tin giảm lãi suất');
        return;
      }
      this.listData.sort((a: any, b: any) => {
        return Number(b.data?.reductPercent) - Number(a.data?.reductPercent);
      });
      reductPercent = this.listData?.[0]?.data?.reductPercent;
      if (!reductPercent) {
        this.messageService.warn('Vui lòng nhập biên độ đề xuất');
        return;
      } else if (reductPercent < 0) {
        this.messageService.warn('Tỉ lệ giảm phải lớn hơn hoặc bằng 0');
        return;
      }
      maxReductPercentOfTypes['0'] = reductPercent;
    } else {
      // Validate chọn thẩm quyền đối với biểu phí
      if (this.listFeeData.length === 0) {
        this.messageService.warn('Vui lòng nhập thông tin giảm phí');
        return;
      }
      // Lấy tỉ lệ giảm lớn nhất nhóm theo phân loại phí
      this.listFeeData.forEach(item => {
        const chargeType = item.data?.chargeType;
        const rateCost = item.data?.rateCost || 0;
        const rateMin = item.data?.rateMin || 0;
        const rateMax = item.data?.rateMax || 0;
        const chargeTypeRate = maxReductPercentOfTypes[chargeType] || 0;
        if (item.data?.chargeGroup === 'REMITTANCES' && this.blockCode === Division.SME) {
          hasTTTM = true;
        }
        const rate = Math.max(rateCost, rateMin, rateMax, chargeTypeRate);
        maxReductPercentOfTypes[chargeType] = rate;
        required += rateCost === 0 && rateMin === 0 && rateMax === 0 ? 1 : 0;
        lowerThanZero += Math.min(rateCost, rateMin, rateMax) < 0 ? 1 : 0;
      });
      if (required) {
        this.messageService.warn('Vui lòng nhập thông tin đề xuất giảm');
        return;
      }
      if (lowerThanZero) {
        this.messageService.warn('Tỉ lệ giảm phải lớn hơn hoặc bằng 0');
        return;
      }
    }

    const customer: any = this.info?.customer;

    let toiCommit;
    if (this.info.type === 1) {
      if (this.info.customer.isNew) {
        toiCommit = toi;
      } else {
        toiCommit = this.convertToiCurrent(this.info.customer.toiCurrent);
      }
    } else {
      // SME. Khách hàng tín dụng lấy thẩm quyền theo TOI cam kết
      // SME. Khách hàng phi tín dụng lấy thẩm quyền không theo TOI
      if (this.blockCode === Division.SME) {
        const toiNum = this.convertToiCurrent(this.info.customer.toiCurrent);
        toiCommit = this.info.customer.customerCreditType === 'Phi tín dụng' ? null : toiNum || toiNum == 0 ? toiNum : toi;
      } else if (this.blockCode !== Division.INDIV) {
        toiCommit = toi ? toi : this.convertToiCurrent(this.info.customer.toiCurrent);
      }
    }


    let department = customer.customerTypeMerge;
    if (!department) {
      if (this.divisionCodeOfUser.includes('SME')) {
        department = 'SME';
      } else if (this.divisionCodeOfUser.length > 0) {
        department = this.divisionCodeOfUser[0];
      }
    }

    let customerSegment;
    let customerType;
    if (department === Division.SME) {
      customerSegment = department + this.customer?.segmentCode;
      customerType = customer?.classification;
    } else if (department === Division.INDIV) {
      customerSegment = 'KHCN';
      customerType = customer?.segmentCode;
    } else {
      customerSegment = department;
      customerType = customer?.classification;
    }
    const body = this.getBodyByDivision(customerType, customerSegment, department, reductPercent, toiCommit);

    const forkJoinReq = [this.categoryService.getCommonCategory(CommonCategory.REDUCTION_PROPOSAL_AUTH_CONFIRM).pipe(catchError(() => of(undefined)))];
    const forkJoinResp = ['cate'];
    const types = Object.keys(maxReductPercentOfTypes);
    types.forEach(type => {
      if (this.info.type !== 0) { // lãi
        const typeInterest = this.typeInterest.find(item => item.id === this.form.controls.typeInterest.value)?.name;
        const block = this.form.controls.typeInterest.value === 6 ? 'CIB' : department;
        const bodyInterest = this.getBodyByDivisionInterest(customerType, customerSegment, department, reductPercent, toiCommit, typeInterest, block);
        forkJoinReq.push(this.reductionProposalApi.getListAuthor(bodyInterest).pipe(catchError(() => of(undefined))));
      } else { // Phí
        body.discountPercent = maxReductPercentOfTypes[type];
        body.typeOfFee = 'Loại ' + type;
        forkJoinReq.push(this.reductionProposalApi.getListFeeAuthor({ ...body }).pipe(catchError(() => of(undefined))));
      }
      forkJoinResp.push('authors' + type);
    });
    this.createReductionProposalComponent.changeLoading(true);
    forkJoin(forkJoinReq).subscribe((forkJoinResp) => {
      const category = forkJoinResp[0];
      let titles = {};
      if (category.content) {
        let normalCIB = '';
        if (this.form.controls.typeInterest.value === 6) {
          normalCIB = '-CIB';
        }
        if(this.form.controls.typeInterest.value === 1){
          normalCIB = '-1';
        }
        const rawTitles = category.content?.find(item => item?.code === this.info.type + '-' + this.blockCode + normalCIB)
          ?.value || '{}';
        titles = JSON.parse(rawTitles);
      }
      let authors = forkJoinResp[1];
      if (types.length > 1) {
        // Tính số lượng cấp phê duyệt theo phân loại phí
        const checkAuth = {};
        for (let index = 1; index < forkJoinResp.length; index++) {
          const auth = forkJoinResp[index];
          if (auth?.status === 200) {

            // for (let itm = 0; itm < auth.length; itm++) {
            //   const element = array[itm];

            // }
            auth?.data?.forEach(itm => {
              checkAuth[index] = Math.max(itm?.orderSigning, checkAuth[index] || 0);
            });
          }
        }
        // Lấy danh sách thẩm quyền có nhiều cấp phê duyệt nhất
        let maxOrderSigning = 0;
        Object.keys(checkAuth).map(idx => {
          const orderSigning = checkAuth[idx];
          if (maxOrderSigning < orderSigning) {
            maxOrderSigning = orderSigning;
            authors = forkJoinResp[idx];
          }
        });
      }
      if (authors?.status === 200) {
        this.createReductionProposalComponent.changeLoading(false);
        const modal = this.dialog.open(ConfirmAuthorizationModalComponent, { width: '85%' });
        modal.componentInstance.authors = authors;
        modal.componentInstance.isRM = this.isRM;
        modal.componentInstance.titles = titles;
        modal.componentInstance.divisionCode = this.blockCode;
        modal.componentInstance.type = this.info?.type;
        modal.componentInstance.interestType = this.form.controls.typeInterest.value
        modal.componentInstance.hasTTTM = hasTTTM;
        modal.afterClosed().subscribe(() => {

          const sorted: any[] = this.info?.authorization?.sort((a, b) => {
            return b.priority - a.priority;
          });
          if (sorted && sorted.length > 0) {
            this.form.controls.lastApprove.setValue(sorted?.[0]?.fullName);
            this.form.controls.lastApproveTitle.setValue(sorted?.[0]?.titleCategory);
            this.form.controls.lastApproveFull.setValue(sorted?.[0]?.titleCategory + ' - ' + sorted?.[0]?.fullName);
          }
        });
      } else {
        this.messageService.error('Không tìm thấy dữ liệu thẩm quyền phê duyệt');
        this.createReductionProposalComponent.changeLoading(false);
      }

    }, (e) => {
      this.createReductionProposalComponent.changeLoading(false);
    });
  }

  clearAuthData() {
    const info = this.reductionProposalApi.info.value;
    info.authorization = [];
    this.reductionProposalApi.info.next(info);
    this.form.controls.lastApprove.setValue('');
    this.form.controls.lastApproveTitle.setValue('');
    this.form.controls.lastApproveFull.setValue('');
  }

  monthDiff(d1, d2) {
    if (Utils.isNull(d1), Utils.isNull(d2)) return '';
    d1 = moment(d1).toDate();
    d2 = moment(d2).toDate();
    let months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth();
    months += d2.getMonth();
    return months <= 0 ? 0 : months;
  }

  getValueMax(row) {
    const maximumMarginReduction = row?.maximumMarginReduction || 0;
    // let baseMargin = row?.baseMargin || 0;
    if (maximumMarginReduction && maximumMarginReduction > 0)
      // return (baseMargin - maximumMarginReduction).toFixed(2)
      return maximumMarginReduction;
    else
      return '--';
  }

  calculateInterest(event, row) {
    const val = event.target.value;
    if (Number(val) <= 0 || Number(val) > Number(row?.minInterestRate) || !val) {
      row.rateMinimum = row?.minInterestRate;
      row.rateInterestReduction = 0;
    }
    this.treeTableInterest.updateSerializedValue();
  }

  getValueMinimumRate(event, row) {
    const val = event.target.value;
    row.rateInterestReduction = ((Number(row?.minInterestRate) - Number(val)) / Number(row?.minInterestRate) * 100).toFixed(2);
    if (Number(val) > Number(row?.minInterestRate)) {

      row.rateMinimumError = 'LS tối thiểu đề xuất ≤ LS tối thiểu quy định';
      this.messageService.warn('LS tối thiểu đề xuất ≤ LS tối thiểu quy định');

    } else if (Number(val) < 0) {
      row.rateMinimumError = 'LS tối thiểu > 0 và ≤ LS tối thiểu quy định';
      this.messageService.warn('LS tối thiểu > 0 và ≤ LS tối thiểu quy định');

    } else {
      delete row.rateMinimumError;
    }
  }

  formatCurrency(element) {
    if (element == 0) {
      return '0';
    }
    if (!element || element === 'NaN' || element === '--') {
      return '--';
    }
    let formated: string = element?.toString();
    let first = '';
    let last = '';
    if (formated.includes('.')) {
      first = formated.substr(0, formated.indexOf('.'));
      last = formated.substr(formated.indexOf('.'));
    } else {
      first = formated;
    }
    if (Number(first)) {
      first = Number(first).toLocaleString('en-US', {
        style: 'currency',
        currency: 'VND'
      });
      // first = first.trim().substr(1, first.length);
      first = first.trim().replace('₫', '');
      formated = first + last;
    }
    return formated;
  }

  formatRateInterest(element) {
    if (element === 0) {
      return '--';
    }
  }

  clearData() {
    this.form.reset();
    this.form.clearValidators();
    this.scopeApplys = [];
    this.listCreditRatings = [];
    this.customerTypeDisplay = '';
    this.interestData = [];
    this.optionCodeData = [];
    this.ldCodeData = [];
    this.listData = [];
    this.commonSegment = [];
    this.listLD = [];
    this.existed = true;
    this.divisionCodeOfUser = [];
    this.info.toi = {};

    this.feeData = [];
    this.listFeeData = [];
  }

  convertToiCurrent(toi) {
    if (toi) {
      toi = Number(Number(toi) * 100).toFixed(2);
    }
    return toi;
  }

  convertDtttrr(dtttrr) {
    if (dtttrr) {
      dtttrr = Number(Number(dtttrr) / 1000000).toFixed(2);
    }
    return dtttrr;
  }

  generateRelationshipTimeWithMB(item) {
    const date = _.get(item, 'customerInfo.openCodeDate');
    if (Utils.isStringNotEmpty(date)) {
      const totalMonth = moment().diff(moment(date, 'DD/MM/YYYY'), 'months');
      return totalMonth > 12
        ? `${Math.floor(totalMonth / 12)} ${_.get(this.fields, 'year')}`
        : `${totalMonth} ${_.get(this.fields, 'month')}`;
    } else {
      return `0 ${_.get(this.fields, 'year')}`;
    }
  }

  customValidator() {
    this.form.controls.monthApply.clearValidators();
    this.form.controls.toi.clearValidators();
    this.form.controls.dtttrr.clearValidators();
    this.form.controls.nimCurrent.clearValidators();
    this.form.controls.nimConfirm.clearValidators();
    this.form.controls.monthConfirm.clearValidators();

    if (this.info.type === 0) {
      if (this.blockCode === Division.INDIV) {
        this.form.controls.monthApply.setValidators(Validators.compose([Validators.maxLength(4), Validators.min(1)]));
        this.form.controls.nimCurrent.setValidators(Validators.compose([CustomValidators.required, Validators.maxLength(12), Validators.min(1)]));
        this.form.controls.nimConfirm.setValidators(Validators.compose([CustomValidators.required, Validators.maxLength(12), Validators.min(1)]));
      } else {
        this.form.controls.monthApply.setValidators(Validators.compose([CustomValidators.required, Validators.maxLength(12)]));
        if (this.displayConfirmInfo && this.blockCode !== Division.FI && this.blockCode !== Division.CIB) {
          this.form.controls.toi.setValidators(CustomValidators.required);
          this.form.controls.dtttrr.setValidators(CustomValidators.required);
        }
      }
    } else {
      this.form.controls.monthApply.setValidators(CustomValidators.required);
      this.form.controls.monthConfirm.setValidators(CustomValidators.required);
      if (this.info?.customer?.customerTypeCode === 'NEW') {
        this.form.controls.dtttrr.setValidators(CustomValidators.required);
        this.form.controls.toi.setValidators(CustomValidators.required);
      }
    }

    this.form.controls.toi.updateValueAndValidity();
    this.form.controls.dtttrr.updateValueAndValidity();
    this.form.controls.monthApply.updateValueAndValidity();
    this.form.controls.monthConfirm.updateValueAndValidity();
    this.form.controls.nimCurrent.updateValueAndValidity();
    this.form.controls.nimConfirm.updateValueAndValidity();
  }

  checkTableInterest() {
    return (this.form.controls.typeInterest.value !== 1 && this.form.controls.typeInterest.value !== 6);
  }

  getBodyByDivisionInterest(customerType: string,
                            customerSegment: string,
                            department: string,
                            reductPercent: number,
                            toiCommit: number,
                            typeInterest: string,
                            block: string): any {

    const code = this.typeInterest.find(item => item.id === this.form.controls.typeInterest.value)?.code;
    if (department === Division.SME) {
      if (code === 10) {
        return {
          branch: this.currUser.branch, // chi nhánh RM tạo tờ trình
          customerSegment,
          department, // Khối
          discountPercent: reductPercent, // Lấy tỉ lệ giảm lớn nhất trong bảng
          rmCode: this.currUser.code, // rmCode
          toiCommit, // toi,
          type: typeInterest
        };
      }
      else if (code === 30 || code === 40) {
        return {
          branch: this.currUser.branch, // chi nhánh RM tạo tờ trình
          customerSegment,
          department, // Khối
          discountPercent: reductPercent, // Lấy tỉ lệ giảm lớn nhất trong bảng
          rmCode: this.currUser.code, // rmCode
          type: typeInterest
        };
      }
      else if (code === 11) {
        return {
          branch: this.currUser.branch, // chi nhánh RM tạo tờ trình
          department: block, // Khối
          discountPercent: reductPercent, // Lấy tỉ lệ giảm lớn nhất trong bảng
          rmCode: this.currUser.code, // rmCode
          toiCommit, // toi,
          type: typeInterest
        };
      }
      else {
        return {
          branch: this.currUser.branch, // chi nhánh RM tạo tờ trình
          customerSegment,
          department, // Khối
          discountPercent: reductPercent, // Lấy tỉ lệ giảm lớn nhất trong bảng
          rmCode: this.currUser.code, // rmCode
          toiCommit, // toi,
          type: 'Thông thường'
        };
      }
    }
    if (department === Division.CIB) {
      if (code === 10 || code === 40) {
        return {
          branch: this.currUser.branch, // chi nhánh RM tạo tờ trình
          customerSegment,
          department, // Khối
          discountPercent: reductPercent, // Lấy tỉ lệ giảm lớn nhất trong bảng
          rmCode: this.currUser.code, // rmCode
          toiCommit, // toi,
          type: typeInterest
        };
      }
      else if (code === 30) {
        return {
          branch: this.currUser.branch, // chi nhánh RM tạo tờ trình
          customerSegment,
          department, // Khối
          discountPercent: reductPercent, // Lấy tỉ lệ giảm lớn nhất trong bảng
          rmCode: this.currUser.code, // rmCode
          type: typeInterest
        };
      }
      else {
        return {
          branch: this.currUser.branch, // chi nhánh RM tạo tờ trình
          customerSegment,
          department, // Khối
          discountPercent: reductPercent, // Lấy tỉ lệ giảm lớn nhất trong bảng
          rmCode: this.currUser.code, // rmCode
          toiCommit, // toi,
          type: 'Thông thường'
        };
      }
    }
  }

  getBodyByDivision(customerType: string, customerSegment: string, department: string, reductPercent: number, toiCommit: number): any {
    if (department === Division.FI) {
      return {
        branch: this.currUser.branch, // chi nhánh RM tạo tờ trình
        customerSegment: 'FI',
        department, // Khối
        discountPercent: reductPercent, // Lấy tỉ lệ giảm lớn nhất trong bảng
        rmCode: this.currUser?.code ? this.currUser?.code : '999', // rmCode
        typeOfFee: ''
      };
    }
    if (department === Division.INDIV) {
      return {
        branch: this.currUser.branch, // chi nhánh RM tạo tờ trình
        customerSegment: 'KHCN',
        customerType,
        department: 'KHCN', // Khối
        discountPercent: reductPercent, // Lấy tỉ lệ giảm lớn nhất trong bảng
        rmCode: this.currUser.code, // rmCode
        typeOfFee: ''
      };
    }
    if (department === Division.CIB
      && (this.info.customer.customerCreditType === 'Tín dụng hiện hữu'
        || this.info.customer.customerCreditType === 'Tín dụng mới')) {
      let toi;
      if (_.isEmpty(this.info.customer.customerCode)) {
        toi = this.info.customer.toiConfirm;
      } else {
        toi = this.info.customer.toiCurrent * 100;
      }
      return {
        branch: this.currUser.branch, // chi nhánh RM tạo tờ trình
        customerSegment,
        department, // Khối
        discountPercent: reductPercent, // Lấy tỉ lệ giảm lớn nhất trong bảng
        rmCode: this.currUser.code, // rmCode
        toiCommit: toi, // toi,
        typeOfFee: '',
        chargeGroup: this.info.customer.customerCreditType === 'Phi tín dụng' ? 'NON_CREDIT' : 'CREDIT'
        // CREDIT: Tín dụng NO_CREDIT: Phi tín dụng
      };
    } else {
      return {
        branch: this.currUser.branch, // chi nhánh RM tạo tờ trình
        customerSegment,
        department, // Khối
        discountPercent: reductPercent, // Lấy tỉ lệ giảm lớn nhất trong bảng
        rmCode: this.currUser.code, // rmCode
        toiCommit: this.info.customer.customerCreditType === 'Phi tín dụng' ? null : Number(toiCommit), // toi,
        typeOfFee: '',
        chargeGroup: this.info.customer.customerCreditType === 'Phi tín dụng' ? 'NON_CREDIT' : 'CREDIT'
        // CREDIT: Tín dụng NO_CREDIT: Phi tín dụng
      };
    }
  }
}

