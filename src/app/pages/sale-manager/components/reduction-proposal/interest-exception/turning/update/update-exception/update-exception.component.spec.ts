import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateExceptionComponent } from './update-exception.component';

describe('UpdateExceptionComponent', () => {
  let component: UpdateExceptionComponent;
  let fixture: ComponentFixture<UpdateExceptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateExceptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateExceptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
