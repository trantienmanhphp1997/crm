import { AfterViewInit, ChangeDetectorRef, Component, Injector, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '../../../../../../../../core/components/base.component';
import { MatDialog } from '@angular/material/dialog';
import { ReductionProposalApi } from '../../../../../../api/reduction-proposal.api';
import { CustomerApi, CustomerDetailApi } from '../../../../../../../customer-360/apis';
import { CustomerDetailSmeApi } from '../../../../../../../customer-360/apis/customer.api';
import { CategoryService } from '../../../../../../../system/services/category.service';
import { BranchApi } from '../../../../../../../rm/apis';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FileService } from '../../../../../../../../core/services/file.service';
import {
  CommonCategory,
  Division,
  FunctionCode,
  functionUri
} from '../../../../../../../../core/utils/common-constants';
import { TreeTable } from 'primeng/treetable';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import * as moment from 'moment/moment';
import { SelectionType } from '@swimlane/ngx-datatable';
import { InterestModalComponent } from '../../../../dialog/select-interest-modal/select-interest-modal.component';
import { OptionCodeModalComponent } from '../../../../dialog/select-optioncode-modal/select-optioncode-modal.component';
import { LdCodeModalComponent } from '../../../../dialog/select-ldcode-modal/select-ldcode-modal.component';
import { ToiConfirmModalComponent } from '../../../../dialog/toi-confirm-modal/toi-confirm-modal.component';
import { Utils } from '../../../../../../../../core/utils/utils';
import * as _ from 'lodash';

@Component({
  selector: 'app-detail-exception',
  templateUrl: './detail-exception.component.html',
  styleUrls: ['./detail-exception.component.scss']
})
export class DetailExceptionComponent extends BaseComponent implements OnInit, AfterViewInit  {
  @ViewChild('treeTableInterest') treeTableInterest: TreeTable;
  customer360Fn: any;
  stateRouter: any
  findPotentialCustomer: any;
  formSearch: any;
  info: any
  customerInfo: any
  existed: boolean
  titleAuthApprovalError: string;
  effectError: string;
  dateApprovalMOError: string;
  dateEndApproveError: string;
  authApprovalError: string;
  fullNameError: string;
  codeReasonError: string;
  codeCnttError: string;
  moCodeError: string;
  isManage: boolean;
  monthConfirmError: string;
  maxDate = new Date();
  maxSize = 20;
  infoSave: any;
  detail: any;
  reductionId: any;
  params: any;

  // Array
  reasonException = [];
  monthConfirms = [];
  months = [];
  listLD = [];
  listData = [];
  interestData = [];
  optionCodeData = [];
  ldCodeData = [];
  listFile = [];


  dataCommonSegment: any;
  form = this.fb.group({
    searchStr: '',
    caseSuggest: [null],
    scopeApply: 'ALL',
    toi: null,
    dtttrr: null,
    monthApply: null,
    monthConfirm: null,
    listData: [],
    authorization: [],
    lastApprove: '',
    lastApproveTitle: '',
    lastApproveFull: '',
    segment: '',
    dtttrrDisplay: null,
    nimCurrent: null,
    nimConfirm: null,
    typeInterest: 1,
    effect: 1,
    dateApprovalMO: null,
    authApproval: null,
    rmConfirm: false,
    titleAuthApproval: null,
    codeCntt: null,
    moCode: null,
    dateEndApprove: null,
    codeReason: null,
    commitment: true,
    fullName: null
  });

  // Common data
  allScopeApples = [
    { code: 'ALL', displayName: 'Tất cả các phương án', caseSuggest: 'ALL', typeNum: 0 },
    { code: 'OPTION_CODE', displayName: 'Mã phương án hạn mức', caseSuggest: 'ALL', typeNum: 1 },
    { code: 'UNKNOWN', displayName: 'Chưa xác định mã phương án', caseSuggest: 'ALL', typeNum: 3 }
  ];
  AllTypeFee = [
    {
      id: 1, code: 10, displayName: 'Biểu lãi suất thông thường', name: 'Thông thường',
      caseSuggest: 'NORMAL', productType: 'TT', typeNum: 0
    },
    {
      id: 2, code: 40, displayName: 'Biểu lãi suất ưu đãi (AIRS)', name: 'Ưu đãi',
      caseSuggest: 'ENDOW', productType: 'UD', typeNum: 0
    },
    {
      id: 4, code: 30, displayName: 'Biểu lãi suất linh hoạt', name: 'Linh hoạt',
      caseSuggest: 'ABILITY', productType: 'UD', typeNum: 0
    }
  ];

  constructor(
    injector: Injector,
    public dialog: MatDialog,
    private reductionProposalApi: ReductionProposalApi,
    private customerApi: CustomerApi,
    private customerDetailSmeApi: CustomerDetailSmeApi,
    private customerDetailApi: CustomerDetailApi,
    private categoryService: CategoryService,
    private branchApi: BranchApi,
    private modal: NgbModal,
    private dtc: ChangeDetectorRef,
    private fileService: FileService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REDUCTION_PROPOSAL}`);
    this.customer360Fn = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);

    this.reductionId = this.route?.snapshot?.queryParams?.id;
    this.params = this.route?.snapshot?.queryParams?.action;

  }
  ngOnInit(): void {
    this.initData();

    const month = [];
    const monthConfirm = [];

    for (let index = 1; index < 13; index++) {
      const item = { code: index, displayName: index.toString() };
      if (index < 7) {
        monthConfirm.push(item);
      }
      month.push(item);
    }
    this.months = month;
    this.monthConfirms = monthConfirm;
  }

  initData(){
    // Check customer is exists customerCode or exists taxCode
    this.isLoading = true;
    return new Promise(async (resolve, reject) => {
      // get common data
      forkJoin([
        this.categoryService.getCommonCategory(CommonCategory.REASON_EXCEPTION_REDUCTION).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.REVENUE_CUSTOMER_SEGMENT).pipe(catchError(() => of(undefined))),
        this.reductionProposalApi.detailReductionProposal(this.reductionId).pipe(catchError(() => of(undefined))),
      ]).subscribe(([categoryReason, customerSegment, detail]) => {
        if (categoryReason.content) {
          categoryReason.content.forEach((value) => {
            this.reasonException.push({ code: value.code, name: value.value });
          });
          this.form.controls.codeReason.setValue(1);
        }
        if(customerSegment.content){
          this.dataCommonSegment = customerSegment.content;
        }
        this.detail = detail;
        this.existed = this.detail?.customerCode;
        const customerHaveCode = detail?.customerCode;
        // implement body for api get inform customer
        const body = {
          customerCode: detail?.customerCode,
          taxCode: detail?.taxCode,
          blockCode: detail?.blockCode
        };
        const searchSmeBody = {
          pageNumber: 0,
          pageSize: 10,
          rsId: this.customer360Fn?.rsId,
          scope: 'VIEW',
          customerType: '',
          search: detail?.customerCode,
          searchFastType: 'CUSTOMER_CODE',
          manageType: 1,
          customerTypeSale: 1
        };
        const param = {
          customerCode: detail?.customerCode,
          businessRegistrationNumber: '',
          loanId: ''
        };
        const bodyCredit = {
          cif: detail?.customerCode,
          taxId: '',
          nationalId: '',
          lob: detail?.blockCode
        };

        if(customerHaveCode){
          forkJoin([
            this.reductionProposalApi.findExistingCustomer(body).pipe(catchError(() => of(undefined))),
            this.customerDetailApi.getByCode(detail?.customerCode).pipe(catchError(() => of(undefined))),
            this.customerApi.searchSme(searchSmeBody).pipe(catchError(() => of(undefined))),
            this.reductionProposalApi.getScopeReduction(param).pipe(catchError(() => of(undefined))),
            this.reductionProposalApi.getToiExisting(detail?.customerCode).pipe(catchError(() => of(undefined))),
            this.reductionProposalApi.getCustomerCredit({ bodyCredit }).pipe(catchError(() => of(undefined))),
          ]).subscribe(([findExistingCustomer, getByCode, searchSme, getScopeReduction,  getToiExisting, getCustomerCredit]) =>{
            const customerType = getCustomerCredit?.data?.customerType;
            this.customerInfo = { ...findExistingCustomer, ...getByCode, ...{ searchSme: searchSme[0] }, ...getScopeReduction, ...getToiExisting, ...{ customerType } };
            this.mappingDataForView(this.customerInfo);
            this.mapDataViewDetail(this.detail);
            this.initTOI();
            this.initInterestData();
            console.log('this.detail: ', this.detail);
            console.log('this info: ', this.info);

            this.isLoading = false;
          }, err => {
            reject(err);
            this.isLoading = false;
          });
        }
        // Get customer info for not exists customerCode
        else {
          const paramScopeReduction = {
            businessRegistrationNumber: detail?.taxCode || '',
            loanId: ''
          };
          const bodyKhtn = {
            customerCode: '',
            taxCode: detail?.businessRegistrationNumber || detail?.taxCode,
            blockCode: ''
          };

          forkJoin([
            this.reductionProposalApi.getScopeReduction(paramScopeReduction).pipe(catchError(() => of(undefined))),
            this.reductionProposalApi.getKhdnPotentialCustomerSegment(detail?.taxCode).pipe(catchError(() => of(undefined))),
            this.reductionProposalApi.findPotentialCustomer(bodyKhtn).pipe(catchError(() => of(undefined))),
          ]).subscribe(([getScopeReduction, getKhdnPotentialCustomerSegment, findPotentialCustomer]) => {
            this.customerInfo = { ...getScopeReduction, ...getKhdnPotentialCustomerSegment, ...findPotentialCustomer};
            console.log('this.customerInfo 2: ', this.customerInfo);
            this.mappingDataForView(this.customerInfo);
            this.mapDataViewDetail(this.detail);
            this.initTOI();
            this.initInterestData();
            console.log('this.detail: ', this.detail);
            console.log('this.interest: ', this.listData);
            this.isLoading = false;
            resolve(true)
          });
        }

      });
    })
  }

  ngAfterViewInit(): void {
    this.form.controls.scopeApply.valueChanges.subscribe(() => {
      this.reset();
    });

    this.form.controls.segment.valueChanges.subscribe((value) => {
      this.resetBySegment();

    });
    this.form.controls.typeInterest.valueChanges.subscribe((value) => {
      this.reset();
    });
    this.form.controls.codeReason.valueChanges.subscribe((value) => {
      if (value.code !== 1) {
        this.codeCnttError = '';
      }
    });
    this.dtc.detectChanges();
  }

  mappingDataForView(customerInfo: any){
    console.log('customerInfo: ', customerInfo);
    this.info = {
      customerCode: this.detail.status !== 'IN_PROGRESS' ? this.detail?.customerCode : this.customerInfo?.customerCode || this.customerInfo?.searchSme?.customerCode || '',
      customerName: this.detail.status !== 'IN_PROGRESS' ? this.detail?.customerName : this.customerInfo?.customerName || this.customerInfo?.searchSme?.customerName  || this.customerInfo?.fullName || '',
      taxCode: this.detail.status !== 'IN_PROGRESS' ? this.detail?.taxCode : this.customerInfo?.taxCode || this.customerInfo?.searchSme?.taxCode || '',
      businessRegistrationNumber: this.detail.status !== 'IN_PROGRESS' ? this.detail?.businessRegistrationNumber : this.customerInfo?.businessRegistrationNumber || this.detail.taxCode ||  '',
      segment: this.detail.status !== 'IN_PROGRESS' ? this.mapSegment(this.detail?.customerSegment) : this.customerInfo?.searchSme?.segment || this.mapSegment(this.customerInfo?.data?.[0]?.cstSegStd) || '',
      segmentCode: this.detail.status !== 'IN_PROGRESS' ? this.detail?.segmentCode : this.customerInfo?.searchSme?.segmentCode || this.customerInfo?.data?.[0]?.cstSegStd || '',
      scoreValue: this.detail.status !== 'IN_PROGRESS' ? this.detail?.creditRanking : this.customerInfo?.ketQuaXhtd || '',
      classification: this.detail.status !== 'IN_PROGRESS' ? this.detail?.classification : this.customerInfo?.searchSme?.classification || '',
      customerType: this.detail.status !== 'IN_PROGRESS' ? this.detail?.customerType === 'NEW' ? 'Khách hàng mới' : 'Khách hàng cũ' : this.customerInfo?.loai_KH === 'Khach hang cu' ? 'Khách hàng cũ' : 'Khách hàng mới',
      toiExisting: this.detail.status !== 'IN_PROGRESS' ? this.detail?.toiExisting : this.customerInfo?.TOI,
      dtttrr: this.detail.status !== 'IN_PROGRESS' ? this.detail?.dtttrrExisting : this.customerInfo?.DTTTRR,
      toiConfirm: this.detail.status !== 'IN_PROGRESS' ? this.detail?.toiConfirm : '',
      dtttrrConfirm: this.detail.status !== 'IN_PROGRESS' ? this.detail?.dtttrrConfirm : '',
      blockCode: this.detail.status !== 'IN_PROGRESS' ? this.detail?.blockCode : this.customerInfo?.searchSme?.customerType || this.detail?.blockCode,
      isNew: this.customerInfo?.loai_KH === 'Khach hang moi' ,
      idCard: '',
      isManage: '',

    }
  }

  mapDataViewDetail(detail: any) {
    if (detail) {
      this.form.controls.effect.setValue(detail?.monthApply);
      this.form.controls.dateApprovalMO.setValue(moment(detail?.approvedDateMO).format('DD/MM/YYYY'));
      this.form.controls.fullName.setValue(detail?.reductionProposalAuthorizationDTOList[0]?.fullName);
      this.form.controls.authApproval.setValue(detail?.reductionProposalAuthorizationDTOList[0]?.userName);
      this.form.controls.titleAuthApproval.setValue(detail?.reductionProposalAuthorizationDTOList[0]?.titleCategory);
      this.form.controls.codeReason.setValue(detail?.reason + '', {
        onlySelf: true,
        emitEvent: false
      });
      this.form.controls.codeCntt.setValue(detail?.codeHTCNTT);
      this.form.controls.moCode.setValue(detail?.moDocId);
      this.form.controls.monthConfirm.setValue(detail?.monthConfirm);
      this.form.controls.typeInterest.setValue(detail?.interestType);
      this.form.controls.toi.setValue(detail?.toiConfirm);
      this.form.controls.dtttrrDisplay.setValue(this.convertDtttrr(detail?.dtttrrConfirm));
      this.form.controls.segment.setValue(this.mapSegment(this.detail.customerSegment), {
        onlySelf: true,
        emitEvent: false
      })

      let scopeType;
      switch (detail?.scopeType) {
        case 0:
          scopeType = 'ALL';
          break;
        case 1:
          scopeType = 'OPTION_CODE';
          break;
        case 2:
          scopeType = 'LD_CODE';
          break;
        case 3:
          scopeType = 'UNKNOWN';
          break;
        case 4:
          scopeType = 'MD_CODE';
          break;
      }
      this.form.controls.scopeApply.setValue(scopeType);
      this.listFile = detail?.reductionProposalFileDTOList;
    }
  }


  save(backToList: boolean) {
    console.log(this.info);
    return new Promise<any>(async (resolve, reject) => {
      if (this.checkValidParam() || !this.checkFile() || !this.isValid()) {
        reject(false);
        return;
      }
      if (!this.form.controls.commitment.value) {
        this.messageService.warn('Vui lòng xác nhận cam kết');
        return;
      }

      let reductionProposalDetailsDTOList = [];

      if (this.listData.length > 0) {
        reductionProposalDetailsDTOList = this.listData?.map(item => {
          item = item.data;
          return {
            planCode: this.form.controls.scopeApply.value === 'OPTION_CODE' ? item.code : '',
            ldCode: this.form.controls.scopeApply.value === 'LD_CODE' ? item.code : '',
            itemCode: item?.productCode,  // mã biểu lãi
            itemName: item?.name, // tên biểu lãi
            maximumCost: '',
            maximumCostOffer: '',
            minimumCost: '',
            minimumCostOffer: '',
            ratioCost: '',
            ratioCostOffer: '',
            type: 3,
            interestValue: item.loanRate, // Lãi suất cho vay
            interestUnit: item.currency, // Đơn vị tiền tệ
            interestType: item.name, // Biểu lãi suất
            interestReference: item.referenceInterestRate, // Lãi suất tham chiếu (%)
            interestProposedValue: item.loanSuggest, // Lãi suất cho vay đề xuất
            interestProposedAmplitude: item.marginSuggest, // Biên độ đề xuất (%)
            interestMaxReduction: item.maximumMarginReduction,// Mức giảm biên độ tối đa (%
            interestLoanPeriod: item.period, // Kỳ hạn vay
            interestAmplitude: item.baseMargin, // Biên độ (%)
            interestAdjustPeriod: item?.adjustmentPeriod, // Kỳ điểu chỉnh lãi suất
            reductionRate: item.reductPercent, // tỷ lệ giảm
            id: this.detail?.reductionProposalDetailsDTOList?.[0].id,
            minInterestRate: item.minInterestRate,
            minInterestRateOffer: item.rateMinimum
          };
        });
      }

      // scopeApply
      let scopeApplyData = [];
      switch (this.form.controls.scopeApply.value) {
        case 'LD_CODE':
          scopeApplyData = this.ldCodeData;
          break;
        case 'OPTION_CODE':
          scopeApplyData = this.optionCodeData;
          break;
        default:
          scopeApplyData = this.interestData;
          break;
      }
      const scopeApply = JSON.stringify(scopeApplyData);
      // scopeType
      const scopeType = this.allScopeApples.find(i => i.code === this.form.controls.scopeApply.value).typeNum;
      const payload = {
        id: this.detail?.id,
        blockCode: this.info?.blockCode || '',
        code: moment.now(),
        customerCode: this.info?.customerCode,
        monthApply: this.form.controls.effect.value,
        monthConfirm: this.form.controls.monthConfirm.value,
        scopeApply,
        scopeType,
        taxCode: this.info.taxCode,
        type: 3,
        dtttrrConfirm: this.form.controls.dtttrr.value ? Number(this.form.controls.dtttrr.value.toString().replace(',', '.')) : this.form.controls.dtttrr.value,
        toiConfirm: this.form.controls.toi.value,
        reductionProposalDetailsDTOList,
        reductionProposalDetailsApprovedDTOList: [],
        reductionProposalTOIDTO: {
          ckhBqNim: this.info?.toi?.coKyHanBqUnit || 0,
          ckhBqSd: this.info?.toi?.coKyHanBq || 0,
          dunoBqNim: this.info?.toi?.duNoNganHanBqUnit || 0,
          dunoBqSd: this.info?.toi?.duNoNganHanBq || 0,
          dunoTdhBqNim: this.info?.toi?.duNoTdhBqUnit || 0,
          dunoTdhBqSd: this.info?.toi?.duNoTdhBq || 0,
          kkhBqNim: this.info?.toi?.khongKyHanBqUnit || 0,
          kkhBqSd: this.info?.toi?.khongKyHanBq || 0,
          tblDsSd: this.info?.toi?.thuBl || 0,
          tblNim: this.info?.toi?.thuBlUnit || 0,
          tfxDsSd: this.info?.toi?.thuFx || 0,
          tfxMpqd: this.info?.toi?.thuFxUnit || 0,
          thuBancaDsSd: this.info?.toi?.thuBanca || 0,
          thuBanCaMpqd: 1,
          thuKhacDsSd: this.info?.toi?.thuKhac || 0,
          thuKhacMpqd: 1,
          tttqtDsSd: this.info?.toi?.thuTTQT || 0,
          tttqtMpqd: this.info?.toi?.thuTTQTUnit || 0,
          ckhBqHq: this.info?.toi?.ckhBqHq ? +this.info?.toi?.ckhBqHq.replace(',', '.') : 0,
          dunoBqHq: 0,
          dunoTdhBqHq: 0,
          kkhBqHq: 0,
          tblHq: 0,
          tfxHq: 0,
          thuBanCaHq: 0,
          thuKhacHq: 0,
          tttqtHq: 0,
          dttck: 0,
          tck: 0
        },

        reductionProposalFileDTOList: this.listFile,
        reductionProposalAuthorizationDTOList: [
          {
            id: this.detail.reductionProposalAuthorizationDTOList?.[0]?.id,
            fullName: this.form.controls.fullName.value,
            userName: this.form.controls.authApproval.value,
            titleCategory: this.form.controls.titleAuthApproval.value
          }
        ],
        customerName: this.info?.customerName,
        rmCode: this.currUser?.rmCode,
        branchCode: this.currUser?.branch,
        branchCodeCustomer: this.info?.branchCode,
        toiExisting: this.info?.toiExisting,
        dtttrrExisting: this.info?.dtttrr,
        customerType: this.info?.customerType?.toLocaleLowerCase() === 'khách hàng cũ' ? 'OLD' : this.info?.customerType?.toLocaleLowerCase() === 'khách hàng mới' ? 'NEW' : '',
        customerSegment: Array.isArray(this.info?.segment) ? this.info?.segment?.[0] : this.info?.segment || this.mapSegment(this.info?.segment) || '',
        businessRegistrationNumber: this.info?.businessRegistrationNumber,
        classification: this.info?.classification,
        creditRanking: this.info?.scoreValue,
        isRmManager: this.info.isManage,
        interestType: this.form.controls.typeInterest.value,
        productCode: this.AllTypeFee.find(item => item.id === this.form.controls.typeInterest.value).code,
        approvedDateMO: moment(this.form.controls.dateApprovalMO.value).toDate(),
        codeHTCNTT: this.form.controls.codeCntt.value?.trim(),
        moCode: this.form.controls.moCode.value?.trim(),
        userApproved: this.form.controls.authApproval.value?.trim(),
        roleApproved: this.form.controls.titleAuthApproval.value?.trim(),
        reason: this.form.controls.codeReason.value,
        fullNameApproved: this.form.controls.fullName.value?.trim(),
        commitment: this.form.controls.commitment.value,
      };

      this.isLoading = true;
      this.reductionProposalApi.saveException(payload).subscribe(
        (result) => {
          this.infoSave = result;
          this.isLoading = false;
          this.messageService.success('Cập nhật thành công. Mã tờ trình: ' + result.code);
          if (backToList) {
            setTimeout(() => {
              this.router.navigateByUrl(functionUri.reduction_proposal, { state: this.prop ? this.prop : this.state });
            }, 2000);
          }
          setTimeout(() => {
            resolve(true);
          }, 100);
        },
        (e) => {
          if (e?.error) {
            this.messageService.error(e?.error?.description);
          } else {
            this.messageService.error(_.get(this.notificationMessage, 'error'));
          }
          this.isLoading = false;
          reject(false);
        });
    });
  }

  async confirmReductionException(){
    await this.save(false);
    const body = {
      code: this.infoSave.code,
      type: 1,
      classification: this.info?.classification == '--' ? '' : this.info?.classification,
      creditRanking: this.info?.scoreValue == '--' ? '' : this.info?.scoreValue,
      customerType: this.info?.customerType?.toLocaleLowerCase() === 'khách hàng cũ' ? 'OLD' : this.info?.customerType?.toLocaleLowerCase() === 'khách hàng mới' ? 'NEW' : '',
      dtttrrExisting: this.info?.dtttrrExisting,
      customerName: this.infoSave.customerName,
    }
    return new Promise<any>(async (resolve, reject) => {
      this.reductionProposalApi.confirmReductionException(body).subscribe(
        (result) => {
          this.infoSave = result;
          this.isLoading = false;
          this.messageService.success('Trình cán bộ quản lý thành công. Mã tờ trình: ' + result);
          setTimeout(() => {
            this.router.navigateByUrl(functionUri.reduction_proposal, { state: this.prop ? this.prop : this.state });
          }, 2000);
          setTimeout(() => {
            resolve(true);
          }, 100);
        },
        (e) => {
          if (e?.error) {
            this.messageService.error(e?.error?.description);
          } else {
            this.messageService.error(_.get(this.notificationMessage, 'error'));
          }
          this.isLoading = false;
          reject(false);
        })
    });
  }

  initTOI() {
    const toi = this.detail?.reductionProposalTOIDTO;
    this.info.toi = {
      duNoNganHanBq: toi?.dunoBqSd,
      duNoNganHanBqUnit: toi?.dunoBqNim,
      duNoTdhBq: toi?.dunoTdhBqSd,
      duNoTdhBqUnit: toi?.dunoTdhBqNim,
      coKyHanBq: toi?.ckhBqSd,
      coKyHanBqUnit: toi?.ckhBqNim,
      khongKyHanBq: toi?.kkhBqSd,
      khongKyHanBqUnit: toi?.kkhBqNim,
      thuBl: toi?.tblDsSd,
      thuBlUnit: toi?.tblNim,
      thuTTQT: toi?.tttqtDsSd,
      thuTTQTUnit: toi?.tttqtMpqd,
      thuFx: toi?.tfxDsSd,
      thuFxUnit: toi?.tfxMpqd,
      thuBanca: toi?.thuBancaDsSd,
      thuKhac: toi?.thuKhacDsSd,
      dtttrr: this.detail?.dtttrrConfirm,
      toi: this.detail?.toiConfirm,
      // Hieu qua
      ckhBqHq: toi?.ckhBqHq,
      dunoBqHq: toi?.dunoBqHq,
      dunoTdhBqHq: toi?.dunoTdhBqHq,
      kkhBqHq: toi?.kkhBqHq,
      tblHq: toi?.tblHq,
      tfxHq: toi?.tfxHq,
      thuBanCaHq: toi?.thuBanCaHq,
      thuKhacHq: toi?.thuKhacHq,
      tttqtHq: toi?.tttqtHq
    };
    this.reductionProposalApi.info.next(this.info);
  }

  initInterestData() {
    this.listData = this.detail?.reductionProposalDetailsDTOList.map(item => {
      return {
        data: {
          code: this.detail?.scopeType === 1 ? item?.planCode : this.detail?.scopeType === 2 ? item?.ldCode : '',
          optionCode: item?.planCode,
          ldCode: item?.ldCode,
          productCode: item?.itemCode,
          name: item?.itemName,
          currency: item?.interestUnit,
          adjustmentPeriod: item?.interestAdjustPeriod,
          period: item?.interestLoanPeriod,
          referenceInterestRate: item?.interestReference,
          baseMargin: item?.interestAmplitude,
          maximumMarginReduction: item?.interestMaxReduction ? item?.interestMaxReduction : null,
          loanRate: item?.interestValue,
          marginSuggest: item?.interestProposedAmplitude,
          loanSuggest: item?.interestProposedValue,
          reductPercent: ((Number(item?.interestAmplitude) - Number(item?.interestProposedAmplitude)) / Number(item?.interestAmplitude) * 100).toFixed(2),
          minInterestRate: item?.minInterestRate,
          rateMinimum: item?.minInterestRateOffer,
          rateInterestReduction: ((Number(item?.minInterestRate) - Number(item?.minInterestRateOffer)) / Number(item?.minInterestRate) * 100).toFixed(2)
        }
      };
    });

    const scopeApplyJson = JSON.parse(this.detail?.scopeApply);
    switch (this.detail?.scopeType) {
      case 0:
        this.interestData = scopeApplyJson;
        break;
      case 1:
        this.optionCodeData = scopeApplyJson;
        break;
      case 2:
        this.ldCodeData = scopeApplyJson;
        break;
    }
    this.treeTableInterest.updateSerializedValue();
  }





  mapSegment(segment: any): string {
    if(segment == 'Không xác định'){
      return '';
    }
    if(segment){
      if(this.dataCommonSegment){
        return this.dataCommonSegment?.filter(item => {return item.description == segment}).map(value => value?.name)?.length != 0 ? this.dataCommonSegment?.filter(item => {return item.description == segment}).map(value => value?.name) : segment;
      }
      else {
        return segment;
      }
    }
    else {
      return '';
    }
  }

  // Function main
  /**
   * Lấy thông tin biểu lãi
   */
  openInterestModal(type: string, item) {

    let segmentCode = '';
    if(this.info?.blockCode !== Division.CIB){
      segmentCode = this.info?.segmentCode || this.form.controls.segment.value;
      if (!segmentCode && !this.existed) {
        this.messageService.warn('Vui lòng chọn phân khúc khách hàng');
        return;
      }
    }

    const planType = this.form?.controls?.scopeApply?.value;
    const config = {
      selectionType: type === 'interest' && planType !== 'UNKNOWN' ? SelectionType.multi : SelectionType.single
    };
    const { toi } = this.form.getRawValue();
    let toiCommit;
    if (this.info.isNew) {
      toiCommit = toi;
    } else {
      toiCommit = this.convertToiCurrent(this.info.toiExisting);
    }
    const modal = this.modal.open(InterestModalComponent, { windowClass: 'list__interest-modal' });
    modal.componentInstance.config = config;
    modal.componentInstance.customerObject = this.info.customerCode ? this.info?.classification : 'NORMAL'; // Loại khách hàng
    modal.componentInstance.customerSegment = this.info?.blockCode === Division.CIB ? Division.CIB : this.info?.blockCode + segmentCode; // Phân khúc
    modal.componentInstance.toi = toiCommit; // thông tin toi
    modal.componentInstance.scoreValue = this.info?.scoreValue || 'BB'; // Xếp hạng tín dụng
    modal.componentInstance.productCode = this.AllTypeFee.find(item => item.id === this.form.controls.typeInterest.value)?.code ; // product code sang MS
    modal.componentInstance.productType = this.AllTypeFee.find(item => item.id === this.form.controls.typeInterest.value)?.productType; // product type sang MSmodal.componentInstance.blockCode = this.form.controls.typeInterest.value === 6 ? 'CIB' : division; // Thông tin khối
    modal.componentInstance.reload();
    modal.componentInstance.listCode = type === 'interest' ? item.map(item => {
      return item.data.productCode;
    }) : [item?.productCode];
    modal.result
      .then((res) => {
        if (res) {
          if (type === 'option') {
            if (res.listSelected.length > 0) {
              const selected = res.listSelected;
              item.productCode = selected[0]?.productCode;
              item.name = selected[0]?.name;
              item.currency = selected[0]?.currency;
              item.adjustmentPeriod = selected[0]?.adjustmentPeriod;
              item.period = selected[0]?.period;
              item.referenceInterestRate = Number(selected[0]?.referenceInterestRate).toFixed(2);
              item.baseMargin = Number(selected[0]?.baseMargin).toFixed(2);
              item.maximumMarginReduction = selected[0]?.maximumMarginReduction
                ? Number(selected[0]?.maximumMarginReduction).toFixed(2) : null;
              item.loanRate = (Number(selected[0]?.baseMargin) + Number(selected[0]?.referenceInterestRate)).toFixed(2);
              item.marginSuggest = null;
              item.loanSuggest = null;
              item.rateMinimum = null;
              item.rateInterestReduction = null;
              item.minInterestRate = selected[0]?.minInterestRate;
              this.mapTable('option-interest', item);
            }
          } else {
            if (planType === 'UNKNOWN') {
              this.interestData = _.uniqBy([...res.listSelected], (i) => i.productCode);
              this.listData = [];
              this.mapTable('interest', null);
            } else {
              this.interestData = _.uniqBy([...this.interestData, ...res.listSelected], (i) => i.productCode);
              this.interestData = _.remove(this.interestData, (i) => _.indexOf(res.listRemove, i.productCode) === -1);
              this.listData = _.remove(this.listData, (i) => _.indexOf(res.listRemove, i.data?.productCode) === -1);
              this.mapTable('interest', null);
            }
          }
        }
      })
      .catch(() => {
      });
  }

  openOptionCodeModal() {
    const config = {
      selectionType: SelectionType.multi
    };
    
    const modal = this.modalService.open(OptionCodeModalComponent, { windowClass: 'list__optioncode-modal' });
    modal.componentInstance.config = config;
    modal.componentInstance.customerCode = this.info?.customerCode; // "";
    modal.componentInstance.businessRegistrationNumber = this.info?.businessRegistrationNumber || this.info?.taxCode;
    modal.componentInstance.idCard = this.info?.idCard;
    modal.componentInstance.type = 1;
    modal.componentInstance.blockCode = this.info?.blockCode;
    modal.componentInstance.listCode = this.optionCodeData?.map((item) => item.optionCode);
    modal.result
      .then((res) => {
        if (res) {
          this.optionCodeData = _.uniqBy([...this.optionCodeData, ...res.listSelected], (i) => i.optionCode);
          this.optionCodeData = _.remove(this.optionCodeData, (i) => _.indexOf(res.listRemove, i.optionCode) === -1);
          // Lãi
          this.listData = _.remove(this.listData, (i) => _.indexOf(res.listRemove, i.data?.code) === -1);
          this.mapTable('option', null);
        }
      })
      .catch(() => {
      });
  }

  openLdCodeModal() {
    const config = {
      selectionType: SelectionType.multi
    };
    const modal = this.modalService.open(LdCodeModalComponent, { windowClass: 'list__ldcode-modal' });
    modal.componentInstance.config = config;
    modal.componentInstance.listLD = this.listLD;
    modal.componentInstance.listCode = this.ldCodeData?.map((item) => item.ldCode);
    modal.componentInstance.customerCode = this.info?.customerCode;
    modal.componentInstance.reload();
    modal.result
      .then((res) => {
        if (res) {
          this.ldCodeData = _.uniqBy([...this.ldCodeData, ...res.listSelected], (i) => i.ldCode);
          this.ldCodeData = _.remove(this.ldCodeData, (i) => _.indexOf(res.listRemove, i.ldCode) === -1);
          if (this.info.type === 3) {
            // Lãi
            this.listData = _.remove(this.listData, (i) => _.indexOf(res.listRemove, i.data?.code) === -1);
            this.mapTable('ld', null);
          }
        }
      })
      .catch(() => {
      });
  }



  mapTable(type: string, interestItem) {
    const division = this.info.blockCode;
    const segmentCode = this.info.segmentCode;
    const scoreValue = this.info.scoreValue;
    const rowData = [];
    const currentSize = this.listData.length;

    let existed;
    switch (type) {
      case 'interest':
        existed = this.listData.map(item => item?.data?.productCode);
        this.interestData?.filter((item) => !existed.includes(item?.productCode)).map((item) => {
          rowData.push(
            {
              data: {
                code: '',
                productCode: item?.productCode,
                name: item.name,
                currency: item.currency,
                adjustmentPeriod: item.adjustmentPeriod,
                period: item.period,
                referenceInterestRate: Number(item.referenceInterestRate).toFixed(2),
                baseMargin: Number(item.baseMargin).toFixed(2),
                maximumMarginReduction: item.maximumMarginReduction ? Number(item.maximumMarginReduction || 0).toFixed(2) : null,
                loanRate: (Number(item.baseMargin) + Number(item.referenceInterestRate)).toFixed(2),
                marginSuggest: Number(item.baseMargin).toFixed(2),
                loanSuggest: 0,
                reductPercent: 0,
                rateMinimum: item?.minInterestRate,
                rateInterestReduction: 0,
                minInterestRate: item?.minInterestRate
              }
            }
          );
        });
        this.listData = [...this.listData, ...rowData];
        console.log(this.listData);
        break;
      case 'option':
        existed = this.listData.map(item => item?.data?.code);
        this.optionCodeData?.filter((item) => !existed.includes(item?.optionCode)).map((item) => {
          rowData.push(
            {
              data: {
                code: item.optionCode,
                productCode: null,
                name: null,
                currency: null,
                adjustmentPeriod: null,
                period: null,
                referenceInterestRate: null,
                baseMargin: null,
                maximumMarginReduction: null,
                loanRate: null,
                marginSuggest: Number(item.baseMargin).toFixed(2),
                loanSuggest: null,
                reductPercent: null,
                rateMinimum: null,
                rateInterestReduction: null,
                minInterestRate: item?.minInterestRate
              }
            }
          );
        });
        this.listData = [...this.listData, ...rowData];
        break;
      case 'ld':
        existed = this.listData.map(item => item?.data?.code);
        this.ldCodeData?.filter((item) => !existed.includes(item?.ldCode)).map((item) => {
          // Lấy thông tin biểu lãi dựa vào các chỉ số của LD
          this.reductionProposalApi.getInterestRate({
            adjustmentPeriod: item?.Y_TS_TDLS,
            currency: item?.NTE,
            customerSegment: division === 'CIB' ? division : division + segmentCode,
            period: item?.KYHANTHANG ? item?.KYHANTHANG?.toString().substr(0, item?.KYHANTHANG?.toString().length - 1) : '',
            rankingCredit: scoreValue ? scoreValue : 'BB',
            customerType: this.info.customer.classification
          }).subscribe((res) => {
            // API bị lỗi sẽ trả về định dạng object
            if (Object.prototype?.toString.call(res) === '[object Object]' || (res as []).length === 0) {
              this.messageService.error('Không tìm thấy dữ liệu biểu lãi');
            } else {
              const obj: any = res[0];
              this.listData = [...this.listData, ...[
                {
                  data: {
                    code: item.ldCode,
                    productCode: obj?.marginId,
                    name: 'LSTC cho vay',
                    currency: item?.NTE,
                    adjustmentPeriod: item?.Y_TS_TDLS,
                    period: item?.KYHANTHANG,
                    referenceInterestRate: item?.INTEREST_FIX,
                    baseMargin: item?.BIENDOLS,
                    maximumMarginReduction: obj?.maximumMarginReduction ? obj?.maximumMarginReduction : null,
                    loanRate: item?.LS,
                    marginSuggest: null,
                    loanSuggest: null,
                    reductPercent: null,
                    codeInterest: obj?.productCode
                  }
                }
              ]];
            }
          });
        });
        break;
      case 'option-interest':
        for (let index = 0; index < this.listData.length; index++) {
          const element = this.listData[index];
          if (interestItem?.code === element?.data?.code) {
            console.log('option-interest: ', interestItem);
            this.listData[index] = {
              data: {
                code: interestItem?.code,
                productCode: interestItem?.productCode,
                name: interestItem.name,
                currency: interestItem.currency,
                adjustmentPeriod: interestItem.adjustmentPeriod,
                period: interestItem.period,
                referenceInterestRate: Number(interestItem.referenceInterestRate).toFixed(2),
                baseMargin: Number(interestItem.baseMargin).toFixed(2),
                maximumMarginReduction: interestItem?.maximumMarginReduction
                  ? Number(interestItem.maximumMarginReduction).toFixed(2) : null,
                loanRate: (Number(interestItem.baseMargin) + Number(interestItem.referenceInterestRate)).toFixed(2),
                marginSuggest: Number(interestItem.baseMargin).toFixed(2),
                loanSuggest: 0,
                reductPercent: 0,
                rateMinimum: interestItem?.minInterestRate,
                rateInterestReduction: 0,
                minInterestRate: interestItem?.minInterestRate
              }
            };
          }

        }
        break;
    }
    this.form.controls.listData.setValue(this.listData);
    this.treeTableInterest.updateSerializedValue();
  }

  onDragOver(event): void {
    event.preventDefault();
  }

  onDropSuccess(event): void {
    const lengthFile = 1;
    const allows = ['.pdf'];
    event.preventDefault();
    if (this.listFile?.length + event.dataTransfer.files?.length > lengthFile) {
      this.messageService.error(`Chỉ được chọn tối đa ${lengthFile} file đính kèm}`);
      return;
    }
    let countMaxData = 0;
    let countNotAllow = 0;
    let errMessage = '';
    const maxSizeByte = this.maxSize * 1024 * 1024;
    for (const file of event.dataTransfer.files) {
      const name = file.name;
      if (file?.size <= maxSizeByte && allows.includes(name.substr(name.lastIndexOf('.')).toLowerCase())) {
        this.upload(file);
      } else if (file?.size > maxSizeByte) {
        countMaxData++;
      } else if (!allows.includes(name.substr(name.lastIndexOf('.')))) {
        countNotAllow++;
      }
    }
    if (countMaxData > 0) {
      errMessage = 'Dung lượng file không được vượt quá 20MB';
    }
    if (countNotAllow > 0) {
      errMessage = 'File không đúng định dạng cho phép (.pdf)';
    }
    if (errMessage?.length > 0) {
      this.messageService.warn(errMessage);
    }
  }

  changeFile(event) {
    const lengthFile = 1;
    const allows = ['.pdf'];
    if (this.listFile?.length + event.target.files?.length > lengthFile) {
      this.messageService.error(`Chỉ được chọn tối đa ${lengthFile} file đính kèm`);
      return;
    }
    let countMaxData = 0;
    let countNotAllow = 0;
    let errMessage = '';
    const maxSizeByte = this.maxSize * 1024 * 1024;
    for (const file of event.target.files) {
      const name = file.name;
      if (file?.size <= maxSizeByte && allows.includes(name.substr(name.lastIndexOf('.')).toLowerCase())) {
        this.upload(file);
      } else if (file?.size > maxSizeByte) {
        countMaxData++;
      } else if (!allows.includes(name.substr(name.lastIndexOf('.')))) {
        countNotAllow++;
      }
    }
    if (countMaxData > 0) {
      errMessage = 'Dung lượng file không được vượt quá 20MB';
    }
    if (countNotAllow > 0) {
      errMessage = 'File không đúng định dạng cho phép (.pdf)';
    }
    if (errMessage.length > 0) {
      this.messageService.warn(errMessage);
    }
  }

  openDownloadFilePopup(inputFile): void {
    inputFile.value = '';
    inputFile.click();
  }

  download(file: any) {
    const title = file?.fileName;
    this.isLoading = true;
    this.fileService.downloadFile(file?.fileId, title).subscribe((res) => {
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
      this.isLoading = false;
    }, err => {
      this.isLoading = false;
    });
  }

  upload(file: any) {
    this.isLoading = true;
    const formData = new FormData();
    formData.append('file', file);
    this.fileService.uploadFile(formData).subscribe(res => {
      const mapFile = {
        fileId: res,
        fileName: file.name,
        type: '',
        filePath: ''
      };
      this.listFile.push(mapFile);
      this.isLoading = false;
    }, err => {
      this.isLoading = false;
    });
  }

  removeFile(id) {
    this.listFile = this.listFile.filter(list => list.fileId !== id);
  }

  getInfoAuthorization(event) {
    const val = event.target.value.trim();
    if(!val){
      return;
    }
    this.isLoading = true;
    if (val) {
      this.reductionProposalApi.getInfoAuthorization(val).subscribe((item) => {
        if (item) {
          this.form.controls.fullName.setValue(item?.fullName);
          this.form.controls.titleAuthApproval.setValue(item?.positionName);
          this.fullNameError = '';
          this.titleAuthApprovalError = '';
        }
        else {
          this.messageService.warn('Không tìm thấy thông tin thẩm quyền')
          this.form.controls.fullName.setValue('');
          this.form.controls.titleAuthApproval.setValue('');
        }
        this.isLoading = false;
      }, err => {
        this.isLoading = false;
      });
    }
  }

  openPopupCalculateTOI(): void {
    const dialogRef = this.dialog.open(ToiConfirmModalComponent);
    dialogRef.componentInstance.type = this.info?.type;
    dialogRef.componentInstance.action = null;
    dialogRef.componentInstance.blockCode = this.info?.blockCode;
    dialogRef.afterClosed().subscribe(() => {
      const infoToi = this.reductionProposalApi?.info?.value;
      const toi: any = infoToi.toi;
      if (toi) {
        this.form.controls.toi.setValue(toi?.toi);
        this.form.controls.dtttrr.setValue(toi?.dtttrr);
        this.form.controls.dtttrrDisplay.setValue(this.convertDtttrr(toi?.dtttrr));
      }
    });
  }

  dateEffect() {
    if (!this.form.controls?.dateApprovalMO?.value || !this.form.controls?.effect?.value) {
      return '';
    }
    const date = moment(this.form.controls?.dateApprovalMO?.value, 'DD/MM/YYYY');
    const ennDateMonth = moment(this.form.controls?.dateApprovalMO?.value, 'DD/MM/YYYY').endOf('month');
    if(date.format('DD/MM/YYYY') == ennDateMonth.format('DD/MM/YYYY')) {
      return moment(date.add(this.form.controls?.effect?.value, 'month').calendar('DD/MM/YYYY'), 'MM/DD/YYYY').endOf('month').format('DD/MM/YYYY');
    }
    return moment(date.add(this.form.controls?.effect?.value, 'month').calendar('DD/MM/YYYY'), 'MM/DD/YYYY').format('DD/MM/YYYY')
  }

  // Common function
  backStep(): void {
    this.router.navigateByUrl(functionUri.reduction_proposal, { state: this.prop ? this.prop : this.state });
  }

  handlerEffect(event, type: number) {
    if (event.value) {
      if(type == 1){
        this.monthConfirmError = '';
      } else {
        this.effectError = '';
      }
    }
  }

  handlerDate(event) {
    if (event) {
      this.dateApprovalMOError = '';
    }
    if (isNaN(Date.parse(event))) {
      this.form.controls.dateApprovalMO.setValue(new Date());
    }
  }

  handlerError(event, type: number) {
    const val = event.target.value;
    if (val) {
      switch (type) {
        case 1:
          this.fullNameError = '';
          break;
        case 2:
          this.dateApprovalMOError = '';
          break;
        case 3:
          this.dateEndApproveError = '';
          break;
        case 4:
          this.authApprovalError = '';
          break;
        case 5:
          this.titleAuthApprovalError = '';
          break;
        case 6:
          this.codeCnttError = '';
          break;
        case 7:
          this.moCodeError = '';
          break;
      }
    }
  }

  checkValidParam() {
    let check = false;

    if(!this.form.controls.monthConfirm.value) {
      this.monthConfirmError = 'Lựa chọn thời gian đánh giá';
      check = true;
    }

    if (!this.listData.length) {
      this.messageService.warn('Vui lòng chọn biểu lãi');
      return true;
    }

    if (!this.form.controls.effect.value) {
      this.effectError = 'Lựa chọn hiệu lức của tờ trình';
      check = true;
    }
    if (!this.form.controls.dateApprovalMO.value) {
      this.dateApprovalMOError = 'Nhập thông tin ngày duyệt tờ trình trên MO';
      check = true;
    }
    if (!this.form.controls.authApproval.value?.trim?.()) {
      this.authApprovalError = 'Nhập thẩm quyền phê duyệt';
      check = true;
    }
    if(!this.form.controls.fullName.value?.trim?.()){
      this.fullNameError = 'Nhập tên thẩm quyền phê duyệt';
      check = true;
    }
    if (!this.form.controls.titleAuthApproval.value?.trim?.()) {
      this.titleAuthApprovalError = 'Nhập chức danh thẩm quyền';
      check = true;
    }
    if (!this.form.controls.codeReason.value) {
      this.codeReasonError = 'Chọn lý do trình ngoại lệ';
      check = true;
    }
    if (!this.form.controls.codeCntt.value?.trim?.() && this.form.controls.codeReason.value == 1) {
      this.codeCnttError = 'Nhập mã hotrocntt';
      this.messageService.warn('Bắt buộc nhập mã hotrocntt trong trường hợp chọn lý do là \"Hệ thống lỗi\"');
      check = true;
    }
    if (!this.form.controls.moCode.value?.trim?.()) {
      this.moCodeError = 'Nhập mã tờ trình trên MO';
      check = true;
    }

    return check;
  }

  isValid() {
    let errCount = 0;
    let errMessage = '';
    if (this.listData?.length === 0) {
      errMessage += 'Chưa nhập thông tin giảm lãi suất. ';
      errCount++;
    } else {
      const fieldErr = [];
      this.listData?.map(item => {
        if (item.data?.marginSuggest === null && !fieldErr.includes('marginSuggest')) {
          errMessage += 'Nhập đầy đủ biên độ đề xuất. ';
          fieldErr.push('marginSuggest');
          errCount++;
        }
      });
    }
    if (Utils.isStringNotEmpty(errMessage)) {
      this.messageService.warn(errMessage);
    }
    return errCount === 0;
  }


  checkSegment(): boolean {
    if(this.existed){
      return true;
    }
    else {
      if (this.info?.segment){
        return true
      }
    }
  }

  formatCurrency(element) {
    if (element == 0) {
      return '0';
    }
    if (!element || element === 'NaN' || element === '--') {
      return '--';
    }
    let formated: string = element?.toString();
    let first = '';
    let last = '';
    if (formated.includes('.')) {
      first = formated.substr(0, formated.indexOf('.'));
      last = formated.substr(formated.indexOf('.'));
    } else {
      first = formated;
    }
    if (Number(first)) {
      first = Number(first).toLocaleString('en-US', {
        style: 'currency',
        currency: 'VND'
      });
      // first = first.trim().substr(1, first.length);
      first = first.trim().replace('₫', '');
      formated = first + last;
    }
    return formated;
  }

  convertToiCurrent(toi) {
    if (toi) {
      toi = Number(Number(toi) * 100).toFixed(2);
    }
    return toi;
  }

  convertDtttrr(dtttrr) {
    if (dtttrr) {
      dtttrr = Number(Number(dtttrr) / 1000000).toFixed(2);
    }
    return dtttrr;
  }

  getValueMax(row) {
    const maximumMarginReduction = row?.maximumMarginReduction || 0;
    // let baseMargin = row?.baseMargin || 0;
    if (maximumMarginReduction && maximumMarginReduction > 0)
      // return (baseMargin - maximumMarginReduction).toFixed(2)
      return maximumMarginReduction;
    else
      return '--';
  }

  calculateMarginSuggest(event, row) {
    const val = event.target.value;
    if (Number(val) <= 0) {
      row.marginSuggest = row?.baseMargin;
      row.loanSuggest = 0;
      row.reductPercent = 0;
    }
    this.treeTableInterest.updateSerializedValue();
  }

  changeMarginSuggest(event, data): void {
    const val = event.target.value;
    if (val) {
      data.loanSuggest = (Number(data.referenceInterestRate) + Number(val)).toFixed(2);
      data.reductPercent = ((Number(data.baseMargin) - Number(val)) / Number(data.baseMargin) * 100).toFixed(2);
      if (Number(val) < 0) {
        if(!data.errorMessage){
          this.messageService.warn('Biên độ đề xuất > 0');
        }
        data.errorMessage = 'Biên độ đề xuất > 0';
      }
      else {
        delete data.errorMessage;
      }
    } else {
      data.loanSuggest = null;
      data.reductPercent = null;
      delete data.errorMessage;
    }
  }

  calculateInterest(event, row) {
    const val = event.target.value;
    if (Number(val) < 0) {
      row.rateMinimum = row?.minInterestRate;
      row.rateInterestReduction = 0;
    }
    this.treeTableInterest.updateSerializedValue();
  }

  getValueMinimumRate(event, row) {
    const val = event.target.value;
    row.rateInterestReduction = ((Number(row?.minInterestRate) - Number(val)) / Number(row?.minInterestRate) * 100).toFixed(2);
  }

  checkTableInterest() {
    return (this.form.controls.typeInterest.value !== 1 && this.form.controls.typeInterest.value !== 6);
  }

  removeItem(data): void {
    const scopeApply = this.form.controls.scopeApply.value;
    if (scopeApply === 'ALL') {
      this.listData = this.listData?.filter(item => {
        return item?.data?.productCode !== data?.productCode;
      });
      this.interestData = this.interestData?.filter(item => {
        return item?.productCode !== data?.productCode;
      });
    } else if (scopeApply === 'UNKNOWN') {
      this.listData = this.listData?.filter(item => {
        return item?.data?.productCode !== data?.productCode;
      });
      this.interestData = this.interestData?.filter(item => {
        return item?.productCode !== data?.productCode;
      });
    } else {
      this.listData = this.listData?.filter(item => {
        return item?.data?.code !== data?.code;
      });
      this.interestData = this.interestData?.filter(item => {
        return item?.productCode !== data?.productCode;
      });
      if (scopeApply === 'OPTION_CODE') {
        this.optionCodeData = this.optionCodeData?.filter(item => {
          return item?.optionCode !== data?.code;
        });
      } else if (scopeApply === 'LD_CODE') {
        this.ldCodeData = this.ldCodeData?.filter(item => {
          return item?.ldCode !== data?.code;
        });
      }
    }
  }

  checkFile() {
    if (!this.listFile.length) {
      this.messageService.warn('Vui lòng chọn file !');
      return false;
    }
    return true;
  }

  reset() {
    this.listData = [];
    this.interestData = [];
    this.optionCodeData = [];
    this.ldCodeData = [];
  }

  resetBySegment() {
    this.reset();
    this.form.controls.monthConfirm.setValue(null);
    this.form.controls.scopeApply.setValue('ALL');
    this.form.controls.typeInterest.setValue(1);
    this.form.controls.effect.setValue(1);
    this.form.controls.dateApprovalMO.setValue(null);
    this.form.controls.authApproval.setValue(null);
    this.form.controls.fullName.setValue(null);
    this.form.controls.titleAuthApproval.setValue(null);
    this.form.controls.codeReason.setValue(1);
    this.form.controls.codeCntt.setValue(null);
    this.form.controls.moCode.setValue(null);
  }

  async approval(status) {
    const body = {
      status,
      code: this.detail?.code,
      type: 1
    };
    this.isLoading = true;
    this.reductionProposalApi.CBQLconfirmReduction(body).subscribe(res => {
      console.log(res);
      this.messageService.success(`${status == 1 ? 'TỪ CHỐI' : 'ĐỒNG Ý'} thành công`);
      this.isLoading = false;
      this.router.navigate([functionUri.reduction_proposal],{ state: {formSearch: this.state?.formSearch} });
    }, err => {
      this.isLoading = false;
      this.messageService.error('Có lỗi xảy ra, vui lòng thử lại sau');
      this.router.navigate([functionUri.reduction_proposal],{ state: {formSearch: this.state?.formSearch} });
    });
  }

}
