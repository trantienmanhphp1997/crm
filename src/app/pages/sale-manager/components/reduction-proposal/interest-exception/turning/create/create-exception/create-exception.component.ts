import { AfterViewInit, ChangeDetectorRef, Component, Injector, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '../../../../../../../../core/components/base.component';
import { MatDialog } from '@angular/material/dialog';
import { ReductionProposalApi } from '../../../../../../api/reduction-proposal.api';
import { CustomerApi, CustomerDetailApi } from '../../../../../../../customer-360/apis';
import { CustomerDetailSmeApi } from '../../../../../../../customer-360/apis/customer.api';
import { CategoryService } from '../../../../../../../system/services/category.service';
import { BranchApi } from '../../../../../../../rm/apis';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FileService } from '../../../../../../../../core/services/file.service';
import {
  CommonCategory,
  Division,
  FunctionCode,
  functionUri
} from '../../../../../../../../core/utils/common-constants';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Utils } from '../../../../../../../../core/utils/utils';
import { SelectionType } from '@swimlane/ngx-datatable';
import { InterestModalComponent } from '../../../../dialog/select-interest-modal/select-interest-modal.component';
import * as _ from 'lodash';
import { OptionCodeModalComponent } from '../../../../dialog/select-optioncode-modal/select-optioncode-modal.component';
import { LdCodeModalComponent } from '../../../../dialog/select-ldcode-modal/select-ldcode-modal.component';
import { TreeTable } from 'primeng/treetable';
import * as moment from 'moment/moment';
import { ToiConfirmModalComponent } from '../../../../dialog/toi-confirm-modal/toi-confirm-modal.component';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-create-exception',
  templateUrl: './create-exception.component.html',
  styleUrls: ['./create-exception.component.scss']
})
export class CreateExceptionComponent extends BaseComponent implements OnInit, AfterViewInit  {
  @ViewChild('treeTableInterest') treeTableInterest: TreeTable;
  customer360Fn: any;
  stateRouter: any
  findPotentialCustomer: any;
  customer: any;
  formSearch: any;
  info: any
  customerInfo: any
  existed: boolean
  titleAuthApprovalError: string;
  effectError: string;
  dateApprovalMOError: string;
  dateEndApproveError: string;
  authApprovalError: string;
  fullNameError: string;
  codeReasonError: string;
  codeCnttError: string;
  moCodeError: string;
  isManage: boolean;
  monthConfirmError: string;
  maxDate = new Date();
  maxSize = 20;
  infoSave: any;

  // Array
  reasonException = [];
  monthConfirms = [];
  months = [];
  listLD = [];
  listData = [];
  interestData = [];
  optionCodeData = [];
  ldCodeData = [];
  listFile = [];


  dataCommonSegment: any;
  form = this.fb.group({
    scopeApply: 'ALL',
    toi: null,
    dtttrr: null,
    monthApply: null,
    monthConfirm: null,
    listData: [],
    segment: '',
    dtttrrDisplay: null,
    typeInterest: 1,
    effect: 1,
    dateApprovalMO: null,
    authApproval: null,
    rmConfirm: false,
    titleAuthApproval: null,
    codeCntt: null,
    moCode: [null, [Validators.required, Validators.pattern(/^[^!@#$%\^&*()+_{:?<>]+$/)]],
    dateEndApprove: null,
    codeReason: null,
    commitment: true,
    fullName: null
  });

  // Common data
  allScopeApples = [
    { code: 'ALL', displayName: 'Tất cả các phương án', caseSuggest: 'ALL', typeNum: 0 },
    { code: 'OPTION_CODE', displayName: 'Mã phương án hạn mức', caseSuggest: 'ALL', typeNum: 1 }
  ];
  AllTypeFee = [
    {
      id: 1, code: 10, displayName: 'Biểu lãi suất thông thường', name: 'Thông thường',
      caseSuggest: 'NORMAL', productType: 'TT', typeNum: 0
    },
    {
      id: 2, code: 40, displayName: 'Biểu lãi suất ưu đãi (AIRS)', name: 'Ưu đãi',
      caseSuggest: 'ENDOW', productType: 'UD', typeNum: 0
    },
    {
      id: 4, code: 30, displayName: 'Biểu lãi suất linh hoạt', name: 'Linh hoạt',
      caseSuggest: 'ABILITY', productType: 'UD', typeNum: 0
    }
  ];

  constructor(
    injector: Injector,
    public dialog: MatDialog,
    private reductionProposalApi: ReductionProposalApi,
    private customerApi: CustomerApi,
    private customerDetailSmeApi: CustomerDetailSmeApi,
    private customerDetailApi: CustomerDetailApi,
    private categoryService: CategoryService,
    private branchApi: BranchApi,
    private modal: NgbModal,
    private dtc: ChangeDetectorRef,
    private fileService: FileService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REDUCTION_PROPOSAL}`);
    this.customer360Fn = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.stateRouter = this.router.getCurrentNavigation().extras.state;
    this.findPotentialCustomer = this.router.getCurrentNavigation().extras.state?.findPotentialCustomer;
    this.customer = this.router.getCurrentNavigation().extras.state?.customer;
    this.formSearch = this.router.getCurrentNavigation().extras.state?.formSearch;

  }
  ngOnInit(): void {
    this.initData();
    this.existed = this.stateRouter?.customer.existed;
    const month = [];
    const monthConfirm = [];

    for (let index = 1; index < 13; index++) {
      const item = { code: index, displayName: index.toString() };
      if (index < 7) {
        monthConfirm.push(item);
      }
      month.push(item);
    }
    this.months = month;
    this.monthConfirms = monthConfirm;
  }

  initData(){
    // Check customer is exists customerCode or exists taxCode
    const customerHaveCode = this.customer?.customerCode;
    this.isLoading = true;
    return new Promise(async (resolve, reject) => {
      // get common data
      forkJoin([
        this.categoryService.getCommonCategory(CommonCategory.REASON_EXCEPTION_REDUCTION).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.REVENUE_CUSTOMER_SEGMENT).pipe(catchError(() => of(undefined))),
      ]).subscribe(([categoryReason, customerSegment]) => {
        if (categoryReason.content) {
          categoryReason.content.forEach((value) => {
            this.reasonException.push({ code: value.code, name: value.value });
          });
          this.form.controls.codeReason.setValue(1);
        }
        if(customerSegment.content){
          this.dataCommonSegment = customerSegment.content;
        }
      });

      // implement body for api get inform customer
      const body = {
        customerCode: this.customer?.customerCode,
        taxCode: this.customer?.taxCode,
        blockCode: this.customer?.blockCode
      };
      const searchSmeBody = {
        pageNumber: 0,
        pageSize: 10,
        rsId: this.customer360Fn?.rsId,
        scope: 'VIEW',
        customerType: '',
        search: this.customer?.customerCode,
        searchFastType: 'CUSTOMER_CODE',
        manageType: 1,
        customerTypeSale: 1
      };
      const param = {
        customerCode: this.customer?.customerCode,
        businessRegistrationNumber: '',
        loanId: ''
      };
      const bodyCredit = {
        cif: this.customer?.customerCode,
        taxId: '',
        nationalId: '',
        lob: this.customer?.blockCode
      };

      if(customerHaveCode){
        forkJoin([
          this.reductionProposalApi.findExistingCustomer(body).pipe(catchError(() => of(undefined))),
          this.customerDetailApi.getByCode(this.customer?.customerCode).pipe(catchError(() => of(undefined))),
          this.customerApi.searchSme(searchSmeBody).pipe(catchError(() => of(undefined))),
          this.reductionProposalApi.getScopeReduction(param).pipe(catchError(() => of(undefined))),
          this.reductionProposalApi.getToiExisting(this.customer?.customerCode).pipe(catchError(() => of(undefined))),
          this.reductionProposalApi.getCustomerCredit({ bodyCredit }).pipe(catchError(() => of(undefined))),
        ]).subscribe(([findExistingCustomer, getByCode, searchSme, getScopeReduction, getToiExisting, getCustomerCredit]) =>{
          const customerType = getCustomerCredit?.data?.customerType;
          this.customerInfo = { ...findExistingCustomer, ...getByCode, ...{ searchSme: searchSme[0] }, ...getScopeReduction, ...getToiExisting, ...{ customerType } };
          console.log('this.customerInfo 1: ', this.customerInfo);
          this.mappingDataForView(this.customerInfo);
          console.log('this.info: ', this.info);
          this.isLoading = false;
        }, err => {
          reject(err);
          this.isLoading = false;
        });
      }
      // Get customer info for not exists customerCode
      else {
        const paramScopeReduction = {
          businessRegistrationNumber: this.findPotentialCustomer?.businessRegistrationNumber || '',
          loanId: ''
        };

        forkJoin([
          this.reductionProposalApi.getScopeReduction(paramScopeReduction).pipe(catchError(() => of(undefined))),
          this.reductionProposalApi.getKhdnPotentialCustomerSegment(this.customer?.taxCode).pipe(catchError(() => of(undefined))),
        ]).subscribe(([getScopeReduction, getKhdnPotentialCustomerSegment]) => {
          this.customerInfo = { ...getScopeReduction, ...getKhdnPotentialCustomerSegment, ...this.findPotentialCustomer};
          console.log('this.customerInfo 2: ', this.customerInfo);
          this.mappingDataForView(this.customerInfo);
          console.log('this.info: ', this.info);
          this.isLoading = false;
          resolve(true)
        });
      }

    })
  }

  ngAfterViewInit(): void {
    this.form.controls.scopeApply.valueChanges.subscribe(() => {
      this.form.controls.typeInterest.setValue(1);
      this.reset();
    });

    this.form.controls.segment.valueChanges.subscribe((value) => {
      this.reset();

    });
    this.form.controls.typeInterest.valueChanges.subscribe((value) => {
      this.reset();
    });
    this.form.controls.codeReason.valueChanges.subscribe((value) => {
      if (value.code !== 1) {
        this.codeCnttError = '';
      }
    });
    this.dtc.detectChanges();
  }

  mappingDataForView(customerInfo: any){
    this.info = {
      customerCode: this.customerInfo?.customerCode || this.customerInfo?.searchSme?.customerCode || '',
      customerName: this.customerInfo?.customerName || this.customerInfo?.searchSme?.customerName  || this.customerInfo?.fullName || '',
      taxCode: this.customerInfo?.taxCode || this.customerInfo?.searchSme?.taxCode || '',
      businessRegistrationNumber: this.customerInfo?.businessRegistrationNumber || '',
      segment: this.customerInfo?.searchSme?.segment || this.mapSegment(this.customerInfo?.data?.[0]?.cstSegStd) || '',
      segmentCode: this.customerInfo?.searchSme?.segmentCode || this.customerInfo?.data?.[0]?.cstSegStd || '',
      scoreValue: this.customerInfo?.ketQuaXhtd || '',
      classification: this.customerInfo?.searchSme?.classification || '',
      customerType: this.customerInfo?.loai_KH === 'Khach hang cu' ? 'Khách hàng cũ' : 'Khách hàng mới',
      toiExisting: this.customerInfo?.TOI,
      dtttrr: this.customerInfo?.DTTTRR,
      toiConfirm: '',
      dtttrrConfirm: '',
      blockCode: this.customerInfo?.searchSme?.customerType || this.customer?.blockCode,
      isNew: this.customerInfo?.loai_KH !== 'Khach hang cu',
      idCard: '',
      isManage: '',
    }
  }


  save(backToList: boolean) {
    return new Promise<any>(async (resolve, reject) => {
      if (this.checkValidParam() || !this.checkFile() || !this.isValid()) {
        reject(false);
        return;
      }
      if (!this.form.controls.commitment.value) {
        this.messageService.warn('Vui lòng xác nhận cam kết');
        return;
      }

      let reductionProposalDetailsDTOList = [];

      if (this.listData.length > 0) {
        reductionProposalDetailsDTOList = this.listData?.map(item => {
          item = item.data;
          return {
            planCode: this.form.controls.scopeApply.value === 'OPTION_CODE' ? item.code : '',
            ldCode: this.form.controls.scopeApply.value === 'LD_CODE' ? item.code : '',
            itemCode: item?.productCode,  // mã biểu lãi
            itemName: item?.name, // tên biểu lãi
            maximumCost: '',
            maximumCostOffer: '',
            minimumCost: '',
            minimumCostOffer: '',
            ratioCost: '',
            ratioCostOffer: '',
            type: 3,
            interestValue: item.loanRate, // Lãi suất cho vay
            interestUnit: item.currency, // Đơn vị tiền tệ
            interestType: item.name, // Biểu lãi suất
            interestReference: item.referenceInterestRate, // Lãi suất tham chiếu (%)
            interestProposedValue: item.loanSuggest, // Lãi suất cho vay đề xuất
            interestProposedAmplitude: item.marginSuggest, // Biên độ đề xuất (%)
            interestMaxReduction: item.maximumMarginReduction,// Mức giảm biên độ tối đa (%
            interestLoanPeriod: item.period, // Kỳ hạn vay
            interestAmplitude: item.baseMargin, // Biên độ (%)
            interestAdjustPeriod: item?.adjustmentPeriod, // Kỳ điểu chỉnh lãi suất
            reductionRate: item.reductPercent, // tỷ lệ giảm
            id: '',
            minInterestRate: item.minInterestRate,
            minInterestRateOffer: item.rateMinimum
          };
        });
      }

      // scopeApply
      let scopeApplyData = [];
      switch (this.form.controls.scopeApply.value) {
        case 'LD_CODE':
          scopeApplyData = this.ldCodeData;
          break;
        case 'OPTION_CODE':
          scopeApplyData = this.optionCodeData;
          break;
        default:
          scopeApplyData = this.interestData;
          break;
      }
      const scopeApply = JSON.stringify(scopeApplyData);
      // scopeType
      const scopeType = this.allScopeApples.find(i => i.code === this.form.controls.scopeApply.value).typeNum;
      const payload = {
        blockCode: this.info?.blockCode || '',
        code: moment.now(),
        customerCode: this.info?.customerCode,
        monthApply: this.form.controls.effect.value,
        monthConfirm: this.form.controls.monthConfirm.value,
        scopeApply,
        scopeType,
        taxCode: this.info.taxCode,
        type: 3,
        leadCode: this.customerInfo?.leadCode,
        dtttrrConfirm: this.form.controls.dtttrr.value ? Number(this.form.controls.dtttrr.value.toString().replace(',', '.')) : this.form.controls.dtttrr.value ,
        toiConfirm: this.form.controls.toi.value,
        reductionProposalDetailsDTOList,
        reductionProposalDetailsApprovedDTOList: [],
        reductionProposalTOIDTO: {
          ckhBqNim: this.info?.obj?.toi?.coKyHanBqUnit || 0,
          ckhBqSd: this.info?.obj?.toi?.coKyHanBq || 0,
          dunoBqNim: this.info?.obj?.toi?.duNoNganHanBqUnit || 0,
          dunoBqSd: this.info?.obj?.toi?.duNoNganHanBq || 0,
          dunoTdhBqNim: this.info?.obj?.toi?.duNoTdhBqUnit || 0,
          dunoTdhBqSd: this.info?.obj?.toi?.duNoTdhBq || 0,
          kkhBqNim: this.info?.obj?.toi?.khongKyHanBqUnit || 0,
          kkhBqSd: this.info?.obj?.toi?.khongKyHanBq || 0,
          tblDsSd: this.info?.obj?.toi?.thuBl || 0,
          tblNim: this.info?.obj?.toi?.thuBlUnit || 0,
          tfxDsSd: this.info?.obj?.toi?.thuFx || 0,
          tfxMpqd: this.info?.obj?.toi?.thuFxUnit || 0,
          thuBancaDsSd: this.info?.obj?.toi?.thuBanca || 0,
          thuBanCaMpqd: 1,
          thuKhacDsSd: this.info?.obj?.toi?.thuKhac || 0,
          thuKhacMpqd: 1,
          tttqtDsSd: this.info?.obj?.toi?.thuTTQT || 0,
          tttqtMpqd: this.info?.obj?.toi?.thuTTQTUnit || 0,
          ckhBqHq: this.info?.obj?.toi?.ckhBqHq ? +this.info?.obj?.toi?.ckhBqHq.replace(',', '.') : 0,
          dunoBqHq: 0,
          dunoTdhBqHq: 0,
          kkhBqHq: 0,
          tblHq: 0,
          tfxHq: 0,
          thuBanCaHq: 0,
          thuKhacHq: 0,
          tttqtHq: 0,
          dttck: 0,
          tck: 0
        },

        reductionProposalFileDTOList: this.listFile,
        reductionProposalAuthorizationDTOList: [
          {
            fullName: this.form.controls.fullName.value,
            userName: this.form.controls.authApproval.value,
            titleCategory: this.form.controls.titleAuthApproval.value
          }
        ],
        customerName: this.info?.customerName,
        rmCode: this.currUser?.rmCode,
        branchCode: this.currUser?.branch,
        branchCodeCustomer: this.info?.branchCode,
        toiExisting: this.info?.toiExisting,
        dtttrrExisting: this.info?.dtttrr,
        customerType: this.info?.customerType?.toLocaleLowerCase() == 'khách hàng cũ' ? 'OLD' : this.info?.customerType?.toLocaleLowerCase() == 'khách hàng mới' ? 'NEW' : '',
        customerSegment: this.info?.segmentCode || this.form.controls.segment.value || '',
        businessRegistrationNumber: this.info?.businessRegistrationNumber,
        classification: this.info?.classification,
        creditRanking: this.info?.scoreValue,
        isRmManager: this.info.isManage,
        interestType: this.form.controls.typeInterest.value,
        productCode: this.AllTypeFee.find(item => item.id === this.form.controls.typeInterest.value).code,
        approvedDateMO: this.form.controls.dateApprovalMO.value,
        codeHTCNTT: this.form.controls.codeCntt.value?.trim(),
        moCode: this.form.controls.moCode.value?.trim(),
        userApproved: this.form.controls.authApproval.value?.trim(),
        roleApproved: this.form.controls.titleAuthApproval.value?.trim(),
        reason: this.form.controls.codeReason.value,
        fullNameApproved: this.form.controls.fullName.value?.trim(),
        commitment: this.form.controls.commitment.value
      };

      this.isLoading = true;
      this.reductionProposalApi.saveException(payload).subscribe(
        (result) => {
          this.infoSave = result;
          this.isLoading = false;
          this.messageService.success('Thêm mới thành công. Mã tờ trình: ' + result.code);
          if (backToList) {
            setTimeout(() => {
              this.router.navigateByUrl(functionUri.reduction_proposal, { state: this.prop ? this.prop : this.state });
            }, 2000);
          }
          setTimeout(() => {
            resolve(true);
          }, 100);
        },
        (e) => {
          if (e?.error) {
            this.messageService.error(e?.error?.description);
          } else {
            this.messageService.error(_.get(this.notificationMessage, 'error'));
          }
          this.isLoading = false;
          reject(false);
        });
    });
  }

  async confirmReductionException(){
    await this.save(false);
    const body = {
      code: this.infoSave.code,
      type: 1,
      classification: this.info?.classification == '--' ? '' : this.info?.classification,
      creditRanking: this.info?.scoreValue == '--' ? '' : this.info?.scoreValue,
      customerType: this.info?.customerType?.toLocaleLowerCase() === 'khách hàng cũ' ? 'OLD' : this.info?.customerType?.toLocaleLowerCase() === 'khách hàng mới' ? 'NEW' : '',
      dtttrrExisting: this.info?.dtttrr,
      customerName: this.info?.customerName,
    }
    return new Promise<any>(async (resolve, reject) => {
      this.reductionProposalApi.confirmReductionException(body).subscribe(
        (result) => {
          this.infoSave = result;
          this.isLoading = false;
          this.messageService.success('Trình cán bộ quản lý thành công. Mã tờ trình: ' + result);
          setTimeout(() => {
            this.router.navigateByUrl(functionUri.reduction_proposal, { state: this.prop ? this.prop : this.state });
          }, 2000);
          setTimeout(() => {
            resolve(true);
          }, 100);
        },
        (e) => {
          if (e?.error) {
            this.messageService.error(e?.error?.description);
          } else {
            this.messageService.error(_.get(this.notificationMessage, 'error'));
          }
          this.isLoading = false;
          reject(false);
        })
    });
  }



  mapSegment(segment: any): string {
    if(segment == 'Không xác định'){
      return '';
    }
    if(segment){
      return this.dataCommonSegment?.filter(item => {return item.description == segment}).map(value => value?.name);
    }
    else {
      return '';
    }
  }

  // Function main
  /**
   * Lấy thông tin biểu lãi
   */
  openInterestModal(type: string, item) {

    let segmentCode = '';
    if(this.info?.blockCode !== Division.CIB){
      segmentCode = this.info?.segmentCode || this.form.controls.segment.value;
      if (!segmentCode && !this.existed) {
        this.messageService.warn('Vui lòng chọn phân khúc khách hàng');
        return;
      }
    }

    const planType = this.form?.controls?.scopeApply?.value;
    const config = {
      selectionType: type === 'interest' && planType !== 'UNKNOWN' ? SelectionType.multi : SelectionType.single
    };
    const { toi } = this.form.getRawValue();
    let toiCommit;
    if (this.info.isNew) {
      toiCommit = toi;
    } else {
      toiCommit = this.convertToiCurrent(this.info.toiExisting);
    }
    const modal = this.modal.open(InterestModalComponent, { windowClass: 'list__interest-modal' });
    modal.componentInstance.config = config;
    modal.componentInstance.customerObject = this.info.customerCode ? this.info?.classification : 'NORMAL'; // Loại khách hàng
    modal.componentInstance.customerSegment = this.info?.blockCode === Division.CIB ? Division.CIB : this.info?.blockCode + segmentCode; // Phân khúc
    modal.componentInstance.toi = toiCommit; // thông tin toi
    modal.componentInstance.scoreValue = this.info?.scoreValue || 'BB'; // Xếp hạng tín dụng
    modal.componentInstance.productCode = this.AllTypeFee.find(item => item.id === this.form.controls.typeInterest.value)?.code ; // product code sang MS
    modal.componentInstance.productType = this.AllTypeFee.find(item => item.id === this.form.controls.typeInterest.value)?.productType; // product type sang MSmodal.componentInstance.blockCode = this.form.controls.typeInterest.value === 6 ? 'CIB' : division; // Thông tin khối
    modal.componentInstance.reload();
    modal.componentInstance.listCode = type === 'interest' ? item.map(item => {
      return item.data.productCode;
    }) : [item?.productCode];
    modal.result
      .then((res) => {
        if (res) {
          if (type === 'option') {
            if (res.listSelected.length > 0) {
              const selected = res.listSelected;
              item.productCode = selected[0]?.productCode;
              item.name = selected[0]?.name;
              item.currency = selected[0]?.currency;
              item.adjustmentPeriod = selected[0]?.adjustmentPeriod;
              item.period = selected[0]?.period;
              item.referenceInterestRate = Number(selected[0]?.referenceInterestRate).toFixed(2);
              item.baseMargin = Number(selected[0]?.baseMargin).toFixed(2);
              item.maximumMarginReduction = selected[0]?.maximumMarginReduction
                ? Number(selected[0]?.maximumMarginReduction).toFixed(2) : null;
              item.loanRate = (Number(selected[0]?.baseMargin) + Number(selected[0]?.referenceInterestRate)).toFixed(2);
              item.marginSuggest = null;
              item.loanSuggest = null;
              item.rateMinimum = null;
              item.rateInterestReduction = null;
              item.minInterestRate = selected[0]?.minInterestRate;
              this.mapTable('option-interest', item);
            }
          } else {
            if (planType === 'UNKNOWN') {
              this.interestData = _.uniqBy([...res.listSelected], (i) => i.productCode);
              this.listData = [];
              this.mapTable('interest', null);
            } else {
              this.interestData = _.uniqBy([...this.interestData, ...res.listSelected], (i) => i.productCode);
              this.interestData = _.remove(this.interestData, (i) => _.indexOf(res.listRemove, i.productCode) === -1);
              this.listData = _.remove(this.listData, (i) => _.indexOf(res.listRemove, i.data?.productCode) === -1);
              this.mapTable('interest', null);
            }
          }
        }
      })
      .catch(() => {
      });
  }

  openOptionCodeModal() {
    const config = {
      selectionType: SelectionType.multi
    };
    
    const modal = this.modalService.open(OptionCodeModalComponent, { windowClass: 'list__optioncode-modal' });
    modal.componentInstance.config = config;
    modal.componentInstance.customerCode = this.info?.customerCode; // "";
    modal.componentInstance.businessRegistrationNumber = this.info?.businessRegistrationNumber || this.info?.taxCode;
    modal.componentInstance.idCard = this.info?.idCard;
    modal.componentInstance.type = 1;
    modal.componentInstance.blockCode = this.info?.blockCode;
    modal.componentInstance.listCode = this.optionCodeData?.map((item) => item.optionCode);
    modal.result
      .then((res) => {
        if (res) {
          this.optionCodeData = _.uniqBy([...this.optionCodeData, ...res.listSelected], (i) => i.optionCode);
          this.optionCodeData = _.remove(this.optionCodeData, (i) => _.indexOf(res.listRemove, i.optionCode) === -1);
          // Lãi
          this.listData = _.remove(this.listData, (i) => _.indexOf(res.listRemove, i.data?.code) === -1);
          this.mapTable('option', null);
        }
      })
      .catch(() => {
      });
  }

  openLdCodeModal() {
    const config = {
      selectionType: SelectionType.multi
    };
    const modal = this.modalService.open(LdCodeModalComponent, { windowClass: 'list__ldcode-modal' });
    modal.componentInstance.config = config;
    modal.componentInstance.listLD = this.listLD;
    modal.componentInstance.listCode = this.ldCodeData?.map((item) => item.ldCode);
    modal.componentInstance.customerCode = this.info?.customerCode;
    modal.componentInstance.reload();
    modal.result
      .then((res) => {
        if (res) {
          this.ldCodeData = _.uniqBy([...this.ldCodeData, ...res.listSelected], (i) => i.ldCode);
          this.ldCodeData = _.remove(this.ldCodeData, (i) => _.indexOf(res.listRemove, i.ldCode) === -1);
          if (this.info.type === 3) {
            // Lãi
            this.listData = _.remove(this.listData, (i) => _.indexOf(res.listRemove, i.data?.code) === -1);
            this.mapTable('ld', null);
          }
        }
      })
      .catch(() => {
      });
  }



  mapTable(type: string, interestItem) {
    const division = this.info.blockCode;
    const segmentCode = this.info.segmentCode;
    const scoreValue = this.info.scoreValue;
    const rowData = [];
    const currentSize = this.listData.length;

    let existed;
    switch (type) {
      case 'interest':
        existed = this.listData.map(item => item?.data?.productCode);
        this.interestData?.filter((item) => !existed.includes(item?.productCode)).map((item) => {
          rowData.push(
            {
              data: {
                code: '',
                productCode: item?.productCode,
                name: item.name,
                currency: item.currency,
                adjustmentPeriod: item.adjustmentPeriod,
                period: item.period,
                referenceInterestRate: Number(item.referenceInterestRate).toFixed(2),
                baseMargin: Number(item.baseMargin).toFixed(2),
                maximumMarginReduction: item.maximumMarginReduction ? Number(item.maximumMarginReduction || 0).toFixed(2) : null,
                loanRate: (Number(item.baseMargin) + Number(item.referenceInterestRate)).toFixed(2),
                marginSuggest: Number(item.baseMargin).toFixed(2),
                loanSuggest: 0,
                reductPercent: 0,
                rateMinimum: item?.minInterestRate,
                rateInterestReduction: 0,
                minInterestRate: item?.minInterestRate
              }
            }
          );
        });
        this.listData = [...this.listData, ...rowData];
        console.log(this.listData);
        break;
      case 'option':
        existed = this.listData.map(item => item?.data?.code);
        this.optionCodeData?.filter((item) => !existed.includes(item?.optionCode)).map((item) => {
          rowData.push(
            {
              data: {
                code: item.optionCode,
                productCode: null,
                name: null,
                currency: null,
                adjustmentPeriod: null,
                period: null,
                referenceInterestRate: null,
                baseMargin: null,
                maximumMarginReduction: null,
                loanRate: null,
                marginSuggest: Number(item.baseMargin).toFixed(2),
                loanSuggest: null,
                reductPercent: null,
                rateMinimum: null,
                rateInterestReduction: null,
                minInterestRate: item?.minInterestRate
              }
            }
          );
        });
        this.listData = [...this.listData, ...rowData];
        break;
      case 'ld':
        existed = this.listData.map(item => item?.data?.code);
        this.ldCodeData?.filter((item) => !existed.includes(item?.ldCode)).map((item) => {
          // Lấy thông tin biểu lãi dựa vào các chỉ số của LD
          this.reductionProposalApi.getInterestRate({
            adjustmentPeriod: item?.Y_TS_TDLS,
            currency: item?.NTE,
            customerSegment: division === 'CIB' ? division : division + segmentCode,
            period: item?.KYHANTHANG ? item?.KYHANTHANG?.toString().substr(0, item?.KYHANTHANG?.toString().length - 1) : '',
            rankingCredit: scoreValue ? scoreValue : 'BB',
            customerType: this.info.customer.classification
          }).subscribe((res) => {
            // API bị lỗi sẽ trả về định dạng object
            if (Object.prototype?.toString.call(res) === '[object Object]' || (res as []).length === 0) {
              this.messageService.error('Không tìm thấy dữ liệu biểu lãi');
            } else {
              const obj: any = res[0];
              this.listData = [...this.listData, ...[
                {
                  data: {
                    code: item.ldCode,
                    productCode: obj?.marginId,
                    name: 'LSTC cho vay',
                    currency: item?.NTE,
                    adjustmentPeriod: item?.Y_TS_TDLS,
                    period: item?.KYHANTHANG,
                    referenceInterestRate: item?.INTEREST_FIX,
                    baseMargin: item?.BIENDOLS,
                    maximumMarginReduction: obj?.maximumMarginReduction ? obj?.maximumMarginReduction : null,
                    loanRate: item?.LS,
                    marginSuggest: null,
                    loanSuggest: null,
                    reductPercent: null,
                    codeInterest: obj?.productCode
                  }
                }
              ]];
            }
          });
        });
        break;
      case 'option-interest':
        for (let index = 0; index < this.listData.length; index++) {
          const element = this.listData[index];
          if (interestItem?.code === element?.data?.code) {
            console.log('option-interest: ', interestItem);
            this.listData[index] = {
              data: {
                code: interestItem?.code,
                productCode: interestItem?.productCode,
                name: interestItem.name,
                currency: interestItem.currency,
                adjustmentPeriod: interestItem.adjustmentPeriod,
                period: interestItem.period,
                referenceInterestRate: Number(interestItem.referenceInterestRate).toFixed(2),
                baseMargin: Number(interestItem.baseMargin).toFixed(2),
                maximumMarginReduction: interestItem?.maximumMarginReduction
                  ? Number(interestItem.maximumMarginReduction).toFixed(2) : null,
                loanRate: (Number(interestItem.baseMargin) + Number(interestItem.referenceInterestRate)).toFixed(2),
                marginSuggest: Number(interestItem.baseMargin).toFixed(2),
                loanSuggest: 0,
                reductPercent: 0,
                rateMinimum: interestItem?.minInterestRate,
                rateInterestReduction: 0,
                minInterestRate: interestItem?.minInterestRate
              }
            };
          }

        }
        break;
    }
    this.form.controls.listData.setValue(this.listData);
    this.treeTableInterest.updateSerializedValue();
  }

  onDragOver(event): void {
    event.preventDefault();
  }

  onDropSuccess(event): void {
    const lengthFile = 1;
    const allows = ['.pdf'];
    event.preventDefault();
    if (this.listFile?.length + event.dataTransfer.files?.length > lengthFile) {
      this.messageService.error(`Chỉ được chọn tối đa ${lengthFile} file đính kèm}`);
      return;
    }
    let countMaxData = 0;
    let countNotAllow = 0;
    let errMessage = '';
    const maxSizeByte = this.maxSize * 1024 * 1024;
    for (const file of event.dataTransfer.files) {
      const name = file.name;
      if (file?.size <= maxSizeByte && allows.includes(name.substr(name.lastIndexOf('.')).toLowerCase())) {
        this.upload(file);
      } else if (file?.size > maxSizeByte) {
        countMaxData++;
      } else if (!allows.includes(name.substr(name.lastIndexOf('.')))) {
        countNotAllow++;
      }
    }
    if (countMaxData > 0) {
      errMessage = 'Dung lượng file không được vượt quá 20MB';
    }
    if (countNotAllow > 0) {
      errMessage = 'File không đúng định dạng cho phép (.pdf)';
    }
    if (errMessage?.length > 0) {
      this.messageService.warn(errMessage);
    }
  }

  changeFile(event) {
    const lengthFile = 1;
    const allows = ['.pdf'];
    if (this.listFile?.length + event.target.files?.length > lengthFile) {
      this.messageService.error(`Chỉ được chọn tối đa ${lengthFile} file đính kèm`);
      return;
    }
    let countMaxData = 0;
    let countNotAllow = 0;
    let errMessage = '';
    const maxSizeByte = this.maxSize * 1024 * 1024;
    for (const file of event.target.files) {
      const name = file.name;
      if (file?.size <= maxSizeByte && allows.includes(name.substr(name.lastIndexOf('.')).toLowerCase())) {
        this.upload(file);
      } else if (file?.size > maxSizeByte) {
        countMaxData++;
      } else if (!allows.includes(name.substr(name.lastIndexOf('.')))) {
        countNotAllow++;
      }
    }
    if (countMaxData > 0) {
      errMessage = 'Dung lượng file không được vượt quá 20MB';
    }
    if (countNotAllow > 0) {
      errMessage = 'File không đúng định dạng cho phép (.pdf)';
    }
    if (errMessage.length > 0) {
      this.messageService.warn(errMessage);
    }
  }

  openDownloadFilePopup(inputFile): void {
    inputFile.value = '';
    inputFile.click();
  }

  download(file: any) {
    const title = file?.fileName;
    this.isLoading = true;
    this.fileService.downloadFile(file?.fileId, title).subscribe((res) => {
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
      this.isLoading = false;
    }, err => {
      this.isLoading = false;
    });
  }

  upload(file: any) {
    this.isLoading = true;
    const formData = new FormData();
    formData.append('file', file);
    this.fileService.uploadFile(formData).subscribe(res => {
      const mapFile = {
        fileId: res,
        fileName: file.name,
        type: '',
        filePath: ''
      };
      this.listFile.push(mapFile);
      this.isLoading = false;
    }, err => {
      this.isLoading = false;
    });
  }

  removeFile(id) {
    this.listFile = this.listFile.filter(list => list.fileId !== id);
  }

  getInfoAuthorization(event) {
    const val = event.target.value.trim();
    if(!val){
      return;
    }
    this.isLoading = true;
    if (val) {
      this.reductionProposalApi.getInfoAuthorization(val).subscribe((item) => {
        if (item) {
          this.form.controls.fullName.setValue(item?.fullName);
          this.form.controls.titleAuthApproval.setValue(item?.positionName);
          this.fullNameError = '';
          this.titleAuthApprovalError = '';
          this.authApprovalError = '';
        }
        else {
          this.fullNameError = 'Trường này bắt buộc';
          this.titleAuthApprovalError = 'Trường này bắt buộc';
          this.form.controls.fullName.setValue('');
          this.form.controls.titleAuthApproval.setValue('');
        }
        this.isLoading = false;
      }, err => {
        this.isLoading = false;
      });
    }
  }

  openPopupCalculateTOI(): void {
    const dialogRef = this.dialog.open(ToiConfirmModalComponent);
    dialogRef.componentInstance.type = this.info?.type;
    dialogRef.componentInstance.blockCode = this.info?.blockCode;
    dialogRef.afterClosed().subscribe(() => {
      const infoToi : any = this.reductionProposalApi?.info?.value;
      const toi: any = infoToi.toi;
      this.info.obj = {toi};
      if (toi) {
        this.form.controls.toi.setValue(toi?.toi);
        this.form.controls.dtttrr.setValue(toi?.dtttrr);
        this.form.controls.dtttrrDisplay.setValue(this.convertDtttrr(toi?.dtttrr));
      }
    });
  }

  dateEffect() {
    if (!this.form.controls?.dateApprovalMO?.value || !this.form.controls?.effect?.value) {
      return '';
    }
    const date = moment(this.form.controls?.dateApprovalMO?.value, 'DD/MM/YYYY');
    const ennDateMonth = moment(this.form.controls?.dateApprovalMO?.value, 'DD/MM/YYYY').endOf('month');
    if(date.format('DD/MM/YYYY') == ennDateMonth.format('DD/MM/YYYY')) {
      return moment(date.add(this.form.controls?.effect?.value, 'month').calendar('DD/MM/YYYY'), 'MM/DD/YYYY').endOf('month').format('DD/MM/YYYY');
    }
    return moment(date.add(this.form.controls?.effect?.value, 'month').calendar('DD/MM/YYYY'), 'MM/DD/YYYY').format('DD/MM/YYYY')
  }

  // Common function
  backStep(): void {
    this.router.navigateByUrl(functionUri.reduction_proposal, { state: this.prop ? this.prop : this.state });
  }

  handlerEffect(event, type: number) {
    if (event.value) {
      if(type == 1){
        this.monthConfirmError = '';
      } else {
        this.effectError = '';
      }
    }
  }

  handlerDate(event) {
    if (event) {
      this.dateApprovalMOError = '';
    }
    if (isNaN(Date.parse(event))) {
      this.form.controls.dateApprovalMO.setValue(new Date());
    }
  }

  handlerError(event, type: number) {
    const val = event.target.value;
    if (val) {
      switch (type) {
        case 1:
          this.fullNameError = '';
          break;
        case 2:
          this.dateApprovalMOError = '';
          break;
        case 3:
          this.dateEndApproveError = '';
          break;
        case 4:
          this.authApprovalError = '';
          break;
        case 5:
          this.titleAuthApprovalError = '';
          break;
        case 6:
          this.codeCnttError = '';
          break;
        case 7:
          this.moCodeError = '';
          break;
      }
    }
  }

  checkValidParam() {
    let check = false;

    if((!this.form.controls.toi.value || Number(this.form.controls.toi.value) === 0) && this.info.isNew){
      this.messageService.warn('Vui lòng tính TOI')
      check = true;
    }

    if(!this.form.controls.monthConfirm.value) {
      this.monthConfirmError = 'Lựa chọn thời gian đánh giá';
      check = true;
    }

    if (!this.listData.length) {
      this.messageService.warn('Vui lòng chọn biểu lãi');
      return true;
    }

    if (!this.form.controls.effect.value) {
      this.effectError = 'Lựa chọn hiệu lức của tờ trình';
      check = true;
    }
    if (!this.form.controls.dateApprovalMO.value) {
      this.dateApprovalMOError = 'Nhập thông tin ngày duyệt tờ trình trên MO';
      check = true;
    }
    if (!this.form.controls.authApproval.value?.trim?.()) {
      this.authApprovalError = 'Nhập thẩm quyền phê duyệt';
      check = true;
    }
    if(!this.form.controls.fullName.value?.trim?.()){
      this.fullNameError = 'Nhập tên thẩm quyền phê duyệt';
      check = true;
    }
    if (!this.form.controls.titleAuthApproval.value?.trim?.()) {
      this.titleAuthApprovalError = 'Nhập chức danh thẩm quyền';
      check = true;
    }
    if (!this.form.controls.codeReason.value) {
      this.codeReasonError = 'Chọn lý do trình ngoại lệ';
      check = true;
    }
    if (!this.form.controls.codeCntt.value?.trim?.() && this.form.controls.codeReason.value == 1) {
      this.codeCnttError = 'Nhập mã hotrocntt';
      this.messageService.warn('Bắt buộc nhập mã hotrocntt trong trường hợp chọn lý do là \"Hệ thống lỗi\"');
      check = true;
    }
    if (!this.form.controls.moCode.value?.trim?.()) {
      this.moCodeError = 'Nhập mã tờ trình trên MO';
      check = true;
    }
    if(this.form.controls.moCode?.errors?.pattern){
      check = true;
    }

    return check;
  }

  isValid() {
    let errCount = 0;
    let errMessage = '';
    if (this.listData?.length === 0) {
      errMessage += 'Chưa nhập thông tin giảm lãi suất. ';
      errCount++;
    } else {
      const fieldErr = [];
      this.listData?.map(item => {
        if (isNaN(item.data?.marginSuggest) && !fieldErr.includes('marginSuggest')) {
          errMessage += 'Vui lòng chọn biểu lãi';
          fieldErr.push('marginSuggest');
          errCount++;
        }
      });
    }
    if (Utils.isStringNotEmpty(errMessage)) {
      this.messageService.warn(errMessage);
    }
    return errCount === 0;
  }


  checkSegment(): boolean {
    if(this.existed){
      return true;
    }
    else {
      if (this.info?.segment){
        return true
      }
    }
  }

  formatCurrency(element) {
    if (element == 0) {
      return '0';
    }
    if (!element || element === 'NaN' || element === '--') {
      return '--';
    }
    let formated: string = element?.toString();
    let first = '';
    let last = '';
    if (formated.includes('.')) {
      first = formated.substr(0, formated.indexOf('.'));
      last = formated.substr(formated.indexOf('.'));
    } else {
      first = formated;
    }
    if (Number(first)) {
      first = Number(first).toLocaleString('en-US', {
        style: 'currency',
        currency: 'VND'
      });
      // first = first.trim().substr(1, first.length);
      first = first.trim().replace('₫', '');
      formated = first + last;
    }
    return formated;
  }
  convertToiCurrent(toi) {
    if (toi) {
      toi = Number(Number(toi) * 100).toFixed(2);
    }
    return toi;
  }

  convertDtttrr(dtttrr) {
    if (dtttrr) {
      dtttrr = Number(Number(dtttrr) / 1000000).toFixed(2);
    }
    return dtttrr;
  }

  getValueMax(row) {
    const maximumMarginReduction = row?.maximumMarginReduction || 0;
    // let baseMargin = row?.baseMargin || 0;
    if (maximumMarginReduction && maximumMarginReduction > 0)
      // return (baseMargin - maximumMarginReduction).toFixed(2)
      return maximumMarginReduction;
    else
      return '--';
  }

  calculateMarginSuggest(event, row) {
    const val = event.target.value;
    if (Number(val) <= 0) {
      row.marginSuggest = row?.baseMargin;
      row.loanSuggest = 0;
      row.reductPercent = 0;
    }
    this.treeTableInterest.updateSerializedValue();
  }

  changeMarginSuggest(event, data): void {
    const val = event.target.value;
    if (val) {
      data.loanSuggest = (Number(data.referenceInterestRate) + Number(val)).toFixed(2);
      data.reductPercent = ((Number(data.baseMargin) - Number(val)) / Number(data.baseMargin) * 100).toFixed(2);
      if (Number(val) < 0) {
        if(!data.errorMessage){
          this.messageService.warn('Biên độ đề xuất > 0');
        }
        data.errorMessage = 'Biên độ đề xuất > 0';
      }
      else {
        delete data.errorMessage;
      }
    } else {
      data.loanSuggest = null;
      data.reductPercent = null;
      delete data.errorMessage;
    }
  }

  calculateInterest(event, row) {
    const val = event.target.value;
    if (Number(val) < 0) {
      row.rateMinimum = row?.minInterestRate;
      row.rateInterestReduction = 0;
    }
    this.treeTableInterest.updateSerializedValue();
  }

  getValueMinimumRate(event, row) {
    const val = event.target.value;
    row.rateInterestReduction = ((Number(row?.minInterestRate) - Number(val)) / Number(row?.minInterestRate) * 100).toFixed(2);
  }

  checkTableInterest() {
    return (this.form.controls.typeInterest.value !== 1 && this.form.controls.typeInterest.value !== 6);
  }

  removeItem(data): void {
    const scopeApply = this.form.controls.scopeApply.value;
    if (scopeApply === 'ALL') {
      this.listData = this.listData?.filter(item => {
        return item?.data?.productCode !== data?.productCode;
      });
      this.interestData = this.interestData?.filter(item => {
        return item?.productCode !== data?.productCode;
      });
    } else if (scopeApply === 'UNKNOWN') {
      this.listData = this.listData?.filter(item => {
        return item?.data?.productCode !== data?.productCode;
      });
      this.interestData = this.interestData?.filter(item => {
        return item?.productCode !== data?.productCode;
      });
    } else {
      this.listData = this.listData?.filter(item => {
        return item?.data?.code !== data?.code;
      });
      this.interestData = this.interestData?.filter(item => {
        return item?.productCode !== data?.productCode;
      });
      if (scopeApply === 'OPTION_CODE') {
        this.optionCodeData = this.optionCodeData?.filter(item => {
          return item?.optionCode !== data?.code;
        });
      } else if (scopeApply === 'LD_CODE') {
        this.ldCodeData = this.ldCodeData?.filter(item => {
          return item?.ldCode !== data?.code;
        });
      }
    }
  }

  checkFile() {
    if (!this.listFile.length) {
      this.messageService.warn('Vui lòng chọn file !');
      return false;
    }
    return true;
  }

  reset() {
    this.listData = [];
    this.interestData = [];
    this.optionCodeData = [];
    this.ldCodeData = [];
  }

}
