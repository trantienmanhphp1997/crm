import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailExceptionComponent } from './detail-exception.component';

describe('DetailExceptionComponent', () => {
  let component: DetailExceptionComponent;
  let fixture: ComponentFixture<DetailExceptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailExceptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailExceptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
