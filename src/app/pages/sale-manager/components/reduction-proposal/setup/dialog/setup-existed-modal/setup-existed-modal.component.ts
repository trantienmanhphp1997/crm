import { Component, HostBinding, ViewEncapsulation } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { functionUri } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'app-setup-existed-modal',
  templateUrl: './setup-existed-modal.component.html',
  styleUrls: ['./setup-existed-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SetupExistedModalComponent {
  @HostBinding('setup-existed-modal') classCreateCalendar = true;
  constructor(
    private router: Router,
    private dialogRef: MatDialogRef<SetupExistedModalComponent>
  ) { }

  state;

  choose(choose: boolean): void {
    let path = '';
    if (choose) {
      path = '/update-setup?customerCode=' + this.state?.customer?.customerCode;
    }
    this.router.navigateByUrl(functionUri.reduction_proposal + path, { state: this.state });
    this.dialogRef.close();
  }
}
