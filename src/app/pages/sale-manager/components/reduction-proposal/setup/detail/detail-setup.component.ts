import { DatePipe } from "@angular/common";
import { Component, Injectable, Injector, OnInit, ViewChild } from "@angular/core";
import { BaseComponent } from "src/app/core/components/base.component";
import { FunctionCode, functionUri } from "src/app/core/utils/common-constants";
import * as _ from 'lodash';
import { ReductionProposalApi } from "src/app/pages/sale-manager/api/reduction-proposal.api";
import { DeclareSetupProposalComponent } from "../declare/declare-setup.component";
import * as moment from "moment";
import { Utils } from "src/app/core/utils/utils";

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'app-detail-setup',
  templateUrl: './detail-setup.component.html',
  styleUrls: ['./detail-setup.component.scss'],
  providers: [DatePipe]
})
export class DetailSetupProposalComponent extends BaseComponent implements OnInit {

  @ViewChild('declare') declare: DeclareSetupProposalComponent;
  customer;
  isApprove;

  constructor(
    injector: Injector,
    private reductionProposalApi: ReductionProposalApi,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REDUCTION_PROPOSAL}`);
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.customer = this.prop ? this.prop?.customer : {};
    this.isApprove = this.prop ? this.prop?.isApprove : false;
  }

  changeLoading(isLoading = false): void {
    this.isLoading = isLoading;
  }

  censorship(isApproved) {
    let endDateError = this.declare.listData.find((item) => {
      let endDate = item.data.toDate;
      return moment(new Date()).isAfter(moment(endDate));
    });

    if (isApproved && Utils.isNotNull(endDateError)) {
      this.messageService.warn('Ngày kết thúc phải lớn hơn hoặc bằng ngày hiện tại. Vui lòng sửa lại cài đặt');
      return;
    }
    this.isLoading = true;
    this.reductionProposalApi.censorshipSetup({
      customerCode: this.declare.customerCode,
      status: isApproved ? 0 : 1
    }).subscribe((res) => {
      this.messageService.success(this.notificationMessage.success);
      this.isLoading = false;
      setTimeout(() => {
        this.backStep();
      }, 2000);
    }, (err) => {
      this.isLoading = false;
      this.messageService.error(this.notificationMessage.error);
    });
  }

  backStep(): void {
    this.router.navigateByUrl(functionUri.reduction_proposal, { state: this.prop ? this.prop : this.state });
  }
}
