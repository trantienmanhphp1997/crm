import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { forkJoin, of } from 'rxjs';
import { ReductionProposalApi } from 'src/app/pages/sale-manager/api/reduction-proposal.api';
import { CommonCategory, FunctionCode } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { CustomerApi } from 'src/app/pages/customer-360/apis';
import { TreeTable } from 'primeng/treetable';
import * as moment from 'moment';
import { Utils } from 'src/app/core/utils/utils';
import { SetupExistedModalComponent } from '../dialog/setup-existed-modal/setup-existed-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { CategoryService } from 'src/app/pages/system/services/category.service';

@Component({
  selector: 'app-declare-setup',
  templateUrl: './declare-setup.component.html',
  styleUrls: ['./declare-setup.component.scss'],
  providers: [NgbModal]
})
export class DeclareSetupProposalComponent extends BaseComponent implements OnInit {

  @Output() changeLoading = new EventEmitter();
  @Input() customer;
  @Input() type;
  @Input() stateObj;
  @ViewChild('treeTable') treeTable: TreeTable;


  form = this.fb.group({
    purpose: ''
  });
  commonData = {
    customerSegment: {},
    minDate: new Date(),
    listPurpose: [
      { code: '1', name: 'Giải ngân thanh toán trong nước' },
      { code: '2', name: 'Giải ngân thanh toán quốc tế' }
    ],
    listPeriod: [
      { code: '0-6', name: '0-6' },
      { code: '7-12', name: '7-12' }
    ],
    interestTypes: [
      { code: '1', name: 'Lãi suất thông thường' },
      { code: '2', name: 'Lãi suất miễn giảm' }
    ]
  }
  customer360Fn: any;
  listData: any = [];
  customerCode;
  item;
  params;
  tabIndex = 0;
  reductionProposalTranslate;

  constructor(
    injector: Injector,
    private reductionProposalApi: ReductionProposalApi,
    private customerApi: CustomerApi,
    private categoryService: CategoryService,
    public dialog: MatDialog
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REDUCTION_PROPOSAL}`);
    this.customer360Fn = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.params = this.route.snapshot.queryParams;
  }

  ngOnInit(): void {
    this.translate.get('reductionProposal').subscribe((reductionProposal) => {
      this.reductionProposalTranslate = reductionProposal;
    });

    this.customerCode = this.params.customerCode;
    if (!this.customerCode) {
      this.customerCode = this.customer?.customerCode;
    }
    this.getSetupDetail(this.customerCode).then((data) => {
      this.commonData.customerSegment = data?.customerSegment;
      this.item = data?.item;

      if (this.type === 'create' && this.item) {
        // tạo mới khách hàng đã có thông tin cài đặt
        const dialogRef = this.dialog.open(SetupExistedModalComponent, { closeOnNavigation: false, disableClose: true });
        dialogRef.componentInstance.state = this.stateObj;
        this.changeLoading.emit(false);
        return;
      }
      if (['update', 'detail'].includes(this.type)) {
        this.form.controls.purpose.setValue(this.item?.purpose);
        this.initTableData(this.item.listSetupBizDetails);
      }

      if (this.type === 'detail') {
        let customerTypeDisplay;
        if (Utils.isNotNull(this.item?.customerType) && this.item?.customerType.toLowerCase() === 'khach hang cu') {
          customerTypeDisplay = 'Khách hàng cũ';
        } else {
          customerTypeDisplay = 'Khách hàng mới';
        }
        this.customer = {
          ...this.item,
          customerTypeMerge: this.item?.blockCode,
          segment: this.commonData.customerSegment[this.item?.customerSegment],
          customerTypeDisplay,
        }
        this.changeLoading.emit(false);
        return;
      }
      this.loadCustomerData();
    });
  }

  getSetupDetail(customerCode) {
    return new Promise<any>((resolve, reject) => {
      forkJoin([
        this.reductionProposalApi.setupDetail(customerCode).pipe(catchError(() => of(undefined))),
        this.categoryService.getCommonCategory(CommonCategory.SEGMENT_CUSTOMER_CONFIG).pipe(catchError(() => of(undefined))),
      ]).subscribe(([item, categorySegment]) => {
        let customerSegment = {};
        if (categorySegment.content) {
          const content = categorySegment.content.filter(obj => obj.value === 'SME');
          content.forEach((item) => {
            customerSegment[item.code] = item.name;
          });
        }
        resolve({
          item: item,
          customerSegment
        });
      });
    });
  }

  loadCustomerData() {
    console.log('DeclareSetupProposalComponent.ngOnInit: ', this.customer);
    const searchSmeBody = {
      pageNumber: 0,
      pageSize: 1,
      rsId: this.customer360Fn?.rsId,
      scope: 'VIEW',
      customerType: '',
      search: this.customerCode,
      searchFastType: 'CUSTOMER_CODE',
      manageType: 1,
      customerTypeSale: 1
    };
    forkJoin([
      this.customerApi.searchSme(searchSmeBody).pipe(catchError(() => of(undefined))),
      this.reductionProposalApi.getScopeReduction({
        customerCode: this.customerCode || '',
        businessRegistrationNumber: '',
        loanId: ''
      }).pipe(catchError(() => of(undefined))),
      this.reductionProposalApi.getToiExisting(this.customerCode).pipe(catchError(() => of(undefined))),
    ]).subscribe(([customerInfo, scopeReduction, toi]) => {
      this.changeLoading.emit(false);
      this.customer.customerCode = this.customerCode;
      if (!_.isEmpty(customerInfo)) {
        const cInfo = customerInfo.length ? customerInfo[0] : {};
        this.customer.customerName = cInfo?.customerName;
        this.customer.taxCode = cInfo?.taxCode;
        this.customer.businessRegistrationNumber = cInfo?.businessRegistrationNumber;
        this.customer.customerTypeMerge = cInfo?.customerTypeMerge;
        this.customer.blockCode = cInfo?.customerTypeMerge;
        this.customer.segment = cInfo?.segmentCode ? cInfo?.segment : '';
        this.customer.customerSegment = cInfo?.segmentCode;
        this.customer.classification = cInfo?.classification;
        this.customer.branchCode = cInfo?.branchCode;
      }
      this.customer.creditRanking = scopeReduction?.ketQuaXhtd;
      if (!toi?.LOAI_KH) {
        toi = { LOAI_KH: 'khach hang moi' };
      }
      this.customer.customerType = toi?.LOAI_KH;
      if (toi?.LOAI_KH && toi?.LOAI_KH.toLowerCase() === 'khach hang cu') {
        this.customer.customerTypeDisplay = 'Khách hàng cũ';
      } else {
        this.customer.customerTypeDisplay = 'Khách hàng mới';
      }
    });
  }

  getNomalInterest(row) {
    if (!row.period) {
      return;
    }
    this.changeLoading.emit(true);
    // Lãi suất thông thường
    this.reductionProposalApi.getInterestRate({
      adjustmentPeriod: '3M',
      currency: 'VND',
      customerSegment: this.customer.customerTypeMerge + this.customer.customerSegment, // Phân khúc KH
      period: row.period.split('-')[1],
      rankingCredit: this.customer.creditRanking || 'BB', // Xếp hạng tín dụng
      customerType: this.customer.classification, // Đối tượng KH
      productCode: "10",
    }).subscribe((res) => {
      this.changeLoading.emit(false);
      // API bị lỗi sẽ trả về định dạng object
      if (Object.prototype.toString.call(res) === '[object Object]' || (res as []).length === 0) {
        this.messageService.error('Không tìm thấy dữ liệu biểu lãi');
        this.resetItem(row, 'period');
      } else {
        let period = row.period.split('-');
        let rawData = res as any[];
        rawData = rawData.filter(info => {
          let validPeriod = info?.periodFrom == period[0] && info?.periodTo == period[1];
          return validPeriod && row.currency === info.currency
            && row.adjustmentPeriod === info.adjustmentPeriod
            ;
        });
        if (rawData.length === 0) {
          this.messageService.error('Không tìm thấy dữ liệu biểu lãi');
          this.resetItem(row, 'period');
          return;
        }
        let marginInfo = _.head(rawData);
        console.log('marginInfo: ', marginInfo);
        row.marginId = marginInfo?.marginId;
        row.margin = marginInfo?.baseMargin;
        row.referenceInterestRate = marginInfo?.referenceInterestRate;
        row.interestRate = !isNaN(marginInfo?.baseMargin) && !isNaN(marginInfo?.referenceInterestRate) ? (Number(marginInfo?.baseMargin) + Number(marginInfo?.referenceInterestRate)).toFixed(2) : null;
        // if (!isNaN(row?.marginSetup)) {
        //   row.interestRateSetup = Number(Number(row.marginSetup) + Number(row.referenceInterestRate)).toFixed(2);
        row.fromDate = null;
        row.toDate = null;
        row.marginSetup = null;
        row.interestRateSetup = null;
        // }
        this.treeTable.updateSerializedValue();
      }
    }, (err) => {
      this.messageService.error('Lấy dữ liệu biểu lãi thất bại');
      this.resetItem(row, 'period');
      this.changeLoading.emit(false);
    });
  }

  getDiscountInterest(row) {
    if (!row.period) {
      return;
    }
    console.log('getDiscountInterest');
    this.changeLoading.emit(true);
    // Lãi suất miễn giảm
    this.reductionProposalApi.getInterestRateDiscounted({
      customerCode: this.customer.customerCode,
      businessRegistrationNumber: this.customer.businessRegistrationNumber,
      proposalReductionCode: ''
    }).subscribe((res: any) => {
      this.changeLoading.emit(false);
      if (!res || (res as []).length === 0) {
        this.messageService.error('Không tìm thấy dữ liệu biểu lãi');
        this.resetItem(row, 'period');
      } else {
        let period = row.period.split('-');
        let marginInfo, maxToDate, proposalReductionCode, compareDate;
        res = res.forEach(itm => {
          let infos = itm?.interestReductionMarginInfos;
          if (infos) {
            infos = infos.filter(info => {
              let validPeriod = info?.periodFrom == period[0] && info?.periodTo == period[1];
              return validPeriod && row.currency === info.currency
                && row.adjustmentPeriod === info.adjustmentPeriod;
            });
            if (infos.length > 0
              && (moment(itm?.approvalDate).isAfter(moment(compareDate)) || !compareDate)
              && (itm?.scopeType === 'ALL')
            ) {
              compareDate = itm?.approvalDate;
              maxToDate = new Date(itm?.proposalEndDate);
              marginInfo = _.head(infos);
              proposalReductionCode = itm?.proposalReductionCode;
            }
            console.log('interestReductionMarginInfos filtered:', marginInfo);

          }
        });
        console.log('marginInfo:', marginInfo);
        if (!marginInfo) {
          this.messageService.error('Không tìm thấy dữ liệu biểu lãi');
          this.resetItem(row, 'period');
          return;
        }
        row.marginId = marginInfo?.marginId;
        row.margin = marginInfo?.marginPropose;
        row.proposalReductionCode = proposalReductionCode;
        row.referenceInterestRate = marginInfo?.referenceInterestRate;
        row.interestRate = !isNaN(marginInfo?.marginPropose) && !isNaN(marginInfo?.referenceInterestRate) ? (Number(marginInfo?.marginPropose) + Number(marginInfo?.referenceInterestRate)).toFixed(2) : null;
        row.maxToDate = maxToDate;
        // if (!isNaN(row?.marginSetup)) {
        //   row.interestRateSetup = Number(Number(row.marginSetup) + Number(row.referenceInterestRate)).toFixed(2);
        row.fromDate = null;
        row.toDate = null;
        row.marginSetup = null;
        row.interestRateSetup = null;
        // }
        this.treeTable.updateSerializedValue();
      }
    }, (err) => {
      this.messageService.error('Lấy dữ liệu biểu lãi thất bại');
      this.resetItem(row, 'period');
      this.changeLoading.emit(false);
    });
  }

  initTableData(setupDetails) {
    if (!setupDetails) {
      return;
    }
    let datas = [];
    setupDetails?.map((itm => {
      let data: any = {
        rowNum: datas.length,
        interestType: itm?.interestType,
        period: itm?.loanPeriod,
        currency: itm?.unit,
        adjustmentPeriod: itm?.adjustPeriod,
        fromDate: new Date(itm?.startDate),
        toDate: new Date(itm?.endDate),
        marginSetup: itm?.amplitudeApply,
        margin: null,
        referenceInterestRate: null,
        interestRate: null,
        interestRateSetup: null
      };

      if (this.type === 'detail') {
        data = {
          ...data,
          margin: itm?.amplitude,
          referenceInterestRate: itm?.interestReference,
          interestRate: itm?.interestValue,
          interestRateSetup: itm?.interestValueApply,
          proposalReductionCode: itm?.proposalReductionCode,
          marginId: itm?.marginId
        }
      }

      datas.push({ data });
    }));
    this.listData = datas;
    this.treeTable.updateSerializedValue();
  }


  addItem() {
    let obj = {
      rowNum: this.listData.length,
      interestType: '1',
      currency: 'VND',
      adjustmentPeriod: '3M',
      period: null,
      margin: '',
      referenceInterestRate: '',
      interestRate: '',
      interestRateSetup: '',
      proposalReductionCode: '',
      marginId: '',
      maxToDate: moment(new Date()).add(2, 'years').toDate()
    };
    this.listData.push({ data: obj });
    this.treeTable.updateSerializedValue();
  }

  removeItem(row) {
    let data = [];
    this.listData = this.listData.filter(item => {
      return item.data.rowNum != row.rowNum;
    }).forEach(item => {
      item.data.rowNum = data.length;
      data.push(item);
    });
    this.listData = data;
    this.treeTable.updateSerializedValue();
  }

  resetItem(row, type) {
    let obj = {
      ...row,
      margin: '',
      referenceInterestRate: '',
      interestRate: '',
      interestRateSetup: '',
      fromDate: '',
      toDate: '',
      marginSetup: '',
      proposalReductionCode: '',
      marginId: '',
      maxToDate: moment(new Date()).add(2, 'years').toDate()
    };
    if (type !== 'period') {
      obj.period = null;
    }
    this.listData = this.listData.map(item => {
      if (item.data.rowNum === row.rowNum) {
        item.data = obj;
      }
      return item;
    });
    if (type !== 'period') {
      this.treeTable.updateSerializedValue();
    }
  }

  changePeriod(row) {
    let duplicated;
    let listPeriod = [];
    this.listData.forEach(item => {
      if (listPeriod.includes(item.data?.period)) {
        duplicated = true;
      } else {
        listPeriod.push(item.data?.period);
      }
    });
    if (duplicated) {
      this.messageService.warn('Không được cài đặt trùng kỳ hạn');
      row.period = null;
      this.updateTable();
      return;
    }
    if (row.interestType === '1') {
      this.getNomalInterest(row);
    } else {
      this.getDiscountInterest(row);
    }
  }

  changeInterestType(row) {
    this.resetItem(row, 'interest');
  }

  validateMarginSetup(val, row) {
    if (!val) {
      row.marginSetupError = 'Không được phép bỏ trống';
      row.interestRateSetup = null
      return false;
    }
    if (val && isNaN(val)) {
      val = val.replaceAll(',', '');
    }

    row.marginSetup = val;
    val = Number(val)
    if (val > 100) {
      row.marginSetupError = 'Biên độ đề xuất ≤ 100';
      return false;
    }
    if (val < row.margin) {
      row.marginSetupError = 'Biên độ đề xuất ≥ biên độ';
      return false;
    }
    if (!isNaN(row.referenceInterestRate)) {
      row.interestRateSetup = Number(val + Number(row.referenceInterestRate)).toFixed(2);
    }
    row.marginSetupError = null;
    return true;
  }

  checkMarginSetup(val, row) {
    this.validateMarginSetup(val, row);
    this.updateTable();
  }

  validateDate(row, colName) {
    let message;
    if (!row?.fromDate && colName === 'fromDate') {
      message = 'Không được phép bỏ trống';
    }
    if (!row?.toDate && colName === 'toDate') {
      message = 'Không được phép bỏ trống';
    }
    if (message) {
      row[colName + 'Error'] = message;
      return;
    }
    if (moment(row.fromDate).isAfter(moment(row.toDate))) {
      if (colName === 'fromDate') {
        message = 'Ngày bắt đầu phải nhỏ hơn hoặc bằng ngày kết thúc';
      } else {
        message = 'Ngày kết thúc phải lớn hơn hoặc bằng ngày bắt đầu';
      }
      row[colName + 'Error'] = message;
      row[colName] = new Date();
      return false;
    }
    delete row['fromDateError'];
    delete row['toDateError'];
    return true;
  }

  validatePeriod(row) {
    if (!row.period) {
      row.periodError = 'Bắt buộc chọn kỳ hạn vay';
      return false;
    }
    delete row.periodError;
    return true;
  }

  updateTable() {
    this.treeTable.updateSerializedValue();
  }

  onTabChanged(event) {
    this.tabIndex = _.get(event, 'index');
    let data = this.tabIndex === 0 ? this.customer.listSetupBizDetails : this.customer.approvedHistory;
    this.initTableData(data);
  }

  getTableTitle() {
    let fieldName = '';
    if (this.type === 'detail') {
      if (this.tabIndex === 0) {
        fieldName = 'currentSetup';
      } else {
        fieldName = 'historySetup';
      }
    } else {
      fieldName = 'setupProposal';
    }
    return this.reductionProposalTranslate.labels[fieldName];
  }

  get isDetail() {
    return this.type === 'detail';
  }
}

