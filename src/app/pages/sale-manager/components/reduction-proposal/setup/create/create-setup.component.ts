import { DatePipe } from "@angular/common";
import { Component, Injectable, Injector, OnInit, ViewChild } from "@angular/core";
import { BaseComponent } from "src/app/core/components/base.component";
import { FunctionCode, functionUri } from "src/app/core/utils/common-constants";
import * as _ from 'lodash';
import { ReductionProposalApi } from "src/app/pages/sale-manager/api/reduction-proposal.api";
import { DeclareSetupProposalComponent } from "../declare/declare-setup.component";

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'app-create-setup',
  templateUrl: './create-setup.component.html',
  styleUrls: ['./create-setup.component.scss'],
  providers: [DatePipe]
})
export class CreateSetupProposalComponent extends BaseComponent implements OnInit {

  @ViewChild('declare') declare: DeclareSetupProposalComponent;
  customer;
  stateObj;
  setupId;

  constructor(
    injector: Injector,
    private reductionProposalApi: ReductionProposalApi,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REDUCTION_PROPOSAL}`);
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.customer = this.prop ? this.prop?.customer : {};
    this.stateObj = this.prop ? this.prop : {};
  }

  changeLoading(isLoading = false): void {
    this.isLoading = isLoading;
  }

  save(backToList) {
    return new Promise<any>(async (resolve, reject) => {
      console.log(this.declare);

      if (this.declare.listData.length === 0) {
        this.messageService.warn('Vui lòng nhập thông tin cài đặt');
        reject(false);
        return;
      }

      if (this.declare.listData.length > this.declare.commonData.listPeriod.length) {
        this.messageService.warn('Số lượng thông tin cài đặt vượt quá quy định');
        reject(false);
        return;
      }

      if (this.declare.listData.length > this.declare.commonData.listPeriod.length) {
        this.messageService.warn('Số lượng thông tin cài đặt vượt quá quy định');
        reject(false);
        return;
      }

      let hasError;
      let setups = this.declare.listData.map(item => {
        item = item.data;
        if (
          !this.declare.validatePeriod(item) ||
          !this.declare.validateMarginSetup(item?.marginSetup, item) ||
          !this.declare.validateDate(item, 'fromDate') || !this.declare.validateDate(item, 'toDate')
        ) {
          hasError = true;
          this.declare.updateTable();
          return;
        }
        return {
          loanPeriod: item.period,
          unit: item.currency,
          adjustPeriod: item.adjustmentPeriod,
          interestType: item.interestType,
          amplitude: Number(item.margin),
          interestReference: Number(item.referenceInterestRate),
          interestValue: Number(item.interestRate),
          startDate: item.fromDate.toISOString(),
          endDate: item.toDate.toISOString(),
          proposalReductionCode: item.proposalReductionCode,
          marginId: item?.marginId,
          amplitudeApply: Number(item.marginSetup),
          interestValueApply: Number(item.interestRateSetup),
        };
      });

      if (hasError) {
        this.messageService.warn('Vui lòng nhập thông tin cài đặt');
        reject(false);
        return;
      }

      let body = {
        customerCode: this.customer.customerCode,
        taxCode: this.customer.taxCode,
        blockCode: this.customer.blockCode,
        customerName: this.customer.customerName,
        customerType: this.customer.customerType,
        creditRanking: this.customer.creditRanking,
        customerSegment: this.customer.customerSegment,
        purpose: this.declare.form.controls.purpose.value,
        branchCode: this.customer.branchCode,
        rmCode: this.currUser.code,
        listSetupBizDetails: setups
      };
      this.isLoading = true;
      let api: any = this.reductionProposalApi.saveSetup(body);
      if (this.setupId) {
        api = this.reductionProposalApi.updateSetup(body);
      }
      api.subscribe((res) => {
        if (res) {
          this.setupId = res;
        }
        if (backToList) {
          this.messageService.success(this.notificationMessage.success);
          this.isLoading = false;
          setTimeout(() => {
            this.backStep();
          }, 2000);
        }
        setTimeout(() => {
          resolve(true)
        }, 100);
      }, (err) => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
        reject(false);
      });
    });
  }

  signSetup() {
    this.isLoading = true;
    this.save(false).then(() => {
      this.reductionProposalApi.signSetup(this.customer.customerCode).subscribe((res) => {
        this.isLoading = false;
        this.messageService.success(this.notificationMessage.success);
        setTimeout(() => {
          this.backStep();
        }, 2000);
      }, (err) => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      });
    }).catch(() => {
      this.isLoading = false;
    });
  }
  backStep(): void {
    this.router.navigateByUrl(functionUri.reduction_proposal, { state: this.prop ? this.prop : this.state });
  }
}
