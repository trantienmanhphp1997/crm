import { forkJoin, of } from 'rxjs';
import { global } from '@angular/compiler/src/util';
import { AfterViewInit, Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';
import { cleanDataForm } from 'src/app/core/utils/function';
import {
  CampaignStatus,
  CommonCategory,
  Division,
  FunctionCode,
  functionUri, ReductionProposalStatus,
  Scopes,
  maxInt32,
  SessionKey
} from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import * as moment from 'moment';
import { MatDialog } from '@angular/material/dialog';
import { ReductionProposalApi } from '../../../api/reduction-proposal.api';
import { ChooseCustomerModalComponent } from '../dialog/choose-customer-modal/choose-customer-modal.component';
import { CategoryService } from '../../../../system/services/category.service';
import { NavigationExtras } from '@angular/router';
import { saveAs } from 'file-saver';
import { HttpResponse } from '@angular/common/http';
import { AppFunction } from 'src/app/core/interfaces/app-function.interface';
import { RmApi } from 'src/app/pages/rm/apis';
import { Utils } from 'src/app/core/utils/utils';
import { FileService } from 'src/app/core/services/file.service';

@Component({
  selector: 'app-reduction-proposal-list-view',
  templateUrl: './reduction-proposal-list-view.component.html',
  styleUrls: ['./reduction-proposal-list-view.component.scss']
})
export class ReductionProposalListViewComponent extends BaseComponent implements OnInit, AfterViewInit {
  showCom = false;
  isLoading = false;
  listData = [];
  minEndDateLast: Date;
  maxEndDateFirst: Date;
  listDataReductionProposal = [];
  listDivision = [];
  listRmManager = [];
  listBranchesManager = [];
  customerType: any;
  objFunctionRM: AppFunction;
  allBranch = {
    branchCode: '',
    code: '',
    name: 'Tất cả'
  };
  allRM = {
    code: '',
    displayName: 'Tất cả'
  };
  formSearch = this.fb.group({
    code: '',
    customerName: '',
    status: '',
    createdDateFirst: null,
    createdDateLast: null,
    blockCode: '',
    customerCode: '',
    taxCode: '',
    type: 0,
    branchCodes: [],
    rmCodes: []
  });
  paramSearch = {
    size: global.userConfig.pageSize,
    page: 0,
    rsId: '',
    scope: Scopes.VIEW
  };
  prevParams: any;
  innerWidth: any;
  pageable: Pageable;
  prop: any;
  listStatus = [];
  listStatusReductionProposal = [
    {
      code: '',
      name: 'Tất cả'
    }
  ];
  listType = [
    { code: 0, name: 'Miễn giảm phí' },
    { code: 1, name: 'Miễn giảm lãi' },
    { code: 3, name: 'Miễn giảm lãi ngoại lệ'},
    { code: 4, name: 'Miễn giảm phí ngoại lệ'},
    { code: 2, name: 'Cài đặt lãi suất online'},
  ];
  commonData = {
		listRmManager: [],
		listRmManagerTerm: [],
		listBranchManager: [],
	};
  listManagerReduction = {};
  maxDate = new Date();
  listPurpose = [];
  listFormality = [];
  @Input() isViewFromPopup = false;
  isSelectedRadio = [];
  @Output() onChangeCampain = new EventEmitter();
  @Input() listBranchChoose: Array<any>;
  isRM = false;
  rmOrManager: any;
  facilities: Array<any>;
  facilityCode: Array<string>;
  highPrioritySort = [Division.INDIV, Division.SME, Division.CIB];
  listStatusSetupInterest = [{
    code: '',
    name: 'Tất cả'
  }];
  listStatusException = [{
    code: '',
    name: 'Tất cả'
  }];

  constructor(
    injector: Injector,
    // private modalActive: NgbActiveModal,
    private reductionProposal: ReductionProposalApi,
    private rmApi: RmApi,
    public dialog: MatDialog,
    private categoryService: CategoryService,
    private fileService: FileService,
  ) {
    super(injector);
    this.isLoading = true;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REDUCTION_PROPOSAL}`);
    this.objFunctionRM = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.paramSearch.rsId = this.objFunction?.rsId;
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
    console.log(this.prop);
    if(this.prop && this.prop.formSearch){
      this.formSearch.patchValue(this.prop.formSearch);
      this.facilityCode = this.formSearch.controls.branchCodes?.value;
    }
  }

  get isHO(): boolean {
    return this.objFunction.HO;
  }

  ngOnInit(): void {
    this.subscribeForm();
    this.innerWidth = window.innerWidth;
    if (!this.reductionProposal.info.closed) {
      this.reductionProposal.info.next({
        customer: {},
        toi: {},
        authorization: [],
        files: []
      });
      this.reductionProposal.info.complete(); // destroy info
    }

    const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
    this.listDivision =
      divisionOfUser?.map((item) => {
        return { code: item.code, displayName: `${item.code || ''} - ${item.name || ''}` };
      }).filter(item => item.code !== Division.DVC) || [];
    this.sortDivision();

    if (this.listDivision.length > 0 && !this.prop && !this.prop?.formSearch.blockCode) {
      this.formSearch.controls.blockCode.setValue(this.listDivision[0].code);
    }

    forkJoin([
      this.commonService.getCommonCategory(CommonCategory.REDUCTION_PROPOSAL_STATUS).pipe(catchError((e) => of(undefined))),
      this.categoryService
        .getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined))),
      this.rmApi
				.post('findAll', {
					page: 0,
					size: maxInt32,
					crmIsActive: true,
					branchCodes: [],
          // rmBlock: "SME",
					rsId: this.objFunctionRM?.rsId,
					scope: Scopes.VIEW,
				})
				.pipe(catchError(() => of(undefined))),
      this.categoryService
				.getTreeBranchesOfUser(this.objFunctionRM?.rsId, Scopes.VIEW)
				.pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.SETUP_INTEREST_STATUS).pipe(catchError((e) => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.REDUCTION_EXCEPTION_STATUS).pipe(catchError((e) => of(undefined))),
    ]).subscribe(([listStatusReductionProposal, branchesOfUser, listRmManagement, treeBranchOfUser, SETUP_INTEREST_STATUS, REDUCTION_EXCEPTION_STATUS]) => {
      this.listStatusReductionProposal = [...this.listStatusReductionProposal, ...listStatusReductionProposal?.content] || [];
      this.listStatusSetupInterest = [...this.listStatusSetupInterest, ...SETUP_INTEREST_STATUS.content];
      this.listStatusException = [...this.listStatusException, ...REDUCTION_EXCEPTION_STATUS.content];
      this.isRM = _.isEmpty(branchesOfUser);
      const listRm = [];
      listRmManagement?.content?.forEach((item) => {
				if (!_.isEmpty(item?.t24Employee?.employeeCode)) {
					listRm.push({
						code: _.trim(item?.t24Employee?.employeeCode),
						displayName: _.trim(item?.t24Employee?.employeeCode) + ' - ' + _.trim(item?.hrisEmployee?.fullName),
						branchCode: _.trim(item?.t24Employee?.branchCode),
					});
				}
			});
      if (!listRm?.find((item) => item.code === this.currUser?.code)) {
				listRm.push({
					code: this.currUser?.code,
					displayName:
						Utils.trimNullToEmpty(this.currUser?.code) + ' - ' + Utils.trimNullToEmpty(this.currUser?.fullName),
					branchCode: this.currUser?.branch,
				});
			}
      if(listRm.length == 1){
        const listRmCode = [listRm[0].code]
        this.formSearch.get('rmCodes').setValue(listRmCode);
      }
      this.commonData.listRmManagerTerm = [...listRm];
			this.commonData.listRmManager = [...listRm];
      if(treeBranchOfUser && treeBranchOfUser.length > 0)
				this.facilities = treeBranchOfUser
      else{
        this.facilities =[{
            code: this.currUser?.branch,
            id: this.currUser?.branch,
            name: this.currUser?.branchName,
            parentCode: 'VN',
            type: 3
        }]
      }
      if(this.facilities.length == 1){
        this.facilityCode = [this.facilities[0].code];
        this.formSearch.get('branchCodes').setValue(this.facilityCode);
      }
			treeBranchOfUser = treeBranchOfUser.map(item => {
				if(item.type === 1 || item.type === 2){
					item.level = 0;
				}else{
					item.level = null;
				}
			});
      this.search(true);
    });

    this.maxEndDateFirst = this.maxDate;
  }

  ngAfterViewInit() {
    this.formSearch.controls.createdDateFirst.valueChanges.subscribe((value) => {
      if (moment(value).isValid()) {
        const createdDateLast = this.formSearch.controls.createdDateLast.value;
        if (moment(createdDateLast).isValid() && moment(value).isAfter(moment(createdDateLast))) {
          this.formSearch.controls.endDateLast.setValue(value);
        }
        this.minEndDateLast = value;
      } else {
        this.minEndDateLast = null;
      }
    });

    this.formSearch.controls.createdDateLast.valueChanges.subscribe((value) => {
      if (moment(value).isValid()) {
        this.maxEndDateFirst = value;
      } else {
        this.maxEndDateFirst = this.maxDate;
      }
    });
  }

  search(isSearch?: boolean) {
    this.isLoading = true;
    let params: any = {};

    if (isSearch) {
      this.onCheckRadio();
      this.paramSearch.page = 0;
      params = cleanDataForm(this.formSearch);
      if (moment(params.createdDateFirst).isValid()) {
        params.createdDateFirst = moment(params.createdDateFirst).format('YYYY-MM-DD');
      } else {
        delete params.createdDateFirst;
      }
      if (moment(params.createdDateLast).isValid()) {
        params.createdDateLast = moment(params.createdDateLast).format('YYYY-MM-DD');
      } else {
        delete params.createdDateLast;
      }
      // this.formSearch.patchValue(params);
    } else {
      if (!this.prevParams) {
        return;
      }
      params = this.prevParams;
    }
    Object.keys(this.paramSearch).forEach((key) => {
      params[key] = this.paramSearch[key];
    });

    this.reductionProposal.searchReductionProposal(params).subscribe(
      (result) => {
        if (result) {
          this.prevParams = params;
          // this.prop.prevParams = params;
          this.listDataReductionProposal = result?.content || [];
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: global.userConfig.pageSize
          };
          const codes = this.listDataReductionProposal?.filter((i) => i.status !== CampaignStatus.InProgress).map(item => item.code);
          this.reductionProposal.getManagerReduction(codes).pipe(catchError(() => of(undefined)))
            .subscribe((managerReductions) => {
              managerReductions?.forEach(item => {
                this.listManagerReduction[item.code] = item.userName;
              });
            });
        }
        this.sessionService.setSessionData(FunctionCode.REDUCTION_PROPOSAL, this.prop);
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );

  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.page = pageInfo.offset;
    this.search(false);
  }

  onCheckRadio(item?: any) {
    this.isSelectedRadio = [];
    if (item) {
      this.isSelectedRadio.push(item);
    }
    this.onChangeCampain.emit(item);
  }

  deleteReductionProposal(item) {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.reductionProposal.deleteReductionProposal(item.id).subscribe(() => {
          this.listData = this.listData?.filter((i) => i.id !== item.id);
          this.search();
          this.messageService.success(this.notificationMessage.success);
        }, (e) => {
          this.isLoading = false;
          if (!_.isEmpty(e?.error?.description)) {
            this.messageService.error(e.error.description);
          } else {
            this.messageService.error(this.notificationMessage.error);
          }
        });
      }
    });
  }

  updateReductionProposal(item): void {

    if(item?.type === 4) {
      this.router.navigate([functionUri.reduction_fee_exception, 'create'], { queryParams: {
          mode: 'edit',
          id: item.id,
        },
        state: {
          search : this.formSearch.value
        }});
      return;
    }

    const customer = {
      existed: true,
      isKhdn: true,
      isNew: false,
      searchLabel: '',
      searchPlaceholder: '',
      searchLabel2: '',
      searchPlaceholder2: '',
      customerCode: '',
      customerName: '',
      taxCode: '',
      segment: '',
      relationship: '',
      dtttrr: '',
      piority: '',
      classification: '',
      toiCurrent: '',
      nimCurrent: '',
      scoreValue: ''
    };
    this.reductionProposal.info.next({ customer, toi: {}, authorization: [], files: [] });
    if(item.type === 3){
      this.router.navigate([functionUri.reduction_interest_exception, 'update'], {
        queryParams: {
          reductionId: item.id,
          type: item.type
        },
        state: {
          item,
          formSearch : this.formSearch.value
        }
      });
      return;
    }
    this.router.navigate(['sale-manager/reduction-proposal', 'update'], {
      skipLocationChange: false,
      queryParams: {
        reductionId: item.id,
        type: item.type
      },
      state: {
        formSearch : this.formSearch.value
      }
    });
  }

  changeBlock(event) {
    this.customerType = event.value;
  }

  getStatusSME(value) {
    return (
      this.listStatusReductionProposal?.find((item) => {
        return item.code === value;
      })?.name || ''
    );
  }

  getType(value) {
    return (
      this.listType?.find((item) => {
        return item.code === value;
      })?.name || ''
    );
  }

  sortDivision(): void {
    const currentDisivionList: any[] = _.cloneDeep(this.listDivision);
    const result = [];
    this.highPrioritySort.forEach(code => {
      const index = currentDisivionList.findIndex(item => item.code === code);
      if (index > -1) {
        result.push(currentDisivionList[index]);
        currentDisivionList.splice(index, 1);
      }
    });

    result.push(...currentDisivionList);
    this.listDivision = result;
  }

  isUpdateAndDelete(item) {
    return (
      [ReductionProposalStatus.InProgress].includes(item.status) && item.createdBy === this.currUser?.username
    );
  }

  isConfirm(item): boolean {
    const checkStatus = [ReductionProposalStatus.APPROVED,ReductionProposalStatus.CBQL_REFUSED_CONFIRM].includes(item.status)
    return ((checkStatus && (item.createdBy === this.currUser?.username)) || ([ReductionProposalStatus.WAITING_CONFIRM].includes(item.status) && (item.type == 4 ? true : this.listManagerReduction[item.code] === this.currUser?.username ))
    );
  }
  isRmOrManager(item): boolean {
    if(this.isRM || ([ReductionProposalStatus.APPROVED, ReductionProposalStatus.CBQL_REFUSED_CONFIRM].includes(item.status) && (item.createdBy === this.currUser?.username))){
      return true;
    }
      return false;
  }

  openConfirm(row): void {
    const navigationExtras: NavigationExtras = {
      state: {
        action: this.isRmOrManager(row) ? 'confirm' : 'approved',
        id: row.id,
        code: row.code,
        formSearch : this.formSearch.value
      },
      queryParams: {
        id: row.id,
        code: row.code,
        isRm: this.isRM,
        action: this.isRmOrManager(row) ? 'confirm' : 'approved',
      }
    };
    if(row?.type == 3){
      this.router.navigate([functionUri.reduction_interest_exception, 'detail'], { queryParams: {
          mode: 'details',
          id: row.id,
          action: 'approved',
        },
        state: {
          formSearch : this.formSearch.value
        }
      });
      return;
    }
    if(row?.type == 4){
      this.router.navigate([functionUri.reduction_fee_exception, 'create'], { queryParams: {
        mode: 'details',
        id: row.id,
        action: 'approved',
      },
      state: {
        search : this.formSearch.value
      }
    });
      return;
    }

    this.router.navigate(['sale-manager/reduction-proposal/detail'], navigationExtras);
  }


  openCreatePopup() {
    this.router.navigate([functionUri.reduction_proposal, 'create'], { state: {
      formSearch : this.formSearch.value
      } });
    // const division = this.formSearch.controls.blockCode.value;
    // const khdn = ['SME', 'CIB', 'FI', 'DVC'];
    // const customer = {
    //   existed: false,
    //   isKhdn: khdn.includes(division),
    //   isNew: false,
    //   searchLabel: '',
    //   searchPlaceholder: '',
    //   searchLabel2: '',
    //   searchPlaceholder2: '',
    //   customerCode: '',
    //   customerName: '',
    //   taxCode: '',
    //   segment: '',
    //   relationship: '',
    //   dtttrr: '',
    //   piority: '',
    //   classification: '',
    //   toiCurrent: '',
    //   nimCurrent: '',
    //   scoreValue: ''
    // };
    // this.reductionProposal.info.next({ customer, toi: {}, authorization: [], files: [] });
    // this.dialog.open(ChooseCustomerModalComponent, {width: '400px'});
  }

  onAction(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      let queryParams: any = {};
      let stateOpt: any = {};
      let path;

      if(item?.type === 4) {
        this.router.navigate([functionUri.reduction_fee_exception, 'create'], { queryParams: {
          mode: 'details',
          id: item.id,
        },
        state: {
          search : this.formSearch.value
        }});
        return;
      }
      if(item?.type === 3) {
        this.router.navigate([functionUri.reduction_interest_exception, 'detail'], { queryParams: {
            mode: 'detail',
            id: item.id,
          },
          state: {
            formSearch : this.formSearch.value
          }});
        return;
      }
      if(item?.type === 2){
        queryParams.customerCode = item.customerCode;
        path = 'detail-setup';
        item.type = 'detail';
        stateOpt.customer = item;
      }else{
        queryParams.id = item.id;
        path = 'detail';
      }

      this.router.navigate([this.router.url, path], {
        queryParams,
        skipLocationChange:false,
        state: {
          ...stateOpt,
          id: item.id,
          formSearch : this.formSearch.value
        }
      });
    }
  }
  fnTrim(event, formControlName){
    this.formSearch.controls[formControlName].setValue(this.formSearch.getRawValue()[formControlName].trim());
  }

  downloadECM(row){
    this.isLoading = true;
    if(row.type == 3 || row.type == 4) {
      this.download(row?.ecmDocId, row?.fileName);
      return;
    }
    this.reductionProposal.downloadFilesECM(row.ecmDocId).subscribe((response: HttpResponse<ArrayBuffer>) => {
      if (response) {
        const blob = new Blob([response.body], { type: 'pdf' || 'application/octet-stream' });
        saveAs(blob, `${row.fileName}.${'pdf'}`);
      }
      this.isLoading = false;
    }, (e) => {
        this.isLoading = false;
        if (!_.isEmpty(e?.error?.description)) {
          this.messageService.error(e.error.description);
        } else {
          this.messageService.error(this.notificationMessage.error);
        }
      });
  }
  // downloadECM(row){
  //   this.isLoading = true;
  //   this.reductionProposal.encryptEcm(row.ecmDocId).subscribe(value => {
  //     this.reductionProposal.downloadFilesECM2(value).subscribe((response: any) => {
  //       if (response) {
  //         const blob = new Blob([response], { type: 'pdf' || 'application/octet-stream' });
  //         const url = window.URL.createObjectURL(blob);
  //         const pwa = window.open(url);
  //         if (!pwa || pwa.closed || typeof pwa.closed === 'undefined') {
  //           alert( 'Please disable your Pop-up blocker and try again.');
  //         }
  //         // saveAs(blob, `${row.fileName}.${'pdf'}`);
  //       }
  //       this.isLoading = false;
  //     }, (e) => {
  //       this.isLoading = false;
  //       if (!_.isEmpty(e?.error?.description)) {
  //         this.messageService.error(e.error.description);
  //       } else {
  //         this.messageService.error(this.notificationMessage.error);
  //       }
  //     });
  //   });
  // }

  getLabelTaxCode(): string {
    return Division.INDIV == this.formSearch.controls.blockCode.value ? 'GTTT' : 'Mã số thuế';
  }

  changeBranch(event) {
		let listAllRMInBranches = [];
    const listRmSelected = this.formSearch.get('rmCodes').value;
    let listRm = [];
		if(event.length > 0){
			event.forEach(element => {
				let listRMInBranch:any;
				listRMInBranch = _.filter(
					this.commonData.listRmManagerTerm,
					(item) => item.branchCode === element
				  );
				listAllRMInBranches.push(...listRMInBranch) ;
			});
      if(listRmSelected){
        listRmSelected.forEach(rm => {
          const checkRmSelect = _.filter(listAllRMInBranches,
              (item) => item.code === rm
          );
          if(checkRmSelect && checkRmSelect?.length > 0){
            listRm.push(rm)
          }
        })
      }

		}else{
      listAllRMInBranches = this.commonData.listRmManagerTerm;
      listRm = [];
    }
    this.formSearch.get('rmCodes').setValue(listRm);
    this.formSearch.get('branchCodes').setValue(this.facilityCode);
		this.commonData.listRmManager = [...listAllRMInBranches];
		this.objFunctionRM?.rsId;
	}

  getValueConcat(code,name){
    if(code && name)
      return code.concat(' - ').concat(name)
    else if(code && !name)
      return code
    else
      return name
  }

  subscribeForm(){
      this.formSearch.controls.type.valueChanges.subscribe(res => {
        this.formSearch.controls.status.setValue('');
        if(res == 2) {
          this.formSearch.controls.blockCode.setValue('SME');
          this.formSearch.controls.blockCode.disable();
          this.search(true);
        } else {
          this.search(true);
          this.formSearch.controls.blockCode.enable();
        }
      });
      this.formSearch.controls.blockCode.valueChanges.subscribe(res => {
        if(res != Division.SME) {
          this.listType = this.listType.filter(x => x.code != 2);
        } else {
          const code = this.listType.find(x => x.code == 2);
          if(!code) {
            this.listType.push({ code: 2, name: 'Cài đặt lãi kênh online'});
          }
        }
        if(res == Division.INDIV || res == Division.DVC || res == Division.FI) {
          this.listType = this.listType.filter(x => x.code != 3 && x.code != 4);
        } else {
          const code = this.listType.find(x => x.code == 3 || x.code == 4);
          if(!code) {
            this.listType.push(
              { code: 3, name: 'Miễn giảm lãi ngoại lệ'},
              { code: 4, name: 'Miễn giảm phí ngoại lệ'},
            );
          }
        }
      });

  }

  updateSetupInterest(item){
    let queryParams: any = {};
    let stateOpt: any = {};

    queryParams.customerCode = item.customerCode;
    item.type = 'update';
    stateOpt.customer = item;

    this.router.navigate([this.router.url, 'update-setup'], {
      queryParams,
      skipLocationChange:false,
      state: {
        ...stateOpt,
        id: item.id,
        formSearch : this.formSearch.value
      }
    });
  }

  isConfirmSetupInterest(row){
    return row.status == 'WAITING_APPROVE' && !this.isRM;
  }

  openConfirmSetupInterest(item){
    let queryParams: any = {};
    let stateOpt: any = {};

    queryParams.customerCode = item.customerCode;
    item.type = 'detail';
    stateOpt.customer = item;
    stateOpt.isApprove = true;

    this.router.navigate([this.router.url, 'detail-setup'], {
      queryParams,
      skipLocationChange:false,
      state: {
        ...stateOpt,
        id: item.id,
        formSearch : this.formSearch.value
      }
    });
  }

  isUpdateSetupInterest(row){
    return row.createdBy == this.currUser?.username || row?.rmCode == this.currUser?.code;
  }

  isApproval(item){
    return ReductionProposalStatus.WAITING_CONFIRM == item.status && !this.isRM;
  }

  isUpdateAndDeleteException(item){
    return (
      [ReductionProposalStatus.InProgress, ReductionProposalStatus.CBQL_REFUSED_CONFIRM].includes(item.status) && item.createdBy === this.currUser?.username
    );
  }

  download(fileId: any, name) {
    this.isLoading = true;
    this.fileService.downloadFile(fileId, name).subscribe((res) => {
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      };
      this.isLoading = false;
    }, err => {
      this.isLoading = false;
    });
  }

}
