import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReductionProposalListViewComponent } from './reduction-proposal-list-view.component';

describe('CampaignListViewComponent', () => {
  let component: ReductionProposalListViewComponent;
  let fixture: ComponentFixture<ReductionProposalListViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReductionProposalListViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReductionProposalListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
