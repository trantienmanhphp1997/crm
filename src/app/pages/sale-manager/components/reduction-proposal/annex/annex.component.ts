import { ChangeDetectorRef, Component, EventEmitter, Injectable, Injector, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { BehaviorSubject, forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import { FileService } from 'src/app/core/services/file.service';
import { CustomerType, Division, FunctionCode } from 'src/app/core/utils/common-constants';
import { Utils } from 'src/app/core/utils/utils';
import { CustomerApi, CustomerDetailSmeApi } from 'src/app/pages/customer-360/apis/customer.api';
import _ from 'lodash';
import { formatDate, formatNumber } from '@angular/common';
import * as moment from 'moment';
import { ReductionProposalApi } from '../../../api/reduction-proposal.api';
import { MODE } from './define';

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'app-free-discount-annex',
  templateUrl: './annex.component.html',
  styleUrls: ['./annex.component.scss']
})
export class FreeDiscountAnnexComponent extends BaseComponent implements OnInit {

  fileInfo = {};
  fileListForm: any[] = [];
  imgListForm: any[] = [];
  uploadedFiles = [];
  overFile: boolean;
  // @Input() fileInfo = {};
  // @Input() fileListForm: any[] = [];
  // @Input() imgListForm: any[] = [];
  form = this.fb.group({
    businessActivities: '',
    planInformation: '',
    financialSituation: '',
    resultBusiness: '', // kết quả kinh doanh qua các năm
    competitiveness: '',
    proposedBasis: '', // cơ sở đề xuất
    policyApplied: '', // chính sách áp dụng
  });
  isLoadingInfo: boolean;
  updateObj;
  previousYearWarning: string;
  show_error_riskManagement: boolean;
  isBusinessCustomer: boolean;
  model: any;
  isBusinessDebit: boolean;
  dataEarly: any;
  isBusinessMobilization: boolean;
  isBusinessGuarantee: boolean;
  isBusinessCommercialFinance: boolean;
  isBusinessServiceProducts: boolean;
  twoMonthAgoWarning: any;
  previousMonthWarning: any;
  creditInformation: any;
  divisionProduct: any;
  registrationNumber: any;
  codeCustomer = '';
  relationshipHT: any;
  relationshipTCTD: any;
  classification: any;
  maxSize = 20; // MAX SIZE UPLOAD 20 MB;
  info: any;
  existed: boolean;
  doanhSoSNK = [];
  noChuY: boolean;
  tyLeKhaiThac: any;
  duNoKHCN = [];
  camKetNgoaiBang = [];
  camKetNgoaiBangData = [];
  totalCredit: any
  dataCIC: any;
  formSearch: any;
  doanhSoTttmTaiHT: any;
  isView: boolean;
  reductionProposalDetail: any;
  doanhSoXNKData = [];
  doanhSoXNKGroup = {};
  loadedDoanhSoSNK = false;
  loadedDoanhSoTttmTaiHT = false;
  loadedTyLeKhaiThac = false;
  nameYearT: any;
  nameT1: any;
  nameT: any;
  dataYearT1 : any = {};
  dataT2: any = {};
  dataT1 : any = {};

  @Input() mode: string;
  @Input() isUpdate = false;
  @Input() searchValue;
  @Input() set onLoaded(data) {
    console.log(data);
    if (data) {
      this.updateObj = data;
      this.loadData();
    }
  };
  @Input() set customerCode(code) {
    console.log(code);
    if (!code) {
      this.registrationNumber = this.reductionProposalDetail?.businessRegistrationNumber;
      this.divisionProduct = this.reductionProposalDetail?.blockCode;
      if (this.registrationNumber) {
        if (this.searchValue?.blockCode != Division.INDIV) {
          this.getCreditInformation({
            identifiedNumber: this.registrationNumber,
            division: this.divisionProduct,
          });
        }

      }
      return;
    }
    this.codeCustomer = code;
    this.existed = true;
    // this.loadDataFromCode(code);
  }
  @Output() changeLoading = new EventEmitter();

  constructor(
    injector: Injector,
    private fileService: FileService,
    private customerDetailSmeApi: CustomerDetailSmeApi,
    private customerApi: CustomerApi,
    public reductionProposalApi: ReductionProposalApi,
    private dtc: ChangeDetectorRef,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.OPPORTUNITY}`); // will edit
  }

  ngOnInit(): void {
    this.existed = this.route.snapshot.queryParams['isExist'] == 'true';
    this.isView = this.mode == MODE.VIEW;
  }

  mapDataDSXNK() {

  }

  formatDate(date, type = 'DD/MM/YYYY') {
    return moment(date).format(type);
  }

  // dư nợ thể tín dụng
  loadDuNoKHCN(idCard) {
    // 065487854
    if (!idCard) {
      return;
    }
    const params = {
      idCard: idCard
    }
    this.customerApi.getDuNoKHCN(params).subscribe(res => {
      if (res.data.length) {
        this.duNoKHCN = res.data.sort((a: any, b: any) => {
          return Number(new Date(this.formatDate(a.SNAPSHOT_DT, 'YYYY/MM/DD'))) - Number(new Date(this.formatDate(b.SNAPSHOT_DT, 'YYYY/MM/DD')));
        });
      }
    })
    const paramCic = {
      idcard: idCard
    }
    this.getCIC(paramCic);
  }

  // lấy dữ liệu CIC biểu phí
  getCIC(params) {
    if (this.searchValue?.type === 0 && this.searchValue?.blockCode === Division.INDIV || this.reductionProposalDetail?.type === 0 && this.reductionProposalDetail?.blockCode === Division.INDIV) {
      if (this.searchValue?.type === 1) {
        return;
      }
      this.customerDetailSmeApi.getCIC(params).subscribe(res => {
        this.dataCIC = res.data;
        this.creditInformation = {};
        this.creditInformation['customerCICDTOS'] = this.dataCIC.sort((a: any, b: any) => {
          return Number(new Date(this.formatDate(a.SNAPSHOT_DT, 'YYYY/MM/DD'))) - Number(new Date(this.formatDate(b.SNAPSHOT_DT, 'YYYY/MM/DD')));
        });
        this.isLoadingInfo = true;
        this.isLoading = false;
      }, err => {
        this.isLoadingInfo = true;
        this.isLoading = false;
      })
    }
  }

  loadDataByTaxCode(taxCode) {
    // doanh số snk
    this.customerApi.getImportAndExportSales(taxCode).subscribe(res => {
      if (res) {
        this.doanhSoSNK = res.data;
      }
      this.loadedDoanhSoSNK = true;
      this.groupDsxnk();
    });
    // nợ chú ý
    this.customerApi.getAttentionOwed(taxCode).subscribe(res => {
      if (res) {
        if (res.data && res.data.length) {
          this.noChuY = true;
        }
      }
    });
    this.loadEximTurnoverTtqt(taxCode);
    this.getOffBalance(taxCode);
    this.getEximMiningRate(taxCode);
    this.loadDuNoKHCN(taxCode);
  }

  findCardValue(name) {
    if (!name || !this.totalCredit) {
      return;
    }
    const findname = this.totalCredit.find(item => item.tenctdphathanh == name);
    if (findname) {
      return findname['sotienphaithanhtoan']
    }
  }

  getToltalByProperty(arr = [], pro) {
    if (!arr.length) {
      return;
    }
    let sum = 0;
    arr.forEach(item => {
      if (!item[pro]) {
        return;
      }
      sum += +item[pro];
    })
    return sum;
  }

  checkNmber(value) {
    let retNumber = Number(value);
    return isNaN(retNumber) ? 0 : retNumber;
  }

  getTotalCredit(taxCode) {
    const params = {
      taxCode: taxCode
    }
    this.customerApi.getTotalCredit(params).subscribe(res => {
      this.totalCredit = res.data;
      this.totalCredit.forEach(element => {
        for (let index = 0; index < this.camKetNgoaiBangData.length; index++) {
          const elm = this.camKetNgoaiBangData[index];
          if (elm?.ten_tctd === element.tenctdphathanh) {
            this.camKetNgoaiBangData[index].the = element.sotienphaithanhtoan != null ? this.convertMilion(element.sotienphaithanhtoan) : '--';
          }
        }
        const findName = this.camKetNgoaiBang.find(item => item.tentctd == element.tenctdphathanh);
        // không có tên thì push thẳng vào data đang có
        if (!findName) {
          this.camKetNgoaiBang.push({
            tentctd: element.tenctdphathanh,
            card: element.sotienphaithanhtoan
          });
          this.camKetNgoaiBangData.push({
            ten_tctd: element.tenctdphathanh,
            the: element.sotienphaithanhtoan != null ? this.convertMilion(element.sotienphaithanhtoan) : '--',
            nhom_no: '--',
            trai_phieu: '--', // chưa có api
            cknb_trd: '--', // Nếu loaitien là VND thì truyền giatri vào đây
            cknb_nt: '--' // Nếu loaitien là CND thì truyền giatri vào đây

          });
        }
      });
    })
  }
  // Cam kết ngoại bảng
  getOffBalance(taxCode) {
    const params = {
      taxCode: taxCode
    }
    this.customerDetailSmeApi.getOffBalance(params).subscribe(res => {
      this.camKetNgoaiBang = res.data;
      this.camKetNgoaiBangData = res.data?.map(i => {
        return {
          ten_tctd: i?.tentctd,
          nhom_no: i?.nhomno !== null && !isNaN(i?.nhomno) ? 'Nhóm ' + Number(i?.nhomno) : '--',
          the: '--', // thẻ
          trai_phieu: '--', // chưa có api
          // KDL confirm giá trị trả về có đơn vị là trđ
          cknb_trd: i?.loaitien === 'VND' ? this.formatCurrency(i?.giatri) : '--', // Nếu loaitien là VND thì truyền giatri vào đây
          cknb_nt: i?.loaitien !== 'VND' ? this.formatCurrencyWithUnit(i?.giatri, i?.loaitien) : '--' // Nếu loaitien là CND thì truyền giatri vào đây
        }
      });
      this.getTotalCredit(taxCode);
    })
  }

  loadEximTurnoverTtqt(taxCode) {
    // '0900218714'
    const params = {
      taxCode: taxCode
    }
    this.customerApi.getEximTurnoverTtqt(params).subscribe(res => {
      if (res) {
        this.doanhSoTttmTaiHT = res.data;
      }
      this.loadedDoanhSoTttmTaiHT = true;
      this.groupDsxnk();
    });
  }

  // tỉ lệ khai thác
  getEximMiningRate(taxCode) {
    const params = {
      taxCode: taxCode
    }
    this.customerApi.getEximMiningRate(params).subscribe(res => {
      if (res) {
        this.tyLeKhaiThac = res.data;
      }
      this.loadedTyLeKhaiThac = true;
      this.groupDsxnk();
    });
  }

  loadDataFromCode(code) {
    forkJoin([
      this.customerDetailSmeApi.get(code).pipe(catchError(() => of(undefined))),
      this.customerDetailSmeApi.getDataEarlyWarningModel(code).pipe(catchError(() => of(undefined))),
      this.customerDetailSmeApi.getClassification({ customerCode: code }).pipe(catchError(() => of(undefined))),
    ]
    ).subscribe(([model, dataEarly, classification]) => {
      console.log(model, dataEarly, classification);

      this.model = model;
      this.dataEarly = dataEarly;
      this.classification = _.isEmpty(classification) ? '' : classification;
      this.divisionProduct = _.get(this.model, 'systemInfo.accountType');
      if (this.divisionProduct === CustomerType.NHS) {
        this.divisionProduct = CustomerType.SME;
      }
      this.registrationNumber = _.get(this.model, 'customerInfo.registrationNumber');
      const paramCreditInfo = {
        identifiedNumber: this.registrationNumber,
        division: this.divisionProduct,
      };
      if (this.registrationNumber) {
        if (this.searchValue?.blockCode != Division.INDIV) {
          this.getCreditInformation(paramCreditInfo);
        }
      }
      if (!_.isEmpty(this.dataEarly)) {
        this.isLoading = false;
        this.dataEarly = this.dataEarly;
        this.previousYearWarning = `${_.get(this.fields, 'year')} ${moment(
          _.get(this.dataEarly, 'dataY1.dataDate')
        ).format('YYYY')}`;
        this.previousMonthWarning = `${_.get(this.fields, 'year')} ${moment(
          _.get(this.dataEarly, 'dataT1.dataDate')
        ).format('YYYY')} (${_.get(this.fields, 'month')} ${moment(_.get(this.dataEarly, 'dataT1.dataDate')).format(
          'MM'
        )})`;

        this.twoMonthAgoWarning = `${_.get(this.fields, 'year')} ${moment(
          _.get(this.dataEarly, 'dataT2.dataDate')
        ).format('YYYY')} (${_.get(this.fields, 'month')} ${moment(_.get(this.dataEarly, 'dataT2.dataDate')).format(
          'MM'
        )})`;
        if (
          _.isEmpty(_.get(this.dataEarly, 'dataY1.dataDate')) ||
          _.isEmpty(_.get(this.dataEarly, 'dataT2.dataDate'))
        ) {
          const dataDateT1 = _.get(this.dataEarly, 'dataT1.dataDate').split('-');
          if (+dataDateT1[1] === 1) {
            this.twoMonthAgoWarning =
              `${_.get(this.fields, 'year')} ` + (dataDateT1[0] - 1) + `(${_.get(this.fields, 'month')} 12)`;
            this.previousYearWarning = `${_.get(this.fields, 'year')} ` + (dataDateT1[0] - 1);
          } else {
            this.twoMonthAgoWarning =
              `${_.get(this.fields, 'year')} ` +
              dataDateT1[0] +
              `(${_.get(this.fields, 'month')} ` +
              moment((dataDateT1[1] - 1).toString()).format('MM') +
              ')';
            this.previousYearWarning = `${_.get(this.fields, 'year')} ` + (dataDateT1[0] - 1);
          }
        }
        this.show_error_riskManagement = false;
      } else {
        const dataDateT1 = +moment().format('MM');
        if (+moment().format('MM') === 1) {
          this.previousYearWarning = `${_.get(this.fields, 'year')} ` + (+moment().format('yyyy') - 2);
          this.previousMonthWarning =
            `${_.get(this.fields, 'year')}` +
            (+moment().format('YYYY') - 1) +
            `(${_.get(this.fields, 'month')} ` +
            '12)';
          this.twoMonthAgoWarning =
            `${_.get(this.fields, 'year')}` +
            (+moment().format('YYYY') - 1) +
            `(${_.get(this.fields, 'month')} ` +
            '11)';
        } else {
          this.previousYearWarning = `${_.get(this.fields, 'year')} ` + (+moment().format('yyyy') - 1);
          this.previousMonthWarning =
            `${_.get(this.fields, 'year')} ${moment().format('YYYY')} (${_.get(this.fields, 'month')} ` +
            moment((dataDateT1 - 1).toString()).format('MM') +
            ')';
          this.twoMonthAgoWarning =
            `${_.get(this.fields, 'year')} ${moment().format('YYYY')} (${_.get(this.fields, 'month')} ` +
            moment((dataDateT1 - 2).toString()).format('MM') +
            ')';
        }
        this.show_error_riskManagement = true;
        this.isLoading = false;
      }
    });
  }

  initFile() {
    const info: any = this.reductionProposalApi?.info?.value;
    if (info?.files) {
      this.uploadedFiles = info?.files;
      this.dtc.detectChanges();
    }

  }

  compareDate(type: string) {
    let message = '';
    const now = moment().startOf('day');
    const date = this.creditInformation?.dsnapShotDt || '';
    if (!Utils.isEmpty(date)) {
      const dateAdd30Day = moment(moment(date, 'DD-MM-YYYY').add(30, 'days'));
      if (dateAdd30Day <= now) {
        message = 'Dữ liệu CIC đã quá hạn. Đề nghị anh/chị thực hiện tra cứu lại!';
      }
    } else {
      message = 'Chưa có dữ liệu CIC. Đề nghị anh/chị thực hiện tra cứu!';
    }

    if (this.searchValue.blockCode == Division.INDIV) {
      // 1.Nợ vay không có dữ liệu + Thẻ không có dữ liệu -> Hiển thị: Chưa có dữ liệu CIC. Đề nghị anh/chị thực hiện tra cứu lại!
      // 2.Nợ vay có dữ liệu thỏa mãn, Thẻ không có dữ liệu ->Hiển thị: Chưa có dữ liệu CIC 21. Đề nghị anh/chị thực hiện tra cứulại!
      // 3.Nợ vay có dữ liệu nhưng quá hạn, Thẻ không có dữ liệu  ->Hiển thị: Dữ liệu CIC 06 đã quá hạn và Chưa có dữ liệu CIC 21. Đề nghị anh/chị thực hiện tra cứu lại!
      // 4.Nợ vay không có dữ liệu + Thẻ có dữ liệu thỏa mãn ->Hiển thị: Chưa có dữ liệu CIC 06. Đề nghị anh/chị thực hiện tra cứu lại!
      // 5.Nợ vay có dữ liệu thỏa mãn,Thẻ có dữ liệu thỏa mãn -> Trình bước tiếp theo
      // 6.Nợ vay có dữ liệu nhưng quá hạn,Thẻ có dữ liệu thỏa mãn -> Hiển thị: Dữ liệu CIC 06 đã quá hạn. Đề nghị anh/chị thực hiện tra cứu lại!
      // 7.Nợ vay không có dữ liệu, Thẻ có dữ liệu nhưng quá hạn -> Hiển thị: Chưa có dữ liệu CIC 06 và Dữ liệu CIC 21 đã quá hạn. Đề nghị anh/chị thực hiện tra cứu lại!
      // 8.Nợ vay có dữ liệu thỏa mãn, Thẻ có dữ liệu nhưng quá hạn -> Hiển thị: Dữ liệu CIC 21 đã quá hạn. Đề nghị anh/chị thực hiện tra cứu lại!
      // 9.Nợ vay có dữ liệu nhưng quá hạn,Thẻ có dữ liệu nhưng quá hạn ->Hiển thị: Dữ liệu CIC đã quá hạn. Đề nghị anh/chị thực hiện tra cứu lại!
      message = '';
      let dateCICKHCN = null;
      let dateDuNoKHCN = null;

      if (this.dataCIC.length) {
        dateCICKHCN = this.dataCIC[0]?.cic_date;
      }
      if (this.duNoKHCN.length) {
        dateDuNoKHCN = this.duNoKHCN[0]?.TIMERESPONSE;
      }
      if (!dateCICKHCN) {
        if (!dateDuNoKHCN) {
          message = 'Chưa có dữ liệu CIC. Đề nghị anh/chị thực hiện tra cứu lại!';
        }
        if (dateDuNoKHCN && moment(moment(dateDuNoKHCN).add(30, 'days')) > now) {
          message = 'Chưa có dữ liệu CIC 06. Đề nghị anh/chị thực hiện tra cứu lại!';
        }
        if (dateDuNoKHCN && moment(moment(dateDuNoKHCN).add(30, 'days')) <= now) {
          message = 'Chưa có dữ liệu CIC 06 và Dữ liệu CIC 21 đã quá hạn. Đề nghị anh/chị thực hiện tra cứu lại!';
        }
      }
      if (moment(moment(dateCICKHCN).add(30, 'days')) > now) {
        if (!dateDuNoKHCN) {
          message = 'Chưa có dữ liệu CIC 21. Đề nghị anh/chị thực hiện tra cứulại!';
        }
        if (dateDuNoKHCN && moment(moment(dateDuNoKHCN).add(30, 'days')) > now) {
          // Trình bước tiếp theo
          message = '';
        }
        if (dateDuNoKHCN && moment(moment(dateDuNoKHCN).add(30, 'days')) <= now) {
          message = 'Dữ liệu CIC 21 đã quá hạn. Đề nghị anh/chị thực hiện tra cứu lại!';
        }
      }
      if (moment(moment(dateCICKHCN).add(30, 'days')) <= now) {
        if (!dateDuNoKHCN) {
          message = 'Dữ liệu CIC 06 đã quá hạn và Chưa có dữ liệu CIC 21. Đề nghị anh/chị thực hiện tra cứu lại!';
        }
        if (dateDuNoKHCN && moment(moment(dateDuNoKHCN).add(30, 'days')) > now) {
          message = 'Dữ liệu CIC 06 đã quá hạn. Đề nghị anh/chị thực hiện tra cứu lại!'
        }
        if (dateDuNoKHCN && moment(moment(dateDuNoKHCN).add(30, 'days')) <= now) {
          message = 'Dữ liệu CIC đã quá hạn. Đề nghị anh/chị thực hiện tra cứu lại!';
        }
      }
      const info: any = this.reductionProposalApi?.info?.value;
      if (info?.customer?.chargeGroup == 'NON_CREDIT' || info?.customer?.chargeGroup == 'REMITTANCES') {
        message = '';
      }
    }
    if (this.searchValue.blockCode == Division.FI) {
      message = '';
    }
    if (type === 'signSubmit' && !Utils.isEmpty(message)) {
      this.messageService.error(message);
    } else if (!Utils.isEmpty(message)) {
      this.messageService.warn(message);
    }
    return Utils.isEmpty(message) ? true : type !== 'signSubmit';
  }

  getValue(item: any, key: string) {
    return Utils.isStringEmpty(_.get(item, key)) ? '---' : _.get(item, key);
  }

  getValueOf(value) {
    return +value.valueOf();
  }


  download(file: any) {
    const title = file?.fileName;
    this.fileService.downloadFile(file?.fileId, title).subscribe((res) => {
      this.changeLoading.emit(false);
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }

  removeFile(id: string): void {
    const info: any = this.reductionProposalApi?.info?.value;
    if (info?.files) {
      const removed = info?.files.filter(file => file.fileId != id);
      info.files = removed;
      this.reductionProposalApi?.info.next(info);
    }
  }

  onDragOver(event): void {
    event.preventDefault();

    // this.overFile = true;
  }

  onDropSuccess(event): void {
    const lengthFile = this.searchValue.blockCode == Division.INDIV ? 5 : 10;
    const uploadedFiles = this.reductionProposalApi?.info?.value?.files;
    const allows = ['.xls', '.xlsx', '.pdf', '.doc', '.docx'];
    event.preventDefault();
    console.log(uploadedFiles);
    if (uploadedFiles?.length + event.dataTransfer.files?.length > lengthFile) {
      this.messageService.error(`Chỉ được chọn tối đa ${lengthFile} file đính kèm}`);
      return;
    }
    let countMaxData = 0;
    let countNotAllow = 0;
    let errMessage = '';
    let maxSizeByte = this.maxSize * 1024 * 1024;
    for (let file of event.dataTransfer.files) {
      const name = file.name;
      if (file?.size <= maxSizeByte && allows.includes(name.substr(name.lastIndexOf('.')).toLowerCase())) {
        this.upload(file);
      } else if (file?.size > maxSizeByte) {
        countMaxData++;
      } else if (!allows.includes(name.substr(name.lastIndexOf('.')))) {
        countNotAllow++;
      }
    }
    if (countMaxData > 0) {
      errMessage = "Dung lượng tối đa 20MB mỗi file. Có " + countMaxData + " vượt quá dung lượng cho phép\n";
    }
    if (countNotAllow > 0) {
      errMessage = "File không đúng định dạng cho phép (.xls, .xlsx, .pdf, .doc, .docx)";
    }
    if (errMessage?.length > 0) {
      this.messageService.warn(errMessage);
    }
  }

  changeFile(event) {
    const lengthFile = this.searchValue.blockCode == Division.INDIV ? 5 : 10;
    const uploadedFiles = this.reductionProposalApi?.info?.value?.files;
    const allows = ['.xls', '.xlsx', '.pdf', '.doc', '.docx'];
    if (uploadedFiles?.length + event.target.files?.length > lengthFile) {
      this.messageService.error(`Chỉ được chọn tối đa ${lengthFile} file đính kèm`);
      return;
    }
    let countMaxData = 0;
    let countNotAllow = 0;
    let errMessage = '';
    let maxSizeByte = this.maxSize * 1024 * 1024;
    for (let file of event.target.files) {
      const name = file.name;
      if (file?.size <= maxSizeByte && allows.includes(name.substr(name.lastIndexOf('.')).toLowerCase())) {
        this.upload(file);
      } else if (file?.size > maxSizeByte) {
        countMaxData++;
      } else if (!allows.includes(name.substr(name.lastIndexOf('.')))) {
        countNotAllow++;
      }
    }
    if (countMaxData > 0) {
      errMessage = "Dung lượng tối đa 20MB mỗi file. Có " + countMaxData + " vượt quá dung lượng cho phép\n";
    }
    if (countNotAllow > 0) {
      errMessage = "File không đúng định dạng cho phép (.xls, .xlsx, .pdf, .doc, .docx)";
    }
    if (errMessage.length > 0) {
      this.messageService.warn(errMessage);
    }
  }

  openDownloadFilePopup(inputFile): void {
    // this.curType = 'file';
    inputFile.value = '';
    inputFile.click();
  }

  get fileIdList(): string[] {
    return Object.keys(this.fileInfo);
  }

  upload(file: any) {
    const formData = new FormData();
    formData.append('file', file);
    this.fileService.uploadFile(formData).subscribe(res => {
      const mapFile = {
        "fileId": res,
        "fileName": file.name,
        "type": '',
        "filePath": ""
      };
      this.reductionProposalApi?.info?.value.files.push(mapFile);
    });
  }

  save() {
    return this.reductionProposalApi?.info?.value.files;
  }

  dragleave(ev) {
    this.overFile = false;
  }

  loadData() {
    this.info = this.reductionProposalApi?.info?.value;
    const customer = this.info.customer;

    if (this.isUpdate) {
      this.existed = this.updateObj?.customerCode ? true : false;
    } else {
      this.existed = customer?.customerCode ? true : false;
    }
    this.form.controls.businessActivities.setValue(this.updateObj?.businessActivities);
    this.form.controls.planInformation.setValue(this.updateObj?.planInformation);
    this.form.controls.financialSituation.setValue(this.updateObj?.financialSituation);
    this.form.controls.resultBusiness.setValue(this.updateObj?.resultBusiness);
    this.form.controls.competitiveness.setValue(this.updateObj?.competitiveness);
  }

  openBusinessCustomer() {
    this.isBusinessCustomer = !this.isBusinessCustomer;
  }

  openBusinessDebit() {
    this.isBusinessDebit = !this.isBusinessDebit;
  }

  getCostValue(data, key) {
    return Utils.isNull(_.get(data, key)) ? '---' : formatNumber(_.get(data, key), 'en', '1.0-2');
  }

  openBusinessMobilization() {
    this.isBusinessMobilization = !this.isBusinessMobilization;
  }

  openBusinessGuarantee() {
    this.isBusinessGuarantee = !this.isBusinessGuarantee;
  }

  openBusinessCommercialFinance() {
    this.isBusinessCommercialFinance = !this.isBusinessCommercialFinance;
  }

  openBusinessServiceProducts() {
    this.isBusinessServiceProducts = !this.isBusinessServiceProducts;
  }

  getCreditInformation(params) {
    if (this.searchValue?.type === 0 && this.searchValue?.blockCode === Division.INDIV) {
      this.getCIC(this.formSearch.taxCode);
    } else {
      // Lấy CIC biểu lãi
      this.customerApi.getCreditInformation(params).subscribe(
        (response: any) => {
          this.creditInformation = response;
          this.isLoadingInfo = true;
          this.isLoading = false;
        },
        (e) => {
          // if (e?.error?.description) {
          //   this.messageService.error(e?.error?.description);
          // } else {
          //   this.messageService.error(_.get(this.notificationMessage, 'E001'));
          // }
          this.isLoadingInfo = true;
          this.isLoading = false;
        }
      );
    }
  }

  displayCardValue(tentctd, card) {
    let val = this.findCardValue(tentctd);
    if (val && !isNaN(val)) {
      return this.convertMilion(val);
    }
    if (card && !isNaN(card)) {
      return this.convertMilion(card);
    }
    return '--';
  }

  convertMilion(value) {
    if (value && !isNaN(value)) {
      let num = Number(value.toString()) / 1000000;
      return this.formatCurrency(num);
    }
    return value;
  }

  convertMilionWithUnit(val, unit) {
    let converted = this.convertMilion(val);
    if (Utils.isNotNull(converted)) {
      val = converted + ' ' + unit;
    }
    return val;
  }

  formatCurrencyWithUnit(val, unit) {
    let converted = this.formatCurrency(val);
    if (converted !== '--') {
      val = converted + ' ' + unit;
    }
    return val;
  }

  formatCurrency(element) {
    if (element == 0) {
      return '0';
    }
    if (!element || element === 'NaN' || element === '--') {
      return '--';
    }
    let formated: string = element.toString();
    let first = '';
    let last = '';
    if (formated.includes('.')) {
      first = formated.substr(0, formated.indexOf('.'));
      last = formated.substr(formated.indexOf('.'));
    } else {
      first = formated;
    }
    if (Number(first)) {
      first = Number(first).toLocaleString('en-US', {
        style: 'currency',
        currency: 'VND',
      });
      first = first.substr(1, first.length);
      formated = first + last;
    }
    return formated;
  }

  groupDsxnk() {
    if (this.loadedDoanhSoSNK && this.loadedDoanhSoTttmTaiHT && this.loadedTyLeKhaiThac) {
      this.doanhSoXNKGroup = {};
      this.doanhSoXNKData = [];
      this.doanhSoSNK?.forEach(item => {
        this.setDsxnkValue(item, 'totalTvrAmt');
      });

      this.doanhSoTttmTaiHT?.forEach(item => {
        this.setDsxnkValue(item, 'turnOverTTQT');
      });

      this.tyLeKhaiThac?.forEach(item => {
        this.setDsxnkValue(item, 'rateKT');
      });

      console.log(this.doanhSoXNKGroup);
      console.log(Object.keys(this.doanhSoXNKGroup).sort().reverse());
      Object.keys(this.doanhSoXNKGroup).sort().reverse().map(year => {
        console.log('year: ' + year);

        if (this.doanhSoXNKData.length < 3) {
          this.doanhSoXNKData.push({
            nam: year,
            doanhSoXNK: this.formatCurrency(this.doanhSoXNKGroup[year]?.totalTvrAmt),
            doanhSoTttmTaiMB: this.formatCurrency(this.doanhSoXNKGroup[year]?.turnOverTTQT),
            tyLeKhaiThac: this.formatCurrency(Number(this.doanhSoXNKGroup[year]?.rateKT).toFixed(2))
          });
        }
      });
      console.log(this.doanhSoXNKData);
    }
  }

  setDsxnkValue(item, key) {
    let year = item?.year;
    let val = item[key];
    if (this.doanhSoXNKGroup[year] === undefined) {
      this.doanhSoXNKGroup[year] = {};
    }
    this.doanhSoXNKGroup[year][key] = val;
  }

  getValueV2(item: any){
    return Utils.isStringEmpty(item) ? '---' : item;
  }

  getCostValueV2(data) {
    return Utils.isNull(data) ? '---' : formatNumber(data, 'en', '1.0-2');
  }

  addPrefix(n: number){
    return (n < 10 ? ('0' + n.toString()) : n.toString());
  }

  getDataCustomerRelation(code, branchCode){
    const currentDate = new Date();

    // Lấy tháng t-1
    const monthT = new Date(currentDate.getFullYear(), currentDate.getMonth(), 0, 23, 59, 59);
    const monthNameT = (monthT.getMonth() + 1);
    const yearNameT = monthT.getFullYear().toString();
    this.nameT = 'Năm '.concat(yearNameT).concat(' (Tháng ').concat(this.addPrefix(monthNameT)).concat(')');
    const t1 = formatDate(monthT, 'MM/yyyy', 'en');

    // Lấy tháng t-2
    const monthT1 = new Date(currentDate.getFullYear(), currentDate.getMonth() - 1, 0, 23, 59, 59);
    const t2 = formatDate(monthT1, 'MM/yyyy', 'en');
    const monthNameT1 = (monthT1.getMonth() + 1);
    const yearNameT1 = monthT1.getFullYear().toString();
    this.nameT1 = 'Năm '.concat(yearNameT1).concat(' (Tháng ').concat(this.addPrefix(monthNameT1)).concat(')');

    // Lấy tháng cuối năm của năm t -1
    const monthT2 = new Date(currentDate.getFullYear() - 1, 11, 31);
    const yearT1 = formatDate(monthT2, 'MM/yyyy', 'en');
    const monthNameT2 = (monthT2.getMonth() + 1).toString();
    const yearNameT2 = monthT2.getFullYear().toString();
    this.nameYearT = 'Năm '.concat(yearNameT2);

    const listYear = yearT1.concat(',').concat(t2).concat(',').concat(t1);
    this.customerDetailSmeApi.getDataCustomerRelation(listYear, code, branchCode).subscribe(value => {
      value.forEach(item => {
        if(item.transactionDateFormat === t1){
          this.dataT1 = item;
          console.log(this.dataT1);
        }
        if(item.transactionDateFormat === t2){
          this.dataT2 = item;
          console.log(this.dataT2);
        }
        if(item.transactionDateFormat === yearT1){
          this.dataYearT1 = item;
          console.log(this.dataYearT1);
        }
        this.mapData();
      });
    });

  }

  mapData(){
    // mapdata theo table gửi lên cho BE
    this.relationshipHT = {
      title: [this.nameYearT, this.nameT1, this.nameT],
      customer: {
        statusCustomer: [this.getValueV2(this.dataYearT1.cstStatus), this.getValueV2(this.dataT2.cstStatus), this.getValueV2(this.dataT1?.cstStatus)],
        objectCustomer: [this.dataYearT1.cstTarget, this.dataT2.cstTarget, this.dataT1.cstTarget]
      },
      debt: {
        argtLoanMedmLongAmtLcyYtd: [
          this.getCostValueV2(this.dataYearT1.mdlLoanAmt), this.getCostValueV2(this.dataT2.mdlLoanAmt),this.getCostValueV2(this.dataT1.mdlLoanAmt)
        ],
        argtLoanShrAmtLcyYtd: [
          this.getCostValueV2(this.dataYearT1.shrLoanAmt), this.getCostValueV2(this.dataT2.shrLoanAmt),this.getCostValueV2(this.dataT1.shrLoanAmt)
        ],
        limitAmt: [
          this.getCostValueV2(this.dataYearT1.limitAmt), this.getCostValueV2(this.dataT2.limitAmt),this.getCostValueV2(this.dataT1.limitAmt)
        ],
        limitRate: [
          this.getCostValueV2(this.dataYearT1.limitRate), this.getCostValueV2(this.dataT2.limitRate),this.getCostValueV2(this.dataT1.limitRate)
        ],
        nimBQTHD: [
          this.getCostValueV2(this.dataYearT1.mdlNim), this.getCostValueV2(this.dataT2.mdlNim),this.getCostValueV2(this.dataT1.mdlNim)
        ],
        nimBQNH: [
          this.getCostValueV2(this.dataYearT1.shrNim), this.getCostValueV2(this.dataT2.shrNim),this.getCostValueV2(this.dataT1.shrNim)
        ],
      },
      mobilize: {
        argtDepoAmtLcyYtd: [
          this.getCostValueV2(this.dataYearT1.depAmt), this.getCostValueV2(this.dataT2.depAmt),this.getCostValueV2(this.dataT1.depAmt)
        ],
        argtCasaAmtLcyYtd: [
          this.getCostValueV2(this.dataYearT1.casaAmt), this.getCostValueV2(this.dataT2.casaAmt),this.getCostValueV2(this.dataT1.casaAmt)
        ],
        rateArgt: [
          this.getCostValueV2(this.dataYearT1.casaPct), this.getCostValueV2(this.dataT2.casaPct),this.getCostValueV2(this.dataT1.casaPct)
        ]
      },
      guarantee: {
        argtGuaranteeAmtLcyYtd: [
          this.getCostValueV2(this.dataYearT1.grtAmtYtd), this.getCostValueV2(this.dataT2.grtAmtYtd),this.getCostValueV2(this.dataT1.grtAmtYtd)
        ],
        guaranteeLimitAmt: [
          this.getCostValueV2(this.dataYearT1.grtLmt), this.getCostValueV2(this.dataT2.grtLmt),this.getCostValueV2(this.dataT1.grtLmt)
        ],
        guamanteeLimitRate: [
          this.getCostValueV2(this.dataYearT1.grtRate), this.getCostValueV2(this.dataT2.grtRate),this.getCostValueV2(this.dataT1.grtRate)
        ],
        argtGuaranteeAmtLcyMtd: [
          this.getCostValueV2(this.dataYearT1.grtAmtMtd), this.getCostValueV2(this.dataT2.grtAmtMtd),this.getCostValueV2(this.dataT1.grtAmtMtd)
        ]
      },
      commerce: {
        argtRevenueTfAmtLcyYtd: [
          this.getCostValueV2(this.dataYearT1.tfAmt), this.getCostValueV2(this.dataT2.tfAmt),this.getCostValueV2(this.dataT1.tfAmt)
        ]
      },
      service: {
        nbrOfPrdLvl4: [
          this.getCostValueV2(this.dataYearT1.nbrPd), this.getCostValueV2(this.dataT2.nbrPd),this.getCostValueV2(this.dataT1.nbrPd)
        ],
        frequencyOfRemTransactions: [
          this.getCostValueV2(this.dataYearT1.nbrTxnDep), this.getCostValueV2(this.dataT2.nbrTxnDep),this.getCostValueV2(this.dataT1.nbrTxnDep)
        ]
      }
    };
  }
}
