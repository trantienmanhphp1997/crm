import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  HostBinding,
  Injector,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import * as EventEmitter from 'events';
import { forkJoin, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { CommonCategory, Division, FunctionCode, functionUri, ReductionProposalStatus, Scopes } from 'src/app/core/utils/common-constants';
import { CustomerApi } from 'src/app/pages/customer-360/apis';
import { CustomerDetailApi, CustomerDetailSmeApi } from 'src/app/pages/customer-360/apis/customer.api';
import { ReductionProposalApi } from '../../../api/reduction-proposal.api';
import {
  DetailView,
  ReductionProposalDetail,
  ReductionProposalFileDTOList
} from '../../../model/reduction-proposal.model';
import { Utils } from 'src/app/core/utils/utils';
import { formatNumber } from '@angular/common';
import * as moment from 'moment';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { ConfirmReductionComponent } from '../confirm-reduction/confirm-reduction.component';
import { FileService } from 'src/app/core/services/file.service';
import { MatDialog } from '@angular/material/dialog';
import { ToiConfirmModalComponent } from '../dialog/toi-confirm-modal/toi-confirm-modal.component';
import { FreeDiscountAnnexComponent } from '../annex/annex.component';
import { MODE } from '../annex/define';

@Component({
  selector: 'app-reduction-proposal-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailReductionProposalComponent extends BaseComponent implements OnInit, AfterViewInit {
  id: any;
  public code: any;
  type = 'report';
  param: any;
  @HostBinding('class.app__right-content') appRightContent = true;
  selectedTab = 0;
  dateTime = new Date();
  customer360Fn: any;
  reductionProposalDetail: any;
  info: any;
  listCreditRatings = [];
  customerTypeDisplay: string;
  creditInformation = [];
  show_data: boolean;
  isRm: boolean;
  listFileRection: ReductionProposalFileDTOList[] = [];
  detailView: DetailView;
  listData = [];

  relationTCTD: any;
  relationMB: any;

  classification: string;

  // **** THQH + QHTD ***
  customerTable: any;
  debt: any;
  guarantee: any;
  mobilize: any;
  service: any;
  commerce: any;
  title: any;
  // ---
  statusCustomer: string;
  argtRevenueTfAmtLcyYtd: string;
  argtLoanMedmLongAmtLcyYtd: string;
  argtLoanShrAmtLcyYtd: string;
  limitAmt: string;
  limitRate: string;
  nimBQNH: string;
  nimBQTHD: string;
  argtGuaranteeAmtLcyMtd: string;
  argtGuaranteeAmtLcyYtd: string;
  guamanteeLimitRate: string;
  guaranteeLimitAmt: string;
  argtCasaAmtLcyYtd: string;
  argtDepoAmtLcyYtd: string;
  rateArgt: string;
  frequencyOfRemTransactions: string;
  nbrOfPrdLvl4: string;
  // **** END ***
  dataEarly = [];
  previousYearWarning: string;
  previousMonthWarning: string;
  twoMonthAgoWarning: string;
  show_error_riskManagement = true;
  isBusinessMobilization: boolean;
  isBusinessGuarantee: boolean;
  isBusinessCommercialFinance: boolean;
  isBusinessServiceProducts: boolean;
  isBusinessCustomer: boolean;
  isBusinessDebit: boolean;
  model: any = {};
  action: any;
  listSegment = [];
  modelValue = {
    toi: '',
    dtttrr: ''
  };
  typeDetail: any;
  customerCreditResp: any;
  customer: any;
  params: any;
  idDetail: any;
  status: any;
  public authorization: any;
  isApprove = false;
  isConfirm = false;
  isShowTable = false;
  isRelationMB = false;
  mode = MODE;
  managementView = {
    showToiCamKet: true,
    showDTTTRRCamKet: true,
    showBtnTinhToi: true,
    showThoiGianDanhGia: true,
    showToiHienHuu: true,
    showDTTTRR: true
  };
  customerObject = [];
  customerSegment = [];
  caseSuggesst = [
    { code: 'FEE', displayName: 'Phương án miễn giảm phí' },
    { code: 'INTEREST', displayName: 'Phương án miễn giảm lãi' }
  ];
  scopeApplys = [
    { code: 'ALL', displayName: 'Tất cả các phương án' },
    { code: 'OPTION_CODE', displayName: 'Mã phương án hạn mức' },
    { code: 'LD_CODE', displayName: 'Mã LD' },
    { code: 'MD_CODE', displayName: 'Mã MD' },
    { code: 'UNKNOWN', displayName: 'Chưa xác định mã phương án' }
  ];
  AllTypeFee = [
    { id: 1, displayName: 'Biểu lãi suất thông thường' },
    { id: 2, displayName: 'Biểu lãi suất ưu đãi (AIRS)' },
    { id: 4, displayName: 'Biểu lãi suất linh hoạt' },
    { id: 6, displayName: 'Biểu lãi suất thông thường CIB' }
  ];
  confirmException = [
    {
      code: 1, name: 'TOI (%)', suggest: null, desc: '', placeholderPropose: 'Nhập TOI (%)'
      , placeholderBasic: 'Nhập nội dung', length: 15, maxDigi: 2, minDigi: 2, type: 3
    },
    {
      code: 2, name: 'Đối tượng KH', suggest: '', desc: '', optionalCustomer: this.customerObject
      , type: 1, placeholderPropose: 'Chọn đối tượng KH', placeholderBasic: 'Nhập nội dung'
    },
    {
      code: 3, name: 'Phân khúc', suggest: '', desc: '', optionalCustomer: this.customerSegment
      , type: 1, placeholderPropose: 'Chọn phân khúc', placeholderBasic: 'Nhập nội dung'
    },
    {
      code: 4, name: 'Tiêu chí khác', suggest: '', desc: '', placeholderPropose: 'Nhập tiêu chí khác'
      , placeholderBasic: 'Nhập nội dung', type: 2
    }
  ];

  customerTypes = [
    { code: 'OLD', name: 'Khách hàng cũ' },
    { code: 'NEW', name: 'Khách hàng mới' }
  ];

  reductionProposalTranslate: any;
  typeInterestDetail: any;
  @Output() dataReduction = new EventEmitter();
  @ViewChild(ConfirmReductionComponent) confirmReductionComponent: ConfirmReductionComponent;
  @ViewChild(FreeDiscountAnnexComponent) freeDiscountAnnexComponent: FreeDiscountAnnexComponent;


  form = this.fb.group({
    businessActivities: [''],
    scopeApply: [''],
    planInformation: [''],
    financialSituation: [''],
    competitiveness: [''],
    caseSuggest: [''],
    lastApprove: [''],
    typeInterest: ['']
  });

  constructor(
    injector: Injector,
    private reductionProposal: ReductionProposalApi,
    private customerApi: CustomerApi,
    private categoryService: CategoryService,
    private fileService: FileService,
    public dialog: MatDialog,
    private customerDetailApi: CustomerDetailApi,
    private dtc: ChangeDetectorRef,
    private customerDetailSmeApi: CustomerDetailSmeApi) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REDUCTION_PROPOSAL}`);
    this.customer360Fn = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.params = this.route.snapshot.queryParams;
    if (this.params) {
      this.idDetail = this.params.id;
    }
    this.action = this.params.action || '';
    if (this.params && this.params.action == 'approved' || this.params.action == 'confirm') {
      this.selectedTab = 3;
      this.isConfirm = true;
    }
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.info = this.reductionProposal?.info?.value;
    this.categoryService.getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW).pipe(catchError((e) => of(undefined))).subscribe(res => {
        this.isRm = _.isEmpty(res);
      }
    );
    this.reductionProposal.detailReductionProposal(this.idDetail ? this.idDetail : this.id).subscribe(res => {
      if (res) {
        this.typeDetail = res?.type
        this.reductionProposalDetail = res;
        this.typeInterestDetail = res.interestType;
        this.freeDiscountAnnexComponent.reductionProposalDetail = res;
        this.freeDiscountAnnexComponent.form.patchValue(this.reductionProposalDetail);
        this.checkViewFEE();
        this.freeDiscountAnnexComponent.loadDataByTaxCode(this.reductionProposalDetail.taxCode || this.reductionProposalDetail?.businessRegistrationNumber);
        this.freeDiscountAnnexComponent.getCreditInformation({
          identifiedNumber: res.businessRegistrationNumber,
          division: res.blockCode
        });
        this.freeDiscountAnnexComponent.getDataCustomerRelation(this.reductionProposalDetail.customerCode, this.reductionProposalDetail?.branchCodeCustomer);
        this.code = this.reductionProposalDetail.code;
        this.status = this.reductionProposalDetail.status;

        if ('IN_PROGRESS' === this.status && this.reductionProposalDetail?.customerType) {
          this.reductionProposalDetail.loai_KH = this.reductionProposalDetail?.customerType;
        } else {
          if (this.reductionProposalDetail?.customerType) {
            this.reductionProposalDetail.loai_KH = this.customerTypes.find(item => item.code == this.reductionProposalDetail?.customerType).name;
          }
        }

        const toi = this.reductionProposalDetail.reductionProposalTOIDTO;
        this.initTOI(toi);
        if ('WAITING_CONFIRM' === this.status
          || 'CONFIRMED' === this.status
          || 'CBQL_REFUSED_CONFIRM' === this.status) {
          this.isConfirm = true;
        }

        const body = {
          customerCode: '',
          taxCode: this.reductionProposalDetail?.taxCode
        }

        const paramScope1 = {
          customerCode: '',
          businessRegistrationNumber: this.reductionProposalDetail?.businessRegistrationNumber,
          loanId: ''
        }

        // TH khách hàng ko có customerCode
        if (!this.reductionProposalDetail?.customerCode) {
          forkJoin([
            this.reductionProposal.getScopeReduction(paramScope1).pipe(catchError(() => of(undefined))),
            this.reductionProposal.findPotentialCustomer(body).pipe(catchError(() => of(undefined))),
            this.categoryService.getCommonCategory(CommonCategory.SEGMENT_CUSTOMER_CONFIG).pipe(catchError(() => of(undefined)))
          ]).subscribe(([scopeReduction, potentialCustomer, segmentResp]) => {
            // Xếp hạng tín dụng
            if (!_.isEmpty(scopeReduction)) {
              this.info.customer.scoreValue = scopeReduction.ketQuaXhtd;
            }
            if (!_.isEmpty(potentialCustomer)) {
              this.info.customer.customerName = potentialCustomer.fullName;
            }

            if (segmentResp.content) {
              const listSegment = [];
              Object.keys(segmentResp.content).forEach((key) => {
                listSegment.push({ code: segmentResp.content[key].code, name: segmentResp.content[key].name });
              });
              this.info.customer.segment = listSegment.find(item => item.code == this.reductionProposalDetail.customerSegment).name;
            }

            this.info.customer.taxCode = this.reductionProposalDetail?.taxCode;
          }, (e) => {
            this.messageService.error(e.error.description);
            this.isLoading = false;
          });
        }

        // Phạm vi hoạt động
        let scopeType;
        switch (this.reductionProposalDetail.scopeType) {
          case 0:
            scopeType = 'ALL';
            break;
          case 1:
            scopeType = 'OPTION_CODE';
            break;
          case 2:
            scopeType = 'LD_CODE';
            break;
          case 3:
            scopeType = 'UNKNOWN';
            break;
          case 4:
            scopeType = 'MD_CODE';
            break;
        }

        // Thẩm quyền phê duyệt
        this.authorization = this.reductionProposalDetail.reductionProposalAuthorizationDTOList;

        const sorted: any[] = this.authorization?.filter(item => item.type == 1).sort((a, b) => {
          return b.priority - a.priority;
        });
        if (sorted && sorted.length > 0) {
          this.reductionProposalDetail.lastApprove = sorted[0]?.titleCategory + ' - ' + sorted[0]?.fullName;
        }

        this.form.controls.caseSuggest.setValue(this.reductionProposalDetail?.type === 1 ? 'INTEREST' : 'FEE');
        this.form.controls.caseSuggest.disable();
        this.form.controls.scopeApply.setValue(scopeType);
        this.form.controls.scopeApply.disable();
        this.form.controls.typeInterest.setValue(this.reductionProposalDetail.interestType);
        this.form.controls.typeInterest.disable();

        this.translate.get(['reductionProposal']).subscribe((result) => {
          this.reductionProposalTranslate = _.get(result, 'reductionProposal');
        });
        if ('IN_PROGRESS' === this.status) { // *** ĐANG SOẠN THẢO ***
          const searchSmeBody = {
            pageNumber: 0,
            pageSize: 10,
            rsId: this.customer360Fn?.rsId,
            scope: 'VIEW',
            customerType: '',
            search: this.reductionProposalDetail?.customerCode,
            searchFastType: 'CUSTOMER_CODE',
            manageType: 1,
            customerTypeSale: 1
          };
          let forkJoinReq = [
            this.customerApi.searchSme(searchSmeBody).pipe(catchError(() => of(undefined))),
            this.customerDetailSmeApi.get(this.reductionProposalDetail.customerCode).pipe(catchError(() => of(undefined)))
          ];
          if (this.reductionProposalDetail.blockCode === Division.INDIV) {
            this.managementView.showBtnTinhToi = false;
            forkJoinReq = [
              this.customerApi.search(searchSmeBody).pipe(catchError(() => of(undefined))),
              this.customerDetailApi.getByCode(this.reductionProposalDetail.customerCode).pipe(catchError(() => of(undefined)))
            ];
          }
          forkJoin([
            ...[
              this.customerDetailSmeApi.getDataCreditRatings(this.reductionProposalDetail.customerCode).pipe(catchError(() => of(undefined))),
              this.categoryService.getCommonCategory(CommonCategory.SEGMENT_CUSTOMER_CONFIG).pipe(catchError(() => of(undefined))),
              this.customerDetailSmeApi.getClassification({ customerCode: this.reductionProposalDetail?.customerCode }).pipe(catchError(() => of(undefined)))],
            ...forkJoinReq
          ]).subscribe(([creditRatings, categorySegment, classificationResp, customerInfo, customerDetails]) => {
            console.log(customerDetails);
            this.info.customer.classification = classificationResp;
            if (categorySegment.content) {
              Object.keys(categorySegment.content).forEach((key) => {
                this.listSegment.push({
                  code: categorySegment.content[key].code,
                  name: categorySegment.content[key].name
                });
              });
            }

            if (!_.isEmpty(customerInfo) && this.reductionProposalDetail?.customerCode) {
              const info = customerInfo && customerInfo.length > 0 ? customerInfo[0] : customerDetails.customerInfo ? customerDetails.customerInfo : {};
              this.info.customer.classification = info?.classification;
              this.info.customer.customerCode = info?.customerCode;
              this.info.customer.customerName = info?.customerName || info?.fullName;
              this.info.customer.taxCode = info?.taxCode;
              this.info.customer.segment = info?.segment || info?.customerSegment;
              // this.reductionProposalDetail.taxCode = this.info.customer.taxCode;
              this.reductionProposalDetail.businessRegistrationNumber = info?.businessRegistrationNumber;
            }

            if (this.reductionProposalDetail?.customerCode && this.reductionProposalDetail.blockCode != Division.INDIV) {
              const reqBody = {
                customerCode: this.reductionProposalDetail?.customerCode.trim(),
                taxCode: '',
                blockCode: this.reductionProposalDetail.blockCode
              };
              this.reductionProposal.findExistingCustomer(reqBody).pipe(
                finalize(() => {
                })
              ).subscribe((data) => {
                if (!data.code) {
                  this.info.customer.customerCode = data.customerCode;
                  this.info.customer.customerName = data.customerName;
                  this.info.customer.taxCode = data.taxCode;
                  this.info.customer.customerTypeMerge = data?.customerTypeMerge;
                  this.info.customer.businessRegistrationNumber = data?.businessRegistrationNumber;
                } else {
                  this.messageService.warn(_.get(this.notificationMessage, 'customerNotExist'));
                }
                this.isLoading = false;
              }, (err) => {
                this.isLoading = false;
                this.messageService.warn(err?.error?.description);
              });

            }

            const khdn = ['SME', 'CIB', 'FI', 'DVC'];
            this.info.customer.isKhdn = khdn.includes(this.info.customer?.customerTypeMerge);
            const paramScope2 = {
              customerCode: this.reductionProposalDetail?.customerCode || '',
              businessRegistrationNumber: this.reductionProposalDetail?.businessRegistrationNumber || '',
              loanId: ''
            };
            // Xếp hạng tín dụng
            this.reductionProposal.getScopeReduction(paramScope2).subscribe(temp => {
              if (temp) {
                this.info.customer.scoreValue = temp.ketQuaXhtd;
              }
            });
            // Api lấy loại Kh, DTTTRR, TOI hiện hữu
            this.reductionProposal.getToiExisting(this.reductionProposalDetail?.customerCode).subscribe(toiData => {
              if (toiData) {
                // Xếp hạng tín dụng
                if(toiData.loai_KH){
                  this.reductionProposalDetail.loai_KH = toiData.loai_KH;
                } else {
                  this.reductionProposalDetail.loai_KH = 'Khách hàng mới';
                }
                this.reductionProposalDetail.dtttrrExisting = toiData.dtttrr;
                this.reductionProposalDetail.toiExisting = toiData.toi;
                // KH cũ ẩn TOI cam kết, DTTTRR cam kết, Thời gian áp dụng, button tính TOI

                if (this.reductionProposalDetail.type == 1) {
                  if ('Khach hang cu' === toiData.loai_KH) {
                    this.reductionProposalDetail.loai_KH = 'Khách hàng cũ';
                    this.managementView.showToiCamKet = false;
                    this.managementView.showDTTTRRCamKet = false;
                    this.managementView.showBtnTinhToi = false;
                  }
                }
              } else {
                this.reductionProposalDetail.loai_KH = 'Khách hàng mới';
              }
            });
            if (!_.isEmpty(creditRatings)) {
              const dataMapCreditRatings = creditRatings.map((item) => {
                item.loanCodeDTOList.forEach((element) => {
                  element.loanId = item.loanId;
                });
                return item.loanCodeDTOList;
              });
              const listCreditRatings = [];
              dataMapCreditRatings.forEach((item) => {
                item.forEach((i) => {
                  listCreditRatings.push(i);
                });
              });
              this.listCreditRatings = listCreditRatings || [];
              this.listCreditRatings = _.orderBy(this.listCreditRatings, [(obj) => new Date(obj.scoreDate)], ['desc']);
            }
            // Bảng Thông tin giảm lãi
            this.initInterestData(this.reductionProposalDetail);
            this.isLoading = false;
          }, (e) => {
            this.messageService.error(e.error.description);
            this.isLoading = false;
          });

          // KH chưa có customerCode set loại KH là mới
          if (!this.reductionProposalDetail.customerCode) {
            if (this.reductionProposalDetail?.customerType) {
              this.reductionProposalDetail.loai_KH = this.customerTypes.find(item => item.code == this.reductionProposalDetail?.customerType).name;
            } else {
              this.reductionProposalDetail.loai_KH = 'Khách hàng mới';
              this.reductionProposalDetail?.customerSegment;
            }
            this.info.customer.segment = this.reductionProposalDetail?.segment;
            this.info.customer.taxCode = this.reductionProposalDetail?.taxCode;
          }
          if (this.reductionProposalDetail?.customerCode) {
            this.getCustomerCredit();
          } else {
            this.customerCreditResp = { customerType: 'Phi tín dụng' };
          }
          // this.isLoading = false;
        } else { // Trạng thái còn lại
          if (!this.info.customer.scoreValue) {
            this.info.customer.scoreValue = this.reductionProposalDetail?.creditRanking ? this.reductionProposalDetail?.creditRanking : '--';
          }
          this.info.customer.classification = this.reductionProposalDetail?.classification;
          this.info.customer.customerCode = this.reductionProposalDetail?.customerCode;
          this.info.customer.customerName = this.reductionProposalDetail?.customerName ? this.reductionProposalDetail?.customerName : '--';
          this.info.customer.taxCode = this.reductionProposalDetail.taxCode;
          this.freeDiscountAnnexComponent.loadDataByTaxCode(this.reductionProposalDetail?.taxCode || this.reductionProposalDetail?.businessRegistrationNumber);
          if (this.reductionProposalDetail?.customerSegment) {
            this.categoryService.getCommonCategory(CommonCategory.SEGMENT_CUSTOMER_CONFIG).subscribe(segment => {
              if (segment.content) {
                Object.keys(segment.content).forEach((key) => {
                  this.listSegment.push({ code: segment.content[key].code, name: segment.content[key].name });
                });
                this.info.customer.segment = this.listSegment.find(item => item.code == this.reductionProposalDetail.customerSegment).name;
              }
            });
          }
          this.info.customer.scoreValue = this.reductionProposalDetail?.creditRanking ? this.reductionProposalDetail?.creditRanking : '--';
          this.initInterestData(this.reductionProposalDetail);

          if (this.reductionProposalDetail.type == 1) {
            if ('NEW' === this.reductionProposalDetail.customerType) {
              this.managementView.showToiCamKet = true;
              this.managementView.showDTTTRRCamKet = true;
              this.managementView.showBtnTinhToi = true;
            }
            if ('OLD' === this.reductionProposalDetail.customerType) {
              this.managementView.showToiCamKet = false;
              this.managementView.showDTTTRRCamKet = false;
              this.managementView.showBtnTinhToi = false;
            }
          } else {
            console.log('this.info.customer : ', this.reductionProposalDetail);
            const displayConfirmInfo = this.reductionProposalDetail?.blockCode === 'FI' || this.reductionProposalDetail?.blockCode === 'CIB' || this.reductionProposalDetail?.customerStructure !== 'Phi tín dụng';
            const isKhdn = ['SME', 'CIB', 'FI', 'DVC'].includes(this.reductionProposalDetail?.blockCode);
            if (displayConfirmInfo && isKhdn) {
              this.managementView.showBtnTinhToi = true;
              this.managementView.showDTTTRRCamKet = true;
              this.managementView.showToiCamKet = true;
            } else {
              this.managementView.showBtnTinhToi = false;
              this.managementView.showDTTTRRCamKet = false;
              this.managementView.showToiCamKet = false;
            }

          }

          this.customerCreditResp = { customerType: this.reductionProposalDetail?.customerStructure };

          this.isLoading = false;
        }

        if (this.reductionProposalDetail.customerCode) {
          this.customerDetailSmeApi.get(this.reductionProposalDetail.customerCode).subscribe(res => {
            if (res) {
              this.reductionProposalDetail.mbRelationship = this.generateRelationshipTimeWithMB(res);
            }
          });
        }

      }
    }, (e) => {
      this.messageService.error(e.error.description);
      this.isLoading = false;
    });
  }

  ngAfterViewInit() {
    this.checkAbilityInterest();
    this.dtc.detectChanges();
  }


  checkBrand(branchCode) {
    if (branchCode != this.currUser.branch) {
      return false;
    }
    return true;
  }

  checkViewFEE() {
    if (this.reductionProposalDetail.blockCode == Division.FI) {
      return;
    }
    if (this.reductionProposalDetail.type == 0) {
      const customerStructure = this.reductionProposalDetail.customerStructure;
      let managementViewValue;
      const checkBranch = this.checkBrand(this.reductionProposalDetail.branchCodeCustomer);
      if (customerStructure != 'Phi tín dụng') {
        if (!checkBranch) {
          this.managementView.showToiHienHuu = false;
          this.managementView.showDTTTRR = false;
          return;
        }
        managementViewValue = true;
      } else {
        if (checkBranch) {
          this.managementView.showBtnTinhToi = false;
          this.managementView.showToiCamKet = false;
          this.managementView.showDTTTRRCamKet = false;
          this.managementView.showThoiGianDanhGia = false;
          return;
        }
        managementViewValue = false;
      }
      Object.keys(this.managementView).forEach(v => this.managementView[v] = managementViewValue);

      // Hiện toi, dtttrr hiện hữu với KH tiềm năng
      if (!this.reductionProposalDetail?.customerCode) {
        this.managementView.showToiHienHuu = true;
        this.managementView.showDTTTRR = true;
      }
      // Khách hàng tín dụng của CIB thì hiển thị toi & dtttrr cam kết
      if (customerStructure === 'Phi tín dụng' && this.reductionProposalDetail.blockCode === Division.CIB) {
        this.managementView.showBtnTinhToi = true;
        this.managementView.showToiCamKet = true;
        this.managementView.showDTTTRRCamKet = true;
      }
      // SME. Đối với KH tín dụng, nếu không trả gia trị TOI hiện hữu thì hiển thị TOI cam kết và nút tính TOI
      if (customerStructure !== 'Phi tín dụng' && this.reductionProposalDetail.blockCode === Division.SME && Utils.isNull(this.reductionProposalDetail.toiExisting)) {
        this.managementView.showBtnTinhToi = true;
        this.managementView.showToiCamKet = true;
        this.managementView.showDTTTRRCamKet = true;
      } else if (customerStructure !== 'Phi tín dụng' && this.reductionProposalDetail.blockCode === Division.SME && Utils.isNotNull(this.reductionProposalDetail.toiExisting)) {
        this.managementView.showBtnTinhToi = false;
        this.managementView.showToiCamKet = false;
        this.managementView.showDTTTRRCamKet = false;
      }
    }
  }

  displayWithUnit(val, unit1, unit2) {
    let str = '--';
    const unit = unit1 == '%' ? unit2 : unit1;
    if (!Utils.isNull(val)) {
      str = val + (unit ? '/' + unit : '');
    }
    return str;
  }

  getCustomerCredit(): any {
    this.reductionProposal.getCustomerCredit({
      cif: this.reductionProposalDetail.customerCode || '',
      taxId: '' || '',
      nationalId: this.reductionProposalDetail.blockCode === Division.INDIV ? this.reductionProposalDetail.taxCode || '' : '',
      lob: this.reductionProposalDetail.blockCode
    }).subscribe((customerCreditResp: any) => {
      this.customerCreditResp = customerCreditResp.data;
    }, (err) => {
    });
  }

  generateRelationshipTimeWithMB(item) {
    const date = _.get(item, 'customerInfo.openCodeDate');
    if (Utils.isStringNotEmpty(date)) {
      const totalMonth = moment().diff(moment(date, 'DD/MM/YYYY'), 'months');
      return totalMonth > 12
        ? `${Math.floor(totalMonth / 12)} ${_.get(this.fields, 'year')}`
        : `${totalMonth} ${_.get(this.fields, 'month')}`;
    } else {
      return `0 ${_.get(this.fields, 'year')}`;
    }
  }


  back() {
    // check tabindex
    // if (this.selectedTab > 0) {
    //   this.selectedTab--;
    // } else {
    this.router.navigate([functionUri.reduction_proposal], { state: this.prop ? this.prop : this.state });
    // }
  }

  updateType(event) {
    const type = event.index === 0 ? 'report' : 'appendix';
    this.type = type;
    if (this.type === 'appendix') {
      // this.updateForm(this.reductionProposalDetail);
    }
    if (this.type === 'report') {
      this.form.controls.caseSuggest.setValue(this.reductionProposalDetail?.type === 1 ? 'INTEREST' : 'FEE');
    }
  }

  // *** TAB PHỤ LỤC ***
  updateForm(data) {
    this.isLoading = true;
    const item = {
      businessActivities: data?.businessActivities ? data?.businessActivities : '',
      planInformation: data?.planInformation ? data?.planInformation : '',
      financialSituation: data?.financialSituation ? data?.financialSituation : '',
      competitiveness: data?.competitiveness ? data?.competitiveness : '',
      scopeApply: Utils.isNotNull(data?.scopeType) ? this.scopeApplys[data?.scopeType]?.code : '',
      caseSuggest: data?.caseSuggest ? data?.caseSuggest : '',
      lastApprove: data?.lastApprove ? data?.lastApprove : ''
    };

    this.form.setValue(item);

    const paramCreditInfo = {
      identifiedNumber: data?.businessRegistrationNumber
    };

    this.listFileRection = data.reductionProposalFileDTOList;

    this.form.controls.businessActivities.disable();
    this.form.controls.planInformation.disable();
    this.form.controls.financialSituation.disable();
    this.form.controls.competitiveness.disable();

    if ('IN_PROGRESS' === this.status) {
      this.getDataEarlyWarningModel();

      const toDay = new Date();
      this.customerApi.getCreditInformation(paramCreditInfo).subscribe(res => {
        if (res) {
          this.creditInformation = res?.customerCICDTOS;
          if (this.creditInformation) {
            this.show_data = Utils.isArrayEmpty(this.creditInformation) ? false : true;
            const date = moment(this.creditInformation[0].dsnapShotDt, 'DD-MMM-YY').toDate();
            // warningDay.setDate(date.getDate() + 30);
            date.setDate(date.getDate() + 30);
            if (date < toDay) {
              this.isShowTable = true;
            }
            this.isLoading = false;
          }
        }
      }, (e) => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      });
    } else {
      // Tình hình quan hệ tín dụng
      if (data?.jsonRelationTCTD) {
        this.relationTCTD = JSON.parse(data?.jsonRelationTCTD);
        if (this.relationTCTD?.customerCICDTOS) {
          this.creditInformation = this.relationTCTD?.customerCICDTOS;
          if (this.creditInformation) {
            this.show_data = Utils.isArrayEmpty(this.creditInformation) ? false : true;
            const date = moment(this.creditInformation[0].dsnapShotDt, 'DD-MMM-YY').toDate();
            const toDay = new Date();
            date.setDate(date.getDate() + 30);
            if (date < toDay) {
              this.isShowTable = true;
            }
            this.isRelationMB = true;
          }
        }
      }

      if (data?.jsonRelationMB) {
        this.relationMB = JSON.parse(data?.jsonRelationMB);

        if (this.relationMB) {
          // Title
          this.title = this.relationMB.title;

          // A.Khách hàng
          this.classification = this.relationMB.customer.objectCustomer;
          this.statusCustomer = this.relationMB.customer.statusCustomer;
          // B.Dư nợ
          this.argtLoanMedmLongAmtLcyYtd = this.relationMB.debt.argtLoanMedmLongAmtLcyYtd;
          this.argtLoanShrAmtLcyYtd = this.relationMB.debt.argtLoanShrAmtLcyYtd;
          this.limitAmt = this.relationMB.debt.limitAmt;
          this.limitRate = this.relationMB.debt.limitRate;
          this.nimBQNH = this.relationMB.debt.nimBQNH;
          this.nimBQTHD = this.relationMB.debt.nimBQTHD;
          // C.Huy động mobilize
          this.argtDepoAmtLcyYtd = this.relationMB.mobilize.argtDepoAmtLcyYtd;
          this.argtCasaAmtLcyYtd = this.relationMB.mobilize.argtCasaAmtLcyYtd;
          this.rateArgt = this.relationMB.mobilize.rateArgt;
          // D.Bảo lãnh guarantee
          this.argtGuaranteeAmtLcyMtd = this.relationMB.guarantee.argtGuaranteeAmtLcyMtd;
          this.argtGuaranteeAmtLcyYtd = this.relationMB.guarantee.argtGuaranteeAmtLcyYtd;
          this.guamanteeLimitRate = this.relationMB.guarantee.guamanteeLimitRate;
          this.guaranteeLimitAmt = this.relationMB.guarantee.guaranteeLimitAmt;
          // E.Tài sản thương mại
          this.argtRevenueTfAmtLcyYtd = this.relationMB.commerce.argtRevenueTfAmtLcyYtd;
          // F.Sản phẩm dịch vụ
          this.frequencyOfRemTransactions = this.relationMB.service.frequencyOfRemTransactions;
          this.nbrOfPrdLvl4 = this.relationMB.service.nbrOfPrdLvl4;
          this.isRelationMB = true;
          this.show_error_riskManagement = false;
        } else {
          this.show_error_riskManagement = true;
        }
      } else {
        this.show_error_riskManagement = true;
        this.isRelationMB = false;
      }
      this.isLoading = false;
    }
    this.isLoading = false;
  }

  getValue(item, key) {
    return Utils.isNull(_.get(item, key)) ? '--' : _.get(item, key);
  }

  getCostValue(data, key) {
    return Utils.isNull(_.get(data, key)) ? '--' : formatNumber(_.get(data, key), 'en', '0.0-2');
  }

  getTruncateValue(item, key, limit) {
    return Utils.isStringNotEmpty(_.get(item, key)) ? Utils.truncate(_.get(item, key), limit) : '--';
  }

  getSubstringValue(item, key) {
    return Utils.isNull(_.get(item, key)) ? '--' : Utils.subString(_.get(item, key));
  }

  convertDateData(str) {
    const date = moment(str, 'DD-MMM-YY').toDate();
    return date;
  }


  confirm() {
    this.confirmReductionComponent.confirm()
  }

  CBQLconfirm(idea: boolean) {
    this.confirmReductionComponent.CBQLconfirm(idea);
  }

  isTabManager(): boolean {
    return [ReductionProposalStatus.InProgress].includes(this.status);
  }

  downloadFile(item) {
    if (item.fileId) {
      this.isLoading = true;
      this.fileService.downloadFile(item.fileId, item.fileName).subscribe((res) => {
        this.isLoading = false;
        if (!res) {
          this.messageService.error(this.notificationMessage.error);
        }
      });
    }
  }

  initInterestData(interestData) {
    this.listData = interestData?.reductionProposalDetailsDTOList.map(item => {
      return {
        data: {
          code: interestData?.scopeType === 1 ? item?.planCode : interestData?.scopeType === 2 ? item?.ldCode : '',
          productCode: item?.interestProposedAmplitude,
          name: item?.itemName,
          currency: item?.interestUnit,
          adjustmentPeriod: item?.interestAdjustPeriod,
          period: item?.interestLoanPeriod,
          referenceInterestRate: item?.interestReference,
          baseMargin: item?.interestAmplitude,
          maximumMarginReduction: item?.interestMaxReduction,
          loanRate: item?.interestValue,
          marginSuggest: item?.interestProposedAmplitude,
          loanSuggest: item?.interestProposedValue,
          reductPercent: ((Number(item?.interestAmplitude) - Number(item?.interestProposedAmplitude)) / Number(item?.interestAmplitude) * 100).toFixed(2),
          minInterestRate: item?.minInterestRate,
          minInterestRateOffer: item?.minInterestRateOffer,
          rateMinimum: ((Number(item?.minInterestRate) - Number(item?.minInterestRateOffer)) / Number(item?.minInterestRate) * 100).toFixed(2),
          planCode: item?.planCode,
          mdCode: item?.mdCode,
          ldCode: item?.ldCode

        }
      };
    });
  }

  getDataEarlyWarningModel() {
    this.isLoading = true;
    if (this.reductionProposalDetail?.customerCode) {
      forkJoin([
        this.customerDetailSmeApi.get(this.reductionProposalDetail?.customerCode).pipe(catchError(() => of(undefined))),
        this.customerDetailSmeApi.getClassification({ customerCode: this.reductionProposalDetail?.customerCode }).pipe(catchError(() => of(undefined))),
        this.customerDetailSmeApi.getDataEarlyWarningModel(this.reductionProposalDetail?.customerCode).pipe(catchError(() => of(undefined)))
      ]).subscribe(([itemCustomer, classification, listEarlyWarning]) => {
        this.model = itemCustomer;
        this.classification = _.isEmpty(classification) ? '' : classification;
        if (!_.isEmpty(listEarlyWarning)) {
          this.isLoading = false;
          this.dataEarly = listEarlyWarning;
          this.previousYearWarning = `${_.get(this.fields, 'year')} ${moment(
            _.get(this.dataEarly, 'dataY1.dataDate')
          ).format('YYYY')}`;
          this.previousMonthWarning = `${_.get(this.fields, 'year')} ${moment(
            _.get(this.dataEarly, 'dataT1.dataDate')
          ).format('YYYY')} (${_.get(this.fields, 'month')} ${moment(_.get(this.dataEarly, 'dataT1.dataDate')).format(
            'MM'
          )})`;

          this.twoMonthAgoWarning = `${_.get(this.fields, 'year')} ${moment(
            _.get(this.dataEarly, 'dataT2.dataDate')
          ).format('YYYY')} (${_.get(this.fields, 'month')} ${moment(_.get(this.dataEarly, 'dataT2.dataDate')).format(
            'MM'
          )})`;
          if (
            _.isEmpty(_.get(this.dataEarly, 'dataY1.dataDate')) ||
            _.isEmpty(_.get(this.dataEarly, 'dataT2.dataDate'))
          ) {
            const dataDateT1 = _.get(this.dataEarly, 'dataT1.dataDate').split('-');
            if (+dataDateT1[1] === 1) {
              this.twoMonthAgoWarning =
                `${_.get(this.fields, 'year')} ` + (dataDateT1[0] - 1) + `(${_.get(this.fields, 'month')} 12)`;
              this.previousYearWarning = `${_.get(this.fields, 'year')} ` + (dataDateT1[0] - 1);
            } else {
              this.twoMonthAgoWarning =
                `${_.get(this.fields, 'year')} ` +
                dataDateT1[0] +
                `(${_.get(this.fields, 'month')} ` +
                moment((dataDateT1[1] - 1).toString()).format('MM') +
                ')';
              this.previousYearWarning = `${_.get(this.fields, 'year')} ` + (dataDateT1[0] - 1);
            }
          }
          this.show_error_riskManagement = false;
        } else {
          const dataDateT1 = +moment().format('MM');
          if (+moment().format('MM') === 1) {
            this.previousYearWarning = `${_.get(this.fields, 'year')} ` + (+moment().format('yyyy') - 2);
            this.previousMonthWarning =
              `${_.get(this.fields, 'year')}` +
              (+moment().format('YYYY') - 1) +
              `(${_.get(this.fields, 'month')} ` +
              '12)';
            this.twoMonthAgoWarning =
              `${_.get(this.fields, 'year')}` +
              (+moment().format('YYYY') - 1) +
              `(${_.get(this.fields, 'month')} ` +
              '11)';
          } else {
            this.previousYearWarning = `${_.get(this.fields, 'year')} ` + (+moment().format('yyyy') - 1);
            this.previousMonthWarning =
              `${_.get(this.fields, 'year')} ${moment().format('YYYY')} (${_.get(this.fields, 'month')} ` +
              moment((dataDateT1 - 1).toString()).format('MM') +
              ')';
            this.twoMonthAgoWarning =
              `${_.get(this.fields, 'year')} ${moment().format('YYYY')} (${_.get(this.fields, 'month')} ` +
              moment((dataDateT1 - 2).toString()).format('MM') +
              ')';
          }
          this.show_error_riskManagement = true;
          this.isLoading = false;
        }
      });
    }
  }

  openBusinessMobilization() {
    this.isBusinessMobilization = !this.isBusinessMobilization;
  }

  openBusinessGuarantee() {
    this.isBusinessGuarantee = !this.isBusinessGuarantee;
  }

  openBusinessCommercialFinance() {
    this.isBusinessCommercialFinance = !this.isBusinessCommercialFinance;
  }

  openBusinessServiceProducts() {
    this.isBusinessServiceProducts = !this.isBusinessServiceProducts;
  }

  openBusinessCustomer() {
    this.isBusinessCustomer = !this.isBusinessCustomer;
  }

  openBusinessDebit() {
    this.isBusinessDebit = !this.isBusinessDebit;
  }

  initTOI(toi) {
    this.info.toi = {
      duNoNganHanBq: toi?.dunoBqSd,
      duNoNganHanBqUnit: toi?.dunoBqNim,
      duNoTdhBq: toi?.dunoTdhBqSd,
      duNoTdhBqUnit: toi?.dunoTdhBqNim,
      coKyHanBq: toi?.ckhBqSd,
      coKyHanBqUnit: toi?.ckhBqNim,
      khongKyHanBq: toi?.kkhBqSd,
      khongKyHanBqUnit: toi?.kkhBqNim,
      thuBl: toi?.tblDsSd,
      thuBlUnit: toi?.tblNim,
      thuTTQT: toi?.tttqtDsSd,
      thuTTQTUnit: toi?.tttqtMpqd,
      thuFx: toi?.tfxDsSd,
      thuFxUnit: toi?.tfxMpqd,
      thuBanca: toi?.thuBancaDsSd,
      thuKhac: toi?.thuKhacDsSd,
      dtttrr: this.reductionProposalDetail?.dtttrrConfirm,
      toi: this.reductionProposalDetail?.toiConfirm,
      // Hieu qua
      ckhBqHq: toi?.ckhBqHq,
      dunoBqHq: toi?.dunoBqHq,
      dunoTdhBqHq: toi?.dunoTdhBqHq,
      kkhBqHq: toi?.kkhBqHq,
      tblHq: toi?.tblHq,
      tfxHq: toi?.tfxHq,
      thuBanCaHq: toi?.thuBanCaHq,
      thuKhacHq: toi?.thuKhacHq,
      tttqtHq: toi?.tttqtHq
    };
    this.reductionProposal.info.next(this.info);
  }

  caculateToi() {
    const dialogRef = this.dialog.open(ToiConfirmModalComponent);
    const toi = this.reductionProposalDetail.reductionProposalTOIDTO;
    this.initTOI(toi);
    dialogRef.componentInstance.action = 'approved';
    dialogRef.afterClosed().subscribe(data => {
      this.info = this.reductionProposal?.info?.value;
      const toi = this.info?.toi;
      if (toi) {
        this.modelValue.toi = toi?.toi;
        this.modelValue.dtttrr = toi?.dtttrr;
      }
    });
  }

  calMaginReduction(row) {
    // let baseMargin = row?.baseMargin || 0;
    const maximumMarginReduction = row?.maximumMarginReduction || 0;
    if (row?.maximumMarginReduction)
      // return (baseMargin - maximumMarginReduction).toFixed(2);
      return maximumMarginReduction.toFixed(2);
    else
      return '--';
  }

  formatCurrency(element) {
    if (element == 0) {
      return '0';
    }
    if (!element || element === 'NaN' || element === '--') {
      return '--';
    }
    let formated: string = element.toString();
    let first = '';
    let last = '';
    if (formated.includes('.')) {
      first = formated.substr(0, formated.indexOf('.'));
      last = formated.substr(formated.indexOf('.'));
    } else {
      first = formated;
    }
    if (Number(first)) {
      first = Number(first).toLocaleString('en-US', {
        style: 'currency',
        currency: 'VND'
      });
      first = first.substr(1, first.length);
      formated = first + last;
    }
    return formated;
  }

  clearSegment(name: string) {
    if (name === 'Không xác định') {
      name = '--';
    }
    return name;
  }

  formatLoaiKH(name: string) {
    if (!name) {
      return name;
    }
    if (name === 'NEW' || name === 'Khach hang moi' || name.toLowerCase() === 'khach hang moi') {
      name = 'Khách hàng mới';
    } else if (name === 'Khach hang cu' || name.toLowerCase() === 'khach hang cu') {
      name = 'Khách hàng cũ';
    }
    return name;
  }

  convertToiCurrent(toi) {
    if (toi) {
      toi = Number(Number(toi) * 100).toFixed(2);
    }
    return toi;
  }

  convertDtttrr(dtttrr) {
    if (dtttrr) {
      dtttrr = Number(Number(dtttrr) / 1000000).toFixed(2);
    }
    return dtttrr;
  }

  checkAbilityInterest() {
    return (this.typeInterestDetail === 2 || this.typeInterestDetail === 4);
  }
}
