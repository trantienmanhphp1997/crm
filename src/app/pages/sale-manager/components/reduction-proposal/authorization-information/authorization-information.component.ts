import { Component, Injector, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ReductionProposalApi } from '../../../api/reduction-proposal.api';
import { forkJoin, of } from 'rxjs';
import { CommonCategory } from '../../../../../core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { CategoryService } from '../../../../system/services/category.service';
import { BaseComponent } from '../../../../../core/components/base.component';

@Component({
  selector: 'app-authorization-information',
  templateUrl: './authorization-information.component.html',
  styleUrls: ['./authorization-information.component.scss']
})
export class AuthorizationInformationComponent extends BaseComponent implements OnInit, OnChanges {
  @Input() code;
  listDataTable = [];
  isLoading = false;
  listReductionAuthorizationPromulgate = [];
  listReductionAuthorizationReview = [];
  listReductionAuthorizationInitialSigned = [];
  toggleItem = {
    1: true,
    2: true,
    3: true
  };
  listStatusReductionProposal = [];

  constructor(
    injector: Injector,
    private reductionProposal: ReductionProposalApi,
    private categoryService: CategoryService
  ) {
    super(injector);
    this.isLoading = true;
  }

  toggle(index: number): void {
    this.toggleItem[index] = !this.toggleItem[index];
  }

  ngOnInit(): void {
    forkJoin([
      this.commonService.getCommonCategory(CommonCategory.REDUCTION_PROPOSAL_STATUS).pipe(catchError((e) => of(undefined)))
    ]).subscribe(([listStatusReductionProposal]) => {
      this.listStatusReductionProposal = [...this.listStatusReductionProposal, ...listStatusReductionProposal?.content] || [];
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.code?.currentValue) {
      this.reductionAuthorizationPromulgate();
      this.reductionAuthorizationReview();
      this.reductionAuthorizationInitialSigned();
    }
  }

  reductionAuthorizationPromulgate() {
    this.isLoading = true;
    const params: any = {};
    params.status = 1;
    params.code = this.code;
    this.reductionProposal.viewAuthorization(params).subscribe(
      (result) => {
        if (result) {
          this.listReductionAuthorizationPromulgate = result || [];
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  reductionAuthorizationReview() {
    this.isLoading = true;
    const params: any = {};
    params.status = 2;
    params.code = this.code;
    this.reductionProposal.viewAuthorization(params).subscribe(
      (result) => {
        if (result) {
          this.listReductionAuthorizationReview = result || [];
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  reductionAuthorizationInitialSigned() {
    this.isLoading = true;
    const params: any = {};
    params.status = 3;
    params.code = this.code;
    this.reductionProposal.viewAuthorization(params).subscribe(
      (result) => {
        if (result) {
          this.listReductionAuthorizationInitialSigned = result || [];
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  getStatus(value) {
    if(value === 'REJECT'){
      return 'Từ chối';
    }
    else {
      return (
        this.listStatusReductionProposal?.find((item) => {
          return item.code === value;
        })?.name || ''
      );
    }
  }
}
