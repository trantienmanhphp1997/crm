import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Injectable,
  Injector,
  OnInit,
  ViewChild
} from '@angular/core';
import { catchError, finalize } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import {
  Division,
  FunctionCode,
  functionUri,
  Scopes,
  SessionKey
} from 'src/app/core/utils/common-constants';
import * as _ from 'lodash';
import { Utils } from 'src/app/core/utils/utils';
import { MatDialog } from '@angular/material/dialog';
import { ReductionProposalApi } from '../../../api/reduction-proposal.api';
import { CreateProposalComponent } from '../create-proposal/create-proposal.component';
import { ChoosePotentialModalComponent } from '../dialog/choose-create-modal/choose-potential-modal.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { forkJoin, of } from 'rxjs';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as moment from 'moment';
import { ConfirmReductionComponent } from '../confirm-reduction/confirm-reduction.component';
import { FreeDiscountAnnexComponent } from '../annex/annex.component';
import { PreviewModalComponent } from '../dialog/preview-modal/preview-modal.component';
import { CustomerApi } from 'src/app/pages/customer-360/apis';
import { CustomerDetailSmeApi } from 'src/app/pages/customer-360/apis/customer.api';
import { DatePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'app-reduction-proposal-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  providers: [DatePipe]
})
export class CreateReductionProposalComponent extends BaseComponent implements OnInit, AfterViewInit {

  @ViewChild(CreateProposalComponent) declareComponent: CreateProposalComponent;
  @ViewChild(ConfirmReductionComponent) confirmReductionComponent: ConfirmReductionComponent;
  @ViewChild(FreeDiscountAnnexComponent) freeDiscountAnnexComponent: FreeDiscountAnnexComponent
  form = this.fb.group({
    customerCode: '',
    taxCode: '',
    idCard: '',
    blockCode: '',
    customerType: '1',
    type: '0'
  });
  listDivision = [];
  highPrioritySort = [Division.INDIV, Division.SME, Division.CIB, Division.FI];
  commonData = {
    divisions: []
  }
  disabled: any;
  tabIndex = 0;
  isLoading = false;
  customer = {
    existed: false,
    isKhdn: false,
    isNew: false,
    searchLabel: '',
    searchPlaceholder: '',
    searchLabel2: '',
    searchPlaceholder2: '',
    customerCode: 'code',
    customerName: 'name',
    taxCode: 'taxCode',
    segment: 'segment',
    relationship: 'relationship',
    dtttrr: 'dtttrr',
    piority: 'piority',
    classification: 'classification',
    toiCurrent: 'toiCurrent',
    nimCurrent: 'nimCurrent',
    scoreValue: 'scoreValue'
  };
  reductionProposalTranslate: any;
  info: any;
  isDeclareTab = false;
  isKhdn = false;
  existed = false;
  dataReduction: any;
  isRm: boolean;
  declareInfo = {
    searchStr: '',
    caseSuggest: 'FEE',
    scopeApply: 'ALL',
    toi: null,
    dtttrr: null,
    monthApply: null,
    monthConfirm: null,
    listData: [],
    branchCode: ''
  }
  customerCode: any;
  createdId = null;
  code: any;
  creditInformation: any;
  creditInformationAtMB: any;
  reductionProposal: [];
  listLD: [];
  divisionCodeOfUser: any[];
  isINDIV: boolean;
  productCode: any;

  constructor(
    injector: Injector,
    public dialog: MatDialog,
    private reductionProposalApi: ReductionProposalApi,
    private categoryService: CategoryService,
    private customerDetailSmeApi: CustomerDetailSmeApi,
    private datePipe: DatePipe,
    private customerApi: CustomerApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REDUCTION_PROPOSAL}`);
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
  }

  ngOnInit(): void {

    this.form.controls.blockCode.valueChanges.subscribe(code => {
      if(code === Division.INDIV || code === Division.FI){
        this.form.controls.customerType.setValue(1);
        this.form.controls.type.setValue(0);
      } else {
        this.form.controls.customerType.setValue(1);
        this.form.controls.type.setValue(0);
        this.form.controls.customerCode.setValue('');
        this.form.controls.taxCode.setValue('');
        this.form.controls.idCard.setValue('');
      }
      this.isINDIV = code === Division.INDIV;
    });
    this.form.controls.type.valueChanges.subscribe( type => {
      if(this.form.controls.blockCode.value === Division.SME && type === 2){
        this.form.controls.customerType.setValue(1);
      }
    });

    this.translate.get(['reductionProposal']).subscribe((result) => {
      this.reductionProposalTranslate = _.get(result, 'reductionProposal');
    });
    this.info = this.reductionProposalApi?.info?.value;
    this.customer = this.info?.customer;
    this.existed = this.customer?.existed;
    this.isKhdn = this.info?.customer?.isKhdn || false;
    this.customer.searchLabel = this.existed ?
      this.reductionProposalTranslate?.labels?.customerCode : this.reductionProposalTranslate?.labels?.taxCode;
    this.customer.searchPlaceholder = this.existed ?
      this.reductionProposalTranslate?.labels?.customerCodePlaceholder
      : this.reductionProposalTranslate?.labels?.taxCodePlaceholder;
    this.customer.searchLabel2 = this.reductionProposalTranslate?.labels?.taxCode;
    this.customer.searchPlaceholder2 = this.reductionProposalTranslate?.labels?.taxCodePlaceholder;

    const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
    this.listDivision =
      divisionOfUser?.map((item) => {
        return { code: item.code, displayName: `${item.code || ''} - ${item.name || ''}` };
      }).filter(item => item.code !== Division.DVC) || [];
    this.sortDivision();
    if (this.listDivision.length > 0) {
      this.form.controls.blockCode.setValue(this.listDivision[0].code);
    }

    forkJoin([
      this.categoryService.getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW).pipe(catchError(() => of(undefined)))
    ]).subscribe(([branchesOfUser]) => {
      this.isRm = _.isEmpty(branchesOfUser);
      const divisionOfUser = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
      this.divisionCodeOfUser = divisionOfUser?.map(item => {
        return item?.code;
      });
    }, () => {
      this.isLoading = false;
    });
  }

  findCustomer(): void {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
      return;
    }
    let { customerCode, taxCode, idCard, type } = this.getAnnexViewChild().formSearch = this.form.getRawValue();
    customerCode = customerCode.trim();
    taxCode = taxCode.trim();
    idCard = idCard.trim();
    this.customerCode = customerCode;
    const isCustomerExisted = this.form.controls.customerType.value;
    const blockCode = this.form.controls.blockCode.value;
    if (isCustomerExisted === 1 && Utils.isEmpty(customerCode) && Utils.isEmpty(taxCode) && blockCode !== Division.INDIV) {
      this.messageService.warn(_.get(this.reductionProposalTranslate?.messages, 'taxCodeAndCustomerCodeIsRequired'));
      return;
    } else if (isCustomerExisted === 0 && Utils.isEmpty(taxCode)) {
      this.messageService.warn(_.get(this.reductionProposalTranslate?.messages, 'taxCodeIsRequired'));
      return;
    } else if (blockCode === Division.INDIV && Utils.isEmpty(idCard) && Utils.isEmpty(customerCode)) {
      this.messageService.warn(_.get(this.reductionProposalTranslate?.messages, 'idCardRequired'));
      return;
    }
    this.isLoading = true;
    const body = this.form.controls.blockCode.value === Division.INDIV ? {
      customerCode: customerCode.trim(),
      idCard: idCard.trim(),
      blockCode: this.form.controls.blockCode.value.trim()
    } : {
      customerCode: customerCode.trim(),
      taxCode: taxCode.trim(),
      blockCode: this.form.controls.blockCode.value.trim()
    };
    this.isKhdn = ['SME', 'CIB', 'FI', 'DVC'].includes(blockCode);

    // KHHH
    this.info.type = type;
    this.info.customer = {
      existed: isCustomerExisted === 1,
      isKhdn: this.isKhdn,
      isNew: false,
      searchLabel: this.info.customer?.searchLabel,
      searchPlaceholder: this.info.customer?.searchPlaceholder,
      searchLabel2: this.info.customer?.searchLabel2,
      searchPlaceholder2: this.info.customer?.searchPlaceholder2,
      blockCode
    };
    this.reductionProposalApi.info.next(this.info);
    this.existed = this.info.customer.existed;
    if (isCustomerExisted === 1) {
      const api = this.form.controls.blockCode.value === Division.INDIV ?
        this.reductionProposalApi.findExistingCustomerIndiv(body)
        : this.reductionProposalApi.findExistingCustomer(body);
      api.pipe(
        finalize(() => {
        })
      ).subscribe((data) => {
        if(type == 4){
          this.router.navigate([functionUri.reduction_fee_exception, 'create'], { state: {
            formSearch: this.form.getRawValue(),
            search: this.state?.formSearch
          }});
          return;
        }
        this.customerCode = data.customerCode;
        if (!data.code) {
          this.tabIndex = 0;
          this.info.customer.customerCode = data.customerCode;
          this.info.customer.customerName = data.customerName;
          this.info.customer.taxCode = data?.taxCode;
          this.info.customer.idCard = data?.taxCode;
          this.info.customer.isKhdn = this.isKhdn;
          this.info.customer.customerTypeMerge = data?.customerTypeMerge;
          this.info.customer.businessRegistrationNumber = data?.businessRegistrationNumber;
          this.declareInfo.branchCode = data.branchCode;
          this.info.customer.branchCode = data.branchCode;

          this.info.customer.isManage = this.checkBrand(data?.branchCode);
          if([1, 2, 3].includes(this.info.type) && !this.info.customer.isManage){
            this.isLoading = false;
            return;
          }
          this.reductionProposalApi.info.next(this.info);
          if ( type !== 2) {
            this.getDeclareViewChild().loadData();
          }
          this.navigateStageReduction(type, this.info.customer, data);
          if ([2,3,4].includes(type)){
            return;
          }
          this.getDeclareViewChild().loadData();
          if (this.info.type === 1 && !this.info.customer.isManage) {
            this.isLoading = false;
            return;
          }
          if (!data?.taxCode && !data?.businessRegistrationNumber) {
            this.messageService.warn('Khách hàng không có mã số thuế và số đăng ký kinh doanh');
            this.isLoading = false;
            return;
          } else {
            this.getAnnexViewChild().loadDataByTaxCode(data?.taxCode || data?.businessRegistrationNumber);
          }
          this.isDeclareTab = true;

          this.getAnnexViewChild()?.getDataCustomerRelation?.(data.customerCode, data.branchCode);
        } else {
          this.messageService.warn(_.get(this.notificationMessage, 'customerNotExist'));
        }
      }, (err) => {
        this.isLoading = false;
        this.messageService.warn(err?.error?.description || 'Lỗi hệ thống!');
      });

    } else {
      // KHTN
      this.reductionProposalApi.findPotentialCustomer(body).pipe(
        finalize(() => {
        })
      ).subscribe((data) => {
        if(type == 4){
          this.router.navigate([functionUri.reduction_fee_exception, 'create'], { state: {
            formSearch: this.form.getRawValue(),
            findPotentialCustomer: data
          }});
          return;
        }
        if (!data.code) {
          this.tabIndex = 0;
          if (!data?.taxCode && !data?.businessRegistrationNumber) {
            this.messageService.warn('Khách hàng không có mã số thuế và số đăng ký kinh doanh');
            this.isLoading = false;
            return;
          } else {
            this.freeDiscountAnnexComponent.loadDataByTaxCode(data?.taxCode || data?.businessRegistrationNumber);
          }
          this.info.customer.customerCode = '';
          this.info.customer.customerName = data?.fullName;
          this.info.customer.taxCode = data?.taxCode;
          this.info.customer.isKhdn = this?.isKhdn;
          this.info.customer.businessRegistrationNumber = data?.businessRegistrationNumber;
          this.info.customer.leadCode = data?.leadCode;
          if (this.divisionCodeOfUser.includes('SME')) {
            this.info.customer.customerTypeMerge = 'SME';
          } else if (this.divisionCodeOfUser.length > 0) {
            this.info.customer.customerTypeMerge = this.divisionCodeOfUser[0];
          }
          this.reductionProposalApi.info.next(this.info);
          // if ( type !== 2 && blockCode !== Division.INDIV) {
          //   this.getDeclareViewChild().loadData();
          // }
          this.navigateStageReduction(type, this.info.customer, data);
          if ([2,3,4].includes(type)){
            return;
          }
          this.getDeclareViewChild().loadData();
          this.isDeclareTab = true;
          if(type == 4){
            this.router.navigate([functionUri.reduction_fee_exception, 'create'], { state: {
              formSearch: this.form.getRawValue(),
            }});
            return;
          }
          if(data?.businessRegistrationNumber){
            this.freeDiscountAnnexComponent.getCreditInformation({ identifiedNumber: data?.businessRegistrationNumber });
          }
        } else {
          this.dialog.open(ChoosePotentialModalComponent);
          this.isLoading = false;
        }
      }, (e) => {
        if (e?.error?.description === 'Khách hàng tiềm năng không tồn tại') {
          this.dialog.open(ChoosePotentialModalComponent);
        } else {
          this.messageService.warn(e?.error?.description);
        }
        this.isLoading = false;
      });
    }
  }

  navigateStageReduction(type: number, customer: any, data: any){
    const stateObj = this.prop || this.state || {};
    stateObj.customer = customer;
    if (type === 2) {
      this.router.navigateByUrl(functionUri.reduction_proposal + '/create-setup',
        { state: stateObj });
    }
    if(type === 3){
      this.router.navigate([functionUri.reduction_interest_exception, 'create'],
        { state: {
            ...stateObj,
            findPotentialCustomer: data
          } });

    }
    if(type === 4){
      this.router.navigate([functionUri.reduction_fee_exception, 'create']);

    }
  }

  ngAfterViewInit() {
    this.form.controls.taxCode.valueChanges.subscribe(t => {
      if (t) {
        this.form.controls.taxCode.setValidators(CustomValidators.onlyNormalAndSpecialChar);
      } else {
        this.form.controls.taxCode.clearValidators();
      }
      this.form.controls.taxCode.updateValueAndValidity({ onlySelf: true, emitEvent: false });
    });

    this.form.controls.customerCode.valueChanges.subscribe(t => {
      if (t) {
        this.form.controls.customerCode.setValidators(CustomValidators.onlyNumber);
      } else {
        this.form.controls.customerCode.clearValidators();
      }
      this.form.controls.customerCode.updateValueAndValidity({ onlySelf: true, emitEvent: false });
    });
  }

  onTabChanged($event) {
    if (!this.declareComponent.form.valid) {
      validateAllFormFields(this.declareComponent.form);
      return;
    }
    this.tabIndex = _.get($event, 'index');
  }

  backStep(): void {
    // check tabindex
    // if (this.isDeclareTab) {
    //   if(this.tabIndex > 0){
    //     this.tabIndex--;
    //   }else{
    //     this.declareComponent.clearData();
    //     this.isDeclareTab = false;
    //     this.createdId = null;
    //   }

    // } else {
      this.router.navigateByUrl(functionUri.reduction_proposal, { state: this.prop ? this.prop : this.state });
    // }
  }

  handlerDataReduction(data) {
    this.dataReduction = data?.customerInfo;
    this.reductionProposal = data?.reductionProposal?.listLoanInfoByLoanId;
    this.listLD = data?.listLD;
    const customerInfo = this.info.customer;
    const paramCreditInfo = {
      identifiedNumber: customerInfo?.businessRegistrationNumber
    };
    const customerCode = customerInfo?.customerCode;
    this.customerCode = customerCode

    const forkJoinArr = [
      this.customerDetailSmeApi.getDataEarlyWarningModel(customerCode).pipe(catchError(() => of(undefined)))
    ];

    if(customerInfo?.businessRegistrationNumber){
      this.getAnnexViewChild().getCreditInformation(paramCreditInfo);
      forkJoinArr.push(this.customerApi.getCreditInformation(paramCreditInfo).pipe(catchError(() => of(undefined))));
    }
    forkJoin(forkJoinArr).subscribe(([creditInformationAtMB, creditInformation]) => {
      if(creditInformation){
        this.creditInformation = creditInformation;
      }
      this.creditInformationAtMB = creditInformationAtMB;
    });
  }

  confirm() {
    this.confirmReductionComponent.confirm()
  }

  CBQLconfirm(idea: boolean) {
    this.confirmReductionComponent.CBQLconfirm(idea);
  }

  saveFile() {
    if (this.freeDiscountAnnexComponent) {
      return this.freeDiscountAnnexComponent.save();
    }
  }

  save(backToList: boolean, type: string) {
    return new Promise<any>(async (resolve, reject) => {
      if (!this.isValid() || !this.freeDiscountAnnexComponent.compareDate(type)) {
        reject(false);
        return;
      }

      const fileList = await this.saveFile();
      const declare: any = this.info = this.reductionProposalApi?.info?.value;
      const annexValue = this.freeDiscountAnnexComponent ? this.freeDiscountAnnexComponent.form.getRawValue() : {};
      const proposalValue = this.declareComponent.form.getRawValue();
      const selected = declare?.authorization ? declare?.authorization : [];
      let reductionProposalDetailsDTOList = [];

      if (this.declareComponent.listData.length > 0) {
        // Mapping thông tin giảm lãi
        reductionProposalDetailsDTOList = this.declareComponent.listData?.map(item => {
          item = item.data;
          return {
            planCode: proposalValue.scopeApply == 'OPTION_CODE' ? item.code : '',
            ldCode: proposalValue.scopeApply == 'LD_CODE' ? item.code : '',
            itemCode: item?.productCode,  // mã biểu lãi
            itemName: item?.name, // tên biểu lãi
            maximumCost: '',
            maximumCostOffer: '',
            minimumCost: '',
            minimumCostOffer: '',
            ratioCost: '',
            ratioCostOffer: '',
            type: proposalValue.caseSuggest == 'FEE' ? 0 : 1, // Loại---0 : Biểu phí, 1: Biểu lãi suất
            interestValue: item.loanRate, // Lãi suất cho vay
            interestUnit: item.currency, // Đơn vị tiền tệ
            interestType: item.name, // Biểu lãi suất
            interestReference: item.referenceInterestRate, // Lãi suất tham chiếu (%)
            interestProposedValue: item.loanSuggest, // Lãi suất cho vay đề xuất
            interestProposedAmplitude: item.marginSuggest, // Biên độ đề xuất (%)
            interestMaxReduction: item.maximumMarginReduction,// Mức giảm biên độ tối đa (%
            interestLoanPeriod: item.period, // Kỳ hạn vay
            interestAmplitude: item.baseMargin, // Biên độ (%)
            interestAdjustPeriod: item?.adjustmentPeriod, // Kỳ điểu chỉnh lãi suất
            reductionRate: item.reductPercent, // tỷ lệ giảm
            id: '',
            minInterestRate: item.minInterestRate,
            minInterestRateOffer: item.rateMinimum
          };
        });
      }
      // Mapping thông tin giảm phí
      let errorCount = 0;
      let lowerThanZero = 0;

      if (this.declareComponent.listFeeData.length > 0) {
        console.log('listFeeData : ',this.declareComponent.listFeeData)
        reductionProposalDetailsDTOList = this.declareComponent.listFeeData?.map(item => {
          item = item.data;
          const rateCost = item?.rateCost || 0;
          const rateMin = item?.rateMin || 0;
          const rateMax = item?.rateMax || 0;
          errorCount += rateCost === 0 && rateMin === 0 && rateMax === 0 ? 1 : 0;
          lowerThanZero += Math.min(rateCost, rateMin, rateMax) < 0 ? 1 : 0 ;


          return {
            planCode: proposalValue.scopeApply == 'OPTION_CODE' ? item.code : '',
            ldCode: proposalValue.scopeApply == 'LD_CODE' ? item.code : '',
            mdCode: proposalValue.scopeApply == 'MD_CODE' ? item.code : '',
            itemCode: item?.chargeCode,  // mã biểu lãi
            itemName: item?.descriptionVn, // tên biểu lãi
            interestUnit: item?.currency, // Đơn vị tiền tệ
            interestType: item?.chargeType, // Loại phí
            ratioCost: item?.chgAmtFrom || !isNaN(item?.chgAmtFrom) ? Number(item?.chgAmtFrom) : null, // Mức phí biểu theo quy định
            minimumCost: item?.minCharge || !isNaN(item?.minCharge) ? Number(item?.minCharge) : null, // Mức phí tối thiểu theo quy định
            maximumCost: item?.maxCharge || !isNaN(item?.maxCharge) ? Number(item?.maxCharge) : null, // Mức phí tối đa theo quy định
            ratioCostOffer: item?.suggestCost || !isNaN(item?.suggestCost)
              ? Number(item?.suggestCost.replaceAll(',', '')) : null, // Mức phí đề xuất
            minimumCostOffer: item?.suggestMin || !isNaN(item?.suggestMin)
              ? Number(item?.suggestMin.replaceAll(',', '')) : null, // Mức phí tối thiểu đề xuất
            maximumCostOffer: item?.suggestMax || !isNaN(item?.suggestMax)
              ? Number(item?.suggestMax.replaceAll(',', '')) : null, // Mức phí tối đa đề xuất
            calcType: item?.calcType,
            chargeUnit: item?.chargeUnit, // Đơn vị phí tối thiểu, tối đa
            valueRefDoc: item?.valueRefDoc, // Đơn vị mức phí
            reductionRate: item?.rateCost ? Number(item?.rateCost) : 0, // tỷ lệ giảm
            type: proposalValue.caseSuggest == 'FEE' ? 0 : 1, // Loại---0 : Biểu phí, 1: Biểu lãi suất
            chargeGroup: item?.chargeGroup,
            sumChargeCode: item?.sumChargeCode // Mã biểu phí của phí cha
          };
        });
      }

      if(errorCount > 0){
        this.messageService.warn('Vui lòng nhập thông tin đề xuất giảm');
        reject(false);
        return;
      }

      if(lowerThanZero > 0){
        this.messageService.warn('Tỉ lệ giảm phải lớn hơn hoặc bằng 0');
        reject(false);
        return;
      }

      let taxCode;
      if (this.info.customer.blockCode === Division.INDIV && this.info.type === 0){
          taxCode = this.info.customer.idCard;
      }else{
          taxCode = declare?.customer?.taxCode;
      }

      // scopeApply
      let scopeApplyData = [];
      switch (proposalValue.scopeApply) {
        case 'LD_CODE':
          scopeApplyData = this.declareComponent.ldCodeData;
        break;
        case 'OPTION_CODE':
          scopeApplyData = this.declareComponent.optionCodeData;
        break;
        case 'MD_CODE':
          scopeApplyData = this.declareComponent.mdCodeData;
        break;
        default:
          if(this.info.type === 1){
            scopeApplyData = this.declareComponent.interestData;
          }else{
            scopeApplyData = this.declareComponent.feeData;
          }
      }
      const scopeApply = JSON.stringify(scopeApplyData);
      // scopeType
      const scopeType = this.declareComponent.scopeApplys.find(i => i.code === proposalValue.scopeApply).typeNum;

      const reductionProposalAuthorizationDTOList = selected.map(item => {
        if (item && item.childs) {
          delete item.childs;
        }
        return item;
      });

      let payload = {
        blockCode: this.info.customer.customerTypeMerge || '',
        businessActivities: annexValue?.businessActivities?.trim(),
        code: moment.now(),
        competitiveness: annexValue?.competitiveness?.trim(),
        customerCode: this.info.customer?.customerCode,
        dtttrrConfirm: proposalValue.dtttrr ? Number(proposalValue.dtttrr.toString().replace(',', '.')) : proposalValue.dtttrr,
        monthApply: proposalValue.monthApply,
        monthConfirm: proposalValue.monthConfirm,
        moDocId: '',
        nimConfirm: proposalValue.nimConfirm,
        nimExisting: proposalValue.nimCurrent,
        planInformation: annexValue.planInformation?.trim(), // Thông tin phương án
        resultBusiness: annexValue?.resultBusiness?.trim(),
        scopeApply,
        scopeType,
        toiConfirm: proposalValue.toi,
        taxCode,
        leadCode: declare?.customer?.leadCode,
        type: proposalValue.caseSuggest == 'FEE' ? 0 : 1,
        reductionProposalDetailsDTOList,
        reductionProposalDetailsApprovedDTOList: [],
        reductionProposalTOIDTO: {
          ckhBqNim: declare?.toi?.coKyHanBqUnit || 0,
          ckhBqSd: declare?.toi?.coKyHanBq || 0,
          dunoBqNim: declare?.toi?.duNoNganHanBqUnit || 0,
          dunoBqSd: declare?.toi?.duNoNganHanBq || 0,
          dunoTdhBqNim: declare?.toi?.duNoTdhBqUnit || 0,
          dunoTdhBqSd: declare?.toi?.duNoTdhBq || 0,
          kkhBqNim: declare?.toi?.khongKyHanBqUnit || 0,
          kkhBqSd: declare?.toi?.khongKyHanBq || 0,
          tblDsSd: declare?.toi?.thuBl || 0,
          tblNim: declare?.toi?.thuBlUnit || 0,
          tfxDsSd: declare?.toi?.thuFx || 0,
          tfxMpqd: declare?.toi?.thuFxUnit || 0,
          thuBancaDsSd: declare?.toi?.thuBanca || 0,
          thuBanCaMpqd: 1,
          thuKhacDsSd: declare?.toi?.thuKhac || 0,
          thuKhacMpqd: 1,
          tttqtDsSd: declare?.toi?.thuTTQT || 0,
          tttqtMpqd: declare?.toi?.thuTTQTUnit || 0,
          ckhBqHq: declare?.toi?.ckhBqHq ? +declare?.toi?.ckhBqHq.replace(',', '.') : 0,
          dunoBqHq: 0,
          dunoTdhBqHq: 0,
          kkhBqHq: 0,
          tblHq: 0,
          tfxHq: 0,
          thuBanCaHq: 0,
          thuKhacHq: 0,
          tttqtHq: 0,
          dttck: 0,
          tck: 0
        },
        reductionProposalFileDTOList: fileList,
        reductionProposalAuthorizationDTOList,
        customerName: this.info.customer?.customerName,
        approverName: '',
        approverTitle: '',
        status: '',
        createdDate: '',
        rmCode: this.currUser?.rmCode,
        branchCode: this.currUser?.branch,
        branchCodeCustomer: this.info.customer?.branchCode,
        financialSituation: annexValue?.financialSituation?.trim(),
        customersPolicyApplied: '',
        proposalReason: '',
        customerProductList: '',
        toiExisting: declare?.customer?.toiCurrent,
        dtttrrExisting: declare?.customer?.dtttrr,
        approvedAuthorization: '',
        customerType: declare?.customer?.customerTypeCode,
        customerSegment: declare?.customer?.segmentCode,
        businessRegistrationNumber: declare?.customer?.businessRegistrationNumber,
        classification: declare?.customer?.classification,
        creditRanking: declare?.customer?.scoreValue,
        jsonRelationTCTD: JSON.stringify(this.getAnnexViewChild().creditInformation),
        yearsOfRelationship: declare?.customer?.mbRelationship,
        jsonRelationMB: JSON.stringify(this.getAnnexViewChild().relationshipHT),
        industryActivity: '',
        businessRegistrationLocation: '',
        startDate: '',
        endDate: '',
        customerStructure: this.info.customer.customerCreditType, // Cơ cấu khách hàng
        policyApplied: annexValue.policyApplied.trim(), // Chính sách đang áp dụng với khách hàng
        proposedBasis: annexValue.proposedBasis.trim(), // Cơ sở đề xuất (uy tín khách hàng, lợi ích mang lại...)
        jsonDebtOther: JSON.stringify(this.isINDIV
          ? this.getAnnexViewChild().duNoKHCN : this.getAnnexViewChild().camKetNgoaiBangData), // Thông tin dư nợ thẻ, trái phiếu và cam kết ngoại bảng
        debtCaution: this.getAnnexViewChild().noChuY ? 1 : 0, // Nợ chú ý trong vòng 12 tháng gần nhất
        jsonImportExportSales: JSON.stringify(this.getAnnexViewChild().doanhSoXNKData),
        identifiedIssueDate: this.getDeclareViewChild().info.customer?.businessInfo?.identifiedIssueDate,
        identifiedIssueArea: this.getDeclareViewChild().info.customer?.businessInfo?.identifiedIssueArea,
        job: this.getDeclareViewChild().info.customer?.jobInfo?.job,
        isRmManager: this.info.customer.isManage
      };
      try {
        payload = this.appendSMEExceptionPayload(payload);
      } catch (error) {
        console.log('payload error');
      }

      if (this.info.customer?.customerTypeMerge === Division.CIB
        && !this.info.customer.isManage
      ) {
        payload.toiExisting = 0;
        payload.dtttrrExisting = 0;
      }

      this.isLoading = true;
      let api;
      if (this.createdId) {
        api = this.reductionProposalApi.update(this.createdId, payload);
      } else {
        api = this.reductionProposalApi.save(payload);
      }
      api.subscribe(
        (result) => {
          this.isLoading = false;
          if (this.createdId) {
            this.messageService.success('Thêm mới thành công');
          } else {
            this.messageService.success('Thêm mới thành công. Mã tờ trình: ' + result.code);
            this.createdId = result?.id;
            if (result.code) {
              this.code = result.code;
            }
          }

          if (backToList) {
            setTimeout(() => {
              this.router.navigateByUrl(functionUri.reduction_proposal, { state: this.prop ? this.prop : this.state });
            }, 2000);
          }
          setTimeout(() => {
            resolve(true)
          }, 100);
        },
        (e) => {
          if (e?.error) {
            this.messageService.error(e?.error?.description);
          } else {
            this.messageService.error(_.get(this.notificationMessage, 'error'));
          }
          this.isLoading = false;
          reject(false)
        });
    });
  }

  appendSMEExceptionPayload(payload) {
    if (this.form.controls.blockCode.value === Division.SME || this.form.controls.blockCode.value === Division.CIB) {
      payload = {
        ...payload,
        interestType: this.declareComponent?.form.controls.typeInterest.value,
        productCode: this.declareComponent?.form?.controls?.scopeApply?.value === 'LD_CODE' ? this.productCode :
          this.declareComponent.typeInterest.find(item => item.id === this.declareComponent?.form.controls.typeInterest.value).code

      };
    }
    return payload;
  }

  next() {
    if (!this.isValid()) {
      return;
    }
    if (!this.createdId) {
      this.save(false, 'save').then(() => {
        this.tabIndex++;
      });
    }else{
      this.tabIndex++;
    }
  }

  async signSubmit() {
    const info: any = this.reductionProposalApi.info.value;
    if (_.isEmpty(info.authorization)) {
      this.messageService.warn(this.notificationMessage.process_proposal_author_warning);
    } else {
      const checkAuthDataChange = await this.getDeclareViewChild().checkAuthDataChange(true);
      if (checkAuthDataChange) {
        return;
      }
      this.save(false, 'signSubmit').then(() => {
          this.isLoading = true;
          const params: any = {};
          params.code = this.code;
          params.customerType = info?.customer?.customerTypeCode;
          params.customerName = info?.customer?.customerName;
          params.toiExisting = info?.customer?.toiCurrent;
          params.dtttrrExisting = info?.customer?.dtttrr;
          params.yearsOfRelationship = info?.customer?.mbRelationship;
          params.businessRegistrationNumber = info?.customer?.businessRegistrationNumber;
          params.customerSegment = info?.customer?.segmentCode;
          params.classification = info?.customer?.classification;
          params.creditRanking = info?.customer?.scoreValue;
          params.jsonRelationTCTD = JSON.stringify(this.getAnnexViewChild().creditInformation);
          params.jsonRelationMB = JSON.stringify(this.getAnnexViewChild().relationshipHT);
          const customerCICDTOS = this.getAnnexViewChild().creditInformation?.customerCICDTOS;
          if (Utils.isNotNull(customerCICDTOS) && customerCICDTOS.length > 0) {
            params.dataDate = new Date(customerCICDTOS[0]?.dsnapShotDt);
          } else {
            params.dataDate = null;
          }
          this.reductionProposalApi.processProposal(params).subscribe((res) => {
            const description = res?.description;
            this.isLoading = false;
            if (res?.status === 200) {
              this.messageService.success(this.notificationMessage.process_proposal_success);
              this.router.navigateByUrl(functionUri.reduction_proposal, { state: this.prop ? this.prop : this.state });
              setTimeout(() => {
                this.backStep();
              }, 5000);
            } else {
              this.messageService.error(description);
              this.isLoading = false;
            }
          }, () => {
            this.messageService.error(this.notificationMessage.process_proposal_error);
            this.isLoading = false;
          });
        }
      ).catch(err => {
        console.log(err);
      });
    }
  }

  isValid() {
    let errCount = 0;
    let errMessage = '';
    // xóa điều kiện validate với KH cũ{
    const declare: any = this.info = this.reductionProposalApi?.info?.value;
    if (declare?.customer?.customerTypeCode === 'OLD') {
      this.declareComponent.form.get('dtttrr').clearValidators();
      this.declareComponent.form.get('toi').clearValidators();
      this.declareComponent.form.get('dtttrr').updateValueAndValidity({ emitEvent: false });
      this.declareComponent.form.get('toi').updateValueAndValidity({ emitEvent: false });
    }



    if (this.form.controls.type.value != 0) {
      // Validate nếu là biểu lãi
      if (!this.declareComponent.form.valid) {
        validateAllFormFields(this.declareComponent.form);
        errCount++;
      }

      if (this.declareComponent.listData.length === 0) {
        errMessage += 'Chưa nhập thông tin giảm lãi suất. ';
        errCount++;
      } else {
        const fieldErr = [];
        this.declareComponent.listData.map(item => {
          if ((item.data?.reductPercent === null || Number(item.data?.reductPercent) < 0) && !fieldErr.includes('reductPercent')) {
            errMessage += 'Tỉ lệ giảm phải lớn hơn hoặc bằng 0. ';
            fieldErr.push('reductPercent');
            errCount++;
          }
          const baseMargin = item.data?.baseMargin || 0;
          if(item.data?.maximumMarginReduction){
            const maximumMarginReduction = item.data?.maximumMarginReduction || 0;
            const margin = (baseMargin - maximumMarginReduction).toFixed(2) || 0;
            const marginSuggestl = item.data?.marginSuggest || 0;
            if (Number(marginSuggestl) < +margin && !fieldErr.includes('margin')) {
              errMessage += 'Biên độ đề xuất phải lớn hơn hoặc bằng mức (Biên độ theo biểu - Mức giảm biên tối đa) ';
              fieldErr.push('margin');
              errCount++;
            }
          }

          if (item.data?.marginSuggest === null && !fieldErr.includes('marginSuggest')) {
            errMessage += 'Nhập đầy đủ biên độ đề xuất. ';
            fieldErr.push('marginSuggest');
            errCount++;
          }
        })
      }
    }else{
      // Validate nếu là biểu phí
      if (!this.declareComponent.form.valid) {
        validateAllFormFields(this.declareComponent.form);
        errCount++;
      }
      if (this.declareComponent.listFeeData.length === 0) {
        errMessage += 'Chưa nhập thông tin giảm phí. ';
        errCount++;
      } else {

      }

    }
    if (Utils.isStringNotEmpty(errMessage)) {
      this.messageService.warn(errMessage);
    }
    return errCount === 0;
  }

  previewPdf() {
    this.isLoading = true;
    const customerInfo = this.info.customer;
    const annexValue = this.freeDiscountAnnexComponent ? this.freeDiscountAnnexComponent.form.getRawValue() : {};
    const proposalValue = this.declareComponent.form.getRawValue();
    const declare: any = this.info = this.reductionProposalApi?.info?.value;
    const listData = this.declareComponent.listData || [];
    const listFeeData = this.declareComponent.listFeeData || [];
    let reductionProposalDetailsDTOList = [];
    let relationshipList = [];
    let planDetailList = [];
    let ldDetailList = [];
    if (listData?.length > 0) {
      reductionProposalDetailsDTOList = listData?.map(item => {
        item = item?.data;
        return {
          minInterestRate: item?.minInterestRate,
          minInterestRateOffer: item?.rateMinimum,
          interestValue: item?.loanRate, // Lãi suất cho vay
          interestUnit: item?.currency, // Đơn vị tiền tệ
          interestType: item?.name, // Biểu lãi suất
          interestReference: item?.referenceInterestRate, // Lãi suất tham chiếu (%)
          interestProposedValue: item?.loanSuggest, // Lãi suất cho vay đề xuất
          interestProposedAmplitude: item?.marginSuggest, // Biên độ đề xuất (%)
          interestMaxReduction: item?.maximumMarginReduction,// Mức giảm biên độ tối đa (%)
          interestLoanPeriod: item?.period, // Kỳ hạn vay
          interestAmplitude: item?.baseMargin, // Biên độ (%)
          interestAdjustPeriod: item?.adjustmentPeriod, // Kỳ điểu chỉnh lãi suất
          reductionRate: item?.reductPercent, // tỷ lệ giảm
          planCode: proposalValue.scopeApply == 'OPTION_CODE' ? item.code : '',
          ldCode: proposalValue.scopeApply == 'LD_CODE' ? item.code : '',
          mdCode: proposalValue.scopeApply == 'MD_CODE' ? item.code : '',
          type: proposalValue.caseSuggest == 'FEE' ? 0 : 1 // Loại---0 : Biểu phí, 1: Biểu lãi suất
        };
      })
    }
    if (listFeeData?.length > 0 ) {
      reductionProposalDetailsDTOList = listFeeData?.map(item => {
        item = item?.data;
        return {
          mdCode: proposalValue.scopeApply == 'MD_CODE' ? item.code : '',
          planCode: proposalValue.scopeApply == 'OPTION_CODE' ? item.code : '',
          ldCode: proposalValue.scopeApply == 'LD_CODE' ? item.code : '',
          itemCode: item?.currency ? item?.productCode.replace(item?.currency, '') : item?.productCode ,  // mã biểu lãi
          itemName: item?.descriptionVn, // tên biểu lãi
          interestUnit: item?.currency, // Đơn vị tiền tệ
          interestType: item?.chargeType, // Loại phí
          ratioCost: Utils.isNotNull(item?.chgAmtFrom) ? Number(item?.chgAmtFrom) : null, // Mức phí biểu theo quy định
          minimumCost: Utils.isNotNull(item?.minCharge) ? Number(item?.minCharge) : null, // Mức phí tối thiểu theo quy định
          maximumCost: Utils.isNotNull(item?.maxCharge) ? Number(item?.maxCharge) : null, // Mức phí tối đa theo quy định
          ratioCostOffer: Utils.isNotNull(item?.suggestCost) ? Number(item?.suggestCost.replaceAll(',','')) : null, // Mức phí đề xuất
          minimumCostOffer: Utils.isNotNull(item?.suggestMin) ? Number(item?.suggestMin.replaceAll(',','')) : null, // Mức phí tối thiểu đề xuất
          maximumCostOffer: Utils.isNotNull(item?.suggestMax) ? Number(item?.suggestMax.replaceAll(',','')) : null, // Mức phí tối đa đề xuất
          calcType:item?.calcType,
          chargeUnit:item?.chargeUnit, // Đơn vị phí tối thiểu, tối đa
          valueRefDoc:item?.valueRefDoc, // Đơn vị mức phí
          reductionRate: Utils.isNotNull(item?.rateCost) ? Number(item?.rateCost) : null, // tỷ lệ giảm
          type: proposalValue.caseSuggest == 'FEE' ? 0 : 1
        };
      })
    }
    // Tinh hinh QH tai TCTD khac
    let ngay_qhvctctd;
    if (this.creditInformation?.customerCICDTOS) {
      ngay_qhvctctd = this.creditInformation?.customerCICDTOS[0]?.dsnapShotDt;
      relationshipList = this.creditInformation?.customerCICDTOS.map(item => {
        return {
          creditInstitutionName: item?.creditOrgName,
          groupDebt: 'Nhóm ' + item?.gprDebitLoan,
          shortDebtInside: item?.debtShortVnd,
          shortDebtOutside: item?.debtShortUsd,
          mediumDebtInside: item?.debtMediumLongVnd,
          mediumDebtOutside: item?.debtMediumLongUsd
        };
      })
    }
    let relationAtMB;
    let ngay_qhtdvmb;
    if (this.creditInformationAtMB?.dataT1) {
      relationAtMB = this.creditInformationAtMB?.dataT1;
      ngay_qhtdvmb = this.datePipe.transform(new Date(relationAtMB?.dsnapShotDate), 'dd/MM/yyyy');
    }
    // Phu luc : Thong tin phuong an trinh dieu chinh
    planDetailList = []
    listData?.forEach(item => {
      const dataDetail = _.find(this.reductionProposal, (e) => e.loanId === item?.data?.code);
      if (dataDetail) {
        const detailMapping = {
          optionCode: dataDetail?.loanId,
          createdDate: dataDetail?.ngayTaoPhuongAn,
          optionType: dataDetail?.loaiPhuongAn,
          listHanMuc: dataDetail?.listHanMuc,
        }
        planDetailList.push(detailMapping);
      }
    });

    // Phu luc : Danh sach LD Trinh dieu chinh
    const listLDCode = this.declareComponent?.ldCodeData?.map(item => {
      return item.ldCode || item.SOKU
    });
    if (listLDCode) {
      ldDetailList = _.filter(
        this.listLD,
        (item) => listLDCode.includes(item.SOKU)
      );
    }

    let scopeApplyData = [];
    switch (proposalValue.scopeApply) {
      case 'LD_CODE':
        scopeApplyData = ldDetailList
        break;
      case 'OPTION_CODE':
        scopeApplyData = this.declareComponent.optionCodeData;
        break;
      case 'MD_CODE':
        scopeApplyData = this.declareComponent.mdCodeData;
        break;
      default:
        if(this.info.type === 1){
          scopeApplyData = this.declareComponent.interestData;
        }else{
          scopeApplyData = this.declareComponent.feeData;
        }
    }
    const scopeApply = JSON.stringify(scopeApplyData);
    // scopeType
    const scopeType = this.declareComponent.scopeApplys.find(i => i.code === proposalValue.scopeApply).typeNum;
    // author
    const selected = declare?.authorization ? declare?.authorization : [];
    const reductionProposalAuthorizationDTOList = selected.map(item => {
      if (item && item.childs) {
        delete item.childs;
      }
      return item;
    });
    const body = {
      interestType: this.declareComponent?.form.controls.typeInterest.value,
      blockCode: this.info.customer.customerTypeMerge || '',
      branchCode: this.currUser?.branch || '',
      businessActivities: annexValue?.businessActivities?.trim() || '',
      code: this.code,
      competitiveness: annexValue?.competitiveness?.trim() || '',
      customerCode: customerInfo?.customerCode || '',
      customerName: customerInfo?.customerName || '',
      monthApply: proposalValue.monthApply || 0,
      monthConfirm: proposalValue.monthConfirm || 0,
      planInformation: annexValue.planInformation?.trim() || '',
      resultBusiness: annexValue?.resultBusiness?.trim() || '',
      scopeApply,
      scopeType,
      toiConfirm: proposalValue.toi || '',
      type: proposalValue.caseSuggest == 'FEE' ? 0 : 1,
      dtttrrConfirm: proposalValue.dtttrr || 0,
      nimConfirm: proposalValue.nimConfirm,
      nimExisting: proposalValue.nimCurrent,
      reductionProposalTOIDTO: {
        ckhBqNim: declare?.toi?.coKyHanBqUnit || 0,
        ckhBqSd: declare?.toi?.coKyHanBq || 0,
        dunoBqNim: declare?.toi?.duNoNganHanBqUnit || 0,
        dunoBqSd: declare?.toi?.duNoNganHanBq || 0,
        dunoTdhBqNim: declare?.toi?.duNoTdhBqUnit || 0,
        dunoTdhBqSd: declare?.toi?.duNoTdhBq || 0,
        kkhBqNim: declare?.toi?.khongKyHanBqUnit || 0,
        kkhBqSd: declare?.toi?.khongKyHanBq || 0,
        tblDsSd: declare?.toi?.thuBl || 0,
        tblNim: declare?.toi?.thuBlUnit || 0,
        tfxDsSd: declare?.toi?.thuFx || 0,
        tfxMpqd: declare?.toi?.thuFxUnit || 0,
        thuBancaDsSd: declare?.toi?.thuBanca || 0,
        thuBanCaMpqd: 1,
        thuKhacDsSd: declare?.toi?.thuKhac || 0,
        thuKhacMpqd: 1,
        tttqtDsSd: declare?.toi?.thuTTQT || 0,
        tttqtMpqd: declare?.toi?.thuTTQTUnit || 0
      },
      customerType: declare?.customer?.customerTypeCode,
      reductionProposalDetailsDTOList,
      financialSituation: annexValue?.financialSituation?.trim(), // Tình hình tài chính (INDIV)
      customersPolicyApplied: 'Chính sách đang áp dụng với khách hàng (INDIV)', // Chính sách đang áp dụng với khách hàng (INDIV)
      proposalReason: 'Cơ sở đề xuất (INDIV)', // Cơ sở đề xuất (INDIV)
      customerProductList: 'Các sản phẩm khác khách hàng đang sử dụng (INDIV)', // Các sản phẩm khác khách hàng đang sử dụng (INDIV)
      toiExisting: declare?.customer?.toiCurrent,// declare?.customer?.toiCurrent? Number(Number(declare?.customer?.toiCurrent)* 100).toFixed(2):"", //TOI hien huu
      dtttrrExisting: declare?.customer?.dtttrr,
      approvedAuthorization: proposalValue?.lastApprove ?
        proposalValue?.lastApproveTitle + ' - ' + proposalValue?.lastApprove : '', // Thẩm quyền phê duyệt
      businessRegistrationNumber: customerInfo?.businessRegistrationNumber || customerInfo?.taxCode, // Số DKKD
      customerSegment: customerInfo?.segment, // Phân khúc
      classification: declare?.customer?.classification, // Đối tượng Khách hàng
      creditRanking: declare?.customer?.scoreValue || '', // Xếp hạng tín dụng
      jsonRelationTCTD: JSON.stringify(this.getAnnexViewChild().creditInformation),
      yearsOfRelationship: declare?.customer?.mbRelationship,
      jsonRelationMB: JSON.stringify(this.getAnnexViewChild().relationshipHT),
      customerStructure: this.info.customer.customerCreditType == 'Phi tín dụng' ? 'NO_CREDIT' : 'CREDIT', // Cơ cấu khách hàng
      policyApplied: annexValue.policyApplied, // Chính sách đang áp dụng với khách hàng
      proposedBasis: annexValue.proposedBasis, // Cơ sở đề xuất (uy tín khách hàng, lợi ích mang lại...)
      jsonDebtOther: JSON.stringify(this.isINDIV ? this.getAnnexViewChild().duNoKHCN : this.getAnnexViewChild().camKetNgoaiBangData), // Thông tin dư nợ thẻ, trái phiếu và cam kết ngoại bảng
      debtCaution: this.getAnnexViewChild().noChuY ? 1 : 0, // Nợ chú ý trong vòng 12 tháng gần nhất
      jsonImportExportSales: JSON.stringify(this.getAnnexViewChild().doanhSoXNKData),
      rmCode: this.currUser?.code,
      isRmManager: this.info?.customer?.isManage,
      identifiedIssueDate: this.getDeclareViewChild().info.customer?.businessInfo?.identifiedIssueDate,
      identifiedIssueArea: this.getDeclareViewChild().info.customer?.businessInfo?.identifiedIssueArea,
      job: this.getDeclareViewChild().info.customer?.jobInfo?.job,
      reductionProposalAuthorizationDTOList
    };
    this.reductionProposalApi.previewReduction(`preview`, 'pdf', body).then(
      (response) => {
        const blob = new Blob([response], { type: 'application/octet-stream' });
        const modal = this.modalService.open(PreviewModalComponent, { windowClass: 'tree__report-modal' });
        modal.componentInstance.data = blob;
        modal.componentInstance.extendUrl = '';
        this.isLoading = false;
      },
      () => {
        this.messageService.error('Không thể tải file');
        this.isLoading = false;
      }
    );
  }

  changeLoading(isLoading = false): void {
    this.isLoading = isLoading;
  }

  sortDivision(): void {
    const currentDisivionList: any[] = _.cloneDeep(this.listDivision);
    const result = [];
    this.highPrioritySort.forEach(code => {
      const index = currentDisivionList.findIndex(item => item.code === code);
      if (index > -1) {
        result.push(currentDisivionList[index]);
        currentDisivionList.splice(index, 1);
      }
    });

    result.push(...currentDisivionList);
    this.listDivision = result;
  }

  checkBrand(branchCode) {
    // Miễn giảm lãi, phí, ngoại lệ lãi check KHHH có mở code tại chi nhánh của RM ko
    if (branchCode != this.currUser.branch && this.existed) {
      if ([1, 2, 3].includes(this.info.type)) {
        this.messageService.warn("Khách hàng không thuộc chi nhánh quản lý");
      }
      return false;
    }
    return true;
  }
  getDeclareViewChild(){
    return this.declareComponent;
  }

  getAnnexViewChild(){
    return this.freeDiscountAnnexComponent;
  }
  checkShow(): boolean {
    return this.form.controls.blockCode.value !== 'INDIV' && this.form.controls.blockCode.value !== 'FI';
  }
}
