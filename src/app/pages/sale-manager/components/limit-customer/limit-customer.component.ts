import { BaseComponent } from 'src/app/core/components/base.component';
import { Component, OnInit, Injector, ViewChild ,ViewChildren ,QueryList} from '@angular/core';
import * as _ from 'lodash';
import { RmApi } from 'src/app/pages/rm/apis';
import { forkJoin, of } from 'rxjs';
import { AppFunction } from 'src/app/core/interfaces/app-function.interface';
import {
	CommonCategory,
	FunctionCode,
	functionUri,
	maxInt32,
	Scopes
} from 'src/app/core/utils/common-constants';
import { Utils } from 'src/app/core/utils/utils';
import { catchError } from 'rxjs/operators';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { v4 as uuIdv4 } from 'uuid';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { SaleManagerApi } from '../../api';
import { global } from '@angular/compiler/src/util';
import { CustomerApi, CustomerAssignmentApi } from 'src/app/pages/customer-360/apis';
import { MatMenuTrigger } from '@angular/material/menu';
import { CalendarAddModalComponent } from 'src/app/pages/calendar-sme/components/calendar-add-modal/calendar-add-modal.component';
import { CustomerLeadService } from 'src/app/pages/lead/service/customer-lead.service';
import { RmProfileService } from 'src/app/core/services/rm-profile.service';
import { FileService } from 'src/app/core/services/file.service';

@Component({
	selector: 'app-limit-customer-component',
	templateUrl: './limit-customer.component.html',
	styleUrls: ['./limit-customer.component.scss']
})
export class LimitCustomerComponent extends BaseComponent implements OnInit {
	// @ViewChild(DatatableComponent) public table: DatatableComponent;
	@ViewChildren(MatMenuTrigger) trigger:  QueryList<any>;
	ColumnMode = ColumnMode;
	listDataShow: any[] = [];
	isEmpty = true;
	isRM = true;
	listTypeHM: any;
	listEmail = [];
	listMiningRateLimit: any;
	abc: any;
	size: number = _.get(global, 'userConfig.pageSize');
	commonData = {
		listRmManager: [],
		listRmManagerTerm: [],
		listBranchManager: [],
	};
	pageable: Pageable;
	page: number = 0;
	objFunctionRM: AppFunction;
	objFunctionLimit: AppFunction;
	objFunctionCustomer: any;
	isOpenMore: boolean;
	formSearch = this.fb.group({
		taxCode: [null],
		customerName: [null],
		customerCode: [null],
		dkkd: [null],
		thoiGianHetHieuLucActiveTBTu: [],
		thoiGianHetHieuLucActiveTBDen: [],
		thoiGianHetHanHanMucTu: [],
		thoiGianHetHanHanMucDen: [],
		rmCodes: [null],
		branchCodes: [null],
		loaiLimit: [null],
		tyLeKhaiThacHanMuc: [null],
		cacLoaiHanMuc: [null],
		rsId:[null],
		scope:[null]
	});
	branchPermisstion: any;
	listTypeLimit = [{
		value: null,
		name: 'Tất cả'
	}, {
		value: 'MOI',
		name: 'Mới'
	}, {
		value: 'CU',
		name: 'Cũ'
	}];
	facilities: Array<any>;
	facilityCode: Array<string>;
	listRMSerch: any;
	getListRM: any;
	listData: any;
	listContactPosition = [];
	itemNoti: any;
	constructor(
		private rmApi: RmApi,
		private categoryService: CategoryService,
		private saleManagerApi: SaleManagerApi,
		private customerAssignmentApi: CustomerAssignmentApi,
		private service: CustomerLeadService,
		private rmProfileService: RmProfileService,
		private fileService: FileService,
		injector: Injector,
	) {
		super(injector);
		this.isLoading = true;
		this.itemNoti = _.get(this.route.snapshot.queryParams, 'itemNoti') !== undefined?JSON.parse(_.get(this.route.snapshot.queryParams, 'itemNoti')):'' ;
		this.objFunctionRM = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
		this.objFunctionLimit = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.LIMIT_CUSTOMER}`);
		this.objFunctionCustomer = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
	}
	ngOnInit(): void {
		if(this.itemNoti !== '')
			this.applyFilterByNoti(this.itemNoti)
		else{
			this.getData();
		}	
		
	}
	applyFilterByNoti(notify){
		this.rmProfileService
			.getByUsername(notify?.user).subscribe((result) => {
				if (result) {
					if(notify.notificationType === 'NOTIFICATION_LIMIT_RESIGN' ){
						var timeStart = new Date(notify.time);			
						var timeEnd =  new Date(notify.time);
						var timeEnd = new Date(timeEnd.setDate(timeEnd.getDate() + 60))
						this.formSearch.controls.thoiGianHetHanHanMucTu.setValue(timeStart);		
						this.formSearch.controls.thoiGianHetHanHanMucDen.setValue(timeEnd);	
						this.formSearch.controls.rmCodes.setValue([result.code]);	
					}else if(notify.notificationType === 'NOTIFICATION_LIMIT_LOW_MINING'){
						this.formSearch.controls.tyLeKhaiThacHanMuc.setValue('0');
						this.formSearch.controls.cacLoaiHanMuc.setValue(['0']);
						this.formSearch.controls.rmCodes.setValue([result.code]);
					}else if(notify.notificationType === 'NOTIFICATION_LIMIT_NOT_ACTIVE'){
						var timeStart = new Date(notify.time);	
						// var timeStart =  new Date(timeStart.setMonth(timeStart.getMonth()- 6))
						var timeEnd =  new Date(notify.time);
						var timeEnd =  new Date(timeEnd.setMonth(timeEnd.getMonth() + 1))
						this.formSearch.controls.thoiGianHetHieuLucActiveTBTu.setValue(timeStart);		
						this.formSearch.controls.thoiGianHetHieuLucActiveTBDen.setValue(timeEnd);
						this.formSearch.controls.rmCodes.setValue([result.code]);	
					}
				}
				this.getData();
			},
		(e) => {
		});
		// _.filter(
		// 	this.commonData.listRmManagerTerm,
		// 	(item) => item.userName === notify.user
		//   );
	}
	getData() {
		this.formSearch.controls.rsId.setValue(this.objFunctionLimit?.rsId);
		this.formSearch.controls.scope.setValue(Scopes.VIEW);
		forkJoin([
			this.rmApi
				.post('findAll', {
					page: 0,
					size: maxInt32,
					crmIsActive: true,
					branchCodes: [],
					rmBlock: "SME",
					rsId: this.objFunctionRM?.rsId,
					scope: Scopes.VIEW,
				})
				.pipe(catchError(() => of(undefined))),
			this.categoryService
				.getTreeBranchesOfUser(this.objFunctionRM?.rsId, Scopes.VIEW)
				.pipe(catchError(() => of(undefined))),
			this.categoryService
				.getBranchesOfUser(this.objFunctionRM?.rsId, Scopes.VIEW)
				.pipe(catchError(() => of(undefined))),
			this.commonService.getCommonCategory(CommonCategory.LIMIT_TYPE).pipe(catchError(() => of(undefined))),
			this.commonService.getCommonCategory(CommonCategory.MINING_RATE_LIMIT).pipe(catchError(() => of(undefined))),
			this.commonService.getCommonCategory(CommonCategory.CONTACT_POSITION).pipe(catchError(() => of(undefined))),
			this.saleManagerApi.getLimitList(this.formSearch.value, 0, this.size).pipe(catchError(() => of(undefined))),
		]).subscribe(([listRmManagement, branchOfUser, branchPermisstion, listTypeHM, listMiningRateLimit,position, data]) => {
			const listRm = [];
			this.listData = data?.content || [];
			this.pageable = {
				totalElements: data?.totalElements,
				totalPages: data?.totalPages,
				currentPage: this.page,
				size: this.size,
			};
			this.listContactPosition = position?.content;
			if (this.listData != null || this.listData != undefined) {
				this.convertData(this.listData)
			}
			listRmManagement?.content?.forEach((item) => {
				if (!_.isEmpty(item?.t24Employee?.employeeCode)) {
					listRm.push({
						code: _.trim(item?.t24Employee?.employeeCode),
						displayName: _.trim(item?.t24Employee?.employeeCode) + ' - ' + _.trim(item?.hrisEmployee?.fullName),
						branchCode: _.trim(item?.t24Employee?.branchCode),
					});
				}
			});
			if (!listRm?.find((item) => item.code === this.currUser?.code)) {
				listRm.push({
					code: this.currUser?.code,
					displayName:
						Utils.trimNullToEmpty(this.currUser?.code) + ' - ' + Utils.trimNullToEmpty(this.currUser?.fullName),
					branchCode: this.currUser?.branch,
				});
			}
			this.commonData.listRmManagerTerm = [...listRm];
			this.commonData.listRmManager = [...listRm];
			this.isRM = true;
			if(branchOfUser && branchOfUser.length > 0)
				this.facilities = branchOfUser 
			else{
				this.facilities =[{
					code: this.currUser?.branch,
					id: this.currUser?.branch,
					name: this.currUser?.branchName,
					parentCode: "VN",
					type: 3
				}]
			}
			branchOfUser = branchOfUser.map(item => {
				if(item.type === 1 || item.type === 2){
					item.level = 0;
				}else{
					item.level = null;
				}
			});
			this.getListRM = listRm || [];
			// this.commonData.listBranchManager =
			// 	branchOfUser?.map((item) => {
			// 		return {
			// 			code: item.code,
			// 			name: `${item.code} - ${item.name}`,
			// 		};
			// 	}) || [];
			// if (!_.find(this.commonData.listBranchManager, (item) => item.code === this.currUser?.branch)) {
			// 	this.commonData.listBranchManager.push({
			// 		code: this.currUser?.branch,
			// 		name: `${this.currUser?.branch} - ${this.currUser?.branchName}`,
			// 	});
			// }
			// if (!_.find(this.commonData.listBranchManager, (item) => item.code === this.currUser?.branch)) {
			// 	this.commonData.listBranchManager.push({
			// 		code: this.currUser?.branch,
			// 		name: `${this.currUser?.branch} - ${this.currUser?.branchName}`,
			// 	});
			// }

			this.listTypeHM = listTypeHM?.content || [];
			this.listMiningRateLimit = listMiningRateLimit?.content || [];
			var allLimit = {"name":"Tất cả"}
			this.listMiningRateLimit.unshift(allLimit);
			// this.convertData(this.fakeDate);
			this.isLoading = false;
		},
			(e) => {
				this.messageService.error(e.error.description);
				this.isLoading = false;
			});
	}
	convertData(listData: any) {
		let count = this.pageable?.currentPage * this.pageable?.size + 1;
		this.listDataShow = [];
		listData.forEach(element => {
			let id = uuIdv4();
			let element1 = {};
			let element2 = {};;
			let element3 = {};;
			element["id"] = id;
			element["stt"] = count++;
			element["parentId"] = "null";
			element["loaiHanMuc"] = "HM Tổng";
			this.listDataShow.push(element);
			if (element.lmBalDuNoRoll > 0 || element.loanBalRoll > 0) {
				element1["id"] = uuIdv4();
				element1["parentId"] = id;
				element1["loaiHanMuc"] = "Tín dụng";
				element1["lmBalTongRoll"] = element.lmBalDuNoRoll;
				element1["tongDuNghiaVu"] = element.loanBalRoll;
				element1["tongHmConlai"] = element.hmTindungConlai;
				element1["lmBalTongRollTyleKt"] = element.lmBalDuNoRollTyleKt;
				element1["treeStatus"] = 'disabled'
				this.listDataShow.push(element1);
			}
			if (element.lmBalBaoLanhRoll > 0 || element.blBalRoll > 0) {
				element2["id"] = uuIdv4();
				element2["parentId"] = id;
				element2["loaiHanMuc"] = "Bảo lãnh";
				element2["lmBalTongRoll"] = element.lmBalBaoLanhRoll;
				element2["tongDuNghiaVu"] = element.blBalRoll;
				element2["tongHmConlai"] = element.hmBaolanhConlai;
				element2["lmBalTongRollTyleKt"] = element.lmBalBaoLanhRollTyleKt;
				element2["treeStatus"] = 'disabled'
				this.listDataShow.push(element2);
			}
			if (element.lmBalLcRoll > 0 || element.lcBalRoll > 0) {
				element3["id"] = uuIdv4();
				element3["parentId"] = id;
				element3["loaiHanMuc"] = "LC";
				element3["lmBalTongRoll"] = element.lmBalLcRoll;
				element3["tongDuNghiaVu"] = element.lcBalRoll;
				element3["tongHmConlai"] = element.hmLcConlai;
				element3["lmBalTongRollTyleKt"] = element.lmBalLcRollTyleKt;
				element3["treeStatus"] = 'disabled'
				this.listDataShow.push(element3);
			}
		});
		this.isEmpty = this.listData.length == 0;
		this.listDataShow = [...this.listDataShow]
		// this.listDataShow = this.listData = this.listDataShow;
	}
	search(pageableSearch) {
		this.formSearch.controls.branchCodes.setValue(this.facilityCode);
		this.formSearch.controls.rsId.setValue(this.objFunctionLimit?.rsId);
		this.formSearch.controls.scope.setValue(Scopes.VIEW);
		var dataSearch = this.formSearch.value;
		var listRmCode = dataSearch.rmCodes;
		if(listRmCode?.length == this.getListRM?.length && listRmCode?.length > 1000){
			dataSearch.rmCodes = [];
		}else{
			if(listRmCode?.length > 1000){
				this.messageService.error("Vui lòng chọn ít hơn 1000 RM hoặc chọn tất cả !");
				return;
			}
		}
		this.isLoading = true;	
		this.saleManagerApi.getLimitList(dataSearch, pageableSearch, this.size).subscribe((result) => {
			if (result) {
				this.listData = result?.content || [];
				this.pageable = {
					totalElements: result?.totalElements,
					totalPages: result?.totalPages,
					currentPage: pageableSearch,
					size: this.size,
				};
				this.isEmpty = this.listData.length == 0;
				if (this.listData != null || this.listData != undefined) {
					this.convertData(this.listData)
				}	
				this.isLoading = false;
			}
		},
			(e) => {
				if (e.error.code) {
					this.messageService.error(e.error.description);
					this.isLoading = false;
					return;
				}
				this.messageService.error(this.notificationMessage.error);
				this.isLoading = false;
			});
	}
	exportFile() {
		if (!this.maxExportExcel) {
     return;
   }
   if (+(this.pageable?.totalElements || 0) === 0) {
     this.messageService.warn(this.notificationMessage.noRecord);
     return;
   }
   if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
     this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
       this.messageService.warn(res);
     });
     return;
   }
   this.isLoading = true;
   this.saleManagerApi.exportLimitList(this.formSearch.value).subscribe(
     (fileId) => {
       if (!_.isEmpty(fileId)) {
         this.fileService
           .downloadFile(fileId, 'ds_han_muc.xlsx')
           .pipe(catchError((e) => of(false)))
           .subscribe((res) => {
             this.isLoading = false;
             if (!res) {
               this.messageService.error(this.notificationMessage.error);
             }
           });
       } else {
         this.messageService.error(this.notificationMessage?.export_error || '');
         this.isLoading = false;
       }
     },
     () => {
       this.messageService.error(this.notificationMessage?.export_error || '');
       this.isLoading = false;
     }
   );	
	}
	onActive(event) {
		if (event.type === 'dblclick' && event.cellIndex !== 0) {
			const item = _.get(event, 'row');
			if (!_.isEmpty(_.trim(item.customerCode))) {
				this.router.navigate([functionUri.customer_360_manager, 'detail', _.toLower(item.block), item.customerCode], {
					skipLocationChange: true,
					queryParams: {
						manageRM: item.rm,
						showBtnRevenueShare: false,
					},
				})
			}
		}
	}
  openItem(item){
    if (item.treeStatus === 'expanded') {
			item.treeStatus = 'collapsed';
		} else {
			item.treeStatus = 'expanded';
		}
    this.listDataShow = [...this.listDataShow];
  }

	onTreeAction(event: any) {
		const row = event.row;
		if (row.treeStatus === 'collapsed') {
			row.treeStatus = 'expanded';
		} else {
			row.treeStatus = 'collapsed';
		}
		this.listDataShow = [...this.listDataShow];
	}
	// changeBranch(event) {
	// 	let listAllRMInBranches = [];
	// 	if(event.length > 0){
	// 		event.forEach(element => {
	// 			let listRMInBranch:any;
	// 			listRMInBranch = _.filter(
	// 				this.commonData.listRmManagerTerm,
	// 				(item) => item.branchCode === element
	// 			  );			
	// 			listAllRMInBranches.push(...listRMInBranch) ; 
	// 		});
	// 	}else{
	// 		listAllRMInBranches = this.commonData.listRmManagerTerm; 
	// 	}
	// 	this.commonData.listRmManager = [...listAllRMInBranches];
	// 	this.objFunctionRM?.rsId;
	// }
	changeBranch(event) {
		let listAllRMInBranches = [];
    let listRmSelected = this.formSearch.get('rmCodes').value;
    let listRm = [];
		if(event.length > 0){
			event.forEach(element => {
				let listRMInBranch:any;
				listRMInBranch = _.filter(
					this.commonData.listRmManagerTerm,
					(item) => item.branchCode === element
				  );			
				listAllRMInBranches.push(...listRMInBranch) ; 
			});
      if(listRmSelected){
        listRmSelected.forEach(rm => {
          let checkRmSelect = _.filter(listAllRMInBranches,
              (item) => item.code === rm
          );
          if(checkRmSelect && checkRmSelect?.length > 0){
            listRm.push(rm)
          }
        })
      }
        
		}else{
      listAllRMInBranches = this.commonData.listRmManagerTerm; 
      listRm = [];
    }
    this.formSearch.get('rmCodes').setValue(listRm)
		this.commonData.listRmManager = [...listAllRMInBranches];
		this.objFunctionRM?.rsId;
	}
	setPage(pageInfo) {
		this.listDataShow = []
		if (this.isLoading) {
			return;
		}
		this.page = pageInfo.offset;
		this.search(this.page);
	}
	mailTo(row, mail) {
		if (_.isEmpty(mail)) {
		  this.trigger.forEach(item => {
			item.closeMenu();
		  });
		  this.messageService.error('Không có thông tin Email');
		} else {
		  const data = {
			type: 'EMAIL',
			customerCode: row.customerCode ? row.customerCode : '',
			leadCode: row.leadCode ? row.leadCode : '',
			scope: Scopes.VIEW,
			rsId: this.objFunctionCustomer.rsId
		  }
		  this.customerAssignmentApi.updateActivityLog(data).subscribe(value => {
		  });
		  const url = `mailto:${mail}`;
		  window.location.href = url;
		}
	}
	createTaskTodo(row) {
		const modal = this.modalService.open(CalendarAddModalComponent, { windowClass: 'create-calendar-modal' });
		modal.componentInstance.customerCode = row.customerCode;
		modal.componentInstance.customerName = row.customerName;
		modal.componentInstance.isDetailLead = true;
		modal.result
		   .then((res) => {
			  if (res) {
  
			  } else {
			  }
		   })
		   .catch(() => {
		   });
	 }
	 getAllMailAndPosition(row) {
		this.listEmail = [];
		const data = {
		  leadCode: '',
		  customerCode:  row.customerCode ? row.customerCode : '',
		  rsId :  this.objFunctionCustomer.rsId,
		  scope : 'VIEW'
		}
		this.service.getAllMail(data).subscribe(value => {
		  if (value) {
			this.listEmail = value.filter(e => e.mail !== undefined);
			if (_.isEmpty(this.listEmail)) {
			  this.mailTo(row, undefined)
			}
			this.listContactPosition.forEach(item => {
			  this.listEmail.forEach(e => {
				if (item.code === e.position) {
				  e.position = item.name;
				}
			  })
			});
		  }
		}, error => {
			this.trigger.forEach(item => {
			  item.closeMenu();
			});
			this.messageService.error(this.notificationMessage.error);
		  }
	  );
	  }
	openMore() {
	this.isOpenMore = !this.isOpenMore;
	}

}