import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalConfirmCreateChatComponent } from './modal-confirm-create-chat.component';

describe('ModalConfirmCreateChatComponent', () => {
  let component: ModalConfirmCreateChatComponent;
  let fixture: ComponentFixture<ModalConfirmCreateChatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalConfirmCreateChatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalConfirmCreateChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
