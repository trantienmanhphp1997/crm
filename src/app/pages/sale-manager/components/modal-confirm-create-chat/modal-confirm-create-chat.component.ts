import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import {ConfirmType, FunctionCode} from 'src/app/core/utils/common-constants';
import { SaleSuggestApi } from '../../api/sale-suggest.api';
import { BaseComponent } from 'src/app/core/components/base.component';
import _ from 'lodash';
import {GroupMbchatModalComponent} from '../list-group-mbchat-modal/group-mbchat-modal.component';
import {createGroupChat11} from '../../../../core/utils/mb-chat';

@Component({
  selector: 'app-modal-confirm-create-chat',
  templateUrl: './modal-confirm-create-chat.component.html',
  styleUrls: ['./modal-confirm-create-chat.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ModalConfirmCreateChatComponent extends BaseComponent implements OnInit {
  data: any;
  message: string | string[];
  groupNameInput: string;
  type: number;
  confirmType = ConfirmType;
  title: string;
  isConfirm = true;
  disableConfirm = false;
  isChoose = false;
  isCopy: boolean;
  background = 'background-white';
  listMessage: string[] = [];
  content = '';
  color = 'light-blue';

  groupName = '';
  remaining = 40;

  isValid = false;
  hasErrDuplicate = false;

  constructor(injector: Injector, private modal: NgbActiveModal, private saleSuggestApi: SaleSuggestApi) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.APP_CHAT}`);
  }

  ngOnInit(): void {
    if (this.message && this.message instanceof Array) {
      for (const mes of this.message) {
        this.listMessage.push(mes);
      }
    } else {
      const mes: any = this.message;
      this.listMessage.push(mes);
    }
    this.isConfirm = this.type === ConfirmType.Confirm || !this.type;
    if (this.type === ConfirmType.CusError) {
      this.background = 'background-red';
      this.color = 'red';
    } else if (this.type === ConfirmType.Warning) {
      this.background = 'background-yellow';
      this.color = 'yellow';
    } else {
      this.background = 'background-white';
    }
  }

  close(event: boolean) {
    this.modal.close(event);
  }

  createGroup() {
    this.isLoading = true;
    const data = this.data;
    data.groupName = this.groupName.trim();
    data.isCheckFilter = true;
    this.saleSuggestApi.groupChat(data).subscribe(
      (response: any) => {
        if (response.code === '0') {
          if (response.createGroup) {
            // tạo nhóm thành công
            if (this.objFunction) {
              this.messageService.success('Tạo nhóm chat thành công!');
              createGroupChat11(response?.data?.groupId, true);
            } else {
              this.messageService.success('Tạo nhóm chat thành công. Truy cập ứng dụng Chat để gửi tin nhắn tới nhóm vừa tạo');
            }
            let event = {
              code: 0,
            };
            this.modal.close(event);
          } else {
            // hiện popup trùng
            const confirm = this.modalService.open(GroupMbchatModalComponent, { windowClass: 'list__group-mbchat-modal' });
            confirm.componentInstance.listData = response.data;
            confirm.componentInstance.data = data;
            confirm.result
              .then((res) => {
                this.modal.close();
              })
              .catch(() => {});
          }
        } else if (response.code === '4011') {
          // Trùng tên nhóm
          this.hasErrDuplicate = true;
          this.isValid = false;
        }
        this.isLoading = false;
      },
      (e) => {
        console.log(e);
        this.messageService.error(_.get(e, 'error.description'));
        this.isLoading = false;
      }
    );
  }

  valueChange(groupName) {
    this.remaining = 40 - groupName.length;
    this.groupName = groupName;
    if (this.groupName.trim().length > 0) {
      this.isValid = true;
    } else {
      this.isValid = false;
    }
  }

  hasErrDup() {
    return (this.hasErrDuplicate && !this.isValid) ? 'border-danger' : '';
  }
}
