import { Component, OnInit, Injector, ViewChild, ViewEncapsulation, HostBinding, Input } from '@angular/core';
import { DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { global } from '@angular/compiler/src/util';
import { CommonCategory, maxInt32, Scopes, ScreenType, SessionKey } from 'src/app/core/utils/common-constants';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { SaleManagerApi } from '../../api';
import { LevelApi } from 'src/app/pages/kpis/apis';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-product-modal',
  templateUrl: './product-modal.component.html',
  styleUrls: ['./product-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [LevelApi],
})
export class ProductModalComponent extends BaseComponent implements OnInit {
  isLoading = false;
  @ViewChild('tablerm') public tablerm: DatatableComponent;
  @HostBinding('class.list__rm-content') appRightContent = true;

  @Input() customerDivision: string;
  @Input() rmLevelCode: string;
  @Input() listSelectedOld = [];

  params: any = {
    pageNumber: 0,
    pageSize: global?.userConfig?.pageSize,
  };
  pageable: Pageable;

  search_value: string;
  listData = [];

  searchType: boolean;

  checkAll = false;
  listSelected = [];
  listProduct = [];

  countCheckedOnPage = 0;

  search_form = this.fb.group({
    search: [''],
    rmLevelCode: [''],
    divisionCode: [''],
  });

  listDivision = [];
  limit = global.userConfig.pageSize;
  listSelectBox = [];
  listLevelRm = [];
  listLevelRmAll = [];

  constructor(
    injector: Injector,
    private modalActive: NgbActiveModal,
    private api: SaleManagerApi,
    private levelApi: LevelApi
  ) {
    super(injector);
  }

  ngOnInit() {
    this.isLoading = true;
    const param = {
      blockCode: 'all',
      page: 0,
      size: maxInt32,
      isActive: true,
    };

    const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
    forkJoin([
      this.commonService.getCommonCategory(CommonCategory.KHOI_PRIORITY).pipe(catchError(() => of(undefined))),
      this.levelApi.fetch(param).pipe(catchError(() => of(undefined))),
    ]).subscribe(([listConfigDivision, listLevelRm]) => {
      listConfigDivision = listConfigDivision?.content || [];
      listConfigDivision = _.orderBy(listConfigDivision, ['value']);
      let divisionConfig = listConfigDivision?.map((item) => item.code);
      this.listDivision =
        divisionOfUser?.map((item) => {
          return {
            code: item.code,
            displayName: item.code + ' - ' + item.name,
            order:
              _.findIndex(divisionConfig, (value) => value === item.code) === -1
                ? 99
                : _.findIndex(divisionConfig, (value) => value === item.code),
          };
        }) || [];
      this.search_form.controls.divisionCode.setValue(_.first(this.listDivision)?.code);
      this.listLevelRmAll = [...listLevelRm?.content];
      this.listLevelRm = listLevelRm?.content
        .filter((item) => item.blockCode === _.first(this.listDivision)?.code)
        .map((i) => {
          return {
            code: i.code,
            displayName: i.name,
          };
        });

      this.search_form.controls.rmLevelCode.setValue(_.first(this.listLevelRm)?.code);
      this.reload(true);
    });

    if (this.listSelectedOld.length > 0) {
      this.listProduct = [...this.listSelectedOld];
    }
  }

  paging($event) {
    if (this.isLoading) {
      return;
    }
    this.params.page = _.get($event, 'offset');
    this.reload(false);
  }

  reload(isSearch?: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.params.page = 0;
    }
    this.api
      .getOppProductRule(
        this.search_form.controls.rmLevelCode.value,
        this.customerDivision,
        this.search_form.controls.search.value,
        this.params.page,
        this.params.pageSize
      )
      .subscribe(
        (response) => {
          this.isLoading = false;
          this.listData = _.get(response, 'content');
          this.countCheckedOnPage = 0;
          for (const item of this.listData) {
            if (
              this.listProduct.findIndex((element) => {
                return element.productCode === item.productCode;
              }) > -1
            ) {
              this.listSelected.push(item);
              this.countCheckedOnPage += 1;
            }
          }
          this.checkAll = this.listData.length > 0 && this.countCheckedOnPage === this.listData.length;

          this.pageable = {
            totalElements: response.totalElements,
            totalPages: response.totalPages,
            currentPage: response.number,
            size: this.limit,
          };
        },
        () => {
          this.isLoading = false;
        }
      );
  }

  choose() {
    const listData = _.uniqBy(this.listSelected, 'productCode');
    const data = {
      listSelected: listData,
    };
    this.modalActive.close(data);
  }

  closeModal() {
    this.modalActive.close(false);
  }

  onCheckboxRmAllFn(isChecked) {
    this.checkAll = isChecked;
    if (!isChecked) {
      this.countCheckedOnPage = 0;
    } else {
      this.countCheckedOnPage = this.listData.length;
    }
    for (const item of this.listData) {
      item.isChecked = isChecked;
      if (isChecked) {
        if (
          this.listProduct.findIndex((element) => {
            return element.productCode === item.productCode;
          }) === -1
        ) {
          this.listProduct.push(item);
          this.listSelected.push(item);
        }
      } else {
        this.listProduct = this.listProduct.filter((element) => {
          return element.productCode !== item.productCode;
        });
        this.listSelected = this.listSelected.filter((itemChoose) => {
          return itemChoose.productCode !== item.productCode;
        });
      }
    }
  }

  onCheckboxRmFn(item, isChecked) {
    if (isChecked) {
      this.listSelected.push(item);
      this.listProduct.push(item);
      this.countCheckedOnPage += 1;
    } else {
      this.listSelected = this.listSelected.filter((itemSelected) => {
        return itemSelected.productCode !== item.productCode;
      });
      this.listProduct = this.listProduct.filter((element) => {
        return element.productCode !== item.productCode;
      });
      this.countCheckedOnPage -= 1;
    }
    this.checkAll = this.countCheckedOnPage === this.listData.length;
  }

  isChecked(item) {
    return (
      this.listProduct.findIndex((element) => {
        return element.productCode === item.productCode;
      }) > -1
    );
  }

  changeDivision(event) {
    this.listLevelRm = this.listLevelRmAll
      .filter((item) => item.blockCode === event.value)
      .map((i) => {
        return {
          code: i.code,
          displayName: i.name,
        };
      });
    this.search_form.controls.rmLevelCode.setValue(_.first(this.listLevelRm)?.code);
  }
}
