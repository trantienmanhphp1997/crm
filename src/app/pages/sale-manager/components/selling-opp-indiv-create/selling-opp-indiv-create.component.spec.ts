import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellingOppIndivCreateComponent } from './selling-opp-indiv-create.component';

describe('SellingOppIndivCreateComponent', () => {
  let component: SellingOppIndivCreateComponent;
  let fixture: ComponentFixture<SellingOppIndivCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellingOppIndivCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellingOppIndivCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
