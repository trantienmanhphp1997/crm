import { global } from '@angular/compiler/src/util';
import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import _ from 'lodash';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import {
  ActionType,
  CommonCategory,
  Division,
  FormType,
  FunctionCode,
  functionUri,
  maxInt32,
  Scopes,
  SessionKey,
} from 'src/app/core/utils/common-constants';
import { CustomerDetailSmeApi } from 'src/app/pages/customer-360/apis/customer.api';
import { RmBlockApi } from 'src/app/pages/rm/apis';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { SaleManagerApi } from '../../api';
import { MatDialog } from '@angular/material/dialog';
import { DialogConfirmComponent } from './dialog-confirm/dialog-confirm.component';

@Component({
  selector: 'app-selling-opp-indiv-create',
  templateUrl: './selling-opp-indiv-create.component.html',
  styleUrls: ['./selling-opp-indiv-create.component.scss'],
})
export class SellingOppIndivCreateComponent extends BaseComponent implements OnInit {
  isLoading = false;
  @ViewChild('table') table: DatatableComponent;
  @ViewChild('tableTarget') tableTarget: DatatableComponent;
  listBranches = [];
  listDivision = [];

  formCheckInfo = this.fb.group({
    textSearch: ['', Validators.required],
    divisionCode: null,
    searchQuickType: null,
  });
  listSearchTypeSME = [
    { name: 'Khách hàng hiện hữu', code: 'KHHH' },
    { name: 'Khách hàng tiềm năng', code: 'KHTN' },
    { name: 'Số ĐKKD', code: 'DKKD' },
  ]

  url: string;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private saleManagerApi: SaleManagerApi,
    private rmBlockApi: RmBlockApi,
    private customerDetailSmeApi: CustomerDetailSmeApi,
    public dialog: MatDialog
  ) {
    super(injector);
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.SELLING_OPPORTUNITY_INDIV}`);

    this.url = _.get(this.route.snapshot.queryParams, 'url');
  }

  showMessage = false;
  btnCheckCustomer = true;

  textSearch = '';
  searchQuickType = '';
  listSearchType = [
    { name: 'Mã KH', code: 'custId' },
    { name: 'Số GTTT', code: 'nationalId' },
    { name: 'SĐT', code: 'mobileNumber' },
  ];

  ngOnInit() {
    this.initData();
    this.formCheckInfo.controls.divisionCode.valueChanges.subscribe(value => {
      if (value != Division.INDIV) {
        this.formCheckInfo.controls.searchQuickType.setValue(this.listSearchTypeSME[0]?.code);
      } else {
        this.formCheckInfo.controls.searchQuickType.setValue(this.listSearchType[0]?.code);
      }
    });
    this.formCheckInfo.controls.divisionCode.setValue(this.prop?.formSearch?.customerTypeMerge);
  }

  initData() {
    // KHOI
    let listDivision = _.map(this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM), (item) => {
      return {
        titleCode: item.titleCode,
        levelCode: item.levelCode,
        code: item.code,
        displayName: `${item.code} - ${item.name}`,
      };
    });
    this.listDivision = listDivision;
  }

  onChangeEventType(event) { }

  onChangeCustomerType(event) { }

  searchOpp() {
    this.isLoading = true;
    const body = {
      code: this.formCheckInfo.controls.textSearch.value,
      block: this.formCheckInfo.controls.divisionCode.value,
      type: this.formCheckInfo.controls.searchQuickType.value
    };
    const api = this.formCheckInfo.controls.divisionCode.value == Division.INDIV ? this.saleManagerApi.searchOpportunityDetailFirst(
      this.formCheckInfo.controls.textSearch.value.trim(),
      this.formCheckInfo.controls.searchQuickType.value
    ) : this.saleManagerApi.getCustomerInfoOpportunity(body);

    api.subscribe(
      (res) => {
        if (this.formCheckInfo.controls.divisionCode.value != Division.INDIV) {
          if (!res) {
            const dialogRef = this.dialog.open(DialogConfirmComponent, {
              width: '300px'
            });
            dialogRef.afterClosed().subscribe(res => {
              if (res) {
                const url = this.router.serializeUrl(this.router.createUrlTree([functionUri.lead_management, 'create'], {
                  state: ({}),
                  queryParams: {
                    customerType: '1',
                    haveInDiv: '1',
                    haveSME: '1'
                  }
                }));
                window.open(url, '_blank');
              }
            });
          } else {
            this.router.navigate(['sale-manager/selling-opp-sme', 'create'], {
              queryParams: {
                customerCode: this.formCheckInfo.controls.textSearch.value,
                blockCode: this.formCheckInfo.controls.divisionCode.value,
                searchQuickType: this.formCheckInfo.controls.searchQuickType.value
              },
              state: {
                formSearch : this.state?.formSearch
              }
            });
          }
          this.isLoading = false;
          return;
        }
        if (res) {
          this.showMessage = false;
          this.router.navigate([this.url, 'detail-v2'], {
            skipLocationChange: true,
            queryParams: {
              customerCode: res.customerCode,
              code: null,
              // actionType: ActionType.CREATE,
              // formType: FormType.CREATE,
              // customerCode: res.customerCode,
              // code: null,
              // customerName: res.customerName,
              // customerBranchCode: res.customerBranchCode,
              // customerBranchName: res.customerBranchName,
              // gender: res.gender,
              // identifiedIssueDate: res.identifiedIssueDate,
              // identifiedNumber: res.identifiedNumber,
              // priorityName: res.priorityName,
              // phone: res.phone,
              // address: res.address,
              // rmManagement: res.rmManagement,
              // email: res.email,
            },
          });
        } else {
          this.showMessage = true;
        }
        this.isLoading = false;
      },
      (e) => {
        this.isLoading = false;
        if (this.formCheckInfo.controls.divisionCode.value != Division.INDIV) {
          const dialogRef = this.dialog.open(DialogConfirmComponent, {
            width: '40%'
          });
          dialogRef.afterClosed().subscribe(res => {
            if (res) {
              const url = this.router.serializeUrl(this.router.createUrlTree([functionUri.lead_management, 'create'], {
                state: ({}),
                queryParams: {
                  customerType: '1',
                  haveInDiv: '1',
                  haveSME: '1'
                }
              }));
              window.open(url, '_blank');
            }
          });
          return;
        }
        console.log(e);
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }

  validateSpace() {
    let result = false;
    const text = this.formCheckInfo.controls.textSearch.value;
    if (text.trim().length > 0) {
      result = true;
    }

    return result;
  }

  back() {
    this.router.navigateByUrl('/sale-manager/selling-opp-indiv', { state: this.prop });
  }
}
