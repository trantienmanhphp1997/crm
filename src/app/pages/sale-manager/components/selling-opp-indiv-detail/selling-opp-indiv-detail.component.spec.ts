import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellingOppIndivDetailComponent } from './selling-opp-indiv-detail.component';

describe('SellingOppIndivDetailComponent', () => {
  let component: SellingOppIndivDetailComponent;
  let fixture: ComponentFixture<SellingOppIndivDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellingOppIndivDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellingOppIndivDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
