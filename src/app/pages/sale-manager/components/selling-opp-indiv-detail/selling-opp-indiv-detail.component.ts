import { forkJoin, of } from 'rxjs';
import { CustomerDetailApi } from 'src/app/pages/customer-360/apis';
import {
  ActionType,
  AgeGroup,
  CommonCategory,
  FormType,
  FunctionCode,
  maxInt32,
  ProductLevel,
  Scopes,
  ScreenType,
  SessionKey,
  TaskType,
} from '../../../../core/utils/common-constants';
import { AfterViewInit, Component, Injector, OnDestroy, OnInit, ViewChild } from '@angular/core';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Utils } from '../../../../core/utils/utils';
import { ViewEncapsulation } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { ActivityActionComponent } from 'src/app/shared/components/activity-action/activity-action.component';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { Validators } from '@angular/forms';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { SaleManagerApi } from '../../api';
import { cleanDataForm } from 'src/app/core/utils/function';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { CreateTaskModalComponent } from 'src/app/pages/tasks/components/create-task-modal/create-task-modal.component';
import { ActivationStart } from '@angular/router';

@Component({
  selector: 'app-selling-opp-indiv-detail',
  templateUrl: './selling-opp-indiv-detail.component.html',
  styleUrls: ['./selling-opp-indiv-detail.component.scss'],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: false },
    },
  ],
  encapsulation: ViewEncapsulation.None,
})
export class SellingOppIndivDetailComponent extends BaseComponent implements OnInit, AfterViewInit {
  formCreate = this.fb.group({
    productCode: ['', Validators.required],
    branchCode: ['', Validators.required],
    memberCode: [null],
    currency: ['', Validators.required],
    amount: [null, Validators.required],
    status: [null, Validators.required],
    guideCode: [null],
    note: [''],
  });

  formUpdate = this.fb.group({
    productCode: ['', Validators.required],
    branchCode: ['', Validators.required],
    memberCode: [null],
    currency: ['', Validators.required],
    amount: [null, Validators.required],
    status: [null, Validators.required],
    guideCode: [null],
    note: [''],
  });

  oldFormUpdate: any;
  oldFormCreate: any;
  formCreateChange = false;
  formUpdateChange = false;

  amountModelCreate = '';
  amountModelUpdate = '';

  listProduct = [];
  listBranches = [];
  listRmManager = [];
  listCurrencies = [];
  listStatusOpp = [];
  listPresenter = [];

  dataDivisionBranch: any = {};

  actionType = '';
  formType = '';

  customerCode: string;
  code: string;
  canEdit: boolean;
  isDisableSaveUpdate = true;
  isDisableSaveCreate = true;
  isDisableCancel = true;
  isDisableFormUpdate = true;

  customerName: string;
  customerBranchCode: string;
  customerBranchName: string;
  gender: string;
  identifiedIssueDate: string;
  identifiedNumber: string;
  priorityName: string;
  phone: string;
  address: string;
  rmManagement: string;
  email: string;

  model: any = {};
  listBranchCode = [];

  isShow = false;

  detailInfo: any;

  accordianData = [];

  prevParams: any;
  detailFormUpdate: any = {};

  oppCode = '';
  saleOppId = '';

  scopes: Array<any>;
  cusCode: string;

  constructor(
    injector: Injector,
    private saleManagerApi: SaleManagerApi,
    private categoryService: CategoryService,
    private customerDetailApi: CustomerDetailApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.SELLING_OPPORTUNITY_INDIV}`);
    this.isLoading = true;
    this.scopes = _.get(this.objFunction, 'scopes');
    this.actionType = _.get(this.route.snapshot.queryParams, 'actionType');
    this.formType = _.get(this.route.snapshot.queryParams, 'formType');
    // dbclick row
    this.customerCode = _.get(this.route.snapshot.queryParams, 'customerCode');
    this.code = _.get(this.route.snapshot.queryParams, 'code');
    this.canEdit = (_.get(this.route.snapshot.queryParams, 'canEdit') === 'true');

    // click create
    this.customerName = _.get(this.route.snapshot.queryParams, 'customerName');
    this.customerBranchCode = _.get(this.route.snapshot.queryParams, 'customerBranchCode');
    this.customerBranchName = _.get(this.route.snapshot.queryParams, 'customerBranchName');
    this.gender = _.get(this.route.snapshot.queryParams, 'gender');
    this.identifiedIssueDate = _.get(this.route.snapshot.queryParams, 'identifiedIssueDate');
    this.identifiedNumber = _.get(this.route.snapshot.queryParams, 'identifiedNumber');
    this.priorityName = _.get(this.route.snapshot.queryParams, 'priorityName');
    this.phone = _.get(this.route.snapshot.queryParams, 'phone');
    this.address = _.get(this.route.snapshot.queryParams, 'address');
    this.rmManagement = _.get(this.route.snapshot.queryParams, 'rmManagement');
    // this.actionType = _.get(this.route.snapshot.queryParams, 'code');
    this.email = _.get(this.route.snapshot.queryParams, 'email');
  }

  tab_index = 0;
  @ViewChild('sellingOppActivity') sellingOppActivity: any;

  ngOnInit(): void {
    this.initData();
  }

  initData() {
    this.getDetailsForDisplay();
  }

  getDetailsForDisplay() {
    if (this.actionType === ActionType.CREATE) {
      this.detailInfo = {
        customerCode: this.customerCode,
        customerName: this.customerName,
        customerBranchCode: this.customerBranchCode,
        customerBranchName: this.customerBranchName,
        gender: this.gender,
        identifiedIssueDate: this.identifiedIssueDate,
        identifiedNumber: this.identifiedNumber,
        priorityName: this.priorityName,
        phone: this.phone,
        address: this.address,
        rmManagement: this.rmManagement,
        email: this.email,
      };
      forkJoin([
        this.saleManagerApi.opportunityINDIVDetail(this.customerCode, this.code, this.objFunction.rsId, Scopes.VIEW),
      ]).subscribe(
        ([oppDetail]) => {
          const accordianDataSort = oppDetail.sort((a, b) => Number(b.selected) - Number(a.selected));
          const accordianData = accordianDataSort.map((item, index) => {
            item.id = index;
          });
          this.accordianData = accordianDataSort;
          this.setDataFilter();
        },
        () => {
          this.isLoading = false;
          this.messageService.error(this.notificationMessage.error);
        }
      );
      this.valueChangeAmountCreate();
    } else if (this.actionType === ActionType.UPDATE) {
      forkJoin([
        this.saleManagerApi.opportunityINDIVDetail(this.customerCode, this.code, this.objFunction.rsId, Scopes.VIEW),
        this.saleManagerApi.searchOpportunityDetailFirst(this.customerCode, 'custId'),
        this.saleManagerApi.getOppDetailForEdit(this.code),
      ]).subscribe(
        ([oppDetail, oppCusDetail, detailForUpdate]) => {
          this.detailInfo = oppCusDetail;
          const accordianDataSort = oppDetail.sort((a, b) => Number(b.selected) - Number(a.selected));
          const accordianData = accordianDataSort.map((item, index) => {
            item.id = index;
          });
          this.accordianData = accordianDataSort;
          this.detailFormUpdate = detailForUpdate;
          this.oppCode = detailForUpdate.code;
          this.saleOppId = detailForUpdate.saleOppId;
          this.setDataFilter();
        },
        () => {
          this.isLoading = false;
          this.messageService.error(this.notificationMessage.error);
        }
      );
      this.valueChangeAmountUpdate();
    }
  }

  valueChangeAmountCreate() {
    this.formCreate.controls.amount.valueChanges.subscribe((value) => {
      if (!_.isEmpty(value)) {
        const reg = new RegExp(/^[0-9]*$/g);
        value = value.toString().replace(/,/g, '');
        if (!reg.test(value)) {
          value = value.replace(/\D/g, '');
        }
        this.formCreate.controls.amount.setValue(Utils.numberWithCommas(value).substr(0, 20), { emitEvent: false });
      }
    });
  }

  valueChangeAmountUpdate() {
    this.formUpdate.controls.amount.valueChanges.subscribe((value) => {
      if (!_.isEmpty(value)) {
        const reg = new RegExp(/^[0-9]*$/g);
        value = value.toString().replace(/,/g, '');
        if (!reg.test(value)) {
          value = value.replace(/\D/g, '');
        }
        this.formUpdate.controls.amount.setValue(Utils.numberWithCommas(value).substr(0, 20), { emitEvent: false });
      }
    });
  }

  setDataFilter() {
    forkJoin([
      this.saleManagerApi.getProductByLevel().pipe(catchError((e) => of(undefined))),
      this.saleManagerApi.getBranches(this.objFunction?.rsId, Scopes.VIEW).pipe(catchError((e) => of(undefined))),
      this.categoryService.getCommonCategory(CommonCategory.CURRENCY_TYPE).pipe(catchError(() => of(undefined))),
      this.saleManagerApi.getPresenter(),
    ]).subscribe(([listProduct, listBranches, listCurrencyType, listPresenter]) => {
      // PRODUCT
      this.listProduct = listProduct?.map((item) => {
        return { code: item.idProductTree, displayName: item.idProductTree + ' - ' + item.description };
      });

      let product = '';
      if (this.formType === FormType.CREATE) {
        product = _.head(this.listProduct)?.code;
      } else if (this.formType === FormType.UPDATE) {
        product = String(this.detailFormUpdate?.productCode);
      }
      this.getFilterStatus(product);

      // BRANCHES
      const ltBranches = listBranches?.map((item) => {
        return {
          code: item.code,
          displayName: item.code + ' - ' + item.name,
          selected: item.selected,
        };
      });
      const listBranchesTemp = ltBranches.sort((a, b) => Number(b.selected) - Number(a.selected));
      this.listBranches = listBranchesTemp;

      //CURRENCY
      this.listCurrencies = _.get(listCurrencyType, 'content');

      // PRESENTER
      this.listPresenter = listPresenter?.map((item, index) => {
        const code = item.code ? item.code : '';
        const fullName = item.fullName ? item.fullName : '';
        return {
          code: item.code,
          displayName: code + ' - ' + fullName,
          selected: item.selected,
          guideHrsCode: item.hrsCode,
          index,
        };
      });

      // Check action & set data form
      if (this.formType === FormType.CREATE) {
        this.resetFormCreateDefault();
      } else if (this.formType === FormType.UPDATE) {
        this.setFormUpdateDefault();
      }
    });
  }

  getFilterStatus(product: string) {
    this.listStatusOpp = [];
    this.saleManagerApi.getStatusProduct(product).subscribe(
      (listStatusOpp) => {
        // STATUS
        if (listStatusOpp?.length > 0) {
          this.listStatusOpp = listStatusOpp?.map((item) => {
            return { code: item?.index, displayName: item?.title };
          });
        }
      },
      (e) => {
        console.log(e);
        this.messageService.error('Thực hiện không thành công');
        this.isLoading = false;
      }
    );
  }

  getRmManager(listBranch) {
    // this.oldFormCreate = {};
    // this.oldFormUpdate = {};
    this.listRmManager = [];
    const branchCodes = listBranch?.filter((code) => !Utils.isStringEmpty(code));
    this.saleManagerApi.getRMUBByBranches(branchCodes).subscribe((res) => {
      // console.log(res);
      const listRm: any[] =
        res?.map((item, index) => {
          return {
            rmCode: item?.code,
            displayName: Utils.trimNullToEmpty(item?.code) + ' - ' + Utils.trimNullToEmpty(item?.fullName),
            memberHrsCode: item?.hrsCode,
            index,
          };
        }) || [];
      this.listRmManager = listRm;

      if (this.formType === FormType.CREATE) {
        this.formCreate.get('memberCode').setValue(null);

        // set old value
        // console.log(this.formCreate.value);
        const formValueCreate = this.formCreate.value;
        this.oldFormCreate = { ...formValueCreate };
      } else if (this.formType === FormType.UPDATE) {
        let memberHrsCode = this.listRmManager.filter(
          (item) => item.memberHrsCode === this.detailFormUpdate?.memberHrsCode
        );
        this.formUpdate.get('memberCode').setValue(memberHrsCode[0]?.index);

        // set old value
        // console.log(this.formUpdate.value);
        const formValueUpdate = this.formUpdate.value;
        this.oldFormUpdate = { ...formValueUpdate };
      }

      this.displayButtons();
      this.checkDisableFormUpdate();
      
      this.isLoading = false;
    });
  }

  onChangeBranch(event) {
    if (this.formType === FormType.CREATE) {
      this.formCreate.controls.memberCode.setValue(null);
    } else if (this.formType === FormType.UPDATE) {
      this.formUpdate.controls.memberCode.setValue(null);
    }
    this.getRmManager([event.value]);
  }

  onChangeProduct(event) {
    if (this.formType === FormType.CREATE) {
      this.formCreate.controls.status.setValue(null);
    } else if (this.formType === FormType.UPDATE) {
      this.formUpdate.controls.status.setValue(null);
    }
    this.getFilterStatus(event.value);
  }

  onChangeAmount(value) {}

  keyPressNumbers(event) {
    var charCode = event.which ? event.which : event.keyCode;
    // Only Numbers 0-9
    if (charCode < 48 || charCode > 57) {
      event.preventDefault();
      return false;
    } else {
      return true;
    }
  }

  showSideBar() {}

  mailTo() {
    const url = `mailto:${this.detailInfo?.email}`;
    window.location.href = url;
  }

  createActivity() {
    const parent: any = {};
    parent.parentType = TaskType.CUSTOMER;
    parent.parentId = this.customerCode;
    parent.parentName = this.customerName;
    const modal = this.modalService.open(ActivityActionComponent, {
      windowClass: 'create-activity-modal',
      scrollable: true,
    });
    modal.componentInstance.parent = parent;
    modal.componentInstance.type = ScreenType.Create;
    modal.componentInstance.isShowFuture = true;
  }

  createTaskTodo() {
    const data: any = {};
    data.parentId = this.customerCode;
    data.parentName = this.customerName;
    data.parentType = TaskType.CUSTOMER;
    const taskModal = this.modalService.open(CreateTaskModalComponent, {
      windowClass: 'create-task-modal',
      scrollable: true,
    });
    taskModal.componentInstance.data = data;
  }

  handleEditClick(event) {
    console.log(event);
    this.oppCode = event.code;
    this.saleOppId = event.saleOppId;
    if (this.formType === FormType.CREATE) {
      if (this.formCreateChange) {
        this.confirmChangeFormType();
      } else {
        this.getDetailForUpdate();
      }
    } else if (this.formType === FormType.UPDATE) {
      if (this.formUpdateChange) {
        this.confirmChangeFormType();
      } else {
        this.getDetailForUpdate();
      }
    }
  }

  confirmChangeFormType() {
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.componentInstance.message =
      'Cơ hội đang khai báo chưa được hoàn tất. Bạn chắc chắn muốn chuyển sang cơ hội khác?';
    confirm.result
      .then((res) => {
        if (res) {
          this.getDetailForUpdate();
        }
      })
      .catch(() => {});
  }

  getDetailForUpdate() {
    this.isLoading = true;
    this.saleManagerApi.getOppDetailForEdit(this.oppCode).subscribe(
      (resDetail) => {
        this.detailFormUpdate = resDetail;
        this.canEdit = true;
        this.getFilterStatus(String(this.detailFormUpdate?.productCode));
        this.setFormUpdateDefault();
      },
      (e) => {
        if (e.error.code) {
          this.messageService.error(e.error.description);
          this.isLoading = false;
          return;
        }
        this.messageService.error(this.notificationMessage.error);
        this.isLoading = false;
      }
    );
  }

  cancel() {
    this.formType = FormType.CREATE;
    this.amountModelCreate = '';
    this.amountModelUpdate = '';
    this.valueChangeAmountCreate();
    this.valueChangeAmountUpdate();
    this.formUpdateChange = false;
    // reset ve form create default
    this.resetFormCreateDefault();
    this.code = '';
  }

  // reset ve form create default
  resetFormCreateDefault() {
    this.formType = FormType.CREATE;
    this.formCreate.get('productCode').setValue('');
    const branchSelected = this.listBranches.filter((item) => item.selected == true);
    this.formCreate.get('branchCode').setValue(branchSelected[0]?.code);
    this.formCreate.get('currency').setValue('VND');
    this.formCreate.get('amount').setValue('');
    this.formCreate.get('status').setValue(null);
    const presenterSelected = this.listPresenter.filter((item) => item.selected == true);

    this.formCreate.get('guideCode').setValue(presenterSelected[0]?.index);
    this.formCreate.get('note').setValue('');

    this.getRmManager([this.formCreate.controls.branchCode.value]);
  }

  // set form update
  setFormUpdateDefault() {
    this.formType = FormType.UPDATE;
    this.formUpdate.get('productCode').setValue(String(this.detailFormUpdate?.productCode));
    this.formUpdate.get('branchCode').setValue(this.detailFormUpdate?.branchCode);
    this.formUpdate.get('currency').setValue(this.detailFormUpdate?.currency);
    this.formUpdate.get('amount').setValue(Utils.numberWithCommas(this.detailFormUpdate?.amount));
    this.amountModelUpdate = Utils.numberWithCommas(this.detailFormUpdate?.amount);
    this.formUpdate.get('status').setValue(this.detailFormUpdate?.status);
    let guideHrsCode = this.listPresenter.filter((item) => item.guideHrsCode === this.detailFormUpdate?.guideHrsCode);
    this.formUpdate.get('guideCode').setValue(guideHrsCode[0]?.index);
    this.formUpdate.get('note').setValue(this.detailFormUpdate?.note);

    this.getRmManager([this.formUpdate.controls.branchCode.value]);
  }

  checkScopes(codes: Array<any>) {
    if (Utils.isArrayEmpty(codes)) return false;
    return Utils.isArrayNotEmpty(_.filter(this.scopes, (x) => codes.includes(x)));
  }

  displayButtons() {
    this.checkDisableSaveCreate();
    this.checkDisableSaveUpdate();
    this.checkDisableCancel();
  }

  checkDisableSaveCreate() {
    this.isDisableSaveCreate = (this.formCreate.valid && this.checkScopes(['CREATE'])) ? false : true;
  }

  checkDisableSaveUpdate() {
    this.isDisableSaveUpdate = (this.formUpdate.valid && this.canEdit && this.checkScopes(['UPDATE'])) ? false : true;
  }

  checkDisableCancel() {
    this.isDisableCancel = this.checkScopes(['CREATE']) ? false : true;
  }

  checkDisableFormUpdate() {
    this.isDisableFormUpdate = (this.canEdit && this.checkScopes(['UPDATE'])) ? false : true;
  }

  save() {
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.componentInstance.message = 'Bạn có muốn lưu cơ hội bán?';
    confirm.result
      .then((res) => {
        if (res) {
          this.isLoading = true;
          // TH create
          if (this.formType === FormType.CREATE) {
            let paramCreate = {
              ...cleanDataForm(this.formCreate),
            };
            paramCreate.customerCode = this.customerCode;
            let outputAmount = parseFloat(this.amountModelCreate.replace(/,/g, ''));
            paramCreate.amount = outputAmount;
            let memberHrsCode = this.listRmManager.filter((item) => item.index === paramCreate.memberCode);
            let guideHrsCode = this.listPresenter.filter((item) => item.index === paramCreate.guideCode);
            paramCreate.memberCode = memberHrsCode[0]?.rmCode;
            paramCreate.memberHrsCode = memberHrsCode[0]?.memberHrsCode;
            paramCreate.guideCode = guideHrsCode[0]?.code;
            paramCreate.guideHrsCode = guideHrsCode[0]?.guideHrsCode;
            // console.log(paramCreate);
            this.saleManagerApi.createOpportunityINDIV(paramCreate).subscribe(
              (resCreate) => {
                this.getDetailRight();
              },
              () => {
                this.isLoading = false;
                this.messageService.error(this.notificationMessage.error);
              }
            );
          }

          // TH update
          if (this.formType === FormType.UPDATE) {
            let paramUpdate = {
              ...cleanDataForm(this.formUpdate),
            };
            let outputAmount = parseFloat(this.amountModelUpdate.replace(/,/g, ''));
            paramUpdate.amount = outputAmount;
            paramUpdate.code = this.oppCode;
            paramUpdate.saleOppId = this.saleOppId;

            let memberHrsCode = this.listRmManager.filter((item) => item.index === paramUpdate.memberCode);
            let guideHrsCode = this.listPresenter.filter((item) => item.index === paramUpdate.guideCode);
            paramUpdate.memberCode = memberHrsCode[0]?.rmCode;
            paramUpdate.memberHrsCode = memberHrsCode[0]?.memberHrsCode;
            paramUpdate.guideCode = guideHrsCode[0]?.code;
            paramUpdate.guideHrsCode = guideHrsCode[0]?.guideHrsCode;
            // console.log(paramUpdate);

            this.saleManagerApi.updateOpportunityINDIV(paramUpdate).subscribe(
              (resUpdate) => {
                this.formType = FormType.CREATE;
                this.getDetailRight();
              },
              () => {
                this.isLoading = false;
                this.messageService.error(this.notificationMessage.error);
              }
            );
          }
        }
      })
      .catch(() => {});
  }

  getDetailRight() {
    this.saleManagerApi
      .opportunityINDIVDetail(this.customerCode, this.code, this.objFunction.rsId, Scopes.VIEW)
      .subscribe(
        (oppDetail) => {
          const accordianDataSort = oppDetail.sort((a, b) => Number(b.selected) - Number(a.selected));
          const accordianData = accordianDataSort.map((item, index) => {
            item.id = index;
          });
          this.accordianData = accordianDataSort;
          // this.formType = FormType.UPDATE;
          this.isLoading = false;
          this.messageService.success(this.notificationMessage.success);
          this.setDataFilter();
        },
        (e) => {
          if (e.error.code) {
            this.messageService.error(e.error.description);
            this.isLoading = false;
            return;
          }
          this.messageService.error(this.notificationMessage.error);
          this.isLoading = false;
        }
      );
  }

  onTabChanged($event) {
    this.tab_index = _.get($event, 'index');
  }

  ngAfterViewInit() {
    this.formCreate.valueChanges.subscribe((value) => {
      if (!this.isLoading) {
        this.checkFormCreateChange(value);
      }
    });

    this.formUpdate.valueChanges.subscribe((value) => {
      if (!this.isLoading) {
        this.checkFormUpdateChange(value);
      }
    });
  }

  checkFormCreateChange(value) {
    if (_.isEqual(this.oldFormCreate, value)) {
      this.formCreateChange = false;
    } else {
      this.formCreateChange = true;
      this.checkDisableSaveCreate();
    }
  }

  checkFormUpdateChange(value) {
    if (_.isEqual(this.oldFormUpdate, value)) {
      this.formUpdateChange = false;
    } else {
      this.formUpdateChange = true;
      this.checkDisableSaveUpdate();
    }
  }

  back() {
    this.confirmNavigate();
  }

  confirmNavigate() {
    if (this.formType === FormType.CREATE) {
      this.checkFormCreateChange(this.formCreate.value);
      if (this.formCreateChange) {
        const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
        confirm.componentInstance.message =
          'Cơ hội đang khai báo chưa được hoàn tất. Bạn chắc chắn muốn chuyển sang cơ hội khác?';
        confirm.result
          .then((res) => {
            if (res) {
              this.router.navigateByUrl('/sale-manager/selling-opp-indiv', { state: this.prop });
            }
          })
          .catch(() => {});
      } else {
        this.router.navigateByUrl('/sale-manager/selling-opp-indiv', { state: this.prop });
      }
    } else if (this.formType === FormType.UPDATE) {
      this.checkFormUpdateChange(this.formUpdate.value);
      if (this.formUpdateChange) {
        const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
        confirm.componentInstance.message =
          'Cơ hội đang khai báo chưa được hoàn tất. Bạn chắc chắn muốn chuyển sang cơ hội khác?';
        confirm.result
          .then((res) => {
            if (res) {
              this.router.navigateByUrl('/sale-manager/selling-opp-indiv', { state: this.prop });
            }
          })
          .catch(() => {});
      } else {
        this.router.navigateByUrl('/sale-manager/selling-opp-indiv', { state: this.prop });
      }
    }
  }
}
