import {Component, Injector, OnInit, QueryList, ViewChild, ViewChildren, ViewEncapsulation} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { global } from '@angular/compiler/src/util';
import {
  CommonCategory,
  FunctionCode,
  functionUri,
  maxInt32,
  Scopes,
} from 'src/app/core/utils/common-constants';
import _ from 'lodash';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import {CustomerApi, CustomerAssignmentApi} from '../../../customer-360/apis';
import {CalendarAddModalComponent} from '../../../calendar-sme/components/calendar-add-modal/calendar-add-modal.component';
import {cleanDataForm} from '../../../../core/utils/function';
import {CustomerLeadService} from '../../../lead/service/customer-lead.service';
import { MatMenuTrigger } from '@angular/material/menu';
import { FileService } from 'src/app/core/services/file.service';
import { catchError } from 'rxjs/operators';
import {  forkJoin, of } from 'rxjs';
import {LeadService} from '../../../../core/services/lead.service';
import { AppFunction } from 'src/app/core/interfaces/app-function.interface';
import { RmApi } from 'src/app/pages/rm/apis';
import { Utils } from 'src/app/core/utils/utils';

@Component({
  selector: 'sale-tracking-report-component',
  templateUrl: './sale-tracking-report.component.html',
  styleUrls: ['./sale-tracking-report.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SaleTrackingReportComponent extends BaseComponent implements OnInit {
  @ViewChildren(MatMenuTrigger) trigger:  QueryList<any>;
  isLoading = false;
  isLoadingRm = false;
  isLoadingData = false;
  isOpenMore = true;
  pageable: Pageable;
  prevParams;
  params: any = {
    pageNumber: 0,
    pageSize: global?.userConfig?.pageSize,
    rsId: '',
    scope: Scopes.VIEW,
  };
  customerType = 'LEAD';
  common = {
    sectorCustomer: [
      {
        code: null,
        displayName: 'Tất cả'
      },{
      code: 1906,
      displayName: 'Đã eKYC'
    },{
      code: 1907,
      displayName: 'Đã ký CA'
    },{
      code: 0,
      displayName: 'Đã định danh'
    }],
    miningStatus: [{
      code: null,
      displayName: 'Tất cả'
    },{
      code: 1,
      displayName: 'Đã tiếp cận'
    },{
      code: 0,
      displayName: 'Chưa tiếp cận'
    }],
    typeStatus: [{
      code: 'LEAD',
      displayName: 'Khách hàng chưa có code tại Hệ thống'
    },{
      code: 'CUSTOMER',
      displayName: 'Khách hàng đã có code tại Hệ thống'
    }],sectorLead: [
      {
        code: null,
        displayName: 'Tất cả'
      }],
  }
  objFunctionCustomer: any;
  objFunctionRM: AppFunction;
  facilities: Array<any>;
  facilityCode: Array<string>;
  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private customerApi: CustomerApi,
    private leadService: LeadService,
    private customerAssignmentApi: CustomerAssignmentApi,
    private service: CustomerLeadService,
    private rmApi: RmApi,
    private fileService: FileService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.SALES_TRACKING_REPORT}`);
    this.objFunctionRM = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
  }
  formSearch = this.fb.group({
    customerCode: [''],
    customerName: [''],
    taxCode: [''],
    startApproachDate: [],
    endApproachDate: [],
    startAllotmentDate: [],
    endAllotmentDate: [],
    miningStatus: [],
    sectorLead: [],
    sectorCustomer: [],
    sector: [],
    branches: [],
    rmCodes: []
  });

  listData = [];
  listContactPosition = [];
  listEmail = [];
  commonData = {
		listRmManager: [],
		listRmManagerTerm: [],
		listBranchManager: [],
	};
  getListRM: any;
  ngOnInit() {
    this.isLoading = true;
    this.isLoadingRm = true;
    forkJoin([
      this.rmApi
				.post('findAll', {
					page: 0,
					size: maxInt32,
					crmIsActive: true,
					branchCodes: [],
          			rmBlock: "SME",
					rsId: this.objFunctionRM?.rsId,
					scope: Scopes.VIEW,
				})
				.pipe(catchError(() => of(undefined))),
      this.categoryService
				.getTreeBranchesOfUser(this.objFunctionRM?.rsId, Scopes.VIEW)
				.pipe(catchError(() => of(undefined))),
      this.categoryService.getCommonCategory(CommonCategory.CONTACT_POSITION).pipe(catchError(() => of(undefined))),
    ]).subscribe(([listRmManagement, branchOfUser, listPosition]) => {
      this.listContactPosition = listPosition;
      const listRm = [];
      listRmManagement?.content?.forEach((item) => {
				if (!_.isEmpty(item?.t24Employee?.employeeCode)) {
					listRm.push({
						code: _.trim(item?.t24Employee?.employeeCode),
						displayName: _.trim(item?.t24Employee?.employeeCode) + ' - ' + _.trim(item?.hrisEmployee?.fullName),
						branchCode: _.trim(item?.t24Employee?.branchCode),
					});
				}
			});
      if (!listRm?.find((item) => item.code === this.currUser?.code)) {
				listRm.push({
					code: this.currUser?.code,
					displayName:
						Utils.trimNullToEmpty(this.currUser?.code) + ' - ' + Utils.trimNullToEmpty(this.currUser?.fullName),
					branchCode: this.currUser?.branch,
				});
			}
      this.commonData.listRmManagerTerm = [...listRm];
			this.commonData.listRmManager = [...listRm];
      if(branchOfUser && branchOfUser.length > 0)
				this.facilities = branchOfUser
      else{
        this.facilities =[{
            code: this.currUser?.branch,
            id: this.currUser?.branch,
            name: this.currUser?.branchName,
            parentCode: "VN",
            type: 3
        }]
      }
			branchOfUser = branchOfUser.map(item => {
				if(item.type === 1 || item.type === 2){
					item.level = 0;
				}else{
					item.level = null;
				}
			});
			this.getListRM = listRm || [];
      this.isLoadingRm = false;
      if(!this.isLoadingData)
        this.isLoading = false;
    },
    (e) => {
      this.messageService.error(e.error.description);
      this.isLoadingRm = false;
      if(!this.isLoadingData)
        this.isLoading = false;
    });
    // this.categoryService.getCommonCategory(CommonCategory.CONTACT_POSITION).subscribe(item => {
    //   if (item.content) {
    //     this.listContactPosition = item.content;
    //   }
    // });
    this.objFunctionCustomer = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.params.rsId = this.objFunction.rsId;
    this.search();
  }

  mapSector(value) {
    if (this.customerType === 'LEAD') {
      return value;
    }
    switch (value){
      case '1906':
        return 'Đã eKYC';
        break
      case '1907' :
        return 'Đã ký CA';
        break
      default:
        return 'Đã định danh';
    }
  }

  mapStatus(value){
    if (this.customerType === 'LEAD') {
      let statusStr = value?.sector === 'SUCCESS'?'Thành công':value?.sector === 'ERROR'?'Thất bại':value?.sector;
      if(value?.stepChannel !== undefined && statusStr !== undefined)
        return value?.stepChannel + ' - ' + statusStr;
      else if(value?.stepChannel !== undefined && statusStr === undefined)
        return value?.stepChannel;
      else if(value?.stepChannel === undefined && !statusStr !== undefined)
        return statusStr;
      else
        return ""
    }
    switch (value.sector){
      case '1906':
        return 'Đã eKYC';
        break
      case '1907' :
        return 'Đã ký CA';
        break
      default:
        return 'Đã định danh';
    }
  }
  search(isSearch?: boolean) {
    if (isSearch) {
      this.params.pageNumber = 0;
    }
    let param = {};
    cleanDataForm(this.formSearch);
    this.prevParams = param;
	this.isLoadingData = true;
    this.isLoading = true;
    if (this.customerType === 'LEAD') {
      this.formSearch.get('sector').setValue(this.formSearch.get('sectorLead').value)
      // this.formSearch.get('branches').setValue(this.facilityCode)
      param = { ...this.params, ...this.formSearch.getRawValue() };
      param['branches'] = this.facilityCode
      this.prevParams = param;
      this.leadService.searchLeadOnboarding(param).subscribe(value => {
        if (value.content) {
          this.listData = value.content;
          this.pageable = {
            totalElements: value.totalElements,
            size: value.pageable.pageSize,
            currentPage: value.pageable.pageNumber
          };
          this.listData.forEach(item => {
            item.sla = this.calculateRemainingTime(item);
            item.miningStatus = this.calMiningStatus(item);
          })
        }
        this.isLoadingData = false;
        if(!this.isLoadingRm)
            this.isLoading = false;
      }, error => {
        this.messageService.error(error.error.description);
        this.isLoadingData = false;
        if(!this.isLoadingRm)
            this.isLoading = false;
      });
    } else {
      this.formSearch.get('sector').setValue(this.formSearch.get('sectorCustomer').value)
      param = { ...this.params, ...this.formSearch.getRawValue() };
      param['branches'] = this.facilityCode
      this.prevParams = param;
      forkJoin([
        this.customerApi.searchCustomerOnboarding(param)
          .pipe(catchError(() => of(undefined))),
        this.customerApi.getCountCustomerOnboarding(param)
          .pipe(catchError(() => of(undefined))),
      ]).subscribe(([data, count]) => {
        this.listData = data;

        this.pageable = {
          totalElements: count,
          size: this.params.pageSize,
          currentPage: this.params.pageNumber
        };
        this.listData.forEach(item => {
          item.sla = this.calculateRemainingTime(item);
          item.miningStatus = this.calMiningStatus(item);
        })
        this.isLoading = false;
      },() => {
          this.isLoading = false;
          this.messageService.error(this.notificationMessage.error);
        });
      // this.customerApi.searchCustomerOnboarding(param).subscribe(value => {
      //   this.listData = value;
      //   this.getCount(param);
      // }, error => {
      //   this.messageService.error(error.error.description);
      //   this.isLoading = false;
      // });
    }
  }

  // getCount(param) {
  //   this.customerApi.getCountCustomerOnboarding(param).subscribe(value => {
  //     this.pageable = {
  //       totalElements: value,
  //       size: param.pageSize,
  //       currentPage: param.pageNumber
  //     };
  //     this.isLoading = false;
  //   })
  //   this.listData.forEach(item => {
  //     item.sla = this.calculateRemainingTime(item);
  //     item.miningStatus = this.calMiningStatus(item);
  //   })
  // }


  calculateRemainingTime(item) {
    const now = new Date().setSeconds(0);
    const assignedDate = new Date(item.assignedDate).setSeconds(0);
    const diff = Math.abs(now.valueOf() - assignedDate.valueOf());
    const minutes = (29 - Math.floor((diff/1000)/60));
    if (minutes > 0 ) {
      return (minutes < 10 ? '0' : '') + minutes.toString()
        + ':' + ((59 - (new Date().getSeconds())) < 10 ? '0' : '') + (59 - (new Date().getSeconds())).toString();
    } else {
      return '00:00';
    }
  }

  calMiningStatus(item) {
    const assDate = new Date(item.assignedDate);
    // đã tiếp cận
    if (item.approachDate != null) {
      if (new Date(item.approachDate).valueOf() <= assDate.setMinutes(assDate.getMinutes() + 30).valueOf()) {
        return item.miningStatus = 'Đã tiếp cận (đúng SLA)';
      } else {
        return item.miningStatus = 'Đã tiếp cận';
      }
    } else {
      // chưa tiếp cận
        return item.miningStatus = 'Chưa tiếp cận';
    }
  }
  changeBranch(event) {
		let listAllRMInBranches = [];
    let listRmSelected = this.formSearch.get('rmCodes').value;
    let listRm = [];
		if(event.length > 0){
			event.forEach(element => {
				let listRMInBranch:any;
				listRMInBranch = _.filter(
					this.commonData.listRmManagerTerm,
					(item) => item.branchCode === element
				  );
				listAllRMInBranches.push(...listRMInBranch) ;
			});
      if(listRmSelected){
        listRmSelected.forEach(rm => {
          let checkRmSelect = _.filter(listAllRMInBranches,
              (item) => item.code === rm
          );
          if(checkRmSelect && checkRmSelect?.length > 0){
            listRm.push(rm)
          }
        })
      }

		}else{
      listAllRMInBranches = this.commonData.listRmManagerTerm;
      listRm = [];
    }
    this.formSearch.get('rmCodes').setValue(listRm)
		this.commonData.listRmManager = [...listAllRMInBranches];
		this.objFunctionRM?.rsId;
	}
  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.pageNumber = pageInfo.offset;
    this.search( );
  }

  onActive(event) {

    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      if (!_.isEmpty(_.trim(item.customerTypeMerge)) && !_.isEmpty(_.trim(item.customerCode))) {
        this.router.navigate(['/customer360/customer-manager', 'detail',  _.toLower(item.customerTypeMerge) , item.customerCode], {
          skipLocationChange: true,
          queryParams: {
            manageRM: item.manageRM,
            showBtnRevenueShare: false,
          },
        });
      }else if(!_.isEmpty(_.trim(item.leadCode))){
        event.cellElement.blur();
        const item = _.get(event, 'row');
        this.router.navigate([functionUri.lead_management, 'detail', item.leadCode], { state: this.prevParams });
      }
    }
  }
  action() {

  }

  classMiningStatus(row) {
    if (row.approachDate != null) {
      if (row.miningStatus === 'Đã tiếp cận (đúng SLA)') {
        return 'text-green'
      } else if (row.miningStatus === 'Đã tiếp cận') {
        return 'text-danger'
      }
    }
  }

  mailTo(row, mail) {
    if (_.isEmpty(mail)) {
      this.trigger.forEach(item => {
        item.closeMenu();
      });
      this.messageService.error('Không có thông tin Email');
    } else {
      const data = {
        type: 'EMAIL',
        customerCode: row.customerCode,
        leadCode: row.leadCode,
        scope: Scopes.VIEW,
        rsId: this.objFunctionCustomer.rsId
      }
      this.customerAssignmentApi.updateActivityLog(data).subscribe(value => {
      });
      const url = `mailto:${mail}`;
      window.location.href = url;
    }
  }

  createTaskTodo(row) {
    const modal = this.modalService.open(CalendarAddModalComponent, {windowClass: 'create-calendar-modal'});
    if (this.customerType === 'LEAD') {
      modal.componentInstance.leadCode = row.leadCode;
      modal.componentInstance.leadName = row.customerName;
    } else {
      modal.componentInstance.customerCode = row.customerCode;
      modal.componentInstance.customerName = row.customerName;
    }
    modal.componentInstance.isDetailLead = true;
    modal.result
      .then((res) => {
        if (res) {

        } else {
        }
      })
      .catch(() => {
      });
  }
  exportFile() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+(this.pageable?.totalElements || 0) === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.isLoading = true;
    if (this.customerType === 'LEAD') {
      this.leadService.exportFileLeadOnboarding(this.prevParams).subscribe(
        (fileId) => {
          if (!_.isEmpty(fileId)) {
            this.fileService
              .downloadFile(fileId, 'crm_lead_biz.xlsx')
              .pipe(catchError((e) => of(false)))
              .subscribe((res) => {
                this.isLoading = false;
                if (!res) {
                  this.messageService.error(this.notificationMessage.error);
                }
              });
          } else {
            this.messageService.error(this.notificationMessage?.export_error || '');
            this.isLoading = false;
          }
        },
        () => {
          this.messageService.error(this.notificationMessage?.export_error || '');
          this.isLoading = false;
        }
      );
    }else{
      this.customerApi.exportFileCustomerOnboarding(this.prevParams).subscribe(
        (fileId) => {
          if (!_.isEmpty(fileId)) {
            this.fileService
              .downloadFile(fileId, 'crm_customer_biz.xlsx')
              .pipe(catchError((e) => of(false)))
              .subscribe((res) => {
                this.isLoading = false;
                if (!res) {
                  this.messageService.error(this.notificationMessage.error);
                }
              });
          } else {
            this.messageService.error(this.notificationMessage?.export_error || '');
            this.isLoading = false;
          }
        },
        () => {
          this.messageService.error(this.notificationMessage?.export_error || '');
          this.isLoading = false;
        }
      );
    }
  }

  getAllMailAndPosition(row) {
    this.listEmail = [];
    const data = {
      leadCode: row.leadCode ? row.leadCode : '',
      customerCode:  row.customerCode ? row.customerCode : '',
      rsId :  this.objFunction.rsId,
      scope : 'VIEW'
    }
    this.service.getAllMail(data).subscribe(value => {
      if (value) {
        this.listEmail = value.filter(e => e.mail !== undefined);
        if (_.isEmpty(this.listEmail)) {
          this.mailTo(row, undefined)
        }
        this.listContactPosition.forEach(item => {
          this.listEmail.forEach(e => {
            if (item.code === e.position) {
              e.position = item.name;
            }
          })
        });
      }
    }, error => {
      this.trigger.forEach(item => {
        item.closeMenu();
      });
      this.messageService.error(this.notificationMessage.error);
    });
  }
  openMore() {
    this.isOpenMore = !this.isOpenMore;
  }
}
