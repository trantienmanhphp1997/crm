import { Component, OnInit, Injector, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { RmApi } from 'src/app/pages/rm/apis';
import { AppFunction } from 'src/app/core/interfaces/app-function.interface';
import {
   CommonCategory,
   FunctionCode,
   functionUri,
   maxInt32,
   Scopes
} from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { forkJoin, of } from 'rxjs';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { Utils } from 'src/app/core/utils/utils';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { global } from '@angular/compiler/src/util';
import { CalendarAddModalComponent } from 'src/app/pages/calendar-sme/components/calendar-add-modal/calendar-add-modal.component';
import { CustomerApi, CustomerAssignmentApi } from 'src/app/pages/customer-360/apis';
import { SaleManagerApi } from '../../api';
import * as moment from 'moment';
import { CustomerLeadService } from '../../../lead/service/customer-lead.service';
import { MatMenuTrigger } from '@angular/material/menu';
import { FileService } from 'src/app/core/services/file.service';

@Component({
   selector: 'app-bidding-list-component',
   templateUrl: './bidding-list.component.html',
   styleUrls: ['./bidding-list.component.scss'],
})
export class BiddingListComponent extends BaseComponent implements OnInit {
   isLoading: boolean = false;
   isOpenMore: boolean = true;
   page: number = 0;
   size: number = _.get(global, 'userConfig.pageSize');
   pageable: Pageable;
   listData = [];
   isEmpty = true;
   prevParams: any;
   facilities: Array<any>;
   facilityCode: Array<string>;
   allBranch = {
      "branchCode": null,
      "code": null,
      "name": " - Tất cả - "
   }
   formSearch = this.fb.group({
      taxCode: [null],
      customerName: [null],
      customerCode: [null],
      biddingName: [null],
      startUpdateDate: [],
      endUpdateDate: [],
      startAssignDate: [],
      endAssignDate: [],
      rmCode: [null],
      branchCode: [null],
      status: [null],
      type: [null],
      customerType: [null],
      customerStatus: [null],
      province: [null],
      district: [null],
      wards: [null]
   });
   commonData = {
      listRmManager: [],
      listRmManagerTerm: [],
      listBranchManager: [],
      biddingStatus: [],
      biddingCustomerType: [],
      biddingCustomerStatus: [],
      listCustomerStatus: [{
         "code": "ALL",
         "name": "Tất cả",
         "value": "0"
     }],
      listProvince: [],
      listDistrict: [],
      listWards: []
   };
   objFunctionRM: AppFunction;
   objFunctionCustomer: any;
   listEmail = [];
   body: any;
   listContactPosition = [];
   getListRM: any;
   @ViewChildren(MatMenuTrigger) trigger: QueryList<any>;
   constructor(
      private rmApi: RmApi,
      private categoryService: CategoryService,
      private saleManagerApi: SaleManagerApi,
      private customerAssignmentApi: CustomerAssignmentApi,
      private service: CustomerLeadService,
      private fileService: FileService,
      injector: Injector,
   ) {
      super(injector);
      this.isLoading = true;
      this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.BIDDING_LIST}`);
      this.objFunctionRM = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
      this.objFunctionCustomer = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
   }
   ngOnInit(): void {
      this.categoryService.getCommonCategory(CommonCategory.CONTACT_POSITION).subscribe(item => {
         if (item.content) {
            this.listContactPosition = item.content;
         }
      });
      this.getData();
      this.getCategory();
   }

   getCategory() {
      this.formSearch.controls['customerType'].valueChanges.subscribe(res => {
         const biddingType = this.commonData.biddingCustomerType.find(item => item.value === res) || {};
         this.commonData.listCustomerStatus = this.commonData.biddingCustomerStatus.filter((item) => !Utils.isEmpty(biddingType) && item.description.includes(biddingType?.code));
         this.formSearch.controls['customerStatus'].setValue(_.head(this.commonData.listCustomerStatus)?.value);
      });
      this.service.getLocation({ isWard: false, locationCode: 'VN' }).subscribe(res => {
         this.commonData.listProvince = res;
      })
      forkJoin([
         this.categoryService.getCommonCategory(CommonCategory.BIDDING_STATUS),
         this.categoryService.getCommonCategory(CommonCategory.CUSTOMER_BIDDING_STATUS),
         this.categoryService.getCommonCategory(CommonCategory.CUSTOMER_BIDDING_TYPE)
      ]).subscribe(([BIDDING_STATUS, CUSTOMER_BIDDING_STATUS, CUSTOMER_BIDDING_TYPE]) => {
         this.commonData.biddingStatus = BIDDING_STATUS.content;
         this.commonData.biddingCustomerType = CUSTOMER_BIDDING_TYPE.content;
         this.commonData.biddingCustomerStatus = CUSTOMER_BIDDING_STATUS.content;
         this.formSearch.controls['customerType'].setValue('0');
         this.formSearch.controls['type'].setValue('0');
      });
      this.formSearch.controls['province'].valueChanges.subscribe(res => {
         this.service.getLocation({ isWard: false, locationCode: res }).subscribe(res => {
            this.commonData.listDistrict = res;
         })
      });
      this.formSearch.controls['district'].valueChanges.subscribe(res => {
         this.service.getLocation({ isWard: true, locationCode: res }).subscribe(res => {
            this.commonData.listWards = res;
         })
      });

   }

   getData() {
      this.body = {
         "customerCode": this.formSearch.get('customerCode').value,
         "customerName": this.formSearch.get('customerName').value,
         "taxCode": this.formSearch.get('taxCode').value,
         "tenBenMoiThau": this.formSearch.get('biddingName').value,
         "thoiGianDangTaiTu": this.formSearch.get('startUpdateDate').value,
         "thoiGianDangTaiDen": this.formSearch.get('endUpdateDate').value,
         "thoiGianNhanDuLieuTu": this.formSearch.get('startAssignDate').value,
         "thoiGianNhanDuLieuDen": this.formSearch.get('endAssignDate').value,
         "rmCode": this.formSearch.get('rmCode').value,
         "branchCode": this.formSearch.get('branchCode').value,
         "scope": "VIEW",
         "rsId": this.objFunction.rsId
      }
      forkJoin([
         this.rmApi
            .post('findAll', {
               page: 0,
               size: maxInt32,
               crmIsActive: true,
               branchCodes: [],
               rmBlock: "SME",
               rsId: this.objFunctionRM?.rsId,
               scope: Scopes.VIEW,
            })
            .pipe(catchError(() => of(undefined))),
         this.categoryService
            .getTreeBranchesOfUser(this.objFunctionRM?.rsId, Scopes.VIEW)
            .pipe(catchError(() => of(undefined))),
         this.saleManagerApi.getBiddingList(this.body, 0, this.size)
            .pipe(catchError(() => of(undefined)))
      ]).subscribe(([listRmManagement, branchOfUser, dataSearch]) => {
         const listRm = [];
         listRmManagement?.content?.forEach((item) => {
            if (!_.isEmpty(item?.t24Employee?.employeeCode)) {
               listRm.push({
                  code: _.trim(item?.t24Employee?.employeeCode),
                  displayName: _.trim(item?.t24Employee?.employeeCode) + ' - ' + _.trim(item?.hrisEmployee?.fullName),
                  branchCode: _.trim(item?.t24Employee?.branchCode),
               });
            }
         });
         if (!listRm?.find((item) => item.code === this.currUser?.code)) {
            listRm.push({
               code: this.currUser?.code,
               displayName:
                  Utils.trimNullToEmpty(this.currUser?.code) + ' - ' + Utils.trimNullToEmpty(this.currUser?.fullName),
               branchCode: this.currUser?.branch,
            });
         }
         this.commonData.listRmManagerTerm = [...listRm];
         this.commonData.listRmManager = [...listRm];
         if (branchOfUser && branchOfUser.length > 0)
            this.facilities = branchOfUser
         else {
            this.facilities = [{
               code: this.currUser?.branch,
               id: this.currUser?.branch,
               name: this.currUser?.branchName,
               parentCode: "VN",
               type: 3
            }]
         }

         branchOfUser = branchOfUser.map(item => {
            if (item.type === 1 || item.type === 2) {
               item.level = 0;
            } else {
               item.level = null;
            }
         });
         this.getListRM = listRm || [];
         // this.commonData.listBranchManager =
         //    branchOfUser?.map((item) => {
         //       return {
         //          code: item.code,
         //          name: `${item.code} - ${item.name}`,
         //       };
         //    }) || [];
         // if (!_.find(this.commonData.listBranchManager, (item) => item.code === this.currUser?.branch)) {
         //    this.commonData.listBranchManager.push({
         //       code: this.currUser?.branch,
         //       name: `${this.currUser?.branch} - ${this.currUser?.branchName}`,
         //    });
         // }
         // if (!_.find(this.commonData.listBranchManager, (item) => item.code === this.currUser?.branch)) {
         //    this.commonData.listBranchManager.push({
         //       code: this.currUser?.branch,
         //       name: `${this.currUser?.branch} - ${this.currUser?.branchName}`,
         //    });
         // }
         // this.commonData.listRmManager = _.filter(
         //    this.commonData.listRmManagerTerm,
         //    (item) => item.branchCode === this.currUser?.branch
         // );
         if (dataSearch) {
            this.listData = dataSearch?.content || [];
            this.pageable = {
               totalElements: dataSearch?.totalElements,
               totalPages: dataSearch?.totalPages,
               currentPage: this.page,
               size: this.size,
            };
         }
         // else {
         //    if (this.commonData.listRmManager.length == 1) {
         //       let rm = this.commonData.listRmManager[0];
         //       this.formSearch.get('rmCode').setValue(rm.code);
         //    }
         // }
         // if (this.commonData.listBranchManager.length > 1)
         //    this.commonData.listBranchManager.unshift(this.allBranch);
         // else {
         //    if (this.commonData.listBranchManager.length == 1) {
         //       let branch = this.commonData.listBranchManager[0];
         //       this.formSearch.get('branchCode').setValue(branch.code);
         //    }
         // }

         this.isLoading = false;
      },
         (e) => {
            this.messageService.error(e.error.description);
            this.isLoading = false;
         });
      // this.formSearch.get('branchCode').valueChanges.subscribe((value) => {
      //    if (!this.isLoading) {
      //       this.formSearch.get('rmCode').setValue('');
      //       if(value == null){
      //          this.commonData.listRmManager = this.commonData.listRmManagerTerm;
      //       }else{
      //          this.commonData.listRmManager = _.filter(
      //             this.commonData.listRmManagerTerm,
      //             (item) => item.branchCode === value
      //          );
      //       }
      //       if (this.commonData.listRmManager.length > 1) {
      //          this.commonData.listRmManager.unshift(this.allRM);
      //       } else {
      //          if (this.commonData.listRmManager.length == 1) {
      //             let rm = this.commonData.listRmManager[0];
      //             this.formSearch.get('rmCode').setValue(rm.code);
      //          }
      //       }

      //    }
      // });
   }

   search(pageinput) {
      let startUpdateDate = this.formSearch.get('startUpdateDate').value;
      let endUpdateDate = this.formSearch.get('endUpdateDate').value;
      let startAssignDate = this.formSearch.get('startAssignDate').value;
      let endAssignDate = this.formSearch.get('endAssignDate').value;
      this.formSearch.controls.branchCode.setValue(this.facilityCode);
      var dataSearch = this.formSearch.value;
      var listRmCode = dataSearch.rmCode;
      if (listRmCode?.length == this.getListRM?.length && listRmCode?.length > 1000) {
         dataSearch.rmCode = [];
      } else {
         if (listRmCode?.length > 1000) {
            this.messageService.error("Vui lòng chọn ít hơn 1000 RM hoặc chọn tất cả !");
            return;
         }
      }
      var listBranchCode = dataSearch.branchCode;
      if (listBranchCode?.length == this.facilities?.length) {
         dataSearch.branchCode = [];
      }
      if (moment(endUpdateDate).startOf('day').isBefore(moment(startUpdateDate).startOf('day'))) {
         this.messageService.warn("Thời gian đăng " + this.notificationMessage.startDateMoreThanEndDatePl);
         return;
      }
      if (moment(endAssignDate).startOf('day').isBefore(moment(startAssignDate).startOf('day'))) {
         this.messageService.warn("Thời gian phân giao " + this.notificationMessage.startDateMoreThanEndDatePl);
         return;
      }
      this.isLoading = true;
      this.isEmpty = _.size(this.listData) === 0;

      const selectedProvince: any[] = this.formSearch.get('province').value || [];
      const provinces = this.commonData.listProvince.filter(item => selectedProvince.includes(item.key)).map(item => item.value);

      const selectedDistrict: any[] = this.formSearch.get('district').value || [];
      const districts = this.commonData.listDistrict.filter(item => selectedDistrict.includes(item.key)).map(item => item.value);

      const selectedWards: any[] = this.formSearch.get('wards').value || [];
      const wards = this.commonData.listWards.filter(item => selectedWards.includes(item.key)).map(item => item.value);

      let customerStatus = this.formSearch.get('customerStatus').value;
      customerStatus = Utils.isNull(customerStatus) || customerStatus === "ALL" ? null : customerStatus;

      this.body = {
         "customerCode": this.formSearch.get('customerCode').value,
         "customerName": this.formSearch.get('customerName').value,
         "taxCode": this.formSearch.get('taxCode').value,
         "tenBenMoiThau": this.formSearch.get('biddingName').value,
         "thoiGianDangTaiTu": this.formSearch.get('startUpdateDate').value,
         "thoiGianDangTaiDen": this.formSearch.get('endUpdateDate').value,
         "thoiGianNhanDuLieuTu": this.formSearch.get('startAssignDate').value,
         "thoiGianNhanDuLieuDen": this.formSearch.get('endAssignDate').value,
         "rmCode": dataSearch.rmCode,
         "branchCode": dataSearch.branchCode,
         "scope": "VIEW",
         "rsId": this.objFunction.rsId,
         "type": this.formSearch.get('type').value,
         "customerType": this.formSearch.get('customerType').value,
         "customerStatus": customerStatus,
         "province": provinces,
         "district": districts,
         "wards": wards
      }
      this.saleManagerApi.getBiddingList(this.body, pageinput, this.size).subscribe((result) => {
         if (result) {
            this.listData = result?.content || [];
            this.pageable = {
               totalElements: result?.totalElements,
               totalPages: result?.totalPages,
               currentPage: this.page,
               size: this.size,
            };
         }
         this.isLoading = false;
      },
         (e) => {
            if (e.error.code) {
               this.messageService.error(e.error.description);
               this.isLoading = false;
               return;
            }
            this.messageService.error(this.notificationMessage.error);
            this.isLoading = false;
         });
   }

   onActive(event) {
      if (event.type === 'dblclick') {
         const item = _.get(event, 'row');
         if (!_.isEmpty(_.trim(item.customerCode))) {
            this.router.navigate([functionUri.customer_360_manager, 'detail', _.toLower(item.block), item.customerCode], {
               skipLocationChange: true,
               queryParams: {
                  manageRM: item.rm,
                  tabIndex: 8,
                  showBtnRevenueShare: false,
               },
            })
         } else if (!_.isEmpty(_.trim(item.leadCode))) {
            event.cellElement.blur();
            const item = _.get(event, 'row');
            this.router.navigate([functionUri.lead_management, 'detail', item.leadCode], { state: this.prevParams });
         }
      }
   }
   changeBranch(event) {
      let listAllRMInBranches = [];
      let listRmSelected = this.formSearch.get('rmCode').value;
      let listRm = [];
      if (event.length > 0) {
         event.forEach(element => {
            let listRMInBranch: any;
            listRMInBranch = _.filter(
               this.commonData.listRmManagerTerm,
               (item) => item.branchCode === element
            );
            listAllRMInBranches.push(...listRMInBranch);
         });
         if (listRmSelected) {
            listRmSelected.forEach(rm => {
               let checkRmSelect = _.filter(listAllRMInBranches,
                  (item) => item.code === rm
               );
               if (checkRmSelect && checkRmSelect?.length > 0) {
                  listRm.push(rm)
               }
            })
         }

      } else {
         listAllRMInBranches = this.commonData.listRmManagerTerm;
         listRm = [];
      }
      this.formSearch.get('rmCode').setValue(listRm)
      this.commonData.listRmManager = [...listAllRMInBranches];
      this.objFunctionRM?.rsId;
   }
   action() {

   }
   setPage(pageInfo) {
      if (this.isLoading) {
         return;
      }
      this.page = pageInfo.offset;
      this.search(this.page);
   }

   mailTo(row, mail) {
      if (_.isEmpty(mail)) {
         this.trigger.forEach(item => {
            item.closeMenu();
         });
         this.messageService.error('Không có thông tin Email');
      } else {
         const data = {
            type: 'EMAIL',
            customerCode: row.customerCode ? row.customerCode : '',
            leadCode: row.leadCode ? row.leadCode : '',
            scope: Scopes.VIEW,
            rsId: this.objFunctionCustomer.rsId
         }
         this.customerAssignmentApi.updateActivityLog(data).subscribe(value => {
         });
         const url = `mailto:${mail}`;
         window.location.href = url;
      }
   }

   createTaskTodo(row) {
      const modal = this.modalService.open(CalendarAddModalComponent, { windowClass: 'create-calendar-modal' });
      modal.componentInstance.customerCode = row.customerCode;
      modal.componentInstance.customerName = row.customerName;
      modal.componentInstance.isDetailLead = true;
      modal.result
         .then((res) => {
            if (res) {

            } else {
            }
         })
         .catch(() => {
         });
   }

   getAllMailAndPosition(row) {
      this.listEmail = [];
      const data = {
         leadCode: row.customerCode ? '' : row.leadCode ? row.leadCode : '',
         customerCode: row.customerCode ? row.customerCode : '',
         rsId: this.objFunction.rsId,
         scope: 'VIEW'
      }
      this.service.getAllMail(data).subscribe(value => {
         if (value) {
            this.listEmail = value.filter(e => e.mail !== undefined);
            if (_.isEmpty(this.listEmail)) {
               this.mailTo(row, undefined)
            }
            this.listContactPosition.forEach(item => {
               this.listEmail.forEach(e => {
                  if (item.code === e.position) {
                     e.position = item.name;
                  }
               })
            });
         }
      }, error => {
         this.trigger.forEach(item => {
            item.closeMenu();
         });
         this.messageService.error(this.notificationMessage.error);
      });
   }
   openMore() {
      this.isOpenMore = !this.isOpenMore;
   }

   exportFile() {
      if (!this.maxExportExcel) {
         return;
      }
      if (+(this.pageable?.totalElements || 0) === 0) {
         this.messageService.warn(this.notificationMessage.noRecord);
         return;
      }
      if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
         this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
            this.messageService.warn(res);
         });
         return;
      }
      this.isLoading = true;
      this.saleManagerApi.exportBiddingList(this.body).subscribe(
         (fileId) => {
            if (!_.isEmpty(fileId)) {
               this.fileService
                  .downloadFile(fileId, 'ds_dau_thau.xlsx')
                  .pipe(catchError((e) => of(false)))
                  .subscribe((res) => {
                     this.isLoading = false;
                     if (!res) {
                        this.messageService.error(this.notificationMessage.error);
                     }
                  });
            } else {
               this.messageService.error(this.notificationMessage?.export_error || '');
               this.isLoading = false;
            }
         },
         () => {
            this.messageService.error(this.notificationMessage?.export_error || '');
            this.isLoading = false;
         }
      );
   }

}
