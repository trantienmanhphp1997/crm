import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import {
  CommonCategory,
  Division,
  FunctionCode,
  functionUri,
  maxInt32,
  Scopes,
  ScreenType,
  SessionKey,
  StatusWork,
} from 'src/app/core/utils/common-constants';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import _ from 'lodash';
import * as moment from 'moment';
import { RmProfileService } from 'src/app/core/services/rm-profile.service';
import { formatNumber } from '@angular/common';
import { Utils } from 'src/app/core/utils/utils';
import { global } from '@angular/compiler/src/util';
import { RmApi, RmBlockApi } from 'src/app/pages/rm/apis';
import { RevenueShareApi } from 'src/app/pages/customer-360/apis/revenue-share.api';
import { ModalRevenueSharingComponent } from 'src/app/pages/customer-360/components/modal-revenue-sharing/modal-revenue-sharing.component';
import { ProductModalComponent } from '../product-model/product-modal.component';
import { CustomerApi, CustomerDetailApi, CustomerDetailSmeApi } from 'src/app/pages/customer-360/apis/customer.api';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { SaleManagerApi, SaleTargetApi } from '../../api';
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'sale-target-update-component',
  templateUrl: './sale-target-update.component.html',
  styleUrls: ['./sale-target-update.component.scss'],
})
export class SaleTargetUpdateComponent extends BaseComponent implements OnInit {
  isLoading = false;
  @ViewChild('table') table: DatatableComponent;
  @ViewChild('tableTarget') tableTarget: DatatableComponent;
  pageable: Pageable;
  listRmManager = [];

  formDetailTarget = this.fb.group({
    targetBranchName: [''],
    targetStatus: [''],
    targetCode: [''],
    targetRMCode: [''],
    product: [''],
    createdBy: [''],
    createdDate: [''],
    targetBranch: [''],
    assignTo: [''],
  });

  limit = global.userConfig.pageSize;
  model: any = {};
  screenType: any;
  opportunityCode: any;
  customerCode: any;
  statusOpportunity: any;
  targetCode: any;
  targetId: any;
  divisionCode: any;
  constructor(
    injector: Injector,
    private customerDetailApi: CustomerDetailApi,
    private customerApi: CustomerApi,

    private saleManagerApi: SaleManagerApi,
    private rmBlockApi: RmBlockApi,
    private customerDetailSmeApi: CustomerDetailSmeApi,
    private saleTargetApi: SaleTargetApi
  ) {
    super(injector);
    this.route?.data?.subscribe((data) => {
      this.screenType = data?.type;
    });
    this.opportunityCode = this.route.snapshot.queryParams.opportunityCode;
    this.divisionCode = this.route.snapshot.queryParams.divisionCode;
    this.customerCode = this.route.snapshot.queryParams.customerCode;
    this.targetCode = this.route.snapshot.queryParams.targetCode;
    this.targetId = this.route.snapshot.queryParams.targetId;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.OPPORTUNITY_TARGET}`);
  }

  params: any = {
    pageNumber: 0,
    pageSize: 10,
    rsId: '',
    scope: Scopes.VIEW,
  };
  is_show = true;
  // showData = true;
  showMessage = false;
  dataDivisionBranch: any = {};
  listBranchesOpp = [];
  rmLevelCode = '';
  btnCheckCustomer = true;

  pageableProduct: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };

  paramsTableProduct = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };

  listDataProduct = [];
  dataProduct = [];
  lstLevelRmCode: any;
  dataProductOld = [];
  dataProductDelete = [];
  segmentCustomer: string;
  detailTarget: any;
  detailOpportunity: any;
  targetBranch: any;
  targetRMCode: any;
  statusTarget: any;
  statusTargetCreated: any;
  apiDetailCustomer: Observable<any>;
  apigetSegmentByCustomerCode: Observable<any>;
  listStatusOpp = [
    { code: '1', displayName: 'Tạo mới' },
    { code: '2', displayName: 'Hủy mục tiêu bán' },
  ];

  ngOnInit() {
    if (this.screenType === ScreenType.Update) {
      this.formDetailTarget.controls.targetRMCode.enable();
      this.formDetailTarget.controls.targetBranchName.enable();
    }
    if (this.divisionCode === Division.SME) {
      this.apiDetailCustomer = this.saleManagerApi
        .customerDetailOpportunity(this.customerCode)
        .pipe(catchError(() => of(undefined)));
      this.apigetSegmentByCustomerCode = this.customerDetailSmeApi
        .getSegmentByCustomerCode(this.customerCode)
        .pipe(catchError(() => of(undefined)));
    } else {
      this.apiDetailCustomer = this.saleManagerApi
        .customerDetailOpportunity(this.customerCode)
        .pipe(catchError(() => of(undefined)));
      this.apigetSegmentByCustomerCode = of([]);
    }
    this.isLoading = true;
    forkJoin([
      this.apiDetailCustomer,
      this.saleManagerApi.detailOpportunity(this.opportunityCode).pipe(catchError(() => of(undefined))),
      this.rmBlockApi
        .fetch({ page: 0, size: maxInt32, hrsCode: this.currUser?.hrsCode, isActive: true })
        .pipe(catchError(() => of(undefined))),
      this.apigetSegmentByCustomerCode,
      this.saleTargetApi.detailTargetSale(this.targetCode).pipe(catchError(() => of(undefined))),
    ]).subscribe(([itemCustomer, detailOpportunity, divisionOfSystem, segment, detailTarget]) => {
      this.detailTarget = detailTarget;
      this.detailOpportunity = detailOpportunity;
      if (this.divisionCode === Division.SME) {
        this.segmentCustomer = segment;
      } else {
        this.segmentCustomer = itemCustomer?.customerInfo?.customerSegment;
      }

      this.targetBranch = detailTarget?.targetBranchName?.split(' ')[0];
      this.targetRMCode = detailTarget?.targetRMCode?.split(' ')[0];

      this.statusTargetCreated = detailTarget?.targetStatus;
      this.formDetailTarget.controls.targetStatus.setValue(detailTarget?.targetStatus);
      if (+detailTarget?.targetStatus === 1) {
        this.statusTarget = 'saleTarget.statusCreated';
      } else if (+detailTarget?.targetStatus === 2) {
        this.statusTarget = 'saleTarget.statusCanceled';
      } else {
        this.statusTarget = 'saleTarget.statusTransferred';
      }

      this.model = itemCustomer;
      // LIST TITLE
      let listDivisioTitle =
        divisionOfSystem?.content?.map((item) => {
          return {
            divisionCode: item?.blockCode,
            levelRmCode: item?.levelRMCode === undefined ? '' : item?.levelRMCode,
          };
        }) || [];

      this.saleManagerApi.getTitleDivisionBranch(listDivisioTitle).subscribe((res) => {
        if (res) {
          this.dataDivisionBranch = res;
          let listBranchConfig = this.dataDivisionBranch.map((item) => item.ruleDivision.branch).toString();
          listBranchConfig = _.unionBy(_.split(listBranchConfig, ','));
          this.lstLevelRmCode = this.dataDivisionBranch.map((item) => item.ruleDivision.ruleAssign).toString();
          this.lstLevelRmCode = _.unionBy(_.split(this.lstLevelRmCode, ','));
          this.getBranchByUser(listBranchConfig);
          this.getRmByBranch(this.targetBranch, this.targetRMCode);
        }
      });

      // this.isLoading = false;
    });
  }

  getBranchByUser(listBranchConfig) {
    const param = {
      listBranchCode: listBranchConfig,
      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW,
    };
    this.saleManagerApi.getBranchByUser(param).subscribe((res) => {
      this.listBranchesOpp = res.map((item) => {
        return {
          code: item.code,
          displayName: item.code + ' - ' + item.name,
        };
      });
      this.formDetailTarget.controls.targetBranchName.setValue(this.targetBranch, { emitEvent: false });
    });
  }

  transferSale() {
    this.isLoading = true;
    this.saleTargetApi.convertSale(this.targetId, this.objFunction?.rsId, Scopes.UPDATE).subscribe(
      (resConvert) => {
        this.isLoading = false;
        this.messageService.success(this.notificationMessage.success);
        this.router.navigateByUrl(functionUri.sale_transfer);
      },
      (e) => {
        this.isLoading = false;
        if (e.error.code) {
          this.messageService.error(e.error.description);
        }
      }
    );
  }

  getRmByBranch(branchCode, assignTo?) {
    let listBranchCode = [];
    listBranchCode.push(branchCode);
    this.formDetailTarget.controls.targetRMCode.disable();
    const param = {
      lstBranchCode: listBranchCode,
      lstLevelRmCode: this.lstLevelRmCode,
      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW,
    };
    this.saleManagerApi.findRmByBranchAndLevelCode(param).subscribe((res) => {
      if (res) {
        this.listRmManager = res?.map((item) => {
          if (!_.isEmpty(item?.rmCode) && item?.active && item?.statusWork !== StatusWork.NTS) {
            return {
              code: item.rmCode,
              displayName: item.rmCode + ' - ' + item.rmName,
            };
          }
        });
        this.listRmManager = this.listRmManager.filter((item) => !_.isEmpty(item));
        if (!_.isEmpty(assignTo)) {
          this.formDetailTarget.controls.targetRMCode.setValue(assignTo);
          this.isLoading = false;
          if (
            _.isEmpty(this.lstLevelRmCode[0]) &&
            this.formDetailTarget.controls.targetBranchName.value !== this.currUser.branch
          ) {
            this.listRmManager = [];
          }
          if (this.screenType === ScreenType.Update) {
            this.formDetailTarget.controls.targetRMCode.enable();
          }
        } else {
          this.isLoading = false;
          if (_.isEmpty(this.lstLevelRmCode[0])) {
            if (this.formDetailTarget.controls.targetBranchName.value === this.currUser.branch) {
              this.formDetailTarget.controls.targetRMCode.setValue(_.first(this.listRmManager)?.code);
            } else {
              this.listRmManager = [];
              this.formDetailTarget.controls.targetRMCode.setValue('');
            }
          } else {
            this.listRmManager = this.listRmManager.filter((item) => !_.isEmpty(item));
            this.formDetailTarget.controls.targetRMCode.setValue(_.first(this.listRmManager)?.code);
          }
          this.formDetailTarget.controls.targetRMCode.enable();
        }
      }
    });
  }

  ngAfterViewInit() {
    this.formDetailTarget.controls.targetBranchName.valueChanges.subscribe((value) => {
      this.getRmByBranch(value);
    });
  }

  checkEditTarget() {
    this.isLoading = true;
    this.saleTargetApi
      .checkUpdateTargetSale(this.divisionCode, this.objFunction?.rsId, Scopes.UPDATE, this.targetId, true)
      .subscribe(
        (res) => {
          this.isLoading = false;
          this.screenType = ScreenType.Update;
          this.formDetailTarget.controls.targetRMCode.enable({ emitEvent: false });
          this.formDetailTarget.controls.targetBranchName.enable();
        },
        (e) => {
          this.isLoading = false;
          if (e.error.code) {
            this.messageService.error(e.error.description);
          }
        }
      );
  }

  getTruncateValue(item, key, limit) {
    return Utils.isStringNotEmpty(_.get(item, key)) ? Utils.truncate(_.get(item, key), limit) : '---';
  }

  editOpportunity() {
    if (
      _.isEmpty(this.formDetailTarget.get('targetRMCode').value) ||
      _.isEmpty(this.listRmManager.filter((item) => item.code === this.formDetailTarget.get('targetRMCode').value))
    ) {
      this.messageService.warn('Chọn RM mục tiêu');
      return;
    }

    this.isLoading = true;
    const paramEdit = {
      id: this.targetId,
      targetRMCode: this.formDetailTarget.get('targetRMCode').value,
      targetBranchCode: this.formDetailTarget.get('targetBranchName').value,
      targetStatus: this.formDetailTarget.get('targetStatus').value,
      rsId: this.objFunction?.rsId,
      scope: Scopes.UPDATE,
      division: this.divisionCode,
    };
    this.saleTargetApi.updateTargetSale(paramEdit).subscribe(
      (result) => {
        this.isLoading = false;
        this.messageService.success(this.notificationMessage.success);
        this.back();
      },
      (e) => {
        if (_.isEmpty(e?.error?.description)) {
          this.messageService.error(this.notificationMessage.error);
          this.isLoading = false;
        } else {
          this.messageService.error(e?.error?.description);
          this.isLoading = false;
        }
      }
    );
  }
}
