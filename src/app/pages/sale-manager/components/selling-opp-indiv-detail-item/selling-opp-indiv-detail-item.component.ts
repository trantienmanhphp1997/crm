import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { FunctionCode, Scopes } from 'src/app/core/utils/common-constants';
import { global } from '@angular/compiler/src/util';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { SaleManagerApi } from '../../api';
import { Utils } from 'src/app/core/utils/utils';
import _ from 'lodash';

@Component({
  selector: 'app-selling-opp-indiv-detail-item',
  templateUrl: './selling-opp-indiv-detail-item.component.html',
  styleUrls: ['./selling-opp-indiv-detail-item.component.scss'],
})
export class SellingOppIndivDetailItemComponent extends BaseComponent implements OnInit {
  @Input('data') data: any;
  @Output() onClickEdit: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('table') table: DatatableComponent;

  isLoading = false;
  limit = global.userConfig.pageSize;
  pageable: Pageable;
  params: any = {
    pageNumber: 0,
    pageSize: global?.userConfig?.pageSize,
    rsId: '',
    scope: Scopes.VIEW,
  };

  expandableAreasState = [];

  numberCurency: number;
  textCurency: string;
  scopes: Array<any>;

  isUpdator = false;

  constructor(injector: Injector, private saleManagerApi: SaleManagerApi) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.SELLING_OPPORTUNITY_INDIV}`);
    this.scopes = _.get(this.objFunction, 'scopes');
  }

  ngOnInit(): void {
    this.checkCurency();
    this.isUpdator = this.checkIsUpdator();
  }

  checkIsUpdator() {
    let result = false;
    if (this.data?.updator && this.checkScopes(['UPDATE'])) {
      result = true;
    }
    return result;
  }

  changeDropdownArrowState(index: number) {
    this.expandableAreasState[index] = !this.expandableAreasState[index];
  }

  checkScopes(codes: Array<any>) {
    if (Utils.isArrayEmpty(codes)) return false;
    return Utils.isArrayNotEmpty(_.filter(this.scopes, (x) => codes.includes(x)));
  }

  checkCurency() {
    if (this.data?.amount < 1000000000) {
      if (this.data?.amount < 1000000) {
        this.numberCurency = this.data?.amount;
        this.textCurency = '';
      } else {
        this.numberCurency = Math.round(this.data?.amount) / 1000000;
        this.textCurency = 'triệu';
      }
    } else {
      this.numberCurency = Math.round(this.data?.amount) / 1000000000;
      this.textCurency = 'tỷ';
    }
  }

  edit(data) {
    this.onClickEdit.emit(data);
  }
}
