import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellingOppIndivDetailItemComponent } from './selling-opp-indiv-detail-item.component';

describe('SellingOppIndivDetailItemComponent', () => {
  let component: SellingOppIndivDetailItemComponent;
  let fixture: ComponentFixture<SellingOppIndivDetailItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellingOppIndivDetailItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellingOppIndivDetailItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
