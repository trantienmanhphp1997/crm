import { Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { global } from '@angular/compiler/src/util';
import {
  CommonCategory,
  Scopes,
  maxInt32,
  FunctionCode,
  functionUri,
  StatusWork,
  TaskType,
  ScreenType,
  ConfirmType,
  ActionType,
} from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { empty, forkJoin, of } from 'rxjs';
import _ from 'lodash';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { FileService } from 'src/app/core/services/file.service';
import { RmBlockApi } from 'src/app/pages/rm/apis';
import { cleanDataForm } from 'src/app/core/utils/function';
import { SaleManagerApi } from '../../api';
import { ChooseBranchesModalComponent } from 'src/app/pages/system/components/choose-branches-modal/choose-branches-modal.component';
import { RmModalComponent } from 'src/app/pages/rm/components/rm-modal/rm-modal.component';
import { SaleSuggestApi } from '../../api/sale-suggest.api';
import { ActivityActionComponent } from 'src/app/shared/components/activity-action/activity-action.component';
import { ActivityHistoryComponent } from 'src/app/shared/components';
import { CustomerDetailApi } from 'src/app/pages/customer-360/apis/customer.api';
import { ModalConfirmCreateChatComponent } from '../modal-confirm-create-chat/modal-confirm-create-chat.component';
import { MenuItem } from 'primeng/api';
import { Utils } from 'src/app/core/utils/utils';
import {CampaignsModalComponent, CampaignsRmModalComponent} from '../../../campaigns/components';
import {MbeeChatService} from '../../../../core/services/mbee-chat.service';
import {createGroupChat11} from '../../../../core/utils/mb-chat';
import {AppFunction} from '../../../../core/interfaces/app-function.interface';


@Component({
  selector: 'app-sale-suggest-list',
  templateUrl: './sale-suggest-list.component.html',
  styleUrls: ['./sale-suggest-list.component.scss'],
})
export class SaleSuggestListComponent extends BaseComponent implements OnInit {
  isLoading = false;
  @ViewChild('table') table: DatatableComponent;
  limit = global.userConfig.pageSize;
  pageable: Pageable;
  params: any = {
    pageNumber: 0,
    pageSize: global?.userConfig?.pageSize,
    rsId: '',
    scope: Scopes.VIEW,
  };
  objFunctionRm: any;
  objFunctionCustomer: any;
  propDetail: any;

  lastUpdateDate: any;
  categoryCommon: any;
  listData = [];
  total: number = 0;
  isOpenMore = true;

  segmentOptions: any[];
  segmentSelected: string[] = [];
  segmentAll: any[] = [];
//  segmentSelectedAll = ['all'];
  segmentSelectedAll = [];
  listPH: any[] = [];

  sectorOptions: any[] = [];
  sectorSelected: any[] = [];

  customerTypeOptions: any[];
  customerTypeSelected: string[] = [];
  customerTypeAll: any[] = [];
//  customerTypeSelectedAll = ['all'];
  customerTypeSelectedAll = [];
  ageGroupOptions: any[];
  ageGroupSelected: string[] = [];
  ageGroupAll: any[] = [];
//  ageGroupSelectedAll = ['all'];
  ageGroupSelectedAll = [];
  selectedProducts: any[];
  useProducts: any[];
  selectedUseProduct: string[];
  notUseProducts: any[];
  arrYes: any[];
  arrNo: any[];
  selectedNotUseProduct: string[];
  rangeValuesSurplusCASA: number[] = [0, 1000000];
  rangeValuesSurplusHDV: number[] = [0, 1000000];
  rangeValuesTDN: number[] = [0, 1000000];
  minValue = 0;
  maxValue = 1000000;
  maxValueForMin = 990000;
  step = 1;

  notShowTextAll = true;
  facilities: Array<any>;
  facilityCode: Array<string>;
  employeeCode: Array<string>;
  employeeCodeParams: Array<string>;

  termData = [];

  model: any = {};
  listGender =  [
    {
      code: '',
      name: 'Tất cả'
    },
    {
      code: 'nam',
      name: 'Nam'
    },
    {
      code: 'nu',
      name: 'Nữ'
    }];
  isLoadFirst = true;
  selectedGender = '';

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private saleManagerApi: SaleManagerApi,
    private fileService: FileService,
    private rmBlockApi: RmBlockApi,
    private customerDetailApi: CustomerDetailApi,
    private saleSuggestApi: SaleSuggestApi,
    private mbeeChatService: MbeeChatService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.objFunctionMBChat = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.APP_CHAT}`);
    this.scopes = _.get(this.objFunction, 'scopes');
    this.propDetail = this.router.getCurrentNavigation()?.extras?.state;
  }
  formSearch = this.fb.group({
    customerCode: [''],
    fullName: [''],
    mobileNumber: [''],
    registrationNumber: [''],
    divisionCode: [''],
    rmCode: [''],
    opportunityBranch: [''],
    status: [''],
    opportunityCode: [''],
    rsId: '',
    scope: Scopes.VIEW,
    identifiedNumber: [''],
  });

  formSlider = this.fb.group({
    minCASA: '',
    maxCASA: '',
    minHDV: '',
    maxHDV: '',
    minTDN: '',
    maxTDN: '',
  });

  listDivision = [];
  listRmManager = [];
  listBranchesOpp = [];
  listStatusOpp = [];
  dataDivisionBranch: any = {};
  listBranch = [];
  lstLevelRmCode: any;
  prevParams: any;
  prop: any;
  permissionOpp = false;
  allRm = true;
  listRuleView = [];
  allBranch = true;

  isRMUB = false;
  isDisableTDN = false;
  rowMenu: MenuItem[];
  itemSelected: any;
  isClickEvent = false;
  scopes: Array<any>;
  isShowCreateOppBtn = false;
  limitImport: number;
  paramsFilterOld: any;
  isShowChat = false;
  objFunctionMBChat: AppFunction;


  ngOnInit() {
    this.isShowChat = this.objFunctionMBChat?.scopes.includes('VIEW');
    this.checkRMUB();
    this.getDataFilter();
    this.pageable = {
      totalElements: 0,
      totalPages: 1,
      currentPage: this.params.pageNumber,
      size: this.limit,
    };
    this.employeeCode = _.get(this.fields, 'all');
    // CRMCN-3672 - NAMNH - START
    this.translate.get('btnAction').subscribe((btnAction) => {
      this.rowMenu = [];
      if (!this.isShowChat && this.isShowCreateOppBtn) {
        this.rowMenu.push({
          label: 'Tạo cơ hội bán',
          command: (e) => {
            this.createOppKHCN(this.itemSelected);
          },
        });
      }

      this.rowMenu.push({
        // sửa file vi.json
        label: btnAction?.sendEmail,
        command: (e) => {
          this.sendMail(this.itemSelected);
        },
      });
      this.rowMenu.push({
        // sửa file vi.json
        label: btnAction?.historyAction,
        command: (e) => {
          this.historyAction(this.itemSelected);
        },
      });
    });
    this.displayButtons();
    // CRMCN-3672 - NAMNH - END
  //  this.search(true, true);
    console.log('nhay vao day: ',this.isLoadFirst);
    if(this.isLoadFirst){
      this.isLoading = false;
      this.messages = 'Vui lòng chọn tiêu chí tìm kiếm.';
    }
  }

  checkRMUB() {
    this.isLoading = true;
    const params = {
      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW,
    };
    forkJoin([
      this.categoryService.getBranchesOfUser(this.objFunction.rsId, Scopes.VIEW),
      this.saleSuggestApi.checkUserRMUB(params)
    ])
    .subscribe(
      ([branchOfUser,res]) => {
        if (res) {
          if (res.role === 'RM/UB') {
            this.isRMUB = true;
          } else {
            this.isRMUB = false;
          }
        }
        this.facilities = branchOfUser || [];
      },
      (e) => {
        console.log(e);
        this.messageService.error('Thực hiện không thành công');
        this.isLoading = false;
      }
    );
  }

  keyPressNumbers(event) {
    var charCode = event.which ? event.which : event.keyCode;
    // Only Numbers 0-9
    if (charCode < 48 || charCode > 57) {
      event.preventDefault();
      return false;
    } else {
      return true;
    }
  }

  onBlurMinTDN(value) {
    if (value === '') {
      this.rangeValuesTDN[0] = this.minValue;
      this.formSlider.controls['minTDN'].setValue(this.minValue);
    } else {
      this.rangeValuesTDN[0] = parseInt(value);
      this.formSlider.controls['minTDN'].setValue(parseInt(value));
      if (this.rangeValuesTDN[1] > this.maxValueForMin) {
        if (value > this.maxValueForMin) {
          this.rangeValuesTDN[0] = this.maxValueForMin;
          this.formSlider.controls['minTDN'].setValue(this.maxValueForMin);
        }
      } else {
        if (value > this.rangeValuesTDN[1]) {
          this.rangeValuesTDN[0] = this.rangeValuesTDN[1];
          this.formSlider.controls['minTDN'].setValue(this.rangeValuesTDN[1]);
        }
      }
    }
    if (value < this.minValue) {
      this.rangeValuesTDN[0] = this.minValue;
    }
  }

  inputMinTDN(value) {}

  keyUpMinTDN(event) {
    let charCode = event.which ? event.which : event.keyCode;
    if (charCode == 38) {
      this.rangeValuesTDN[0] += this.step;
      if (this.rangeValuesTDN[0] > this.maxValueForMin) {
        this.rangeValuesTDN[0] = this.maxValueForMin;
      } else if (this.rangeValuesTDN[0] >= this.rangeValuesTDN[1]) {
        this.rangeValuesTDN[0] = this.rangeValuesTDN[1];
      }
    } else if (charCode == 40) {
      this.rangeValuesTDN[0] -= this.step;
      if (this.rangeValuesTDN[0] < this.minValue) {
        this.rangeValuesTDN[0] = this.minValue;
      }
    }
  }

  onBlurMaxTDN(value) {
    if (value === '') {
      this.rangeValuesTDN[1] = this.maxValue;
      this.formSlider.controls['maxTDN'].setValue(this.maxValue);
    } else {
      this.rangeValuesTDN[1] = parseInt(value);
      this.formSlider.controls['maxTDN'].setValue(parseInt(value));
      if (value <= this.rangeValuesTDN[0]) {
        this.rangeValuesTDN[1] = this.rangeValuesTDN[0];
        this.formSlider.controls['maxTDN'].setValue(this.rangeValuesTDN[0]);
      } else {
        if (value > this.maxValue) {
          this.rangeValuesTDN[1] = this.maxValue;
          this.formSlider.controls['maxTDN'].setValue(this.maxValue);
        }
      }
    }
    if (value < this.minValue) {
      this.rangeValuesTDN[1] = this.maxValue;
    }
  }

  inputMaxTDN(value) {}

  keyUpMaxTDN(event) {
    let charCode = event.which ? event.which : event.keyCode;
    if (charCode == 38) {
      this.rangeValuesTDN[1] += this.step;
      if (this.rangeValuesTDN[1] > this.maxValue) {
        this.rangeValuesTDN[1] = this.maxValue;
      }
    } else if (charCode == 40) {
      this.rangeValuesTDN[1] -= this.step;
      if (this.rangeValuesTDN[1] <= this.rangeValuesTDN[0]) {
        this.rangeValuesTDN[1] = this.rangeValuesTDN[0];
      }
    }
  }

  onBlurMinCASA(value) {
    if (value === '') {
      this.rangeValuesSurplusCASA[0] = this.minValue;
      this.formSlider.controls['minCASA'].setValue(this.minValue);
    } else {
      this.rangeValuesSurplusCASA[0] = parseInt(value);
      this.formSlider.controls['minCASA'].setValue(parseInt(value));
      if (this.rangeValuesSurplusCASA[1] > this.maxValueForMin) {
        if (value > this.maxValueForMin) {
          this.rangeValuesSurplusCASA[0] = this.maxValueForMin;
          this.formSlider.controls['minCASA'].setValue(this.maxValueForMin);
        }
      } else {
        if (value > this.rangeValuesSurplusCASA[1]) {
          this.rangeValuesSurplusCASA[0] = this.rangeValuesSurplusCASA[1];
          this.formSlider.controls['minCASA'].setValue(this.rangeValuesSurplusCASA[1]);
        }
      }
    }
    if (value < this.minValue) {
      this.rangeValuesSurplusCASA[0] = this.minValue;
    }
  }

  inputMinCASA(value) {}

  keyUpMinCASA(event) {
    let charCode = event.which ? event.which : event.keyCode;
    if (charCode == 38) {
      this.rangeValuesSurplusCASA[0] += this.step;
      if (this.rangeValuesSurplusCASA[0] > this.maxValueForMin) {
        this.rangeValuesSurplusCASA[0] = this.maxValueForMin;
      } else if (this.rangeValuesSurplusCASA[0] >= this.rangeValuesSurplusCASA[1]) {
        this.rangeValuesSurplusCASA[0] = this.rangeValuesSurplusCASA[1];
      }
    } else if (charCode == 40) {
      this.rangeValuesSurplusCASA[0] -= this.step;
      if (this.rangeValuesSurplusCASA[0] < this.minValue) {
        this.rangeValuesSurplusCASA[0] = this.minValue;
      }
    }
  }

  onBlurMaxCASA(value) {
    if (value === '') {
      this.rangeValuesSurplusCASA[1] = this.maxValue;
      this.formSlider.controls['maxCASA'].setValue(this.maxValue);
    } else {
      this.rangeValuesSurplusCASA[1] = parseInt(value);
      this.formSlider.controls['maxCASA'].setValue(parseInt(value));
      if (value <= this.rangeValuesSurplusCASA[0]) {
        this.rangeValuesSurplusCASA[1] = this.rangeValuesSurplusCASA[0];
        this.formSlider.controls['maxCASA'].setValue(this.rangeValuesSurplusCASA[0]);
      } else {
        if (value > this.maxValue) {
          this.rangeValuesSurplusCASA[1] = this.maxValue;
          this.formSlider.controls['maxCASA'].setValue(this.maxValue);
        }
      }
    }
    if (value < this.minValue) {
      this.rangeValuesSurplusCASA[1] = this.maxValue;
    }
  }

  inputMaxCASA(value) {}

  keyUpMaxCASA(event) {
    let charCode = event.which ? event.which : event.keyCode;
    if (charCode == 38) {
      this.rangeValuesSurplusCASA[1] += this.step;
      if (this.rangeValuesSurplusCASA[1] > this.maxValue) {
        this.rangeValuesSurplusCASA[1] = this.maxValue;
      }
    } else if (charCode == 40) {
      this.rangeValuesSurplusCASA[1] -= this.step;
      if (this.rangeValuesSurplusCASA[1] <= this.rangeValuesSurplusCASA[0]) {
        this.rangeValuesSurplusCASA[1] = this.rangeValuesSurplusCASA[0];
      }
    }
  }

  onBlurMinHDV(value) {
    if (value === '') {
      this.rangeValuesSurplusHDV[0] = this.minValue;
      this.formSlider.controls['minHDV'].setValue(this.minValue);
    } else {
      this.rangeValuesSurplusHDV[0] = parseInt(value);
      this.formSlider.controls['minHDV'].setValue(parseInt(value));
      if (this.rangeValuesSurplusHDV[1] > this.maxValueForMin) {
        if (value > this.maxValueForMin) {
          this.rangeValuesSurplusHDV[0] = this.maxValueForMin;
          this.formSlider.controls['minHDV'].setValue(this.maxValueForMin);
        }
      } else {
        if (value > this.rangeValuesSurplusHDV[1]) {
          this.rangeValuesSurplusHDV[0] = this.rangeValuesSurplusHDV[1];
          this.formSlider.controls['minHDV'].setValue(this.rangeValuesSurplusHDV[1]);
        }
      }
    }
    if (value < this.minValue) {
      this.rangeValuesSurplusHDV[0] = this.minValue;
    }
  }

  inputMinHDV(value) {}

  keyUpMinHDV(event) {
    let charCode = event.which ? event.which : event.keyCode;
    if (charCode == 38) {
      this.rangeValuesSurplusHDV[0] += this.step;
      if (this.rangeValuesSurplusHDV[0] > this.maxValueForMin) {
        this.rangeValuesSurplusHDV[0] = this.maxValueForMin;
      } else if (this.rangeValuesSurplusHDV[0] >= this.rangeValuesSurplusHDV[1]) {
        this.rangeValuesSurplusHDV[0] = this.rangeValuesSurplusHDV[1];
      }
    } else if (charCode == 40) {
      this.rangeValuesSurplusHDV[0] -= this.step;
      if (this.rangeValuesSurplusHDV[0] < this.minValue) {
        this.rangeValuesSurplusHDV[0] = this.minValue;
      }
    }
  }

  onBlurMaxHDV(value) {
    if (value === '') {
      this.rangeValuesSurplusHDV[1] = this.maxValue;
      this.formSlider.controls['maxHDV'].setValue(this.maxValue);
    } else {
      this.rangeValuesSurplusHDV[1] = parseInt(value);
      this.formSlider.controls['maxHDV'].setValue(parseInt(value));
      if (value <= this.rangeValuesSurplusHDV[0]) {
        this.rangeValuesSurplusHDV[1] = this.rangeValuesSurplusHDV[0];
        this.formSlider.controls['maxHDV'].setValue(this.rangeValuesSurplusHDV[0]);
      } else {
        if (value > this.maxValue) {
          this.rangeValuesSurplusHDV[1] = this.maxValue;
          this.formSlider.controls['maxHDV'].setValue(this.maxValue);
        }
      }
    }
    if (value < this.minValue) {
      this.rangeValuesSurplusHDV[1] = this.maxValue;
    }
  }

  inputMaxHDV(value) {}

  keyUpMaxHDV(event) {
    let charCode = event.which ? event.which : event.keyCode;
    if (charCode == 38) {
      this.rangeValuesSurplusHDV[1] += this.step;
      if (this.rangeValuesSurplusHDV[1] > this.maxValue) {
        this.rangeValuesSurplusHDV[1] = this.maxValue;
      }
    } else if (charCode == 40) {
      this.rangeValuesSurplusHDV[1] -= this.step;
      if (this.rangeValuesSurplusHDV[1] <= this.rangeValuesSurplusHDV[0]) {
        this.rangeValuesSurplusHDV[1] = this.rangeValuesSurplusHDV[0];
      }
    }
  }

  onChangeTDN(event) {
    const arr = event.values;
    if (arr[1] >= this.maxValueForMin) {
      if (arr[0] > this.maxValueForMin) {
        this.rangeValuesTDN[0] = this.maxValueForMin;
      }
    } else {
      if (arr[0] > this.rangeValuesTDN[1]) {
        this.rangeValuesTDN[0] = this.rangeValuesTDN[1];
      }
    }
  }

  handleChangeTDN(event) {
    const min = event.values[0];
    if (min > this.maxValueForMin) {
      this.formSlider.controls['minTDN'].setValue(this.maxValueForMin);
      this.rangeValuesTDN[0] = this.maxValueForMin;
    }
  }

  onChangeCASA(event) {
    const arr = event.values;
    if (arr[1] >= this.maxValueForMin) {
      if (arr[0] > this.maxValueForMin) {
        this.rangeValuesSurplusCASA[0] = this.maxValueForMin;
      }
    } else {
      if (arr[0] > this.rangeValuesSurplusCASA[1]) {
        this.rangeValuesSurplusCASA[0] = this.rangeValuesSurplusCASA[1];
      }
    }
  }

  handleChangeCASA(event) {
    const min = event.values[0];
    if (min > this.maxValueForMin) {
      this.formSlider.controls['minCASA'].setValue(this.maxValueForMin);
      this.rangeValuesSurplusCASA[0] = this.maxValueForMin;
    }
  }

  onChangeHDV(event) {
    const arr = event.values;
    if (arr[1] >= this.maxValueForMin) {
      if (arr[0] > this.maxValueForMin) {
        this.rangeValuesSurplusHDV[0] = this.maxValueForMin;
      }
    } else {
      if (arr[0] > this.rangeValuesSurplusHDV[1]) {
        this.rangeValuesSurplusHDV[0] = this.rangeValuesSurplusHDV[1];
      }
    }
  }

  handleChangeHDV(event) {
    const min = event.values[0];
    if (min > this.maxValueForMin) {
      this.formSlider.controls['minHDV'].setValue(this.maxValueForMin);
      this.rangeValuesSurplusHDV[0] = this.maxValueForMin;
    }
  }

  /* Bỏ set màu cho phân khúc */
  // updateCss() {
  //   let style = '';
  //   if (this.listPH.length > 0) {
  //     this.listPH.forEach((element) => {
  //       if (element.value) {
  //         style = `width: 3600px; background-color: ` + element.value + ` !important;`;
  //       }
  //       let selects = document.getElementsByClassName(element.className);
  //       for (var i = 0, il = selects.length; i < il; i++) {
  //         selects[i].setAttribute('style', style);
  //       }
  //     });
  //   }
  // }

  unCheckAllSegment() {
    this.segmentSelectedAll = [];
    if (this.segmentSelected.length === 0) {
      this.segmentSelectedAll = ['all'];
    } else if (this.segmentSelected.length === this.segmentOptions.length) {
      this.segmentSelected = [];
      if (this.segmentSelectedAll.length === 0) {
        this.segmentSelectedAll = ['all'];
      }
    }
    this.setSector();
  }

  allClickSegment() {
    this.segmentSelectedAll = ['all'];
    this.segmentSelected = [];
    this.setSector();
  }

  unCheckAllCustomerType() {
    this.customerTypeSelectedAll = [];
    if (this.customerTypeSelected.length === 0) {
      this.customerTypeSelectedAll = ['all'];
    } else if (this.customerTypeSelected.length === this.customerTypeOptions.length) {
      this.customerTypeSelected = [];
      if (this.customerTypeSelectedAll.length === 0) {
        this.customerTypeSelectedAll = ['all'];
      }
    }
  }

  allClickCustomerType() {
    this.customerTypeSelectedAll = ['all'];
    this.customerTypeSelected = [];
  }

  unCheckAllAgeGroup() {
    this.ageGroupSelectedAll = [];
    if (this.ageGroupSelected.length === 0) {
      this.ageGroupSelectedAll = ['all'];
    } else if (this.ageGroupSelected.length === this.ageGroupOptions.length) {
      this.ageGroupSelected = [];
      if (this.ageGroupSelectedAll.length === 0) {
        this.ageGroupSelectedAll = ['all'];
      }
    }
  }

  allClickAgeGroup() {
    this.ageGroupSelectedAll = ['all'];
    this.ageGroupSelected = [];
  }

  getSelectedValue(dataSelected, dataAll) {
    let result = null;
    if (dataSelected.length > 0 && dataAll.length === 0) {
      result = dataSelected;
    }
    return result;
  }

  getDataFilter() {
    this.isLoading = true;
    this.categoryCommon = this.sessionService.getSessionData(FunctionCode.SALE_SUGGEST_LIST);
    if (!this.categoryCommon) {
      const listCommonCategory = [
        CommonCategory.SEGMENT_SUGGEST_SELLING_PRODUCT,
        CommonCategory.CUSTOMER_TYPE_SUGGEST_SELLING_PRODUCT,
        CommonCategory.AGE_SUGGEST_SELLING_PRODUCT,
        CommonCategory.CUSTOMER_USED_PRODUCT_SUGGEST_SELLING_PRODUCT,
        CommonCategory.CUSTOMER_NOT_USED_PRODUCT_SUGGEST_SELLING_PRODUCT,
        CommonCategory.SECTOR_SUGGEST_SELLING_PRODUCT,
        CommonCategory.LIMIT_IMPORT_CUSTOMER_TO_CAMPAIGN,
      ];
      this.commonService.getCommonCategoryByListCode(listCommonCategory).subscribe(
        (result) => {
          const listSegment = [];
          const listCustomer = [];
          const listAge = [];
          const listCustomerUseProduct = [];
          const listCustomerNotUseProduct = [];
          const listSector = [];
          let limitImport: number;
          if (result?.length > 0) {
            result.forEach((category) => {
              if (category.commonCategoryCode == CommonCategory.SEGMENT_SUGGEST_SELLING_PRODUCT) {
                listSegment.push(category);
              } else if (category.commonCategoryCode == CommonCategory.CUSTOMER_TYPE_SUGGEST_SELLING_PRODUCT) {
                listCustomer.push(category);
              } else if (category.commonCategoryCode == CommonCategory.AGE_SUGGEST_SELLING_PRODUCT) {
                listAge.push(category);
              } else if (category.commonCategoryCode == CommonCategory.CUSTOMER_USED_PRODUCT_SUGGEST_SELLING_PRODUCT) {
                listCustomerUseProduct.push(category);
                listCustomerNotUseProduct.push(category);
              } else if (category.commonCategoryCode == CommonCategory.SECTOR_SUGGEST_SELLING_PRODUCT) {
                listSector.push(category);
              } else if (category.commonCategoryCode == CommonCategory.LIMIT_IMPORT_CUSTOMER_TO_CAMPAIGN) {
                limitImport = Number(category.value);
              }
            });
          }
          this.sessionService.setSessionData(FunctionCode.SALE_SUGGEST_LIST, {
            listSegment,
            listCustomer,
            listAge,
            listCustomerUseProduct,
            listCustomerNotUseProduct,
            listSector,
            limitImport,
          });
          this.setDataFilter();
        },
        () => {
          this.isLoading = false;
        }
      );
    } else {
      this.setDataFilter();
    }
  }

  setSector() {
    this.saleSuggestApi.getSectorBySegment(this.segmentSelected).subscribe((res) => {
      if (res) {
        this.sectorOptions = [];
        res.forEach((item) => {
          let obj = {
            name: item,
            value: item,
          };
          this.sectorOptions.push(obj);
        });
        this.sectorSelected = [];
        this.sectorOptions.map((item) => this.sectorSelected.push(item.value));
      }
    });
  }

  setDataFilter() {
    this.categoryCommon = this.sessionService.getSessionData(FunctionCode.SALE_SUGGEST_LIST);

    const segmentOptionsSort = this.categoryCommon?.listSegment;
    this.segmentOptions = this.sortArr(segmentOptionsSort) || [];
    this.segmentAll = [{ name: 'Tất cả', value: 'all' }];

    const customerTypeOptionsSort = this.categoryCommon?.listCustomer;
    this.customerTypeOptions = this.sortArr(customerTypeOptionsSort) || [];
    this.customerTypeAll = [{ name: 'Tất cả', value: 'all' }];

    const ageGroupOptionsSort = this.categoryCommon?.listAge;
    this.ageGroupOptions = this.sortArr(ageGroupOptionsSort) || [];
    this.ageGroupAll = [{ name: 'Tất cả', value: 'all' }];

    const useProductSort = this.categoryCommon?.listCustomerUseProduct;
    this.useProducts = this.sortArr(useProductSort) || [];

    const notUseProductSort = this.categoryCommon?.listCustomerNotUseProduct;
    this.notUseProducts = this.sortArr(notUseProductSort) || [];

    this.limitImport = this.categoryCommon?.limitImport;

    this.setSector();
  }

  sortArr(arr) {
    return arr.sort((a, b) => (a.orderNum > b.orderNum ? 1 : b.orderNum > a.orderNum ? -1 : 0));
  }

  changeBranch(event) {
    this.facilityCode = event;
    this.removeRMSelected();
  }

  searchRM() {
    const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
    modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
    modal.componentInstance.branchCodes = this.facilityCode;
    modal.result
      .then((res) => {
        if (res) {
          this.employeeCode = [];
          this.employeeCodeParams = [];
          const listRMSelect = res.listSelected?.map((item) => {
            this.employeeCode.push(item?.t24Employee?.employeeCode);
            this.employeeCodeParams.push(item?.hrisEmployee?.employeeId);
            return {
              hrsCode: item?.hrisEmployee?.employeeId,
              code: item?.t24Employee?.employeeCode,
              name: item?.hrisEmployee?.fullName,
            };
          });
          this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
          this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);

          this.translate.get('fields').subscribe((res) => {
            this.fields = res;
            // const codes = this.employeeCode;
            const codeParams = this.employeeCodeParams;
            // const countCode = (codes) => codes.reduce((a, b) => ({ ...a, [b]: (a[b] || 0) + 1 }), {});
            const countCodeParams = (codeParams) => codeParams.reduce((a, b) => ({ ...a, [b]: (a[b] || 0) + 1 }), {});

            // this.employeeCode = Object.keys(countCode(codes));
            this.employeeCodeParams = Object.keys(countCodeParams(codeParams));
            // this.employeeCode =
            //   _.size(this.employeeCode) === 0 ||
            //   (_.size(this.employeeCode) === 1 && this.employeeCode[0] === _.get(this.fields, 'all'))
            //     ? _.get(this.fields, 'all')
            //     : _.join(this.employeeCode, ', ');
          });

          if (this.employeeCode.length === 0) {
            this.removeRMSelected();
          }
        } else {
          this.removeRMSelected();
        }
      })
      .catch(() => {});
  }

  removeRMSelected() {
    this.employeeCode = _.get(this.fields, 'all');
    this.employeeCodeParams = null;
    this.termData = [];
  }

  removeBranchSelected() {
    this.facilityCode = null;
    this.removeRMSelected();
  }

  checkEmployeeCode() {
    let show = false;
    this.translate.get('fields').subscribe((res) => {
      this.fields = res;
      if (this.employeeCode === _.get(this.fields, 'all')) {
        show = false;
      } else {
        show = true;
      }
    });
    return show;
  }

  setDefaultProductParam() {
    if (this.useProducts) {
      this.arrYes = new Array(this.useProducts.length).fill('empty');
      this.arrNo = new Array(this.useProducts.length).fill('empty');
      this.setFilterUseProduct();
    }
  }

  setParamGroupChat() {
    this.setDefaultProductParam();
    let minCASAParam, maxCASAParam, minHDVParam, maxHDVParam, minTDNParam, maxTDNParam;
    // if (this.isFirstLoad) {
    //   minCASAParam = null;
    //   maxCASAParam = null;
    //   minHDVParam = null;
    //   maxHDVParam = null;
    //   minTDNParam = null;
    //   maxTDNParam = null;
    // } else {
    //   minCASAParam = this.rangeValuesSurplusCASA[0];
    //   maxCASAParam = this.rangeValuesSurplusCASA[1];
    //   minHDVParam = this.rangeValuesSurplusHDV[0];
    //   maxHDVParam = this.rangeValuesSurplusHDV[1];
    //   minTDNParam = this.rangeValuesTDN[0];
    //   maxTDNParam = this.rangeValuesTDN[1];
    // }

    minCASAParam = this.rangeValuesSurplusCASA[0];
    maxCASAParam = this.rangeValuesSurplusCASA[1];
    minHDVParam = this.rangeValuesSurplusHDV[0];
    maxHDVParam = this.rangeValuesSurplusHDV[1];
    minTDNParam = this.rangeValuesTDN[0];
    maxTDNParam = this.rangeValuesTDN[1];

    let tongDuNo = null,
      khTraLuong = null,
      creditCard = null,
      activeApp = null,
      tksd = null,
      dsKieuHoi = null,
      dsBANCAS = null,
      dsSPDT = null;
    if (this.selectedProducts && this.selectedProducts.length === 8) {
      tongDuNo = this.selectedProducts[0];
      khTraLuong = this.selectedProducts[1];
      creditCard = this.selectedProducts[2];
      activeApp = this.selectedProducts[3];
      tksd = this.selectedProducts[4];
      dsKieuHoi = this.selectedProducts[5];
      dsBANCAS = this.selectedProducts[6];
      dsSPDT = this.selectedProducts[7];
    }

    const unit = 1000000;
    let params = {
      phanKhucKH: this.getSelectedValue(this.segmentSelected, this.segmentSelectedAll),
      flagKHActive: this.getSelectedValue(this.customerTypeSelected, this.customerTypeSelectedAll),
      nhomTuoi: this.getSelectedValue(this.ageGroupSelected, this.ageGroupSelectedAll),
      sectors:
        this.sectorSelected.length == this.sectorOptions.length || this.sectorSelected.length === 0
          ? null
          : this.sectorSelected,

      tongDuNo: tongDuNo,
      khTraLuong: khTraLuong,
      creditCard: creditCard,
      activeApp: activeApp,
      tksd: tksd,
      dsKieuHoi: dsKieuHoi,
      dsBANCAS: dsBANCAS,
      dsSPDT: dsSPDT,

      sdbqCASA3MMin: this.setValue(minCASAParam, unit),
      sdbqCASA3MMax: this.setValue(maxCASAParam, unit),
      sdbqTK3MMin: this.setValue(minHDVParam, unit),
      sdbqTK3MMax: this.setValue(maxHDVParam, unit),
      tongdunoMin: this.setValue(minTDNParam, unit),
      tongdunoMax: this.setValue(maxTDNParam, unit),

      rmCode: this.checkEmployeeCode() ? this.employeeCode : null,
      maPGDQly: this.facilityCode || null,

      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW,
      gender: this.selectedGender
    };
    if (this.isDisableTDN) {
      delete params.tongdunoMin;
      delete params.tongdunoMax;
    }
    // console.log(params);
    return params;
  }

  search(isSearch: boolean, useDefaultParam: boolean) {
    if(this.isLoadFirst){
      if(_.isEmpty(this.segmentSelectedAll) && _.isEmpty(this.segmentSelected)){
        this.segmentSelectedAll = ['all'];
      }
      if(_.isEmpty(this.customerTypeSelectedAll) && _.isEmpty(this.customerTypeSelected)){
        this.customerTypeSelectedAll = ['all'];
      }
      if(_.isEmpty(this.ageGroupSelectedAll) && _.isEmpty(this.ageGroupSelected)){
        this.ageGroupSelectedAll = ['all'];
      }
      this.isLoadFirst = false;
      this.isOpenMore = false;
    }
    this.messages = global.messageTable;
    this.isLoading = true;
    this.setDefaultProductParam();
    let minCASAParam, maxCASAParam, minHDVParam, maxHDVParam, minTDNParam, maxTDNParam;
    if (isSearch) {
      this.params.pageNumber = 0;
    }
    if (useDefaultParam) {
      minCASAParam = null;
      maxCASAParam = null;
      minHDVParam = null;
      maxHDVParam = null;
      minTDNParam = null;
      maxTDNParam = null;
    } else {
      minCASAParam = this.rangeValuesSurplusCASA[0];
      maxCASAParam = this.rangeValuesSurplusCASA[1];
      minHDVParam = this.rangeValuesSurplusHDV[0];
      maxHDVParam = this.rangeValuesSurplusHDV[1];
      minTDNParam = this.rangeValuesTDN[0];
      maxTDNParam = this.rangeValuesTDN[1];
    }

    let tongDuNo = null,
      khTraLuong = null,
      creditCard = null,
      activeApp = null,
      tksd = null,
      dsKieuHoi = null,
      dsBANCAS = null,
      dsSPDT = null;
    if (this.selectedProducts && this.selectedProducts.length === 8) {
      tongDuNo = this.selectedProducts[0];
      khTraLuong = this.selectedProducts[1];
      creditCard = this.selectedProducts[2];
      activeApp = this.selectedProducts[3];
      tksd = this.selectedProducts[4];
      dsKieuHoi = this.selectedProducts[5];
      dsBANCAS = this.selectedProducts[6];
      dsSPDT = this.selectedProducts[7];
    }

    const unit = 1000000;
    const paramsFilter = {
      pageNumber: this.params.pageNumber,
      pageSize: this.limit,
      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW,
      // hrsCode: this.currUser?.hrsCode || null,

      phanKhucKH: this.getSelectedValue(this.segmentSelected, this.segmentSelectedAll),
      flagKHActive: this.getSelectedValue(this.customerTypeSelected, this.customerTypeSelectedAll),
      nhomTuoi: this.getSelectedValue(this.ageGroupSelected, this.ageGroupSelectedAll),
      sectors:
        this.sectorSelected.length == this.sectorOptions.length || this.sectorSelected.length === 0
          ? null
          : this.sectorSelected,

      tongDuNo: tongDuNo,
      khTraLuong: khTraLuong,
      creditCard: creditCard,
      activeApp: activeApp,
      tksd: tksd,
      dsKieuHoi: dsKieuHoi,
      dsBANCAS: dsBANCAS,
      dsSPDT: dsSPDT,

      sdbqCASA3MMin: this.setValue(minCASAParam, unit),
      sdbqCASA3MMax: this.setValue(maxCASAParam, unit),
      sdbqTK3MMin: this.setValue(minHDVParam, unit),
      sdbqTK3MMax: this.setValue(maxHDVParam, unit),
      tongdunoMin: this.setValue(minTDNParam, unit),
      tongdunoMax: this.setValue(maxTDNParam, unit),

      rmCode: this.checkEmployeeCode() ? this.employeeCode : null,
      maPGDQly: this.facilityCode || null,
      gender: this.selectedGender
    };
    if (this.isDisableTDN) {
      delete paramsFilter.tongdunoMin;
      delete paramsFilter.tongdunoMax;
    }
    // console.log(paramsFilter);
    this.paramsFilterOld = paramsFilter;
  //  const branchesOfUser = this.categoryService.getBranchesOfUser(this.objFunction.rsId, Scopes.VIEW);
    const fetchAll = this.saleSuggestApi.getSuggestSelling(paramsFilter);
    const countAll = this.saleSuggestApi.countSuggestSelling(paramsFilter);
    forkJoin([fetchAll, countAll]).subscribe(
      ([ listSuggest, resCount]) => {
        if (listSuggest.length > 0) {
          this.lastUpdateDate = new Date(Math.max(...listSuggest.map((e) => new Date(e.ngayDuLieu))));
        }

        let arrTemp = [...listSuggest];
        listSuggest.forEach((element, index) => {
          let ub = '';
          if (element.maUBQly && element.tenUBQly) {
            ub = element.maUBQly + ' - ' + element.tenUBQly;
          }
          arrTemp[index].ub = ub;
        });
        this.listData = arrTemp || [];
        this.total = resCount;
   //     this.facilities = branchesOfUser || [];

        this.pageable = {
          totalElements: this.total,
          totalPages: Math.floor(this.total / this.params.pageSize),
          currentPage: this.params.pageNumber,
          size: this.params.pageSize,
        };
        this.isLoading = false;

        /* Bỏ set màu cho phân khúc */
        // this.listPH = [];
        // this.segmentOptions.forEach((element) => {
        //   if (element.code && element.description) {
        //     this.listPH.push({ className: 'BG_' + element.code, value: element.description });
        //   }
        // });

        // const timeDelay = 5000;
        // const timer = setTimeout(() => {
        //   this.updateCss();
        //   this.isLoading = false;
        //   clearTimeout(timer);
        // }, timeDelay);
      },
      (e) => {
        console.log(e);
        this.messageService.error('Thực hiện không thành công');
        this.isLoading = false;
      }
    );
  }

  setValue(number: any, unit: number) {
    let result;
    if (number === 0) {
      result = 0;
    } else if (number === null) {
      result = null;
    } else {
      result = number * unit;
    }
    return result;
  }

  notUseProductChange(event) {
    if (event.length > 0) {
      if (event.includes("TONGDUNO")) {
        this.isDisableTDN = true;
      } else {
        this.isDisableTDN = false;
      }
    } else {
      this.isDisableTDN = false;
    }
  }

  setFilterUseProduct() {
    if (this.selectedUseProduct) {
      this.arrYes = this.setArr(this.useProducts, this.selectedUseProduct);
    }
    if (this.selectedNotUseProduct) {
      this.arrNo = this.setArr(this.useProducts, this.selectedNotUseProduct);
    }
    this.selectedProducts = this.collectUseProduct(this.useProducts, this.arrYes, this.arrNo);
    // console.log(this.selectedProducts);
  }

  setArr(A, S) {
    let R = [];
    if (A && S) {
      if (A.length > 0) {
        R = new Array(A.length).fill('empty');
        A.forEach((element, index) => {
          if (S.length > 0) {
            for (let i = 0; i < S.length; i++) {
              if (element.value === S[i]) {
                R[index] = S[i];
              }
            }
          }
        });
      }
    }
    return R;
  }

  collectUseProduct(A, Y, N) {
    let B = [];
    if (A && Y && N) {
      if (A.length > 0) {
        for (let i = 0; i < A.length; i++) {
          B.push('empty');
          if (Y[i] === 'empty') {
            if (N[i] != 'empty') {
              B[i] = '0';
            } else {
              B[i] = null;
            }
          } else {
            B[i] = '1';
            if (N[i] != 'empty') {
              B[i] = '';
            }
          }
        }
      }
    }
    return B;
  }

  sendMail(row) {
    this.customerDetailApi.get(row.cif).subscribe((itemCustomer) => {
      this.model = itemCustomer;
      const url = `mailto:${this.model?.customerInfo?.emailDisplay}`;
      window.location.href = url;
    });
  }

  createAction(row) {
    const parent: any = {};
    parent.parentType = TaskType.CUSTOMER;
    parent.parentId = row.cif;
    parent.parentName = row.customerName;
    const modal = this.modalService.open(ActivityActionComponent, {
      windowClass: 'create-activity-modal',
      scrollable: true,
    });
    modal.componentInstance.parent = parent;
    modal.componentInstance.type = ScreenType.Create;
    modal.componentInstance.isShowFuture = true;
  }

  historyAction(row) {
    const parent: any = {};
    parent.parentType = TaskType.CUSTOMER;
    parent.parentId = row.cif;
    parent.parentName = row.customerName;
    const modal = this.modalService.open(ActivityHistoryComponent, {
      windowClass: 'create-activity-modal',
      scrollable: true,
    });
    modal.componentInstance.parent = parent;
    modal.componentInstance.type = ScreenType.Create;
    modal.componentInstance.isShowFuture = true;
  }

  openMore() {
    this.isOpenMore = !this.isOpenMore;
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.pageNumber = pageInfo.offset;
    if(!this.isLoadFirst){
      this.search(false, false);
    }
  }

  handleChangePageSize(event) {
    this.params.pageNumber = 0;
    this.params.pageSize = event;
    this.limit = event;
    this.search(false, false);
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      localStorage.setItem('DETAIL_CUS_ID', JSON.stringify(item.cif));
      const url = this.router.serializeUrl(
        this.router.createUrlTree([functionUri.customer_360_manager], {
          skipLocationChange: true,
        })
      );
      window.open(url, '_blank');
    }
    if (event.type === 'click') {
      this.itemSelected = event.row;
    }
  }

  exportFile() {
    return;
    if (!this.maxExportExcel) {
      return;
    }
    if (+this.pageable?.totalElements <= 0) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.isLoading = true;
    const paramExport = {
      ...this.formSearch.value,
    };
    paramExport.rsId = this.objFunction.rsId;
    paramExport.pageNumber = 0;
    paramExport.pageSize = maxInt32;
    if (_.isEmpty(paramExport.rmCode)) {
      this.allRm = true;
      let listRmManagerSearch = [...this.listRmManager];
      paramExport.listAssignTo = _.remove(listRmManagerSearch, (i) => !_.isEmpty(i?.code)).map((item) => item.code);
    } else {
      this.allRm = false;
      let listAssignTo = [];
      listAssignTo.push(paramExport.rmCode);
      paramExport.listAssignTo = listAssignTo;
    }
    delete paramExport.rmCode;
    if (_.isEmpty(paramExport.opportunityBranch)) {
      this.allBranch = true;
      let listBranchesOppSearch = [...this.listBranchesOpp];
      paramExport.lstBranches = _.remove(listBranchesOppSearch, (i) => !_.isEmpty(i?.code)).map((item) => item.code);
    } else {
      this.allBranch = false;
      let listBranchesOpp = [];
      listBranchesOpp.push(paramExport.opportunityBranch);
      paramExport.lstBranches = listBranchesOpp;
    }
    paramExport.allBranch = this.allBranch;
    paramExport.allRm = this.allRm;
    paramExport.listRuleView = this.listRuleView;
    delete paramExport.opportunityBranch;
    this.saleManagerApi.excelOpportunity(paramExport).subscribe(
      (res) => {
        if (res) {
          this.download(res);
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(_.get(this.notificationMessage, 'export_error'));
        this.isLoading = false;
      }
    );
  }

  download(fileId: string) {
    let todayExcel = new Date();
    let titleExcel =
      'co-hoi-ban-' +
      this.currUser.username +
      '-' +
      todayExcel.getFullYear().toString() +
      (todayExcel.getMonth() + 1).toString() +
      todayExcel.getDate().toString() +
      '.xlsx';
    this.fileService.downloadFile(fileId, titleExcel).subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }

  createGroupChat() {
    this.isLoading = true;
    let data = this.setParamGroupChat();

    this.saleSuggestApi.countSuggestSelling(data).subscribe(
      (response: any) => {
        this.isLoading = false;
        if (response === 0) {
          // no data: show popup err
          let res = {
            message: 'Không có khách hàng để tạo nhóm chat',
            groupName: '',
          };
          this.openModalInfo(res);
        } else {
          const confirm = this.modalService.open(ModalConfirmCreateChatComponent, { windowClass: 'confirm-dialog' });
          confirm.componentInstance.data = data;
          confirm.result
            .then((res) => {
              if (res) {
                if (res.code === 1) {
                  this.openModalInfo(res);
                }
              }
            })
            .catch(() => {});
        }
      },
      (e) => {
        console.log(e);
        this.messageService.error(_.get(e, 'error.description'));
        this.isLoading = false;
      }
    );
  }

  openModalInfo(res: any) {
    const confirm = this.modalService.open(ModalConfirmCreateChatComponent, { windowClass: 'confirm-dialog' });
    confirm.componentInstance.type = ConfirmType.CusError;
    confirm.componentInstance.message = res.message;
    confirm.componentInstance.groupNameInput = res.groupName;
    confirm.result
      .then((res) => {
        if (res) {
          console.log(res);
        }
      })
      .catch(() => {});
  }


  addToCampain() {
    if (this.limitImport < this.pageable.totalElements) {
      this.confirmService.warn('Vượt quá số lượng tối đa được phép đẩy vào chiến dịch. Vui lòng chọn lại danh sách KH.').then();
      return;
    }

    if (this.isRMUB) {
      const modal = this.modalService.open(CampaignsRmModalComponent, { windowClass: 'list__campaigns-rm-modal' });
      modal.componentInstance.paramsFilterOld = this.paramsFilterOld;
      modal.result
        .then((res) => {
          if (res) {
          }
        })
        .catch(() => { });
    } else {
      // nếu CBQL chỉ có 1 chi nhánh thì vào thẳng màn hình chiến dịch
      if (this.facilities.length === 1) {
        const modal2 = this.modalService.open(CampaignsModalComponent, { windowClass: 'list__campaigns-modal' });
        modal2.componentInstance.listBranchChoose = _.map(this.facilities, 'code');
        modal2.componentInstance.paramsFilterOld = this.paramsFilterOld;
        modal2.result
          .then((res2) => {
            if (res2) {
            }
          })
          .catch(() => { });
      } else {
        const modal = this.modalService.open(ChooseBranchesModalComponent, { windowClass: 'tree__branches-modal' });
        modal.componentInstance.listBranchOld = [];
        modal.componentInstance.listBranch = this.facilities;
        modal.result
          .then((res) => {
            if (res) {
              const modal2 = this.modalService.open(CampaignsModalComponent, { windowClass: 'list__campaigns-modal' });
              modal2.componentInstance.listBranchChoose = _.map(res, 'code');
              modal2.componentInstance.paramsFilterOld = this.paramsFilterOld;
              modal2.result
                .then((res2) => {
                  if (res2) {
                  }
                })
                .catch(() => { });
            }
          })
          .catch(() => { });
      }
    }
  }

  createOppKHCN(row) {
    const url = this.router.serializeUrl(
      this.router.createUrlTree([functionUri.selling_opp_indiv, 'detail-v2'], {
        skipLocationChange: true,
        queryParams: {
          customerCode: row.cif,
          code: null,
        }
      })
    );
    window.open(url, '_blank');
  }

  onShowMenu(e) {
    this.isClickEvent = true;
  }

  onHideMenu(e) {
    this.isClickEvent = false;
  }

  checkScopes(codes: Array<any>) {
    if (Utils.isArrayEmpty(codes)) return false;
    return Utils.isArrayNotEmpty(_.filter(this.scopes, (x) => codes.includes(x)));
  }

  checkIsDisableCreateOppBtn() {
    this.isShowCreateOppBtn = this.checkScopes(['CREATE_OPPORTUNITY_SALE']) ? true : false;
  }

  displayButtons() {
    this.checkIsDisableCreateOppBtn();
  }
  createGroupChat11(row) {
    this.isLoading = true;
    const customerCode = row.cif;
    this.mbeeChatService.createGroupChat(customerCode).subscribe(value => {
      if (value.data) {
        createGroupChat11(value.data);
      } else {
        this.messageService.error(value.message);
      }
      this.isLoading = false;
    }, error => {
      this.messageService.error(JSON.parse(error.error)?.messages.vn);
      this.isLoading = false;
    });

  }
}
