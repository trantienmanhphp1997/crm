import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import {
  Division,
  FunctionCode,
  functionUri,
  maxInt32,
  Scopes,
  ScreenType,
  StatusWork,
} from 'src/app/core/utils/common-constants';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import _ from 'lodash';
import { Utils } from 'src/app/core/utils/utils';
import { global } from '@angular/compiler/src/util';
import { RmBlockApi } from 'src/app/pages/rm/apis';
import { ProductModalComponent } from '../product-model/product-modal.component';
import { CustomerApi, CustomerDetailApi, CustomerDetailSmeApi } from 'src/app/pages/customer-360/apis/customer.api';
import { SaleManagerApi } from '../../api';

@Component({
  selector: 'sale-manager-detail-component',
  templateUrl: './sale-manager-detail.component.html',
  styleUrls: ['./sale-manager-detail.component.scss'],
})
export class SaleManagerDetailComponent extends BaseComponent implements OnInit {
  isLoading = false;
  @ViewChild('table') table: DatatableComponent;
  @ViewChild('tableTarget') tableTarget: DatatableComponent;
  pageable: Pageable;
  listBranches = [];
  listDivision = [];
  listStatusShare = [];
  listRmManager = [];
  listTitleConfig = [];
  formCreated = this.fb.group({
    customerCode: [''],
    rmCode: [''],
    branchCode: [''],
    currentFundraising: [''],
    currentCredit: [''],
    divisionCode: [''],
    shareStatus: [''],
  });

  formCheckInfo = this.fb.group({
    customerCode: [''],
    divisionCode: [''],
  });

  formProduct = this.fb.group({
    opportunityBranch: [{ value: '', disabled: true }],
    assignTo: [{ value: '', disabled: true }],
    opportunityCode: [{ value: '', disabled: true }],
    statusOpportunity: '',
  });
  objFunctionRm: any;
  limit = global.userConfig.pageSize;
  model: any = {};
  rmManager: any;
  screenType: any;
  opportunityCode: any;
  opportunityId: any;
  customerCode: any;
  statusOpportunity: any;
  divisionCodeOpp: any;
  constructor(
    injector: Injector,
    private customerDetailApi: CustomerDetailApi,
    private customerApi: CustomerApi,

    private saleManagerApi: SaleManagerApi,
    private rmBlockApi: RmBlockApi,
    private customerDetailSmeApi: CustomerDetailSmeApi
  ) {
    super(injector);
    this.route?.data?.subscribe((data) => {
      this.screenType = data?.type;
    });
    this.opportunityCode = this.route.snapshot.queryParams.opportunityCode;
    this.opportunityId = this.route.snapshot.queryParams.opportunityId;
    this.customerCode = this.route.snapshot.queryParams.customerCode;
    this.divisionCodeOpp = this.route.snapshot.queryParams.divisionCodeOpp;

    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.OPPORTUNITY}`);
  }

  params: any = {
    pageNumber: 0,
    pageSize: 10,
    rsId: '',
    scope: Scopes.VIEW,
  };
  is_show = true;
  showData = true;
  showMessage = false;
  dataFake = [];
  infoUserName: any = {};
  listBranch = [];
  dataDivisionBranch: any = {};
  listBranchesOpp = [];
  rmLevelCode = '';
  btnCheckCustomer = true;

  pageableProduct: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };

  paramsTableProduct = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };

  listDataProduct = [];
  dataProduct = [];
  lstLevelRmCode: any;
  dataProductOld = [];
  dataProductDelete = [];
  segmentCustomer: string;
  statusOpportunityCreated: any;
  opportunityBranch: any;
  assignTo: any;
  detailOpportunity: any;
  apiDetailCustomer: Observable<any>;
  apigetSegmentByCustomerCode: Observable<any>;
  listStatusOpp = [
    { code: '1', displayName: 'Tạo mới' },
    { code: '2', displayName: 'Hủy cơ hội bán' },
  ];

  ngOnInit() {
    if (this.screenType === ScreenType.Update) {
      this.formProduct.controls.opportunityBranch.enable();
      this.formProduct.controls.assignTo.enable();
    }
    if (this.divisionCodeOpp === Division.SME) {
      this.apiDetailCustomer = this.saleManagerApi
        .customerDetailOpportunity(this.customerCode)
        .pipe(catchError(() => of(undefined)));
      this.apigetSegmentByCustomerCode = this.customerDetailSmeApi
        .getSegmentByCustomerCode(this.customerCode)
        .pipe(catchError(() => of(undefined)));
    } else {
      this.apiDetailCustomer = this.saleManagerApi
        .customerDetailOpportunity(this.customerCode)
        .pipe(catchError(() => of(undefined)));
      this.apigetSegmentByCustomerCode = of([]);
    }
    this.isLoading = true;
    forkJoin([
      this.apiDetailCustomer,
      this.customerApi.getRmManagerByCustomerCode(this.customerCode).pipe(catchError(() => of(undefined))),
      this.saleManagerApi.detailOpportunity(this.opportunityCode).pipe(catchError(() => of(undefined))),
      this.saleManagerApi.listProductOpportunity(this.opportunityId, 0, maxInt32).pipe(catchError(() => of(undefined))),
      this.rmBlockApi
        .fetch({ page: 0, size: maxInt32, hrsCode: this.currUser?.hrsCode, isActive: true })
        .pipe(catchError(() => of(undefined))),
      this.apigetSegmentByCustomerCode.pipe(catchError(() => of(undefined))),
    ]).subscribe(([itemCustomer, rmManager, detailOpportunity, listProductOpportunity, divisionOfSystem, segment]) => {
      this.detailOpportunity = detailOpportunity;
      this.opportunityBranch = detailOpportunity?.opportunityBranch?.split(' ')[0];
      this.assignTo = detailOpportunity?.assignTo?.split(' ')[0];
      if (this.divisionCodeOpp === Division.SME) {
        this.segmentCustomer = segment;
      } else {
        this.segmentCustomer = itemCustomer?.customerInfo?.customerSegment;
      }
      this.formProduct.controls.statusOpportunity.setValue(detailOpportunity?.status);
      this.formProduct.controls.opportunityCode.setValue(detailOpportunity?.opportunityCode);
      if (+detailOpportunity?.status === 1) {
        this.statusOpportunityCreated = detailOpportunity.status;
        this.statusOpportunity = 'saleOpportunity.statusCreated';
      } else if (+detailOpportunity?.status === 2) {
        this.statusOpportunity = 'saleOpportunity.statusCancel';
      } else {
        this.statusOpportunity = 'saleOpportunity.statusChange';
      }

      this.model = itemCustomer;
      this.rmManager = rmManager;
      this.dataProduct = listProductOpportunity.content;
      this.dataProductOld = [...listProductOpportunity.content];
      this.mapDataProduct();
      // LIST TITLE
      let listDivisioTitle =
        divisionOfSystem?.content?.map((item) => {
          return {
            divisionCode: item?.blockCode,
            levelRmCode: item?.levelRMCode === undefined ? '' : item?.levelRMCode,
          };
        }) || [];

      this.saleManagerApi.getTitleDivisionBranch(listDivisioTitle).subscribe((res) => {
        if (res) {
          this.dataDivisionBranch = res;
          let listBranchConfig = this.dataDivisionBranch.map((item) => item.ruleDivision.branch).toString();
          listBranchConfig = _.unionBy(_.split(listBranchConfig, ','));
          this.lstLevelRmCode = this.dataDivisionBranch.map((item) => item.ruleDivision.ruleAssign).toString();
          this.lstLevelRmCode = _.unionBy(_.split(this.lstLevelRmCode, ','));
          this.getBranchByUser(listBranchConfig);
          this.getRmByBranch(this.opportunityBranch, this.assignTo);
        }
      });
    });
  }

  getBranchByUser(listBranchConfig) {
    const param = {
      listBranchCode: listBranchConfig,
      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW,
    };
    this.saleManagerApi.getBranchByUser(param).subscribe((res) => {
      this.listBranchesOpp = res.map((item) => {
        return {
          code: item.code,
          displayName: item.code + ' - ' + item.name,
        };
      });
      this.formProduct.controls.opportunityBranch.setValue(this.opportunityBranch, { emitEvent: false });
    });
  }

  transferOpportunity() {
    this.isLoading = true;
    this.saleManagerApi.convertOpportunity(this.opportunityId, this.objFunction?.rsId, Scopes.UPDATE).subscribe(
      (resConvert) => {
        this.isLoading = true;
        this.messageService.success(this.notificationMessage.success);
        this.router.navigateByUrl(functionUri.sale_target);
      },
      (e) => {
        this.isLoading = false;
        if (e.error.code) {
          this.messageService.error(e.error.description);
        }
      }
    );
  }

  getRmByBranch(branchCode, assignTo?) {
    let listBranchCode = [];
    listBranchCode.push(branchCode);
    this.formProduct.controls.assignTo.disable();
    const param = {
      lstBranchCode: listBranchCode,
      lstLevelRmCode: this.lstLevelRmCode,
      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW,
    };
    this.saleManagerApi.findRmByBranchAndLevelCode(param).subscribe((res) => {
      if (res) {
        this.listRmManager = res?.map((item) => {
          if (!_.isEmpty(item?.rmCode) && item?.active && item?.statusWork !== StatusWork.NTS) {
            return {
              code: item.rmCode,
              displayName: item.rmCode + ' - ' + item.rmName,
              name: item.rmName,
            };
          }
        });
        this.listRmManager = this.listRmManager.filter((item) => !_.isEmpty(item));
        if (!_.isEmpty(assignTo)) {
          this.formProduct.controls.assignTo.setValue(assignTo);
          this.isLoading = false;
          if (
            _.isEmpty(this.lstLevelRmCode[0]) &&
            this.formProduct.controls.opportunityBranch.value !== this.currUser.branch
          ) {
            this.listRmManager = [];
          }
          if (this.screenType === ScreenType.Update) {
            this.formProduct.controls.assignTo.enable();
          }
        } else {
          this.isLoading = false;
          if (_.isEmpty(this.lstLevelRmCode[0])) {
            if (this.formProduct.controls.opportunityBranch.value === this.currUser.branch) {
              this.formProduct.controls.assignTo.setValue(_.first(this.listRmManager)?.code);
            } else {
              this.listRmManager = [];
              this.formProduct.controls.assignTo.setValue('');
            }
          } else {
            this.listRmManager = this.listRmManager.filter((item) => !_.isEmpty(item));
            this.formProduct.controls.assignTo.setValue(_.first(this.listRmManager)?.code);
          }
          this.formProduct.controls.assignTo.enable();
        }
      }
    });
  }

  ngAfterViewInit() {
    this.formProduct.controls.opportunityBranch.valueChanges.subscribe((value) => {
      this.getRmByBranch(value);
    });
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.pageNumber = pageInfo.offset;
    this.mapDataProduct();
  }

  editOpportunity() {
    this.isLoading = true;
    this.saleManagerApi
      .checkRuleUpdate(this.divisionCodeOpp, this.objFunction?.rsId, Scopes.UPDATE, this.opportunityId, true)
      .subscribe(
        (res) => {
          this.isLoading = false;
          this.screenType = ScreenType.Update;
          this.formProduct.controls.opportunityBranch.enable({ emitEvent: false });
          this.formProduct.controls.assignTo.enable();
        },
        (e) => {
          this.isLoading = false;
          if (e.error.code) {
            this.messageService.error(e.error.description);
          }
        }
      );
  }

  addProduct() {
    const modalConfirm = this.modalService.open(ProductModalComponent, {
      windowClass: 'list__rm-modal',
    });
    modalConfirm.componentInstance.customerDivision = this.divisionCodeOpp;
    modalConfirm.componentInstance.rmLevelCode = this.rmLevelCode;
    modalConfirm.componentInstance.listSelectedOld = this.dataProduct;
    modalConfirm.result
      .then((res) => {
        if (res) {
          this.dataProduct = res.listSelected;
          if (this.dataProduct.length > 0) {
            this.is_show = true;
          } else {
            this.is_show = false;
          }
          this.mapDataProduct();
        }
      })
      .catch(() => {});
  }

  getTruncateValue(item, key, limit) {
    return Utils.isStringNotEmpty(_.get(item, key)) ? Utils.truncate(_.get(item, key), limit) : '---';
  }

  mapDataProduct() {
    const total = this.dataProduct.length;
    this.pageableProduct.totalElements = total;
    this.pageableProduct.totalPages = Math.floor(total / this.pageableProduct.size);
    this.pageableProduct.currentPage = this.paramsTableProduct.page;
    const start = this.paramsTableProduct.page * this.paramsTableProduct.size;
    this.listDataProduct = this.dataProduct?.slice(start, start + this.paramsTableProduct.size);
  }

  removeRecord(item) {
    if (!_.isEmpty(this.dataProductOld.find((i) => i.productCode === item.productCode))) {
      this.dataProductDelete.push(this.dataProductOld.find((i) => i.productCode === item.productCode));
    }
    this.dataProductDelete = _.uniqBy(this.dataProductDelete, 'id');
    _.remove(this.dataProduct, (i) => i.productCode === item.productCode);
    _.remove(this.listDataProduct, (i) => i.productCode === item.productCode);
    if (this.listDataProduct.length === 0 && this.pageableProduct.currentPage > 0) {
      this.paramsTableProduct.page -= 1;
    }
    this.listDataProduct = [...this.listDataProduct];
    if (this.dataProduct.length === 0) {
      this.is_show = false;
    }
    this.mapDataProduct();
  }
  createOpportunity() {
    let productEdit = [];
    if (
      _.isEmpty(this.formProduct.get('assignTo').value) &&
      _.isEmpty(this.listRmManager.filter((item) => item.code === this.formProduct.get('assignTo').value))
    ) {
      this.messageService.warn('Chọn RM cơ hội');
      return;
    }
    this.dataProduct.forEach((element) => {
      if (_.isEmpty(this.dataProductOld.find((i) => i.productCode === element.productCode))) {
        productEdit.push(element);
      } else {
        _.remove(this.dataProductDelete, (i) => i.productCode === element.productCode);
      }
    });

    this.isLoading = true;
    let assignToName = this.listRmManager.filter((item) => item.code === this.formProduct.get('assignTo').value)[0]
      .name;
    let productDTOList = productEdit
      .map((item) => {
        return _.omit(item, ['customerDivision', 'rmLevelCode', 'isChecked']);
      })
      .concat(this.dataProductDelete);
    const paramEdit = {
      id: this.opportunityId,
      branchCode: this.model?.systemInfo?.branchCode || '',
      rmCode: this.rmManager || '',
      opportunityBranch: this.formProduct.get('opportunityBranch').value,
      assignTo: this.formProduct.get('assignTo').value,
      productDTOList: productDTOList,
      status: this.formProduct.get('statusOpportunity').value,
      assignToName: assignToName,
      rsId: this.objFunction?.rsId,
      scope: Scopes.UPDATE,
      divisionCode: this.divisionCodeOpp,
    };
    this.saleManagerApi.updateOpportunity(paramEdit).subscribe(
      (result) => {
        this.isLoading = false;
        this.messageService.success(this.notificationMessage.success);
        this.back();
      },
      (e) => {
        if (_.isEmpty(e?.error?.description)) {
          this.messageService.error(this.notificationMessage.error);
          this.isLoading = false;
        } else {
          this.messageService.error(e?.error?.description);
          this.isLoading = false;
        }
      }
    );
  }
}
