import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import {
  CommonCategory,
  Division,
  FunctionCode,
  maxInt32,
  Scopes,
  ScreenType,
  SessionKey,
  StatusWork,
} from 'src/app/core/utils/common-constants';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import _ from 'lodash';
import * as moment from 'moment';
import { Utils } from 'src/app/core/utils/utils';
import { global } from '@angular/compiler/src/util';
import { RmBlockApi } from 'src/app/pages/rm/apis';
import { CustomerApi, CustomerDetailApi, CustomerDetailSmeApi } from 'src/app/pages/customer-360/apis/customer.api';
import { SaleManagerApi, SaleTargetApi, SaleTransferApi } from '../../api';

@Component({
  selector: 'sale-transfer-detail-component',
  templateUrl: './sale-transfer-detail.component.html',
  styleUrls: ['./sale-transfer-detail.component.scss'],
})
export class SaleTransferDetailComponent extends BaseComponent implements OnInit {
  isLoading = false;
  @ViewChild('table') table: DatatableComponent;
  @ViewChild('tableTarget') tableTarget: DatatableComponent;
  pageable: Pageable;
  listRmManager = [];

  formDetailTarget = this.fb.group({
    targetBranchName: [''],
    targetStatus: [''],
    targetCode: [''],
    targetRMCode: [''],
    product: [''],
    createdBy: [''],
    createdDate: [''],
    targetBranch: [''],
    assignTo: [''],
  });

  limit = global.userConfig.pageSize;
  model: any = {};
  screenType: any;
  opportunityCode: any;
  customerCode: any;
  statusOpportunity: any;
  targetCode: any;
  targetId: any;
  divisionCode: any;
  saleCode: any;
  saleId: any;
  constructor(
    injector: Injector,
    private saleTransferApi: SaleTransferApi,
    private saleManagerApi: SaleManagerApi,
    private rmBlockApi: RmBlockApi,
    private customerDetailSmeApi: CustomerDetailSmeApi,
    private saleTargetApi: SaleTargetApi
  ) {
    super(injector);
    this.route?.data?.subscribe((data) => {
      this.screenType = data?.type;
    });
    this.opportunityCode = this.route.snapshot.queryParams.opportunityCode;
    this.divisionCode = this.route.snapshot.queryParams.divisionCode;
    this.customerCode = this.route.snapshot.queryParams.customerCode;
    this.targetCode = this.route.snapshot.queryParams.targetCode;
    this.targetId = this.route.snapshot.queryParams.targetId;
    this.saleCode = this.route.snapshot.queryParams.saleCode;
    this.saleId = this.route.snapshot.queryParams.saleId;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.OPPORTUNITY_SALE}`);
  }

  params: any = {
    pageNumber: 0,
    pageSize: 10,
    rsId: '',
    scope: Scopes.VIEW,
  };
  is_show = true;
  showMessage = false;
  dataDivisionBranch: any = {};
  listBranchesOpp = [];
  rmLevelCode = '';
  btnCheckCustomer = true;

  pageableProduct: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };

  paramsTableProduct = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };

  lstLevelRmCode: any;
  segmentCustomer: string;
  detailTarget: any;
  detailOpportunity: any;
  targetBranch: any;
  targetRMCode: any;
  statusTarget: any;
  statusSale: any;
  apiDetailCustomer: Observable<any>;
  apigetSegmentByCustomerCode: Observable<any>;
  detailSale: any;
  listStatusOpp = [];
  formDetailSale = this.fb.group({
    saleStatus: [''],
  });

  ngOnInit() {
    if (this.divisionCode === Division.SME) {
      this.apiDetailCustomer = this.saleManagerApi
        .customerDetailOpportunity(this.customerCode)
        .pipe(catchError(() => of(undefined)));
      this.apigetSegmentByCustomerCode = this.customerDetailSmeApi
        .getSegmentByCustomerCode(this.customerCode)
        .pipe(catchError(() => of(undefined)));
    } else {
      this.apiDetailCustomer = this.saleManagerApi
        .customerDetailOpportunity(this.customerCode)
        .pipe(catchError(() => of(undefined)));
      this.apigetSegmentByCustomerCode = of([]);
    }
    this.isLoading = true;
    forkJoin([
      this.apiDetailCustomer,
      this.saleManagerApi.detailOpportunity(this.opportunityCode).pipe(catchError(() => of(undefined))),
      this.rmBlockApi
        .fetch({ page: 0, size: maxInt32, hrsCode: this.currUser?.hrsCode, isActive: true })
        .pipe(catchError(() => of(undefined))),
      this.apigetSegmentByCustomerCode,
      this.saleTargetApi.detailTargetSale(this.targetCode).pipe(catchError(() => of(undefined))),
      this.saleTransferApi.detailTransferSale(this.saleCode).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.SALE_STATUS).pipe(catchError(() => of(undefined))),
    ]).subscribe(
      ([itemCustomer, detailOpportunity, divisionOfSystem, segment, detailTarget, detailSale, listStatusOpp]) => {
        this.detailTarget = detailTarget;
        this.detailOpportunity = detailOpportunity;
        this.detailSale = detailSale;

        if (this.divisionCode === Division.SME) {
          this.segmentCustomer = segment;
        } else {
          this.segmentCustomer = itemCustomer?.customerInfo?.customerSegment;
        }

        // Status Mục tiêu
        if (+detailTarget?.targetStatus === 1) {
          this.statusTarget = 'saleTarget.statusCreated';
        } else if (+detailTarget?.targetStatus === 2) {
          this.statusTarget = 'saleTarget.statusCanceled';
        } else {
          this.statusTarget = 'saleTarget.statusTransferred';
        }
        // STATUS
        this.listStatusOpp = listStatusOpp?.content.map((item) => {
          return { code: item.code, displayName: item.name };
        });
        // Status Sale
        this.statusSale = this.listStatusOpp.find((item) => item.code === this.detailSale.saleStatus).displayName;
        this.formDetailSale.controls.saleStatus.setValue(this.detailSale.saleStatus);
        this.model = itemCustomer;
        this.isLoading = false;
      }
    );
  }

  checkEditSale() {
    this.isLoading = true;
    this.saleTransferApi
      .checkUpdateSale(this.divisionCode, this.objFunction?.rsId, Scopes.UPDATE, this.saleId)
      .subscribe(
        (res) => {
          this.isLoading = false;
          this.screenType = ScreenType.Update;
        },
        (e) => {
          this.isLoading = false;
          if (e.error.code) {
            this.messageService.error(e.error.description);
          }
        }
      );
  }

  getTruncateValue(item, key, limit) {
    return Utils.isStringNotEmpty(_.get(item, key)) ? Utils.truncate(_.get(item, key), limit) : '---';
  }

  editSale() {
    this.isLoading = true;
    const paramEdit = {
      id: this.saleId,
      saleStatus: this.formDetailSale.controls.saleStatus.value,
      rsId: this.objFunction?.rsId,
      scope: Scopes.UPDATE,
      division: this.divisionCode,
    };
    this.saleTransferApi.updateSale(paramEdit).subscribe(
      (result) => {
        this.isLoading = false;
        this.messageService.success(this.notificationMessage.success);
        this.back();
      },
      (e) => {
        if (_.isEmpty(e?.error?.description)) {
          this.messageService.error(this.notificationMessage.error);
          this.isLoading = false;
        } else {
          this.messageService.error(e?.error?.description);
          this.isLoading = false;
        }
      }
    );
  }
}
