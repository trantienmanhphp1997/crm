import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { FunctionCode, Scopes } from 'src/app/core/utils/common-constants';
import { SaleManagerApi } from '../../api';
import { Utils } from 'src/app/core/utils/utils';
import _ from 'lodash';

@Component({
  selector: 'app-selling-opp-indiv-detail-item-v2',
  templateUrl: './selling-opp-indiv-detail-item.component.html',
  styleUrls: ['./selling-opp-indiv-detail-item.component.scss'],
})
export class SellingOppIndivDetailItemV2Component extends BaseComponent implements OnInit {
  @Input('data') data: any;
  @Input('id') id: any;
  @Output() onClickEdit: EventEmitter<any> = new EventEmitter<any>();



  numberCurency: number;
  textCurency: string;


  constructor(injector: Injector, private saleManagerApi: SaleManagerApi) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.SELLING_OPPORTUNITY_INDIV}`);
  }

  ngOnInit(): void {
    console.log(this.data);
    this.checkCurency();
    this.getStatusProducts();
  }


  checkScopes(codes: Array<any>) {
    if (Utils.isArrayEmpty(codes)) return false;
    return Utils.isArrayNotEmpty(_.filter(this.objFunction.scopes, (x) => codes.includes(x)));
  }

  checkCurency() {
    if (this.data?.amount < 1000000000) {
      if (this.data?.amount < 1000000) {
        this.numberCurency = this.data?.amount;
        this.textCurency = '';
      } else {
        this.numberCurency = Math.round(this.data?.amount) / 1000000;
        this.textCurency = 'triệu';
      }
    } else {
      this.numberCurency = Math.round(this.data?.amount) / 1000000000;
      this.textCurency = 'tỷ';
    }
  }

  edit(data) {
    this.onClickEdit.emit(data);
  }

  getStatusProducts() {
    this.data?.statusProducts.forEach(item => {
     if (item?.number === this.data.status) {
       this.data.statusProductsDisplay = item.text;
     }
    })
  }
}
