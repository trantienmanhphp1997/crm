import {
  AfterViewInit,
  Component,
  Injectable,
  Injector,
  OnInit,
  ViewChild
} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CommonCategory, FunctionCode, Scopes } from 'src/app/core/utils/common-constants';
import * as _ from 'lodash';
import { MatDialog } from '@angular/material/dialog';
import { DatePipe } from '@angular/common';
import { ConstructionGeneralComponent } from '../construction-general/construction-general.component';
import { ConstructionCashflowTrackingComponent } from '../construction-cashflow-tracking/construction-cashflow-tracking.component';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { CashFlowApi } from '../../../api/cash-flow.api';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'construction-info',
  templateUrl: './construction-info.component.html',
  styleUrls: ['./construction-info.component.scss'],
  providers: [DatePipe]
})
export class ConstructionInfoComponent extends BaseComponent implements OnInit, AfterViewInit {

  @ViewChild('general') public constructionGeneral: ConstructionGeneralComponent;
  @ViewChild('tracking') public constructionCashflowTracking: ConstructionCashflowTrackingComponent;

  constructionTranslate;
  tabIndex = 0;
  item;

  constructor(
    injector: Injector,
    public dialog: MatDialog,
    private categoryService: CategoryService,
    private cashFlowApi: CashFlowApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DEBT_CLAIM}`);
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
  }

  ngOnInit(): void {
    this.item = this.prop?.item;
    this.translate.get(['debtClaims']).subscribe((result) => {
      this.constructionTranslate = _.get(result, 'debtClaims');
    });
  }

  ngAfterViewInit(): void {

  }

  onTabChanged($event) {
    this.tabIndex = _.get($event, 'index');
  }

}
