import {
  AfterViewInit,
  Component,
  Injectable,
  Injector,
  Input,
  OnInit,
  ViewChild
} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CommonCategory, FunctionCode } from 'src/app/core/utils/common-constants';
import * as _ from 'lodash';
import { MatDialog } from '@angular/material/dialog';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { CashFlowApi } from '../../../api/cash-flow.api';
import { global } from '@angular/compiler/src/util';
import * as moment from 'moment';
import { Utils } from 'src/app/core/utils/utils';
import { catchError } from 'rxjs/operators';
import { forkJoin, of } from 'rxjs';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { ConfirmUnassignlModalComponent } from '../confirm-unassign-modal/confirm-unassign-modal.component';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { CreateValuationReportModalComponent } from '../create-valuation-report-modal/create-valuation-report-modal.component';

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'construction-cashflow-tracking',
  templateUrl: './construction-cashflow-tracking.component.html',
  styleUrls: ['./construction-cashflow-tracking.component.scss']
})
export class ConstructionCashflowTrackingComponent extends BaseComponent implements OnInit, AfterViewInit {
  @ViewChild('ngxDataTable') ngxDataTable: DatatableComponent;
  @Input() item;
  @Input() data;
  @Input() filter;

  form = this.fb.group({
    accNumber: '',
    fromDate: [new Date(moment().add(-6, 'day').toDate()), CustomValidators.required],
    toDate: [new Date(moment().toDate()), CustomValidators.required],
    ftCode: '',
    status: 'ALL',
    assetSuggest: '0'
  });

  commonData = {
    maxDate: new Date(),
    minDate: moment().add(-90, 'day').toDate(),
    listStatus: [
      { code: 'ALL', name: 'Tất cả' },
      { code: 'ASSIGNED', name: 'Đã gán' }
    ],
    listAccountNumber: [],
    //LIST_BATCH
    listPaymentRound: [],
    listPaymentType: []
  }

  constructionTranslate;
  pageable: Pageable;
  limit = global?.userConfig?.pageSize;
  editable;

  allFT = [];
  listFT = [];
  rawFT = [];
  listEditingFT = [];
  countAllFT = 0;

  constructor(
    injector: Injector,
    public dialog: MatDialog,
    private cashFlowApi: CashFlowApi,
    private categoryService: CategoryService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DEBT_CLAIM}`);
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
  }

  ngOnInit(): void {
    this.translate.get(['debtClaims']).subscribe((result) => {
      this.constructionTranslate = _.get(result, 'debtClaims');
    });
    this.isLoading = true;
  }

  ngAfterViewInit(): void {
    this.form.controls.fromDate.valueChanges.subscribe(value => {
      this.checkFilterDate();
    });

    this.form.controls.toDate.valueChanges.subscribe(value => {
      this.checkFilterDate();
    });

    forkJoin([
      this.categoryService.getCommonCategory(CommonCategory.LIST_BATCH).pipe(catchError(() => of(undefined))),
      this.cashFlowApi.getAccountByCustomerCode({ customerId: this.item?.codeT24 }).pipe(catchError(() => of(undefined)))
    ]).subscribe(([listBatch, listAccount]) => {
      this.commonData.listPaymentRound = _.get(listBatch, 'content') || [];
      let accounts = [];
      if (listAccount?.status === 200) {
        accounts = _.head(listAccount?.data)?.accountInfos || [];
      }

      // Khởi tạo danh sách tài khoản
      let hasCDTAccount;
      this.commonData.listAccountNumber = accounts?.map(item => {
        if (hasCDTAccount === undefined && item?.accountNumber && item?.accountNumber === this.item?.soTaiKhoan) {
          hasCDTAccount = true;
        }
        return { code: item?.accountNumber, name: item?.accountNumber };
      });
      this.form.controls.accNumber.setValue(_.head(this.commonData.listAccountNumber)?.code);

      // Loại thanh toán
      this.commonData.listPaymentType = [
        {
          "name": "Tạm ứng",
          "value": 0
        }, {
          "name": "Thanh toán",
          "value": 1
        }
      ];


      // Chỉ xem đối với các user không phải là người tạo thôn gtin công trình
      this.editable = this.item?.nguoiKhoiTao === this.currUser.username;

      this.search();
    });
  }

  // LOAD DATA
  search() {
    if (this.form.invalid || !this.checkFilterDate()) {
      return;
    }
    this.isLoading = true;
    let fromDate = moment(this.form.controls.fromDate.value).format('DD/MM/YYYY');
    let toDate = moment(this.form.controls.toDate.value).format('DD/MM/YYYY');
    let ftCdoe = this.form.controls.ftCode.value || '';
    let accNum = this.form.controls.accNumber.value || '';
    let assetSuggest = this.form.controls.assetSuggest.value || '';
    let fromDateTimeStamp = moment(this.form.controls.fromDate.value).startOf('day').valueOf();
    let toDateTimeStamp = moment(this.form.controls.toDate.value).endOf('day').valueOf();
    let forkJoinReq = [];

    if (this.isStatusAll) {
      let params: any = {
        accountNo: accNum,
        startTime: fromDateTimeStamp,
        endTime: toDateTimeStamp,
        ft: ftCdoe
      }

      forkJoinReq.push(this.cashFlowApi.countAllFT(params).pipe(catchError(() => of(undefined))));

      forkJoin(forkJoinReq).subscribe(([countResp]) => {
        this.filter = {

        }
        if (countResp && Number(countResp) > 2000) {
          // Nếu số bản ghi > 2000 thì cảnh báo để user chọn lại khoảng thời gian
          this.messageService.warn(this.constructionTranslate.message.ftMoreThan2000);
          return;
        }
        this.countAllFT = Number(countResp);

        params.collateralCode = this.item?.maTaiSan || '';
        params.listAcc = this.commonData.listAccountNumber.map(item => item.code).join();
        this.cashFlowApi.getAllFT(params).subscribe(
          (ftResp) => {
            if (ftResp) {

              this.allFT = ftResp.map(item => {
                // item.trxDt = this.formatDate("Mon Jan 02 23:59:00 ICT 2023");
                item.id = "id" + Math.random().toString(16).slice(2);
                item.trxDt = this.formatDate(item.trxDt);
                item.checked = !(Utils.isEmpty(item?.status) || Utils.isNull(item?.status));
                item.showDuplicateBtn = item.checked === true;
                return item;
              });


              // Gợi ý tài sản
              if (assetSuggest == '1') {
                this.allFT = this.allFT.filter(item => item?.dscp.includes(this.item?.soHsplHdmb));
              }

              // Lọc theo số FT
              if (ftCdoe) {
                this.allFT = this.allFT.filter(item => item.refNo.toLowerCase().includes(ftCdoe.toLowerCase()));
              }

              // Hiển thị bản ghi theo Ngày tiền về giảm dần, Nếu Ngày tiền về = nhau thì sắp xếp số FT theo alphabet
              let allFtTemp = [];
              this.allFT.sort((obj1, obj2) => {
                var trxDt1 = moment(obj1.trxDt, 'DD/MM/YYYY').startOf('day');
                var trxDt2 = moment(obj2.trxDt, 'DD/MM/YYYY').startOf('day');
                var refNo1 = obj1.refNo;
                var refNo2 = obj2.refNo;
                if (trxDt1.format('DD/MM/YYYY') == trxDt2.format('DD/MM/YYYY')) {
                  return (refNo1 < refNo2) ? -1 : (refNo1 > refNo2) ? 1 : 0;
                } else {
                  return trxDt1.isBefore(trxDt2) ? 1 : -1
                }
              }).map((item => {
                let listCashFlow = item?.listCashFlow || [];
                if (listCashFlow.length > 0) {
                  listCashFlow.forEach(cashFlow => {
                    item.id = "id" + Math.random().toString(16).slice(2);
                    item.checked = 'checked';
                    item.amountCollateral = cashFlow?.amountCollateral;
                    item.period = cashFlow?.period;
                    item.paymentType = cashFlow?.paymentType;
                    item.amtRemaining = '';
                    item.showDuplicateBtn = true;
                    allFtTemp.push(_.cloneDeep(item));
                  });
                } else {
                  allFtTemp.push(item);
                }
              }));
              this.allFT = allFtTemp;

              let totalPages = this.allFT.length / this.limit;
              if (!(this.allFT.length % this.limit)) {
                totalPages++;
              }
              this.pageable = {
                totalElements: this.allFT.length,
                totalPages,
                currentPage: 0,
                size: this.limit
              };


              this.listFT = this.allFT.slice(0, this.limit);
              this.rawFT = _.cloneDeep(this.listFT);
              this.isLoading = false;
            } else {
              this.messageService.error(this.notificationMessage.E001);
            }
          }, (err) => {
            this.isLoading = false;
            if (err?.status === 0) {
              this.messageService.error(this.notificationMessage.E001);
              return;
            }
            this.messageService.error(err?.error?.description);
          });
      });
    } else {
      let assignedReq = {
        "collateralCode": this.item?.maTaiSan || '',
        "accountNo": accNum,
        "ft": ftCdoe,
        "fromDate": fromDate,
        "toDate": toDate,
        "status": '',
        "page": this.pageable ? this.pageable.offset : 0,
        "size": this.limit
      }
      forkJoinReq.push(this.cashFlowApi.getAssignedFT(assignedReq).pipe(catchError(() => of(undefined))));

      forkJoin(forkJoinReq).subscribe(([assigned]) => {
        this.allFT = _.get(assigned, 'content') || [];
        this.pageable = {
          totalElements: _.get(assigned, 'totalElements', 0),
          totalPages: _.get(assigned, 'totalPages', 0),
          currentPage: 0,
          size: this.limit
        };
        this.listFT = this.allFT.map(item => {
          return {
            "id": "id" + Math.random().toString(16).slice(2),
            "accountType": "",
            "acctNo": "",
            "additionalDscp": "",
            "availBal": 0,
            "bcBankSortCode": "",
            "beneficiary": "",
            "beneficiaryAcct": "",
            "cateGory": "",
            "ccyCd": "",
            "cif": "",
            "creditAmt": item?.amount,
            "dscp": item?.content,
            "kbCitadCode": "",
            "oriTerminalId": "",
            "refNo": item?.ft,
            "stmtId": "",
            "transactionType": "",
            "creditAccountNo": "",
            "trxDtMilS": "",
            "status": "check",
            "checked": true,
            "amountCollateral": item?.amountCollateral,
            "period": item?.period,
            'trxDt': item?.transactionDate ? moment(new Date(item?.transactionDate)).format('DD/MM/YYYY') : '',
            "paymentType": item?.paymentType,
            "showDuplicateBtn": true,
            'amtRemaining': ''
          }
        });
        this.rawFT = _.cloneDeep(this.listFT);
        this.isLoading = false;
      });
    }
  }

  completed() {
    this.isLoading = true;
    let body = [this.item?.maTaiSan];
    this.cashFlowApi.createValuationReport(body).subscribe(
      (res) => {
        this.isLoading = false;
        this.openCompletedDialog(res);
      },
      (err) => {
        this.isLoading = false;
        this.messageService.error(err?.error?.description);
      }
    );
  }

  openCompletedDialog(res: any): void {
    const dialogRef = this.dialog.open(CreateValuationReportModalComponent);
    dialogRef.componentInstance.message = "Hệ thống đã khởi tạo Báo cáo định giá lại số " + res.maChungThu + " để giảm giá trị cho QĐN này. Vui lòng truy cập CMV để kiểm tra và gửi duyệt!";
    dialogRef.afterClosed().subscribe((isTrue) => {
      if (isTrue) {
        window.open(res.bcdgViewUrl);
      }
    });
  }

  // ACTION
  duplicateFT(row) {
    this.isLoading = true;
    let parrams = {
      ft: row?.refNo
    };
    this.cashFlowApi.getAmountRemaining(parrams).subscribe(
      (res) => {
        // row.showDuplicateBtn = false;
        let newRow = _.cloneDeep(row);
        if (res && !isNaN(res)) {
          newRow.amtRemaining = (row?.creditAmt - Number(res)).toFixed(2);
        } else {
          newRow.amtRemaining = row?.creditAmt;
        }
        newRow.editing = true;
        newRow.checked = true;
        newRow.amountCollateral = null;
        newRow.period = null;
        newRow.paymentType = null;
        newRow.amountCollateralErr = '';
        newRow.periodErr = false;
        newRow.periodErrMessage = '';
        newRow.paymentTypeErr = false;
        newRow.paymentTypeErrMessage = '';
        newRow.duplicatedId = "dupl" + Math.random().toString(16).slice(2);
        newRow.id = "id" + Math.random().toString(16).slice(2);
        const lastIndex = this.lastIndexOf(this.listFT, row?.refNo) + 1;
        let fts = this.listFT;
        fts.splice(lastIndex, 0, newRow);
        this.pageable.totalElements++;
        this.listFT = _.cloneDeep(fts);
        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
        if (err?.status === 0) {
          this.messageService.error(this.notificationMessage.E001);
          return;
        }
        this.messageService.error(err?.error?.description);
      }
    );
  }

  removeDuplicatedFT(row) {
    this.listFT = this.listFT.filter(itm => itm?.id !== row?.id);
    this.allFT = this.allFT.filter(itm => itm?.id !== row?.id);
    this.rawFT = this.rawFT.filter(itm => itm?.id !== row?.id);
    this.pageable.totalElements--;
    console.log(this.listFT);
  }

  lastIndexOf(array, refNo) {
    for (let i = array.length - 1; i >= 0; i--) {
      if (array[i].refNo === refNo)
        return i;
    }
    return -1;
  };
  editFT(row) {
    if (!row.duplicatedId) {
      this.getAmountRemaining(row);
    }
    this.listEditingFT.push(row?.id);
    row.editing = true;
  }

  cancelEditFT(row, checked) {
    row.editing = false;
    if (checked === false && !row?.status) {
      row.amountCollateral = null;
      row.period = null;
      row.checked = false;
      this.resetRow(row);
    }

    if (checked === false && row?.status) {
      if (row?.duplicatedId) {
        // dòng ft thêm mới
        this.removeDuplicatedFT(row);
      } else {
        // các dòng đã gán
        // hoặc dòng ft duy nhất trong d/s
        let obj = this.rawFT.find(item => item.id === row.id);
        row.amountCollateralErr = '';
        row.periodErr = false;
        row.periodErrMessage = '';
        row.paymentTypeErr = false;
        row.paymentTypeErrMessage = '';
        row.amountCollateral = obj?.amountCollateral;
        row.period = obj?.period;
        row.paymentType = obj?.paymentType;
      }
    }
    row.amtRemaining = '';

    this.listEditingFT = this.listEditingFT.filter(item => item?.id !== row?.id);
  }

  openConfirmDialog(row): void {
    const dialogRef = this.dialog.open(ConfirmUnassignlModalComponent);
    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        this.save(row, false);
      }
    });
  }

  save(row, isAssign) {
    let checkAmountCollateral = this.checkAmountCollateral(row.amountCollateral, row);
    let checkPaymentRound = this.checkPaymentRound(row);
    let checkPaymentType = this.checkPaymentType(row);
    let checkDupl = this.checkDuplicateInfo(row);

    if (isAssign && (checkAmountCollateral || checkPaymentRound || checkPaymentType || checkDupl)) {
      return;
    }
    this.isLoading = true;
    let body = {
      collateralCode: this.item?.maTaiSan, // Mã công trình
      productName: this.item?.tenCongTrinh, // Tên công trình
      ft: row?.refNo,
      transactionDate: moment(row?.trxDt, "DD/MM/YYYY").format('YYYY-MM-DD'), // row?.trxDt
      amount: row?.creditAmt,
      content: row?.dscp,
      amountCollateral: row?.amountCollateral,
      period: row?.period,
      type: 1,
      isAssign: isAssign, // true = gán mới | false = nhả gán
      accountNo: this.form.controls.accNumber.value,
      paymentType: row?.paymentType
    };
    this.cashFlowApi.updateAssignFT(body).subscribe(
      (res) => {
        this.messageService.success(this.notificationMessage.success);
        if (isAssign) {
          row.status = 'checked';
          row.showDuplicateBtn = true;
          delete row.duplicatedId;

          let lastIndex = this.lastIndexOf(this.rawFT, row?.refNo) + 1;
          let fts = this.rawFT;
          fts.splice(lastIndex, 0, row);
          this.rawFT = _.cloneDeep(fts);

          lastIndex = this.lastIndexOf(this.allFT, row?.refNo) + 1;
          fts = this.allFT;
          fts.splice(lastIndex, 0, row);
          this.allFT = _.cloneDeep(fts);

          this.cancelEditFT(row, true);
        } else {
          row.status = '';
          row.checked = false;
          row.showDuplicateBtn = false;
          this.resetRow(row);
        }
        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
        if (err?.status === 0) {
          this.messageService.error(this.notificationMessage.E001);
          return;
        }
        this.messageService.error(err?.error?.description);
      }
    );
  }

  getAmountRemaining(row) {
    this.isLoading = true;
    let parrams = {
      ft: row?.refNo
    };
    this.cashFlowApi.getAmountRemaining(parrams).subscribe(
      (res) => {
        if (res && !isNaN(res)) {
          row.amtRemaining = (row?.creditAmt - Number(res)).toFixed(2);
        } else {
          row.amtRemaining = row?.creditAmt;
        }
        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
        if (err?.status === 0) {
          this.messageService.error(this.notificationMessage.E001);
          return;
        }
        this.messageService.error(err?.error?.description);
      }
    );
  }

  // VALIDATE

  checkValue(row, checked, event) {
    console.log('checked: ', checked);
    if (checked) {
      if (!row?.duplicatedId) {
        this.getAmountRemaining(row);
      }
      row.amountCollateral = !row?.status ? row?.creditAmt : row.amountCollateral;
      row.checked = true;
      this.editFT(row);
    } else {
      if (!row?.status) {
        row.checked = false;
        this.resetRow(row);
        this.cancelEditFT(row, null);
      } else {
        if(row?.duplicatedId){
          row.checked = false;
          this.removeDuplicatedFT(row);
          return;
        }
        // Chỉ cho phép xóa gán trong vòng 3 tháng kế từ ngày tiền về
        // Hệ thống check: Today - Ngày tiền về <= 90 ngày 
        let allowUnasign = row.trxDt ? moment(row.trxDt, 'DD/MM/YYYY').add(90, 'days').isAfter(moment()) : false;
        if (!allowUnasign) {
          event.preventDefault();
          row.checked = true;
          this.messageService.warn(this.constructionTranslate.message.notAllowUnassignFT);
          return;
        }
        // Confirm có đồng ý bỏ gán dòng tiền với công trình không
        this.openConfirmDialog(row);
      }
    }
  }

  checkFilterDate() {
    let fromDate = this.form.controls.fromDate.value;
    let toDate = this.form.controls.toDate.value;
    if (!fromDate || !toDate) {
      return false;
    }
    fromDate = moment(this.form.controls.fromDate.value);
    toDate = moment(this.form.controls.toDate.value);

    // Từ ngày không được lón hơn đến ngày
    if (fromDate.isAfter(toDate)) {
      this.messageService.warn(this.constructionTranslate.message.startDateMoreThanEndDate);
      return false;
    }

    // Không được nhập quá 90 ngày
    if (fromDate.add(90, 'days').startOf('day').isBefore(toDate)) {
      this.messageService.warn(this.constructionTranslate.message.max90days);
      return false;
    }

    return true;
  }

  checkAmountCollateral(value, row) {
    if (Utils.isNull(value)) {
      return true;
    }
    value = Number(value.toString());
    let errMessage;
    if (value <= 0) {
      // Giá trị tiền về phải lớn hơn 0
      errMessage = this.constructionTranslate.message.amountCollateralGeaterThanZero;
    } else if (value > row.creditAmt) {
      // Giá trị tiền về của công trình phải nhỏ hơn hoặc bằng giá trị tiền về
      errMessage = this.constructionTranslate.message.amountCollateralGeaterThanAmount;
    }
    row.amountCollateralErr = errMessage;
    return errMessage;
  }

  checkPaymentRound(row) {
    if (row.period) {
      let dupl = this.checkDuplicateInfo(row);
      if (dupl) {
        row.periodErr = true;
        row.periodErrMessage = 'Vui lòng chọn lại';
      } else {
        row.periodErr = false;
        row.periodErrMessage = '';
        if (!Utils.isNull(row.paymentType)) {
          row.paymentTypeErr = false;
          row.paymentTypeErrMessage = '';
        }
      }
    } else {
      row.periodErr = true;
      row.periodErrMessage = 'Vui lòng chọn đợt thanh toán';
    }
    return row.periodErr;
  }

  checkPaymentType(row) {
    if (!Utils.isNull(row.paymentType)) {
      let dupl = this.checkDuplicateInfo(row);
      if (dupl) {
        row.paymentTypeErr = true;
        row.paymentTypeErrMessage = 'Vui lòng chọn lại';
      } else {
        row.paymentTypeErr = false;
        row.paymentTypeErrMessage = '';
        if(row.period){
          row.periodErr = false;
          row.periodErrMessage = '';
        }
      }
    } else {
      row.paymentTypeErr = true;
      row.paymentTypeErrMessage = 'Vui lòng chọn loại thanh toán';
    }
    return row.paymentTypeErr;
  }

  checkDuplicateInfo(row) {
    let dupl = this.listFT.filter((item) => {
      return item.paymentType === row.paymentType && item.period === row.period && item.refNo === row.refNo;
    });
    if (dupl.length > 1) {
      this.messageService.warn('Đã có dòng tiền của loại và đợt thanh toàn này');
    }
    return dupl.length > 1;
  }

  // UTILS
  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    if (this.isStatusAll) {
      let start = pageInfo.offset * this.limit;
      let end = start + this.limit;

      this.listFT = this.allFT.slice(start, end);
      this.rawFT = _.cloneDeep(this.listFT);
    } else {
      this.pageable.offset = pageInfo.offset;
      this.search();
    }
  }

  onAction(event) {
  }

  resetRow(row) {
    row.amountCollateralErr = '';
    row.amountCollateral = null;
    row.period = null;
    row.periodErr = false;
    row.periodErrMessage = '';
    row.paymentType = null;
    row.paymentTypeErr = false;
    row.paymentTypeErrMessage = '';
    row.amtRemaining = '';
  }

  formatDate(dateStr: string) {
    if (dateStr) {
      let dateSpl = dateStr.split(' ');
      let day = dateSpl[2];
      let year = dateSpl[dateSpl.length - 1];
      let month = moment().month(dateSpl[1]).format("M");
      month = month.length === 1 ? '0' + month : month;
      dateStr = [day, month, year].join("/");
    }
    return dateStr;
  }

  showRemoveDuplicateBtn(row) {
    return row.duplicatedId && !row?.status;
  }

  get isStatusAll() {
    return this.form.controls.status.value === 'ALL';
  }

}
