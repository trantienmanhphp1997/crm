import { Component, HostBinding, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { functionUri } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'app-potential-modal',
  templateUrl: './confirm-unassign-modal.component.html',
  styleUrls: ['./confirm-unassign-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ConfirmUnassignlModalComponent implements OnInit {
  @HostBinding('confirm-unassign-modal') classCreateCalendar = true;
  constructor(
    private dialogRef: MatDialogRef<ConfirmUnassignlModalComponent>
  ) { }

  ngOnInit(): void {
  }

  choose(choose: boolean): void {
    this.dialogRef.close(choose);
  }
}
