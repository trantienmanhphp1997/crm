import {
  Component,
  Injectable,
  Injector,
  OnInit
} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { FunctionCode } from 'src/app/core/utils/common-constants';
import * as _ from 'lodash';
import { MatDialog } from '@angular/material/dialog';
import { DatePipe } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { CashFlowApi } from '../../../api/cash-flow.api';

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'create-construction',
  templateUrl: './create-construction.component.html',
  styleUrls: ['./create-construction.component.scss'],
  providers: [DatePipe]
})
export class CreateConstructionComponent extends BaseComponent implements OnInit {

  embededLink;

  constructor(
    injector: Injector,
    private sanitizer: DomSanitizer,
    public dialog: MatDialog,
    private cashFlowApi: CashFlowApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DEBT_CLAIM}`);
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.cashFlowApi.getConstructionGeneralInfo({ maTaiSan: '' }).subscribe(
      (res) => {
        if (res) {
          this.embededLink = this.sanitizer.bypassSecurityTrustResourceUrl(res);
        } else {
          this.messageService.error(this.notificationMessage.E001);
        }
        this.isLoading = false;
      }, (err) => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.E001);
      },
    )
  }

}
