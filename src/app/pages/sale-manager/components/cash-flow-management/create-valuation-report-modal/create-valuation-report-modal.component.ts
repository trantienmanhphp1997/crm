import { Component, HostBinding, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { functionUri } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'app-create-valuation-report-modal',
  templateUrl: './create-valuation-report-modal.component.html',
  styleUrls: ['./create-valuation-report-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CreateValuationReportModalComponent implements OnInit {
  @HostBinding('create-valuation-report-modal') classCreateCalendar = true;
  constructor(
    private dialogRef: MatDialogRef<CreateValuationReportModalComponent>
  ) { }

  message: String;

  ngOnInit(): void {
  }

  choose(choose: boolean): void {
    this.dialogRef.close(choose);
  }

}
