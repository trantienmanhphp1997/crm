import {
  Component,
  EventEmitter,
  Injectable,
  Injector,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { FunctionCode } from 'src/app/core/utils/common-constants';
import * as _ from 'lodash';
import { MatDialog } from '@angular/material/dialog';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { CashFlowApi } from '../../../api/cash-flow.api';

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'construction-general',
  templateUrl: './construction-general.component.html',
  styleUrls: ['./construction-general.component.scss']
})
export class ConstructionGeneralComponent extends BaseComponent implements OnInit {

  @Input() item: any;

  constructionTranslate;
  embededLink: SafeUrl;

  constructor(
    injector: Injector,
    public dialog: MatDialog,
    private cashFlowApi: CashFlowApi,
    private sanitizer: DomSanitizer
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DEBT_CLAIM}`);
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
  }

  ngOnInit(): void {
    this.translate.get(['debtClaims']).subscribe((result) => {
      this.constructionTranslate = _.get(result, 'debtClaims');
    });
    this.loadData();
  }

  loadData() {
    this.isLoading = true;
    forkJoin([
      this.cashFlowApi.getConstructionGeneralInfo({ maTaiSan: this.item?.maTaiSan }).pipe(catchError((e) => of(undefined))),
    ]).subscribe(([generalInfo]) => {
      if (generalInfo) {
        this.embededLink = this.sanitizer.bypassSecurityTrustResourceUrl(generalInfo);
      } else {
        this.messageService.error(this.notificationMessage.E001);
      }
      this.isLoading = false;
    });
  }

}
