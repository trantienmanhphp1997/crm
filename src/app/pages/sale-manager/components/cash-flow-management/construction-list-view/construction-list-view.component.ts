import { forkJoin, of } from 'rxjs';
import { global } from '@angular/compiler/src/util';
import { AfterViewInit, Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';
import { cleanDataForm } from 'src/app/core/utils/function';
import {
  FunctionCode,
  Scopes,
  maxInt32,
  SessionKey,
} from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import * as moment from 'moment';
import { MatDialog } from '@angular/material/dialog';
import { CashFlowApi } from '../../../api/cash-flow.api';
import { CategoryService } from '../../../../system/services/category.service';
import { AppFunction } from 'src/app/core/interfaces/app-function.interface';
import { RmApi } from 'src/app/pages/rm/apis';
import { Utils } from 'src/app/core/utils/utils';

@Component({
  selector: 'app-manager-construction-list-view',
  templateUrl: './construction-list-view.component.html',
  styleUrls: ['./construction-list-view.component.scss']
})
export class ConstructionListViewComponent extends BaseComponent implements OnInit, AfterViewInit {
  isLoading = false;
  listData = [];
  isInit = true;
  listRmManager = [];
  listBranchesManager = [];
  customerType: any;
  objFunctionRM: AppFunction;
  searchForm = this.fb.group({
    code: '',
    name: '',
    startDate: null,
    endDate: null,
    customerCode: '',
    customerName: '',
    taxCode: '',
    type: null,
    branchCode: [''],
    rmCode: ['']
  });
  paramSearch = {
    pageSize: global.userConfig.pageSize,
    page: 0,
    rsId: '',
    scope: Scopes.VIEW
  };
  prevParams: any;
  innerWidth: any;
  pageable: Pageable;
  prop: any;
  maxDate = new Date();
  maxStartDate: Date;
  isRm = false;
  isPermission: boolean;
  listConstruction: [];
  listTypeConstruction = [
    { code: '1', name: 'Thi công' },
    { code: '2', name: 'Lắp đặt' },
    { code: '3', name: 'Thương mại' }
  ];
  listTypeBid = [
    { code: '1', name: 'Nhà thầu chính' },
    { code: '2', name: 'Nhà thầu phụ' },
    { code: '3', name: 'Nhà thầu liên danh' },
    { code: '4', name: 'Nhà thầu khác' }
  ];

  constructor(
    injector: Injector,
    // private modalActive: NgbActiveModal,
    private cashFlowApi: CashFlowApi,
    private rmApi: RmApi,
    public dialog: MatDialog,
    private categoryService: CategoryService
  ) {
    super(injector);
    this.isLoading = true;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DEBT_CLAIM}`);
    this.objFunctionRM = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
  }

  ngOnInit(): void {
    this.innerWidth = window.innerWidth;
    this.initData();
    this.maxStartDate = this.maxDate
  }

  ngAfterViewInit() {
    this.searchForm.controls.branchCode.valueChanges.subscribe((value) => {
      if (!this.isLoading) {
        this.searchForm.controls.rmCode.setValue('');
        this.getRmManager();
      }
    });
    this.searchForm.controls.startDate.valueChanges.subscribe((value) => {
      if (moment(value).isValid()) {
        const endDate = this.searchForm.controls.endDate.value;
        if (moment(endDate).isValid() && moment(value).isAfter(moment(endDate))) {
          this.searchForm.controls.endDate.setValue(value);
        }
      }
    });

    this.searchForm.controls.endDate.valueChanges.subscribe((value) => {
      if (moment(value).isValid()) {
        const startDate = this.searchForm.controls.startDate.value;
        if (moment(startDate).isValid() && moment(startDate).isAfter(moment(value))) {
          this.searchForm.controls.startDate.setValue(value);
        }
        this.maxStartDate = value
      } else {
        this.maxStartDate = null;
      }
    });
  }

  formatDate(date: string) {
    return moment(Number(date)).format('DD/MM/YYYY')
  }

  getTypeConstruction(value) {
    return (
      this.listTypeConstruction?.find((item) => {
        return item.code === value;
      })?.name || ''
    );
  }

  getTypeBid(value) {
    return (
      this.listTypeBid?.find((item) => {
        return item.code === value;
      })?.name || ''
    );
  }

  initData() {
    this.paramSearch.rsId = this.objFunction?.rsId;
    this.categoryService.getBranchesOfUser(this.objFunctionRM?.rsId, Scopes.VIEW)
      .pipe(catchError(() => of(undefined)))
      .subscribe(listBranch => {
        this.isRm = _.isEmpty(listBranch);
        this.isPermission = _.size(listBranch) > 0;
        this.listBranchesManager = listBranch.map((item) => {
          return { code: item.code, name: item.code + ' - ' + item.name };
        });
        if (listBranch.length > 1) {
          this.listBranchesManager?.unshift({ code: '', name: this.fields.all });
        }
        if (!_.find(this.listBranchesManager, (item) => item.code === this.currUser?.branch)) {
          this.listBranchesManager.push({
            code: this.currUser?.branch,
            name: `${this.currUser?.branch} - ${this.currUser?.branchName}`
          });
        }
        this.searchForm.controls.branchCode.setValue(_.get(this.listBranchesManager, '[0].code', null));
        this.getRmManager();
      });
  }

  getRmManager() {
    const data = this.searchForm.getRawValue();
    this.listRmManager = []
    if (this.isPermission) {
      // tslint:disable-next-line:max-line-length
      const branchCodes = !data.branchCode ? this.listBranchesManager.filter(branch => branch.code).map(branch => branch.code) : [data.branchCode]
      this.isLoading = true;
      this.rmApi
        .post('findAll', {
          page: 0,
          size: maxInt32,
          crmIsActive: true,
          branchCodes,
          rmBlock: 'SME',
          rsId: this.objFunctionRM?.rsId,
          scope: Scopes.VIEW,
          isAssignRm: false
        })
        .subscribe((res) => {
          const listRm: any[] =
            res?.content?.map((item) => {
              return {
                code: item?.hrisEmployee?.employeeId || '',
                rmCode: item?.t24Employee?.employeeCode,
                username: item?.hrisEmployee?.userName || '',
                name:
                  Utils.trimNullToEmpty(item?.t24Employee?.employeeCode) +
                  ' - ' +
                  Utils.trimNullToEmpty(item?.hrisEmployee?.fullName)
              };
            }) || [];
          if (listRm.length > 1) {
            this.listRmManager = [
              ...[
                { code: '', name: this.fields.all, rmCode: '', username: '' },
                // { code: 'UN_ASSIGN', name: this.fields.unAssign, rmCode: 'UN_ASSIGN' }
              ],
              ...listRm
            ];
          } else {
            this.listRmManager = listRm;
          }
          // if (!this.listRmManager?.find((item) => item.code === this.currUser?.hrsCode)) {
          //   this.listRmManager.push({
          //     code: this.currUser?.hrsCode,
          //     rmCode: this.currUser?.hrsCode,
          //     username: this.currUser?.username,
          //     name:
          //       Utils.trimNullToEmpty(this.currUser?.code) + ' - ' + Utils.trimNullToEmpty(this.currUser?.fullName)
          //   });
          // }
          this.searchForm.controls.rmCode.setValue(_.get(this.listRmManager, '[0].code', null));
          this.isLoading = false;
          // check call api when first load
          if (this.isInit) {
            this.search(true)
            this.isInit = false
          }
        });
    } else {
      if (_.size(this.listRmManager) === 0) {
        this.listRmManager.push({
          code: '',
          hrsCode: '',
          username: '',
          name: 'Tất cả'
        });
        this.listRmManager.push({
          code: this.currUser?.code,
          hrsCode: this.currUser?.hrsCode,
          username: this.currUser?.username,
          name: `${this.currUser?.code} - ${this.currUser?.fullName}`
        });
      }
      this.searchForm.get('rmCode').setValue(_.head(this.listRmManager)?.code, { emitEvent: false });
      // check call api when first load
      if (this.isInit) {
        this.search(true)
        this.isInit = false
      }
    }
  }


  search(isSearch?: boolean) {
    this.isLoading = true;
    let params: any = {};
    const formData = cleanDataForm(this.searchForm);
    if (isSearch) {
      this.paramSearch.page = 0;
      if (moment(formData.startDate).isValid()) {
        params.ngayKhoiTaoFrom = moment(formData.startDate).toDate().getTime();
      } else {
        delete params.ngayKhoiTaoFrom;
      }
      if (moment(formData.endDate).isValid()) {
        params.ngayKhoiTaoTo = moment(formData.endDate).endOf('day').toDate().getTime();
      } else {
        delete params.ngayKhoiTaoTo;
      }
      // this.searchForm.patchValue(params);
      const userRM = this.listRmManager.find((item) => item.code === formData.rmCode)
      params.maTaiSan = formData.code;
      params.tenCongTrinh = formData.name;
      params.tenKhachHang = formData.customerName;
      params.codeT24 = formData.customerCode;
      params.mstDkkd = formData.taxCode;
      params.userKhoiTao = userRM?.username;
      params.branchCode = formData.branchCode;
    } else {
      if (!this.prevParams) {
        return;
      }
      params = this.prevParams;
    }
    Object.keys(this.paramSearch).forEach((key) => {
      params[key] = this.paramSearch[key];
    });

    this.cashFlowApi.getListConstruction(params).subscribe(
      (result) => {
        if (result) {
          this.prevParams = params;
          // this.prop.prevParams = params;
          this.listConstruction = result?.content || [];
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: global.userConfig.pageSize
          };
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );

  }

  create() {
    this.router.navigate([this.router.url, 'create']);
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.page = pageInfo.offset;
    this.search(false);
  }

  onAction(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      this.router.navigate([this.router.url, 'info'], {
        state: { item }
      });
    }
  }
}
