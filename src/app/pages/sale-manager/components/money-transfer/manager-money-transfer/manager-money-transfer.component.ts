import { ChangeDetectorRef, Component, Injector, OnInit } from '@angular/core';
import { global } from '@angular/compiler/src/util';
import {
  FunctionCode,
  maxInt32,
  Scopes
} from '../../../../../core/utils/common-constants';
import { Pageable } from '../../../../../core/interfaces/pageable.interface';
import { BaseComponent } from '../../../../../core/components/base.component';
import * as _ from 'lodash';
import { catchError, finalize } from 'rxjs/operators';
import { forkJoin, of } from 'rxjs';
import { Utils } from '../../../../../core/utils/utils';
import { RmApi } from '../../../../rm/apis';
import { CategoryService } from '../../../../system/services/category.service';
import { PopupMoneyTransferComponent } from '../popup-money-transfer/popup-money-transfer.component';
import { MatDialog } from '@angular/material/dialog';
import { cleanDataForm } from '../../../../../core/utils/function';
import { CustomerApi } from '../../../../customer-360/apis';


@Component({
  selector: 'app-manager-money-transfer',
  templateUrl: './manager-money-transfer.component.html',
  styleUrls: ['./manager-money-transfer.component.scss']
})
export class ManagerMoneyTransferComponent extends BaseComponent implements OnInit{

  isLoading = false;
  paramSearch = {
    size: global.userConfig.pageSize,
    page: 0,
    rsId: '',
    scope: Scopes.VIEW
  };
  prevParams: any;
  pageable: Pageable;
  prop: any;
  formSearch = this.fb.group({
    customerCode: '',
    type: '',
    rmCodes: [],
    branchCodes: []
  });

  listProductMoneyTransfer = [
    {
      'code': 0,
      'name': 'Block TTR'
    },
    {
      'code': 1,
      'name': 'Combo TTQT'
    }
  ];
  commonData = {
    listRmManagement: [],
    listBranch: []
  };
  scopes: Array<any>;

  listMoneyTransfer = []

  constructor(
    injector: Injector,
    private rmApi: RmApi,
    private categoryService: CategoryService,
    public dialog: MatDialog,
    public dtc: ChangeDetectorRef,
    private customerApi: CustomerApi,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.SALES_MONEY_TRANSFER}`);
    this.scopes = _.get(this.objFunction, 'scopes');
    this.paramSearch.rsId = this.objFunction?.rsId;
  }

  ngOnInit(): void {
    this.initData();
  }

  initData() {
    this.isLoading = true;
    this.getRmManager();
    forkJoin([
      /*Lấy danh sách chi nhánh quản lý*/
      this.categoryService.getBranchesOfUser(this.paramSearch.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined)))])
      .subscribe(([branchOfUser]) => {
        this.commonData.listBranch = branchOfUser?.map((item) => {
          return {
            code: item.code, name: `${item.code} - ${item.name}`
          };
        }) || [];
        if (!_.find(this.commonData.listBranch, (item) => item.code === this.currUser?.branch)) {
          this.commonData.listBranch.push({
            code: this.currUser?.branch, name: `${this.currUser?.branch} - ${this.currUser?.branchName}`
          });
        }
        this.sessionService.setSessionData(FunctionCode.SALES_MONEY_TRANSFER, this.commonData);
      }, () => {
        this.isLoading = false;
      });
    this.search(true);
    this.pageable = {
      totalElements: 1,
      totalPages: 1,
      currentPage: 1,
      size: 1
    };
    this.isLoading = false;
  }

  /*Lấy danh sách combo TTQT và block TTR*/
  search(isSearch: boolean) {
    let param = {};
    cleanDataForm(this.formSearch);
    param = { ...this.paramSearch, ...this.formSearch.getRawValue() };
    this.isLoading = true;
    this.customerApi.getMoneyTransfer(param).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe( value => {
      if (value.content) {
        this.listMoneyTransfer = value.content;
        this.pageable = {
          totalElements: value.totalElements,
          totalPages: value.totalPages,
          currentPage: value.pageable.pageNumber,
          size: value.pageable.pageSize,
        };
      }
    }, error => {
      this.messageService.error(this.notificationMessage.error);
    })
  }

  /*Lấy danh sách RM quản lý*/
  getRmManager() {
    this.rmApi
      .post('findAll', {
        page: 0, size: maxInt32, crmIsActive: true, branchCodes: [], rsId: this.objFunction?.rsId, scope: Scopes.VIEW
      })
      .pipe(catchError(() => of(undefined)))
      .subscribe((res) => {
        const listRm = [];
        res?.content?.forEach((item) => {
          if (!_.isEmpty(item?.t24Employee?.employeeCode)) {
            listRm.push({
              code: _.trim(item?.t24Employee?.employeeCode),
              displayName: _.trim(item?.t24Employee?.employeeCode) + ' - ' + _.trim(item?.hrisEmployee?.fullName),
              branchCode: _.trim(item?.t24Employee?.branchLevel2)
            });
          }
        });
        if (!listRm?.find((item) => item.code === this.currUser?.code)) {
          listRm.push({
            code: this.currUser?.code,
            displayName: Utils.trimNullToEmpty(this.currUser?.code) + ' - ' + Utils.trimNullToEmpty(this.currUser?.fullName),
            branchCode: this.currUser?.branch
          });
        }
        this.commonData.listRmManagement = [...listRm];
      });
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.page = pageInfo.offset;
    this.search(false);
  }

  /*Mở popup chi tiết*/
  onAction(event) {
    event.event.preventDefault();
    const item = _.get(event, 'row');
    if (event.type === 'dblclick') {
      const dialogRef = this.dialog.open(PopupMoneyTransferComponent, {width: '100%'});
      dialogRef.componentInstance.search(item.customerCode, item.type);
    }
  }

  getStatus(value) {
    return (
      this.listProductMoneyTransfer?.find((item) => {
        return item.code === value;
      })?.name || ''
    );
  }

}
