import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerMoneyTransferComponent } from './manager-money-transfer.component';

describe('ManagerMoneyTransferComponent', () => {
  let component: ManagerMoneyTransferComponent;
  let fixture: ComponentFixture<ManagerMoneyTransferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerMoneyTransferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerMoneyTransferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
