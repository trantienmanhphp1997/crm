import { AfterViewInit, Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from '../../../../../core/components/base.component';
import { global } from '@angular/compiler/src/util';
import { Scopes } from '../../../../../core/utils/common-constants';
import { Pageable } from '../../../../../core/interfaces/pageable.interface';
import { MatDialogRef } from '@angular/material/dialog';
import { finalize } from 'rxjs/operators';
import { CustomerApi } from '../../../../customer-360/apis';
import { Utils } from '../../../../../core/utils/utils';
import * as _ from 'lodash';
import { FileService } from '../../../../../core/services/file.service';


@Component({
  selector: 'app-popup-money-transfer',
  templateUrl: './popup-money-transfer.component.html',
  styleUrls: ['./popup-money-transfer.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PopupMoneyTransferComponent extends BaseComponent implements OnInit, AfterViewInit {

  isLoading = false;
  paramSearch = {
    size: global.userConfig.pageSize,
    page: 0,
    rsId: '',
    scope: Scopes.VIEW
  };
  prevParams: any;
  pageable: Pageable;
  prop: any;
  customerCode: any;
  type: any;
  listDetailMoneyTransfer = [];

  constructor(
    injector: Injector,
    private dialogRef: MatDialogRef<PopupMoneyTransferComponent>,
    private customerApi: CustomerApi,
    private fileService: FileService,
  ) {
    super(injector);
  }

  ngAfterViewInit() {
  }

  ngOnInit(): void {
  }

  closeModal() {
    this.dialogRef.close();
  }

  search(customerCode, type) {
    this.type = type;
    this.isLoading = true;
    this.customerApi.getDetailMoneyTransfer(customerCode, type).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(value => {
      if (value) {
        this.listDetailMoneyTransfer = value;
      }
    }, error => {
      this.messageService.error(this.notificationMessage.error);
    });
  }

  exportFile() {
    if (_.isEmpty(this.listDetailMoneyTransfer)) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    this.isLoading = true;
    const params: any = {};
    params.rmCode = this.listDetailMoneyTransfer[0]?.rmCode;
    params.customerCode = this.listDetailMoneyTransfer[0]?.customerCode;
    params.type = this.type ;

    this.customerApi.exportExcelMoneyTransfer(params).subscribe(
      (res) => {
        if (Utils.isStringNotEmpty(res)) {
          this.download(res);
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      },
      (err) => {
        this.messageService.error(_.get(this.notificationMessage, 'export_error'));
        this.isLoading = false;
      }
    );
  }

  download(fileId: string) {
    this.fileService.downloadFile(fileId, 'Danh sách chi tiết.xlsx').subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }
}
