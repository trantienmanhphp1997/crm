import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupMoneyTransferComponent } from './popup-money-transfer.component';

describe('PopupMoneyTransferComponent', () => {
  let component: PopupMoneyTransferComponent;
  let fixture: ComponentFixture<PopupMoneyTransferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupMoneyTransferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupMoneyTransferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
