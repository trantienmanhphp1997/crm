import { forkJoin } from 'rxjs';
import { CustomerDetailApi } from 'src/app/pages/customer-360/apis';
import {
  FunctionCode,
  Scopes,
  ScreenType,
  TaskType,
} from '../../../../core/utils/common-constants';
import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Utils } from '../../../../core/utils/utils';
import { ViewEncapsulation } from '@angular/core';
import { ActivityActionComponent } from 'src/app/shared/components/activity-action/activity-action.component';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { Validators } from '@angular/forms';
import { SaleManagerApi } from '../../api';
import { CreateTaskModalComponent } from 'src/app/pages/tasks/components/create-task-modal/create-task-modal.component';
import {ActivityHistoryComponent} from '../../../../shared/components';
import {SellingOppIndivCreateEditModalComponent} from '../selling-opp-indiv-create-edit-modal/selling-opp-indiv-create-edit-modal-component';
import { global } from '@angular/compiler/src/util';

@Component({
  selector: 'app-selling-opp-indiv-detail-v2',
  templateUrl: './selling-opp-indiv-detail.component.html',
  styleUrls: ['./selling-opp-indiv-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SellingOppIndivDetailV2Component extends BaseComponent implements OnInit {

  detailInfo: any;
  customerCode: string;
  saleOppId: any;
  is_show = true;
  formUpdate = this.fb.group({
    productCode: ['', Validators.required],
    branchCode: ['', Validators.required],
    memberCode: [null],
    currency: ['', Validators.required],
    amount: [null, Validators.required],
    status: [null, Validators.required],
    guideCode: [null],
    note: [''],
  });
  accordianData = [];
  dataDetailBottomRight: any;
  textCurency: string;
  numberCurency: number;
  constructor(
    injector: Injector,
    private saleManagerApi: SaleManagerApi,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.SELLING_OPPORTUNITY_INDIV}`);
    this.customerCode = _.get(this.route.snapshot.queryParams, 'customerCode');
    this.saleOppId = _.get(this.route.snapshot.queryParams, 'saleOppId');
    this.isLoading = true;
  }

  @ViewChild('sellingOppActivity') sellingOppActivity: any;

  ngOnInit(): void {
    this.initData();
  }

  initData() {
    this.getDetailsForDisplay(this.saleOppId ? Number(this.saleOppId) : null, this.saleOppId ? true : false);
  }

  getDetailsForDisplay(saleOppId, goToList? :boolean) {
      forkJoin([
        this.saleManagerApi.opportunityINDIVDetail(this.customerCode, '', this.objFunction.rsId, Scopes.VIEW),
        this.saleManagerApi.searchOpportunityDetailFirst(this.customerCode, 'custId'),
        this.saleManagerApi.searchOpportunityDetailFirstLead(this.customerCode),
      ]).subscribe(
        ([oppDetail, oppCusDetail, oppCusDetailLead]) => {
          if (oppCusDetail || oppCusDetailLead) {
            this.detailInfo = oppCusDetail ? oppCusDetail : oppCusDetailLead;
          } else {
            this.messageService.error('Không có thông tin KH');
          }
          // const accordianDataSort = oppDetail.sort((a, b) => Number(b.selected) - Number(a.selected));
          this.accordianData = oppDetail;
          // Nếu vào từ màn danh sách thì cho cơ hội được click lên đầu
          if (goToList) {
            const temp = _.find(oppDetail, item => item.saleOppId === saleOppId);
            this.accordianData = _.remove(this.accordianData, item => {
              return item.saleOppId !== saleOppId;
            });
            this.accordianData.unshift(temp);
          }
          // Mặc định thì lấy phần tử đầu
          if (!_.isEmpty(oppDetail)) {
            this.dataDetailBottomRight = (saleOppId && _.some(this.accordianData,{'saleOppId':saleOppId})) ? this.accordianData.filter(item => item.saleOppId === saleOppId)[0] : oppDetail[0];
            console.log('op: ', this.dataDetailBottomRight);
            this.checkCurency();
            this.handlerStepOpp();
          }
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
          this.messageService.error(this.notificationMessage.error);
        }
      );
  }

  mailTo() {
    const url = `mailto:${this.detailInfo?.emails.email}`;
    window.location.href = url;
  }

  createActivity() {
    const parent: any = {};
    parent.parentType = TaskType.CUSTOMER;
    parent.parentId = this.customerCode;
    // parent.parentName = this.customerName;
    const modal = this.modalService.open(ActivityActionComponent, {
      windowClass: 'create-activity-modal',
      scrollable: true,
    });
    modal.componentInstance.parent = parent;
    modal.componentInstance.type = ScreenType.Create;
    modal.componentInstance.isShowFuture = true;
  }

  createTaskTodo() {
    const data: any = {};
    data.parentId = this.customerCode;
    // data.parentName = this.customerName;
    data.parentType = TaskType.CUSTOMER;
    const taskModal = this.modalService.open(CreateTaskModalComponent, {
      windowClass: 'create-task-modal',
      scrollable: true,
    });
    taskModal.componentInstance.data = data;
  }

  show() {
    this.is_show = true;
  }

  close() {
    this.is_show = false;
  }

  createOpp() {
    const modal = this.modalService.open(SellingOppIndivCreateEditModalComponent, {
      windowClass: 'app-create-edit-selling',
      scrollable: true,
    });
    modal.componentInstance.customerCode = this.customerCode;
    modal.componentInstance.title = 'Tạo mới cơ hội';
    modal.result
      .then((res) => {
        if (res) {
          this.isLoading = true;
          this.getDetailsForDisplay(null);
        }
      })
  }

  historyAction() {
    const parent: any = {};
    parent.parentType = TaskType.CUSTOMER;
    parent.parentId = this.detailInfo.customerCode;
    // parent.parentName = row.customerName;
    const modal = this.modalService.open(ActivityHistoryComponent, {
      windowClass: 'create-activity-modal',
      scrollable: true,
    });
    modal.componentInstance.parent = parent;
    modal.componentInstance.type = ScreenType.Create;
    modal.componentInstance.isShowFuture = true;
  }

  checkScopes(codes: Array<any>) {
    if (Utils.isArrayEmpty(codes)) return false;
    return Utils.isArrayNotEmpty(_.filter(this.objFunction.scopes, (x) => codes.includes(x)));
  }

  handleDetailLeftClick(id) {
    this.dataDetailBottomRight = this.accordianData.filter(item => item.saleOppId === id)[0];
    this.checkCurency();
    this.handlerStepOpp();
  }

  checkCurency() {
    if (this.dataDetailBottomRight?.amount < 1000000000) {
      if (this.dataDetailBottomRight?.amount < 1000000) {
        this.numberCurency = this.dataDetailBottomRight?.amount;
        this.textCurency = '';
      } else {
        this.numberCurency = Math.round(this.dataDetailBottomRight?.amount) / 1000000;
        this.textCurency = 'triệu';
      }
    } else {
      this.numberCurency = Math.round(this.dataDetailBottomRight?.amount) / 1000000000;
      this.textCurency = 'tỷ';
    }
  }

  edit() {
    const modal = this.modalService.open(SellingOppIndivCreateEditModalComponent, {
      windowClass: 'app-create-edit-selling',
      scrollable: true,
    });
    modal.componentInstance.customerCode = this.customerCode;
    modal.componentInstance.dataEdit = this.dataDetailBottomRight;
    modal.componentInstance.isEditDisplay = true;
    modal.componentInstance.title = this.dataDetailBottomRight?.code + ' - ' + this.dataDetailBottomRight?.subProductName;
    modal.result
      .then((res) => {
        if (res) {
          this.isLoading = true;
          this.getDetailsForDisplay(res);
        }
      })
  }

  handlerStepOpp() {
    this.dataDetailBottomRight.statusIndex = _.findIndex(this.dataDetailBottomRight?.statusProducts, { number: this.dataDetailBottomRight.status });
    const currentStepNumber = this.dataDetailBottomRight?.status;
    const currentStepText = _.chain(this.dataDetailBottomRight.statusProducts)
      .find(item => item.number === currentStepNumber).value()?.text;
    if (currentStepText === 'Không thành công') {
      _.remove(this.dataDetailBottomRight.statusProducts, {
        text:'Thành công'
      });
    } else {
      _.remove(this.dataDetailBottomRight.statusProducts, {
        text:'Không thành công'
      });
    }
  }

  back() {
    this.isLoading = true;
    if (global?.previousUrl) {
      this.router.navigateByUrl(global.previousUrl, { state: this.prop ? this.prop : this.state });
    } else {
      this.router.navigateByUrl('/sale-manager/selling-opp-indiv', { state: this.prop ? this.prop : this.state });
    }
  }
}
