import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatNumberToMillion',
})
export class FormatNumberToMillionPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    var rounded;

    if (Number.isNaN(value)) {
      return null;
    }

    if (value < 10000) {
      rounded = 0;
    } else {
      if (value % 1000000 === 0) {
        rounded = (value / 1000000).toFixed(0);
      } else {
        rounded = (value / 1000000).toFixed(args);
      }
    }

    rounded = this.numberWithCommas(rounded);
    return rounded;
  }

  numberWithCommas(x) {
    return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');
  }
}
