import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CalendarViewComponent } from './containers/calendar-view/calendar-view.component';
import { CalendarRoutingModule } from './calendar-routing.module';
import { CalendarLeftViewComponent } from './components/calendar-left-view/calendar-left-view.component';
import { CalendarDetailModalComponent } from './components/calendar-detail-modal/calendar-detail-modal.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { FullCalendarModule } from '@fullcalendar/angular';
import { CalendarAddModalComponent } from './components/calendar-add-modal/calendar-add-modal.component';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { BadgeModule } from 'primeng/badge';
import { ChipModule } from 'primeng/chip';
import { AvatarModule } from 'primeng/avatar';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';

import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import listPlugin from '@fullcalendar/list';
import { CalendarComponent } from './components/calendar/calendar.component';

FullCalendarModule.registerPlugins([
  // register FullCalendar plugins
  dayGridPlugin,
  interactionPlugin,
  listPlugin,
  timeGridPlugin,
]);

@NgModule({
  declarations: [
    CalendarViewComponent,
    CalendarLeftViewComponent,
    CalendarDetailModalComponent,
    CalendarAddModalComponent,
    CalendarComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    CalendarModule,
    SharedModule,
    CalendarRoutingModule,
    TranslateModule,
    FullCalendarModule,
    ButtonModule,
    RippleModule,
    BadgeModule,
    ChipModule,
    AvatarModule,
    DropdownModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class CalendarViewModule {}
