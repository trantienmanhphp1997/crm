import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarDetailModalComponent } from './calendar-detail-modal.component';

describe('CalendarDetailModalComponent', () => {
  let component: CalendarDetailModalComponent;
  let fixture: ComponentFixture<CalendarDetailModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarDetailModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarDetailModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
