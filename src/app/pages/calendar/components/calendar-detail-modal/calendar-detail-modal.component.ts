import { Component, OnInit, ViewEncapsulation, HostBinding, Injector, AfterViewInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Format, PriorityTaskOrCalendar, StatusTask } from 'src/app/core/utils/common-constants';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { CalendarService } from '../../services/calendar.service';
import { Utils } from 'src/app/core/utils/utils';
import {ConfirmDialogComponent} from "../../../../shared/components";

@Component({
  selector: 'app-calendar-detail-modal',
  templateUrl: './calendar-detail-modal.component.html',
  styleUrls: ['./calendar-detail-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CalendarDetailModalComponent extends BaseComponent implements OnInit, AfterViewInit {
  data: any;
  @HostBinding('class.app-detail-calendar') classDetailCalendar = true;
  isLoading = false;
  now = new Date();
  form = this.fb.group({
    id: [''],
    name: ['', CustomValidators.required],
    start: [this.now, CustomValidators.required],
    end: [this.now, CustomValidators.required],
    description: [''],
    location: [''],
    level: [''],
  });
  optCreateDate: any = {
    format: Format.DateTimeUp,
    minDate: this.now,
  };
  optDueDate: any = {
    format: Format.DateTimeUp,
    minDate: this.now,
  };
  priority = {
    high: false,
    medium: false,
    low: false,
  };
  isView: boolean;
  isDelete: boolean;
  isEdit = true;
  startText: string;
  isViewDelete: boolean;

  constructor(injector: Injector, private modalActive: NgbActiveModal, private calendarService: CalendarService) {
    super(injector);
  }

  ngOnInit(): void {
    if (this.data) {
      this.optDueDate.minDate = this.data.start;
      this.data.isSave = false;
    }
    this.form.patchValue(this.data);
    this.priority[this.data?.level?.toLowerCase()] = true;
    if (moment().isAfter(moment(this.data.start))) {
      this.form.controls.start.disable();
    }
    if (this.data.userHrsCode !== this.currUser?.hrsCode || this.data.status === StatusTask.DONE) {
      this.isEdit = false;
    }
    if (this.isView) {
      this.form.disable();
    }

    if(moment().isBefore(moment(this.data?.end)) && this.data?.status !== 'DONE'){
      this.isViewDelete = true;
    }
    else{
      this.isViewDelete = false;
    }
  }

  ngAfterViewInit() {
    this.form.controls.start.valueChanges.subscribe((value) => {
      this.optDueDate.minDate = value;
      if (value.valueOf() > this.form.controls.end.value.valueOf()) {
        this.form.controls.end.setValue(value);
      }
    });
  }

  save(isCompleted: boolean) {
    if (this.form.valid || this.form.disabled) {
      const formData = cleanDataForm(this.form);
      if (!this.form.controls.start.disabled && !isCompleted && moment(this.now).isAfter(formData.start)) {
        this.messageService.error(this.notificationMessage.startDateNotLessDateNow);
        return;
      }
      this.isLoading = true;
      Object.keys(formData).forEach((key) => {
        if (this.data[key]) {
          this.data[key] = formData[key];
        }
      });
      if (isCompleted) {
        this.data.status = StatusTask.DONE;
      }
      if (Utils.isStringNotEmpty(this.form.get('location').value)) {
        this.data.location = this.form.get('location').value;
      }
      if (Utils.isStringNotEmpty(this.form.get('description').value)) {
        this.data.description = this.form.get('description').value;
      }
      this.calendarService.update(this.data).subscribe(
        (item) => {
          if(item?.errorCode && item?.errorCode === 'err.api.errorcode.CRM90'){
            this.isLoading = false;
            const confirmSave = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
            confirmSave.componentInstance.message = item?.message + '  Bạn có chắc chắn muốn tiếp tục chỉnh sửa lịch làm việc không?';
            confirmSave.componentInstance.title = 'Cảnh báo';
            confirmSave.result
              .then((confirmed: boolean) => {
                if (confirmed) {
                  this.data.isSave = true;
                  this.isLoading = true;
                  this.calendarService.update(this.data).subscribe((resNew:any) => {
                    this.messageService.success(this.notificationMessage.success);
                    this.isLoading = false;
                    this.modalActive.close({ isDelete: isCompleted, data: this.data });
                  }, (er) => {
                    this.messageService.error(this.notificationMessage.error);
                    this.isLoading = false;
                  });
                }
              })
              .catch(() => {
                this.messageService.error(this.notificationMessage.error);
              });
          }
          else{
            this.messageService.success(this.notificationMessage.success);
            this.modalActive.close({ isDelete: isCompleted, data: this.data });
          }
        },
        () => {
          this.messageService.error(this.notificationMessage.error);
          this.isLoading = false;
        }
      );
    } else {
      validateAllFormFields(this.form);
    }
  }

  delete() {
    this.confirmService
      .confirm()
      .then((res) => {
        if (res) {
          this.isLoading = true;
          this.calendarService.delete(this.data.id).subscribe(
            (response) => {
              console.log('res: ', response);
              this.messageService.success(this.notificationMessage.success);
              this.modalActive.close({ isDelete: true });
            },
            (e) => {
              console.log('error: ', e);
              if(e?.error?.code){
              //  this.messageService.error(e?.error.description);
                this.confirmService.warn(e?.error.description);
              }
              else{
                this.messageService.error(this.notificationMessage.error);
              }
              this.isLoading = false;
            }
          );
        }
      })
      .catch(() => {});
  }

  handlePriority(value: number) {
    if (!this.isEdit || this.isView) {
      return;
    }
    this.priority = {
      high: value === 1,
      medium: value === 2,
      low: value === 3,
    };
    if (value === 1) {
      this.form.controls.level.setValue(PriorityTaskOrCalendar.High);
    } else if (value === 2) {
      this.form.controls.level.setValue(PriorityTaskOrCalendar.Medium);
    } else if (value === 3) {
      this.form.controls.level.setValue(PriorityTaskOrCalendar.Low);
    }
  }

  closeModal() {
    this.modalActive.close(false);
  }

  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }

}
