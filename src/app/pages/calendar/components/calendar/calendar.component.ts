import { ActivityService } from 'src/app/core/services/activity.service';
import { catchError } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { forkJoin, of } from 'rxjs';
import {
  Component,
  ViewChild,
  OnInit,
  Injector,
  AfterViewInit,
  ViewEncapsulation,
  Input,
  EventEmitter,
} from '@angular/core';
import { CalendarService } from '../../services/calendar.service';
import { CalendarDetailModalComponent } from '../../components/calendar-detail-modal/calendar-detail-modal.component';
import { BaseComponent } from 'src/app/core/components/base.component';
import { RmApi } from 'src/app/pages/rm/apis';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import listPlugin from '@fullcalendar/list';
import { CalendarOptions, FullCalendarComponent } from '@fullcalendar/angular';
import allLocales from '@fullcalendar/core/locales-all';
import { CalendarAddModalComponent } from '../../components/calendar-add-modal/calendar-add-modal.component';
import {
  CalendarType,
  FunctionCode,
  PriorityTaskOrCalendar,
  ScreenType,
  StatusTask,
} from 'src/app/core/utils/common-constants';
import { Dropdown } from 'primeng/dropdown';
import { ActivityActionComponent } from 'src/app/shared/components/activity-action/activity-action.component';
import * as moment from 'moment';
import * as _ from 'lodash';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe],
})
export class CalendarComponent extends BaseComponent implements OnInit, AfterViewInit {
  colors = {
    high: '#ffd9dc',
    medium: '#ffe5ce',
    low: '#cdf1dd',
  };
  @ViewChild('dateFilter') dateFilter: any;
  @Input() viewChange: EventEmitter<boolean> = new EventEmitter();
  listData = [];
  isLoading = false;
  show = false;

  events: any[];
  calendarOptions: CalendarOptions = {
    eventClick: (e) => {
      this.openEvent(e.event);
    },
    themeSystem: 'bootstrap',
    plugins: [dayGridPlugin, interactionPlugin, timeGridPlugin, listPlugin],
    initialView: 'timeGridWeek',
    height: '100%',
    locales: allLocales,
    locale: 'vi',
    buttonIcons: false,
    allDaySlot: false,
    headerToolbar: {
      left: 'prev,next today',
      center: 'title selectDate',
      right: 'dayGridMonth,timeGridWeek,timeGridDay',
    },
    customButtons: {
      selectDate: {
        icon: ' btnCustom f-14',
        click: (e) => {
          this.triggerDateFilterClick(e);
        },
      },
    },
    eventOrder: (a: any, b: any) => {
      return b.end - b.start - (a.end - a.start);
    },

    moreLinkClick: (e) => {
      if (e.date.getDay() !== 1) {
        this.handleClearTimeOver();
      }
    },
    dayMaxEvents: 3,
    events: [],
  };
  params = {
    type: '',
    hrsCode: '',
    level: '',
  };
  listEventType = [];
  listRm = [];
  rmSelected: any;
  priority = {
    high: false,
    medium: false,
    low: false,
    all: true,
  };
  level: any;
  @ViewChild('calendar') calendarComponent: FullCalendarComponent;

  constructor(
    injector: Injector,
    private calendarService: CalendarService,
    private rmApi: RmApi,
    private activityService: ActivityService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CALENDAR}`);
    this.isLoading = true;
  }

  ngOnInit() {
    this.params.hrsCode = this.currUser?.hrsCode;
    this.listRm.push({
      hrsCode: this.currUser?.hrsCode,
      displayName: `${this.currUser?.code || ''} - ${this.currUser?.fullName || ''}`,
    });
    this.rmSelected = this.listRm?.find((item) => {
      return item.hrsCode === this.currUser?.hrsCode;
    });
    this.listEventType.push({ code: '', name: this.fields.all });
    this.translate.get('calendar.type').subscribe((result) => {
      if (result) {
        Object.keys(result).forEach((key) => {
          if (
            result[key] &&
            (key.toUpperCase() === CalendarType.CALENDAR || key.toUpperCase() === CalendarType.ACTIVITY)
          ) {
            this.listEventType.push({ code: key.toUpperCase(), name: result[key] });
          }
        });
      }
    });
    this.getEvents();
  }

  getEvents() {
    this.isLoading = true;
    this.calendarService.getEvents(this.params).subscribe(
      (events) => {
        if (events) {
          this.listData = events;
          this.events = [];
          events?.forEach((item) => {
            if (item?.type === CalendarType.CALENDAR || item?.type === CalendarType.ACTIVITY) {
              item.title = item.name;
              item.start = new Date(item.start);
              item.end = item.end ? new Date(item.end) : null;
              item.color = this.colors[item.level?.toLowerCase()];
              item.classNames = item.status === StatusTask.DONE ? 'event-done' : '';
              this.events.push(item);
            }
          });
          this.calendarOptions.events = this.events;
        }

        this.calendarComponent.getApi().resetOptions(this.calendarOptions);
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngAfterViewInit() {
    this.subscriptions.push(
      this.viewChange.subscribe((change) => {
        if (change) {
          this.calendarComponent.getApi().render();
        }
      })
    );
    setTimeout(() => {
      this.show = true;
      setTimeout(() => {
        if (this.calendarComponent.getApi().el.getElementsByClassName('btnCustom').item(0)) {
          this.calendarComponent.getApi().el.getElementsByClassName('btnCustom').item(0).className =
            'las la-calendar-alt f-14';
        }
      }, 100);
    }, 100);
  }

  genderCalendar() {
    this.calendarComponent.getApi().render();
  }

  create() {
    const modal = this.modalService.open(CalendarAddModalComponent, { windowClass: 'create-calendar-modal' });
    modal.result
      .then((res) => {
        if (res) {
          this.params.hrsCode = this.currUser?.hrsCode;
          this.rmSelected = this.listRm?.find((item) => {
            return item.hrsCode === this.currUser?.hrsCode;
          });
          this.getEvents();
        }
      })
      .catch(() => {});
  }

  openEvent(event: any) {
    this.isLoading = true;
    if (event._def?.extendedProps?.type === CalendarType.CALENDAR) {
      const item = this.events?.find((item) => {
        return item.id === event.id;
      });
      const modalDetail = this.modalService.open(CalendarDetailModalComponent, {
        windowClass: 'detail-calendar-modal',
      });
      modalDetail.componentInstance.isView = !this.objFunction?.update;
      modalDetail.componentInstance.isDelete = this.objFunction?.delete && item && moment(item.start).isAfter(moment());
      modalDetail.componentInstance.data = item;
      modalDetail.result
        .then((res) => {
          if (res) {
            this.getEvents();
          }
        })
        .catch(() => {});

      this.isLoading = false;
    } else if (event._def?.extendedProps?.type === CalendarType.ACTIVITY) {
      const params = {
        campaignId: event._def?.extendedProps?.campaignId
      }
      // this.isLoading = true;
      this.activityService
        .getByCodeAndCampaignId(event._def?.extendedProps?.parentId, params)
        .pipe(catchError((e) => of(undefined)))
        .subscribe((data) => {
          this.isLoading = false;
          if (_.isEmpty(data)) {
            this.messageService.error(this.notificationMessage.dataEmpty);
            return;
          }
          const activityModal = this.modalService.open(ActivityActionComponent, {
            windowClass: 'create-activity-modal',
            scrollable: true,
          });
          activityModal.componentInstance.type = ScreenType.Detail;
          activityModal.componentInstance.isActionHeader = true;
          activityModal.componentInstance.data = data;
          activityModal.componentInstance.id = event._def?.extendedProps?.parentId;
          activityModal.result
            .then((res) => {
              if (res) {
                this.getEvents();
              }
            })
            .catch(() => {});
        });
    }
  }

  onChangeEventType(event) {
    this.params.type = event.value.code;
    this.getEvents();
  }

  onChangeRM(event) {
    this.params.hrsCode = event.value.hrsCode;
    this.getEvents();
  }

  onBlur(dropdown: Dropdown) {
    this.ref.detectChanges();
  }

  handlePriority(value: number) {
    this.priority = {
      high: value === 1,
      medium: value === 2,
      low: value === 3,
      all: value === 4,
    };
    if (value === 1) {
      this.params.level = PriorityTaskOrCalendar.High;
    } else if (value === 2) {
      this.params.level = PriorityTaskOrCalendar.Medium;
    } else if (value === 3) {
      this.params.level = PriorityTaskOrCalendar.Low;
    } else {
      this.params.level = '';
    }
    this.ref.detectChanges();
    this.getEvents();
  }

  triggerDateFilterClick(e) {
    this.dateFilter.toggle();
    e.stopPropagation();
  }

  gotoDate(value: Date) {
    this.calendarComponent.getApi().gotoDate(value);
  }

  handleClearTimeOver() {
    setTimeout(() => {
      const elements = document
        .getElementsByClassName('fc-popover-body')
        ?.item(0)
        ?.getElementsByClassName('fc-event-time');
      for (let index = 0; index <= elements.length; index++) {
        const el = elements.item(index);
        if (el?.textContent === '00') {
          el?.setAttribute('class', 'fc-event-time d-none');
        }
      }
    });
  }
}
