import { Component, OnInit, HostBinding, Injector, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { CalendarService } from '../../services/calendar.service';
import { Format, PriorityTaskOrCalendar } from 'src/app/core/utils/common-constants';
import {ConfirmDialogComponent} from "../../../../shared/components";
import {Utils} from "../../../../core/utils/utils";

@Component({
  selector: 'app-calendar-add-modal',
  templateUrl: './calendar-add-modal.component.html',
  styleUrls: ['./calendar-add-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CalendarAddModalComponent extends BaseComponent implements OnInit {
  @HostBinding('class.app-create-calendar') classCreateCalendar = true;
  isLoading = false;
  now = new Date();
  form = this.fb.group({
    name: ['', CustomValidators.required],
    start: [this.now, CustomValidators.required],
    end: [this.now, CustomValidators.required],
    description: [''],
    location: [''],
    level: [PriorityTaskOrCalendar.Medium],
    day: [''],
    isSave: false
  });
  optCreateDate = {
    format: Format.DateTimeUp,
    minDate: this.now,
  };
  optDueDate = {
    format: Format.DateTimeUp,
    minDate: this.now,
  };
  priority = {
    high: false,
    medium: true,
    low: false,
  };

  constructor(injector: Injector, private modalActive: NgbActiveModal, private calendarService: CalendarService) {
    super(injector);
  }

  ngOnInit(): void {}

  ngAfterViewInit() {
    this.form.controls.start.valueChanges.subscribe((value) => {
      this.optDueDate.minDate = value;
      if (value.valueOf() > this.form.controls.end.value.valueOf()) {
        this.form.controls.end.setValue(value);
      }
    });
  }

  confirmDialog() {
    if (this.form.valid) {
      this.isLoading = true;
      const data = cleanDataForm(this.form);
      data.allDay = false;
      this.calendarService.create(data).subscribe(
        (item) => {
          if(item?.errorCode && item?.errorCode === 'err.api.errorcode.CRM90'){
            this.isLoading = false;
            const confirmSave = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
            confirmSave.componentInstance.message = item?.message + '  Bạn có chắc chắn muốn tiếp tục thêm mới lịch làm việc không?';
            confirmSave.componentInstance.title = 'Cảnh báo';
            confirmSave.result
              .then((confirmed: boolean) => {
                if (confirmed) {
                  data.isSave = true;
                  this.isLoading = true;
                  this.calendarService.create(data).subscribe((resNew:any) => {
                    this.messageService.success(this.notificationMessage.success);
                    this.isLoading = false;
                    this.modalActive.close(item);
                  }, (er) => {
                    this.messageService.error(this.notificationMessage.error);
                    this.isLoading = false;
                  });
                }
              })
              .catch(() => {
                this.messageService.error(this.notificationMessage.error);
              });
          }
          else{
            this.messageService.success(this.notificationMessage.success);
            this.modalActive.close(item);
          }
        },
        () => {
          this.messageService.error(this.notificationMessage.error);
          this.isLoading = false;
        }
      );
    } else {
      validateAllFormFields(this.form);
    }
  }

  handlePriority(value: number) {
    this.priority = {
      high: value === 1,
      medium: value === 2,
      low: value === 3,
    };
    if (value === 1) {
      this.form.controls.level.setValue(PriorityTaskOrCalendar.High);
    } else if (value === 2) {
      this.form.controls.level.setValue(PriorityTaskOrCalendar.Medium);
    } else if (value === 3) {
      this.form.controls.level.setValue(PriorityTaskOrCalendar.Low);
    }
  }

  closeModal() {
    this.modalActive.close(false);
  }

  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }
}
