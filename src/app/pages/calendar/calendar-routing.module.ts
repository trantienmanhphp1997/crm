import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGuardService } from 'src/app/core/services/role-guard.service';
import { FunctionCode } from 'src/app/core/utils/common-constants';
import { CalendarLeftViewComponent } from './components/calendar-left-view/calendar-left-view.component';
import { CalendarViewComponent } from './containers/calendar-view/calendar-view.component';

const routes: Routes = [
  {
    path: '',
    component: CalendarLeftViewComponent,
    outlet: 'app-left-content',
  },
  {
    path: '',
    component: CalendarViewComponent,
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.CALENDAR,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CalendarRoutingModule {}
