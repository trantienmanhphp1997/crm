import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CalendarComponent } from '../../components/calendar/calendar.component';

@Component({
  selector: 'app-calendar-view',
  templateUrl: './calendar-view.component.html',
  styleUrls: ['./calendar-view.component.scss'],
})
export class CalendarViewComponent extends BaseComponent implements OnInit {
  @ViewChild(CalendarComponent) calendar: CalendarComponent;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {}

  showSideBar() {
    setTimeout(() => {
      this.calendar.genderCalendar();
    }, 100);
  }
}
