import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CalendarService {
  constructor(private http: HttpClient) {}

  getActivities(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_activity}/activities`, { params });
  }

  getEvents(params?): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/events/findAll`, params);
  }

  create(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/events`, data);
  }

  update(data): Observable<any> {
    return this.http.put(`${environment.url_endpoint_activity}/events/v2`, data);
  }

  delete(id): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_activity}/events/${id}`);
  }
}
