import { Component, Injector, OnChanges, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseComponent } from '../../../../core/components/base.component';
import { FunctionCode, functionUri, Scopes } from '../../../../core/utils/common-constants';
import { TicketService } from '../../services/ticket.service';
import { LIST_PRIORITY, LIST_STATUS } from '../../constant';
import { global } from '@angular/compiler/src/util';
import { callbackify } from 'util';

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.scss']
})

export class TicketListComponent extends BaseComponent implements OnInit {
  @ViewChild('tablerm') public tablerm: DatatableComponent;
  searchValue = '';
  isShowFilter: Boolean = false;

  tickets = [];
  count: number;
  offset: number;
  limit = global.userConfig.pageSize;

  listStatus = LIST_STATUS;
  listPriority = LIST_PRIORITY;

  formSearch = this.fb.group({
    TICKET_NAME: '',
    CREATED_BY: '',
    UPDATED_DATE: '',
    UPDATED_BY: '',
    CHANNEL: '',
    ACTION_USER: '',
    PRIORITY: '',
    IS_ACTIVED: '',
    DESCRIPTION: '',
    CREATED_DATE: '',
  });
  itemFilter = {
    id: 'filter',
    name: '',
    createdByRm: '',
    createdDate: '',
    description: '',
    status: '',
    priority: '',
    actionUser: '',
    channel: '',
    updatedBy: '',
    updatedDate: ''
  };
  isLoading = false;
  paramSearch = {
    pageSize: global?.userConfig?.pageSize,
    pageNumber: 0,
    searchValue: '',
    sortColumn: '',
    sortValue: 0,
    filters: [],
  }
  isFiltered: boolean;

  props: any;

  constructor(injector: Injector, private ticketService: TicketService) {
    super(injector);
    this.props = this.router.getCurrentNavigation()?.extras?.state;
  }

  ngOnInit() {
    if (this.props) { // xử lý khi back lại từ update screen
      this.formSearch.patchValue(this.props.form);
      this.paramSearch = this.props?.param || {};
      this.searchValue = this.paramSearch?.searchValue || '';
      this.isShowFilter = this.props?.show || false;
    }
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.TICKETS}`);
    this.getData(false, () => {
      this.isShowFilter && this.showSearchAdvance(true);
    });
  }

  getData(isPaging?: Boolean, callback?: Function) {
    try {
      let params: any;
      this.isLoading = true;
      if (isPaging) {
        this.paramSearch.pageNumber = 0;
      }
      this.paramSearch.searchValue = this.searchValue.trim();
      params = { ...this.paramSearch, rsId: this.objFunction?.rsId, scope: Scopes.VIEW };
      params.pageNumber = this.paramSearch.pageNumber;
      this.ticketService.getTickets(params).subscribe(result => {
        this.paramSearch = params;
        this.tickets = this.convertListTicket(result?.content || []);
        console.log('this.tickets', this.tickets);
        this.count = result?.totalElements || 0;
        this.offset = result?.number || 0;
        this.isLoading = false;
        callback && callback();
      }, () => {
        this.messageService.error('Kết nối hệ thống không thành công. Vui lòng thử lại sau');
        this.isLoading = false;
      });
    } catch (err) {
      this.messageService.error('Kết nối hệ thống không thành công. Vui lòng thử lại sau');
      this.isLoading = false;
    }
  }

  searchBasic() {
    this.getData(true, () => {
      this.isShowFilter && this.showSearchAdvance(true);
    });
  }

  convertListTicket(list) {
    const tickets = list.map(item => {
      const isActivedName = LIST_STATUS.find(element => element.code === item.isActived)?.name || '';
      const priorityName = LIST_PRIORITY.find(element => element.code === item.priority)?.name || '';
      return {
        ...item,
        isActivedName,
        priorityName
      }
    }) || [];
    return tickets;
  }

  showSearchAdvance(status?: Boolean) {
    if (status) {
      this.count++
      this.tickets = [this.itemFilter, ...this.tickets]
    } else {
      this.count--
      this.tickets = [...this.tickets.slice(1)];
      if(this.paramSearch?.filters?.length > 0) {
        this.paramSearch.filters = [];
        this.formSearch.reset();
        this.getData();
      } 
    }
    this.isShowFilter = status;
  }

  paging(event) {
    if (!this.isLoading) {
      this.paramSearch.pageNumber = event?.page ? event.page - 1 : 0;
      this.getData(false, () => {
        this.isShowFilter && this.showSearchAdvance(true);
      });
    }
  }

  filter() {
    const filterList = [];
    Object.keys(this.formSearch.value).map((key: string) => {
      if (this.formSearch.value[key]) {
        filterList.push({
          key,
          value: this.formSearch.value[key]
        })
      }
    })

    this.paramSearch.filters = filterList;
    this.getData(true, () => {
      this.showSearchAdvance(this.isShowFilter);
    });
  }

  onSelect(item) {
    if (item.type === 'dblclick' && item.row.id !== 'filter') {
      this.update(item.row);
    }
  }

  pin(item) {
    const data = {
      id: item.chatTicketId,
      paramSearch: this.paramSearch
    };
    this.isLoading = true;
    this.ticketService.pinTicket(data).subscribe((result) => {
      this.isLoading = false;
      this.tickets = this.convertListTicket(result?.content || []);
    }, () => {
      this.messageService.error('Kết nối hệ thống không thành công. Vui lòng thử lại sau');
      this.isLoading = false;
    });
  }

  sort(value) {
    this.paramSearch.sortColumn = value.column.prop;
    this.paramSearch.sortValue = value.newValue === 'asc' ? 1 : -1;
    this.getData(true, () => {
      this.isShowFilter && this.showSearchAdvance(true);
    });
  }

  navigate(url: string, chatTicketId: number) {
    this.router.navigateByUrl(url, {
      state: {
        form: this.formSearch.getRawValue(),
        param: this.paramSearch,
        chatTicketId,
        show: this.isShowFilter,
      }
    });
  }

  update(row) {
    this.navigate(`${functionUri.tickets_manager}/update/${row.chatTicketId}`, row.chatTicketId);
  }
}
