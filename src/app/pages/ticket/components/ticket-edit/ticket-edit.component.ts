import { Component, Injector, OnInit } from '@angular/core';
import { SelectionType } from '@swimlane/ngx-datatable';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Utils } from 'src/app/core/utils/utils';
import { CustomerAssignmentApi } from 'src/app/pages/customer-360/apis';
import { RmApi } from 'src/app/pages/rm/apis';
import { RmModalComponent } from 'src/app/pages/rm/components';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { BaseComponent } from '../../../../core/components/base.component';
import { FunctionCode, functionUri, Scopes } from '../../../../core/utils/common-constants';
import { LIST_PRIORITY, LIST_STATUS } from '../../constant';
import { TicketService } from '../../services/ticket.service';
import { StatusModalComponent } from '../status-modal/status-modal.component';

@Component({
  selector: 'app-ticket-detail',
  templateUrl: './ticket-edit.component.html',
  styleUrls: ['./ticket-edit.component.scss']
})
export class TicketEditComponent extends BaseComponent implements OnInit {
  listStatus = LIST_STATUS;
  listPriority = LIST_PRIORITY;
  listCategory = [];
  listChildCategory = [];
  listRawChildCate = [];

  param: any;
  isLoading = true;
  form = this.fb.group({
    rm: '',
    actionUser: '',
    priority: '',
    category: '',
    childCategory: '',
  });

  ticketInfo = {
    ticketName: '',
    ticketCode: '',
    description: '',
    sla: '',
    createdByName: '',
    createdByRm: '',
    createdDate: '',
    updatedBy: '',
    updatedDate: '',
    actionDated: '',
    requestUser: '',
    createdByType: '',
    statusName: '',
    isActived: '',
    note: ''
  };

  isCBQL: boolean;

  constructor(
    injector: Injector,
    private ticketService: TicketService,
    private rmApi: RmApi,
    private categoryService: CategoryService,
  ) {
    super(injector);
    const navigation = this.router?.getCurrentNavigation();
    const state = navigation?.extras?.state;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.TICKETS}`);
    this.param = state;
  }

  ngOnInit() {
    try {
      const chatTicketId = this.param.chatTicketId;
      this.handleInputStatus();

      forkJoin([
        this.ticketService.getDetailTicket(chatTicketId),
        this.ticketService.getCategoryConfig(),
        this.categoryService
          .getBranchesOfUser(this.objFunction?.rsId, Scopes.UPDATE)
          .pipe(catchError((e) => of(undefined))),
      ]).subscribe(([ticketDetail, CategoryConfig, listBranch]) => {
        this.isCBQL = listBranch?.length > 0;
        this.isLoading = false;
        this.listCategory = CategoryConfig?.parent || [];
        this.listRawChildCate = CategoryConfig?.child || [];

        const categoryCodeSelected = this.listCategory.find(item => item.value === ticketDetail?.category)?.code;
        this.listChildCategory = this.listRawChildCate.filter(item => item.commonCategoryCode === categoryCodeSelected);
        const childCateCodeSelected = this.listRawChildCate.find(item => item.value === ticketDetail?.subCategory)?.code;

        this.ticketInfo = {
          ...ticketDetail,
          statusName: this.getStatusName(ticketDetail.isActived)
        }

        this.form.patchValue({
          category: categoryCodeSelected,
          childCategory: childCateCodeSelected,
          priority: ticketDetail.priority,
          actionUser: ticketDetail.actionUser,
          rm: ticketDetail.actionUserName,
        });

      }, () => {
        this.isLoading = false;
      });
    } catch (err) {
      this.isLoading = false;
      console.log('err', err);
    }
  }

  ngAfterViewInit() {
    this.form.controls['category'].valueChanges.subscribe(value => {
      this.listChildCategory = this.listRawChildCate.filter(item => item.commonCategoryCode === value);
    });
  }

  getStatusName(code) {
    const name = LIST_STATUS.find(item => item.code === code)?.name;
    return name;
  }

  handleInputStatus() {
    const inputStatus = document.getElementById('input-status');
    inputStatus.addEventListener('click', () => {
      const config = {
        selectionType: SelectionType.single,
      };
      const modal = this.modalService.open(StatusModalComponent, {windowClass: 'status-modal'});
      modal.componentInstance.config = config;
      modal.componentInstance.data = this.ticketInfo;
      modal.result
      .then((res) => {
        this.ticketInfo.isActived = res.isActived;
        this.ticketInfo.statusName = this.getStatusName(res.isActived);
        this.ticketInfo.note = res.note;
      })
    });
  }

  selectRm() {
    const formSearch = {
      crmIsActive: { value: 'true', disabled: true },
      isAssignRm: true,
    };
    const config = {
      selectionType: SelectionType.single,
    };
    const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
    modal.componentInstance.config = config;
    modal.componentInstance.dataSearch = formSearch;
    modal.componentInstance.listHrsCode = [this.form.controls.actionUser.value];
    modal.result
      .then((res) => {
        if (res?.listSelected?.length > 0) {
          if (Utils.trimNullToEmpty(res.listSelected[0]?.t24Employee?.employeeCode) === '') {
            this.confirmService.warn(this.notificationMessage.ASSIGN010);
            return;
          }
          forkJoin([
            this.rmApi.checkRmAssign(res.listSelected[0].hrisEmployee?.employeeId).pipe(catchError((e) => of(false))),
            this.rmApi
              .checkRmNTSNotAssign(res.listSelected[0].hrisEmployee?.employeeId)
              .pipe(catchError((e) => of(false))),
          ]).subscribe(([checkRmAssign, checkRmNTSNotAssign]) => {
            if (checkRmAssign || checkRmNTSNotAssign) {
              const messages = [];
              if (checkRmAssign) {
                messages.push(this.notificationMessage.ASSIGN008);
              }
              if (checkRmNTSNotAssign) {
                messages.push(this.notificationMessage.ASSIGN009);
              }
              this.confirmService.warn(messages);
              return;
            } else {
              this.form.controls.rm.setValue(`${res.listSelected[0].hrisEmployee?.fullName}`);
              this.form.controls.actionUser.setValue(res.listSelected[0].hrisEmployee?.employeeId);
            }
          });
        }
      })
      .catch(() => { });
  }

  update() {
    try {
      this.isLoading = true;
      const data = {
        ...this.ticketInfo,
        actionUser: this.form.controls.actionUser.value,
        actionUserName: this.form.controls.rm.value,
        priority: this.form.controls.priority.value,
        category: this.listCategory.find(item => item.code === this.form.controls.category.value)?.value || '',
        subCategory: this.listRawChildCate.find(item => item.code === this.form.controls.childCategory.value)?.value || '',
      };
      console.log('data', data);

      this.ticketService.updateTicket(data).subscribe(value => {
        this.isLoading = false;
        this.messageService.success(this.notificationMessage.success);
        this.back();
      }, () => {
        this.messageService.error('Kết nối hệ thống không thành công. Vui lòng thử lại sau');
        this.isLoading = false;
      })
    } catch (err) {
      console.log(err);
      this.messageService.error('Kết nối hệ thống không thành công. Vui lòng thử lại sau');
      this.isLoading = false;
    }
  }

  back() {
    this.router.navigate([`${functionUri.tickets_manager}`], { state: this.param })
  }

}
