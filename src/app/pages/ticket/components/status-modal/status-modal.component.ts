import { Component, HostBinding, Injector, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseComponent } from 'src/app/core/components/base.component';
import { LIST_STATUS } from '../../constant';
import { TicketService } from '../../services/ticket.service';

@Component({
  selector: 'app-status-modal',
  templateUrl: './status-modal.component.html',
  styleUrls: ['./status-modal.component.scss'],

  encapsulation: ViewEncapsulation.None,
})
export class StatusModalComponent extends BaseComponent implements OnInit {
  listStatus = LIST_STATUS;
  @Input() data: any;

  form = this.fb.group({
    isActived: '',
    note: ''
  });

  constructor(injector: Injector, private ticketService: TicketService, public activeModal: NgbActiveModal) {
    super(injector);
  }

  ngOnInit() {
    this.form.patchValue({
      isActived: this.data.isActived,
      note: this.data.note
    });
  }

  updateStatus() {
    try {
      this.isLoading = true;
      const data = {
        ...this.data,
        isActived: this.form.controls.isActived.value,
        note: this.form.controls.note.value,
      };

      this.ticketService.updateTicket(data).subscribe(value => {
        this.isLoading = false;
        this.messageService.success(this.notificationMessage.success);
        const result = {
          isActived: this.form.controls.isActived.value,
          note: this.form.controls.note.value,
        };
        this.activeModal.close(result);
      }, () => {
        this.messageService.error('Kết nối hệ thống không thành công. Vui lòng thử lại sau');
        this.isLoading = false;
      })
    } catch (err) {
      console.log(err);
      this.messageService.error('Kết nối hệ thống không thành công. Vui lòng thử lại sau');
      this.isLoading = false;
    }
  }

  closeModal() {
    this.activeModal.dismiss('Cross click');
  }

}
