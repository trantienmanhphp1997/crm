import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoleGuardService } from 'src/app/core/services/role-guard.service';
import { TicketLeftViewComponent } from './components/ticket-left-view/ticket-left-view.component';
import { TicketEditComponent } from './components/ticket-edit/ticket-edit.component';
import { FunctionCode, Scopes } from 'src/app/core/utils/common-constants';
import { TicketListComponent } from './components/ticket-list/ticket-list.component';

const routes: Routes = [
  // {
  //   path: '',
  //   component: TicketLeftViewComponent,
  //   outlet: 'app-left-content',
  // },
  {
    path: 'ticket-management',
    canActivateChild: [RoleGuardService],
    data: {
      code: FunctionCode.TICKETS
    },
    children: [
      {
        path: '',
        component: TicketListComponent,
        // runGuardsAndResolvers: 'always',
        data: {
          scope: Scopes.VIEW
        }
      },
      {
        path: 'update/:code',
        component: TicketEditComponent,
        // runGuardsAndResolvers: 'always',
        data: {
          scope: Scopes.UPDATE
        }
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TicketRoutingModule {}
