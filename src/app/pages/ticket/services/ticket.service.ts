import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class TicketService {
  constructor(private http: HttpClient) {
  }
  getTickets(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_ticket}/ticket`, data);
  }
  updateTicket(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_ticket}/ticket/process`, data);
  }
  getDetailTicket(id): Observable<any> {
    return this.http.get(`${environment.url_endpoint_ticket}/ticket/${id}`);
  }
  getCategoryConfig(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_ticket}/ticket/sla-category-config`);
  }
  pinTicket(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_ticket}/ticket/pin/${data.id}`, data.paramSearch);
  }
}
