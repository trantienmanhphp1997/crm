import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { SharedModule } from 'src/app/shared/shared.module';
import { TicketEditComponent } from './components/ticket-edit/ticket-edit.component';
import { TicketListComponent } from './components/ticket-list/ticket-list.component';
import { TicketRoutingModule } from './ticket-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { TicketLeftViewComponent } from './components/ticket-left-view/ticket-left-view.component';
import { StatusModalComponent } from './components/status-modal/status-modal.component';

@NgModule({
  imports: [
    CommonModule,
    TicketRoutingModule,
    HttpClientModule,
    SharedModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    DropdownModule,
    CalendarModule
  ],
  // exports: [
  //   TicketViewComponent
  // ],
  declarations: [TicketLeftViewComponent,
    TicketListComponent,
    TicketEditComponent,
    StatusModalComponent
  ],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class TicketModule { }
