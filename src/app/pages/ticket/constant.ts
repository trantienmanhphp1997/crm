export const LIST_STATUS = [
    {code: 1, name: "Mở"},
    {code: 2, name: "Tiến hành"},
    {code: 3, name: "Chờ người dùng phản hồi"},
    {code: 4, name: "Đóng"}
];

export const LIST_PRIORITY = [
    {code: 1, name: "Cao"},
    {code: 2, name: "TB"},
    {code: 3, name: "Thấp"},
];