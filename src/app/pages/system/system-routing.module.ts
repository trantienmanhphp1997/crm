import { BlocksCategoryUpdateComponent } from './components/blocks-category-update/blocks-category-update.component';
import { BlocksCategoryCreateComponent } from './components/blocks-category-create/blocks-category-create.component';
import { RoleCategoryUpdateComponent } from './components/role-category-update/role-category-update.component';
import { RoleCategoryCreateComponent } from './components/role-category-create/role-category-create.component';
import { ManipulationCategoryCreateComponent } from './components/manipulation-category-create/manipulation-category-create.component';
import { ApplicationsConfigEditModalComponent } from './components/applications-config-edit-modal/applications-config-edit-modal.component';
import { ApplicationsConfigAddModalComponent } from './components/applications-config-add-modal/applications-config-add-modal.component';
import { ResourcesCategoryUpdateComponent } from './components/resources-category-update/resources-category-update.component';
import { ResourcesCategoryCreateComponent } from './components/resources-category-create/resources-category-create.component';
import { BranchCategoryUpdateComponent } from './components/branch-category-update/branch-category-update.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersConfigComponent } from './container/users-config/users-config.component';
import { TemplateConfigComponent } from './container/template-config/template-config.component';
import { CategoryConfigComponent } from './container/category-config/category-config.component';
import { WorkflowConfigComponent } from './container/workflow-config/workflow-config.component';
import { UserSessionsComponent } from './container/user-sessions/user-sessions.component';
import { ApplicationsConfigComponent } from './container/applications-config/applications-config.component';
import { CommonCategoryComponent } from './container/common-category/common-category.component';
import { BranchCategoryComponent } from './container/branch-category/branch-category.component';
import { BlocksCategoryComponent } from './container/blocks-category/blocks-category.component';
import { ResourcesCategoryComponent } from './container/resources-category/resources-category.component';
import { ManipulationCategoryComponent } from './container/manipulation-category/manipulation-category.component';
import { RoleCategoryComponent } from './container/role-category/role-category.component';
import { UserRoleConfigComponent } from './container/user-role-config/user-role-config.component';
import { LoggingHistoryComponent } from './container/logging-history/logging-history.component';
import { PermissionCategoryComponent } from './container/permission-category/permission-category.component';
import { PermissionCategoryCreateComponent } from './components/permission-category-create/permission-category-create.component';
import { PermissionCategoryUpdateComponent } from './components/permission-category-update/permission-category-update.component';
import { UserPermissionConfigComponent } from './container/user-permission-config/user-permission-config.component';
import { UserPermissionImportComponent } from './components/user-permission-import/user-permission-import.component';
import { FunctionCode, Scopes, ScreenType } from 'src/app/core/utils/common-constants';
import { ManipulationCategoryUpdateComponent } from './components/manipulation-category-update/manipulation-category-update.component';
import { UserAddModalComponent } from './components/user-add-modal/user-add-modal.component';
import { UserEditModalComponent } from './components/user-edit-modal/user-edit-modal.component';
import { ProcessManagementComponent } from './components/process-management/process-management.component';
import { ProcessManagementActionComponent } from './components/process-management.action/process-management.action.component';
import { TrueResolve } from './resolvers';
import { RoleGuardService } from 'src/app/core/services/role-guard.service';
import { PermissionForRoleComponent } from './container/permission-for-role/permission-for-role.component';
import { ListTitleRelationshipGroup } from './components/title-relationship-group/list-title-relationship-group.component';
import { TitleRelationshipGroupCreate } from './components/title-relationship-group-create/title-relationship-group-create.component';
import { ExportDataComponent } from './components/export-data/export-data.component';
import { NoteComponent } from './components/note/note.component';
import { UploadImageComponent } from './components/upload-image/upload-image.component';
import {NewsCreateComponent} from './components/news/news-create.component';
import {NewsListComponent} from './components/news/list/news-list.component';
import {UploadImageNewComponent} from "./components/upload-image-new/upload-image-new.component";
import { FrequencyOfUsingFunctionComponent } from './components/frequency-of-using-function/frequency-of-using-function.component';

const routes: Routes = [
  {
    path: '',
    outlet: 'app-left-content',
  },
  {
    path: '',
    // component: SystemComponent,
    children: [
      // { path: '', redirectTo: 'users-sessions', pathMatch: 'full' },
      {
        path: 'users-config',
        canActivateChild: [RoleGuardService],
        data: { code: FunctionCode.ADMIN_USER },
        children: [
          {
            path: '',
            component: UsersConfigComponent,
            data: { scope: Scopes.VIEW },
          },
          {
            path: 'create',
            component: UserAddModalComponent,
            data: {
              scope: Scopes.CREATE,
            },
          },
          {
            path: 'update/:username',
            component: UserEditModalComponent,
            data: {
              scope: Scopes.UPDATE,
            },
          },
        ],
      },
      {
        path: 'template-config',
        component: TemplateConfigComponent,
      },
      {
        path: 'category-config',
        component: CategoryConfigComponent,
      },
      {
        path: 'workflow-config',
        component: WorkflowConfigComponent,
      },
      {
        path: 'users-sessions',
        component: UserSessionsComponent,
        canActivate: [RoleGuardService],
        data: { code: FunctionCode.ADMIN_SESSIONS },
      },
      {
        path: 'applications-config',
        canActivateChild: [RoleGuardService],
        data: { code: FunctionCode.ADMIN_APP },
        children: [
          {
            path: '',
            component: ApplicationsConfigComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
          {
            path: 'create',
            component: ApplicationsConfigAddModalComponent,
            data: {
              scope: Scopes.CREATE,
            },
          },
          {
            path: 'update/:id',
            component: ApplicationsConfigEditModalComponent,
            data: {
              scope: Scopes.UPDATE,
            },
          },
        ],
      },
      {
        path: 'common-category',
        canActivateChild: [RoleGuardService],
        data: { code: FunctionCode.ADMIN_COMMON },
        children: [
          {
            path: '',
            component: CommonCategoryComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
        ],
      },
      {
        path: 'branches-category',
        canActivateChild: [RoleGuardService],
        data: { code: FunctionCode.ADMIN_BRANCH },
        children: [
          {
            path: '',
            component: BranchCategoryComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
          {
            path: 'update/:code',
            component: BranchCategoryUpdateComponent,
            data: {
              scope: Scopes.UPDATE,
            },
          },
        ],
      },
      {
        path: 'blocks-category',
        canActivateChild: [RoleGuardService],
        data: { code: FunctionCode.ADMIN_BLOCK },
        children: [
          {
            path: '',
            component: BlocksCategoryComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
          {
            path: 'create',
            component: BlocksCategoryCreateComponent,
            data: {
              scope: Scopes.CREATE,
            },
          },
          {
            path: 'update/:id',
            component: BlocksCategoryUpdateComponent,
            data: {
              scope: Scopes.UPDATE,
            },
          },
        ],
      },
      {
        path: 'resources-category',
        canActivateChild: [RoleGuardService],
        data: { code: FunctionCode.ADMIN_FUNCTION },
        children: [
          {
            path: '',
            component: ResourcesCategoryComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
          {
            path: 'create',
            component: ResourcesCategoryCreateComponent,
            data: {
              scope: Scopes.CREATE,
            },
          },
          {
            path: 'update/:id',
            component: ResourcesCategoryUpdateComponent,
            data: {
              scope: Scopes.UPDATE,
            },
          },
        ],
      },
      {
        path: 'manipulation-category',
        canActivateChild: [RoleGuardService],
        data: { code: FunctionCode.ADMIN_SCOPES },
        children: [
          {
            path: '',
            component: ManipulationCategoryComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
          {
            path: 'create',
            component: ManipulationCategoryCreateComponent,
            data: {
              scope: Scopes.CREATE,
            },
          },
          {
            path: 'update/:id',
            component: ManipulationCategoryUpdateComponent,
            data: {
              scope: Scopes.UPDATE,
            },
          },
        ],
      },
      {
        path: 'role-category',
        canActivateChild: [RoleGuardService],
        data: { code: FunctionCode.ADMIN_ROLE },
        children: [
          {
            path: '',
            component: RoleCategoryComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
          {
            path: 'create',
            component: RoleCategoryCreateComponent,
            data: {
              scope: Scopes.CREATE,
            },
          },
          {
            path: 'update/:name',
            component: RoleCategoryUpdateComponent,
            data: {
              scope: Scopes.UPDATE,
            },
          },
        ],
      },
      {
        path: 'user-role-config',
        component: UserRoleConfigComponent,
        canActivate: [RoleGuardService],
        data: { code: FunctionCode.ADMIN_USER_ROLE },
      },
      {
        path: 'permission-role',
        component: PermissionForRoleComponent,
        canActivate: [RoleGuardService],
        data: { code: FunctionCode.ADMIN_PERMISSION_ROLE },
      },
      {
        path: 'log-history',
        component: LoggingHistoryComponent,
        canActivate: [RoleGuardService],
        data: { code: FunctionCode.ADMIN_LOG },
      },
      {
        path: 'permission-config',
        canActivateChild: [RoleGuardService],
        data: { code: FunctionCode.ADMIN_PERMISSION },
        children: [
          {
            path: '',
            component: PermissionCategoryComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
          {
            path: 'create',
            component: PermissionCategoryCreateComponent,
            data: {
              scope: Scopes.CREATE,
            },
          },
          {
            path: 'update/:id/:type',
            component: PermissionCategoryUpdateComponent,
            data: {
              scope: Scopes.UPDATE,
            },
          },
        ],
      },
      {
        path: 'user-permission-config',
        canActivateChild: [RoleGuardService],
        data: { code: FunctionCode.ADMIN_DATA_DOMAIN },
        children: [
          {
            path: '',
            component: UserPermissionConfigComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
          {
            path: 'import',
            component: UserPermissionImportComponent,
            data: {
              scope: Scopes.IMPORT,
            },
          },
        ],
      },
      {
        path: 'process-management',
        canActivateChild: [RoleGuardService],
        data: { code: FunctionCode.PROCESS_MANAGEMENT },
        children: [
          {
            path: '',
            component: ProcessManagementComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
          {
            path: 'create',
            component: ProcessManagementActionComponent,
            resolve: { isCreate: TrueResolve },
            data: {
              scope: Scopes.CREATE,
            },
          },
          {
            path: 'update/:code',
            component: ProcessManagementActionComponent,
            resolve: { isUpdate: TrueResolve },
            data: {
              scope: Scopes.UPDATE,
            },
          },
          {
            path: 'view/:code',
            component: ProcessManagementActionComponent,
            resolve: { isView: TrueResolve },
            data: {
              scope: Scopes.VIEW,
            },
          },
        ],
      },
      {
        path: 'export-data',
        canActivateChild: [RoleGuardService],
        data: { code: FunctionCode.EXPORT_DATA },
        component: ExportDataComponent,
      },
      {
        path: 'note',
        canActivateChild: [RoleGuardService],
        data: { code: FunctionCode.NOTE },
        component: NoteComponent,
      },
      {
        path: 'image-manager',
        component: UploadImageNewComponent,
        canActivate: [RoleGuardService],
        data: { code: FunctionCode.IMAGE_MANAGER },
      },
    ],
  },
  {
    path: 'title-relationship-group',
    canActivateChild: [RoleGuardService],
    data: { code: FunctionCode.KPI_RM_GROUP_RELATIONSHIP },
    children: [
      {
        path: '',
        component: ListTitleRelationshipGroup,
      },
      {
        path: 'create',
        component: TitleRelationshipGroupCreate,
        data: {
          type: ScreenType.Create,
        },
      },
      {
        path: 'edit/:id/:bizline',
        component: TitleRelationshipGroupCreate,
        data: {
          type: ScreenType.Update,
        },
      },
      {
        path: 'detail/:id/:bizline',
        component: TitleRelationshipGroupCreate,
        data: {
          type: ScreenType.Detail,
        },
      },
    ],
  },
  {
    path: 'management-news',
    canActivateChild: [RoleGuardService],
    data: { code: FunctionCode.NEWS },
    children: [
      {
        path: '',
        component: NewsListComponent,
      },
      {
        path: 'create',
        component: NewsCreateComponent,
        data: {
          type: ScreenType.Create,
        },
      },{
        path: 'update/:id',
        component: NewsCreateComponent,
        data: {
          type: ScreenType.Update,
        },
      },{
        path: 'detail/:id',
        component: NewsCreateComponent,
        data: {
          type: ScreenType.Detail,
        },
      },
    ],
  },
  {
    path: 'frequency-of-using-function',
    canActivateChild: [RoleGuardService],
    component: FrequencyOfUsingFunctionComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SystemRoutingModule {}
