import { FalseResolve, TrueResolve } from './boolean.resolver';

export { FalseResolve, TrueResolve } from './boolean.resolver';

export const RESOLVERS = [
  FalseResolve,
  TrueResolve
];