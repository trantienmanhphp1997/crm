import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlocksCategoryComponent } from './blocks-category.component';

describe('BlocksCategoryComponent', () => {
  let component: BlocksCategoryComponent;
  let fixture: ComponentFixture<BlocksCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BlocksCategoryComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlocksCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
