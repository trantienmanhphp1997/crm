import { global } from '@angular/compiler/src/util';
import { Component, OnInit, Injector } from '@angular/core';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { ExportExcelService } from 'src/app/core/services/export-excel.service';
import { FunctionCode, maxInt32 } from 'src/app/core/utils/common-constants';
import { BaseComponent } from 'src/app/core/components/base.component';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { CategoryService } from '../../services/category.service';
import _ from 'lodash';

declare const $: any;

@Component({
  selector: 'app-blocks-category',
  templateUrl: './blocks-category.component.html',
  styleUrls: ['./blocks-category.component.scss'],
})
export class BlocksCategoryComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listData: any;
  textFilter = '';
  searchForm = this.fb.group({
    code: [''],
    name: [''],
  });
  paramSearch = {
    size: global.userConfig.pageSize,
    page: 0,
    name: '',
    code: '',
  };
  pageable: Pageable;
  tooltip: any;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private exportExcelService: ExportExcelService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_BLOCK}`);
  }

  ngOnInit(): void {}

  search(isSearch: boolean) {
    if (this.tooltip && this.tooltip.isOpen()) {
      this.tooltip.close();
    }
    const searchValue = this.searchForm.getRawValue();
    this.paramSearch.code = searchValue.code.length > 0 ? searchValue.code.trim() : '';
    this.paramSearch.name = searchValue.name.length > 0 ? searchValue.name.trim() : '';
    if (isSearch) {
      this.paramSearch.page = 0;
    }
    this.isLoading = true;
    this.categoryService.searchBlocksCategory(this.paramSearch).subscribe(
      (result) => {
        if (result) {
          if (result.content.length === 0 && result.number > 0) {
            this.paramSearch.page -= 1;
            this.search(false);
            return;
          }
          this.listData = result.content || [];
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: this.paramSearch.size,
          };
          setTimeout(() => {
            this.filter();
          }, 10);
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    this.paramSearch.page = pageInfo.offset;
    this.search(false);
  }

  openSearch(tooltip) {
    this.tooltip = tooltip;
    this.searchForm.controls.name.setValue(this.paramSearch.name);
    this.searchForm.controls.code.setValue(this.paramSearch.code);
  }

  create() {
    this.router.navigate([this.router.url, 'create']);
  }

  update(item) {
    this.router.navigate([this.router.url, 'update', item.id]);
  }

  delete(item) {
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.result
      .then((res) => {
        if (res) {
          this.isLoading = true;
          this.categoryService.deleteBlocksCategory(item.id).subscribe(
            () => {
              this.search(false);
              this.messageService.success(this.notificationMessage.success);
              this.isLoading = false;
            },
            (e) => {
              this.isLoading = false;
              if (e && e.error && e.error.code) {
                this.messageService.warn(this.notificationMessage[e.error.code]);
              } else {
                this.messageService.error(this.notificationMessage.error);
              }
            }
          );
        }
      })
      .catch(() => {});
  }

  refeshList() {
    const params = JSON.parse(JSON.stringify(this.paramSearch));
    params.page = 0;
    params.size = this.listData ? (this.paramSearch.page + 1) * this.paramSearch.size : this.paramSearch.size;
    this.categoryService.searchBlocksCategory(params).subscribe((result) => {
      if (result && result.content) {
        this.listData = result.content;
      }
    });
  }

  filter() {
    const value = this.textFilter.trim().toLowerCase();
    $('.block__view .datatable-row-wrapper').filter(function () {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
    });
  }

  exportFile() {
    this.translate.get('fields').subscribe(
      (fields) => {
        const fieldLabels = fields;
        let data = [];
        let obj: any = {};
        const params = JSON.parse(JSON.stringify(this.paramSearch));
        params.page = 0;
        params.size = maxInt32;
        this.categoryService.searchBlocksCategory(params).subscribe((result) => {
          if (result && result.content) {
            result.content.forEach((item) => {
              obj = {};
              obj[fieldLabels.blockCode] = item.code;
              obj[fieldLabels.blockName] = item.name;
              data.push(obj);
            });
            this.exportExcelService.exportAsExcelFile(data, 'blocks_category');
          } else {
            this.exportExcelService.exportAsExcelFile(data, 'blocks_category');
          }
        });
      },
      () => {}
    );
  }

  onActive($event) {
    if ($event.type === 'dblclick') {
      if (_.get(this.objFunction, 'update') === true) {
        var item = _.get($event, 'row');
        this.update(item);
      }
    }
  }
}
