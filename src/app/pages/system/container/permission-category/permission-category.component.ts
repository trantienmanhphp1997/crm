import { forkJoin } from 'rxjs';
import { CustomKeycloakService } from './../../services/custom-keycloak.service';
import { Component, OnInit, ViewChild, AfterViewInit, Injector } from '@angular/core';
import { ColumnMode, DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { global } from '@angular/compiler/src/util';
import { FunctionCode, maxInt32 } from 'src/app/core/utils/common-constants';
import { ExportExcelService } from 'src/app/core/services/export-excel.service';
import { CategoryService } from '../../services/category.service';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';

@Component({
  selector: 'app-permission-category',
  templateUrl: './permission-category.component.html',
  styleUrls: ['./permission-category.component.scss'],
})
export class PermissionCategoryComponent extends BaseComponent implements OnInit, AfterViewInit {
  isLoading = false;
  listApp = [];
  listData = [];
  listDataTemp = [];
  listResource = [];
  listPermission = [];
  textSearch = '';
  ColumnMode = ColumnMode;
  SelectionType = SelectionType;
  notificationMessage: any;
  idApp = '';
  selected = [];
  @ViewChild(DatatableComponent) public table: DatatableComponent;

  constructor(
    injector: Injector,
    private customKeycloakService: CustomKeycloakService,
    private exportExcelService: ExportExcelService,
    private categoryService: CategoryService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_PERMISSION}`);
  }

  ngOnInit(): void {
    this.isLoading = true;
    forkJoin([
      this.categoryService.searchResourceCategory({ page: 0, size: maxInt32 }),
      this.categoryService.searchAppCategory({ page: 0, size: maxInt32 }),
      this.categoryService.getPermission(),
    ]).subscribe(
      ([listResource, listApp, listPermission]) => {
        this.listApp = listApp?.content || [];
        this.listApp.unshift({ id: '', name: this.fields?.all });
        this.idApp = this.listApp[0]?.id;
        listPermission = listPermission || [];
        listPermission?.forEach((permission) => {
          permission.treeStatus = 'disabled';
          permission.parentId = permission?.resources[0];
          if (permission.parentId) {
            const itemResource = listResource?.content?.find((resource) => resource.id === permission.parentId);
            if (itemResource) {
              permission.appId = itemResource?.type;
              itemResource.appId = itemResource?.type;
              if (itemResource?.parentId) {
                delete itemResource.parentId;
              }
              itemResource.treeStatus = 'expanded';
              this.listDataTemp.push(itemResource);
            }
          }
        });
        this.listDataTemp = _.uniqBy(this.listDataTemp, 'id');
        this.listDataTemp = [...this.listDataTemp, ...listPermission];
        this.listDataTemp?.sort((a, b) => {
          if (a.name.toLowerCase() < b.name.toLowerCase()) {
            return -1;
          }
          if (a.name.toLowerCase() > b.name.toLowerCase()) {
            return 1;
          }
          return 0;
        });
        this.listData = [...this.listDataTemp];
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngAfterViewInit() {
    this.table.messages = global.messageTable;
  }

  filter() {
    const val = this.textSearch.trim().toLowerCase();
    // filter our data
    const result = this.listDataTemp?.filter((item) => {
      return item.name.toLowerCase().indexOf(val) !== -1 || !val;
    });
    const parents = [];
    result.forEach((item) => {
      if (item.parentId) {
        parents.push(this.listDataTemp?.find((i) => i.id === item.parentId));
      }
    });
    const resultAll = [...result, ...parents];
    resultAll.sort((a, b) => {
      if (a.name.toLowerCase() < b.name.toLowerCase()) {
        return -1;
      }
      if (a.name.toLowerCase() > b.name.toLowerCase()) {
        return 1;
      }
      return 0;
    });

    // update the rows
    this.listData = [...new Set(resultAll)];
  }

  findParent(item: any, listAll: any[], listResult: any[]) {
    if (item.parentId) {
      const itemOld = listAll.find((obj) => {
        return obj.id === item.parentId;
      });
      if (itemOld) {
        listResult.push(itemOld);
        this.findParent(itemOld, listAll, listResult);
      }
    }
    return listResult;
  }

  changeApp(event) {
    this.filter();
  }

  create() {
    let resourceId = '';
    if (this.selected.length > 0 && !this.selected[0].parentName) {
      resourceId = this.selected[0].id;
    }
    this.router.navigate([this.router.url, 'create'], {
      state: { resourceId: resourceId, appId: this.idApp },
    });
  }

  edit(item) {
    this.router.navigateByUrl(`${this.router.url}/update/${item.id}/${item.type}`);
    // this.router.navigate([this.router.url, 'update', item.id]);
  }

  delete(item) {
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.result
      .then((res) => {
        if (res) {
          this.isLoading = true;
          this.categoryService.getPermissionById(item.id, item.type).subscribe(
            (itemOld) => {
              if (itemOld) {
                this.customKeycloakService.deletePermission(item.id).subscribe(
                  () => {
                    this.messageService.success(this.notificationMessage.success);
                    this.listData = this.listData.filter((itemOld) => {
                      return item.id !== itemOld.id;
                    });
                    if (
                      this.listData.filter((itemOld) => {
                        return itemOld.parentId === item.parentId;
                      }).length === 0
                    ) {
                      this.listData = this.listData.filter((itemOld) => {
                        return item.parentId !== itemOld.id;
                      });
                    }
                    this.listDataTemp = this.listDataTemp.filter((itemOld) => {
                      return item.id !== itemOld.id;
                    });
                    if (
                      this.listDataTemp.filter((itemOld) => {
                        return itemOld.parentId === item.parentId;
                      }).length === 0
                    ) {
                      this.listDataTemp = this.listDataTemp.filter((itemOld) => {
                        return item.parentId !== itemOld.id;
                      });
                    }
                    this.listData = [...this.listData];
                    // this.search();
                    this.isLoading = false;
                  },
                  () => {
                    this.isLoading = false;
                    this.messageService.error(this.notificationMessage.error);
                  }
                );
              } else {
                this.messageService.warn(this.notificationMessage.notExistObject);
                this.isLoading = false;
              }
            },
            () => {
              this.listData = this.listData.filter((itemOld) => {
                return item.id !== itemOld.id;
              });
              this.listData = [...this.listData];
              this.isLoading = false;
            }
          );
        }
      })
      .catch(() => {});
  }

  generateAppName(type) {
    if (this.listApp && this.listApp.length > 0) {
      for (const app of this.listApp) {
        if (type === app.id) {
          return app.name;
        }
      }
    }
    return '';
  }

  onTreeAction(event: any) {
    const row = event.row;
    if (row.treeStatus === 'collapsed') {
      row.treeStatus = 'expanded';
    } else {
      row.treeStatus = 'collapsed';
    }
    this.listData = [...this.listData];
  }

  export() {
    const fieldLabels = this.fields;
    let data = [],
      obj = {};
    for (const item of this.listData) {
      if (!item.parentId) {
        continue;
      }
      obj = {};
      obj[fieldLabels.app] = this.generateAppName(item.appId);
      obj[fieldLabels.permissionName] = item.name;
      obj[fieldLabels.resourceCode] = item.parentCode;
      obj[fieldLabels.resourceName] = item.parentName;
      data.push(obj);
    }
    this.exportExcelService.exportAsExcelFile(data, 'permission');
  }

  onActive($event) {
    if ($event.type === 'dblclick') {
      var item = _.get($event, 'row');
      if (_.get(this.objFunction, 'update') === true && item?.parentId) {
        this.edit(item);
      }
    }
  }
}
