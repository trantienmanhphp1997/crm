import { Component, OnInit, AfterViewInit, ViewChild, Injector } from '@angular/core';
import { forkJoin } from 'rxjs';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { ExportExcelService } from 'src/app/core/services/export-excel.service';
import { FunctionCode, maxInt32 } from 'src/app/core/utils/common-constants';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { CategoryService } from '../../services/category.service';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import { BaseComponent } from 'src/app/core/components/base.component';
import _ from 'lodash';

@Component({
  selector: 'app-resources-category',
  templateUrl: './resources-category.component.html',
  styleUrls: ['./resources-category.component.scss'],
})
export class ResourcesCategoryComponent extends BaseComponent implements OnInit, AfterViewInit {
  isLoading = false;
  listData: any;
  temp = [];
  textFilter = '';
  searchForm = this.fb.group({
    code: [''],
    name: [''],
    type: [''],
  });
  paramSearch = {
    size: maxInt32,
    page: 0,
    name: '',
    code: '',
    type: '',
  };
  tooltip: any;
  listApp: any;
  listResourceParent: any;
  pageable: Pageable;
  ColumnMode = ColumnMode;
  @ViewChild(DatatableComponent) public table: DatatableComponent;

  constructor(
    injector: Injector,
    private exportExcelService: ExportExcelService,
    private categoryService: CategoryService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_FUNCTION}`);
  }

  ngOnInit(): void {
    this.isLoading = true;
    forkJoin([
      this.categoryService.searchResourceCategory(this.paramSearch),
      this.categoryService.searchAppCategory({ page: 0, size: maxInt32 }),
    ]).subscribe(
      ([listData, listApp]) => {
        this.isLoading = false;
        for (const item of listData.content) {
          item.treeStatus = 'expanded';
        }
        listData.content.sort((a, b) => {
          if (a.code.toLowerCase() < b.code.toLowerCase()) {
            return -1;
          }
          if (a.code.toLowerCase() > b.code.toLowerCase()) {
            return 1;
          }
          return 0;
        });
        this.temp = listData.content || [];
        this.listData = listData.content || [];

        this.pageable = {
          totalElements: listData.totalElements,
          totalPages: listData.totalPages,
          currentPage: listData.number,
        };
        this.listApp = listApp.content;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngAfterViewInit() {}

  search() {
    this.updateFilter();
  }

  updateFilter() {
    const val = this.textFilter.trim().toLowerCase();
    this.listData = [];

    // filter our data
    const temp = this.temp.filter((d) => {
      return d.code.toLowerCase().indexOf(val) !== -1 || d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });
    let listResult = [...temp];
    for (const item of temp) {
      listResult = [...listResult, ...this.findParentBranch(item, this.temp, [])];
    }
    listResult.sort((a, b) => {
      if (a.code.toLowerCase() < b.code.toLowerCase()) {
        return -1;
      }
      if (a.code.toLowerCase() > b.code.toLowerCase()) {
        return 1;
      }
      return 0;
    });

    listResult = [...new Set(listResult)];
    // update the rows
    this.listData = listResult;
  }

  findParentBranch(item: any, listAll: any[], listResult: any[]) {
    if (item.parentCode) {
      const itemOld = listAll.find((obj) => {
        return obj.code === item.parentCode;
      });
      if (itemOld) {
        listResult.push(itemOld);
        this.findParentBranch(itemOld, listAll, listResult);
      }
    }
    return listResult;
  }

  create() {
    this.router.navigate([this.router.url, 'create']);
  }

  update(item) {
    this.router.navigate([this.router.url, 'update', item.id]);
  }

  delete(item) {
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.result
      .then((res) => {
        if (res) {
          this.isLoading = true;
          this.categoryService.deleteResourceCategory(item.id).subscribe(
            () => {
              this.listData = this.listData.filter((itemOld) => {
                return itemOld.id !== item.id;
              });
              this.messageService.success(this.notificationMessage.success);
              this.isLoading = false;
            },
            (e) => {
              if (e?.error) {
                this.messageService.warn(e?.error?.description);
              } else {
                this.messageService.error(this.notificationMessage.error);
              }
              this.isLoading = false;
            }
          );
        }
      })
      .catch(() => {});
  }

  getAllResource() {
    const params = {
      page: 0,
      size: maxInt32,
    };
    this.categoryService.searchResourceCategory(params).subscribe((result) => {
      if (result && result.content) {
        this.listResourceParent = result.content;
      }
    });
  }
  generateAppName(type) {
    if (this.listApp && this.listApp.length > 0) {
      for (const app of this.listApp) {
        if (type === app.id) {
          return app.name;
        }
      }
    }
    return '';
  }

  onTreeAction(event: any) {
    const row = event.row;
    if (row.treeStatus === 'collapsed') {
      row.treeStatus = 'expanded';
    } else {
      row.treeStatus = 'collapsed';
    }
    this.listData = [...this.listData];
  }

  exportFile() {
    this.translate.get('fields').subscribe(
      (fields) => {
        const fieldLabels = fields;
        const data = [];
        let obj: any = {};
        const params = JSON.parse(JSON.stringify(this.paramSearch));
        params.page = 0;
        params.size = maxInt32;
        this.categoryService.searchResourceCategory(params).subscribe((result) => {
          if (result && result.content) {
            result.content.forEach((item) => {
              obj = {};
              obj[fieldLabels.resourceCode] = item.code;
              obj[fieldLabels.resourceName] = item.name;
              obj[fieldLabels.app] = this.generateAppName(item.type);
              data.push(obj);
            });
            this.exportExcelService.exportAsExcelFile(data, 'resources_category');
          } else {
            this.exportExcelService.exportAsExcelFile(data, 'resources_category');
          }
        });
      },
      () => {}
    );
  }

  onActive($event) {
    if ($event.type === 'dblclick') {
      if (_.get(this.objFunction, 'update') === true) {
        var item = _.get($event, 'row');
        this.update(item);
      }
    }
  }
}
