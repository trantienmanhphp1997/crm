import { CustomKeycloakService } from '../../services/custom-keycloak.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ColumnMode, DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { UserService } from '../../services/users.service';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { maxInt32 } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'app-template-config',
  templateUrl: './template-config.component.html',
  styleUrls: ['./template-config.component.scss'],
})
export class TemplateConfigComponent implements OnInit, AfterViewInit {
  limit = 10;
  columnsUser = [{ prop: 'username' }, { prop: 'hrsCode' }, { prop: 'code' }, { prop: 'fullName' }, { prop: 'branch' }];
  columnsRole = [{ prop: 'name' }, { prop: 'description' }];
  listUser = [];
  listRole = [];
  roleSelected = [];
  userSelected: any;

  ColumnMode = ColumnMode;
  SelectionType = SelectionType;
  currentPageLimit = 10;
  currentVisible = 5;
  userSearch = '';
  roleSearch = '';
  paramsUser = {
    page: 0,
    size: this.limit,
    username: '',
  };
  pageableUser: Pageable;
  paramsRole = {
    first: 0,
    max: this.limit,
    search: '',
  };
  pageableRole: Pageable;
  @ViewChild(DatatableComponent) public table: DatatableComponent;

  constructor(private customKeycloakService: CustomKeycloakService, private userService: UserService) {}

  ngOnInit(): void {
    this.searchUser(true);
    this.searchRole();
  }

  ngAfterViewInit() {
    this.table.messages.emptyMessage = 'Khong co du lieu';
  }

  searchUser(isSearch: boolean) {
    this.userSearch = this.userSearch.trim();
    if (isSearch) {
      this.paramsUser.page = 0;
    }
    this.userService.search(this.paramsUser).subscribe(
      (result) => {
        if (result) {
          this.listUser = result.content || [];
          this.pageableUser = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: this.limit,
          };
        }
      },
      () => {}
    );
  }

  searchRole() {
    const params = JSON.parse(JSON.stringify(this.paramsRole));
    params.first = 0;
    params.max = maxInt32;
    // this.customKeycloakService.searchRole(params).subscribe(
    //   (roles) => {
    //     if (roles) {
    //       this.pageableRole = {
    //         totalElements: roles.length,
    //         totalPages: roles.length === 0 ? 0 : Math.ceil(roles.length / this.limit),
    //         currentPage: this.paramsRole.first === 0 ? 0 : Math.floor(this.paramsRole.first / this.limit),
    //         size: this.limit,
    //       };
    //       this.listRole = roles.slice(this.paramsRole.first, this.paramsRole.max);
    //     }
    //   },
    //   () => {}
    // );
  }

  setPage(pageInfo, type) {
    if (type === 'user') {
      this.paramsUser.page = pageInfo.offset;
      this.searchUser(false);
    } else {
      this.paramsRole.first = pageInfo.offset * this.limit;
      this.paramsRole.max = this.paramsRole.first + this.limit;
      this.searchRole();
    }
  }

  save() {}

  isCheckedUser(value) {
    return value === this.userSelected;
  }

  isCheckedRole(value) {
    return (
      this.roleSelected.findIndex((role) => {
        return role === value;
      }) > -1
    );
  }

  chooseUserFn(value) {
    this.userSelected = value;
  }

  onCheckboxRoleFn(value, isChecked) {
    if (isChecked) {
      this.roleSelected.push(value);
    } else {
      this.roleSelected = this.roleSelected.filter((role) => {
        return role !== value;
      });
    }
  }

  fetch1(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data/100k.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }
  fetch2(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data/company.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }
}
