import { global } from '@angular/compiler/src/util';
import { Component, OnInit, Injector } from '@angular/core';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { ExportExcelService } from 'src/app/core/services/export-excel.service';
import { FunctionCode, maxInt32 } from 'src/app/core/utils/common-constants';
import { CategoryService } from '../../services/category.service';
import { BaseComponent } from 'src/app/core/components/base.component';
import _ from 'lodash';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { CommonCategoryCreateComponent } from '../../components/common-category-create/common-category-create.component';
import { CommonCategoryUpdateComponent } from '../../components/common-category-update/common-category-update.component';

@Component({
  selector: 'app-common-category',
  templateUrl: './common-category.component.html',
  styleUrls: ['./common-category.component.scss'],
})
export class CommonCategoryComponent extends BaseComponent implements OnInit {
  textFilter = '';
  paramSearch = {
    size: global.userConfig.pageSize,
    page: 0,
    search: '',
  };
  prevParams: any;
  listDataParent = [];
  listDataChildren = [];
  term = [];
  pageable: Pageable;
  pageableChildren: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig.pageSize,
  };
  selected = [];
  codeParent: string;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private exportExcelService: ExportExcelService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_COMMON}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.search(true);
  }

  search(isSearch: boolean) {
    this.isLoading = true;
    let params: any;
    if (isSearch) {
      this.paramSearch.page = 0;
      this.paramSearch.search = _.trim(this.paramSearch.search);
      params = { ...this.paramSearch };
    } else {
      params = this.prevParams ? { ...this.prevParams, page: this.paramSearch.page } : { ...this.paramSearch };
    }
    this.categoryService.searchTypeCommonCategory(params).subscribe(
      (data) => {
        this.prevParams = params;
        this.listDataParent = data?.content || [];
        if (this.listDataParent.length > 0) {
          this.selected = [this.listDataParent[0]];
          this.codeParent = this.listDataParent[0]?.code;
          this.searchChildren();
        }
        this.pageable = {
          totalElements: data?.totalElements,
          totalPages: data?.totalPages,
          currentPage: data?.number,
          size: this.paramSearch.size,
        };
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.page = pageInfo.offset;
    this.search(false);
  }

  setPageChildren(pageInfo) {
    this.pageableChildren.currentPage = pageInfo.offset;
    this.getDataChildren();
  }

  getDataChildren() {
    const start = this.pageableChildren.currentPage * this.paramSearch.size;
    const end = start + this.paramSearch.size;
    this.listDataChildren = this.term.slice(start, end);
  }

  searchChildren(currentPage?: number) {
    const params = {
      page: 0,
      size: maxInt32,
      commonCategoryCode: this.codeParent,
    };
    this.isLoading = true;
    this.categoryService
      .searchCommonCategoryByType(params)
      .pipe(catchError(() => of(undefined)))
      .subscribe((data) => {
        this.term = data?.content || [];
        const start = (currentPage || 0) * this.paramSearch.size;
        const end = start + this.paramSearch.size;
        this.listDataChildren = this.term.slice(start, end);
        const total = data?.content?.length || 0;
        this.pageableChildren = {
          totalElements: total,
          totalPages: Math.floor(total / this.paramSearch.size),
          currentPage: currentPage || 0,
          size: this.paramSearch.size,
        };
        this.isLoading = false;
      });
  }

  create() {
    const addModal = this.modalService.open(CommonCategoryCreateComponent, {
      scrollable: true,
      windowClass: 'common-category-create',
    });
    addModal.componentInstance.commonCategoryCode = this.codeParent;
    addModal.result
      .then((res) => {
        if (res) {
          this.searchChildren(this.pageableChildren.currentPage);
        }
      })
      .catch(() => {});
  }

  delete(item) {
    this.confirmService
      .confirm()
      .then((res) => {
        if (res) {
          this.isLoading = true;
          this.categoryService.deleteCommonCategory(item.id).subscribe(
            () => {
              this.isLoading = false;
              this.searchChildren(this.pageableChildren.currentPage);
              this.messageService.success(this.notificationMessage.success);
            },
            (e) => {
              if (e?.error) {
                this.messageService.warn(e?.error?.description);
              } else {
                this.messageService.error(this.notificationMessage.error);
              }
              this.isLoading = false;
            }
          );
        }
      })
      .catch(() => {});
  }

  update(item) {
    const updateModal = this.modalService.open(CommonCategoryUpdateComponent, {
      scrollable: true,
      windowClass: 'common-category-update',
    });
    updateModal.componentInstance.data = item;
    updateModal.componentInstance.isUpdate = true;
    updateModal.result
      .then((data) => {
        if (data) {
          this.searchChildren(this.pageableChildren.currentPage);
        }
      })
      .catch(() => {});
  }

  onSelect(event) {
    if (event?.selected[0]?.code !== this.codeParent) {
      this.codeParent = event?.selected[0]?.code;
      this.searchChildren();
    }
  }

  exportFile() {
    let data = [];
    let obj: any = {};
    const params = JSON.parse(JSON.stringify(this.prevParams));
    params.page = 0;
    params.size = maxInt32;
    this.categoryService.searchTypeCommonCategory(params).subscribe((result) => {
      if (result && result.content) {
        result.content.forEach((item) => {
          obj = {};
          obj[this.fields.code] = item.code;
          obj[this.fields.typeCategory] = item.name;
          data.push(obj);
        });
        this.exportExcelService.exportAsExcelFile(data, 'type_common_category');
      } else {
        this.exportExcelService.exportAsExcelFile(data, 'type_common_category');
      }
    });
  }

  exportFileChildren() {
    let data = [];
    let obj: any = {};
    this.term?.forEach((item) => {
      obj = {};
      obj[this.fields.code] = item.code;
      obj[this.fields.name] = item.name;
      obj[this.fields.value] = item.value;
      obj[this.fields.description] = item.description;
      data.push(obj);
    });
    this.exportExcelService.exportAsExcelFile(data, 'type_common_category');
  }
}
