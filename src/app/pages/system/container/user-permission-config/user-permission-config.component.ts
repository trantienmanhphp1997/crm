import { global } from '@angular/compiler/src/util';
import { Component, OnInit, AfterViewInit, Injector } from '@angular/core';
import { ColumnMode, SelectionType } from '@swimlane/ngx-datatable';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { ExportExcelService } from 'src/app/core/services/export-excel.service';
import { FunctionCode, maxInt32 } from 'src/app/core/utils/common-constants';
import { BaseComponent } from 'src/app/core/components/base.component';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { CategoryService } from '../../services/category.service';
import { ChooseBranchesModalComponent } from '../../components/choose-branches-modal/choose-branches-modal.component';
import { UserService } from '../../services/users.service';

@Component({
  selector: 'app-user-permission-config',
  templateUrl: './user-permission-config.component.html',
  styleUrls: ['./user-permission-config.component.scss'],
})
export class UserPermissionConfigComponent extends BaseComponent implements OnInit, AfterViewInit {
  limit = global.userConfig.pageSize;
  listUser = [];
  // listUserTemp = [];
  listPermission = [];
  listPermissionTemp = [];
  listBranch = [];
  listBranchTemp = [];
  listAllBranch = [];
  listBranchOfPer = [];
  paramsUser = {
    page: 0,
    size: this.limit,
    searchAdvanced: '',
  };
  paramsPermission = {
    first: 0,
    max: this.limit,
    search: '',
  };
  paramsBranch = {
    first: 0,
    max: this.limit,
    search: '',
  };
  isSearch = {
    user: false,
    permission: false,
    branch: false,
  };
  pageableUser: Pageable;
  pageablePermission: Pageable;
  pageableBranch: Pageable;
  selectedUser = [];
  selectedPermission = [];
  ColumnMode = ColumnMode;
  SelectionType = SelectionType;
  currentVisible = 5;
  permissionSearch = '';
  userSearch = '';
  branchSearch = '';
  isPermissionAll: boolean;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private exportExcelService: ExportExcelService,
    private userService: UserService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_DATA_DOMAIN}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.categoryService.searchBranches({ page: 0, size: maxInt32 }).subscribe((listBranch) => {
      this.listAllBranch = listBranch?.content || [];
    });
    this.searchUser(true);
  }

  ngAfterViewInit() {}

  searchUser(isSearch?: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.paramsUser.page = 0;
      this.isSearch.user = isSearch;
    }
    this.permissionSearch = '';
    if (this.isSearch.user) {
      this.paramsUser.searchAdvanced = this.userSearch.trim();
    }
    this.paramsPermission.first = 0;
    this.paramsPermission.max = this.limit;
    this.paramsPermission.search = this.permissionSearch;
    this.listBranchTemp = [];
    this.listBranch = [];
    this.listBranchOfPer = [];
    this.pageableBranch = undefined;
    this.pageablePermission = undefined;
    this.userService.searchUser(this.paramsUser).subscribe(
      (users) => {
        this.listUser = users?.content || [];
        this.pageableUser = {
          totalElements: users.totalElements,
          totalPages: users.totalPages,
          currentPage: users.number,
          size: this.limit,
        };
        if (this.listUser?.length === 0) {
          this.selectedUser = [];
          this.selectedPermission = [];
          this.listPermission = [];
          this.listBranch = [];
          this.listBranchTemp = [];
        } else {
          this.selectedUser = [this.listUser[0]];
        }
        this.searchPermission();
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  searchPermission(isSearch?: boolean) {
    this.listBranchTemp = [];
    this.listBranch = [];
    this.listBranchOfPer = [];
    this.pageableBranch = undefined;
    if (this.selectedUser[0]?.username) {
      if (isSearch) {
        this.paramsPermission.first = 0;
        this.paramsPermission.max = this.limit;
        this.isSearch.permission = isSearch;
      }
      this.isLoading = true;
      if (this.isSearch.permission) {
        this.paramsPermission.search = this.permissionSearch.trim();
      }
      const params = {
        username: this.selectedUser[0].username,
        search: this.paramsPermission.search,
      };
      this.categoryService.getPermissionByUser(params).subscribe(
        (permissions) => {
          this.listPermissionTemp = permissions;
          this.listPermission = permissions.slice(this.paramsPermission.first, this.paramsPermission.max);
          this.pageablePermission = {
            totalElements: permissions.length,
            currentPage: this.paramsPermission.first === 0 ? 0 : Math.floor(this.paramsPermission.first / this.limit),
            size: this.limit,
          };
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
    } else {
      this.isLoading = false;
    }
  }

  searchBranch(isSearch?: boolean) {
    if (
      this.selectedUser.length > 0 &&
      this.selectedUser[0].username &&
      this.selectedPermission.length > 0 &&
      this.selectedPermission[0].id
    ) {
      this.isLoading = true;
      if (isSearch) {
        this.paramsBranch.first = 0;
        this.paramsBranch.max = this.limit;
        this.isSearch.branch = isSearch;
      }
      const params = {
        username: this.selectedUser[0].username,
        permissionId: this.selectedPermission[0].id,
      };
      this.listBranch = [];
      this.categoryService.getBranchByUserAndPermission(params).subscribe(
        (listBranch) => {
          listBranch.sort((a, b) => {
            if (a.code.toLowerCase() < b.code.toLowerCase()) {
              return -1;
            }
            if (a.code.toLowerCase() > b.code.toLowerCase()) {
              return 1;
            }
            return 0;
          });
          this.listBranchOfPer = listBranch;
          this.listBranch = this.filterBranch(listBranch).slice(this.paramsBranch.first, this.paramsBranch.max);

          this.pageableBranch = {
            totalElements: this.listBranchTemp.length,
            currentPage: this.paramsBranch.first === 0 ? 0 : Math.floor(this.paramsBranch.first / this.limit),
            size: this.limit,
          };
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
    }
  }

  filterBranch(list) {
    let val = '';
    if (this.isSearch.branch) {
      val = this.branchSearch.trim().toLowerCase();
    }

    // filter our data
    const temp = list.filter((d) => {
      return (
        d.code.toLowerCase().indexOf(val) !== -1 ||
        d.name.toLowerCase().indexOf(val) !== -1 ||
        (d.parentName && d.parentName.toLowerCase().indexOf(val) !== -1)
      );
    });

    this.listBranchTemp = temp;
    return temp;
  }

  setPage(pageInfo, table) {
    if (this.isLoading) {
      return;
    }
    if (table === 'user') {
      this.paramsUser.page = pageInfo.offset;
      this.searchUser();
    } else if (table === 'permission') {
      this.paramsPermission.first = pageInfo.offset * this.limit;
      this.paramsPermission.max = this.paramsPermission.first + this.limit;
      this.searchPermission();
    } else if (table === 'branch') {
      this.paramsBranch.first = pageInfo.offset * this.limit;
      this.paramsBranch.max = this.paramsBranch.first + this.limit;
      this.searchBranch();
    }
  }

  onSelect(table) {
    if (table === 'user') {
      this.searchPermission(true);
      this.listBranch = [];
      this.selectedPermission = [];
    } else {
      this.searchBranch();
    }
  }

  checkAllPermission(isChecked) {
    this.isPermissionAll = isChecked;
    this.selectedPermission = [];
    this.listBranchTemp = [];
    this.listBranch = [];
  }

  isClickSearch(type) {
    if (type === 'user') {
      this.isSearch.user = false;
    } else if (type === 'permission') {
      this.isSearch.permission = false;
    } else if (type === 'branch') {
      this.isSearch.branch = false;
    }
  }

  confirmBranch() {
    if (this.isPermissionAll) {
      const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.componentInstance.message = this.notificationMessage.confirmPermissionAll;
      confirm.result
        .then((confirmed: boolean) => {
          if (confirmed) {
            this.chooseBranch();
          }
        })
        .catch(() => {});
    } else {
      this.chooseBranch();
    }
  }

  chooseBranch() {
    const modal = this.modalService.open(ChooseBranchesModalComponent, { windowClass: 'tree__branches-modal' });
    modal.componentInstance.listBranchOld = this.listBranchOfPer?.map((item) => item.code) || [];
    modal.componentInstance.listBranch = this.listAllBranch;
    modal.result
      .then((res) => {
        if (res) {
          const branchCodes = [];
          res.forEach((branch) => {
            branchCodes.push(branch.code);
          });
          const data: any = {};
          data.branches = branchCodes;
          data.username = this.selectedUser[0].username;
          if (this.isPermissionAll) {
            data.isAllPermission = true;
          } else {
            data.permissionId = this.selectedPermission[0].id;
          }
          this.isLoading = true;
          this.categoryService.addDomainPermissionByUser(data).subscribe(
            () => {
              this.isLoading = false;
              if (this.isPermissionAll) {
                this.selectedPermission = this.listPermission.slice(0, 1);
              }
              this.branchSearch = '';
              this.searchBranch(true);
              this.messageService.success(this.notificationMessage.success);
            },
            () => {
              this.isLoading = false;
              this.messageService.error(this.notificationMessage.error);
            }
          );
        }
      })
      .catch(() => {});
  }

  download() {
    window.open('/assets/template/template_import_phanquyen.xlsx', '_self');
  }

  import() {
    this.router.navigate([this.router.url, 'import'], {
      state: { funciton: 'user-permission', title: 'system.menu.userPermissionConfig' },
    });
  }

  exportUser() {
    const fieldLabels = this.fields;
    const params = JSON.parse(JSON.stringify(this.paramsUser));
    params.page = 0;
    params.size = maxInt32;

    this.categoryService.getUserRoleMapping(params).subscribe((result) => {
      const data = [];
      let obj: any = {};
      result?.content.forEach((item, i) => {
        obj = {};
        obj[fieldLabels.order] = i + 1;
        obj[fieldLabels.username] = item.username;
        obj[fieldLabels.fullName] = item.fullName;
        obj[fieldLabels.rmCode] = item.code;
        obj[fieldLabels.hrsCode] = item.hrsCode;
        data.push(obj);
      });
      if (data.length === 0) {
        this.messageService.warn(this.notificationMessage.noRecord);
        return;
      }
      this.exportExcelService.exportAsExcelFile(data, 'user_category');
    });
  }

  exportPermission() {
    const fieldLabels = this.fields;
    const data = [];
    let count = 1;
    let obj = {};
    for (const item of this.listPermissionTemp) {
      obj = {};
      obj[fieldLabels.order] = count;
      obj[fieldLabels.permissionName] = item.name;
      data.push(obj);
      count += 1;
    }
    this.exportExcelService.exportAsExcelFile(data, 'permissions');
  }

  exportBranch() {
    const fieldLabels = this.fields;
    const data = [];
    let count = 1;
    let obj = {};
    const listData = this.filterBranch(this.listBranchTemp);
    for (const item of listData) {
      obj = {};
      obj[fieldLabels.order] = count;
      obj[fieldLabels.branchCode] = item.code;
      obj[fieldLabels.branchName] = item.name;
      obj[fieldLabels.branchParent] = item.parentName;
      data.push(obj);
      count += 1;
    }
    this.exportExcelService.exportAsExcelFile(data, 'branches');
  }
}
