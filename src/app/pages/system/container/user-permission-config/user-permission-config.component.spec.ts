import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPermissionConfigComponent } from './user-permission-config.component';

describe('UserPermissionConfigComponent', () => {
  let component: UserPermissionConfigComponent;
  let fixture: ComponentFixture<UserPermissionConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPermissionConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPermissionConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
