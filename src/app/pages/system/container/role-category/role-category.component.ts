import { global } from '@angular/compiler/src/util';
import { Component, OnInit, Injector } from '@angular/core';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { ExportExcelService } from 'src/app/core/services/export-excel.service';
import { FunctionCode, maxInt32, rolesDefault } from 'src/app/core/utils/common-constants';
import { BaseComponent } from 'src/app/core/components/base.component';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { CustomKeycloakService } from '../../services/custom-keycloak.service';
import _ from 'lodash';
import { CategoryService } from '../../services/category.service';

declare const $: any;

@Component({
  selector: 'app-role-category',
  templateUrl: './role-category.component.html',
  styleUrls: ['./role-category.component.scss'],
})
export class RoleCategoryComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listData = [];
  listRoleDefault = [];
  paramSearch = {
    size: global?.userConfig?.pageSize,
    page: 0,
    search: '',
  };
  textSearch = '';
  tooltip: any;
  // pageSize = global.userConfig.pageSize;
  pageable: Pageable = {
    totalPages: 0,
    totalElements: 0,
    currentPage: 0,
  };
  messages = global.messageTable;

  constructor(
    injector: Injector,
    private customKeycloakService: CustomKeycloakService,
    private exportExcelService: ExportExcelService,
    private categoryService: CategoryService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_ROLE}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.search(true);
  }

  search(isSearch: boolean) {
    this.isLoading = true;
    this.paramSearch.search = this.paramSearch.search.trim();
    if (isSearch) {
      this.paramSearch.page = 0;
      // this.paramSearch.max = this.pageSize;
      this.textSearch = this.textSearch.trim();
      this.paramSearch.search = this.textSearch;
    }
    const params = JSON.parse(JSON.stringify(this.paramSearch));
    params.first = 0;
    params.max = maxInt32;

    this.categoryService.searchRole(this.paramSearch).subscribe(
      (data) => {
        this.listData = data?._embedded?.keycloakRoleList || [];
        this.pageable = {
          totalElements: data?.page?.totalElements,
          totalPages: data?.page?.totalPages,
          currentPage: data?.page?.number,
          size: global?.userConfig?.pageSize,
        };
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.page = pageInfo.offset;
    // this.paramSearch.max = this.paramSearch.first + this.pageSize;
    this.search(false);
  }

  create() {
    this.router.navigate([this.router.url, 'create']);
  }

  update(item) {
    // this.router.navigate([this.router.url, 'update', item.id]);
    this.router.navigate([this.router.url, 'update', item.name]);
  }

  delete(item) {
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.result
      .then((res) => {
        if (res) {
          this.isLoading = true;
          // this.categoryService.deleteScopeById(item.id).subscribe(
          this.categoryService.deletePolicyById(item.id).subscribe(
            () => {
              this.search(false);
              this.messageService.success(this.notificationMessage.success);
              this.isLoading = false;
            },
            (e) => {
              if (e?.error?.description) {
                this.messageService.warn(e?.error?.description);
              } else {
                this.messageService.error(this.notificationMessage.error);
              }
              this.isLoading = false;
            }
          );
        }
      })
      .catch(() => {});
  }

  exportFile() {
    this.translate.get('fields').subscribe(
      (fields) => {
        const fieldLabels = fields;
        let data = [];
        let obj: any = {};
        const params = JSON.parse(JSON.stringify(this.paramSearch));
        params.first = 0;
        params.max = maxInt32;
        this.categoryService.searchRole(params).subscribe((result) => {
          if (result) {
            result?._embedded?.keycloakRoleList?.forEach((item) => {
              if (rolesDefault.indexOf(item.name) === -1) {
                obj = {};
                obj[fieldLabels.roleCode] = item.name;
                obj[fieldLabels.roleName] = item.description;
                data.push(obj);
              }
            });
            this.exportExcelService.exportAsExcelFile(data, 'role_category');
          } else {
            this.exportExcelService.exportAsExcelFile(data, 'role_category');
          }
        });
      },
      () => {}
    );
  }

  onActive($event) {
    if ($event.type === 'dblclick') {
      if (_.get(this.objFunction, 'update') === true) {
        var item = _.get($event, 'row');
        this.update(item);
      }
    }
  }
}
