import { catchError } from 'rxjs/operators';
import { global } from '@angular/compiler/src/util';
import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { FunctionCode, maxInt32 } from 'src/app/core/utils/common-constants';
import * as _ from 'lodash';
import { CategoryService } from '../../services/category.service';
import { forkJoin, of } from 'rxjs';
import { saveAs } from 'file-saver';
import { FileService } from 'src/app/core/services/file.service';

@Component({
  selector: 'app-permission-for-role',
  templateUrl: './permission-for-role.component.html',
  styleUrls: ['./permission-for-role.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PermissionForRoleComponent extends BaseComponent implements OnInit {
  pageSize = global?.userConfig?.pageSize || 10;
  params = {
    first: 0,
    max: this.pageSize,
    search: '',
  };
  listApp = [];
  listPermission = [];
  listTerm = [];
  listRole = [];
  roleSelected = [];
  pageable: Pageable;
  treePermission = [];
  colsTree = [
    { field: 'name', header: this.fields.name, width: '80%' },
    { field: 'type', header: this.fields.type, width: '20%' },
  ];
  idApp: string;
  roleCodeSelected: string;
  selected = [];
  treeNode = [];
  permissionOfRoleOld = [];
  currPermissionOfRole = [];
  countPermissionOfApp = 0;
  isCheckedAll: boolean;

  constructor(injector: Injector, private categoryService: CategoryService, private fileService: FileService) {
    super(injector);
    this.isLoading = true;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_PERMISSION_ROLE}`);
  }

  ngOnInit(): void {
    const params = {
      page: 0,
      size: maxInt32,
    };
    forkJoin([
      this.categoryService.searchRole(params).pipe(catchError(() => of(undefined))),
      this.categoryService.searchAppCategory({ page: 0, size: maxInt32 }).pipe(catchError(() => of(undefined))),
      this.categoryService.searchResourceCategory({ page: 0, size: maxInt32 }).pipe(catchError(() => of(undefined))),
      this.categoryService.getPermission().pipe(catchError(() => of(undefined))),
    ]).subscribe(
      ([listRole, listApp, listResource, listPermission]) => {
        this.listTerm = listRole?._embedded?.keycloakRoleList || [];
        const listData = [];
        listApp?.content?.forEach((item, i) => {
          const itemNew = {
            id: `${i + 1}`,
            name: item.name,
            originId: item.id,
          };
          listData.push({ data: itemNew });
          this.listApp.push(itemNew);
        });
        listPermission?.forEach((item) => {
          if (item.resources[0]) {
            const itemNew = {
              id: item.id,
              name: item.name,
              permissionType: item.type,
              parentId: item.resources[0],
              type: 'P',
              checked: false,
            };
            listData.push({ data: itemNew });
            this.listPermission.push(itemNew);
          }
        });
        listResource?.content?.forEach((item) => {
          const itemNew = {
            id: item.id,
            name: item.name,
            checked: false,
            type: 'F',
            parentId: item.parentId ? item.parentId : listData?.find((i) => i.data.originId === item.type)?.data.id,
          };
          listData.push({ data: itemNew });
        });
        this.treePermission = listData?.filter((item) => !item.data.parentId && item.data.originId);
        this.mapTreePermission(listData, this.treePermission);
        this.searchRole(true);
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  searchRole(isSearch?: boolean) {
    this.isLoading = true;
    this.params.search = this.params.search.trim();
    if (isSearch) {
      this.params.first = 0;
      this.params.max = this.pageSize;
      this.params.search = this.params.search.trim();
    }
    const listAll = _.filter(
      this.listTerm,
      (item) =>
        item?.name?.toLowerCase().includes(this.params.search?.toLowerCase()) ||
        item?.description?.toLowerCase().includes(this.params.search?.toLowerCase()) ||
        !this.params.search
    );
    const totalCount = _.size(listAll) || 0;
    this.listRole = _.slice(listAll, this.params.first, this.params.max) || [];
    this.pageable = {
      totalElements: totalCount,
      totalPages: Math.floor(totalCount / this.pageSize),
      currentPage: Math.floor(this.params.first / this.pageSize),
      size: this.pageSize,
    };
    this.checkAll({ checked: false });
    if (this.listRole.length > 0) {
      this.roleSelected = _.slice(this.listRole, 0, 1);
      this.roleCodeSelected = this.roleSelected[0].name;
      this.getPermissionByRole();
    } else {
      this.roleSelected = [];
      this.roleCodeSelected = '';
      this.isLoading = false;
      this.selected = [];
    }
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.first = pageInfo.offset * this.pageSize;
    this.params.max = this.params.first + this.pageSize;
    this.searchRole(false);
  }

  getPermissionByRole() {
    this.isLoading = true;
    this.categoryService
      .getPermissionByRole(this.roleCodeSelected)
      .pipe(catchError(() => of(undefined)))
      .subscribe((data) => {
        this.permissionOfRoleOld = data;
        this.mapSelectedTreeNode();
        this.isLoading = false;
      });
  }

  mapSelectedTreeNode() {
    const listPermission = [];
    this.selected = [];
    this.listPermission?.forEach((item) => {
      const itemTree = _.get(this.treeNode, item.path);
      if (itemTree && this.permissionOfRoleOld?.includes(itemTree?.data.id)) {
        listPermission.push(item);
        this.selected.push(itemTree);
        itemTree.data.checked = true;
        let path = item.path;
        while (true) {
          if (!path.includes('.')) {
            const itemParent = _.get(this.treeNode, path);
            itemParent.data.checked = true;
            break;
          }
          path = path.replace(/\.[^\.]+$/, '');
          const itemParent = _.get(this.treeNode, path);
          itemParent.data.checked = true;
        }
      }
    });
    this.currPermissionOfRole = _.filter(
      this.permissionOfRoleOld,
      (value) => _.findIndex(listPermission, (i) => i.id === value) === -1
    );
  }

  onSelect({ selected }) {
    if (selected[0].name !== this.roleCodeSelected) {
      this.checkAll({ checked: false });
      this.roleCodeSelected = selected[0].name;
      this.getPermissionByRole();
    }
  }

  save() {
    this.isLoading = true;
    const data = [];
    this.selected?.forEach((item) => {
      if (item.data.permissionType) {
        data.push({ id: item.data.id, type: item.data.permissionType });
      }
    });
    this.currPermissionOfRole.forEach((value) => {
      const item = _.find(this.listPermission, (i) => i.id === value);
      data.push({ id: item.id, type: item.permissionType });
    });
    this.categoryService.addPermissionForRole({ roleCode: this.roleCodeSelected, permissions: data }).subscribe(
      () => {
        this.isLoading = false;
        this.messageService.success(this.notificationMessage.success);
        this.permissionOfRoleOld = _.map(data, (item) => item.id);
      },
      (e) => {
        this.messageService.error(e?.error?.description ? e?.error?.description : this.notificationMessage.error);
        this.isLoading = false;
      }
    );
  }

  changeApp(event) {
    this.countPermissionOfApp = _.size(_.filter(this.listPermission, (item) => `${item.index + 1}` === event.value));
    const result = _.find(this.treePermission, (item) => item.data.id === event.value);
    const tree = _.get(result, 'children', []);
    this.treeNode = [...tree];
    this.checkAll({ checked: false });
    this.mapSelectedTreeNode();
  }

  searchPathTree(nodeId, parent) {
    const stack = [[parent, []]];
    while (stack.length) {
      const [node, path] = stack.pop();
      if (node.data.id === nodeId) {
        return path;
      }
      if (node.children) {
        stack.push(...node.children.map((node, i) => [node, [...path, i]]));
      }
    }
  }

  mapTreePermission(list, listParent) {
    for (const item of listParent) {
      const listChildren = list?.filter((i) => i.data.parentId === item.data.id);
      if (listChildren.length > 0) {
        item.children = listChildren;
        this.mapTreePermission(list, item.children);
      }
    }
    this.mapPermission();
  }

  mapPermission() {
    this.listPermission?.forEach((permission, i) => {
      for (let index = 0; index < this.treePermission.length; index++) {
        const item = this.treePermission[index];
        const path = this.searchPathTree(permission.id, item);
        if (path) {
          let strPath = '';
          path?.forEach((value, n) => {
            strPath += n > 0 ? `.children[${value}]` : `[${value}]`;
            const itemChild = _.get(item, `children${strPath}`);
            itemChild.path = strPath;
          });
          this.listPermission[i].path = strPath;
          this.listPermission[i].index = index;
          break;
        }
      }
    });
  }

  checkAll(e) {
    this.selected = [];
    this.treeNode.forEach((item) => {
      item.data.checked = e.checked;
      this.onChangeCheckedChildren(item, e.checked);
    });
  }

  onChangeCheckbox(event, data) {
    const item = _.get(data, 'node');
    item.data.checked = event.checked;
    this.onChangeCheckedChildren(item, event.checked);
    this.onChangeCheckedParent(item, event.checked);
  }

  onChangeCheckedParent(item, isChecked: boolean) {
    if (_.has(item, 'data.permissionType')) {
      if (isChecked) {
        this.selected = [...this.selected, ...[_.get(this.treeNode, item.path)]];
      } else {
        _.remove(this.selected, (i) => i.data.id === _.get(item, 'data.id'));
      }
    }
    let path = item.path;
    while (true) {
      if (!path.includes('.')) {
        break;
      }
      path = path.replace(/\.[^\.]+$/, '');
      const itemParent = _.get(this.treeNode, path);
      if (isChecked) {
        itemParent.data.checked = true;
      } else {
        let hasSelected = false;
        const list = _.get(itemParent, 'children', []);
        for (const itemChild of list) {
          if (itemChild.data.checked) {
            hasSelected = true;
            break;
          }
        }
        itemParent.data.checked = hasSelected;
      }
    }
  }

  onChangeCheckedChildren(item, isChecked) {
    if (_.get(item, 'children')) {
      item.children.forEach((itemChild) => {
        const itemNode = _.get(this.treeNode, itemChild.path);
        if (itemNode) {
          itemNode.data.checked = isChecked;
        }
        this.onChangeCheckedChildren(itemNode, isChecked);
      });
    } else {
      if (isChecked && _.has(item, 'data.permissionType')) {
        this.selected = [...this.selected, ...[_.get(this.treeNode, item.path)]];
        this.selected = _.uniqBy(this.selected, 'data.id');
      } else {
        _.remove(this.selected, (i) => i.data.id === _.get(item, 'data.id'));
      }
      this.isCheckedAll = this.countPermissionOfApp > 0 && this.countPermissionOfApp === this.selected.length;
    }
  }

  exportFile() {
    if (_.isEmpty(this.roleCodeSelected)) {
      return;
    }
    this.isLoading = true;
    this.categoryService.exportExcelPermissionsByRole(this.roleCodeSelected).subscribe(
      (fileId) => {
        if (!_.isEmpty(fileId)) {
          this.fileService
            .downloadFile(fileId, `Permission-of-${this.roleCodeSelected}.xlsx`)
            .pipe(catchError((e) => of(false)))
            .subscribe((res) => {
              this.isLoading = false;
              if (!res) {
                this.messageService.error(this.notificationMessage.error);
              }
            });
        } else {
          this.messageService.error(this.notificationMessage?.export_error || '');
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(this.notificationMessage?.export_error || '');
        this.isLoading = false;
      }
    );
  }
}
