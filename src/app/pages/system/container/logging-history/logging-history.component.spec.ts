import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoggingHistoryComponent } from './logging-history.component';

describe('LoggingHistoryComponent', () => {
  let component: LoggingHistoryComponent;
  let fixture: ComponentFixture<LoggingHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoggingHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoggingHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
