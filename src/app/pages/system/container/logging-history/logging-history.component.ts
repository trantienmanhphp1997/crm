import { forkJoin } from 'rxjs';
import { cleanDataForm } from 'src/app/core/utils/function';
import { Component, OnInit, AfterViewInit, Injector, ViewChild } from '@angular/core';
import { ExportExcelService } from 'src/app/core/services/export-excel.service';
import { CategoryService } from '../../services/category.service';
import * as moment from 'moment';
import { FunctionCode, maxInt32 } from 'src/app/core/utils/common-constants';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { global } from '@angular/compiler/src/util';
import { DatePipe } from '@angular/common';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';

declare const $: any;

@Component({
  selector: 'app-logging-history',
  templateUrl: './logging-history.component.html',
  styleUrls: ['./logging-history.component.scss'],
  providers: [DatePipe],
})
export class LoggingHistoryComponent extends BaseComponent implements OnInit, AfterViewInit {
  textFilter = '';
  paramSearch = {
    size: global.userConfig.pageSize,
    page: 0,
    objectName: '',
    scope: '',
    startDate: '',
    endDate: '',
    username: '',
  };
  searchForm = this.fb.group({
    objectName: [''],
    scope: [''],
    startDate: [new Date()],
    endDate: [new Date()],
    username: [''],
  });
  listData: any;
  isLoading = false;
  @ViewChild('tooltipSearch') tooltip: any;
  pageable: Pageable;
  typeMethod: any;
  listResource: any;
  listTypeMethod = [];
  maxDate = new Date();
  maxStartDate = this.maxDate;
  minStartDate = moment().add('days', -30).toDate();
  @ViewChild('elForm') elForm: any;
  outsideClickListener: any;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private exportExcelService: ExportExcelService,
    private datePipe: DatePipe
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_LOG}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.checkFormSearch();
    this.paramSearch.startDate = moment(this.paramSearch.startDate).format('YYYY-MM-DD');
    this.paramSearch.endDate = moment(this.paramSearch.endDate).format('YYYY-MM-DD');
    forkJoin([
      this.categoryService.searchScope({ page: 0, size: maxInt32 }),
      this.categoryService.searchResourceCategory({ page: 0, size: maxInt32 }),
    ]).subscribe(
      ([listScope, listResource]) => {
        this.listResource = listResource?.content || [];
        this.listTypeMethod = listScope?._embedded?.keycloakScopeList || [];
        this.listData = [];
        this.search(true);
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngAfterViewInit() {
    this.searchForm.controls.endDate.valueChanges.subscribe((value) => {
      if (!_.isNull(value)) {
        this.maxStartDate = value;
      } else {
        this.maxStartDate = this.maxDate;
      }
      this.minStartDate = moment(this.maxStartDate).add('days', -30).toDate();
    });
  }

  getManipulation() {
    const params = {
      page: 0,
      size: maxInt32,
    };
    this.categoryService.searchScope(params).subscribe((result) => {
      this.listTypeMethod = result?._embedded?.keycloakScopeList || [];
    });
  }

  search(isSearch?: boolean) {
    if (this.tooltip && this.tooltip.isOpen()) {
      this.tooltip.close();
    }
    if (!this.checkFormSearch()) {
      this.listData = [];
      this.pageable = {
        totalPages: 0,
        totalElements: 0,
        currentPage: 0,
      };
      return;
    }
    if (isSearch) {
      this.paramSearch.page = 0;
      this.paramSearch.size = global.userConfig.pageSize;
    }
    this.isLoading = true;
    const params = JSON.parse(JSON.stringify(this.paramSearch));
    params.startDate = moment(this.paramSearch.startDate).format('YYYY-MM-DD');
    params.endDate = moment(this.paramSearch.endDate).format('YYYY-MM-DD');
    this.categoryService.searchLogHistory(params).subscribe(
      (result) => {
        if (result) {
          if (result.content.length === 0 && result.number > 0) {
            this.paramSearch.page -= 1;
            this.search();
            return;
          }
          this.listData = [];
          for (const item of result.content) {
            item.objectName = this.listResource?.find((i) => i.code === item.objectName)?.name;
            item.scope = this.listTypeMethod?.find((i) => i.name === item.scope)?.displayName;
            this.listData.push(item);
          }
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: this.paramSearch.size,
          };
        }
        setTimeout(() => {
          this.filter();
        }, 10);
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    this.paramSearch.page = pageInfo.offset;
    this.search();
  }

  checkFormSearch() {
    const searchValue = cleanDataForm(this.searchForm);
    this.paramSearch.objectName =
      searchValue.objectName === null || searchValue.objectName === undefined ? '' : searchValue.objectName;
    this.paramSearch.scope =
      searchValue.scope === null || searchValue.objectName === undefined ? '' : searchValue.scope;
    this.paramSearch.username = searchValue.username;
    this.paramSearch.startDate = searchValue.startDate;
    this.paramSearch.endDate = searchValue.endDate;
    if (_.isNull(searchValue.startDate) || _.isNull(searchValue.endDate)) {
      this.messageService.warn(this.notificationMessage.noMoreThan30Days);
      return false;
    }
    return true;
  }

  openSearch() {
    if (!this.tooltip?.isOpen()) {
      this.tooltip.open();
    } else {
      return;
    }
    this.searchForm.controls.objectName.setValue(this.paramSearch.objectName);
    this.searchForm.controls.scope.setValue(this.paramSearch.scope);
    this.searchForm.controls.username.setValue(this.paramSearch.username);
    this.searchForm.controls.startDate.setValue(
      this.paramSearch.startDate ? new Date(this.paramSearch.startDate) : null
    );
    this.searchForm.controls.endDate.setValue(this.paramSearch.endDate ? new Date(this.paramSearch.endDate) : null);
    const timer = setTimeout(() => {
      this.outsideClickListener = (event) => {
        if (this.isOutsideClicked(event) && this.tooltip?.isOpen()) {
          this.tooltip.close();
        }
      };
      document.addEventListener('click', this.outsideClickListener);
      clearTimeout(timer);
    }, 200);
  }

  generateManipulationName(type) {
    return this.listTypeMethod?.find((i) => i.name === type)?.displayName;
  }

  exportFile() {
    this.translate.get('fields').subscribe(
      (fields) => {
        const fieldLabels = fields;
        const data = [];
        let obj: any = {};
        const params = JSON.parse(JSON.stringify(this.paramSearch));
        params.page = 0;
        params.size = maxInt32;
        this.categoryService.searchLogHistory(params).subscribe((result) => {
          if (result && result.content) {
            result.content.forEach((item) => {
              obj = {};
              obj[fieldLabels.object] = this.listResource?.find((i) => i.code === item.objectName)?.name;
              obj[fieldLabels.typeChange] = this.generateManipulationName(item?.method);
              obj[fieldLabels.timeChange] = moment(item.dateCreated).format('DD/MM/YYYY');
              obj[fieldLabels.userChange] = item.username;
              data.push(obj);
            });
            this.exportExcelService.exportAsExcelFile(data, 'log_history');
          } else {
            this.exportExcelService.exportAsExcelFile(data, 'log_history');
          }
        });
      },
      () => {}
    );
  }

  formatDateTime(time) {
    return this.datePipe.transform(time, 'dd-MM-yyyy HH:mm') || '';
  }

  filter() {
    const value = this.textFilter.trim().toLowerCase();
    $('.log__history .datatable-row-wrapper').filter(function () {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
    });
  }

  isOutsideClicked(event) {
    const el = document.getElementById('formSearch');
    return !(el?.isSameNode(event.target) || el?.contains(event.target));
  }

  onCloseTooltip() {
    document.removeEventListener('click', this.outsideClickListener);
    this.outsideClickListener = null;
  }
}
