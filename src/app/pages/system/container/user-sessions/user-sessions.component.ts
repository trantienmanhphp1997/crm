import { ExportExcelService } from './../../../../core/services/export-excel.service';
import { Component, OnInit, Injector } from '@angular/core';
import { CustomKeycloakService } from '../../services/custom-keycloak.service';
import * as moment from 'moment';
import { FunctionCode } from 'src/app/core/utils/common-constants';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { global } from '@angular/compiler/src/util';
import { DatePipe } from '@angular/common';
import { BaseComponent } from 'src/app/core/components/base.component';

declare var $: any;

@Component({
  selector: 'app-user-sessions',
  templateUrl: './user-sessions.component.html',
  styleUrls: ['./user-sessions.component.scss'],
  providers: [DatePipe],
})
export class UserSessionsComponent extends BaseComponent implements OnInit {
  listData: any;
  listSessions: any;
  isLoading = false;
  textFilter = '';
  limit = global.userConfig.pageSize;
  params = {
    max: this.limit,
    first: 0,
    username: '',
    ipAddress: '',
    fromLogin: '',
    toLogin: '',
  };
  searchForm = this.fb.group({
    username: [''],
    ipAddress: [''],
    fromLogin: [''],
    toLogin: [''],
  });
  valueSearch = {};
  fromOptions = {
    format: 'DD/MM/YYYY HH:mm',
    sideBySide: true,
  };
  toOptions: any = {
    format: 'DD/MM/YYYY HH:mm',
    sideBySide: true,
  };
  tooltip: any;
  pageable: Pageable;

  constructor(
    injector: Injector,
    private customKeycloakService: CustomKeycloakService,
    private exportExcelService: ExportExcelService,
    private datePipe: DatePipe
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_SESSIONS}`);
  }

  ngOnInit(): void {
    this.search(true);
    this.searchForm.valueChanges.subscribe((result) => {
      const searchValue = this.searchForm.getRawValue();
      if (searchValue.fromLogin) {
        this.toOptions = {
          format: 'DD/MM/YYYY HH:mm',
          sideBySide: true,
          minDate: searchValue.fromLogin,
        };
      }
    });
  }

  search(isSearch: boolean) {
    if (this.tooltip && this.tooltip.isOpen()) {
      this.tooltip.close();
    }
    this.isLoading = true;
    const search = this.searchForm.getRawValue();
    this.params.username = search.username.length > 0 ? search.username.trim() : '';
    this.params.ipAddress = search.ipAddress.length > 0 ? search.ipAddress.trim() : '';
    this.params.fromLogin = search.fromLogin;
    this.params.toLogin = search.toLogin;
    if (isSearch) {
      this.params.first = 0;
      this.params.max = this.limit;
    }
    this.customKeycloakService.getAllSessions().subscribe(
      (result) => {
        if (result) {
          if (result.length === 0 && this.params.first > 0 && Math.floor(this.params.first / this.limit) > 0) {
            this.params.first -= this.limit;
            this.params.max -= this.limit;
            this.search(false);
            return;
          }
          this.listData = result;
          this.filterData(this.listData);
          this.pageable = {
            totalElements: result.length,
            totalPages: result.length === 0 ? 0 : Math.ceil(result.length / this.limit),
            currentPage: this.params.first === 0 ? 0 : Math.floor(this.params.first / this.limit),
            size: this.limit,
          };
          this.isLoading = false;
        }
        setTimeout(() => {
          this.filter();
        }, 10);
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    this.params.first = pageInfo.offset * this.limit;
    this.params.max = this.params.first + this.limit;
    this.search(false);
  }

  openSearch(tooltip) {
    this.tooltip = tooltip;
    this.searchForm.patchValue(this.params);
  }

  revokeUser(item) {
    this.customKeycloakService.revokeBySessionId(item.id).subscribe(
      (result) => {
        this.search(false);
        this.messageService.success(this.notificationMessage.success);
      },
      () => {
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }

  revokeAll() {
    this.customKeycloakService.revokeAll().subscribe(
      (result) => {
        this.messageService.success(this.notificationMessage.success);
      },
      () => {
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }

  filterData(data) {
    let newData = data;
    newData = newData.filter((item) => {
      if (this.params.username && this.params.username.length > 0) {
        return item.username.toLowerCase().includes(this.params.username.toLowerCase());
      } else {
        return item;
      }
    });
    newData = newData.filter((item) => {
      if (this.params.ipAddress && this.params.ipAddress.length > 0) {
        return item.ipAddress.toLowerCase().includes(this.params.ipAddress.toLowerCase());
      } else {
        return item;
      }
    });
    newData = newData.filter((item) => {
      if (this.params.fromLogin) {
        return this.params.fromLogin.valueOf() <= item.start;
      } else {
        return item;
      }
    });
    newData = newData.filter((item) => {
      if (this.params.toLogin) {
        return this.params.toLogin.valueOf() >= item.start;
      } else {
        return item;
      }
    });
    this.listSessions = newData.slice(this.params.first, this.params.max).sort((a, b) => {
      return b.start - a.start;
    });
  }

  formatDateTime(time) {
    return this.datePipe.transform(time, 'dd-MM-yyyy HH:mm') || '';
  }

  filter() {
    const value = this.textFilter.trim().toLowerCase();
    $('.users__session .datatable-row-wrapper').filter(function () {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
    });
  }

  exportFile() {
    let data = [];
    let obj: any = {};
    this.listSessions.forEach((item) => {
      obj = {};
      obj[this.fields.username] = item.username;
      obj[this.fields.ipAddress] = item.ipAddress;
      obj[this.fields.startLogin] = item.start ? moment.unix(item.start / 1000).format('DD/MM/YYYY HH:mm:ss') : '';
      data.push(obj);
    });
    this.exportExcelService.exportAsExcelFile(data, 'sessions');
  }
}
