import { global } from '@angular/compiler/src/util';
import { Component, OnInit, Injector } from '@angular/core';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { ExportExcelService } from 'src/app/core/services/export-excel.service';
import { FunctionCode, maxInt32 } from 'src/app/core/utils/common-constants';
import { BaseComponent } from 'src/app/core/components/base.component';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import _ from 'lodash';
import { CategoryService } from '../../services/category.service';

declare const $: any;

@Component({
  selector: 'app-manipulation-category',
  templateUrl: './manipulation-category.component.html',
  styleUrls: ['./manipulation-category.component.scss'],
})
export class ManipulationCategoryComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listData = [];
  limit = global.userConfig?.pageSize;
  paramSearch = {
    size: this.limit,
    page: 0,
    search: '',
  };
  prevParams: any;
  pageable: Pageable;
  messages = global.messageTable;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private exportExcelService: ExportExcelService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_SCOPES}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.search(true);
  }

  search(isSearch: boolean) {
    if (isSearch) {
      this.paramSearch.page = 0;
      this.paramSearch.search = _.trim(this.paramSearch?.search);
    } else {
      this.prevParams.page = this.paramSearch.page;
    }
    this.isLoading = true;
    this.categoryService.searchScope(isSearch ? this.paramSearch : this.prevParams).subscribe(
      (result) => {
        this.prevParams = isSearch ? this.paramSearch : this.prevParams;
        this.listData = result?._embedded?.keycloakScopeList || [];
        this.pageable = {
          totalElements: result?.page?.totalElements,
          totalPages: result?.page?.totalPages,
          currentPage: result?.page?.number,
          size: this.limit,
        };
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.page = pageInfo.offset;
    this.search(false);
  }

  create() {
    this.router.navigate([this.router.url, 'create']);
  }

  update(item) {
    this.router.navigate([this.router.url, 'update', item.id]);
  }

  delete(item) {
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.result
      .then((res) => {
        if (res) {
          this.isLoading = true;
          this.categoryService.deleteScopeById(item.id).subscribe(
            () => {
              this.search(false);
              this.messageService.success(this.notificationMessage.success);
            },
            (e) => {
              this.isLoading = false;
              if (e?.error?.description) {
                this.messageService.warn(e?.error?.description);
              } else {
                this.messageService.error(this.notificationMessage.error);
              }
            }
          );
        }
      })
      .catch(() => {});
  }

  exportFile() {
    let data = [];
    let obj: any = {};
    const params = { ...this.paramSearch };
    this.categoryService.searchScope(params).subscribe((result) => {
      if (result) {
        result.forEach((item) => {
          obj = {};
          obj[this.fields.manipulationCode] = item.name;
          obj[this.fields.manipulationName] = item.displayName;
          obj[this.fields.description] = item.iconUri;
          data.push(obj);
        });
        this.exportExcelService.exportAsExcelFile(data, 'manipulation_category');
      } else {
        this.exportExcelService.exportAsExcelFile(data, 'manipulation_category');
      }
    });
  }

  onActive($event) {
    if ($event.type === 'dblclick') {
      if (_.get(this.objFunction, 'update') === true) {
        var item = _.get($event, 'row');
        this.update(item);
      }
    }
  }
}
