import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManipulationCategoryComponent } from './manipulation-category.component';

describe('ManipulationCategoryComponent', () => {
  let component: ManipulationCategoryComponent;
  let fixture: ComponentFixture<ManipulationCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManipulationCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManipulationCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
