import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserRoleConfigComponent } from './user-role-config.component';

describe('UserRoleConfigComponent', () => {
  let component: UserRoleConfigComponent;
  let fixture: ComponentFixture<UserRoleConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserRoleConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserRoleConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
