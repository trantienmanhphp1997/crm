import { forkJoin } from 'rxjs';
import { Component, OnInit, ViewChild, AfterViewInit, Injector } from '@angular/core';
import { ColumnMode, DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { FunctionCode, maxInt32, rolesDefault } from 'src/app/core/utils/common-constants';
import { CategoryService } from '../../services/category.service';
import { UserService } from '../../services/users.service';
import { global } from '@angular/compiler/src/util';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { RmProfileService } from 'src/app/core/services/rm-profile.service';

@Component({
  selector: 'app-user-role-config',
  templateUrl: './user-role-config.component.html',
  styleUrls: ['./user-role-config.component.scss'],
})
export class UserRoleConfigComponent extends BaseComponent implements OnInit, AfterViewInit {
  isLoading = false;
  limit = global.userConfig.pageSize;
  columnsUser = [{ prop: 'username' }, { prop: 'hrsCode' }, { prop: 'code' }, { prop: 'fullName' }, { prop: 'branch' }];
  columnsRole = [{ prop: 'name' }, { prop: 'description' }];
  listUser = [];
  listRole = [];
  roleSelected = [];
  userSelected: any;
  selected = [];
  ColumnMode = ColumnMode;
  SelectionType = SelectionType;
  public currentPageLimit: number = global.userConfig.pageSize;
  public currentVisible = 5;
  userSearch = '';
  roleSearch = '';
  paramsUser = {
    page: 0,
    size: this.limit,
    searchAdvanced: '',
    isActive: true,
  };
  pageableUser: Pageable;
  paramsRole = {
    first: 0,
    max: this.limit,
    search: '',
  };
  pageableRole: Pageable;
  listBranches: any;
  dataRole = [];
  @ViewChild('tableUser') public tableUser: DatatableComponent;
  @ViewChild('tableRole') public tableRole: DatatableComponent;

  constructor(
    injector: Injector,
    private userService: UserService,
    private categoryService: CategoryService,
    private rmProfileService: RmProfileService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_USER_ROLE}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.categoryService.searchBranches({ page: 0, size: maxInt32 }).subscribe((dataBranches) => {
      this.listBranches = dataBranches.content;
    });
    forkJoin([
      this.userService.searchUser(this.paramsUser),
      this.categoryService.searchRole({ page: 0, size: maxInt32 }),
    ]).subscribe(
      ([dataUsers, listRoles]) => {
        this.listUser = dataUsers?.content || [];
        this.selected = [this.listUser[0]];
        this.userSelected = this.selected[0];
        this.pageableUser = {
          totalElements: dataUsers.totalElements,
          totalPages: dataUsers.totalPages,
          currentPage: dataUsers.number,
          size: this.limit,
        };
        this.dataRole = listRoles?._embedded?.keycloakRoleList?.filter((role) => {
          return rolesDefault.indexOf(role.name) === -1;
        });
        const totalSize = this.dataRole?.length || 0;
        this.pageableRole = {
          totalElements: totalSize,
          totalPages: Math.ceil(totalSize / this.limit),
          currentPage: Math.floor(this.paramsRole.first / this.limit),
          size: this.limit,
        };
        this.listRole =
          this.dataRole
            ?.filter((item) => {
              return (
                item.name.toLowerCase().includes(this.paramsRole?.search?.toLowerCase()) ||
                item.description.toLowerCase().includes(this.paramsRole?.search?.toLowerCase())
              );
            })
            ?.slice(this.paramsRole.first, this.paramsRole.max) || [];
        this.getRolesOfUser();
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngAfterViewInit() {}

  searchUser(isSearch: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.paramsUser.page = 0;
      this.paramsUser.searchAdvanced = this.userSearch.trim();
    }
    this.selected = [];
    this.userSelected = null;
    this.userService.searchUser(this.paramsUser).subscribe(
      (result) => {
        if (result) {
          this.listUser = result.content || [];
          this.selected = this.listUser?.slice(0, 1) || [];
          this.userSelected = this.selected[0];
          this.pageableUser = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: this.limit,
          };
          this.getRolesOfUser();
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  searchRole(isSearch: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.paramsRole.first = 0;
      this.paramsRole.max = this.limit;
      this.paramsRole.search = this.roleSearch.trim();
    }
    const dataResult = this.dataRole?.filter((item) => {
      return (
        item?.name?.toLowerCase().includes(this.paramsRole.search.toLowerCase()) ||
        item?.description?.toLowerCase().includes(this.paramsRole.search.toLowerCase()) ||
        !this.paramsRole.search
      );
    });
    const totalSize = dataResult?.length || 0;
    this.pageableRole = {
      totalElements: totalSize,
      totalPages: Math.ceil(totalSize / this.limit),
      currentPage: Math.floor(this.paramsRole.first / this.limit),
      size: this.limit,
    };
    this.listRole = dataResult?.slice(this.paramsRole.first, this.paramsRole.max) || [];
    this.isLoading = false;
  }

  getRolesOfUser() {
    if (this.userSelected?.username) {
      this.isLoading = true;
      this.rmProfileService.getRolesByUser(this.userSelected.username).subscribe(
        (roles) => {
          this.roleSelected = roles || [];
          for (const itemRole of this.dataRole) {
            itemRole.isChecked =
              this.roleSelected?.findIndex((role) => {
                return role?.name === itemRole?.name;
              }) > -1;
          }
          this.dataRole?.sort((a, b) => {
            return a?.isChecked === b?.isChecked ? 0 : a?.isChecked ? -1 : 1;
          });
          this.searchRole(true);
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
    } else {
      for (const itemRole of this.dataRole) {
        itemRole.isChecked = false;
      }
      this.dataRole?.sort((a, b) => {
        return a?.isChecked === b?.isChecked ? 0 : a?.isChecked ? -1 : 1;
      });
      this.searchRole(true);
    }
  }

  setPage(pageInfo, type) {
    if (type === 'user') {
      this.paramsUser.page = pageInfo.offset;
      this.searchUser(false);
    } else {
      this.paramsRole.first = pageInfo.offset * this.limit;
      this.paramsRole.max = this.paramsRole.first + this.limit;
      this.searchRole(false);
    }
  }

  save() {
    if (this.roleSelected.length > 0 && this.userSelected) {
      this.isLoading = true;
      this.roleSelected.forEach((item) => {
        delete item.isChecked;
      });
      const listRoleName = this.roleSelected?.map((role) => role.name) || [];
      this.rmProfileService.updateRolesByUser(this.userSelected.username, listRoleName).subscribe(
        () => {
          for (const itemRole of this.dataRole) {
            itemRole.isChecked =
              this.roleSelected.findIndex((role) => {
                return role.name === itemRole.name;
              }) > -1;
          }
          this.dataRole.sort((a, b) => {
            return a.isChecked === b.isChecked ? 0 : a.isChecked ? -1 : 1;
          });
          this.searchRole(true);
          this.messageService.success(this.notificationMessage.success);
          this.isLoading = false;
        },
        (e) => {
          if (e?.error?.description) {
            this.messageService.warn(e?.error?.description);
          } else {
            this.messageService.error(this.notificationMessage.error);
          }
          this.isLoading = false;
        }
      );
    }
  }

  onCheckboxRoleFn(row, isChecked) {
    if (isChecked) {
      this.roleSelected.push(row);
    } else {
      this.roleSelected = this.roleSelected.filter((role) => {
        return role.name !== row.name;
      });
    }
    this.dataRole.find((item) => row.name === item.name).isChecked = isChecked;
  }

  onSelect({ selected }) {
    this.userSelected = selected[0];
    this.roleSelected = [];
    this.paramsRole.search = this.roleSearch.trim();
    const params = JSON.parse(JSON.stringify(this.paramsRole));
    params.first = 0;
    params.max = maxInt32;
    this.getRolesOfUser();
  }

  generateBranchName(branchCode) {
    if (this.listBranches) {
      for (const item of this.listBranches) {
        if (item.code === branchCode) {
          return item.name;
        }
      }
    }
    return '';
  }
}
