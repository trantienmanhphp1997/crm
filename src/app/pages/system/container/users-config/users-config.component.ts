import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { cleanDataForm } from 'src/app/core/utils/function';
import { Component, OnInit, Injector, AfterViewInit } from '@angular/core';
import { FunctionCode, maxInt32 } from 'src/app/core/utils/common-constants';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { CategoryService } from '../../services/category.service';
import { UserService } from '../../services/users.service';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { global } from '@angular/compiler/src/util';
import { BaseComponent } from 'src/app/core/components/base.component';
import { FileService } from 'src/app/core/services/file.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-users-config',
  templateUrl: './users-config.component.html',
  styleUrls: ['./users-config.component.scss'],
  providers: [NgbActiveModal, NgbModal],
})
export class UsersConfigComponent extends BaseComponent implements OnInit, AfterViewInit {
  isLoading = false;
  listData = [];
  textFilter = '';
  limit = global.userConfig.pageSize;
  searchForm = this.fb.group({
    username: '',
    fullName: '',
    email: '',
    isActive: '',
    unLock: '',
  });
  paramSearch = {
    size: this.limit,
    page: 0,
    searchAdvanced: '',
  };
  listBranches = [];
  listStatus = [];
  listLock = [];
  pageable: Pageable;
  isAdvance = false;
  prevParams: any;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private userService: UserService,
    private fileService: FileService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_USER}`);
  }

  ngOnInit(): void {
    this.getBranches();
    this.listStatus.push({ code: '', name: this.fields.all });
    this.listStatus.push({ code: true, name: this.fields.isActive });
    this.listStatus.push({ code: false, name: this.fields.inActive });
    this.listLock.push({ code: '', name: this.fields.all });
    this.listLock.push({ code: true, name: this.fields.open });
    this.listLock.push({ code: false, name: this.fields.close });
  }

  isShow() {
    this.isAdvance = !this.isAdvance;
  }

  ngAfterViewInit() {
    this.ref.detectChanges();
  }

  getBranches() {
    const paramsBranch = {
      page: 0,
      size: maxInt32,
    };
    this.categoryService.searchBranches(paramsBranch).subscribe((result) => {
      if (result && result.content) {
        this.listBranches = result.content;
      }
    });
  }

  searchAdvance() {
    const params = cleanDataForm(this.searchForm);
    params.page = 0;
    params.size = this.paramSearch.size;
    this.search(params);
  }

  searchBasic() {
    this.paramSearch.searchAdvanced = _.trim(this.paramSearch.searchAdvanced);
    this.paramSearch.page = 0;
    this.search(this.paramSearch);
  }

  search(params?: any) {
    this.isLoading = true;
    let paramsNew: any;
    if (params) {
      paramsNew = params;
    } else {
      paramsNew = { ...this.prevParams, page: this.paramSearch.page };
    }
    this.userService.searchUser(paramsNew).subscribe(
      (result) => {
        this.prevParams = paramsNew;
        if (result?.content?.length === 0 && result?.number > 0) {
          this.paramSearch.page -= 1;
          this.search(false);
          return;
        }
        this.listData = result?.content || [];
        this.pageable = {
          totalElements: result?.totalElements,
          totalPages: result?.totalPages,
          currentPage: result?.number,
          size: this.limit,
        };
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    this.paramSearch.page = pageInfo.offset;
    this.search();
  }

  update(item) {
    this.router.navigate([this.router.url, 'update', item.username]);
  }

  isActive(el, item) {
    el?.parentElement?.parentElement?.blur();
    if (this.objFunction?.unLogUser) {
      this.confirmService.confirm().then((res) => {
        if (res) {
          item.active = !item.active;
          this.isLoading = true;
          this.userService.changeStatus(item.username, item.id, item.active).subscribe(
            () => {
              this.search(false);
              this.messageService.success(this.notificationMessage.success);
              this.isLoading = false;
            },
            () => {
              item.active = !item.active;
              this.messageService.error(this.notificationMessage.error);
              this.isLoading = false;
            }
          );
        }
      });
      this.ref.detectChanges();
    }
  }

  isLock(el, item) {
    el?.parentElement?.parentElement?.blur();
    if (this.objFunction?.unLogUser) {
      this.confirmService.confirm().then((res) => {
        if (res) {
          item.unLock = !item.unLock;
          this.isLoading = true;
          this.userService.changeLocked(item.username, item.id, item.unLock).subscribe(
            () => {
              this.search(false);
              this.messageService.success(this.notificationMessage.success);
              this.isLoading = false;
            },
            () => {
              item.unLock = !item.unLock;
              this.messageService.error(this.notificationMessage.error);
              this.isLoading = false;
            }
          );
        }
      });
      this.ref.detectChanges();
    }
  }

  delete(item) {
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.result
      .then((res) => {
        if (res) {
          this.userService.delete(item?.username).subscribe(
            () => {
              this.search(false);
              this.messageService.success(this.notificationMessage.success);
            },
            (e) => {
              if (e?.error) {
                this.messageService.warn(e?.error?.description);
              } else {
                this.messageService.error(this.notificationMessage.error);
              }
            }
          );
        }
      })
      .catch(() => {});
  }

  generateBranchName(code) {
    return (
      this.listBranches.find((item) => {
        return item.code === code;
      })?.name || ''
    );
  }

  exportFile() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+(this.pageable?.totalElements || 0) === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.isLoading = true;
    this.userService.createFileExcel(this.prevParams).subscribe(
      (fileId) => {
        this.fileService.downloadFile(fileId, 'danh-sach-nguoi-dung.xlsx').subscribe((res) => {
          if (res) {
            this.isLoading = false;
          } else {
            this.isLoading = false;
            this.messageService.error(this.notificationMessage.error);
          }
        });
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }

  onActive($event) {
    if ($event.type === 'dblclick') {
      if (_.get(this.objFunction, 'update') === true) {
        const item = _.get($event, 'row');
        this.update(item);
      }
    }
  }
}
