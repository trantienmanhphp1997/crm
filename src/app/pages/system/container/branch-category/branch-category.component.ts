import { Component, OnInit, OnDestroy, ViewChild, Injector } from '@angular/core';
import { FunctionCode, maxInt32 } from 'src/app/core/utils/common-constants';
import { CategoryService } from '../../services/category.service';
import { ExportExcelService } from 'src/app/core/services/export-excel.service';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import { BaseComponent } from 'src/app/core/components/base.component';
import _ from 'lodash';

declare const $: any;

@Component({
  selector: 'app-branch-category',
  templateUrl: './branch-category.component.html',
  styleUrls: ['./branch-category.component.scss'],
})
export class BranchCategoryComponent extends BaseComponent implements OnInit, OnDestroy {
  isLoading = false;
  rows = [];
  temp = [];
  ColumnMode = ColumnMode;
  textSearch = '';
  @ViewChild(DatatableComponent) public table: DatatableComponent;
  listBranchTypeKPI: any;
  listRegionKPI: any;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private exportExcelService: ExportExcelService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ADMIN_BRANCH}`);
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.categoryService.searchBranches({ page: 0, size: maxInt32 }).subscribe(
      (listBranch) => {
        if (listBranch && listBranch.content) {
          for (const item of listBranch.content) {
            if (item.parentCode === item.code) {
              delete item.parentCode;
            } else {
              item.parentName = listBranch.content.find((branch) => {
                return branch.code === item.parentCode;
              })?.name;
            }
            const listChildren = listBranch.content.filter((branch) => {
              return branch.parentCode === item?.code;
            });
            item['treeStatus'] = listChildren.length > 0 ? 'expanded' : 'disabled';
            this.rows.push(item);
          }
          this.rows.sort((a, b) => {
            if (a.code.toLowerCase() < b.code.toLowerCase()) {
              return -1;
            }
            if (a.code.toLowerCase() > b.code.toLowerCase()) {
              return 1;
            }
            return 0;
          });
          this.temp = [...this.rows];
          this.rows = [...this.rows];
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngAfterViewInit() {
    this.table.messages = global.messageTable;
  }

  search() {
    this.updateFilter();
  }

  onTreeAction(event: any) {
    const row = event.row;
    if (row.treeStatus === 'collapsed') {
      row.treeStatus = 'expanded';
    } else {
      row.treeStatus = 'collapsed';
    }
    this.rows = [...this.rows];
  }

  updateFilter() {
    const val = this.textSearch.trim().toLowerCase();

    // filter our data
    const temp = this.temp.filter(function (d) {
      return d.code.toLowerCase().indexOf(val) !== -1 || d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });
    let listResult: any = [];
    listResult = [...temp];
    temp.forEach((item) => {
      listResult = [...listResult, ...this.findParentBranch(item, this.temp, [])];
    });
    listResult.sort((a, b) => {
      if (a.code.toLowerCase() < b.code.toLowerCase()) {
        return -1;
      }
      if (a.code.toLowerCase() > b.code.toLowerCase()) {
        return 1;
      }
      return 0;
    });

    // update the rows
    this.rows = [...new Set(listResult)];
  }

  findParentBranch(item: any, listAll: any[], listResult: any[]) {
    if (item.parentCode) {
      const itemOld = listAll.find((obj) => {
        return obj.code === item.parentCode;
      });
      if (itemOld) {
        listResult.push(itemOld);
        this.findParentBranch(itemOld, listAll, listResult);
      }
    }
    return listResult;
  }

  openDetail(item) {
    this.router.navigate([this.router.url, 'update', item.code], { state: { data: item } });
  }

  exportFile() {
    this.translate.get('fields').subscribe(
      (fields) => {
        const fieldLabels = fields;
        let data = [];
        let obj: any = {};
        this.rows.forEach((item) => {
          obj = {};
          obj[fieldLabels.branchCode] = item.code;
          obj[fieldLabels.branchName] = item.name;
          data.push(obj);
        });
        this.exportExcelService.exportAsExcelFile(data, 'branch_category');
      },
      () => { }
    );
  }

  ngOnDestroy() { }

  onActive($event) {
    if ($event.type === 'dblclick') {
      if (_.get(this.objFunction, 'update') === true) {
        var item = _.get($event, 'row');
        this.openDetail(item);
      }
    }
  }
}
