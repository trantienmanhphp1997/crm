import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationsConfigComponent } from './applications-config.component';

describe('ApplicationsConfigComponent', () => {
  let component: ApplicationsConfigComponent;
  let fixture: ComponentFixture<ApplicationsConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ApplicationsConfigComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationsConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
