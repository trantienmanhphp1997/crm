import { UsersConfigComponent } from './container/users-config/users-config.component';
import { SystemComponent } from './system.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SystemRoutingModule } from './system-routing.module';
import { SystemMenuComponent } from './components/system-menu/system-menu.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { LanesConfigDetailComponent } from './components/lanes-config-detail/lanes-config-detail.component';
import { UserEditModalComponent } from './components/user-edit-modal/user-edit-modal.component';
import { UserAddModalComponent } from './components/user-add-modal/user-add-modal.component';
import { ImportUsersModalComponent } from './components/import-users-modal/import-users-modal.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { TemplateConfigComponent } from './container/template-config/template-config.component';
import { CategoryConfigComponent } from './container/category-config/category-config.component';
import { WorkflowConfigComponent } from './container/workflow-config/workflow-config.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { FieldConfigModalComponent } from './components/field-config-modal/field-config-modal.component';
import { UserSessionsComponent } from './container/user-sessions/user-sessions.component';
import { TranslateModule } from '@ngx-translate/core';
import { ApplicationsConfigComponent } from './container/applications-config/applications-config.component';
import { ApplicationsConfigAddModalComponent } from './components/applications-config-add-modal/applications-config-add-modal.component';
import { ApplicationsConfigEditModalComponent } from './components/applications-config-edit-modal/applications-config-edit-modal.component';
import { CommonCategoryComponent } from './container/common-category/common-category.component';
import { CommonCategoryCreateComponent } from './components/common-category-create/common-category-create.component';
import { CommonCategoryUpdateComponent } from './components/common-category-update/common-category-update.component';
import { BranchCategoryComponent } from './container/branch-category/branch-category.component';
import { BranchCategoryUpdateComponent } from './components/branch-category-update/branch-category-update.component';
import { BlocksCategoryUpdateComponent } from './components/blocks-category-update/blocks-category-update.component';
import { BlocksCategoryCreateComponent } from './components/blocks-category-create/blocks-category-create.component';
import { BlocksCategoryComponent } from './container/blocks-category/blocks-category.component';
import { ResourcesCategoryComponent } from './container/resources-category/resources-category.component';
import { ResourcesCategoryCreateComponent } from './components/resources-category-create/resources-category-create.component';
import { ResourcesCategoryUpdateComponent } from './components/resources-category-update/resources-category-update.component';
import { ManipulationCategoryComponent } from './container/manipulation-category/manipulation-category.component';
import { ManipulationCategoryCreateComponent } from './components/manipulation-category-create/manipulation-category-create.component';
import { ManipulationCategoryUpdateComponent } from './components/manipulation-category-update/manipulation-category-update.component';
import { RoleCategoryUpdateComponent } from './components/role-category-update/role-category-update.component';
import { RoleCategoryCreateComponent } from './components/role-category-create/role-category-create.component';
import { RoleCategoryComponent } from './container/role-category/role-category.component';
import { UserRoleConfigComponent } from './container/user-role-config/user-role-config.component';
import { LoggingHistoryComponent } from './container/logging-history/logging-history.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { PermissionCategoryComponent } from './container/permission-category/permission-category.component';
import { PermissionCategoryCreateComponent } from './components/permission-category-create/permission-category-create.component';
import { PermissionCategoryUpdateComponent } from './components/permission-category-update/permission-category-update.component';
import { NgxSelectModule } from 'ngx-select-ex';
import { UserPermissionConfigComponent } from './container/user-permission-config/user-permission-config.component';
import { BranchesModalComponent } from './components/branches-modal/branches-modal.component';
import { UserPermissionImportComponent } from './components/user-permission-import/user-permission-import.component';
import { BranchModalComponent } from './components/branch-modal/branch-modal.component';
import { ChooseBranchesModalComponent } from './components/choose-branches-modal/choose-branches-modal.component';
import { IndustryModalComponent } from './components/industry-modal/industry-modal.component';
import { ProcessManagementComponent } from './components/process-management/process-management.component';
import { ProcessManagementActionComponent } from './components/process-management.action/process-management.action.component';
import { HistoryProcessTableComponent } from './components/history-process-table/history-process-table.component';
import { PermissionForRoleComponent } from './container/permission-for-role/permission-for-role.component';
import { RESOLVERS } from './resolvers';
import { MatTabsModule } from '@angular/material/tabs';
import { CalendarModule } from 'primeng/calendar';
import { InputSwitchModule } from 'primeng/inputswitch';
import { ProcessModalComponent } from './components/process-modal/process-modal.component';
import { RadioButtonModule } from 'primeng/radiobutton';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { TreeModule } from 'primeng/tree';
import { TreeTableModule } from 'primeng/treetable';
import { CheckboxModule } from 'primeng/checkbox';
import { ProcessHistoryModalComponent } from './components/process-history-modal/process-history-modal.component';
import { ProcessHistoryDetailComponent } from './components/process-history-detail/process-history-detail.component';
import { ButtonModule } from 'primeng/button';
import { ListTitleRelationshipGroup } from './components/title-relationship-group/list-title-relationship-group.component';
import { InputTextModule } from 'primeng/inputtext';
import { TitleRelationshipGroupCreate } from './components/title-relationship-group-create/title-relationship-group-create.component';
import { AddTitleModalComponent } from './components/add-title-modal/add-title-modal.component';
import { ExportDataComponent } from './components/export-data/export-data.component';
import { NoteComponent } from './components/note/note.component';
import { UploadImageComponent } from './components/upload-image/upload-image.component';
import { ImageFormComponent } from './components/upload-image/image-form/image-form.component';
import {InputNumberModule} from 'primeng/inputnumber';
import { ChooseTreeBranchesModalComponent } from './components/choose-tree-branches/choose-tree-branches.component';
import {NewsCreateComponent} from './components/news/news-create.component';
import {NewsListComponent} from './components/news/list/news-list.component';
import {PreviewBannerNewsComponent} from "./components/news/preview-img/preview-banner-news.component";
import {UploadImageNewComponent} from "./components/upload-image-new/upload-image-new.component";
import {ImageFormNewComponent} from "./components/upload-image-new/image-form-new/image-form-new.component";
import { FrequencyOfUsingFunctionComponent } from './components/frequency-of-using-function/frequency-of-using-function.component';
import { ViewTableComponent } from './components/frequency-of-using-function/view-table/view-table.component';

@NgModule({
  declarations: [
    SystemMenuComponent,
    SystemComponent,
    LanesConfigDetailComponent,
    UserEditModalComponent,
    UserAddModalComponent,
    UsersConfigComponent,
    ImportUsersModalComponent,
    TemplateConfigComponent,
    CategoryConfigComponent,
    WorkflowConfigComponent,
    FieldConfigModalComponent,
    UserSessionsComponent,
    ApplicationsConfigComponent,
    ApplicationsConfigAddModalComponent,
    ApplicationsConfigEditModalComponent,
    CommonCategoryComponent,
    CommonCategoryCreateComponent,
    CommonCategoryUpdateComponent,
    BranchCategoryComponent,
    BranchCategoryUpdateComponent,
    BlocksCategoryComponent,
    BlocksCategoryUpdateComponent,
    BlocksCategoryCreateComponent,
    ResourcesCategoryComponent,
    ResourcesCategoryCreateComponent,
    ResourcesCategoryUpdateComponent,
    ManipulationCategoryComponent,
    ManipulationCategoryCreateComponent,
    ManipulationCategoryUpdateComponent,
    RoleCategoryUpdateComponent,
    RoleCategoryCreateComponent,
    RoleCategoryComponent,
    UserRoleConfigComponent,
    LoggingHistoryComponent,
    PermissionCategoryComponent,
    PermissionCategoryCreateComponent,
    PermissionCategoryUpdateComponent,
    UserPermissionConfigComponent,
    BranchesModalComponent,
    UserPermissionImportComponent,
    BranchModalComponent,
    ChooseBranchesModalComponent,
    IndustryModalComponent,
    ProcessManagementComponent,
    ProcessManagementActionComponent,
    ProcessModalComponent,
    HistoryProcessTableComponent,
    PermissionForRoleComponent,
    ProcessHistoryModalComponent,
    ProcessHistoryDetailComponent,
    ListTitleRelationshipGroup,
    TitleRelationshipGroupCreate,
    AddTitleModalComponent,
    ExportDataComponent,
    NoteComponent,
    UploadImageComponent,
    ImageFormComponent,
    ChooseTreeBranchesModalComponent,
    NewsCreateComponent,
    NewsListComponent,
    PreviewBannerNewsComponent,
    UploadImageNewComponent,
    ImageFormNewComponent,
    FrequencyOfUsingFunctionComponent,
    ViewTableComponent
  ],
  imports: [
    CommonModule,
    SystemRoutingModule,
    SharedModule,
    NgbModule,
    DragDropModule,
    ReactiveFormsModule,
    FormsModule,
    InfiniteScrollModule,
    TranslateModule,
    NgxDatatableModule,
    NgxSelectModule,
    MatTabsModule,
    CalendarModule,
    InputSwitchModule,
    RadioButtonModule,
    OverlayPanelModule,
    DropdownModule,
    MultiSelectModule,
    TreeModule,
    CheckboxModule,
    TreeTableModule,
    ButtonModule,
    InputTextModule,
    InputNumberModule,
  ],
  providers: [...RESOLVERS],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class SystemViewModule {}
