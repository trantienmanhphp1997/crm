import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourcesCategoryCreateComponent } from './resources-category-create.component';

describe('ResourcesCategoryCreateComponent', () => {
  let component: ResourcesCategoryCreateComponent;
  let fixture: ComponentFixture<ResourcesCategoryCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ResourcesCategoryCreateComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourcesCategoryCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
