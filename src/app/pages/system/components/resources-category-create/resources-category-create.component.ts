import { forkJoin } from 'rxjs';
import { CategoryService } from '../../services/category.service';
import { Component, OnInit, AfterViewInit, OnChanges, SimpleChanges, Injector } from '@angular/core';
import { FormArray, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { maxInt32 } from 'src/app/core/utils/common-constants';
import { BaseComponent } from 'src/app/core/components/base.component';

declare const $: any;

@Component({
  selector: 'app-resources-category-create',
  templateUrl: './resources-category-create.component.html',
  styleUrls: ['./resources-category-create.component.scss'],
  providers: [NgbModal, NgbActiveModal],
})
export class ResourcesCategoryCreateComponent extends BaseComponent implements OnInit, AfterViewInit, OnChanges {
  isLoading = false;
  listApp: any;
  listResourceParent: any;
  listManipulation: any;
  listUris = [];
  form = this.fb.group({
    name: ['', CustomValidators.required],
    code: ['', [CustomValidators.required, CustomValidators.code]],
    displayName: [''],
    type: ['', CustomValidators.required],
    iconUri: [''],
    uri: ['', CustomValidators.required],
    parentId: [''],
    scopes: [''],
    uriOfResources: [''],
    indexNumber: ['', [CustomValidators.required, Validators.min(0), CustomValidators.onlyNumber]],
    api: [''],
    apis: this.fb.array([]),
  });
  api = '';
  parentId = '';

  constructor(injector: Injector, private categoryService: CategoryService) {
    super(injector);
  }

  ngOnInit(): void {
    this.isLoading = true;
    forkJoin([
      this.categoryService.searchResourceCategory({ page: 0, size: maxInt32 }),
      this.categoryService.searchScope({ page: 0, size: maxInt32 }),
      this.categoryService.searchAppCategory({ page: 0, size: maxInt32 }),
    ]).subscribe(
      ([listResources, listManipulation, listApp]) => {
        this.isLoading = false;
        this.listApp = listApp.content;
        this.listResourceParent = listResources.content;
        this.listManipulation = listManipulation?._embedded?.keycloakScopeList || [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnChanges(changes: SimpleChanges) {}

  ngAfterViewInit() {}

  confirmDialog() {
    if (this.form.valid) {
      const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result
        .then((res) => {
          if (res) {
            this.isLoading = true;
            const formData = cleanDataForm(this.form);
            const data: any = {};
            Object.keys(formData).forEach((key) => {
              if (key !== 'api' && key !== 'apis') {
                data[key] = formData[key];
              }
            });
            data.uriOfResources = [];
            formData.apis.forEach((api) => {
              if (api.name.trim() !== '') {
                data.uriOfResources.push(api.name);
              }
            });
            if (data.scopes === '') {
              data.scopes = [];
            }
            data.parentId = this.parentId;
            this.categoryService.createResourceCategory(data).subscribe(
              () => {
                this.isLoading = false;
                this.location.back();
                this.messageService.success(this.notificationMessage.success);
              },
              (e) => {
                if (e?.error) {
                  this.messageService.warn(e?.error?.description);
                } else {
                  this.messageService.error(this.notificationMessage.error);
                }
                this.isLoading = false;
              }
            );
          }
        })
        .catch(() => {});
    } else {
      validateAllFormFields(this.form);
    }
  }

  get apis() {
    return this.form.get('apis') as FormArray;
  }

  createApi(index, value?): FormGroup {
    return this.fb.group({
      id: 'api' + index,
      name: value ? value : '',
    });
  }

  addUri() {
    const value = this.form.controls.api.value.trim();
    if (value.length > 0) {
      this.apis.push(this.createApi(this.apis.length, value));
      this.form.controls.api.setValue('');
    }
  }

  deleteUri(i) {
    this.apis.removeAt(i);
  }

  back() {
    this.location.back();
  }
}
