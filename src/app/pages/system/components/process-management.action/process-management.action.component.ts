import { Component, HostBinding, OnInit } from '@angular/core';
import { ProcessManagementService } from '../../services/process-management.service';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import _ from 'lodash';
import { Utils } from '../../../../core/utils/utils';
import { NotifyMessageService } from '../../../../core/components/notify-message/notify-message.service';
import { FormBuilder, NgForm, RequiredValidator } from '@angular/forms';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { TRISTATECHECKBOX_VALUE_ACCESSOR } from 'primeng/tristatecheckbox';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProcessModalComponent } from '../process-modal/process-modal.component';
import { number } from 'echarts';

@Component({
  selector: 'app-process-management.action',
  templateUrl: './process-management.action.component.html',
  styleUrls: ['./process-management.action.component.scss'],
})
export class ProcessManagementActionComponent implements OnInit {
  constructor(
    private service: ProcessManagementService,
    private translate: TranslateService,
    route: ActivatedRoute,
    private router: Router,
    private messageService: NotifyMessageService,
    private modalService: NgbModal
  ) {
    this.isCreate = _.get(route.snapshot.data, 'isCreate');
    this.isUpdate = _.get(route.snapshot.data, 'isUpdate');
    this.isView = _.get(route.snapshot.data, 'isView');
    this.id = _.get(route.snapshot.params, 'code');
    this.translate.get(['notificationMessage', 'system']).subscribe((result) => {
      this.notificationMessage = _.get(result, 'notificationMessage');
      this.sys_fields = _.get(result, 'system');
    });
    this.title = this.convert_title();
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
  }
  prop: any;
  isCreate: boolean;
  isUpdate: boolean;
  isView: boolean;
  id: string;
  model: any = {};
  sys_fields: any;
  isLoading: boolean;
  title: string;
  notificationMessage: any;
  form: any;
  tab_index = 0;
  process_name: string;
  desc: string;
  index = 0;
  setting_form: any;
  start: Date;
  repeat_status: Array<any>;
  time_configs: Array<any>;
  rpt_value: string;
  week_configs: Array<any>;
  day_of_months: Array<any>;
  week_of_months: Array<any>;
  time_option = false;
  month_of_years: Array<any>;
  outs: Array<any>;
  ins: Array<any>;
  monthForDays: Array<string>;
  monthForDates: Array<string>;
  isValidate: boolean;

  @HostBinding('class.app__right-content') appRightContent = true;

  ngOnInit(): void {
    this.isLoading = true;
    this.day_of_months = this.init_value(31);
    this.week_of_months = this.init_value(5);
    this.month_of_years = this.init_value(12);
    this.repeat_status = [
      {
        code: 'ONETIME',
        name: 'Một lần',
      },
      {
        code: 'REPEAT',
        name: 'Lặp lại',
      },
      {
        code: 'DAILY',
        name: 'Hàng ngày',
      },
      {
        code: 'WEEKLY',
        name: 'Hàng tuần',
      },
      {
        code: 'MONTHLY',
        name: 'Hàng tháng',
      },
      {
        code: 'YEARLY',
        name: 'Hàng năm',
      },
    ];
    this.time_configs = [
      {
        code: 'SECONDS',
        name: 'Giây',
      },
      {
        code: 'MINUTE',
        name: 'Phút',
      },
      {
        code: 'HOURS',
        name: 'Giờ',
      },
    ];

    this.week_configs = [
      {
        code: 'Sunday',
        name: 'Chủ nhật',
      },
      {
        code: 'Monday',
        name: 'Thứ hai',
      },
      {
        code: 'Tuesday',
        name: 'Thứ ba',
      },
      {
        code: 'Wednesday',
        name: 'Thứ tư',
      },
      {
        code: 'Thursday',
        name: 'Thứ năm',
      },
      {
        code: 'Friday',
        name: 'Thứ sáu',
      },
      {
        code: 'Saturday',
        name: 'Thứ bảy',
      },
    ];
    if (Utils.isStringEmpty(this.id)) {
      this.model = {};
      this.model.schedule = {};
      _.set(this.model, 'schedule.startTime', new Date());
      _.set(this.model, 'schedule.typeScheduler', 'ONETIME');
      this.change('ONETIME');
      this.isLoading = false;
    } else {
      this.service.getProcessById(this.id).subscribe(
        (response: any) => {
          this.model = response;
          this.process_name = _.get(this.model, 'processCategory.name');
          _.set(this.model, 'processCategoryId', _.get(this.model, 'processCategory.processCategoryId'));
          _.set(this.model, 'schedule', _.get(this.model, 'processScheduler'));
          if (Utils.isNotNull(_.get(this.model, 'schedule'))) {
            _.set(this.model, 'schedule.startTime', moment(_.get(this.model, 'schedule.startTime')).toDate());
            if (Utils.isNotNull(_.get(this.model, 'schedule.endTime'))) {
              _.set(this.model, 'schedule.endTime', moment(_.get(this.model, 'schedule.endTime')).toDate());
            }
            this.change(_.get(this.model, 'schedule.typeScheduler'));
            let offset = new Date().getTimezoneOffset() / -60;
            _.set(
              this.model,
              'schedule.timeRepeat',
              moment(_.get(this.model, 'schedule.timeRepeat')).add('hour', offset).toDate()
            );
            if (_.get(this.model, 'schedule.typeRecur') == 'NGAY') {
              this.monthForDates = _.get(this.model, 'schedule.monthList');
            }
            if (_.get(this.model, 'schedule.typeRecur') === 'THU') {
              this.monthForDays = _.get(this.model, 'schedule.monthList');
            }
          }
          if (Utils.isNull(_.get(this.model, 'schedule'))) {
            this.model.schedule = {};
          }
          if (Utils.isArrayNotEmpty(_.get(this.model, 'processParams'))) {
            let parameters = _.get(this.model, 'processParams');
            this.outs = _.filter(parameters, (x) => x.inOut === 'OUT') || [];
            this.ins = _.filter(parameters, (x) => x.inOut === 'IN') || [];
            this.ins = _.map(this.ins, (x) => ({ ...x, value: this.convertValue(x) }));
          }
          this.isLoading = false;
        },
        () => {
          this.model = {};
          this.messageService.error(_.get(this.notificationMessage, 'E001'));
          this.isLoading = false;
        }
      );
    }
  }

  convertValue(value) {
    let type = _.get(value, 'type');
    switch (type) {
      case 'STRING':
        return _.get(value, 'valueDeserialization');
      case 'INTEGER':
        return _.get(value, 'valueDeserialization') ? Number.parseInt(_.get(value, 'valueDeserialization')) : null;
      case 'DATE':
        return _.get(value, 'valueDeserialization') ? moment(_.get(value, 'valueDeserialization')).toDate() : null;
      case 'DATETIME':
        return _.get(value, 'valueDeserialization') ? moment(_.get(value, 'valueDeserialization')).toDate() : null;
      case 'TIME':
        return _.get(value, 'valueDeserialization')
          ? moment(_.get(value, 'valueDeserialization')).add(-7, 'hour').toDate()
          : null;
    }
  }

  init_value(count: number) {
    let values = [];
    for (let i = 1; i <= count; i++) {
      values.push({ code: `${i}`, name: `${i}` });
    }
    return values;
  }

  next() {
    if (this.tab_index === 0) {
      if (Utils.isStringEmpty(this.model.code) || Utils.isStringEmpty(this.model.name)) {
        this.messageService.error('Bạn phải nhập thông tin');
        return;
      }
    }
    if (this.tab_index === 1) {
      if (Utils.isStringEmpty(this.model.processCategoryId)) {
        this.messageService.error('Bạn phải chọn tiến trình');
        return;
      }
      if (_.isArray(this.ins)) {
        for (const item of this.ins) {
          this.isValidate = item.value === null;
          if (this.isValidate) {
            return;
          }
        }
      }
    }
    this.tab_index++;
  }

  previours() {
    this.tab_index--;
  }

  save() {
    this.isLoading = true;
    let parameters = [];
    if (Utils.isArrayNotEmpty(this.ins) && Utils.isArrayNotEmpty(this.outs)) {
      parameters = [...this.ins, ...this.outs];
    } else {
      if (Utils.isArrayEmpty(this.ins) && Utils.isArrayNotEmpty(this.outs)) {
        parameters = this.outs;
      } else {
        parameters = this.ins;
      }
    }
    _.forEach(parameters, (x) => {
      if (_.get(x, 'type') === 'TIME') {
        _.set(x, 'value', moment(x.value).add('hour', 7));
      }
    });
    this.model.parameters = parameters;
    _.set(this.model, 'processParams', null);
    if (Utils.isArrayNotEmpty(this.monthForDates) && _.get(this.model, 'schedule.typeRecur') === 'THU') {
      _.set(this.model, 'monthList', this.monthForDates);
    }
    if (Utils.isArrayNotEmpty(this.monthForDays) && _.get(this.model, 'schedule.typeRecur') === 'NGAY') {
      _.set(this.model, 'monthList', this.monthForDays);
    }
    let message = this.message();
    this.service.createProcess(this.model).subscribe(
      () => {
        this.messageService.success(_.get(message, 'success'));
        this.router.navigate([`/system/process-management`], { state: this.prop });
        this.isLoading = false;
      },
      (error) => {
        if (_.get(error, 'error.code') === 'err.api.errorcode.CRM196') {
          this.messageService.error(_.get(error, 'error.description'));
          this.isLoading = false;
          return;
        }
        this.messageService.error(_.get(message, 'error'));
        this.isLoading = false;
      }
    );
  }

  message() {
    if (this.isCreate) {
      return {
        success: 'Tạo mới tiến trình thành công',
        error: 'Tạo mới tiến trình không thành công',
      };
    } else if (this.isUpdate) {
      return {
        success: 'Cập nhật tiến trình thành công',
        error: 'Cập nhật tiến trình không thành công',
      };
    }
  }

  back() {
    this.router.navigate(['/system/process-management'], { state: this.prop });
  }

  chooseProcess() {
    const modal = this.modalService.open(ProcessModalComponent, { windowClass: 'tree__branch-modal' });
    modal.componentInstance.processCode = this.model || [];
    modal.result.then((res) => {
      if (res) {
        let process = _.first(res);
        this.process_name = _.get(process, 'name');
        _.set(this.model, 'isEmail', _.get(process, 'email'));
        _.set(this.model, 'isSms', _.get(process, 'sms'));
        _.set(this.model, 'isNotify', _.get(process, 'notify'));
        _.set(this.model, 'contentEmail', _.get(process, 'contentEmail'));
        _.set(this.model, 'contentSms', _.get(process, 'contentSms'));
        _.set(this.model, 'contentNotify', _.get(process, 'contentNotify'));
        _.set(this.model, 'subject', _.get(process, 'subject'));
        _.set(this.model, 'processCategoryId', _.get(_.first(res), 'processCategoryId'));
        this.service.findAllProcessCategoryParams(_.get(_.first(res), 'processCategoryId')).subscribe(
          (response) => {
            let paramIns = _.chain(response)
              .filter((x) => x.inOut === 'IN')
              .value();
            this.ins = _.map(paramIns, (x) => ({ ...x, value: null }));
            this.outs = _.chain(response)
              .filter((x) => x.inOut === 'OUT')
              .value();
          },
          () => {
            this.model.parameters = [];
            this.outs = [];
          }
        );
      }
    });
  }

  change($event) {
    this.rpt_value = $event;
    if (this.rpt_value === 'MONTHLY') {
      if (Utils.isNull(_.get(this.model, 'schedule.typeRecur'))) {
        _.set(this.model, 'schedule.typeRecur', 'NGAY');
        this.change_time('NGAY');
      }
    }
  }

  change_time($event) {
    this.model.schedule.typeRecur = $event;
  }

  onTabChanged($event) {
    this.tab_index = _.get($event, 'index');
  }

  onNotiTabChanged($event) {
    this.index = _.get($event, 'index');
  }

  onContentEmailChange($event) {}

  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }

  convert_title() {
    if (this.isCreate) {
      return _.get(this.sys_fields, 'fields.create_process_management');
    } else if (this.isUpdate) {
      return _.get(this.sys_fields, 'fields.update_process_management');
    } else if (this.isView) {
      return _.get(this.sys_fields, 'fields.view_process_management');
    }
  }

  getValue(item, key) {
    return _.get(item, key);
  }

  isSelected(index: number) {
    if (this.tab_index == index) {
      return false;
    } else {
      return true;
    }
  }
}
