import { Validators } from '@angular/forms';
import { Component, OnInit, AfterViewInit, ViewChildren, QueryList, Injector } from '@angular/core';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { UserService } from '../../services/users.service';
import { CategoryService } from '../../services/category.service';
import { maxInt32 } from 'src/app/core/utils/common-constants';
import { BaseComponent } from 'src/app/core/components/base.component';

declare var $: any;

@Component({
  selector: 'app-user-add-modal',
  templateUrl: './user-add-modal.component.html',
  styleUrls: ['./user-add-modal.component.scss'],
  providers: [NgbModal],
})
export class UserAddModalComponent extends BaseComponent implements OnInit, AfterViewInit {
  isLoading = false;
  listBranches = [];
  form = this.fb.group({
    username: ['', [CustomValidators.required]],
    fullName: ['', [CustomValidators.required]],
    branch: ['', [CustomValidators.required]],
    email: ['', [CustomValidators.required, Validators.email]],
    phoneNumber: ['', [CustomValidators.required, CustomValidators.phoneMobileVN]],
    isActive: [true],
  });
  @ViewChildren('otpBranch') otpBranch: QueryList<any>;
  notificationMessage: any;
  placehoderSelect: string;

  constructor(injector: Injector, private userService: UserService, private categoryService: CategoryService) {
    super(injector);
  }

  ngOnInit(): void {
    this.isLoading = true;
    const paramsBranch = {
      page: 0,
      size: maxInt32,
    };
    this.categoryService.searchBranches(paramsBranch).subscribe(
      (result) => {
        if (result && result.content) {
          this.listBranches = result.content;
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngAfterViewInit() {}

  confirmDialog() {
    if (this.form.valid) {
      const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result
        .then((res) => {
          if (res) {
            const data = cleanDataForm(this.form);
            this.isLoading = true;
            this.userService.create(data).subscribe(
              () => {
                this.location.back();
                this.messageService.success(this.notificationMessage.success);
              },
              (e) => {
                if (e?.error) {
                  this.messageService.error(e?.error?.description);
                } else {
                  this.messageService.error(this.notificationMessage.error);
                }
                this.isLoading = false;
              }
            );
          }
        })
        .catch(() => {});
    } else {
      validateAllFormFields(this.form);
    }
  }

  back() {
    this.location.back();
  }
}
