import { Component, HostBinding, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import _ from 'lodash';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { Utils } from 'src/app/core/utils/utils';
import { global } from '@angular/compiler/src/util';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ProcessManagementService } from '../../services/process-management.service';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';
import { SessionService } from 'src/app/core/services/session.service';
import { FunctionCode } from 'src/app/core/utils/common-constants';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProcessHistoryModalComponent } from '../process-history-modal/process-history-modal.component';

@Component({
  selector: 'history-process-table',
  templateUrl: './history-process-table.component.html',
  styleUrls: ['./history-process-table.component.scss'],
})
export class HistoryProcessTableComponent implements OnInit {
  constructor(
    private api: ProcessManagementService,
    private translate: TranslateService,
    private route: ActivatedRoute,
    private messageService: NotifyMessageService,
    private router: Router,
    private modalService: NgbModal
  ) {
    this.messages = global.messageTable;
  }

  obj: any;
  isLoading = false;
  fields: string;
  messages: any;
  process_status: Array<any>;
  pageable: Pageable;
  params: any = {
    size: global.userConfig.pageSize,
    page: 0,
  };

  prevParams = this.params;
  models: Array<any>;
  offset: number;
  count: number;
  isSearch: boolean;
  limit: number = 10;
  isFisrt: boolean = true;
  @Input() processCode: string;
  model: Array<any> = [];

  @ViewChild('table') table: DatatableComponent;

  @HostBinding('class.app__right-content') appRightContent = true;

  ngOnInit(): void {
    this.isLoading = false;
    _.set(this.params, 'processId', this.processCode);
    this.reload(true);
  }

  ftime: boolean = true;
  paging($event) {
    if (this.isFisrt) {
      return;
    }
    this.params.page = _.get($event, 'offset');
    this.reload(false);
  }

  search() {
    this.reload(true);
  }

  reload(isSearch?: boolean) {
    this.isLoading = true;
    let params: any = {};
    if (isSearch) {
      this.params.page = 0;
    } else {
      params = this.prevParams;
    }
    Object.keys(this.params).forEach((key) => {
      params[key] = this.params[key];
    });
    params.page = this.params.page;
    params.size = this.params.size;
    this.api.findAllHistories(params).subscribe(
      (response) => {
        this.isFisrt = false;
        this.isLoading = false;
        this.models = _.get(response, 'content') || [];
        this.pageable = {
          totalElements: _.get(response, 'totalElements'),
          totalPages: _.get(response, 'totalPages'),
          currentPage: _.get(response, 'number'),
          size: _.get(global, 'userConfig.pageSize'),
        };
        this.prevParams = params;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  getValue(row, key) {
    return _.get(row, key);
  }

  viewDetail($event) {
    // let type = _.get($event, 'type');
    // let id = _.get($event, 'row.processHistoryId');
    // if (type === 'dblclick') {
    //   $event.cellElement.blur();
    //   if (Utils.isStringNotEmpty(id)) {
    //     const modal = this.modalService.open(ProcessHistoryModalComponent, { windowClass: 'process-history-modal' });
    //     modal.componentInstance.processHistoryId = id;
    //   }
    // }
  }
}
