import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-import-users-modal',
  templateUrl: './import-users-modal.component.html',
  styleUrls: ['./import-users-modal.component.scss'],
})
export class ImportUsersModalComponent implements OnInit {
  isFile = false;
  fileImport: any;
  fileName = 'Chọn file';
  name = 'Import Users';

  constructor(private modal: NgbActiveModal, private modalService: NgbModal) {}

  ngOnInit(): void {}

  import() {
    const confirmModal = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirmModal.result
      .then((confirmed: boolean) => {
        if (confirmed) {
          this.modal.close(this.fileImport);
        }
      })
      .catch(() => {});
  }

  handleFileInput(files: FileList) {
    if (files) {
      this.isFile = true;
      this.fileImport = files.item(0);
      this.fileName = files.item(0).name;
    } else {
      this.isFile = false;
    }
  }

  closeModal() {
    this.modal.close(false);
  }
}
