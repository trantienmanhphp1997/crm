import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManipulationCategoryCreateComponent } from './manipulation-category-create.component';

describe('ManipulationCategoryCreateComponent', () => {
  let component: ManipulationCategoryCreateComponent;
  let fixture: ComponentFixture<ManipulationCategoryCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManipulationCategoryCreateComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManipulationCategoryCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
