import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { global } from '@angular/compiler/src/util';
import { Component, OnInit, ViewEncapsulation, ViewChild, AfterViewInit, HostBinding } from '@angular/core';
import { ColumnMode, DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { CategoryService } from '../../services/category.service';
import { SalesService } from '../../services/sales.service';
import { Utils } from '../../../../core/utils/utils';
import _ from 'lodash';
import { NotifyMessageService } from '../../../../core/components/notify-message/notify-message.service';

@Component({
  selector: 'app-industry-modal',
  templateUrl: './industry-modal.component.html',
  styleUrls: ['./industry-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class IndustryModalComponent implements OnInit, AfterViewInit {
  constructor(
    private modalActive: NgbActiveModal,
    private categoryService: CategoryService,
    private salesService: SalesService,
    private messageService: NotifyMessageService
  ) { }

  isLoading = false;
  rows = [];
  temp = [];
  listChoose: any;
  listBranchOld: any;
  typeCustomer: any;
  rsId: string;
  scope: string;
  ColumnMode = ColumnMode;
  textSearch = '';
  isChecked: boolean;
  model: Array<any> = [];
  SelectionType = SelectionType;
  @ViewChild(DatatableComponent) public table: DatatableComponent;
  @HostBinding('class.app-industry-modal') industryModal = true;

  ngOnInit() {
    this.isLoading = true;
    if(this.typeCustomer ==='KHTN'){
      this.salesService.getAllIndustry().subscribe(
        (industries: Array<any>) => {
          if (Utils.isArrayNotEmpty(industries)) {
            let listChildren: Array<any>;
            let listData = []; 
            industries.forEach((e) => {
              let item = {
                "key":e.id,
                "value":e.loanPurpose,
                "parent":e.loai
              }
              listData.push(item);
            })
            listData.forEach((item) => {
              if (item.parent === item.key) {
                delete item.parent;
                listChildren = listData.filter((itemChild) => {
                  return itemChild.parent === item.key;
                });
              } else {
                listChildren = listData.filter((industry) => {
                  return industry.parent === item.key;
                });
                const value = listData.find((industry) => {
                  return industry.key === item.parent;
                });
                if (Utils.isNotNull(value)) {
                  item.parentValue = value.value;
                }
              }
              item.treeStatus = listChildren.length ? 'expanded' : 'disabled';
              this.rows.push(item);
            });
            this.rows.sort((a, b) => {
              if (a.key.toLowerCase() < b.key.toLowerCase()) {
                return -1;
              }
              if (a.key.toLowerCase() > b.key.toLowerCase()) {
                return 1;
              }
              return 0;
            });
            this.temp = [...this.rows];
            this.rows = [...this.rows];
            if (Utils.isArrayNotEmpty(this.listBranchOld)) {
              this.model = _.chain(this.rows)
                .filter((x) => x.key === this.listBranchOld[0].key)
                .value();
            }
          }
          this.isLoading = false;
        },
        () => {
          this.rows = [];
          this.isLoading = false;
        }
      );
    } else{
      this.categoryService.getIndustries({}).subscribe(
        (industries: Array<any>) => {
          if (Utils.isArrayNotEmpty(industries)) {
            let listChildren: Array<any>;
            industries.forEach((item) => {
              if (item.parent === item.key) {
                delete item.parent;
                listChildren = industries.filter((itemChild) => {
                  return itemChild.parent === item.key;
                });
              } else {
                listChildren = industries.filter((industry) => {
                  return industry.parent === item.key;
                });
                const value = industries.find((industry) => {
                  return industry.key === item.parent;
                });
                if (Utils.isNotNull(value)) {
                  item.parentValue = value.value;
                }
              }
              item.treeStatus = listChildren.length ? 'expanded' : 'disabled';
              this.rows.push(item);
            });
            this.rows.sort((a, b) => {
              if (a.key.toLowerCase() < b.key.toLowerCase()) {
                return -1;
              }
              if (a.key.toLowerCase() > b.key.toLowerCase()) {
                return 1;
              }
              return 0;
            });
            this.temp = [...this.rows];
            this.rows = [...this.rows];
            if (Utils.isArrayNotEmpty(this.listBranchOld)) {
              this.model = _.chain(this.rows)
                .filter((x) => x.key === this.listBranchOld[0].key)
                .value();
            }
          }
          this.isLoading = false;
        },
        () => {
          this.rows = [];
          this.isLoading = false;
        }
      );
    }

  }

  ngAfterViewInit() {
    this.table.messages = global?.messageTable;
  }

  onTreeAction(event: any) {
    const row = event.row;
    if (row.treeStatus === 'collapsed') {
      row.treeStatus = 'expanded';
    } else {
      row.treeStatus = 'collapsed';
    }
    this.rows = [...this.rows];
  }

  search() {
    this.updateFilter();
  }

  updateFilter() {
    const val = Utils.parseToEnglish(this.textSearch.trim().toLowerCase());
    this.rows = [];
    const temp = this.temp.filter((d) => {
      return d.key.toLowerCase().indexOf(val) !== -1 || d.value.toLowerCase().indexOf(val) !== -1 || !val;
    });
    let listResult = [...temp];
    for (const item of temp) {
      listResult = [...listResult, ...this.findParentBranch(item, this.temp, [])];
    }
    listResult.sort((a, b) => {
      if (a.key.toLowerCase() < b.key.toLowerCase()) {
        return -1;
      }
      if (a.key.toLowerCase() > b.key.toLowerCase()) {
        return 1;
      }
      return 0;
    });

    // update the rows
    this.rows = [...new Set(listResult)];
    _.forEach(this.listChoose, (item) => {
      const itemOld = this.rows.find((row) => {
        return row.key === item.key;
      });
      if (itemOld) {
        item = itemOld;
      }
    });
  }

  findParentBranch(item: any, listAll: any[], listResult: any[]) {
    if (item.parent) {
      const itemOld = listAll.find((obj) => {
        return obj.key === item.parent;
      });
      if (itemOld) {
        listResult.push(itemOld);
        this.findParentBranch(itemOld, listAll, listResult);
      }
    }
    return listResult;
  }

  choose() {
    if (Utils.isArrayEmpty(this.model)) {
      this.messageService.error('Chưa chọn thông tin ngành nghề kinh doanh');
      return;
    }else if(!this.model[0]?.parent && this.typeCustomer === 'KHTN'){
      this.messageService.error('Chỉ được chọn ngành nghề con');
      return;
    } else {
      this.modalActive.close(this.model);
    }
  }

  closeModal() {
    this.modalActive.close(false);
  }
}
