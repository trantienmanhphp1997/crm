import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { global } from '@angular/compiler/src/util';
import { Component, OnInit, ViewEncapsulation, ViewChild, AfterViewInit, HostBinding } from '@angular/core';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import { CategoryService } from '../../services/category.service';
import { maxInt32 } from 'src/app/core/utils/common-constants';
import _ from 'lodash';

@Component({
  selector: 'app-branches-modal',
  templateUrl: './branches-modal.component.html',
  styleUrls: ['./branches-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class BranchesModalComponent implements OnInit, AfterViewInit {
  isLoading = false;
  rows = [];
  temp = [];
  listChoose = [];
  listBranchOld: any;
  ColumnMode = ColumnMode;
  textSearch = '';
  isAllBranch: boolean;
  @ViewChild(DatatableComponent) public table: DatatableComponent;
  @HostBinding('class.app-branches-modal') chooseBranchesModal = true;

  constructor(private modalActive: NgbActiveModal, private categoryService: CategoryService) { }

  ngOnInit(): void {
    this.isLoading = true;
    if (this.listBranchOld) {
      this.listChoose = this.listBranchOld;
    }
    this.categoryService.searchBranches({ page: 0, size: maxInt32 }).subscribe(
      (listBranch) => {
        if (listBranch && listBranch.content) {
          this.isAllBranch = _.size(this.listChoose) === _.size(listBranch.content);
          let listChildren: any;
          for (const item of _.get(listBranch, 'content')) {
            if (_.get(item, 'parentCode') === _.get(item, 'code')) {
              delete item.parentCode;
              listChildren = _.find(_.get(listBranch, 'context'), (x) => x.parentCode === _.get(item, 'code'));
              item.isDisabledAll = this.isAllBranch;
              item.isCheckedAll =
                _.size(
                  _.chain(this.listBranchOld).filter((x) => x.parentCode === item.code || x.code === item.code)
                ) ===
                _.size(listChildren) + 1;
            } else {
              item.parentName = _.get(
                _.find(_.get(listBranch, 'content'), (x) => x.code === item.parentCode),
                'name'
              );
            }
            item.children = _.size(listChildren);
            item.isChecked = this.listBranchOld
              ? _.findIndex(this.listBranchOld, (x) => x.code === item.code) > -1
              : false;
            item.treeStatus = _.size(listChildren) > 0 ? 'expanded' : 'disabled';
            this.rows.push(item);
          }
          this.rows.sort((a, b) => {
            if (a?.code?.toLowerCase() < b?.code?.toLowerCase()) {
              return -1;
            }
            if (a?.code?.toLowerCase() > b?.code?.toLowerCase()) {
              return 1;
            }
            return 0;
          });
          this.temp = [...this.rows];
          this.rows = [...this.rows];
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngAfterViewInit() {
    this.table.messages = global.messageTable;
  }

  search() {
    this.updateFilter();
  }

  onCheckboxBranch(row, isChecked) {
    row.isChecked = isChecked;
    const parentCode = row.parentCode ? row.parentCode : row.code;
    if (isChecked) {
      this.listChoose.push(row);
    } else {
      this.isAllBranch = false;
      this.listChoose = this.listChoose.filter((item) => {
        return item.code !== row.code;
      });
    }
    this.listChoose.forEach((item) => {
      if (!row.parentCode && row.code === item.code) {
        item.isCheckedAll = isChecked;
      }
    });
    this.rows.forEach((item) => {
      if (!row.parentCode && row.code === item.code) {
        item.isCheckedAll = false;
        item.isCheckedAll =
          item.children + 1 ===
          this.listChoose.filter((itemOld) => {
            return itemOld.code === parentCode || itemOld.parentCode === parentCode;
          }).length;
      }
    });
  }

  onCheckboxBranchAll(row, isChecked) {
    row.isCheckedAll = isChecked;
    this.rows.forEach((item) => {
      if (item.code === row.code || item.parentCode === row.code) {
        item.isChecked = isChecked;
        if (isChecked) {
          if (
            this.listChoose.findIndex((itemChoose) => {
              return itemChoose.code === item.code;
            }) === -1
          ) {
            this.listChoose.push(item);
          }
        } else {
          this.listChoose = this.listChoose.filter((itemOld) => {
            return itemOld.code !== item.code;
          });
        }
        return item;
      }
    });
  }

  chooseAllbranch(isChecked) {
    this.isAllBranch = isChecked;
    this.listChoose = [];
    this.rows.forEach((item) => {
      if (!item.parentCode) {
        item.isDisabledAll =
          this.rows.filter((itemOld) => {
            return itemOld.code === item.code || itemOld.parentCode === item.code;
          }).length !==
          item.children + 1;
      }
      item.isChecked = isChecked;
      item.isDisabled = false;
      item.isCheckedAll = false;
      if (isChecked) {
        this.listChoose.push(item);
      }
    });

    // this.temp.forEach((itemTemp) => {
    //   itemTemp.isChecked = isChecked;
    //   itemTemp.isDisabled = false;
    //   itemTemp.isCheckedAll = false;
    //   itemTemp.isDisabledAll = isChecked;
    // });
  }

  onTreeAction(event: any) {
    const row = event.row;
    if (row.treeStatus === 'collapsed') {
      row.treeStatus = 'expanded';
    } else {
      row.treeStatus = 'collapsed';
    }
    this.rows = [...this.rows];
  }

  updateFilter() {
    const val = this.textSearch.trim().toLowerCase();
    this.rows = [];

    // filter our data
    const temp = this.temp.filter((d) => {
      return d?.code?.toLowerCase().indexOf(val) !== -1 || d?.name?.toLowerCase().indexOf(val) !== -1 || !val;
    });
    let listResult: any = [];
    listResult = [...temp];
    temp.forEach((item) => {
      listResult = [...listResult, ...this.findParentBranch(item, this.temp, [])];
    });
    listResult.sort((a, b) => {
      if (a.code.toLowerCase() < b.code.toLowerCase()) {
        return -1;
      }
      if (a.code.toLowerCase() > b.code.toLowerCase()) {
        return 1;
      }
      return 0;
    });

    listResult = [...new Set(listResult)];
    listResult.forEach((item) => {
      if (!item.parentCode) {
        item.isDisabledAll =
          listResult.filter((itemOld) => {
            return itemOld.code === item.code || itemOld.parentCode === item.code;
          }).length !==
          item.children + 1;
      }
    });
    // update the rows
    this.rows = listResult;
    this.listChoose.forEach((item) => {
      const itemOld = this.rows.find((row) => {
        return row.code === item.code;
      });
      if (itemOld) {
        item = itemOld;
      }
    });
  }

  findParentBranch(item: any, listAll: any[], listResult: any[]) {
    if (item.parentCode) {
      const itemOld = listAll.find((obj) => {
        return obj.code === item.parentCode;
      });
      if (itemOld) {
        listResult.push(itemOld);
        this.findParentBranch(itemOld, listAll, listResult);
      }
    }
    return listResult;
  }

  choose() {
    this.modalActive.close(this.listChoose);
  }

  closeModal() {
    this.modalActive.close(false);
  }
}
