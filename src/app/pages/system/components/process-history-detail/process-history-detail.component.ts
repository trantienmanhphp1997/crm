import { Component, OnInit, Inject, ViewChild, Input, HostBinding } from '@angular/core';
import { ProcessManagementService } from '../../services/process-management.service';
import { global } from '@angular/compiler/src/util';
import { TranslateService } from '@ngx-translate/core';
import _ from 'lodash';
import { Utils } from '../../../../core/utils/utils';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifyMessageService } from '../../../../core/components/notify-message/notify-message.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { CategoryService } from '../../services/category.service';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { SessionService } from 'src/app/core/services/session.service';
import { EncrDecrService } from '../../../../core/services/encr-decr.service';

@Component({
  selector: 'process-history-detail',
  templateUrl: './process-history-detail.component.html',
  styleUrls: ['./process-history-detail.component.scss'],
})
export class ProcessHistoryDetailComponent implements OnInit {
  @Input() type: string;
  @Input() processHistoryId: string;

  constructor(
    private api: ProcessManagementService,
    private translate: TranslateService,
    private route: ActivatedRoute,
    private messageService: NotifyMessageService,
    private router: Router,
    private commonApi: CategoryService,
    private sessionService: SessionService,
    private enc: EncrDecrService
  ) {
    this.translate.get(['fields', 'messageTable', 'notificationMessage']).subscribe((result) => {
      this.fields = _.get(result, 'fields');
      this.messages = _.get(result, 'messageTable');
      this.notificationMessage = _.get(result, 'notificationMessage');
    });
  }
  isLoading = false;
  fields: string;
  messages: any;
  pageable: Pageable;
  params: any = {
    size: global.userConfig.pageSize,
    page: 0,
    status: '',
  };
  notificationMessage: any;

  prevParams = this.params;
  models: Array<any>;
  offset: number;
  count: number;
  isSearch: boolean;
  limit: number = 10;
  isFisrt: boolean = true;

  @ViewChild('table') table: DatatableComponent;

  @HostBinding('class.app__right-content') appRightContent = true;

  ngOnInit(): void {
    this.isLoading = false;
    this.search();
  }

  ftime: boolean = true;
  paging($event) {
    if (this.isFisrt) {
      return;
    }
    this.params.page = _.get($event, 'offset');
    this.reload(false);
  }

  search() {
    this.reload(true);
  }

  reload(isSearch?: boolean) {
    this.isLoading = true;
    let params: any = {};
    if (isSearch) {
      this.params.page = 0;
    } else {
      params = this.prevParams;
    }
    Object.keys(this.params).forEach((key) => {
      params[key] = this.params[key];
    });
    params.page = this.params.page;
    params.size = this.params.size;
    this.api.fetch(params).subscribe(
      (response) => {
        this.isFisrt = false;
        this.isLoading = false;
        this.models = _.get(response, 'content');
        this.pageable = {
          totalElements: _.get(response, 'totalElements'),
          totalPages: _.get(response, 'totalPages'),
          currentPage: _.get(response, 'number'),
          size: _.get(global, 'userConfig.pageSize'),
        };
        this.prevParams = params;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  getValue(row: any, key: string) {
    return _.get(row, key);
  }
}
