import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { global } from '@angular/compiler/src/util';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import { ScreenType, SessionKey } from 'src/app/core/utils/common-constants';
import * as _ from 'lodash';
import { AddTitleModalComponent } from '../add-title-modal/add-title-modal.component';
import { TitleRelationshipService } from '../../services/title-relationship.service';

@Component({
  selector: 'title-relationship-group-create-component',
  templateUrl: './title-relationship-group-create.component.html',
  styleUrls: ['./title-relationship-group-create.component.scss'],
})
export class TitleRelationshipGroupCreate extends BaseComponent implements OnInit {
  formSearch = this.fb.group({
    divisionCode: [''],
  });

  limit = global.userConfig.pageSize;

  listDataManager = [];
  listDataEmployee = [];
  ColumnMode = ColumnMode;
  @ViewChild('tableManager') tableManager: DatatableComponent;
  @ViewChild('tableEmployee') tableEmployee: DatatableComponent;
  // listDivision = [];

  pageableManager: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };
  pageableEmployee: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };

  paramsTableManager = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };
  paramsTableEmployee = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };

  dataEmployee = [];
  dataManager = [];

  listDivision = [];
  listSelectedTitle = [];
  screenType: any;
  id: any;
  bizline: any;

  listDataManagerOld = [];
  listDataEmployeeOld = [];
  titleFuction: string;

  constructor(injector: Injector, private api: TitleRelationshipService) {
    super(injector);
    this.route?.data?.subscribe((data) => {
      this.screenType = data?.type;
    });
    this.id = _.get(this.route.snapshot.params, 'id');
    this.bizline = _.get(this.route.snapshot.params, 'bizline');
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.translate.get('titleRelationship.title' + this.screenType).subscribe((res) => {
      this.titleFuction = res;
    });

    const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
    this.listDivision =
      divisionOfUser?.map((item) => {
        return { code: item.code, name: `${item.code || ''} - ${item.name || ''}` };
      }) || [];
    if (this.screenType === ScreenType.Create) {
      this.formSearch.controls.divisionCode.setValue(_.first(this.listDivision)?.code, { emitEvent: false });
      this.isLoading = false;
    } else if (this.screenType === ScreenType.Detail || this.screenType === ScreenType.Update) {
      this.api.detailTitle(this.id, this.bizline).subscribe(
        (res) => {
          this.isLoading = false;
          this.formSearch.controls.divisionCode.setValue(res?.bizline);
          this.formSearch.controls.divisionCode.disable();
          this.listDataEmployeeOld = [...res.typeViewRM];
          this.dataEmployee = res.typeViewRM;
          this.dataEmployee?.sort((a, b) => {
            return a?.isParent === b?.isParent ? 0 : a?.isParent ? -1 : 1;
          });
          this.mapDataEmployee();

          this.dataEmployee.forEach((element) => {
            if (element.isParent) {
              this.listSelectedTitle.push(element);
            }
          });
          this.listDataManagerOld = [...res.typeViewCBQL];
          this.dataManager = res.typeViewCBQL;
          this.dataManager?.sort((a, b) => {
            return a?.isParent === b?.isParent ? 0 : a?.isParent ? -1 : 1;
          });
          this.mapDataManager();
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
          this.messageService.error(this.notificationMessage.error);
        }
      );
    }
  }

  ngAfterViewInit() {
    this.formSearch.controls.divisionCode.valueChanges.subscribe(() => {
      this.dataEmployee = [];
      this.paramsTableEmployee.page = 0;
      this.mapDataEmployee();

      this.dataManager = [];
      this.paramsTableManager.page = 0;
      this.mapDataManager();

      this.listSelectedTitle = [];
    });
  }

  setPage(pageInfo, type) {
    if (type === 'success') {
      this.paramsTableEmployee.page = pageInfo.offset;
      this.mapDataEmployee();
    } else {
      this.paramsTableManager.page = pageInfo.offset;
      this.mapDataManager();
    }
  }

  mapDataEmployee() {
    const total = this.dataEmployee.length;
    this.pageableEmployee.totalElements = total;
    this.pageableEmployee.totalPages = Math.floor(total / this.pageableEmployee.size);
    this.pageableEmployee.currentPage = this.paramsTableEmployee.page;
    const start = this.paramsTableEmployee.page * this.paramsTableEmployee.size;
    this.listDataEmployee = this.dataEmployee?.slice(start, start + this.paramsTableEmployee.size);
  }

  addTitle(isEmployee: boolean) {
    const modalConfirm = this.modalService.open(AddTitleModalComponent, {
      windowClass: 'list__rm-modal',
    });
    modalConfirm.componentInstance.divisionCode = this.formSearch.controls.divisionCode.value;
    modalConfirm.componentInstance.listSelectedOld = isEmployee ? this.dataEmployee : this.dataManager;
    modalConfirm.componentInstance.screenTypeTitle = this.screenType;
    modalConfirm.componentInstance.isManager = isEmployee;
    modalConfirm.result
      .then((res) => {
        if (res) {
          if (isEmployee) {
            this.dataEmployee = res.listSelected?.map((item) => {
              return {
                ...item,
                isParent: item.isParent ? 1 : 0,
              };
            });
            if (
              this.listSelectedTitle.length > 0 &&
              this.dataEmployee.filter((item) => item.titleId === this.listSelectedTitle[0].titleId).length === 0
            ) {
              this.listSelectedTitle = [];
            }
            this.mapDataEmployee();
          } else {
            this.dataManager = res.listSelected?.map((item) => {
              return {
                ...item,
                isParent: item.titleId === this.listSelectedTitle[0]?.titleId ? 1 : 0,
              };
            });
            if (
              this.listSelectedTitle.length > 0 &&
              this.dataManager.filter((item) => item.titleId === this.listSelectedTitle[0].titleId).length === 0
            ) {
              this.listSelectedTitle = [];
            }
            this.mapDataManager();
          }
        }
      })
      .catch(() => {});
  }

  removeRecordEmployee(item) {
    if (this.listSelectedTitle.filter((element) => element.titleId === item.titleId).length > 0) {
      _.remove(this.listSelectedTitle, (i) => i.titleId === item.titleId);
      _.remove(this.dataManager, (i) => i.titleId === item.titleId);
      this.mapDataManager();
    }
    _.remove(this.dataEmployee, (i) => i.titleId === item.titleId);
    _.remove(this.listDataEmployee, (i) => i.titleId === item.titleId);
    if (this.listDataEmployee.length === 0 && this.pageableEmployee.currentPage > 0) {
      this.paramsTableEmployee.page -= 1;
    }
    this.listDataEmployee = [...this.listDataEmployee];
    this.mapDataEmployee();
  }

  onCheckboxEmp(item) {
    if (
      this.listSelectedTitle.length > 0 &&
      this.listSelectedTitle.filter((element) => element.titleId === item.titleId).length > 0
    ) {
      return;
    }
    this.listSelectedTitle = [];
    this.listSelectedTitle.push(item);
    this.dataManager.unshift(item);
    this.dataManager = _.uniqBy(this.dataManager, 'titleId');
    this.mapDataManager();
  }

  isCheckedTitleEmp(item) {
    if (this.listSelectedTitle.filter((element) => element.titleId === item.titleId).length > 0 || item.isParent) {
      return true;
    }
    return false;
  }

  // CBQL

  mapDataManager() {
    const total = this.dataManager.length;
    this.pageableManager.totalElements = total;
    this.pageableManager.totalPages = Math.floor(total / this.pageableManager.size);
    this.pageableManager.currentPage = this.paramsTableManager.page;
    const start = this.paramsTableManager.page * this.paramsTableManager.size;
    this.listDataManager = this.dataManager?.slice(start, start + this.paramsTableManager.size);
  }

  removeRecordManager(item) {
    if (this.listSelectedTitle.filter((element) => element.titleId === item.titleId).length > 0) {
      _.remove(this.listSelectedTitle, (i) => i.titleId === item.titleId);
      _.remove(this.dataEmployee, (i) => i.titleId === item.titleId);
      this.mapDataEmployee();
    }
    _.remove(this.dataManager, (i) => i.titleId === item.titleId);
    _.remove(this.listDataManager, (i) => i.titleId === item.titleId);
    if (this.listDataManager.length === 0 && this.pageableManager.currentPage > 0) {
      this.paramsTableManager.page -= 1;
    }
    this.listDataManager = [...this.listDataManager];
    this.mapDataManager();
  }

  onCheckboxManager(item) {
    if (
      this.listSelectedTitle.length > 0 &&
      this.listSelectedTitle.filter((element) => element.titleId === item.titleId).length > 0
    ) {
      return;
    }
    this.listSelectedTitle = [];
    this.listSelectedTitle.push(item);
    this.dataEmployee.unshift(item);
    this.dataEmployee = _.uniqBy(this.dataEmployee, 'titleId');
    this.mapDataEmployee();
  }

  isCheckedTitleManager(item) {
    if (this.listSelectedTitle?.filter((element) => element.titleId === item.titleId).length > 0 || item.isParent) {
      return true;
    }
    return false;
  }

  saveTitle() {
    this.isLoading = true;
    if (this.screenType === ScreenType.Create) {
      if (this.listSelectedTitle.length === 0) {
        this.messageService.error(this.notificationMessage.selectTitleParent);
        this.isLoading = false;
        return;
      }
      const typeViewRM = this.dataEmployee?.map((item) => {
        return {
          typeViewManager: 0,
          titleId: item.titleId,
          isParent: this.listSelectedTitle[0].titleId === item.titleId ? 1 : 0,
        };
      });

      const typeViewCBQL = this.dataManager?.map((item) => {
        return {
          typeViewManager: 1,
          titleId: item.titleId,
          isParent: this.listSelectedTitle[0].titleId === item.titleId ? 1 : 0,
        };
      });
      if (typeViewRM.length === 1 && typeViewCBQL.length === 1) {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.requiredChooseSubtitle);
        return;
      }

      const paramCreate = {
        bizline: this.formSearch.controls.divisionCode.value,
        typeViewCBQL: typeViewCBQL,
        typeViewRM: typeViewRM,
      };
      this.api.createTitle(paramCreate).subscribe(
        () => {
          this.messageService.success(this.notificationMessage.success);
          this.back();
          this.isLoading = false;
        },
        (e) => {
          this.isLoading = false;
          if (e.error.code) {
            this.messageService.error(e.error.description);
          }
        }
      );
    } else {
      // EMP

      let listEmp = [];
      this.dataEmployee.forEach((element) => {
        listEmp.push(this.listDataEmployeeOld.filter((item) => item.titleId === element.titleId)[0]);
      });
      listEmp = listEmp.filter((item) => item);
      let listCreateEmp = this.dataEmployee.filter((item) =>
        _.isEmpty(listEmp.find((i) => item.titleId === i.titleId))
      );
      listCreateEmp = listCreateEmp.map((item) => {
        return {
          typeViewManager: 0,
          titleId: item.titleId,
          isParent: 0,
          action: 'CREATE',
        };
      });
      let listDeleteEmp = this.listDataEmployeeOld.filter((item) =>
        _.isEmpty(listEmp.find((i) => item.titleId === i.titleId))
      );
      listDeleteEmp = listDeleteEmp.map((item) => {
        return {
          id: item.id,
          typeViewManager: item.typeViewManager,
          titleId: item.titleId,
          isParent: 0,
          action: 'DELETE',
        };
      });
      const typeViewRM = listEmp
        ?.map((item) => {
          return _.omit(item, ['name', 'code']);
        })
        ?.concat(listDeleteEmp, listCreateEmp);

      //MANAGER

      let listManager = [];
      this.dataManager.forEach((element) => {
        listManager.push(this.listDataManagerOld.filter((item) => item.titleId === element.titleId)[0]);
      });
      listManager = listManager.filter((item) => item);
      let listCreateManager = this.dataManager.filter((item) =>
        _.isEmpty(listManager.find((i) => item.titleId === i.titleId))
      );
      listCreateManager = listCreateManager.map((item) => {
        return {
          typeViewManager: 1,
          titleId: item.titleId,
          isParent: 0,
          action: 'CREATE',
        };
      });
      let listDeleteManager = this.listDataManagerOld.filter((item) =>
        _.isEmpty(listManager.find((i) => item.titleId === i.titleId))
      );
      listDeleteManager = listDeleteManager.map((item) => {
        return {
          id: item.id,
          typeViewManager: item.typeViewManager,
          titleId: item.titleId,
          isParent: 0,
          action: 'DELETE',
        };
      });
      const typeViewCBQL = listManager
        ?.map((item) => {
          return _.omit(item, ['name', 'code']);
        })
        ?.concat(listDeleteManager, listCreateManager);

      if (
        typeViewRM.filter((item) => item.action === 'CREATE' || _.isEmpty(item.action)).length === 1 &&
        typeViewCBQL.filter((item) => item.action === 'CREATE' || _.isEmpty(item.action)).length === 1
      ) {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.requiredChooseSubtitle);
        return;
      }
      const paramUpdate = {
        bizline: this.formSearch.controls.divisionCode.value,
        typeViewCBQL: typeViewCBQL,
        typeViewRM: typeViewRM,
      };

      this.api.updateTitle(this.id, paramUpdate).subscribe(
        () => {
          this.messageService.success(this.notificationMessage.success);
          this.back();
          this.isLoading = false;
        },
        (e) => {
          this.isLoading = false;
          if (e.error.code) {
            this.messageService.error(e.error.description);
          }
        }
      );

      // this.isLoading = false;
      // return;
    }
  }

  isDisabled() {
    return this.screenType === ScreenType.Update || this.screenType === ScreenType.Detail;
  }

  isDelete(item) {
    if (this.screenType === ScreenType.Detail) {
      return false;
    }
    return (this.screenType === ScreenType.Update && !item.isParent) || this.screenType === ScreenType.Create;
  }
}
