import { Component, OnInit, Injector } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { functionUri } from 'src/app/core/utils/common-constants';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { BaseComponent } from 'src/app/core/components/base.component';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { CustomKeycloakService } from '../../services/custom-keycloak.service';
import { CategoryService } from '../../services/category.service';

@Component({
  selector: 'app-manipulation-category-update',
  templateUrl: './manipulation-category-update.component.html',
  styleUrls: ['./manipulation-category-update.component.scss'],
  providers: [NgbModal],
})
export class ManipulationCategoryUpdateComponent extends BaseComponent implements OnInit {
  data: any;
  isLoading = false;
  isUpdate = true;
  form = this.fb.group({
    id: [''],
    code: ['', [CustomValidators.required, CustomValidators.code]],
    name: ['', [CustomValidators.required]],
    description: [''],
  });

  constructor(injector: Injector, private categoryService: CategoryService) {
    super(injector);
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.categoryService.getScopeById(id).subscribe(
      (data) => {
        this.data = data;
        this.form.patchValue(this.data);
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  confirmDialog() {
    if (this.form.valid) {
      const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result
        .then((res) => {
          if (res) {
            this.isLoading = true;
            const data = cleanDataForm(this.form);
            this.categoryService.updateScope(data).subscribe(
              () => {
                this.location.back();
                this.messageService.success(this.notificationMessage.success);
              },
              (e) => {
                this.isLoading = false;
                if (e?.error?.description) {
                  this.messageService.error(e?.error?.description);
                } else {
                  this.messageService.error(this.notificationMessage.error);
                }
              }
            );
          }
        })
        .catch(() => {});
    } else {
      validateAllFormFields(this.form);
    }
  }

  back() {
    this.location.back();
  }
}
