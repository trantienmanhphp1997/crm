import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManipulationCategoryUpdateComponent } from './manipulation-category-update.component';

describe('ManipulationCategoryUpdateComponent', () => {
  let component: ManipulationCategoryUpdateComponent;
  let fixture: ComponentFixture<ManipulationCategoryUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManipulationCategoryUpdateComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManipulationCategoryUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
