import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { global } from '@angular/compiler/src/util';
import { Component, OnInit, ViewEncapsulation, ViewChild, AfterViewInit, Input, HostBinding } from '@angular/core';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import { Utils } from '../../../../core/utils/utils';
import _ from 'lodash';
import { NotifyMessageService } from '../../../../core/components/notify-message/notify-message.service';

@Component({
	selector: 'choose-tree-branches',
	templateUrl: './choose-tree-branches.component.html',
	styleUrls: ['./choose-tree-branches.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class ChooseTreeBranchesModalComponent implements OnInit, AfterViewInit {
	isLoading = false;
	rows = [];
	temp = [];
	listChoose = [];
	@Input() listBranchOld: Array<any>;
	@Input() listBranch: Array<any>;
	ColumnMode = ColumnMode;
	textSearch = '';
	isAllBranch: boolean;
	@ViewChild(DatatableComponent) public table: DatatableComponent;
	@HostBinding('class.choose-tree-branches') chooseBranchesModal = true;

	constructor(private modalActive: NgbActiveModal, private messageService: NotifyMessageService) { }

	ngOnInit(): void {
		if (Utils.isArrayNotEmpty(this.listBranchOld)) {
			this.listChoose = _.chain(this.listBranch)
				.filter((x) => this.listBranchOld.includes(x.code))
				.value();
		}
		if (Utils.isArrayNotEmpty(this.listBranch)) {
			this.isAllBranch = _.size(this.listChoose) === _.size(this.listBranch);
			let listChildren: any[];
			for (const item of this.listBranch) {
				listChildren = [];

				// if (!item.parentCode || item.code === item.parentCode) {
				// 	delete item.parentCode;
				listChildren = _.chain(this.listBranch)
					.filter((x) => x.parentCode === item.code)
					.value();
				
				if (listChildren.length > 0) {
					// delete item.parentCode;
					const isCheckedAll =
						_.size(
							_.chain(this.listBranchOld)
								.filter((x) => _.findIndex(listChildren, (i) => i.code === x) !== -1)
								.value()
						) === _.size(listChildren) && _.findIndex(this.listBranchOld, (code) => code === item.code) !== -1;
					item.isCheckedAll = this.isAllBranch || isCheckedAll;
					// item.isDisabledAll = this.isAllBranch;
				} else {
					var value = _.chain(this.listBranch)
						.filter((x) => x.code === item.parentCode)
						.first()
						.value();
					if (Utils.isNotNull(value)) {
						item.parentName = value.name;
					}
				}
				item['children'] = _.size(listChildren);
				item['listChildren'] = listChildren;
				
				item['isChecked'] =
					this.listBranchOld?.findIndex((code) => {
						return code === item.code;
					}) > -1;
				// item['treeStatus'] = _.size(listChildren) > 0 ? 'expanded' : 'disabled';
				
				item['treeStatus'] = (item.type !== 3 && _.size(listChildren)) > 0 ? 'expanded':'disabled';		
				this.rows.push(item);
			}
			this.rows.sort((a, b) => {
				if (a.code.toLowerCase() < b.code.toLowerCase()) {
					return -1;
				}
				if (a.code.toLowerCase() > b.code.toLowerCase()) {
					return 1;
				}
				return 0;
			});
			this.temp = [...this.rows];
			this.rows = [...this.rows];
		}
	}

	ngAfterViewInit() {
		this.table.messages = global?.messageTable;
	}

	search() {
		this.updateFilter();
	}

	onCheckboxBranch(row, isChecked) {
		row.isChecked = isChecked;
		const parentCode = row.parentCode ? row.parentCode : row.code;
		if (isChecked) {
			this.listChoose.push(row);
		} else {
			this.isAllBranch = false;
			this.listChoose = this.listChoose.filter((item) => {
				return item.code !== row.code;
			});
		}
		this.rows?.find((item) => {
			if (parentCode === item.code) {
				const listChildren = _.chain(this.listBranch)
					.filter((x) => x.parentCode === item.code)
					.value();
				const isCheckedAll =
					_.size(
						_.chain(this.listChoose)
							.filter((x) => _.findIndex(listChildren, (i) => i.code === x.code) !== -1)
							.value()
					) === _.size(listChildren) &&
					_.findIndex(this.listChoose, (itemChoose) => itemChoose.code === item.code) !== -1;
				item.isCheckedAll = this.isAllBranch || isCheckedAll;
				// item.isDisabledAll = false;
			}
		});
	}

	onCheckboxBranchAll(row, isChecked) {
		this.rows.forEach((item) => {
			if (item.code === row.code || item.parentCode === row.code) {
				item.isChecked = isChecked;
				item.isCheckedAll = isChecked;
				if (isChecked) {
					if (
						this.listChoose.findIndex((itemChoose) => {
							return itemChoose.code === item.code;
						}) === -1
					) {
						this.listChoose.push(item);
						
						if(row.type === 1){
							if((item.type === 2||item.parentCode === 'HO')  && item.listChildren.length > 0){
								item.listChildren.forEach(element => {
									element.isChecked = isChecked;
									this.listChoose.push(element);
								});
							}
						}
					}
				} else {
					this.listChoose = _.chain(this.listChoose)
						.filter((x) => x.code !== item.code)
						.value();
					if(row.type === 1){
						if((item.type === 2||item.parentCode === 'HO') && item.listChildren.length > 0){
							item.listChildren.forEach(element => {
								element.isChecked = isChecked;
								this.listChoose = _.chain(this.listChoose)
								.filter((x) => x.code !== element.code)
								.value();
							});
						}
					}
				}
				return item;
			}
		});
	}

	chooseAllbranch(isChecked) {
		this.isAllBranch = isChecked;
		this.listChoose = [];
		this.rows.forEach((item) => {
			if ( item.listChildren.length > 0) {
				// item.isDisabledAll = isChecked;
				item.isCheckedAll = isChecked;
			}
			item.isChecked = isChecked;
			item.isDisabled = false;
			if (isChecked) {
				this.listChoose.push(item);
			}
		});
	}

	onTreeAction(event: any) {
		const row = event.row;
		if (row.treeStatus === 'collapsed') {
			row.treeStatus = 'expanded';
		} else {
			row.treeStatus = 'collapsed';
		}
		this.rows = [...this.rows];
	}

	updateFilter() {
		const val = this.textSearch.trim().toLowerCase();
		this.rows = [];

		// filter our data
		const temp = this.temp.filter(function (d) {
			return d.code.toLowerCase().indexOf(val) !== -1 || d.name.toLowerCase().indexOf(val) !== -1 || !val;
		});
		let listResult: any = [];
		listResult = [...temp];
		temp.forEach((item) => {
			listResult = [...listResult, ...this.findParentBranch(item, this.temp, [])];
		});
		listResult.sort((a, b) => {
			if (a.code.toLowerCase() < b.code.toLowerCase()) {
				return -1;
			}
			if (a.code.toLowerCase() > b.code.toLowerCase()) {
				return 1;
			}
			return 0;
		});

		listResult = [...new Set(listResult)];
		listResult.forEach((item) => {
			if (!item.parentCode) {
				item.isDisabledAll =
					listResult.filter((itemOld) => {
						return itemOld.code === item.code || itemOld.parentCode === item.code;
					}).length !==
					item.children + 1;
			}
		});
		// update the rows
		this.rows = listResult;
		_.forEach(this.listChoose, (item) => {
			const itemOld = this.rows.find((row) => {
				return row.code === item.code;
			});
			if (itemOld) {
				item = itemOld;
			}
		});
	}

	findParentBranch(item: any, listAll: any[], listResult: any[]) {
		if (item.parentCode) {
			const itemOld = listAll.find((obj) => {
				return obj.code === item.parentCode;
			});
			if (itemOld) {
				listResult.push(itemOld);
				this.findParentBranch(itemOld, listAll, listResult);
			}
		}
		return listResult;
	}

	choose() {
		this.modalActive.close(this.listChoose);
	}

	closeModal() {
		this.modalActive.close(false);
	}
}
