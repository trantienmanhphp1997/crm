import { forkJoin } from 'rxjs';
import { Component, OnInit, Injector } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { maxInt32 } from 'src/app/core/utils/common-constants';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { CategoryService } from '../../services/category.service';
import { UserService } from '../../services/users.service';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { RmProfileService } from 'src/app/core/services/rm-profile.service';

declare const $: any;

@Component({
  selector: 'app-role-category-update',
  templateUrl: './role-category-update.component.html',
  styleUrls: ['./role-category-update.component.scss'],
  providers: [NgbModal],
})
export class RoleCategoryUpdateComponent extends BaseComponent implements OnInit {
  isLoading = false;
  data: any;
  listUser = [];
  usersOfRole = [];
  form = this.fb.group({
    id: [''],
    code: [{ value: '', disabled: true }, [CustomValidators.required, CustomValidators.code]],
    name: ['', CustomValidators.required],
    description: [''],
  });
  pageable: Pageable;
  limit = 10;
  paramUserSearch = {
    size: this.limit,
    page: 0,
    searchAdvanced: '',
    isActive: true,
  };
  textFilter = '';
  listBranches: any;
  isList = false;
  isUpdate = true;
  listRoleOfUser = [];

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private userService: UserService,
    private rmProfileService: RmProfileService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.isLoading = true;
    const roleName = this.route.snapshot.paramMap.get('name');
    forkJoin([
      // this.categoryService.getRoleById(roleName),
      // this.categoryService.getUserByRoleId(roleName),
      this.categoryService.getRoleByName(roleName),
      this.categoryService.getUserByRoleName(roleName),
      this.userService.searchUser(this.paramUserSearch),
      this.categoryService.searchBranches({ page: 0, size: maxInt32 }),
    ]).subscribe(
      ([itemRole, usersOfRole, listUser, branches]) => {
        console.log(itemRole, usersOfRole);
        this.data = itemRole;
        this.form.controls.code.setValue(this.data.name);
        this.form.controls.name.setValue(this.data.description);
        if (this.data.attributes?.description) {
          this.form.controls.description.setValue(this.data.attributes?.description[0]);
        }
        this.listUser = listUser.content;
        this.listBranches = branches.content;
        for (const user of usersOfRole) {
          this.usersOfRole.push(user.username);
        }
        this.pageable = {
          totalElements: listUser.totalElements,
          totalPages: listUser.totalPages,
          currentPage: listUser.number,
          size: this.limit,
        };
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  search(isSearch: boolean) {
    if (isSearch) {
      this.paramUserSearch.page = 0;
    }
    this.paramUserSearch.searchAdvanced = this.textFilter.trim();
    this.userService.searchUser(this.paramUserSearch).subscribe(
      (result) => {
        if (result) {
          this.listUser = result.content || [];
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: this.limit,
          };
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    this.paramUserSearch.page = pageInfo.offset;
    this.search(false);
  }

  onCheckboxFn(item, isChecked) {
    this.isLoading = true;
    this.rmProfileService.getRolesByUser(item.username).subscribe(
      (roles) => {
        this.listRoleOfUser = roles?.map((i) => i.name) || [];
        if (isChecked) {
          this.listRoleOfUser.push(this.data.name);
        } else {
          this.listRoleOfUser.splice(this.listRoleOfUser.indexOf(this.data.name), 1);
        }
        this.rmProfileService.updateRolesByUser(item.username, this.listRoleOfUser).subscribe(
          () => {
            this.isLoading = false;
            this.messageService.success(this.notificationMessage.success);
            if (isChecked) {
              this.usersOfRole.push(item.username);
            } else {
              this.usersOfRole.splice(this.usersOfRole.indexOf(item.username), 1);
            }
          },
          () => {
            this.isLoading = false;
            this.messageService.error(this.notificationMessage.error);
          }
        );
      },
      () => {
        this.messageService.error(this.notificationMessage.E001);
        this.isLoading = false;
      }
    );
  }

  isChecked(item) {
    return (
      this.usersOfRole.findIndex((username) => {
        return username === item.username;
      }) > -1
    );
  }

  chooseTab() {
    this.isList = !this.isList;
  }

  confirmDialog() {
    if (this.form.valid) {
      const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result
        .then((res) => {
          if (res) {
            this.isLoading = true;
            const formValue = cleanDataForm(this.form);
            const data = {
              id: this.data.id,
              code:this.data.name,
              name: formValue.name,
              description: formValue.description,
            };
            // this.categoryService.updateRole(data).subscribe(
            this.categoryService.updatePolicy(data).subscribe(
              () => {
                this.location.back();
                this.messageService.success(this.notificationMessage.success);
              },
              (e) => {
                if (e?.error?.description) {
                  this.messageService.error(e?.error?.description);
                } else {
                  this.messageService.error(this.notificationMessage.error);
                }
                this.isLoading = false;
              }
            );
          }
        })
        .catch(() => {});
    } else {
      validateAllFormFields(this.form);
    }
  }

  back() {
    this.location.back();
  }
}
