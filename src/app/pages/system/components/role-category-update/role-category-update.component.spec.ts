import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleCategoryUpdateComponent } from './role-category-update.component';

describe('RoleCategoryUpdateComponent', () => {
  let component: RoleCategoryUpdateComponent;
  let fixture: ComponentFixture<RoleCategoryUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RoleCategoryUpdateComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleCategoryUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
