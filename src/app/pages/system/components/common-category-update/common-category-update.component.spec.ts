import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonCategoryUpdateComponent } from './common-category-update.component';

describe('CommonCategoryUpdateComponent', () => {
  let component: CommonCategoryUpdateComponent;
  let fixture: ComponentFixture<CommonCategoryUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CommonCategoryUpdateComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonCategoryUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
