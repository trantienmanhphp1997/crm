import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, ViewEncapsulation, Injector } from '@angular/core';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { BaseComponent } from 'src/app/core/components/base.component';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { CategoryService } from '../../services/category.service';

@Component({
  selector: 'app-common-category-update',
  templateUrl: './common-category-update.component.html',
  styleUrls: ['./common-category-update.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CommonCategoryUpdateComponent extends BaseComponent implements OnInit {
  isLoading = false;
  data: any;
  isUpdate: boolean;
  form = this.fb.group({
    id: [''],
    code: ['', [CustomValidators.required, CustomValidators.code]],
    name: ['', CustomValidators.required],
    value: ['', CustomValidators.required],
    commonCategoryCode: [''],
    orderNum: [''],
    isDefault: [''],
    description: [''],
  });

  constructor(injector: Injector, private categoryService: CategoryService, private modalActive: NgbActiveModal) {
    super(injector);
  }

  ngOnInit(): void {
    this.form.patchValue(this.data);
    if (!this.isUpdate) {
      this.form.disable();
    }
  }

  confirmDialog() {
    if (this.form.valid) {
      const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result
        .then((res) => {
          if (res) {
            this.isLoading = true;
            const data = cleanDataForm(this.form);
            this.categoryService.updateCommonCategory(data).subscribe(
              () => {
                this.modalActive.close(true);
                this.messageService.success(this.notificationMessage.success);
              },
              (e) => {
                if (e?.error) {
                  this.messageService.warn(e?.error?.description);
                } else {
                  this.messageService.error(this.notificationMessage.error);
                }
                this.isLoading = false;
              }
            );
          }
        })
        .catch(() => {});
    } else {
      validateAllFormFields(this.form);
    }
  }

  closeModal() {
    this.modalActive.close(false);
  }
}
