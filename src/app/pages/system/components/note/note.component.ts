import { forkJoin, of } from 'rxjs';
import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { cleanDataForm } from 'src/app/core/utils/function';
import { FunctionCode } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { ExportDataApi } from '../../services/export-data.api';
import { CommonCategoryService } from 'src/app/core/services/common-category.service';
import { CommonCategory } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss'],
})
export class NoteComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listDB = [];
  formSearch = this.fb.group({
    schema: '', // DB name
    command: '', // Query
  });
  prevParams: any;
  prop: any;
  notificationMessage: any;

  constructor(injector: Injector, private commonCategoryService: CommonCategoryService, private exportDataApi: ExportDataApi) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.NOTE}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.prop = this.sessionService.getSessionData(FunctionCode.NOTE);
    console.log('this.prop', this.prop);

    if (this.prop) {
      this.prevParams = { ...this.prop?.prevParams };
      this.formSearch.patchValue(this.prevParams);
      this.listDB = this.prop?.listDB || [];
      this.isLoading = false;
    } else {
      forkJoin([
        this.commonCategoryService.getCommonCategory(CommonCategory.DB_SCHEMA).pipe(catchError((e) => of(undefined))),
      ]).subscribe(([resDBSchema]) => {
        this.listDB = resDBSchema?.content || [];
        this.listDB.length && this.formSearch.controls.schema.setValue(this.listDB[0].value)

        this.prop = {
          listDB: this.listDB,
          prevParams: { schema: this.listDB.length && this.listDB[0].value || '' }
        };

        this.sessionService.setSessionData(FunctionCode.NOTE, this.prop);
        this.isLoading = false;
      });
    }
  }

  update() {
    let params: any = {};
    params = cleanDataForm(this.formSearch);
    if (!params.command) {
      this.messageService.error(this.notificationMessage.please_input_query);
      return;
    }
    this.isLoading = true;
    this.exportDataApi.updateData(params).subscribe(
      (result) => {
        if (result) {
          this.prevParams = params;
          this.prop.prevParams = params;
        }
        this.sessionService.setSessionData(FunctionCode.NOTE, this.prop);

        this.messageService.success(this.notificationMessage.success);
        this.isLoading = false;
      },
      (err) => {
        const message = err?.error?.description || _.get(this.notificationMessage, 'error');
        this.messageService.error(message);
        this.isLoading = false;
      }
    );
  }
}
