import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LaneService } from '../../services/lanes.service';
import { FormGroup } from '@angular/forms';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';

@Component({
  selector: 'app-lanes-config-detail',
  templateUrl: './lanes-config-detail.component.html',
  styleUrls: ['./lanes-config-detail.component.scss'],
})
export class LanesConfigDetailComponent implements OnInit {
  name: string;
  data: any;
  form: FormGroup;

  constructor(
    private modal: NgbActiveModal,
    private laneService: LaneService,
    private modalService: NgbModal,
    private messageService: NotifyMessageService
  ) {}

  ngOnInit(): void {
    this.name = this.data.description;
  }

  closeModal() {
    this.modal.close(false);
  }

  confirmDialog() {
    this.modalService
      .open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' })
      .result.then((result) => {
        if (result) {
          this.update();
        }
      })
      .catch(() => {});
  }

  update() {
    this.laneService.update(this.form.value).subscribe((result) => {
      if (result) {
        this.modal.close(true);
        this.messageService.success('Cập nhật thành công!');
      }
    });
  }
}
