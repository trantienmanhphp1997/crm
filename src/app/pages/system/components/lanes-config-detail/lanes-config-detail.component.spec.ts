import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LanesConfigDetailComponent } from './lanes-config-detail.component';

describe('LanesConfigDetailComponent', () => {
  let component: LanesConfigDetailComponent;
  let fixture: ComponentFixture<LanesConfigDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LanesConfigDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LanesConfigDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
