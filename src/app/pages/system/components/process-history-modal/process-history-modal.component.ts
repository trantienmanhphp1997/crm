import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, HostBinding, Input, OnInit } from '@angular/core';
import _ from 'lodash';

@Component({
  selector: 'app-process-history-modal',
  templateUrl: './process-history-modal.component.html',
  styleUrls: ['./process-history-modal.component.scss']
})
export class ProcessHistoryModalComponent implements OnInit {
  constructor(private modalActive: NgbActiveModal) { }
  @Input() processHistoryId: string;
  isLoading: boolean;
  tab_index: number = 0;
  @HostBinding('class.process-history-detail') processHistoryDetail = true;

  ngOnInit(): void {
    console.log(this.processHistoryId);
  }

  close() {
    this.modalActive.close();
  }

  onTabChanged($event) {
    this.tab_index = _.get($event, 'index');
  }

}
