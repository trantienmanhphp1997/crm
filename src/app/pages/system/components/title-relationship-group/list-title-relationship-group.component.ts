import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { global } from '@angular/compiler/src/util';
import { SessionKey } from 'src/app/core/utils/common-constants';
import _ from 'lodash';
import { TitleRelationshipService } from '../../services/title-relationship.service';

@Component({
  selector: 'list-title-relationship-group-component',
  templateUrl: './list-title-relationship-group.component.html',
  styleUrls: ['./list-title-relationship-group.component.scss'],
})
export class ListTitleRelationshipGroup extends BaseComponent implements OnInit {
  isLoading = false;
  @ViewChild('table') table: DatatableComponent;
  limit = global.userConfig.pageSize;
  pageable: Pageable;
  params: any = {
    pageNumber: 0,
    pageSize: global?.userConfig?.pageSize,
  };

  constructor(injector: Injector, private titleRelationshipService: TitleRelationshipService) {
    super(injector);
  }
  formSearch = this.fb.group({
    divisionCode: [''],
    titleCode: [''],
    titleName: [''],
  });

  listDivision = [];
  listData = [];

  ngOnInit() {
    this.isLoading = true;
    const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
    this.listDivision =
      divisionOfUser?.map((item) => {
        return { code: item.code, name: `${item.code || ''} - ${item.name || ''}` };
      }) || [];
    this.listDivision.unshift({ code: '', name: this.fields.all });
    this.search(true);
  }

  search(isSearch: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.params.pageNumber = 0;
    }
    const param = {
      blockCode: this.formSearch.controls.divisionCode.value,
      code: this.formSearch.controls.titleCode.value,
      name: this.formSearch.controls.titleName.value,
      page: this.params.pageNumber,
      size: this.params.pageSize,
    };
    this.titleRelationshipService.searchTitleRelationship(param).subscribe(
      (result) => {
        if (result) {
          this.isLoading = false;
          this.listData = result.content || [];
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: this.limit,
          };
        }
      },
      () => {
        this.messageService.error(this.notificationMessage.error);
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.pageNumber = pageInfo.offset;
    this.search(false);
  }

  viewEdit(data) {
    this.router.navigate([this.router.url, 'edit', data.id, data.blockCode], {
      skipLocationChange: true,
    });
  }

  createTitle() {
    this.router.navigate([this.router.url, 'create']);
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      this.router.navigate([this.router.url, 'detail', item.id, item.blockCode]);
    }
  }

  deleteTitle(item) {
    this.confirmService.confirm().then((res) => {
      if (res) {
        this.isLoading = true;
        this.titleRelationshipService.deleteTitle(item.id, item.blockCode).subscribe(
          () => {
            this.isLoading = false;
            this.search(true);
            this.messageService.success(this.notificationMessage.success);
          },
          (e) => {
            this.isLoading = false;
            if (e?.error) {
              this.messageService.error(e?.error?.description);
            } else {
              this.messageService.error(this.notificationMessage.error);
            }
          }
        );
      }
    });
  }
}
