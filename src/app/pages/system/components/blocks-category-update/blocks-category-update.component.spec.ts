import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlocksCategoryUpdateComponent } from './blocks-category-update.component';

describe('BlocksCategoryUpdateComponent', () => {
  let component: BlocksCategoryUpdateComponent;
  let fixture: ComponentFixture<BlocksCategoryUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BlocksCategoryUpdateComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlocksCategoryUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
