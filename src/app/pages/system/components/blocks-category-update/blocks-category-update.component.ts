import { Component, OnInit, AfterViewInit, Injector } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { BaseComponent } from 'src/app/core/components/base.component';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { CategoryService } from '../../services/category.service';

declare const $: any;

@Component({
  selector: 'app-blocks-category-update',
  templateUrl: './blocks-category-update.component.html',
  styleUrls: ['./blocks-category-update.component.scss'],
  providers: [NgbModal],
})
export class BlocksCategoryUpdateComponent extends BaseComponent implements OnInit, AfterViewInit {
  maxLength = {
    code: 50,
    name: 200,
    customerType: 550,
  };
  isLoading = false;
  form = this.fb.group({
    id: [''],
    code: ['', [CustomValidators.required, CustomValidators.code]],
    name: ['', [CustomValidators.required]],
    customerType: [''],
  });
  data: any;
  isUpdate = true;
  customerType = [];

  constructor(injector: Injector, private categoryService: CategoryService) {
    super(injector);
    this.customerType.push({ code: 'INDIV', name: this.fields.customerINDIV });
    this.customerType.push({ code: 'COR', name: this.fields.customerCOR });
  }

  ngOnInit(): void {
    this.isLoading = true;
    const id = this.route.snapshot.paramMap.get('id');
    this.categoryService.getBlocksCategoryById(id).subscribe(
      (item) => {
        this.data = item;
        this.form.patchValue(this.data);
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
    if (!this.isUpdate) {
      this.form.disable();
    }
  }

  ngAfterViewInit() {}

  back() {
    this.location.back();
  }

  confirmDialog() {
    if (!this.isUpdate) {
      return;
    }
    if (this.form.valid) {
      const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result
        .then((res) => {
          if (res) {
            this.isLoading = true;
            const data = cleanDataForm(this.form);
            this.categoryService.updateBlocksCategory(data).subscribe(
              () => {
                this.location.back();
                this.messageService.success(this.notificationMessage.success);
              },
              (e) => {
                if (e?.error) {
                  this.messageService.warn(e?.error?.description);
                } else {
                  this.messageService.error(this.notificationMessage.error);
                }
                this.isLoading = false;
              }
            );
          }
        })
        .catch(() => {});
    } else {
      validateAllFormFields(this.form);
    }
  }
}
