import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPermissionImportComponent } from './user-permission-import.component';

describe('UserPermissionImportComponent', () => {
  let component: UserPermissionImportComponent;
  let fixture: ComponentFixture<UserPermissionImportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPermissionImportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPermissionImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
