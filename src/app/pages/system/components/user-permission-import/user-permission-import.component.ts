import { Component, OnInit, AfterViewInit, Injector, ViewChild } from '@angular/core';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CategoryService } from '../../services/category.service';
import { typeExcel } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'app-user-permission-import',
  templateUrl: './user-permission-import.component.html',
  styleUrls: ['./user-permission-import.component.scss'],
})
export class UserPermissionImportComponent extends BaseComponent implements OnInit, AfterViewInit {
  isLoading = false;
  fileName: string;
  isFile = false;
  fileImport: File;
  files: any;
  ColumnMode = ColumnMode;
  temp = [];
  listData = [];
  prop: any;
  translateObj: any;
  limit = 10;
  checkImport = false;
  page: Pageable;
  params = {
    first: 0,
    max: this.limit,
  };
  isSuccess = false;
  isError = false;
  isUpload = false;
  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(injector: Injector, private categoryService: CategoryService) {
    super(injector);
    this.prop = this.router.getCurrentNavigation()?.extras?.state;
  }

  ngOnInit(): void {
    this.translate.get(['fields', 'notificationMessage']).subscribe((res) => {
      this.translateObj = res;
    });
  }

  ngAfterViewInit() {}

  search(isSearch: boolean) {
    if (isSearch) {
      this.params.first = 0;
      this.params.max = this.limit;
    }
    this.page = {
      totalElements: this.temp.length,
      currentPage: this.params.first === 0 ? 0 : Math.floor(this.params.first / this.limit),
      size: this.limit,
    };
    this.listData = this.temp.slice(this.params.first, this.params.max);
  }

  setPage(pageInfo) {
    this.params.first = pageInfo.offset * this.limit;
    this.params.max = this.params.first + this.limit;
    this.search(false);
  }

  handleFileInput(files) {
    if (files && files.length > 0) {
      if (files?.item(0)?.size > 10485760) {
        this.messageService.warn(this.notificationMessage.ECRM005);
        return;
      }
      if (!typeExcel.includes(files?.item(0)?.type)) {
        this.messageService.error(this.notificationMessage.CANNOT_READ_DATA_FROM_FILE);
        return;
      }
      this.isFile = true;
      this.fileImport = files.item(0);
      this.fileName = files.item(0).name;
    } else {
      this.isFile = false;
    }
  }

  clearFile() {
    this.isUpload = false;
    this.isFile = false;
    this.fileName = null;
    this.fileImport = null;
    this.files = null;
    this.isSuccess = false;
    this.isError = false;
    this.listData = [];
  }

  importFile(type: boolean) {
    if (type && !this.checkImport) {
      return;
    }
    this.checkImport = type;
    if (this.isFile) {
      this.isLoading = true;
      const formData: FormData = new FormData();
      formData.append('file', this.fileImport);
      this.categoryService.importDomainPermission(formData, this.checkImport).subscribe(
        (res) => {
          if (res && res.dataValidateError) {
            this.temp = res.dataValidateError;
            this.search(true);
            this.isError = true;
            this.isSuccess = false;
            this.checkImport = false;
          } else if (res && res.dataExcelSuccesses) {
            this.temp = res.dataExcelSuccesses;
            this.isSuccess = true;
            this.isError = false;
            this.search(true);
            this.checkImport = true;
          } else {
            this.listData = [];
            this.checkImport = false;
            this.isSuccess = false;
            this.isError = false;
            this.messageService.success(this.translateObj.notificationMessage.success);
          }
          this.isUpload = true;
          this.isLoading = false;
        },
        (e) => {
          if (e?.error) {
            this.messageService.error(e?.error?.description);
          } else {
            this.messageService.error(this.translateObj.notificationMessage.error);
          }
          this.listData = [];
          this.checkImport = false;
          this.isLoading = false;
        }
      );
    }
  }

  downloadTemplate() {
    window.open('/assets/template/template_import_phanquyen.xlsx', '_self');
  }

  back() {
    this.location.back();
  }
}
