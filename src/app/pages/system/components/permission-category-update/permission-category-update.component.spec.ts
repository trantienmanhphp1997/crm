import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PermissionCategoryUpdateComponent } from './permission-category-update.component';

describe('PermissionCategoryUpdateComponent', () => {
  let component: PermissionCategoryUpdateComponent;
  let fixture: ComponentFixture<PermissionCategoryUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PermissionCategoryUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionCategoryUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
