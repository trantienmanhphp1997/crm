import { forkJoin } from 'rxjs';
import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { CustomKeycloakService } from '../../services/custom-keycloak.service';
import { maxInt32 } from 'src/app/core/utils/common-constants';
import { CategoryService } from '../../services/category.service';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';

@Component({
  selector: 'app-permission-category-update',
  templateUrl: './permission-category-update.component.html',
  styleUrls: ['./permission-category-update.component.scss'],
})
export class PermissionCategoryUpdateComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listResourceType = [];
  listResource = [];
  listScope = [];
  listRoleForApply = [];
  listRoleSelected = [];
  listPolicies = [];
  listScopesOfResource = [];
  listPermission = [];
  rows = [];
  roleId = '';
  form = this.fb.group({
    id: [''],
    type: [{ value: '', disabled: true }, CustomValidators.required],
    name: ['', CustomValidators.required],
    resources: ['', CustomValidators.required],
    description: [''],
    scopes: [],
    policies: [''],
  });
  ColumnMode = ColumnMode;
  isShowScopes = false;
  messagesTable = {
    emptyMessage: '',
    selectedMessage: '',
    totalMessage: '',
  };
  data: any;
  resourceId = '';
  fisrt = true;
  @ViewChild(DatatableComponent) public table: DatatableComponent;

  constructor(
    injector: Injector,
    private customKeycloakService: CustomKeycloakService,
    private categoryService: CategoryService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.listResourceType.push({ code: 'resource', name: this.fields?.permissionForResource });
    this.listResourceType.push({ code: 'scope', name: this.fields?.permissionForScope });
    const permissionId = this.route.snapshot.paramMap.get('id');
    const type = this.route.snapshot.paramMap.get('type');
    forkJoin([
      this.categoryService.getPermissionById(permissionId, type),
      this.categoryService.searchResourceCategory({ page: 0, size: maxInt32 }),
      this.categoryService.searchRole({ page: 0, size: maxInt32 }),
      this.categoryService.searchScope({ page: 0, size: maxInt32 }),
      this.customKeycloakService.getAllPolicy({ first: 0, max: maxInt32 }),
      this.categoryService.getPermission(),
    ]).subscribe(
      ([itemPermission, listResource, listRole, listScope, listPolicies, listPermission]) => {
        this.listPermission = listPermission || [];
        this.listScope = listScope?._embedded?.keycloakScopeList || [];
        this.listPolicies = listPolicies || [];
        this.listResource = listResource?.content || [];
        this.resourceId = itemPermission?.resources[0];
        this.data = itemPermission;
        this.form.patchValue(itemPermission);
        const listRoleAll = listRole?._embedded?.keycloakRoleList || [];
        this.listRoleSelected = _.filter(listRoleAll, (i) => itemPermission?.roleNames?.includes(i.name));
        this.listRoleForApply = _.filter(listRoleAll, (i) => !itemPermission?.roleNames?.includes(i.name));
        this.sortRole();
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
    this.form.controls.resources.valueChanges.subscribe((value) => {
      if (value !== null) {
        this.categoryService.getResourceCategoryById(value).subscribe((itemResource) => {
          this.listScopesOfResource = [];
          itemResource?.scopes?.forEach((scope) => {
            this.listScopesOfResource.push(
              this.listScope.find((itemScope) => {
                return scope === itemScope.code;
              })
            );
          });
        });
      } else {
        this.listScopesOfResource = [];
      }
    });
    this.form.controls.type.valueChanges.subscribe((value) => {
      if (value === this.listResourceType[1].code) {
        this.form.controls.scopes.setValidators(CustomValidators.required);
        this.form.controls.scopes.updateValueAndValidity();
        this.isShowScopes = true;
      } else {
        this.form.controls.scopes.clearValidators();
        this.form.controls.scopes.updateValueAndValidity();
        this.isShowScopes = false;
      }
    });
  }

  changeParentId(resourceId) {
    this.form.controls.resources.setValue(resourceId);
  }

  deleteRole(itemRole) {
    this.listRoleForApply.push(itemRole);
    this.sortRole();
    _.remove(this.listRoleSelected, (item) => item?.name === itemRole?.name);
    this.listRoleSelected = [...this.listRoleSelected];
  }

  sortRole() {
    this.listRoleForApply?.sort((a, b) => {
      if (a.description?.toLowerCase() < b.description?.toLowerCase()) {
        return -1;
      }
      if (a.description?.toLowerCase() > b.description?.toLowerCase()) {
        return 1;
      }
      return 0;
    });
  }

  confirmDialog() {
    if (this.form.valid) {
      const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result.then((res) => {
        if (res) {
          const data = cleanDataForm(this.form);
          data.decisionStrategy = 'AFFIRMATIVE';
          data.logic = 'POSITIVE';
          const policyIds = [];
          this.listRoleSelected.forEach((role) => {
            const itemPolicy = this.listPolicies.find((policy) => {
              return policy.name === role.name;
            });
            if (itemPolicy) {
              policyIds.push(itemPolicy.id);
            }
          });
          data.policies = policyIds;
          data.resources = [this.resourceId];
          this.isLoading = true;
          if (this.data?.name?.toLowerCase() !== data.name?.toLowerCase()) {
            let flag = true;
            for (const item of this.listPermission) {
              if (item?.name?.toLowerCase() === data?.name?.toLowerCase()) {
                flag = false;
                break;
              }
            }
            if (flag) {
              this.categoryService.getPermissionById(data.id, data.type).subscribe(
                () => {
                  this.customKeycloakService.updatePermission(data).subscribe(
                    () => {
                      this.messageService.success(this.notificationMessage.success);
                      this.isLoading = false;
                      this.back();
                    },
                    (e) => {
                      this.messageService.error(this.notificationMessage.error);
                      this.isLoading = false;
                    }
                  );
                },
                () => {
                  this.isLoading = false;
                  this.messageService.error(this.notificationMessage.notExistObject);
                }
              );
            } else {
              this.messageService.warn(this.notificationMessage.existName);
              this.isLoading = false;
            }
          } else {
            this.customKeycloakService.updatePermission(data).subscribe(
              () => {
                this.messageService.success(this.notificationMessage.success);
                this.isLoading = false;
                this.location.back();
              },
              (e) => {
                this.messageService.error(this.notificationMessage.error);
                this.isLoading = false;
              }
            );
          }
        }
      });
    } else {
      validateAllFormFields(this.form);
    }
  }

  itemchoose(value) {
    this.listRoleSelected.push(
      this.listRoleForApply.find((item) => {
        return item?.name === value;
      })
    );
    this.listRoleSelected = [...this.listRoleSelected];
    this.listRoleForApply = this.listRoleForApply.filter((item) => {
      return item?.name !== value;
    });
    value = '';
  }

  back() {
    this.location.back();
  }
}
