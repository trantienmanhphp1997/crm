import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleCategoryCreateComponent } from './role-category-create.component';

describe('RoleCategoryCreateComponent', () => {
  let component: RoleCategoryCreateComponent;
  let fixture: ComponentFixture<RoleCategoryCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RoleCategoryCreateComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleCategoryCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
