import { Component, OnInit, Injector } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { BaseComponent } from 'src/app/core/components/base.component';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { CategoryService } from '../../services/category.service';
import { CustomKeycloakService } from '../../services/custom-keycloak.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import * as _ from 'lodash';

@Component({
  selector: 'app-role-category-create',
  templateUrl: './role-category-create.component.html',
  styleUrls: ['./role-category-create.component.scss'],
  providers: [NgbModal],
})
export class RoleCategoryCreateComponent extends BaseComponent implements OnInit {
  isLoading = false;
  form = this.fb.group({
    code: ['', [CustomValidators.required, CustomValidators.code]],
    name: ['', [CustomValidators.required]],
    description: [''],
  });

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private customKeycloakService: CustomKeycloakService
  ) {
    super(injector);
  }

  ngOnInit(): void {}

  confirmDialog() {
    if (this.form.valid) {
      const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result
        .then((res) => {
          if (res) {
            this.isLoading = true;
            const data = cleanDataForm(this.form);
            this.categoryService.createRole(data).subscribe(
              () => {
                this.isLoading = false;
                this.messageService.success(this.notificationMessage.success);
                this.back();
              },
              (e) => {
                if (e?.error?.description) {
                  this.messageService.error(e?.error?.description);
                } else {
                  this.messageService.error(this.notificationMessage.error);
                }
                this.isLoading = false;
              }
            );
          }
        })
        .catch(() => {});
    } else {
      validateAllFormFields(this.form);
    }
  }

  back() {
    this.location.back();
  }
}
