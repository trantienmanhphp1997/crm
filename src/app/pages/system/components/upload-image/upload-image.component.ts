import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormGroup, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CommonCategory, FunctionCode, SessionKey } from 'src/app/core/utils/common-constants';
import { pick, map, orderBy, first } from 'lodash';
import { CategoryService } from '../../services/category.service';
import { ImageFormComponent } from './image-form/image-form.component';
import { UploadImgService } from '../../services/upload-img.service';

@Component({
  selector: 'app-upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.scss']
})
export class UploadImageComponent extends BaseComponent implements OnInit {

  form = this.fb.group({
    division: '',
    type: '',
    images: this.fb.array([]),
  });
  divisionForm = this.fb.group({});
  listDivision = [];

  // new
  groupList = [];
  selectedImageIndex = 0;
  imageList = [];
  selectedForm: FormGroup;
  raw: any;

  isFirstLoading = true;
  imageFromDivision = [];
  isReloadFromAction = false;
  prefixImg = 'data:image/jpeg;base64,';

  @ViewChild('imgForm') imgFormComp: ImageFormComponent;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private uploadImageService: UploadImgService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.IMAGE_MANAGER}`);

  }

  ngOnInit(): void {
    const list = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM);
    this.listDivision = map(list, (item) => {
      return { code: item.code, name: `${item.code} - ${item.name}` };
    }).filter(item => ['SME', 'INDIV', 'CIB'].includes(item.code));
    this.listDivision = orderBy(this.listDivision, ['name'], ['asc', 'desc']);
    this.form.get('division').setValue(first(this.listDivision)?.code);

    this.getConfig();
  }

  ngAfterViewInit(): void {
    this.form.controls.division.valueChanges.subscribe(value => {
      this.setInactiveVaidate();
      this.findImagesWithDivision(value);
    })

    this.form.controls.type.valueChanges.subscribe(value => {
      this.setInactiveVaidate();
      if (this.isFirstLoading) return;
      const divisionCode = this.form.value.division;
      const imageForm = this.divisionForm.controls[divisionCode] as FormArray;

      this.removeTrashForm(imageForm);

      this.setImageList(imageForm, value);

      if (!this.imageList.length) {
        this.setSelectedForm(null);
      } else {
        const index = this.getFirstIndex(imageForm, value);

        this.setSelectedForm(imageForm.controls[index]);
        this.setStatus();
      }
    })
  }

  buildForm(): void {

    this.listDivision.forEach(division => {
      const divisionCode = division?.code;
      this.divisionForm.addControl(divisionCode, this.fb.array([]))
      const divisionForm = this.divisionForm.controls[divisionCode] as FormArray;

      if (this.imageFromDivision.length) {
        this.imageFromDivision.forEach((item, i) => {
          this.setImgForm(divisionForm, i, item);

        });
      }

      this.divisionForm.controls[divisionCode].valueChanges.subscribe(() => {
        this.checkDuplicate();
      });
    })

    const firstDivisionCode = this.listDivision[0]?.code;
    const imageForm = this.divisionForm.controls[firstDivisionCode] as FormArray;
    const index = (imageForm as FormArray).getRawValue().findIndex(item => item.type === this.form.value.type);

    this.setSelectedForm(imageForm.at(index));

    if (this.raw?.uuidImage) this.selectedForm.disable();

    this.setImageList(imageForm);
  }

  setImgForm(formArray: FormArray, index: number, data = {}): void {
    formArray.push(
      this.fb.group(this.getTemp(data))
    );


  }

  getConfig(): void {
    this.isLoading = true;
    const divisionCode = this.form.value.division;
    forkJoin([
      this.categoryService.getCommonCategory(CommonCategory.BANNER_CONFIG),
      this.uploadImageService.getImgFromDivisions({ divisionCodes: divisionCode }),
    ]).pipe(
      finalize(() => {
        this.isLoading = false;
        this.isFirstLoading = false;
      })
    ).subscribe(([config, firstImages]) => {

      firstImages = this.addPrefix(firstImages);

      this.groupList = config.content || [];
      this.imageFromDivision = firstImages || [];
      this.form.controls.type.setValue(this.groupList[0]?.value);

      this.buildForm();
    })
  }

  // with type
  getFormArrayLength(formArray: FormArray, type = null): number {
    return type && type === 'poster' ? 5 : (formArray as FormArray).getRawValue().filter(item => item.type === (this.form.value.type)).length;
  }

  create(type = undefined) {

    if (this.selectedForm && this.selectedForm.getRawValue().isUpdate) {
      this.selectedForm.patchValue(this.raw);
    }

    if (!type) type = this.form.value.type;

    const { formArray } = this.getCurrentForm();

    if (!formArray) return;

    let index = this.getFormArrayLength(formArray, type);

    formArray.push(
      this.fb.group(this.getTemp({ type }))
    );

    index = formArray.length - 1;
    this.setSelectedForm(formArray.controls[index]);

    this.selectedForm.controls.isCreate.setValue(true);
    this.setStatus('enable');

  }

  update() {
    this.selectedForm.enable();
    this.selectedForm.controls.isUpdate.setValue(true);
  }

  deleteInfo() {
    this.confirmService
      .confirm().then(res => {
        if (!res) return;
        const { formArray } = this.getCurrentForm();
        const index = this.getCurrentIndex();

        this.isLoading = true;

        this.uploadImageService.deleteInfo(pick(this.selectedForm.getRawValue(), ['divisionCode', 'uuidImage'])).pipe(
          finalize(() => {
            this.isLoading = false;
          })
        ).subscribe(() => {
          const indexOfImgList = this.imageList.findIndex(item => item.uuidImage === this.selectedForm.getRawValue().uuidImage);
          this.imageList.splice(indexOfImgList, 1);

          formArray.removeAt(index);

          this.setImageList(formArray);

          const fisrtIndex = this.getFirstIndex(formArray);

          this.setSelectedForm(formArray.controls[fisrtIndex]);
          this.messageService.success('Xóa thành công!');
        });
      })

  }

  chooseForm(uuidImage: string) {
    const { formArray } = this.getCurrentForm();

    this.setInactiveVaidate();

    // revert prev form
    this.selectedForm.patchValue(this.raw);

    this.removeTrashForm(formArray);

    const index = formArray.getRawValue().findIndex(item => item.uuidImage === uuidImage);

    this.setSelectedForm(formArray.at(index));


    this.setStatus();
  }

  removeTrashForm(formArray: FormArray): void {
    // remove all form not save
    formArray.getRawValue().forEach((v, i) => {
      if (!v.uuidImage) formArray.removeAt(i);
    })
  }

  // get current form with division and type
  getCurrentForm(i = null) {

    const { division } = this.form.value;

    let currentDivision = this.divisionForm.controls[division] as FormArray;

    if (!currentDivision) return { formGroup: null, formArray: null };

    const index = i !== null ? i : currentDivision.length - 1;

    return { formGroup: currentDivision.controls[index] as FormGroup, formArray: currentDivision, index };
  }

  getTemp(data: any = {}) {

    const searchData = this.form.value;

    // uuidImage is id

    let { uuidImage, type, priority, name, divisionCode, id } = data;

    if (!type) type = searchData.type;

    if (type === 'poster') priority = 6;

    return {
      id,
      name: [name, [Validators.required]],
      priority: [priority, Validators.required],
      image: [data.image],
      divisionCode,
      type,
      isUpdate: false,
      isCreate: !uuidImage,
      uuidImage
    }
  }

  findImagesWithDivision(divisionCode: string): void {
    this.isLoading = true;
    this.uploadImageService.getImgFromDivisions({ divisionCodes: divisionCode }).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(images => {
      images = this.addPrefix(images);

      this.imageFromDivision = images || [];

      this.setImgFormGroup(divisionCode);
    });
  }

  cancelUpdate(): void {
    this.selectedForm.patchValue(this.raw);

    const value = this.selectedForm.getRawValue();
    this.selectedForm.controls.isUpdate.setValue(false);
    if (value.uuidImage) {
      this.setStatus();
    }
  }

  setStatus(status = 'disable'): void {
    setTimeout(() => {
      status === 'disable' ? this.selectedForm.disable() : this.selectedForm.enable();
    }, 0)
  }

  get canCreate(): boolean {
    switch (this.form?.value?.type) {
      case 'banner':
        return (this.divisionForm?.controls[this.form?.value?.division] as FormArray)?.controls.filter((item: FormGroup) => item.getRawValue().type === 'banner').length < 5;
      case 'poster':
        return (this.divisionForm?.controls[this.form?.value?.division] as FormArray)?.controls.filter((item: FormGroup) => item.getRawValue().type === 'poster').length < 1;
    }

  }

  get haveFormNotSave(): boolean {
    return !!(this.divisionForm?.controls[this.form?.value?.division] as FormArray)?.controls.find((item: FormGroup) => !item.getRawValue().uuidImage);
  }

  checkDuplicate(): void {
    const { formArray } = this.getCurrentForm();
    const duplicates = ['name', 'priority'];

    duplicates.forEach(key => {
      if (this.selectedForm) {
        const isExist = (formArray as FormArray).getRawValue().filter(item => item[key] === this.selectedForm.getRawValue()[key]);
        let errors = this.selectedForm.get(key).errors;
        if (isExist.length > 1) {
          errors = errors ? {...errors, duplicated: true} : {duplicated: true};
        } else if (errors) {
          delete errors.duplicated;
        }
        this.selectedForm.get(key).setErrors(errors);
      }

    })

  }

  setImgFormGroup(divisionCode: string): void {
    const divisionForm = this.divisionForm.controls[divisionCode] as FormArray;

    divisionForm.clear();
    this.imageList = [];
    if (this.imageFromDivision.length) {
      this.imageFromDivision.forEach((item, i) => {
        this.setImgForm(divisionForm, i, item);
      });
    }

    const imageForm = this.divisionForm.controls[divisionCode] as FormArray;

    //
    this.setImageList(imageForm);

    if (!this.isReloadFromAction) {
      const firstIndex = this.getFirstIndex(imageForm);

      this.setSelectedForm(imageForm.controls[firstIndex] as FormGroup);
    }

    //
    if (this.selectedForm) this.setStatus();

    this.isReloadFromAction = false;
  }

  onCreate(): void {
    this.findImagesWithDivision(this.form.value.division);
  }

  // remove create form
  onDelete(): void {
    this.findImagesWithDivision(this.form.value.division);
  }

  setSelectedForm(formGroup: any): void {
    this.selectedForm = formGroup;
    this.raw = this.selectedForm?.getRawValue();
  }

  getCurrentIndex(): number {
    const {formArray} = this.getCurrentForm();
    return formArray.getRawValue().findIndex(item => item.uuidImage === this.selectedForm.getRawValue().uuidImage);
  }

  setImageList(formArray: FormArray, type: string = ''): void {
    this.imageList = formArray.getRawValue().filter(item => item.type === (type || this.form.value.type));
    this.imageList = orderBy(this.imageList, ['priority'], ['asc']);
  }

  onUpdate(): void {
    this.isReloadFromAction = true;
    this.findImagesWithDivision(this.form.value.division);
  }

  // first index of type
  getFirstIndex(formArray: FormArray, type = null): number {
    if (!type) type = this.form.value.type;
    const firstItem = this.imageList.find(item => item?.type === type);

    return formArray.getRawValue().findIndex(item => item.uuidImage === firstItem?.uuidImage);
  }

  setInactiveVaidate(): void {
    if (this.imgFormComp) {
      this.imgFormComp.isFirstChange = false;
    }
  }

  onLoading(status: boolean): void {
    this.isLoading = status;
    this.ref.detectChanges();
  }

  addPrefix(imageList: any[]): any[] {
    return imageList.map(item => {
      item.image = `${this.prefixImg}${item.image}`;
      return item;
    });
  }
}
