import { Component, ElementRef, EventEmitter, Injector, Input, OnInit, Optional, Output, SimpleChanges, ViewChild } from '@angular/core';
import { ControlContainer, FormGroup } from '@angular/forms';
import { concatMap, finalize } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import { UploadImgService } from '../../../services/upload-img.service';
import { pick, cloneDeep } from 'lodash';
import { Subject } from 'rxjs';

class ImageSnippet {
  constructor(
    public src: string,
    public file: any
  ) { }
}

@Component({
  selector: 'app-image-form',
  templateUrl: './image-form.component.html',
  styleUrls: ['./image-form.component.scss']
})
export class ImageFormComponent extends BaseComponent implements OnInit {

  @Input() baseForm;
  @Input() imgForm: FormGroup;
  @Input() prefixImg: string;
  @Output() cancelUpdate = new EventEmitter();
  @Output() onCreate = new EventEmitter();
  @Output() onDelete = new EventEmitter();
  @Output() onUpdate = new EventEmitter();
  @Output() onLoading = new EventEmitter();
  @ViewChild('inputFile') inputFile: ElementRef;

  form: FormGroup;
  selectedImage = '';
  prevImg: ImageSnippet;
  isLoading = false;
  isFirstChange = false;

  constructor(
    injector: Injector,
    private uploadImageService: UploadImgService,
    @Optional() public controlContainer: ControlContainer
  ) {
    super(injector)
  }

  ngOnInit(): void {
  }


  ngOnChanges(changes: SimpleChanges): void {
    this.form = changes.imgForm?.currentValue;
    this.selectedImage = null;

    if (!this.form) {
      return;
    }

    // this.getImg(this.form.value);
    if (this.selectedImage) {
      this.prevImg = cloneDeep(this.selectedImage);
    }

    this.selectedImage = this.form.value.image;
  }


  changeFile(fileEvent: any): void {
    const file = fileEvent.target.files[0];
    if (!file) return;

    if (!this.isValidType(file.type)) {
      this.selectedImage = '';
      this.messageService.warn('Ảnh sai định dạng!');
      return;
    }

    this.convertAndSetBase64(file);
  }

  save(): void {
    const data = this.form.value;
    this.isFirstChange = true;

    if (this.form.invalid || !this.selectedImage) return;

    this.isFirstChange = false;
    this.onLoading.emit(true);

    data.image = data.image.split(';base64,')[1];

    data.image = this.base64toBlob(data.image.replace(this.prefixImg, ''), 'image/jpeg');
    if (data.id) {
      this.update(data);
    } else {
      this.create(data);
    }
  }

  create(data): void {
    this.isLoading = true;
    this.uploadImageService.uploadImage(data).subscribe(imgRes => {
      this.saveInfo(data, imgRes);
    }, () => {
      this.onLoading.emit(false);
    })
  }

  update(data): void {

    data.updateImage = true;
    this.uploadImageService.uploadImage(data).subscribe(imgRes => {
      this.updateInfo(data, imgRes);
    }, () => {
      this.onLoading.emit(false);
    })

  }

  saveInfo(data, imgRes): void {
    this.uploadImageService.saveInfo({
      ...pick(data, ['name', 'priority', 'type']),
      uuidImage: imgRes.uuidImage,
      mimeType: imgRes.mimeType,
      divisionCode: this.baseForm.division
    }).pipe(
      finalize(() => {
        this.onLoading.emit(false);
      })
    ).subscribe(imgInfo => {
      imgInfo.id = imgInfo.uuidImage;
      imgInfo.isCreate = false;
      this.form.patchValue(imgInfo);

      this.onCreate.next(imgInfo);
      this.messageService.success('Tạo mới thành công!');

    });
  }

  updateInfo(data, imgRes): void {
    this.uploadImageService.updateInfo({
      ...pick(data, ['name', 'priority', 'type', 'id']),
      uuidImage: imgRes.uuidImage,
      mimeType: imgRes.mimeType,
      divisionCode: this.baseForm.division
    }).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(() => {
      this.form.get('isUpdate').setValue(false);
      this.onUpdate.next();
      this.messageService.success('Cập nhật thành công!');
    });
  }

  get isDisabled(): boolean {
    return this.form?.status === 'DISABLED';
  }

  cancel(): void {
    const { isUpdate, isCreate } = this.form?.value;

    if (isUpdate) {
      if (this.prevImg) {
        this.selectedImage = cloneDeep(this.prevImg);
      }
      this.cancelUpdate.next();
    } else if (isCreate) {
      this.onDelete.next();
    }

  }

  getImg(data): void {

    if (!data?.uuidImage) {
      this.selectedImage = null;
      return;
    }

    if (this.selectedImage) {
      this.prevImg = cloneDeep(this.selectedImage);
    }

    this.selectedImage = data.image;
  }

  convertAndSetBase64(file: any): void {
    const reader = new FileReader();
    reader.onload = (event: any) => {
      if (this.selectedImage) {
        this.prevImg = cloneDeep(this.selectedImage);
      }

      this.selectedImage = event.target.result;
      this.form.get('image').setValue(this.selectedImage);

    }

    reader.readAsDataURL(file);
  }

  isValidType(type: string): boolean {
    return type.includes('image/');
  }

  get canCancel(): boolean {
    return this.form?.value?.isUpdate || this.form?.value?.isCreate;
  }

  onPress(event): void {
    let value = event.target.value;
    const invalid = this.baseForm.type === 'banner' ? !value.match(/^[1-5]$/g) : Number(value) !== 6;
    value = invalid ? (this.baseForm.type === 'poster' ? 6 : null) : Number(value);

    event.target.value = value;
    this.form.controls.priority.setValue(value);
  }

  base64toBlob(base64Data, contentType): Blob {

    contentType = contentType || '';

    const sliceSize = 1024;

    const byteCharacters = atob(base64Data);

    const bytesLength = byteCharacters.length;

    const slicesCount = Math.ceil(bytesLength / sliceSize);

    const byteArrays = new Array(slicesCount);



    for (let sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {

      const begin = sliceIndex * sliceSize;

      const end = Math.min(begin + sliceSize, bytesLength);



      const bytes = new Array(end - begin);

      for (let offset = begin, i = 0; offset < end; ++i, ++offset) {

        bytes[i] = byteCharacters[offset].charCodeAt(0);

      }

      byteArrays[sliceIndex] = new Uint8Array(bytes);

    }

    return new Blob(byteArrays, { type: contentType });

  }

}
