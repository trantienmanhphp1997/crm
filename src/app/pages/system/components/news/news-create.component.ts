import {AfterViewInit, Component, Injector, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CommonCategory, ScreenType} from 'src/app/core/utils/common-constants';
import {CustomValidators} from 'src/app/core/utils/custom-validations';
import {cleanDataForm} from 'src/app/core/utils/function';
import {BaseComponent} from 'src/app/core/components/base.component';
import {CategoryService} from '../../services/category.service';
import {RmFluctuateApi} from '../../../rm/apis';
import {FileService} from '../../../../core/services/file.service';
import {finalize} from 'rxjs/operators';
import * as _ from 'lodash';
import {formatDate} from '@angular/common';
import {ConfirmDialogComponent} from '../../../../shared/components';
import {PreviewBannerNewsComponent} from './preview-img/preview-banner-news.component';
import {MatDialog} from '@angular/material/dialog';
import {forkJoin} from 'rxjs';


@Component({
  selector: 'app-news-create',
  templateUrl: './news-create.component.html',
  styleUrls: ['./news-create.component.scss'],
  providers: [NgbModal],
})
export class NewsCreateComponent extends BaseComponent implements OnInit, AfterViewInit {
  isLoading = false;
  form = this.fb.group({
    block: 'INDIV',
    title: ['', CustomValidators.required],
    newsType: ['1'],
    attachments: [[]],
    bannerImgId: ['', CustomValidators.required],
    bannerImgName: ['', CustomValidators.required],
    orderNum: null,
    startDate: ['', CustomValidators.required],
    endDate: ['', CustomValidators.required],
    content: '',
    links: '',
    featureCode: '',
    newsTemplatesId: '',
    isActived: true,
    defaultTop: false,
    channel: ''
  });
  oneMB = 1048576;
  fileInfo = [];
  curType = 'file';
  content = '';
  selectedImage = '';
  newsFeatureConfigList = [];
  selectedChannel: string[];
  today = new Date();
  screenType = this.screenType.create;
  id: any;
  formChange = false;
  oldForm: any;
  listImageValid = ['image/jpg', 'image/jpeg', 'image/png', 'image/svg'];
  listChannel = [];
  block = 'INDIV';
  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private rmFluctuateApi: RmFluctuateApi,
    private fileService: FileService,
    public dialog: MatDialog,
  ) {
    super(injector);
    this.id = _.get(this.route.snapshot.params, 'id');
    this.route?.data?.subscribe((data) => {
      this.screenType = data?.type;
    });
    this.block = _.get(this.route.snapshot.queryParams, 'block');
    this.form.controls.block.setValue(_.get(this.route.snapshot.queryParams, 'block'));
  }

  ngOnInit(): void {
    this.isLoading = true;
    console.log('block: ', this.block);
    // chi tiết hoặc cập nhật
    if (this.screenType === ScreenType.Update || this.screenType === ScreenType.Detail) {
      if (this.screenType === ScreenType.Detail) {
        this.form.disable();
      }
      forkJoin([
        this.categoryService.getDetailNews({id: this.id}),
        this.commonService.getCommonCategory(CommonCategory.NEWS_FEATURE_CONFIG),
        this.commonService.getCommonCategory(CommonCategory.EVENT_NEWS_CHANNEL)
      ]).subscribe(([detailNews, config, lstChannel]) => {
        // tải base64 của ảnh banner
        this.categoryService.viewFile(detailNews.bannerImgId).subscribe(data => {
          if (data) {
            this.selectedImage = 'data:image/png;base64,' + data.base64Data;
          }
        });
        this.newsFeatureConfigList = _.get(config, 'content');
        this.listChannel = _.get(lstChannel,'content');
        this.listChannel = _.filter(this.listChannel, (item) => item.value.includes(detailNews.block));
        this.listChannel = this.listChannel.map((i) => {
          return {
            code: i?.code,
            name: i?.name
          }
        });
        this.selectedChannel = detailNews?.channel ?  detailNews?.channel.split(',') : [];
        if(this.selectedChannel && this.selectedChannel.length > 0){
          this.selectedChannel = this.selectedChannel.filter((i) => this.isChannelAvailabel(i));
        }
        detailNews.startDate = detailNews.startDate ? new Date(detailNews.startDate) : '';
        detailNews.endDate = detailNews.startDate ? new Date(detailNews.endDate): '';
        this.form.patchValue(detailNews);
        this.content = detailNews.content;
        this.fileInfo = detailNews.attachments ? detailNews.attachments : [];
        const formValue = this.form.getRawValue();
        this.oldForm = {...formValue};
        this.isLoading = false;
        // this.form.patchValue(value);
        if (detailNews.defaultTop) {
          // là bản ghi default
          // chỉ được update ảnh banner đối với bản ghi default
          this.setFormForDefaulRecord();
        }
      });
    } else {
      // Màn hình thêm mới
      forkJoin([
        this.commonService.getCommonCategory(CommonCategory.NEWS_FEATURE_CONFIG),
        this.commonService.getCommonCategory(CommonCategory.EVENT_NEWS_CHANNEL)
      ]).subscribe(([value,lstChannel]) => {
        this.newsFeatureConfigList = _.get(value, 'content');
        this.listChannel = _.get(lstChannel,'content');
        this.listChannel = _.filter(this.listChannel, (item) => item.value.includes(this.block));
        this.listChannel = this.listChannel.map((i) => {
          return {
            code: i?.code,
            name: i?.name
          }
        });
        console.log('lstChannel: ', this.listChannel);
        const formValue = this.form.value;
        this.oldForm = {...formValue};
        this.isLoading = false;
      });
    }
  }
  isChannelAvailabel(i){
    let isAvailable = false;
    this.listChannel.forEach((item) => {
      if(item?.code === i){
        isAvailable = true;
        return;
      }
    });
    return isAvailable;
  }

  ngAfterViewInit() {
    this.form.valueChanges.subscribe((value) => {
      if (!this.isLoading) {
        this.checkFormChange(value);
      }
    });
    this.form.controls.newsType.valueChanges.subscribe(value => {
      if (value == 1) {
        this.form.controls.links.setValue('');
        this.form.controls.featureCode.setValue('');
      } else {
        this.form.controls.featureCode.setValue(_.head(this.newsFeatureConfigList).code);
        this.form.controls.content.setValue('');
        this.content = '';
        this.fileInfo = [];
      }
    });
  }

  setFormForDefaulRecord() {
    this.form.controls.title.disable({emitEvent: false});
    this.form.controls.newsType.disable({emitEvent: false});
    this.form.controls.featureCode.disable({emitEvent: false});
    this.form.controls.startDate.disable({emitEvent: false});
    this.form.controls.endDate.disable({emitEvent: false});
    this.form.controls.orderNum.disable({emitEvent: false});
//    this.form.controls.channel.disable({emitEvent: false});
  }

  checkFormChange(value) {
    if (!this.oldForm || _.isEqual(this.oldForm, value)) {
      this.formChange = false;
    } else {
      this.formChange = true;
    }
  }

  upload(fileEvent): void {
    const file = fileEvent.target.files[0];
    if (file.size > this.oneMB * 10) {
      this.messageService.warn('Tải tệp không thành công, kích thước tệp tối đa 10MB.');
      return;
    }
    const formData = new FormData();
    formData.append('file', file);
    this.rmFluctuateApi.uploadFile(formData).subscribe(id => {
      this.saveInfo(id, file.size);
    })
  }

  changeBanner(fileEvent: any): void {
    const file = fileEvent.target.files[0];
    if (file.size > this.oneMB * 10) {
      this.messageService.warn('Tải ảnh không thành công, kích thước ảnh tối đa 10MB.');
      return;
    }
    const formData = new FormData();
    formData.append('file', file);
    if (!file) return;

    if (!this.isValidType(file.type)) {
      this.selectedImage = '';
      this.messageService.warn('Ảnh sai định dạng!');
      return;
    }

    this.convertAndSetBase64(file);
    this.rmFluctuateApi.uploadFile(formData).subscribe(id => {
      this.rmFluctuateApi.getFileInfo(id).pipe(
        finalize(() => {
        })
      ).subscribe(res => {
        this.form.controls.bannerImgName.setValue(res.fileName);
        this.form.controls.bannerImgId.setValue(res.id);
      });
    })
  }

  isValidType(type: string): boolean {
    // return type.includes('image/') ;
    return this.listImageValid.includes(type);
  }

  saveInfo(id: string, size): void {
    this.rmFluctuateApi.getFileInfo(id).pipe(
      finalize(() => {
      })
    ).subscribe(res => {
      let formatedSize;
      const s = Number(size / this.oneMB);
      if (s.toString().includes('.')) {
        formatedSize = Number(s).toFixed(2);
      } else {
        formatedSize = s;
      }
      this.fileInfo[this.fileInfo.length] = {
        newsAttachmentId: res.id,
        fileName: res.fileName,
        size: `${formatedSize} MB`,
        filePath: res.filePath,
        type: this.curType,
        sizeNum: formatedSize
      };

      this.form.controls.attachments.setValue(this.fileInfo);
    });
  }

  convertAndSetBase64(file: any): void {
    const reader = new FileReader();
    reader.onload = (event: any) => {
      this.selectedImage = event.target.result;
      // this.form.get('bannerImgId').setValue(this.selectedImage);
    }

    reader.readAsDataURL(file);
  }

  removeBanner() {
    this.selectedImage = '';
    this.form.controls.bannerImgName.setValue('');
    this.form.controls.bannerImgId.setValue('');
  }

  close() {
    if (this.formChange) {
      const confirm = this.modalService.open(ConfirmDialogComponent, {windowClass: 'confirm-dialog'});
      confirm.componentInstance.message =
        'Thay đổi chưa được lưu, bạn có chắc chắn muốn quay lại?';
      confirm.result
        .then((res) => {
          if (res) {
            this.back();
          }
        })
        .catch(() => {
        });
    } else {
      this.back();
    }
  }

  get fileIdList(): string[] {
    return Object.keys(this.fileInfo);
  }

  download(fileId: string) {
    const title1 = this.fileInfo[fileId].fileName;
    this.fileService.downloadFile(this.fileInfo[fileId].newsAttachmentId, title1).subscribe((res) => {
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }

  removeFile(id: string): void {
    const info = this.fileInfo[id];
    if (!info) return;
    _.remove(this.fileInfo, info);
    // delete this.fileInfo[id];
    // this.form.controls.attachments.setValue(this.getFileList(this.form.controls.attachments.value));
  }

  saveAll() {
    if (this.form.controls.newsType.value === '1' && _.isEmpty(this.content)) {
      this.messageService.warn('Không được để trống Nội dung bài viết.');
      return;
    }
    if (this.form.controls.featureCode.value === 'KHAC' && _.isEmpty(this.form.controls.links.value)) {
      this.messageService.warn('Trường liên kết không được để trống với tính năng Khác.');
      return;
    }
    this.isLoading = true;
    let data: any = {};
    data = cleanDataForm(this.form);
    data.startDate = data.startDate ? formatDate(data.startDate, 'yyyy-MM-dd', 'en') : '';
    data.endDate = data.startDate ? formatDate(data.endDate, 'yyyy-MM-dd', 'en'): '';
    data.content = this.content;
    data.channel = this.selectedChannel.join(',');
    if (this.screenType === 'CREATE') {
      // data.attachments = this.getFileList(data.attachments);
      this.categoryService.createNews(data).subscribe(value => {
        this.isLoading = false;
        this.messageService.success(this.notificationMessage.success);
        this.back();
      }, () => {
        this.messageService.error(this.notificationMessage.error);
        this.isLoading = false;
      });
    } else {
      this.categoryService.updateNews(data).subscribe(value => {
        this.isLoading = false;
        this.messageService.success(this.notificationMessage.success);
        this.back();
      }, () => {
        this.messageService.error(this.notificationMessage.error);
        this.isLoading = false;
      });
    }

  }

  getFileList(fileObj): any[] {
    return Object.keys(fileObj).map(id => ({
      newsAttachmentId: id,
      fileName: fileObj[id].fileName,
      filePath: fileObj[id].filePath,
    }));
  }

  onContentChange(event) {
    this.form.controls.content.setValue(this.content);
  }

  back() {
    this.router.navigateByUrl('/system/management-news', {state: this.prop});
  }

  checkDisableSaveBtn() {

    if (this.form.controls.newsType.value == '1') {
      return (this.form.invalid || _.isEmpty(this.content) || _.isEmpty(this.selectedChannel));
    } else {
      return (this.form.invalid || (this.form.controls.featureCode.value === 'KHAC' && _.isEmpty(this.form.controls.links.value)) || _.isEmpty(this.selectedChannel));
    }
  }

  changeScreenType() {
    this.screenType = 'UPDATE';
    this.form.enable({emitEvent: false});
    if (this.form.controls.defaultTop.value) {
      // là bản ghi default
      // chỉ được update ảnh banner đối với bản ghi default
      this.setFormForDefaulRecord();
    }
  }

  previewBanner() {
    const dialogRef = this.dialog.open(PreviewBannerNewsComponent, {
      panelClass: 'poster',
      disableClose: true,
      data: {
        image: this.selectedImage,
      }
    });
  }
}
