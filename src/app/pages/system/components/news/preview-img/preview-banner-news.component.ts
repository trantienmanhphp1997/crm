import { Component, Inject, Injector, OnInit } from '@angular/core';
import { orderBy, map } from 'lodash';
import { UploadImgService } from 'src/app/pages/system/services/upload-img.service';
import { forkJoin } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import {PosterComponent} from '../../../../../core/components/poster/poster.component';
import {BaseComponent} from '../../../../../core/components/base.component';

@Component({
  selector: 'app-preview-banner-news',
  templateUrl: './preview-banner-news.component.html',
  styleUrls: ['./preview-banner-news.component.scss']
})
export class PreviewBannerNewsComponent extends BaseComponent implements OnInit {

  selectedImg = null;

  notShowAgain = new FormControl(false);

  sessionKey = 'session_poster';
  liveKey = 'poster';
  key = {
    session: 'session_poster',
    live: 'poster'
  }
  heighImg: any;
  widthImg: any;

  constructor(
    injector: Injector,
    public dialogRef: MatDialogRef<PosterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    super(injector);
  }

  ngOnInit(): void {
    const newImg = new Image();
    newImg.src = this.data.image;
    const interval = setInterval(() => {
      if (newImg.naturalWidth) {
        this.heighImg = newImg.height * 2;
        this.widthImg = newImg.width * 2;
        this.pickImg();
        clearInterval(interval);
      }
    }, 10);
  }


  close(): void {
    this.dialogRef.close();
  }

  pickImg(): void {
    this.selectedImg = this.data.image;
  }

  getKey(type): string {
    const {uuidImage, username} = this.data;
    return [uuidImage, this.key[type], username].join('_');
  }
}
