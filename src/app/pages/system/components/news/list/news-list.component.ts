import {AfterViewInit, Component, Injector, OnInit, ViewEncapsulation} from '@angular/core';
import {BaseComponent} from 'src/app/core/components/base.component';
import {forkJoin, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import * as _ from 'lodash';
import {global} from '@angular/compiler/src/util';
import {CategoryService} from '../../../services/category.service';
import {CommonCategory, FunctionCode, Scopes, SessionKey} from '../../../../../core/utils/common-constants';
import {Pageable} from '../../../../../core/interfaces/pageable.interface';
import {StandardKpiCbqlApi} from '../../../../kpis/apis/standard-kpi-cbql.api';
import {ConfirmDialogComponent} from '../../../../../shared/components';
import {FileService} from "../../../../../core/services/file.service";
import {PreviewBannerNewsComponent} from "../preview-img/preview-banner-news.component";
import {MatDialog} from "@angular/material/dialog";



@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NewsListComponent extends BaseComponent implements OnInit, AfterViewInit {

  paramSearch = {
    pageSize: global.userConfig.pageSize,
    pageNumber: 0,
    rsId: '',
    scope: Scopes.VIEW,
  };

  pageable: Pageable;
  listData = [];
  limit = global.userConfig.pageSize;
  divisionOfUser: any[];
  block: any;
  listDivision: [];
  listNumberView: [];
  newsNumberDisplay: any;
  oldNewsNumberDisplay: any;
  isEditNewsNumberDisplay = false;

  constructor(
    injector: Injector,
    private standardKpiCbqlApi: StandardKpiCbqlApi,
    private categoryService: CategoryService,
    private fileService: FileService,
    public dialog: MatDialog,

  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.NEWS}`);
    this.divisionOfUser = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];

  }

  ngOnInit() {
    this.divisionOfUser =
      this.divisionOfUser?.map((item) => {
        return { code: item.code, name: `${item.code || ''} - ${item.name || ''}` };
      }) || [];
    this.block = _.head(this.divisionOfUser).code;
    this.prop = this.sessionService.getSessionData(FunctionCode.NEWS);
    if (!this.prop) {
      this.search(true);
    } else {
      this.paramSearch = _.get(this.prop, 'paramSearch');
      this.block = _.get(this.prop, 'block');
      this.search(false);
    }

  }

  ngAfterViewInit() {

  }

  onActive(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      this.router.navigate([this.router.url, 'detail', item.newsTemplatesId]);
    }
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.pageNumber = pageInfo.offset;
    this.search(false);
  }

  update(value) {
    this.router.navigate([this.router.url, 'update', value.newsTemplatesId]);
  }

  delete(row) {
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.result
      .then((res) => {
        if (res) {
          this.isLoading = true;
          this.categoryService.deleteNewsById({id: _.get(row, 'newsTemplatesId')}).subscribe(
            () => {
              this.messageService.success(this.notificationMessage.success);
              this.isLoading = false;
              this.search(true);
            },
            (e) => {
              this.messageService.error(this.notificationMessage.error);
              this.isLoading = false;
            }
          );
        }
      })
      .catch(() => {});
  }

  search(isSearch?: boolean) {
    if (isSearch) {
      this.paramSearch.pageNumber = 0;
    }
    this.isLoading = true;
    const params:any = {};
    params.block = this.block;
    params.page = this.paramSearch.pageNumber;
    params.size = this.paramSearch.pageSize;
    params.isWeb = true;
    this.prop = {
      paramSearch: this.paramSearch,
      block: this.block
    }
    this.sessionService.setSessionData(FunctionCode.NEWS, this.prop);

    forkJoin([
      this.categoryService.getNewsConfigByDivision({block: this.block}).pipe(catchError((e) => of(undefined))),
      this.categoryService.getAllNews(params).pipe(catchError((e) => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.NEWS_NUMBER_CONFIG).pipe(catchError((e) => of(undefined))),
    ]).subscribe(([config, news, numberMaxConfig]) => {
      const arrNumber =Array.from({ length: Number(_.head(numberMaxConfig.content).value) + 1 }, (v, i) => i);
      arrNumber.shift();
      this.listNumberView = _.map(arrNumber, (item) => {
        return { code: item};
      });
     this.newsNumberDisplay = _.get(config, 'eventNumber') || 3;
     this.oldNewsNumberDisplay = this.newsNumberDisplay;
      this.listData = _.get(news, 'content');
      this.pageable = {
        totalElements: news.totalElements,
        totalPages: news.totalPages,
        currentPage: this.paramSearch.pageNumber,
        size: this.paramSearch.pageSize,
      };
      this.isLoading = false;
    });
  }

  create() {
    this.router.navigate([this.router.url, 'create'], {
      queryParams: {
        block: this.block
      }});
  }

  handleChangePageSize(event) {
    this.paramSearch.pageNumber = 0;
    this.paramSearch.pageSize = event;
    this.limit = event;
    this.search(true);
  }

  getStatus(row) {
    if (row.defaultTop) {
      return 'Còn hiệu lực';
    }
    const start = new Date(row.startDate);
    const end = new Date(row.endDate);
    const today = new Date();
    if ((start < today || this.isSameDay(start, today)) && (today < end || this.isSameDay(end, today))) {
      return 'Còn hiệu lực';
    } else if (today > end) {
      return 'Hết hiệu lực';
    } else if (today < start) {
      return 'Sắp diễn ra';
    }
  }

  isSameDay(d1, d2) {
    return d1.getFullYear() === d2.getFullYear() &&
      d1.getMonth() === d2.getMonth() &&
      d1.getDate() === d2.getDate();
  }

  onEditNewsNumberDisplay() {
    this.newsNumberDisplay = this.oldNewsNumberDisplay;
    this.isEditNewsNumberDisplay = !this.isEditNewsNumberDisplay;
  }

  onUpdateNewsNumber() {
    this.isLoading = true;
    const data = {
      block: this.block,
      orderNum: this.newsNumberDisplay
    };
    this.categoryService.updateNewsConfigByDivision(data).subscribe(value => {
      if (value) {
        this.oldNewsNumberDisplay = this.newsNumberDisplay;
        this.isLoading = false;
        this.isEditNewsNumberDisplay = false;
        this.messageService.success('Cập nhật số lượng tin tức hiển thị thành công');
      }
    }, (e) => {
        this.messageService.error(this.notificationMessage.error);
        this.isLoading = false;
      });
  }

  previewImg(fileId: string, title) {
    this.isLoading = true;
    this.categoryService.viewFile(fileId).subscribe(data => {
      if (data) {
        this.isLoading = false;
        const dialogRef = this.dialog.open(PreviewBannerNewsComponent, {
          panelClass: 'poster',
          disableClose: true,
          data: {
            image: 'data:image/png;base64,' + data.base64Data,
          }
        });
      }
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
      this.messageService.error(this.notificationMessage.error);
    });
    // this.fileService.downloadFile(fileId, title).subscribe((res) => {
    //   if (!res) {
    //     this.messageService.error(this.notificationMessage.error);
    //   }
    // });
  }
}
