import { forkJoin, of } from 'rxjs';
import { Component, OnInit, AfterViewInit, ViewChildren, QueryList, Injector } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { CategoryService } from '../../services/category.service';
import { CommonCategory } from 'src/app/core/utils/common-constants';
import { BaseComponent } from 'src/app/core/components/base.component';
import { catchError } from 'rxjs/operators';

declare const $: any;

@Component({
  selector: 'app-branch-category-update',
  templateUrl: './branch-category-update.component.html',
  styleUrls: ['./branch-category-update.component.scss'],
  providers: [NgbActiveModal, NgbModal],
})
export class BranchCategoryUpdateComponent extends BaseComponent implements OnInit, AfterViewInit {
  data: any;
  listBranchTypeKPI: any;
  listLocationKpi: any;
  form = this.fb.group({
    name: [{ value: '', disabled: true }],
    code: [{ value: '', disabled: true }],
    address: [{ value: '', disabled: true }],
    parentCode: [{ value: '', disabled: true }],
    type: [{ value: '', disabled: true }],
    dateOfEstablishment: [{ value: '', disabled: true }],
    description: [{ value: '', disabled: true }],
    typeKpi: [''],
    vungMienM: [{ value: '', disabled: true }],
    khuVucM: [{ value: '', disabled: true }],
    locationKpi: '',
  });
  a2eOptions = {
    format: 'DD/MM/YYYY',
  };
  listBranchType = [];
  isLoading = false;
  @ViewChildren('otpBranchTypeKPI') otpBranchTypeKPI: QueryList<any>;
  @ViewChildren('otpRegionKPI') otpRegionKPI: QueryList<any>;

  constructor(injector: Injector, private categoryService: CategoryService) {
    super(injector);
    this.translate.get('system.branchType').subscribe((result) => {
      Object.keys(result).forEach((key, index) => {
        this.listBranchType.push({ code: index + 1, name: result[key] });
      });
    });
    this.data = this.router.getCurrentNavigation().extras?.state?.data || null;
  }

  ngOnInit(): void {
    this.isLoading = true;
    forkJoin([
      this.categoryService.getBranchByCode(this.route.snapshot.paramMap.get('code')),
      this.commonService.getCommonCategory(CommonCategory.KPI_SYS_ORG_TYPE).pipe(catchError((e) => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.KPI_LOCATION).pipe(catchError((e) => of(undefined))),
    ]).subscribe(
      ([branch, listBranchTypeKPI, listRegionKPI]) => {
        this.data = branch;
        this.data.dateOfEstablishment = branch?.dateOfEstablishment ? new Date(branch?.dateOfEstablishment) : null;
        this.form.patchValue(this.data);
        this.listBranchTypeKPI = listBranchTypeKPI?.content || [];
        this.listLocationKpi = listRegionKPI?.content || [];
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngAfterViewInit() {}

  confirmDialog() {
    if (this.form.valid) {
      const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result
        .then((res) => {
          if (res) {
            this.isLoading = true;
            const data = {
              code: this.data.code,
              typeKpi: this.form.controls.typeKpi.value,
              locationKpi: this.form.controls.locationKpi.value,
            };
            this.categoryService.updateBranch(data).subscribe(
              () => {
                this.location.back();
                this.messageService.success(this.notificationMessage.success);
              },
              () => {
                this.isLoading = false;
                this.messageService.error(this.notificationMessage.error);
              }
            );
          }
        })
        .catch(() => {});
    } else {
      validateAllFormFields(this.form);
    }
  }

  back() {
    this.location.back();
  }
}
