import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchCategoryUpdateComponent } from './branch-category-update.component';

describe('BranchCategoryUpdateComponent', () => {
  let component: BranchCategoryUpdateComponent;
  let fixture: ComponentFixture<BranchCategoryUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchCategoryUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchCategoryUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
