import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PermissionCategoryCreateComponent } from './permission-category-create.component';

describe('PermissionCategoryCreateComponent', () => {
  let component: PermissionCategoryCreateComponent;
  let fixture: ComponentFixture<PermissionCategoryCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PermissionCategoryCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionCategoryCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
