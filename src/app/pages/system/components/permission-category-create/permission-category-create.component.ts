import { CustomKeycloakService } from './../../services/custom-keycloak.service';
import { CategoryService } from '../../services/category.service';
import { forkJoin } from 'rxjs';
import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { maxInt32 } from 'src/app/core/utils/common-constants';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';

@Component({
  selector: 'app-permission-category-create',
  templateUrl: './permission-category-create.component.html',
  styleUrls: ['./permission-category-create.component.scss'],
})
export class PermissionCategoryCreateComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listResourceType = [];
  listResource = [];
  listManipulation = [];
  listRoleForApply = [];
  listRoleSelected = [];
  listPolicies = [];
  listScopesOfResource = [];
  rows = [];
  form = this.fb.group({
    type: ['resource', CustomValidators.required],
    name: ['', CustomValidators.required],
    resources: ['', CustomValidators.required],
    description: [''],
    scopes: [''],
    policies: [''],
  });
  ColumnMode = ColumnMode;
  isShowScopes = false;
  messagesTable = {
    emptyMessage: '',
    selectedMessage: '',
    totalMessage: '',
  };
  appId = '';
  parentId: string;
  @ViewChild(DatatableComponent) public table: DatatableComponent;

  constructor(
    injector: Injector,
    private customKeycloakService: CustomKeycloakService,
    private categoryService: CategoryService
  ) {
    super(injector);
    this.parentId = this.router.getCurrentNavigation()?.extras?.state?.resourceId || '';
    this.appId = this.router.getCurrentNavigation()?.extras?.state?.appId || '';
  }

  ngOnInit(): void {
    this.isLoading = true;

    this.listResourceType.push({ code: 'resource', name: this.fields?.permissionForResource });
    this.listResourceType.push({ code: 'scope', name: this.fields?.permissionForScope });
    forkJoin([
      this.categoryService.searchResourceCategory({ page: 0, size: maxInt32, type: this.appId }),
      this.categoryService.searchScope({ page: 0, size: maxInt32 }),
      this.customKeycloakService.getAllPolicy({ first: 0, max: maxInt32 }),
      this.categoryService.searchRole({ page: 0, size: maxInt32 }),
    ]).subscribe(
      ([listResource, listScope, listPolicies, listRole]) => {
        this.listResource = listResource.content;
        if (!_.isEmpty(this.parentId)) {
          this.form.controls.resources.setValue(this.parentId);
        }
        this.listManipulation = listScope?._embedded?.keycloakScopeList;
        this.listRoleForApply = listRole?._embedded?.keycloakRoleList || [];
        this.listRoleForApply?.forEach((item) => {
          item.description = item.description ? item.description : '';
        });
        this.listRoleForApply?.sort((a, b) => {
          if (a.description?.toLowerCase() < b.description?.toLowerCase()) {
            return -1;
          }
          if (a.description?.toLowerCase() > b.description?.toLowerCase()) {
            return 1;
          }
          return 0;
        });
        this.listPolicies = listPolicies;
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
    this.form.controls.resources.valueChanges.subscribe((value) => {
      if (value !== null) {
        this.categoryService.getResourceCategoryById(value).subscribe((itemResource) => {
          itemResource?.scopes?.forEach((scope) => {
            this.listScopesOfResource.push(
              this.listManipulation.find((itemScope) => {
                return scope === itemScope.code;
              })
            );
          });
        });
      } else {
        this.listScopesOfResource = [];
      }
    });
    this.form.controls.type.valueChanges.subscribe((value) => {
      if (value === this.listResourceType[1].code) {
        this.form.controls.scopes.setValidators(CustomValidators.required);
        this.form.controls.scopes.updateValueAndValidity();
        this.isShowScopes = true;
      } else {
        this.form.controls.scopes.clearValidators();
        this.form.controls.scopes.updateValueAndValidity();
        this.isShowScopes = false;
      }
    });
  }

  changeParentId(parentId) {
    this.form.controls.resources.setValue(parentId);
    this.parentId = parentId;
  }

  deleteRole(itemRole) {
    this.listRoleForApply.push(itemRole);
    this.listRoleForApply.sort((a, b) => {
      if (a.description.toLowerCase() < b.description.toLowerCase()) {
        return -1;
      }
      if (a.description.toLowerCase() > b.description.toLowerCase()) {
        return 1;
      }
      return 0;
    });
    _.remove(this.listRoleSelected, (item) => item?.name === itemRole?.name);
    // this.listRoleSelected = this.listRoleSelected.filter((item) => {
    //   return item.name !== itemRole.name;
    // });
    this.listRoleSelected = [...this.listRoleSelected];
  }

  confirmDialog() {
    if (this.form.valid) {
      const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result.then((res) => {
        if (res) {
          const data = cleanDataForm(this.form);
          data.decisionStrategy = 'AFFIRMATIVE';
          data.logic = 'POSITIVE';
          const policyIds = [];
          let itemPolicy: any;
          this.listRoleSelected.forEach((role) => {
            itemPolicy = this.listPolicies.find((policy) => {
              return policy.name === role.name;
            });
            if (itemPolicy) {
              policyIds.push(itemPolicy.id);
            }
          });
          data.policies = policyIds;
          if (data.type === 'resource') {
            delete data.scopes;
          }
          data.resources = [data.resources];
          this.isLoading = true;

          this.categoryService.getPermission().subscribe(
            (listItemOld) => {
              let flag = true;
              for (const item of listItemOld) {
                if (item.name.toLowerCase() === data.name.toLowerCase()) {
                  flag = false;
                  break;
                }
              }
              if (flag) {
                this.customKeycloakService.createPermission(data).subscribe(
                  () => {
                    this.messageService.success(this.notificationMessage.success);
                    this.isLoading = false;
                    this.back();
                  },
                  () => {
                    this.messageService.error(this.notificationMessage.error);
                    this.isLoading = false;
                  }
                );
              } else {
                this.messageService.warn(this.notificationMessage.existName);
                this.isLoading = false;
              }
            },
            () => {
              this.isLoading = false;
              this.messageService.error(this.notificationMessage.error);
            }
          );
        }
      });
    } else {
      validateAllFormFields(this.form);
    }
  }

  itemchoose(value) {
    this.listRoleSelected.push(
      this.listRoleForApply.find((item) => {
        return item?.name === value;
      })
    );
    console.log(this.listRoleSelected)
    this.listRoleSelected = [...this.listRoleSelected];
    this.listRoleForApply = this.listRoleForApply.filter((item) => {
      return item?.name !== value;
    });
    value = '';
  }

  back() {
    this.location.back();
  }
}
