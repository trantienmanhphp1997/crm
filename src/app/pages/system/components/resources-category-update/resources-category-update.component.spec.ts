import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourcesCategoryUpdateComponent } from './resources-category-update.component';

describe('ResourcesCategoryUpdateComponent', () => {
  let component: ResourcesCategoryUpdateComponent;
  let fixture: ComponentFixture<ResourcesCategoryUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ResourcesCategoryUpdateComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourcesCategoryUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
