import { forkJoin } from 'rxjs';
import { Component, OnInit, Injector } from '@angular/core';
import { FormArray, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { maxInt32 } from 'src/app/core/utils/common-constants';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { CategoryService } from '../../services/category.service';
import { BaseComponent } from 'src/app/core/components/base.component';

declare const $: any;

@Component({
  selector: 'app-resources-category-update',
  templateUrl: './resources-category-update.component.html',
  styleUrls: ['./resources-category-update.component.scss'],
  providers: [NgbActiveModal, NgbModal],
})
export class ResourcesCategoryUpdateComponent extends BaseComponent implements OnInit {
  isLoading = false;
  isUpdate = true;
  data: any;
  listApp: any;
  listResourceParent: any;
  listManipulation: any;
  listUris = [];
  form = this.fb.group({
    id: [''],
    name: ['', CustomValidators.required],
    code: [{ value: '', disabled: true }, [CustomValidators.required, CustomValidators.code]],
    displayName: [''],
    type: ['', CustomValidators.required],
    iconUri: [''],
    uri: ['', CustomValidators.required],
    parentId: [''],
    scopes: [''],
    uriOfResources: [''],
    indexNumber: ['', [CustomValidators.required, CustomValidators.onlyNumber, Validators.min(0)]],
    api: [''],
    apis: this.fb.array([]),
  });
  parentId = '';

  constructor(injector: Injector, private categoryService: CategoryService) {
    super(injector);
  }

  ngOnInit(): void {
    this.isLoading = true;
    const functionId = this.route.snapshot.paramMap.get('id');
    forkJoin([
      this.categoryService.getResourceParentCategoryById(functionId),
      this.categoryService.getResourceCategoryById(functionId),
      this.categoryService.searchScope({ page: 0, size: maxInt32 }),
      this.categoryService.searchAppCategory({ page: 0, size: maxInt32 }),
    ]).subscribe(
      ([listResourceParent, resource, listManipulation, listApp]) => {
        this.data = resource;
        this.form.patchValue(this.data);
        this.listApp = listApp.content;
        this.listResourceParent = listResourceParent;
        this.listManipulation = listManipulation?._embedded?.keycloakScopeList || [];
        if (this.data && this.data.parentId) {
          this.parentId = this.data.parentId;
        }
        if (this.data?.uriOfResources && this.data.uriOfResources.length > 0) {
          this.data.uriOfResources.forEach((item, index) => {
            this.apis.push(this.createApi(index, item));
          });
        }
        this.listUris = this.data.uriOfResources || [];
        if (!this.isUpdate) {
          this.form.disable();
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  confirmDialog() {
    if (this.form.valid) {
      const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result
        .then((res) => {
          if (res) {
            this.isLoading = true;
            const formData = cleanDataForm(this.form);
            const data: any = {};
            Object.keys(formData).forEach((key) => {
              if (key !== 'api' && key !== 'apis') {
                data[key] = formData[key];
              }
            });
            data.uriOfResources = [];
            formData.apis.forEach((api) => {
              if (api.name.trim() !== '') {
                data.uriOfResources.push(api.name);
              }
            });
            if (data.scopes === '') {
              data.scopes = [];
            }
            data.parentId = this.parentId;
            this.categoryService.updateResourceCategory(data).subscribe(
              () => {
                this.isLoading = false;
                this.location.back();
                this.messageService.success(this.notificationMessage.success);
              },
              (e) => {
                if (e?.error) {
                  this.messageService.warn(e?.error?.description);
                } else {
                  this.messageService.error(this.notificationMessage.error);
                }
                this.isLoading = false;
              }
            );
          }
        })
        .catch(() => {});
    } else {
      validateAllFormFields(this.form);
    }
  }

  addUri() {
    if (!this.isUpdate) {
      return;
    }
    const value = this.form.controls.api.value.trim();
    if (value.length > 0) {
      this.apis.push(this.createApi(this.apis.length, value));
      this.form.controls.api.setValue('');
    }
  }

  get apis() {
    return this.form.get('apis') as FormArray;
  }

  createApi(index, value?): FormGroup {
    return this.fb.group({
      id: 'api' + index,
      name: value ? value : '',
    });
  }

  deleteUri(i) {
    this.apis.removeAt(i);
  }

  back() {
    this.location.back();
  }
}
