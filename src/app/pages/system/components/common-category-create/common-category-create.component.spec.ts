import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonCategoryCreateComponent } from './common-category-create.component';

describe('CommonCategoryCreateComponent', () => {
  let component: CommonCategoryCreateComponent;
  let fixture: ComponentFixture<CommonCategoryCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CommonCategoryCreateComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonCategoryCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
