import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { CategoryService } from '../../../services/category.service';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { global } from '@angular/compiler/src/util';

@Component({
  selector: 'app-view-table',
  templateUrl: './view-table.component.html',
  styleUrls: ['./view-table.component.scss']
})
export class ViewTableComponent implements OnInit {
  body: any;
  data = [];
  pageable: Pageable;
  isLoading = false;
  messages = global.messageTable;
  firstLoad = false;
  constructor(private dialogRef: MatDialogRef<ViewTableComponent>, private categoryService: CategoryService
  ) {
    this.pageable = {
      size: 10,
      currentPage: 0,
      totalElements: 1
    };
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.isLoading = true;
    this.categoryService.reportListRatingFunction(this.body).subscribe(res => {
      this.isLoading = false;
      if(res) {
        this.data = res?.content;
        this.pageable = {
          totalElements: res?.totalElements,
          totalPages: res?.totalPages,
          currentPage: res?.number,
          size: res?.size,
        };
      }
      this.firstLoad = true;
    }, err => {
      this.firstLoad = true;
      this.isLoading = false;
    })
  }

  setPage(pageInfo) {
    if(this.body.pageNumber == pageInfo.offset) {
      return;
    }
    this.body.pageNumber = pageInfo.offset;
    this.getData();
  }

  handleChangePageSize(event) {
    this.body.pageSize = event;
    this.body.pageNumber = 0;
    this.getData();
  }

  close(){
    this.dialogRef.close();
  }
}
