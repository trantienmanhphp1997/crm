import { global } from '@angular/compiler/src/util';
import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { FunctionCode, Scopes, SessionKey } from 'src/app/core/utils/common-constants';
import { ChooseBranchesModalComponent } from '../choose-branches-modal/choose-branches-modal.component';
import _ from 'lodash';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { CategoryService } from '../../services/category.service';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as moment from 'moment';
import { Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ViewTableComponent } from './view-table/view-table.component';
import { FileService } from 'src/app/core/services/file.service';
import { CommonCategory } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'app-frequency-of-using-function',
  templateUrl: './frequency-of-using-function.component.html',
  styleUrls: ['./frequency-of-using-function.component.scss']
})
export class FrequencyOfUsingFunctionComponent extends BaseComponent implements OnInit {
  form = this.fb.group({
    function: [null, Validators.required],
    blockCode: [null, Validators.required],
    system: ['CRM_WEB', Validators.required],
    toDate: [moment().endOf('month').format('DD/MM/YYYY'), Validators.required],
    fromDate: [moment().startOf('month').format('DD/MM/YYYY'), Validators.required]
  });
  listFunctions = [];
  listDivision = [];
  listSystem = [
    { name: 'SmartRM SME', value: 'SMART_SME' },
    { name: 'Web CRM', value: 'CRM_WEB' },
    { name: 'App Mobile CRM', value: 'CRM_MOBILE' },

  ];
  termData = [];
  listDataTable = [];
  listBranch = [];
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };

  paramsTable = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };
  maxSize = 0;
  listFunctionsApp = [];
  maxLengthBranch: number;
  errorDate: any;
  send: boolean;

  constructor(injector: Injector, private categoryService: CategoryService, public dialog: MatDialog, private fileService: FileService

  ) {
    super(injector);
    this.getlistMenu(this.sessionService.getSessionData(SessionKey.MENU));
    const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
    this.listDivision = divisionOfUser?.map((item) => { return { code: item.code, displayName: `${item.code || ''} - ${item.name || ''}` } }) || [];
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.SYSTEM_REPORT}`);
    this.getData();
    this.subscribeForm();
  }

  ngOnInit() {

  }

  getlistMenu(menu = []) {
    if (menu.length) {
      menu.forEach(element => {
        if (!element.subMenu.length) {
          this.listFunctions.push(element);
        } else {
          this.getlistMenu(element.subMenu);
        }
      });
    }
  }
  search() {
    const modal = this.modalService.open(ChooseBranchesModalComponent, { windowClass: 'tree__branches-modal' });
    modal.componentInstance.listBranchOld = _.map(this.termData, (x) => x.code) || [];
    modal.componentInstance.listBranch = this.listBranch;
    modal.componentInstance.maxLengthSelect = +this.maxLengthBranch;
    modal.result
      .then((res) => {
        if (res) {
          this.termData = res;
          this.mapData();
        }
      })
      .catch(() => { });
  }

  mapData() {
    const total = this.termData.length;
    this.pageable.totalElements = total;
    this.pageable.totalPages = Math.floor(total / this.pageable.size);
    this.pageable.currentPage = this.paramsTable.page;
    const start = this.paramsTable.page * this.paramsTable.size;
    this.listDataTable = this.termData?.slice(start, start + this.paramsTable.size);
  }

  getData() {
    this.categoryService.getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW).subscribe(res => {
      if (res) {
        this.listBranch = res;
      }
    });
    this.commonService.getCommonCategory(CommonCategory.CONFIG_EXPORT_CUSTOMER_LIMIT_COUNT).subscribe(res => {
      this.maxSize = +res?.content?.find(item => item.code == 'EXPORT_FUNCTION')?.value
    });
    this.commonService.getCommonCategory(CommonCategory.REPORTS_MAX_LENGTH_RM).subscribe(res => {
      this.maxLengthBranch = res?.content.find(item => item.code == 'TABLEAU_MAX_BRANCH_REPORT')?.value;
    });
  }

  setPage(pageInfo) {
    this.paramsTable.page = pageInfo.offset;
    this.mapData();
  }

  removeList() {
    this.termData = [];
    this.paramsTable.page = 0;
    this.mapData();
  }

  removeRecord(item) {
    _.remove(this.termData, (i) => i.code === item.code);
    _.remove(this.listDataTable, (i) => i.code === item.code);
    if (this.listDataTable.length === 0 && this.pageable.currentPage > 0) {
      this.paramsTable.page -= 1;
    }
    this.listDataTable = [...this.listDataTable];
    this.mapData();
  }

  viewReport() {
    this.form.markAllAsTouched();
    const valueForm = this.form.getRawValue();
    const body = {
      "functionCode": valueForm.function[0] == 'ALL' ? [] : valueForm.function,
      "dateStart": moment(valueForm.fromDate, 'DD/MM/YYYY'),
      "dateEnd": moment(valueForm.toDate, 'DD/MM/YYYY'),
      "appCode": valueForm.system,
      "blockCode": valueForm.blockCode,
      "branches": this.termData.map(item => item?.code),
      "pageNumber": 0,
      "pageSize": 10,
      // "size": 0
    };
    this.send = true;
    if (this.form.invalid || this.errorDate || !this.listDataTable.length) {
      return;
    }
    const dialog = this.dialog.open(ViewTableComponent, {
      width: '80%'
    });
    dialog.componentInstance.body = body;
  }

  exportReport() {
    this.form.markAllAsTouched();
    const valueForm = this.form.getRawValue();
    const body = {
      "functionCode": valueForm.function[0] == 'ALL' ? [] : valueForm.function,
      "dateStart": moment(valueForm.fromDate, 'DD/MM/YYYY'),
      "dateEnd": moment(valueForm.toDate, 'DD/MM/YYYY'),
      "appCode": valueForm.system,
      "blockCode": valueForm.blockCode,
      "branches": this.termData.map(item => item?.code),
      // "pageNumber": 0,
      // "pageSize": 10,
      "size": this.maxSize
    };
    this.send = true;
    if (this.form.invalid || this.errorDate || !this.listDataTable.length) {
      return;
    }
    this.isLoading = true;
    this.categoryService.exportRatingFunction(body).subscribe(res => {
      if(!res){
        this.messageService.warn('Không có dữ liệu!');
        this.isLoading = false;
        return;
      }
      this.fileService.downloadFile(res, `${moment(new Date(), 'DDMMYYYY').format('DDMMYYYY')}.BAO-CAO-TINH-HINH-SU-DUNG-CRM.xlsx`).subscribe(val => {
        this.isLoading = false;
        this.messageService.warn(`Chỉ xuất được tối đa ${ this.maxSize} bản ghi / lần`)
      }, error => {
        this.messageService.error(this.notificationMessage.error);
        this.isLoading = false;
      });
    })
  }

  subscribeForm() {
    this.form.controls.blockCode.setValue(this.listDivision[0].code);
    this.form.controls.system.valueChanges.subscribe(res => {
      if (res != 'CRM_WEB') {
        this.listFunctionsApp = [{
          code: 'ALL',
          name: 'Tất cả'
        }]
        this.form.controls.function.setValue([this.listFunctionsApp[0].code]);
        return;
      }
      this.form.controls.function.setValue(null);
    });
  }

  validDate(ev) {
    const res = this.form.value;
    if (moment(res?.fromDate, 'DD/MM/YYYY') > moment(res?.toDate, 'DD/MM/YYYY')) {
      this.errorDate = "Đến ngày không được nhỏ hơn từ ngày";
      return;
    }
    if (moment(moment(res?.toDate, 'DD/MM/YYYY').subtract(3, 'month').calendar('MM/DD/YYYY'), 'MM/DD/YYYY') > moment(res?.fromDate, 'DD/MM/YYYY')) {
      this.errorDate = "Phạm vi không được vượt quá 3 tháng";
      return;
    }
    this.errorDate = undefined;
  }

}
