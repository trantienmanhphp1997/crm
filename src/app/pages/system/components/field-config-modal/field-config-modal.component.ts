import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FieldType } from 'src/app/core/utils/common-constants';

declare const $: any;

@Component({
  selector: 'app-field-config-modal',
  templateUrl: './field-config-modal.component.html',
  styleUrls: ['./field-config-modal.component.scss'],
})
export class FieldConfigModalComponent implements OnInit, AfterViewInit {
  title = 'Cấu hình trường thông tin';
  form = this.fb.group({
    label: ['', Validators.required],
    name: [{ value: '', disabled: true }],
    type: ['', Validators.required],
    validations: [''],
    options: [{ value: '', disabled: true }],
    inputType: [{ value: '', disabled: true }],
    optionColumn: [{ value: '', disabled: true }],
    dateType: [{ value: '', disabled: true }],
  });
  isLoading = false;
  listOptions: any;
  listValidations: any;
  listType = [];

  constructor(private fb: FormBuilder, private modalActive: NgbActiveModal) {
    Object.keys(FieldType).forEach((key) => {
      this.listType.push({ code: key, name: key });
    });
  }

  ngOnInit(): void {}

  ngAfterViewInit() {
    $('.selectpicker').selectpicker();
    this.form.controls.type.valueChanges.subscribe((value) => {
      if (value === FieldType.textbox) {
        this.form.controls.inputType.enable();
        this.form.controls.options.disable();
        this.form.controls.options.reset();
        this.form.controls.dateType.disable();
        this.form.controls.dateType.reset();
        this.form.controls.optionColumn.reset();
        this.form.controls.optionColumn.disable();
      } else if (value === FieldType.checkbox || value === FieldType.radio) {
        this.form.controls.inputType.disable();
        this.form.controls.inputType.reset();
        this.form.controls.options.enable();
        this.form.controls.dateType.disable();
        this.form.controls.dateType.reset();
        this.form.controls.optionColumn.enable();
      } else if (value === FieldType.select) {
        this.form.controls.inputType.disable();
        this.form.controls.inputType.reset();
        this.form.controls.options.enable();
        this.form.controls.dateType.disable();
        this.form.controls.dateType.reset();
        this.form.controls.optionColumn.disable();
        this.form.controls.optionColumn.reset();
      } else if (value === FieldType.date) {
        this.form.controls.inputType.reset();
        this.form.controls.inputType.disable();
        this.form.controls.options.reset();
        this.form.controls.options.disable();
        this.form.controls.dateType.enable();
        this.form.controls.optionColumn.reset();
        this.form.controls.optionColumn.disable();
      } else {
        this.form.controls.inputType.reset();
        this.form.controls.inputType.disable();
        this.form.controls.options.reset();
        this.form.controls.options.disable();
        this.form.controls.dateType.reset();
        this.form.controls.dateType.disable();
        this.form.controls.optionColumn.reset();
        this.form.controls.optionColumn.disable();
      }
    });
  }

  closeModal() {
    this.modalActive.close();
  }

  save() {}
}
