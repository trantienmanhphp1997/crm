import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldConfigModalComponent } from './field-config-modal.component';

describe('FieldConfigModalComponent', () => {
  let component: FieldConfigModalComponent;
  let fixture: ComponentFixture<FieldConfigModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldConfigModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldConfigModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
