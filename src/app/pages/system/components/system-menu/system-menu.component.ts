import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-system-menu',
  templateUrl: './system-menu.component.html',
  styleUrls: ['./system-menu.component.scss'],
})
export class SystemMenuComponent implements OnInit {
  public systemMenus = [
    {
      code: 'userSessions',
      routerLink: '/system/users-sessions',
    },
    {
      code: 'applicationsConfig',
      routerLink: '/system/applications-config',
    },
    {
      code: 'commonCategory',
      routerLink: '/system/common-category',
    },
    {
      code: 'branchCategory',
      routerLink: '/system/branches-category',
    },
    {
      code: 'blocksCategory',
      routerLink: '/system/blocks-category',
    },
    {
      code: 'resourcesCategory',
      routerLink: '/system/resources-category',
    },
    {
      code: 'manipulationCategory',
      routerLink: '/system/manipulation-category',
    },
    {
      code: 'userCategory',
      routerLink: '/system/users-config',
    },
    {
      code: 'roleCategory',
      routerLink: '/system/role-category',
    },
    {
      code: 'roleForUser',
      routerLink: '/system/user-role-config',
    },
    {
      code: 'permissionCategory',
      routerLink: '/system/permission-config',
    },
    {
      code: 'userPermissionConfig',
      routerLink: '/system/user-permission-config',
    },
    {
      code: 'logHistory',
      routerLink: '/system/log-history',
    },
    // {
    //   name: 'Cấu hình trường thông tin',
    //   routerLink: '/system/fields-config',
    // },
    // {
    //   name: 'Cấu hình trường Lanes',
    //   routerLink: '/system/lanes-config',
    // },
    // {
    //   name: 'Cấu hình Email',
    //   routerLink: '/system/email-config',
    // },
    // {
    //   name: 'Cấu hình template',
    //   routerLink: '/system/template-config',
    // },
    // {
    //   name: 'Quản lý danh mục',
    //   routerLink: '/system/category-config',
    // },
    // {
    //   name: 'Quản lý quy trình',
    //   routerLink: '/system/workflow-config',
    // },
  ];

  constructor() {}

  ngOnInit(): void {}
}
