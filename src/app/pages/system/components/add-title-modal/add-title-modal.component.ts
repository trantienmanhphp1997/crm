import { Component, OnInit, Injector, ViewEncapsulation, HostBinding, Input } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { global } from '@angular/compiler/src/util';
import { ScreenType, SessionKey } from 'src/app/core/utils/common-constants';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { TitleGroupApi } from 'src/app/pages/rm/apis';
import { TitleRelationshipService } from '../../services/title-relationship.service';

@Component({
  selector: 'app-rm-modal',
  templateUrl: './add-title-modal.component.html',
  styleUrls: ['./add-title-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [TitleGroupApi],
})
export class AddTitleModalComponent extends BaseComponent implements OnInit {
  isLoading = false;
  @HostBinding('class.list__rm-content') appRightContent = true;

  @Input() divisionCode: string;
  @Input() listSelectedOld = [];
  @Input() isManager: boolean;
  @Input() screenTypeTitle: string;
  params: any = {
    pageNumber: 0,
    pageSize: global?.userConfig?.pageSize,
  };
  pageable: Pageable;

  search_value: string;
  listData = [];

  searchType: boolean;

  checkAll = false;
  listSelected = [];
  listTitleCode = [];

  countCheckedOnPage = 0;

  search_form = this.fb.group({
    divisionCode: [{ value: '', disabled: true }],
    isManager: [''],
    search: [''],
  });
  listDivision = [];
  limit = global.userConfig.pageSize;
  listSelectBox = [];

  constructor(injector: Injector, private modalActive: NgbActiveModal, private api: TitleRelationshipService) {
    super(injector);
  }

  ngOnInit() {
    if (this.screenTypeTitle === ScreenType.Create && this.isManager) {
      this.listSelectBox = [
        { name: 'CBQL', value: true },
        { name: 'Nhân viên', value: false },
      ];
    } else if (this.screenTypeTitle === ScreenType.Update && this.isManager) {
      this.listSelectBox = [{ name: 'Nhân viên', value: false }];
    } else {
      this.listSelectBox = [{ name: 'CBQL', value: true }];
    }
    const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
    this.listDivision =
      divisionOfUser?.map((item) => {
        return { code: item.code, displayName: `${item.code || ''} - ${item.name || ''}` };
      }) || [];
    this.search_form.controls.divisionCode.setValue(this.divisionCode);
    this.search_form.controls.isManager.setValue(_.first(this.listSelectBox)?.value);

    if (this.listSelectedOld.length > 0) {
      this.listTitleCode = this.listSelectedOld.map((item) => {
        return {
          titleId: item.titleId,
          ...item,
        };
      });
    }
    this.reload(true);
  }

  paging($event) {
    if (this.isLoading) {
      return;
    }
    this.params.page = _.get($event, 'offset');
    this.reload(false);
  }

  reload(isSearch?: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.params.page = 0;
    }
    const paramSearch = {
      blockCode: this.search_form.controls.divisionCode.value,
      isManager: this.search_form.controls.isManager.value,
      search: this.search_form.controls.search.value,
      isActive: true,
      page: this.params.pageNumber,
      size: this.params.pageSize,
    };
    this.api.searchTitleRelationshipModal(paramSearch).subscribe(
      (response) => {
        this.isLoading = false;
        this.listData = _.get(response, 'content')?.map((item) => {
          return {
            titleId: item.id,
            ...item,
          };
        });
        this.countCheckedOnPage = 0;
        for (const item of this.listData) {
          if (
            this.listTitleCode.findIndex((title) => {
              return title.titleId === item.id;
            }) > -1
          ) {
            this.listSelected.push(item);
            this.countCheckedOnPage += 1;
          }
        }
        this.checkAll = this.listData.length > 0 && this.countCheckedOnPage === this.listData.length;

        this.pageable = {
          totalElements: response.totalElements,
          totalPages: response.totalPages,
          currentPage: response.number,
          size: this.limit,
        };
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  choose() {
    let listData = _.uniqBy(this.listSelected, 'id');
    if (this.screenTypeTitle === ScreenType.Update) {
      listData = listData.concat(this.listSelectedOld);
    }
    listData = _.uniqBy(listData, 'titleId');
    const data = {
      listSelected: listData,
    };
    this.modalActive.close(data);
  }

  closeModal() {
    this.modalActive.close(false);
  }

  onCheckboxRmAllFn(isChecked) {
    this.checkAll = isChecked;
    if (!isChecked) {
      this.countCheckedOnPage = 0;
    } else {
      this.countCheckedOnPage = this.listData.length;
    }
    for (const item of this.listData) {
      item.isChecked = isChecked;
      if (isChecked) {
        if (
          this.listTitleCode.findIndex((title) => {
            return title.titleId === item.titleId;
          }) === -1
        ) {
          this.listTitleCode.push(item);
          this.listSelected.push(item);
        }
      } else {
        this.listTitleCode = this.listTitleCode.filter((title) => {
          return title.titleId !== item.titleId;
        });
        this.listSelected = this.listSelected.filter((itemChoose) => {
          return itemChoose.titleId !== item.titleId;
        });
      }
    }
  }

  onCheckboxRmFn(item, isChecked) {
    if (isChecked) {
      this.listSelected.push(item);
      this.listTitleCode.push(item);
      this.countCheckedOnPage += 1;
    } else {
      this.listSelected = this.listSelected.filter((itemSelected) => {
        return itemSelected.titleId !== item.titleId;
      });
      this.listTitleCode = this.listTitleCode.filter((title) => {
        return title.titleId !== item.titleId;
      });
      this.countCheckedOnPage -= 1;
    }
    this.checkAll = this.countCheckedOnPage === this.listData.length;
  }

  isChecked(item) {
    return (
      this.listTitleCode.findIndex((title) => {
        return title.titleId === item.titleId;
      }) > -1
    );
  }
}
