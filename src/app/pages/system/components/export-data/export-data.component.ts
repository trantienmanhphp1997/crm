import { forkJoin, of } from 'rxjs';
import { global } from '@angular/compiler/src/util';
import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';
import { cleanDataForm } from 'src/app/core/utils/function';
import { FunctionCode } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { ExportDataApi } from '../../services/export-data.api';
import { CommonCategoryService } from 'src/app/core/services/common-category.service';
import { CommonCategory } from 'src/app/core/utils/common-constants';
import { FileService } from 'src/app/core/services/file.service';
import { Utils } from 'src/app/core/utils/utils';

@Component({
  selector: 'app-export-data',
  templateUrl: './export-data.component.html',
  styleUrls: ['./export-data.component.scss'],
})
export class ExportDataComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listData = [];
  listDB = [];
  listColumn = [];
  formSearch = this.fb.group({
    schema: '', // DB name
    command: '', // Query
  });
  paramSearch = {
    size: global.userConfig.pageSize,
    page: 1,
  };
  prevParams: any;
  pageable: Pageable;
  prop: any;
  notificationMessage: any;

  constructor(injector: Injector, private commonCategoryService: CommonCategoryService, private exportDataApi: ExportDataApi, private fileService: FileService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.EXPORT_DATA}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.prop = this.sessionService.getSessionData(FunctionCode.EXPORT_DATA);
    if (this.prop) {
      this.prevParams = { ...this.prop?.prevParams };
      this.paramSearch.page = this.prevParams?.page;
      this.formSearch.patchValue(this.prevParams);
      this.listDB = this.prop?.listDB || [];
      this.isLoading = false;
    } else {
      forkJoin([
        this.commonCategoryService.getCommonCategory(CommonCategory.DB_SCHEMA).pipe(catchError((e) => of(undefined))),
      ]).subscribe(([resDBSchema]) => {
        this.listDB = resDBSchema?.content || [];
        this.listDB.length && this.formSearch.controls.schema.setValue(this.listDB[0].value)

        this.prop = {
          listDB: this.listDB,
        };

        this.sessionService.setSessionData(FunctionCode.EXPORT_DATA, this.prop);
        this.isLoading = false;
      });
    }
  }

  search(isSearch?: boolean) {
    let params: any = {};
    if (isSearch) {
      this.paramSearch.page = 1;
      params = cleanDataForm(this.formSearch);
    } else {
      if (!this.prevParams) {
        return;
      }
      params = this.prevParams;
    }
    // Merge size, page to params
    Object.keys(this.paramSearch).forEach((key) => {
      params[key] = this.paramSearch[key];
    });

    if (!params.command) {
      this.messageService.error(this.notificationMessage.please_input_query);
      return;
    }

    this.isLoading = true;

    this.exportDataApi.selectData(params).subscribe(
      (result) => {
        if (result) {
          this.prevParams = params;
          this.prop.prevParams = params;

          this.listData = result?.data || [];
          this.listColumn = result?.columnName || [];
          this.pageable = {
            totalElements: result.totalRow,
            totalPages: result.totalRow / 10,
            currentPage: params.page - 1,
            size: global.userConfig.pageSize,
          };
        }
        this.sessionService.setSessionData(FunctionCode.EXPORT_DATA, this.prop);
        this.isLoading = false;
      },
      (err) => {
        const message = err?.error?.description || _.get(this.notificationMessage, 'error');
        this.messageService.error(message);
        this.isLoading = false;
      }
    );
  }

  exportFile() {
    const formValue = this.formSearch.value;
    let params = {
      schema: formValue.schema,
      command: formValue.command,
    };

    if (!params.command) {
      this.messageService.error(this.notificationMessage.please_input_query);
      return;
    }

    this.isLoading = true;

    this.exportDataApi.exportData(params).subscribe(
      (res) => {
        if (Utils.isStringNotEmpty(res.idFile)) {
          this.download(res.idFile);
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      },
      (err) => {
        const message = err?.error?.description || _.get(this.notificationMessage, 'export_error');
        this.messageService.error(message);
        this.isLoading = false;
      }
    );
  }

  download(fileId: string) {
    this.fileService.downloadFile(fileId, 'Data.csv').subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.page = pageInfo.offset + 1;
    this.search(false);
  }
}
