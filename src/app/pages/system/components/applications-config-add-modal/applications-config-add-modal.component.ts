import { CategoryService } from './../../services/category.service';
import { Component, OnInit, Injector } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { HttpRespondCode } from 'src/app/core/utils/common-constants';
import { BaseComponent } from 'src/app/core/components/base.component';

@Component({
  selector: 'app-applications-config-add-modal',
  templateUrl: './applications-config-add-modal.component.html',
  styleUrls: ['./applications-config-add-modal.component.scss'],
  providers: [NgbModal, NgbActiveModal],
})
export class ApplicationsConfigAddModalComponent extends BaseComponent implements OnInit {
  isLoading = false;
  form = this.fb.group({
    code: ['', [CustomValidators.required, CustomValidators.code]],
    name: ['', [CustomValidators.required]],
    description: [''],
  });
  notificationMessage: any;

  constructor(injector: Injector, private categoryService: CategoryService) {
    super(injector);
  }

  ngOnInit(): void {}

  confirmDialog() {
    if (this.form.valid) {
      const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result
        .then((res) => {
          if (res) {
            this.isLoading = true;
            const data = cleanDataForm(this.form);
            this.categoryService.createAppCategory(data).subscribe(
              () => {
                this.location.back();
                this.messageService.success(this.notificationMessage.success);
              },
              (e) => {
                if (e?.error) {
                  this.messageService.warn(e?.error?.description);
                } else {
                  this.messageService.error(this.notificationMessage.error);
                }
                this.isLoading = false;
              }
            );
          }
        })
        .catch(() => {});
    } else {
      validateAllFormFields(this.form);
    }
  }

  back() {
    this.location.back();
  }
}
