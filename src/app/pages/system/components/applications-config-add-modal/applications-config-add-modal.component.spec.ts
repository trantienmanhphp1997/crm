import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationsConfigAddModalComponent } from './applications-config-add-modal.component';

describe('ApplicationsConfigAddModalComponent', () => {
  let component: ApplicationsConfigAddModalComponent;
  let fixture: ComponentFixture<ApplicationsConfigAddModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ApplicationsConfigAddModalComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationsConfigAddModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
