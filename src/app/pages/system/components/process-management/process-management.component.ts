import { Component, OnInit, Inject, ViewChild, HostBinding } from '@angular/core';
import { ProcessManagementService } from '../../services/process-management.service';
import { global } from '@angular/compiler/src/util';
import { CommonCategory, FunctionCode } from '../../../../core/utils/common-constants';
import { TranslateService } from '@ngx-translate/core';
import _ from 'lodash';
import { Utils } from '../../../../core/utils/utils';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifyMessageService } from '../../../../core/components/notify-message/notify-message.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { CategoryService } from '../../services/category.service';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { SessionService } from 'src/app/core/services/session.service';
import { EncrDecrService } from '../../../../core/services/encr-decr.service';

@Component({
  selector: 'app-process-management',
  templateUrl: './process-management.component.html',
  styleUrls: ['./process-management.component.scss'],
})
export class ProcessManagementComponent implements OnInit {
  constructor(
    private api: ProcessManagementService,
    private translate: TranslateService,
    private route: ActivatedRoute,
    private messageService: NotifyMessageService,
    private router: Router,
    private commonApi: CategoryService,
    private sessionService: SessionService,
    private enc: EncrDecrService
  ) {
    this.obj = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.PROCESS_MANAGEMENT}`);
    this.translate.get(['fields', 'messageTable', 'notificationMessage']).subscribe((result) => {
      this.fields = _.get(result, 'fields');
      this.messages = _.get(result, 'messageTable');
      this.notificationMessage = _.get(result, 'notificationMessage');
    });
  }

  obj: any;
  isLoading = false;
  fields: string;
  messages: any;
  process_status: Array<any>;
  pageable: Pageable;
  params: any = {
    size: global.userConfig.pageSize,
    page: 0,
    status: '',
  };
  notificationMessage: any;

  prevParams = this.params;
  models: Array<any>;
  offset: number;
  count: number;
  isSearch: boolean;
  limit: number = 10;
  isFisrt: boolean = true;
  prop: string;
  status: Array<any>;

  @ViewChild('table') table: DatatableComponent;

  @HostBinding('class.app__right-content') appRightContent = true;

  ngOnInit(): void {
    this.isLoading = false;

    if (Utils.isNotNull(this.prop)) {
      this.params = this.prop;
    }

    forkJoin([
      this.commonApi.getCommonCategory(CommonCategory.PROCESS_MANAGEMENT_STATUS).pipe(catchError((e) => of(undefined))),
    ]).subscribe(([status]) => {
      this.process_status = _.get(status, 'content');
      this.status = [..._.get(status, 'content')];
      this.process_status = [
        {
          code: '',
          name: 'Tất cả',
        },
        ...this.process_status,
      ];
      this.reload(true);
    });
  }

  ftime: boolean = true;
  paging($event) {
    if (this.isFisrt) {
      return;
    }
    this.params.page = _.get($event, 'offset');
    this.reload(false);
  }

  search() {
    this.reload(true);
  }

  reload(isSearch?: boolean) {
    this.isLoading = true;
    let params: any = {};
    if (isSearch) {
      this.params.page = 0;
    } else {
      params = this.prevParams;
    }
    Object.keys(this.params).forEach((key) => {
      params[key] = this.params[key];
    });
    params.page = this.params.page;
    params.size = this.params.size;
    this.api.fetch(params).subscribe(
      (response) => {
        this.isFisrt = false;
        this.isLoading = false;
        this.models = _.get(response, 'content');
        this.pageable = {
          totalElements: _.get(response, 'totalElements'),
          totalPages: _.get(response, 'totalPages'),
          currentPage: _.get(response, 'number'),
          size: _.get(global, 'userConfig.pageSize'),
        };
        this.prevParams = params;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  create() {
    this.router.navigate(['/system/process-management/create'], { state: this.prevParams });
  }

  update(row) {
    this.router.navigate([`/system/process-management/update/${_.get(row, 'processId')}`], { state: this.prevParams });
  }

  delete(row) {
    this.api.deleteProcess(_.get(row, 'processId')).subscribe(
      () => {
        this.messageService.success(_.get(this.notificationMessage, 'deleteSuccess'));
        this.search();
      },
      () => {
        this.messageService.error(_.get(this.notificationMessage, 'deleteNotSuccess'));
      }
    );
  }

  onActive($event) {
    let type = _.get($event, 'type');
    let id = _.get($event, 'row.processId');
    if (type === 'dblclick') {
      if (Utils.isStringNotEmpty(id)) {
        this.router.navigate([`/system/process-management/view/${id}`], { state: this.prevParams });
      }
    }
  }

  getValue(row: any, key: string) {
    return _.get(row, key);
  }

  startProcess(row) {
    this.api.start(_.get(row, 'processId')).subscribe(
      () => {
        this.messageService.success('Thực hiện chạy tiến trình thành công');
        this.reload(true);
      },
      () => {
        this.messageService.error('Thực hiện chạy tiến trình không thành công');
      }
    );
  }

  stopProcess(row) {
    this.api.stop(_.get(row, 'processId')).subscribe(
      () => {
        this.messageService.success('Thực hiện tạm dừng tiến trình thành công');
        this.reload(true);
      },
      () => {
        this.messageService.error('Thực hiện tạm dừng tiến trình không thành công');
      }
    );
  }

  getStatus(row: any) {
    return _.get(
      _.find(this.status, (x) => x.code === `${_.get(row, 'status')}`),
      'name'
    );
  }
}
