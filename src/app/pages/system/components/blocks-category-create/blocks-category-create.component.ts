import { Component, OnInit, AfterViewInit, Injector } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpRespondCode } from 'src/app/core/utils/common-constants';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { BaseComponent } from 'src/app/core/components/base.component';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { CategoryService } from '../../services/category.service';
@Component({
  selector: 'app-blocks-category-create',
  templateUrl: './blocks-category-create.component.html',
  styleUrls: ['./blocks-category-create.component.scss'],
  providers: [NgbModal],
})
export class BlocksCategoryCreateComponent extends BaseComponent implements OnInit, AfterViewInit {
  maxLength = {
    code: 50,
    name: 200,
    customerType: 550,
  };
  isLoading = false;
  form = this.fb.group({
    code: ['', [CustomValidators.required, CustomValidators.code]],
    name: ['', [CustomValidators.required]],
    customerType: [''],
  });
  customerType = [];
  constructor(injector: Injector, private categoryService: CategoryService) {
    super(injector);
    this.customerType.push({ code: 'INDIV', name: this.fields.customerINDIV });
    this.customerType.push({ code: 'COR', name: this.fields.customerCOR });
  }

  ngOnInit(): void {}

  ngAfterViewInit() {}

  confirmDialog() {
    if (this.form.valid) {
      const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result
        .then((res) => {
          if (res) {
            this.isLoading = true;
            const data = cleanDataForm(this.form);
            this.categoryService.createBlocksCategory(data).subscribe(
              () => {
                this.location.back();
                this.messageService.success(this.notificationMessage.success);
              },
              (e) => {
                if (e?.error) {
                  this.messageService.warn(e?.error?.description);
                } else {
                  this.messageService.error(this.notificationMessage.error);
                }
                this.isLoading = false;
              }
            );
          }
        })
        .catch(() => {});
    } else {
      validateAllFormFields(this.form);
    }
  }

  back() {
    this.location.back();
  }
}
