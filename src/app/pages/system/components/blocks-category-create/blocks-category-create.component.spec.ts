import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlocksCategoryCreateComponent } from './blocks-category-create.component';

describe('BlocksCategoryCreateComponent', () => {
  let component: BlocksCategoryCreateComponent;
  let fixture: ComponentFixture<BlocksCategoryCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BlocksCategoryCreateComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlocksCategoryCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
