import {
  AfterViewInit, ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Optional,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {ControlContainer, FormGroup} from '@angular/forms';
import {distinct, finalize} from 'rxjs/operators';
import {BaseComponent} from 'src/app/core/components/base.component';
import {UploadImgService} from '../../../services/upload-img.service';
import * as _ from 'lodash';
import {cloneDeep, pick} from 'lodash';
import {ADS_CHANNEL, AdvertisementConfigType, typeDoc, typeExcel} from "../../../../../core/utils/common-constants";
import {FileService} from "../../../../../core/services/file.service";
import {RmFluctuateApi} from "../../../../rm/apis";
import {PreviewBannerNewsComponent} from "../../news/preview-img/preview-banner-news.component";
import {MatDialog} from "@angular/material/dialog";
import {Utils} from "../../../../../core/utils/utils";
import {Subscription} from "rxjs";
import {ConfirmDialogComponent} from '../../../../../shared/components';

class ImageSnippet {
  constructor(
    public src: string,
    public file: any
  ) {
  }
}

@Component({
  selector: 'app-image-form-new',
  templateUrl: './image-form-new.component.html',
  styleUrls: ['./image-form-new.component.scss']
})
export class ImageFormNewComponent extends BaseComponent implements OnInit, AfterViewInit {
  @Input() listChannel: Array<any>;
  @Input() baseForm;
  @Input() imgForm: FormGroup;
  @Input() prefixImg: string;
  @Input() newsFeatureConfigList = [];
  @Output() cancelUpdate = new EventEmitter();
  @Output() onCreate = new EventEmitter();
  @Output() onDelete = new EventEmitter();
  @Output() onUpdate = new EventEmitter();
  @Output() onLoading = new EventEmitter();
  @ViewChild('inputFile') inputFile: ElementRef;

  form: FormGroup;
  oldForm: any;
  selectedImage = '';
  nameImage = '';
  prevImg: ImageSnippet;
  isLoading = false;
  updateImage = false;
  isFirstChange = false;
  selectedChannel= [];
//  listChannel = ADS_CHANNEL;
  oneMB = 1048576;
  curType = 'file';
  content;
  fileInfo = [];
  contentPosterTypeChange = new Subscription();
  isViewAddInfo = false;
  constructor(
    injector: Injector,
    private uploadImageService: UploadImgService,
    @Optional() public controlContainer: ControlContainer,
    private fileService: FileService,
    private rmFluctuateApi: RmFluctuateApi,
    public dialog: MatDialog,
    public dtc: ChangeDetectorRef
  ) {
    super(injector)
  }

  ngOnInit(): void {
    // this.isLoading = true;
    // console.log('init');
    // this.commonService.getCommonCategory(CommonCategory.NEWS_FEATURE_CONFIG).pipe(
    //   finalize(() => {
    //     this.isLoading = false;
    //   })
    // ).subscribe((config) => {
    //   this.newsFeatureConfigList = _.get(config, 'content');
    //   this.form.controls.featureCode.setValue(this.newsFeatureConfigList[0].value);
    //   this.form.controls.contentPosterType.setValue(0);
    // })
  }

  ngAfterViewInit(): void {
    this.dtc.detectChanges();
    //
    // console.log(this.form.value);
    // this.form.controls.contentPosterType?.valueChanges.subscribe(value => {
    //   debugger
    //   if (value === 0) {
    //     this.form.controls.featureLink.setValue('');
    //     this.form.controls.featureCode.setValue('');
    //     this.content = '';
    //     this.fileInfo = [];
    //   } else if(value === 1) {
    //     this.form.controls.featureLink.setValue('');
    //     this.form.controls.featureCode.setValue('');
    //   }
    //   else{
    //     this.form.controls.featureCode.setValue(_.head(this.newsFeatureConfigList).code);
    //     this.form.controls.posts.setValue('');
    //     this.content = '';
    //     this.fileInfo = [];
    //   }
    // });

  }

  ngOnChanges(changes: SimpleChanges): void {
    const imgForm = changes.imgForm?.currentValue as FormGroup;
    if (imgForm) {
      this.form = new FormGroup({...imgForm.controls});
      if (this.form.controls.image) {
        this.form.controls.image.setValue(imgForm.value.image);
      }

      //   this.oldForm = this.form.value;
      //  console.log('oldForm: ', this.oldForm);

      if (!this.form) {
        return;
      }
      // this.getImg(this.form.value);
      if (this.selectedImage) {
        this.prevImg = cloneDeep(this.selectedImage);
      }
      this.selectedImage = null;

      this.selectedImage = this.form.get('image').value;
      this.selectedChannel = this.form.value.channel;
      console.log('channel: ', this.listChannel);
      console.log('selected channel: ',this.selectedChannel);
      if(this.selectedChannel && this.selectedChannel.length > 0){
        this.selectedChannel = this.selectedChannel.filter(i => this.isChannelAvailabel(i));
      }
      this.checkViewAppChannel();
      this.content = this.form.value.posts;
      this.fileInfo = this.form.value.attachmentInPosts;
      this.nameImage = this.form.value.nameImage;
      // if (this.contentPosterTypeChange) {
      //   this.contentPosterTypeChange.unsubscribe();
      // }
      //  distinct()
      console.log('form: ', this.form);
      this.contentPosterTypeChange = this.form.controls.contentPosterType?.valueChanges.pipe().subscribe(value => {
        this.dtc.detectChanges();
        if (value == 0) {
          this.form.controls.featureLink.setValue('');
          this.form.controls.featureCode.setValue('');
          this.content = '';
          this.fileInfo = [];
        } else if (value == 1) {
          this.form.controls.featureLink.setValue('');
          this.form.controls.featureCode.setValue('');
          if (!Utils.isStringEmpty(imgForm.value.posts)) {
            this.content = imgForm.value.posts;
          }
          if (imgForm.value.attachmentInPosts && imgForm.value.attachmentInPosts.length > 0) {
            this.fileInfo = imgForm.value.attachmentInPosts;
          }
        } else {
          if (Utils.isStringEmpty(imgForm.value.featureCode)) {
            this.form.controls.featureCode.setValue(_.head(this.newsFeatureConfigList).code);
          } else {
            this.form.controls.featureCode.setValue(imgForm.value.featureCode);
          }
          if (!Utils.isStringEmpty(imgForm.value.featureLink)) {
            this.form.controls.featureLink.setValue(imgForm.value.featureLink);
          }
          this.form.controls.posts.setValue('');
          this.content = '';
          this.fileInfo = [];
        }
        //  this.form.controls.contentPosterType?.updateValueAndValidity({ emitEvent: false });
      });
    } else {
      this.form = undefined;
    }
  }

  isChannelAvailabel(i){
    let isAvailable = false;
    this.listChannel.forEach((item) => {
      if(item?.code === i){
        isAvailable = true;
        return;
      }
    });
    return isAvailable;
  }


  changeFile(fileEvent: any): void {
    const file = fileEvent.target.files[0];
    console.log('image: ', file);
    if (!file) return;
    if (file.size > this.oneMB * 10) {
      this.messageService.warn('Tải ảnh không thành công, kích thước ảnh tối đa 10MB.');
      return;
    }
    if (!this.isValidType(file.type)) {
      this.selectedImage = '';
      this.messageService.warn('Ảnh sai định dạng!');
      return;
    }
    this.updateImage = true;
    this.nameImage =  file?.name;
    this.convertAndSetBase64(file);
  }

  save(): void {
    //   debugger
    this.form.get('image').setValue(this.selectedImage);
    const data = this.form.value;
    this.isFirstChange = true;

    if (this.form.invalid || !this.selectedImage) return;

    this.isFirstChange = false;
    this.onLoading.emit(true);

    data.image = data.image.split(';base64,')[1];
    data.image = this.base64toBlob(data.image.replace(this.prefixImg, ''), 'image/jpeg');
    if (data.id) {
      this.update(data);
    } else {
      this.create(data);
    }
  }

  create(data): void {
    this.isLoading = true;
    this.uploadImageService.uploadImage(data).subscribe(imgRes => {
      this.saveInfo(data, imgRes);
    }, () => {
      this.onLoading.emit(false);
    })
  }

  update(data): void {

    data.updateImage = true;
    if (this.updateImage) {
      this.uploadImageService.uploadImage(data).subscribe(imgRes => {
        this.updateInfo(data, imgRes);
      }, () => {
        this.onLoading.emit(false);
      })
    } else {
      const imgRes = {
        uuidImage: data.uuidImage,
        mimeType: data?.contentType
      }
      this.updateInfo(data, imgRes);
    }

  }

  saveInfo(data, imgRes): void {
    let inputParam = pick(data, ['name', 'priority', 'type']);
    if (data.type === AdvertisementConfigType.POSTER) {
      inputParam = pick(data, ['name', 'priority', 'type', 'startDate', 'endDate', 'numberOfDisplay', 'enableSkipPoster', 'contentPosterType', 'featureCode', 'featureLink', 'posts', 'channel', 'sendNotification']);
      inputParam.channel = this.selectedChannel;
      inputParam.attachmentInPosts = this.fileInfo;
      inputParam.nameImage = this.nameImage
    }
    this.uploadImageService.saveInfo({
      ...inputParam,
      uuidImage: imgRes.uuidImage,
      mimeType: imgRes.mimeType,
      divisionCode: this.baseForm.divisionCodes
    }).pipe(
      finalize(() => {
        this.onLoading.emit(false);
      })
    ).subscribe(imgInfo => {
      imgInfo.id = imgInfo.uuidImage;
      imgInfo.isCreate = false;
      this.messageService.success('Tạo mới thành công!');
      // this.form.patchValue(imgInfo);
      this.onCreate.next(imgInfo);

    }, (err) => {
      this.messageService.error(err?.error?.messages.vn);
    });
  }

  updateInfo(data, imgRes): void {
    let inputParam = pick(data, ['name', 'priority', 'type', 'id']);
    if (data.type === AdvertisementConfigType.POSTER) {
      inputParam = pick(data, ['id', 'name', 'priority', 'type', 'startDate', 'endDate', 'numberOfDisplay', 'enableSkipPoster', 'contentPosterType', 'featureCode', 'featureLink', 'posts', 'channel', 'attachmentInPosts', 'sendNotification']);
      inputParam.channel = this.selectedChannel;
      inputParam.attachmentInPosts = this.fileInfo;
      inputParam.nameImage = this.nameImage
    }
    this.uploadImageService.updateInfo({
      ...inputParam,
      uuidImage: imgRes.uuidImage,
      mimeType: imgRes.mimeType,
      divisionCode: this.baseForm.divisionCodes
    }).pipe(
      finalize(() => {
        this.isLoading = false;
        this.onLoading.emit(false);
      })
    ).subscribe((res) => {
      this.form.get('isUpdate').setValue(false);
      res.uuidImage = imgRes.uuidImage;
      res.image = this.selectedImage;
      // this.form.patchValue(res);
      // console.log('form check: ', this.form);
      //   this.form.patchValue(res);
      this.onUpdate.next();
      this.messageService.success('Cập nhật thành công!');
    }, (er) => {
      this.messageService.error(er?.error?.messages.vn);
      this.isLoading = false;
    });
  }

  get isDisabled(): boolean {
    return this.form.controls.name?.status === 'DISABLED';
  }

  cancel(): void {
    const confirm = this.modalService.open(ConfirmDialogComponent, {windowClass: 'confirm-dialog'});
    confirm.componentInstance.message = 'Thay đổi chưa được lưu bạn chắc muốn quay lại?';
    const {isUpdate, isCreate} = this.form?.value;
    confirm.result
      .then((confirmed: boolean) => {
        if (confirmed) {
          if (isUpdate) {
            if (this.prevImg) {
              this.selectedImage = cloneDeep(this.prevImg);
            }
            this.cancelUpdate.next();
          } else if (isCreate) {
            this.onDelete.next();
          }
        }
        // else {
        //   this.cancelUpdate.next();
        // }
      }).catch(() => {
    });
  }

  getImg(data): void {

    if (!data?.uuidImage) {
      this.selectedImage = null;
      return;
    }

    if (this.selectedImage) {
      this.prevImg = cloneDeep(this.selectedImage);
    }

    this.selectedImage = data.image;
  }

  convertAndSetBase64(file: any): void {
    const reader = new FileReader();
    reader.onload = (event: any) => {
      if (this.selectedImage) {
        this.prevImg = cloneDeep(this.selectedImage);
      }

      this.selectedImage = event.target.result;
      this.form.get('image').setValue(this.selectedImage);
    }

    reader.readAsDataURL(file);
  }

  isValidType(type: string): boolean {
    return type.includes('image/');
  }

  isValidTypePost(type: string): boolean {
    return typeExcel.includes(type) || type.includes('image/') || type.includes('application/pdf') || typeDoc.includes(type);
    // return type.includes('image/');
  }

  get canCancel(): boolean {
    return this.form?.value?.isUpdate || this.form?.value?.isCreate;
  }

  onPress(event): void {
    let value = event.target.value;
    const invalid = this.baseForm.type === AdvertisementConfigType.BANNER ? !value.match(/^[1-5]$/g) : Number(value) !== 6;
    // value = invalid ? (this.baseForm.type === AdvertisementConfigType.POSTER ? 6 : null) : Number(value);
    //  if(this.baseForm.type === AdvertisementConfigType.POSTER){
    //    if(value > 99){
    //      value = 99;
    //    }
    //  }
    event.target.value = value;
    this.form.controls.priority.setValue(value);
  }

  base64toBlob(base64Data, contentType): Blob {

    contentType = contentType || '';

    const sliceSize = 1024;

    const byteCharacters = atob(base64Data);

    const bytesLength = byteCharacters.length;

    const slicesCount = Math.ceil(bytesLength / sliceSize);

    const byteArrays = new Array(slicesCount);


    for (let sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {

      const begin = sliceIndex * sliceSize;

      const end = Math.min(begin + sliceSize, bytesLength);


      const bytes = new Array(end - begin);

      for (let offset = begin, i = 0; offset < end; ++i, ++offset) {

        bytes[i] = byteCharacters[offset].charCodeAt(0);

      }

      byteArrays[sliceIndex] = new Uint8Array(bytes);

    }

    return new Blob(byteArrays, {type: contentType});

  }

  //thom bo sung them
  get fileIdList(): string[] {
    return Object.keys(this.fileInfo);
  }

  download(fileId: string) {
    const title1 = this.fileInfo[fileId].name;
    this.fileService.downloadFile(this.fileInfo[fileId].id, title1).subscribe((res) => {
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }

  removeFile(id: string): void {
    const info = this.fileInfo[id];
    if (!info) return;
    _.remove(this.fileInfo, info);
    // delete this.fileInfo[id];
    // this.form.controls.attachments.setValue(this.getFileList(this.form.controls.attachments.value));
  }

  onContentChange(event) {
    this.form.controls.posts.setValue(this.content);
  }

  upload(fileEvent): void {
    const file = fileEvent.target.files[0];
    if (file.size > this.oneMB * 10) {
      this.messageService.warn('Tải tệp không thành công, kích thước tệp tối đa 10MB.');
      return;
    }
    if (!this.isValidTypePost(file.type)) {
      this.messageService.warn('Tập tin sai định dạng!');
      return;
    }
    const formData = new FormData();
    formData.append('file', file);
    this.rmFluctuateApi.uploadFile(formData).subscribe(id => {
      this.saveInfoFileUpload(id, file.size);
    })
  }

  saveInfoFileUpload(id: string, size): void {
    this.rmFluctuateApi.getFileInfo(id).pipe(
      finalize(() => {
      })
    ).subscribe(res => {
      let formatedSize;
      const s = Number(size / this.oneMB);
      if (s.toString().includes('.')) {
        formatedSize = Number(s).toFixed(2);
      } else {
        formatedSize = s;
      }
      this.fileInfo[this.fileInfo.length] = {
        id: res.id,
        name: res.fileName,
        // size: `${formatedSize} MB`,
        // filePath: res.filePath,
        // type: this.curType,
        fileSize: +formatedSize
      };
      //   this.fileInfo[this.fileInfo.length] = res.fileName;
      //  this.form.controls.attachmentInPosts.setValue(this.fileInfo);
      //  console.log('form chekc file: ',this.form);
    });
  }

  checkDisableSaveBtn() {
    if (this.baseForm.type === AdvertisementConfigType.BANNER) {
      return this.form.invalid || !this.selectedImage;
    }
    if (this.form.controls.contentPosterType.value == '0') {
      return this.form.invalid || _.isEmpty(this.selectedChannel) || !this.selectedImage;
    } else if (this.form.controls.contentPosterType.value == '1') {
      return (this.form.invalid || _.isEmpty(this.content)) || _.isEmpty(this.selectedChannel) || !this.selectedImage;
    } else if (this.form.controls.contentPosterType.value == '2') {
      return (this.form.invalid || !this.selectedImage || _.isEmpty(this.selectedChannel) || (this.form.controls.featureCode.value === 'KHAC' && _.isEmpty(this.form.controls.featureLink.value)));
    }
  }

  previewBanner() {
    const dialogRef = this.dialog.open(PreviewBannerNewsComponent, {
      panelClass: 'poster',
      disableClose: true,
      data: {
        image: this.selectedImage,
      }
    });
  }

  getFormValue(name: string) {
    return this.form.get(name).value ? this.form.get(name).value : '';
  }
  checkViewAppChannel(){
    console.log('select: ', this.selectedChannel);
    let check = false;
    this.isViewAddInfo = false;
    if(this.selectedChannel && this.selectedChannel.length > 0){
      this.selectedChannel.forEach((i) => {
        if(i.toUpperCase().includes('APP')){
          this.isViewAddInfo = true;
          check = true;
          return;
        }
      });
      if(!check){
        this.isViewAddInfo = false;
      }
    }
  }
}
