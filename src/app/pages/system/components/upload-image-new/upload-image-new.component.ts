import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormGroup, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs';
import {finalize, mergeMap} from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import {
  ADS_IMAGE_STATUS_LIST,
  AdvertisementConfigType,
  CommonCategory, ConfirmType,
  FunctionCode,
  SessionKey
} from 'src/app/core/utils/common-constants';
import { pick, map, orderBy, first } from 'lodash';
import { CategoryService } from '../../services/category.service';
import { UploadImgService } from '../../services/upload-img.service';
import {ImageFormNewComponent} from "./image-form-new/image-form-new.component";
import {cleanDataForm} from "../../../../core/utils/function";
import * as _ from 'lodash';
import {types} from "util";
import {CustomValidators} from "../../../../core/utils/custom-validations";
import {ConfirmDialogComponent} from "../../../../shared/components";
@Component({
  selector: 'app-upload-image-new',
  templateUrl: './upload-image-new.component.html',
  styleUrls: ['./upload-image-new.component.scss']
})
export class UploadImageNewComponent extends BaseComponent implements OnInit {

  form = this.fb.group({
    divisionCodes: '',
    type: '',
    images: this.fb.array([]),
    isActived: true
  });
  divisionForm = this.fb.group({});
  listDivision = [];
  listStatus = ADS_IMAGE_STATUS_LIST;
  // new
  groupList = [];
  selectedImageIndex = 0;
  imageList = [];
  selectedForm: FormGroup;
  raw: any;
  searchParams: any;
  isFirstLoading = true;
  imageFromDivision = [];
  isReloadFromAction = false;
  prefixImg = 'data:image/jpeg;base64,';
  newsFeatureConfigList = [];
  checkButton = false;
  listChannel = [];
  listChannelByBlock = [];

  @ViewChild('imgForm') imgFormComp: ImageFormNewComponent;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private uploadImageService: UploadImgService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.IMAGE_MANAGER}`);
  }

  ngOnInit(): void {
    const list = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM);
    this.listDivision = map(list, (item) => {
      return { code: item.code, name: `${item.code} - ${item.name}` };
    });
  //  this.listDivision = orderBy(this.listDivision, ['name'], ['asc', 'desc']);
    this.form.get('divisionCodes').setValue(first(this.listDivision)?.code);
    this.getConfig();

  }

  ngAfterViewInit(): void {
    // this.form.controls.division.valueChanges.subscribe(value => {
    //   this.setInactiveVaidate();
    //   this.findImagesWithDivision(value);
    // })
    //
    // this.form.controls.type.valueChanges.subscribe(value => {
    //   this.setInactiveVaidate();
    //   if (this.isFirstLoading) return;
    //   const divisionCode = this.form.value.division;
    //   const imageForm = this.divisionForm.controls[divisionCode] as FormArray;
    //
    //   this.removeTrashForm(imageForm);
    //
    //   this.setImageList(imageForm, value);
    //
    //   if (!this.imageList.length) {
    //     this.setSelectedForm(null);
    //   } else {
    //     const index = this.getFirstIndex(imageForm, value);
    //
    //     this.setSelectedForm(imageForm.controls[index]);
    //     this.setStatus();
    //   }
    // })
  }

  buildForm(): void {

    this.listDivision.forEach(division => {
      const divisionCode = division?.code;
      this.divisionForm.addControl(divisionCode, this.fb.array([]))
      const divisionForm = this.divisionForm.controls[divisionCode] as FormArray;

      if (this.imageFromDivision.length && this.form.value.divisionCodes === divisionCode) {
        this.imageFromDivision.forEach((item, i) => {
          this.setImgForm(divisionForm, i, item);

        });
        // this.divisionForm.controls[divisionCode].valueChanges.subscribe(() => {
        //   this.checkDuplicate();
        // });
      }

      // this.divisionForm.controls[divisionCode].valueChanges.subscribe(() => {
      //   this.checkDuplicate();
      // });
    })

    const firstDivisionCode = this.listDivision[0]?.code;
    const imageForm = this.divisionForm.controls[firstDivisionCode] as FormArray;
    const index = (imageForm as FormArray).getRawValue().findIndex(item => item.type === this.form.value.type);

    this.setSelectedForm(imageForm.at(index));

    if (this.raw?.uuidImage) this.selectedForm.disable();

    this.setImageList(imageForm);
  }

  setImgForm(formArray: FormArray, index: number, data = {}): void {
    formArray.push(
      this.fb.group(this.getTemp(data))
    );


  }

  getConfig(): void {
    this.isLoading = true;
  //  const divisionCode = this.form.value.division;
    forkJoin([
      this.categoryService.getCommonCategory(CommonCategory.BANNER_CONFIG),
      this.commonService.getCommonCategory(CommonCategory.NEWS_FEATURE_CONFIG),
      this.commonService.getCommonCategory(CommonCategory.APPLICATION_CHANNEL),
    //  this.uploadImageService.getImgFromDivisions({ divisionCodes: divisionCode }),
    ]).pipe(
      finalize(() => {
        this.isFirstLoading = false;
      })
    ).subscribe(([config,lstFeatureConfig, lstChannel]) => {
      this.newsFeatureConfigList = _.get(lstFeatureConfig, 'content');
      this.groupList = config.content || [];
      this.form.controls.type.setValue(this.groupList[0]?.value);
    //  this.imageFromDivision = firstImages || [];
      this.form.controls.type.setValue(this.groupList[0]?.value);
      const param = cleanDataForm(this.form);
      this.searchParams = param;
      this.listChannel = _.get(lstChannel,'content');
      this.setListChannelByBlock();
      this.uploadImageService.getImgFromDivisions(param).subscribe((firstImages) => {
        if(firstImages && firstImages.length > 0 && this.form.value.type === AdvertisementConfigType.BANNER){
          firstImages = this.addPrefix(firstImages);
        }
        this.imageFromDivision = firstImages || [];
        this.isLoading = false;
        this.buildForm();
      }, (er) => {
        this.imageFromDivision = [];
        this.isLoading = false;
        this.buildForm();
      });
    })
  }



  // with type
  getFormArrayLength(formArray: FormArray, type = null): number {
    return type && type === AdvertisementConfigType.POSTER ? 5 : (formArray as FormArray).getRawValue().filter(item => item.type === (this.form.value.type)).length;
  }

  create(type = undefined) {
    if (this.selectedForm && this.selectedForm.getRawValue().isUpdate) {
      this.selectedForm.patchValue(this.raw);
    }

    if (!type) type = this.form.value.type;

    const { formArray } = this.getCurrentForm();

    if (!formArray) return;

    let index = this.getFormArrayLength(formArray, type);
    let dataForm: any = {type};
    if(AdvertisementConfigType.POSTER === type){
      dataForm = {
        ...dataForm,
        numberOfDisplay: 1,
        enableSkipPoster: true,
        sendNotification: true
      }
    }
    formArray.push(
      this.fb.group(this.getTemp(dataForm))
    );
    index = formArray.length - 1;
    this.setSelectedForm(formArray.controls[index]);
    this.selectedForm.controls.isCreate.setValue(true);
    this.checkButton = true;
    this.setStatus('enable');

  }

  update() {
    this.selectedForm.enable();
    this.checkButton = true;
    this.selectedForm.controls.isUpdate.setValue(true);
  }

  deleteInfo() {
   // const confirm = this.modalService.open(ConfirmDialogComponent, {windowClass: 'confirm-dialog'});
    const confirm = this.confirmService.openModal(ConfirmType.Confirm, 'Bạn có chắc chắn muốn xóa ảnh này?');

    confirm.then(res => {
      if (!res) return;
      const { formArray } = this.getCurrentForm();
      const index = this.getCurrentIndex();

      this.isLoading = true;

      this.uploadImageService.deleteInfo(pick(this.selectedForm.getRawValue(), ['divisionCode', 'uuidImage'])).pipe(
        finalize(() => {
          this.isLoading = false;
        })
      ).subscribe(() => {
        const indexOfImgList = this.imageList.findIndex(item => item.uuidImage === this.selectedForm.getRawValue().uuidImage);
        this.imageList.splice(indexOfImgList, 1);

        formArray.removeAt(index);
        if(formArray.length > 0){
          this.setImageList(formArray);
          const fisrtIndex = this.getFirstIndex(formArray);
          this.setSelectedForm(formArray.controls[fisrtIndex]);
        }
        else{
          this.selectedForm = undefined;
        }
        this.messageService.success('Xóa thành công!');
      });
    })

  }

  chooseForm(uuidImage: string) {
    const { formArray } = this.getCurrentForm();

    this.setInactiveVaidate();

    // revert prev form
    this.selectedForm.patchValue(this.raw);

    this.removeTrashForm(formArray);

    const index = formArray.getRawValue().findIndex(item => item.uuidImage === uuidImage);

    this.setSelectedFormV2(formArray.at(index));

    this.checkButton = false;
    this.setStatus();
  }

  removeTrashForm(formArray: FormArray): void {
    // remove all form not save
    formArray.getRawValue().forEach((v, i) => {
      if (!v.uuidImage) formArray.removeAt(i);
    })
  }

  // get current form with division and type
  getCurrentForm(i = null) {

    const { divisionCodes } = this.form.value;

    let currentDivision = this.divisionForm.controls[divisionCodes] as FormArray;

    if (!currentDivision) return { formGroup: null, formArray: null };

    const index = i !== null ? i : currentDivision.length - 1;

    return { formGroup: currentDivision.controls[index] as FormGroup, formArray: currentDivision, index };
  }

  getTemp(data: any = {}) {

    const searchData = this.form.value;

    // uuidImage is id
  //  let { uuidImage, type, priority, name, divisionCode, id } = data;
    let { uuidImage, type, priority, name, divisionCode, id, startDate, endDate, numberOfDisplay, enableSkipPoster, contentPosterType, featureCode, featureLink,posts,channel,contentType,sendNotification,nameImage } = data;
    if (!type) type = searchData.type;
    let returnData: any;
    if(type === AdvertisementConfigType.BANNER){
      returnData = {
        id,
        name: [name, [Validators.required]],
        priority: [priority, Validators.required],
        image: data.image,
        divisionCode,
        type,
        isUpdate: false,
        isCreate: !uuidImage,
        contentType: data?.contentType,
        uuidImage
      }
    }
    else if (type === AdvertisementConfigType.POSTER) {
      //   priority = 6;
      returnData = {
        id,
        name: [name, [Validators.required]],
        priority: [priority, Validators.required],
        image: data.image,
        divisionCode,
        type,
        isUpdate: false,
        isCreate: !uuidImage,
        uuidImage,
        contentType: data?.contentType,
        startDate: [data?.startDate ? new Date(data?.startDate) : '', CustomValidators.required],
        endDate: [data?.endDate ? new Date(data?.endDate) : '', CustomValidators.required],
        numberOfDisplay: [data?.numberOfDisplay,Validators.required],
        enableSkipPoster: data?.enableSkipPoster,
        contentPosterType: data?.contentPosterType ? data?.contentPosterType : 0,
        featureCode: data?.featureCode,
        featureLink: data?.featureLink,
        posts: data?.posts,
        channel: this.fb.array(data?.channel || []),
        sendNotification: data?.sendNotification,
        attachmentInPosts: this.fb.array(data?.attachmentInPosts || []),
        nameImage: data?.nameImage
      }
    }

    return returnData;
  }

  findImagesWithDivision(): void {
    this.isLoading = true;
    const param = cleanDataForm(this.form);
    this.searchParams = param;
    this.uploadImageService.getImgFromDivisions(param).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(images => {
      if(images && images.length > 0 && this.form.value.type === AdvertisementConfigType.BANNER){
        images = this.addPrefix(images);
      }
      this.imageFromDivision = images || [];
      if(images || _.isEmpty(images)){
        this.selectedForm = undefined;
      }
      this.setImgFormGroup(this.form.controls.divisionCodes.value);
    });
  }

  cancelUpdate(): void {
    this.selectedForm.patchValue(this.raw);
    this.checkButton = false;
    const value = this.selectedForm.getRawValue();
    this.selectedForm.controls.isUpdate.setValue(false);
    if (value.uuidImage) {
      this.setStatus();
    }
  }

  setStatus(status = 'disable'): void {
    setTimeout(() => {
       status === 'disable' ? this.selectedForm.disable() : this.selectedForm.enable();
    }, 0)
  }

  get canCreate(): boolean {
    switch (this.form?.value?.type) {
      case AdvertisementConfigType.BANNER:
        return (this.divisionForm?.controls[this.form?.value?.divisionCodes] as FormArray)?.controls.filter((item: FormGroup) => item.getRawValue().type === AdvertisementConfigType.BANNER).length < 5;
      case AdvertisementConfigType.POSTER:
        return true;
      // return (this.divisionForm?.controls[this.form?.value?.division] as FormArray)?.controls.filter((item: FormGroup) => item.getRawValue().type === 'poster').length < 1;
    }

  }

  get haveFormNotSave(): boolean {
    return !!(this.divisionForm?.controls[this.form?.value?.divisionCodes] as FormArray)?.controls.find((item: FormGroup) => !item.getRawValue().uuidImage);
  }

  checkDuplicate(): void {
    const { formArray } = this.getCurrentForm();
    const duplicates = ['name', 'priority'];

    duplicates.forEach(key => {
      if (this.selectedForm) {
        const isExist = (formArray as FormArray).getRawValue().filter(item => item[key] === this.selectedForm.getRawValue()[key]);
        let errors = this.selectedForm.get(key).errors;
        if (isExist.length > 1) {
          errors = errors ? {...errors, duplicated: true} : {duplicated: true};
        } else if (errors) {
          delete errors.duplicated;
        }
        this.selectedForm.get(key).setErrors(errors);
      }

    })

  }

  setImgFormGroup(divisionCode: string): void {
    const divisionForm = this.divisionForm.controls[divisionCode] as FormArray;
    divisionForm.clear();
    this.imageList = [];
    if (this.imageFromDivision.length) {
      this.imageFromDivision.forEach((item, i) => {
        this.setImgForm(divisionForm, i, item);
      });
    }

    const imageForm = this.divisionForm.controls[divisionCode] as FormArray;

    this.setImageList(imageForm);

    if (!this.isReloadFromAction) {
      const firstIndex = this.getFirstIndex(imageForm);

      this.setSelectedFormV2(imageForm.controls[firstIndex] as FormGroup);
    }
    else{
      if (this.selectedForm) this.setStatus();
    }
    this.isReloadFromAction = false;
  }

  onCreate(): void {
    this.checkButton = false;
    this.findImagesWithDivision();
  }

  // remove create form
  onDelete(): void {
    this.checkButton = false;
    this.findImagesWithDivision();
  }

  setSelectedForm(formGroup: any): void {
    if(formGroup.value.type === AdvertisementConfigType.POSTER && formGroup.value?.uuidImage){
      this.isLoading = true;
      this.uploadImageService.getImagBase64(formGroup.value.uuidImage).subscribe((res) => {
        if(res){
          formGroup.value.image = `${this.prefixImg}${res?.base64Data}`;
          formGroup.value.fileName = res?.fileName;
          this.selectedForm = formGroup;
          this.raw = this.selectedForm?.getRawValue();
        }
        this.isLoading = false;
      });
    }
    else{
      this.isLoading = false;
      this.selectedForm = formGroup;
      this.raw = this.selectedForm?.getRawValue();
    }

  }
  setSelectedFormV2(formGroup: any): void {
    if(formGroup.value.type === AdvertisementConfigType.POSTER && formGroup.value?.uuidImage){
      this.isLoading = true;
      this.uploadImageService.getImagBase64(formGroup.value.uuidImage).subscribe((res) => {
        if(res){
          formGroup.value.image = `${this.prefixImg}${res?.base64Data}`;
          formGroup.value.fileName = res?.fileName;
          this.selectedForm = formGroup;
          this.raw = this.selectedForm?.getRawValue();
          if (this.selectedForm)
            this.setStatus();
          this.isLoading = false;
        }
      }, (err) => {
        this.selectedForm = formGroup;
        this.raw = this.selectedForm?.getRawValue();
        if (this.selectedForm) this.setStatus();
        this.isLoading = false;
      });
    }
    else{
      this.selectedForm = formGroup;
      this.raw = this.selectedForm?.getRawValue();
      if (this.selectedForm) this.setStatus();
    }

  }

  getCurrentIndex(): number {
    const {formArray} = this.getCurrentForm();
    return formArray.getRawValue().findIndex(item => item.uuidImage === this.selectedForm.getRawValue().uuidImage);
  }

  setImageList(formArray: FormArray, type: string = ''): void {
    this.imageList = formArray.getRawValue().filter(item => item.type === (type || this.form.value.type));
    this.imageList = orderBy(this.imageList, ['priority'], ['asc']);
  }

  onUpdate(): void {
    // this.isReloadFromAction = true;
    //  console.log('update: ', value);
    this.checkButton = false;
    this.findImagesWithDivision();
  }

  // first index of type
  getFirstIndex(formArray: FormArray, type = null): number {
    if (!type) type = this.form.value.type;
    const firstItem = this.imageList.find(item => item?.type === type);

    return formArray.getRawValue().findIndex(item => item.uuidImage === firstItem?.uuidImage);
  }

  setInactiveVaidate(): void {
    if (this.imgFormComp) {
      this.imgFormComp.isFirstChange = false;
    }
  }

  onLoading(status: boolean): void {
    this.isLoading = status;
    this.ref.detectChanges();
  }

  addPrefix(imageList: any[]): any[] {
    return imageList.map(item => {
      item.image = `${this.prefixImg}${item.image}`;
      return item;
    });
  }
  search(){
    this.imageList = [];
    this.selectedForm = undefined;
    this.checkButton = false;
    this.setListChannelByBlock();
    this.setInactiveVaidate();
    this.findImagesWithDivision();
  }
  setListChannelByBlock(){
    this.listChannelByBlock = _.filter(this.listChannel, (i) => i.value.includes(this.form.value.divisionCodes));
    this.listChannelByBlock = this.listChannelByBlock.map((item) => {
      return {
        code: item?.code,
        name: item?.name
      }
    });
  }
}
