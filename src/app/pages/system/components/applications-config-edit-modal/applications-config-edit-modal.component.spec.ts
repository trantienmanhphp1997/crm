import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationsConfigEditModalComponent } from './applications-config-edit-modal.component';

describe('ApplicationsConfigEditModalComponent', () => {
  let component: ApplicationsConfigEditModalComponent;
  let fixture: ComponentFixture<ApplicationsConfigEditModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ApplicationsConfigEditModalComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationsConfigEditModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
