import { CategoryService } from './../../services/category.service';
import { Component, OnInit, Injector } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { BaseComponent } from 'src/app/core/components/base.component';

@Component({
  selector: 'app-applications-config-edit-modal',
  templateUrl: './applications-config-edit-modal.component.html',
  styleUrls: ['./applications-config-edit-modal.component.scss'],
  providers: [NgbModal],
})
export class ApplicationsConfigEditModalComponent extends BaseComponent implements OnInit {
  isLoading = false;
  data: any;
  isUpdate = true;
  form = this.fb.group({
    id: [''],
    code: ['', [CustomValidators.required, CustomValidators.code]],
    name: ['', CustomValidators.required],
    description: [''],
  });

  constructor(injector: Injector, private categoryService: CategoryService) {
    super(injector);
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.categoryService.getAppCategoryById(this.route.snapshot.paramMap.get('id')).subscribe(
      (data) => {
        this.data = data;
        this.form.patchValue(this.data);
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  back() {
    this.location.back();
  }

  confirmDialog() {
    if (!this.isUpdate) {
      return;
    }
    if (this.form.valid) {
      const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result
        .then((res) => {
          if (res) {
            this.isLoading = true;
            const data = cleanDataForm(this.form);
            this.categoryService.updateAppCategory(data).subscribe(
              () => {
                this.location.back();
                this.messageService.success(this.notificationMessage.success);
              },
              (e) => {
                if (e && e.error && e.error.code) {
                  this.messageService.warn(this.notificationMessage[e.error.code]);
                } else {
                  this.messageService.error(this.notificationMessage.error);
                }
                this.isLoading = false;
              }
            );
          }
        })
        .catch(() => {});
    } else {
      validateAllFormFields(this.form);
    }
  }
}
