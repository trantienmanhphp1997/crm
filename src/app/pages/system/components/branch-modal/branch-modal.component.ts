import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { global } from '@angular/compiler/src/util';
import { Component, OnInit, ViewEncapsulation, ViewChild, AfterViewInit, Input, HostBinding } from '@angular/core';
import { ColumnMode, DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { Utils } from '../../../../core/utils/utils';
import _ from 'lodash';

@Component({
  selector: 'app-branch-modal',
  templateUrl: './branch-modal.component.html',
  styleUrls: ['./branch-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class BranchModalComponent implements OnInit, AfterViewInit {
  constructor(private modalActive: NgbActiveModal) { }
  isLoading = true;
  rows = [];
  temp = [];
  listChoose: any;
  @Input() listBranchOld: any;
  @Input() listBranch: any;
  ColumnMode = ColumnMode;
  textSearch = '';
  is_checked: boolean;
  model: Array<any> = [];
  SelectionType = SelectionType;
  @ViewChild(DatatableComponent) public table: DatatableComponent;
  @HostBinding('class.app-branch-modal') chooseBranchesModal = true;

  ngOnInit(): void {
    const timer = setTimeout(() => {
      let listChildren: Array<any>;
      this.listBranch.forEach((item) => {
        if (item?.parentCode === item?.code) {
          delete item.parentCode;
          listChildren = this.listBranch?.filter((branch) => {
            return branch.parentCode === item.code;
          });
        } else {
          listChildren = this.listBranch?.filter((branch) => {
            return branch?.parentCode === item?.code;
          });
          const value = this.listBranch?.find((branch) => {
            return branch?.code === item?.parentCode;
          });
          if (Utils.isNotNull(value)) {
            item.parentName = value?.name;
          }
        }
        item['treeStatus'] = listChildren.length ? 'expanded' : 'disabled';
        this.rows.push(item);
      });
      this.rows.sort((a, b) => {
        if (a?.code?.toLowerCase() < b?.code?.toLowerCase()) {
          return -1;
        }
        if (a?.code?.toLowerCase() > b?.code?.toLowerCase()) {
          return 1;
        }
        return 0;
      });
      this.temp = [...this.rows];
      this.rows = [...this.rows];
      if (Utils.isArrayNotEmpty(this.listBranchOld)) {
        this.model = _.chain(this.rows)
          .filter((x) => x.code === this.listBranchOld[0].code)
          .value();
      }
      clearTimeout(timer);
      this.isLoading = false;
    }, 100);
  }

  ngAfterViewInit() {
    this.table.messages = global?.messageTable;
  }

  search() {
    this.updateFilter();
  }

  onSelect($event) { }

  onTreeAction(event: any) {
    const row = event.row;
    if (row.treeStatus === 'collapsed') {
      row.treeStatus = 'expanded';
    } else {
      row.treeStatus = 'collapsed';
    }
    this.rows = [...this.rows];
  }

  updateFilter() {
    const val = this.textSearch.trim().toLowerCase();
    this.rows = [];

    // filter our data
    const temp = this.temp.filter(function (d) {
      return d?.code?.toLowerCase().indexOf(val) !== -1 || d?.name?.toLowerCase().indexOf(val) !== -1 || !val;
    });
    let listResult: any = [];
    listResult = [...temp];
    temp.forEach((item) => {
      listResult = [...listResult, ...this.findParentBranch(item, this.temp, [])];
    });
    listResult.sort((a, b) => {
      if (a?.code?.toLowerCase() < b?.code?.toLowerCase()) {
        return -1;
      }
      if (a?.code?.toLowerCase() > b?.code?.toLowerCase()) {
        return 1;
      }
      return 0;
    });

    // update the rows
    this.rows = [...new Set(listResult)];

    _.forEach(this.listChoose, (item) => {
      const itemOld = this.rows.find((row) => {
        return row.code === item.code;
      });
      if (itemOld) {
        item = itemOld;
      }
    });
  }

  findParentBranch(item: any, listAll: any[], listResult: any[]) {
    if (item.parentCode) {
      const itemOld = listAll.find((obj) => {
        return obj.code === item.parentCode;
      });
      if (itemOld) {
        listResult.push(itemOld);
        this.findParentBranch(itemOld, listAll, listResult);
      }
    }
    return listResult;
  }

  choose() {
    this.modalActive.close(this.model);
  }

  closeModal() {
    this.modalActive.close(false);
  }
}
