import { forkJoin } from 'rxjs';
import { Component, OnInit, ViewChildren, QueryList, AfterViewInit, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../../services/users.service';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { maxInt32, rolesDefault } from 'src/app/core/utils/common-constants';
import { CategoryService } from '../../services/category.service';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { RmProfileService } from 'src/app/core/services/rm-profile.service';

declare var $: any;

@Component({
  selector: 'app-user-edit-modal',
  templateUrl: './user-edit-modal.component.html',
  styleUrls: ['./user-edit-modal.component.scss'],
  providers: [NgbModal],
})
export class UserEditModalComponent extends BaseComponent implements OnInit, AfterViewInit {
  isLoading = false;
  data: any;
  isUpdate = true;
  listBranches: any;
  listRoleOfUser: any;
  form = this.fb.group({
    id: [''],
    username: [{ value: '', disabled: true }, [CustomValidators.required, CustomValidators.noSpace]],
    fullName: ['', [CustomValidators.required]],
    branch: ['', [CustomValidators.required]],
    email: ['', [CustomValidators.required, Validators.email]],
    phoneNumber: ['', [CustomValidators.required, CustomValidators.phoneMobileVN]],
    active: [''],
    hrsCode: [''],
    code: [''],
  });
  @ViewChildren('otpBranch') otpBranch: QueryList<any>;
  notificationMessage: any;
  pageable: Pageable;
  dataRole = [];
  listRole = [];
  paramSearch = {
    max: 10,
    first: 0,
    search: '',
  };
  tooltip: any;
  pageSize = 10;
  isList = false;
  roleSelected = [];

  constructor(
    injector: Injector,
    private userService: UserService,
    private categoryService: CategoryService,
    private rmProfileService: RmProfileService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.form.disable();
    const username = this.route.snapshot.paramMap.get('username');
    this.isLoading = true;
    forkJoin([
      this.userService.getUserByUserName(username),
      this.categoryService.searchBranches({ page: 0, size: maxInt32 }),
      this.categoryService.searchRole({ page: 0, size: maxInt32 }),
    ]).subscribe(
      ([itemUser, listBranches, listRoles]) => {
        this.data = itemUser;
        this.listBranches = listBranches.content;
        this.form.patchValue(this.data);
        this.dataRole =
          listRoles?._embedded?.keycloakRoleList?.filter((role) => {
            return rolesDefault.indexOf(role.name) === -1;
          }) || [];
        const totalSize = this.dataRole?.length || 0;
        this.pageable = {
          totalElements: totalSize,
          totalPages: Math.ceil(totalSize / this.pageSize),
          currentPage: Math.floor(this.paramSearch.first / this.pageSize),
          size: this.pageSize,
        };
        this.listRole =
          this.dataRole
            ?.filter((item) => {
              return (
                item.name.toLowerCase().includes(this.paramSearch?.search?.toLowerCase()) ||
                item.description.toLowerCase().includes(this.paramSearch?.search?.toLowerCase())
              );
            })
            ?.slice(this.paramSearch.first, this.paramSearch.max) || [];
        this.getRolesOfUser();
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
    if (!this.isUpdate) {
      this.form.disable();
    }
  }

  getRolesOfUser() {
    this.rmProfileService.getRolesByUser(this.data.username).subscribe((roles) => {
      this.roleSelected = roles || [];
      for (const itemRole of this.dataRole) {
        itemRole.isChecked =
          this.roleSelected?.findIndex((role) => {
            return role?.name === itemRole?.name;
          }) > -1;
      }
      this.dataRole?.sort((a, b) => {
        return a?.isChecked === b?.isChecked ? 0 : a?.isChecked ? -1 : 1;
      });
      this.searchRole(true);
    });
  }

  searchRole(isSearch: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.paramSearch.first = 0;
      this.paramSearch.max = this.pageSize;
      this.paramSearch.search = this.paramSearch.search.trim();
    }
    const dataResult =
      this.dataRole?.filter((item) => {
        return (
          item?.name?.toLowerCase().includes(this.paramSearch.search.toLowerCase()) ||
          item?.description?.toLowerCase().includes(this.paramSearch.search.toLowerCase()) ||
          !this.paramSearch.search
        );
      }) || [];
    const totalSize = dataResult?.length || 0;
    this.pageable = {
      totalElements: totalSize,
      totalPages: Math.ceil(totalSize / this.pageSize),
      currentPage: Math.floor(this.paramSearch.first / this.pageSize),
      size: this.pageSize,
    };
    this.listRole = dataResult?.slice(this.paramSearch.first, this.paramSearch.max) || [];
    this.isLoading = false;
  }

  setPage(pageInfo) {
    this.paramSearch.first = pageInfo.offset * this.pageSize;
    this.paramSearch.max = this.paramSearch.first + this.pageSize;
    this.searchRole(false);
  }

  onCheckboxRoleFn(row, isChecked) {
    this.isLoading = true;
    const listRoleName = this.roleSelected?.map((role) => role.name) || [];
    if (isChecked) {
      listRoleName.push(row.name);
    } else {
      listRoleName.splice(listRoleName.indexOf(row.name), 1);
    }
    this.rmProfileService.updateRolesByUser(this.data.username, listRoleName).subscribe(
      () => {
        this.isLoading = false;
        this.messageService.success(this.notificationMessage.success);
        if (this.listRoleOfUser) {
          this.listRoleOfUser.push(row);
        } else {
          this.listRoleOfUser = [row];
        }
      },
      () => {
        this.isLoading = false;
      }
    );
    this.dataRole.find((item) => row.id === item.id).isChecked = isChecked;
  }

  isChecked(item) {
    return (
      this.listRoleOfUser?.findIndex((role) => {
        return role.id === item.id;
      }) > -1
    );
  }

  ngAfterViewInit() {}

  chooseTab() {
    this.isList = !this.isList;
    if (this.dataRole.length === 0) {
      this.searchRole(true);
    }
  }

  confirmDialog() {
    if (this.form.valid) {
      const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result
        .then((res) => {
          if (res) {
            this.isLoading = true;
            const data = cleanDataForm(this.form);
            data.isActive = data.active;
            delete data.active;
            this.userService.update(data).subscribe(
              () => {
                this.location.back();
                this.messageService.success(this.notificationMessage.success);
              },
              (e) => {
                if (e?.error) {
                  this.messageService.warn(e?.error?.description);
                } else {
                  this.messageService.error(this.notificationMessage.error);
                }
                this.isLoading = false;
              }
            );
          }
        })
        .catch(() => {});
    } else {
      validateAllFormFields(this.form);
    }
  }

  back() {
    this.location.back();
  }
}
