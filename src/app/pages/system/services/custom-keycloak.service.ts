import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { maxInt32 } from 'src/app/core/utils/common-constants';
import { retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class CustomKeycloakService {
  baseUrl = `${environment.keycloak.issuer}admin/realms/${environment.keycloak.realm}`;
  frontendId: string;
  relationshipId: string;
  realmManagementId: string;
  constructor(private http: HttpClient) {
    this.getClients();
  }

  getClients() {
    this.http.get(`${environment.url_endpoint_category}/keycloak/clients/current`).subscribe((res: any) => {
      this.relationshipId = res?.id;
      localStorage.setItem('RELATIONSHIP_CLIENT', this.relationshipId);
    });
    this.http
      .get(`${this.baseUrl}/clients`, { params: { first: '0', max: maxInt32.toString() } })
      .pipe(retry(3))
      .subscribe((listClient: any[]) => {
        this.frontendId = listClient?.find((item) => item.clientId === environment.keycloak.client)?.id;
        this.realmManagementId = listClient?.find((item) => item.clientId === environment.keycloak.realmManagement)?.id;
      });
  }

  getAllSessions(): Observable<any> {
    return this.http.get(`${this.baseUrl}/clients/${this.frontendId}/user-sessions`);
  }

  revokeBySessionId(id): Observable<any> {
    return this.http.delete(`${this.baseUrl}/sessions/${id}`, {});
  }

  revokeAll(): Observable<any> {
    return this.http.post(`${this.baseUrl}/logout-all`, {});
  }

  getUserByUsername(username: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/users?username=${username}`);
  }

  getUserByRole(roleName): Observable<any> {
    return this.http.get(`${this.baseUrl}/roles/${roleName}/users`);
  }

  createPolicyForRole(data): Observable<any> {
    return this.http.post(`${this.baseUrl}/clients/${this.relationshipId}/authz/resource-server/policy/role`, data);
  }

  createPermission(data): Observable<any> {
    return this.http.post(
      `${this.baseUrl}/clients/${this.relationshipId}/authz/resource-server/permission/${data.type}`,
      data
    );
  }

  updatePermission(data): Observable<any> {
    return this.http.put(
      `${this.baseUrl}/clients/${this.relationshipId}/authz/resource-server/permission/${data.type}/${data.id}`,
      data
    );
  }

  deletePermission(id): Observable<any> {
    return this.http.delete(
      `${this.baseUrl}/clients/${this.relationshipId}/authz/resource-server/permission/scope/${id}`
    );
  }

  getAllPolicy(params): Observable<any> {
    return this.http.get(`${this.baseUrl}/clients/${this.relationshipId}/authz/resource-server/policy`, { params });
  }

  // getPoliciesByPermissionId(permissionId): Observable<any> {
  //   return this.http.get(
  //     `${this.baseUrl}/clients/${this.relationshipId}/authz/resource-server/policy/${permissionId}/associatedPolicies`
  //   );
  // }

  getFunctionByUser(): Observable<any> {
    const data2 = new URLSearchParams();
    data2.set('audience', environment.keycloak.relationShip);
    data2.set('response_mode', 'permissions');
    data2.set('grant_type', 'urn:ietf:params:oauth:grant-type:uma-ticket');
    return this.http.post(`${environment.keycloak.issuer}realms/${environment.keycloak.realm}/protocol/openid-connect/token`, data2.toString(), {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    });
  }

  getRolesRealmManagement(): Observable<any> {
    return this.http.get(`${this.baseUrl}/clients/${this.realmManagementId}/roles?first=0&max=100`);
  }

  getRolesRealmManagementByUserId(userId: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/users/${userId}/role-mappings/clients/${this.realmManagementId}`);
  }

  createRolesRealmManagementByUserId(userId: string, data: any[]): Observable<any> {
    return this.http.post(`${this.baseUrl}/users/${userId}/role-mappings/clients/${this.realmManagementId}`, data);
  }

  deleteRolesRealmManagementByUserId(userId: string, data: any[]): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: data,
    };
    return this.http.delete(`${this.baseUrl}/users/${userId}/role-mappings/clients/${this.realmManagementId}`, options);
  }
}
