import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ProcessManagementService {
  constructor(private http: HttpClient) { }

  fetch(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_integration}/processes/find-all`, data);
  }

  getProcessById(id): Observable<any> {
    return this.http.post(`${environment.url_endpoint_integration}/processes/find-by-id?processId=${id}`, {});
  }

  start(id): Observable<any> {
    return this.http.post(`${environment.url_endpoint_integration}/processes/start-process?processId=${id}`, {});
  }

  stop(id): Observable<any> {
    return this.http.post(`${environment.url_endpoint_integration}/processes/stop-process?processId=${id}`, {});
  }

  findAllHistories(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_integration}/processes/find-all-histories`, data);
  }

  createProcess(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_integration}/processes/save`, data);
  }

  updateProcess(data): Observable<any> {
    return this.http.put(`${environment.url_endpoint_integration}/processes`, data);
  }

  deleteProcess(id): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_integration}/processes/${id}`);
  }

  findAllProcessCategories(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_integration}/process-categories/find-all`, data)
  }

  findAllProcessCategoryParams(id): Observable<any> {
    return this.http.post(`${environment.url_endpoint_integration}/process-categories/find-all-params?processCategoryId=${id}`, {})
  }

}
