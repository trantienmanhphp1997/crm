import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  searchUser(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees`, { params });
  }

  search(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_admin}/users`, { params });
  }

  changeStatus(username: string, id: string, isActive: boolean): Observable<any> {
    return this.http.put(
      `${environment.url_endpoint_rm}/employees/changeStatus/${username}?status=${isActive}&id=${id}`,
      {}
    );
  }

  changeLocked(username, id: string, locked: boolean): Observable<any> {
    return this.http.put(
      `${environment.url_endpoint_rm}/employees/changeLocked/${username}?status=${locked}&id=${id}`,
      {}
    );
  }

  getUserByCode(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_admin}/users/userByCode`, { params });
  }

  getRmByPermission(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/employees/getRM`, params);
  }

  getRmByCBQL(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/findAllRmByCBQL`, {params});
  }

  getUserByUserName(username): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/username/${username}`);
  }

  create(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/employees/createUser`, data);
  }

  update(data): Observable<any> {
    return this.http.put(`${environment.url_endpoint_rm}/employees/${data?.username}`, data);
  }

  delete(username: string): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_rm}/employees/${username}`);
  }

  createFileExcel(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/exports`, { params, responseType: 'text' });
  }
}
