import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { LogModel } from 'src/app/core/interfaces/log-model.interface';
import { FunctionCode, RequestMethod, Scopes } from 'src/app/core/utils/common-constants';

@Injectable({
  providedIn: 'root',
})
export class LogService {
  private baseUrl = `${environment.url_endpoint_admin}/user-activities`;
  constructor(private http: HttpClient) {}

  createLog(data: LogModel): Observable<any> {
    return this.http.post(this.baseUrl, data);
  }
}
