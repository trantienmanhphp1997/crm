import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { maxInt32 } from 'src/app/core/utils/common-constants';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  constructor(private http: HttpClient) {}

  searchAppCategory(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/decentralization`, { params });
  }

  getAppCategoryById(id): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/decentralization/${id}`);
  }

  createAppCategory(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/decentralization`, data);
  }

  updateAppCategory(data): Observable<any> {
    return this.http.put(`${environment.url_endpoint_category}/decentralization`, data);
  }

  deleteAppCategory(id): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_category}/decentralization/${id}`);
  }

  searchTypeCommonCategory(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/common-categories`, { params });
  }

  searchCommonCategoryByCode(code): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/commons/${code}`);
  }

  searchCommonCategoryByType(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/commons`, { params });
  }

  createCommonCategory(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/commons`, data);
  }

  updateCommonCategory(data): Observable<any> {
    return this.http.put(`${environment.url_endpoint_category}/commons`, data);
  }

  deleteCommonCategory(id): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_category}/commons/${id}`);
  }

  searchBranches(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/branches`, { params });
  }

  getBranchByCode(code): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/branches/${code}`);
  }

  synchronizedBranches(): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/branches/synchronize`, {});
  }

  updateBranch(data): Observable<any> {
    return this.http.put(`${environment.url_endpoint_category}/branches`, data);
  }

  searchBlocksCategory(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/blocks`, { params });
  }

  getBlocksCategoryById(id): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/blocks/${id}`);
  }

  getBlocksCategoryByRm(params?): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/blocks/findBlockByRM`, { params });
  }

  createBlocksCategory(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/blocks`, data);
  }

  updateBlocksCategory(data): Observable<any> {
    return this.http.put(`${environment.url_endpoint_category}/blocks`, data);
  }

  deleteBlocksCategory(id): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_category}/blocks/${id}`);
  }

  searchResourceCategory(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/function`, { params });
  }

  getResourceCategoryById(id): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/function/${id}`);
  }

  getResourceParentCategoryById(id): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/function/getFunctionOtherBranch?functionId=${id}`);
  }

  createResourceCategory(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/function`, data);
  }

  updateResourceCategory(data): Observable<any> {
    return this.http.put(`${environment.url_endpoint_category}/function`, data);
  }

  deleteResourceCategory(id): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_category}/function/${id}`);
  }

  searchLogHistory(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_admin}/user-activities`, { params });
  }

  getPermission(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/permissions`);
  }

  getPermissionById(id: string, type: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/permissions/${id}?type=${type}`);
  }

  getPermissionByUser(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/permissions/userPermissionMapping`, { params });
  }

  getPermissionByRole(roleCode: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/permissions/permissions-by-role?roleCode=${roleCode}`);
  }

  exportExcelPermissionsByRole(roleCode: string): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_category}/permissions/export-permissions-by-role?roleCode=${roleCode}`,
      {
        responseType: 'text',
      }
    );
  }

  addPermissionForRole(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/permissions/assign-permissions-role`, data);
  }

  getUserRoleMapping(params?): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/userRoleMapping`, { params });
  }

  searchRole(params: any): Observable<any> {
    //return this.http.get(`${environment.url_endpoint_category}/roles`, { params });
    return this.http.get(`${environment.url_endpoint_category}/roles/policy`, { params });
  }

  getRoleById(id: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/roles/${id}`);
  }

  createRole(data: any): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/roles/add-role-by-client`, data);
  }

  updateRole(data: any): Observable<any> {
    return this.http.put(`${environment.url_endpoint_category}/roles`, data);
  }

  deleteRoleById(id: string): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_category}/roles/${id}`);
  }

  getUserByRoleId(id: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/roles/${id}/users`);
  }

  searchScope(params: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/scopes`, { params });
  }

  getScopeById(id: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/scopes/${id}`);
  }

  createScope(data: any): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/scopes`, data);
  }

  updateScope(data: any): Observable<any> {
    return this.http.put(`${environment.url_endpoint_category}/scopes`, data);
  }

  deleteScopeById(id: string): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_category}/scopes/${id}`);
  }

  getBranchByUserAndPermission(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/users-permissions/getBranchFromUserPermission`, {
      params,
    });
  }

  getRegion(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/regions/locations`);
  }

  getBranchesOfUser(rsId, scope): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/branches/branchesByUser?rsId=${rsId}&scope=${scope}`);
  }

  getIndustries(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/industries`, {
      params,
    });
  }

  addDomainPermissionByUser(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/users-permissions`, data);
  }

  importDomainPermission(data: FormData, isImport: boolean): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_category}/users-permissions/import?checkImport=${isImport}`,
      data
    );
  }

  getCountry(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/countries`);
  }

  getProvinceByCountryId(countryId): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/town-districts?countryId=${countryId}`);
  }

  getDistrictByProvinceCode(provinceCode): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/town-districts/${provinceCode}`);
  }

  getCommonCategory(parentCode, code?): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/commons`, {
      params: {
        page: '0',
        size: maxInt32.toString(),
        commonCategoryCode: parentCode,
        code: code || '',
      },
    });
  }

  saveTracing(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/device/save-user-tracing`, data);
  }

  checkManageType(rsId, scope): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/branches/checkManageType?rsId=${rsId}&scope=${scope}`);
  }

  getRoleByName(name: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/roles/policy/${name}`);
  }
  getUserByRoleName(name: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/roles/policy/${name}/users`);
  }
  updatePolicy(data: any): Observable<any> {
    return this.http.put(`${environment.url_endpoint_category}/roles/policy`, data);
  }
  deletePolicyById(id: string): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_category}/roles/policy/${id}`);
  }
  getTreeBranchesOfUser(rsId, scope): Observable<any> {
    // return this.http.get(`http://localhost:10168/crm02-category/branches/treeBranchesByUser?rsId=${rsId}&scope=${scope}`);
    return this.http.get(`${environment.url_endpoint_category}/branches/treeBranchesByUser?rsId=${rsId}&scope=${scope}`);
  }
  getAllBranches(page, size): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/branches?page=${page}&size=${size}`);
  }
  branchesByUserPermission(rsId, scope): Observable<any> {
    // return this.http.get(`http://localhost:10168/crm02-category/branches/treeBranchesByUser?rsId=${rsId}&scope=${scope}`);
    return this.http.get(`${environment.url_endpoint_category}/branches/branchesByUserPermission?rsId=${rsId}&scope=${scope}`);
  }

  createNews(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/news/create`, data);
  }

  getDetailNews(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/news/detail`, { params });
  }

  updateNews(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/news/update`, data);
  }

  deleteNewsById(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/news/del`, { params });
  }

  getAllNews(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/news/findAll`,{ params });
  }

  getNewsConfigByDivision(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/news/config`,{ params });
  }

  updateNewsConfigByDivision(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/news/config/update`, data);
  }

  viewFile(fileId): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/files/view/${fileId}`);
  }

  getBranches(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/branches?page=0&size=2147483647`);
  }

  ratingFeature(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/rating-feature/crm/add`, body);
  }

  reportListRatingFunction(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/rating-feature/rate-function-usage`, body);
  }

  exportRatingFunction(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/rating-feature/export-rate-function-usage`, body, { responseType: 'text' });
  }

  reportRating(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/rating-feature/crm/export`, body, { responseType: 'text' });
  }
}
