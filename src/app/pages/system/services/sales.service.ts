import { Injectable } from '@angular/core';
import { HttpWrapper } from '../../../core/apis/http-wapper';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import { NullVisitor } from '@angular/compiler/src/render3/r3_ast';

@Injectable({
  providedIn: 'root'
})
export class SalesService extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_sale}`);
  }

  getAllIndustry(): Observable<any> {
    return this.get('onboarding-account/industry', null);
  }
}
