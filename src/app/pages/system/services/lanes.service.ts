import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class LaneService {
  constructor(private http: HttpClient) {}

  getAll(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_workflow}/lanes`);
  }

  update(data): Observable<any> {
    return this.http.put(`${environment.url_endpoint_category}/lanes/${data.id}`, data);
  }
}
