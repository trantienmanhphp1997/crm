import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TitleRelationshipService {
  constructor(private http: HttpClient) {}

  searchTitleRelationship(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/title-groups/findAllByCodeAndNameAndBlockCode`, {
      params,
    });
  }

  searchTitleRelationshipModal(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/title-groups`, {
      params,
    });
  }

  createTitle(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/kpiRmGroupRelationship`, params);
  }

  detailTitle(id, bizLine): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/kpiRmGroupRelationship?id=${id}&bizLine=${bizLine}`);
  }

  //
  deleteTitle(idTitle, bizline): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_rm}/kpiRmGroupRelationship/${idTitle}?bizLine=${bizline}`);
  }

  updateTitle(idTitle, params): Observable<any> {
    return this.http.put(`${environment.url_endpoint_rm}/kpiRmGroupRelationship/${idTitle}`, params);
  }
}
