import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})

export class ExportDataApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_sale}`);
  }

  selectData(params): Observable<any> {
    const data = {
      schema: params.schema,
      command: params.command,
      page: params.page,
      size: params.size,
      type: 0
    }
    return this.http.post(`${environment.url_endpoint_report}/tool/execute`, data);
  }

  exportData(params): Observable<any> {
    const data = {
      schema: params.schema,
      command: params.command,
      type: 1
    }
    return this.http.post(`${environment.url_endpoint_report}/tool/execute`, data);
  }

  updateData(params): Observable<any> {
    const data = {
      schema: params.schema,
      command: params.command,
      type: 2
    }
    return this.http.post(`${environment.url_endpoint_report}/tool/execute`, data);
  }
}
