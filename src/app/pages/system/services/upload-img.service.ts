import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpWrapper } from 'src/app/core/apis/http-wapper';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UploadImgService extends HttpWrapper {

  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_admin}/image`);
  }

  findImagesWithDivision(params: any): Observable<any> {
    return this.get('findAll', params)
  }

  uploadImage(params: any): Observable<any> {
    const formData = new FormData();

    formData.append('file', params.image);

    if (params.updateImage) {
      formData.append('reserveId', params.uuidImage);
    }

    return this.post('upload/image', formData);
  }

  saveInfo(body: any): Observable<any> {
    return this.post('upload', body);
  }

  updateInfo(body: any): Observable<any> {
    return this.post('update', body);
  }

  deleteInfo(body: any): Observable<any> {
    return this.post('delete', body);
  }

  getImg(uuidImage: string): Observable<any> {
    return this.http.get(`${this.baseURL}/public/image/${uuidImage}`, {responseType: 'blob'});
  }

  getImgWithDivision(params: any): Observable<any> {
    return this.http.get(`${this.baseURL}/public/image`, { params, responseType: 'blob'});
  }

  getImgFromDivisions(params): Observable<any> {
   // return this.get(`findByDivision`, params);
    return this.http.get(`${environment.url_endpoint_admin}/image/findByDivision`,{params});
  }

  getImgBlob(imgBase64): Observable<any> {
    return this.http.get(imgBase64, {responseType: 'blob'});
  }
  getImagBase64(id): Observable<any>{
    return this.http.get(`${environment.url_endpoint_rm}/files/view/${id}`);
  }
  skipViewPoster(param): Observable<any>{
    return this.http.post(`${environment.url_endpoint_admin}/image/hiddenPoster`,param);
  }
  findPoster(param): Observable<any>{
    return this.http.post(`${environment.url_endpoint_admin}/image/findPoster`,param);
  }
}
export class PopupImageDTO {
  id: string;
  uuidImage: string;
  enableSkipPoster?: boolean;
  numberOfDisplay?:number;
  image?:string;
  countNumberShow?:number
}
