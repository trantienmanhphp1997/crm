import { Component } from '@angular/core';
import { NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-system-view',
  template: ` <router-outlet></router-outlet> `,
  providers: [NgbModalConfig],
})
export class SystemComponent {
  constructor(config: NgbModalConfig) {
    // customize default values of modals used by this component tree
    config.backdrop = 'static';
    config.keyboard = false;
  }
}
