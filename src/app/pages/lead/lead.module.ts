import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material/tabs';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { LeadRoutingModule } from './lead-routing.module';
import {
  LeadActionsComponent,
  LeadDetailsComponent,
  LeadInformationExtendedComponent,
  LeadInformationExtendedNewComponent,
  LeadManagementComponent,
  LeadAssignComponent,
  LeadImportComponent,
  LeadModalComponent,
} from './components';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { SharedModule } from 'src/app/shared/shared.module';
import { MultiSelectModule } from 'primeng/multiselect';
import { CalendarModule } from 'primeng/calendar';
import { InputNumberModule } from 'primeng/inputnumber';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputMaskModule } from 'primeng/inputmask';
import { Customer360Module } from '../customer-360/customer-360.module';
import { CheckboxModule } from 'primeng/checkbox';
import { InputTextareaModule } from 'primeng/inputtextarea';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {MatMenuModule} from '@angular/material/menu';
import {StepsModule} from 'primeng/steps';
import { TableModule } from 'primeng/table';
import { LeadHistoryComponent } from './components/lead-history/lead-history.component';
import { LeadReportFinanceComponent } from './components/lead-report-finance/lead-report-finance.component';
import { LocationComponent } from './components/location/location.component';

const COMPONENTS = [
  LeadManagementComponent,
  LeadActionsComponent,
  LeadDetailsComponent,
  LeadInformationExtendedComponent,
  LeadAssignComponent,
  LeadImportComponent,
  LeadModalComponent,
  LeadInformationExtendedNewComponent,
  LeadReportFinanceComponent,
  LocationComponent,
  LeadHistoryComponent,
];

@NgModule({
  declarations: [...COMPONENTS],
    imports: [
        CommonModule,
        SharedModule,
        NgbModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        LeadRoutingModule,
        NgxDatatableModule,
        MatTabsModule,
        InputTextModule,
        DropdownModule,
        MultiSelectModule,
        CalendarModule,
        RadioButtonModule,
        InputNumberModule,
        InputMaskModule,
        CheckboxModule,
        Customer360Module,
        InputTextareaModule,
        InfiniteScrollModule,
        MatMenuModule,
        StepsModule,
        TableModule

    ],
  entryComponents: [...COMPONENTS],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class LeadModule {}
