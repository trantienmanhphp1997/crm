import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewEncapsulation,
  ChangeDetectorRef
} from '@angular/core';
import { Validators } from '@angular/forms';
import _ from 'lodash';
import { Utils } from 'src/app/core/utils/utils';
import * as moment from 'moment';
import { BaseComponent } from 'src/app/core/components/base.component';
import { FormGroup, FormArray } from '@angular/forms';
import { CustomerLeadService } from '../../service/customer-lead.service';
import { cleanDataForm } from 'src/app/core/utils/function';
import { catchError, retry } from 'rxjs/operators';
import { of } from 'rxjs';
import { ScreenType, CommonCategory, } from 'src/app/core/utils/common-constants';
import { CustomValidators } from 'src/app/core/utils/custom-validations';

@Component({
  selector: 'lead-information-extended-new',
  templateUrl: './lead-information-extended-new.component.html',
  styleUrls: ['./lead-information-extended-new.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LeadInformationExtendedNewComponent extends BaseComponent implements OnInit, OnChanges {
  listGender =  [
    {
      "code": null,
      "name": "---"
    },
    {
      "code": "Nam",
      "name": "Nam"
    },{
      "code": "Nữ",
      "name": "Nữ"
    },{
      "code": "Khác",
      "name": "Khác"
    }

  ]

  constructor(injector: Injector, private service: CustomerLeadService,private cdr: ChangeDetectorRef) {
    super(injector);

    
  }
  @Output() onClickMail: EventEmitter<any> = new EventEmitter<any>();
  today = new Date();
  actionType: string;
  haveNewRecord:number = 0;
  haveEditRecord:number = 0;
  haveDeleteRecord:number = 0;
  isDelete = false;
  crrDate = new Date();
  @Input() data: any;
  disabled: boolean = false;
  form = this.fb.group({
    leadCorpContacts: [],
    leadCode: null,
    debtCIC: null,
    numberYearOperation: null,
    amountLoanAndBonds: null,
    authorizedCapital: null,
    suggestProduct: null,
    numberTCTDLoan: null,
    disableEmail: false,
    disablePhone: false,
    disableSMS: false,
    disableMeeting: false,
  });
  listContactRemove = [];
  listPosition = [
    {
      "code":null,
      "name":"---"
    }
  ];
  director: any;
  chief: any;
  listContact: any[];
  listData: any[];
  arrContact: FormArray;
  contactInfo = this.fb.group({
    arrayContact: this.fb.array([]),
  });
  virtualList = [{}]
  ngOnInit() {

    this.commonService.getCommonCategory(CommonCategory.CONTACT_POSITION).subscribe((value) => {
      let data = value?.content;
      data.forEach(element => {
        if(element.isDefault === true)
          this.listPosition.push(element);
      });
    })
    this.form.controls.disableEmail.disable();
    this.form.controls.disablePhone.disable();
    this.form.controls.disableSMS.disable();
    this.form.controls.disableMeeting.disable();
    
  }

  formatCurrency(value) {
    return Utils.numberWithCommas(value);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.mapData();
  }

  mapData() {
    // value changes 
    this.arrayContact().valueChanges.subscribe(() => {
      this.listData = [...this.arrayContact().value];
    })
    if (this.data) {
      this.isDelete = false;
      this.form.patchValue(this.data);
      _.forEach(this.data?.leadCorpContacts, (info) => {
        if(info?.birthday !== undefined)
          info["birthday"] = new Date(info?.birthday);
      });
      // this.listData = this.data?.leadCorpContacts;
      const leadCorpContacts = [...this.data?.leadCorpContacts];
      this.arrayContact().clear();
      if (leadCorpContacts.length > 0) {
        _.forEach(leadCorpContacts, (contact) => {
          const form = this.fb.group({
            id: null,
            leadCode: null,
            taxCode: null,
            positionName: null,
            position: null,
            name: null,
            birthday: null,
            email: ['', CustomValidators.email],
            phone: ['', CustomValidators.onlyNumber],
            idCard: ['', CustomValidators.taxCode],
            gender: null,
            isUpdate:false,
            isCreate:false,
            oldValue:null
          });
          // contact.birthday = contact?.birthday ? new Date(contact.birthday) : null;
          // contact.birthday = contact?.birthday ? new Date(contact.birthday) : null;
          form.patchValue(contact);
          this.arrayContact().push(form);
        });

      } else {
        //this.arrayContact().push(this.newArrayContactForm());
      }
    }

    
  }

  updateExtend2(index:number) {
    this.getForm(index).controls.oldValue.setValue(this.getForm(index).value);
    this.getForm(index).controls.isUpdate.setValue(true);
    this.haveEditRecord++;
  }
  save() {
    let validate = true;
    this.arrContact = this.contactInfo.get('arrayContact') as FormArray;
    _.forEach(this.arrContact.controls, (contact) => {
      if(!this.checkContactIsEmpty(contact?.value)){
        validate = false;
        return; 
      }
      contact.markAllAsTouched();
    });
    if(!validate){
      this.messageService.error('Thông  tin liên hệ không được để trống');
      return;
    }
    if(this.contactInfo.valid){
      this.confirmService.confirm().then((isConfirm) => {
        if (isConfirm) {
          this.isLoading = true;
          const data = cleanDataForm(this.form);
          data.leadCorpContacts = [];
          
          const listContact = cleanDataForm(this.contactInfo)?.arrayContact;
          _.forEach(listContact, (contact) => {
            data.leadCorpContacts.push(contact);
          });
          data.leadCorpContacts = [...data.leadCorpContacts, ...this.listContactRemove];
          _.forEach(data.leadCorpContacts, (contact) => {
            contact.leadCode = this.data.leadCode;
            if(!_.isEmpty(contact?.name))
              contact.name = _.trim(contact.name)
            if(!_.isEmpty(contact?.email))
              contact.email = _.trim(contact.email)
            if(!_.isEmpty(contact?.phone))
              contact.phone = _.trim(contact.phone)
            if(!_.isEmpty(contact?.idCard))
              contact.idCard = _.trim(contact.idCard)
            if (_.isEmpty(contact?.action)) {
              contact.action = _.isEmpty(_.trim(contact.id)) ? 'CREATE' : 'UPDATE';
            }
          });
          this.service.updateInfoExtend(data).subscribe(
            () => {
              this.service
                .getLeadByCode(data.leadCode) 
                .pipe(
                  retry(3),
                  catchError(() => of(undefined))
                )
                .subscribe((data) => {
                  if (data) {
                    this.data = data;
                    this.mapData();
                  }
                  this.messageService.success(this.notificationMessage.success);
                  this.isLoading = false;
                  this.haveNewRecord = 0;
                  this.haveEditRecord = 0;
                  this.haveDeleteRecord = 0;
                });
                this.ref.detectChanges();
            },
            () => {
              this.messageService.error(this.notificationMessage.error);
              this.isLoading = false;
            }
          );
        }else{
          this.haveDeleteRecord = 0;
          this.listContactRemove = [];
        }
      });
    }
  }
  checkContactIsEmpty(item){
    if(item?.name || item?.position || item?.idCard || item?.phone || item?.email || item?.gender || item?.birthday){
      return true
    }else{
      return false;
    }
  } 
  getPosition(item: any, key: string){
    let codePos = Utils.isStringEmpty(_.get(item, key)) ? '' : _.get(item, key);
    let position  =_.find(this.listPosition, (x) => x.code === codePos)
    return position?.name;
  }
  getValue(item: any, key: string) {
    return Utils.isStringEmpty(_.get(item, key)) ? '' : _.get(item, key);
  }
  
  cancel(rowIndex) {
    if(this.getForm(rowIndex).controls?.isCreate?.value){
      this.arrayContact().removeAt(rowIndex);
      if(this.haveNewRecord > 0 ) {
        this.haveNewRecord--;  
      }
      this.ref.detectChanges();
      return;
    }
    this.getForm(rowIndex).controls.isUpdate.setValue(false);
    let oldValue = this.getForm(rowIndex).controls.oldValue.value;
    this.getForm(rowIndex).controls.name.setValue(oldValue.name);
    this.getForm(rowIndex).controls.position.setValue(oldValue.position);
    this.getForm(rowIndex).controls.idCard.setValue(oldValue.idCard);
    this.getForm(rowIndex).controls.phone.setValue(oldValue.phone);
    this.getForm(rowIndex).controls.email.setValue(oldValue.email);
    this.getForm(rowIndex).controls.gender.setValue(oldValue.gender);
    this.getForm(rowIndex).controls.birthday.setValue(oldValue.birthday);
    if(this.haveEditRecord > 0 ) {
      this.haveEditRecord--;  
    }
    this.ref.detectChanges();
  }
  // removeRecord(item){
  //   this.isDelete = true;
  //   this.listData = this.arrayRemove(this.listData,item);
  //   this.listData = [...this.listData];
  // }
  // arrayRemove(arr, value) { 
  //   return arr.filter(function(ele){ 
  //       return ele != value; 
  //   });
  // }
  createPersonContact() {
    this.arrayContact().push(this.newArrayContactForm());
    this.haveNewRecord++;
    this.ref.detectChanges();
  }

  newArrayContactForm(): FormGroup {
    return this.fb.group({
      id: null,
      leadCode: null,
      position: null,
      name: null,
      birthday: null,
      email: ['', CustomValidators.email],
      phone: ['', CustomValidators.onlyNumber],
      idCard: ['', CustomValidators.taxCode],
      gender: null,
      isUpdate:false,
      isCreate:true,
      oldValue:null
    });
  }
  deleteInfo(i:number){
    this.haveDeleteRecord++;
    const item = this.arrayContact().at(i).value;
    if (!_.isEmpty(_.trim(item?.id))) {
      item.action = 'DELETE';
      this.listContactRemove.push(item);
    }
    this.save();
  }
  getForm(i: number): FormGroup {
    return this.arrayContact().controls[i] as FormGroup;
  }
  arrayContact(): FormArray {
    return this.contactInfo.get('arrayContact') as FormArray;
  }

  getValueDateString(data, key) {
    return _.isDate(_.get(data, key)) ? moment(_.get(data, key)).format('DD/MM/YYYY') : '';
  }

  sendMailToPerson(item: any, key: string) {
    if (!Utils.isStringEmpty(_.get(item, key))) {
      this.onClickMail.emit(_.get(item, key));
    }
  }
}
