import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadInformationExtendedNewComponent } from './lead-information-extended-new.component';

describe('LeadInformationExtendedNewComponent', () => {
  let component: LeadInformationExtendedNewComponent;
  let fixture: ComponentFixture<LeadInformationExtendedNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadInformationExtendedNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadInformationExtendedNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
