import { AfterViewInit, Component, HostBinding, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { global } from '@angular/compiler/src/util';
import { DatePipe } from '@angular/common';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { maxInt32 } from 'src/app/core/utils/common-constants';
import { CustomerLeadService } from '../../service/customer-lead.service';
import * as moment from 'moment';
import { orderBy } from 'lodash';

@Component({
  selector: 'app-lead-history',
  templateUrl: './lead-history.component.html',
  styleUrls: ['./lead-history.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe]
})
export class LeadHistoryComponent extends BaseComponent implements OnInit, AfterViewInit {
  @HostBinding('class') elementClass = 'app-lead-history';
  @ViewChild('table') table: DatatableComponent;
  listData = [];
  temp = [];
  isLoading = false;
  textFilter = '';
  customerName = '';
  messages = global.messageTable;
  hrsCode: string;
  customerTypeSale: number;
  customerType: string;
  manageType: number;
  contractNumber: any;
  systemAssign: any;
  isShowColumnContractNumber: boolean;
  leadCode: string;
  fullName: string;

  constructor(
    injector: Injector,
    private leadService: CustomerLeadService,
    private datePipe: DatePipe,
    private modalActive: NgbActiveModal
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.isLoading = true;
    const params = {
      leadCode: this.leadCode,
      page: 0,
      size: maxInt32
    };

    /**
     * @author BinhDT
     * @since 15-09-2022
     * @Note Lấy danh sách lịch sử phân giao khách hàng
     */
    this.leadService.searchHistory(params).subscribe(
      (listHistories) => {

        this.listData = listHistories?.content || [];
        this.temp = listHistories?.content || [];
        this.listData = orderBy(listHistories?.content || [], [
            item => moment(new Date(item.createdDate), 'DD/MM/YYYY').startOf('day').valueOf(),
            item => moment(new Date(item.updatedDate), 'DD/MM/YYYY').startOf('day').valueOf()],
          ['desc', 'desc']
        );
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngAfterViewInit() {
    this.table.groupExpansionDefault = true;
  }

  filter() {
    this.textFilter = this.textFilter.trim();

    // filter our data
    const temp = this.temp.filter((d) => {
      return d.blockCode.toLowerCase().indexOf(this.textFilter) !== -1 || !this.textFilter;
    });
    this.listData = temp;
  }


  closeModal() {
    this.modalActive.close();
  }

  toggleExpandGroup(group) {
    this.table.groupExpansionDefault = false;
    this.table.groupHeader.toggleExpandGroup(group);
  }
}
