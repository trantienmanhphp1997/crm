import { Component, Inject, Injector, LOCALE_ID, OnInit, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CustomerLeadService } from '../../service/customer-lead.service';
import * as _ from 'lodash';
import {
  CommonCategory, FunctionCode, functionUri, maxInt32, Scopes, ScreenType
} from 'src/app/core/utils/common-constants';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import * as moment from 'moment';
import { catchError } from 'rxjs/operators';
import { forkJoin, of } from 'rxjs';
import { Utils } from 'src/app/core/utils/utils';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { RmApi } from 'src/app/pages/rm/apis';
import { AppFunction } from 'src/app/core/interfaces/app-function.interface';
import { Validators } from '@angular/forms';
import { LeadService } from 'src/app/core/services/lead.service';
import { LocationComponent } from '../location/location.component';
import {
  formatNumber
} from '@angular/common';
import { SessionKey } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'app-lead-actions',
  templateUrl: './lead-actions.component.html',
  styleUrls: ['./lead-actions.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LeadActionsComponent extends BaseComponent implements OnInit {
  form = this.fb.group({
    fullName: ['', CustomValidators.required],
    taxCode: [null, [CustomValidators.required, CustomValidators.taxCode]],
    dateOfBirth: null,
    businessRegistrationNumber: [null, [CustomValidators.required, CustomValidators.taxCode]],
    nationCode: 'VN',
    address: '',
    phone: [null, CustomValidators.onlyNumber],
    sourceLeadCode: '',
    sourceLead: '',
    financialRevenue: null,
    segment: { value: '', disabled: true },
    industry: '',
    industryCode: '',
    numberOfEmployees: [],
    typeOfBusiness: '',
    salesImportAndExport: '',
    leadCode: { value: '', disabled: true },
    customerType: '1',
    branchCode: this.currUser?.branch,
    createdDate: { value: moment().format('DD/MM/YYYY'), disabled: true },
    rmCode: this.currUser?.code,
    customerTypeName: { value: '', disabled: true },
    branchUpload: { value: '', disabled: true },
    email: ['', [Validators.maxLength(50), CustomValidators.email]],
    website: [],
    wardCode: [],
    wardNm: [],
    districtCode: [],
    districtNm: [],
    townCityCode: [],
    townCityNm: [],
    location: [{ value: '', disabled: false }],
    vnCapAmt: [],
    frgCapAmt: [],
    totalInvestCapAmt: []
  });
  numberVnCapAmt: [];
  numberFrgCapAmt: [];
  numberTotalInvestCapAmt: [];
  numberOfEmployees: [];
  salesImportAndExport: '';
  financialRevenue: '';

  // form KHCN
  formKHCN = this.fb.group({
    customerType: '1',
    customerName: ['', [CustomValidators.required, Validators.maxLength(200)]],
    idCard: [null, [CustomValidators.required, Validators.maxLength(50)]],
    birthday: null,
    gender: ['1'],
    national: 'VN',
    email: ['', [Validators.maxLength(50), CustomValidators.email]],
    phone: [null, [CustomValidators.onlyNumber, Validators.maxLength(15)]],
    address: ['', [Validators.maxLength(255)]],
    potentialLevel: '',
    job: ['', [Validators.maxLength(255)]],
    incomePerMonth: [null, CustomValidators.onlyNumber],
    campaign: '',
    customerResources: '',
    leadCode: { value: '', disabled: true },
    customerTypeName: { value: '', disabled: true },
    branchCode: this.currUser?.branch,
    rmCode: this.currUser?.code,
    branchUpload: { value: '', disabled: true },
    createdDate: { value: moment().format('DD/MM/YYYY'), disabled: true }
  });

  commonData = {
    listCountry: [{ code: 'VN', name: 'Việt Nam' }, { code: 'NN', name: 'Nước Ngoài' }],
    listSourceCustomer: [],
    listBusiness: [],
    listCustomerType: [],
    isIndiv: false,
    isKHDN: false,
    listRmManager: [],
    listRmManagerTerm: [],
    listBranchManager: [],

    //KHCN
    listPotentialLevel: [],
    listIncomePerMonth: [],
    listCampaign: [],
    listCustomerResources: []
  };
  toDate = new Date();
  industryCode: string;
  actionType: string;
  title = 'Thêm mới khách hàng tiềm năng';
  objFunctionRM: AppFunction;
  objFunction360: AppFunction;

  customerTypeFromList: string;
  code: string;

  today = new Date();
  idDetail: number;
  haveInDiv: boolean;
  haveSME: boolean;

  constructor(injector: Injector, private service: CustomerLeadService, private categoryService: CategoryService, private rmApi: RmApi, private leadService: LeadService, @Inject(LOCALE_ID) public locale: string) {
    super(injector);
    this.isLoading = true;
    this.actionType = this.route?.snapshot?.data?.screenType;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.LEAD}`);
    this.objFunctionRM = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.objFunction360 = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);

    this.code = this.route.snapshot.paramMap.get('code');
    this.customerTypeFromList = _.get(this.route.snapshot.queryParams, 'customerType');

    this.haveInDiv = this.route.snapshot.queryParams.haveInDiv === 'true';
    this.haveSME = this.route.snapshot.queryParams.haveSME === 'true';
  }

  ngOnInit() {
    // set default customerType from list
    this.form.get('customerType').setValue(this.customerTypeFromList);
    this.initData();
  }

  save() {
    if (this.form.valid) {
      this.confirmService.confirm().then((isConfirm) => {
        if (isConfirm) {
          const data = { ...cleanDataForm(this.form) };
          data.rsId = this.objFunctionRM?.rsId;
          data.sourceLead = data.sourceLeadCode;
          data.scope = 'VIEW';
          data.vnCapAmt = this.numberVnCapAmt;
          data.frgCapAmt = this.numberFrgCapAmt;
          data.totalInvestCapAmt = this.numberTotalInvestCapAmt;
          data.numberOfEmployees = this.numberOfEmployees;
          data.salesImportAndExport = this.salesImportAndExport;
          data.financialRevenue = this.financialRevenue;
          data.address = this.getAddress(data?.address,data?.wardNm,data?.districtNm,data?.townCityNm);
          delete data.location;
          if (!_.isEmpty(data.dateOfBirth)) {
            data.dateOfBirth = moment(data.dateOfBirth).format('YYYY-MM-DD');
          }
          if (!_.isEmpty(data.financialRevenue)) {
            data.financialRevenue = data.financialRevenue.toString().replace(/,/g, '');
          }
          this.isLoading = true;
          if (this.actionType === ScreenType.Update) {
            this.service.updateLead(data, data.leadCode).subscribe(() => {
              this.messageService.success(this.notificationMessage.success);
              this.isLoading = false;
              this.router.navigate([functionUri.lead_management, 'detail', data.leadCode], {
                state: this.state, queryParams: {
                  customerType: '1'
                }
              });
            }, (e) => {
              this.isLoading = false;
              if (_.endsWith(e?.error?.code, 'CRMSMEW538') || _.endsWith(e?.error?.code, 'CRMSMEW539')) {
                this.messageService.error(e?.error?.description?.replace('%s', data.taxCode));
              } else if (_.endsWith(e?.error?.code, 'CRMSMEW545') || _.endsWith(e?.error?.code, 'CRMSMEW546')) {
                this.messageService.error(e?.error?.description?.replace('%s', data.businessRegistrationNumber));
              } else if (!_.isEmpty(e?.error?.description)) {
                this.messageService.error(e?.error?.description);
              } else {
                this.messageService.error(this.notificationMessage.error);
              }
            });
          } else {
            this.service.createLead(data).subscribe((leadCode) => {
              this.messageService.success(this.notificationMessage.success);
              this.isLoading = false;
              this.router.navigate([functionUri.lead_management, 'detail', leadCode], {
                state: this.state, queryParams: {
                  customerType: '1'
                }
              });
            }, (e) => {
              e.error = JSON.parse(e.error);
              this.isLoading = false;
              if (_.endsWith(e?.error?.code, 'CRMSMEW538') || _.endsWith(e?.error?.code, 'CRMSMEW539')) {
                this.messageService.error(e?.error?.description?.replace('%s', data.taxCode));
              } else if (_.endsWith(e?.error?.code, 'CRMSMEW545') || _.endsWith(e?.error?.code, 'CRMSMEW546')) {
                this.messageService.error(e?.error?.description?.replace('%s', data.businessRegistrationNumber));
              } else if (!_.isEmpty(e?.error?.description)) {
                this.messageService.error(e?.error?.description);
              } else {
                this.messageService.error(this.notificationMessage.error);
              }
            });
          }
        }
      });
    } else {
      validateAllFormFields(this.form);
    }
  }

  // back() {
  //   this.router.navigateByUrl(functionUri.lead_management, { state: this.state });
  // }

  fillIndustry(value) {
    this.form.controls.industryCode.setValue(value);
    const industries = this.sessionService.getSessionData(SessionKey.LIST_INDUSTRY);
    let item = _.find(industries, (item) => item.key === value)?.value;
    this.form.controls.industry.setValue(item);
  }

  ngAfterViewInit() {
    this.form.controls.customerType.valueChanges.subscribe((value) => {
      this.isLoading = true;
      if (value === '0') {
        this.commonData.isIndiv = true;
        this.commonData.isKHDN = false;
        this.initDataKHCN();
      } else {
        this.commonData.isIndiv = false;
        this.commonData.isKHDN = true;
        this.initDataKHDN();
      }
    });
  }

  initData() {
    const cusTypeSelected = this.form.controls.customerType.value;
    if (cusTypeSelected === '0') {
      this.commonData.isIndiv = true;
      this.commonData.isKHDN = false;
      this.initDataKHCN();
    } else {
      this.commonData.isIndiv = false;
      this.commonData.isKHDN = true;
      this.initDataKHDN();
    }
  }

  initDataKHDN() {
    this.form.get('branchUpload').setValue(`${this.currUser?.branch} - ${this.currUser?.branchName}`);
    // const divisionOfUser = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM);
    // if (_.find(divisionOfUser, (item) => item.code === Division.INDIV)) {
    //   this.commonData.isIndiv = true;
    //   if (divisionOfUser?.length === 1) {
    //     this.form.controls.customerType.setValue('0');
    //     this.form.controls.customerTypeName.setValue(
    //       _.find(this.commonData.listCustomerType, (item) => item.code === '0')?.name
    //     );
    //   }
    // }
    // this.commonData.isKHDN =
    //   _.filter(
    //     divisionOfUser,
    //     (item) => item.code === Division.CIB || item.code === Division.SME || item.code === Division.FI
    //   ).length > 0;

    forkJoin([this.rmApi
      .post('findAll', {
        page: 0, size: maxInt32, crmIsActive: true, branchCodes: [], rsId: this.objFunctionRM?.rsId, scope: Scopes.VIEW
      })
      .pipe(catchError(() => of(undefined))), this.commonService.getCommonCategory(CommonCategory.LEAD_TYPE).pipe(catchError(() => of(undefined))), this.commonService.getCommonCategory(CommonCategory.LEAD_ORG_TYPE).pipe(catchError(() => of(undefined))), this.commonService.getCommonCategory(CommonCategory.LEAD_SOURCE).pipe(catchError(() => of(undefined))), this.categoryService
      .getBranchesOfUser(this.objFunctionRM?.rsId, Scopes.VIEW)
      .pipe(catchError(() => of(undefined)))]).subscribe(([listRmManagement, leadTypeConfig, leadBusinessConfig, leadSourceConfig, branchOfUser]) => {
      const listRm = [];
      listRmManagement?.content?.forEach((item) => {
        if (!_.isEmpty(item?.t24Employee?.employeeCode)) {
          listRm.push({
            code: _.trim(item?.t24Employee?.employeeCode),
            displayName: _.trim(item?.t24Employee?.employeeCode) + ' - ' + _.trim(item?.hrisEmployee?.fullName),
            branchCode: _.trim(item?.t24Employee?.branchCode)
          });
        }
      });
      if (!listRm?.find((item) => item.code === this.currUser?.code)) {
        listRm.push({
          code: this.currUser?.code,
          displayName: Utils.trimNullToEmpty(this.currUser?.code) + ' - ' + Utils.trimNullToEmpty(this.currUser?.fullName),
          branchCode: this.currUser?.branch
        });
      }
      this.commonData.listRmManagerTerm = [...listRm];
      this.commonData.listRmManager = [...listRm];
      this.commonData.listBranchManager = branchOfUser?.map((item) => {
        return {
          code: item.code, name: `${item.code} - ${item.name}`
        };
      }) || [];
      if (!_.find(this.commonData.listBranchManager, (item) => item.code === this.currUser?.branch)) {
        this.commonData.listBranchManager.push({
          code: this.currUser?.branch, name: `${this.currUser?.branch} - ${this.currUser?.branchName}`
        });
      }
      this.commonData.listBusiness = leadBusinessConfig?.content || [];
      this.commonData.listCustomerType = leadTypeConfig?.content || [];
      this.commonData.listSourceCustomer = leadSourceConfig?.content || [];
      if (this.actionType === ScreenType.Update) {
        this.title = 'Cập nhật khách hàng tiềm năng';
        this.service.getLeadByCode(this.code).subscribe((data) => {
          data.customerTypeName = _.find(this.commonData.listCustomerType, (item) => item.code === data.customerType)?.name;
          if (_.isEmpty(data.dateOfBirth)) {
            data.dateOfBirth = null;
          } else {
            const arrDate = _.split(data.dateOfBirth, '/');
            data.dateOfBirth = new Date(`${arrDate[1]}/${arrDate[0]}/${arrDate[2]}`);
          }
          this.industryCode = data.industryCode;
          // data.financialRevenue = Utils.numberWithCommas(data.financialRevenue);
          data.salesImportAndExport = _.isEmpty(data.salesImportAndExport) ? null : data.salesImportAndExport;
          data.branchCode = _.trim(_.split(data.branchCode, ' - ')[0]);
          this.commonData.listRmManager = _.filter(this.commonData.listRmManagerTerm, (item) => item.branchCode === data.branchCode);
          data.rmCode = _.split(data.rmCode, ' - ')[0];
          if(data.townCityNm && data.districtNm && data.wardNm) data.location = `${data.townCityNm} - ${data.districtNm} - ${data.wardNm}`;
          // update KHDN
          data.vnCapAmt = this.convertNumber(data.vnCapAmt, 'vnCapAmt', false);
          data.numberOfEmployees = this.convertNumber(data.numberOfEmployees, 'numberOfEmployees', false);
          data.frgCapAmt = this.convertNumber(data.frgCapAmt, 'frgCapAmt', false);
          data.totalInvestCapAmt = this.convertNumber(data.totalInvestCapAmt, 'totalInvestCapAmt', false);
          data.salesImportAndExport = this.convertNumber(data.salesImportAndExport, 'salesImportAndExport', false);
          data.financialRevenue = this.convertNumber(data.financialRevenue, 'financialRevenue', false);
          this.form.patchValue(data, { emitEvent: false });
          let address = data.address;
          if(data.wardNm)
            address = address.replace(" - " + data.wardNm,"");
          if(data.districtNm)
            address = address.replace(" - " + data.districtNm,"");
          if(data.townCityNm)
            address = address.replace(" - " + data.townCityNm,"");
          this.form.controls.address.setValue(address);
          this.isLoading = false;
        }, (e) => {
          this.isLoading = false;
          this.messageService.error(e?.error?.description);
        });
      } else {
        this.form.controls.customerTypeName.setValue(_.find(this.commonData.listCustomerType, (item) => item.code === this.form.controls.customerType.value)?.name);
        this.commonData.listRmManager = _.filter(this.commonData.listRmManagerTerm, (item) => item.branchCode === this.currUser?.branch);
        this.isLoading = false;
      }
    });

    this.form.controls.customerType.valueChanges.subscribe((value) => {
      this.form.controls.customerTypeName.setValue(this.form.controls.customerType.value);
    });

    this.form.controls.financialRevenue.valueChanges.subscribe((value) => {
      if (!_.isEmpty(value)) {
        const reg = new RegExp(/^[0-9]*$/g);
        value = value.toString().replace(/,/g, '');
        if (!reg.test(value)) {
          value = value.replace(/\D/g, '');
        }
        this.form.controls.financialRevenue.setValue(Utils.numberWithCommas(value));
      }
    });

    this.form.get('branchCode').valueChanges.subscribe((value) => {
      if (!this.isLoading) {
        this.form.get('rmCode').setValue('');
        this.commonData.listRmManager = _.filter(this.commonData.listRmManagerTerm, (item) => item.branchCode === value);
      }
    });

    this.form.get('email').valueChanges.subscribe((value) => {
      this.form.get('email').setValue(value.trim(), { emitEvent: false });
    });
  }

  initDataKHCN() {
    this.formKHCN.get('branchUpload').setValue(`${this.currUser?.branch} - ${this.currUser?.branchName}`);

    forkJoin([this.leadService.getRmByBranch({
      rsId: this.objFunction360?.rsId, scope: Scopes.VIEW
    }).pipe(catchError(() => of(undefined))), // this.rmApi
      //   .post('findAll', {
      //     page: 0,
      //     size: maxInt32,
      //     crmIsActive: true,
      //     branchCodes: [],
      //     rsId: this.objFunctionRM?.rsId,
      //     scope: Scopes.VIEW,
      //   })
      //   .pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.LEAD_TYPE).pipe(catchError(() => of(undefined))), this.commonService.getCommonCategory(CommonCategory.POTENTIAL_LEVEL).pipe(catchError(() => of(undefined))), this.commonService.getCommonCategory(CommonCategory.POTENTIAL_RESOURCE).pipe(catchError(() => of(undefined))), this.categoryService
        .getBranchesOfUser(this.objFunction360?.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined)))]).subscribe(([listRmManagement, leadTypeConfig, listPotentialLevel, listCustomerResources, branchOfUser]) => {
      const listRm = [];
      listRmManagement?.forEach((item) => {
        if (!_.isEmpty(item?.code)) {
          listRm.push({
            code: item?.code, displayName: item?.code + ' - ' + item?.fullName, branchCode: item?.branch
          });
        }
      });
      if (!listRm?.find((item) => item.code === this.currUser?.code)) {
        listRm.push({
          code: this.currUser?.code,
          displayName: Utils.trimNullToEmpty(this.currUser?.code) + ' - ' + Utils.trimNullToEmpty(this.currUser?.fullName),
          branchCode: this.currUser?.branch
        });
      }
      this.commonData.listRmManagerTerm = [...listRm];
      this.commonData.listRmManager = [...listRm];
      this.commonData.listBranchManager = branchOfUser?.map((item) => {
        return {
          code: item.code, name: `${item.code} - ${item.name}`
        };
      }) || [];
      if (!_.find(this.commonData.listBranchManager, (item) => item.code === this.currUser?.branch)) {
        this.commonData.listBranchManager.push({
          code: this.currUser?.branch, name: `${this.currUser?.branch} - ${this.currUser?.branchName}`
        });
      }

      this.commonData.listPotentialLevel = listPotentialLevel?.content || [];
      this.commonData.listCustomerResources = listCustomerResources?.content || [];
      this.commonData.listCustomerType = leadTypeConfig?.content || [];

      if (this.actionType === ScreenType.Update) {
        this.getLeadDetail();
      } else {
        this.formKHCN.controls.customerTypeName.setValue(_.find(this.commonData.listCustomerType, (item) => item.code === this.form.controls.customerType.value)?.name);
        this.commonData.listRmManager = _.filter(this.commonData.listRmManagerTerm, (item) => item.branchCode === this.currUser?.branch);
        this.isLoading = false;
      }
    });

    this.formKHCN.controls.customerType.valueChanges.subscribe((value) => {
      this.formKHCN.controls.customerTypeName.setValue(this.form.controls.customerType.value);
    });

    this.formKHCN.get('branchCode').valueChanges.subscribe((value) => {
      if (!this.isLoading) {
        this.formKHCN.get('rmCode').setValue('');
        this.commonData.listRmManager = _.filter(this.commonData.listRmManagerTerm, (item) => item.branchCode === value);
      }
    });

    this.formKHCN.get('email').valueChanges.subscribe((value) => {
      this.formKHCN.get('email').setValue(value.trim(), { emitEvent: false });
    });
  }

  getLeadDetail() {
    this.title = 'Cập nhật khách hàng tiềm năng';
    this.leadService.getLeadByCode(this.code).subscribe((data) => {
      // console.log(data);
      this.idDetail = data.id;
      data.customerTypeName = _.find(this.commonData.listCustomerType, (item) => item.code === data.customerType)?.name;
      if (_.isEmpty(data.birthday)) {
        data.birthday = null;
      } else {
        data.birthday = new Date(data.birthday);
      }
      data.createdDate = new Date(data.createdDate);
      data.createdDate = this.convertDateToString(data.createdDate);
      data.branchCode = _.trim(_.split(data.branchCode, ' - ')[0]);
      this.commonData.listRmManager = _.filter(this.commonData.listRmManagerTerm, (item) => item.branchCode === data.branchCode);
      data.rmCode = _.split(data.rmCode, ' - ')[0];


      this.formKHCN.patchValue(data, { emitEvent: false });

      this.isLoading = false;
    }, (e) => {
      this.isLoading = false;
      this.messageService.error(e?.error?.messages?.vn);
    });
  }

  convertDateToString(dateString) {
    let date = new Date(dateString);
    return (date.getFullYear() + '-' + (date.getMonth() > 8 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1)) + '-' + (date.getDate() > 9 ? date.getDate() : '0' + date.getDate()));
  }

  saveKHCN() {
    if (this.formKHCN.valid) {
      this.confirmService.confirm().then((isConfirm) => {
        if (isConfirm) {
          const data = { ...cleanDataForm(this.formKHCN) };
          data.rsId = this.objFunction?.rsId;
          data.sourceLead = data.sourceLeadCode;
          data.scope = 'VIEW';
          delete data.location;
          if (!_.isEmpty(data.birth)) {
            data.birthday = moment(data.birthday).format('YYYY-MM-DD');
            data.birthday = this.convertDateToString(data.birthday);
          }
          data.createdDate = this.convertDateToString(new Date());
          data.id = this.idDetail;
          data.customerType = '0';
          this.isLoading = true;
          if (this.actionType === ScreenType.Update) {
            this.leadService.updateLeadKHCN(data).subscribe((value) => {
              this.messageService.success(this.notificationMessage.success);
              this.isLoading = false;
              this.router.navigate([functionUri.lead_management, 'detail', value.id], {
                state: this.state, queryParams: {
                  customerType: '0'
                }
              });
            }, (e) => {
              this.isLoading = false;
              this.messageService.error(e?.error?.messages?.vn);
            });
          } else {
            this.leadService.createLeadKHCN(data).subscribe((value) => {
              this.messageService.success(this.notificationMessage.success);
              this.isLoading = false;
              this.router.navigate([functionUri.lead_management, 'detail', value.id], {
                state: this.state, queryParams: {
                  customerType: '0'
                }
              });
            }, (e) => {
              this.isLoading = false;
              this.messageService.error(e?.error?.messages?.vn);
            });
          }
        }
      });
    } else {
      validateAllFormFields(this.formKHCN);
    }
  }

  cancelKHCN() {
    this.back();
  }

  chooseLocation(): void {
    const modal = this.modalService.open(LocationComponent, { windowClass: 'location-modal' });
    modal.result
      .then((res) => {
        const title = Object.keys(res).map(key => res[key].name).join(' - ');
        Object.keys(res).forEach((key, index) => {
          switch (index) {
            case 0:
              this.form.controls.townCityNm.setValue(res[key].name);
              this.form.controls.townCityCode.setValue(res[key].key);
              break;
            case 1:
              this.form.controls.districtNm.setValue(res[key].name);
              this.form.controls.districtCode.setValue(res[key].key);
              break;
            case 2:
              this.form.controls.wardNm.setValue(res[key].name);
              this.form.controls.wardCode.setValue(res[key].key);
              break;
            default:
              break;
          }
        });
        this.form.controls.location.setValue(title);
      });
  }

  detach() {
    this.ref.detectChanges();
  }

  convertNumber(value, fieldName, setValue = true) {
    // set max length: 39
    if( typeof(value) == 'number')
      value = value.toString();
    value = value?.substring(0, 40);
    
    if (value != null) {
      const raw = value.toString().replace(/[^0-9]+/g, '');
      if (fieldName === 'vnCapAmt') this.numberVnCapAmt = raw;
      if (fieldName === 'frgCapAmt') this.numberFrgCapAmt = raw;
      if (fieldName === 'totalInvestCapAmt') this.numberTotalInvestCapAmt = raw;
      if (fieldName === 'numberOfEmployees') this.numberOfEmployees = raw;
      if (fieldName === 'salesImportAndExport') this.salesImportAndExport = raw;
      if (fieldName === 'financialRevenue') this.financialRevenue = raw;
      const rawSpl = raw.split('.');
      let str = rawSpl[0];
      str = str.replaceAll(',', '');
      let result = '';
      let from = 0;
      let to = str.length;
      while (true) {
        from = to - 3;
        if (from <= 0) {
          from = 0;
        }
        result += str.substring(from, to) + ',';
        if (from === 0) {
          break;
        }
        to = from;
      }
      result = result.substr(0, result.length - 1);
      const splitString = result.split(',');
      const reverseArray = splitString.reverse();
      result = reverseArray.join(',');
      if (setValue) this.form.controls[fieldName].setValue(result);
      return result;
    }
  }
  checkUrlWebsite(event){
    const url = event.target.value.replace(/\s/g, '');
    this.form.controls.website.setValue(url.normalize('NFD').replace(/[\u0300-\u036f]/g, ''));
  }
  getAddress(address,ward,district,townCity){
    let strAddress = "";
    if(address !== undefined && address !== null && address !== '')
      strAddress= address;
    if(strAddress !== undefined && strAddress !== null && strAddress !== ''){
      if(ward !== undefined && ward !== null && ward !== '')
        strAddress = strAddress.concat(' - ',ward)
    }else{
      if(ward !== undefined && ward !== null && ward !== '')
        strAddress = strAddress.concat(ward);
    }
    if(strAddress !== undefined && strAddress !== null && strAddress !== ''){
      if(district !== undefined && district !== null && district !== '')
        strAddress = strAddress.concat(' - ',district)
    }else{
      if(district !== undefined && district !== null && district !== '')
        strAddress = strAddress.concat(district);
    }
    if(strAddress !== undefined && strAddress !== null && strAddress !== ''){
      if(townCity !== undefined && townCity !== null && townCity !== '')
        strAddress = strAddress.concat(' - ',townCity)
    }else{
      if(townCity !== undefined && townCity !== null && townCity !== '')
        strAddress = strAddress.concat(townCity);
    }
    return this.getValueNull(strAddress);
  }
  getValueNull(value) {
    return Utils.isStringNotEmpty(value) ? value : '---';
  }
}

