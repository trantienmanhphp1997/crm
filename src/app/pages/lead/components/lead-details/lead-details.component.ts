import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CustomerLeadService } from '../../service/customer-lead.service';
import * as _ from 'lodash';
import {
  CommonCategory,
  FunctionCode,
  functionUri,
  Scopes,
  SessionKey,
  TaskType,
} from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { forkJoin, of } from 'rxjs';
import { Utils } from 'src/app/core/utils/utils';
import { CustomerApi, CustomerAssignmentApi } from 'src/app/pages/customer-360/apis';
import * as moment from 'moment';
import { CalendarAddModalComponent } from '../../../calendar-sme/components/calendar-add-modal/calendar-add-modal.component';
import { ActivityLogComponent } from '../../../customer-360/components/activity-log/activity-log.component';
import { MatMenuTrigger } from '@angular/material/menu';
import { LeadService } from 'src/app/core/services/lead.service';
import {global} from "@angular/compiler/src/util";

@Component({
  selector: 'app-lead-details',
  templateUrl: './lead-details.component.html',
  styleUrls: ['./lead-details.component.scss'],
})
export class LeadDetailsComponent extends BaseComponent implements OnInit {
  @ViewChild(ActivityLogComponent) activityLog: ActivityLogComponent;
  @ViewChild(MatMenuTrigger) triggerMatMenu: MatMenuTrigger;
  data: any;
  isShow: boolean;
  tabIndex = 0;
  creditTabIndex = 0;
  collateral: any;
  model: any = {};
  creditInformation: any;
  isCreditInfoLoaded: boolean;
  code: any;
  resultFilter: any;
  campaign_tab_index = 0;
  firstBidding = true;
  isManager = false;
  show_data_bidding: boolean;
  bidding: any;
  warningPermission: any;
  listEmail = [];
  taxCode:any;
  customerTypeFromList = '1';
  statusActive: string = "";
  constructor(
    injector: Injector,
    private service: CustomerLeadService,
    private customerApi: CustomerApi,
    private customerAssignmentApi: CustomerAssignmentApi,
    private leadService: LeadService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.LEAD}`);
    this.isLoading = true;
    this.code = this.route.snapshot.paramMap.get('code');
    this.customerTypeFromList = _.get(this.route.snapshot.queryParams, 'customerType');
    this.taxCode = _.get(this.route.snapshot.queryParams, 'taxCode');
  }

  ngOnInit() {
    if (this.customerTypeFromList === '0') {
      this.initDataKHCN();
    } else {
      this.initDataKHDN();
    }
  }

  initDataKHDN() {
    const dataGetAllMail = {
      leadCode: this.code,
      rsId: this.objFunction.rsId,
      scope: 'VIEW',
    };
    const paramStatusCustomer = {
      taxCode: this.taxCode,
    };
    forkJoin([
      this.commonService.getCommonCategory(CommonCategory.CONTACT_POSITION).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.LEAD_TYPE).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.LEAD_ORG_TYPE).pipe(catchError(() => of(undefined))),
      this.service.getLeadByCode(this.code),
      this.service.getResultFilterByLeadCode(this.code).pipe(catchError(() => of({}))),
      this.customerApi.getImportAndExportSales(this.taxCode).pipe(catchError(() => of(undefined))),
      this.customerApi.getStatusActiveMsCore(paramStatusCustomer).pipe(catchError(() => of(undefined))),
    ]).subscribe(
      ([contactPositionConfig, leadTypeConfig, leadBusinessConfig, data, resultFilter, dsxnk, dataStatusActive]) => {
        this.data = data;
        this.resultFilter = resultFilter;
        const paramCreditInfo = {
          identifiedNumber: this.data?.businessRegistrationNumber,
        };
        if(!Utils.isEmpty(dataStatusActive) && dataStatusActive?.length > 0){
          this.statusActive = dataStatusActive[0]?.NOIDUNGTRANGTHAI;
        }
        forkJoin([
          this.service.getAllMail(dataGetAllMail).pipe(catchError(() => of(undefined))),
          this.customerApi
            .getCollateralInformation(this.data?.businessRegistrationNumber)
            .pipe(catchError(() => of(undefined))),
          this.customerApi.getCreditInformation(paramCreditInfo).pipe(catchError(() => of(undefined))),
        ]).subscribe(([allMail, collateral, creditInformation]) => {
          if (allMail) {
            this.listEmail = allMail.filter((e) => e.mail !== undefined);
            contactPositionConfig?.content?.forEach((item) => {
              this.listEmail.forEach((e) => {
                if (item.code === e.position) {
                  e.position = item.name;
                }
              });
            });
          }
          if (creditInformation === undefined) {
            this.messageService.error(this.notificationMessage.error);
          }
          this.collateral = collateral;
          this.creditInformation = creditInformation;
          const listCredit = _.get(this.creditInformation, 'customerCICDTOS');
          const countCredit = _.size(listCredit);
          this.data.amountLoan =
            countCredit > 0 ? Utils.numberWithCommas(_.sumBy(listCredit, 'totalLoanVND')) : undefined;
          this.data.amountBonds = Utils.numberWithCommas(this.creditInformation?.totalBondAmt);
          this.data.numberTCTDLoan = countCredit > 0 ? countCredit : undefined;
          this.data.cicCheckTime = _.first(listCredit)?.cicDate
            ? moment(_.first(listCredit).cicDate).format('DD/MM/YYYY')
            : undefined;
          this.data.debtCIC = _.last(_.sortBy(listCredit, 'gprDebitLoan'))?.gprDebitLoan
            ? parseInt(_.last(_.sortBy(listCredit, 'gprDebitLoan'))?.gprDebitLoan, 10)
            : undefined;
          this.isCreditInfoLoaded = true;
          this.isLoading = false;
        });
        this.data.typeOfBusiness = _.find(
          leadBusinessConfig?.content,
          (item) => item.code === this.data.typeOfBusiness
        )?.name;
        this.data.customerType = _.find(leadTypeConfig?.content, (item) => item.code === this.data.customerType)?.name;
        // const industries = this.sessionService.getSessionData(SessionKey.LIST_INDUSTRY);
        // this.data.industry = _.find(industries, (item) => item.key === this.data.industry)?.value;
        this.data.financialRevenue = Utils.numberWithCommas(this.data.financialRevenue);
        // this.data.salesImportAndExport = Utils.numberWithCommas(this.data.salesImportAndExport);
        this.data.salesImportAndExport = this.getImportAndExportSales(dsxnk?.data);
        this.data.numberOfEmployees = Utils.numberWithCommas(this.data.numberOfEmployees);
        this.data.frgCapAmt = Utils.numberWithCommas(this.data.frgCapAmt);
        this.data.totalInvestCapAmt = Utils.numberWithCommas(this.data.totalInvestCapAmt);
        this.data.vnCapAmt = Utils.numberWithCommas(this.data.vnCapAmt);
        if (this.data?.nationCode === 'VN') {
          this.data.nationCode = 'Việt Nam';
        } else if (this.data.nationCode === 'NN') {
          this.data.nationCode = 'Nước ngoài';
        }
        this.isManager = _.get(this.data, 'isFix');
        this.warningPermission = "Bạn không có quyền xem dữ liệu này !";
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }

  initDataKHCN() {
    forkJoin([
      this.commonService.getCommonCategory(CommonCategory.POTENTIAL_LEVEL).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.POTENTIAL_RESOURCE).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.LEAD_TYPE).pipe(catchError(() => of(undefined))),
      this.leadService.getLeadByCode(this.code).pipe(catchError(() => of(undefined))),
    ]).subscribe(([potentialLevelConfig, potentialResourceConfig, leadTypeConfig ,data]) => {
      // this.commonDataKHCN.potentialLevelConfig = potentialLevelConfig?.content;
      // this.commonDataKHCN.potentialResourceConfig = potentialResourceConfig?.content;
      console.log(data);
      const tempData = data || {};
      tempData.incomePerMonth = Utils.numberWithCommas(tempData.incomePerMonth);
      if (tempData.gender === '1') {
        tempData.genderDisplay = 'Nam';
      } else {
        tempData.genderDisplay = 'Nữ';
      }

      // potentialLevel
      if (tempData.potentialLevel) {
        tempData.potentialLevelDisplay = _.get(_.find(potentialLevelConfig?.content,(i) => i.code === tempData.potentialLevel ), 'name');
      }

      // customerResources
      if (tempData.customerResources) {
        tempData.customerResourcesDisplay = _.get(_.find(potentialResourceConfig?.content,(i) => i.code === tempData.customerResources ), 'name');
      }

      if (tempData.customerType) {
        tempData.customerTypeDisplay = _.get(_.find(leadTypeConfig?.content,(i) => i.code === tempData.customerType ), 'name');
      }


      this.data = tempData;

      this.isLoading = false;
    }, e => {
      e.error = JSON.parse(e.error);
      this.isLoading = false;
      this.messageService.error(e?.error?.messages?.vn);
    });
  }

  onBiddingChange($event) {
    this.bidding = $event;
    this.ref.detectChanges();
  }

  onFirstBiddingChange($event) {
    this.firstBidding = $event;
    this.ref.detectChanges();
  }

  getValue(data, key) {
    return _.get(data, key, '');
  }

  isOpenMore() {
    this.isShow = !this.isShow;
  }

  onCampaignChanged($event) {
    this.campaign_tab_index = _.get($event, 'index');
  }

  onTabChange(event) {
    this.tabIndex = _.get(event, 'index');
    // if (this.tab_index === 2) {
    //   this.onRiskTabChanged({ index: 0 });
    // }
    // if (this.tab_index === 4) {
    //   this.onRelationshipWithMBChanged({ index: 0 });
    // }
    // if (this.tabIndex === 5 && !this.collateral && !this.creditInformation) {
    //   forkJoin([
    //     this.customerApi.getCollateralInformation(this.data?.businessRegistrationNumber),
    //     this.customerApi.getCreditInformation(this.data?.businessRegistrationNumber),
    //   ]).subscribe(
    //     ([collateral, creditInformation]) => {
    //       this.collateral = collateral;
    //       this.creditInformation = creditInformation;
    //       this.isCreditInfoLoaded = true;
    //     },
    //     () => {
    //       this.isCreditInfoLoaded = true;
    //       this.messageService.error(this.notificationMessage.E001);
    //     }
    //   );
    // }
    if (this.tabIndex === 3) {
      this.onCampaignChanged({ index: 0 });
    }
    this.isShow = false;
  }

  onCreditFinanceChanged(event) {
    this.creditTabIndex = _.get(event, 'index');
  }

  onShowDataBiddingChange($event) {
    this.show_data_bidding = $event;
    this.ref.detectChanges();
  }

  onLoadingChange($event) {
    this.isLoading = $event;
    this.ref.detectChanges();
  }

  mailTo(mail) {
    if (_.isEmpty(mail)) {
      if (!_.isEmpty(this.listEmail)) {
        return;
      } else {
        this.triggerMatMenu.closeMenu();
        this.messageService.error('Không có thông tin Email');
      }
    } else {
      const data = {
        type: 'EMAIL',
        leadCode: this.data?.leadCode,
        scope: Scopes.VIEW,
        rsId: this.objFunction.rsId,
      };
      this.customerAssignmentApi.updateActivityLog(data).subscribe((value) => {
        if (value) {
          this.activityLog.getListActivityLog();
        } else {
        }
      });
      const url = `mailto:${mail}`;
      window.location.href = url;
    }
  }

  sendMailToPerson(item: string) {
    const data = {
      type: 'EMAIL',
      leadCode: this.data?.leadCode,
      scope: Scopes.VIEW,
      rsId: this.objFunction.rsId,
    };
    this.customerAssignmentApi.updateActivityLog(data).subscribe((value) => {
      if (value) {
        this.activityLog.getListActivityLog();
      } else {
      }
    });
    const url = `mailto:${item}`;
    window.location.href = url;
  }

  createTaskTodo() {
    const modal = this.modalService.open(CalendarAddModalComponent, { windowClass: 'create-calendar-modal' });
    modal.componentInstance.leadCode = this.code;
    modal.componentInstance.leadName = this.data?.fullName;
    modal.componentInstance.isDetailLead = true;
    modal.result
      .then((res) => {
        if (res) {
          this.activityLog.getListActivityLog();
        } else {
        }
      })
      .catch(() => {});
  }

  back() {
    if (global?.previousUrl.includes('/lead/lead-management')) {
      this.router.navigateByUrl('/lead/lead-management', { state: this.prop ? this.prop : this.state });
    } else {
      this.router.navigateByUrl(global.previousUrl, { state: this.prop ? this.prop : this.state });
    }
  }
  getClassStatus(status) {
    // if (status == null) {
    //   this.info.status = 'Không có thông tin';
    //   return 'status-gray';
    // }
    if (status?.includes('NNT đang hoạt động (đã được cấp GCN ĐKT)') || status?.includes('NNT đang hoạt động (được cấp thông báo MST)')) {
      return 'status-green';
    } else if (status?.includes('NNT tạm nghỉ kinh doanh có thời hạn') || status?.includes('NNT đã chuyển sang tỉnh khác')) {
      return 'status-yellow' ;
    } else if (status?.includes('NNT không hoạt động tại địa chỉ đã đăng ký')
        || status?.includes('NNT không hoạt động tại địa chỉ đăng ký')
        || status?.includes('Không có thông tin')) {
      return 'status-gray';
    } else if (status?.includes('NNT ngừng hoạt động')
        || status?.includes('NNT ngừng hoạt động nhưng chưa hoàn thành thủ tục đóng MST')
        || status?.includes('NNT ngừng hoạt động và đã đóng MST')
        || status?.includes('NNT đã chuyển cơ quan thuế quản lý')) {
      return 'status-red';
    }else{
      return 'status-hidden';
    }
  }

  getClassStatusNew(status){
    return Utils.isEmpty(status)?'status-hidden':'status-green';
  }

  getAddress(address,ward,district,townCity){
    let strAddress = "";
    if(address !== undefined && address !== null && address !== '')
      strAddress= address;
    if(strAddress !== undefined && strAddress !== null && strAddress !== ''){
      if(ward !== undefined && ward !== null && ward !== '')
        strAddress = strAddress.concat(' - ',ward)
    }else{
      if(ward !== undefined && ward !== null && ward !== '')
        strAddress = strAddress.concat(ward);
    }
    if(strAddress !== undefined && strAddress !== null && strAddress !== ''){
      if(district !== undefined && district !== null && district !== '')
        strAddress = strAddress.concat(' - ',district)
    }else{
      if(district !== undefined && district !== null && district !== '')
        strAddress = strAddress.concat(district);
    }
    if(strAddress !== undefined && strAddress !== null && strAddress !== ''){
      if(townCity !== undefined && townCity !== null && townCity !== '')
        strAddress = strAddress.concat(' - ',townCity)
    }else{
      if(townCity !== undefined && townCity !== null && townCity !== '')
        strAddress = strAddress.concat(townCity);
    }
    return this.getValueNull(strAddress);
  }
  getValueNull(value) {
    return Utils.isStringNotEmpty(value) ? value : '---';
  }
//
  getImportAndExportSales(data: any) {
    if (data && data.length > 0) {
      return _.orderBy(data, ['year'], ['desc'])[0];
    }
  }
  
  convertNumber(value){
    if(value)
      return Utils.numberWithCommas(value);
    else
      return '--';
  }
}
