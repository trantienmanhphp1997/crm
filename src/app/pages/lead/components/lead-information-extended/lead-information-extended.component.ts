import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewEncapsulation
} from '@angular/core';
import { Validators } from '@angular/forms';
import _ from 'lodash';
import { Utils } from 'src/app/core/utils/utils';
import * as moment from 'moment';
import { BaseComponent } from 'src/app/core/components/base.component';
import { FormGroup, FormArray } from '@angular/forms';
import { CustomerLeadService } from '../../service/customer-lead.service';
import { cleanDataForm } from 'src/app/core/utils/function';
import { catchError, retry } from 'rxjs/operators';
import { of } from 'rxjs';
import { ScreenType } from 'src/app/core/utils/common-constants';
import { CustomValidators } from 'src/app/core/utils/custom-validations';

@Component({
  selector: 'lead-information-extended',
  templateUrl: './lead-information-extended.component.html',
  styleUrls: ['./lead-information-extended.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LeadInformationExtendedComponent extends BaseComponent implements OnInit, OnChanges {
  constructor(injector: Injector, private service: CustomerLeadService) {
    super(injector);
  }
  @Output()  onClickMail: EventEmitter<any> = new EventEmitter<any>();
  isChiefAccountants = true;
  isContactInfos = true;
  isDirectors = true;
  isUpdate = false;
  today = new Date();
  actionType: string;
  @Input() data: any;
  disabled: boolean = false;
  form = this.fb.group({
    leadCorpContacts: [],
    leadCode: null,
    debtCIC: null,
    numberYearOperation: null,
    amountLoanAndBonds: null,
    authorizedCapital: null,
    suggestProduct: null,
    numberTCTDLoan: null,
    disableEmail: false,
    disablePhone: false,
    disableSMS: false,
    disableMeeting: false,
  });
  directorForm = this.fb.group({
    id: null,
    leadCode: null,
    position: null,
    name: null,
    birthday: null,
    email: ['', CustomValidators.email],
    phone: ['', CustomValidators.onlyNumber],
    idCard: null,
  });
  chiefForm = this.fb.group({
    id: null,
    leadCode: null,
    position: null,
    name: null,
    birthday: null,
    email: ['', CustomValidators.email],
    phone: ['', CustomValidators.onlyNumber],
    idCard: null,
  });
  listContactRemove = [];
  director: any;
  chief: any;
  listContact: any[];
  contactInfo = this.fb.group({
    arrayContact: this.fb.array([]),
  });

  ngOnInit() {
    this.form.controls.disableEmail.disable();
    this.form.controls.disablePhone.disable();
    this.form.controls.disableSMS.disable();
    this.form.controls.disableMeeting.disable();
  }

  formatCurrency(value) {
    return Utils.numberWithCommas(value);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.mapData();
  }

  mapData() {
    if (this.data) {
      this.form.patchValue(this.data);
      this.director = _.find(this.data?.leadCorpContacts, (item) => item.position === 'DIR') || {};
      this.director.birthday = this.director?.birthday ? new Date(this.director.birthday) : null;
      this.directorForm.patchValue(this.director);
      this.chief = _.find(this.data?.leadCorpContacts, (item) => item.position === 'CHIEF') || {};
      this.chief.birthday = this.chief?.birthday ? new Date(this.chief.birthday) : null;
      this.chiefForm.patchValue(this.chief);
      this.listContact = _.filter(this.data?.leadCorpContacts, (item) => item.position === 'TRADER');
      this.arrayContact().clear();
      if (_.size(this.listContact) > 0) {
        _.forEach(this.listContact, (contact) => {
          const form = this.fb.group({
            id: null,
            leadCode: null,
            position: null,
            name: null,
            birthday: null,
            email: ['', CustomValidators.email],
            phone: ['', CustomValidators.onlyNumber],
            idCard: null,
          });
          contact.birthday = contact?.birthday ? new Date(contact.birthday) : null;
          form.patchValue(contact);
          this.arrayContact().push(form);
        });
      } else {
        this.arrayContact().push(this.newArrayContact());
      }
    }
  }

  openChiefAccountants() {
    if (this.isUpdate) {
      return;
    }
    this.isChiefAccountants = !this.isChiefAccountants;
  }

  openContactInfos() {
    if (this.isUpdate) {
      return;
    }
    this.isContactInfos = !this.isContactInfos;
  }

  openDirectors() {
    if (this.isUpdate) {
      return;
    }
    this.isDirectors = !this.isDirectors;
  }

  getValue(item: any, key: string) {
    return Utils.isStringEmpty(_.get(item, key)) ? '---' : _.get(item, key);
  }

  updateExtend() {
    this.mapData();
    this.isUpdate = !this.isUpdate;
    this.isChiefAccountants = true;
    this.isContactInfos = true;
    this.isDirectors = true;
    if (!this.isUpdate) {
      this.form.controls.disableEmail.disable();
      this.form.controls.disablePhone.disable();
      this.form.controls.disableSMS.disable();
      this.form.controls.disableMeeting.disable();
    } else {
      this.form.controls.disableEmail.enable();
      this.form.controls.disablePhone.enable();
      this.form.controls.disableSMS.enable();
      this.form.controls.disableMeeting.enable();
    }
  }

  save() {
    var d = new Date();
    var n = d.getTimezoneOffset();
    var timezone = n / -60;
    if (this.directorForm.valid && this.chiefForm.valid && this.contactInfo.valid) {
      this.confirmService.confirm().then((isConfirm) => {
        if (isConfirm) {
          const data = cleanDataForm(this.form);
          data.leadCorpContacts = [];
          const director = cleanDataForm(this.directorForm);
          const chief = cleanDataForm(this.chiefForm);
          const listContact = cleanDataForm(this.contactInfo)?.arrayContact;
          director.position = 'DIR';
          data.leadCorpContacts.push(director);
          chief.position = 'CHIEF';
          data.leadCorpContacts.push(chief);
          _.forEach(listContact, (contact) => {
            contact.position = 'TRADER';
            data.leadCorpContacts.push(contact);
          });
          data.leadCorpContacts = [...data.leadCorpContacts, ...this.listContactRemove];
          _.forEach(data.leadCorpContacts, (contact) => {
            contact.leadCode = this.data.leadCode;
            if (_.isEmpty(contact?.action)) {
              contact.action = _.isEmpty(_.trim(contact.id)) ? 'CREATE' : 'UPDATE';
            }
          });
          this.isLoading = true;
          _.forEach(_.get(data, 'leadCorpContacts'), (x) => {
            if (Utils.isNotNull(x.birthday)) {
              x.birthday = moment(x.birthday).add('hours', timezone);
            }
          });
          this.service.updateInfoExtend(data).subscribe(
            () => {
              this.service
                .getLeadByCode(data.leadCode)
                .pipe(
                  retry(3),
                  catchError(() => of(undefined))
                )
                .subscribe((data) => {
                  if (data) {
                    this.data = data;
                    this.mapData();
                  }
                  this.messageService.success(this.notificationMessage.success);
                  this.isLoading = false;
                  this.isUpdate = false;
                });
            },
            () => {
              this.messageService.error(this.notificationMessage.error);
              this.isLoading = false;
            }
          );
          this.form.controls.disableEmail.disable();
          this.form.controls.disablePhone.disable();
          this.form.controls.disableSMS.disable();
          this.form.controls.disableMeeting.disable();
        }
      });
    }
  }

  cancel() {
    this.isUpdate = false;
    this.mapData();
    if (!this.isUpdate) {
      this.form.controls.disableEmail.disable();
      this.form.controls.disablePhone.disable();
      this.form.controls.disableSMS.disable();
      this.form.controls.disableMeeting.disable();
    } else {
      this.form.controls.disableEmail.enable();
      this.form.controls.disablePhone.enable();
      this.form.controls.disableSMS.enable();
      this.form.controls.disableMeeting.enable();
    }
    this.directorForm.reset();
    this.chiefForm.reset();
    this.contactInfo.reset();
  }

  createPersonContact() {
    this.arrayContact().push(this.newArrayContact());
  }

  newArrayContact(): FormGroup {
    return this.fb.group({
      id: null,
      leadCode: null,
      position: null,
      name: null,
      birthday: null,
      email: ['', CustomValidators.email],
      phone: ['', CustomValidators.onlyNumber],
      idCard: null,
    });
  }

  removeInfo(i: number) {
    const item = this.arrayContact().at(i).value;
    if (!_.isEmpty(_.trim(item?.id))) {
      item.action = 'DELETE';
      this.listContactRemove.push(item);
    }
    this.arrayContact().removeAt(i);
  }

  getForm(i: number): FormGroup {
    return this.arrayContact().controls[i] as FormGroup;
  }

  arrayContact(): FormArray {
    return this.contactInfo.get('arrayContact') as FormArray;
  }

  getValueDateString(data, key) {
    return _.isDate(_.get(data, key)) ? moment(_.get(data, key)).format('DD/MM/YYYY') : '---';
  }

  sendMailToPerson(item: any, key: string){
    if(!Utils.isStringEmpty(_.get(item, key))) {
      this.onClickMail.emit(_.get(item, key));
    }
  }
}
