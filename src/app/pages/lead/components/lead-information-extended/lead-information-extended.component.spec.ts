import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadInformationExtendedComponent } from './lead-information-extended.component';

describe('LeadInformationExtendedComponent', () => {
  let component: LeadInformationExtendedComponent;
  let fixture: ComponentFixture<LeadInformationExtendedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadInformationExtendedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadInformationExtendedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
