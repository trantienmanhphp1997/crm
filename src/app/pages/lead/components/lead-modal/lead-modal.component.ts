import { Component, OnInit, Injector, ViewChild, ViewEncapsulation, HostBinding, Input } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import {CustomerLeadService} from '../../service/customer-lead.service';
import {Pageable} from '../../../../core/interfaces/pageable.interface';
import {global} from '@angular/compiler/src/util';
import {functionUri, Scopes} from '../../../../core/utils/common-constants';

@Component({
  selector: 'app-lead-modal',
  templateUrl: './lead-modal.component.html',
  styleUrls: ['./lead-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LeadModalComponent extends BaseComponent implements OnInit {
  @HostBinding('class.list__lead-content') appRightContent = true;
  isLoading = false;
  @Input() dataSearch: any;
  listData = [];
  prevParams: any;
  notificationType: any;
  pageable: Pageable;
  paramSearch = {
    id: '',
    notiType: '',
    pageNumber: 0,
    pageSize: _.get(global, 'userConfig.pageSize'),
  };
  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private modalActive: NgbActiveModal,
    private service: CustomerLeadService
  ) {
    super(injector);
  }

  ngOnInit() {
    this.search();
  }

  search() {
    this.isLoading = true;
    this.paramSearch.id = this.dataSearch.id;
    this.paramSearch.notiType = this.dataSearch.notiType;
    this.service.getLeadCustomerByNotiId(this.paramSearch).subscribe(
      (data) => {
        this.listData = data.content;
        this.isLoading = false;
        this.pageable = {
          size: 10,
          totalElements: data?.totalElements,
          totalPages: data?.totalPages,
          currentPage: 0,
        };
      },
      () => {
        this.isLoading = false;
      }
    );
  }
  paging(event) {
    if (!this.isLoading) {
      this.paramSearch.pageNumber = _.get(event, 'page') ? _.get(event, 'page') - 1 : 0;
      this.search();
    }
  }

  closeModal() {
    this.modalActive.close(false);
  }

  onActive(event) {
    if (event.type === 'click') {
      this.modalActive.close(false);
      event.cellElement.blur();
      const item = _.get(event, 'row');
      if (this.notificationType === 'NOTIFICATION_ASSIGN_LEAD_GROUP'
        || this.notificationType === 'NOTIFICATION_PREPARE_TO_EXPIRE_LEAD_GROUP') {
        this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
          this.router.navigate([functionUri.lead_management, 'detail', item.code], { state: this.prevParams })
        )
      } else if (this.notificationType === 'NOTIFICATION_ASSIGN_CUSTOMER_GROUP') {
        if (!_.isEmpty(_.trim(item.customerTypeMerge)) && !_.isEmpty(_.trim(item.code))) {
          this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
          this.router.navigate(['/customer360/customer-manager', 'detail', _.toLower(item.customerTypeMerge), item.code], {
            skipLocationChange: true,
            queryParams: {
              manageRM: item.manageRM,
              showBtnRevenueShare: false,
            },
          }))
        }
      }

    }
  }
}
