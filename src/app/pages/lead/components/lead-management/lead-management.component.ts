import {Component, Injector, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';
import { global } from '@angular/compiler/src/util';
import { cleanDataForm } from 'src/app/core/utils/function';
import { CustomerLeadService } from '../../service/customer-lead.service';
import {
  CommonCategory,
  Division,
  FunctionCode,
  functionUri,
  maxInt32,
  Scopes,
  SessionKey,
} from 'src/app/core/utils/common-constants';
import { forkJoin, of } from 'rxjs';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { catchError, finalize } from 'rxjs/operators';
import { RmApi } from 'src/app/pages/rm/apis';
import { FileService } from 'src/app/core/services/file.service';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {DatatableComponent, SelectionType} from '@swimlane/ngx-datatable';
import { RmModalComponent } from 'src/app/pages/rm/components';
import { Utils } from 'src/app/core/utils/utils';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { TranslateService } from '@ngx-translate/core';
import { LeadService } from 'src/app/core/services/lead.service';
import { LeadHistoryComponent } from '../lead-history/lead-history.component';
import { FormControl } from '@angular/forms';
import { UserService } from '../../../system/services/users.service';

@Component({
  selector: 'app-lead-management',
  templateUrl: './lead-management.component.html',
  styleUrls: ['./lead-management.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'lead-management',
  },
})
export class LeadManagementComponent extends BaseComponent implements OnInit {

  @ViewChild('table') table: DatatableComponent;
  limit = global.userConfig.pageSize;
  commonData = {
    listRmManagementTerm: [],
    listRmManagement: [],
    listStatus: [],
    listCustomerType: [],
    listBranch: [],
    listScreeningResults: [],
    listSourceCustomer: [],
    listBusiness: [],
    listSegment: [],
    potentialLevelConfig: [],
    potentialResourceConfig: [],
    listSlas: [
      {
        value: '3',
      },
      {
        value: '2',
      },
      {
        value: '1',
      },
    ],
  };

  searchForm = this.fb.group({
    leadCode: null,
    fullName: '',
    customerType: '1',
    businessRegistrationNumber: '',
    taxCode: '',
    phone: '',
    listBranchSearch: [],
    lstRmCode: [],
    filters: [],
    sourceLead: '',
    status: '',
    industry: '',
    segment: '',
    slas: [],
    listTownCiti: [[]],
    listDistrict: [[]],
    listWard: [[]],
  });

  commonDataKHCN = {
    listRmManagementTerm: [],
    listRmManagement: [],
    listStatus: [],
    listCustomerType: [],
    listBranch: [],
    potentialLevelConfig: [],
    potentialResourceConfig: [],
  };

  searchFormKHCN = this.fb.group({
    leadCode: null,
    customerName: '',
    customerType: '',
    idCard: '',
    phone: '',
    email: '',
    status: '',
    listBranch: [],
    listRM: [],
    campaign: [],
  });
  isLoadingSearch = false;
  isOpenMore: boolean;
  industryCode = '';
  listData = [];
  selected = [];
  pageable: Pageable;
  user: any;
  paramSearch = {
    page: 0,
    size: _.get(global, 'userConfig.pageSize'),
    rsId: '',
    scope: Scopes.VIEW,
  };
  prevParams: any;
  prevParamsKHCN: any;
  checkAll = false;

  isFirst = false;
  scopes: Array<any>;
  limitKHTN: any;
  isShow = true;
  cityList = [];
  districtList = [];
  streetList = [];

  @Input() dataSearch: any;
  @Input() listSelected = [];
  @Input() isModal = false;
  @Input() isCheck = false;
  @Input() modalActive: NgbModalRef;

  constructor(
    injector: Injector,
    private service: CustomerLeadService,
    private categoryService: CategoryService,
    private rmApi: RmApi,
    protected translate: TranslateService,
    private fileService: FileService,
    private leadService: LeadService,
    private userService: UserService
  ) {
    super(injector);
    this.isLoading = true;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.LEAD}`);
    this.scopes = _.get(this.objFunction, 'scopes');
    this.paramSearch.rsId = this.objFunction?.rsId;
    this.user = this.sessionService.getSessionData(SessionKey.USER_INFO);
    this.prop = this.router.getCurrentNavigation()?.extras?.state;
  }

  ngOnInit() {
    if (this.state) {
      this.limit = this.state.size;
      // this.pageable = {
      //   size: this.paramSearch.size,
      //   totalElements: data?.totalElements,
      //   totalPages: data?.totalPages,
      //   currentPage: this.paramSearch.page,
      // };
      this.paramSearch.size =  this.state.size;
      this.paramSearch.page =  this.state.page;
    }
    if (this.prop) {
      this.searchForm.controls.customerType.setValue(this.prop.customerType);
      this.searchFormKHCN.controls.customerType.setValue(this.prop.customerType);
    }
    if (this.searchForm.controls.customerType.value === '0') {
      this.initDataKHCN();
    } else {
      this.initData();
    }
    this.setValueChanges();

    this.getLocation('city');
    this.commonService.getCommonCategory(CommonCategory.LIMIT_CUSTOMER_KHTN).pipe(catchError(() => of(undefined))).subscribe((res) => {
      this.limitKHTN = res?.content?.find((item) => item.code === 'LIMIT_KHTN')?.value;
    });
  }

  initData() {
    this.isLoading= true;
    this.listData = [];
    const state = this.sessionService.getSessionData(FunctionCode.LEAD);
    if (state) {
      // if (this.state?.customerType === undefined) {
      //   this.state.customerType = '0';
      // }
      this.commonData = state;
      this.commonDataKHCN = state;
      if (this.prop) {
        // console.log(this.prop);
        this.searchForm.patchValue(this.prop);
        this.prop = '';
      } else {
        this.searchForm.controls.customerType.setValue(
          _.find(this.commonData.listCustomerType, (item) => item.isDefault === true)?.code ||
          _.first(this.commonData.listCustomerType)?.code
        );
        this.searchFormKHCN.controls.customerType.setValue(
          _.find(this.commonData.listCustomerType, (item) => item.isDefault === true)?.code ||
          _.first(this.commonData.listCustomerType)?.code
        );
      }
      if (_.find(this.commonData.listCustomerType, (item) => item.code === '1') === undefined) {
        this.commonDataKHCN = this.commonData;
        this.searchKHCN(true);
        this.sessionService.setSessionData(FunctionCode.LEAD_KHCN, this.commonData);
      } else {
        this.search(false);
      }
    } else {
      this.isLoading = true;
      this.getRmManager();
      forkJoin([
        this.categoryService.getBranches().pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.LEAD_TYPE).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.LEAD_STATUS).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.LEAD_LEVEL).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.LEAD_FILTER).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.LEAD_SOURCE).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.LEAD_ORG_TYPE).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.POTENTIAL_LEVEL).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.POTENTIAL_RESOURCE).pipe(catchError(() => of(undefined))),
        this.commonService
          .getCommonCategory(CommonCategory.REVENUE_CUSTOMER_SEGMENT)
          .pipe(catchError(() => of(undefined))),
      ]).subscribe(
        ([
           branchAll,
           leadTypeConfig,
           leadStatusConfig,
           leadLevelConfig,
           leadFilterConfig,
           leadSourceConfig,
           leadBusinessConfig,
           potentialLevelConfig,
           potentialResourceConfig,
           leadSegmentConfig,
         ]) => {
          this.commonData.potentialLevelConfig = potentialLevelConfig?.content;
          this.commonData.potentialResourceConfig = potentialResourceConfig?.content;
          this.commonData.listBranch = branchAll?.content.map((item) => {
            return {
              code: item.code, name: `${item.code} - ${item.name}`
            };
          }) || [];
          this.commonData.listCustomerType = leadTypeConfig?.content || [];
          const divisionOfUser = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM);
          if (!_.find(divisionOfUser, (item) => item.code === Division.INDIV)) {
            this.commonData.listCustomerType = _.filter(this.commonData.listCustomerType, (item) => item.code !== '0');
          }
          if (!_.find(divisionOfUser, (item) => item.code === Division.FI)) {
            this.commonData.listCustomerType = _.filter(this.commonData.listCustomerType, (item) => item.code !== '2');
          }
          if (!_.find(divisionOfUser, (item) => item.code === Division.CIB || item.code === Division.SME)) {
            this.commonData.listCustomerType = _.filter(this.commonData.listCustomerType, (item) => item.code !== '1');
          }
          this.commonData.listSegment = leadSegmentConfig?.content || [];
          this.commonData.listSegment.unshift({ code: '', name: this.fields.all });
          this.commonData.listStatus = leadStatusConfig?.content || [];
          this.commonData.listStatus.unshift({ code: '', name: this.fields.all });
          this.commonData.listSourceCustomer = leadSourceConfig?.content || [];
          this.commonData.listSourceCustomer.unshift({ code: '', name: this.fields.all });
          this.commonData.listScreeningResults =
            leadFilterConfig?.content?.map((item) => {
              return {
                code: item.code,
                name: `${item.code} - ${item.name}`,
              };
            }) || [];
            this.commonData.listBusiness = leadBusinessConfig?.content || [];
            this.searchForm.controls.customerType.setValue(_.find(this.commonData.listCustomerType, (item) => item.isDefault === true)?.code || _.first(this.commonData.listCustomerType)?.code);
            this.searchFormKHCN.controls.customerType.setValue(_.find(this.commonData.listCustomerType, (item) => item.isDefault === true)?.code || _.first(this.commonData.listCustomerType)?.code);
            this.sessionService.setSessionData(FunctionCode.LEAD, this.commonData);
            if (_.find(this.commonData.listCustomerType, (item) => item.code === '1') === undefined) {
              this.commonDataKHCN = this.commonData;
              this.searchKHCN(true);
              this.sessionService.setSessionData(FunctionCode.LEAD_KHCN, this.commonData);
            } else {
              this.search(true);
            }
            // this.isLoading = false;
          }, () => {
            // this.isLoading = false;
          });

    }
    this.searchForm.controls.leadCode.valueChanges.subscribe((value) => {
      if (!_.isEmpty(value)) {
        const reg = new RegExp(/^[0-9]*$/g);
        value = value.toString().replace(/,/g, '');
        if (!reg.test(value)) {
          value = value.replace(/\D/g, '');
        }
        this.searchForm.controls.leadCode.setValue(value, { emitEvent: false });
      }
    });
    this.searchForm.controls.listBranchSearch.valueChanges.subscribe((value) => {
      if (!this.isLoading) {
        this.searchForm.get('lstRmCode').setValue([]);
        if (_.isEmpty(value)) {
          this.commonData.listRmManagement = [...this.commonData.listRmManagementTerm];
        } else {
          this.commonData.listRmManagement = _.filter(this.commonData.listRmManagementTerm, (item) =>
            _.includes(value, item.branchCode)
          );
        }
      }
    });
  }

  search(isSearch?: boolean) {
    this.isLoading = true;
    if (this.dataSearch) {
      _.forEach(this.dataSearch, (item, key) => {
        if (_.get(this.dataSearch, `${key}.disabled`, false)) {
          this.searchForm.get(key).disable();
        }
        if (_.get(this.dataSearch, `${key}.value`)) {
          this.searchForm.get(key).setValue(_.get(this.dataSearch, `${key}.value`));
        }
      });
    }
    let params = cleanDataForm(this.searchForm);
    if (isSearch) {
      this.paramSearch.page = 0;
    }
    params = { ...params, ...this.paramSearch };
    params.listBranchSearch = _.isEmpty(params.listBranchSearch) ? [] : params.listBranchSearch;
    params.lstRmCode = _.isEmpty(params.lstRmCode) ? [] : params.lstRmCode;
    this.service.searchLead(params).subscribe(
      (data) => {
        this.selected = [];
        this.listData = data?.content || [];
        const listIndustry = this.sessionService.getSessionData(SessionKey.LIST_INDUSTRY);
        for (const item of this.listData) {
          const itemSelected = _.find(listIndustry, (o) => o.key === item.industry);
          item.industryName = itemSelected ? `${itemSelected.key} - ${itemSelected.value}` : '';
          if (_.find(this.listSelected, (o) => o.leadCode === item.leadCode)) {
            this.selected.push(item);
          }
        }
        this.pageable = {
          size: this.paramSearch.size,
          totalElements: data?.totalElements,
          totalPages: data?.totalPages,
          currentPage: this.paramSearch.page,
        };
        this.checkAll = _.size(this.selected) === +this.pageable.size;
        this.isLoading = false;
        this.prevParams = params;
      },
      () => {
        this.isLoading = false;
      });

  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.page = pageInfo.offset;
    if (this.searchForm.controls.customerType.value === '0') {
      this.searchKHCN(false);
    } else {
      this.search(false);
    }
  }

  onActive(event) {
    if (event.type === 'dblclick' && !this.isModal) {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      if (this.searchForm.controls.customerType.value === '0') {
        this.router.navigate([functionUri.lead_management, 'detail', item.id], {
          skipLocationChange: true,
          state: this.prevParamsKHCN,
          queryParams: {
            customerType: '0'
          }
        });
      } else {
        this.router.navigate([functionUri.lead_management, 'detail', item.leadCode], {
          skipLocationChange: true, state: this.prevParams, queryParams: {
            customerType: '1',
            taxCode: item.taxCode
          }
        });
      }
    }
  }

  fillIndustry(value) {
    this.searchForm.controls.industry.setValue(value);
  }

  openMore() {
    this.isOpenMore = !this.isOpenMore;
  }

  getRmManager() {
    if (this.searchForm.controls.customerType.value === '0') {
      this.commonDataKHCN.listRmManagement = [];
    } else {
      this.commonData.listRmManagement = [];
    }
    const paramsUser = {
      page: 0,
      isActive:1,
      size: maxInt32,
      searchAdvanced: '',
    };
    this.userService.searchUser(paramsUser).pipe(catchError((e) => of(undefined))),
    this.rmApi
      .post('findAll', {
        page: 0,
        size: maxInt32,
        crmIsActive: true,
        branchCodes: [],
        rsId: this.objFunction?.rsId,
        scope: Scopes.VIEW,
      })
      .pipe(catchError(() => of(undefined)))
      .subscribe((res) => {
        const listRm = [];
        res?.content?.forEach((item) => {
          if (!_.isEmpty(item?.t24Employee?.employeeCode)) {
            listRm.push({
              code: _.trim(item?.t24Employee?.employeeCode),
              displayName: _.trim(item?.t24Employee?.employeeCode) + ' - ' + _.trim(item?.hrisEmployee?.fullName),
              branchCode: _.trim(item?.t24Employee?.branchLevel2),
            });
          }
        });
        if (!listRm?.find((item) => item.code === this.currUser?.code)) {
          listRm.push({
            code: this.currUser?.code,
            displayName:
              Utils.trimNullToEmpty(this.currUser?.code) + ' - ' + Utils.trimNullToEmpty(this.currUser?.fullName),
            branchCode: this.currUser?.branch,
          });
        }
        if (this.searchForm.controls.customerType.value === '0') {
          this.commonDataKHCN.listRmManagementTerm = [...listRm];
          this.commonDataKHCN.listRmManagement = [...listRm];
        } else {
          this.userService.searchUser(paramsUser).pipe(catchError((e) => of(undefined))).subscribe((res2) => {
            const listRmAll = [];
            res2?.content?.forEach((item) => {
              if (!_.isEmpty(item?.code)) {
                listRmAll.push({
                  code: _.trim(item?.code),
                  displayName: _.trim(item?.code) + ' - ' + _.trim(item?.fullName),
                  branchCode: _.trim(item?.branch)
                });
              }
            });
            this.commonData.listRmManagementTerm = [...listRmAll];
            this.commonData.listRmManagement = [...listRmAll];
          })
        }
      });
  }

  exportFile() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+(this.pageable?.totalElements || 0) === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.isLoading = true;
    this.service.exportFileLead(this.prevParams).subscribe(
      (fileId) => {
        if (!_.isEmpty(fileId)) {
          this.fileService
            .downloadFile(fileId, 'crm_lead.xlsx')
            .pipe(catchError((e) => of(false)))
            .subscribe((res) => {
              this.isLoading = false;
              if (!res) {
                this.messageService.error(this.notificationMessage.error);
              }
            });
        } else {
          this.messageService.error(this.notificationMessage?.export_error || '');
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(this.notificationMessage?.export_error || '');
        this.isLoading = false;
      }
    );
  }

  createFromFile() {
    this.router.navigate([this.router.url, 'importFile'],
      { state: (this.searchForm.controls.customerType.value === '0' ? this.prevParamsKHCN : this.prevParams), skipLocationChange: true });
  }

  createLead() {
    this.router.navigate([this.router.url, 'create'], {
      state: (this.searchForm.controls.customerType.value === '0' ? this.prevParamsKHCN : this.prevParams),
      skipLocationChange: true,
      queryParams: {
        customerType: this.searchForm.controls.customerType.value,
        haveInDiv:  _.find(this.commonData.listCustomerType, (item) => item.code === '0')?.code === '0',
        haveSME:  _.find(this.commonData.listCustomerType, (item) => item.code === '1')?.code === '1',
      },
    });
  }

  updateLead(item) {
    this.router.navigate([this.router.url, 'update', item.leadCode], {
      state: this.prevParams,
      skipLocationChange: true,
    });
  }

  deleteLead(item) {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.deleteLead(item.leadCode).subscribe(
          () => {
            this.listData = this.listData?.filter((i) => i.leadCode !== item.leadCode);
            this.search();
            this.messageService.success(this.notificationMessage.success);
          },
          (e) => {
            this.isLoading = false;
            if (!_.isEmpty(e?.error?.description)) {
              this.messageService.error(e.error.description);
            } else {
              this.messageService.error(this.notificationMessage.error);
            }
          }
        );
      }
    });
  }

  assignRM(item) {
    const formSearch = {
      isAssignRm: true,
      crmIsActive: { value: 'true', disabled: true },
      statusWork: 'NTS',
    };
    const config = {
      selectionType: SelectionType.single,
    };
    const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
    modal.componentInstance.config = config;
    modal.componentInstance.dataSearch = formSearch;
    modal.componentInstance.typeAssignKHTN = this.searchForm.get('customerType').value === '0' ? 'CN' : 'DN';
    modal.result
      .then((res) => {
        if (res?.listSelected?.length > 0) {
          if (Utils.trimNullToEmpty(res.listSelected[0]?.t24Employee?.employeeCode) === '') {
            this.confirmService.warn(this.notificationMessage.ASSIGN010);
            return;
          } else {
            this.isLoading = true;
            const data = {
              rmCode: res.listSelected[0]?.t24Employee?.employeeCode,
              listLeadCode: [item.leadCode],
              management: 'rm',
            };
            this.service.checkResultFilter(data.listLeadCode).subscribe(
              (result) => {
                if (result.code === '200') {
                  this.service.assignToBranchOrRm(data).subscribe(
                    () => {
                      this.isLoading = false;
                      this.messageService.success(this.notificationMessage.success);
                      this.search();
                    },
                    (e) => {
                      this.isLoading = false;
                      this.messageService.error(e?.error?.description);
                    }
                  );
                } else {
                  this.isLoading = false;
                  this.confirmService.error(result.message);
                }
              },
              () => {
                this.isLoading = false;
                this.messageService.error(this.notificationMessage.E001);
              }
            );
          }
        }
      })
      .catch(() => {});
  }

  onCheckboxAllFn(event) {
    if (event.checked) {
      this.selected = this.listData;
      this.listSelected = _.unionBy(this.listSelected, this.listData, 'leadCode');
    } else {
      this.selected = [];
      this.listSelected = _.differenceBy(this.listSelected, this.listData, 'leadCode');
    }
  }

  onCheckboxAll(event, row) {
    if (event.checked) {
      this.selected.push(row);
      this.listSelected.push(row);
    } else {
      this.selected = this.selected.filter(item => item.leadCode !== row.leadCode);
      this.listSelected = this.listSelected.filter(item => item.leadCode !== row.leadCode);
    }
  }

  closeModal() {
    this.modalActive?.close();
  }

  choose() {
    this.modalActive?.close(this.listSelected);
  }

  randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  getClassByNumber(n: number) {
    let classBtn = '';
    switch (n) {
      case 3:
        classBtn = 'btn-outline--active';
        break;
      case 2:
        classBtn = 'btn-outline--warning';
        break;
      case 1:
        classBtn = 'btn-outline--danger';
        break;
      default:
        classBtn = 'btn-outline';
    }
    return classBtn;
  }

  onTimeExtension(item) {
    let value = '';
    if (item.retryExtendTime === 3) {
      value = 'Bạn đã hết số lượt gia hạn!';
    } else {
      this.translate
        .get('notificationMessage.extendSLA', { fullName: item.fullName, retryExtendTime: 3 - item.retryExtendTime })
        .subscribe((data) => {
          value = data;
        });
    }
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.componentInstance.message = value;
    confirm.componentInstance.title = 'Gia hạn SLA';
    confirm.componentInstance.disableConfirm = item.retryExtendTime === 3 ? true : false;
    confirm.result
      .then((confirmed: boolean) => {
        if (confirmed) {
          const params = {
            id: item.id,
            leadCode: item.leadCode,
          };
          this.service.extendTimeSla(params).subscribe(
            (value) => {
              if (value === 'SUCCESS') {
                this.messageService.success(this.notificationMessage.success);
                this.search(true);
              }
            },
            (error) => {
              this.messageService.error(JSON.parse(error.error)?.description);
            }
          );
        }
      })
      .catch(() => {});
  }

  /* KHCN */
  checkScopes(codes: Array<any>) {
    if (Utils.isArrayEmpty(codes)) return false;
    return Utils.isArrayNotEmpty(_.filter(this.scopes, (x) => codes.includes(x)));
  }

  onChangeCustomerType(event) {
    this.searchFormKHCN.controls.customerType.setValue(event.value);
    this.searchForm.controls.customerType.setValue(event.value);
    if (event.value !== '0' && event.value !== '1') {
      if (_.isEmpty(this.commonData)) {
        this.initData();
      } else {
        this.search(true);
      }
    } else {
      if (event.value === '0') {
        this.initDataKHCN();
      } else {
        this.initData();
      }
    }
  }

  onChangeCustomerTypeKHCN(event) {
    this.searchFormKHCN.controls.customerType.setValue(event.value);
    this.searchForm.controls.customerType.setValue(event.value);
    if (event.value !== '0' && event.value !== '1') {
      if (_.isEmpty(this.commonData)) {
        this.initData();
      } else {
        this.search(true);
      }
    } else {
      if (event.value === '0') {
        this.initDataKHCN();
      } else {
        this.initData();
      }
    }
  }

  initDataKHCN() {
    this.listData = [];
    const state = this.sessionService.getSessionData(FunctionCode.LEAD_KHCN);
    if (state) {
      this.commonDataKHCN = state;
      this.commonData = state;
      if (this.prop) {
        this.searchFormKHCN.patchValue(this.prop);
        this.prop = '';
      } else {
        this.searchFormKHCN.controls.customerType.setValue('0');
      }
      this.searchKHCN(false);
    } else {
      this.getRmManager();
      forkJoin([
        this.categoryService
          .getBranchesOfUser(this.paramSearch.rsId, Scopes.VIEW)
          .pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.LEAD_TYPE).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.LEAD_STATUS).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.POTENTIAL_LEVEL).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.POTENTIAL_RESOURCE).pipe(catchError(() => of(undefined))),
      ]).subscribe(([branchOfUser, leadTypeConfig, leadStatusConfig, potentialLevelConfig, potentialResourceConfig]) => {
        this.commonDataKHCN.potentialLevelConfig = potentialLevelConfig?.content;
        this.commonDataKHCN.potentialResourceConfig = potentialResourceConfig?.content;
        this.commonDataKHCN.listBranch =
          branchOfUser?.map((item) => {
            return {
              code: item.code,
              name: `${item.code} - ${item.name}`,
            };
          }) || [];
        if (!_.find(this.commonDataKHCN.listBranch, (item) => item.code === this.currUser?.branch)) {
          this.commonDataKHCN.listBranch.push({
            code: this.currUser?.branch,
            name: `${this.currUser?.branch} - ${this.currUser?.branchName}`,
          });
        }
        this.commonDataKHCN.listCustomerType = leadTypeConfig?.content || [];
        const divisionOfUser = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM);
        if (!_.find(divisionOfUser, (item) => item.code === Division.INDIV)) {
          this.commonDataKHCN.listCustomerType = _.filter(
            this.commonDataKHCN.listCustomerType,
            (item) => item.code !== '0'
          );
        }
        if (!_.find(divisionOfUser, (item) => item.code === Division.FI)) {
          this.commonDataKHCN.listCustomerType = _.filter(
            this.commonDataKHCN.listCustomerType,
            (item) => item.code !== '2'
          );
        }
        if (!_.find(divisionOfUser, (item) => item.code === Division.CIB || item.code === Division.SME)) {
          this.commonDataKHCN.listCustomerType = _.filter(
            this.commonDataKHCN.listCustomerType,
            (item) => item.code !== '1'
          );
        }

        this.commonDataKHCN.listStatus = leadStatusConfig?.content || [];
        this.commonDataKHCN.listStatus.unshift({ code: '', name: this.fields.all });

        this.searchFormKHCN.controls.customerType.setValue('0');
        this.sessionService.setSessionData(FunctionCode.LEAD_KHCN, this.commonDataKHCN);
        this.searchKHCN(true);
      });
    }

    // this.searchFormKHCN.controls.leadCode.valueChanges.subscribe((value) => {
    //   if (!_.isEmpty(value)) {
    //     const reg = new RegExp(/^[0-9]*$/g);
    //     value = value.toString().replace(/,/g, '');
    //     if (!reg.test(value)) {
    //       value = value.replace(/\D/g, '');
    //     }
    //     this.searchFormKHCN.controls.leadCode.setValue(value, { emitEvent: false });
    //   }
    // });

    this.searchFormKHCN.controls.listBranch.valueChanges.subscribe((value) => {
      if (!this.isLoading) {
        this.searchFormKHCN.get('listRM').setValue([]);
        if (_.isEmpty(value)) {
          this.commonDataKHCN.listRmManagement = [...this.commonDataKHCN.listRmManagementTerm];
        } else {
          this.commonDataKHCN.listRmManagement = _.filter(this.commonDataKHCN.listRmManagementTerm, (item) =>
            _.includes(value, item.branchCode)
          );
        }
      }
    });
  }

  searchKHCN(isSearch?: boolean) {
    this.listData = [];
    this.isLoading = true;
    if (this.dataSearch) {
      _.forEach(this.dataSearch, (item, key) => {
        if (_.get(this.dataSearch, `${key}.disabled`, false)) {
          this.searchFormKHCN.get(key).disable();
        }
        if (_.get(this.dataSearch, `${key}.value`)) {
          this.searchFormKHCN.get(key).setValue(_.get(this.dataSearch, `${key}.value`));
        }
      });
    }
    let params = cleanDataForm(this.searchFormKHCN);
    if (isSearch) {
      this.paramSearch.page = 0;
    }
    params = { ...params, ...this.paramSearch };
    params.listBranch = _.isEmpty(params.listBranch) ? [] : params.listBranch;
    params.listRM = _.isEmpty(params.listRM) ? [] : params.listRM;
    this.leadService.searchLeadKHCN(params).subscribe(
      (data) => {
        this.selected = [];
        let listDataTemp = data?.content || [];
        const listIndustry = this.sessionService.getSessionData(SessionKey.LIST_INDUSTRY);

        if (listDataTemp?.length > 0) {
          listDataTemp.forEach((item) => {
            const itemSelected = _.find(listIndustry, (o) => o.key === item.industry);
            item.industryName = itemSelected ? `${itemSelected.key} - ${itemSelected.value}` : '';
            if (_.find(this.listSelected, (o) => o.customerCode === item.customerCode)) {
              this.selected.push(item);
            }

            // branchDisplay = branchCode - branchName
            const branchCode = item.branchCode ? item.branchCode : '';
            const branchName = item.branchName ? item.branchName : '';
            item.branchDisplay = branchCode + ' - ' + branchName;
            if (!item.branchCode && !item.branchName) {
              item.branchDisplay = '';
            }

            // rmDisplay = rmCode - rmName
            const rmCode = item.rmCode ? item.rmCode : '';
            const rmName = item.rmName ? item.rmName : '';
            item.rmDisplay = rmCode + ' - ' + rmName;
            if (!item.rmCode && !item.rmName) {
              item.rmDisplay = '';
            }

            // statusDisplay
            item.statusDisplay = _.get(_.find(this.commonDataKHCN.listStatus,(i) => i.code === item.status ), 'name');
            if (item.potentialLevel) {
              item.potentialLevelDisplay = _.get(_.find(this.commonDataKHCN.potentialLevelConfig,(i) => i.code === item.potentialLevel ), 'name');
            }
            if (item.customerResources) {
              item.customerResourcesDisplay = _.get(_.find(this.commonDataKHCN.potentialResourceConfig,(i) => i.code === item.customerResources ), 'name');
            }
          });
        }

        this.listData = listDataTemp;
        // console.log(this.listData);

        this.pageable = {
          size: this.paramSearch.size,
          totalElements: data?.totalElements,
          totalPages: data?.totalPages,
          currentPage: this.paramSearch.page,
        };
        this.checkAll = _.size(this.selected) === +this.pageable.size;
        this.isLoading = false;
        this.prevParamsKHCN = params;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  exportFileKHCN() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+(this.pageable?.totalElements || 0) === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.isLoading = true;
    this.leadService.exportFileLeadKHCN(this.prevParamsKHCN).subscribe(
      (fileId) => {
        if (!_.isEmpty(fileId)) {
          this.fileService
            .downloadFile(fileId, 'crm_lead_khcn.xlsx')
            .pipe(catchError((e) => of(false)))
            .subscribe((res) => {
              this.isLoading = false;
              if (!res) {
                this.messageService.error(this.notificationMessage.error);
              }
            });
        } else {
          this.messageService.error(this.notificationMessage?.export_error || '');
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(this.notificationMessage?.export_error || '');
        this.isLoading = false;
      }
    );
  }

  updateLeadKHCN(item) {
    this.router.navigate([this.router.url, 'update', item.id], {
      state: this.prevParamsKHCN,
      skipLocationChange: true,
      queryParams: {
        customerType: this.searchForm.controls.customerType.value,
      },
    });
  }

  deleteLeadKHCN(item) {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.leadService.deleteLeadKHCN(item.id).subscribe(
          () => {
            this.listData = this.listData?.filter((i) => i.id !== item.id);
            this.searchKHCN();
            this.messageService.success(this.notificationMessage.success);
          },
          (e) => {
            e.error = JSON.parse(e.error);
            this.isLoading = false;
            this.messageService.error(e.error?.messages?.vn);
          }
        );
      }
    });
  }

  handleChangePageSize(event) {
    this.paramSearch.page = 0;
    this.paramSearch.size = event;
    this.limit = event;
    this.search(false);
  }

  handleChangePageSizeKHCN(event) {
    this.paramSearch.page = 0;
    this.paramSearch.size = event;
    this.limit = event;
    this.searchKHCN(false);
  }

  get cityFormList() {
    return this.searchForm.getRawValue().city;
  }

  get districtFormList() {
    return this.searchForm.getRawValue().district;
  }

  get streetFormList() {
    return this.searchForm.getRawValue().street;
  }

  get customerType(): FormControl {
    return this.searchForm.get('customerType') as FormControl;
  }

  getLocation(name: string): void {

    const params = {
      isWard: name === 'street',
      locationCode: 'VN'
    };

    const data = this.searchForm.getRawValue();

    switch (name) {
      // case 'city':
      //   params.locationCode = data.city;
      //   break;
      case 'district':
        params.locationCode = data.listTownCiti.join(',');
        break;
      case 'street':
        params.locationCode = data.listDistrict.join(',');
        break;
      default: break;
    }

    this.service.getLocation(params).pipe(
      finalize(() => {

      })
    ).subscribe(res => {
      this[`${name}List`] = res;

    })
  }

  setValueChanges(): void {
    const district = this.searchForm.controls.listDistrict;
    const street = this.searchForm.controls.listWard;
    district.disable();
    street.disable();
    this.searchForm.controls.listTownCiti.valueChanges.subscribe(list => {
      district.setValue([]);
      street.setValue([]);
      if (list?.length) {
        district.enable();
        this.getLocation('district');
        return;
      }
    });

    this.searchForm.controls.listDistrict.valueChanges.subscribe(list => {
      const street = this.searchForm.controls.listWard;
      street.setValue([]);
      if (list?.length) {
        street.enable();
        this.getLocation('street');
        return;
      }
      street.disable();
    });

  }

  historyAssign(item: any) {
    const modal = this.modalService.open(LeadHistoryComponent, {
      windowClass: 'lead-history-assignment',
    });
    modal.componentInstance.leadCode = item?.leadCode;
    modal.componentInstance.fullName = item?.fullName;
  }


  /*Thêm mới KHTN từ danh bạ*/
  initDataCheckBox(item): boolean {
    return item.branchCode === null || item.branchCode === 'VN0010001'
  }

  save() {
    const data: any = {};
    data.note = '';
    data.branchCode = this.currUser.branch;
    data.rmCode = this.currUser.code;
    data.management = 'rm';
    data.typeCustomer = 'SME';
    data.isFile = false;
    data.listLeadCode = _.map(this.listSelected, (item) => item.leadCode);
    if(_.isEmpty(data.listLeadCode)){
      return this.messageService.warn('Vui lòng chọn khách hàng');
    }
    if(_.lt(+this.limitKHTN, +data.listLeadCode.length)){
      this.translate.get('notificationMessage.LIMIT_KHTN', { number: this.limitKHTN }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.checkResultFilter(data.listLeadCode).subscribe(
          (result) => {
            if (result.code === '200') {
              this.service.assignToBranchOrRm(data).subscribe(
                () => {
                  this.messageService.success(this.notificationMessage.success);
                  this.search(true);
                  this.isLoading = false;
                },
                (e) => {
                  this.isLoading = false;
                  this.messageService.error(e?.error?.description);
                }
              );
            } else {
              this.isLoading = false;
              this.confirmService.error(result.message);
            }
          },
          () => {
            this.isLoading = false;
            this.messageService.error(this.notificationMessage.E001);
          }
        );
      }
    });
  }
}
