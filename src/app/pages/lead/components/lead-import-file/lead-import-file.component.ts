import { global } from '@angular/compiler/src/util';
import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { BaseComponent } from 'src/app/core/components/base.component';
import _ from 'lodash';
import { FunctionCode, functionUri, typeExcel, CommonCategory } from 'src/app/core/utils/common-constants';
import { FileService } from 'src/app/core/services/file.service';
import { CustomerLeadService } from '../../service/customer-lead.service';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { catchError } from 'rxjs/operators';
import {  forkJoin, of } from 'rxjs';

@Component({
  templateUrl: './lead-import-file.component.html',
  styleUrls: ['./lead-import-file.component.scss'],
})
export class LeadImportComponent extends BaseComponent implements OnInit {
  isLoading = false;
  fileName: string;
  isFile = false;
  fileImport: File;
  files: any;
  ColumnMode = ColumnMode;
  listDataSuccess = [];
  listDataError = [];
  prop: any;
  limit = global.userConfig.pageSize;
  pageSuccess: Pageable;
  pageError: Pageable;
  paramSuccess = {
    size: this.limit,
    page: 0,
    search: '',
    status: true,
    requestId: '',
  };
  textSearchSuccess = '';
  paramError = {
    size: this.limit,
    page: 0,
    search: '',
    status: false,
    requestId: '',
  };
  textSearchError = '';
  fileId: string;
  maxExportExcel:number;
  isUpload = false;
  isBusiness = true;
  searchSuccessDone = true;
  searchErrorDone = true;
  messages = global.messageTable;
  @ViewChild('tableSuccess') tableSuccess: DatatableComponent;
  @ViewChild('tableError') tableError: DatatableComponent;

  constructor(injector: Injector, 
    private service: CustomerLeadService, 
    private categoryService: CategoryService,
    private fileService: FileService) {
    super(injector);
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.CUSTOMER_360_OPERATION_BLOCK_MANAGER}`
    );
  }

  ngOnInit(
    
  ): void {
    let defaultExportExcel = 5000;
    this.categoryService.getCommonCategory(CommonCategory.CONFIG_EXPORT_CUSTOMER_LIMIT_COUNT).subscribe((res) => {
      this.maxExportExcel = (
        res?.content?.find((item) => item.code === CommonCategory.IMPORT_LEAD)?.value || defaultExportExcel
      );
      console.log('maxExportExcel',this.maxExportExcel)
    });
    
  }

  searchSuccess(isSearch: boolean) {
    this.isLoading = true;
    this.searchSuccessDone = false;
    if (isSearch) {
      this.paramSuccess.page = 0;
      this.textSearchSuccess = this.textSearchSuccess.trim();
      this.paramSuccess.search = this.textSearchSuccess;
    }
    this.service.searchDataImport(this.paramSuccess).subscribe(
      (listData) => {
        this.listDataSuccess = listData.content || [];
        this.pageSuccess = {
          totalElements: listData.totalElements,
          totalPages: listData.totalPages,
          currentPage: listData.number,
          size: this.limit,
        };
        this.searchSuccessDone = true;
        if (this.searchErrorDone && this.searchSuccessDone) {
          this.isLoading = false;
        }
      },
      () => {
        this.searchSuccessDone = true;
        if (this.searchErrorDone && this.searchSuccessDone) {
          this.isLoading = false;
        }
      }
    );
  }

  searchError(isSearch: boolean) {
    this.isLoading = true;
    this.searchErrorDone = false;
    if (isSearch) {
      this.paramError.page = 0;
      this.textSearchError = this.textSearchError.trim();
      this.paramError.search = this.textSearchError;
    }
    this.service.searchDataImport(this.paramError).subscribe(
      (listData) => {
        this.listDataError = listData.content || [];
        this.pageError = {
          totalElements: listData.totalElements,
          totalPages: listData.totalPages,
          currentPage: listData.number,
          size: this.limit,
        };
        this.searchErrorDone = true;
        if (this.searchErrorDone && this.searchSuccessDone) {
          this.isLoading = false;
        }
      },
      () => {
        this.searchErrorDone = true;
        if (this.searchErrorDone && this.searchSuccessDone) {
          this.isLoading = false;
        }
      }
    );
  }

  setPage(pageInfo, type) {
    if (type === 'success') {
      this.paramSuccess.page = pageInfo.offset;
      this.searchSuccess(false);
    } else {
      this.paramError.page = pageInfo.offset;
      this.searchError(false);
    }
  }

  handleFileInput(files) {
    if (files && files.length > 0) {
      if (!typeExcel.includes(files?.item(0)?.type)) {
        this.messageService.error(this.notificationMessage.CANNOT_READ_DATA_FROM_FILE_KHTN);
        return;
      }
      if (files?.item(0)?.size > 10485760) {
        this.messageService.warn(this.notificationMessage.ECRM005);
        return;
      }
      this.isFile = true;
      this.fileImport = files.item(0);
      this.fileName = files.item(0).name;
    } else {
      this.isFile = false;
    }
  }

  clearFile() {
    this.isUpload = false;
    this.isFile = false;
    this.fileName = null;
    this.fileImport = null;
    this.files = null;
    this.listDataSuccess = [];
    this.listDataError = [];
    this.fileId = undefined;
    this.pageError = undefined;
    this.pageSuccess = undefined;
    this.textSearchSuccess = '';
    this.textSearchError = '';
    this.paramError.search = '';
    this.paramSuccess.search = '';
  }

  importFile() {
    if (this.isFile && !this.isUpload) {
      this.isLoading = true;
      const formData: FormData = new FormData();
      formData.append('file', this.fileImport);
      this.service.importFile(formData, this.isBusiness).subscribe(
        (fileId) => {
          this.fileId = fileId;
          this.paramError.requestId = this.fileId;
          this.paramSuccess.requestId = this.fileId;
          this.checkImportSuccess(this.fileId);
        },
        (e) => {
          const error = JSON.parse(e.error);
          if (error?.description) {
            this.messageService.error(error?.description);
          } else {
            this.messageService.error(this.notificationMessage.error);
          }
          this.listDataError = [];
          this.listDataSuccess = [];
          this.isLoading = false;
        }
      );
    }
  }

  save() {
    if ((this.isLoading && !this.fileId) || this.pageSuccess.totalElements === 0) {
      return;
    }
    this.isLoading = true;
    this.service.writeDataFile(this.fileId).subscribe(
      () => {
        this.messageService.success(this.notificationMessage.success);
        this.isLoading = false;
        this.fileId = undefined;
        this.listDataSuccess = [];
        this.listDataError = [];
        this.back();
      },
      (e) => {
        if (e?.error) {
          this.messageService.warn(e?.error?.description);
        } else {
          this.messageService.error(this.notificationMessage.error);
        }
        this.isLoading = false;
      }
    );
  }

  back() {
    this.router.navigateByUrl(functionUri.lead_management, { state: this.prop ? this.prop : this.state });
  }

  checkImportSuccess(fileId: string) {
    const interval = setInterval(() => {
      this.service.checkFileImport(fileId).subscribe((res) => {
        if (res?.status === 'COMPLETE') {
          this.isUpload = true;
          this.isLoading = false;
          this.searchError(true);
          this.searchSuccess(true);
          clearInterval(interval);
        } else if (res?.status === 'FAIL') {
          console.log('res?.msgError : ',res?.msgError)
          if (res?.msgError?.includes('FILE_DOES_NOT_EXCEED_RECORDS')) {
            const maxRecord = res?.msgError?.replace('FILE_DOES_NOT_EXCEED_RECORDS_', '');
            const type = this.fileName?.split('.')[this.fileName?.split('.')?.length - 1];
            this.translate
              .get('notificationMessage.FILE_DOES_NOT_EXCEED_RECORDS', { number: maxRecord, type })
              .subscribe((res) => {
                this.messageService.error(res);
              });
          } else if (res?.msgError === 'FILE_NO_CONTENT_EXCEPTION') {
            this.messageService.error(this.notificationMessage.FILE_NO_CONTENT_EXCEPTION);
          } else {
            this.messageService.error(this.notificationMessage.CANNOT_READ_DATA_FROM_FILE);
          }
          this.isLoading = false;
          clearInterval(interval);
        }
      });
    }, 5000);
  }

  downloadTemplate() {
    this.isLoading = true;
    this.service.downloadTemplate().subscribe(
      (fileId) => {
        this.fileService.downloadFile(fileId, 'template-leads.xlsx').subscribe(
          (res) => {
            this.isLoading = false;
            if (!res) {
              this.messageService.error(this.notificationMessage.error);
            }
          },
          () => {
            this.isLoading = false;
          }
        );
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }
  downloadResultImport(result){
    var totalElements = 0;
    var fileName = "";
    if(result){
      totalElements = this.pageSuccess?.totalElements;
      fileName = 'list_import_succcess.xlsx';
    }
    else{
      totalElements = this.pageError?.totalElements  
      fileName = 'list_import_errors.xlsx';
    }
    if (!this.maxExportExcel) {
      return;
    }
    if (+(totalElements || 0) === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    if (_.lt(+this.maxExportExcel, totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.isLoading = true;
    this.service.exportFileResult(this.fileId,result).subscribe(
      (fileId) => {
        if (!_.isEmpty(fileId)) {
          this.fileService
            .downloadFile(fileId, fileName)
            .pipe(catchError((e) => of(false)))
            .subscribe((res) => {
              this.isLoading = false;
              if (!res) {
                this.messageService.error(this.notificationMessage.error);
              }
            });
        } else {
          this.messageService.error(this.notificationMessage?.export_error || '');
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(this.notificationMessage?.export_error || '');
        this.isLoading = false;
      }
    );
    
  }
}
