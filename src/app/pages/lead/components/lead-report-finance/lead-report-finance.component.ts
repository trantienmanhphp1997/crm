import { Component, OnInit, Injector, Input } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { CustomerLeadService } from '../../service/customer-lead.service';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { formatNumber } from '@angular/common';

@Component({
    selector: 'lead-report-finance',
    templateUrl: './lead-report-finance.component.html',
    styleUrls: ['./lead-report-finance.component.scss'],
    // encapsulation: ViewEncapsulation.None,
})
export class LeadReportFinanceComponent extends BaseComponent implements OnInit {
    @Input() taxCode: string;
    listData = [];
    columns = [];
    tabs = ['Kết quả kinh doanh', 'Tài sản, nguồn vốn', 'Lưu chuyển tiền tệ trực tiếp',"Lưu chuyển tiền tệ gián tiếp"];
    tabIndex = 0;
    growth = 0;
    etlDate = null;
    tab0 = [
        {
            name: this.fields?.dtbhvccdv,
            code: 'dtbhvccdv',
        },
        {
            name: this.fields?.dttbhvccdv,
            code: 'dttbhvccdv',
        },
        {
            name: this.fields?.lng,
            code: 'lng',
        },
        {
            name: this.fields?.hdkd,
            code: 'hdkd',
        },
        {
            name: this.fields?.lntt,
            code: 'lntt',
        },
        {
            name: this.fields?.lnst,
            code: 'lnst',
        },
        {
            name: this.fields?.cphdtc,
            code: 'cphdtc',
        },
        {
            name: this.fields?.dthdtc,
            code: 'dthdtc',
        },
        {
            name: this.fields.dsxnk,
            code: 'dsxnk',
        },
    ];
    tab1 = [
        {
            name: this.fields?.tongTaiSan,
            code: 'tongTaiSan',
        },
        {
            name: this.fields?.ttsnh,
            code: 'ttsnh',
        },
        {
            name: this.fields?.ttsdh,
            code: 'ttsdh',
        },
        {
            name: this.fields?.vcsh,
            code: 'vcsh',
        },
    ];
    tab2 = [
        {
            name: this.fields?.lctsxkdtt,
            code: 'lctsxkdtt',
        },
        {
            name: this.fields?.lctdttt,
            code: 'lctdttt',
        },
        {
            name: this.fields?.lcttctt,
            code: 'lcttctt',
        },
    ];
    tab3 = [
        {
            name: this.fields?.lctsxkdtt,
            code: 'lctsxkdgt',
        },
        {
            name: this.fields?.lctdttt,
            code: 'lctdtgt',
        },
        {
            name: this.fields?.lcttctt,
            code: 'lcttcgt',
        },
    ];
    data: any[];

    constructor(
        injector: Injector,
        private service: CustomerLeadService
    ) {
        super(injector);
        this.isLoading = true;
    }

    ngOnInit(): void {
        this.service
            .getReportFinanceByTaxCode(this.taxCode)
            .pipe(catchError(() => of([])))
            .subscribe((data) => {
                this.data = _.orderBy(data, 'years');
                this.growth = _.get(this.data, '[3].grow');
                let lastValue = _.get(this.data, '[3].data');
                if(lastValue)
                    this.etlDate = lastValue.etlDt;
                _.forEach(this.data, (item) => {
                    this.columns.push({
                        name: `Năm ${item?.years}`,
                        prop: `year${item?.years}`,
                        sortable: false,
                        draggable: false,
                        year: `${item?.years}`,
                    });
                });
                this.columns.unshift({
                    name: 'Chỉ tiêu',
                    prop: 'label',
                    sortable: false,
                    draggable: false,
                });
                this.onChangeTab({ index: 0 });
                this.isLoading = false;
            },() => {
                this.isLoading = false;
                this.messageService.error(this.notificationMessage.error);
            });
    }

    onChangeTab(event) {
        this.tabIndex = _.get(event, 'index');
        this.listData = [];
        const list: any[] = this[`tab${this.tabIndex}`];
        let countRowEmpty = 0;
        _.forEach(list, (itemRow) => {
            if (!this.checkRowNotEmpty(this.data, itemRow.code)) {
                countRowEmpty += 1;
            }
            this.listData.push({
                label: itemRow.name,
                [`year${_.get(this.data, '[0].years')}`]: this.getCostValue(
                    _.get(this.data, '[0].data'),
                    itemRow.code
                ),
                [`year${_.get(this.data, '[1].years')}`]: this.getCostValue(
                    _.get(this.data, '[1].data'),
                    itemRow.code
                ),
                [`year${_.get(this.data, '[2].years')}`]: this.getCostValue(
                    _.get(this.data, '[2].data'),
                    itemRow.code
                ),
                [`year${_.get(this.data, '[3].years')}`]: this.getCostValue(
                    _.get(this.data, '[3].data'),
                    itemRow.code
                ),
            });
        });
        if (countRowEmpty === _.size(this.listData)) {
            this.listData = [];
        }
    }
    checkRowNotEmpty(data, key) {
        return (
            _.isNumber(_.get(data, `[0].data.${key}`)) ||
            _.isNumber(_.get(data, `[1].data.${key}`)) ||
            _.isNumber(_.get(data, `[2].data.${key}`)) ||
            _.isNumber(_.get(data, `[3].data.${key}`))
        );
    }
    getCostValue(data, key) {
        // console.log("data : ",data)
        return _.isNumber(_.get(data, key)) ? formatNumber(_.get(data, key), 'en', '1.0-2') : '---';
    }

}