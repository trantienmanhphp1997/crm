import { Component, HostBinding, Injector, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { MenuItem } from 'primeng/api';
import { CustomerSaleLeadService } from '../../service/customer-sale-lead.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-location', templateUrl: './location.component.html', styleUrls: ['./location.component.scss']
})
export class LocationComponent extends BaseComponent implements OnInit {
  isLoading = false;
  @HostBinding('class.app__right-content') appRightContent = true;
  @Input() dataSearch: any;
  locations = [];
  params = {
    isWard: false, locationCode: 'VN'
  };
  step = 0;
  chooseLocation = {
    city: {
      name: null, key: null
    }, district: {
      name: null, key: null
    }, street: {
      name: null, key: null
    }
  };
  filterList = [];

  items: MenuItem[];
  activeIndex = 0;
  form = this.fb.group({
    search: null
  });

  constructor(injector: Injector, private categoryService: CategoryService, private modalActive: NgbActiveModal, private saleLead: CustomerSaleLeadService) {
    super(injector);
  }

  ngOnInit() {
    this.items = [{ label: 'Tỉnh thành phố' }, { label: 'Quận/huyện' }, { label: 'Phường/xã' }];
    this.search();
  }


  closeModal() {
    this.modalActive.close(false);
  }

  search() {
    this.isLoading = true;
    this.saleLead.searchLocation(this.params).pipe(finalize(() => {
      this.isLoading = false;
    })).subscribe(value => {
      this.locations = value;
      this.filterList = value;
    });
  }

  filerLocation(event, item) {

    this.configNextStep(item, this.activeIndex);
    this.activeIndex = this.activeIndex + 1;
    this.search();
  }

  backLocation() {
    if (this.activeIndex === 0) return;
    const stepBack = this.activeIndex - 2 < 0 ? 1 : 2;
    const key = Object.keys(this.chooseLocation)[this.activeIndex - stepBack];
    const preLocation = this.chooseLocation[key];
    const preStep = this.activeIndex - 1;

    this.configBackStep(preLocation, this.activeIndex - 1);
    this.activeIndex = preStep;
    this.search();
  }

  configNextStep(item, step): void {
    // step 1
    if (step === 0) {
      this.form.controls.search.setValue('');
      this.chooseLocation.city = { name: item.value, key: item.key };
    }
    // step 2
    if (step === 1) {
      this.form.controls.search.setValue('');
      this.params.isWard = true;
      this.chooseLocation.district = { name: item.value, key: item.key };
    }
    this.params.locationCode = item.key;
    // step 3
    if (step === 2) {
      this.form.controls.search.setValue('');
      this.chooseLocation.street = { name: item.value, key: item.key };
      this.modalActive.close(this.chooseLocation);
      return;
    }
  }

  configBackStep(item, step): void {
    const obj = {
      name: null, key: null
    };
    switch (step) {
      case 0:
        this.chooseLocation = {
          city: obj, district: obj, street: obj
        };
        break;
      case 1:
        this.chooseLocation.district = obj;
        break;
      default:
        break;
    }
    this.params = {
      locationCode: step === 0 ? 'VN' : item.key, isWard: step === 2
    };
  }

  searchLocation() {
    this.filterList = this.locations.filter((obj) => {
      return obj.value.trim().toLowerCase().includes(this.form.controls.search.value.trim().toLowerCase());
    });
  }
}
