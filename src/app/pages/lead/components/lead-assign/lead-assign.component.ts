import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';
import { LeadManagementComponent } from '../lead-management/lead-management.component';
import { global } from '@angular/compiler/src/util';
import { Division, FunctionCode, maxInt32, Scopes, typeExcel } from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { RmApi, RmBlockApi } from 'src/app/pages/rm/apis';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { CustomerLeadService } from '../../service/customer-lead.service';
import { AppFunction } from 'src/app/core/interfaces/app-function.interface';

@Component({
  templateUrl: './lead-assign.component.html',
  styleUrls: ['./lead-assign.component.scss'],
})
export class LeadAssignComponent extends BaseComponent implements OnInit {
  commonData = {
    listBranch: [],
    listRm: [],
  };
  listLead = [];
  listLeadTerm = [];
  pageable: Pageable = {
    currentPage: 0,
    size: global?.userConfig?.pageSize,
    totalElements: 0,
    totalPages: 0,
  };
  form = {
    note: '',
    branchCode: '',
    rmCode: '',
    management: '',
    isFile: false,
  };
  startAssign = false;
  title = 'Gán KH tiềm năng cho đơn vị / RM';
  fileId: string;
  isUpload = false;
  isBusiness = true;
  fileName: string;
  isFile = false;
  fileImport: File;
  files: any;
  listDataSuccess = [];
  listDataError = [];
  pageSuccess: Pageable;
  pageError: Pageable;
  paramSuccess = {
    size: global?.userConfig?.pageSize,
    page: 0,
    status: true,
    requestId: '',
    management: '',
  };
  paramError = {
    size: global?.userConfig?.pageSize,
    page: 0,
    status: false,
    requestId: '',
    management: '',
  };
  objFunctionRm: AppFunction;
  listLeadType: string[] = [];

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private rmApi: RmApi,
    private service: CustomerLeadService,
    private rmBlockApi: RmBlockApi
  ) {
    super(injector);
    this.objFunctionRm = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
  }

  ngOnInit() {}

  setPage(type: string, pageInfo?) {
    if (type === 'selected') {
      if (pageInfo) {
        this.pageable.currentPage = pageInfo.offset;
      }
      const start = this.pageable.currentPage * this.pageable.size;
      this.pageable.totalElements = this.listLeadTerm.length;
      this.pageable.totalPages = Math.floor(this.pageable.totalElements / this.pageable.size);
      this.listLead = _.slice(this.listLeadTerm, start, start + this.pageable.size);
    } else if (type === 'success') {
      this.paramSuccess.page = pageInfo.offset;
      this.searchSuccess(false);
    } else {
      this.paramError.page = pageInfo.offset;
      this.searchError(false);
    }
  }

  deleteLead(row) {
    _.remove(this.listLeadTerm, (item) => item.leadCode === row.leadCode);
    this.setPage('selected');
  }

  start() {
    if (_.isEmpty(this.form.management)) {
      return;
    } else if (this.form.management === 'rm') {
      this.title = 'Gán KH tiềm năng cho RM';
    } else if (this.form.management === 'branch') {
      this.title = 'Gán KH tiềm năng cho đơn vị';
    }

    this.isLoading = true;
    if (this.form.management === 'branch') {
      this.categoryService
        .getBranchesOfUser(this.objFunctionRm?.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined)))
        .subscribe((branches) => {
          this.commonData.listBranch = _.map(branches, (item) => {
            return { code: item.code, name: `${item.code} - ${item.name}` };
          });
          if (!_.find(this.commonData.listBranch, (item) => item.code === this.currUser?.branch)) {
            this.commonData.listBranch.push({
              code: this.currUser.branch,
              name: `${this.currUser.branch} - ${this.currUser.branchName}`,
            });
          }
          this.isLoading = false;
          this.startAssign = true;
        });
    } else {
      this.rmApi
        .post('findAll', {
          page: 0,
          size: maxInt32,
          crmIsActive: true,
          isAssignRm: true,
          rsId: this.objFunctionRm?.rsId,
          scope: Scopes.VIEW,
          statusWork: 'NTS',
          typeAssignKHTN: 'ALL',
        })
        .pipe(catchError(() => of(undefined)))
        .subscribe((data) => {
          const listRm = [];
          data?.content?.forEach((item) => {
            if (!_.isEmpty(item?.t24Employee?.employeeCode)) {
              listRm.push({
                code: _.trim(item?.t24Employee?.employeeCode),
                name: _.trim(item?.t24Employee?.employeeCode) + ' - ' + _.trim(item?.hrisEmployee?.fullName),
                hrsCode: _.trim(item?.hrisEmployee?.employeeId),
              });
              if (!listRm?.find((item) => item.code === this.currUser?.code)) {
                listRm.push({
                  code: this.currUser?.code,
                  name: _.trim(this.currUser?.code) + ' - ' + _.trim(this.currUser?.fullName),
                  branchCode: this.currUser?.branch,
                });
              }
            }
          });
          this.commonData.listRm = listRm;
          this.isLoading = false;
          this.startAssign = true;
        });
    }
  }

  selectLead() {
    if (_.isEmpty(this.form.rmCode) && this.form.management === 'rm') {
      return this.messageService.warn('Vui lòng chọn RM');
    }
    const dataSearch: any = {};
    if (this.listLeadType.length === 1) {
      dataSearch.customerType = { value: this.listLeadType[0] === Division.INDIV ? '0' : '1', disabled: true };
    }
    const modal = this.modalService.open(LeadManagementComponent, { windowClass: 'lead-management-view' });
    modal.componentInstance.isModal = true;
    modal.componentInstance.isCheck = true;
    modal.componentInstance.listSelected = this.listLeadTerm;
    modal.componentInstance.modalActive = modal;
    modal.componentInstance.dataSearch = dataSearch;
    modal.result
      .then((listLead) => {
        if (listLead) {
          this.listLeadTerm = listLead;
          this.pageable.totalElements = _.size(listLead);
          this.setPage('selected');
        }
      })
      .catch(() => {});
  }

  save() {
    if (this.form.management === 'rm' && _.isEmpty(this.form.rmCode)) {
      return this.messageService.warn('Vui lòng chọn RM');
    } else if (this.form.management === 'branch' && _.isEmpty(this.form.branchCode)) {
      return this.messageService.warn('Vui lòng chọn đơn vị');
    }
    if (this.listLeadTerm.length === 0) {
      return this.messageService.warn('Vui lòng chọn khách hàng đầu mối.');
    }
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        let data: any = { ...this.form };
        data.listLeadCode = _.map(this.listLeadTerm, (item) => item.leadCode);
        this.service.checkResultFilter(data.listLeadCode).subscribe(
          (result) => {
            if (result.code === '200') {
              this.service.assignToBranchOrRm(data).subscribe(
                () => {
                  this.isLoading = false;
                  this.messageService.success(this.notificationMessage.success);
                  this.cancel();
                },
                (e) => {
                  this.isLoading = false;
                  this.messageService.error(e?.error?.description);
                }
              );
            } else {
              this.isLoading = false;
              this.confirmService.error(result.message);
            }
          },
          () => {
            this.isLoading = false;
            this.messageService.error(this.notificationMessage.E001);
          }
        );
      }
    });
  }

  searchSuccess(isSearch: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.paramSuccess.page = 0;
    }
    this.paramSuccess.management = this.form.management;
    this.service.searchDataImport(this.paramSuccess).subscribe(
      (listData) => {
        this.listDataSuccess = listData.content || [];
        this.pageSuccess = {
          totalElements: listData.totalElements,
          totalPages: listData.totalPages,
          currentPage: listData.number,
          size: global?.userConfig?.pageSize,
        };
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  searchError(isSearch: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.paramError.page = 0;
    }
    this.paramError.management = this.form.management;
    this.service.searchDataImport(this.paramError).subscribe(
      (listData) => {
        this.listDataError = listData.content || [];
        this.pageError = {
          totalElements: listData.totalElements,
          totalPages: listData.totalPages,
          currentPage: listData.number,
          size: global?.userConfig?.pageSize,
        };

        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  handleFileInput(files) {
    if (files && files.length > 0) {
      if (!typeExcel.includes(files.item(0).type)) {
        this.messageService.error(this.notificationMessage.CANNOT_READ_DATA_FROM_FILE);
        return;
      }
      if (files.item(0).size > 10485760) {
        this.messageService.warn(this.notificationMessage.ECRM005);
        return;
      }
      this.isFile = true;
      this.fileImport = files.item(0);
      this.fileName = files.item(0).name;
    } else {
      this.isFile = false;
    }
  }

  clearFile() {
    this.isUpload = false;
    this.isFile = false;
    this.fileName = null;
    this.fileImport = null;
    this.files = null;
    this.listDataSuccess = [];
    this.listDataError = [];
    this.fileId = undefined;
    this.pageError = undefined;
    this.pageSuccess = undefined;
  }

  importFile() {
    if (this.isFile && !this.isUpload) {
      this.isLoading = true;
      const formData: FormData = new FormData();
      formData.append('file', this.fileImport);
      const params: any = {
        management: this.form.management,
        rsId: this.objFunctionRm?.rsId,
        scope: Scopes.VIEW,
      };
      this.service.importFileAssignToBranchOrRM(formData, params).subscribe(
        (fileId) => {
          this.fileId = fileId;
          this.paramError.requestId = this.fileId;
          this.paramSuccess.requestId = this.fileId;
          this.checkImportSuccess(this.fileId);
        },
        (e) => {
          const error = JSON.parse(e.error);
          if (error?.description) {
            this.messageService.error(error?.description);
          } else {
            this.messageService.error(this.notificationMessage.error);
          }
          this.listDataError = [];
          this.listDataSuccess = [];
          this.isLoading = false;
        }
      );
    }
  }

  checkImportSuccess(fileId: string) {
    const interval = setInterval(() => {
      this.service.checkFileImport(fileId).subscribe((res) => {
        if (res?.status === 'COMPLETE') {
          this.isUpload = true;
          this.isLoading = false;
          this.searchError(true);
          this.searchSuccess(true);
          clearInterval(interval);
        } else if (res?.status === 'FAIL') {
          if (res?.msgError?.includes('FILE_DOES_NOT_EXCEED_RECORDS')) {
            const maxRecord = res?.msgError?.replace('FILE_DOES_NOT_EXCEED_RECORDS_', '');
            const type = this.fileName?.split('.')[this.fileName?.split('.')?.length - 1];
            this.translate
              .get('notificationMessage.FILE_DOES_NOT_EXCEED_RECORDS', { number: maxRecord, type })
              .subscribe((res) => {
                this.messageService.error(res);
              });
          } else if (res?.msgError === 'FILE_NO_CONTENT_EXCEPTION') {
            this.messageService.error(this.notificationMessage.FILE_NO_CONTENT_EXCEPTION);
          } else {
            this.messageService.error(this.notificationMessage.CANNOT_READ_DATA_FROM_FILE);
          }
          this.isLoading = false;
          clearInterval(interval);
        }
      });
    }, 5000);
  }

  saveDataFile() {
    if ((this.isLoading && !this.fileId) || this.pageSuccess.totalElements === 0 || this.pageError.totalElements > 0) {
      return;
    }
    this.isLoading = true;
    const params = {
      type: this.form.management,
      rsId: this.objFunctionRm?.rsId,
      scope: Scopes.VIEW,
      requestId: this.fileId,
    };
    this.service.writeDataFileAssign(params).subscribe(
      () => {
        this.messageService.success(this.notificationMessage.success);
        this.isLoading = false;
        this.fileId = undefined;
        this.listDataSuccess = [];
        this.listDataError = [];
        this.cancel();
      },
      (e) => {
        if (e?.error) {
          this.messageService.warn(e?.error?.description);
        } else {
          this.messageService.error(this.notificationMessage.error);
        }
        this.isLoading = false;
      }
    );
  }

  downloadTemplate() {
    if (this.form.management === 'rm') {
      window.open('/assets/template/lead_phan_giao_cho_rm.xlsx', '_self');
    } else {
      window.open('/assets/template/lead_phan_giao_don_vi.xlsx', '_self');
    }
  }

  changeRM(event) {
    this.listLeadType = [];
    const item = this.commonData.listRm?.find((item) => item.code === event.value);
    this.isLoading = true;
    const params = {
      isActive: true,
      hrsCode: item?.hrsCode,
      page: 0,
      size: maxInt32,
    };
    this.rmBlockApi.fetch(params).subscribe(
      (listBlocks) => {
        if (_.find(listBlocks?.content, (item) => item.blockCode === Division.INDIV)) {
          this.listLeadType.push(Division.INDIV);
        }
        if (_.find(listBlocks?.content, (item) => item.blockCode === Division.SME || item.blockCode === Division.CIB)) {
          this.listLeadType.push(Division.SME);
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  cancel() {
    this.startAssign = false;
    this.form = {
      note: '',
      branchCode: '',
      rmCode: '',
      management: '',
      isFile: false,
    };
    this.pageable = {
      currentPage: 0,
      size: global?.userConfig?.pageSize,
      totalElements: 0,
      totalPages: 0,
    };
    this.listLeadTerm = [];
    this.listLead = [];
    this.isBusiness = true;
    this.clearFile();
    this.title = 'Gán KH tiềm năng cho đơn vị / RM';
    this.title = 'Gán KH tiềm năng cho đơn vị / RM';
  }
}
