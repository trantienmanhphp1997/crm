import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGuardService } from 'src/app/core/services/role-guard.service';
import { FunctionCode, Scopes, ScreenType } from 'src/app/core/utils/common-constants';
import {
  LeadManagementComponent,
  LeadActionsComponent,
  LeadDetailsComponent,
  LeadAssignComponent,
  LeadImportComponent,
} from './components';

const routes: Routes = [
  // {
  //   path: '',
  //   component: CustomerLeftViewComponent,
  //   outlet: 'app-left-content',
  // },
  {
    path: '',
    children: [
      {
        path: 'lead-management',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.LEAD,
        },
        children: [
          {
            path: '',
            component: LeadManagementComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
          {
            path: 'detail/:code',
            component: LeadDetailsComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
          {
            path: 'create',
            component: LeadActionsComponent,
            canActivate: [],
            data: {
              scope: Scopes.CREATE,
              screenType: ScreenType.Create,
            },
          },
          {
            path: 'update/:code',
            component: LeadActionsComponent,
            canActivate: [],
            data: {
              scope: Scopes.UPDATE,
              screenType: ScreenType.Update,
            },
          },
          {
            path: 'importFile',
            component: LeadImportComponent,
            canActivate: [],
            data: {
              scope: Scopes.CREATE,
            },
          },
        ],
      },
      {
        path: 'lead-assignment',
        canActivate: [RoleGuardService],
        component: LeadAssignComponent,
        data: {
          code: FunctionCode.ASSIGN_LEAD,
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LeadRoutingModule {}
