import { Injectable } from '@angular/core';
import { HttpWrapper } from '../../../core/apis/http-wapper';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomerSaleLeadService extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_sale}`);
  }

  searchLocation(params): Observable<any> {
    return this.get('onboarding-account/town-district', params);
  }
}
