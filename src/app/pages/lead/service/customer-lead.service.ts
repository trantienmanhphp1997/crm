import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root',
})
export class CustomerLeadService {
  baseUrl = `${environment.url_endpoint_customer_sme}/leads`;
  constructor(private http: HttpClient) {}

  searchLead(params): Observable<any> {
    return this.http.post(`${this.baseUrl}/search?page=${params.page}&size=${params.size}`, params);
  }

  searchLeadByRm(params): Observable<any> {
    return this.http.post(`${this.baseUrl}/searchByRM?page=${params.page}&size=${params.size}`, params);
  }

  exportFileLead(params): Observable<any> {
    return this.http.post(`${this.baseUrl}/export`, params, { responseType: 'text' });
  }

  exportFileLeadByRm(params): Observable<any> {
    return this.http.post(`${this.baseUrl}/exportByRM`, params, { responseType: 'text' });
  }

  getLeadByCode(code: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/${code}`);
  }

  createLead(data): Observable<any> {
    return this.http.post(`${this.baseUrl}`, data, { responseType: 'text' });
  }

  updateLead(data, code: string): Observable<any> {
    return this.http.put(`${this.baseUrl}/${code}`, data);
  }

  deleteLead(code: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${code}`);
  }

  getLeadCode(): Observable<any> {
    return this.http.get(`${this.baseUrl}/getSequence`, { responseType: 'text' });
  }

  updateInfoExtend(data: any): Observable<any> {
    return this.http.put(`${this.baseUrl}/updateDetails`, data);
    // return this.http.put(`http://localhost:10186/crm19-customer-org/leads/updateDetails`, data);
  }

  downloadTemplate() {
    return this.http.get(`${this.baseUrl}/template`, { responseType: 'text' });
  }

  importFile(data: FormData, status: boolean): Observable<any> {
    return this.http.post(`${this.baseUrl}/import?status=${status}`, data, { responseType: 'text' });
  }

  importFileAssignToBranchOrRM(data: FormData, params: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/import-${params.management}`, data, { responseType: 'text', params });
  }

  checkFileImport(requestId: string): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_customer}/operation-block-management/checkProcessImport?fileId=${requestId}`
    );
  }

  writeDataFile(requestId: string): Observable<any> {
    return this.http.post(`${this.baseUrl}/save-data-import?requestId=${requestId}`, {});
  }

  writeDataFileAssign(params): Observable<any> {
    return this.http.put(`${this.baseUrl}/assign-to-${params.type}-by-file`, {}, { params });
  }

  searchDataImport(params): Observable<any> {
    return this.http.get(`${this.baseUrl}/data-import${_.has(params, 'management') ? '-' + params.management : ''}`, {
      params,
    });
  }
  exportFileResult(fileId,result): Observable<any> {
    return this.http.get(`${this.baseUrl}/export-data-import?status=${result}&requestId=${fileId}`, { responseType: 'text' });
  }

  assignToBranchOrRm(data): Observable<any> {
    return this.http.put(`${this.baseUrl}/assign-to-${data?.management}`, data);
  }

  getResultFilterByLeadCode(leadCode: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/filter?leadCode=${leadCode}`);
  }

  checkResultFilter(leadCodes: string[]): Observable<any> {
    return this.http.post(`${this.baseUrl}/filter-update`, leadCodes);
  }

  getLeadCustomerByNotiId(params): Observable<any> {
    return this.http.get(`${this.baseUrl}/get-list-lead-customer-by-noti`, {params});
  }

  extendTimeSla(data): Observable<any> {
    return this.http.post(`${this.baseUrl}/extend-time`, data,{
      responseType: 'text',
    });
  }

  getActivityLogByCusCode(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/customer-activity-log/find-all`,{
      params,
    });
  }

  getAllMail(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/leads/find-all-mail2`,data);
  }
  
  getLocation(params: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/onboarding-account/town-district`, {params});
  }

  searchHistory(params): Observable<any> {
    return this.http.get(
      `${this.baseUrl}/lead-assignment-history`,
      { params }
    );
  }

  getReportFinanceByTaxCode(taxCode: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/finance-report?taxCode=${taxCode}`);
  }
}
