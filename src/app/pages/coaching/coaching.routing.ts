import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGuardService } from 'src/app/core/services/role-guard.service';
import { FunctionCode, Scopes } from 'src/app/core/utils/common-constants';
import { CoachingLeftViewComponent } from './components/coaching-left-view/coaching-left-view.component';
import { PlanAfterCoachingComponent } from './components/plan-after-coaching/plan-after-coaching.component';
import { SuggestResultImportComponent } from './components/suggest-result-import/suggest-result-import.component';
import { SuggestComponent } from './components/suggest/suggest.component';


const routes: Routes = [
  {
    path: '',
    component: CoachingLeftViewComponent,
    outlet: 'app-left-content',
  },
  {
    path: '',
    children: [
      {
        path: 'suggest',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.COACHING_SUGGEST
        },
        children: [
          {
            path: '',
            component: SuggestComponent,
            data: {
              scope: Scopes.VIEW
            },
          }
        ]
      },
      {
        path: 'plan',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.PLAN_AFTER_COACHING
        },
        children: [
          {
            path: '',
            component: PlanAfterCoachingComponent,
            data: {
              scope: Scopes.VIEW
            },
          }
        ]
      },
      {
        path: 'suggest-result-import',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.COACHING_SUGGEST_RESULT_IMPORT
        },
        children: [
          {
            path: '',
            component: SuggestResultImportComponent,
            data: {
              scope: Scopes.VIEW
            },
          }
        ]
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoachingRoutingModule { }
