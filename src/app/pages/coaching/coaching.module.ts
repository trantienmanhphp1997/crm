import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SuggestComponent } from './components/suggest/suggest.component';
import { CoachingRoutingModule } from './coaching.routing';
import { CoachingLeftViewComponent } from './components/coaching-left-view/coaching-left-view.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { GridsterModule } from 'angular-gridster2';
import { DynamicModule } from 'ng-dynamic-component';
import { NgxEchartsModule } from 'ngx-echarts';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgxSelectModule } from 'ngx-select-ex';
import { AccordionModule } from 'primeng/accordion';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import { InputNumberModule } from 'primeng/inputnumber';
import { InputTextModule } from 'primeng/inputtext';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { RmModule } from '../rm/rm.module';
import * as echarts from 'echarts';
import { SharedModule } from 'src/app/shared/shared.module';
import { PlanAfterCoachingComponent } from './components/plan-after-coaching/plan-after-coaching.component';
import { SuggestResultImportComponent } from './components/suggest-result-import/suggest-result-import.component';


@NgModule({
  declarations: [
    SuggestComponent,
    CoachingLeftViewComponent,
    PlanAfterCoachingComponent,
    SuggestResultImportComponent
  ],
  imports: [
    CommonModule,
    CoachingRoutingModule,
    SharedModule,
    NgbModule,
    DragDropModule,
    ReactiveFormsModule,
    FormsModule,
    InfiniteScrollModule,
    TranslateModule,
    NgxDatatableModule,
    NgxSelectModule,
    RmModule,
    MatProgressBarModule,
    MatTabsModule,
    NgxEchartsModule.forRoot({
      echarts,
    }),
    GridsterModule,
    DynamicModule,
    AccordionModule,
    TabViewModule,
    ButtonModule,
    InputTextModule,
    DropdownModule,
    CalendarModule,
    RadioButtonModule,
    InputNumberModule,
    TableModule,
  ]
})
export class CoachingModule { }
