import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { FileService } from 'src/app/core/services/file.service';
import { typeExcel } from 'src/app/core/utils/common-constants';
import { size } from 'lodash';
import { Utils } from 'src/app/core/utils/utils';
import { finalize } from 'rxjs/operators';
import { ResultImportService } from '../../services/result-import.service';

@Component({
  selector: 'app-suggest-result-import',
  templateUrl: './suggest-result-import.component.html',
  styleUrls: ['./suggest-result-import.component.scss']
})
export class SuggestResultImportComponent extends BaseComponent implements OnInit {
  isUpload = false;
  isFile = false;
  isLoading = false;

  fileName: string;
  fileImport: File;
  notificationMessage: any;
  files: any;

  listDataError = [];

  constructor(
    injector: Injector,
    private resultImportService: ResultImportService,
    private fileService: FileService,
  ) {
    super(injector);
  }

  ngOnInit(): void {
  }

  handleFileInput(files) {
    if (files?.item(0)?.size > 10485760) {
      this.messageService.warn(this.notificationMessage.ECRM005);
      return;
    }
    if (files?.item(0) && !typeExcel.includes(files?.item(0)?.type)) {
      this.messageService.error(this.notificationMessage.CANNOT_READ_DATA_FROM_FILE);
      return;
    }

    if (size(files) > 0) {
      this.isFile = true;
      this.fileImport = files.item(0);
      this.fileName = files.item(0).name;
    } else {
      this.isFile = false;
    }
  }

  importFile() {
    if (this.isFile && !this.isUpload) {
      this.isLoading = true;
      const formData: FormData = new FormData();
      formData.append('file', this.fileImport);
      this.resultImportService.importFile(formData)
        .pipe(
          finalize(() => {
            this.isLoading = false;
          })
        ).subscribe(
          (res) => {
            if (res.error) {
              this.listDataError = res.data;
              if (!res.data.length && res.errorMessage) {
                this.messageService.error(res.errorMessage);
              }
            } else {
              this.messageService.success(this.notificationMessage.success);
            }
          }
        );
    }
  }

  downloadTemplate() {
    this.isLoading = true;
    this.resultImportService
      .downloadTemplate()
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe((res) => {
        if (Utils.isStringNotEmpty(res)) {
          this.dowloadFile(res);
        } else {
          this.messageService.error(this.notificationMessage.error);
        }
      });
  }

  dowloadFile(fieId: string) {
    this.fileService.downloadFile(fieId, 'Template_import_ket_qua_de_xuat_coaching.xlsx')
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      ).subscribe((res) => {
        if (!res) {
          this.messageService.error(this.notificationMessage.error);
        }
      });
  }

  clearFile() {
    this.isUpload = false;
    this.isFile = false;
    this.fileName = null;
    this.fileImport = null;
    this.files = null;
    this.listDataError = [];
  }

}
