import { ChangeDetectorRef, Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { forkJoin, of } from 'rxjs';
import { finalize, catchError } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import { FileService } from 'src/app/core/services/file.service';
import { FunctionCode, Scopes } from 'src/app/core/utils/common-constants';
import { CalendarAddModalComponent } from 'src/app/pages/calendar-sme/components/calendar-add-modal/calendar-add-modal.component';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { CoachingService } from '../../services/coaching.service';
import { isEmpty, isEqual, pick } from 'lodash';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import * as moment from 'moment';
import { IPlanBody } from '../../interfaces/coaching.interface';
import { Utils } from 'src/app/core/utils/utils';

@Component({
  selector: 'app-plan-after-coaching',
  templateUrl: './plan-after-coaching.component.html',
  styleUrls: ['./plan-after-coaching.component.scss']
})
export class PlanAfterCoachingComponent extends BaseComponent implements OnInit, OnDestroy {

  isLoading = true;

  listData = [];
  originList = [];
  listBranch = [];
  virtualList = [{}]; // virtual scroll primeng is super lag, use list with 1 elm instead

  isPersonal = new FormControl(false);
  today = new Date();
  translateCoaching: any;

  commonData = {
    isRM: false,
  }
  paramSearch = {
    search: '',
    date: this.today,
    rsId: '',
    scope: Scopes.VIEW,
  };

  rowEdit = {};
  coachingForm = this.fb.array([]);
  currentDate = moment(this.today).format('MM/YYYY');

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private coachingService: CoachingService,
    private cdk: ChangeDetectorRef,
    private fileService: FileService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.PLAN_AFTER_COACHING}`);
    this.paramSearch.rsId = this.objFunction?.rsId;
  }

  ngOnInit(): void {
    this.isPersonal.valueChanges.subscribe(() => {
      this.search();
    });

    this.getCommonData();
  }

  getData() {
    this.isLoading = true;
    let apiName = this.commonData.isRM ? 'getRMPlanList' : 'getCBQLPlanList';

    const date = moment(this.paramSearch.date).format('MM/YYYY');
    const params = {
      ...pick(this.paramSearch, ['date', 'rsId', 'scope']),
      ...{ date }
    }

    this.coachingService[apiName](params).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe((result) => {
      this.originList = result;
      this.listData = result;
      this.currentDate = date;
      
      this.initForm();
      this.search();
    });
  }

  getCommonData() {
    forkJoin([
      this.categoryService
        .getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined)))
    ]).subscribe(([branchesOfUser]) => {
      this.commonData.isRM = isEmpty(branchesOfUser);
      this.initMode();
      this.getData();
    })
  }

  initMode(): void {
    this.isPersonal.setValue(this.commonData.isRM);
    if (this.commonData.isRM) {
      this.isPersonal.disable({ emitEvent: false });
    }
  }

  exportFile() {
    if (this.originList.length === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    this.isLoading = true;
    const isPersonal = this.isPersonal.value;
    const params = {
      ...this.paramSearch,
      search: this.paramSearch.search,
      ...{ date: moment(this.paramSearch.date).format('MM/YYYY') }
    };

    this.coachingService
      .exportCoachingPlan(params)
      .pipe(
        finalize(() => {
          this.isLoading = false;
        }),
        catchError(() => of(undefined))
      )
      .subscribe((fileId) => {
        if (Utils.isStringNotEmpty(fileId)) {
          this.fileService.downloadFile(fileId, 'ke_hoach_sau_coaching.xlsx').subscribe((res) => {
            if (!res) {
              this.messageService.error(this.notificationMessage.error);
            }
          });
        } else {
          this.messageService.error(this.notificationMessage?.export_error || '');
        }
      });
  }

  search() {
    const date = moment(this.paramSearch.date).format('MM/YYYY');
    if (this.currentDate !== date) {
      this.getData();
      return;
    }

    const keyword = this.paramSearch.search.trim().toLowerCase();

    this.listData = this.originList.filter(item => {
      return (this.isPersonal.value ? item.rmCode === this.currUser.code : true) &&
        [item.rmName, item.rmCode, item.hrsCode].filter(v => (v || '').toLowerCase().includes(keyword)).length &&
        item.month === date;
    });
    this.virtualList = this.listData.length ? [{}] : [];
  }

  // compare current with next row
  isSameParent(index): boolean {
    return this.listData[index].rmCode === this.listData[index + 1]?.rmCode &&
      this.listData[index].rmLevelCode === this.listData[index + 1]?.rmLevelCode &&
      this.listData[index].rmTitleCode === this.listData[index + 1]?.rmTitleCode;
  }

  isFirstChild(index): boolean {
    return this.listData[index - 1]?.rmCode !== this.listData[index]?.rmCode ||
      (
        this.listData[index - 1]?.rmCode === this.listData[index]?.rmCode &&
        this.listData[index - 1]?.rmTitleCode !== this.listData[index]?.rmTitleCode
      ) ||
      (
        this.listData[index - 1]?.rmCode === this.listData[index]?.rmCode &&
        this.listData[index - 1]?.rmTitleCode === this.listData[index]?.rmTitleCode &&
        this.listData[index - 1]?.rmLevelCode !== this.listData[index]?.rmLevelCode
      );
  }

  changeToEditMode(row): void {
    const children = this.listData.filter(item => row.rmCode === item.rmCode && row.rmTitleCode === item.rmTitleCode && row.rmLevelCode === item.rmLevelCode);
    children.forEach(child => {
      this.setRowEdit(child, true);
    })

    this.cdk.detectChanges();
  }

  isInvalidPattern(controlName, rowIndex): boolean {
    return this.coachingForm.controls[rowIndex].get(controlName).errors?.onlyNumber
  }

  apply(row): void {
    const changeList = [];
    let body = [];

    this.childList(row).forEach(child => {
      const planIndex = this.getPlanFormIndex(child);
      const plan = this.coachingForm.controls[planIndex].value;
      if (planIndex === -1) return;

      const isSame = isEqual(child, plan);
      if (!isSame) {
        plan.target = plan.target ? Number(plan.target) : null; // convert to number
        changeList.push({ plan, index: planIndex })
      }
    });

    body = changeList.map(v => v.plan);
    if (body.length === 0) {
      this.setRowEdit(row, false);
      return
    };

    // update new data from api
    const update = (list: IPlanBody[]) => {
      changeList.forEach(item => {
        const newPlan = this.getPlan(list, item.plan);
        const originIndex = this.getPlanIndex(this.originList, item.plan);
        const currentPlanIndex = this.getPlanFormIndex(item.plan);

        if (!newPlan || originIndex === -1 || currentPlanIndex === -1) return;

        this.coachingForm.controls[currentPlanIndex].patchValue(newPlan);
        this.originList[originIndex] = newPlan;
      })
    }

    this.isLoading = true;

    this.coachingService.setPlan({ data: body }).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(res => {
      update(res);
      this.search();
      this.setRowEdit(row, false);
      this.messageService.success(this.notificationMessage.plan_coaching_success);
    });
  }

  cancel(row): void {
    this.childList(row).forEach(child => {
      const planIndex = this.getPlanFormIndex(child);
      if (planIndex === -1) return;

      this.coachingForm.controls[planIndex].patchValue(child);

    });

    this.setRowEdit(row, false);
  }

  childList(row): any[] {
    return this.listData
      .filter(child => {
        return child.rmCode === row?.rmCode &&
          child.rmLevelCode === row?.rmLevelCode &&
          child.rmTitleCode === row?.rmTitleCode;
      })
  }

  setRowEdit(row, status: boolean): void {
    this.rowEdit[`${row.rmCode}-${row.rmTitleCode}-${row.rmLevelCode}`] = status;
  }

  getPlanFormIndex(data): number {
    return this.coachingForm.controls.findIndex(v =>
      v.value.rmCode === data.rmCode &&
      v.value.rmLevelCode === data.rmLevelCode &&
      v.value.rmTitleCode === data.rmTitleCode &&
      v.value.coachingTopicCode === data.coachingTopicCode);
  }

  getPlanIndex(mainList, supList): number {
    return mainList.findIndex(v =>
      v.rmCode === supList.rmCode &&
      v.rmLevelCode === supList.rmLevelCode &&
      v.rmTitleCode === supList.rmTitleCode &&
      v.coachingTopicCode === supList.coachingTopicCode
    )
  }

  getPlan(mainList, supList): number {
    return mainList.find(v =>
      v.rmCode === supList.rmCode &&
      v.rmLevelCode === supList.rmLevelCode &&
      v.rmTitleCode === supList.rmTitleCode &&
      v.coachingTopicCode === supList.coachingTopicCode
    )
  }

  initForm(): void {
    this.rowEdit = {};
    this.coachingForm.clear();
    this.originList.forEach(item => {
      this.coachingForm.push(
        this.fb.group({
          ...item,
          ...{
            target: [item.target === null ? '' : item.target, [CustomValidators.onlyNumberAndDecimal]],
            notes: [item.notes || '']
          }
        })
      )
    });
  }
}
