import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { ARM_STATUS_LIST, FunctionCode, Scopes } from 'src/app/core/utils/common-constants';
import { forkJoin, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { FileService } from 'src/app/core/services/file.service';
import { Utils } from 'src/app/core/utils/utils';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as _ from 'lodash';
import { CoachingService } from '../../services/coaching.service';
import { FormControl } from '@angular/forms';
import * as moment from 'moment';
import { CalendarAddModalComponent } from 'src/app/pages/calendar-sme/components/calendar-add-modal/calendar-add-modal.component';

@Component({
  selector: 'app-suggest',
  templateUrl: './suggest.component.html',
  styleUrls: ['./suggest.component.scss']
})
export class SuggestComponent extends BaseComponent implements OnInit, OnDestroy {
  isLoading = false;
  listData = [];
  originList = [];
  commonData = {
    isRM: false,
  }
  paramSearch = {
    rsId: '',
    scope: Scopes.VIEW,
  };
  textSearch = '';
  isPersional = new FormControl(false);
  virtualList = [{}]; // virtual scroll primeng is super lag, use list with 1 elm instead

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private fileService: FileService,
    private coachingService: CoachingService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.COACHING_SUGGEST}`);
    this.paramSearch.rsId = this.objFunction?.rsId;
  }

  ngOnInit(): void {
    this.isPersional.valueChanges.subscribe(() => {
      this.search();
    });

    this.getCommonData();
  }

  getData() {
    this.isLoading = true;
    this.coachingService.findAll(this.paramSearch).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe((result) => {
      this.originList = result;
      this.listData = result;
      this.search();
    });
  }

  getCommonData() {
    forkJoin([
      this.categoryService
        .getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined)))
    ]).subscribe(([branchesOfUser]) => {
      this.commonData.isRM = _.isEmpty(branchesOfUser);
      this.initMode();
      this.getData();
    })
  }

  initMode(): void {
    this.isPersional.setValue(this.commonData.isRM);
    if (this.commonData.isRM) {
      this.isPersional.disable({ emitEvent: false });
    }
  }

  exportFile() {
    if (this.originList.length === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    this.isLoading = true;
    const isPersional = this.isPersional.value;
    const params = { 
      ...this.paramSearch, 
      search: this.textSearch, 
      ...{ isPersional, ...isPersional ? { rmCode: this.currUser.code || '' } : {} } 
    };

    this.coachingService
      .exportCoachingSuggest(params)
      .pipe(
        finalize(() => {
          this.isLoading = false;
        }),
        catchError(() => of(undefined))
      )
      .subscribe((fileId) => {
        if (Utils.isStringNotEmpty(fileId)) {
          this.fileService.downloadFile(fileId, 'danh_sach_de_xuat_coaching.xlsx').subscribe((res) => {
            if (!res) {
              this.messageService.error(this.notificationMessage.error);
            }
          });
        } else {
          this.messageService.error(this.notificationMessage?.export_error || '');
        }
      });
  }

  search() {
    const keyword = this.textSearch.trim().toLowerCase();

    this.listData = this.originList.filter(item => {
      return (this.isPersional.value ? item.rmCode === this.currUser.code : true) &&
        [item.fullname, item.rmCode, item.hrisCode].filter(v => (Utils.isNull(v) ? '' : v).toLowerCase().includes(keyword)).length;
    });
    this.virtualList = this.listData.length ? [{}] : [];
  }

  // compare current with next row
  isSameParent(index): boolean {
    return this.listData[index].rmCode === this.listData[index + 1]?.rmCode && 
    this.listData[index].rmLevelCode === this.listData[index + 1]?.rmLevelCode && 
    this.listData[index].rmTitleCode === this.listData[index + 1]?.rmTitleCode;
  }

  isFirstChild(index): boolean {
    return this.listData[index - 1]?.rmCode !== this.listData[index]?.rmCode ||
    (
      this.listData[index - 1]?.rmCode === this.listData[index]?.rmCode && 
      this.listData[index - 1]?.rmTitleCode !== this.listData[index]?.rmTitleCode
    ) ||
    (
      this.listData[index - 1]?.rmCode === this.listData[index]?.rmCode && 
      this.listData[index - 1]?.rmTitleCode === this.listData[index]?.rmTitleCode &&
      this.listData[index - 1]?.rmLevelCode !== this.listData[index]?.rmLevelCode
    );
  }

  book(info) {
    const { hrisCode, rmCode, fullname } = info;
    const formData = {
      activity: "ACTIVITY_TRAINING",
      function: FunctionCode.COACHING_SUGGEST,
      listUser: [
        {
          hrsCodeAssigns: this.currUser.hrsCode,
          name: this.currUser.code ? `${this.currUser.code} - ${this.currUser.fullName}` : this.currUser.fullName,
          isCurrentUser: true
        }
      ]
    }

    if (!this.isPersional.value && this.currUser.code !== rmCode) {
      formData.listUser.push({
        hrsCodeAssigns: hrisCode,
        name: `${rmCode || ''} - ${fullname}`,
        isCurrentUser: false
      })
    }
    
    const modal = this.modalService.open(CalendarAddModalComponent, { windowClass: 'create-calendar-modal' });
    modal.componentInstance.formData = formData;
  }
}
