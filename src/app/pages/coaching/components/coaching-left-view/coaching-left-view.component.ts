import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BaseComponent } from 'src/app/core/components/base.component';
import { ActivityService } from 'src/app/core/services/activity.service';
import { FunctionCode, Scopes } from 'src/app/core/utils/common-constants';
import { getRole } from 'src/app/core/utils/function';
import { global } from '@angular/compiler/src/util';
import { Utils } from 'src/app/core/utils/utils';

@Component({
  selector: 'app-coaching-left-view',
  templateUrl: './coaching-left-view.component.html',
  styleUrls: ['./coaching-left-view.component.scss']
})
export class CoachingLeftViewComponent extends BaseComponent implements OnInit, OnDestroy {

  activityCalls = [];
  role: string;
  branchCode: string;
  timer = true;
  timeOutId: any;
  paramActivity = {
    page: 0,
    size: 3,
    parentId: '',
    scope: Scopes.VIEW,
    rsId: '',
    isLeftSide: true,
  };
  subscription: Subscription;
  constructor(
    injector: Injector,
    private activityService: ActivityService // private communicateService: CommunicateService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.COACHING}`);
    this.role = getRole(global.roles);
    this.branchCode = this.currUser?.attributes?.branches ? this.currUser?.attributes.branches[0] : '';
    this.subscription = this.communicateService.request$.subscribe((req) => {
      if (req?.name === 'coaching-left-view') {
        this.paramActivity.rsId = this.objFunction?.rsId;
        this.paramActivity.parentId = Utils.trimNullToEmpty(req.data.parentId);
        if (this.timeOutId) {
          clearTimeout(this.timeOutId);
        }
        if (!req?.data?.view) {
          this.activityCalls = [];
        } else {
          this.getData();
        }
      }
    });
  }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.activityService.search(this.paramActivity).subscribe((result) => {
      this.activityCalls = [];
      if (result) {
        result.content.forEach((activity) => {
          activity.isCollapsed = true;
          this.activityCalls.push(activity);
        });
        // this.timeOutId = setTimeout(() => {
        //   if (this.timer) {
        //     this.getData();
        //   }
        // }, 5000);
      }
    });
  }

  collapsedActivity(i: number) {
    this.activityCalls[i].isCollapsed = this.activityCalls[i].isCollapsed ? false : true;
  }

  identify(index, item) {
    return item ? item.id : undefined;
  }

  ngOnDestroy() {
    this.timer = false;
    clearTimeout(this.timeOutId);
    this.subscription.unsubscribe();
  }

}
