import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { HttpWrapper } from 'src/app/core/apis/http-wapper';
import { environment } from '../../../../environments/environment';
import { ISearchParams, ISearchPlanParams } from '../interfaces/coaching.interface';
import { Utils } from 'src/app/core/utils/utils';
import { groupBy } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class CoachingService extends HttpWrapper {

  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_kpi}/coaching`);
  }

  findAll(params: ISearchParams): Observable<any> {
    return this.post('findAll', params).pipe(
      map(data => {
        return this.formatList(data);
      })
    );
  }

  exportCoachingSuggest(params: ISearchParams): Observable<any> {
    return this.http.post(`${this.baseURL}/export`, Utils.getParams(params), { responseType: 'text' });
  }

  getRMPlanList(params: ISearchParams): Observable<any> {
    return this.get('rmPlan', params).pipe(
      map(data => {
        return this.formatList(data);
      })
    );
  }

  getCBQLPlanList(params: ISearchPlanParams): Observable<any> {
    return this.get('cbqlPlan', params).pipe(
      map(data => {
        return this.formatList(data);
      })
    );
  }

  setPlan(body): Observable<any> {
    return this.post('plan', body);
  }

  exportCoachingPlan(params: ISearchParams): Observable<any> {
    return this.http.get(`${this.baseURL}/cbqlPlan/export`, { params: Utils.getParams(params), responseType: 'text' });
  }


  formatList(data) {
    let rmObj = groupBy(data, 'rmCode');
    let result = [];

    // sort level
    const sortLevel = (rmList) => {
      let lvList = [];
      const levelByObject = groupBy(rmList, 'rmLevelCode');
      Object.keys(levelByObject).forEach(key => {
        lvList = [...lvList, ...levelByObject[key]];
      });

      return lvList;
    }

    // merge all rm group
    Object.keys(rmObj).forEach(key => {
      rmObj[key] = sortLevel(rmObj[key]);
      result = [...result, ...rmObj[key]];
    });
    return result;
  }
}
