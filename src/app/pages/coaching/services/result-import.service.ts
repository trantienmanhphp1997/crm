import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpWrapper } from 'src/app/core/apis/http-wapper';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ResultImportService extends HttpWrapper {

  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_kpi}/coaching`);
  }

  downloadTemplate(): Observable<any> {
    return this.http.get(`${this.baseURL}/template`, { responseType: 'text' });
  }

  importFile(formData: FormData): Observable<any> {
    return this.post('import', formData);
  }
}
