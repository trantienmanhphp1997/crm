export interface ISearchParams {
    fullname?: String;
    rmCode?: String;
    hrisCode?: String;
    rsId: String;
    scope: String;
}

export interface ISearchPlanParams {
    date?: String;
    rsId: String;
    scope: String;
}

export interface ICoachingBody {
    id?: string;
    branch?: string;
    entrustPoint?: number;
    fullname: string;
    hrisCode: string;
    monthYear: string;
    rmCode: string;
    rmLevelCode: string;
    rmLevelName: string;
    rmTitleCode: string;
    rmTitleName: string;
    topicName?: string;
    topicPropose?: string;
}

export interface IPlanBody {
    id: string;
    branchCode: string;
    branchName: string;
    coachingTopicCode: string;
    coachingTopicName: string;
    employeeId: string;
    hrsCode: string;
    month: string;
    notes: string;
    rmCode: string;
    rmLevelCode: string;
    rmName: string;
    rmTitleCode: string;
    rmTitleName: string;
    target: number
}