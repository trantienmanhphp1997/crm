import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SupportRmRoutingModule } from './support-rm.routing';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SupportRmRoutingModule
  ]
})
export class SupportRmModule { }
