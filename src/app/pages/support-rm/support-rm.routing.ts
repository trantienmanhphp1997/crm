import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    children: [
      // {
      //   path: 'hotline',
      //   canActivateChild: [RoleGuardService],
      //   data: {
      //     code: FunctionCode.SUPPORT_RM_HOTLINE,
      //     scope: Scopes.VIEW,
      //   },
      //   component: HotlineComponent,
      // }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupportRmRoutingModule { }
