import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGuardService } from 'src/app/core/services/role-guard.service';
import { FunctionCode } from 'src/app/core/utils/common-constants';
import { CategoryDeclareComponent, CharacteristicDeclareComponent, InformationDeclareComponent } from './components';

const routes: Routes = [
	{
		path: 'category-declare',
		canActivate: [RoleGuardService],
		data: {
			code: FunctionCode.SALEKIT_CATEGORY_DECLARE,
		},
		component: CategoryDeclareComponent,
	},

	{
		path: 'characteristic-declare',
		canActivate: [RoleGuardService],
		data: {
			code: FunctionCode.SALEKIT_CHARACTERISTIC_DECLARE,
		},
		component: CharacteristicDeclareComponent,
	},
	{
		path: 'info-declare',
		component: InformationDeclareComponent,
		canActivate: [RoleGuardService],
		data: {
			code: FunctionCode.SALEKIT_INFO_DECLARE,
		},
	},
	{
		path: 'approve',
		component: InformationDeclareComponent,
		canActivate: [RoleGuardService],
		data: {
			code: FunctionCode.SALEKIT_APPROVE,
			isApprove: true,
		},
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class SalekitRoutingModule {}
