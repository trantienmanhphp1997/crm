import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pageable } from 'src/app/core/interfaces/pageable-data.interface';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';
import { CategoryModel, GroupInfoModel, CharacteristicModel } from '../models';
import { global } from '@angular/compiler/src/util';

@Injectable({
  providedIn: 'root',
})
export class CategoryDeclareService {
  constructor(private http: HttpClient) {}

  getGroupInfo(): Observable<GroupInfoModel[]> {
    return this.http.get(`${environment.url_endpoint_sale}/sale-kit/functions`).pipe(
      map<any, GroupInfoModel[]>((respond) => {
        const data: GroupInfoModel[] = _.get(respond, 'functions', []).map((item) => {
          return <GroupInfoModel>{
            code: item.id,
            name: item.name,
            type: item.type,
          };
        });
        return data;
      })
    );
  }

  searchProduct(params: any): Observable<Pageable<CategoryModel>> {
    return this.http.get(`${environment.url_endpoint_sale}/sale-kit/categoryByFunctionIdAndBizLine`, { params }).pipe(
      map<any, Pageable<CategoryModel>>((pageable) => {
        const data: Pageable<CategoryModel> = {
          totalElements: _.get(pageable, 'totalElements', 0),
          totalPages: _.get(pageable, 'totalPages', 0),
          currentPage: _.get(pageable, 'number', 0),
          size: _.get(global, 'userConfig.pageSize'),
          rows: _.get(pageable, 'content', []),
        };
        return data;
      })
    );
  }

  createdProduct(data: CategoryModel): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sale-kit/category`, data);
  }

  deleteProduct(saleKitCategoryId: any): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_sale}/sale-kit/category/${saleKitCategoryId}`);
  }

  updateProduct(data: CategoryModel): Observable<any> {
    return this.http.put(`${environment.url_endpoint_sale}/sale-kit/category`, data);
  }

  searchCharacteristic(params: any): Observable<Pageable<CharacteristicModel>> {
    return this.http.post(`${environment.url_endpoint_sale}/sale-kit/category-attribute-search`, params).pipe(
      map<any, Pageable<CharacteristicModel>>((pageable) => {
        const data: Pageable<CharacteristicModel> = {
          totalElements: _.get(pageable, 'totalElements', 0),
          totalPages: _.get(pageable, 'totalPages', 0),
          currentPage: _.get(pageable, 'number', 0),
          size: _.get(global, 'userConfig.pageSize'),
          rows: _.get(pageable, 'content', []),
        };
        return data;
      })
    );
  }

  createCharacteristic(data: CharacteristicModel): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sale-kit/category-attribute-save`, data);
  }
}
