import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SalekitInfoService {
  baseUrl = `${environment.url_endpoint_sale}/sale-kit`;
  constructor(private http: HttpClient) {}

  save(data): Observable<any> {
    return this.http.post(`${this.baseUrl}/save-product`, data);
  }

  update(data): Observable<any> {
    return this.http.post(`${this.baseUrl}/update-product`, data);
  }

  delete(id): Observable<any> {
    return this.http.post(`${this.baseUrl}/delete-product`, { id });
  }

  send(id): Observable<any> {
    return this.http.post(`${this.baseUrl}/send-product`, { id });
  }

  approve(id): Observable<any> {
    return this.http.post(`${this.baseUrl}/approve-product`, { id });
  }

  refuse(id, reasonOfDisapproved): Observable<any> {
    return this.http.post(`${this.baseUrl}/refuse-product`, { id, reasonOfDisapproved });
  }

  getProduct(params): Observable<any> {
    return this.http.get(`${this.baseUrl}/get-product`, { params });
  }
}
