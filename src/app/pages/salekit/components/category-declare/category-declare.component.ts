import { FunctionCode, SessionKey } from 'src/app/core/utils/common-constants';
import { Component, Injector, OnInit } from '@angular/core';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CategoryDeclareService } from '../../services/category-declare.service';
import { global } from '@angular/compiler/src/util';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { Validators } from '@angular/forms';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { CategoryModel, GroupInfoModel } from '../../models';

@Component({
  selector: 'category-declare',
  templateUrl: './category-declare.component.html',
  styleUrls: ['./category-declare.component.scss'],
})
export class CategoryDeclareComponent extends BaseComponent implements OnInit {
  constructor(injector: Injector, private categoryDeclareService: CategoryDeclareService) {
    super(injector);
    this.isLoading = true;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.SALEKIT_CATEGORY_DECLARE}`);
  }
  limit = global.userConfig.pageSize;
  form = this.fb.group({
    division: '',
    group: '',
  });
  formItem = this.fb.group({
    saleKitCategoryId: null,
    productName: ['', CustomValidators.required],
    order: ['', [Validators.min(0), CustomValidators.onlyNumber]],
    description: '',
  });
  listDivision = [];
  listGroup: GroupInfoModel[] = [];
  listProduct = [];
  pageable: Pageable;
  paramSearch = {
    page: 0,
    size: this.limit,
    saleKitFunctionId: '',
    bizLine: '',
  };
  screen = this.screenType.detail;
  selected = [];
  titleGroup = '';
  itemOld: any;

  ngOnInit(): void {
    const list = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM);
    this.listDivision = _.map(list, (item) => {
      return { code: item.code, name: `${item.code} - ${item.name}` };
    });
    this.listDivision = _.orderBy(this.listDivision, ['name'], ['asc', 'desc']);
    this.form.get('division').setValue(_.first(this.listDivision)?.code);
    this.categoryDeclareService
      .getGroupInfo()
      .pipe(catchError(() => of([])))
      .subscribe((data) => {
        this.listGroup = data;
        this.form.get('group').setValue(_.first(this.listGroup)?.code);
        this.search();
      });
  }

  ngAfterViewInit() {
    this.form.controls.division.valueChanges.subscribe((value) => {
      this.screen = this.screenType.detail;
      this.paramSearch.page = 0;
      this.selected = [];
      this.search();
    });
    this.form.controls.group.valueChanges.subscribe((value) => {
      this.titleGroup = _.find(this.listGroup, (item) => item.code === value)?.name?.toLowerCase();
      this.screen = this.screenType.detail;
      this.selected = [];
      this.paramSearch.page = 0;
      this.search();
    });
  }

  onActive(event) {
    if (event.type === 'click') {
      this.screen = this.screenType.detail;
      const data = { ...event.row };
      this.resetForm();
      this.mapDataFormItem(data);
    }
  }

  mapDataFormItem(data: CategoryModel) {
    if (data) {
      const item = {
        description: data.description,
        productName: data.saleKitCategoryName,
        order: data.numberOrder,
        saleKitCategoryId: data.saleKitCategoryId,
      };
      this.formItem.patchValue(item);
    }
  }

  cancel() {
    this.screen = this.screenType.detail;
    this.resetForm();
    if (this.selected.length > 0) {
      this.mapDataFormItem(this.selected[0]);
    }
  }

  create() {
    if (this.screen !== this.screenType.create) {
      this.selected = [];
      this.screen = this.screenType.create;
      this.resetForm();
    }
  }

  update() {
    this.screen = this.screenType.update;
    this.formItem.enable();
  }

  resetForm() {
    this.formItem.reset();
    if (this.screen === this.screenType.detail) {
      this.formItem.disable();
    } else {
      this.formItem.enable();
    }
  }

  deleteProduct() {
    if (!this.valueFormItem.saleKitCategoryId) {
      this.translate.get('notificationMessage.messageCategoryDeclare', { name: this.titleGroup }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.confirmService.confirm().then((res) => {
      if (res) {
        this.isLoading = true;
        this.categoryDeclareService.deleteProduct(this.valueFormItem.saleKitCategoryId).subscribe(
          () => {
            this.messageService.success(this.notificationMessage.success);
            this.selected = [];
            this.search();
          },
          (e) => {
            this.isLoading = false;
            this.messageService.error(this.notificationMessage.error);
          }
        );
      }
    });
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.page = pageInfo.offset;
    this.search();
  }

  saveProduct() {
    if (this.formItem.valid) {
      this.confirmService.confirm().then((res) => {
        if (res) {
          this.isLoading = true;
          const data: CategoryModel = {
            saleKitCategoryId: this.valueFormItem.saleKitCategoryId,
            saleKitCategoryName: _.trim(this.valueFormItem.productName),
            saleKitFunctionId: this.valueForm.group,
            numberOrder: parseInt(this.valueFormItem.order, 10),
            bizLine: this.valueForm.division,
            description: _.trim(this.valueFormItem.description),
          };
          if (data.saleKitCategoryId) {
            delete data.bizLine;
            delete data.saleKitFunctionId;
            this.categoryDeclareService.updateProduct(data).subscribe(
              (result) => {
                if (result === null) {
                  this.messageService.success(this.notificationMessage.success);
                  this.screen = this.screenType.detail;
                  this.search();
                }
              },
              (e) => {
                this.isLoading = false;
                this.messageService.error(this.notificationMessage.error);
                return;
              }
            );
          } else {
            delete data.saleKitCategoryId;
            this.categoryDeclareService.createdProduct(data).subscribe(
              (result) => {
                if (result) {
                  this.messageService.success(this.notificationMessage.success);
                  this.screen = this.screenType.detail;
                  this.search();
                }
              },
              (e) => {
                this.isLoading = false;
                this.messageService.error(this.notificationMessage.error);
                return;
              }
            );
          }
        }
      });
    } else {
      validateAllFormFields(this.formItem);
    }
  }

  search() {
    this.isLoading = true;
    this.paramSearch.saleKitFunctionId = this.form.get('group').value;
    this.paramSearch.bizLine = this.form.get('division').value;
    this.categoryDeclareService.searchProduct(this.paramSearch).subscribe(
      (data) => {
        this.listProduct = data.rows;
        const index = _.findIndex(
          this.listProduct,
          (item) => item.saleKitCategoryId === this.selected[0]?.saleKitCategoryId
        );
        if (index !== -1) {
          this.selected = [this.listProduct[index]];
        } else if (this.listProduct[0]) {
          this.selected = [this.listProduct[0]];
        }
        this.resetForm();
        this.mapDataFormItem(this.selected[0]);
        this.pageable = {
          totalElements: data.totalElements,
          totalPages: data.totalPages,
          currentPage: data.currentPage,
          size: this.limit,
        };
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  checkBtnCreate() {
    return (
      _.isEmpty(this.form.controls.division.value) ||
      _.isEmpty(_.trim(this.form.controls.group.value)) ||
      this.screen !== this.screenType.detail
    );
  }

  get valueFormItem() {
    return this.formItem.value;
  }

  get valueForm() {
    return this.form.value;
  }
}
