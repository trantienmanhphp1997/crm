import { catchError } from 'rxjs/operators';
import { SessionKey, maxInt32, CommonCategory, FunctionCode } from 'src/app/core/utils/common-constants';
import { AfterViewInit, Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { FileService } from 'src/app/core/services/file.service';
import { forkJoin, of } from 'rxjs';
import { SalekitInfoService } from '../../services/salekit-info.service';
import { CategoryDeclareService } from '../../services/category-declare.service';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { ConfirmAssignmentModalComponent } from 'src/app/pages/customer-360/components';
import * as moment from 'moment';
import { global } from '@angular/compiler/src/util';
import { CategoryModel, CharacteristicModel, GroupInfoModel, SaleKitInformationModel } from '../../models';

@Component({
  selector: 'information-declare',
  templateUrl: './information-declare.component.html',
  styleUrls: ['./information-declare.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class InformationDeclareComponent extends BaseComponent implements OnInit, AfterViewInit {
  constructor(
    injector: Injector,
    private fileService: FileService,
    private categorySalekitService: CategoryDeclareService,
    private service: SalekitInfoService
  ) {
    super(injector);
    this.isApprove = _.get(this.route, 'snapshot.data.isApprove', false);
    if (this.isApprove) {
      this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.SALEKIT_APPROVE}`);
    } else {
      this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.SALEKIT_INFO_DECLARE}`);
    }
    this.isLoading = true;
  }

  isApprove = false;
  form = this.fb.group({
    division: '',
    group: '',
    productId: '',
    approveStatus: '',
  });
  formItem = this.fb.group({
    id: '',
    code: ['', CustomValidators.required],
    name: ['', CustomValidators.required],
    shortName: ['', CustomValidators.required],
    startDate: [new Date(), CustomValidators.required],
    endDate: null,
    approveStatus: '0',
    description: '',
    reasonOfDisapproved: '',
    isNotification: false,
  });
  listDivision = [];
  listGroup: GroupInfoModel[] = [];
  listProduct: CategoryModel[] = [];
  listProductTable: SaleKitInformationModel[] = [];
  listCharacteristic: CharacteristicModel[] = [];
  listTerm = [];
  listStatus = [];
  pageable: Pageable;
  paramsTable = {
    page: 0,
    size: _.get(global, 'userConfig.pageSize'),
    productId: '',
    status: '',
    approve: false,
  };
  screen = this.screenType.detail;
  textColumnName: string;
  titleByGroup: string;
  uploadedFiles: any[] = [];
  isValidator = false;
  minDate = new Date();
  selected = [];
  @ViewChild('fileUpload') fileUpload: any;

  ngOnInit(): void {
    this.paramsTable.approve = this.isApprove;
    const list = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM);
    this.listDivision = _.map(list, (item) => {
      return { code: item.code, name: `${item.code} - ${item.name}` };
    });
    this.form.controls.division.setValue(_.first(this.listDivision)?.code, { emitEvent: false });
    forkJoin({
      listStatus: this.commonService
        .getCommonCategory(CommonCategory.SALEKIT_APPROVE_STATUS)
        .pipe(catchError(() => of(undefined))),
      listGroup: this.categorySalekitService.getGroupInfo().pipe(catchError(() => of([]))),
    }).subscribe((data) => {
      this.listStatus = _.get(data, 'listStatus.content', []).map((status) => {
        return { value: status.value, name: `${status.value} - ${status.name}` };
      });
      this.listStatus.unshift({ value: '', name: this.fields.all });
      if (this.isApprove) {
        this.form.controls.approveStatus.setValue('1', { emitEvent: false });
        this.paramsTable.status = '1';
      }
      this.listGroup = data.listGroup;
      this.form.controls.group.setValue(_.first(this.listGroup)?.code);
    });
  }

  getProductParent() {
    const params = {
      saleKitFunctionId: this.form.controls.group.value,
      bizLine: this.form.controls.division.value,
      page: 0,
      size: maxInt32,
    };
    if (!_.isEmpty(_.trim(params.saleKitFunctionId)) && !_.isEmpty(params.bizLine)) {
      this.categorySalekitService.searchProduct(params).subscribe((data) => {
        this.listProduct = _.get(data, 'rows', []).map((item) => {
          return { code: item.saleKitCategoryId, name: _.get(item, 'saleKitCategoryName', '') };
        });
        this.form.controls.productId.setValue(_.first(this.listProduct)?.code);
      });
    } else {
      this.listProductTable = [];
      this.pageable = {
        totalElements: 0,
        totalPages: 0,
        currentPage: 0,
        size: this.paramsTable.size,
      };
      this.isLoading = false;
    }
  }

  ngAfterViewInit() {
    this.form.controls.group.valueChanges.subscribe((value) => {
      this.titleByGroup = _.find(this.listGroup, (item) => item.code === value)?.name?.toLowerCase();
      this.textColumnName = `${this.fields.name} ${this.titleByGroup}`;
      this.screen = this.screenType.detail;
      this.getProductParent();
      this.resetForm();
      this.selected = [];
    });
    this.form.controls.division.valueChanges.subscribe((value) => {
      if (!this.isLoading) {
        this.screen = this.screenType.detail;
        this.getProductParent();
        this.resetForm();
        this.selected = [];
      }
    });
    this.form.controls.productId.valueChanges.subscribe((value) => {
      this.paramsTable.productId = value;
      this.paramsTable.page = 0;
      this.screen = this.screenType.detail;
      this.selected = [];
      this.searchProductChildren();
      this.getListCharacteristicByProductId(value);
    });
    this.form.controls.approveStatus.valueChanges.subscribe((value) => {
      if (this.isApprove && !this.isLoading) {
        this.paramsTable.status = value;
        this.paramsTable.page = 0;
        this.searchProductChildren();
        this.selected = [];
      }
    });
    this.formItem.controls.startDate.valueChanges.subscribe((value) => {
      if (!this.isLoading && (_.isNull(value) || _.isUndefined(value))) {
        this.minDate = new Date();
        this.formItem.controls.endDate.setValue(null);
      } else {
        this.minDate = value;
        const endDate = this.formItem.controls.endDate.value;
        if (!_.isNull(endDate) && moment(value).isAfter(moment(endDate))) {
          this.messageService.warn('Ngày bắt đầu hiệu lực không được lớn hơn ngày hết hiệu lực');
        }
      }
    });
  }

  onActive(event) {
    if (event.type === 'click') {
      this.screen = this.screenType.detail;
      const data = { ...event.row };
      this.resetForm();
      this.mapDataFormItem(data);
    }
  }

  mapDataFormItem(data) {
    if (data) {
      data.startDate = data.startDate > 0 ? new Date(data.startDate) : null;
      data.endDate = data.endDate > 0 ? new Date(data.endDate) : null;
      this.formItem.patchValue(data);
      _.forEach(this.listCharacteristic, (item) => {
        const itemAttribute = _.find(_.get(data, 'attributes'), (i) => i.attributeId === item.attributeId);
        item.content = itemAttribute?.name ? itemAttribute.name : '';
      });
      this.uploadedFiles = [];
      _.forEach(_.get(data, 'attachments', []), (item) => {
        this.uploadedFiles.push({
          fileId: item.fileId,
          fileName: item.name,
          size: item?.size,
          idFake: _.size(this.uploadedFiles),
        });
      });
    }
  }

  getListCharacteristicByProductId(id) {
    if (_.trim(id) !== '') {
      const params = {
        saleKitCategoryId: id,
        page: 0,
        size: maxInt32,
      };
      this.categorySalekitService
        .searchCharacteristic(params)
        .pipe(catchError(() => of(undefined)))
        .subscribe((data) => {
          this.listCharacteristic = _.get(data, 'rows', []).map((item) => {
            return {
              attributeId: item.saleKitCategoryAttributeId,
              name: item.saleKitCategoryAttributeName,
              disabled: this.screen === this.screenType.detail,
              content: '',
            };
          });
          if (this.selected.length > 0) {
            this.mapDataFormItem(this.selected[0]);
          }
        });
    } else {
      this.listCharacteristic = [];
    }
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramsTable.page = pageInfo.offset;
    this.searchProductChildren();
  }

  searchProductChildren() {
    this.isLoading = true;
    this.service
      .getProduct(this.paramsTable)
      .pipe(catchError(() => of(undefined)))
      .subscribe((data) => {
        this.listProductTable = _.get(data, 'content', []);
        const index = _.findIndex(this.listProductTable, (item) => item.id === this.selected[0]?.id);
        if (index !== -1) {
          this.selected = [this.listProductTable[index]];
        } else if (this.listProductTable[0]) {
          this.selected = [this.listProductTable[0]];
        } else {
          this.selected = [];
        }
        this.resetForm();
        this.mapDataFormItem(this.selected[0]);
        this.pageable = {
          totalElements: _.get(data, 'totalElements'),
          totalPages: _.get(data, 'totalPages'),
          currentPage: _.get(data, 'number'),
          size: _.get(this.paramsTable, 'size'),
        };
        this.isLoading = false;
      });
  }

  save() {
    if (this.formItem.valid) {
      const dataForm = cleanDataForm(this.formItem);
      const data: SaleKitInformationModel = { ...dataForm };
      if (!_.isNull(data.endDate) && moment(data.startDate).isAfter(moment(data.endDate))) {
        this.messageService.warn('Ngày bắt đầu hiệu lực không được lớn hơn ngày hết hiệu lực');
        return;
      }
      data.saleKitCategoryId = _.trim(this.form.controls.productId.value);
      data.startDate = new Date(data.startDate).valueOf();
      data.endDate = new Date(data.endDate).valueOf();
      data.attributes = _.map(this.listCharacteristic, (item) => {
        return { attributeId: item.attributeId, content: item.content };
      });
      data.reasonOfDisapproved = '';
      data.approveStatus = '0';
      this.confirmService.confirm().then((isConfirm) => {
        if (isConfirm) {
          this.isLoading = true;
          const apiUploadFiles = [];
          const arrIndex = [];
          data.attachments = [];
          this.uploadedFiles.forEach((itemFile, index) => {
            if (!itemFile.fileId) {
              const formData = new FormData();
              formData.append('file', itemFile.file);
              apiUploadFiles.push(this.fileService.uploadFile(formData));
              arrIndex.push(index);
            }
            data.attachments.push({ fileId: itemFile.fileId, name: itemFile.fileName });
          });
          if (apiUploadFiles.length > 0) {
            forkJoin(apiUploadFiles).subscribe(
              (listId: string[]) => {
                arrIndex.forEach((value, i) => {
                  data.attachments[value].fileId = listId[i];
                });
                this.saveData(data);
              },
              () => {
                this.isLoading = false;
                this.messageService.error(this.notificationMessage.error);
              }
            );
          } else {
            this.saveData(data);
          }
        }
      });
    } else {
      validateAllFormFields(this.formItem);
      this.isValidator = true;
    }
  }

  saveData(data: SaleKitInformationModel) {
    if (_.trim(data.id) === '') {
      this.service.save(data).subscribe(
        () => {
          this.screen = this.screenType.detail;
          this.searchProductChildren();
          this.messageService.success(this.notificationMessage.success);
        },
        (e) => {
          this.isLoading = false;
          if (_.get(e, 'error.code', '').includes('CRM301')) {
            this.messageService.error(_.get(e, 'error.description', '').replace('${code}', data.code));
          } else {
            this.messageService.error(this.notificationMessage.error);
          }
        }
      );
    } else {
      this.service.update(data).subscribe(
        () => {
          this.screen = this.screenType.detail;
          this.searchProductChildren();
          this.messageService.success(this.notificationMessage.success);
        },
        (e) => {
          this.isLoading = false;
          if (_.get(e, 'error.code', '').includes('CRM301')) {
            this.messageService.error(_.get(e, 'error.description', '').replace('${code}', data.code));
          } else if (_.get(e, 'error.code', '').includes('.203')) {
            this.messageService.error('Không được phép sửa khi đang Chờ phê duyệt!');
          } else {
            this.messageService.error(this.notificationMessage.error);
          }
        }
      );
    }
  }

  resetForm() {
    this.formItem.reset();
    this.formItem.controls.startDate.setValue(new Date());
    this.formItem.controls.endDate.setValue(null);
    this.formItem.controls.isNotification.setValue(false);
    if (this.screen === this.screenType.detail) {
      this.formItem.disable();
    } else {
      this.formItem.enable();
    }
    _.forEach(this.listCharacteristic, (item) => {
      item.disabled = this.screen === this.screenType.detail;
      item.content = '';
    });
    this.uploadedFiles = [];
  }

  cancel() {
    this.screen = this.screenType.detail;
    this.resetForm();
    if (this.selected.length > 0) {
      this.mapDataFormItem(this.selected[0]);
    }
  }

  create() {
    if (this.screen !== this.screenType.create) {
      this.selected = [];
      this.screen = this.screenType.create;
      this.resetForm();
    }
  }

  update() {
    if (_.get(this.selected, '[0].approveStatus') === '1') {
      this.confirmService.warn('Không được phép sửa khi đang Chờ phê duyệt!');
      return;
    }
    this.screen = this.screenType.update;
    this.formItem.enable();
    this.formItem.controls.approveStatus.disable();
    this.formItem.controls.reasonOfDisapproved.disable();
    _.forEach(this.listCharacteristic, (item) => {
      item.disabled = false;
    });
  }

  delete() {
    if (_.get(this.selected, '[0].approveStatus') === '1') {
      this.confirmService.warn('Bản ghi đang được trình ký. Không được phép xóa!');
      return;
    }
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.delete(this.selected[0]?.id).subscribe(
          () => {
            this.selected = [];
            this.searchProductChildren();
            this.messageService.success(this.notificationMessage.success);
          },
          (e) => {
            this.isLoading = false;
            if (_.get(e, 'error.code', '').includes('.203')) {
              this.messageService.error('Bản ghi đang được trình ký. Không được phép xóa!');
            } else {
              this.messageService.error(this.notificationMessage.error);
            }
          }
        );
      }
    });
  }

  sendApprove() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.send(this.selected[0]?.id).subscribe(
          () => {
            this.searchProductChildren();
            this.messageService.success(this.notificationMessage.success);
          },
          (e) => {
            this.isLoading = false;
            if (_.get(e, 'error.code', '').includes('.203')) {
              this.messageService.error(
                'Không được phép gửi phê duyệt khi chưa soạn thảo chỉnh sửa hoặc đã gửi phê duyệt!'
              );
            } else {
              this.messageService.error(this.notificationMessage.error);
            }
          }
        );
      }
    });
  }

  approved() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.approve(this.selected[0]?.id).subscribe(
          () => {
            this.searchProductChildren();
            this.messageService.success(this.notificationMessage.success);
          },
          (e) => {
            this.isLoading = false;
            if (_.get(e, 'error.code', '').includes('.203')) {
              this.messageService.error(
                `${this.titleByGroup} '${this.selected[0].code}-${this.selected[0].name}' đã được phê duyệt/ từ chối. Không được phép thực hiện phê duyệt.`
              );
            } else {
              this.messageService.error(this.notificationMessage.error);
            }
          }
        );
      }
    });
  }

  refuse() {
    const modalConfirm = this.modalService.open(ConfirmAssignmentModalComponent, {
      windowClass: 'confirm-approved-modal',
    });
    modalConfirm.componentInstance.isApproved = false;
    modalConfirm.componentInstance.maxlength = 1000;
    modalConfirm.result
      .then((res) => {
        if (res) {
          this.isLoading = true;
          this.service.refuse(this.selected[0]?.id, res.note).subscribe(
            () => {
              this.searchProductChildren();
              this.messageService.success(this.notificationMessage.success);
            },
            (e) => {
              this.isLoading = false;
              if (_.get(e, 'error.code', '').includes('.203')) {
                this.messageService.error(
                  `${this.titleByGroup} '${this.selected[0].code}-${this.selected[0].name}' đã được phê duyệt/ từ chối. Không được phép thực hiện từ chối`
                );
              } else {
                this.messageService.error(this.notificationMessage.error);
              }
            }
          );
        }
      })
      .catch();
  }

  uploadFile(event, fileUpload) {
    const strType = 'doc, docx, xls, xlsx, txt, pdf, png, jpg, bmp, rar, zip';
    for (const file of event.files) {
      const type = _.last(file.name?.split('.'))?.toLowerCase();
      if (file.size > 10485760 || !strType.includes(type)) {
        this.messageService.error('File không đúng định dạng/File vượt quá giới hạn cho phép');
        fileUpload.clear();
        return;
      }
      const fileOld = _.find(
        this.uploadedFiles,
        (item) =>
          item?.file?.lastModified === file.lastModified && item?.fileName === file.name && item?.size === file.size
      );
      if (!fileOld) {
        this.uploadedFiles.push({
          fileId: null,
          fileName: file.name,
          size: file.size,
          idFake: _.size(this.uploadedFiles),
          file,
        });
      }
    }
    fileUpload.clear();
  }

  removeFile(fileItem) {
    _.remove(this.uploadedFiles, (item) => item.idFake === fileItem.idFake);
  }

  checkBtnCreate() {
    return (
      _.isEmpty(this.form.controls.division.value) ||
      _.isEmpty(_.trim(this.form.controls.group.value)) ||
      _.isEmpty(_.trim(this.form.controls.productId.value)) ||
      this.screen !== this.screenType.detail
    );
  }

  downloadFile(item) {
    if (item.fileId && this.screen === this.screenType.detail) {
      this.isLoading = true;
      this.fileService.downloadFile(item.fileId, item.fileName).subscribe((res) => {
        this.isLoading = false;
        if (!res) {
          this.messageService.error(this.notificationMessage.error);
        }
      });
    }
  }

  showStatus() {
    return (
      this.screen === this.screenType.detail &&
      !_.isEmpty(this.formItem.controls.approveStatus.value) &&
      this.formItem.controls.approveStatus.value !== '0'
    );
  }
}
