import { CategoryDeclareComponent } from './category-declare/category-declare.component';
import { CharacteristicDeclareComponent } from './characteristic-declare/characteristic-declare.component';
import { InformationDeclareComponent } from './information-declare/information-declare.component';

export * from './category-declare/category-declare.component';
export * from './characteristic-declare/characteristic-declare.component';
export * from './information-declare/information-declare.component';

export const COMPONENTS = [CategoryDeclareComponent, CharacteristicDeclareComponent, InformationDeclareComponent];
