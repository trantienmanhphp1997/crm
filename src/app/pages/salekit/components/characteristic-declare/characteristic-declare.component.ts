import { FunctionCode, maxInt32, SessionKey } from 'src/app/core/utils/common-constants';
import { Component, Injector, OnInit } from '@angular/core';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { catchError } from 'rxjs/operators';
import { CategoryDeclareService } from '../../services/category-declare.service';
import { global } from '@angular/compiler/src/util';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { Validators } from '@angular/forms';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { of } from 'rxjs';
import { CharacteristicModel, GroupInfoModel } from '../../models';

@Component({
  selector: 'characteristic-declare',
  templateUrl: './characteristic-declare.component.html',
  styleUrls: ['./characteristic-declare.component.scss'],
})
export class CharacteristicDeclareComponent extends BaseComponent implements OnInit {
  constructor(injector: Injector, private categoryDeclareService: CategoryDeclareService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.SALEKIT_CHARACTERISTIC_DECLARE}`);
  }
  limit = global.userConfig.pageSize;
  form = this.fb.group({
    division: '',
    group: '',
    product: '',
  });
  formItem = this.fb.group({
    saleKitCategoryAttributeId: '',
    saleKitCategoryAttributeName: ['', CustomValidators.required],
    numberOrder: ['', [Validators.min(0), CustomValidators.onlyNumber]],
    isCompare: [false],
  });
  listDivision = [];
  listGroup: GroupInfoModel[] = [];
  listProduct = [];
  listCharacteristic = [];
  pageable: Pageable;
  paramSearchProduct = {
    page: 0,
    size: maxInt32,
    saleKitFunctionId: '',
    bizLine: '',
  };
  paramSearchCharacteristict = {
    page: 0,
    size: this.limit,
    saleKitCategoryId: '',
  };
  screen = this.screenType.detail;
  selected = [];
  titleGroup = '';

  ngOnInit(): void {
    const list = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM);
    this.listDivision = _.map(list, (item) => {
      return { code: item.code, name: `${item.code} - ${item.name}` };
    });
    this.listDivision = _.orderBy(this.listDivision, ['name'], ['asc', 'desc']);
    this.form.get('division').setValue(_.first(this.listDivision)?.code);
    this.categoryDeclareService
      .getGroupInfo()
      .pipe(catchError(() => of([])))
      .subscribe((data) => {
        this.listGroup = data;
        this.form.get('group').setValue(_.first(this.listGroup)?.code);
      });
  }

  ngAfterViewInit() {
    this.form.controls.division.valueChanges.subscribe((value) => {
      if (!this.isLoading) {
        this.screen = this.screenType.detail;
        this.getListProduct();
        this.resetForm();
        this.selected = [];
      }
    });
    this.form.controls.group.valueChanges.subscribe((value) => {
      this.titleGroup = _.find(this.listGroup, (item) => item.code === value)?.name;
      this.screen = this.screenType.detail;
      this.getListProduct();
      this.resetForm();
      this.selected = [];
    });
    this.form.controls.product.valueChanges.subscribe((value) => {
      this.screen = this.screenType.detail;
      this.search();
      this.selected = [];
    });
  }

  onActive(event) {
    if (event.type === 'click') {
      this.screen = this.screenType.detail;
      const data = { ...event.row };
      this.resetForm();
      this.mapDataFormItem(data);
    }
  }

  mapDataFormItem(data: CharacteristicModel) {
    if (data) {
      this.formItem.patchValue(data);
    }
  }

  cancel() {
    this.screen = this.screenType.detail;
    this.resetForm();
    if (this.selected.length > 0) {
      this.mapDataFormItem(this.selected[0]);
    }
  }

  create() {
    if (this.screen !== this.screenType.create) {
      this.selected = [];
      this.screen = this.screenType.create;
      this.resetForm();
    }
  }

  update() {
    this.screen = this.screenType.update;
    this.formItem.enable();
  }

  resetForm() {
    this.formItem.reset();
    if (this.screen === this.screenType.detail) {
      this.formItem.disable();
    } else {
      this.formItem.enable();
    }
  }

  deleteProduct() {
    const param: CharacteristicModel = {
      saleKitCategoryAttributeId: this.formItem.get('saleKitCategoryAttributeId').value,
      saleKitCategoryId: this.form.get('product').value,
      saleKitCategoryAttributeName: this.formItem.get('saleKitCategoryAttributeName').value,
      isActive: false,
    };
    this.confirmService.confirm().then((res) => {
      if (res) {
        this.isLoading = true;
        this.categoryDeclareService.createCharacteristic(param).subscribe(
          () => {
            this.messageService.success(this.notificationMessage.success);
            this.selected = [];
            this.search();
          },
          (e) => {
            this.isLoading = false;
            this.messageService.error(this.notificationMessage.error);
          }
        );
      }
    });
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearchCharacteristict.page = pageInfo.offset;
    this.search();
  }

  saveProduct() {
    if (this.formItem.valid) {
      this.confirmService.confirm().then((res) => {
        if (res) {
          this.isLoading = true;
          const data: CharacteristicModel = {
            saleKitCategoryId: this.form.get('product').value,
            saleKitCategoryAttributeName: this.formItem.get('saleKitCategoryAttributeName').value.trim(),
            saleKitCategoryAttributeId: this.formItem.get('saleKitCategoryAttributeId').value,
            numberOrder: parseInt(this.formItem.get('numberOrder').value),
            isCompare: this.formItem.get('isCompare').value,
          };
          if (_.isEmpty(_.trim(data.saleKitCategoryAttributeId))) {
            delete data.saleKitCategoryAttributeId;
          }
          this.categoryDeclareService.createCharacteristic(data).subscribe(
            (result) => {
              if (result === null) {
                this.messageService.success(this.notificationMessage.success);
                this.screen = this.screenType.detail;
                this.search();
              }
            },
            (e) => {
              this.isLoading = false;
              this.messageService.error(this.notificationMessage.error);
              return;
            }
          );
        }
      });
    } else {
      validateAllFormFields(this.formItem);
    }
  }

  getListProduct() {
    this.isLoading = true;
    this.paramSearchProduct.saleKitFunctionId = this.form.get('group').value;
    this.paramSearchProduct.bizLine = this.form.get('division').value;
    this.categoryDeclareService.searchProduct(this.paramSearchProduct).subscribe(
      (result) => {
        if (result) {
          this.listProduct = result.rows;
          this.listProduct =
            this.listProduct.map((item) => {
              return { code: item.saleKitCategoryId, name: item.saleKitCategoryName };
            }) || [];
          this.form.get('product').setValue(_.first(this.listProduct)?.code);
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  search() {
    this.isLoading = true;
    this.paramSearchCharacteristict.saleKitCategoryId = this.form.get('product').value;
    this.categoryDeclareService.searchCharacteristic(this.paramSearchCharacteristict).subscribe(
      (data) => {
        this.listCharacteristic = data.rows;
        const index = _.findIndex(
          this.listCharacteristic,
          (item) => item.saleKitCategoryAttributeId === this.selected[0]?.saleKitCategoryAttributeId
        );
        if (index !== -1) {
          this.selected = [this.listCharacteristic[index]];
        } else if (this.listCharacteristic[0]) {
          this.selected = [this.listCharacteristic[0]];
        }
        this.resetForm();
        this.mapDataFormItem(this.selected[0]);
        this.pageable = {
          totalElements: data.totalElements,
          totalPages: data.totalPages,
          currentPage: data.currentPage,
          size: data.size,
        };
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  checkBtnCreate() {
    return (
      _.isEmpty(this.form.controls.division.value) ||
      _.isEmpty(_.trim(this.form.controls.group.value)) ||
      _.isEmpty(_.trim(this.form.controls.product.value)) ||
      this.screen !== this.screenType.detail
    );
  }
}
