export interface GroupInfoModel {
  code: number;
  name: string;
  type: string;
}
