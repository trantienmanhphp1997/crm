export interface CategoryModel {
  saleKitCategoryId: number;
  saleKitCategoryName: string;
  saleKitFunctionId: number;
  numberOrder: number;
  isActive?: boolean;
  bizLine: string;
  description: string;
}
