export interface SaleKitInformationModel {
  id: string;
  code: string;
  name: string;
  shortName: string;
  startDate: number;
  endDate?: number;
  approveStatus: string;
  isNotification?: boolean;
  reasonOfDisapproved?: string;
  description: string;
  saleKitCategoryId: string;
  attributes: Attribute[];
  attachments: Attachment[];
}

export interface Attribute {
  attributeId: number;
  name: string;
}

export interface Attachment {
  fileId: string;
  name: string;
}
