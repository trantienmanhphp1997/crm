export interface CharacteristicModel {
  saleKitCategoryId: number;
  saleKitCategoryAttributeName: string;
  saleKitCategoryAttributeId: number;
  numberOrder?: number;
  isActive?: boolean;
  isCompare?: boolean;
}
