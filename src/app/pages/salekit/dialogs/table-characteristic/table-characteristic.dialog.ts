import { global } from '@angular/compiler/src/util';
import { Component, Injector, Input, OnInit, ViewEncapsulation, HostBinding } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';
import { CategoryDeclareService } from '../../services/category-declare.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'table-characteristic',
  templateUrl: './table-characteristic.dialog.html',
  styleUrls: ['./table-characteristic.dialog.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TableCharacteristicDialog extends BaseComponent implements OnInit {
  constructor(
    injector: Injector,
    private modalActive: NgbActiveModal,
    private categoryDeclareService: CategoryDeclareService
  ) {
    super(injector);
    this.isLoading = true;
  }
  @HostBinding('class.table-characteristic-content') appRightContent = true;
  limit = global.userConfig.pageSize;
  listCharacteristic = [];
  @Input() listSelected = [];
  @Input() saleKitCategoryId: any;
  pageable: Pageable;
  isCheckAll = false;
  params = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };
  paramSearchCharacteristic = {
    page: 0,
    size: this.limit,
    saleKitCategoryId: '',
  };

  ngOnInit() {
    this.paramSearchCharacteristic.saleKitCategoryId = this.saleKitCategoryId;
    if (this.listSelected) {
      this.categoryDeclareService
        .searchCharacteristic(this.paramSearchCharacteristic)
        .pipe(catchError(() => of(undefined)))
        .subscribe((data) => {
          this.listCharacteristic = data.content || [];
          _.forEach(this.listCharacteristic, (item) => {
            item.checked =
              _.findIndex(this.listSelected, (i) => i.saleKitCategoryAttributeId === item.saleKitCategoryAttributeId) >
              -1;
          });
          this.isLoading = false;
        });
    }
  }

  search(isSearch?: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.paramSearchCharacteristic.page = 0;
    }
    this.categoryDeclareService
      .searchCharacteristic(this.paramSearchCharacteristic)
      .pipe(catchError(() => of(undefined)))
      .subscribe((data) => {
        if (data) {
          console.log('aa', data);
          this.listCharacteristic = data.content || [];
          this.pageable = {
            totalElements: data.totalElements,
            totalPages: data.totalPages,
            currentPage: data.number,
            size: this.limit,
          };
        }

        this.isLoading = false;
      });
  }

  setPage(pageInfo) {
    if (this.saleKitCategoryId) {
      this.paramSearchCharacteristic.page = pageInfo.offset;
      this.search(false);
    }
  }

  onChangeIsCheckAll(event) {
    this.isCheckAll = !this.isCheckAll;
    _.forEach(this.listCharacteristic, (item) => {
      item.checked = this.isCheckAll;
      if (!this.isCheckAll) {
        _.remove(
          this.listSelected,
          (itemOld) => itemOld.saleKitCategoryAttributeId === item.saleKitCategoryAttributeId
        );
      }
    });
  }

  onChangeChooseItem(event, item) {
    item.checked = event.checked;
    if (event.checked) {
      this.listSelected.push(item);
    } else {
      _.remove(this.listSelected, (itemOld) => itemOld.saleKitCategoryAttributeId === item.saleKitCategoryAttributeId);
    }
  }

  onChangeIsCompare(event, item) {
    item.isCompare = event.checked;
    const itemSelected = _.find(
      this.listSelected,
      (itemOld) => itemOld.saleKitCategoryAttributeId === item.saleKitCategoryAttributeId
    );
    itemSelected.isCompare = item.isCompare;
  }

  choose() {
    this.modalActive.close(this.listSelected);
  }

  closeModal() {
    this.modalActive.close(false);
  }
}
