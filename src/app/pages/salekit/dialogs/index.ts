import { TableCharacteristicDialog } from './table-characteristic/table-characteristic.dialog';

export * from './table-characteristic/table-characteristic.dialog';

export const DIALOGS = [TableCharacteristicDialog];
