import {BaseComponent} from '../../../../core/components/base.component';
import {AfterViewInit, Component, Injector, OnInit} from '@angular/core';
import {ExtensionsService} from "../../services/extensions.service";
import {forkJoin, of} from "rxjs";
import {
  CampaignStatus,
  CommonCategory,
  FunctionCode,
  Scopes,
  SessionKey
} from "../../../../core/utils/common-constants";
import {catchError} from "rxjs/operators";
import {global} from "@angular/compiler/src/util";
import * as moment from "moment";
import {Division} from 'src/app/core/utils/common-constants';
import * as _ from 'lodash';
import {FileService} from "../../../../core/services/file.service";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-extensions-salekit-view',
  templateUrl: './extensions-salekit-view.component.html',
  styleUrls: ['./extensions-salekit-view.component.scss'],
})

export class ExtensionsSalekitViewComponent extends BaseComponent implements OnInit, AfterViewInit {
  listFunction = [];
  listCategoryProduct = [];
  listProduct = [];
  tabIndex: number;
  tabObject: any;
  product: any;
  salekitType: string;
  categoryId: number;
  firstProductId: number;
  listDivision = [];
  customerType = '';
  currentProductId: number;
  prevParam: any;
  isBack = false;

  constructor(injector: Injector,
              private extensionsService: ExtensionsService,
              private fileService: FileService,
              private sanitized: DomSanitizer) {
    super(injector);
    this.isLoading = true;
    //  this.prevParam = this.router.getCurrentNavigation()?.extras?.state;
    this.prevParam = this.sessionService.getSessionData(FunctionCode.EXTENSIONS);
    console.log('param: ', this.prevParam);
    if (this.prevParam) {
      this.categoryId = Number(this.prevParam.categoryId);
      this.salekitType = this.prevParam.salekitType;
      this.customerType = this.prevParam.block;
      this.currentProductId = this.prevParam.compareProductId;
      this.isBack = true;
    }
  }

  ngAfterViewInit(): void {

  }

  ngOnInit(): void {
    this.tabIndex = 0;
    const listDivision = _.map(this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM), (item) => {
      return {
        titleCode: item.titleCode,
        levelCode: item.levelCode,
        code: item.code,
        displayName: `${item.code} - ${item.name}`,
      };
    });
    this.listDivision = listDivision;
    if (!this.customerType) {
      this.customerType = this.listDivision[0].code;
    }
    if (this.prevParam) {
      this.initData();
    } else {
      this.initData();
    }
  }

  initData() {
    this.isLoading = true;
    this.product = null;
    forkJoin([

      this.extensionsService.getListFunction('', this.customerType).pipe(catchError((e) => of(undefined))),

    ]).subscribe(([lstFunctions]) => {
      console.log('function1: ', lstFunctions);
      this.listFunction = lstFunctions?.functions || [];
      if (!this.isBack) {
        this.salekitType = _.head(this.listFunction)?.type || '';
      }
      this.tabIndex = _.findIndex(this.listFunction, (i) => (this.salekitType === i?.type));
      this.getProductInfo(this.salekitType, this.customerType, this.isBack);
      // this.extensionsService.getCategoryProduct( this.salekitType,  this.customerType)
      //   .subscribe((response: any) => {
      //     this.listCategoryProduct = response?.categories;
      //     console.log('category_product: ', this.listCategoryProduct);
      //
      //   //  this.listProduct =_.head(this.listCategoryProduct)?.products || [];
      //     if(!isBack){
      //       this.categoryId = _.head(this.listCategoryProduct)?.id || '';
      //     }
      //     this.listProduct = [];
      //     this.listCategoryProduct.forEach((i) => {
      //       if(i.id === this.categoryId){
      //         this.listProduct =  i?.products;
      //       }
      //     });
      //     if(!_.isEmpty(this.listProduct)){
      //       this.firstProductId = _.head(this.listProduct)?.id || '';
      //       let productId = this.firstProductId;
      //       if(this.currentProductId && isBack){
      //         productId = this.currentProductId;
      //       }
      //       this.extensionsService.getProductDetail(productId)
      //         .subscribe((result: any) => {
      //           this.product = result;
      //           console.log('product: ', this.product);
      //           this.isLoading = false;
      //         }, (er) => {
      //           this.isLoading = false;
      //           this.messageService.error(this.notificationMessage.error);
      //         });
      //     }
      //     else{
      //       this.isLoading = false;
      //     }
      //   }, (e) => {
      //     this.isLoading = false;
      //     this.messageService.error(this.notificationMessage.error);
      //   });
    }, (err) => {
      this.isLoading = false;
      this.messageService.error(this.notificationMessage.error);
    });
  }

  getProductInfo(type, block, isBack) {
    this.isLoading = true;
    this.extensionsService.getCategoryProduct(type, block)
      .subscribe((response: any) => {
        this.listCategoryProduct = response?.categories;
        console.log('category_product: ', this.listCategoryProduct);

        //  this.listProduct =_.head(this.listCategoryProduct)?.products || [];
        if (!isBack) {
          this.categoryId = _.head(this.listCategoryProduct)?.id || '';
        }
        this.listProduct = [];
        this.listCategoryProduct.forEach((i) => {
          if (i.id === this.categoryId) {
            this.listProduct = i?.products;
            return;
          }
        });
        if (!_.isEmpty(this.listProduct)) {
          this.firstProductId = _.head(this.listProduct)?.id || '';
          let productId = this.firstProductId;
          if (this.currentProductId && isBack) {
            productId = this.currentProductId;
          }
          this.extensionsService.getProductDetail(productId)
            .subscribe((result: any) => {
              const dataRes = result;
              dataRes.attributes = dataRes.attributes.map(att => {
                if(att.content){
                  const textContent = att?.content.replace(/(?:^|<\/pre>)[^]*?(?:<pre>|$)/g, function(m) {
                    return m.replace(/[\n\t]+/g, '');
                  });
                  return {...att, content: this.sanitized.bypassSecurityTrustHtml(textContent)}
                }
              });
              this.product = dataRes;
              console.log('product: ', this.product);
              this.isLoading = false;
            }, (er) => {
              this.isLoading = false;
              this.messageService.error(this.notificationMessage.error);
            });
          const data = {
            compareProductId: this.product.id,
            productId: this.firstProductId,
            categoryId: this.categoryId,
            salekitType: this.salekitType,
            block: this.customerType
          }
          this.sessionService.setSessionData(FunctionCode.EXTENSIONS, data);
        } else {
          this.isLoading = false;
        }
      }, (e) => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      });
  }

  onTabChanged(e) {
    console.log('change tab');
    //  this.isLoading = true;
    this.tabIndex = e?.index;
    this.listCategoryProduct = [];
    this.listProduct = [];
    this.product = null;
    this.tabObject = this.listFunction[this.tabIndex];
    this.salekitType = this.tabObject?.type;
    this.getProductInfo(this.tabObject?.type, this.customerType, this.isBack);
    if (this.isBack) {
      this.isBack = false;
    }
    // this.extensionsService.getCategoryProduct(this.tabObject?.type,this.customerType)
    //   .subscribe((response: any) => {
    //     this.listCategoryProduct = response?.categories;
    //     this.listProduct =_.head(this.listCategoryProduct)?.products || [];
    //     this.categoryId = _.head(this.listCategoryProduct)?.id || '';
    //     if(!_.isEmpty(this.listProduct)){
    //       this.firstProductId = _.head(this.listProduct)?.id || '';
    //       this.extensionsService.getProductDetail(_.head(this.listProduct)?.id)
    //         .subscribe((result: any) => {
    //           this.product = result;
    //           this.isLoading = false;
    //         });
    //     }
    //     else{
    //       this.isLoading = false;
    //     }
    //   });
  }

  getCategoryProduct(item: any) {
    this.isLoading = true;
    this.listProduct = [];
    this.product = null;
    this.listProduct = item?.products;
    this.categoryId = item.id;
    this.firstProductId = item?.products[0].id;
    this.extensionsService.getProductDetail(_.head(this.listProduct)?.id)
      .subscribe((result: any) => {
        const dataRes = result;
        dataRes.attributes = dataRes.attributes.map(att => {
          if(att.content){
            const textContent = att?.content.replace(/(?:^|<\/pre>)[^]*?(?:<pre>|$)/g, function(m) {
              return m.replace(/[\n\t]+/g, '');
            });
            return {...att, content: this.sanitized.bypassSecurityTrustHtml(textContent)}
          }
        });
        this.product = dataRes;
        this.isLoading = false;
        const data = {
          compareProductId: this.product.id,
          productId: this.firstProductId,
          categoryId: this.categoryId,
          salekitType: this.salekitType,
          block: this.customerType
        }
        this.sessionService.setSessionData(FunctionCode.EXTENSIONS, data);
      });
  }

  viewDetailProduct(item) {
    this.isLoading = true;

    if (item) {
      this.extensionsService.getProductDetail(item?.id)
        .subscribe((response: any) => {
          const dataRes = response;
          dataRes.attributes = dataRes.attributes.map(att => {
            if(att.content){
              const textContent = att?.content.replace(/(?:^|<\/pre>)[^]*?(?:<pre>|$)/g, function(m) {
                return m.replace(/[\n\t]+/g, '');
              });
              return {...att, content: this.sanitized.bypassSecurityTrustHtml(textContent)}
            }
          });
          this.product = dataRes;
          this.isLoading = false;
          console.log('product: ', response);
          const data = {
            compareProductId: this.product.id,
            productId: this.firstProductId,
            categoryId: this.categoryId,
            salekitType: this.salekitType,
            block: this.customerType
          }
          this.sessionService.setSessionData(FunctionCode.EXTENSIONS, data);
        });
    }
  }

  compareProduct(product) {
    const data = {
      compareProductId: product.id,
      productId: this.firstProductId,
      categoryId: this.categoryId,
      salekitType: this.salekitType,
      block: this.customerType
    }
    this.sessionService.setSessionData(FunctionCode.EXTENSIONS, data);
    this.router.navigate(['/extensions/salekit/compareDetail'], {
      state: data
    });
  }

  isViewCompareButton() {
    let isView = false;
    if (this.listProduct.length > 0) {
      const listAttribute = this.product?.attributes;
      if (!listAttribute || listAttribute.length < 0) {
        return isView;
      }
      listAttribute.forEach((i) => {
        if (i?.isCompare) {
          isView = true;
          return;
        }
      });
    }
    return isView;
  }

  downloadFile(item) {
    if (item.fileId) {
      this.isLoading = true;
      if (item.fileId) {
        this.isLoading = true;
        this.fileService.downloadFile(item.fileId, item.name).subscribe((res) => {
          this.isLoading = false;
          if (!res) {
            this.messageService.error(this.notificationMessage.error);
          }
        });
      }
    }
  }

  changeBlock(event) {
    this.customerType = event.value;
    this.initData();
  }
}
