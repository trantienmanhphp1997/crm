import {BaseComponent} from '../../../../core/components/base.component';
import {AfterViewInit, Component, Injector, OnInit} from '@angular/core';
import {ExtensionsService} from '../../services/extensions.service';
import {forkJoin, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {FunctionCode} from '../../../../core/utils/common-constants';
import * as _ from 'lodash';
import {DomSanitizer} from "@angular/platform-browser";


@Component({
  selector: 'app-extensions-compare-product',
  templateUrl: './extensions-compare-product.component.html',
  styleUrls: ['./extensions-compare-product.component.scss'],
})

export class ExtensionsCompareProductComponent extends BaseComponent implements OnInit, AfterViewInit {
  productId: number;
  categoryId: number;
  compareProductId: number;
  salekitType: string;
  block: string;
  listProduct = [];
  productFirst: any;
  productSecond: any;
  category: any;
  productItems = [];
  prevParam: any;
  constructor(injector: Injector, private extensionsService: ExtensionsService, private sanitized: DomSanitizer) {
    super(injector);
    this.prevParam = this.router.getCurrentNavigation()?.extras?.state;
    if(this.prevParam){
      this.productId = this.prevParam.productId;
      this.categoryId = Number(this.prevParam.categoryId);
      this.salekitType = this.prevParam.salekitType;
      this.block = this.prevParam.block;
      this.compareProductId = this.prevParam.compareProductId;
    }
  }

  ngAfterViewInit(): void {
  }

  ngOnInit(): void {
    this.isLoading = true;
    forkJoin([
      this.extensionsService.getCategoryProduct(this.salekitType, this.block).pipe(catchError((e) => of(undefined))),
      this.extensionsService.getProductDetail(this.compareProductId).pipe(catchError((e) => of(undefined))),
      this.extensionsService.getProductDetail(this.productId).pipe(catchError((e) => of(undefined))),
    ]).subscribe(([lstProduct, compareProduct, product]) => {
      this.listProduct = lstProduct?.categories || [];
      this.category = _.filter(this.listProduct, (i) => i.id === this.categoryId);
      this.productFirst = this.modifyProduct(compareProduct);
      this.productSecond = this.modifyProduct(product);
      this.productItems = this.category[0].products;
      this.isLoading = false;
    });
  }
  viewProductFirst(item){
    if(item){
      this.isLoading = true;
      this.extensionsService.getProductDetail(item?.id)
        .subscribe((response: any) => {
          this.productFirst = this.modifyProduct(response);
    //      this.compareProductId = item?.id;
          this.isLoading = false;
        } );
    }
  }
  modifyProduct(product){
    product.attributes = product.attributes.map(att => {
      if(att.content){
        const textContent = att?.content.replace(/(?:^|<\/pre>)[^]*?(?:<pre>|$)/g, function(m) {
          return m.replace(/[\n\t]+/g, '');
        });
        return {...att, content: this.sanitized.bypassSecurityTrustHtml(textContent)}
      }
      else{
        return {...att};
      }
      //return {...att};
    });
    // product.attributes.forEach((att) => {
    //   if(att?.content){
    //         att?.content.replace(/(?:^|<\/pre>)[^]*?(?:<pre>|$)/g, function(m) {
    //           return m.replace(/[\n\t]+/g, '');
    //         });
    //         console.log('att?.content: ',att?.content);
    //   }
    // })
    // console.log('check product: ', product);
     return product;
  }
  viewProductSecond(item){
    if(item){
      this.isLoading = true;
      this.extensionsService.getProductDetail(item?.id)
        .subscribe((response: any) => {
          this.productSecond = this.modifyProduct(response);
    //      this.productId = item?.id;
          this.isLoading = false;
        } );
    }
  }
  back(){
    const data = {
      compareProductId: this.compareProductId,
      productId: this.productId,
      categoryId: this.categoryId,
      salekitType: this.salekitType,
      block:this.block
    }
    console.log('backData: ', data);
    this.router.navigate(['/extensions/salekit'], {
      state: data
    });
  }
}
