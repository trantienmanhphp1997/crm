import {HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {Observable, Observer} from "rxjs";

@Injectable({
  providedIn: 'root',
})
export class ExtensionsService{
  constructor(private http: HttpClient) {}

  getListFunction(saleKitType,bizLine): Observable<any>{
    return this.http.get(`${environment.url_endpoint_sale}/sale-kit/functions?saleKitType=${saleKitType}&bizLine=${bizLine}`);
  }

  getCategoryProduct(saleKitType,bizLine): Observable<any>{
    return this.http.get(`${environment.url_endpoint_sale}/sale-kit/categories-products?saleKitType=${saleKitType}&bizLine=${bizLine}`, );
  }

  getProductDetail(productId): Observable<any>{
    return this.http.get(`${environment.url_endpoint_sale}/sale-kit/product-detail?productId=${productId}`);
  }

  downloadFile(id): Observable<any>{
    return this.http.get(`${environment.url_endpoint_rm}/files/download/${id}`);
  }

}
