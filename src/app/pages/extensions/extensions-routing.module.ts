import {RouterModule, Routes} from '@angular/router';
import {FunctionCode, Scopes} from '../../core/utils/common-constants';
import {RoleGuardService} from '../../core/services/role-guard.service';
import {ExtensionsSalekitViewComponent} from './components/extensions-salekit-view/extensions-salekit-view.component';
import {NgModule} from '@angular/core';
import {Customer360INDIVDetailComponent} from "../customer-360/components";
import {ExtensionsCompareProductComponent} from "./components/extensions-compare-product/extensions-compare-product.component";

const routes: Routes= [
  {
    path: 'salekit',
    data: {
      code: FunctionCode.EXTENSIONS
    },
    canActivateChild: [RoleGuardService],
    children: [
      {
        path: '',
        component: ExtensionsSalekitViewComponent,
        data: {
          scope: Scopes.VIEW,
        }
      },
      {
        path: 'compareDetail',
        component: ExtensionsCompareProductComponent,
        data: {
          scope: Scopes.VIEW,
        },
      }
    ],

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
  }
)

export  class ExtensionsRoutingModule{
}
