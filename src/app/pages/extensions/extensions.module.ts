import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {ExtensionsRoutingModule} from './extensions-routing.module';
import {MatTabsModule} from '@angular/material/tabs';
import {ExtensionsSalekitViewComponent} from './components/extensions-salekit-view/extensions-salekit-view.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {TranslateModule} from '@ngx-translate/core';
import {CommonModule} from "@angular/common";
import {ExtensionsCompareProductComponent} from "./components/extensions-compare-product/extensions-compare-product.component";
import { MatMenuModule } from '@angular/material/menu';
import {FileUploadModule} from "primeng/fileupload";
import {DropdownModule} from "primeng/dropdown";
import {FormsModule} from "@angular/forms";
import {SharedModule} from "../../shared/shared.module";


const COMPONENTS = [
  ExtensionsSalekitViewComponent,
  ExtensionsCompareProductComponent
];

@NgModule({
  declarations: [...COMPONENTS],
    imports: [
        ExtensionsRoutingModule,
        MatTabsModule,
        NgxDatatableModule,
        TranslateModule,
        CommonModule,
        MatMenuModule,
        FileUploadModule,
        DropdownModule,
        FormsModule,
        SharedModule,
    ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA]
})

export class ExtensionsModule{}
