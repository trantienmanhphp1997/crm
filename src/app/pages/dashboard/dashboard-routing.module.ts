import { DashboardLeftViewComponent } from './components/dashboard-left-view/dashboard-left-view.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardViewComponent } from './containers/dashboard-view/dashboard-view.component';
import {DashboardManageCardComponent} from './components/manage-card/dashboard-manage-card/dashboard-manage-card.component';
import {FunctionCode, Scopes} from '../../core/utils/common-constants';
import { AppActiveChartDetailComponent } from './components/app-active-chart-detail/app-active-chart-detail.component';
import { AppInitService } from 'src/app/core/services/app-init-data.service';
import { ChartCampaignResultDetailComponent } from './components/chart-campaign-result-detail/chart-campaign-result-detail.component';
import {ChartCampaignStageDetailComponent} from "./components/chart-campaign-stage-detail/chart-campaign-stage-detail";

const routes: Routes = [
  {
    path: '',
    component: DashboardLeftViewComponent,
    outlet: 'app-left-content',
  },
  { path: '', component: DashboardViewComponent, canActivate: [AppInitService] },
  {
    path: 'manage-card',
    component: DashboardManageCardComponent,
    data: {
      scope: Scopes.VIEW,
      code: FunctionCode.DASHBOARD_MANAGE_CARD
    },
  },
  {
    path: 'app-active-chart-detail',
    component: AppActiveChartDetailComponent,
    data: {
      scope: Scopes.VIEW,
      code: FunctionCode.APP_ACTIVE_CHART_DETAIL
    },
  },
    {
      path: 'chart-campaign-result-detail',
      component: ChartCampaignResultDetailComponent,
      data: {
        scope: Scopes.VIEW,
        code: FunctionCode.CHART_CAMPAIGN_RESULT_DETAIL
      },
    },
    {
      path: 'chart-campaign-stage-detail',
      component: ChartCampaignStageDetailComponent,
      data: {
        scope: Scopes.VIEW,
        code: FunctionCode.CHART_CAMPAIGN_STAGE_DETAIL
      },
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
