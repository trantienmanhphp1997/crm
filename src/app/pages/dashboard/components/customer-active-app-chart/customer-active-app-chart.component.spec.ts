import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerActiveAppChartComponent } from './customer-active-app-chart.component';

describe('CustomerBscoreChartComponent', () => {
  let component: CustomerActiveAppChartComponent;
  let fixture: ComponentFixture<CustomerActiveAppChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerActiveAppChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerActiveAppChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
