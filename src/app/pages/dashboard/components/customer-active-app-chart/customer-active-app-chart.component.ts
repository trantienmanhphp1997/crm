import { Component, OnInit, Injector, Input, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import {CustomerType, EChartType, FunctionCode, Scopes} from 'src/app/core/utils/common-constants';
import { percentage } from 'src/app/core/utils/function';
import { EChartsOption } from 'echarts';
import { DatePipe, formatNumber } from '@angular/common';
import _ from 'lodash';
import * as moment from 'moment';
import {CustomerApi} from '../../../customer-360/apis';

@Component({
  selector: 'app-customer-active-app-chart',
  templateUrl: './customer-active-app-chart.component.html',
  styleUrls: ['./customer-active-app-chart.component.scss'],
  providers: [DatePipe],
})
export class CustomerActiveAppChartComponent extends BaseComponent implements OnInit, OnChanges {
  isPieChart = true;
  listBranchCode = [];
  @Input() viewChange: EventEmitter<boolean> = new EventEmitter();
  data: any[];
  input: any;
  dataPercentage: string[] = [];
  option: EChartsOption;
  title = 'fields.appactive_chart';
  note: string;
  dateSnapshot: string;
  dataTotal: number

  isClick = false;
  isBack = false;
  prevParams: any;
  preClickData: any;

  constructor(injector: Injector, private datePipe: DatePipe,private customerApi: CustomerApi) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DASHBOARD_CUSTOMER}`);

  }

  ngOnInit(): void {
    this.isLoading = true;
    this.listBranchCode = this.input?.branchCodes || []
    const params = {
      customerTypes: [CustomerType.INDIV, CustomerType.NHS],
      rsId: this.objFunction?.rsId,
      scope: Scopes.VIEW,
      branchCodes: this.listBranchCode
    };

    this.customerApi.getAppActiveDataChart(params).subscribe(data => {
      this.data = data?.data;
      this.dateSnapshot = data?.snapshotTime
      if (_.size(this.data) > 0) {
        const sum = _.reduce(
          _.map(this.data, (x) => x.value),
          (a, c) => {
            return a + c;
          }
        );
        this.dataTotal = sum
        _.forEach(this.data, (item) => {
          this.dataPercentage.push(percentage(item.value || 0, sum));
        });
        this.handleMapData();
      }
      this.isLoading = false;
    });
  }

  ngOnChanges(changes: SimpleChanges) {

  }

  onClickChart(data, chart?) {
    if (this.dataTotal && this.dataTotal <= 0)
    {
      return;
    }
    this.goReport({data})
  }

  handleMapData() {
    if (_.size(this.data) > 0) {
      this.option = {
        tooltip: {
          trigger: 'item',
          formatter: (params) => {
            return `${formatNumber(params.value || 0, 'en', '1.0-0')} (${params.percent})%`;
          },
        },
        legend: {
          show: false,
          bottom: 0,
        },
        series: [
          {
            // bottom: '40%',
            type: EChartType.Pie,
            radius: ['30%', '80%'],
            labelLine: {
              length: 5,
            },
            label: {
              formatter: '{d}%',
            },
            data: [],
          },
        ],
      };
      _.forEach(this.data, (item, index) => {
        if (item.value > 0) {
          this.option.series[0].data.push({
            value: item.value,
            name: item.label,
            itemStyle: { color: item.color },
            code: item.code,
            indexItem: index
          });
        }
      });
    } else {
      this.data = undefined;
    }
  }

  goReport(params = {}) {
    const dataSend = {
      branch: this.listBranchCode,
        ...params
    };
    this.router.navigate(['/dashboard/app-active-chart-detail'], {
      state: dataSend
    });
  }
}
