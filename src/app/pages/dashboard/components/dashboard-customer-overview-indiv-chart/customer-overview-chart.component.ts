import {
  Component,
  Input,
  OnInit,
  EventEmitter,
  ViewEncapsulation,
  Injector
} from '@angular/core';
import { EChartsOption } from 'echarts';
import * as _ from 'lodash';
import {CommonCategory, FunctionCode, functionUri, Scopes} from 'src/app/core/utils/common-constants';
import {DashboardService} from '../../services/dashboard.service';
import {AppFunction} from '../../../../core/interfaces/app-function.interface';
import {SessionService} from '../../../../core/services/session.service';
import {forkJoin, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {BaseComponent} from '../../../../core/components/base.component';
import {formatNumber} from '@angular/common';

@Component({
  selector: 'customer-overview-chart',
  templateUrl: './customer-overview-chart.component.html',
  styleUrls: ['./customer-overview-chart.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CustomerOverviewChartComponent extends BaseComponent implements OnInit {
  @Input() viewChange: EventEmitter<boolean> = new EventEmitter();
  @Input() data: any;
  dataChart: any[];
  input: any;
  isLoading = false;
  isTable = false;
  option: EChartsOption;
  objFunction: AppFunction;
  chartColors = ['rgb(255, 169, 90)', 'rgb(88, 207, 141)', 'rgb(158, 179, 248)'];
  commonData = {
    segment : [],
    groupCustomer: [
      { value: 'ALL', name: 'Tất cả'},
      { value: '1M', name: 'KH mới 1 tháng'}
    ]
  };
  segment = '';
  groupCustomer = 'ALL';

  constructor(
    injector: Injector,
    private service: DashboardService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DASHBOARD_CUSTOMER}`);
  }

  ngOnInit(): void {
    this.isLoading = true;
    forkJoin([
      this.commonService.getCommonCategory('DASHBOARD_INDIV_SEGMENT').pipe(catchError(() => of(undefined))),
    ]).subscribe(([segment]) => {
      this.commonData.segment = [...this.commonData.segment, ..._.get(segment, 'content')];
      this.segment = _.first(this.commonData.segment)?.value;
      this.search();
    });
  }

  search() {
    this.isLoading = true;
    const params = {
      segment: [this.segment],
      activeFlag: this.groupCustomer,
      rsId: this.objFunction?.rsId,
      scope: 'VIEW',
      branchCodes: this.input?.branchCodes || []
    }

    this.service.getDataChartsOverviewCustomer(params).subscribe((dataChart) => {
    this.data.data = _.get(dataChart,'khachHang');
    this.data.totalCustomer = _.get(dataChart,'totalCustomer');
    this.data.dsnapshotDate = _.get(dataChart,'dsnapshotDate');
    this.mapChart();
    this.isLoading = false;
    }, (err) => {
        this.isLoading = false;
        this.messageService.error(err?.error?.description);
      });
  }

  mapChart() {
    if (_.isEmpty(this.data?.data)) {
      return;
    }
    this.option = {
      grid: {
        left: '12%',
        right: '10%'
      },
      color: this.chartColors,
      legend: {
        show: true,
        selectedMode: false,
        bottom: 0
      },
      tooltip: {
        trigger: 'item',
        axisPointer: {
          type: 'shadow'
        },
      },
      xAxis: [
        {
          nameTextStyle: {
            fontSize: 15,
            fontWeight: 'bold',
            fontFamily: 'Lato',
            verticalAlign: 'top',
            padding: 16
          },
          nameGap: -10,
          offset: 10,
          name: 'Độ tuổi',
          type: 'category',
          axisTick: {
            alignWithLabel: true
          },
          data: this.getColumnChartByFieldName('ageGroup')
        }
      ],
      yAxis: [
        {
          nameTextStyle: {
            fontSize: 15,
            fontWeight: 'bold',
            fontFamily: 'Lato',
          },
          type: 'value',
          name: 'Số lượng KH',
          position: 'left',
          alignTicks: true,
          axisLine: {
            show: true,
            lineStyle: {
            }
          },
          axisLabel: {
            formatter: '{value}'
          }

        },
        {
          type: 'value',
          name: 'Số lượng khách hàng active',
          show: false,
          position: 'right',
          alignTicks: true,
          offset: 80,
          axisLabel: {
            formatter: '{value}'
          }
        },
        {
          nameTextStyle: {
            fontSize: 15,
            fontWeight: 'bold',
            fontFamily: 'Lato',
          },
          type: 'value',
          name: 'Tỷ lệ',
          position: 'right',
          alignTicks: true,
          max: 100,
          min: 0,
          axisLine: {
            show: true,
          },
          axisLabel: {
            formatter: (params) => {
              return `${formatNumber(params || 0, 'en', '1.0-0')}%`;
            },
          }
        }
      ],
      series: [
        {
          name: 'Số lượng khách hàng',
          barGap: 0.05,
          type: 'bar',
          data: this.getColumnChartByFieldName('customer')
        },
        {
          name: 'Số lượng khách hàng active',
          type: 'bar',
          yAxisIndex: 0,
          data: this.getColumnChartByFieldName('activeCustomer')
        },
        {
          name: 'Tỷ lệ active(%)',
          type: 'line',
          yAxisIndex: 2,
          data: this.getColumnChartByFieldName('activeRateCustomer', true)
        }
      ]
    };
  }

  switchChart() {
    if (this.isLoading) {
      return;
    }
    this.isTable = !this.isTable;
  }

  getColumnChartByFieldName(fieldName: string, fomatNumber = false) {
    const data =
      fomatNumber ?
      _.map(this.data.data, i => {
          return formatNumber(_.get(i, fieldName) || 0, 'en', '1.0-0')
        })
      : _.map(this.data.data, fieldName);
    return data;
  }

  formatNumber(value) {
    return formatNumber(value || 0, 'en', '1.0-0');
  }


}
