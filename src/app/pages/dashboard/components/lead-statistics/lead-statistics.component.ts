import { global } from '@angular/compiler/src/util';
import { Component, Input, OnInit, EventEmitter, OnDestroy } from '@angular/core';
import { DisplayGrid, GridsterConfig, GridsterItem, GridsterItemComponentInterface, GridType } from 'angular-gridster2';
import { Subscription } from 'rxjs';
import { Roles } from 'src/app/core/utils/common-constants';
import { getRole } from 'src/app/core/utils/function';
import { DashboardService } from '../../services/dashboard.service';

@Component({
  selector: 'app-lead-statistics',
  templateUrl: './lead-statistics.component.html',
  styleUrls: ['./lead-statistics.component.scss'],
})
export class LeadStatisticsComponent implements OnInit, OnDestroy {
  @Input() widget: GridsterItem;
  @Input() resizeEvent: EventEmitter<GridsterItemComponentInterface>;
  @Input() viewChange: EventEmitter<boolean>;
  @Input() role: string;
  @Input() branchCode: string;
  resizeSub: Subscription = new Subscription();
  lanes = {
    new: { total: 0, name: 'Chưa liên hệ' },
    waiting: { total: 0, name: 'Đang liên hệ' },
    unqualified: { total: 0, name: 'Không có nhu cầu' },
    qualified: { total: 0, name: 'Có nhu cầu' },
  };
  options: GridsterConfig;
  dashboard: Array<GridsterItem>;
  isLoading = false;

  constructor(private dashboardService: DashboardService) {
    this.role = getRole(global.roles);
  }

  ngOnInit(): void {
    this.options = {
      gridType: GridType.VerticalFixed,
      displayGrid: DisplayGrid.None,
      margin: 20,
      outerMargin: true,
      outerMarginTop: 0,
      outerMarginRight: 0,
      outerMarginBottom: 0,
      outerMarginLeft: 0,
      mobileBreakpoint: 640,
      minCols: 1,
      minRows: 1,
      maxCols: 4,
      maxRows: 4,
      defaultItemCols: 1,
      defaultItemRows: 1,
      fixedRowHeight: 105,
      draggable: {
        enabled: false,
        dragHandleClass: 'drag-handler',
      },
      resizable: {
        delayStart: 0,
        enabled: false,
        handles: {
          s: true,
          e: true,
          n: true,
          w: true,
          se: true,
          ne: true,
          sw: true,
          nw: true,
        },
      },
    };
    this.dashboard = [
      {
        cols: 1,
        rows: 1,
        y: 0,
        x: 0,
        total: 0,
        name: 'Chưa liên hệ',
        icon: 'las la-list-alt',
        iconBackground: 'grey',
      },
      {
        cols: 1,
        rows: 1,
        y: 0,
        x: 1,
        total: 0,
        name: 'Đang liên hệ',
        icon: 'las la-history',
        iconBackground: 'yellow',
      },
      {
        cols: 1,
        rows: 1,
        y: 0,
        x: 2,
        total: 0,
        name: 'Không có nhu cầu',
        icon: 'las la-check-circle',
        iconBackground: 'red',
      },
      {
        cols: 1,
        rows: 1,
        y: 0,
        x: 3,
        total: 0,
        name: 'Có nhu cầu',
        icon: 'las la-folder-open',
        iconBackground: 'blue',
      },
    ];
    this.getSlideBar();
    this.resizeSub.add(
      this.resizeEvent.subscribe((itemComponent) => {
        if (itemComponent.item.component === this.widget.component) {
          // console.log(itemComponent);
        }
      })
    );
    this.resizeSub.add(
      this.viewChange.subscribe((change) => {
        if (change && this.options.api && this.options.api.resize) {
          console.log(change);
          setTimeout(() => {
            this.options.api.resize();
          }, 200);
        }
      })
    );
    // if (this.role !== Roles.RGM) {
    //   this.dashboardService.onSlidebar().subscribe((res) => {
    //     if (res && res.branchCode === this.branchCode) {
    //       this.getSlideBar();
    //     }
    //   });
    // } else {
    //   setInterval(() => {
    //     this.getSlideBar();
    //   }, 90000);
    // }
  }

  getSlideBar() {
    this.isLoading = true;
    this.dashboardService.getSlideBarData().subscribe(
      (result) => {
        if (result) {
          result.forEach((item) => {
            if (item.status && this.lanes[item.status.toLowerCase()]) {
              this.lanes[item.status.toLowerCase()].total = item.totalByStatus;
            }
          });
          this.isLoading = false;
        }
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnDestroy() {
    this.resizeSub.unsubscribe();
  }
}
