import { Component, OnInit, Injector, Input, EventEmitter, OnChanges } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CommonCategory, EChartType, FunctionCode } from 'src/app/core/utils/common-constants';
import { EChartsOption } from 'echarts';
import { formatNumber } from '@angular/common';
import { percentage } from 'src/app/core/utils/function';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { catchError } from 'rxjs/operators';
import { forkJoin, of } from 'rxjs';

@Component({
  templateUrl: './customer-common-chart.component.html',
  styleUrls: ['./customer-common-chart.component.scss'],
})
export class CustomerCommonChartComponent extends BaseComponent implements OnInit, OnChanges {
  @Input() viewChange: EventEmitter<boolean> = new EventEmitter();
  @Input('data') dataChart: any;
  data: any[];
  dataPercentage: any[] = [];
  option: EChartsOption;
  title: string;
  nullData: boolean;

  constructor(injector: Injector, private categoryService: CategoryService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.isLoading = false;
    // DASHBOARD_CUSTOMER_STATUS
    // SEGMENT_CUSTOMER_CONFIG
    // CUSTOMER_OBJECT_CONFIG
  }

  ngOnInit(): void { }

  ngOnChanges() {
    let sum = 0;
    if (this.dataChart?.data.length > 0) {
      sum = this.dataChart?.data
        ?.map((i) => i.value || 0)
        .reduce((a, c) => {
          return a + c;
        }) || 0;
    }

    this.dataChart?.data?.forEach((item) => {
      this.dataPercentage.push(percentage(item.value || 0, sum));
    });
    this.data = this.dataChart?.data ? [...this.dataChart?.data] : [];
    this.title = this.dataChart?.title;
    this.nullData = this.data.every(item => !item.value);
    this.handleMapData();
  }

  handleMapData() {
    if (this.data?.length > 0) {
      this.option = {
        tooltip: {
          trigger: 'item'
        },
        // legend: {
        //   bottom: '10%',
        //   left: 'center',
        //   // doesn't perfectly work with our tricks, disable it
        //   selectedMode: false,
        //   width: "2%",
        // },
        series: [
          {
            name: this.title,
            type: 'pie',
            radius: ['35%', '70%'],
            center: ['50%', '50%'],
            // adjust the start angle
            startAngle: 180,
            itemStyle: {
              borderRadius: 0,
              borderColor: '#fff',
              borderWidth: 2
            },
            label: {
              show: true,
              formatter(param) {
                // correct the percentage
                return param.percent + '%';
              }
            },
            data: [
              // { value: 1048, name: 'Search Engine' },
              // { value: 735, name: 'Direct' },
              // { value: 580, name: 'Email' },
              // { value: 484, name: 'Union Ads' },

            ]
          }
        ]
      };
      this.data?.forEach((item) => {
        if (item.value > 0) {
          this.option.series[0].data.push(item);
        }
      });
    } else {
      this.data = undefined;
    }
  }

}
