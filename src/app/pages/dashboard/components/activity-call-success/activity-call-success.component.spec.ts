import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityCallSuccessComponent } from './activity-call-success.component';

describe('ActivityCallSuccessComponent', () => {
  let component: ActivityCallSuccessComponent;
  let fixture: ComponentFixture<ActivityCallSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityCallSuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityCallSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
