import { Component, Input, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { GridsterItem, GridsterItemComponentInterface } from 'angular-gridster2';
import { Subscription } from 'rxjs';
import { Roles } from 'src/app/core/utils/common-constants';
import { DashboardService } from '../../services/dashboard.service';

@Component({
  selector: 'app-opportunity-won-report',
  templateUrl: './opportunity-won-report.component.html',
  styleUrls: ['./opportunity-won-report.component.scss'],
})
export class OpportunityWonReportComponent implements OnInit, OnDestroy {
  @Input() widget: GridsterItem;
  @Input() resizeEvent: EventEmitter<GridsterItemComponentInterface>;
  @Input() role: string;
  @Input() branchCode: string;
  resizeSub: Subscription;
  opportunitesWon: any;
  isLoading = false;

  constructor(private dashboardService: DashboardService) {}

  ngOnInit(): void {
    this.resizeSub = this.resizeEvent.subscribe((itemComponent) => {
      if (itemComponent.item.component === this.widget.component) {
        // console.log(itemComponent);
      }
    });
    this.isLoading = true;
    this.dashboardService.getOpportunityReport().subscribe(
      (result) => {
        if (result) {
          this.opportunitesWon = result;
          this.isLoading = false;
        }
      },
      () => {
        this.isLoading = false;
      }
    );
    // if (this.role !== Roles.RGM) {
    //   this.dashboardService.onUpdateData().subscribe((res) => {
    //     if (res && res.branchCode === this.branchCode) {
    //       this.getData();
    //     }
    //   });
    // } else {
    //   setInterval(() => {
    //     this.getData();
    //   }, 90000);
    // }
  }

  getData() {
    this.dashboardService.getOpportunityReport().subscribe((result) => {
      if (result) {
        this.opportunitesWon = result;
      }
    });
  }

  ngOnDestroy() {
    this.resizeSub.unsubscribe();
  }
}
