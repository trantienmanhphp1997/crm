import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpportunityWonReportComponent } from './opportunity-won-report.component';

describe('OpportunityWonReportComponent', () => {
  let component: OpportunityWonReportComponent;
  let fixture: ComponentFixture<OpportunityWonReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpportunityWonReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpportunityWonReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
