import { Component, OnInit, Injector, Input, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import {CustomerType, EChartType, FunctionCode, Scopes} from 'src/app/core/utils/common-constants';
import { EChartsOption } from 'echarts';
import * as _ from 'lodash';
import {DashboardService} from '../../services/dashboard.service';
import {formatDate, formatNumber} from '@angular/common';
import * as echarts from "echarts";

@Component({
  selector: 'app-card-chart',
  templateUrl: './card-chart.component.html',
  styleUrls: ['./card-chart.component.scss'],
})
export class CardChartComponent extends BaseComponent implements OnInit, OnChanges {
  isPieChart = true;
  @Input() viewChange: EventEmitter<boolean> = new EventEmitter();
  @Input() data: any;
  dataPercentage: any[] = [];
  option: EChartsOption;
  listBranchCode = [];
  objFunction1 : any;
  private myChart: any = null;
  preClickData: any;
  HIGHTLIGHT = 'highlight';
  DOWNPLAY = 'downplay';
  constructor(injector: Injector,private service: DashboardService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.objFunction1 = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DASHBOARD_MANAGE_CARD}`);
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.search();
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  search() {
    const params = {
      rsId: this.objFunction?.rsId,
      scope: Scopes.VIEW,
      branch: _.isEmpty(this.data?.branchCodes) ? [] : this.data?.branchCodes.toString()
    };
    this.listBranchCode = this.data?.branchCodes;
    this.service.getDataChartsCreditCard(params).subscribe((data) => {
      this.data = data;
      this.handleMapData();
      this.isLoading = false;
    }, (err) => {
      this.isLoading = false;
      this.messageService.error(err?.error?.description);
    })
  }

  handleMapData() {
    let index = 0;
    if (this.data) {
      this.option = {
        emphasis: {
          itemStyle: {
            borderDashOffset: 30,
          },
        },
        tooltip: {
          trigger: 'item',
          formatter: (params) => {
            return `${params.name}: <b>${this.formatNumber(params.value)}</b>`;
          },
        },
        legend: {
          show: false,
          bottom: 0,
        },
        series: [
          {
            startAngle: 170,
            type: EChartType.Pie,
            radius: ['30%', '65%'],
            itemStyle: {
              borderColor: '#fff',
              borderWidth: 2,
            },
            labelLine: {
              length: 5,
            },
            label: {
              show: false,
              formatter: '{c}',
              position: 'inner',
              fontSize: 15,
              fontWeight: 700,
              lineHeight: 24
            },
            data: [],
          },
          {
            emphasis: {
              scaleSize: 13,
              itemStyle: {
                borderWidth: 8,
              }
            },
            startAngle: 170,
            type: EChartType.Pie,
            itemStyle: {
              borderColor: '#fff',
              borderWidth: 2
            },
            radius: ['65%', '100%'],
            label: {
              position: 'inner',
              formatter: (params) => {
                return params.value !== 0 ? `${this.formatNumber(params.value)}` : ''
              },
              fontSize: 10,
              fontWeight: 550,
              lineHeight: 12,
              align: 'center'
            },
            labelLine: {
              length: 30
            },
            data: []
          }
        ],
      };
      this.data?.data?.forEach((item) => {
        if (item.value <= 0) {
          return;
        }
        this.option.series[0].data.push({
          value: item.value,
          name: item.name,
          itemStyle: { color: item.color },
        });
        item.detail?.forEach((itemDetail) => {
          if(itemDetail.value > 0){
            this.option.series[1].data.push({
              value: itemDetail.value,
              name: itemDetail.name,
              itemStyle: { color: itemDetail.color },
              code: itemDetail.code,
              indexItem: index
            });
            itemDetail.indexItem = index;
            index ++;
          }
        });
      });

    } else {
      this.data = undefined;
    }
  }

  goReport() {
    const dataSend = {
      branch: this.listBranchCode
    };
    this.router.navigate(['/dashboard/manage-card'], {
      state: dataSend
    });
  }

  onClickChart(data, chart?) {
    if(chart && chart?.componentIndex === 0){
      return;
    }
    if(this.objFunction1 && this.objFunction1.view){
      const dataSend = {
        data: this.data,
        dataChoose: data,
        branch: this.listBranchCode
      };
      this.router.navigate(['/dashboard/manage-card'], {
        state: dataSend
      });
    }
    else{
      if(this.preClickData){
        this.checkViewDashboard(this.preClickData.indexItem,this.DOWNPLAY);
      }
      if(this.preClickData?.code === data.code ){
        this.preClickData = null;
      }
      else{
          this.preClickData = data;
          if(data.value > 0){
            this.checkViewDashboard(data.indexItem,this.HIGHTLIGHT);
          }
      }
    }
  }
  checkViewDashboard(index, typeAction) {
    const myChart = echarts.init((document.getElementById('chartId')) as any);
    if (!myChart) {
      return;
    }
    if (!this.myChart) {
      this.myChart = myChart;
    }
    //
    // myChart.on('rendered', () => {
    //   console.log('rendered')
    // })

    this.myChart.dispatchAction({
      type: typeAction,
      seriesIndex: 1,
      dataIndex: index
    });
  }
  formatNumber(value){
    return formatNumber(value || 0, 'en', '0.0-2')
  }
}
