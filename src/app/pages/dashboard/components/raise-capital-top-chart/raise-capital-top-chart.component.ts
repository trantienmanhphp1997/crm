import { formatNumber } from '@angular/common';
import { Component, Input, OnInit, EventEmitter, ViewEncapsulation, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { EChartsOption } from 'echarts';
import * as _ from 'lodash';
import { functionUri } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'raise-capital-top-chart',
  templateUrl: './raise-capital-top-chart.component.html',
  styleUrls: ['./raise-capital-top-chart.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class RaiseCapitalTopChartComponent implements OnInit, OnChanges {
  @Input() viewChange: EventEmitter<boolean> = new EventEmitter();
  @Input() data: any[];
  dataChart: any[];
  isLoading = false;
  isTable = false;
  option: EChartsOption;

  constructor(private router: Router) {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges) {
    if (_.size(this.data) > 0) {
      this.data = [...this.data];
      this.dataChart = [...this.data];
    } else {
      this.data = undefined;
      this.dataChart = undefined;
    }
    this.mapChart();
  }

  mapChart() {
    const data = _.reverse(this.dataChart);
    const businessDate = _.get(data, '[0].businessDate');
    if (_.size(data) > 0) {
      const length = _.size(this.data) % 2 ? 9 : 10;
      for (let index = 0; index < (length - _.size(this.data)) / 2; index++) {
        data.push({ code: '', name: '', value: null, businessDate: businessDate });
        data.unshift({ code: '', name: '', value: null, businessDate: businessDate });
      }
    }
    this.option = {
      grid: {
        top: 8,
        bottom: 8,
        left: 165,
        right: 100,
      },
      xAxis: {
        type: 'value',
        show: false,
      },
      yAxis: {
        type: 'category',
        offset: 10,
        data: _.map(data, (item) => item.code || ''),
        axisTick: {
          show: false,
        },
        axisLine: {
          onZeroAxisIndex: 1,
        },
        axisLabel: {
          show: true,
          formatter: (value, index) => {
            return this.getCustomerCode(data, index) + '\n{code|' + value + '}';
          },
          rich: {
            code: {
              color: '#bbbbfd',
            },
          },
        },
        triggerEvent: true,
      },
      series: [
        {
          type: 'bar',
          label: {
            show: true,
            position: 'right',
            formatter: (params: any) => {
              return formatNumber(parseFloat(params.data), 'en', '0.0-0');
            },
          },
          color: '#bbbbfd',
          data: _.map(data, (item) => item.value),
          barWidth: 30,
          barGap: '120%',
        },
      ],
    };
  }

  switchChart() {
    if (this.isLoading) {
      return;
    }
    this.isTable = !this.isTable;
  }

  getCustomerCode(data, index) {
    return _.get(data, `[${index}].name`, '');
  }

  onChartClick(event) {
    if (event.componentType === 'yAxis' && event.type === 'click' && event.value) {
      const customerType = _.find(this.data, (item) => item.code === event.value)?.customerType?.toLowerCase();
      this.router.navigateByUrl(`${functionUri.customer_360_manager}/detail/${customerType}/${event.value}`);
    }
  }
}
