import { RouterStateSnapshot } from '@angular/router';
import { division } from './../../common';
import { Component, Injector, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import { DashboardService } from 'src/app/pages/dashboard/services/dashboard.service';
import { functionUri } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent extends BaseComponent implements OnInit {
  @Input() data: any;
  dateNow = null;
  lastDay = null;
  lastMonth = null;
  lastYear = null;
  datatable: any;
  id: number;
  page = {
    curPage: 1,
    offset: 0,
    pageSize: 10,
    rowCount: 0
  };
 
  colsTable = [
    { header: "1", },
    { header: "2", },
    { header: "3", },
    { header: "4", },
    { header: "5", },
    { header: "6", },
    { header: "7", },
    { header: "8", },
    { header: "9", },
    { header: "10",},
    { header: "12",},
    { header: "12",},
  ]
  dataView = [];
  constructor(private dashboardService: DashboardService, injector: Injector,) {
    super(injector)
  }

  ngOnInit() {
    this.getDataById(this.data.id);
    this.id = this.data.id;
  }

  async getDataById(id) {
    this.isLoading = true;
    const body = {
      rmCode: this.data?.dataForm?.isRegion ? 'ALL' : this.data?.dataForm?.rmCode,
      branchCodecap2: this.data?.dataForm?.branchCode ? this.data?.dataForm?.branchCode :this.data.listBranch,
      lobNM: this.data?.dataForm?.divisionCode,
    }
    if (id == 1) {
      this.datatable = await this.dashboardService.businessDashboardTopLoan(body).pipe(catchError(() => of(undefined))).toPromise();
    }
    if (id == 2) {
      this.datatable = await this.dashboardService.businessDashboardTopDepo(body).pipe(catchError(() => of(undefined))).toPromise();;
    }
    if (id == 3) {
      this.datatable = await this.dashboardService.businessDashboardTopMd(body).pipe(catchError(() => of(undefined))).toPromise();;
    }
    this.isLoading = false;
    this.datatable.data = this.datatable.data.map((el, index) => {
      el.index = index;
      return el;
    });
    const D_SNAPSHOT_DT = this.datatable?.data[0]?.D_SNAPSHOT_DT;
    this.buildDateTitle(D_SNAPSHOT_DT);
    this.page.rowCount = this.datatable?.data.length;
    this.dataView = this.datatable.data.slice(0, this.page.pageSize);
  }

  handleRowClick(id) {
    // this.router.navigate(['customer360/customer-manager'], {
    //   queryParams: {
    //     id,
    //     division: 'SME'
    //   },
    // });
    localStorage.setItem('DETAIL_CUS_ID', JSON.stringify(id));
    localStorage.setItem('BLOCK_CODE_OPP', JSON.stringify('SME'));
    const url = this.router.serializeUrl(
      this.router.createUrlTree([functionUri.customer_360_manager], {
        skipLocationChange: true,
      })
    );
    window.open(url, '_blank');
  }

  changePage(ev) {
    this.dataView = this.datatable.data.slice((ev.page - 1) * (this.page.pageSize), ev.page * this.page.pageSize);
  }

  buildDateTitle(D_SNAPSHOT_DT) {
    if (!D_SNAPSHOT_DT) {
      return;
    }
    this.dateNow = moment(D_SNAPSHOT_DT, 'DD/MM/YYYY').format('DD/MM/YYYY');
    this.lastDay = `${moment(moment(D_SNAPSHOT_DT, 'DD/MM/YYYY').subtract(1, 'day').calendar('DD/MM/YYYY')).format('DD/MM/YYYY')}`;
    this.lastMonth = `${moment(moment(D_SNAPSHOT_DT, 'DD/MM/YYYY').subtract(1, 'month').calendar('DD/MM/YYYY')).format('MM/YYYY')}`;
    this.lastYear = `${moment(moment(D_SNAPSHOT_DT, 'DD/MM/YYYY').subtract(1, 'year').calendar('DD/MM/YYYY')).format('YYYY')}`;
  }
}
