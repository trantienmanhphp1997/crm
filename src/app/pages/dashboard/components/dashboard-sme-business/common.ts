import * as moment from "moment"

export const getTitle = (data, periods) => {
    return data.map(element => {
        if(periods == 'DAY'){
            return element.D_SNAPSHOT_DT;
        }
        if(periods == 'WEEK'){
            return `${ moment(element.D_SNAPSHOT_DT,'DD/MM/YYYY').startOf('week').add(1, 'day').format('DD/MM')}-${ moment(element.D_SNAPSHOT_DT,'DD/MM/YYYY').endOf('week').format('DD/MM')}`
        }
        if (periods == 'MONTH'){
            return moment(element.D_SNAPSHOT_DT,'DD/MM/YYYY').format('MM/YYYY');
        }
        if (periods == 'YEAR'){
            return moment(element.D_SNAPSHOT_DT,'DD/MM/YYYY').format('YYYY');
        }
    });
}

export const division = (value) => {
    return +value / 1000000000;
}