import { Division } from './../../../../../../core/utils/common-constants';
import { Component, Injector, Input, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { DashboardService } from 'src/app/pages/dashboard/services/dashboard.service';
import { division, getTitle } from '../../common';

@Component({
  selector: 'app-HDV',
  templateUrl: './HDV.component.html',
  styleUrls: ['./HDV.component.scss']
})
export class HDVComponent extends BaseComponent implements OnInit {
  @Input() data: any;

  option: any;
  options = [];
  form = this.fb.group({
    period: 'ALL',
  });
  isLoading: boolean;
  codeColor = {
    up: '#91CC75',
    down: '#EE6666'
  }

  constructor(injector: Injector, private dashboardService: DashboardService) {
    super(injector);
  }

  ngOnInit() {
    this.form.valueChanges.subscribe(res => {
      this.getData();
    });
    this.options = this.data.category[3].content.map(item => {
      return {
        code: item.code,
        name: item.name
      }
    });
    this.getData();
  }

  buildOptions(data) {
    if (!data || !data.length) {
      this.option = null;
      return;
    }
    this.option = {
      // title: {
      //   text: 'Income of Germany and France since 1950'
      // },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
          crossStyle: {
            color: '#999'
          }
        },
        appendToBody: true
      },
      xAxis: {
        type: 'category',
        data: getTitle(data, this.data.dataForm.periodsSME),
        axisTick: {
          alignWithLabel: true
        },
        axisPointer: {
          type: 'shadow'
        },
        axisLabel: {
          fontSize: 9,
        }
      },
      yAxis: [
        {
          type: 'value',
          axisLabel: {
            formatter: '{value}'
          },
          show: false

        }
      ],
      legend: {
        data: ['HĐV TĐ (tỷ đồng)', 'Tăng/giảm NET (tỷ đồng)']
      },
      series: [
        {
          name: 'HĐV TĐ (tỷ đồng)',
          type: 'line',
          data: data.map(el => (+el.BAL_DEPO_T / 1000000000).toFixed(2)),
          tooltip: {
            valueFormatter: function (value) {
              return value as number;
            }
          },
          label: {
            show: true,
            position: 'top',
          }
        },
        {
          name: 'Tăng/giảm NET (tỷ đồng)',
          type: 'bar',
          data: data.map(el => {
            return {
              value: (+el.NET_DEPO / 1000000000).toFixed(2),
              itemStyle: {
                color: el.NET_DEPO > 0 ? this.codeColor.up : this.codeColor.down

              },
              // label: {
              //   show: true,
              //   position: el.NET_DEPO > 0 ? 'top' : 'bottom',
              // }
            }
          }),
          tooltip: {
            valueFormatter: function (value) {
              return value as number;
            }
          },
          barWidth: 40,
        }
      ]
    };
  }

  getData() {
    const form = { ...this.data.dataForm, ...this.form.getRawValue() };
    const body = {
      lobNM: form.divisionCode,
      "reportType": form?.isRegion ? 'BRANCH' : 'RM',
      "rmCode": form?.isRegion ? 'ALL' : form?.rmCode,
      "branchCodecap2": form?.branchCode ? form?.branchCode : this.data.listBranch,
      "timeType": form?.periodsSME,
      "pdType": form?.period,
      "debtGroup": null,
      "pdTerm": null,
      "productCode": null
    }
    this.isLoading = true
    this.dashboardService.smeBusinessDepoDashboard(body).subscribe((res: any) => {
      if (form?.periodsSME != 'DAY' && res?.data?.data.length > 4) {
        res.data.data.splice(0, 1);
      }
      this.buildOptions(res?.data?.data);
      this.isLoading = false
    }, error => {
      this.isLoading = false
    });
  }

}
