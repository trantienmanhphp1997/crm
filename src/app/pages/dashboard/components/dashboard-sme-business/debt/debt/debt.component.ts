import { catchError, filter, map } from 'rxjs/operators';
import { Component, Injector, Input, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { DashboardService } from 'src/app/pages/dashboard/services/dashboard.service';
import { of } from 'rxjs';
import { division, getTitle } from '../../common';
import { FormatNumberPipe } from 'src/app/shared/pipe/format-number.pipe';

@Component({
  selector: 'app-debt',
  templateUrl: './debt.component.html',
  styleUrls: ['./debt.component.scss']
})
export class DebtComponent extends BaseComponent implements OnInit {
  @Input() data: any;
  option: any;
  products = [];
  periods = [];
  debtGroups = [];
  form = this.fb.group({
    product: 'ALL',
    period: 'ALL',
    debtGroup: 'ALL'
  });
  isLoading: boolean;
  codeColor = {
    up: '#91CC75',
    down: '#EE6666'
  }
  constructor(injector: Injector, private dashboardService: DashboardService){
    super(injector);
  }

  ngOnInit() {
    this.mapCategory();
    this.form.valueChanges.subscribe(res => {
      this.getDatadebt();
    });
    this.getDatadebt();
  }

  mapCategory() {
    this.products = this.data.category[2].content.map(item => {
      return {
        value: item.value,
        name: item.name
      }
    })
    this.debtGroups = this.data.category[0].content.map(item => {
      return {
        value: item.value,
        name: item.name
      }
    })
    this.periods = this.data.category[1].content.map(item => {
      return {
        value: item.value,
        name: item.name
      }
    })
  }

  async getDatadebt() {
    const form = { ...this.data.dataForm, ...this.form.getRawValue() };

    const body = {
      lobNM: form.divisionCode,
      "reportType": form?.isRegion ? 'BRANCH' : 'RM',
      "rmCode": form?.isRegion ? 'ALL' : form?.rmCode,
      "branchCodecap2": form?.branchCode ? form?.branchCode : this.data.listBranch,
      "timeType": form?.periodsSME,
      "pdType": form?.product,
      "debtGroup": form?.debtGroup,
      "pdTerm": form?.period,
      "productCode": null // k can
    }

    // const body = {
    //   "reportType": "RM",
    //   "rmCode": "ALL",
    //   "branchCodecap2": "",
    //   "timeType": "DAY",
    //   "pdType": "ALL",
    //   "debtGroup": "ALL",
    //   "pdTerm": "ALL",
    //   "productCode": "ALL"
    // }

    this.isLoading = true;
    const res = await this.dashboardService.smeBusinessLoanDashboard(body).pipe(catchError(() => of(undefined))).toPromise();
    if (res?.data?.data) {
      if(form?.periodsSME != 'DAY' && res?.data?.data.length > 4){
        res.data.data.splice(0 , 1);
      }
      this.buildOption(res.data.data);
    }
    this.isLoading = false;
  }

  filterPeriods(index , periods) {
    if (periods != 'DAY'){
      return index < 4;
    }
    return true;
  }

  buildOption(data) {
    if(!data || !data.length){
      this.option = null;
      return;
    }
    this.option = {
      xAxis: {
        type: 'category',
        data: getTitle(data, this.data.dataForm.periodsSME),
        axisTick: {
          alignWithLabel: true
        },
        axisPointer: {
          type: 'shadow'
        },
        axisLabel: {
          fontSize: 9,
        }
      },
      legend: {
        data: ['Dư nợ TĐ (tỷ đồng)', 'Tăng/giảm NET (tỷ đồng)']
      },
      yAxis: [
        {
          type: 'value',
          axisLabel: {
            formatter: '{value}'
          },
          show: false
        }
      ],
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
          crossStyle: {
            color: '#999'
          }
        },
        appendToBody: true
      },
      series: [
        {
          tooltip: {
            valueFormatter: function (value) {
              return value as number;
            }
          },
          data: data.map(el =>{
            return {
              value: (+el.BAL_LOAN_T/1000000000).toFixed(2),
              itemStyle: {
                width: 10
              },
              label: {
                show: true,
                position: 'top',
              }
            }
          }),
          type: 'line',
          name: 'Dư nợ TĐ (tỷ đồng)',
        },
        {
          tooltip: {
            valueFormatter: function (value) {
              return value as number;
            }
          },
          data: data.map(el => {
            return {
              value: (+el.NET_LOAN/1000000000).toFixed(2),
              itemStyle: {
                color: el.NET_LOAN > 0 ? this.codeColor.up : this.codeColor.down
              },
            }
          }),
          type: 'bar',
          name: 'Tăng/giảm NET (tỷ đồng)',
          barWidth: 40,
        }
      ]
    };
  }

}
