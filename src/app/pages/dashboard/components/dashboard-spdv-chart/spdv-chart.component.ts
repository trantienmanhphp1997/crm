import {
  Component,
  Input,
  OnInit,
  EventEmitter,
  ViewEncapsulation,
  Injector
} from '@angular/core';
import * as _ from 'lodash';
import {CommonCategory, FunctionCode, functionUri, Scopes, SessionKey} from 'src/app/core/utils/common-constants';
import {DashboardService} from '../../services/dashboard.service';
import {AppFunction} from '../../../../core/interfaces/app-function.interface';
import {SessionService} from '../../../../core/services/session.service';
import {forkJoin, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {BaseComponent} from '../../../../core/components/base.component';
import {formatNumber} from '@angular/common';

@Component({
  selector: 'spdv-chart',
  templateUrl: './spdv-chart.component.html',
  styleUrls: ['./spdv-chart.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SpdvChartComponent extends BaseComponent implements OnInit {
  @Input() viewChange: EventEmitter<boolean> = new EventEmitter();
  @Input() data: any;
  dataChart: any[];
  input: any;
  isLoading = false;
  objFunction: AppFunction;
  commonData = {
    segment : [],
    groupCustomer: [
      { value: 'ALL', name: 'Tất cả'},
      { value: '1M', name: 'KH mới 1 tháng'}
    ],
    ageGroup: []
  };
  segment = '';
  groupCustomer = 'ALL';
  ageGroup = '';

  constructor(
    injector: Injector,
    private service: DashboardService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DASHBOARD_CUSTOMER}`);
    this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
  }

  ngOnInit(): void {
    this.isLoading = true;
    forkJoin([
      this.commonService.getCommonCategory('DASHBOARD_INDIV_SEGMENT').pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory('DASHBOARD_INDIV_AGE_GROUP').pipe(catchError(() => of(undefined))),
    ]).subscribe(([segment, ageGroup]) => {
      this.commonData.segment = [...this.commonData.segment, ..._.get(segment, 'content')];
      this.commonData.ageGroup = [...this.commonData.ageGroup, ..._.get(ageGroup, 'content')];
      this.segment = _.first(this.commonData.segment)?.value;
      this.ageGroup = _.get(_.find(this.commonData.ageGroup, (i) => i.isDefault), 'value');
      this.search();
    });
  }

  search() {
    this.isLoading = true;
    const params = {
      branch_code_cap2: this.currUser.branch,
      activeFlag: this.groupCustomer,
      rsId: this.objFunction?.rsId,
      ageGroup: this.ageGroup,
      scope: 'VIEW',
      segment: [this.segment],
      branchCodes: this.input?.branchCodes || []
    }
    this.service.getDataChartsSPDV(params).subscribe((dataChart) => {
      this.data.data = _.get(dataChart,'data');
      this.data.dsnapshotDate = _.get(dataChart,'dsnapshotDate');
      this.isLoading = false;
    }, (err) => {
      this.isLoading = false;
      this.messageService.error(err?.error?.description);
    });
  }

  formatNumber(value) {
    return formatNumber(value || 0, 'en', '1.0-0');
  }

  highlightTotal(row) {
    return {
      'text-hightligh': row.name.includes('TỔNG SLKH')
    };
  }
}
