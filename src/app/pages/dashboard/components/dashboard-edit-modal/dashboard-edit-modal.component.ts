import { componentDashboards } from 'src/app/core/utils/common-constants';
import { Component, HostBinding, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CompactType, DisplayGrid, GridsterConfig, GridsterItem, GridType } from 'angular-gridster2';
import { DashboardAddComponentModalComponent } from '../dashboard-add-component-modal/dashboard-add-component-modal.component';

@Component({
  selector: 'app-dashboard-edit-modal',
  templateUrl: './dashboard-edit-modal.component.html',
  styleUrls: ['./dashboard-edit-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DashboardEditModalComponent implements OnInit {
  @HostBinding('class.dashboard-edit-content') classHost = true;
  options: GridsterConfig;
  dashboard: Array<GridsterItem>;
  isLoading = false;
  data: any;

  constructor(private modalActive: NgbActiveModal, private modalService: NgbModal) {}

  ngOnInit(): void {
    this.options = {
      gridType: GridType.VerticalFixed,
      compactType: CompactType.CompactUp,
      margin: 10,
      mobileBreakpoint: 340,
      minCols: 1,
      minRows: 1,
      maxCols: 2,
      defaultItemCols: 1,
      defaultItemRows: 1,
      // fixedColWidth: 105,
      fixedRowHeight: 60,
      draggable: {
        delayStart: 0,
        enabled: true,
      },
      resizable: {
        enabled: false,
      },
      pushItems: true,
      displayGrid: DisplayGrid.None,
    };
    if (this.data) {
      this.dashboard = [];
      this.data.forEach((item) => {
        item.image = componentDashboards[item.component].image;
        this.dashboard.push(item);
      });
    }
  }

  closeModal() {
    this.modalActive.close(false);
  }

  save() {
    console.log(this.dashboard);
    this.modalActive.close(this.dashboard);
  }

  addItem() {
    const addComponent = this.modalService.open(DashboardAddComponentModalComponent, {
      windowClass: 'dashboard-add-component',
      scrollable: true,
    });
    addComponent.result
      .then((result) => {
        if (result) {
          this.dashboard.push({
            rows: result.rows,
            cols: result.cols,
            x: 0,
            y: 0,
            component: result.component,
            image: result.image,
          });
        }
      })
      .catch(() => {});
  }

  removeItem($event, item) {
    $event.preventDefault();
    $event.stopPropagation();
    this.dashboard.splice(this.dashboard.indexOf(item), 1);
  }
}
