import {
  Component,
  Input,
  OnInit,
  EventEmitter,
  ViewEncapsulation,
  OnChanges,
  SimpleChanges,
  Output,
  OnDestroy,
  HostListener,
} from '@angular/core';
import { Router } from '@angular/router';
import { EChartsOption } from 'echarts';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { SessionService } from 'src/app/core/services/session.service';
import { DashboardOtherIndiv, DashboardType, functionUri, SessionKey } from 'src/app/core/utils/common-constants';
import { DashboardService } from '../../services/dashboard.service';

@Component({
  selector: 'dashboard-business-chart-sme',
  templateUrl: './dashboard-business-chart-sme.component.html',
  styleUrls: ['./dashboard-business-chart-sme.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DashboardBusinessChartSMEComponent implements OnInit, OnChanges, OnDestroy {
  @Input() viewChange: EventEmitter<boolean> = new EventEmitter();
  @Input() data: any;
  @Output() onSomething = new EventEmitter<any>();
  dataChart: any[];
  isLoading = false;
  isTable = false;
  option: EChartsOption;
  title: string;
  isEmpty = true;
  listDataTable = [];
  listType = [
    {
      code: 'DAILY',
      name: 'Ngày',
    },
    {
      code: 'WEEKLY',
      name: 'Tuần',
    },
    {
      code: 'MONTHLY',
      name: 'Tháng',
    },
  ];
  today = `Hôm nay`;
  yesterday = `Hôm trước`;
  lastMonth = `Tháng trước`;
  subscription: Subscription;
  code: string;
  type: string;
  dashboardType = DashboardType;

  branchName = {
    [DashboardType.OTHER_INDIV_REVENUE]: {
      level_1: 'BRANCH_NAME_LV1',
      level_2: 'BRANCH_NAME'
    },
    [DashboardType.OTHER_INDIV_BUSINESS]: {
      level_1: 'PARENT_NAME',
      level_2: 'BRANCH_NAME'
    }
  }

  businessKeys = {
    [DashboardOtherIndiv.DU_NO]: { dataBarKey: 'DUNO_NET', dataLineKey: 'DUNO' },
    [DashboardOtherIndiv.HDV]: { dataBarKey: 'HDV_NET', dataLineKey: 'HUYDONG' },
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.setSmallDesktopOptions();
  }

  constructor(private service: DashboardService, private router: Router, private sessionService: SessionService) { }

  ngOnInit(): void {
    this.type = this.listType[0]?.code;
    this.subscription = this.viewChange.subscribe((data) => {
      if (data) {
        this.data = data[this.code];
        this.type = this.listType[0]?.code;
        this.changeData();
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.data) {
      this.code = this.data.key;
      this.changeData();
    }
  }

  changeData() {
    if (this.data) {
      this.title = this.data.title;
      this.isTable = this.data?.type === 'table';
      if (this.isTable) {
        this.listDataTable = _.get(this.data, 'dataTable', []);
        const dataDate = _.get(this.listDataTable, '[0].DATA_DATE', null);
        if (dataDate) {
          this.today = `Hôm nay(${moment(dataDate).format('D/M')})`;
          this.yesterday = `Hôm trước(${moment(dataDate).add(-1, 'day').format('D/M')})`;
          this.lastMonth = `Tháng trước(T${moment(dataDate).add(-1, 'month').format('M')})`;
        }
        this.isEmpty = _.size(this.listDataTable) === 0;
        this.mapTable(_.get(this.data, 'dataTable', []));
      } else {
        this.isEmpty = _.size(this.data?.dataBar) === 0 && _.size(this.data?.dataLine) === 0;
        this.mapChart();
      }
    }
  }

  mapChart() {
    const series: any = [];
    if (this.data.dataBar) {
      series.push({
        name: this.data.legend[0],
        type: 'bar',
        data: _.map(this.data.dataBar, (value) => {
          return {
            value: value,
            itemStyle: {
              color: _.get(this.data, value >= 0 ? 'configColor.tang_net' : 'configColor.giam_net'),
            },
          };
        }),
        barWidth: 25,
      });
    }

    if (this.data.dataLine) {
      series.push({
        name: this.data.legend[1],
        type: 'line',
        data: this.data.dataLine.map(item => ({label:{formatter: this.numberWithCommas(item)}, value: item})),
        lineStyle: {
          color: _.get(this.data, 'configColor.line'),
          width: 1.6,
        },
        itemStyle: {
          color: _.get(this.data, 'configColor.line'),
        },
        label: {
          show: true,
        },
      });
    }
    this.option = {
      grid: {
        bottom: 35,
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'none',
        },
      },
      xAxis: [
        {
          type: 'category',
          axisTick: {
            alignWithLabel: true,
          },
          data: this.data.labels,
        },
      ],
      yAxis: [
        {
          type: 'value',
          position: 'right',
          axisLine: {
            show: false,
          },
          axisLabel: {
            show: false,
          },
        },
      ],
      series: series,
    };
    this.setSmallDesktopOptions();
  }

  mapTable(data: any[]) {
    this.listDataTable = _.cloneDeep(data) || [];
    const unit = this.data.key === 'TOP10_HĐV' || this.data.key === 'TOP10_DUNO' ? 10e8 : 10e5;
    this.listDataTable.forEach((item) => {
      item.T1 = item?.T1 ? _.round(_.divide(item.T1, unit), 2) : 0;
      item.T2 = item?.T2 ? _.round(_.divide(item.T2, unit), 2) : 0;
      item.M1 = item?.M1 ? _.round(_.divide(item.M1, unit), 2) : 0;
      item.T1_NET = item.T1_NET ? _.round(_.divide(item.T1_NET, unit), 2) : 0;
      item.M1_NET = item.M1_NET ? _.round(_.divide(item.M1_NET, unit), 2) : 0;
    });
  }

  onChangeType(event) {
    if (this.data) {
      this.isLoading = true;
      const params = {
        divisionCode: this.data.dataForm?.divisionCode,
        rmCode: this.data.dataForm?.isRegion ? '' : this.data.dataForm.rmCode,
        regionCode: this.data.dataForm?.isRegion ? this.data.dataForm?.regionCode : '',
        branchCode: this.data.dataForm?.branchCode,
        ...this.data.extraData
      };
      let type = _.includes(
        [
          DashboardOtherIndiv.DU_NO,
          DashboardOtherIndiv.HDV,
          DashboardOtherIndiv.TOP10_DU_NO,
          DashboardOtherIndiv.TOP10_HDV,
        ],
        this.data.key
      );
      this.service
        .getDataChartsBusinessOtherIndiv(
          {
            dashboardType: this.data.key,
            type: event.value,
            ...params,
          },
          type
        )
        .pipe(
          finalize(() => {
            this.isLoading = false;
          })
        )
        .subscribe((dataChart) => {
          this.data.dataLine = [];
          this.data.labels = [];
          if (this.data.key === DashboardOtherIndiv.DU_NO || this.data.key === DashboardOtherIndiv.HDV) {
            this.data.dataBar = [];
          }
          
          const result = this.data?.dashboardName === this.dashboardType.OTHER_INDIV_REVENUE ? 
          this.getDataChartRevenue(dataChart, this.data.key, event.value) : 
          this.getDataChartBusiness(dataChart?.dashboardValue, this.businessKeys[this.data.key], event.value);

          this.data.dataLine = result.dataLine;
          this.data.labels = result.labels;
          this.data.dataBar = result.dataBar;
          this.isEmpty = _.size(this.data?.dataBar) === 0 && _.size(this.data?.dataLine) === 0;
          
          this.mapChart();
        });
    }
  }

  viewDetail(item) {
    const url = this.router.serializeUrl(
      this.router.createUrlTree([`${functionUri.customer_360_manager}/detail/${this.data.dataForm?.divisionCode}/${item.CUSTOMER_CODE}`])
    );
    window.open(url, '_blank');
  }

  getDataLine(data) {
    if (this.data?.key === DashboardOtherIndiv.DU_NO) {
      return _.round(_.divide(data.DUNO, 10e8), 2).toFixed(2);
    } else if (this.data?.key === DashboardOtherIndiv.HDV) {
      return _.round(_.divide(data.HUYDONG, 10e8), 2).toFixed(2);
    } else if (this.data?.key === DashboardOtherIndiv.DTTTRR) {
      return _.round(_.divide(data.DTTTRR, 10e5), 2).toFixed(2);
    } else if (this.data?.key === DashboardOtherIndiv.DTTSRR) {
      return _.round(_.divide(data.DTTSRR, 10e5), 2).toFixed(2);
    } else if (this.data?.key === DashboardOtherIndiv.DTTTRR_TBT) {
      return _.round(_.divide(data.DTTSRR, 10e5), 2).toFixed(2);
    } else {
      return null;
    }
  }

  parseJSON(value: string) {
    if (value) {
      return JSON.parse(value);
    }
    return null;
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }

  getDataChartRevenue(raw: any[], type: string, dateType: string) {
    let groupList = [];

    const result = {
      dataLine: [],
      labels: [],
      dataBar: []
    }

    const orderData = (dashboardValue: any[]) => {
      switch (dateType) {
        case 'DAILY':
          return _.orderBy(dashboardValue, [obj => new Date(obj.DATA_DATE)], ['asc']);
        case 'MONTHLY':
          return _.orderBy(dashboardValue, ['MONTH'], ['desc']);
        default:
          return _.orderBy(dashboardValue, ['WEEK'], ['desc']);
      }
    }
    if (raw) {
      // group by date
      raw.forEach(item => {
        const groupBy = {};
        const dashboardData = this.parseJSON(item.dashboardValue);
        let dashboardValue = orderData(dashboardData);
        _.forEach(dashboardValue, v => {
          let time = '';
          switch (dateType) {
            case 'DAILY':
              time = moment(v.DATA_DATE).format('DD/MM');
              break;
            case 'MONTHLY':
              // time = `T${new Date(v.DATA_DATE).getMonth() + 1}`;
              time = moment(v.DATA_DATE).format('MM/YYYY');
              break;
            default:
              time = moment(v.DATA_START_DATE).format('DD/MM') + '-' + moment(v.DATA_END_DATE).format('DD/MM')
              break;
          }
          groupBy[time] = Number(v[type]);
        });
        groupList.push(groupBy);
      });

      // sum
      const formatData = { ...groupList[0] };
      
      const formatDataKeys = Object.keys(formatData).splice(dateType === 'WEEKLY' && Object.keys(formatData).length > 4 ? -4 : 0);
      // for (let i = 1; i < groupList.length; i++) {
      //   formatDataKeys.forEach(key => {
      //     formatData[key] += typeof groupList[i][key] !== 'number' ? 0 : groupList[i][key];
      //   })
      // }

      formatDataKeys.forEach(key => {
        result.dataLine.push(_.round(_.divide(formatData[key], 10e5), 2).toFixed(2))
        result.labels.push(key);
      });
    }

    return result;
  }

  getDataChartBusiness(dashboardValue: string, keys: any, dateType: string) {
    const { dataBarKey, dataLineKey } = keys;
    const unit = 10e8;

    const result = {
      dataLine: [],
      labels: [],
      dataBar: []
    }

    const orderData = (dashboardValue: any[]) => {
      switch (dateType) {
        case 'DAILY':
          return _.orderBy(dashboardValue, [obj => new Date(obj.DATA_DATE)], ['asc']);
        case 'MONTHLY':
          return _.orderBy(dashboardValue, ['MONTH'], ['desc']);
        default:
          return _.orderBy(dashboardValue, ['WEEK'], ['desc']);
      }
    }

    if (dashboardValue) {
      let dashboardData = this.parseJSON(dashboardValue);
      dashboardData = orderData(dashboardData);
      dashboardData = dashboardData.splice(dateType === 'WEEKLY' && dashboardData.length > 4 ? -4 : 0);

      _.forEach(dashboardData, (item) => {
        result.dataBar.push(_.round(_.divide(item[dataBarKey], unit), 2).toFixed(2));
        result.dataLine.push(_.round(_.divide(item[dataLineKey], unit), 2).toFixed(2));

        let time = '';
        switch (dateType) {
          case 'DAILY':
            time = moment(item.DATA_DATE).format('DD/MM');
            break;
          case 'MONTHLY':
            // time = `T${new Date(item.DATA_DATE).getMonth() + 1}`;
            time = moment(item.DATA_DATE).format('MM/YYYY');
            break;
          default:
            time = moment(item.DATA_START_DATE).format('DD/MM') + '-' + moment(item.DATA_END_DATE).format('DD/MM')
            break;
        }

        result.labels.push(time);
      });
    }

    return result;
  }

  setSmallDesktopOptions(): void {
    let xAxis = this.option?.xAxis[0];
    if (!xAxis) return;
    if (window.innerWidth < 1500) {
      xAxis['axisLabel'] = { fontSize: 10, interval: 0, rotate: 20 }
    } else {
      xAxis['axisLabel'] = {}
    }
    this.option = { ...this.option };
  }

  numberWithCommas(x) {
    return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');
  }
}
