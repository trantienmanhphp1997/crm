import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { componentDashboards } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'app-dashboard-add-component-modal',
  templateUrl: './dashboard-add-component-modal.component.html',
  styleUrls: ['./dashboard-add-component-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DashboardAddComponentModalComponent implements OnInit {
  item: any;
  isDisabled = true;
  components = [];

  constructor(private modalActive: NgbActiveModal) {
    Object.keys(componentDashboards).forEach((key) => {
      this.components.push(componentDashboards[key]);
    });
  }

  ngOnInit(): void {}

  choseComponent(item) {
    this.isDisabled = false;
    this.item = item;
  }

  choose() {
    if (this.isDisabled) {
      return;
    }
    this.modalActive.close(this.item);
  }

  closeModal() {
    this.modalActive.close(false);
  }
}
