import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardAddComponentModalComponent } from './dashboard-add-component-modal.component';

describe('DashboardAddComponentModalComponent', () => {
  let component: DashboardAddComponentModalComponent;
  let fixture: ComponentFixture<DashboardAddComponentModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardAddComponentModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardAddComponentModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
