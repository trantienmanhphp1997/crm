import { Component, OnInit, Injector, Input, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import {
  CustomerType,
  EChartType,
  FunctionCode,
  Scopes,
  functionUri,
  SessionKey
} from 'src/app/core/utils/common-constants';
import { percentage, percentagev2 } from 'src/app/core/utils/function';
import { EChartsOption } from 'echarts';
import { DatePipe, formatNumber } from '@angular/common';
import _ from 'lodash';
import {CustomerApi} from '../../../customer-360/apis';
import { CampaignsService } from 'src/app/pages/campaigns/services/campaigns.service';
import { catchError, finalize } from 'rxjs/operators';
import * as echarts from 'echarts';
import { throwError } from 'rxjs';

@Component({
  selector: 'chart-campaign-stage-overview',
  templateUrl: './chart-campaign-stage-overview.component.html',
  styleUrls: ['./chart-campaign-stage-overview.component.scss'],
  providers: [DatePipe],
})
export class ChartCampaignStageOverviewComponent extends BaseComponent implements OnInit, OnChanges {
  isPieChart = true;
  listBranchCode = [];
  @Input() viewChange: EventEmitter<boolean> = new EventEmitter();
  data: any[];
  input: any;
  dataPercentage: string[] = [];
  option: EChartsOption;
  title = 'Dashboard giai đoạn';
  note: string;
  dateSnapshot: string;
  dataTotal: number;
  rawData : any;

  isClick = false;
  isBack = false;
  prevParams: any;
  preClickData: any;
  leftItems  = [];
  rightItems = [];
  private myChart: any = null;

  HIGHTLIGHT = 'highlight';
  DOWNPLAY = 'downplay';


  constructor(injector: Injector, private datePipe: DatePipe,private campaignService: CampaignsService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DASHBOARD_CUSTOMER}`);

  }

  ngOnInit(): void {
    this.isLoading = true;

    console.log(this.input);
    const params = {
      rsId: this.objFunction?.rsId,
      branchCode: this.input?.branchCode || null,
      campaignId: this.input?.campaignId || null,
      hrsCode: this.input?.hrsCode || null,
      listBranchCode: this.input?.listBranchCode || null
    };

    const today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0');
    const yyyy = today.getFullYear();

    const formattedDate = dd + '/' + mm + '/' + yyyy;

    this.campaignService.getCampaignStageOverview(params,
      this.input?.isbranchDimention,
      this.input?.isRm)
      .pipe(
        finalize(() => { this.isLoading = false; }),
        catchError(error => {
          this.handleMapData();
          return throwError(error.message || 'Server error');
        })
      )
    .subscribe(data => {

      this.rawData = data;

      this.data = data;

      this.dateSnapshot = formattedDate

      if (_.size(this.data) > 0) {

        // split column for legend
        for (let i = 0; i < this.data.length; i++) {
          if (i % 2 === 0) {
              this.leftItems.push(this.data[i]);
          } else {
              this.rightItems.push(this.data[i]);
          }
        }

        const sum = _.reduce(
          _.map(this.data, (x) => x.value),
          (a, c) => {
            return a + c;
          }
        );
        this.dataTotal = sum
        _.forEach(this.data, (item) => {
          this.dataPercentage.push(percentage(item.value || 0, sum));
        });
      }
      this.handleMapData();
    });
  }

  ngOnChanges(changes: SimpleChanges) {

  }


  onClickChart(data, chart?) {
    // if (this.dataTotal && this.dataTotal <= 0) {
    //   return;
    // }

    if (!this.input.isRm && this.input.isbranchDimention) {
      this.goReport({data});
      return;
    }

    if (this.input.isRm) {
      this.showCampaignImplementation(data?.code);
    } else {
      this.showCampaignDetail(this.input?.hrsCode, this.input?.branchCode,  this.input?.rmCode,  data?.code);
    }
  }

  handleMapData() {
    if (this.data) {
      this.option = {
        emphasis: {
          itemStyle: {
            borderDashOffset: 30,
          },
        },
        tooltip: {
          trigger: 'item',
          formatter: (params) => {
            return `<div class="text-black" style="width: 150px; font-weight: 600; white-space: pre-wrap;"><span style="font-weight: 400;">${params.name}</span>: ${formatNumber(params.value || 0, 'en', '1.0-0')}</div><div class="text-black" style="font-weight: 600; white-space: pre-wrap; "><span style="font-weight: 400;">Tỷ lệ</span>: ${percentagev2(params.value, this.dataTotal)}%</div>`;
          },
        },
        legend: {
          show: false,
          bottom: 0,
        },
        series: [
          {
            // bottom: '40%',
            emphasis: {
              scaleSize: 13,
              itemStyle: {
                borderWidth: 8,
              }
            },
            type: EChartType.Pie,
            radius: ['30%', '80%'],
            itemStyle: {
              borderColor: '#fff',
              borderWidth: 2,
            },
            labelLine: {
              length: 5,
            },
            label: {
              formatter: (params) =>  (`${formatNumber(Number(params.value), 'en', '1.0-0')} KH`),
            },
            data: [],
          },
        ],
      };
      //thomvt chinh sua, chi danh index nhung goc bieu do gia tri > 0 de bat su kien hightlight tren bieu do
      let index = 0;
      _.forEach(this.data, (item) => {
        if (item.value > 0) {
          this.option.series[0].data.push({
            value: item.value,
            name: item.label,
            itemStyle: { color: item.color },
            code: item.code,
            indexItem: index
          });
          item.indexItem = index;
          index++;
        }
      });
    } else {
      this.data = undefined;
    }
  }

  goReport(params = {}) {
    const dataSend = {
      prevdata: this.rawData,
      isbranchDimention: this.input.isbranchDimention,
      branch: this.input?.branchCode,
      campaignId: this.input?.campaignId,
      hrsCode: this.input?.hrsCode,
      isRm: this.input?.isRm,
      regionCode: this.input?.regionCode,
      campaignList: this.input?.campaignList,
      listBranchCode: this.input?.listBranchCode,
        ...params
    };
    localStorage.setItem(FunctionCode.CHART_CAMPAIGN_STAGE_DETAIL,JSON.stringify(dataSend));
    console.log('dataSend: ',dataSend);
 //   this.sessionService.setSessionData(FunctionCode.CHART_CAMPAIGN_STAGE_DETAIL,dataSend);
    this.router.navigate(['/dashboard/chart-campaign-stage-detail'], {
    //  state: dataSend
    });
  }

  formatCustomerNumber(number : Number) : String {
    return formatNumber(Number(number), 'en', '1.0-0')
  }

  onclickLegend(currentData: any) {
    console.log(currentData);
    // this.resetAndHightLightChart(currentData.indexItem, this.HIGHTLIGHT);
   // if(currentData.value > 0)
      this.onClickChart(currentData);

  }

  showCampaignDetail(hrsCode, branchCode, rmCode, stage) {
    this.isLoading = true;
    const campaignId = this.input?.campaignId;
    console.log('stage: ', stage);
    if(branchCode === 'ALL'){
      branchCode = '';
    }
    const urlTree = this.router.createUrlTree([functionUri.campaign_detail, campaignId], {
      queryParams: { hrsCode,branchCode, stage,rmCode },
      skipLocationChange: true,
    });
    const url = this.router.serializeUrl(urlTree);
    window.open(url, '_blank');

    this.isLoading = false;
  }

  showCampaignImplementation(stage) {
    this.isLoading = true;
    const showBtnAddCampaign = false;
    const campaignId = this.input?.campaignId;
    const urlTree = this.router.createUrlTree([functionUri.campaign_rm, 'detail', campaignId], {
      queryParams: {showBtnAddCampaign,stage},
      skipLocationChange: true,
    });
    const url = this.router.serializeUrl(urlTree);
    window.open(url, '_blank');

    this.isLoading = false;
  }

}
