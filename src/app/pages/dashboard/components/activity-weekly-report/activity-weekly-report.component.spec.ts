import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityWeeklyReportComponent } from './activity-weekly-report.component';

describe('ActivityWeeklyReportComponent', () => {
  let component: ActivityWeeklyReportComponent;
  let fixture: ComponentFixture<ActivityWeeklyReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityWeeklyReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityWeeklyReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
