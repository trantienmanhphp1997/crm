import { Component, Input, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { GridsterItem, GridsterItemComponentInterface } from 'angular-gridster2';
import { Subscription } from 'rxjs';
import { DashboardService } from '../../services/dashboard.service';
import { formatDate } from '@angular/common';
import { CommunicateService } from 'src/app/core/services/communicate.service';
import { Roles } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'app-activity-weekly-report',
  templateUrl: './activity-weekly-report.component.html',
  styleUrls: ['./activity-weekly-report.component.scss'],
})
export class ActivityWeeklyReportComponent implements OnInit, OnDestroy {
  @Input() widget: GridsterItem;
  @Input() resizeEvent: EventEmitter<GridsterItemComponentInterface>;
  @Input() role: string;
  @Input() branchCode: string;
  resizeSub: Subscription;
  chartTimeTrans: any;
  sizeClass: number = 9;
  isLoading = false;

  constructor(private dashboardService: DashboardService, private realtimeChartService: CommunicateService) {}

  ngOnInit(): void {
    this.resizeSub = this.resizeEvent.subscribe((itemComponent) => {
      if (itemComponent.item.component === this.widget.component && this.chartTimeTrans) {
        if (itemComponent.$item.cols === 1) {
          this.sizeClass = 12;
          this.chartTimeTrans.dynHeight = 200;
          this.chartTimeTrans.dynWidth = 450;
        } else {
          this.sizeClass = 9;
          this.chartTimeTrans.dynHeight = 400;
          this.chartTimeTrans.dynWidth = 900;
        }
      }
    });
    // if (this.role !== Roles.RGM) {
    //   this.dashboardService.onUpdateData().subscribe((res) => {
    //     if (res && res.branchCode === this.branchCode) {
    //       this.getDataChart();
    //     }
    //   });
    // } else {
    //   setInterval(() => {
    //     this.getDataChart();
    //   }, 90000);
    // }
    this.isLoading = true;
    this.dashboardService.getActivityReportChart().subscribe(
      (result) => {
        if (result) {
          this.chartTimeTrans = {};
          this.chartTimeTrans.dynHeight = 400;
          this.chartTimeTrans.dynWidth = 900;
          this.chartTimeTrans.chartLabels = [];
          this.chartTimeTrans.chartData = [
            { data: [], label: 'Hẹn gọi lại', lineTension: 0, fill: false },
            { data: [], label: 'Không có nhu cầu', lineTension: 0, fill: false },
            { data: [], label: 'Có nhu cầu', lineTension: 0, fill: false },
            { data: [], label: 'Khác', lineTension: 0, fill: false },
          ];
          this.chartTimeTrans.chartColors = [
            {
              borderColor: '#ffc107',
              backgroundColor: '#ffc107',
              borderWidth: 1,
              pointRadius: 1.5,
              pointHoverRadius: 2,
            },
            {
              borderColor: '#ee4d4d',
              backgroundColor: '#ee4d4d',
              borderWidth: 1,
              pointRadius: 1.5,
              pointHoverRadius: 2,
            },
            {
              borderColor: '#28a745',
              backgroundColor: '#28a745',
              borderWidth: 1,
              pointRadius: 1.5,
              pointHoverRadius: 2,
            },
            {
              borderColor: '#6c757d',
              backgroundColor: '#6c757d',
              borderWidth: 1,
              pointRadius: 1.5,
              pointHoverRadius: 2,
            },
          ];
          const data = result.activityReports.reverse();
          let totalActivities = 0;
          data.forEach((item) => {
            this.chartTimeTrans.chartLabels.push(formatDate(item.date, 'dd/MM', 'en-US'));
            this.chartTimeTrans.chartData[0].data.push(item.totalWaiting);
            this.chartTimeTrans.chartData[1].data.push(item.totalFail);
            this.chartTimeTrans.chartData[2].data.push(item.totalSuccess);
            this.chartTimeTrans.chartData[3].data.push(item.totalMissing);
            totalActivities =
              totalActivities + item.totalWaiting + item.totalFail + item.totalSuccess + item.totalMissing;
          });
          this.chartTimeTrans.chartOptions = {
            responsive: true,
            legend: {
              position: 'bottom',
            },
            scales: {
              yAxes: [
                {
                  ticks: {
                    padding: 5,
                    maxTicksLimit: 10,
                    suggestedMin: 0,
                    suggestedMax: totalActivities === 0 ? 100 : totalActivities,
                  },
                  gridLines: {
                    // display: false,
                    // drawBorder: false,
                  },
                },
              ],
              xAxes: [
                {
                  // offset: true,
                  gridLines: {
                    // display: false,
                  },
                },
              ],
            },
            plugins: {
              labels: {
                render: () => {
                  return '';
                },
              },
            },
          };
          this.isLoading = false;
        }
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  getDataChart() {
    this.dashboardService.getActivityReportChart().subscribe((result) => {
      if (result) {
        this.chartTimeTrans.chartLabels = [];
        this.chartTimeTrans.chartData[0].data = [];
        this.chartTimeTrans.chartData[1].data = [];
        this.chartTimeTrans.chartData[2].data = [];
        this.chartTimeTrans.chartData[3].data = [];
        const data = result.activityReports.reverse();
        let totalActivities = 0;
        const dataRequest = {
          name: undefined,
          data: undefined,
        };
        data.forEach((item) => {
          this.chartTimeTrans.chartLabels.push(formatDate(item.date, 'dd/MM', 'en-US'));
          this.chartTimeTrans.chartData[0].data.push(item.totalWaiting);
          this.chartTimeTrans.chartData[1].data.push(item.totalFail);
          this.chartTimeTrans.chartData[2].data.push(item.totalSuccess);
          this.chartTimeTrans.chartData[3].data.push(item.totalMissing);
          totalActivities =
            totalActivities + item.totalWaiting + item.totalFail + item.totalSuccess + item.totalMissing;
        });
        this.chartTimeTrans.chartOptions = {
          responsive: true,
          maintainAspectRatio: true,
          legend: {
            position: 'bottom',
          },
          scales: {
            yAxes: [
              {
                ticks: {
                  maxTicksLimit: 10,
                  suggestedMin: 0,
                  suggestedMax: totalActivities === 0 ? 100 : totalActivities,
                },
              },
            ],
          },
          plugins: {
            labels: {
              render: () => {
                return '';
              },
            },
          },
        };
        dataRequest.name = 'lineChart';
        dataRequest.data = this.chartTimeTrans;
        this.realtimeChartService.request(dataRequest);
      }
    });
  }

  ngOnDestroy() {
    this.resizeSub.unsubscribe();
  }
}
