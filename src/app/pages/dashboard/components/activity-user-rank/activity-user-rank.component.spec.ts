import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityUserRankComponent } from './activity-user-rank.component';

describe('ActivityUserRankComponent', () => {
  let component: ActivityUserRankComponent;
  let fixture: ComponentFixture<ActivityUserRankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityUserRankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityUserRankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
