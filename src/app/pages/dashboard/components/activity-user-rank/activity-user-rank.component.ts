import { Component, Input, OnInit, EventEmitter, OnDestroy } from '@angular/core';
import { GridsterItem, GridsterItemComponentInterface } from 'angular-gridster2';
import { Subscription } from 'rxjs';
import { Roles } from 'src/app/core/utils/common-constants';
import { DashboardService } from '../../services/dashboard.service';

@Component({
  selector: 'app-activity-user-rank',
  templateUrl: './activity-user-rank.component.html',
  styleUrls: ['./activity-user-rank.component.scss'],
})
export class ActivityUserRankComponent implements OnInit, OnDestroy {
  @Input() widget: GridsterItem;
  @Input() resizeEvent: EventEmitter<GridsterItemComponentInterface>;
  @Input() role: string;
  @Input() branchCode: string;
  resizeSub: Subscription;
  listUsers: any;
  isLoading = false;

  constructor(private dashboardService: DashboardService) {}

  ngOnInit(): void {
    this.resizeSub = this.resizeEvent.subscribe((itemComponent) => {
      if (itemComponent.item.component === this.widget.component) {
        // console.log(itemComponent);
      }
    });
    this.isLoading = false;
    this.dashboardService.getActivityCallsByTeam().subscribe(
      (result) => {
        if (result) {
          this.listUsers = result.activityByUserTeams;
          this.isLoading = false;
        }
      },
      () => {
        this.isLoading = false;
      }
    );
    // if (this.role !== Roles.RGM) {
    //   this.dashboardService.onUpdateData().subscribe((res) => {
    //     if (res && res.branchCode === this.branchCode) {
    //       this.getData();
    //     }
    //   });
    // } else {
    //   setInterval(() => {
    //     this.getData();
    //   }, 90000);
    // }
  }

  getData() {
    if (this.role !== Roles.RGM) {
      this.dashboardService.getActivityCallsByTeam().subscribe((result) => {
        if (result) {
          this.listUsers = result.activityByUserTeams;
        }
      });
    }
  }

  ngOnDestroy() {
    this.resizeSub.unsubscribe();
  }
}
