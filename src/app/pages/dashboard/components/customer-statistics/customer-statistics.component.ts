import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { DashboardService } from '../../services/dashboard.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-customer-statistics',
  templateUrl: './customer-statistics.component.html',
  styleUrls: ['./customer-statistics.component.scss']
})
export class CustomerStatisticsComponent implements OnInit {
  data: any;
  CstProfileInfo: any;
  constructor(private dashboardService: DashboardService) { }

  ngOnInit() {
    this.getCstProfileInfo();
  }

  getCstProfileInfo() {    
    const params = {
      transactionDate: moment(this.data.date).format('DD/MM/YYYY'),
      blockCode: this.data.divisionCode,
      rmCode: this.data.isRegion ? '' : this.data.rmCode,
      branchCodeCap1: this.data.branchCodeLv1,
      branchCodeCap2: !this.data.branchCode ? this.data.listBranch : this.data.branchCode,
      area: !this.data.regionCode ? this.data.listRegion.map(item => item.locationCode).join(',') : this.data.regionCode
    }
    this.dashboardService.getCstProfileInfo(params).subscribe( (res: any) => {
      if(res.length){
        let listData: any = _.cloneDeep(res[0]);
        // tính tổng
        res.forEach( (element, index )=> {
          if (!element) {
            return;
          }
          if (index == 0) {
            return;
          }
          Object.keys(element).forEach((key) => {
            if(!+element[key]){
              return;
            }
            listData[key] = +listData[key] || 0;
            listData[key] = listData[key] + (+element[key] || 0);
          })
        });
        this.CstProfileInfo = listData;
      }
    });
  }
}
