import {
  Component,
  Input,
  OnInit,
  EventEmitter,
  ViewEncapsulation,
  Injector
} from '@angular/core';
import { EChartsOption } from 'echarts';
import * as _ from 'lodash';
import {CommonCategory, FunctionCode, functionUri, Scopes, SessionKey} from 'src/app/core/utils/common-constants';
import {DashboardService} from '../../services/dashboard.service';
import {AppFunction} from '../../../../core/interfaces/app-function.interface';
import {SessionService} from '../../../../core/services/session.service';
import {forkJoin, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {BaseComponent} from '../../../../core/components/base.component';
import {formatNumber} from '@angular/common';

@Component({
  selector: 'b-core-chart',
  templateUrl: './b-core-chart.component.html',
  styleUrls: ['./b-core-chart.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class BCoreChartComponent extends BaseComponent implements OnInit {
  @Input() viewChange: EventEmitter<boolean> = new EventEmitter();
  @Input() data: any;
  dataChart: any[];
  input: any;
  isLoading = false;
  isTable = true;
  option: EChartsOption;
  objFunction: AppFunction;
  chartColorsDuNo = [ 'rgb(88, 207, 141)', '#b489ad'];
  chartColorsNhomNo = [ 'rgb(255, 169, 90)','rgb(158, 179, 248)'];
  commonData = {
    filterList: [
      { value: 'NhomNo', name: 'Nhóm nợ'},
      { value: 'DuNo', name: 'Dư nợ'}
    ]
  };
  filter = 'NhomNo';

  constructor(
    injector: Injector,
    private service: DashboardService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DASHBOARD_CUSTOMER}`);
    this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);

  }

  ngOnInit(): void {
    this.isLoading = true;
    this.isLoading = false;
    this.search();
  }

  search() {
    this.isLoading = true;
    const params = {
      branch_code_cap2: this.currUser.branch,
      rsId: this.objFunction?.rsId,
      scope: 'VIEW',
      branchCodes: this.input?.branchCodes || []
    }
    this.service.getDataChartsBscore(params).subscribe((dataChart) => {
      this.data.data = _.get(dataChart,'data');
      this.data.dsnapshotDate = _.get(dataChart,'dsnapshotDate');
      this.data.totalCustomer = _.get(dataChart,'totalCustomer');
      // this.mapChart();
      this.isLoading = false;
    }, (err) => {
      this.isLoading = false;
      this.messageService.error(err?.error?.description);
    });
  }

  mapChart() {
    if (_.isEmpty(this.data?.data)) {
      return;
    }
    this.option = {
      grid: {
        left: '12%',
        right: '10%'
      },
      color: this.filter === 'NhomNo' ? this.chartColorsNhomNo : this.chartColorsDuNo,
      legend: {
        show: true,
        selectedMode: false,
        bottom: 0
      },
      tooltip: {
        trigger: 'item',
        axisPointer: {
          type: 'shadow'
        },
      },
      xAxis: [
        {
          nameTextStyle: {
            fontSize: 15,
            fontWeight: 'bold',
            fontFamily: 'Lato',
            verticalAlign: 'top',
            padding: 16
          },
          nameGap: -10,
          offset: 10,
          name: 'Hạng',
          type: 'category',
          axisTick: {
            alignWithLabel: true
          },
          data: this.getColumnChartByFieldName('classGroup')
        }
      ],
      yAxis: [
        {
          nameTextStyle: {
            fontSize: 15,
            fontWeight: 'bold',
            fontFamily: 'Lato',
          },
          type: 'value',
          name: 'Số lượng KH',
          position: 'left',
          alignTicks: true,
          axisLine: {
            show: true,
            lineStyle: {
            }
          },
          axisLabel: {
            formatter: '{value}'
          }

        },
      ],
      series: [
      ]
    };
    this.option.series[0] = {
      name: this.filter === 'NhomNo' ? 'Nhóm nợ 1' : 'Dư nợ tín chấp',
      barGap: 0.05,
      // barWidth: 35,
      type: 'bar',
      stack: 'NhomNo',
      yAxisIndex: 0,
      data: this.getColumnChartByFieldName(this.filter === 'NhomNo' ? 'debtGroup1' : 'unScrLn')
    };
    this.option.series[1] = {
      name: this.filter === 'NhomNo' ? 'Nhóm nợ còn lại' : 'Dư nợ có tài sản đảm bảo',
      barGap: 0.05,
      // barWidth: 35,
      type: 'bar',
      stack: 'NhomNo',
      yAxisIndex: 0,
      data: this.getColumnChartByFieldName(this.filter === 'NhomNo' ? 'debtGroupOther' : 'scrLn')
    };
  }

  switchChart() {
    if (this.isLoading) {
      return;
    }
    this.isTable = !this.isTable;
  }

  getColumnChartByFieldName(fieldName: string, fomatNumber = false) {
    const data =
      fomatNumber ?
        _.map(this.data.data, i => {
          return formatNumber(_.get(i, fieldName) || 0, 'en', '1.0-0')
        })
        : _.map(this.data.data, fieldName);
    return data;
  }

  formatNumber(value) {
    return formatNumber(value || 0, 'en', '1.0-0');
  }


}
