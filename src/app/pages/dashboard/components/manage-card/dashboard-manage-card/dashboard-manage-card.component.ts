import {AfterViewInit, Component, Injector, OnInit, ViewChild} from '@angular/core';
import {BaseComponent} from '../../../../../core/components/base.component';
import {Pageable} from '../../../../../core/interfaces/pageable.interface';

import {
  BRANCH_HO,
  CustomerType,
  DashboardType,
  EChartType,
  FunctionCode,
  Scopes,
  SessionKey
} from '../../../../../core/utils/common-constants';
import {DashboardService} from '../../../services/dashboard.service';
import {global} from '@angular/compiler/src/util';
import {forkJoin} from 'rxjs';
import {CategoryService} from '../../../../system/services/category.service';
import _ from 'lodash';
import * as echarts from 'echarts';
import {EChartsOption} from 'echarts';
import {formatDate, formatNumber} from '@angular/common';
import {Utils} from "../../../../../core/utils/utils";
import {finalize} from "rxjs/operators";

@Component({
  selector: 'app-dashboard-manage-card',
  templateUrl: './dashboard-manage-card.component.html',
  styleUrls: ['./dashboard-manage-card.component.scss']
})
export class DashboardManageCardComponent extends BaseComponent implements OnInit, AfterViewInit {

  // @ViewChild(ECharts) echarts: ECharts;
  data: any;
  listDataCard = [];
  listBranchCode = [];
  listBranch: Array<any>;
  pageable: Pageable;
  option: EChartsOption;
  limit = global.userConfig.pageSize;
  statusNameCard = 'Thông tin chi tiết';
  paramsDetail = {
    rsId: '',
    scope: Scopes.VIEW,
    card_classification: '',
    page: 0,
    size: global?.userConfig?.pageSize,
    branch: ''
  };
  intervalDownloadFileS3 = null;
  paramsExport = {
    rsId: '',
    scope: 'VIEW',
    isRm: true,
    transactionDate: '',
    rmCode: '',
    branchCodes: []
  }
  paramCharts = {
    rsId: '',
    scope: Scopes.VIEW,
    branch: ''
  };
  isClick = false;
  isBack = false;
  prevParams: any;
  preClickData: any;
  private myChart: any = null;
  timeout;
  HIGHTLIGHT = 'highlight';
  DOWNPLAY = 'downplay';
  isRm = true;
  isCBQL = false;
  dataDate : Date ;
  constructor(injector: Injector, private dashboardService: DashboardService, private categoryService: CategoryService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
   // this.objFunction1 = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.prevParams = _.get(this.router.getCurrentNavigation(), 'extras.state');
    this.paramsDetail.rsId = this.objFunction?.rsId;
    this.paramCharts.rsId = this.objFunction?.rsId;
    this.paramsExport.rsId = this.objFunction?.rsId;
    this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
    if (this.prevParams) {
      this.isBack = true;
      this.listBranchCode = this.prevParams?.branch ? this.prevParams?.branch : '';
    //  this.listBranchCode = this.prevParams?.branch ? this.prevParams?.branch.toString() : '';
      if (this.prevParams?.data) {
        this.preClickData = this.prevParams?.dataChoose;
        this.paramsDetail.card_classification = this.preClickData.code;
        this.statusNameCard = `Thông tin chi tiết - ${this.preClickData?.name}`;
      }
    }
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.pageable = {
      totalElements: 0,
      totalPages: 0,
      currentPage: this.paramsDetail.page,
      size: this.limit
    }
    console.log('data: ', this.dataDate);
    this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
    this.categoryService.getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW).pipe(
      finalize(() => {
        this.isRm = _.isEmpty(this.listBranch);
        this.isCBQL = (this.listBranch?.length > 0)
        this.paramsExport.isRm = this.isRm;
        //   const currentDate = new Date();
        this.paramsExport.rmCode = this.currUser?.code;
        this.search();
      })
    ).subscribe(result => {
      this.listBranch = result || [];
    }, (err) => {
      this.listBranch = [];
    });
  }

  ngAfterViewInit() {
    if (this.isBack && this.preClickData) {
      // setTimeout(() =>this.checkViewDashboard(this.preClickData.indexItem, 'highlight'), 1000);
      // this.isBack = false;
    }
  }

  search() {
    // if (!this.currUser?.code) {
    //   this.isLoading = false;
    //   return;
    // }
    const selectedBranch = this.listBranchCode.length;
    const totalBranch = this.listBranch.length;
    this.isLoading = true;
    this.paramsDetail.rsId = this.objFunction?.rsId;
    this.paramCharts.rsId = this.objFunction?.rsId;
    this.paramsDetail.branch = selectedBranch === totalBranch ? '' : this.listBranchCode.toString();
    this.paramCharts.branch = this.paramsDetail.branch;
    forkJoin([this.dashboardService.getDataChartsCreditCard(this.paramCharts),
      this.dashboardService.getDataChartsCreditCardDetail(this.paramsDetail)])
      .subscribe(([dataChart, dataChartDetail]) => {
        this.data = dataChart;
        if(this.data && this.data.date){
          this.paramsExport.transactionDate = this.data?.date.substring(6,10) + this.data?.date.substring(3,5) + this.data?.date.substring(0,2);
        }
        else{
          this.paramsExport.transactionDate = '';
        }
        this.listDataCard = dataChartDetail ? dataChartDetail?.content : [];
        this.pageable = {
          totalElements: dataChartDetail?.totalElements || 0,
          totalPages: dataChartDetail?.totalPages || 0,
          currentPage: this.paramsDetail.page,
          size: this.paramsDetail.size
        }
        this.handleMapData();
        this.isLoading = false;
        if (this.preClickData && this.isBack) {
          this.checkView(this.preClickData);
        }

      }, (err) => {
        this.paramsExport.transactionDate =  this.getYesterday();
        this.messageService.error('Thực hiện không thành công');
        this.isLoading = false;
      });

  }

  checkView(preClickData) {
    const test = document.getElementById('chartId');
    if (test) {
      this.checkViewDashboard(preClickData.indexItem, this.HIGHTLIGHT);
      clearTimeout(this.timeout);
    } else {
      this.timeout = setTimeout(() => this.checkView(preClickData), 1000);
    }
  }

  setPage(pageInfor) {
    if (this.isLoading) {
      return;
    }
    this.paramsDetail.page = pageInfor.offset;
    this.search();
  }

  removeBranchSelected() {
  }

  changeBranch(event) {
    this.listBranchCode = event;
    if (this.preClickData) {
      this.statusNameCard = 'Thông tin chi tiết';
      this.paramsDetail.card_classification = '';
      this.checkViewDashboard(this.preClickData.indexItem, this.DOWNPLAY);
      this.preClickData = null;
    }
    this.search();
  }

  onClickChart(data, chart?) {
    if (chart && chart?.componentIndex === 0) {
      return;
    }
    this.isLoading = true;
    if (this.preClickData) {
      this.checkViewDashboard(this.preClickData.indexItem, this.DOWNPLAY);
    }
    if (data?.code === this.preClickData?.code) {
      this.statusNameCard = 'Thông tin chi tiết';
      this.paramsDetail.card_classification = '';
      this.preClickData = null;
    } else {
      this.statusNameCard = `Thông tin chi tiết - ${data?.name}`;
      this.paramsDetail.card_classification = data?.code;
      this.preClickData = data;
      if (data.value > 0) {
        this.checkViewDashboard(data.indexItem, this.HIGHTLIGHT);
      }
    }
    this.paramsDetail.branch = this.listBranch.length === this.listBranchCode.length ? '' : this.listBranchCode.toString();
    this.paramsDetail.page = 0;
    this.paramsDetail.size = 10;
    this.dashboardService.getDataChartsCreditCardDetail(this.paramsDetail).subscribe(result => {
      this.listDataCard = result ? result?.content : [];
      this.pageable = {
        totalElements: result?.totalElements || 0,
        totalPages: result?.totalPages || 0,
        currentPage: this.paramsDetail.page,
        size: this.paramsDetail.size
      }
      this.isLoading = false;
    }, (err) => {
      this.isLoading = false;
      this.messageService.error(this.notificationMessage.error);
    })
  }

  handleMapData() {
    let index = 0;
    if (this.data) {
      this.option = {
        emphasis: {
          itemStyle: {
            borderDashOffset: 30,
          },
        },
        tooltip: {
          alwaysShowContent: false,
          trigger: 'item',
          formatter: (params) => {
            return `${params.name}: <b>${this.formatNumber(params.value)}</b>`;
          },
        },
        legend: {
          show: false,
          bottom: 0,
        },
        series: [
          {
            startAngle: 150,
            type: EChartType.Pie,
            radius: ['21%', '43%'],
            itemStyle: {
              borderColor: '#fff',
              borderWidth: 2,
            },
            labelLine: {
              length: 5,
            },
            label: {
              show: false,
              formatter: '{c}',
              position: 'inner',
              fontSize: 16,
              fontWeight: 700,
              lineHeight: 20
            },
            data: [],
          },
          {
            emphasis: {
              scaleSize: 13,
              itemStyle: {
                borderWidth: 8,
              }
            },
            startAngle: 150,
            type: EChartType.Pie,
            //   selectedOffset: 20,
            itemStyle: {
              borderColor: '#fff',
              borderWidth: 2
            },
            radius: ['43%', '65%'],
            label: {
              position: 'inner',
              //formatter: '{c}',
              formatter: (params) => {
                return params.value !== 0 ? `${this.formatNumber(params.value)}` : ''
              },
              fontSize: 10,
              fontWeight: 550,
              lineHeight: 12,
              align: 'center'
            },
            labelLine: {
              length: 30
            },
            data: []
          }
        ],
      };
      this.data?.data?.forEach((item) => {
        if (item.value <= 0) {
          return;
        }
        this.option.series[0].data.push({
          value: item.value,
          name: item.name,
          itemStyle: {color: item.color},
          code: item.code
        });
        item.detail?.forEach((itemDetail) => {
          if(itemDetail.value > 0){
            this.option.series[1].data.push({
              value: itemDetail.value,
              name: itemDetail.name,
              itemStyle: {color: itemDetail.color},
              code: itemDetail.code,
              indexItem: index
            });
            // itemDetail = {...itemDetail, indexItem: index};
            itemDetail.indexItem = index;
            index++;
          }
        });
      });
    } else {
      this.data = undefined;
    }
  }

  back() {
    const data = {
      block: CustomerType.INDIV,
      type: DashboardType.INDIV_CARD,
      branch: this.prevParams?.branch
    }
    this.router.navigate(['/dashboard'], {
      state: data
    });
  }

  handleChangePageSize(event) {
    this.pageable.currentPage = 0;
    this.pageable.size = event;
    this.paramsDetail.size = event;
    this.paramsDetail.page = 0;
    this.limit = event;
    this.search();
  }

  checkViewDashboard(index, typeAction) {
    if(index >= 0){
      const myChart = echarts.init((document.getElementById('chartId')) as any);
      if (!myChart) {
        return;
      }
      if (!this.myChart) {
        this.myChart = myChart;
      }
      //
      // myChart.on('rendered', () => {
      //   console.log('rendered')
      // })

      this.myChart.dispatchAction({
        type: typeAction,
        seriesIndex: 1,
        dataIndex: index
      });
    }
  }
  export(){
   // this.paramsExport.transactionDate = formatDate(this.dataDate, 'yyyyMMdd', 'en');
    console.log('transaction date: ', this.paramsExport.transactionDate);
    if(Utils.isEmpty(this.paramsExport.transactionDate)){
      this.messageService.warn(_.get(this.notificationMessage, 'report_empty'));
      return;
    }
    if(this.isRm || this.isCBQL){
      this.isLoading = true;
      this.paramsExport.branchCodes = this.listBranch.length === this.listBranchCode.length ? [] : this.listBranchCode;
      this.paramsExport.branchCodes = Object.keys(this.paramsExport.branchCodes).length > 0? this.paramsExport.branchCodes : [];
      this.dashboardService.exportReportManageCard(this.paramsExport).subscribe((urls) => {
        this.isLoading = false;
        if (urls.length > 0) {
          const download = (urls) => {
            const url = urls.pop();
            window.open(url, '_self');

            if (!urls.length && this.intervalDownloadFileS3) {
              clearInterval(this.intervalDownloadFileS3);
            }
          }

          this.intervalDownloadFileS3 = setInterval(download, 500, urls);
        } else {
          this.messageService.warn(_.get(this.notificationMessage, 'report_empty'));
        }
      }, () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      });
    }
  }
  getYesterday(){
    return formatDate(new Date(new Date().getTime() - 24*60*60*1000), 'yyyyMMdd', 'en')
  }
  formatNumber(value){
    return formatNumber(value || 0, 'en', '0.0-2')
  }
}
