import {BaseComponent} from '../../../../core/components/base.component';
import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewEncapsulation
} from '@angular/core';
import {EChartsOption} from 'echarts';
import * as _ from 'lodash';
import * as moment from "moment";
import {
  CommonCategory,
  DashboardOtherIndiv,
  DashboardType,
  FunctionCode, Scopes
} from "../../../../core/utils/common-constants";
import {DashboardService} from "../../services/dashboard.service";
import {catchError, finalize} from "rxjs/operators";
import {Pageable} from "../../../../core/interfaces/pageable.interface";
import {global} from "@angular/compiler/src/util";
import {forkJoin, of} from "rxjs";
import {Utils} from "../../../../core/utils/utils";
@Component({
  selector: 'app-dashboard-mobilize-indiv',
  templateUrl: 'dashboard-mobilize-indiv.component.html',
  styleUrls: ['dashboard-mobilize-indiv.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DashboardMobilizeIndivComponent extends BaseComponent implements OnInit,OnChanges{
  limit = global.userConfig.pageSize;
  @Input() branchCodeFilter: string;
  @Input() state: Array<any>;
  @Input() dashboardType: string;
  @Output() onShowLoading: EventEmitter<any> = new EventEmitter();
  data: any;
  pageable: Pageable;
  listData = [];
  listTime = [
    {
      code: '01',
      name: 'Ngày',
    },
    {
      code: '02',
      name: 'Tuần',
    },
    {
      code: '03',
      name: 'Tháng',
    },
  ];
  listType = [
    {
      code: '01',
      name: 'Casa',
      checked: true,
      disable: false
    },
    {
      code: '02',
      name: 'Tiết kiệm',
      checked: true,
      disable: false
    },
    {
      code: '03',
      name: 'CDs',
      checked: true,
      disable: false
    },
  ];
  title = 'Huy động vốn và CDs thời điểm';
  typeTime = '01';
  listChecked = ['01','02','03'];
  isEmpty = true;
  option: EChartsOption;
  loadingListRm = true;
  searchParam = {
    branchCodecap2: 'ALL',
    moduleType: '',
    dateType: '01',
    dashboardType: CommonCategory.HDV_INDIV,
    isWeb: true
  }
  searchRmParam = {
    branchCodecap2: 'ALL',
    moduleType: '01',
    dateType: '01',
    dashboardType: CommonCategory.HDV_INDIV,
    page: 0,
    size: global.userConfig.pageSize
  }
  img = {
    up: './assets/images/arrow-up.svg',
    down: './assets/images/arrow-down.svg'
  };
  constructor(injector: Injector, private service: DashboardService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log('changes', this.dashboardType, changes);
    this.isLoading = true;
    if(changes.branchCodeFilter){
      console.log('dashboardType: ',this.dashboardType);
      if(this.dashboardType === CommonCategory.DUNO_INDIV){
        this.title = 'Dư nợ thời điểm';
      }
      this.searchParam.dashboardType = this.dashboardType;
      this.searchRmParam.dashboardType = this.dashboardType;
      console.log('state: ',this.state);
      this.search(true);
    }
  }
  ngOnInit(): void {
  }
  search(isSearch){
    this.isLoading = true;
    this.loadingListRm = true;
    if(isSearch){
      this.searchRmParam.page = 0;
      this.searchRmParam.size = 10;
    }
    this.searchParam.branchCodecap2 = this.branchCodeFilter;
    this.searchParam.moduleType = this.listChecked.join(',');
    this.searchParam.dateType = this.typeTime;
    this.searchRmParam.branchCodecap2 = this.branchCodeFilter;
    this.searchRmParam.moduleType = this.listChecked.join(',');
    this.searchRmParam.dateType = this.typeTime;
    forkJoin([
      this.service.businessDashboardHdvIndiv(this.searchParam).pipe(catchError(() => of(undefined))),
      this.service.dashboardHdDunoRmDetail(this.searchRmParam).pipe(catchError(() => of(undefined)))
    ]).subscribe(([data,listDataRm]) => {
      if(data){
        this.data = this.mapDataDashboardMobilizeCbql(data);
        this.isEmpty = _.size(this.data?.dataBar) === 0 && _.size(this.data?.dataLine) === 0;
        this.mapChart();
      }
      else{
        this.data = undefined;
        this.isEmpty = true;
      }
      this.listData = _.get(listDataRm,'data') || [];
      this.validateDataListRm();
      this.pageable = {
        totalElements: listDataRm?.TOTAL_LENGTH,
        totalPages: listDataRm?.TOTAL_LENGTH/this.searchRmParam.size,
        currentPage: this.searchRmParam.page,
        size: this.searchRmParam.size,
      };
      this.isLoading = false;
      this.loadingListRm = false;
      //   this.onShowLoading.emit(false);
    }, (er) => {
      this.messageService.error(this.notificationMessage.error);
      this.isLoading = false;
      this.loadingListRm = false;
      //    this.onShowLoading.emit(false);
    })
  }
  onChangeType(event){
    console.log('event: ',event);
    if(this.isLoading){
      return;
    }
    this.search(true);
  }
  parseJSON(value: string) {
    if (value) {
      return JSON.parse(value);
    }
    return null;
  }
  mapDataDashboardMobilizeCbql(dataChart){
    console.log('state dashboard: ', this.state);

    let data = {
      key: DashboardOtherIndiv.HDV,
      //dataForm: this.form.getRawValue(),
      title: 'Huy động vốn và CDs thời điểm',
      type: 'chart',
      labels: [],
      legend: ['Tăng/giảm NET (tỷ đồng)', 'Quy mô TĐ (tỷ đồng)'],
      dataBar: [],
      dataLine: [],
      configColor: this.parseJSON(
        _.find(
          this.state,
          (item) => item.code === CommonCategory.HDV_INDIV
        )?.description
      ),
      isLoading: true,
      dashboardName: DashboardType.OTHER_INDIV_BUSINESS,
      businessDate: dataChart?.businessDate
    }
    if(this.dashboardType === CommonCategory.DUNO_INDIV){
      data = {
        key: DashboardOtherIndiv.HDV,
        //dataForm: this.form.getRawValue(),
        title: 'Dư nợ thời điểm',
        type: 'chart',
        labels: [],
        legend: ['Tăng/giảm NET (tỷ đồng)', 'Quy mô TĐ (tỷ đồng)'],
        dataBar: [],
        dataLine: [],
        configColor: this.parseJSON(
          _.find(
            this.state,
            (item) => item.code === CommonCategory.DUNO_INDIV
          )?.description
        ),
        isLoading: true,
        dashboardName: DashboardType.OTHER_INDIV_BUSINESS,
        businessDate: dataChart?.businessDate
      }
    }
    const result = this.getDataChartBusiness(dataChart.dashboardValue, { dataBarKey: 'biendong', dataLineKey: 'sodu' });
    data.dataBar = result.dataBar;
    data.dataLine = result.dataLine;
    data.labels = result.labels;
    return data;
  }
  getDataChartBusiness(dashboardValue: string, options: any) {
    const { dataBarKey, dataLineKey } = options;
    const unit = 10e8;

    const result = {
      dataLine: [],
      labels: [],
      dataBar: []
    }

    // const orderData = (dashboardValue: any[]) => {
    //   return _.orderBy(dashboardValue, [obj => new Date(obj.datadate)], ['asc']);
    // }

    if (dashboardValue) {
      //  const dashboardData = this.parseJSON(dashboardValue);
      //  dashboardData = orderData(dashboardData);

      _.forEach(dashboardValue, (item) => {
        result.dataBar.push(_.round(_.divide(item[dataBarKey], unit), 2).toFixed(2));
        result.dataLine.push(_.round(_.divide(item[dataLineKey], unit), 2).toFixed(2));
        result.labels.push(
          item.datadate
        );
      });
    }

    return result;
  }
  mapChart() {
    const series: any = [];
    if (this.data.dataBar) {
      series.push({
        name: this.data.legend[0],
        type: 'bar',
        label: {
          show: true,
          //  position: 'auto'
        },
        data: _.map(this.data.dataBar, (value) => {
          return {
            value: value,
            label: {formatter: this.numberWithCommas(value),
              position: value >= 0 ? 'top' : 'bottom',
              marginTop: value >= 0 ? 150 : -150,
            },
            itemStyle: {
              color: _.get(this.data, value >= 0 ? 'configColor.tang_net' : 'configColor.giam_net'),
            },
          };
        }),
        barWidth: 30,
      });
    }

    if (this.data.dataLine) {
      series.push({
        name: this.data.legend[1],
        type: 'line',
        data: this.data.dataLine.map(item => ({label:{formatter: this.numberWithCommas(item)}, value: item})),
        lineStyle: {
          color: _.get(this.data, 'configColor.line'),
          width: 1.6,
        },
        itemStyle: {
          color: _.get(this.data, 'configColor.line'),
        },
        label: {
          show: true,
        },
      });
    }
    this.option = {
      grid: {
        bottom: 35
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'none',
        },
        position: function (point, params, dom, rect, size) {
          // fixed at top
          return [point[0], '10%'];
        },
        valueFormatter: function (value: number) {
          return value.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');
        }
      },
      xAxis: [
        {
          type: 'category',
          axisTick: {
            alignWithLabel: true,
          },
          axisLabel: {
            align: 'center',
            margin: 20,
            fontWeight: 1000
          },
          data: this.data.labels,
        },
      ],
      yAxis: [
        {
          type: 'value',
          position: 'right',
          axisLine: {
            show: false,
          },
          axisLabel: {
            show: false,
          },
        },
      ],
      series: series,
    };
    //  this.setSmallDesktopOptions();
  }
  setSmallDesktopOptions(): void {
    let xAxis = this.option?.xAxis[0];
    if (!xAxis) return;
    if (window.innerWidth < 1500) {
      xAxis['axisLabel'] = { fontSize: 10, interval: 0, rotate: 20 }
    } else {
      xAxis['axisLabel'] = {}
    }
    this.option = { ...this.option };
  }
  numberWithCommas(x) {
    return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');
  }
  onCheckboxFn(item, isChecked){
    if(isChecked){
      this.listChecked.push(item?.code);
      this.setupCheckbox();
    }
    else{
      this.listChecked = this.listChecked.filter((i) => i !== item?.code);
      this.setupCheckbox();
    }
    console.log('listChecked: ',this.listChecked);
    this.search(true);
  }
  setupCheckbox(){
    if(this.listChecked.length === 1){
      this.listType.forEach( (item) => {
        if(item.code === this.listChecked[0]){
          item.disable = true;
        }
      });
    }
    else{
      this.listType.forEach((item) => {
        item.disable = false;
      })
    }
  }
  setPage(pageInfo){
    if(this.isLoading){
      return;
    }
    this.searchRmParam.page = pageInfo.offset;
    this.searchRm();
  }
  handleChangePageSize(event){
    if(this.isLoading){
      return;
    }
    this.searchRmParam.page = 0;
    this.searchRmParam.size = event;
    this.limit = event;
    this.searchRm();
  }

  searchRm(){
    this.loadingListRm = true;
    this.searchRmParam.branchCodecap2 = this.branchCodeFilter;
    this.searchRmParam.moduleType = this.listChecked.join(',');
    this.searchRmParam.dateType = this.typeTime;
    this.service.dashboardHdDunoRmDetail(this.searchRmParam).pipe(catchError(() => of(undefined))).subscribe(listDataRm => {
      this.listData = _.get(listDataRm,'data') || [];
      this.validateDataListRm();
      this.pageable = {
        totalElements: listDataRm?.TOTAL_LENGTH,
        totalPages: listDataRm?.TOTAL_LENGTH/this.searchRmParam.size,
        currentPage: this.searchRmParam.page,
        size: this.searchRmParam.size,
      };
      this.loadingListRm = false;
    }, (er) => {
      this.messageService.error(this.notificationMessage.error);
      this.loadingListRm = false;
    })
  }
  validateDataListRm(){
    const unit = 10e8;
    console.log('unit: ', unit);

    this.listData = this.listData.map((item) => {
      let rmInfo = '';
      if(item?.RM_CODE === 'Chưa phân giao'){
        rmInfo = item?.RM_CODE;
      }
      else{
        if(item?.RM_CODE){
          rmInfo += item?.RM_CODE;
        }
        if(item?.RM_NAME){
          if(Utils.isStringNotEmpty(rmInfo)){
            rmInfo += ' - ' + item?.RM_NAME;
          }
          else{
            rmInfo +=  item?.RM_NAME;
          }
        }
      }
      if(Number.isFinite(item?.BAL_CHANGE_1)){
        item.BAL_CHANGE_1 = _.round(_.divide(item.BAL_CHANGE_1, unit), 2).toFixed(2);
        item.BAL_CHANGE_1_1 = this.absValue(item?.BAL_CHANGE_1);
      }
      if(Number.isFinite(item?.BAL_CHANGE_2)){
        item.BAL_CHANGE_2 = _.round(_.divide(item.BAL_CHANGE_2, unit), 2).toFixed(2);
        item.BAL_CHANGE_2_2 = this.absValue(item.BAL_CHANGE_2);
      }
      if(Number.isFinite(item?.BAL_CHANGE_3)){
        item.BAL_CHANGE_3 = _.round(_.divide(item.BAL_CHANGE_3, unit), 2).toFixed(2);
        item.BAL_CHANGE_3_3 = this.absValue(item.BAL_CHANGE_3);
      }
      if(Number.isFinite(item?.BAL_DAY1)){
        item.BAL_DAY1 = this.numberWithCommas(_.round(_.divide(item.BAL_DAY1, unit), 2).toFixed(2));
      }
      return {
        ...item,
        RM_INFO: rmInfo
      }
    })
  }
  absValue(value){
    return this.numberWithCommas(Math.abs(value));
  }
}
