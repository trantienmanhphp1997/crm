import {BaseComponent} from "../../../../core/components/base.component";
import {AfterViewInit, Component, Injector, OnInit} from "@angular/core";
import {EChartsOption} from "echarts";
import {Pageable} from "../../../../core/interfaces/pageable.interface";
import {CampaignsService} from "../../../campaigns/services/campaigns.service";
import {
  CustomerType,
  DashboardType,
  EChartType,
  FunctionCode,
  functionUri,
  Scopes
} from "../../../../core/utils/common-constants";
import {percentage} from "../../../../core/utils/function";
import * as _ from 'lodash';
import {formatNumber} from "@angular/common";
import {global} from "@angular/compiler/src/util";
import {Utils} from "../../../../core/utils/utils";
import * as echarts from "echarts";
import {finalize} from "rxjs/operators";
import {forkJoin} from "rxjs";
@Component({
  selector: 'app-chart-campaign-stage-detail',
  styleUrls: ['chart-campaign-stage-detail.scss'],
  templateUrl: 'chart-campaign-stage-detail.html'
})
export class ChartCampaignStageDetailComponent extends BaseComponent implements OnInit,AfterViewInit{
  dataTotal: number;
  data: any;
  option: EChartsOption;
  pageable: Pageable;
  listData = [];
  dataPercentage: string[] = [];
  prevParams: any;
  limit = global.userConfig.pageSize;
  private myChart: any = null;
  timeout;
  HIGHTLIGHT = 'highlight';
  DOWNPLAY = 'downplay';
  preClickData: any;
  paramChart = {
      rsId: '',
      branchCode: null,
      campaignId: null,
      hrsCode: null,
      listBranchCode: []
  };
  paramList = {
    page: 0,
    size: global.userConfig.pageSize,
    status: '',
    campaignId: '',
    rsId: '',
    branchCodePerform: [],
    region: '',
    scope: Scopes.VIEW
  };
  dateSnapshot: string;
  leftItems  = [];
  rightItems = [];
  isBack = false;
  constructor(injector: Injector,
              private campaignService: CampaignsService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DASHBOARD_CUSTOMER}`);
    this.paramChart.rsId = this.objFunction?.rsId;
    this.prevParams = JSON.parse(localStorage.getItem(FunctionCode.CHART_CAMPAIGN_STAGE_DETAIL));
    console.log('prevParams: ',this.prevParams);
     if(this.prevParams){
       this.isBack = true;
       this.preClickData = this.prevParams?.data;
       this.paramList.status = this.preClickData?.code;
       this.paramList.campaignId = this.prevParams?.campaignId || null;
       if(this.prevParams?.branch !== '' && this.prevParams?.branch !== 'ALL'){
         this.paramList.branchCodePerform = [this.prevParams?.branch];
       }
       this.paramList.region = this.prevParams?.regionCode;
     }
    this.paramChart = {
      rsId: this.objFunction?.rsId,
      branchCode: this.prevParams?.branch || null,
      campaignId: this.prevParams?.campaignId || null,
      hrsCode: this.prevParams?.hrsCode || null,
      listBranchCode: this.prevParams?.listBranchCode || []
    };
    this.paramList.rsId = this.objFunction?.rsId;
  }

  ngOnInit(): void {

    const today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0');
    const yyyy = today.getFullYear();

    const formattedDate = dd + '/' + mm + '/' + yyyy;
    this.dateSnapshot = formattedDate
    this.search(true);
  }
  search(isSearch){
    this.isLoading = true;
    if(isSearch){
      this.paramList.page = 0;
      this.paramList.size = 10;
    }

    forkJoin([
      this.campaignService.getCampaignStageOverview(this.paramChart, this.prevParams?.isbranchDimention, this.prevParams?.isRm).pipe(),
      this.campaignService.getDashboardCampaignStageDetail(this.paramList).pipe()
    ]).subscribe(([dataChart, dataList]) =>{
        this.isLoading = false;
        if(dataChart){
          this.data = dataChart;
          if(this.data.length > 0){
            const sum = _.reduce(
              _.map(this.data, (x) => x.value),
              (a, c) => {
                return a + c;
              }
            );
            this.dataTotal = sum
            _.forEach(this.data, (item) => {
              this.dataPercentage.push(percentage(item.value || 0, sum));
            });
          }
          this.handleMapData();
          if(this.isBack){
            this.checkView(this.preClickData);
            this.isBack = false;
          }
        }
        if(dataList){
          this.listData = _.get(dataList,'content');
          console.log('total: ', this.preClickData?.value);
          this.listData = this.listData.map((item) => {
            let rmInfo = '';
            if(item?.rmPerform){
              rmInfo += item?.rmPerform;
            }
            if(item?.rmName){
              if(Utils.isStringNotEmpty(rmInfo)){
                rmInfo += ' - ' + item?.rmName;
              }
              else{
                rmInfo +=  item?.rmName;
              }
            }
            return {
              ...item,
              rmInfo: rmInfo,
              percentage: percentage(item.count || 0, this.preClickData?.value)
            }
          })
          this.pageable = {
            totalPages: dataList?.totalPages,
            totalElements: dataList?.totalElements,
            currentPage: dataList?.number,
            size: this.limit
          }
      }
    }, (err) => {
      this.data = {
        data:[],
        date:null,
        total:0,
        code:null
      };
      this.handleMapData();
      this.isLoading = false;
      this.messageService.error(this.notificationMessage.error);
    })
  }

  back() {
    const data = {
      block: CustomerType.INDIV,
      type: DashboardType.INDIV_CAMPAIGN,
      branch: this.prevParams?.branch,
      campaignList: this.prevParams?.campaignList,
      branchCode: this.prevParams?.branch,
      regionCode: this.prevParams?.regionCode,
      campaignId: this.prevParams?.campaignId,
    };
    this.router.navigate(['/dashboard'], {
      state: data,
    });
  }
  handleMapData(){
    if (_.size(this.data) > 0) {
      this.option = {
        emphasis: {
          itemStyle: {
            borderDashOffset: 30,
          },
        },
        tooltip: {
          trigger: 'item',
          formatter: (params) => {
            return `${params.name}: <span class="text-black" style="font-weight: 600;">${formatNumber(params.value || 0, 'en', '1.0-0')}</span><br>Tỷ lệ: <span class="text-black" style="font-weight: 600;">${params.percent}%<span>`;
          },
        },
        legend: {
          show: false,
          bottom: 0,
        },
        series: [
          {
            emphasis: {
              scaleSize: 13,
              itemStyle: {
                borderWidth: 8,
              }
            },
            // bottom: '40%',
            type: EChartType.Pie,
            radius: ['30%', '80%'],
            itemStyle: {
              borderColor: '#fff',
              borderWidth: 2,
            },
            labelLine: {
              length: 5,
            },
            label: {
              formatter: (params) =>  (`${formatNumber(Number(params.value), 'en', '1.0-0')} KH`),
            },
            data: [],

          },
        ],
      };
      let index = 0;
      const seriesData = [];
      _.forEach(this.data, (item, i) => {
        if (item.value > 0) {
          seriesData.push({
            value: item.value,
            name: item.label,
            itemStyle: { color: item.color },
            code: item.code,
            indexItem: index,
            selected: this.preClickData.indexItem === i,
          });
          item.indexItem = index;
          index ++;
        }
        this.option.series[0].data = seriesData;
      });
    } else {
      this.data = undefined;
    }
  }

  checkViewDashboard(index, typeAction) {
    if(index >= 0){
      if (!this.myChart) {
        const myChart = echarts.init(document.getElementById('chartId') as any);
        if (!myChart) {
          return;
        }
        this.myChart = myChart;
      }

      // myChart.on('rendered', () => {
      // });
      this.myChart.dispatchAction({
        type: typeAction,
        seriesIndex: 0,
        dataIndex: index,
      })
    }
  }

  onClickChart(data){
    this.isLoading = true;
    if (this.preClickData && data?.code !== this.preClickData?.code) {
      this.checkViewDashboard(this.preClickData.indexItem, this.DOWNPLAY);
      this.paramList.status = data?.code;
      this.preClickData = data;
      if (data.value > 0) {
        this.checkViewDashboard(data.indexItem, this.HIGHTLIGHT);
      }
    }
    else{
      this.isLoading = false;
      return;
    }

    this.search(true);
  }

  checkView(preClickData) {
    const test = document.getElementById('chartId');
    if (test) {
      this.checkViewDashboard(preClickData.indexItem, this.HIGHTLIGHT);
      clearTimeout(this.timeout);
    } else {
      this.timeout = setTimeout(() => this.checkView(preClickData), 1000);
    }
  }

  onActive(event){
    if (event.type === 'click' && event.column.prop === 'rmInfo'){
      this.isLoading = true;
      localStorage.setItem('DETAIL_RM_CODE', JSON.stringify(event?.row?.assignedUserId));

      const campaignId = this.paramChart.campaignId;
      const branchCode = this.paramChart.branchCode === 'ALL' ? undefined : this.paramChart.branchCode;
      const hrsCode = event?.row?.assignedUserId;
      const stage = this.paramList?.status;
      const rmCode = event?.row?.rmPerform;

      const urlTree = this.router.createUrlTree([functionUri.campaign_detail, campaignId, ], {
        queryParams: { hrsCode, branchCode, stage, rmCode},
        skipLocationChange: true,
      });

      const url = this.router.serializeUrl(urlTree);

      window.open(url, '_blank');

      this.isLoading = false;
    }
  }

  setPage(event){
    if(this.isLoading){
      return;
    }
    this.paramList.page = event.offset;
    this.search(false);
  }

  formatNumber(value){
    return formatNumber(Number(value), 'en', '1.0-0');
  }

  formatPercent(value): string {
    return `${(Number(value)).toFixed(2)}%`;
  }

  handleChangePageSize(event){
    this.paramList.page = 0;
    this.paramList.size = event;
    this.limit = event;
    this.search(false);
  }

  ngAfterViewInit(): void {
  }
}
