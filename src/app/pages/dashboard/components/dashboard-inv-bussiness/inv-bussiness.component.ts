import {
  Component,
  OnInit,
  EventEmitter,
  Injector,
  ViewChild,
  ViewEncapsulation,
  HostListener,
  Input,
  Output,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import {
  BRANCH_HO,
  CommonCategory, DashboardOtherIndiv, DashboardType,
  DATE_TYPES,
  DAY_MONTH,
  FunctionCode,
  ORDER_CHART,
  RECORD,
  REPORT_ID,
  Scopes,
  TYPE_MESSAGE_NEXTGEN,
} from 'src/app/core/utils/common-constants';
import * as moment from 'moment';
import { environment } from 'src/environments/environment';
import { CasaVolatilityComponent } from '../casa-volatility/casa-volatility.component';
import { DashboardService } from '../../services/dashboard.service';
import * as _ from 'lodash';
import { forkJoin, of } from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import { ChartContainer, FetchChart } from '../../models/chart-customer.model';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { RmApi } from 'src/app/pages/rm/apis';

@Component({
  selector: 'app-inv-bussiness',
  templateUrl: './inv-bussiness.component.html',
  styleUrls: ['./inv-bussiness.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class InvBussinessComponent extends BaseComponent implements OnInit, OnChanges {

  @Input() branchCodeFilter: string;
  @Output() onLoading: EventEmitter<any> = new EventEmitter();
  @Input() isRm: boolean;
  @Input() state: Array<any>;
  @Input() listBranch: Array<any>;
  branchMobilizeStr : string;
  rowMenu: any;
  dashboard: any;
  listType = [];
  actionType = REPORT_ID;
  // dataBranch = [];
  branchCodes = [];
  codeBranch = [];
  chartData = [];
  balChangeMax: any;
 // state: any;
  listRM = [];
  form = this.fb.group({
    divisionCode: 'INDIV',
    type: '',
    branchCodeLv1: '',
    branchCode: '',
    rmCode: '',
    regionCode: '',
    isRegion: false,
    dateType: [{ value: DATE_TYPES.MONTH, disabled: true }],
    fromDate: new Date(new Date(moment().year(), moment().month() - 4)),
    toDate: new Date(moment().year(), moment().month() - 1),
  });
  isRmLoading = false;
  isFirst = true;
  isChangeSide = false;
  data : any;
  chartDefault: FetchChart = {
    data: [],
    loading: false,
    max: 0,
    maxBalChange: 0,
    request: {
      report_id: '',
      fluctype: '01',
      datatype: '01',
      limit: 10,
    },
    tranDate: ''
  };

  sortOrders = ORDER_CHART;
  dayMonths = DAY_MONTH;
  records = RECORD;

  actionTypeLst = [
    { code: 'CASA', value: '01', filter: true },
    { code: 'DEPO', value: '02', filter: true },
    { code: 'CREDIT', value: '9999', filter: false },
  //  { code: 'CARD', value: '03' },
  ];
  dashboardTypeLst = ['HDV_INDIV','DUNO_INDIV'];

  chartContainer: ChartContainer = {};
  isHO = false;
  constructor(
    injector: Injector,
    private rmApi: RmApi,
    private categoryService: CategoryService,
    private service: DashboardService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
  }

  ngOnInit(): void {

  }
  parseJSON(value: string) {
    if (value) {
      return JSON.parse(value);
    }
    return null;
  }

  // loadData() {
  // //  this.branchMobilizeStr = this.branchCodeFilter;
  // //   if(this.isHO || this.branchCodeFilter !== 'ALL'){
  // //     this.branchMobilizeStr = this.branchCodeFilter;
  // //   }
  //   forkJoin([
  //     this.categoryService.getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW).pipe(catchError(() => of(undefined))),
  //     this.commonService.getCommonCategory(CommonCategory.DASHBOARD_BUSINESS_COLOR).pipe(catchError(() => of(undefined)))
  //   ]).subscribe(([lstBranch,lstCommon]) => {
  //     this.state = _.get(lstCommon,'content',[]);
  //     if (!_.isEmpty(lstBranch)) {
  //       this.listBranch = lstBranch;
  //       this.listBranch.forEach((item) => {
  //         if (item.code) {
  //           this.branchCodes.push(item.code);
  //         }
  //       });
  //       if(!this.isHO && this.branchMobilizeStr === 'ALL'){
  //         this.branchMobilizeStr = this.branchCodes.join(',');
  //       }
  //       // else{
  //       //   this.branchMobilizeStr = this.branchCodeFilter;
  //       // }
  //       if (this.branchCodes.length > 0) {
  //         this.getDataCbql(this.branchCodeFilter)
  //       }
  //     } else {
  //       this.actionTypeLst.forEach((item) => {
  //         if (!this.chartContainer[item.value]) {
  //           this.chartContainer[item.value] = JSON.parse(JSON.stringify({ ...this.chartDefault }));
  //         }
  //         this.chartContainer[item.value].request.report_id = item.value;
  //         this.chartContainer[item.value].loading = true
  //         this.getChartData(this.chartContainer[item.value].request);
  //       });
  //     }
  //   });
  //   this.isLoading = false;
  // }

  loadData() {
    if (!_.isEmpty(this.listBranch)) {
      this.listBranch.forEach((item) => {
        if (item.code) {
          this.branchCodes.push(item.code);
        }
      });
      if(!this.isHO && this.branchCodeFilter === 'ALL'){
        this.branchMobilizeStr = this.branchCodes.join(',');
      }
      else{
        this.branchMobilizeStr = this.branchCodeFilter;
      }
      if (this.branchCodes.length > 0) {
        this.getDataCbql(this.branchCodeFilter)
      }
    } else {
      this.actionTypeLst.forEach((item) => {
        if (!this.chartContainer[item.value]) {
          this.chartContainer[item.value] = JSON.parse(JSON.stringify({ ...this.chartDefault }));
        }
        this.chartContainer[item.value].request.report_id = item.value;
        this.chartContainer[item.value].loading = true
        this.getChartData(this.chartContainer[item.value].request);
      });
    }
  }
  getDataCbql(branch) {
    const branCode = branch.toString();
    this.actionTypeLst.forEach((item) => {
      if (!this.chartContainer[item.value]) {
        this.chartContainer[item.value] = JSON.parse(JSON.stringify({ ...this.chartDefault }));
      }
      this.chartContainer[item.value].request.report_id = item.value;
      this.chartContainer[item.value].request.branchcodecap2 = branCode;
      this.chartContainer[item.value].loading = true
      this.getChartData(this.chartContainer[item.value].request);
    });
  }

  getChartData(params) {
    this.isLoading = true;
    if (params.branchcodecap2) {
      params.rsId = this.objFunction.rsId;
      params.scope = 'VIEW';
      this.service.getChartTopCustomerCbql(params).subscribe(
        (res) => {
          if (res) {
            const max = _.maxBy(res, 'bal');
            const maxBalChange = _.maxBy(res, 'balChange');
            this.chartContainer[params.report_id] = {
              ...this.chartContainer[params.report_id],
              data: res,
              loading: false,
              max: max?.bal || 0,
              maxBalChange: maxBalChange?.balChange || 0,
              tranDate: (res[0]?.transactionDate || res[0]?.dsnapshotDate || '').split(' ')[0]
            };
          }
        },
        (e) => {
          this.messageService.error(e.error.description);
          this.isLoading = false;
        }
      );
    } else {
      this.service.getChartTopCustomer(params).subscribe(
        (res) => {
          if (res) {
            const max = _.maxBy(res, 'bal');
            const maxBalChange = _.maxBy(res, 'balChange');
            this.chartContainer[params.report_id] = {
              ...this.chartContainer[params.report_id],
              data: res,
              loading: false,
              max: max?.bal || 0,
              maxBalChange: maxBalChange?.balChange || 0,
              tranDate: (res[0]?.transactionDate || res[0]?.dsnapshotDate || '').split(' ')[0]
            };
          }
        },
        (e) => {
          this.messageService.error(e.error.description);
          this.isLoading = false;
        }
      );
    }
    this.isLoading = false;
  }

  recieveRequest(request) {
    console.log(request);
  }

  onShowLoading($event: any) {
    if ($event)
    {
      this.onLoading.emit(true)
    } else {
      this.onLoading.emit(false)
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.isHO = this.currUser?.branch === BRANCH_HO;
    if (changes.branchCodeFilter && this.state && this.listBranch) {
      console.log('branchFilter: ', this.branchCodeFilter);
      if(changes.branchCodeFilter.firstChange)
      {
        this.actionTypeLst.forEach((item) => {
          if (!this.chartContainer[item.value]) {
            this.chartContainer[item.value] = JSON.parse(JSON.stringify({ ...this.chartDefault }));
          }
          this.chartContainer[item.value].request.report_id = item.value;
          this.chartContainer[item.value].request.fluctype = '03';
          this.chartContainer[item.value].loading = true
        });
        this.loadData();
      } else {
        this.loadData()
      }
    }
  }
}
