import {
  Component,
  ViewEncapsulation,
  OnInit,
  EventEmitter,
  Input,
  Injector
} from '@angular/core';
import { EChartsOption } from 'echarts';
import { AppFunction } from 'src/app/core/interfaces/app-function.interface';
import { FunctionCode, Scopes, functionUri } from 'src/app/core/utils/common-constants';
import { DashboardService } from '../../services/dashboard.service';
import * as _ from 'lodash';
import { ImformationChartModel } from '../../models/imformation-chart.model';
import {BaseComponent} from '../../../../core/components/base.component';
import {CategoryService} from '../../../system/services/category.service';

@Component({
  selector: 'card-spending',
  templateUrl: './card-spending.component.html',
  styleUrls: ['./card-spending.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CardSpendingComponent extends BaseComponent implements OnInit {
  @Input() viewChange: EventEmitter<boolean> = new EventEmitter();
  @Input() data: any;
  @Input() flag: any;
  dataChart: any[];
  isLoading = false;
  isTable = false;
  option: EChartsOption;
  objFunction: AppFunction;
  chartData: ImformationChartModel[] = [];
  chartDataDisplay: any[] = [];
  balChangeMax: any;
  branchCodes = [];
  listBranch = [];


  paramDisplay = {
    report_id: '03',
    fluctype: '01',
    datatype: '01',
    limit: 10
  };

  sortOrders = [
    { id: '01', value: 'Tăng' },
    { id: '02', value: 'Giảm' }
  ];
  dayMonths = [
    { id: '01', value: 'Ngày' },
    { id: '02', value: 'Tháng'}
  ];
  records = [
    { id: 1, value: 10 },
    { id: 2, value: 20 },
    { id: 3, value: 30 },
    { id: 4, value: 40 },
    { id: 5, value: 50 }
  ];

  formSearch = this.fb.group({
    fluctype: '01',
    datatype: '01',
    limit: '10'
  });

  constructor(injector: Injector, private service: DashboardService, private categoryService: CategoryService,) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DASHBOARD_CUSTOMER}`);
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.getChartData();
  }

  switchChart() {
    if (this.isLoading) {
      return;
    }
    this.isTable = !this.isTable;
  }

  getChartData() {
    this.isLoading = true;
    const params = {
      ... this.formSearch.getRawValue(),
      branchcodecap2: _.isEmpty(this.data?.branchCodes) ? 'ALL' : this.data?.branchCodes.toString(),
      rsId: this.objFunction.rsId,
      scope: 'VIEW',
      report_id: '03'
    };

    if (this.data?.isRm) {
      this.service.getChartTopCustomer(params).subscribe(res => {
        if(res) {
          this.chartData = res;
          this.balChangeMax = this.chartData[0]?.balChange;
        }
        this.isLoading = false;
      }, error =>  {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      });
    } else {
      this.service.getChartTopCustomerCbql(params).subscribe(res => {
        if(res) {
          this.chartData = res;
          this.balChangeMax = this.chartData[0]?.balChange;
        }
        this.isLoading = false;
      }, error =>  {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      });
    }
  }

  showDetailCustomer360(customerCode) {
    localStorage.setItem('DETAIL_CUS_ID', JSON.stringify(customerCode));
    const url = this.router.serializeUrl(
      this.router.createUrlTree([functionUri.customer_360_manager], {
        skipLocationChange: true,
      })
    );
    window.open(url, '_blank');
  }

  onActivate(event) {
    if(event.type === 'click') {
      this.showDetailCustomer360(event.row.customerCode);
    }
  }
}
