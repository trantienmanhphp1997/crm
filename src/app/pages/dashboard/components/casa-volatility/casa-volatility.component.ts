import { Component, EventEmitter, Injector, Input, OnChanges, OnInit, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { AppFunction } from 'src/app/core/interfaces/app-function.interface';
import {
  ConfirmType,
  DAY_MONTH,
  FunctionCode, functionUri,
  ORDER_CHART,
  RECORD,
  Scopes,
  TYPE_MESSAGE_NEXTGEN
} from 'src/app/core/utils/common-constants';
import { FetchChart } from '../../models/chart-customer.model';
import { DashboardService } from '../../services/dashboard.service';
import * as _ from 'lodash';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { CustomerApi } from '../../../customer-360/apis';

@Component({
  selector: 'app-casa-volatility',
  templateUrl: './casa-volatility.component.html',
  styleUrls: ['./casa-volatility.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CasaVolatilityComponent extends BaseComponent implements OnInit, OnChanges {
  @Output() sendRequest: EventEmitter<any> = new EventEmitter();
  @Output() onShowLoading: EventEmitter<any> = new EventEmitter();
  @Input() actionType: string;
  @Input() chartFetchData: FetchChart;
  @Input() showFilter = true;
  isTable = false;
  objFunction: AppFunction;
  selectedItemFilter: any;

  itemPermission = {
    customerCode: '',
    rsId: '',
    scope: Scopes.VIEW,
  };

  SORTS1 = [
    { id: 1, name: 'Tăng', key: '01', icon: './assets/images/arrow-up.svg',},
    { id: 2, name: 'Giảm', key: '02', icon: './assets/images/arrow-down.svg' },
    { id: 3, name: 'Số dư', key: '03', icon: './assets/images/icon-bars.svg' }
  ]

  SORTS2 = [
    { id: 3, name: 'Số dư', key: '03', icon: './assets/images/icon-bars.svg' }
  ]

  sortOrders = this.SORTS1

  OPTION1 = [
    { id: 1, value: 'Ngày', key: '01' }
  ];

  OPTION2 = [
    { id: 1, value: 'Ngày', key: '01' },
    { id: 2, value: 'Tháng', key: '02' }
  ];

  dayMonths = this.OPTION2

  filterType = {
    sort: 1,
    date: 2,
    size: 3
  }
  records = [
    { id: 1, value: 10, text: 'Top 10' },
    { id: 2, value: 20, text: 'Top 20' },
    { id: 3, value: 30, text: 'Top 30' },
    { id: 4, value: 40, text: 'Top 40' },
    { id: 5, value: 50, text: 'Top 50' }
  ];

  formSearch = this.fb.group({
    fluctype: '',
    datatype: '',
    limit: '',
  });

  constructor(injector: Injector, private customerApi: CustomerApi, private service: DashboardService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.itemPermission.rsId = this.objFunction.rsId;
  }

  ngOnInit(): void {
    console.log('this.chartFetchData ', this.chartFetchData);
    if(this.showFilter)
    {
      this.sortOrders = this.SORTS1
      this.onSelectItemFilter(this.sortOrders[2])
    } else {
      this.sortOrders = this.SORTS2
      this.onSelectItemFilter(this.sortOrders[0])
    }
  }

  ngOnChanges(changes?: SimpleChanges) {
    console.log(changes);
  }

  getDataChart(event, type) {
    this.chartFetchData.loading = true
    if (type === this.filterType.size) {
      // filter page size
      const pageSize = _.find(this.records, (limit) => limit.id === event.value)?.value;
      this.chartFetchData.request.limit = pageSize;
    } else if (type === this.filterType.date) {
      // filter date, months
      this.chartFetchData.request.datatype = event.value
    } else if (type === this.filterType.sort) {
      // filter asc, desc, amount
      this.chartFetchData.request.fluctype = event.key
    }
    this.sendRequest.emit(this.chartFetchData.request);
  }

  switchChart() {
    if (this.isLoading) {
      return;
    }
    this.isTable = !this.isTable;
  }

  showDetailCustomer360(customerCode) {
    const param = {
      customerCode,
    };

    this.onShowLoading.emit(true)
    // checkCustomerPermission
    this.customerApi.checkCustomerPermission(param).subscribe(
      (res) => {
        if (res) {
          localStorage.setItem('DETAIL_CUS_ID', JSON.stringify(customerCode));
          const url = this.router.serializeUrl(
            this.router.createUrlTree([functionUri.customer_360_manager], {
              skipLocationChange: true,
            })
          );
          window.open(url, '_blank');
        } else {
          this.openModalInfo();
        }
        this.onShowLoading.emit(false)
      },
      (e) => {
        console.log(e);
        this.messageService.error('Thực hiện không thành công');
        this.onShowLoading.emit(false)
      }
    );
  }

  openModalInfo() {
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.componentInstance.type = ConfirmType.Warning;
    confirm.componentInstance.message = 'Bạn không có quyền xem thông tin KH do không được phân giao quản lý KH này';
    confirm.result
      .then((res) => {
        if (res) {
          console.log(res);
        }
      })
      .catch(() => {});
  }

  onSelectItemFilter(item: any) {
    this.selectedItemFilter = item
    if (item.id === 3)
    {
      // chọn số dư
      this.dayMonths = this.OPTION1
    } else {
      this.dayMonths = this.OPTION2
    }
    this.getDataChart(item, 1)
  }
}
