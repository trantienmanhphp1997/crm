import { AfterViewInit, Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';


import { global } from '@angular/compiler/src/util';
import _ from 'lodash';
import * as echarts from 'echarts';
import { EChartsOption } from 'echarts';
import { formatDate, formatNumber } from '@angular/common';
import { BaseComponent } from '../../../../core/components/base.component';
import { Pageable } from '../../../../core/interfaces/pageable.interface';
import {
  BRANCH_HO,
  CustomerType,
  DashboardType,
  EChartType,
  FunctionCode,
  functionUri,
  Scopes,
  SessionKey
} from '../../../../core/utils/common-constants';
import { CampaignsService } from 'src/app/pages/campaigns/services/campaigns.service';
import { percentagev2 } from 'src/app/core/utils/function';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'chart-campaign-result-detail',
  templateUrl: './chart-campaign-result-detail.component.html',
  styleUrls: ['./chart-campaign-result-detail.component.scss'],
})
export class ChartCampaignResultDetailComponent extends BaseComponent implements OnInit {
  form = this.fb.group({
    branchCode: '',
  });
  data: any;
  listDataCard = [];
  listBranchCode = '';
  listBranch: Array<any>;
  pageable: Pageable;
  option: EChartsOption;
  limit = global.userConfig.pageSize;
  statusNameCard = 'Thông tin chi tiết';
  isHO = false;
  paramsDetail = {
    rsId: '',
    scope: Scopes.VIEW,
    branchCode: '',
    page: 0,
    size: global?.userConfig?.pageSize,
    resultActivity: '',
    campaignId: '',
    listBranchCode: [],
  };
  paramsExport = {
    rsId: '',
    scope: 'VIEW',
    isRm: true,
    transactionDate: '',
    rmCode: '',
  };
  paramCharts = {
    rsId: '',
    scope: Scopes.VIEW,
    branchCode: '',
    campaignId: '',
  };
  isClick = false;
  isBack = false;
  prevParams: any;
  preClickData: any;
  private myChart: any = null;
  timeout;
  HIGHTLIGHT = 'highlight';
  DOWNPLAY = 'downplay';
  isRm = true;
  dataDate: Date;
  dateSnapshot: string;
  dataTotal: number;
  PARAMS_KEY = 'FUNCTION_CAMPAIGN_DETAIL_PARAMS'
  isbranchDimention = true;
  campaignId = '';
  prevOverviewData : Array<any>;
  leftItems  = [];
  rightItems = [];
  dataPercentage: string[] = [];
  chartData: [];
  constructor(
    injector: Injector,
    private campaignService: CampaignsService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DASHBOARD_CUSTOMER}`);
    this.prevParams = _.get(this.router.getCurrentNavigation(), 'extras.state');
    this.paramsDetail.rsId = this.objFunction?.rsId;
    this.paramCharts.rsId = this.objFunction?.rsId;
    this.paramsExport.rsId = this.objFunction?.rsId;

    this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
    if (this.prevParams) {
      localStorage.setItem(this.PARAMS_KEY, JSON.stringify(this.prevParams))
      this.handlerPrevData()
    } else if (localStorage.getItem(this.PARAMS_KEY)){
      this.prevParams = JSON.parse(localStorage.getItem(this.PARAMS_KEY))
      this.handlerPrevData()
    }


  }

  ngOnInit(): void {
    this.isHO = this.currUser?.branch === BRANCH_HO;
    this.pageable = {
      totalElements: 0,
      totalPages: 0,
      currentPage: this.paramsDetail.page,
      size: this.limit,
    };
    this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
    this.data = this.prevParams?.data;

    this.paramsDetail.page = 0;
    this.paramsDetail.size = 10;

    this.paramsDetail.rsId = this.objFunction?.rsId;
    this.paramsDetail.branchCode = this.listBranchCode;
    this.paramsDetail.campaignId = this.campaignId;
    this.paramsDetail.resultActivity = this.data?.code;
    this.paramsDetail.listBranchCode = this.prevParams?.listBranchCode;;

    this.processChartData(this.prevOverviewData);
    // this.isLoading = false;
    // this.search();

    setTimeout(()=> {
          this.onClickChart(this.prevParams?.data);
    }, 1000);
  }

  handlerPrevData() {

    this.isBack = true;
    this.listBranchCode = this.prevParams?.branch ;
    this.isRm = _.isEmpty(this.listBranchCode);
    this.isbranchDimention = this.prevParams?.isbranchDimention ;
    this.campaignId = this.prevParams?.campaignId;
    this.prevOverviewData = this.prevParams?.prevdata;
    if (this.prevParams?.data) {
      this.paramsDetail.resultActivity =  this.prevParams?.data.code;
      // this.preClickData = this.prevParams?.data;
      this.preClickData = {...this.prevParams?.data};
      this.preClickData.code = -1;
    }



  }

  search(isSearch?) {
    this.isLoading = true;

    if(isSearch){
      this.paramsDetail.page = 0;
      this.paramsDetail.size = 10;
    }

    this.campaignService.getCampaignResultDetail(this.paramsDetail)
    .pipe(finalize(()=>(this.isLoading = false)))
    .subscribe(
      (result) => {
        this.handleTableData(result);
        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );


  }

  private processChartData(data: any) {
    const today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0');
    const yyyy = today.getFullYear();

    const formattedDate = dd + '/' + mm + '/' + yyyy;

    if(data?.length === 0) {
      data = {data: [], total: null, date: null, code: null}
      this.data = data.data;
    } else {
      this.data = data;
    }
    this.dateSnapshot = formattedDate
    if (_.size(this.data) > 0) {
      // split column for legend
      for (let i = 0; i < this.data.length; i++) {
        if (i % 2 === 0) {
            this.leftItems.push(this.data[i]);
        } else {
            this.rightItems.push(this.data[i]);
        }
      }
      const sum = _.reduce(
        _.map(this.data, (x) => x.value),
        (a, c) => {
          return a + c;
        }
      );
      this.dataTotal = sum
      _.forEach(this.data, (item) => {
        this.dataPercentage.push(percentagev2(item.value || 0, sum));
      });
      this.handleMapData();
    }
  }

  private handleTableData(data: any) {
    this.listDataCard = data ? data?.content : [];
    this.pageable = {
      totalElements: data?.totalElements || 0,
      totalPages: data?.totalPages || 0,
      currentPage: this.paramsDetail.page,
      size: this.paramsDetail.size,
    };

    this.listDataCard = this.listDataCard.map((item) => {
      return {
        ...item,
        fullName: item.rmCode + ' - ' + item.fullName,
      }
    });
  }

  checkView(preClickData) {
    const chart = document.getElementById('chartId');
    if (chart) {
      this.resetAndHightLightChart(preClickData.indexItem, this.HIGHTLIGHT);
      clearTimeout(this.timeout);
    } else {
      this.timeout = setTimeout(() => this.checkView(preClickData), 1000);
    }
  }

  setPage(pageInfor) {
    if (this.isLoading) {
      return;
    }
    this.paramsDetail.page = pageInfor.offset;
    this.search();
  }


  onClickChartV2(data, isFirstCall?: boolean, chart?) {
    if (this.dataTotal && this.dataTotal <= 0) {
      return;
    }

    if (data?.code === this.preClickData?.code && !isFirstCall) {
      return;
    }

    this.isLoading = true;
    if (data?.code === this.preClickData?.code || data?.code === undefined) {
      this.preClickData = null;
      this.paramsDetail.resultActivity = '';
      this.resetHighLight();
    } else {
      this.preClickData = data;
      if (data.value > 0) {
        this.resetAndHightLightChart(data.indexItem, this.HIGHTLIGHT);
      }
    }

    if (!isFirstCall) {
      this.paramsDetail.branchCode = this.listBranchCode;
      this.paramsDetail.page = 0;
      this.paramsDetail.size = 10;
      this.paramsDetail.resultActivity = data?.code;
      this.campaignService.getCampaignResultDetail(this.paramsDetail)
        .pipe(finalize(() => (isFirstCall = false)))
        .subscribe(
        (result) => {
          this.handleTableData(result);
          this.isLoading = false;
        },
        (err) => {
          this.isLoading = false;
          this.messageService.error(this.notificationMessage.error);
        }
      );
    }
  }

  handleMapData() {
    if (this.data) {
      this.option = {
        emphasis: {
          itemStyle: {
            borderDashOffset: 30,
          },
        },
        tooltip: {
          trigger: 'item',
          formatter: (params) => {
            return `<div class="text-black" style="width: 150px; font-weight: 600; white-space: pre-wrap;"><span style="font-weight: 400;">${params.name}</span>: ${formatNumber(params.value || 0, 'en', '1.0-0')}</div><div class="text-black" style="font-weight: 600; white-space: pre-wrap; "><span style="font-weight: 400;">Tỷ lệ</span>: ${percentagev2(params.value, this.dataTotal)}%</div>`;
          },
        },
        legend: {
          show: false,
          bottom: 0,
        },
        series: [
          {
            // bottom: '40%',
            emphasis: {
              scaleSize: 13,
              itemStyle: {
                borderWidth: 8,
              }
            },
            type: EChartType.Pie,
            radius: ['30%', '80%'],
            itemStyle: {
              borderColor: '#fff',
              borderWidth: 2,
            },
            labelLine: {
              length: 5,
            },
            label: {
              formatter: (params) =>  (`${formatNumber(Number(params.value), 'en', '1.0-0')} KH`),
            },
            data: [],
          },
        ],
      };
      let index = 0;
      const seriesData = [];
      _.forEach(this.data, (item, i) => {
        if (item.value > 0) {
          seriesData.push({
            value: item.value,
            name: item.label,
            itemStyle: { color: item.color },
            code: item.code,
            indexItem: index,
            selected: this.preClickData.indexItem === i,
          });
          item.indexItem = index;
          index ++;
        }
        this.option.series[0].data = seriesData;
      });
    } else {
      this.data = undefined;
    }
  }



  back() {
    const data = {
      block: CustomerType.INDIV,
      type: DashboardType.INDIV_CAMPAIGN,
      campaignId: this.prevParams?.campaignId,
      campaignList: this.prevParams?.campaignList,
      branchCode: this.prevParams?.branch,
      regionCode: this.prevParams?.regionCode,
    };
    this.router.navigate(['/dashboard'], {
      state: data,
    });
  }

  onclickLegend(currentData: any) {
    if (currentData.value === 0) {
      this.paramsDetail.resultActivity = currentData?.code;
      this.preClickData = {code: currentData?.code} ;
      this.resetHighLight();
      this.search();
      return;
    }
    const selectedData = this.option.series[0].data.filter(item => (item.code === currentData.code))
    this.onClickChart(selectedData.length > 0 ? selectedData[0] : 0);
  }

  handleChangePageSize(event) {
    this.pageable.currentPage = 0;
    this.pageable.size = event;
    this.paramsDetail.size = event;
    this.paramsDetail.page = 0;
    this.limit = event;
    this.search();
  }

  resetHighLight() {
    _.forEach(this.data, (value, i) => {
      this.hightLightSelected(i, this.DOWNPLAY);
    });
  }

  resetAndHightLightChart(index: number, type: string) {
    _.forEach(this.data, (value, i) => {
      this.hightLightSelected(i, this.DOWNPLAY);
    });
    this.hightLightSelected(index, type);
  }

  hightLightSelected(index, typeAction) {
    const myChart = echarts.init(document.getElementById('chartId') as any);
    if (!myChart) {
      return;
    }
    if (!this.myChart) {
      this.myChart = myChart;
    }

    myChart.on('rendered', () => {
    });

    myChart.dispatchAction({
      type: typeAction,
      seriesIndex: 0,
      dataIndex: index,
    });
  }

  onActive(event) {
    if (event.type === 'click' && event.column.prop === 'fullName') {
      this.showCampaignDetail(event.row.hrisCode, event.row.rmCode);
    }
  }

  showCampaignDetail(_hrsCode, rmCode) {
    this.isLoading = true;

    if (this.paramsDetail.resultActivity === 'UNDEFINED') {
      this.isLoading = false;
      return;
    }
    localStorage.setItem('DETAIL_CAMPAIGN', JSON.stringify(_hrsCode));

    const campaignId = this.paramsDetail.campaignId;
    const branchCode = this.paramsDetail.branchCode === 'ALL' ? undefined : this.paramsDetail.branchCode;
    const hrsCode = _hrsCode;
    const activityResult = this.paramsDetail.resultActivity;


    const urlTree = this.router.createUrlTree([functionUri.campaign_detail, campaignId, ], {
      queryParams: { hrsCode, branchCode, activityResult, rmCode},
      skipLocationChange: true,
    });

    const url = this.router.serializeUrl(urlTree);

    window.open(url, '_blank');

    this.isLoading = false;
  }

  formatPercent(value: number): string {
    return `${(value).toFixed(2)}%`;
  }

  formatCustomerNumber(number : Number) : String {
    return formatNumber(Number(number), 'en', '1.0-0')
  }

  onClickChart(data){
    this.isLoading = true;
    if (this.preClickData && data?.code !== this.preClickData?.code) {
      this.checkViewDashboard(this.preClickData.indexItem, this.DOWNPLAY);
      this.paramsDetail.resultActivity = data?.code;
      this.preClickData = data;
      if (data.value > 0) {
        this.checkViewDashboard(data.indexItem, this.HIGHTLIGHT);
      }
    }
    else{
      this.isLoading = false;
      return;
    }

    this.search(true);
  }

  checkViewDashboard(index, typeAction) {
    if(index >= 0){
      if (!this.myChart) {
        const myChart = echarts.init(document.getElementById('chartId') as any);
        if (!myChart) {
          return;
        }
        this.myChart = myChart;
      }

      // myChart.on('rendered', () => {
      // });
      this.myChart.dispatchAction({
        type: typeAction,
        seriesIndex: 0,
        dataIndex: index,
      })
    }
  }

}
