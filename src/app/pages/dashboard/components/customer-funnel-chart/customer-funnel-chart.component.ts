import { ChangeDetectorRef, Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import * as moment from 'moment';
import { BaseComponent } from 'src/app/core/components/base.component';
import { DashboardService } from '../../services/dashboard.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-customer-funnel-chart',
  templateUrl: './customer-funnel-chart.component.html',
  styleUrls: ['./customer-funnel-chart.component.scss']
})
export class CustomerFunnelChartComponent extends BaseComponent implements OnInit {


  data: any;
  option1: any;
  option2: any;
  form = this.fb.group({
    dateOption: '',
    dateBitz: '',
    year: ''
  })
  dateTimeBitzOption = [
    {
      name: 'Theo tháng', code: 'month'
    },
    {
      name: 'Theo năm', code: 'year'
    }
  ];
  maxYear = 2028;
  minYaer = 2018
  rangerYear = [];
  CstProfileInfo = [];
  constructor(injector: Injector, private dashboardService: DashboardService) {
    super(injector);
    for (let index = this.minYaer; index <= this.maxYear; index++) {
      this.rangerYear.push({ name: index, code: index.toString() })
    }
  }

  ngOnInit() {
    this.form.controls.dateOption.setValue('month');
    console.log(moment().year());
    this.form.controls.year.setValue((moment().year() - 1).toString());
    this.form.valueChanges.subscribe(res => {
      this.getDigitalTransform();
    })
    this.form.controls.dateBitz.setValue(new Date(moment().year(), moment().month() - 1))
    if (this.data) {
      this.getCstProfileInfo();
    }
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.ref.detectChanges();
    }, 0);
  }

  generateOption1(option) {
    if (!option) {
      return;
    }
    this.option1 = {
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          // Use axis to trigger tooltip
          type: 'shadow', // 'shadow' as default; can also be 'line' or 'shadow'
        }
      },
      legend: {
        bottom: '0'
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '15%',
        containLabel: true
      },
      xAxis: {
        type: 'value'
      },
      yAxis: {
        type: 'category',
        data: ['KH active BIZ', 'KH Active đăng ký BIZ', 'KH Active', 'KH 360']
      },
      series: [
        {
          type: 'bar',
          stack: 'total',
          label: {
            show: true
          },
          emphasis: {
            focus: 'series'
          },
          data: [null, option?.nbrCstActiveBizF, option?.nbrCstStActive, option?.totalNbrCst]
        },
        {
          name: 'KH có GD BIZ trong 30 này',
          type: 'bar',
          stack: 'total',
          label: {
            show: true
          },
          emphasis: {
            focus: 'series'
          },
          data: [option?.nbrCstActiveBiz30D]
        },
        {
          name: 'KH có GD BIZ từ 31-60 ngày',
          type: 'bar',
          stack: 'total',
          label: {
            show: true
          },
          emphasis: {
            focus: 'series'
          },
          data: [option?.nbrCstActiveBiz60D]
        },
        {
          name: 'KH có GD BIZ từ 61-90 ngày',
          type: 'bar',
          stack: 'total',
          label: {
            show: true
          },
          emphasis: {
            focus: 'series'
          },
          data: [option?.nbrCstActiveBiz90D]
        }
      ]
    };
  }

  generateOption2(option = []) {
    console.log(option);
    if (!option.length) {
      return;
    }
    this.option2 = {
      tooltip: {
        trigger: 'axis'
      },
      legend: {
        data: ['Số lượng Khách hàng', 'Số lượng giao dịch'],
        bottom: '0'
      },
      grid: {
        left: '6%',
        right: '8%',
        bottom: '8%',
        containLabel: true
      },
      // toolbox: {
      //   feature: {
      //     saveAsImage: {}
      //   }
      // },
      xAxis: {
        type: 'category',
        data: option.map(item => {
          return this.form.controls.dateOption.value == 'month' ? moment(item.month).format('MM/YYYY') : item.year;
        })
      },
      yAxis: {
        type: 'value'
      },
      series: [
        {
          name: 'Số lượng Khách hàng',
          type: 'line',
          data: option.map(item => {
            return this.form.controls.dateOption.value == 'month' ? item.nbrCstBizMTD : item.nbrCstBizYTD;
          })
        },
        {
          name: 'Số lượng giao dịch',
          type: 'line',
          data: option.map(item => {
            return this.form.controls.dateOption.value == 'month' ? item.nbrTxnBizMTD : item.nbrTxnBizYTD;
          })
        }
      ]
    };
  }

  // dd/mm/YYYY
  // dd/yyyy
  getCstProfileInfo() {
    const params = {
      transactionDate: moment(this.data.date).format('DD/MM/YYYY'),
      blockCode: this.data.divisionCode,
      rmCode: this.data.isRegion ? '' : this.data.rmCode,
      branchCodeCap1: this.data.branchCodeLv1,
      branchCodeCap2: !this.data.branchCode ? this.data.listBranch : this.data.branchCode,
      area: !this.data.regionCode ? this.data.listRegion.map(item => item.locationCode).join(',') : this.data.regionCode
    }
    this.dashboardService.getCstProfileInfo(params).subscribe((res: any) => {
      if (res.length) {
        let listData: any = _.cloneDeep(res[0]);
        // tính tổng
        res.forEach((element, index) => {
          if (!element) {
            return;
          }
          if (index == 0) {
            return;
          }
          Object.keys(element).forEach((key) => {
            if (!+element[key]) {
              return;
            }
            listData[key] = +listData[key] || 0;
            listData[key] = listData[key] + (+element[key] || 0);
          })
        });
        this.generateOption1(listData);
      }
    });
  }

  getDigitalTransform() {
    const formatOption = this.form.controls.dateOption.value == 'month' ? 'MM/YYYY' : 'YYYY';
    const date = this.form.controls.dateBitz.value;
    const params = {
      transactionDate: this.form.controls.dateOption.value == 'month' ? moment(date).format(formatOption) : this.form.controls.year.value,
      blockCode: this.data.divisionCode,
      rmCode: this.data.isRegion ? '' : this.data.rmCode,
      branchCodeCap1: this.data.branchCodeLv1,
      branchCodeCap2: !this.data.branchCode ? this.data.listBranch : this.data.branchCode,
      area: !this.data.regionCode ? this.data.listRegion.map(item => item.locationCode).join(',') : this.data.regionCode
    }
    this.dashboardService.getDigitalTransform(params).subscribe((res: any) => {
      let mapData = [];
      if (this.form.controls.dateOption.value == 'month') {
        mapData = [
          {
            month: moment(moment(date).subtract(5, 'month').calendar()),
            nbrCstBizMTD: 0,
            nbrTxnBizMTD: 0
          },
          {
            month: moment(moment(date).subtract(4, 'month').calendar()),
            nbrCstBizMTD: 0,
            nbrTxnBizMTD: 0
          },
          {
            month: moment(moment(date).subtract(3, 'month').calendar()),
            nbrCstBizMTD: 0,
            nbrTxnBizMTD: 0
          },
          {
            month: moment(moment(date).subtract(2, 'month').calendar()),
            nbrCstBizMTD: 0,
            nbrTxnBizMTD: 0
          },
          {
            month: moment(moment(date).subtract(1, 'month').calendar()),
            nbrCstBizMTD: 0,
            nbrTxnBizMTD: 0
          },
          {
            month: moment(moment(date).subtract(0, 'month').calendar()),
            nbrCstBizMTD: 0,
            nbrTxnBizMTD: 0
          }
        ]
        mapData.forEach((item, index) => {
          const resItem = res.filter(resItem => {
            return moment(resItem.month).format('MM/YYYY') == item.month.format('MM/YYYY');
          })
          if (!resItem.length) {
            return;
          }
          let oBSum: any = _.cloneDeep(resItem[0]);
          // tính tổng
          resItem.forEach((element, index) => {
            if (!element) {
              return;
            }
            if (index == 0) {
              return;
            }
            Object.keys(element).forEach((key) => {
              if (!+element[key]) {
                return;
              }
              if(key == 'nbrCstBizMTD' || key == 'nbrTxnBizMTD'){
                oBSum[key] = +oBSum[key] || 0;
                oBSum[key] = oBSum[key] + (+element[key] || 0);
              }
            })
          });
          mapData[index] = { ...mapData[index], ...oBSum };
        })
      }

      if (this.form.controls.dateOption.value == 'year') {
        const year = this.form.controls.year.value;
        mapData = [
          {
            year: +year - 3,
            nbrCstBizYTD: 0,
            nbrTxnBizYTD: 0
          },
          {
            year: +year - 2,
            nbrCstBizYTD: 0,
            nbrTxnBizYTD: 0
          },
          {
            year: +year - 1,
            nbrCstBizYTD: 0,
            nbrTxnBizYTD: 0
          },
          {
            year: +year,
            nbrCstBizYTD: 0,
            nbrTxnBizYTD: 0
          }
        ];
        mapData.forEach((item, index) => {
          const resItem = res.filter(resItem => {
            if (!resItem.year) {
              return false
            }
            return resItem.year == item.year.toString();
          })
          if (!resItem.length) {
            return;
          }
          let oBSum: any = _.cloneDeep(resItem[0]);
          // tính tổng
          resItem.forEach((element, index) => {
            if (!element) {
              return;
            }
            if (index == 0) {
              return;
            }
            Object.keys(element).forEach((key) => {
              if (!+element[key]) {
                return;
              }
              if(key == 'nbrCstBizYTD' || key == 'nbrTxnBizYTD' ){
                oBSum[key] = +oBSum[key] || 0;
                oBSum[key] = oBSum[key] + (+element[key] || 0);
              }
            })
          });
          mapData[index] = { ...mapData[index], ...oBSum };
        })
      }
      this.generateOption2(mapData);
    });
  }

}
