import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardLeftViewComponent } from './dashboard-left-view.component';

describe('DashboardLeftViewComponent', () => {
  let component: DashboardLeftViewComponent;
  let fixture: ComponentFixture<DashboardLeftViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardLeftViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardLeftViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
