import { trigger, state, style, AUTO_STYLE, transition, animate } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../services/dashboard.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-dashboard-left-view',
  templateUrl: './dashboard-left-view.component.html',
  styleUrls: ['./dashboard-left-view.component.scss'],
  animations: [
    trigger('collapse', [
      state(
        'false',
        style({ height: AUTO_STYLE, visibility: AUTO_STYLE, paddingTop: '0.5rem', borderTop: '1px solid' })
      ),
      state('true', style({ height: '0', visibility: 'hidden' })),
      transition('false => true', animate(150 + 'ms ease-in')),
      transition('true => false', animate(150 + 'ms ease-out')),
    ]),
  ],
})
export class DashboardLeftViewComponent implements OnInit {
  activityCalls: any[];
  role: string;
  branchCode: string;
  constructor(private dashboardService: DashboardService) {
    // this.role = getRole(global.roles);
    // this.branchCode = _.get(global, 'user.attributes.branches[0]');
  }

  ngOnInit(): void {
    this.getData();
    // if (this.role !== Roles.RGM) {
    //   this.dashboardService.onUpdateData().subscribe((res) => {
    //     if (res && res.branchCode === this.branchCode) {
    //       this.getData();
    //     }
    //   });
    // } else {
    //   setInterval(() => {
    //     this.getData();
    //   }, 90000);
    // }
  }

  getData() {
    this.dashboardService.getActivityCallsByRole().subscribe((result) => {
      this.activityCalls = [];
      // result?.forEach((activity) => {
      //   activity.isCollapsed = true;
      //   this.activityCalls.push(activity);
      // });
    });
  }

  collapsedActivity(i: number) {
    this.activityCalls[i].isCollapsed = !this.activityCalls[i].isCollapsed;
  }

  identify(index, item) {
    return item ? item.id : undefined;
  }
}
