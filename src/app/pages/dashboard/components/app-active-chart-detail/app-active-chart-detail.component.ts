import { AfterViewInit, Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';


import { global } from '@angular/compiler/src/util';
import { forkJoin } from 'rxjs';
import _ from 'lodash';
import * as echarts from 'echarts';
import { EChartsOption } from 'echarts';
import { formatDate, formatNumber } from '@angular/common';
import { BaseComponent } from '../../../../core/components/base.component';
import { Pageable } from '../../../../core/interfaces/pageable.interface';
import {
  BRANCH_HO,
  ConfirmType,
  CustomerType,
  DashboardType,
  EChartType,
  FunctionCode,
  functionUri,
  Scopes,
  SessionKey
} from '../../../../core/utils/common-constants';
import { DashboardService } from '../../services/dashboard.service';
import { CategoryService } from '../../../system/services/category.service';
import * as moment from 'moment';
import { SaleManagerApi } from '../../../sale-manager/api';
import { ConfirmDialogComponent } from '../../../../shared/components';
import { CustomerApi } from 'src/app/pages/customer-360/apis';

@Component({
  selector: 'app-active-chart-detail',
  templateUrl: './app-active-chart-detail.component.html',
  styleUrls: ['./app-active-chart-detail.component.scss'],
})
export class AppActiveChartDetailComponent extends BaseComponent implements OnInit {
  // @ViewChild(ECharts) echarts: ECharts;
  form = this.fb.group({
    branchCode: '',
  });
  data: any;
  listDataCard = [];
  listBranchCode = [];
  listBranch: Array<any>;
  pageable: Pageable;
  option: EChartsOption;
  limit = global.userConfig.pageSize;
  statusNameCard = 'Thông tin chi tiết';
  isHO = false;
  paramsDetail = {
    rsId: '',
    scope: Scopes.VIEW,
    branchCodes: [],
    page: 0,
    size: global?.userConfig?.pageSize,
    appStatus: 'APP_F',
  };
  intervalDownloadFileS3 = null;
  paramsExport = {
    rsId: '',
    scope: 'VIEW',
    isRm: true,
    transactionDate: '',
    rmCode: '',
  };
  paramCharts = {
    rsId: '',
    scope: Scopes.VIEW,
    branchCodes: [],
  };
  isClick = false;
  isBack = false;
  prevParams: any;
  preClickData: any;
  private myChart: any = null;
  timeout;
  HIGHTLIGHT = 'highlight';
  DOWNPLAY = 'downplay';
  isRm = true;
  dataDate: Date;
  dateSnapshot: string;
  dataTotal: number;
  PARAMS_KEY = 'FUNCTION_APP_ACTIVE_PARAMS'

  constructor(
    injector: Injector,
    private dashboardService: DashboardService,
    private categoryService: CategoryService,
    private customerApi: CustomerApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DASHBOARD_CUSTOMER}`);
    this.prevParams = _.get(this.router.getCurrentNavigation(), 'extras.state');
    this.paramsDetail.rsId = this.objFunction?.rsId;
    this.paramCharts.rsId = this.objFunction?.rsId;
    this.paramsExport.rsId = this.objFunction?.rsId;
    this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
    if (this.prevParams) {
      localStorage.setItem(this.PARAMS_KEY, JSON.stringify(this.prevParams))
      this.handlerPrevData()
    } else if (localStorage.getItem(this.PARAMS_KEY)){
      this.prevParams = JSON.parse(localStorage.getItem(this.PARAMS_KEY))
      this.handlerPrevData()
    }

  }

  ngOnInit(): void {
    this.isHO = this.currUser?.branch === BRANCH_HO;
    this.pageable = {
      totalElements: 0,
      totalPages: 0,
      currentPage: this.paramsDetail.page,
      size: this.limit,
    };
    // console.log('data: ', this.dataDate);
    this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
    this.categoryService.getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW).subscribe(
      (result) => {
        const branches = result || [];
        if (!_.isEmpty(branches)) {
          this.listBranch = [];
          this.listBranch.push({ code: '', name: 'Tất cả' });
          branches.forEach((b) => {
            this.listBranch.push({
              code: b.code,
              name: `${b.code} - ${b.name}`,
            });
          });
        }
        this.isRm = _.isEmpty(this.listBranch);

        if (!this.listBranchCode || _.isEmpty(this.listBranchCode)) {
          this.listBranchCode = this.listBranch.map((b) => b.code);
          this.search();
        }
      },
      (err) => {
        this.listBranch = [];
      }
    );

    if (!_.isEmpty(this.listBranchCode) && this.listBranchCode[0] === '' && this.isHO) {
      this.listBranchCode = ['ALL']
    }

    if (!_.isEmpty(this.listBranchCode)) {
      this.search();
    }
  }

  handlerPrevData() {
    this.isBack = true;
    this.listBranchCode = this.prevParams?.branch ? this.prevParams?.branch : [];
    this.isRm = _.isEmpty(this.listBranchCode);
    if (this.prevParams?.data) {
      this.preClickData = this.prevParams?.data;
      this.paramsDetail.appStatus = this.preClickData.code;
      this.statusNameCard = `Thông tin chi tiết - ${this.preClickData?.name}`;

      if (this.listBranchCode.length === 1 && this.listBranchCode[0] === 'ALL') {
        // tất cả
        this.form.controls.branchCode.setValue('');
      } else if (this.listBranchCode.length === 1) {
        // choose one
        this.form.controls.branchCode.setValue(this.listBranchCode[0]);
      }
    }
  }

  search() {
    this.isLoading = true;
    this.paramsDetail.rsId = this.objFunction?.rsId;
    this.paramCharts.rsId = this.objFunction?.rsId;
    this.paramsDetail.branchCodes = this.listBranchCode.filter((x) => x);
    this.paramCharts.branchCodes = this.listBranchCode.filter((x) => x);

    forkJoin([
      this.dashboardService.getAppActiveDataChart(this.paramCharts, this.isHO),
      this.dashboardService.getDataChartsDashboardAppDetail(this.paramsDetail, this.isHO),
    ]).subscribe(
      ([dataChart, dataChartDetail]) => {
        this.isLoading = false;
        this.data = dataChart?.data;
        this.listDataCard = dataChartDetail ? dataChartDetail?.content : [];
        this.pageable = {
          totalElements: dataChartDetail?.totalElements || 0,
          totalPages: dataChartDetail?.totalPages || 0,
          currentPage: this.paramsDetail.page,
          size: this.paramsDetail.size,
        };

        // sum
        const sum = _.reduce(
          _.map(this.data, (x) => x.value),
          (a, c) => {
            return a + c;
          }
        );
        this.dataTotal = sum;

        this.handleMapData();
        if (this.preClickData && this.dataTotal > 0) {
          this.checkView(this.preClickData);
        }
      },
      (err) => {
        this.messageService.error('Thực hiện không thành công');
        this.isLoading = false;
      }
    );
  }

  checkView(preClickData) {
    const chart = document.getElementById('chartId');
    if (chart) {
      this.resetAndHightLightChart(preClickData.indexItem, this.HIGHTLIGHT);
      clearTimeout(this.timeout);
    } else {
      this.timeout = setTimeout(() => this.checkView(preClickData), 1000);
    }
  }

  setPage(pageInfor) {
    if (this.isLoading) {
      return;
    }
    this.paramsDetail.page = pageInfor.offset;
    this.search();
  }

  changeBranch(event) {
    console.log(event);
    if (event.value === '') {
      if (this.isHO) {
        this.listBranchCode = ['ALL'];
      } else {
        this.listBranchCode = this.listBranch.filter((b) => b.code !== '').map((branch) => branch.code);
      }
    } else {
      this.listBranchCode = [event.value];
    }

    this.pageable.currentPage = 0;
    this.paramsDetail.page = 0;
    this.search();
  }

  onClickChart(data, chart?) {
    if (this.dataTotal && this.dataTotal <= 0) {
      return;
    }

    this.isLoading = true;
    this.paramsDetail.appStatus = data?.code;
    // this.paramsDetail.appStatus = 'APP_F';
    if (data?.code === this.preClickData?.code) {
      // uncheck selected
      this.statusNameCard = 'Thông tin chi tiết';
      this.preClickData = null;
      this.paramsDetail.appStatus = 'APP_F';
      this.resetHighLight();
    } else {
      this.statusNameCard = `Thông tin chi tiết - ${data?.name}`;
      this.preClickData = data;
      if (data.value > 0) {
        this.resetAndHightLightChart(data.indexItem, this.HIGHTLIGHT);
      }
    }

    this.paramsDetail.branchCodes = this.listBranchCode;
    this.paramsDetail.page = 0;
    this.paramsDetail.size = 10;
    this.dashboardService.getDataChartsDashboardAppDetail(this.paramsDetail, this.isHO).subscribe(
      (result) => {
        // console.log(result)
        this.listDataCard = result ? result?.content : [];
        this.pageable = {
          totalElements: result?.totalElements || 0,
          totalPages: result?.totalPages || 0,
          currentPage: this.paramsDetail.page,
          size: this.paramsDetail.size,
        };
        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }

  handleMapData() {
    if (_.size(this.data) > 0) {
      this.option = {
        tooltip: {
          trigger: 'item',
          formatter: (params) => {
            return `${formatNumber(params.value || 0, 'en', '1.0-0')} (${params.percent})%`;
          },
        },
        legend: {
          show: false,
          bottom: 0,
        },
        series: [
          {
            // bottom: '40%',
            type: EChartType.Pie,
            radius: ['30%', '65%'],
            labelLine: {
              length: 5,
            },
            label: {
              formatter: '{d}%',
            },
            data: [],
          },
        ],
      };
      _.forEach(this.data, (item, index) => {
        if (item.value > 0) {
          this.option.series[0].data.push({
            value: item.value,
            name: item.label,
            itemStyle: { color: item.color },
            code: item.code,
            indexItem: index,
          });
        }
      });
    } else {
      this.data = undefined;
    }
  }

  back() {
    const data = {
      block: CustomerType.INDIV,
      type: DashboardType.INDIV_CUSTOMER,
      branch: this.prevParams?.branch,
    };
    this.router.navigate(['/dashboard'], {
      state: data,
    });
  }

  handleChangePageSize(event) {
    this.pageable.currentPage = 0;
    this.pageable.size = event;
    this.paramsDetail.size = event;
    this.paramsDetail.page = 0;
    this.limit = event;
    this.search();
  }

  resetHighLight() {
    _.forEach(this.data, (value, i) => {
      this.hightLightSelected(i, this.DOWNPLAY);
    });
  }

  resetAndHightLightChart(index: number, type: string) {
    _.forEach(this.data, (value, i) => {
      this.hightLightSelected(i, this.DOWNPLAY);
    });
    this.hightLightSelected(index, type);
  }

  hightLightSelected(index, typeAction) {
    const myChart = echarts.init(document.getElementById('chartId') as any);
    if (!myChart) {
      return;
    }
    if (!this.myChart) {
      this.myChart = myChart;
    }

    myChart.on('rendered', () => {
      console.log('rendered');
    });

    myChart.dispatchAction({
      type: typeAction,
      seriesIndex: 0,
      dataIndex: index,
    });
  }

  onActive(event) {
    if (event.type === 'click' && (event.column.prop === 'customerId' || event.column.prop === 'customerName')) {
      this.showDetailCustomer360(event.row.customerId);
    }
  }

  showDetailCustomer360(customerCode) {
    const param = {
      customerCode,
    };

    this.isLoading = true
    // checkCustomerPermission
    this.customerApi.checkCustomerPermission(param).subscribe(
      (res) => {
        if (res) {
          localStorage.setItem('DETAIL_CUS_ID', JSON.stringify(customerCode));
          const url = this.router.serializeUrl(
            this.router.createUrlTree([functionUri.customer_360_manager], {
              skipLocationChange: true,
            })
          );
          window.open(url, '_blank');
        } else {
          this.openModalInfo();
        }
        this.isLoading = false;
      },
      (e) => {
        console.log(e);
        this.messageService.error('Thực hiện không thành công');
        this.isLoading = false;
      }
    );
  }

  openModalInfo() {
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.componentInstance.type = ConfirmType.Warning;
    confirm.componentInstance.message = 'Bạn không có quyền xem thông tin KH do không được phân giao quản lý KH này';
    confirm.result
      .then((res) => {
        if (res) {
          console.log(res);
        }
      })
      .catch(() => {});
  }
}
