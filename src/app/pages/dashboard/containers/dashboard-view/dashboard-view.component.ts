import { Component, OnInit, EventEmitter, Injector, ViewChild, ViewEncapsulation, HostListener } from '@angular/core';
import {
  DisplayGrid,
  GridsterComponent,
  GridsterConfig,
  GridsterItemComponentInterface,
  GridType,
} from 'angular-gridster2';
import * as _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import {
  CustomerPrivatePriorityChartComponent,
  CustomerProductUsedChartComponent,
  CustomerSegmentChartComponent,
} from 'src/app/pages/customer-360/components';
import { CustomerStatusChartComponent } from 'src/app/pages/customer-360/components/customer-status-chart/customer-status-chart.component';
import { CustomerBscoreChartComponent } from 'src/app/pages/customer-360/components/customer-bscore-chart/customer-bscore-chart.component';
import { CustomerToiChartComponent } from 'src/app/pages/customer-360/components/customer-toi-chart/customer-toi-chart.component';
import {
  BRANCH_HO,
  CommonCategory,
  CustomerType,
  DashboardOtherIndiv,
  DashboardType,
  DATE_TYPES,
  Division,
  FunctionCode, functionUri,
  Scopes,
  SessionKey, TYPE_MESSAGE_EMBEDED,
} from 'src/app/core/utils/common-constants';
import { RaiseCapitalTopChartComponent } from '../../components/raise-capital-top-chart/raise-capital-top-chart.component';
import { CreditTopChartComponent } from '../../components/credit-top-chart/credit-top-chart.component';
import { DashboardBusinessChartSMEComponent } from '../../components/dashboard-business-chart-sme/dashboard-business-chart-sme.component';
import { CustomerCommonChartComponent } from '../../components/customer-common-chart/customer-common-chart.component';
import { DashboardService } from '../../services/dashboard.service';
import { forkJoin, Observable, of } from 'rxjs';
import {
  catchError,
  concatMap,
  finalize,
  map,
  tap,
} from 'rxjs/operators';
import { ChartCustomerModel } from '../../models/chart-customer.model';
import { DataChartCustomerModel } from '../../models/data-chart-customer.model';
import { CustomerApi } from 'src/app/pages/customer-360/apis';
import * as moment from 'moment';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { KpiTotalPointChartComponent } from 'src/app/pages/kpis/components/kpi-total-point-chart/kpi-total-point-chart.component';
import { KpiTargetChartComponent } from 'src/app/pages/kpis/components/kpi-target-chart/kpi-target-chart.component';
import { KpiDashboardService } from 'src/app/pages/kpis/apis/kpi-dashboard.service';
import { RmApi } from 'src/app/pages/rm/apis/rm.api';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { environment } from '../../../../../environments/environment';
import { Utils } from 'src/app/core/utils/utils';
import { CustomerOverviewChartComponent } from '../../components/dashboard-customer-overview-indiv-chart/customer-overview-chart.component';
import { CardChartComponent } from '../../components/dashboard-card-indiv-chart/card-chart.component';
import { CardSpendingComponent } from '../../components/card-spending/card-spending.component';
import { CustomerStatisticsComponent } from '../../components/customer-statistics/customer-statistics.component';
import { CustomerFunnelChartComponent } from '../../components/customer-funnel-chart/customer-funnel-chart.component';
import { BCoreChartComponent } from '../../components/dashboard-b-core-indiv-chart/b-core-chart.component';
import { SpdvChartComponent } from '../../components/dashboard-spdv-chart/spdv-chart.component';
import { CustomerActiveAppChartComponent } from '../../components';
import { ChartCampaignResultOverviewComponent } from '../../components/chart-campaign-result-overview/chart-campaign-result-overview.component';
import { CampaignsService } from 'src/app/pages/campaigns/services/campaigns.service';
import { ChartCampaignStageOverviewComponent } from '../../components/chart-campaign-stage-overview/chart-campaign-stage-overview.component';

import { GuaranteeComponent } from '../../components/dashboard-sme-business/guarantee/guarantee/guarantee.component';
import { HDVComponent } from '../../components/dashboard-sme-business/HDV/HDV/HDV.component';
import { DebtComponent } from '../../components/dashboard-sme-business/debt/debt/debt.component';
import { TableComponent } from '../../components/dashboard-sme-business/table/table/table.component';

@Component({
  selector: 'app-dashboard-view',
  templateUrl: './dashboard-view.component.html',
  styleUrls: ['./dashboard-view.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DashboardViewComponent extends BaseComponent implements OnInit {
  isHO = false;
  rowMenu: any;
  options: GridsterConfig;
  dashboard: any;
  resizeEvent: EventEmitter<GridsterItemComponentInterface> = new EventEmitter<GridsterItemComponentInterface>();
  viewChange: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('gridDashboard') gridDashboard: GridsterComponent;
  listDivision = [];
  listType = [];
  // listBranch is branch lv2
  listBranch = [];
  listBranchTerm = [];
  paramBranch = [];
  listBranchDashbroad = [];
  branchDashboards = [];
  listRM = [];
  listCampaign = [];
  listRegion = [];
  listRegion2 = [];
  DATE_TYPES = DATE_TYPES;
  form = this.fb.group({
    divisionCode: '',
    type: '',
    branchCodeLv1: '',
    branchCode: '',
    rmCode: '',
    campaigns: '',
    regionCode: '',
    isRegion: false,
    date: new Date(moment().year(), moment().month(), moment().date() - 1),
    dateType: [{ value: DATE_TYPES.MONTH, disabled: false }],
    fromDate: new Date(new Date(moment().year(), moment().month() - 4)),
    toDate: new Date(moment().year(), moment().month() - 1),
    periodsSME: [{ value: 'DAY', disabled: false}],
  });
  currentType: string;
  oldType: string;
  isPermission: boolean;
  DEFAULT_MAXCOLS = 3;
  dateFormatList = {
    day: 'dd/mm/yy',
    month: 'mm/yy',
  };
  dateFormat = this.dateFormatList.month;
  dateView = 'month';
  kpiMaxDate;
  isRmLoading = false;
  listRmTerm = [];
  hrsCode = '';
  datePickerLabel = {
    [DATE_TYPES.DAY]: {
      from: 'Từ ngày',
      to: 'Đến ngày',
    },
    [DATE_TYPES.MONTH]: {
      from: 'Từ tháng',
      to: 'Đến tháng',
    },
  };
  kpiRmList = {};
  kpiPeriods = [
    {code: 'month', name: 'Tháng'},
    {code: 'day', name: 'Ngày'}
  ]
  SMEPeriods = [
    { code: 'DAY', name: 'Ngày' },
    { code: 'WEEK', name: 'Tuần' },
    { code: 'MONTH', name: 'Tháng' },
    { code: 'YEAR', name: 'Năm' },

  ]


  branchesByDomain = {}; // group by region
  regions = {} // group branch lv1, lv2

  branchLv1List = [];
  allBranchLv1 = [];
  allBranchLv2 = [];
  isFirst = true;
  isChangeSide = false;
  showEmbedded = false;
  source: SafeUrl;
  isRm = false;
  isCBQL = false;
  changeTimePoint = false;
  facilities: Array<any>;
  facilityCode: Array<string>;
  blockTranfer = '';
  typeTranfer = '';
  isBack = false;
  prevParams: any;
  kpiMonthPicker = true;
  branchFilter: string;
  isbranchDimention = true;
  state = [];
  lstBranchDashboardBusiness = [];
  constructor(
    injector: Injector,
    private service: DashboardService,
    private customerApi: CustomerApi,
    private categoryService: CategoryService,
    private kpiDashboardService: KpiDashboardService,
    private campaignService: CampaignsService,
    private rmApi: RmApi,
    private sanitizer: DomSanitizer
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DASHBOARD_CUSTOMER}`);
    this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
    this.prevParams = _.get(this.router.getCurrentNavigation(), 'extras.state');
    // if(block){
    //   this.form.controls.divisionCode.setValue(block);
    // }
    // if(type){
    //   this.form.controls.type.setValue(type);
    // }
  }

  @HostListener('window:message', ['$event'])
  onClickKH360(event?) {

    const origin = event.origin;
    if (origin !== environment.url_endpoint_embedded) {
      return;
    }
    if (event.data.type === TYPE_MESSAGE_EMBEDED.KH_360) {
      const customerCode = event.data.data;
      const customerType = 'indiv';
      localStorage.setItem('DETAIL_CUS_ID', JSON.stringify(customerCode));
      const url = this.router.serializeUrl(
        this.router.createUrlTree([functionUri.customer_360_manager], {
          skipLocationChange: true,
        })
      );
      window.open(url, '_blank');
      // this.router.navigateByUrl(`${functionUri.customer_360_manager}/detail/${customerType}/${event.data.data}`, {skipLocationChange: true});
    }
  }

  showSideBar() {
    const timer = setTimeout(() => {
      if (this.options.api && this.options.api.resize) {
        this.options.api.resize();
        this.viewChange.emit(true);
        clearTimeout(timer);
      }
    }, 100);
  }

  ngOnInit(): void {
    this.source = this.sanitizer.bypassSecurityTrustResourceUrl(environment.url_endpoint_embedded
      + `/dashboard?username=${this.currUser?.username}&token=${this.sessionService.getSessionData(SessionKey.TOKEN)}`);
    this.isLoading = true;
    this.isHO = this.currUser?.branch === BRANCH_HO;

    this.options = {
      gridType: GridType.VerticalFixed,
      displayGrid: DisplayGrid.None,
      // compactType: CompactType.CompactUp,
      margin: 10,
      outerMargin: true,
      outerMarginTop: 0,
      outerMarginRight: 0,
      outerMarginBottom: 0,
      outerMarginLeft: 0,
      mobileBreakpoint: 640,
      minCols: 1,
      minRows: 1,
      maxCols: this.DEFAULT_MAXCOLS,
      // maxRows: 9,
      defaultItemCols: 1,
      defaultItemRows: 1,
      // fixedColWidth: 105,
      fixedRowHeight: 120,
      draggable: {
        delayStart: 0,
        enabled: false,
      },
      resizable: {
        delayStart: 0,
        enabled: false,
      },
      itemInitCallback: (item, itemComponent) => {
        this.resizeEvent.emit(itemComponent);
      },
    };

    this.setValueChanges();

    forkJoin([
      this.getCommonData(),
      this.service.getConfigDefaultDashboard().pipe(catchError(() => of(undefined))),
      this.categoryService.getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW).pipe(catchError(() => of(undefined))),

      // this.categoryService.getRegion().pipe(catchError((e) => of(undefined))),
      this.service.getBranchByDomain({
        rsId: this.objFunction?.rsId,
        scope: Scopes.VIEW
      }),
      this.commonService.getCommonCategory(CommonCategory.DASHBOARD_BUSINESS_COLOR).pipe(catchError(() => of(undefined))),
      this.categoryService.getRegion().pipe(catchError((e) => of(undefined)))
    ]).subscribe(([commonData, defaultCode, listBranch, branchesByDomain,lstCommon, regionList]) => {
      this.isRm = _.isEmpty(listBranch);
      this.isCBQL = (this.listBranch?.length > 0 && this.currUser?.branch !== BRANCH_HO)
      this.currentType = defaultCode;
      this.sessionService.setSessionData(`FUNCTION_${FunctionCode.DASHBOARD}`, commonData);
      this.state = _.get(lstCommon,'content',[]);
      this.lstBranchDashboardBusiness = listBranch;
      // region, branch lv1 , branch lv2
      const regionObj = _.groupBy(branchesByDomain, 'region');
      const listRegion = Object.keys(regionObj).map(key => ({ locationCode: key, locationName: key }));
      this.listRegion2 = regionList;

      Object.keys(regionObj).forEach(key => {
        const branchLv1 = _.groupBy(regionObj[key], 'parentCode');
        const branchLv1List = Object.keys(branchLv1).map(branchKey => {
          return {
            code: branchLv1[branchKey][0].parentCode,
            name: branchLv1[branchKey][0].parentName,
            displayName: `${branchLv1[branchKey][0].parentCode} - ${branchLv1[branchKey][0].parentName}`,
            khuVucM: key,
            branch_lv2: branchLv1[branchKey].map(branch => {
              return { code: branch.code, name: branch.name, displayName: `${branch.code} - ${branch.name}`, khuVucM: key, parentCode: branchLv1[branchKey][0].parentCode }
            })
          }
        })
        if (!this.regions[key]) {
          this.regions[key] = {
            branch_lv1: branchLv1List
          }
        }
      });

      // all branch lv
      Object.keys(this.regions).forEach(key => {
        this.regions[key].branch_lv1.forEach(item => {
          this.allBranchLv2.push(...item.branch_lv2);
        });
      });

      Object.keys(this.regions).forEach(key => {
        this.regions[key].branch_lv1.forEach(item => {
          const branchLv1 = this.allBranchLv1.find(branch => branch.code === item.code);
          if (!branchLv1) this.allBranchLv1.push(item);
        });
      });

      _.forEach(listBranch, (item) => {
        item.displayName = `${item.code} - ${item.name}`;
      });
      this.listBranchTerm = this.allBranchLv2 || [];
      this.sessionService.setSessionData(SessionKey.LIST_BRANCH, this.listBranchTerm);

      this.listBranch = _.cloneDeep(this.listBranchTerm);
      this.isPermission = _.size(listBranch) > 0;
      if (!_.find(this.listBranch, (item) => item.code === this.currUser?.branch)) {
        this.listBranch.push({
          code: this.currUser?.branch,
          displayName: `${this.currUser?.branch} - ${this.currUser?.branchName}`,
        });
      }

      if (this.isHO) {
        this.listRegion = listRegion;
      } else {
        listRegion?.forEach((item) => {
          if (
            this.listBranchTerm?.findIndex((i) => i.khuVucM === item?.locationCode) !== -1 &&
            this.listRegion.findIndex((r) => r.locationCode === item?.locationCode) === -1
          ) {
            this.listRegion.push(item);
          }
        });
      }

      // sort region list
      this.listRegion = _.orderBy(this.listRegion, [region => region.locationName], ['asc']);

      // switch dashboard
      this.setIsRegionByRole(false);
      this.setBranchOfUser();

      // Chỗ này tạm thời chỉ hiển thị khô INDIV
      for (let i = 0; i < commonData.listDivision.length; i++) {
        if (commonData.listDivision[i].code === 'INDIV') {
          this.listDivision.push({
              code: commonData.listDivision[i].code,
              name: `${commonData.listDivision[i].code} - ${commonData.listDivision[i].name}`,
            }
          );
        }
      }

      // Comment tạm đoạn này
      // this.listDivision = _.map(commonData.listDivision, (item) => {
      //   return {
      //     code: item.code,
      //     name: `${item.code} - ${item.name}`,
      //   };
      // });

      this.form.patchValue(
        { divisionCode: _.head(this.listDivision)?.code, type: defaultCode, branchCode: _.head(this.listDivision)?.code === 'INDIV' ? '' : _.head(this.listBranch)?.code },
        { emitEvent: false }
      );
      if (this.prevParams) {
        this.form.controls.divisionCode.setValue(this.prevParams?.block);
        this.form.controls.type.setValue(this.prevParams?.type);
        if (!this.isRm) {
          console.log('branch: ', this.prevParams?.branch);
          this.form.controls.branchCode.setValue(this.prevParams?.branch);
          this.facilityCode = this.prevParams?.branch ? this.prevParams?.branch : [];
        }
        this.showEmbedded = false;
      }
      this.mapListType(this.form.controls.divisionCode.value);
      const data = this.form.getRawValue();

      if (
        (data.type === DashboardType.OTHER_INDIV_BUSINESS || data.type === DashboardType.OTHER_INDIV_REVENUE) &&
        data.divisionCode !== Division.INDIV
      ) {
        this.getListRm();
      } else if (data.type === DashboardType.OTHER_INDIV_KPI && data.divisionCode !== Division.INDIV) {
        this.getListRmForKpi();
      } else {
        this.getDataV2(data.type, data.divisionCode);
      }

      if (data.type === DashboardType.INDIV_CAMPAIGN) {

        if (this.prevParams?.branchCode
          && this.prevParams?.campaignId
          && this.prevParams?.regionCode
          && this.prevParams?.campaignList) {
          this.initializeRegionListIndv()
          this.form.get('regionCode').setValue(this.prevParams?.regionCode, { emitEvent: false });

          // this.listBranch = this.allBranchLv2.filter(branch => branch.khuVucM === this.prevParams?.regionCode);

          if (this.form.get('isRegion').value == true) {
            this.listBranch = this.lstBranchDashboardBusiness.filter(branch => branch.khuVucM === this.prevParams?.regionCode);
          } else {
            this.listBranch = this.lstBranchDashboardBusiness;
          }

          if(this.listBranch.length > 1) {
            this.listBranch.unshift({code: '', displayName: 'Tất cả'});
          }

          this.form.get('branchCode').setValue(this.prevParams?.branchCode === 'ALL'? '' : this.prevParams?.branchCode,
          { emitEvent: false });

          this.listCampaign = this.prevParams.campaignList;
          this.form.get('campaigns').setValue(this.prevParams?.campaignId, { emitEvent: false });
          const formData = this.form.getRawValue();
          this.getDataV2(formData.type, formData.divisionCode);

        } else {
          this.setBranchOfUser();
          this.listRegion = this.listRegion.filter(item => item.locationCode !== '');
          this.getCampaignList();
    }
      }
    });

    this.categoryService.getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW).subscribe(temp => {
      if (temp) {
        this.facilities = temp;
        this.listBranchDashbroad = temp;
        this.listBranchDashbroad = this.listBranchDashbroad?.map((item) => {
          return { code: item.code, name: item.code + ' - ' + item.name };
        });
        this.listBranchDashbroad?.unshift({ code: '', name: this.fields.all });
      }
    });
  }

  onChangeForm() {
    // const block = _.get(this.route.snapshot.queryParams, 'block');
    // const type = _.get(this.route.snapshot.queryParams, 'type');
    // console.log('block: ', block);
    // console.log('type: ', type);
    // if(block){
    //   this.form.controls.divisionCode.setValue(block);
    // }
    // if(type){
    //   this.form.controls.type.setValue(type);
    // }
    const data = this.form.getRawValue();
    if (!_.isEmpty(data.divisionCode) && !_.isEmpty(data.type)) {
      this.isLoading = true;
      this.getDataV2(data.type, data.divisionCode);
    }
  }

  mapListType(divisionCode: string) {
    const state = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DASHBOARD}`);
    if (divisionCode === CustomerType.INDIV) {
      // Chỗ này check chỉ hiển thị Dashboard chiến dịch
      for (let i = 0; i < state[CommonCategory.DASHBOARD_LIST].length; i++){
        if (state[CommonCategory.DASHBOARD_LIST][i].code === 'DASHBOARD_CAMPAIGN'){
          this.listType = [state[CommonCategory.DASHBOARD_LIST][i]];
        }
      }
      // Dòng này comment tạm
      // this.listType = state[CommonCategory.DASHBOARD_LIST][];
      this.form.controls.branchCode.setValue('', { emitEvent: false });
    } else {
      this.showEmbedded = false;
      this.listType = _.map(state[CommonCategory.DASHBOARD_MANAGERMENT], (item) => {
        return { code: item.code, name: item.description };
      });
    }
    // if (this.isHO) {
    //   _.remove(this.listType, (item) => item.code === DashboardType.INDIV_CUSTOMER);
    // }
    if (!_.find(this.listType, (item) => item.code === this.form.controls.type.value)) {
      this.dashboard = [];
      this.form.controls.type.setValue(_.first(this.listType)?.code);
    } else {
      const data = this.form.getRawValue();
      if (
        (data.type === DashboardType.OTHER_INDIV_BUSINESS || data.type === DashboardType.OTHER_INDIV_REVENUE || data.type === DashboardType.OTHER_INDIV_CUSTOMER) &&
        data.divisionCode !== Division.INDIV
      ) {
        this.getListRm();
      } else if (data.type === DashboardType.OTHER_INDIV_KPI && data.divisionCode !== Division.INDIV) {
        this.setMaxCols(data.type);
        this.getListRmForKpi();
      } else {
        this.setMaxCols(data.type);
        this.onChangeForm();
      }
    }
  }

  async mapDataComponent(dataChart, divisionCode: string, type: string) {
    // params for business, revenue
    const state = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DASHBOARD}`);
    const extraData: any = {
      rsId: this.objFunction?.rsId,
      scope: Scopes.VIEW,
    }

    const dataForm = this.form.getRawValue();

    if (
      (dataForm.regionCode && !dataForm.branchCode) ||
      (dataForm.branchCodeLv1 && !dataForm.branchCode)
    ) {
      extraData.branchCode = this.listBranch.filter(branch => branch.code).map(branch => branch.code).join(',');
    }

    // order
    if (divisionCode === CustomerType.INDIV && type === DashboardType.INDIV_CUSTOMER) {
      this.dashboard = [
        {
          cols: 1,
          rows: 3,
          y: 0,
          x: 0,
          component: CustomerSegmentChartComponent,
          data: dataChart?.dataSegment,
        },
        {
          cols: 1,
          rows: 3,
          y: 0,
          x: 0,
          component: CustomerPrivatePriorityChartComponent,
          data: dataChart?.dataPrivate,
        },
        {
          cols: 1,
          rows: 3,
          y: 0,
          x: 0,
          component: CustomerProductUsedChartComponent,
          data: dataChart?.dataProductUsed,
        },
        {
          cols: 1,
          rows: 3,
          y: 0,
          x: 0,
          component: CustomerStatusChartComponent,
          data: dataChart?.dataStatus,
        },
        {
          cols: 1,
          rows: 3,
          y: 0,
          x: 0,
          component: CustomerBscoreChartComponent,
          data: dataChart?.dataBScore,
        },
        {
          cols: 1,
          rows: 3,
          y: 0,
          x: 0,
          component: CustomerToiChartComponent,
          data: dataChart?.dataToi,
        },
      ];
    } else if (divisionCode === CustomerType.INDIV && type === DashboardType.INDIV_BUSINESS) {
      this.dashboard = [
        {
          cols: 1,
          rows: 4,
          y: 0,
          x: 0,
          component: RaiseCapitalTopChartComponent,
          data: dataChart?.dataRaiseCapital,
        },
        {
          cols: 1,
          rows: 4,
          y: 0,
          x: 0,
          component: CreditTopChartComponent,
          data: dataChart?.dataCredit,
        },
      ];
    } else if (divisionCode !== CustomerType.INDIV && type === DashboardType.OTHER_INDIV_CUSTOMER) {
      let listData: any = _.cloneDeep(dataChart[0]);
      // tính tổng
      dataChart.forEach((element, index) => {
        if (!element) {
          return;
        }
        if (index == 0) {
          return;
        }
        Object.keys(element).forEach((key) => {
          if (!+listData[key] && typeof(+listData[key]) != 'number') {
            listData[key] = 0;
            return;
          }
          listData[key] = +listData[key];
          listData[key] += +element[key];
        })
      });
      const grid: any = {
        cols: 1,
        rows: 3,
        y: 0,
        x: 0,
        component: CustomerCommonChartComponent,
      };
      const grid1: any = {
        cols: 3,
        rows: 2,
        y: 0,
        x: 0,
        data: _.cloneDeep({ ...dataForm, ...{ listRegion: this.listRegion, listBranch: this.listBranch.filter(branch => branch.code).map(branch => branch.code).join(',') } }),
        component: CustomerStatisticsComponent,
      };
      const grid2: any = {
        cols: 3,
        rows: 5,
        y: 3,
        x: 0,
        data: _.cloneDeep({ ...dataForm, ...{ listRegion: this.listRegion, listBranch: this.listBranch.filter(branch => branch.code).map(branch => branch.code).join(',') } }),
        component: CustomerFunnelChartComponent,
      };
      this.dashboard = [];
      // _.forEach(listData, (item) => {
      //   this.dashboard.push({ ...grid, data: item });
      // });
      forkJoin([this.categoryService.getCommonCategory(CommonCategory.DASHBOARD_CUSTOMER_STATUS).pipe(catchError(() => of(undefined))),
      this.categoryService.getCommonCategory(CommonCategory.SEGMENT_CUSTOMER_CONFIG).pipe(catchError(() => of(undefined))),
      this.categoryService.getCommonCategory(CommonCategory.CUSTOMER_OBJECT_CONFIG).pipe(catchError(() => of(undefined)))]).subscribe(res => {
        const status = res[0].content;
        this.dashboard.push({
          ...grid, data: {
            title: 'Trạng thái',
            id: 1,
            data: [
              {
                value: listData?.nbrCstStActive,
                name: status.find(item => item.code == 'ACTIVE')?.name,
                itemStyle: {
                  color: JSON.parse(status.find(item => item.code == 'ACTIVE')?.description)?.color,
                }
              },
              {
                value: listData?.nbrCstStInActive,
                name: status.find(item => item.code == 'INACTIVE')?.name,
                itemStyle: {
                  color: JSON.parse(status.find(item => item.code == 'INACTIVE')?.description)?.color,
                }
              },
              {
                value: listData?.nbrCstStDormant,
                name: status.find(item => item.code == 'DORMANT')?.name,
                itemStyle: {
                  color: JSON.parse(status.find(item => item.code == 'DORMANT')?.description)?.color,
                }
              },
              {
                value: listData?.nbrCstStOther,
                name: status.find(item => item.code == 'NOT_YET_RATE')?.name,
                itemStyle: {
                  color: JSON.parse(status.find(item => item.code == 'DORMANT')?.description)?.color,
                }
              },
            ]
          }
        });
        const tagerts = res[2].content;
        this.dashboard.push({
          ...grid, data: {
            title: 'Đối tượng Khách hàng',
            id: 2,
            // Premier
            data: [
              {
                value: listData?.nbrCstTargertPremier,
                name: tagerts.find(item => item.code == 'PRIVATE')?.name,
                itemStyle: {
                  color: JSON.parse(tagerts.find(item => item.code == 'PRIVATE')?.description)?.color,
                }
              },
              {
                value: listData?.nbrCstTargertVip,
                name: tagerts.find(item => item.code == 'VIP')?.name,
                itemStyle: {
                  color: JSON.parse(tagerts.find(item => item.code == 'VIP')?.description)?.color,
                }
              },
              {
                value: listData?.nbrCstTargertNormal,
                name: tagerts.find(item => item.code == 'THONG_THUONG')?.name,
                itemStyle: {
                  color: JSON.parse(tagerts.find(item => item.code == 'THONG_THUONG')?.description)?.color,
                }
              },
              {
                value: listData?.nbrCstTargertOther,
                name: tagerts.find(item => item.code == 'CHUAXACDINH')?.name,
                itemStyle: {
                  color: JSON.parse(tagerts.find(item => item.code == 'CHUAXACDINH')?.description)?.color,
                }
              },
            ]
          }
        });
        const SEGMENTS = res[1].content
        this.dashboard.push({
          ...grid, data: {
            title: 'Phân khúc',
            id: 3,
            data: dataForm.divisionCode == CustomerType.CIB ? [
              {
                value: listData?.nbrCstSegCIB,
                name: SEGMENTS.find(item => item.code == CustomerType.CIB)?.name,
                itemStyle: {
                  color: JSON.parse(SEGMENTS.find(item => item.code == CustomerType.CIB)?.description)?.color,
                }
              },
            ] : [
                {
                  value: listData?.nbrCstSegVua,
                  name: SEGMENTS.find(item => item.code == 'VUA')?.name,
                  itemStyle: {
                    color: JSON.parse(SEGMENTS.find(item => item.code == 'VUA')?.description)?.color,
                  }
                },
                {
                  value: listData?.nbrCstSegNho,
                  name: SEGMENTS.find(item => item.code == 'NHO')?.name,
                  itemStyle: {
                    color: JSON.parse(SEGMENTS.find(item => item.code == 'NHO')?.description)?.color,
                  }
                },
                {
                  value: listData?.nbrCstSegSieuNho,
                  name: SEGMENTS.find(item => item.code == 'SIEUNHO')?.name,
                  itemStyle: {
                    color: JSON.parse(SEGMENTS.find(item => item.code == 'SIEUNHO')?.description)?.color,
                  }
                },
                {
                  value: 0,
                  name: SEGMENTS.find(item => item.code == 'SME_NOT_YET_RATE')?.name,
                  itemStyle: {
                    color: JSON.parse(SEGMENTS.find(item => item.code == 'SME_NOT_YET_RATE')?.description)?.color,
                  }
                },
              ]
          }
        });
        this.dashboard.push(grid1);
        this.dashboard.push(grid2);
      })
    } else if ((divisionCode == CustomerType.SME || divisionCode == CustomerType.CIB) && type === DashboardType.OTHER_INDIV_BUSINESS) {
      const state = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DASHBOARD}`);
      const category = await forkJoin([
      this.categoryService.getCommonCategory(CommonCategory.BUSINESS_SME_DEBT_GROUP).pipe(catchError(() => of(undefined))),
      this.categoryService.getCommonCategory(CommonCategory.BUSINESS_SME_PERIOD).pipe(catchError(() => of(undefined))),
      this.categoryService.getCommonCategory(CommonCategory.BUSINESS_SME_PRODUCT).pipe(catchError(() => of(undefined))),
      this.categoryService.getCommonCategory(CommonCategory.BUSINESS_SME_PERIOD_HDV).pipe(catchError(() => of(undefined))),
      this.categoryService.getCommonCategory(CommonCategory.BUSINESS_SME_PRODUCT_GUARANTEE).pipe(catchError(() => of(undefined))),
      ]).toPromise();
      const listBranch = this.listBranch.filter(branch => branch.code).map(branch => branch.code).join(',');
      this.dashboard = [
        {
          cols: 1,
          rows: 5,
          y: 0,
          x: 0,
          component: DebtComponent,
          data: {
            category,
            dataForm,
            listBranch
          },
        },
        {
          cols: 1,
          rows: 5,
          y: 0,
          x: 0,
          component: HDVComponent,
          data: {
            category,
            dataForm,
            listBranch
          },
        },
        {
          cols: 1,
          rows: 5,
          y: 0,
          x: 0,
          component: GuaranteeComponent,
          data: {
            category,
            dataForm,
            listBranch
          },
        },
        {
          cols: 3,
          rows: 5.5,
          y: 0,
          x: 0,
          component: TableComponent,
          data: {
            id: 1,
            title: 'Top 30 KH có dư nợ lớn nhất',
            dataForm,
            listBranch
          },
        },
        {
          cols: 3,
          rows: 5.5,
          y: 0,
          x: 0,
          component: TableComponent,
          data: {
            id: 2,
            title: 'Top 30 KH HĐV lớn nhất',
            dataForm,
            listBranch
          },
        },
        {
          cols: 3,
          rows: 5.5,
          y: 0,
          x: 0,
          component: TableComponent,
          data: {
            id: 3,
            title: 'Top 30 KH bảo lãnh lớn nhất',
            dataForm,
            listBranch
          },
        },
      ];
    } else if (divisionCode !== CustomerType.INDIV && type === DashboardType.OTHER_INDIV_BUSINESS) {
      const state = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DASHBOARD}`);
      const data = {
        [DashboardOtherIndiv.DU_NO]: {
          key: DashboardOtherIndiv.DU_NO,
          dataForm: this.form.getRawValue(),
          title: 'Dư nợ thời điểm',
          type: 'chart',
          labels: [],
          legend: ['Tăng/giảm NET(tỷ đồng)', 'Dư nợ TĐ(tỷ đồng)'],
          dataBar: [],
          dataLine: [],
          configColor: this.parseJSON(
            _.find(
              state[CommonCategory.DASHBOARD_BUSINESS_COLOR],
              (item) => item.code === CommonCategory.CONFIG_COLOR_TINDUNG_TD
            )?.description
          ),
          isLoading: true,
          extraData,
          dashboardName: DashboardType.OTHER_INDIV_BUSINESS
        },
        [DashboardOtherIndiv.TOP10_DU_NO]: {
          key: DashboardOtherIndiv.TOP10_DU_NO,
          dataForm: this.form.getRawValue(),
          title: 'Top 10 Khách hàng có dư nợ lớn nhất',
          type: 'table',
          dataTable: this.parseJSON(dataChart?.dataTop10DuNo?.dashboardValue),
          isLoading: true,
          extraData,
          dashboardName: DashboardType.OTHER_INDIV_BUSINESS,
          showNote: true
        },
        [DashboardOtherIndiv.HDV]: {
          key: DashboardOtherIndiv.HDV,
          dataForm: this.form.getRawValue(),
          title: 'HĐV thời điểm',
          type: 'chart',
          labels: [],
          legend: ['Tăng/giảm NET(tỷ đồng)', 'Huy động vốn TĐ(tỷ đồng)'],
          dataBar: [],
          dataLine: [],
          configColor: this.parseJSON(
            _.find(
              state[CommonCategory.DASHBOARD_BUSINESS_COLOR],
              (item) => item.code === CommonCategory.CONFIG_COLOR_HUYDONGVON_TD
            )?.description
          ),
          isLoading: true,
          extraData,
          dashboardName: DashboardType.OTHER_INDIV_BUSINESS
        },
        [DashboardOtherIndiv.TOP10_HDV]: {
          key: DashboardOtherIndiv.TOP10_HDV,
          dataForm: this.form.getRawValue(),
          title: 'Top 10 Khách hàng có huy động vốn lớn nhất',
          type: 'table',
          dataTable: this.parseJSON(dataChart?.dataTop10HDV?.dashboardValue),
          isLoading: true,
          extraData,
          dashboardName: DashboardType.OTHER_INDIV_BUSINESS
        },
      };

      const result_DU_NO = this.getDataChartBusiness(dataChart?.dataChartDuNo?.dashboardValue, { dataBarKey: 'DUNO_NET', dataLineKey: 'DUNO' });
      data[DashboardOtherIndiv.DU_NO].dataBar = result_DU_NO.dataBar;
      data[DashboardOtherIndiv.DU_NO].dataLine = result_DU_NO.dataLine;
      data[DashboardOtherIndiv.DU_NO].labels = result_DU_NO.labels;

      const result_HDV = this.getDataChartBusiness(dataChart?.dataChartHDV?.dashboardValue, { dataBarKey: 'HDV_NET', dataLineKey: 'HUYDONG' });
      data[DashboardOtherIndiv.HDV].dataBar = result_HDV.dataBar;
      data[DashboardOtherIndiv.HDV].dataLine = result_HDV.dataLine;
      data[DashboardOtherIndiv.HDV].labels = result_HDV.labels;

      this.dashboard = [
        {
          cols: 1,
          rows: 4,
          y: 0,
          x: 0,
          component: DashboardBusinessChartSMEComponent,
          data: data[DashboardOtherIndiv.DU_NO],
        },
        {
          cols: 2,
          rows: 4,
          y: 0,
          x: 0,
          component: DashboardBusinessChartSMEComponent,
          data: data[DashboardOtherIndiv.TOP10_DU_NO],
        },
        {
          cols: 1,
          rows: 4,
          y: 0,
          x: 0,
          component: DashboardBusinessChartSMEComponent,
          data: data[DashboardOtherIndiv.HDV],
        },
        {
          cols: 2,
          rows: 4,
          y: 0,
          x: 0,
          component: DashboardBusinessChartSMEComponent,
          data: data[DashboardOtherIndiv.TOP10_HDV],
        },
      ];
      this.viewChange.emit(data);
    } else if (divisionCode !== CustomerType.INDIV && type === DashboardType.OTHER_INDIV_REVENUE) {

      const data = {
        [DashboardOtherIndiv.DTTTRR]: {
          key: DashboardOtherIndiv.DTTTRR,
          dataForm,
          title: 'DTT trước rủi ro',
          type: 'chart',
          labels: [],
          legend: ['Tăng/giảm NET(triệu đồng)', 'Doanh thu(triệu đồng)'],
          dataLine: [],
          configColor: this.parseJSON(
            _.find(
              state[CommonCategory.DASHBOARD_BUSINESS_COLOR],
              (item) => item.code === CommonCategory.CONFIG_COLOR_HUYDONGVON_TD
            )?.description
          ),
          isLoading: true,
          extraData,
          dashboardName: DashboardType.OTHER_INDIV_REVENUE
        },
        [DashboardOtherIndiv.TOP10_TRR]: {
          key: DashboardOtherIndiv.TOP10_TRR,
          dataForm,
          title: 'Top 10 Khách hàng phát sinh biến động doanh thu trước rủi ro lớn nhất',
          type: 'table',
          dataTable: dataChart?.dataTop10DTTTRR ? this.parseJSON(dataChart?.dataTop10DTTTRR[0]?.dashboardValue) : null,
          isLoading: true,
          dashboardName: DashboardType.OTHER_INDIV_REVENUE,
          showNote: true
        },
        [DashboardOtherIndiv.DTTSRR]: {
          key: DashboardOtherIndiv.DTTSRR,
          dataForm,
          title: 'DTT sau rủi ro',
          type: 'chart',
          labels: [],
          legend: ['Tăng/giảm NET(triệu đồng)', 'Doanh thu(triệu đồng)'],
          dataBar: [],
          dataLine: [],
          configColor: this.parseJSON(
            _.find(
              state[CommonCategory.DASHBOARD_BUSINESS_COLOR],
              (item) => item.code === CommonCategory.CONFIG_COLOR_HUYDONGVON_TD
            )?.description
          ),
          isLoading: true,
          extraData,
          dashboardName: DashboardType.OTHER_INDIV_REVENUE
        },
        [DashboardOtherIndiv.TOP10_SRR]: {
          key: DashboardOtherIndiv.TOP10_SRR,
          dataForm: this.form.getRawValue(),
          title: 'Top 10 Khách hàng phát sinh biến động doanh thu sau rủi ro lớn nhất',
          type: 'table',
          dataTable: dataChart?.dataTop10DTTSRR ? this.parseJSON(dataChart?.dataTop10DTTSRR[0]?.dashboardValue) : null,
          isLoading: true,
          dashboardName: DashboardType.OTHER_INDIV_REVENUE
        },
        [DashboardOtherIndiv.DTTTRR_TBT]: {
          key: DashboardOtherIndiv.DTTTRR_TBT,
          dataForm: this.form.getRawValue(),
          title: 'DTT trước rủi ro - thu bất thường',
          type: 'chart',
          labels: [],
          legend: ['Tăng/giảm NET(triệu đồng)', 'Doanh thu(triệu đồng)'],
          dataLine: [],
          configColor: this.parseJSON(
            _.find(
              state[CommonCategory.DASHBOARD_BUSINESS_COLOR],
              (item) => item.code === CommonCategory.CONFIG_COLOR_HUYDONGVON_TD
            )?.description
          ),
          isLoading: true,
          extraData,
          dashboardName: DashboardType.OTHER_INDIV_REVENUE
        }
      };

      const result_DTTTRR = this.getDataChartRevenue(dataChart?.dataChartDTTTRR, 'DTTTRR');
      data[DashboardOtherIndiv.DTTTRR].dataLine = result_DTTTRR.dataLine;
      data[DashboardOtherIndiv.DTTTRR].labels = result_DTTTRR.labels;

      const result_DTTSRR = this.getDataChartRevenue(dataChart?.dataChartDTTSRR, 'DTTSRR');
      data[DashboardOtherIndiv.DTTSRR].dataLine = result_DTTSRR.dataLine;
      data[DashboardOtherIndiv.DTTSRR].labels = result_DTTSRR.labels;


      const result_DTTTRR_TBT = this.getDataChartRevenue(dataChart?.dataChartDTTTRR_TBT, 'DTTTRR_TBT');
      data[DashboardOtherIndiv.DTTTRR_TBT].dataLine = result_DTTTRR_TBT.dataLine;
      data[DashboardOtherIndiv.DTTTRR_TBT].labels = result_DTTTRR_TBT.labels;

      // if (this.oldType !== type) {
      const DTTChart = [
        {
          cols: 1,
          rows: 4,
          y: 0,
          x: 0,
          component: DashboardBusinessChartSMEComponent,
          data: data[DashboardOtherIndiv.DTTTRR],
        },
        {
          cols: 1,
          rows: 4,
          y: 0,
          x: 0,
          component: DashboardBusinessChartSMEComponent,
          data: data[DashboardOtherIndiv.DTTSRR],
        },
        {
          cols: 1,
          rows: 4,
          y: 0,
          x: 0,
          component: DashboardBusinessChartSMEComponent,
          data: data[DashboardOtherIndiv.DTTTRR_TBT],
        }
      ]
      const top10List = [
        {
          cols: 3,
          rows: 4,
          y: 0,
          x: 0,
          component: DashboardBusinessChartSMEComponent,
          data: data[DashboardOtherIndiv.TOP10_TRR],
        },
        {
          cols: 3,
          rows: 4,
          y: 0,
          x: 0,
          component: DashboardBusinessChartSMEComponent,
          data: data[DashboardOtherIndiv.TOP10_SRR],
        }
      ]
      this.dashboard = [
        ...(this.isSME && !this.form.get('isRegion').value) ? [] : DTTChart,
        ...top10List
      ];
      // }

      this.viewChange.emit(data);
    } else if (divisionCode !== CustomerType.INDIV && type === DashboardType.OTHER_INDIV_KPI) {
      const chartList = dataChart.kpiTargets.reduce((result, data) => {
        if (!result) result = [];
        const dateType = this.form.controls.dateType.value;
        let fullDate;
        if (dateType === 'month') {
          fullDate = moment()
          .month(data.month - 1)
          .year(data.year);
        } else {
          fullDate = moment(data?.bussiessDate);
        }

        const value = {
          itemCode: data.itemCode,
          itemName: data.itemName,
          maxValue: data.maxValue,
          minValue: data.minValue,
          unitName: data.unitName,
          list: [
            {
              barName: fullDate.format(dateType === 'month' ? 'MM/YY' : 'DD/MM/YY'),
              barValue: data.entrustValue,
              lineValue: data.value,
              linePoint: data.entrustPoint,
              month: data.month,
              fullDate,
            },
          ],
        };
        const index = result.findIndex((v) => v.itemCode === data.itemCode);
        if (index > -1) {
          result[index].list.push(value.list[0]);
          // sort
          result[index].list.sort((first, second) => {
            if (first.fullDate.isAfter(second.fullDate)) {
              return 1;
            } else if (first.fullDate.isBefore(second.fullDate)) {
              return -1;
            } else {
              return first.month - second.month;
            }
          });
        } else {
          result.push(value);
        }
        return result;
      }, []);

      // set color to chart - max 8
      let counter = 0;
      chartList.forEach((chart) => {
        if (counter + 1 > 8) counter = 0;
        chart.colorIndex = counter;
        counter++;
      });

      const maxChartOnRow = 3;
      const chartHeight = 300;
      const upTo = Math.ceil(chartHeight / this.options.fixedRowHeight);
      let rows = chartList.length ? Math.ceil(chartList.length / maxChartOnRow) * upTo : 8;
      // 8 if have payroll amount
      rows = Math.max(rows, 6);

      this.dashboard = [
        {
          cols: 2,
          rows,
          y: 0,
          x: 0,
          component: KpiTotalPointChartComponent,
          data: {
            rank: dataChart.kpiRankingCategories,
            avg: dataChart.kpiAvgPointEntrust,
            sum4months: dataChart.kpiFourMonths,
            type: this.form.controls.dateType.value
          },
        },
        {
          cols: 6,
          rows,
          y: 0,
          x: 0,
          component: KpiTargetChartComponent,
          data: { raw: dataChart.kpiTargets, chartList, timePoint: dataChart.timePoint },
        },
      ];
    } else {
      this.dashboard = [];
    }
    this.oldType = type;
    this.isLoading = false;
  }

  getCommonData(): Observable<any> {
    const commonData = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DASHBOARD}`);
    if (commonData) {
      return of(commonData);
    } else {
      return forkJoin({
        [CommonCategory.STATUS_CUSTOMER_CONFIG]: this.commonService
          .getCommonCategory(CommonCategory.STATUS_CUSTOMER_CONFIG)
          .pipe(
            map((data) => _.get(data, 'content', [])),
            catchError(() => of(undefined))
          ),
        [CommonCategory.CUSTOMER_OBJECT_CONFIG]: this.commonService
          .getCommonCategory(CommonCategory.CUSTOMER_OBJECT_CONFIG)
          .pipe(
            map((data) => _.get(data, 'content', [])),
            catchError(() => of(undefined))
          ),
        [CommonCategory.SEGMENT_CUSTOMER_CONFIG]: this.commonService
          .getCommonCategory(CommonCategory.SEGMENT_CUSTOMER_CONFIG)
          .pipe(
            map((data) => _.get(data, 'content', [])),
            catchError(() => of(undefined))
          ),
        [CommonCategory.DASHBOARD_MANAGERMENT]: this.commonService
          .getCommonCategory(CommonCategory.DASHBOARD_MANAGERMENT)
          .pipe(
            map((data) => _.get(data, 'content', [])),
            catchError(() => of(undefined))
          ),
        [CommonCategory.DASHBOARD_LIST]: this.commonService.getCommonCategory(CommonCategory.DASHBOARD_LIST).pipe(
          map((data) => _.get(data, 'content', [])),
          catchError(() => of(undefined))
        ),
        [CommonCategory.DASHBOARD_BUSINESS_COLOR]: this.commonService
          .getCommonCategory(CommonCategory.DASHBOARD_BUSINESS_COLOR)
          .pipe(
            map((data) => _.get(data, 'content', [])),
            catchError(() => of(undefined))
          ),
        listDivision: of(this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM)),
      });
    }
  }

  pin() {
    this.service.updateConfigDefaultDashboard(this.form.get('type').value).subscribe(() => {
      this.currentType = this.form.get('type').value;
    });
  }

  getDataCustomerSME(data, state) {
    const item = _.find(
      state[CommonCategory.DASHBOARD_MANAGERMENT],
      (item) => item.code === DashboardType.OTHER_INDIV_CUSTOMER
    );
    const config = this.parseJSON(item?.value);
    const listChart: DataChartCustomerModel[] = [];
    let objChart: DataChartCustomerModel = {
      title: '',
      data: [],
    };
    let listConfig: any[];
    let modelChart: ChartCustomerModel;
    if (data) {
      Object.keys(data).forEach((keyDTO) => {
        const dtoChart = data[keyDTO];
        listConfig = state[dtoChart.keyConfig];
        objChart = {
          title: dtoChart.title,
          data: [],
          order: 0,
        };
        Object.keys(dtoChart).forEach((key) => {
          if (key !== 'keyConfig' && key !== 'code' && key !== 'title') {
            const config = this.parseJSON(_.find(listConfig, (item) => item.code === key)?.description);
            modelChart = {
              code: key,
              label: config?.description,
              color: config?.color,
              value: dtoChart[key],
            };
            objChart.data.push(modelChart);
          }
        });
        listChart.push(objChart);
      });
      if (config) {
        Object.keys(config).forEach((key, i) => {
          const index = listChart?.findIndex((item) => item.title === config[key]);
          if (index != -1) {
            listChart[index].order = i;
          } else {
            listChart.push({
              title: config[key],
              data: [],
              order: i,
            });
          }
        });
      }
    }
    return _.orderBy(listChart, 'order');
  }

  getData(type: string, divisionCode: string) {
    // some dashboard will showing if match division
    if (divisionCode === CustomerType.INDIV) {
      if (type === DashboardType.INDIV_CUSTOMER) {
        const params = {
          customerTypes: [CustomerType.INDIV, CustomerType.NHS],
          rsId: this.objFunction?.rsId,
          scope: Scopes.VIEW,
        };
        return forkJoin({
          dataSegment: this.customerApi.getSegmentDataChart(params).pipe(catchError(() => of(undefined))),
          dataPrivate: this.customerApi.getPrivatePriorityDataChart(params).pipe(catchError(() => of(undefined))),
          dataProductUsed: this.customerApi.getProductUsedDataChart(params).pipe(catchError(() => of(undefined))),
          dataStatus: this.customerApi.getStatusDataChart(params).pipe(catchError(() => of(undefined))),
          dataBScore: this.customerApi.getBScoreDataChart(params).pipe(catchError(() => of(undefined))),
          dataToi: this.customerApi.getTOIDataChart(params).pipe(catchError(() => of(undefined))),
        });
      } else {
        const params = {
          rsId: this.objFunction?.rsId,
          scope: Scopes.VIEW,
          blockCode: divisionCode,
        };
        return forkJoin({
          dataRaiseCapital: this.service.getDataTopRaiseCapitalCustomer(params).pipe(catchError(() => of(undefined))),
          dataCredit: this.service.getDataTopCreditCustomer(params).pipe(catchError(() => of(undefined))),
        });
      }
    } else {
      const dataForm = _.cloneDeep(this.form.getRawValue());
      const params = {
        divisionCode,
        rmCode: dataForm.isRegion ? '' : dataForm.rmCode,
        regionCode: dataForm.isRegion ? dataForm.regionCode : null,
        branchCode: dataForm.branchCode,
      };
      const revenueParams = this.getRevenueParams(params);
      if (type === DashboardType.OTHER_INDIV_CUSTOMER) {
        // const params = {
        //   divisionCode,
        //   rmCode: this.currUser?.code,
        //   businessDate: moment().add(-1, 'day').format('DD-MMM-YY'),
        // };
        const params = {
          transactionDate: moment(dataForm.date).format('DD/MM/YYYY'),
          blockCode: dataForm.divisionCode,
          rmCode: dataForm.isRegion ? '' : dataForm.rmCode,
          branchCodeCap1: dataForm.branchCodeLv1,
          branchCodeCap2: !dataForm.branchCode ? this.listBranch.filter(branch => branch.code).map(branch => branch.code).join(',') : dataForm.branchCode,
          area: !dataForm.regionCode ? this.listRegion.map(item => item.locationCode).join(',') : dataForm.regionCode
        }
        return this.service.getCstProfileInfo(params).pipe(catchError(() => of([])));
        //  this.service.getDataChartsCustomerOtherIndiv(params).pipe(catchError(() => of({})));
      } else if (type === DashboardType.OTHER_INDIV_KPI) {
        // Start get dashboard kpi datas
        let toDate = moment(dataForm.toDate);

        const kpiParams = {
          divisionCode,
          rmCode: dataForm.rmCode,
          toDate: toDate.format('DD/MM/YYYY'),
          rsId: this.objFunction?.rsId,
          scope: Scopes.VIEW,
        };
        let forkJoinReq;

        return this.kpiDashboardService.getTimePoint({ divisionCode }).pipe(
          tap(data => {
            if (Utils.isNotNull(data) && !this.changeTimePoint) {
              this.setKpiMaxDate(data);
              toDate = moment(data?.value, 'DD/MM/YYYY');
            }
            this.changeTimePoint = false;
            const maxDayOfMonth = this.kpiMaxDate.getDate();
            const isSameMonth = toDate.isSame(moment(this.kpiMaxDate), 'month');
            const lastDayInMonth = toDate.daysInMonth();

            if (isSameMonth && lastDayInMonth > maxDayOfMonth) {
              toDate.date(maxDayOfMonth);
            } else {
              toDate.date(lastDayInMonth);
            }

            if (this.form.controls.dateType.value === 'day') {
              // day period
              kpiParams.toDate = moment(this.form.controls.toDate.value).format('DD/MM/YYYY');
              forkJoinReq = {
                kpiRankingCategories: this.kpiDashboardService.getRankingCategoriesDailies(kpiParams).pipe(catchError(() => of(undefined))),
                kpiAvgPointEntrust: this.kpiDashboardService.getAvgPointEntrustDailies(kpiParams).pipe(catchError(() => of(undefined))),
                kpiTargets: this.kpiDashboardService.getKpiTargetsDailies(kpiParams).pipe(catchError(() => of(undefined))),
                kpiFourMonths: this.kpiDashboardService.getSummaryFourDays(kpiParams).pipe(catchError(() => of(undefined))),
              };
            } else {
              // month period
              kpiParams.toDate = toDate.format('DD/MM/YYYY');
              forkJoinReq = {
                kpiRankingCategories: this.kpiDashboardService.getRankingCategories(kpiParams).pipe(catchError(() => of(undefined))),
                kpiAvgPointEntrust: this.kpiDashboardService.getAvgPointEntrust(kpiParams).pipe(catchError(() => of(undefined))),
                kpiTargets: this.kpiDashboardService.getKpiTargets(kpiParams).pipe(catchError(() => of(undefined))),
                kpiFourMonths: this.kpiDashboardService.getSummaryFourMonths(kpiParams).pipe(catchError(() => of(undefined))),
              };
            }
          }),
          catchError(() => of(undefined)),
          concatMap(() =>
            forkJoin(forkJoinReq)
          )
        );
      } else if (type === DashboardType.OTHER_INDIV_REVENUE) {
        return forkJoin({
          dataChartDTTTRR: this.service
            .getDataChartsBusinessOtherIndiv(
              {
                dashboardType: DashboardOtherIndiv.DTTTRR,
                type: 'DAILY',
                ...revenueParams,
              },
              false
            )
            .pipe(catchError(() => of(undefined))),
          dataChartDTTSRR: this.service
            .getDataChartsBusinessOtherIndiv(
              {
                dashboardType: DashboardOtherIndiv.DTTSRR,
                type: 'DAILY',
                ...revenueParams,
              },
              false
            )
            .pipe(catchError(() => of(undefined))),
          dataTop10DTTTRR: this.service
            .getDataChartsBusinessOtherIndiv(
              {
                dashboardType: DashboardOtherIndiv.TOP10_TRR,
                ...revenueParams,
              },
              false
            )
            .pipe(catchError(() => of(undefined))),
          dataTop10DTTSRR: this.service
            .getDataChartsBusinessOtherIndiv(
              {
                dashboardType: DashboardOtherIndiv.TOP10_SRR,
                ...revenueParams,
              },
              false
            )
            .pipe(catchError(() => of(undefined))),
          dataChartDTTTRR_TBT: this.service
            .getDataChartsBusinessOtherIndiv(
              {
                dashboardType: DashboardOtherIndiv.DTTTRR_TBT,
                type: 'DAILY',
                ...revenueParams,
              },
              false
            )
            .pipe(catchError(() => of(undefined))),
        });
      } else {
        return forkJoin({
          dataChartDuNo: this.service
            .getDataChartsBusinessOtherIndiv(
              {
                dashboardType: DashboardOtherIndiv.DU_NO,
                type: 'DAILY',
                ...revenueParams,
              },
              true
            )
            .pipe(catchError(() => of(undefined))),
          dataChartHDV: this.service
            .getDataChartsBusinessOtherIndiv(
              {
                dashboardType: DashboardOtherIndiv.HDV,
                type: 'DAILY',
                ...revenueParams,
              },
              true
            )
            .pipe(catchError(() => of(undefined))),
          dataTop10DuNo: this.service
            .getDataChartsBusinessOtherIndiv(
              {
                dashboardType: DashboardOtherIndiv.TOP10_DU_NO,
                ...revenueParams,
              },
              true
            )
            .pipe(catchError(() => of(undefined))),
          dataTop10HDV: this.service
            .getDataChartsBusinessOtherIndiv(
              {
                dashboardType: DashboardOtherIndiv.TOP10_HDV,
                ...revenueParams,
              },
              true
            )
            .pipe(catchError(() => of(undefined))),
        });
      }
    }
  }

  parseJSON(value: string) {
    if (value) {
      return JSON.parse(value);
    }
    return null;
  }

  getListRm() {
    const data = this.form.getRawValue();
    this.isLoading = true;
    if (this.isPermission) {
      const params = {
        branchCodes: !data.branchCode ? this.listBranch.filter(branch => branch.code).map(branch => branch.code) : [data.branchCode],
        blockCode: data.divisionCode
      }

      this.service.getAllRMByBranches(params).subscribe((listRm) => {
        listRm = listRm.filter(rm => rm.rmCode);

        // filter RM that is unavailable
        if (this.form.get('type').value === DashboardType.INDIV_CAMPAIGN) {
          listRm = listRm.filter(item => {
            if (item.statusWork === 'NTS')
              return false;
            if (item.statusWork === 'NV')
              return false;
            return true;
          } )
        }

        this.listRM = _.map(listRm, (item) => {
          return {
            code: item.rmCode,
            name: `${item.rmCode} - ${item.rmName}`,
          };
        });

      this.listRM = _.orderBy(this.listRM, [rm => rm.name], ['asc']);
      if (this.form.get('type').value === DashboardType.INDIV_CAMPAIGN && !this.form.get('isRegion').value ) {

        this.isLoading = false;
        this.form.get('rmCode').setValue(_.head(this.listRM)?.code, { emitEvent: false });
        this.form.controls.rmCode.updateValueAndValidity();
      } else {
        this.form.get('rmCode').setValue(_.head(this.listRM)?.code, { emitEvent: false });
        this.getDataV2(data.type, data.divisionCode);
        }
      });
    } else {
      if (_.size(this.listRM) === 0) {
        this.listRM.push({
          code: this.currUser?.code,
          name: `${this.currUser?.code} - ${this.currUser?.fullName}`,
        });
      }
      this.form.get('rmCode').setValue(_.head(this.listRM)?.code, { emitEvent: false });
      this.getDataV2(data.type, data.divisionCode);
    }
  }

  setMaxCols(type: string = null): void {
    switch (type) {
      case DashboardType.OTHER_INDIV_KPI:
        this.options.maxCols = 8;
        break;
      case DashboardType.INDIV_CUSTOMER:
        this.options.maxCols = 6;
        break;
      default:
        this.options.maxCols = this.DEFAULT_MAXCOLS;
        break;
    }
    this.options = { ...this.options };
  }

  // without branch
  getRmListObserable(value = ''): Observable<any> {
    this.isRmLoading = true;
    const data = this.form.getRawValue();
    const params = {
      crmIsActive: 'true',
      page: 0,
      rsId: this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`)?.rsId,
      search: value,
      scope: 'VIEW',
      size: 100000,
      rmBlock: data.divisionCode,
    };
    return this.rmApi.post('findAll', params);
  }

  getListRmForKpi(): void {
    const { divisionCode, type } = this.form.getRawValue();
    const getData = () => {
      this.form.get('rmCode').setValue(this.currUser.code, { emitEvent: false });

      this.getDataV2(type, divisionCode);
    };

    if (!_.isEmpty(this.kpiRmList[divisionCode])) {
      this.listRM = this.kpiRmList[divisionCode];
      getData();
      return;
    }

    this.isLoading = true;
    if (this.isRm) {
      this.kpiRmList[divisionCode] = this.listRM;
      this.addCurrUser();
      getData();
    } else {
      this.getRmListObserable().subscribe((result) => {
        this.listRM = result.content.map((rm) => {
          return {
            code: rm.t24Employee.employeeCode,
            name: `${rm.t24Employee.employeeCode} - ${rm.hrisEmployee.fullName}`,
          };
        });
        this.listRM = _.uniq(this.listRM, 'code');
        this.kpiRmList[divisionCode] = this.listRM;
        this.addCurrUser();
        getData();
      }, () => {
        this.isLoading = false;
      });
    }
  }

  addCurrUser(): void {
    const isCurrentUserExist = this.listRM.find((rm) => rm.code === this.currUser.code);
    if (!isCurrentUserExist) {
      this.listRM.push({
        code: this.currUser.code,
        name: `${this.currUser.code} - ${this.currUser.fullName}`,
      });
    }
  }

  setKpiMaxDate(timepoint: any): void {
    // kpi time point
    if (this.form.controls.dateType.value === 'month') {
      let fromDate;
      const date = timepoint?.value || new Date();
      const maxDate = moment(date, 'DD/MM/YYYY');
      this.kpiMaxDate = new Date(maxDate.year(), maxDate.month(), parseInt(maxDate.format('DD')));
      this.form.controls.toDate.setValue(this.kpiMaxDate, { emitEvent: false });
      fromDate = moment(date, 'DD/MM/YYYY').subtract(3, 'months');
      fromDate = new Date(fromDate.year(), fromDate.month(), parseInt(fromDate.format('DD')));
      this.form.controls.fromDate.setValue(fromDate, { emitEvent: false });
    }
  }

  get isNormalRm(): boolean {
    return this.listBranchTerm.length === 0;
  }

  getDataChartRevenue(raw: any[], type: string) {
    const groupList = [];

    const result = {
      dataLine: [],
      labels: []
    }

    const orderData = (dashboardValue: any[]) => {
      return _.orderBy(dashboardValue, [obj => new Date(obj.DATA_DATE)], ['asc']);
    }

    if (raw) {
      // group by date
      raw.forEach(item => {
        const groupBy = {};
        const dashboardData = this.parseJSON(item.dashboardValue);
        const dashboardValue = orderData(dashboardData);

        _.forEach(dashboardValue, v => {
          const time = moment(v.DATA_DATE).format('DD/MM');
          groupBy[time] = Number(v[type]);
        });
        groupList.push(groupBy);
      });

      // sum
      const formatData = { ...groupList[0] };
      for (let i = 1; i < groupList.length; i++) {
        Object.keys(formatData).forEach(key => {
          formatData[key] += typeof groupList[i][key] !== 'number' ? 0 : groupList[i][key];
        })
      }

      Object.keys(formatData).forEach(key => {
        result.dataLine.push(_.round(_.divide(formatData[key], 10e5), 2).toFixed(2))
        result.labels.push(key);
      });
    }

    return result;
  }

  setBranchLv1List(options: any = {}): void {
    const { regionCode, branchCode } = this.form.getRawValue();
    const { setLv2List = true, resetCode = false } = options;
    const branchLv1List = regionCode ? this.regions[regionCode]?.branch_lv1 : [];

    if (resetCode) {
      this.form.controls.branchCode.setValue('', { emitEvent: false });
      this.form.controls.branchCodeLv1.setValue('', { emitEvent: false });
    }

    if (!branchLv1List?.length) {
      this.branchLv1List = _.cloneDeep(this.allBranchLv1);
    } else {
      this.branchLv1List = branchLv1List;
    }

    // sort alphabe
    this.branchLv1List = _.orderBy(this.branchLv1List, [branch => branch.displayName], ['asc']);

    const haveAllOption = this.branchLv1List.find(branch => branch.code === '');
    if (this.branchLv1List.length > 1 && !haveAllOption) {
      this.branchLv1List.unshift({ code: '', displayName: 'Tất cả' });
    }

    let value = _.head(this.branchLv1List)?.code;

    // follow branch lv2
    if (branchCode && !setLv2List) {
      const branchLv2 = this.allBranchLv2.find(branch => branch.code === branchCode);

      if (branchLv2?.parentCode) value = branchLv2?.parentCode;
    }

    this.form.controls.branchCodeLv1.setValue(value, { emitEvent: false });

    if (setLv2List) {
      this.setBranchLv2List();
    }

  }

  setBranchLv2List(options: any = {}): void {
    let branchLv2List = [];
    let { regionCode, branchCodeLv1, branchCode } = this.form.getRawValue();
    let { onChange = false, alreadyPick = false } = options;

    // Don't need branchCodeLv1 anymore in dashboard INDIV_CAMPAIGN
    if (this.form.get('type').value === DashboardType.INDIV_CAMPAIGN) {
      if (this.form.get('isRegion').value == true) {
        branchLv2List = this.lstBranchDashboardBusiness.filter(branch => branch.khuVucM === regionCode);
      } else {
        branchLv2List = this.lstBranchDashboardBusiness;
      }
    } else {
      if (regionCode && branchCodeLv1) {
        const branchLv1 = this.regions[regionCode]?.branch_lv1.find(branch => branch.code === branchCodeLv1);
        branchLv2List = branchLv1?.branch_lv2 || [];
      } else if (regionCode) {
        const branchLv1List = this.regions[regionCode].branch_lv1;
        branchLv1List.filter(branch => branch.code).forEach(item => {
          branchLv2List.push(...item.branch_lv2);
        });
      } else if (branchCodeLv1) {
        branchLv2List = this.allBranchLv2.filter(branch => branch.parentCode === branchCodeLv1);
      } else {
        branchLv2List = this.allBranchLv2;
      }
    }

    this.listBranch = _.cloneDeep(branchLv2List);

    this.listBranch = _.orderBy(this.listBranch, [branch => branch.displayName], ['asc']);

    this.setOptionAllToBrLv2();

    if (!onChange && !alreadyPick) {
      this.form.controls.branchCode.setValue(_.head(this.listBranch)?.code, { emitEvent: false });
    }
  }

  getDataChartBusiness(dashboardValue: string, options: any) {
    const { dataBarKey, dataLineKey } = options;
    const unit = 10e8;

    const result = {
      dataLine: [],
      labels: [],
      dataBar: []
    }

    const orderData = (dashboardValue: any[]) => {
      return _.orderBy(dashboardValue, [obj => new Date(obj.DATA_DATE)], ['asc']);
    }

    if (dashboardValue) {
      let dashboardData = this.parseJSON(dashboardValue);
      dashboardData = orderData(dashboardData);

      _.forEach(dashboardData, (item) => {
        result.dataBar.push(_.round(_.divide(item[dataBarKey], unit), 2).toFixed(2));
        result.dataLine.push(_.round(_.divide(item[dataLineKey], unit), 2).toFixed(2));
        result.labels.push(
          moment(item.DATA_DATE).format('DD/MM')
        );
      });
    }

    return result;
  }

  setValueChanges(): void {
    this.form.controls.type.valueChanges.subscribe((value) => {
      if (this.isLoading) {
        return;
      }
      this.showEmbedded = false;
      this.isLoading = true;
      if (value === DashboardType.INDIV_BUSINESS) {
        this.showEmbedded = true;
      }
      const data = this.form.getRawValue();
      this.dashboard = [];
      this.setMaxCols(data.type);
      if (
        ( data.type === DashboardType.OTHER_INDIV_BUSINESS
          || data.type === DashboardType.OTHER_INDIV_REVENUE
        )
        &&data.divisionCode !== Division.INDIV
      ) {
        this.getListRm();
        this.setIsRegionByRole(false);
        this.setBranchOfUser();

        // fix lỗi nhấp nháy khi chọn sme-business
        if(data.divisionCode != Division.SME){
          this.onChangeForm();
        }

      } else if (data.type === DashboardType.OTHER_INDIV_KPI && data.divisionCode !== Division.INDIV) {
        this.getListRmForKpi();
      } else if (data.type === DashboardType.INDIV_CAMPAIGN) {
        this.initializeRegionListIndv()
        this.getCampaignList();
      }
      else {
        this.onChangeForm();
      }



    });
    this.form.controls.divisionCode.valueChanges.subscribe((value) => {
      if (this.isLoading) {
        return;
      }
      this.mapListType(value);
    });
    this.form.controls.branchCode.valueChanges.subscribe((value) => {
      if (this.isLoading) {
        return;
      }
      // set branchcode lv1 from branch code lv2
      const { branchCode, branchCodeLv1, isRegion } = this.form.getRawValue();

      if (branchCode && !branchCodeLv1) {
        const branchLv1 = this.listBranch.find(branch => branch.code === branchCode);

        this.form.controls.branchCodeLv1.setValue(branchLv1.parentCode, { emitEvent: false });

        this.setBranchLv2List({ onChange: true });
      }

      if (!isRegion) {
        this.getListRm();

      } else {
        // if branch dimension is selected
        this.getCampaignList();

        if(this.form.get('type').value !== DashboardType.INDIV_CAMPAIGN)
          this.onChangeForm();
      }


    });
    this.form.controls.rmCode.valueChanges.subscribe(() => {
      if (this.isLoading) {
        return;
      }

      const formData = this.form.getRawValue();

      if(this.form.get('type').value === DashboardType.INDIV_CAMPAIGN && !formData.isRegion) {
        this.getCampaignList();
      }

      if(this.form.get('type').value !== DashboardType.INDIV_CAMPAIGN) {
        this.onChangeForm();
      }
    });

    // listen campagins change
    this.form.controls.campaigns.valueChanges.subscribe(() => {
      if (this.isLoading) {
        return;
      }
      this.onChangeForm();
    });

    // listen campagins change
    // this.form.controls.campaigns.valueChanges.subscribe(() => {
    //   if (this.isLoading) {
    //     return;
    //   }
    //   this.onChangeForm();
    // });

    this.form.controls.periodsSME.valueChanges.subscribe(() => {
      if (this.isLoading) {
        return;
      }
      this.onChangeForm();
    });

    this.form.controls.regionCode.valueChanges.subscribe((value) => {

      if (this.isLoading) {
        return;
      }
      // filter branch lv1, lv2

      if(this.form.get('type').value !== DashboardType.INDIV_CAMPAIGN) {
        if (!this.isChangeSide) this.setBranchLv1List({resetCode: true});
        this.isChangeSide = false;


        if (this.form.get('type').value !== DashboardType.INDIV_CAMPAIGN) {
          this.onChangeForm();
        }
      }
      else {
        this.setBranchLv2List();
        this.form.controls.branchCode.patchValue(this.form.controls.branchCode.value, { emitEvent: true });

      }
    });
    this.form.controls.isRegion.valueChanges.subscribe((value) => {
      if (this.isLoading) {
        return;
      }

      if (value) {
        // reset options
        this.isbranchDimention = true;
        this.isChangeSide = true;
        this.setHeadRegion();
        this.setBranchOfUser({ changeRegionCode: true });

      } else {
        this.isbranchDimention = false;
        this.listBranch = _.cloneDeep(this.listBranchTerm);
        if (!_.find(this.listBranch, (item) => item.code === this.currUser?.branch)) {
          this.listBranch.push({
            code: this.currUser?.branch,
            displayName: `${this.currUser?.branch} - ${this.currUser?.branchName}`,
          });
        }
        this.listBranch = _.orderBy(this.listBranch, [branch => branch.displayName], ['asc']);

        this.setOptionAllToBrLv2();

        this.form.controls.branchCode.setValue(this.currUser?.branch, { emitEvent: false });

        this.getListRm();


      }
    });

    this.form.controls.toDate.valueChanges.subscribe((value: Date) => {
      if (this.isLoading) {
        return;
      }

      const fromDate = new Date(value);
      if (this.form.controls.dateType.value === 'month') {
        fromDate.setMonth(value.getMonth() - 3);
      } else {
        fromDate.setDate(value.getDate() - 3);
      }
      this.form.get('fromDate').setValue(fromDate);
      this.changeTimePoint = true;
      this.onChangeForm();
    });

    this.form.controls.branchCodeLv1.valueChanges.subscribe(value => {
      if (this.isLoading) {
        return;
      }
      this.setBranchLv2List();

      this.onChangeForm();
    })

    this.form.controls.date.valueChanges.subscribe(res => {
      this.onChangeForm();
    });
    this.form.controls.dateType.valueChanges.subscribe(res => {
      const { divisionCode, type } = this.form.getRawValue();
      if (res === 'day') {
        this.kpiMonthPicker = false;
        let fromDate;
        const date = moment(new Date(), 'DD/MM/YYYY').subtract(1, 'days');
        const maxDate = moment(date, 'DD/MM/YYYY');
        this.kpiMaxDate = new Date(maxDate.year(), maxDate.month(), parseInt(maxDate.format('DD')));
        this.form.controls.toDate.setValue(this.kpiMaxDate, { emitEvent: false });
        fromDate = moment(date, 'DD/MM/YYYY').subtract(3, 'days');
        fromDate = new Date(fromDate.year(), fromDate.month(), parseInt(fromDate.format('DD')));
        this.form.controls.fromDate.setValue(fromDate, { emitEvent: false });
      } else {
        this.kpiMonthPicker = true;
      }
      this.getDataV2(type, divisionCode);
    })
  }

  initializeRegionListIndv() {
    this.listRegion = [];
    const uniqueRegionCodes = new Set();
    const branchesOfUserSet = new Set(this.lstBranchDashboardBusiness?.map((i) => i.khuVucM));
    this.listRegion2?.forEach((item) => {
      if (branchesOfUserSet.has(item?.locationCode) && !uniqueRegionCodes.has(item?.locationCode)) {
        this.listRegion.push({locationCode: item.locationCode, locationName: item.locationName});
        uniqueRegionCodes.add(item?.locationCode);
      }
    });
    // dashboard is individual DIVISION and campaign TYPE, do not show option ALL
    this.setBranchOfUser();
    this.listRegion = this.listRegion.filter(item => item.locationCode !== '');
  }

  setIsRegionByRole(setChange = true): void {
    if (this.listBranchTerm.length === 0) {
      this.form.get('isRegion').disable();
    } else {
      this.form.get('isRegion').setValue(true, { emitEvent: setChange });
    }
    this.setHeadRegion();
  }

  setHeadRegion(): void {
    // add option all - regionlist
    if (this.form.get('type').value !== DashboardType.INDIV_CAMPAIGN) {
      const haveOptionAll = this.listRegion.find(region => region.locationCode === '');
      if (!haveOptionAll && this.listRegion.length > 1) {
        this.listRegion.unshift(
          {
            locationCode: '',
            locationName: 'Tất cả'
          }
        )
      }
      this.form.controls.regionCode.setValue(_.head(this.listRegion)?.locationCode, { emitEvent: false });
    }
  }

  // set default regionCode with branch lv1, vlv2
  setBranchOfUser(options: any = {}) {
    const { isRegion } = this.form.getRawValue();
    const { changeRegionCode = false } = options;
    if (this.currUser?.branch) {

      var branchLv2;

      if (this.form.get('type').value === DashboardType.INDIV_CAMPAIGN) {
        branchLv2 = this.lstBranchDashboardBusiness.find(branch => branch.code === this.currUser.branch);
      } else {
        branchLv2 = this.allBranchLv2.find(branch => branch.code === this.currUser.branch);
      }

      this.form.controls.branchCode.setValue(this.currUser.branch, { emitEvent: false });
      this.form.controls.regionCode.setValue(branchLv2?.khuVucM, { emitEvent: changeRegionCode });
    }

    if (isRegion) {
      this.setBranchLv1List({ setLv2List: false });
      this.setBranchLv2List({ alreadyPick: true });
    }
  }

  getRevenueParams(params) {
    const dataForm = this.form.getRawValue();
    const revenueParams = {
      ...{ ...params },
      rsId: this.objFunction?.rsId,
      scope: Scopes.VIEW,
    }
    if (!revenueParams.regionCode) revenueParams.regionCode = '';

    if (
      dataForm.isRegion &&
      (
        (revenueParams.regionCode && !revenueParams.branchCode) ||
        (dataForm.branchCodeLv1 && !revenueParams.branchCode)
      )
    ) {
      revenueParams.branchCode = this.listBranch.filter(branch => branch.code).map(branch => branch.code).join(',');
    }
    return revenueParams;
  }

  setOptionAllToBrLv2(): void {
    const haveAllOption = this.listBranch.find(branch => branch.code === '');
    if (this.listBranch.length > 1 && !haveAllOption) {
      this.listBranch.unshift({ code: '', displayName: 'Tất cả' });
    }
  }

  get isSME(): boolean {
    return this.form.get('divisionCode').value === Division.SME;
  }


  getDataV2(type: string, divisionCode: string) {
    // v2
    if (divisionCode === CustomerType.INDIV) {
      if (type === DashboardType.INDIV_BUSINESS) {
        this.mapDataComponentIndivBusiness()
      } else if (type === DashboardType.INDIV_CUSTOMER) {
        this.mapDataComponentIndivCustomer();
      } else if (type === DashboardType.INDIV_CARD) {
        this.mapDataComponentIndivCard();
      }else if (type === DashboardType.INDIV_CAMPAIGN) {
        this.mapDataComponentIndivCampaign();
      }
    } else {
      this.getData(type, divisionCode).subscribe((dataChart) => {
        this.mapDataComponent(dataChart, divisionCode, type);
      });
    }
  }

  mapDataComponentIndivCampaign() {
    const data = this.form.getRawValue();

    const params = {
      branchCodes: [],
      hrsCode: this.hrsCode,
      campaignId: this.form.get('campaigns').value,
      branchCode: this.form.get('branchCode').value === "" ? "ALL" : this.form.get('branchCode').value,
      campaignList: this.listCampaign,
      isbranchDimention: this.isbranchDimention,
      regionCode: this.form.get('regionCode').value,
      isRm: this.isRm,
      listBranchCode: [],
      rmCode: this.form.get('rmCode').value
    }

    if (data.branchCode === '') {
      params.branchCodes = ['ALL'];
      params.listBranchCode = this.allBranchLv2
                                  .filter(branch => branch.khuVucM === this.form.get('regionCode').value)
                                  .map(branch => branch.code);
    } else {
      params.branchCodes = !data.branchCode ? this.allBranchLv2.map(branch => branch.code) : [data.branchCode]
    }

    this.dashboard = [
      {
        cols: 1,
        rows: 5,
        y: 0,
        x: 0,
        component: ChartCampaignResultOverviewComponent,
        data: [],
        input: params
      },
      {
        cols: 1,
        rows: 5,
        y: 0,
        x: 0,
        component: ChartCampaignStageOverviewComponent,
        data: [],
        input: params
      }
    ]
    this.isLoading = false;

  }

  mapDataComponentIndivBusiness() {
    this.isLoading = false;
    this.showEmbedded = true;
    const data = this.form.getRawValue();

    if (this.isHO && !data.branchCode) {
      this.branchFilter = 'ALL';
    } else {
      this.branchFilter = !data.branchCode ? 'ALL' : data.branchCode
    }
    this.isLoading = false;
  }

  mapDataComponentIndivCustomer() {

    const data = this.form.getRawValue();
    const params = {
      branchCodes: []
    }

    if (this.isHO && !data.branchCode) {
      params.branchCodes = ['ALL'];
    } else {
      params.branchCodes = !data.branchCode ? this.allBranchLv2.map(branch => branch.code) : [data.branchCode]
    }

    this.dashboard = [
      // {
      //   cols: 2,
      //   rows: 3,
      //   y: 0,
      //   x: 0,
      //   component: CustomerSegmentChartComponent,
      //   data: [],
      // },
      // {
      //   cols: 2,
      //   rows: 3,
      //   y: 0,
      //   x: 0,
      //   component: CustomerPrivatePriorityChartComponent,
      //   data: [],
      // },
      // {
      //   cols: 2,
      //   rows: 3,
      //   y: 0,
      //   x: 0,
      //   component: CustomerProductUsedChartComponent,
      //   data: [],
      // },
      // {
      //   cols: 2,
      //   rows: 3,
      //   y: 0,
      //   x: 0,
      //   component: CustomerStatusChartComponent,
      //   data: [],
      // },
      // {
      //   cols: 2,
      //   rows: 3,
      //   y: 0,
      //   x: 0,
      //   component: CustomerBscoreChartComponent,
      //   data: [],
      // },
      // {
      //   cols: 2,
      //   rows: 3,
      //   y: 0,
      //   x: 0,
      //   component: CustomerToiChartComponent,
      //   data: [],
      // },
      {
        cols: 3,
        rows: 5,
        y: 0,
        x: 0,
        component: CustomerOverviewChartComponent,
        data: [],
        input: params
      },
      {
        cols: 3,
        rows: 5,
        y: 0,
        x: 0,
        component: BCoreChartComponent,
        data: [],
        input: params
      },
      {
        cols: 6,
        rows: 4,
        y: 0,
        x: 0,
        component: SpdvChartComponent,
        data: [],
        input: params
      },
      {
        cols: 3,
        rows: 4,
        y: 0,
        x: 0,
        component: CustomerActiveAppChartComponent,
        data: [],
        input: params
      },
    ];
    this.isLoading = false;
  }

  mapDataComponentIndivCard() {
    if (_.isEmpty(this.currUser.code)) {
      this.dashboard = [
        {
          cols: 1,
          rows: 5,
          y: 0,
          x: 0,
          component: CardChartComponent,
          data: {
            branchCodes: this.facilities?.length === this.facilityCode?.length ? '' : this.facilityCode,
          },
        },
        {
          component: CardSpendingComponent,
          cols: 1,
          rows: 5,
          y: 0,
          x: 0,
          data: {
            branchCodes: this.facilities?.length === this.facilityCode?.length ? 'ALL' : this.facilityCode,
            isRm: this.isRm
          },
        },
      ];
    } else {
      this.dashboard = [
        {
          cols: 1,
          rows: 5,
          y: 0,
          x: 0,
          component: CardChartComponent,
          data: {
            branchCodes: this.facilityCode
          },
        },
        {
          component: CardSpendingComponent,
          cols: 1,
          rows: 5,
          y: 0,
          x: 0,
          data: {
            branchCodes: this.facilities?.length === this.facilityCode?.length ? 'ALL' : this.facilityCode,
            isRm: this.isRm
          },
        }
      ];
    }
    this.isLoading = false;
  }

  changeBranch(event) {
    // this.viewChange.emit(event);
    this.onChangeForm();
  }

  onShowLoading($event: any) {
    if ($event)
    {
      this.isLoading = true
    } else {
      this.isLoading = false
    }
  }

  isShowDashboardFilter() : boolean {
    return this.form.get('type').value === 'BUSINESS_DASHBOARD'
    || this.form.get('type').value === 'REVENUE_DASHBOARD'
    || this.form.get('type').value === 'CUSTOMER_DASHBOARD'
    || this.form.get('type').value === DashboardType.INDIV_CAMPAIGN;
  }

  isShowBranchLevel1(): boolean {
    return this.form.get('isRegion').value
      && this.form.get('type').value !== DashboardType.INDIV_CAMPAIGN;
  }

  isShowRMdroplist() :boolean {
    return !this.form.get('isRegion').value && !this.isRm;
  }

  isShowCampaigndroplist() : boolean {
    return this.form.get('type').value === DashboardType.INDIV_CAMPAIGN;
  }

  isShowPinButton() : boolean {
    return this.form.get('type').value !== DashboardType.INDIV_CAMPAIGN;
  }

  buildParamsGetCampagin(isRm: boolean) : any {
    const isRegion = this.form.get('isRegion').value;
    const rmConditional = !this.isRm && isRegion ===  false;
    const branchConditional = !this.isRm && isRegion ===  true;
    const selectedBranchCode = [this.form.get('branchCode').value];

    if(!this.isRm && isRegion ===  false) {
      const selectedRMCode = this.form.get('rmCode').value;
      this.rmApi.getDetailRMByRmCode(selectedRMCode)
        .pipe(catchError(()=>(of(undefined))))
        .subscribe((rmData) => {
          this.hrsCode = rmData.hrsCode;
        })
    }

    return {
      isRm: isRm,
      isCampaignActive: true,
      branchCodes: branchConditional ? selectedBranchCode : null,
      isRmScr: isRm,
      customerType: "INDIV",
      size: 200,
      page: 0,
      rsId: this.objFunction?.rsId,
      hrsCode: rmConditional ? this.hrsCode : "",
      scope: Scopes.VIEW
    }
  }

  getCampaignList(){
    const data = this.form.getRawValue();
    const isRegion = this.form.get('isRegion').value;
    const rmConditional = !this.isRm && isRegion ===  false;
    const selectedBranchCode = this.form.get('branchCode').value;

    var params = {};
    this.isLoading = true;
    this.listCampaign = [];

    if (!isRegion) {
      // RM dimension
      if(!this.isRm) {
        // CBQL role or admin
        const selectedRMCode = this.form.get('rmCode').value;
        if (selectedRMCode !== "") {
          this.rmApi.getDetailRMByRmCode(selectedRMCode)
          .pipe(catchError(async () => (this.isLoading = false)))
          .subscribe((rmData) => {
            this.hrsCode = rmData.hrsCode;

            params = {
              isRm: true,
              isCampaignActive: true,
              isRmScr: this.isRm,
              customerType: "INDIV",
              status: "ACTIVATED",
              size: 200,
              page: 0,
              rsId: this.objFunction?.rsId,
              hrsCode: rmConditional ? rmData.hrsCode : "",
              scope: Scopes.VIEW
            }

            this._getCampaignList(params, data);
          });
        } else {
          this.isLoading = false;
        }
      } else {
        // CBQL role or admin
        this.hrsCode = null;
        params = {
          isRm: true,
          isCampaignActive: true,
          status: "ACTIVATED",
          size: 200,
          page: 0,
          rsId: this.objFunction?.rsId,
          scope: Scopes.VIEW
        }
        this._getCampaignList(params, data);
    }

    } else {
      // region dimension
        const params = {
          isRm: this.isRm,
          isCampaignActive: true,
          branchCodes: selectedBranchCode !== "" ? [selectedBranchCode] : "",
          isRmScr: this.isRm,
          customerType: "INDIV",
          status: "ACTIVATED",
          size: 200,
          page: 0,
          rsId: this.objFunction?.rsId,
          scope: Scopes.VIEW
        }

        if (selectedBranchCode === "") {
          params.branchCodes = this.listBranch.filter(data => data.code !== "").map(data => data.code);
        }

        this._getCampaignList(params, data);

    }
  }


  private _getCampaignList(params, formData) {

    this.campaignService.searchForDashboard(params)
    .pipe(finalize(()=>{
      this.isLoading = false;
    }))
    .subscribe((campaigns) => {
      this.listCampaign = _.map(campaigns.content, (item) => {
        return {
          code: item.id,
          name: item.name,
        };
      });
      this.isLoading = false;
      this.form.get('campaigns').setValue(_.head(this.listCampaign)?.code, { emitEvent: false });
      this.getDataV2(formData.type, formData.divisionCode);
    });
  }
}
