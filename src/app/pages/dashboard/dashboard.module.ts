import { TableModule } from 'primeng/table';
import { DashboardLeftViewComponent } from './components/dashboard-left-view/dashboard-left-view.component';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardViewComponent } from './containers/dashboard-view/dashboard-view.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { GridsterModule } from 'angular-gridster2';
import { DynamicModule } from 'ng-dynamic-component';
import { LeadStatisticsComponent } from './components/lead-statistics/lead-statistics.component';
import { ActivityCallSuccessComponent } from './components/activity-call-success/activity-call-success.component';
import { OpportunityWonReportComponent } from './components/opportunity-won-report/opportunity-won-report.component';
import { ActivityWeeklyReportComponent } from './components/activity-weekly-report/activity-weekly-report.component';
import { ActivityUserRankComponent } from './components/activity-user-rank/activity-user-rank.component';
import { DashboardEditModalComponent } from './components/dashboard-edit-modal/dashboard-edit-modal.component';
import { DashboardAddComponentModalComponent } from './components/dashboard-add-component-modal/dashboard-add-component-modal.component';
import { CreditTopChartComponent } from './components/credit-top-chart/credit-top-chart.component';
import { RaiseCapitalTopChartComponent } from './components/raise-capital-top-chart/raise-capital-top-chart.component';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxEchartsModule } from 'ngx-echarts';
import * as echarts from 'echarts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MenuModule } from 'primeng/menu';
import { CustomerCommonChartComponent } from './components/customer-common-chart/customer-common-chart.component';
import { DashboardBusinessChartSMEComponent } from './components/dashboard-business-chart-sme/dashboard-business-chart-sme.component';
import { RadioButtonModule } from 'primeng/radiobutton';
import { CalendarModule } from 'primeng/calendar';
import {CardChartComponent} from './components/dashboard-card-indiv-chart/card-chart.component';
import {CustomerOverviewChartComponent} from './components/dashboard-customer-overview-indiv-chart/customer-overview-chart.component';
import {DashboardManageCardComponent} from './components/manage-card/dashboard-manage-card/dashboard-manage-card.component';
import {CardSpendingComponent} from './components/card-spending/card-spending.component';
import {BCoreChartComponent} from './components/dashboard-b-core-indiv-chart/b-core-chart.component';
import {SpdvChartComponent} from './components/dashboard-spdv-chart/spdv-chart.component';
import { CustomerFunnelChartComponent } from './components/customer-funnel-chart/customer-funnel-chart.component';
import { CustomerStatisticsComponent } from './components/customer-statistics/customer-statistics.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { CasaVolatilityComponent } from './components/casa-volatility/casa-volatility.component';
import { InvBussinessComponent } from './components/dashboard-inv-bussiness/inv-bussiness.component';
import { AppActiveChartDetailComponent, CustomerActiveAppChartComponent } from './components';
import { ChartCampaignResultOverviewComponent } from './components/chart-campaign-result-overview/chart-campaign-result-overview.component';
import { ChartCampaignResultDetailComponent } from './components/chart-campaign-result-detail/chart-campaign-result-detail.component';
import { ChartCampaignStageOverviewComponent } from './components/chart-campaign-stage-overview/chart-campaign-stage-overview.component';
import {ChartCampaignStageDetailComponent} from "./components/chart-campaign-stage-detail/chart-campaign-stage-detail";
import {DashboardMobilizeIndivComponent} from "./components/dashboard-mobilize-indiv/dashboard-mobilize-indiv.component";
import { GuaranteeComponent } from './components/dashboard-sme-business/guarantee/guarantee/guarantee.component';
import { HDVComponent } from './components/dashboard-sme-business/HDV/HDV/HDV.component';
import { DebtComponent } from './components/dashboard-sme-business/debt/debt/debt.component';
import { TableComponent } from './components/dashboard-sme-business/table/table/table.component';

@NgModule({
  declarations: [
    DashboardViewComponent,
    DashboardLeftViewComponent,
    LeadStatisticsComponent,
    ActivityCallSuccessComponent,
    OpportunityWonReportComponent,
    ActivityWeeklyReportComponent,
    ActivityUserRankComponent,
    DashboardEditModalComponent,
    DashboardAddComponentModalComponent,
    CreditTopChartComponent,
    RaiseCapitalTopChartComponent,
    CustomerCommonChartComponent,
    DashboardBusinessChartSMEComponent,
    CustomerOverviewChartComponent,
    BCoreChartComponent,
    SpdvChartComponent,
    DashboardManageCardComponent,
    CardChartComponent,
    CardSpendingComponent,
    CustomerStatisticsComponent,
    CustomerFunnelChartComponent,
    CasaVolatilityComponent,
    InvBussinessComponent,
    CustomerActiveAppChartComponent,
    AppActiveChartDetailComponent,
    ChartCampaignResultOverviewComponent,
    ChartCampaignResultDetailComponent,
    ChartCampaignStageOverviewComponent,
    ChartCampaignStageDetailComponent,
    DashboardMobilizeIndivComponent,
    GuaranteeComponent,
    HDVComponent,
    DebtComponent,
    TableComponent
  ],
  imports: [
    CommonModule,
    MatDatepickerModule,
    DashboardRoutingModule,
    MatNativeDateModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts,
    }),
    NgbModule,
    NgxDatatableModule,
    TranslateModule,
    GridsterModule,
    DynamicModule,
    DropdownModule,
    ButtonModule,
    MenuModule,
    RadioButtonModule,
    CalendarModule,
    TableModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class DashboardViewModule {}
