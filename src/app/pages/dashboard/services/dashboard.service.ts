import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  constructor(private http: HttpClient) {}

  getActivity(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_activity}/activities`, { params });
  }

  getActivityCallsByRole(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_activity}/activities/userCalled`);
  }

  getActivityCallsByTeam(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_activity}/activities/teamRate`);
  }

  getActivityReportChart(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_activity}/activities/reports/charts`);
  }

  getActivityReport(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_activity}/activities/reports`);
  }

  getOpportunityReport(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/opportunities/getStatistic`);
  }

  getSlideBarData(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/leads/getSlideBarData?type=INSIGHT`);
  }

  getDataTopRaiseCapitalCustomer(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_report}/dashboard/mobilizationTop10`, { params });
  }

  getDataTopCreditCustomer(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_report}/dashboard/creditTop10`, { params });
  }

  getConfigDefaultDashboard(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_report}/dashboard/dashboardView`, { responseType: 'text' });
  }

  updateConfigDefaultDashboard(value: string): Observable<any> {
    return this.http.post(`${environment.url_endpoint_report}/dashboard/pinDashboard?code=${value}`, {});
  }

  getDataChartsCustomerOtherIndiv(params: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_report}/dashboard/viewSMEDashBoard`, { params });
  }

  getDataChartsBusinessOtherIndiv(params: any, type: boolean): Observable<any> {
    const view = type ? 'viewSMEBusinessDashBoard' : 'viewSMERevenueDashBoard';
    return this.http.get(
      `${environment.url_endpoint_report}/dashboard/${view}`,
      { params }
    );
  }

  getListRmByBranch(branchCode: string, divisionCode: string): Observable<any> {
    return this.http
      .get(
        `${environment.url_endpoint_rm}/employees/findAllByBranchAndBlockCode?branchCode=${branchCode}&blockCode=${divisionCode}`
      )
      .pipe(catchError(() => of([])));
  }

  getBranchByDomain(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/branches/getBranchByDomain`, { params });
  }

  getAllRMByBranches(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/employees/findRMInBranches`, params);
  }

  getDataChartsOverviewCustomer(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_report}/dashboard/overview-chart`, params);
  }
  getDataChartsBscore(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_report}/dashboard/bscore-chart`, params );
  }
  getDataChartsSPDV(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_report}/dashboard/spdv-chart`, params);
  }
  getDataChartsCreditCard(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_report}/dashboard/credit-card-chart`, { params });
  }

  getChartTopCustomer(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_report}/dashboard/chart-top-customer`, { params });
  }

  getChartTopCustomerCbql(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_report}/dashboard/chart-top-customer-cbql`, { params });
  }
  getDataChartsCreditCardDetail(params): Observable<any>{
    return this.http.get(`${environment.url_endpoint_report}/dashboard/credit-card-detail`,{params});
  }
  exportReportManageCard(params) : Observable<any>{
    return this.http.post(`${environment.url_endpoint_report}/dashboard/export-credit-card`, params);
  }

  getCstProfileInfo(params){
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/get-cst-profile-info`, { params:  params});
  }

  getDigitalTransform(params){
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/get-digital-transform`, { params:  params});
  }

  getDataChartsDashboardAppDetail(params, isHO = false): Observable<any>{
    const { page, size } = params
    return this.http.post(`${environment.url_endpoint_report}/dashboard/app-detail-chart?page=${page + 1}&size=${size}`, params);
  }

  getAppActiveDataChart(params, isHO = false): Observable<any> {
    return this.http.post(`${environment.url_endpoint_report}/dashboard/app-summary-chart`, params);
  }

  businessDashboardHdvIndiv(body){
    return this.http.post(`${environment.url_endpoint_report}/dashboard/business-dashboard-hdv-duno-indiv`,body);
  }
  dashboardHdDunoRmDetail(body){
    return this.http.post(`${environment.url_endpoint_report}/dashboard/dashboard-hdv-duno-rm-detail`,body);
  }
  // lấy thông tin bảo lãnh SME
  smeBusinessMdDashboard(body){
    return this.http.post(`${environment.url_endpoint_report}/dashboard/sme-business-md-dashboard`, body);
  }

  // lấy thông tin dư nợ SME
  smeBusinessLoanDashboard(body): Observable<any>{
    return this.http.post(`${environment.url_endpoint_report}/dashboard/sme-business-loan-dashboard`, body);
  }

  // lấy thông tin huy động vốn SME
  smeBusinessDepoDashboard(body){
    return this.http.post(`${environment.url_endpoint_report}/dashboard/sme-business-depo-dashboard`, body);
  }

  // lấy thông tin tổng quan bảo lãnh SME
  businessDashboardTopMd(body){
    return this.http.post(`${environment.url_endpoint_report}/dashboard/business-dashboard-top-md`, body);
  }

  // lấy thông tin tổng quan tín dụng
  businessDashboardTopLoan(body){
    return this.http.post(`${environment.url_endpoint_report}/dashboard/business-dashboard-top-loan`, body);
  }

  // lấy thông tin tổng quan huy động vốn
  businessDashboardTopDepo(body){
    return this.http.post(`${environment.url_endpoint_report}/dashboard/business-dashboard-top-depo`, body);
  }



}
