export class ImformationChartModel {
  stt: string;
  rmCode: string;
  branchCodeCap2: string;
  customerCode: string;
  customerName: string;
  balPre: number;
  bal: number;
  balChange: number;
  transactionDate: string;
  transactionTime: string;
}
