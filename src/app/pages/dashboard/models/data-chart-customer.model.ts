import { ChartCustomerModel } from './chart-customer.model';

export interface DataChartCustomerModel {
  title: string;
  data: ChartCustomerModel[];
  order?: number;
}
