import { ImformationChartModel } from './imformation-chart.model';

export interface ChartCustomerModel {
  label: string;
  code: string;
  value: number;
  color: string;
  orderNum?: number;
}

export class FetchChart {
  request: ChartRequest;
  data: ImformationChartModel[];
  loading: boolean;
  max: number;
  maxBalChange: number;
  tranDate: string;
}
export class ChartRequest {
  report_id: string;
  fluctype: string;
  datatype: string;
  limit: number;
  branchcodecap2?: string;
}
export class ChartContainer {
  [x: string]: FetchChart;
}
