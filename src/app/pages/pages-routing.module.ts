import { global } from '@angular/compiler/src/util';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Routes, RouterModule, Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { AccessDeniedComponent } from '../shared/components/access-denied/access-denied.component';
import { PageNotFoundComponent } from '../shared/components/page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
  },
  {
    path: 'tasks',
    loadChildren: () => import('./tasks/tasks.module').then((m) => m.TasksViewModule),
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then((m) => m.DashboardViewModule),
  },
  {
    path: 'campaigns',
    loadChildren: () => import('./campaigns/campaigns.module').then((m) => m.CampaignsViewModule),
  },
  {
    path: 'calendar',
    loadChildren: () => import('./calendar/calendar.module').then((m) => m.CalendarViewModule),
  },
  {
    path: 'calendarSme',
    loadChildren: () => import('./calendar-sme/calendar.module').then((m) => m.CalendarViewModule),
  },
  {
    path: 'system',
    loadChildren: () => import('./system/system.module').then((m) => m.SystemViewModule),
  },
  {
    path: 'rm360',
    loadChildren: () => import('./rm/rm.module').then((x) => x.RmModule),
  },
  {
    path: 'customer360',
    loadChildren: () => import('./customer-360/customer-360.module').then((x) => x.Customer360Module),
  },
  {
    path: 'lead',
    loadChildren: () => import('./lead/lead.module').then((x) => x.LeadModule),
  },
  {
    path: 'kpis',
    loadChildren: () => import('./kpis/kpis.module').then((x) => x.KpisModule),
  },
  {
    path: 'report-management',
    loadChildren: () => import('./report/report.module').then((x) => x.ReportModule),
  },
  {
    path: 'device-smart',
    loadChildren: () => import('./device/device-smart.module').then((x) => x.DeviceSmartModule),
  },
  {
    path: 'migrate',
    loadChildren: () => import('./migrate/migrate.module').then((x) => x.MigrateModule),
  },
  {
    path: 'tickets',
    loadChildren: () => import('./ticket/ticket.module').then((x) => x.TicketModule),
  },
  {
    path: 'marketing',
    loadChildren: () => import('./marketing/marketing.module').then((x) => x.MarketingModule),
  },
  {
    path: 'salekit',
    loadChildren: () => import('./salekit/salekit.module').then((x) => x.SalekitModule),
  },
  {
    path: 'sale-manager',
    loadChildren: () => import('./sale-manager/sale-manager.module').then((x) => x.SaleManagerModule),
  },
  {
    path: 'customer-plan',
    loadChildren: () => import('./planing/customer-plan.module').then((x) => x.CustomerPlanModule),
  },
  {
    path: 'coaching',
    loadChildren: () => import('./coaching/coaching.module').then((x) => x.CoachingModule),
  },
  {
    path: 'support-rm',
    loadChildren: () => import('./support-rm/support-rm.module').then((x) => x.SupportRmModule),
  },
  {
    path: 'warning',
    loadChildren: () => import('./warning/warning.module').then((x) => x.WarningModule),
  },
  {
    path: 'embed',
    loadChildren: () => import('./embed/embed.module').then((m) => m.EmbedModule),
  },
  {
    path: 'extensions',
    loadChildren: () => import('./extensions/extensions.module').then((x) => x.ExtensionsModule),
  },
  {
    path: 'page-not-found',
    component: PageNotFoundComponent,
  },
  {
    path: 'access-denied',
    component: AccessDeniedComponent,
  },
  { path: '**', redirectTo: 'dashboard', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class PagesRoutingModule {
  currentUrl: string;
  constructor(private router: Router) {
    this.router.events.pipe(filter((event) => event instanceof NavigationEnd)).subscribe((event: NavigationEnd) => {
      global.previousUrl = this.currentUrl;
      this.currentUrl = event.url;
    });
  }
}
