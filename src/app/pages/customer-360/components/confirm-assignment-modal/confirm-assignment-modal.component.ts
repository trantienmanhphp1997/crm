import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CommonCategory } from 'src/app/core/utils/common-constants';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-confirm-assignment-modal',
  templateUrl: './confirm-assignment-modal.component.html',
  styleUrls: ['./confirm-assignment-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ConfirmAssignmentModalComponent extends BaseComponent implements OnInit{
  isApproved: boolean;
  content = '';
  form = this.fb.group({
    content: ['', CustomValidators.required],
    exceptionReason: [null],
  });
  maxlength = 400;
  isSubmit = false;
  exceptionReasons = [];

  constructor(
    injector: Injector, 
    private modalActive: NgbActiveModal,
  ) {
    super(injector);
  }

  ngOnInit(): void {
    if (this.isApproved) {
      this.form.get('exceptionReason').setValidators(Validators.required);
    }
    
    this.getExceptionReasonList();
  }

  closeModal() {
    const timer = setTimeout(() => {
      this.modalActive.close(false);
      clearTimeout(timer);
    }, 100);
  }

  save() {
    this.isSubmit = true;
    if (!this.isApproved && !this.form.valid) {
      this.form.controls.content.markAsTouched({ onlySelf: true });
      return;
    }
    this.modalActive.close({ 
      continue: true, 
      note: this.form.controls.content.value.trim(),
      ...this.isApproved ? { exceptionReason: this.form.controls.exceptionReason.value } : {}
    });
  }


  // p-inputwrapper-focus is change
  // rm bug
  onFocus(){
    this.ref.detectChanges();
  }

  getExceptionReasonList() {
    this.commonService.getCommonCategory(CommonCategory.CUSTOMERS_ASSIGNMENT_EXCEPTION_REASON).subscribe(data => {
      this.exceptionReasons = data.content || [];
      if (this.exceptionReasons.length) {
        this.form.get('exceptionReason').setValue(this.exceptionReasons[0].code);
      }
    })
  }
}
