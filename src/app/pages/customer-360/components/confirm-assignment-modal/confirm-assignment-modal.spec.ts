import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ConfirmAssignmentModalComponent } from './confirm-assignment-modal.component';

describe('ConfirmAssignmentModalComponent', () => {
  let component: ConfirmAssignmentModalComponent;
  let fixture: ComponentFixture<ConfirmAssignmentModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConfirmAssignmentModalComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmAssignmentModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
