import {
  Component,
  EventEmitter,
  ElementRef,
  Input,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation,
  OnChanges,
} from '@angular/core';
import { global } from '@angular/compiler/src/util';
import _ from 'lodash';
import { formatNumber } from '@angular/common';
import { Utils } from 'src/app/core/utils/utils';
import { CustomerApi } from '../../apis';
import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';

@Component({
  selector: 'bidding',
  templateUrl: './bidding.component.html',
  styleUrls: ['./bidding.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class BiddingComponent implements OnInit, OnChanges {
  constructor(private api: CustomerApi, private messageService: NotifyMessageService) { }

  @Input() code: string;
  @Input() registrationNumber: string;
  @ViewChild('biddingTable') biddingTable: ElementRef;
  @Input() loading: boolean;
  @Output() loadingChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() first: boolean;
  @Output() firstChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() notificationMessage: any;
  @Input() fields: any;
  messages = global.messageTable;
  @Input() showData: boolean;
  @Output() showDataChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() data: any;
  @Output() dataChange: EventEmitter<any> = new EventEmitter<any>();
  paramSearch = {
    pageSize: global.userConfig.pageSize,
    pageNo: 0,
  };
  pageable: Pageable;
  prevParams: any = {};
  listData = [];

  ngOnInit(): void {
    if (this.first) {
      if (!this.registrationNumber) {
        return;
      }
      this.getBidding();
    } else {
      this.listData = this.data.listData;
      this.pageable = this.data.pageable;
      this.showData = Utils.isArrayEmpty(this.listData) ? false : true;
    }
  }

  ngOnChanges(e) {
    this.listData = this.data?.listData;
    this.pageable = this.data?.pageable;
    this.showData = Utils.isArrayEmpty(this.listData) ? false : true;
  }

  getBidding() {
    this.loadingChange.emit(true);
    let params: any = {};
    Object.keys(this.paramSearch).forEach((key) => {
      params[key] = this.paramSearch[key];
    });
    params.taxCode = this.registrationNumber;
    this.api.getBidding(params).subscribe(
      (result) => {
        this.listData =
          result?.content?.map(item => {
            item.giatriGoiThau = item.giatriGoiThau ? formatNumber(parseInt(item.giatriGoiThau), 'en') : '--';
            item.loaiDanhSach = item.loaiDanhSach === 'FULL' ? 'Trúng thầu' : 'Dự thầu';
            return item;
          }) || [];
        this.pageable = {
          totalElements: result.totalElements,
          totalPages: result.totalPages,
          currentPage: result.pageable.pageNumber,
          size: global.userConfig.pageSize,
        };
        this.dataChange.emit({ listData: this.listData, pageable: this.pageable });
        this.first = false;
        this.loadingChange.emit(false);
        this.firstChange.emit(this.first);
      },
      () => {
        this.messageService.error(_.get(this.notificationMessage, 'E001'));
        this.loadingChange.emit(false);
      }
    );
  }

  setPage(pageInfo) {
    if (this.loading) {
      return;
    }
    this.paramSearch.pageNo = pageInfo.offset;
    this.getBidding();
  }
}
