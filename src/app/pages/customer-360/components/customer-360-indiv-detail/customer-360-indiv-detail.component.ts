import { forkJoin, of } from 'rxjs';
import { CustomerApi, CustomerDetailApi } from 'src/app/pages/customer-360/apis';
import {
  AgeGroup,
  CommonCategory,
  FunctionCode,
  functionUri,
  Scopes,
  ScreenType,
  SessionKey,
  TaskType,
} from '../../../../core/utils/common-constants';
import { Component, Injector, OnDestroy, OnInit, ViewChild } from '@angular/core';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as moment from 'moment';
import { ExportExcelService } from 'src/app/core/services/export-excel.service';
import { Utils } from '../../../../core/utils/utils';
import { CreateTaskModalComponent } from 'src/app/pages/tasks/components/create-task-modal/create-task-modal.component';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ViewEncapsulation } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { ActivityActionComponent } from 'src/app/shared/components/activity-action/activity-action.component';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { v4 as uuIdv4 } from 'uuid';
import { formatNumber } from '@angular/common';
import { SaleManagerApi } from 'src/app/pages/sale-manager/api/sale-manager.api';
import {AppFunction} from '../../../../core/interfaces/app-function.interface';
import {MbeeChatService} from '../../../../core/services/mbee-chat.service';
import {createGroupChat11} from '../../../../core/utils/mb-chat';

@Component({
  selector: 'app-customer-360-indiv-detail',
  templateUrl: './customer-360-indiv-detail.component.html',
  styleUrls: ['./customer-360-indiv-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class Customer360INDIVDetailComponent extends BaseComponent implements OnInit, OnDestroy {
  constructor(
    injector: Injector,
    private customerApi: CustomerApi,
    private exportExcelService: ExportExcelService,
    private customerDetailApi: CustomerDetailApi,
    private categoryService: CategoryService,
    private saleManagerApi: SaleManagerApi,
    private mbeeChatService: MbeeChatService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.objFunctionMBChat = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.APP_CHAT}`);
    this.scopes = _.get(this.objFunction, 'scopes');
    this.isLoading = true;
    this.format = new Intl.NumberFormat();
    this.code = _.get(this.route.snapshot.params, 'code');
  }

  code: string;
  format: any;
  excel_fields: any;
  excel_title: any;
  titles: Array<any>;
  month: number;
  year: number;
  costs: any;
  hyper_link: string;
  model: any = {};
  emodel: any;
  form = this.fb.group({});
  messageTable: any;
  param: any;
  is_show = false;
  is_edit: boolean;
  is_show_form: boolean;
  index = 0;
  phoneConfig = [];
  emailConfig = [];
  ageGroup: any;
  sector: any;
  today = moment();
  identificationPapers = [];
  listPhone = [];
  listEmail = [];
  tab_index = 0;
  risk_management_tab_index = 0;
  isLoading: boolean;
  title: string;
  show_error: boolean;
  error_mes: string;

  rmManager: string;
  collaterals: Array<any>;
  data_collaterals: Array<any>;
  rows: Array<any> = [];
  search_value: string;
  list_collaterals: Array<any>;
  asset_types: Array<any>;
  asset_groups: Array<any>;
  bscores: Array<any>;
  list_bscores: Array<any>;
  loading_count = 0;
  collaterals_items: Array<any>;
  bscore_items: Array<any>;
  load_coast = false;
  load_bscore = false;
  load_collateral = false;
  is_show_bscore = true;
  listBranchCode = [];
  commonData: any;
  isShowCreateOppBtn = false;
  scopes: Array<any>;

  @ViewChild('tablecollateral') public tablecollateral: DatatableComponent;
  @ViewChild('customerProduct') customerProduct: any;
  @ViewChild('customerActivity') customerActivity: any;

  isShowChat = false;
  objFunctionMBChat: AppFunction;

  ngOnInit() {
    this.isShowChat = this.objFunctionMBChat?.scopes.includes('VIEW');
    this.displayButtons();
    this.commonData = this.sessionService.getSessionData(SessionKey.COMMON_DATA_CUSTOMER_DETAILS);
    if (!this.commonData) {
      forkJoin([
        this.translate.get(['customer360', 'collateralMessage']).pipe(catchError((e) => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.ASSET_TYPE).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.ASSET_GROUP).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.KH_PHONE_NO_CONFIG).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.KH_EMAIL_CONFIG).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.AGE_GROUP).pipe(catchError(() => of(undefined))),
        this.commonService
          .getCommonCategory(CommonCategory.PRIVATE_PRIORITY_CONFIG)
          .pipe(catchError((e) => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.ACCOUNT_GROUP).pipe(catchError(() => of(undefined))),
        this.categoryService
          .getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW)
          .pipe(catchError(() => of(undefined))),
      ]).subscribe(
        ([
          objTranslate,
          asset_types,
          asset_groups,
          phoneConfig,
          emailConfig,
          ageGroup,
          sector,
          accountGroup,
          branchesOfUser,
        ]) => {
          this.commonData = {
            asset_types: asset_types?.content || [],
            asset_groups: asset_groups?.content || [],
            phoneConfig: phoneConfig?.content || [],
            emailConfig: emailConfig?.content || [],
            ageGroup: ageGroup?.content || [],
            sector: sector?.content || [],
            accountGroup: accountGroup?.content || [],
            objTranslate,
            branchesOfUser,
          };
          this.sessionService.setSessionData(SessionKey.COMMON_DATA_CUSTOMER_DETAILS, this.commonData);
          this.mapData();
        }
      );
    }
    forkJoin([
      this.customerDetailApi.get(this.code),
      this.customerApi.getRmManagerByCustomerCode(this.code).pipe(catchError(() => of(undefined))),
      this.customerDetailApi.get(`cost-collection/${this.code}`).pipe(catchError(() => of(undefined))),
      this.customerDetailApi.get(`bscore/${this.code}`).pipe(catchError(() => of(undefined))),
      this.customerDetailApi.get(`collateral/${this.code}`).pipe(catchError(() => of(undefined))),
    ]).subscribe(
      ([itemCustomer, rmManager, costs, bscores, collaterals]) => {
        this.rmManager = rmManager;
        const date = moment().add(-1, 'month');
        this.year = date.year();
        this.model = itemCustomer;
        if (this.model?.customerInfo?.customerGroup) {
          this.model.customerInfo.customerGroupName = this.commonData?.accountGroup?.find(
            (i) => i.code === this.model?.customerInfo?.customerGroup
          )?.name;
        }
        this.costs = costs || {};
        this.bscores = bscores || [];
        this.collaterals_items = collaterals || [];
        this.getIdentificationPapers();
        this.mapData();
        const req = {
          name: 'customer-left-view',
          data: {
            parentType: TaskType.CUSTOMER,
            parentId: _.get(this.model, 'systemInfo.code'),
            view: ScreenType.Detail,
          },
        };
        this.communicateService.request(req);
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.E001);
      }
    );
  }

  mapData() {
    if (this.model && this.commonData) {
      this.excel_fields = _.get(this.commonData, 'objTranslate.customer360.fields');
      this.excel_title = _.get(this.commonData, 'objTranslate.customer360.title');
      this.messageTable = _.get(this.commonData, 'objTranslate.collateralMessage');
      this.asset_types = this.commonData?.asset_types || [];
      this.asset_groups = this.commonData?.asset_groups || [];
      this.phoneConfig = this.commonData?.phoneConfig || [];
      this.emailConfig = this.commonData?.emailConfig || [];
      this.ageGroup = this.commonData?.ageGroup || [];
      this.sector = this.commonData?.sector || [];
      this.listBranchCode = this.commonData?.branchesOfUser?.map((item) => {
        return item.code;
      });
      for (const phone of this.phoneConfig) {
        if (this.model?.customerInfo?.phoneList[phone.value]?.length > 0) {
          this.listPhone.push({
            label: phone.name,
            lstPhone: _.uniqBy(this.model.customerInfo.phoneList[phone.value]),
          });
        }
      }
      for (const email of this.emailConfig) {
        if (this.model?.customerInfo?.emails[email.value]?.length > 0) {
          this.listEmail.push({
            label: email.name,
            lstEmail: _.uniqBy(this.model.customerInfo.emails[email.value]),
          });
        }
      }
      this.title = `${_.get(this.excel_title, 'year')} ${this.year}`;
      const month = moment(_.get(this.costs, 'transactionDate')).format('MM');
      this.title = Utils.isStringNotEmpty(_.get(this.costs, 'transactionDate'))
        ? `${_.get(this.excel_title, 'year')} ${this.year} (${_.get(this.excel_title, 'cost_month')} ${month})`
        : `${_.get(this.excel_title, 'year')} ${this.year}`;
    }
  }

  getAssetType(code: string) {
    return (
      _.get(
        _.chain(this.asset_types)
          .filter((x) => x.code === code)
          .first()
          .value(),
        'name'
      ) || ''
    );
  }

  getAssetGroup(code: string) {
    return (
      _.get(
        _.chain(this.asset_groups)
          .filter((x) => x.code === code)
          .first()
          .value(),
        'name'
      ) || ''
    );
  }

  convert_collateral(datas: Array<any>) {
    const data_collateral_values = [];
    this.data_collaterals = [];
    if (Utils.isArrayNotEmpty(datas)) {
      datas = _.orderBy(datas, ['matDt'], ['asc', 'desc']);
      const values = _.chain(datas)
        .groupBy((x) => x.cltId)
        .map((item) => item)
        .value();
      if (Utils.isArrayNotEmpty(values)) {
        _.forEach(values, (x) => {
          const parent = _.chain(x).first().value();
          const newParent = {
            arId: '',
            balFcy: _.get(parent, 'balFcy'),
            cltCd: _.get(parent, 'cltCd'),
            cltId: _.get(parent, 'cltId'),
            cltType: _.get(parent, 'cltType'),
            assetType: _.get(parent, 'assetType'),
            assetGroup: _.get(parent, 'assetGroup'),
            currency: _.get(parent, 'currency'),
            description: _.get(parent, 'description'),
            dsnapShotDt: '',
            matDt: '',
            id: `${_.get(parent, 'id')}-${moment().format('DD-MM-YYYY')}`,
            percent: '',
          };
          const v = _.map(x, (y) => ({ ...y, parentCode: _.get(newParent, 'id'), description: '', id: uuIdv4() }));
          data_collateral_values.push(newParent);
          data_collateral_values.push(v);
        });
        this.data_collaterals = _.flattenDeep(data_collateral_values);
      } else {
        this.data_collaterals = [];
      }
    }
    this.convert_to_rows(this.data_collaterals);
  }

  convert_to_rows(datas: Array<any>) {
    let listChildren: Array<any>;
    this.rows = [];
    if (Utils.isArrayNotEmpty(datas)) {
      datas.forEach((item) => {
        if (item.parentCode === item.id) {
          delete item.parentCode;
          listChildren = datas.filter((value) => {
            return value.parentCode === item.id;
          });
        } else {
          listChildren = datas.filter((value) => {
            return value.parentCode === item.id;
          });
        }
        item.treeStatus = listChildren.length ? 'expanded' : 'disabled';
        this.rows.push(item);
      });
    } else {
      this.rows = [];
    }
  }

  getValue(row, key) {
    return _.get(row, key);
  }

  onTreeAction(event: any) {
    const row = event.row;
    if (row.treeStatus === 'collapsed') {
      row.treeStatus = 'expanded';
    } else {
      row.treeStatus = 'collapsed';
    }
    this.rows = [...this.rows];
  }

  showSideBar() {
    const timer = setTimeout(() => {
      this.customerProduct?.resizeWindow();
      clearTimeout(timer);
    }, 300);
  }

  exportFile() {
    this.translate.get('fields').subscribe((fields) => {
      const fieldLabels = fields;
      const data = [];
      let obj: any = {};
      this.isLoading = true;

      if (Utils.isArrayNotEmpty(this.rows)) {
        _.forEach(this.rows, (x) => {
          obj = {};
          obj[fieldLabels.assetCode] = _.get(x, 'cltId');
          obj[fieldLabels.assetGroup] = _.get(x, 'assetGroup');
          obj[fieldLabels.assetType] = _.get(x, 'assetType');
          obj[fieldLabels.value] = this.format.format(_.get(x, 'balFcy'));
          obj[fieldLabels.currency] = _.get(x, 'currency');
          obj[fieldLabels.contractCode] = _.get(x, 'arId');
          obj[fieldLabels.guaranteedRate] = _.get(x, 'percent');
          obj[fieldLabels.dueDate] = Utils.isStringNotEmpty(_.get(x, 'matDt'))
            ? moment(_.get(x, 'matDt')).format('DD/MM/YYYY')
            : '';
          obj[fieldLabels.description] = _.get(x, 'description');
          data.push(obj);
        });
        this.exportExcelService.exportAsExcelFile(data, 'tsdb');
        this.isLoading = false;
      } else {
        this.isLoading = false;
        this.messageService.warn(this.notificationMessage.noRecord);
      }
    });
    this.isLoading = false;
  }

  edit_form() {
    this.is_edit = !this.is_edit;
    if (this.is_edit) {
      this.emodel = this.model;
    }
  }

  show() {
    this.is_show = true;
  }

  close() {
    this.is_show = false;
  }

  show_form() {
    this.is_show_form = !this.is_show_form;
  }

  collaps() {
    this.is_show = !this.is_show;
  }

  update() {
    this.is_edit = !this.is_edit;
  }

  cancel() {
    this.is_edit = !this.is_edit;
  }

  generateGender(item) {
    const gender: string = _.get(item, 'gender');
    if (gender) {
      return this.fields[gender.toLowerCase()];
    } else {
      return '';
    }
  }

  generateMarialStatus(item) {
    const status: string = _.get(item, 'marialStatus');
    if (status) {
      return this.fields[status.toLowerCase()];
    } else {
      return '';
    }
  }

  generateAgeGroup(item) {
    const birthDay = _.get(item, 'dateOfBirth');
    if (birthDay && birthDay !== '' && this.ageGroup) {
      const age = this.today.diff(moment(birthDay, 'DD/MM/YYYY'), 'years');
      if (age < 18) {
        return (
          this.ageGroup.find((model) => {
            return model.code === AgeGroup.AGE_LESS_18;
          })?.description || ''
        );
      } else if (age > 18 && age <= 25) {
        return (
          this.ageGroup.find((model) => {
            return model.code === AgeGroup.AGE_BETWEEN_18_25;
          })?.description || ''
        );
      } else if (age > 25 && age <= 35) {
        return (
          this.ageGroup.find((model) => {
            return model.code === AgeGroup.AGE_BETWEEN_26_35;
          })?.description || ''
        );
      } else if (age > 35 && age <= 50) {
        return (
          this.ageGroup.find((model) => {
            return model.code === AgeGroup.AGE_BETWEEN_36_50;
          })?.description || ''
        );
      } else if (age > 50 && age <= 60) {
        return (
          this.ageGroup.find((model) => {
            return model.code === AgeGroup.AGE_BETWEEN_51_60;
          })?.description || ''
        );
      } else if (age > 60) {
        return (
          this.ageGroup.find((model) => {
            return model.code === AgeGroup.AGE_BETWEEN_OVER_60;
          })?.description || ''
        );
      } else {
        return '';
      }
    }
  }

  generateRelationshipTimeWithMB(item) {
    const openCodeDate = _.get(item, 'openCodeDate');
    if (openCodeDate && openCodeDate !== '') {
      return this.today.diff(moment(openCodeDate, 'DD/MM/YYYY'), 'months') + ' ' + this.fields.month;
    } else {
      return 0 + ' ' + this.fields.month;
    }
  }

  getIdentificationPapers() {
    let obj: any = {};
    if (
      Utils.isStringNotEmpty(_.get(this.model, 'businessInfo.identifiedNumber')) ||
      Utils.isNotNull(_.get(this.model, 'businessInfo.identifiedIssueDate')) ||
      Utils.isStringNotEmpty(_.get(this.model, 'businessInfo.identifiedIssueArea'))
    ) {
      obj = {};
      obj.number = _.get(this.model, 'businessInfo.identifiedNumber');
      obj.issueDate = _.get(this.model, 'businessInfo.identifiedIssueDate');
      obj.issuePlace = _.get(this.model, 'businessInfo.identifiedIssueArea');
      obj.identificationPaperType = _.get(this.fields, 'cmt');
      this.identificationPapers.push(obj);
    }
  }

  generatePrivateOrPriority(item) {
    const sector = _.get(item, 'sector');
    if (sector) {
      for (const itemSector of this.sector) {
        if (itemSector.value.indexOf(sector) > -1) {
          return itemSector.name;
        }
      }
    }
    return (
      this.sector.find((obj) => {
        return obj.code === 'OTHER';
      }).name || ''
    );
  }

  onTabChanged($event) {
    this.tab_index = _.get($event, 'index');
    if (this.tab_index === 2) {
      if (
        Utils.isNull(_.get(this.costs, 'crmDtttrrI')) &&
        Utils.isNull(_.get(this.costs, 'crmNetCreditCollection')) &&
        Utils.isNull(_.get(this.costs, 'crmDDepositNetMobilization')) &&
        Utils.isNull(_.get(this.costs, 'crmNonInterestIncome'))
      ) {
        this.show_error = true;
        this.error_mes = _.get(this.excel_fields, 'empty_data');
      }
    }
    if (this.tab_index === 3) {
      this.onRiskTabChanged({ index: 0 });
    }
    this.close();
  }

  onRiskTabChanged($event) {
    this.risk_management_tab_index = _.get($event, 'index');
    if (this.risk_management_tab_index === 0) {
      if (!this.load_bscore) {
        this.convert_bscore(this.bscores);
      }
    }
    if (this.risk_management_tab_index === 1) {
      this.isLoading = false;
      this.collaterals = _.map(this.collaterals_items, (x) => ({
        ...x,
        id: `${x.id}-${x.arId}`,
        assetType: this.getAssetType(x.cltType),
        dsnapShotDt: '',
        assetGroup: this.getAssetGroup(x.cltCd),
        value_to_search: `${Utils.parseToEnglish(x.cltId)} - ${Utils.parseToEnglish(
          this.getAssetGroup(x.cltCd)
        )} - ${Utils.parseToEnglish(this.getAssetType(x.cltType))} - ${Utils.parseToEnglish(
          x.balFcy
        )} - ${Utils.parseToEnglish(x.arId)} - ${Utils.parseToEnglish(x.currency)} - ${Utils.parseToEnglish(
          x.description
        )} - ${this.format.format(x.balFcy)}`,
      }));
      this.list_collaterals = [...this.collaterals];
      this.convert_collateral(this.collaterals);
    }
    this.close();
  }

  search() {
    if (Utils.isStringNotEmpty(this.search_value)) {
      this.search_value = Utils.trim(this.search_value);
      const datas = this.list_collaterals.filter((item) => {
        return (
          Utils.trimNullToEmpty(item.value_to_search).toLowerCase().indexOf(Utils.parseToEnglish(this.search_value)) !==
          -1
        );
      });
      this.convert_collateral(datas);
    } else {
      this.convert_collateral(this.collaterals);
    }
  }

  createTaskTodo() {
    const data: any = {};
    data.parentId = this.model?.systemInfo?.code;
    data.parentName = this.model?.customerInfo?.fullName;
    data.parentType = TaskType.CUSTOMER;
    const taskModal = this.modalService.open(CreateTaskModalComponent, {
      windowClass: 'create-task-modal',
      scrollable: true,
    });
    taskModal.componentInstance.data = data;
  }

  createActivity() {
    const parent: any = {};
    parent.parentType = TaskType.CUSTOMER;
    parent.parentId = _.get(this.model, 'systemInfo.code');
    parent.parentName = _.get(this.model, 'customerInfo.fullName');
    const modal = this.modalService.open(ActivityActionComponent, {
      windowClass: 'create-activity-modal',
      scrollable: true,
    });
    modal.componentInstance.parent = parent;
    modal.componentInstance.type = ScreenType.Create;
    modal.componentInstance.isShowFuture = true;
  }

  showLoading(isLoading) {
    this.isLoading = isLoading;
    this.ref.detectChanges();
  }

  export() {
    if (!this.show_error) {
      this.isLoading = true;
      const values = [];
      const datas = [];
      values.push({ name: _.get(this.excel_fields, 'crmDtttrrI'), value: _.get(this.costs, 'crmDtttrrI') });
      values.push({
        name: _.get(this.excel_fields, 'crmNetCreditCollection'),
        value: _.get(this.costs, 'crmNetCreditCollection'),
      });
      values.push({
        name: _.get(this.excel_fields, 'crmDDepositNetMobilization'),
        value: _.get(this.costs, 'crmDDepositNetMobilization'),
      });
      values.push({
        name: _.get(this.excel_fields, 'crmNonInterestIncome'),
        value: _.get(this.costs, 'crmNonInterestIncome'),
      });
      _.forEach(values, (value) => {
        const obj = {};
        obj[_.get(this.excel_title, 'target')] = _.get(value, 'name');
        obj[`${this.title}`] = _.get(value, 'value');
        datas.push(obj);
      });
      this.exportExcelService.exportAsExcelFile(datas, 'cost');
      this.isLoading = false;
    } else {
      this.messageService.warn(_.get(this.excel_fields, 'empty_data'));
    }
  }

  ngOnDestroy() {
    this.removeCusId();
    const req = {
      name: 'customer-left-view',
      data: {
        parentType: TaskType.CUSTOMER,
        parentCustomerId: '',
      },
    };
    this.communicateService.request(req);
  }

  removeCusId() {
    localStorage.setItem("DETAIL_CUS_ID", JSON.stringify(''));
  }

  convert_bscore(datas) {
    const values = _.chain(datas)
      .groupBy((x) => x.monthValue)
      .map((item) => item)
      .value();
    this.list_bscores = [];
    if (Utils.isArrayNotEmpty(values)) {
      _.forEach(values, (x) => {
        this.list_bscores.push({
          monthValue: _.get(_.chain(x).first().value(), 'monthValue'),
          month: moment(_.get(_.chain(x).first().value(), 'monthValue')).format('MM/YYYY'),
          assets: _.map(x, (x) => x.pdNm),
          customer_rating: _.get(_.chain(x).first().value(), 'cstLvlCurr'),
          asset_ratings: _.map(x, (x) => x.pdLvl),
          value_size: _.size(x),
        });
      });
      this.list_bscores = _.orderBy(this.list_bscores, ['monthValue'], ['desc', 'asc']);
      this.is_show_bscore = true;
    } else {
      this.is_show_bscore = false;
    }
  }

  getPayroll(model, key) {
    const value = _.get(model, key);
    switch (value) {
      case 'N':
        return _.get(this.fields, 'n');
      case 'Y':
        return _.get(this.fields, 'yes');
      case 'n':
        return _.get(this.fields, 'n');
      case 'y':
        return _.get(this.fields, 'yes');
      default:
        return '';
    }
  }

  getCustomerDetail(value: string, url: string, disabled: boolean = false) {
    this.customerDetailApi.get(`${url}`).subscribe(
      (response) => {
        if (!disabled) {
          this.isLoading = false;
        }
        this[value] = response;
      },
      () => {
        this.isLoading = false;
        this[value] = {};
      }
    );
  }

  getCostValue(data, key) {
    return Utils.isNull(_.get(data, key)) ? '---' : formatNumber(_.get(data, key), 'en', '0.0-2');
  }

  mailTo() {
    const url = `mailto:${this.model?.customerInfo?.emailDisplay}`;
    window.location.href = url;
  }

  createOppKHCN() {
    const url = this.router.serializeUrl(
      this.router.createUrlTree([functionUri.selling_opp_indiv, 'detail-v2'], {
        skipLocationChange: true,
        queryParams: {
          customerCode: this.code,
          code: null,
        }
      })
    );
    window.open(url, '_blank');
  }

  checkScopes(codes: Array<any>) {
    if (Utils.isArrayEmpty(codes)) return false;
    return Utils.isArrayNotEmpty(_.filter(this.scopes, (x) => codes.includes(x)));
  }

  checkIsDisableCreateOppBtn() {
    this.isShowCreateOppBtn = this.checkScopes(['CREATE_OPPORTUNITY_SALE']) ? true : false;
  }

  displayButtons() {
    this.checkIsDisableCreateOppBtn();
  }
  // CRMCN-3672 - NAMNH - END

  createGroupChat11() {
    this.isLoading = true;
    this.mbeeChatService.createGroupChat(this.code).subscribe(value => {
      if (value.data) {
        createGroupChat11(value.data);
      } else {
        this.messageService.error(value.message);
      }
      this.isLoading = false;
    }, error => {
      this.messageService.error(JSON.parse(error.error)?.messages.vn);
      this.isLoading = false;
    });

  }
}
