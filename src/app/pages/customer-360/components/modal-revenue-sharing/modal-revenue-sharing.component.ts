import { Component, HostBinding, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { validateAllFormFields } from 'src/app/core/utils/function';

@Component({
  selector: 'modal-revenue-sharing',
  templateUrl: './modal-revenue-sharing.component.html',
  styleUrls: ['./modal-revenue-sharing.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ModalRevenueSharingComponent extends BaseComponent implements OnInit {
  action: string;
  content = '';
  form = this.fb.group({
    content: ['', CustomValidators.required],
  });
  maxlength = 400;
  constructor(injector: Injector, private modalActive: NgbActiveModal) {
    super(injector);
  }

  actionConfig = {
    refuse: {title:'confirmRefuse', btnName: 'btnAction.refuse'},
    approved: {title:'confirmApproved', btnName: 'btnAction.approved'},
    end: {title:'confirmEnd', btnName: 'btnAction.buttonEnd'},
  }

  ngOnInit(): void {}

  closeModal() {
    const timer = setTimeout(() => {
      this.modalActive.close(false);
      clearTimeout(timer);
    }, 100);
  }

  save() {
    if (this.form.valid) {
      this.modalActive.close(this.form.controls.content.value.trim());
    } else {
      validateAllFormFields(this.form);
    }
  }
}
