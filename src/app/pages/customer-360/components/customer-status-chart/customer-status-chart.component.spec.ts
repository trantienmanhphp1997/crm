import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerStatusChartComponent } from './customer-status-chart.component';

describe('CustomerStatusChartComponent', () => {
  let component: CustomerStatusChartComponent;
  let fixture: ComponentFixture<CustomerStatusChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerStatusChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerStatusChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
