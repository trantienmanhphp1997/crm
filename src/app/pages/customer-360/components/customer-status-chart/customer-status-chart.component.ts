import { Component, OnInit, Injector, Input, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import {CustomerType, EChartType, FunctionCode, Scopes} from 'src/app/core/utils/common-constants';
import { percentage } from 'src/app/core/utils/function';
import { EChartsOption } from 'echarts';
import { formatNumber } from '@angular/common';
import {CustomerApi} from "../../apis";

@Component({
  selector: 'app-customer-status-chart',
  templateUrl: './customer-status-chart.component.html',
  styleUrls: ['./customer-status-chart.component.scss'],
})
export class CustomerStatusChartComponent extends BaseComponent implements OnInit, OnChanges {
  isPieChart = true;
  @Input() viewChange: EventEmitter<boolean> = new EventEmitter();
  data: any[];
  dataPercentage: string[] = [];
  option: EChartsOption;

  constructor(injector: Injector,private customerApi: CustomerApi) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
  }

  ngOnInit(): void {
    this.isLoading = true;
    const params = {
      customerTypes: [CustomerType.INDIV, CustomerType.NHS],
      rsId: this.objFunction?.rsId,
      scope: Scopes.VIEW,
    };
    this.customerApi.getStatusDataChart(params).subscribe(dataStatus => {
      this.data = dataStatus;
      if (this.data?.length > 0) {
        const sum =
          this.data
            ?.map((i) => i.value || 0)
            .reduce((a, c) => {
              return a + c;
            }) || 0;
        this.data?.forEach((item) => {
          this.dataPercentage.push(percentage(item.value || 0, sum));
        });
        this.handleMapData(EChartType.Pie);
      }
      this.isLoading = false;
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    // if (this.data?.length > 0) {
    //   const sum =
    //     this.data
    //       ?.map((i) => i.value || 0)
    //       .reduce((a, c) => {
    //         return a + c;
    //       }) || 0;
    //   this.data?.forEach((item) => {
    //     this.dataPercentage.push(percentage(item.value || 0, sum));
    //   });
    //   this.handleMapData(EChartType.Pie);
    // }
  }

  handleMapData(type: string) {
    if (this.data?.length > 0) {
      if (type === EChartType.Pie) {
        this.option = {
          tooltip: {
            trigger: 'item',
            formatter: (params) => {
              return `${formatNumber(params.value || 0, 'en', '1.0-0')} (${params.percent})%`;
            },
          },
          legend: {
            show: false,
            bottom: 0,
          },
          series: [
            {
              // bottom: '40%',
              type: EChartType.Pie,
              radius: ['30%', '55%'],
              labelLine: {
                length: 5,
              },
              label: {
                formatter: '{d}%',
              },
              data: [],
            },
          ],
        };
        this.data?.forEach((item) => {
          if (item.value > 0) {
            this.option.series[0].data.push({
              value: item.value,
              name: item.label,
              itemStyle: { color: item.color },
            });
          }
        });
      } else {
        this.option = {
          xAxis: {
            type: 'category',
          },
          yAxis: {
            type: 'value',
          },
          legend: {
            show: false,
            bottom: 0,
          },
          grid: {
            left: '5%',
            containLabel: true,
          },
          tooltip: {
            trigger: 'item',
            formatter: (params) => {
              return `${formatNumber(params.value || 0, 'en', '1.0-0')} (${
                this.dataPercentage[params.seriesIndex] || 0
              })%`;
            },
          },
          series: [],
        };
        const data = [];
        this.data?.forEach((item) => {
          data.push({
            name: item.label,
            data: [item.value],
            type: EChartType.Bar,
            showBackground: false,
            itemStyle: {
              color: item.color,
            },
            barWidth: 28,
            barGap: '120%',
          });
        });
        this.option.series = data;
      }
    } else {
      this.data = undefined;
    }
  }

  switchChart() {
    if (this.isLoading) {
      return;
    }
    this.isPieChart = !this.isPieChart;
    if (this.isPieChart) {
      this.handleMapData(EChartType.Pie);
    } else {
      this.handleMapData(EChartType.Bar);
    }
  }
}
