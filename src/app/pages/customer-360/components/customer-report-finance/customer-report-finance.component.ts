import { Component, OnInit, Injector, Input } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { CustomerDetailSmeApi } from '../../apis/customer.api';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { formatNumber } from '@angular/common';

@Component({
  selector: 'customer-report-finance',
  templateUrl: './customer-report-finance.component.html',
  styleUrls: ['./customer-report-finance.component.scss'],
  // encapsulation: ViewEncapsulation.None,
})
export class CustomerReportFinanceComponent extends BaseComponent implements OnInit {
  @Input() customerCode: string;
  listData = [];
  columns = [];
  tabs = ['Kết quả kinh doanh', 'Tài sản, nguồn vốn', 'Lưu chuyển tiền tệ'];
  tabIndex = 0;
  tab0 = [
    {
      name: this.fields?.dtbhvccdv,
      code: 'dtbhvccdv',
    },
    {
      name: this.fields?.dttbhvccdv,
      code: 'dttbhvccdv',
    },
    {
      name: this.fields?.lng,
      code: 'lng',
    },
    {
      name: this.fields?.hdkd,
      code: 'hdkd',
    },
    {
      name: this.fields?.lntt,
      code: 'lntt',
    },
    {
      name: this.fields?.lnst,
      code: 'lnst',
    },
    {
      name: this.fields?.cphdtc,
      code: 'cphdtc',
    },
    {
      name: this.fields?.dthdtc,
      code: 'dthdtc',
    },
    {
      name: this.fields.dsxnk,
      code: 'dsxnk',
    },
  ];
  tab1 = [
    {
      name: this.fields?.tongTaiSan,
      code: 'tongTaiSan',
    },
    {
      name: this.fields?.ttsnh,
      code: 'ttsnh',
    },
    {
      name: this.fields?.ttsdh,
      code: 'ttsdh',
    },
    {
      name: this.fields?.vcsh,
      code: 'vcsh',
    },
  ];
  tab2 = [
    {
      name: this.fields?.lctsxkdtt,
      code: 'lctsxkdtt',
    },
    {
      name: this.fields?.lctdttt,
      code: 'lctdttt',
    },
    {
      name: this.fields?.lcttctt,
      code: 'lcttctt',
    },
  ];
  data: any[];

  constructor(injector: Injector, private service: CustomerDetailSmeApi) {
    super(injector);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.service
      .getReportFinanceByCustomerCode(this.customerCode)
      .pipe(catchError(() => of([])))
      .subscribe((data) => {
        this.data = _.orderBy(data, 'year');
        _.forEach(this.data, (item) => {
          this.columns.push({
            name: `Năm ${item.year}`,
            prop: `year${item.year}`,
            sortable: false,
            draggable: false,
            year: `${item.year}`,
          });
        });
        this.columns.unshift({
          name: 'Chỉ tiêu',
          prop: 'label',
          sortable: false,
          draggable: false,
        });
        this.onChangeTab({ index: 0 });
        this.isLoading = false;
      });
  }

  onChangeTab(event) {
    this.tabIndex = _.get(event, 'index');
    this.listData = [];
    const list: any[] = this[`tab${this.tabIndex}`];
    let countRowEmpty = 0;
    _.forEach(list, (itemRow) => {
      if (!this.checkRowNotEmpty(this.data, itemRow.code)) {
        countRowEmpty += 1;
      }
      this.listData.push({
        label: itemRow.name,
        [`year${_.get(this.data, '[0].year')}`]: this.getCostValue(
          _.get(this.data, '[0].listFinanceReportDTO[0]'),
          itemRow.code
        ),
        [`year${_.get(this.data, '[1].year')}`]: this.getCostValue(
          _.get(this.data, '[1].listFinanceReportDTO[0]'),
          itemRow.code
        ),
        [`year${_.get(this.data, '[2].year')}`]: this.getCostValue(
          _.get(this.data, '[2].listFinanceReportDTO[0]'),
          itemRow.code
        ),
        [`year${_.get(this.data, '[3].year')}`]: this.getCostValue(
          _.get(this.data, '[3].listFinanceReportDTO[0]'),
          itemRow.code
        ),
      });
    });
    if (countRowEmpty === _.size(this.listData)) {
      this.listData = [];
    }
  }

  getCostValue(data, key) {
    return _.isNumber(_.get(data, key)) ? formatNumber(_.get(data, key), 'en', '1.0-2') : '---';
  }

  checkRowNotEmpty(data, key) {
    return (
      _.isNumber(_.get(data, `[0].listFinanceReportDTO[0].${key}`)) ||
      _.isNumber(_.get(data, `[1].listFinanceReportDTO[0].${key}`)) ||
      _.isNumber(_.get(data, `[2].listFinanceReportDTO[0].${key}`)) ||
      _.isNumber(_.get(data, `[3].listFinanceReportDTO[0].${key}`))
    );
  }
}
