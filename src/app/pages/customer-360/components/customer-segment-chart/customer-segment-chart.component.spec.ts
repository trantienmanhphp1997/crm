import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerSegmentChartComponent } from './customer-segment-chart.component';

describe('CustomerSegmentChartComponent', () => {
  let component: CustomerSegmentChartComponent;
  let fixture: ComponentFixture<CustomerSegmentChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerSegmentChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerSegmentChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
