import { Component, OnInit, ViewEncapsulation, HostBinding, Input, OnChanges, SimpleChanges } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-customer-modal',
  templateUrl: './customer-modal.component.html',
  styleUrls: ['./customer-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CustomerModalComponent implements OnInit, OnChanges {
  @HostBinding('class.list__customer-content') listContent = true;
  checkAll = false;
  @Input() listSelectedOld = [];
  @Input() dataSearch: any;
  @Input() rsId: string;
  isLoading = true;
  listCustomer = [];
  listSelected = [];

  constructor(private modalActive: NgbActiveModal) {}

  showLoading(isShowLoading) {
    this.isLoading = isShowLoading;
  }

  selectedCustomer(selected) {
    this.listSelected = [...selected];
  }

  ngOnInit() {
    if (this.listSelectedOld.length > 0) {
      this.listCustomer = [...this.listSelectedOld];
    }
  }

  ngOnChanges(changes: SimpleChanges) {}

  choose() {
    const data = {
      listSelected: this.listSelected,
    };
    this.modalActive.close(data);
  }

  closeModal() {
    this.modalActive.close(false);
  }
}
