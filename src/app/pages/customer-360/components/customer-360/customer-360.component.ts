import { Component, OnInit, Injector, ViewEncapsulation, ViewChild, OnDestroy } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { BaseComponent } from 'src/app/core/components/base.component';
import { LayoutService } from 'src/app/core/services/layout.service';
import { ComponentType, FunctionCode, functionUri } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'app-customer-360',
  templateUrl: './customer-360.component.html',
  styleUrls: ['./customer-360.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class Customer360Component extends BaseComponent implements OnInit, OnDestroy {
  isRefreshTable: number;
  @ViewChild('customerList') customerList: any;
  type = ComponentType.SinglePage;

  constructor(injector: Injector, private layoutService: LayoutService) {
    super(injector);
    this.prop = this.router.getCurrentNavigation()?.extras?.state;
  }

  ngOnInit() {
    this.isLoading = true;
    this.layoutService.bannerObs.next(false);
  }

  ngOnDestroy() {
    this.layoutService.bannerObs.next(true);
  }

  showLoading(isShowLoading) {
    this.isLoading = isShowLoading;
    this.ref.detectChanges();
  }

  redirectTo() {
    this.router.navigateByUrl(`${functionUri.customer_360_dashboard}`);
  }
}
