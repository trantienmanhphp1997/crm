import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationDivisionManagementComponent } from './operation-division-management.component';

describe('OperationDivisionManagementComponent', () => {
  let component: OperationDivisionManagementComponent;
  let fixture: ComponentFixture<OperationDivisionManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationDivisionManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationDivisionManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
