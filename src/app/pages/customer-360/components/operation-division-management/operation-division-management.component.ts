import { forkJoin, of } from 'rxjs';
import { cleanData } from 'src/app/core/utils/function';
import { global } from '@angular/compiler/src/util';
import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { FunctionCode, Scopes } from 'src/app/core/utils/common-constants';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { OperationDivisionApi } from '../../apis';
import { BranchApi } from 'src/app/pages/rm/apis';
import { catchError } from 'rxjs/operators';
import { FileService } from 'src/app/core/services/file.service';

@Component({
  selector: 'app-operation-division-management',
  templateUrl: './operation-division-management.component.html',
  styleUrls: ['./operation-division-management.component.scss'],
})
export class OperationDivisionManagementComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listData = [];
  limit = global.userConfig.pageSize;
  paramSearch = {
    pageSize: this.limit,
    pageNumber: 0,
    // searchAdvanced: '',
    branches: [],
    rsId: '',
    scope: Scopes.VIEW,
    customerCode: '',
    isAdvance: false,
  };
  pageable: Pageable;
  isAdvance = false;
  listBranches = [];
  prevParams: any;
  listSearchType = [];
  textSearch = '';
  searchType = 'customerCode';
  @ViewChild('table') table: DatatableComponent;

  constructor(
    injector: Injector,
    private operationDivisionApi: OperationDivisionApi,
    private branchApi: BranchApi,
    private fileService: FileService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.CUSTOMER_360_OPERATION_BLOCK_MANAGER}`
    );
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
    if (this.prop) {
      this.paramSearch = this.prop.prevParams;
      this.prevParams = this.prop.prevParams;
      this.searchType = this.prop?.searchType || 'customerCode';
      this.textSearch = this.prop?.textSearch || '';
    }
    const rsIdCustomer360 = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`)?.rsId;
    this.paramSearch.rsId = rsIdCustomer360;
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.listSearchType = [
      { code: 'customerCode', name: this.fields.customerCode },
      { code: 'customerName', name: this.fields.customerName },
      { code: 'branchCode', name: this.fields._branchCode },
      { code: 'branchName', name: this.fields._branchName },
    ];
    this.branchApi.selectize(this.paramSearch.rsId, Scopes.VIEW).subscribe(
      (listBranches: any[]) => {
        this.listBranches = listBranches;
        this.paramSearch.branches = listBranches?.map((i) => i.code);
        this.search(!this.prop);
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  isShow() {
    this.isAdvance = !this.isAdvance;
  }

  searchAdvance(isSearch?: boolean) {
    this.paramSearch.isAdvance = true;
    this.search(isSearch);
  }

  searchBasic(isSearch?: boolean) {
    this.paramSearch.isAdvance = false;
    this.search(isSearch);
  }

  search(isSearch?: boolean) {
    let params: any;
    if (isSearch) {
      this.paramSearch.pageNumber = 0;
      params = { ...this.paramSearch };
      if (this.paramSearch.isAdvance) {
        delete params.searchAdvanced;
      } else {
        delete params.branches;
        delete params.customerCode;
        params[this.searchType] = _.trim(this.textSearch);
      }
    } else {
      if (!this.prevParams) {
        return;
      }
      params = this.prevParams;
      params.pageNumber = this.paramSearch.pageNumber;
    }
    this.isLoading = true;
    cleanData(params);
    forkJoin([
      this.operationDivisionApi.findAll(params).pipe(catchError(() => of(undefined))),
      this.operationDivisionApi.countAll(params).pipe(catchError(() => of(undefined))),
    ]).subscribe(
      ([listData, count]) => {
        this.prevParams = params;
        this.listData = listData || [];
        count = count || 0;
        this.pageable = {
          totalElements: count,
          totalPages: Math.floor(count / this.paramSearch.pageSize),
          currentPage: this.paramSearch.pageNumber,
          size: this.paramSearch.pageSize,
        };
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.pageNumber = pageInfo.offset;
    this.search(false);
  }

  assign() {
    this.router.navigate([this.router.url, 'assign'], {
      state: { prevParams: this.prevParams, textSearch: this.textSearch, searchType: this.searchType },
    });
  }

  unAssign() {
    this.router.navigate([this.router.url, 'unassign'], {
      state: { prevParams: this.prevParams, textSearch: this.textSearch, searchType: this.searchType },
    });
  }

  exportFile() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+(this.pageable?.totalElements || 0) === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.isLoading = true;
    this.operationDivisionApi.exportFile(this.prevParams).subscribe(
      (fileId) => {
        this.fileService.downloadFile(fileId, 'danh-sach-khach-hang.xlsx').subscribe((res) => {
          if (res) {
            this.isLoading = false;
          } else {
            this.isLoading = false;
            this.messageService.error(this.notificationMessage.error);
          }
        });
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }
}
