import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { global } from '@angular/compiler/src/util';
import {
  CommonCategory,
  FunctionCode,
  functionUri,
  maxInt32,
  REASON_STATUS,
  Scopes,
  ScreenType,
  SessionKey,
} from 'src/app/core/utils/common-constants';
import { ModalRevenueSharingComponent } from '../modal-revenue-sharing/modal-revenue-sharing.component';
import { forkJoin, Observable, of } from 'rxjs';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { catchError } from 'rxjs/operators';
import _ from 'lodash';
import { CustomerApi, RmApi } from '../../apis';
import { Utils } from 'src/app/core/utils/utils';
import * as moment from 'moment';
import { RevenueShareApi } from '../../apis/revenue-share.api';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { formatNumber } from '@angular/common';

@Component({
  selector: 'revenue-sharing-create-component',
  templateUrl: './revenue-sharing-create.component.html',
  styleUrls: ['./revenue-sharing-create.component.scss'],
})
export class RevenueSharingCreateComponent extends BaseComponent implements OnInit {
  isLoading = false;
  @ViewChild('table') table: DatatableComponent;
  @ViewChild('tableTarget') tableTarget: DatatableComponent;
  pageable: Pageable;
  screenType: any;
  listBranches = [];
  listDivision = [];
  listRmShare = [];
  listStatusShare = [];
  listRmManager = [];
  listTitleConfig = [];
  formCreated = this.fb.group({
    customerCode: [{ value: '', disabled: true }],
    customerName: [{ value: '', disabled: true }],
    rmCode: [{ value: '', disabled: true }],
    branchCode: [{ value: '', disabled: true }],
    currentFundraising: [{ value: '', disabled: true }],
    currentCredit: [{ value: '', disabled: true }],
    divisionCode: [{ value: '', disabled: true }],

    shareBranch: ['', CustomValidators.required],
    shareDivision: ['', CustomValidators.required],
    shareRmCode: [''],

    shareStatus: [{ value: '', disabled: true }],
    note: ['', CustomValidators.required],
    startDate: [new Date(), CustomValidators.required],
    endDate: [new Date(), CustomValidators.required],
    shareTargetDTOList: [],
  });
  myModel = 0;
  code: string;
  minDate = new Date();
  paramSearchCustomer = {
    pageNumber: 0,
    pageSize: 10,
    rsId: '',
    scope: 'VIEW',
    search: '',
    searchFastType: 'CUSTOMER_CODE',
  };
  objFunctionCustomer: any;
  objFunctionRm: any;
  apiInfoRevenueShare: Observable<any>;
  idRevenue: any;
  vaildateRmShare: boolean;
  maxRate = 100;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private customerApi: CustomerApi,
    private rmApi: RmApi,
    private revenueShareApi: RevenueShareApi
  ) {
    super(injector);
    this.isLoading = true;
    this.objFunctionCustomer = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REVENUE_SHARE}`);
    this.objFunctionRm = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.route?.data?.subscribe((data) => {
      this.screenType = data?.type;
    });
    const isShowRevenue = _.get(this.route.snapshot.queryParams, 'showBtnRevenueShare') === 'true';
    if (!isShowRevenue && this.screenType === ScreenType.Create) {
      this.router.navigateByUrl(functionUri.dashboard);
    }
  }
  listTarget = [];
  listData = [];
  limit = global.userConfig.pageSize;
  params: any = {
    pageNumber: 0,
    pageSize: global?.userConfig?.pageSize,
    rsId: '',
    scope: Scopes.VIEW,
  };

  ngOnInit() {
    if (this.screenType === ScreenType.Detail) {
      this.idRevenue = _.get(this.route.snapshot.params, 'id');
      this.apiInfoRevenueShare = this.revenueShareApi
        .detailRevenueShare(this.idRevenue)
        .pipe(catchError(() => of(undefined)));
    } else {
      this.paramSearchCustomer.search = _.get(this.route.snapshot.params, 'code');
      this.paramSearchCustomer.rsId = this.objFunctionCustomer?.rsId;
      this.apiInfoRevenueShare = this.customerApi
        .searchSme(this.paramSearchCustomer)
        .pipe(catchError(() => of(undefined)));
    }

    forkJoin([
      this.apiInfoRevenueShare,
      this.categoryService
        .getBranchesOfUser(this.objFunctionCustomer?.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.SHARE_REVENUE_LINE).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.SHARE_ITEM).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.ACC_REVENUE_SHARING_ST).pipe(catchError(() => of(undefined))),
      this.commonService
        .getCommonCategory(CommonCategory.SHARE_REVENUE_ROLE_INPUT)
        .pipe(catchError(() => of(undefined))),
      this.commonService
        .getCommonCategory(CommonCategory.SHARE_REVENUE)
        .pipe(catchError(() => of(undefined))),
    ]).subscribe(([infoCustomer, listBranches, listDivisionShare, listShareItem, listStatusShare, listTitleConfig, shareRevenue]) => {
      this.listTitleConfig = listTitleConfig?.content;
      if (_.isEmpty(this.idRevenue)) {
        this.formCreated.get('customerCode').setValue(_.get(infoCustomer[0], 'customerCode'));
        this.formCreated.get('customerName').setValue(_.get(infoCustomer[0], 'customerName'));
        this.formCreated.get('rmCode').setValue(_.get(infoCustomer[0], 'manageRM'));
        this.formCreated.get('branchCode').setValue(_.get(infoCustomer[0], 'branchCode'));
        this.formCreated
          .get('currentFundraising')
          .setValue(formatNumber(_.get(infoCustomer[0], 'balanceRaisingCapital') || 0, 'en', '0.0-2'));
        this.formCreated
          .get('currentCredit')
          .setValue(formatNumber(_.get(infoCustomer[0], 'balanceCredit', '') || 0, 'en', '0.0-2'));
        this.formCreated.get('divisionCode').setValue(_.get(infoCustomer[0], 'customerType'));
      } else {
        this.formCreated.get('customerCode').setValue(_.get(infoCustomer, 'customerCode'));
        this.formCreated.get('customerName').setValue(_.get(infoCustomer, 'customerName'));
        this.formCreated.get('rmCode').setValue(_.get(infoCustomer, 'rmCode'));
        this.formCreated.get('branchCode').setValue(_.get(infoCustomer, 'branchCode'));
        this.formCreated.get('currentFundraising').setValue(_.get(infoCustomer, 'currentFundraising') || 0);
        this.formCreated.get('currentCredit').setValue(_.get(infoCustomer, 'currentCredit') || 0);
        this.formCreated.get('divisionCode').setValue(_.get(infoCustomer, 'divisionCode'));
        this.formCreated.get('endDate').setValue(moment(_.get(infoCustomer, 'enDate'), 'DD-MM-YYYY').toDate());
        this.formCreated.get('startDate').setValue(moment(_.get(infoCustomer, 'startDate'), 'DD-MM-YYYY').toDate());

        this.formCreated.get('note').setValue(_.get(infoCustomer, 'note'));
      }
      // chi nhánh
      this.listBranches = listBranches?.map((item) => {
        return { code: item.code, displayName: item.code + ' - ' + item.name };
      });
      this.formCreated.controls.shareBranch.setValue(null);

      this.listStatusShare = listStatusShare?.content.map((item) => {
        return { code: item.code, displayName: item.name, valueStatus: item.name };
      });

      this.listStatusShare.forEach((item) => {
        if (item.code === 'PENDING') {
          this.formCreated.get('shareStatus').setValue(item?.code);
        }
      });

      // Khối
      let valueDivisionShare = listDivisionShare?.content?.find(
        (item) =>
          item.code === _.get(infoCustomer[0], 'customerType') || item.code === _.get(infoCustomer, 'divisionCode')
      )?.value;

      let listDivision =
        this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM)?.map((item) => {
          return {
            code: item.code,
            displayName: item.code + ' - ' + item.name,
          };
        }) || [];
      listDivision?.forEach((item) => {
        if (valueDivisionShare.includes(item.code)) {
          this.listDivision.push(item);
        }
      });
      if (_.isEmpty(this.idRevenue)) {
        // listTarget
        const dataTarget = [];
        listShareItem?.content.forEach((element) => {
          dataTarget.push({
            target: element.code,
            targetName: element.name,
            rateOfRmManagement: 100,
            ratioRmAmShared: 0,
          });
        });

        this.listTarget = dataTarget;
        this.formCreated.get('shareDivision').setValue(_.get(infoCustomer[0], 'customerType'), { emitEvent: false });
        this.getRmManager([], _.get(infoCustomer[0], 'customerType'));
        this.isLoading = false;
      } else {
        const nameShareItem = listShareItem?.content.map((item) => {
          return {
            code: item.code,
            name: item.name,
          };
        });
        this.listTarget = _.get(infoCustomer, 'shareTargetDTOList')?.map((item) => {
          let nameShare = nameShareItem.filter((i) => i.code === item.target)[0]?.name || '';
          return { ...item, targetName: nameShare };
        });
        this.formCreated.get('shareDivision').setValue(_.get(infoCustomer, 'shareDivision'), { emitEvent: false });
        this.getRmManager([], _.get(infoCustomer, 'shareDivision'), _.get(infoCustomer, 'shareRmCode'));
        this.formCreated.get('shareStatus').setValue(_.get(infoCustomer, 'shareStatus'));
        this.formCreated.get('shareBranch').setValue(_.get(infoCustomer, 'shareBranch', ''), { emitEvent: false });
      }

      // config share revenue
      this.maxRate = shareRevenue?.content?.find(item => item.code === 'MAX_REVENUE').value || 100;
      
    });
  }

  ngAfterViewInit() {
    this.formCreated.controls.shareBranch.valueChanges.subscribe((value) => {
      if (value) {
        this.getRmManager([value], this.formCreated.controls.shareDivision.value);
      }
    });

    this.formCreated.controls.shareDivision.valueChanges.subscribe((value) => {
      this.getRmManager([this.formCreated.controls.shareBranch.value], value);
    });

  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.pageNumber = pageInfo.offset;
    this.search(false);
  }

  search(isSearch: boolean) {
    this.listData = [{ customerName: 1, branch: '0012211', phone: '2565656', email: 'qqqqqq' }];
    this.pageable = {
      totalElements: 1,
      totalPages: 1,
      currentPage: 1,
      size: this.limit,
    };
    this.isLoading = false;
  }

  editRateSharedRM(row) {
    for (const item of this.listTarget) {
      if (item.target === row.target) {
        if (_.isNull(row.ratioRmAmShared)) {
          item.ratioRmAmShared = 0;
        } else {
          item.rateOfRmManagement = 100 - row.ratioRmAmShared;
        }
      }
    }
  }

  getRmManager(listBranch, rmBlock?: string, codeRmEdit?: string) {
    this.formCreated.controls.shareRmCode.clearValidators();
    this.vaildateRmShare = false;
    this.formCreated.get('shareRmCode').disable();
    this.listRmManager = [];
    this.rmApi
      .post('findAll', {
        page: 0,
        size: maxInt32,
        crmIsActive: true,
        branchCodes: listBranch?.filter((code) => !Utils.isStringEmpty(code)),
        rmBlock: rmBlock || '',
        rsId: this.objFunctionCustomer?.rsId,
        scope: Scopes.VIEW,
        searchRevenue: true,
        customerBranchCode: this.formCreated.get('branchCode').value || '',
      })
      .subscribe((res) => {
        const listRm: any[] =
          res?.content?.map((item) => {
            return {
              code: item?.t24Employee?.employeeCode || '',
              displayName:
                Utils.trimNullToEmpty(item?.t24Employee?.employeeCode) +
                ' - ' +
                Utils.trimNullToEmpty(item?.hrisEmployee?.fullName),
            };
          }) || [];
        this.listRmManager = listRm.filter((item) => {
          return item.code !== this.formCreated.get('rmCode').value?.split(' ')[0] && item.code;
        });

        if (_.isEmpty(codeRmEdit)) {
          this.formCreated.controls.shareRmCode.setValue(null);
        } else {
          this.isLoading = false;
          this.formCreated.controls.shareRmCode.setValue(codeRmEdit);
        }
        this.formCreated.controls.shareRmCode.setValidators(CustomValidators.required);
        this.formCreated.get('shareRmCode').enable();
      });
  }

  createdRevenueShare() {
    this.vaildateRmShare = true;
    if (this.formCreated.valid) {
      const formData = { ...this.formCreated.getRawValue() };
      if (moment(formData.endDate).isBefore(moment(formData.startDate))) {
        this.messageService.warn(this.notificationMessage.startDateMoreThanEndDate);
        return;
      }

      formData.startDate = moment(formData.startDate).format('YYYY-MM-DD');
      formData.endDate = moment(formData.endDate).format('YYYY-MM-DD');
      formData.endDate = moment(formData.endDate).format('YYYY-MM-DD');
      formData.rmCode = formData.rmCode?.split(' ')[0];
      const countTarget = this.listTarget.filter(
        (item) => _.isNull(item.ratioRmAmShared) || item.ratioRmAmShared === 0
      );
      if (countTarget.length === 2) {
        this.messageService.error(this.notificationMessage.targetShare);
        return;
      }
      formData.shareTargetDTOList = this.listTarget;
      let apiRevenueShare: Observable<any>;
      if (_.isEmpty(this.idRevenue)) {
        apiRevenueShare = this.revenueShareApi.createRevenueShare(formData);
      } else {
        apiRevenueShare = this.revenueShareApi.editRevenueShare(this.idRevenue, formData);
      }

      this.isLoading = true;
      apiRevenueShare.subscribe(
        (res) => {
          if (res) {
            this.isLoading = false;
            this.messageService.success(this.notificationMessage.success);
            this.router.navigateByUrl(`${functionUri.revenue_share}`);
          }
        },
        (e) => {
          if (_.isEmpty(e.error.code)) {
            this.messageService.error(this.notificationMessage.error);
          } else {
            this.messageService.error(e.error.description);
          }
          this.isLoading = false;
        }
      );
    } else {
      validateAllFormFields(this.formCreated);
    }
  }

  backShare() {
    if (this.screenType === ScreenType.Create) {
      this.router.navigateByUrl(`${functionUri.customer_360_manager}`);
    } else {
      this.back();
    }
  }

  haveReason(): boolean {
    return REASON_STATUS.includes(this.formCreated.getRawValue().shareStatus);
  }

  get isDetail(): boolean {
    return this.screenType === ScreenType.Detail;
  }
}
