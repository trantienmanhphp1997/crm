import { global } from '@angular/compiler/src/util';
import {
  AfterViewInit,
  EventEmitter,
  Injector,
  Input,
  Output,
  Component,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';
import { Utils } from 'src/app/core/utils/utils';
import { catchError } from 'rxjs/operators';
import { CustomerApi, CustomerAssignmentApi } from '../../apis';
import { cleanDataForm } from 'src/app/core/utils/function';
import {
  CommonCategory,
  ComponentType,
  ConfirmType,
  CustomerType,
  Division,
  ExportKey,
  FunctionCode,
  functionUri,
  ListCustomerType,
  maxInt32,
  Scopes,
  SessionKey,
  LEVEL_TTTM,
} from 'src/app/core/utils/common-constants';
import { SelectionType } from '@swimlane/ngx-datatable';
import { RmModalComponent } from 'src/app/pages/rm/components';
import { forkJoin, Observable, of, Subscription } from 'rxjs';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { RmApi, RmBlockApi } from 'src/app/pages/rm/apis';
import { FileService } from 'src/app/core/services/file.service';
import { ActivationEnd, ActivationStart } from '@angular/router';
import { HistoryAssignmentComponent } from '../history-assignment/history-assignment.component';
import { RevenueShareApi } from '../../apis/revenue-share.api';
import { SaleManagerApi } from 'src/app/pages/sale-manager/api/sale-manager.api';
import { AppFunction } from '../../../../core/interfaces/app-function.interface';
import { MbeeChatService } from '../../../../core/services/mbee-chat.service';
import { SaleSuggestApi } from './../../../sale-manager/api/sale-suggest.api';
import { createGroupChat11 } from '../../../../core/utils/mb-chat';
import { MenuItem } from 'primeng/api';
@Component({
  selector: 'app-customer-management',
  templateUrl: 'customer-management.component.html',
  styleUrls: ['customer-management.component.scss'],
})
export class CustomerManagementComponent extends BaseComponent implements OnInit {
  listCustomerClassification = [
    {
      name: 'Tất cả',
      value: -1,
    },
    {
      name: 'Khách hàng trực tiếp',
      value: 0,
    },
    {
      name: 'Khách hàng gián tiếp',
      value: 1,
    },
  ];
  textSearch = '';
  searchQuickType = '';
  isShowAdvance = true;
  isOpenMore = false;
  isCount = false;
  is8FirstDayInMonth = true;
  numberFirstDayInMonth = true;
  @Input() rsId: string;
  @Input() dataSearch: any;
  @Input() refreshTable: number;
  @Input() showAction: boolean;
  @Input() propData: any;
  @Input() type = ComponentType.Modal;
  @Input() listSelectedOld: any[];
  @Output() loading: EventEmitter<any> = new EventEmitter<any>();
  @Output() selectedCustomer: EventEmitter<any> = new EventEmitter<any>();
  listData = [];
  listTransactionFrequency = [];
  sectors: any[] = [];
  selectedSectors: any[] = [];
  listBranches = [];
  listDivision = [];
  listRmManager = [];
  listStatus = [
    {code:'',name:'Tất cả'},
    {code:"ACTIVE",name:'ACTIVE'},
    {code:"INACTIVE",name:'INACTIVE'},
    {code:"DORMANT",name:'DORMANT'},
    {code:"NOT_YET_RATE",name:'Chưa xác định'},
  ];
  listSegmentAll = [];
  listSegment = [];
  listCustomerObject = [];
  listSearchType = [];
  listTitleConfig = [];
  phonePriority: string;
  emailPriority: string;
  pageable: Pageable;
  searchForm = this.fb.group({
    customerCode: [''],
    customerName: [''],
    phone: [''],
    idCard: [''],
    customerType: [''],
    transactionFrequency: [''],
    transactionFrequencyText: [this.fields.all],
    branchCode: [''],
    hrsCode: [''],
    businessRegistrationNumber: [''],
    taxCode: [''],
    swiftCode: [''],
    industry: [''],
    status: [''],
    segment: [''],
    sectorArray: [''],
    customerObjects: [''],
    customerTypeSale: [1],
    manageType: [1],
    productGroup: [''],
    rmCode: [''],
    customerClassification: [-1],
    keyWord:['']
  });
  titleSegment: string = 'Phân khúc KH (DT gần nhất)';
  params: any = {
    pageNumber: 0,
    pageSize: global?.userConfig?.pageSize,
    rsId: '',
    scope: Scopes.VIEW,
    customerType: '',
  };
  prevParams: any = {};
  industryCode = '';
  checkAll = false;
  checkTransactionFrequencyGroupAll = true;
  countCheckedOnPage = 0;
  countDataTransactionFrequencyGroup = 0;
  searchAdvance: string;
  subCount: Subscription;
  currState: any=[];
  isSmeTable = false;
  disabledSearchAdvance = true;
  disabledSearchBasic = true;
  divisionConfig = [];
  listCustomerType = [];
  listManageType = [];
  isRMorUB = false;
  listProductType = [];
  listRmSale = [];
  isCustomerTypeSale = false;
  objFunctionRM: any;
  isOpenCodeDateShow = false;
  isShowBtnCampaign = false;
  scopes: Array<any>;
  isShowCreateOppBtn = false;
  isShowColumnContractNumber = false;
  systemAssign: string;
  flag: any;
  isLoadTable: boolean = true;
  priorityBlock: [];
  constructor(
    injector: Injector,
    private customerApi: CustomerApi,
    private customerAssignmentApi: CustomerAssignmentApi,
    private categoryService: CategoryService,
    private rmApi: RmApi,
    private fileService: FileService,
    private rmBlockApi: RmBlockApi,
    private revenueShareApi: RevenueShareApi,
    private saleManagerApi: SaleManagerApi,
    private mbeeChatService: MbeeChatService,
    private saleSuggestApi: SaleSuggestApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.objFunctionRM = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.objFunctionMBChat = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.APP_CHAT}`);
    this.scopes = _.get(this.objFunction, 'scopes');
    this.paramsRmSale.rsId = this.objFunctionRM.rsId;
    this.isLoading = true;
  }
  paramsRmSale: any = {
    divisionCode: 'SME',
    rsId: '',
    scope: Scopes.VIEW,
    page: 0,
    size: maxInt32,
    productGroup: '',
    branchCodes: '',
  };
  viewCustomerType = true;
  listDivisionSale = [];
  paramSearchDivision = {
    size: global.userConfig.pageSize,
    page: 0,
    name: '',
    code: '',
  };

  customerId: any;

  isShowChat = false;
  objFunctionMBChat: AppFunction;
  rowMenu: MenuItem[];
  itemSelected: any;
  isClickEvent = false;

  ngOnInit(): void {
    this.listDivision = _.map(this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM), (item) => {
      return {
        titleCode: item.titleCode,
        levelCode: item.levelCode,
        code: item.code,
        displayName: `${item.code} - ${item.name}`,
      };
    });
    if (_.head(this.listDivision)?.code === Division.INDIV) {
      this.isLoadTable = false;
    }

    this.isShowChat = this.objFunctionMBChat?.scopes.includes('VIEW');

    this.customerId = JSON.parse(localStorage.getItem('DETAIL_CUS_ID'));
    if (this.customerId) {
      this.router.navigate([this.router.url, 'detail', 'indiv', this.customerId], {
        skipLocationChange: true,
      });
    }
    this.titleSegment = this.params.customerType === 'SME' ? 'Phân khúc KH (DTBQ 3 năm)' : 'Phân khúc KH (DT gần nhất)';

    this.rowMenu = [];
    if (this.objFunction?.rmGrant) {
      if (this.isShowChat) {
        this.rowMenu.push({
          label: 'Chat',
        });
      }

      if (this.isShowCreateOppBtn) {
        this.rowMenu.push({
          label: 'Tạo cơ hội bán',
        });
      }
    }
    this.onClickSearch(true, 'advance');
    this.initData()
    this.getRmManager([], this.searchForm.controls.customerType.value);
  }
  initData() {
    this.listCustomerType = ListCustomerType;
    this.isDownLoading = this.sessionService.getSessionData(ExportKey.Customer);
    this.searchQuickType = 'CUSTOMER_CODE';
    this.router.events.subscribe((e) => {
      if (e instanceof ActivationStart && e.snapshot?.data?.code !== FunctionCode.CUSTOMER_360_MANAGER) {
        this.prop = null;
        this.sessionService.setSessionData(FunctionCode.CUSTOMER_360_MANAGER, null);
      }
    });

    this.params.rsId = !this.rsId ? this.objFunction?.rsId : this.rsId;

    if (!this.listSelectedOld) {
      this.listSelectedOld = [];
    }

    if (this.type !== ComponentType.Modal) {
      this.prop = this.sessionService.getSessionData(FunctionCode.CUSTOMER_360_MANAGER);
    }

    if (!this.prop) {
      if (!this.currState) {
        
      } else {
        this.search(true);
      }
    } else {
      const timer = setTimeout(() => {
        this.isLoadTable = this.prop?.isLoadTable;
        this.isShowAdvance = this.prop?.isShowAdvance;
        this.isOpenMore = this.prop?.isOpenMore;
        this.prevParams = this.prop?.params;
        this.params.pageNumber = this.prop?.params?.pageNumber || 0;
        this.params.pageSize = this.prop?.params?.pageSize || global?.userConfig?.pageSize;
        this.params.customerType = this.prop?.params?.customerType || _.head(this.listDivision)?.code;
        this.industryCode = this.prop?.params?.industry || '';
        this.textSearch = this.prop?.params?.search || '';
        this.searchQuickType = this.prop?.params?.searchFastType || 'CUSTOMER_CODE';
        this.searchAdvance = this.prop?.searchAdvance;
        this.listRmSale = this.prop?.listRmSale;
        if (this.prop.params && this.searchAdvance === 'advance') {
          this.searchForm.patchValue(this.prop.dataForm);
        }
        this.listRmManager = this.prop?.listRmManager;
        this.listSegment = this.prop?.listSegment;
        if (this.prop?.params?.sectorArray) {
          this.selectedSectors = [];
          this.prop.params.sectorArray.forEach((item) => {
            let obj = {
              id: item,
            };
            this.selectedSectors.push(obj);
          });
        } else {
          this.selectedSectors = this.sectors;
        }
        this.listCustomerObject = this.prop?.listCustomerObject;
        this.listTransactionFrequency?.forEach((item) => {
          item.isChecked = this.prop?.params?.transactionFrequency?.includes(item.code);
        });
        this.checkTransactionFrequencyGroupAll =
          this.prop?.params?.transactionFrequency?.length === this.listTransactionFrequency?.length;
        if (this.prop?.pageable) {
          this.pageable = this.prop.pageable;
          this.isCount = true;
        } else {
          this.getCountCustomer(this.prevParams);
        }
        this.isCustomerTypeSale = this.prop?.isCustomerTypeSale;
        this.isSmeTable = this.prop?.isSmeTable;
        this.listData = this.prop?.listData;
        this.toggleLoading(false);
        this.table?.recalculate();
        clearTimeout(timer);
      }, 800);
    }
    this.disabledSearchAdvance = this.isShowAdvance;
    this.disabledSearchBasic = !this.isShowAdvance;
  }

  search(isSearch?: boolean, type?: string) {
    if (!this.isLoadTable) {
      this.toggleLoading(false);
      return;
    }
    this.toggleLoading(true);
    let params: any = {};
    if (
      (this.searchForm.get('customerType').value === 'SME' && this.isShowAdvance) ||
      (!this.isShowAdvance && this.params.customerType === 'SME')
    ) {
      this.titleSegment = 'Phân khúc KH (DTBQ 3 năm)';
    } else {
      this.titleSegment = 'Phân khúc KH (DT gần nhất)';
    }
    if (isSearch) {
      this.searchAdvance = type;
      this.params.pageNumber = 0;
      if (this.searchAdvance === 'advance') {
        const dataForm = cleanDataForm(this.searchForm);
        params = { ...dataForm };
        delete params.transactionFrequencyText;
        if (!this.checkTransactionFrequencyGroupAll) {
          params.transactionFrequency = [];
          this.listTransactionFrequency?.map((item) => {
            if (item.isChecked) {
              params.transactionFrequency.push(item.code);
            }
          });
        } else {
          delete params.transactionFrequency;
        }
        if (this.selectedSectors.length == this.sectors.length || this.selectedSectors.length === 0) {
          delete params.sectorArray;
        } else {
          params.sectorArray = [];
          this.selectedSectors?.map((item) => {
            params.sectorArray.push(item.id);
          });
        }
        params.branches = [params.branchCode];
        params.rsId = this.params.rsId;
        params.scope = this.params.scope;
        if (!_.isEmpty(_.trim(params.customerObjects))) {
          params.customerObjects = _.split(params.customerObjects, ',');
        } else {
          params.customerObjects = [];
        }
        delete params.branchCode;
      } else {
        params = this.params;
        this.textSearch = Utils.trimNullToEmpty(this.textSearch);
        params.search = this.textSearch;
        params.searchFastType = this.searchQuickType;
        params.manageType = this.searchForm.controls.manageType.value;
        params.customerTypeSale = this.listCustomerType?.length > 1 ? 1 : 2;
      }
    } else {
      params = this.prevParams;
    }
    this.isShowColumnContractNumber = this.searchForm.controls.manageType.value === 0 ? true : false;
    this.systemAssign = this.searchForm.controls.manageType.value === 0 ? '103' : '';
    this.listData = [];
    this.messages = { ...this.messages, emptyMessage: '' };
    params.pageNumber = this.params.pageNumber || 0;
    params.pageSize = this.params.pageSize;
    if (this.dataSearch) {
      Object.keys(this.dataSearch).forEach((key) => {
        if (this.dataSearch[key].value) {
          params[key] = this.dataSearch[key].value;
        }
      });
    }
    let apiSearch: Observable<any>;
    if (this.searchQuickType === 'BUSINESS_REGISTRATION_NUMBER' || params.customerType !== Division.INDIV) {
      params.rmCodeManager = _.find(this.listRmManager, (item) => item.code === params.hrsCode)?.rmCode;
      apiSearch = this.customerApi.searchSme(params);
      this.isSmeTable = true;
    } else {
      apiSearch = this.customerApi.search(params);
      this.isSmeTable = false;
    }

    if (params.customerTypeSale === 2) {
      if (this.searchAdvance === 'advance') {
        params.productGroup = this.searchForm.controls.productGroup.value;
      } else {
        params.rmCode = this.currUser?.code;
      }
      params.branches = _.filter(params.branches, (item) => !_.isEmpty(item));
      delete params.hrsCode;
      this.customerApi.searchSmeSale(params).subscribe(
        (listData) => {
          this.isCustomerTypeSale = true;
          this.mapRespondSearch(listData, params, true);
        },
        () => {
          this.toggleLoading(false);
        }
      );
    } else {
      apiSearch.subscribe(
        (listData) => {
          this.isCustomerTypeSale = false;
          this.mapRespondSearch(listData, params, false);
        },
        () => {
          this.toggleLoading(false);
        }
      );
      this.getCountCustomer(params);
    }

    this.disabledSearchAdvance = this.searchAdvance === 'advance';
    this.disabledSearchBasic = this.searchAdvance !== 'advance';

    this.flag = params.customerType;
    if (this.flag === 'INDIV') {
      if (!_.find(this.rowMenu, (i) => i.label === 'Chat')) {
        this.rowMenu.unshift({
          label: 'Chat',
        });
      }
    } else {
      if (_.find(this.rowMenu, (i) => i.label === 'Chat')) {
        this.rowMenu.shift();
      }
    }
  }

  mapRespondSearch(data, params, isSale: boolean) {
    this.messages = { ...this.messages, emptyMessage: _.cloneDeep(global?.messageTable?.emptyMessage) };
    this.isLoading = false;
    this.listData = isSale ? data.content : data || [];
    this.countCheckedOnPage = 0;
    // if (isSale) {
    //   this.pageable = {
    //     totalElements: data.totalElements,
    //     totalPages: data.totalPages,
    //     currentPage: data.number,
    //     size: global?.userConfig?.pageSize,
    //   };
    //   this.isCount = true;
    // } else {
    //   if (!this.isCount) {
    //     this.pageable = {
    //       totalElements: this.listData.length,
    //       totalPages: 0,
    //       currentPage: this.params.pageNumber,
    //       size: this.params.pageSize,
    //     };
    //   }
    // }
    // this.pageable.currentPage = this.params?.pageNumber || 0;
    // this.pageable.size = this.params?.pageSize || global?.userConfig?.pageSize;
    // this.checkAll = this.listData.length > 0 && this.countCheckedOnPage === this.listData.length;
    // this.prevParams = params;
    this.toggleLoading(false);
  }

  getCountCustomer(params) {
    this.isCount = false;
    this.subCount?.unsubscribe();
    let apiCount: Observable<any>;
    if (this.searchQuickType === 'BUSINESS_REGISTRATION_NUMBER' || params.customerType !== Division.INDIV) {
      apiCount = this.customerApi.countSme(params);
    } else {
      apiCount = this.customerApi.count(params);
    }
    apiCount.pipe(catchError(() => of(0))).subscribe((count) => {
      this.isCount = true;
      count = count || 0;
      this.pageable = {
        totalElements: count,
        totalPages: Math.floor(count / this.params.pageSize),
        currentPage: this.params?.pageNumber || 0,
        size: this.params.pageSize,
      };
    });
  }

  onClickSearch(isSearch?: boolean, type?: string) {
    this.isLoadTable = true;
    this.search(isSearch, type);
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.pageNumber = pageInfo.offset;
    this.search(false);
  }

  handleChangePageSize(event) {
    this.params.pageSize = event;
    this.params.pageNumber = 0;
    if (this.isShowAdvance) {
      this.search(true, 'advance');
    } else {
      this.search(true, 'basic');
    }
  }

  // exportFile() {
  //   if (!this.maxExportExcel) {
  //     return;
  //   }
  //   if (+(this.pageable?.totalElements || 0) === 0) {
  //     this.messageService.warn(this.notificationMessage.noRecord);
  //     return;
  //   }
  //   if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
  //     this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
  //       this.messageService.warn(res);
  //     });
  //     return;
  //   }
  //   this.toggleLoading(true);
  //   let api;
  //   if (this.searchForm.get('customerTypeSale').value === 2 && this.isCustomerTypeSale) {
  //     api = this.customerApi.createFileSaleNew(this.prevParams);
  //   } else {
  //     if (this.searchQuickType === 'BUSINESS_REGISTRATION_NUMBER' || this.prevParams.customerType !== Division.INDIV) {
  //       api = this.customerApi.createFileSME(this.prevParams);
  //     } else {
  //       api = this.customerApi.createFile(this.prevParams);
  //     }
  //   }

  //   api.subscribe(
  //     (res) => {
  //       if (Utils.isStringNotEmpty(res)) {
  //         this.download(res);
  //       } else {
  //         this.messageService.error(this.notificationMessage?.export_error || '');
  //         this.toggleLoading(false);
  //       }
  //     },
  //     () => {
  //       this.messageService.error(this.notificationMessage?.export_error || '');
  //       this.toggleLoading(false);
  //     }
  //   );
  // }

  download(fileId: string) {
    this.isDownLoading = true;
    this.sessionService.setSessionData(ExportKey.Customer, true);
    this.fileService
      .downloadFile(fileId, 'crm_customer.xlsx', ExportKey.Customer)
      .pipe(catchError((e) => of(false)))
      .subscribe((res) => {
        this.toggleLoading(false);
        if (!res) {
          this.messageService.error(this.notificationMessage.error);
        }
        this.isDownLoading = false;
        this.sessionService.setSessionData(ExportKey.Customer, false);
      });
  }

  toggleLoading(isLoading: boolean) {
    this.loading.emit(isLoading);
    this.isLoading = isLoading;
  }

  onShowMenu(e) {
    this.isClickEvent = true;
  }

  onHideMenu(e) {
    this.isClickEvent = false;
  }
  getRmManager(listBranch, rmBlock?: string, isProp?: boolean) {
    this.listRmManager = [];
    this.rmApi
      .post('findAll', {
        page: 0,
        size: maxInt32,
        crmIsActive: true,
        branchCodes: listBranch?.filter((code) => !Utils.isStringEmpty(code)),
        rmBlock: rmBlock || '',
        rsId: this.objFunction?.rsId,
        scope: Scopes.VIEW,
        isAssignRm: _.get(this.dataSearch, 'isCheckGroupCBQL.value', false),
      })
      .subscribe((res) => {
        const listRm: any[] =
          res?.content?.map((item) => {
            return {
              code: item?.hrisEmployee?.employeeId || '',
              rmCode: item?.t24Employee?.employeeCode,
              displayName:
                Utils.trimNullToEmpty(item?.t24Employee?.employeeCode) +
                ' - ' +
                Utils.trimNullToEmpty(item?.hrisEmployee?.fullName),
            };
          }) || [];
        if (this.listBranches.length > 1) {
          this.listRmManager = [
            ...[
              { code: '', displayName: this.fields.all, rmCode: '' },
              { code: 'UN_ASSIGN', displayName: this.fields.unAssign, rmCode: 'UN_ASSIGN' },
            ],
            ...listRm,
          ];
        } else {
          this.listRmManager = listRm;
        }
        if (!this.listRmManager?.find((item) => item.code === this.currUser?.hrsCode)) {
          this.listRmManager.push({
            code: this.currUser?.hrsCode,
            displayName:
              Utils.trimNullToEmpty(this.currUser?.code) + ' - ' + Utils.trimNullToEmpty(this.currUser?.fullName),
          });
        }
        // this.searchForm.controls.hrsCode.setValue(_.get(this.listRmManager, '[0].code', null));
      });
  }
  navigateDetail(data){
    this.router.navigate([this.router.url, 'detail', data]);
  }
}
