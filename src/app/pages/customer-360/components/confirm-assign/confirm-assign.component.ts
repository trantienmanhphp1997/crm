import { forkJoin, of } from 'rxjs';
import { global } from '@angular/compiler/src/util';
import { Component, OnInit, Injector, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { FunctionCode, Scopes, SessionKey } from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { ConfirmAssignApi } from '../../apis/confirm-assign.api';
import { catchError } from 'rxjs/operators';
import * as _ from 'lodash';
import { FileService } from 'src/app/core/services/file.service';
import * as moment from 'moment';

@Component({
  selector: 'app-confirm-assign',
  templateUrl: './confirm-assign.component.html',
  styleUrls: ['./confirm-assign.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ConfirmAssignComponent extends BaseComponent implements OnInit {
  pageable: Pageable;
  listData: any[];
  listBranch: any[];
  listDivision: any[];
  prevParams: any;
  params = {
    page: 0,
    size: global?.userConfig?.pageSize,
    branchCode: '',
    customerCode: '',
    businessDate: null,
    blockCode: '',
    blockCodes: [],
    rsId: '',
    scope: Scopes.VIEW,
  };
  optionDate = {
    minDate: null,
    maxDate: null,
    disabledDates: [],
  };

  constructor(
    injector: Injector,
    private api: ConfirmAssignApi,
    private categoryService: CategoryService,
    private fileService: FileService
  ) {
    super(injector);
    this.isLoading = true;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_CONFIRM_ASSIGN}`);
    const rsIdCustomer360 = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`)?.rsId;
    this.params.rsId = rsIdCustomer360;
    this.listDivision = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM);
    this.listDivision =
      this.listDivision?.map((item) => {
        return { code: item.code, name: `${item.code || ''} - ${item.name || ''}` };
      }) || [];
    this.listDivision.unshift({ code: '', name: this.fields.all });
  }

  ngOnInit(): void {
    forkJoin([
      this.categoryService.getBranchesOfUser(this.params.rsId, Scopes.VIEW).pipe(catchError(() => of(undefined))),
      this.commonService.getBeforeDay().pipe(catchError(() => of(undefined))),
      this.commonService.getDayWorkingByDay({ effDate: new Date() }).pipe(catchError(() => of(undefined))),
    ]).subscribe(([listBranch, beforeDate, dateT0]) => {
      const toDay = moment().startOf('day');
      this.optionDate.minDate = beforeDate?.effDate ? new Date(beforeDate?.effDate) : toDay;
      this.optionDate.maxDate = dateT0 ? new Date(moment(dateT0).startOf('day').valueOf()) : toDay;
      const dayDiff = moment(this.optionDate.maxDate).diff(moment(this.optionDate.minDate), 'day');
      for (let index = 1; index < dayDiff; index++) {
        const time = this.optionDate.minDate.valueOf();
        this.optionDate.disabledDates.push(new Date(time + 86400000 * index));
      }
      this.listBranch =
        listBranch?.map((item) => {
          return { code: item.code, name: `${item.code || ''} - ${item.name || ''}` };
        }) || [];
      this.listBranch.unshift({ code: '', name: this.fields.all });
      this.params.businessDate = this.optionDate.minDate;
      this.search(true);
    });
  }

  search(isSearch?: boolean) {
    this.isLoading = true;
    let params: any;
    if (isSearch) {
      this.params.page = 0;
      params = this.params;
      params.blockCodes = _.isEmpty(params.blockCode) ? [] : [params.blockCode];
    } else {
      params = this.prevParams;
      params.page = this.params.page;
    }
    this.api.findAll(params).subscribe(
      (rows) => {
        this.prevParams = params;
        this.listData = rows?.content || [];
        this.listData?.forEach((item) => {
          item.branch = this.listBranch?.find((i) => i.code === item.branchCode)?.name;
          item.statusName = item.status ? this.fields.changed : this.fields.unChange;
          item.listRm = [{ code: item.currentRmCode, name: item.currentRmName }];
          item.rmCodeNew = item.currentRmCode;
        });
        this.pageable = {
          totalElements: rows?.totalElements,
          totalPages: rows?.totalPages,
          currentPage: rows?.number,
          size: global?.userConfig?.pageSize,
        };
        this.isLoading = false;
      },
      () => {
        this.listData = [];
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.page = pageInfo.offset;
    this.search(false);
  }

  confirm(item) {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        const data = new FormData();
        data.append('id', item.id);
        data.append('rmCode', item.rmCodeNew);
        this.api.approve(data).subscribe(
          () => {
            this.messageService.success(this.notificationMessage.success);
            this.search(false);
          },
          (e) => {
            this.isLoading = false;
            if (e?.error) {
              this.messageService.error(e?.error?.description);
            } else {
              this.messageService.error(this.notificationMessage.error);
            }
          }
        );
      }
    });
  }

  exportExcel() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+(this.pageable?.totalElements || 0) === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.isLoading = true;
    this.api.exportFile(this.prevParams).subscribe(
      (fileId) => {
        this.fileService.downloadFile(fileId, 'danh-sach-khach-hang.xlsx').subscribe((res) => {
          if (res) {
            this.isLoading = false;
          } else {
            this.isLoading = false;
            this.messageService.error(this.notificationMessage.error);
          }
        });
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }

  getRmByCustomer(item) {
    if (item.isGetRm) {
      return;
    }
    this.api
      .getRmAssign(
        _.get(item, 'customerCode'),
        _.get(item, 'branchCode'),
        moment(_.get(item, 'businessDate')).format('YYYY-MM-DD')
      )
      .subscribe((listCustomer) => {
        if (!_.isEmpty(listCustomer)) {
          item.listRm = listCustomer?.map((i) => {
            return { code: i.rmCode, name: `${i.rmCode || ''} - ${i.rmName || ''}` };
          });
        }
        item.isGetRm = true;
      });
  }
}
