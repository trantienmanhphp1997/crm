import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { global } from '@angular/compiler/src/util';
import { CommonCategory, Scopes, SessionKey, maxInt32, FunctionCode } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { forkJoin, of } from 'rxjs';
import _ from 'lodash';
import { Utils } from 'src/app/core/utils/utils';
import { RmApi } from '../../apis';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { RevenueShareApi } from '../../apis/revenue-share.api';
import { RmProfileService } from 'src/app/core/services/rm-profile.service';
import { FileService } from 'src/app/core/services/file.service';
import { RmBlockApi } from 'src/app/pages/rm/apis';
import { cleanDataForm } from 'src/app/core/utils/function';

@Component({
  selector: 'list-revenue-sharing-component',
  templateUrl: './list-revenue-sharing.component.html',
  styleUrls: ['./list-revenue-sharing.component.scss'],
})
export class ListRevenueSharingComponent extends BaseComponent implements OnInit {
  isLoading = false;
  @ViewChild('table') table: DatatableComponent;
  limit = global.userConfig.pageSize;
  pageable: Pageable;
  params: any = {
    pageNumber: 0,
    pageSize: global?.userConfig?.pageSize,
    rsId: '',
    scope: Scopes.VIEW,
  };
  objFunctionRm: any;
  objFunctionCustomer: any;

  constructor(
    injector: Injector,
    private rmApi: RmApi,
    private categoryService: CategoryService,
    private revenueShareApi: RevenueShareApi,
    private rmProfileService: RmProfileService,
    private fileService: FileService,
    private rmBlockApi: RmBlockApi
  ) {
    super(injector);
    this.objFunctionCustomer = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.objFunctionRm = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REVENUE_SHARE}`);
  }
  formSearch = this.fb.group({
    divisionCode: [''],
    customerCode: [''],
    customerName: [''],
    rmCode: [''],
    branchCode: [''],
    shareBranchCode: [''],
    shareRmCode: [''],
    shareStatus: [''],
    rsId: '',
    scope: Scopes.VIEW,
  });

  listDivision = [];
  listData = [];

  listRmManager = [];
  listBranchesShare = [];
  listBranchesManager = [];
  listStatusShare = [];
  listRmShare = [];

  checkAll = false;
  listSelected = [];
  countCheckedOnPage = 0;
  approvedList = false;
  listTitleConfig = [];
  divisionOfRm = [];
  prevParams: any;
  prop: any;

  ngOnInit() {
    this.prop = this.sessionService.getSessionData(FunctionCode.REVENUE_SHARE);
    if (this.prop) {
      this.prevParams = { ...this.prop?.prevParams };
      this.formSearch.patchValue(this.prevParams);
      this.listDivision = this.prop?.listDivision || [];
      this.listBranchesShare = this.prop?.listBranchesShare || [];
      this.listBranchesManager = this.prop?.listBranchesManager || [];
      this.listStatusShare = this.prop?.listStatusShare || [];
      this.listRmManager = this.prop?.listRmManager || [];
      this.listRmShare = this.prop?.listRmShare || [];
      this.listTitleConfig = this.prop?.listTitleConfig || [];
      this.approvedList = this.prop?.approvedList;
      this.divisionOfRm = this.prop?.divisionOfRm || [];
      this.search(true);
    } else {
      this.isLoading = true;
      forkJoin([
        this.commonService.getCommonCategory(CommonCategory.SHARE_REVENUE_LINE).pipe(catchError(() => of(undefined))),
        this.categoryService
          .getBranchesOfUser(this.objFunctionCustomer?.rsId, Scopes.VIEW)
          .pipe(catchError(() => of(undefined))),
        this.commonService
          .getCommonCategory(CommonCategory.ACC_REVENUE_SHARING_ST)
          .pipe(catchError(() => of(undefined))),
        this.commonService
          .getCommonCategory(CommonCategory.SHARE_REVENUE_ROLE_APPROVE_EMAIL)
          .pipe(catchError(() => of(undefined))),
        this.rmProfileService.getRolesByUser(this.currUser.username).pipe(catchError(() => of(undefined))),
        this.commonService
          .getCommonCategory(CommonCategory.SHARE_REVENUE_ROLE_INPUT)
          .pipe(catchError(() => of(undefined))),
        this.rmBlockApi
          .fetch({ page: 0, size: maxInt32, hrsCode: this.currUser?.hrsCode, isActive: true })
          .pipe(catchError(() => of(undefined))),
      ]).subscribe(
        ([
          listDivisionShare,
          listBranches,
          listStatusShare,
          listRoleShare,
          listRoleUser,
          listTitleConfig,
          divisionOfRm,
        ]) => {
          this.listTitleConfig = listTitleConfig?.content;

          listRoleShare = listRoleShare?.content;
          listRoleUser?.forEach((element) => {
            if (!_.isEmpty(listRoleShare?.find((item) => item.code === element.name))) {
              this.approvedList = true;
            }
          });
          // Khôi rm
          this.divisionOfRm =
            divisionOfRm?.content?.map((item) => {
              return {
                titleCode: item?.titleGroupCode,
                code: item.blockCode,
                displayName: item.blockCode + ' - ' + item.blockName,
              };
            }) || [];

          // chi nhánh
          listBranches = listBranches?.map((item) => {
            return { code: item.code, displayName: item.code + ' - ' + item.name };
          });
          listBranches?.unshift({ code: '', displayName: this.fields.all });
          this.listBranchesShare = listBranches;
          this.listBranchesManager = listBranches;

          listDivisionShare = listDivisionShare?.content;
          let listDivision =
            this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM)?.map((item) => {
              return {
                code: item.code,
                displayName: item.code + ' - ' + item.name,
              };
            }) || [];
          listDivision?.forEach((item) => {
            if (!_.isEmpty(listDivisionShare.find((i) => i.code === item.code))) {
              this.listDivision.push(item);
            }
          });
          this.formSearch.get('divisionCode').setValue(_.first(this.listDivision)?.code, { emitEvent: false });

          this.listStatusShare = listStatusShare?.content.map((item) => {
            return { code: item.code === 'ALL' ? '' : item.code, displayName: item.name, valueStatus: item.name };
          });
          this.listStatusShare.forEach((item) => {
            if (item.code === 'PENDING') {
              this.formSearch.get('shareStatus').setValue(item?.code);
            }
          });
          this.getRmManager([], this.formSearch.get('divisionCode').value);
          this.getRmShare([], this.formSearch.get('divisionCode').value);

          this.prop = {
            listDivision: this.listDivision,
            listBranchesShare: this.listBranchesShare,
            listBranchesManager: this.listBranchesManager,
            listStatusShare: this.listStatusShare,
            listTitleConfig: this.listTitleConfig,
            approvedList: this.approvedList,
            divisionOfRm: this.divisionOfRm,
          };
          this.sessionService.setSessionData(FunctionCode.REVENUE_SHARE, this.prop);
          this.search(true);
        }
      );
    }
  }

  ngAfterViewInit() {
    this.formSearch.controls.branchCode.valueChanges.subscribe((value) => {
      this.getRmManager([value], this.formSearch.controls.divisionCode.value);
    });

    this.formSearch.controls.shareBranchCode.valueChanges.subscribe((value) => {
      this.getRmShare([value], this.formSearch.controls.divisionCode.value);
    });

    this.formSearch.controls.divisionCode.valueChanges.subscribe((value) => {
      this.getRmManager([this.formSearch.controls.branchCode.value], value);
      this.getRmShare([this.formSearch.controls.shareBranchCode.value], value);
    });
  }

  search(isSearch: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.params.pageNumber = 0;
    }
    const params = {
      ...cleanDataForm(this.formSearch),
      rsId: this.objFunctionCustomer.rsId,
    };
    this.revenueShareApi.searchRevenueShare(params, this.params.pageNumber, this.params.pageSize).subscribe(
      (result) => {
        if (result) {
          this.prevParams = params;
          this.prop.prevParams = params;

          this.isLoading = false;
          this.listData = result.content || [];
          this.listData.forEach((item) => {
            item.statusCode = item.shareStatus;
            item.shareStatus = this.listStatusShare.find((i) => i.code === item.shareStatus).displayName;
          });
          this.countCheckedOnPage = 0;
          this.listData.forEach((item) => {
            if (
              this.listSelected.findIndex((itemChoose) => {
                return itemChoose.id === item.id;
              }) > -1
            ) {
              this.countCheckedOnPage += 1;
            }
          });
          this.checkAll = this.listData.length > 0 && this.countCheckedOnPage === this.listData.length;
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: this.limit,
          };
        }
      },
      (e) => {
        this.messageService.error(this.notificationMessage.error);
        this.isLoading = false;
      }
    );
  }

  onCheckboxFn(item, isChecked) {
    if (isChecked) {
      this.listSelected.push({ id: item.id, note: '', status: item.statusCode });
      this.countCheckedOnPage += 1;
    } else {
      this.listSelected = this.listSelected.filter((itemChoose) => {
        return itemChoose.id !== item.id;
      });
      this.countCheckedOnPage -= 1;
    }
    this.checkAll = this.countCheckedOnPage === this.listData.length;
  }

  isChecked(item) {
    return this.listSelected?.findIndex((itemSelected) => itemSelected.id === item.id) > -1;
  }

  onCheckboxAllFn(isChecked) {
    this.checkAll = isChecked;
    if (!isChecked) {
      this.countCheckedOnPage = 0;
    } else {
      this.countCheckedOnPage = this.listData.length;
    }
    for (const item of this.listData) {
      // item.isChecked = isChecked;
      if (isChecked) {
        if (
          this.listSelected.findIndex((itemChoose) => {
            return itemChoose.id === item.id;
          }) === -1
        ) {
          this.listSelected.push({ id: item.id, note: '', status: item.statusCode });
        }
      } else {
        this.listSelected = this.listSelected.filter((itemChoose) => {
          return itemChoose.id !== item.id;
        });
      }
    }
  }

  isApprovedList() {
    if (this.listSelected.length === 0) {
      this.messageService.warn(this.notificationMessage.noRecordSelected);
      return;
    } else if (this.listSelected?.find((item) => item.status !== 'PENDING')) {
      this.messageService.warn(this.notificationMessage.peddingRevenShare);
      return;
    } else {
      this.isLoading = true;
      const param = {
        approvalHistoryDTOList: this.listSelected.map((item) => {
          return _.omit(item, ['status']);
        }),
      };
      this.revenueShareApi.approveAllRevenueShare(param).subscribe(
        (res) => {
          this.listSelected = [];
          this.checkAll = false;
          this.isLoading = false;
          this.messageService.success(this.notificationMessage.success);
          this.search(true);
        },
        (e) => {
          this.listSelected = [];
          this.checkAll = false;
          this.isLoading = false;
          this.messageService.error(this.notificationMessage.error);
        }
      );
    }
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.pageNumber = pageInfo.offset;
    this.search(false);
  }

  getRmManager(listBranch, rmBlock?: string) {
    this.formSearch.get('rmCode').disable();
    this.listRmManager = [];
    this.formSearch.controls.rmCode.setValue('');
    this.rmApi
      .post('findAll', {
        page: 0,
        size: maxInt32,
        crmIsActive: true,
        branchCodes: listBranch?.filter((code) => !Utils.isStringEmpty(code)),
        rmBlock: rmBlock || '',
        rsId: this.objFunctionCustomer?.rsId,
        scope: Scopes.VIEW,
      })
      .subscribe((res) => {
        const listRm: any[] =
          res?.content?.map((item) => {
            if (!_.isEmpty(item?.t24Employee?.employeeCode)) {
              return {
                code: item?.t24Employee?.employeeCode || '',
                displayName:
                  Utils.trimNullToEmpty(item?.t24Employee?.employeeCode) +
                  ' - ' +
                  Utils.trimNullToEmpty(item?.hrisEmployee?.fullName),
              };
            }
          }) || [];

        this.listRmManager = listRm;
        this.listRmManager?.unshift({ code: '', displayName: this.fields.all });
        this.formSearch.controls.rmCode.setValue('');
        this.formSearch.get('rmCode').enable();
        this.prop = { ...this.prop, listRmManager: this.listRmManager };
        this.sessionService.setSessionData(FunctionCode.REVENUE_SHARE, this.prop);
      });
  }

  getRmShare(listBranch, rmBlock?: string) {
    this.formSearch.get('shareRmCode').disable();
    this.listRmShare = [];
    this.formSearch.controls.shareRmCode.setValue('');
    this.rmApi
      .post('findAll', {
        page: 0,
        size: maxInt32,
        crmIsActive: true,
        branchCodes: listBranch?.filter((code) => !Utils.isStringEmpty(code)),
        rmBlock: rmBlock || '',
        rsId: this.objFunctionCustomer?.rsId,
        scope: Scopes.VIEW,
        searchRevenue: true,
        customerBranchCode: _.trim(this.formSearch.get('shareBranchCode').value),
      })
      .subscribe((res) => {
        const listRm: any[] =
          res?.content?.map((item) => {
            if (!_.isEmpty(item?.t24Employee?.employeeCode)) {
              return {
                code: item?.t24Employee?.employeeCode || '',
                displayName:
                  Utils.trimNullToEmpty(item?.t24Employee?.employeeCode) +
                  ' - ' +
                  Utils.trimNullToEmpty(item?.hrisEmployee?.fullName),
              };
            }
          }) || [];

        this.listRmShare = listRm;
        this.listRmShare?.unshift({ code: '', displayName: this.fields.all });
        this.formSearch.get('shareRmCode').enable();
        this.formSearch.controls.shareRmCode.setValue('');
        this.prop = { ...this.prop, listRmShare: this.listRmShare };
        this.sessionService.setSessionData(FunctionCode.REVENUE_SHARE, this.prop);
      });
  }

  viewEdit(data, isEdit?: boolean) {
    if (data.statusCode === 'PENDING') {
      if (this.approvedList) {
        if (isEdit) {
          this.router.navigate([this.router.url, 'edit', data.id], {
            skipLocationChange: true,
          });
        } else {
          this.router.navigate([this.router.url, 'approve', data.id]);
        }
      } else {
        this.router.navigate([this.router.url, 'edit', data.id], {
          skipLocationChange: true,
        });
      }
    } else if (data.statusCode === 'APPROVE' || data.statusCode === 'END') {
      this.router.navigate([this.router.url, 'approve', data.id]);
    } else if (data.statusCode === 'REFUSE') {
      this.router.navigate([this.router.url, 'edit', data.id], {
        skipLocationChange: true,
      });
    }
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      this.router.navigate([this.router.url, 'approve', item.id]);
    }
  }

  isShowEdit(data) {
    return (data.statusCode === 'PENDING' || data.statusCode === 'REFUSE') && this.isRoleRevenueEdit(data);
  }

  isRoleRevenueEdit(data) {
    const titleCodeConfig = this.listTitleConfig?.find((item) => item.code === `TITLE_${data.divisionCode}`)?.value;
    const titleOfDivision = this.divisionOfRm?.find((item) => item.code === data.divisionCode)?.titleCode;
    return this.objFunctionCustomer.revenueShare || titleCodeConfig?.includes(titleOfDivision);
  }

  exportFile() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+this.pageable?.totalElements <= 0) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.isLoading = true;
    const paramExport = {
      ...this.formSearch.value,
      rsId: this.objFunctionCustomer.rsId,
    };
    paramExport.pageNumber = 0;
    paramExport.pageSize = maxInt32;
    this.revenueShareApi.excelRevenueShare(paramExport).subscribe(
      (res) => {
        if (res) {
          this.download(res);
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(_.get(this.notificationMessage, 'export_error'));
        this.isLoading = false;
      }
    );
  }

  download(fileId: string) {
    this.fileService.downloadFile(fileId, 'danh-sach-chia-se-doanh-thu.xlsx').subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }

  checkUserCase(data) {
    const titleCodeConfig = this.listTitleConfig?.find((item) => item.code === `TITLE_${data.divisionCode}`)?.value;
    const titleOfDivision = this.divisionOfRm?.find((item) => item.code === data.divisionCode)?.titleCode;
    return titleCodeConfig?.includes(titleOfDivision);
  }
}
