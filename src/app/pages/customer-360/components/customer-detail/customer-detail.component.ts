import { forkJoin } from 'rxjs';
import { Component, OnInit, Injector, Input } from '@angular/core';
import { FormArray, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SessionKey, maxInt32 } from 'src/app/core/utils/common-constants';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CustomerApi } from '../../apis';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import _ from 'lodash';

@Component({
  selector: 'app-customer-detail',
  templateUrl: 'customer-detail.compoment.html',
  styleUrls: ['customer-detail.component.scss'],
  providers: [NgbActiveModal, NgbModal],
})
export class CustomerDetailComponent extends BaseComponent implements OnInit {
  listCustomerGroup = [];
  listCustomerResources = [];
  listStatus = [];
  listBankAccount = [];
  listContact = [];
  listPersonInCharge = [];
  uploadedFiles: any[] = [];
  isLoading = false;
  data: any;
  form = this.fb.group({
    phone: [''],
    customerCode: [''],
    customerName: [''],
    customerType: ['0'],
    businessRegistrationNumber: [''],
    dateOfIncorporation: [''],
    email: [''],
    taxCode: [''],
    staffSize: [''],
    address: [''],
    customerGroup: [''],
    customerResources: [''],
    status: [''],
    description: [''],
    name: [''],
    bankAccount: [''],
    bankName: [''],
    personInCharge: [''],
    personInChargePhone: [''],
    personInChargeEmail: [''],
  });
  ColumnMode = ColumnMode;
  @Input() fileInfo = {};
  constructor(injector: Injector, private customerService: CustomerApi) {
    super(injector);
  }
  ngOnInit(): void {
    this.isLoading = true;
    this.listCustomerGroup = _.map(this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM), (item) => {
      return {
        titleCode: item.titleCode,
        levelCode: item.levelCode,
        code: item.code,
        displayName: `${item.code} - ${item.name}`,
      };
    });
    this.customerService.getCustomerById(this.route.snapshot.paramMap.get('id')).subscribe(
      (data) => {
        this.data = data;
        this.form.patchValue({
          customerCode:this.data.systemInfo.code,
          customerName:this.data.customerInfo.fullName,
          address:this.data.customerInfo.address,
          customerGroup:this.data.systemInfo.accountType,
        });
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }
  back() {
    this.location.back();
  }
  deleteRole(data) {}
  addRow() {
    
    this.listBankAccount = [...[{}]]
  }
  deleteRoleContact(data) {}
  addRowContact() {}
  uploadFile(event, fileUpload) {
    const strType = 'doc, docx, xls, xlsx, txt, pdf, png, jpg, bmp, rar, zip';
    for (const file of event.files) {
      const type = _.last(file.name?.split('.'))?.toLowerCase();
      if (file.size > 10485760 || !strType.includes(type)) {
        this.messageService.error('File không đúng định dạng/File vượt quá giới hạn cho phép');
        fileUpload.clear();
        return;
      }
      const fileOld = _.find(
        this.uploadedFiles,
        (item) =>
          item?.file?.lastModified === file.lastModified && item?.fileName === file.name && item?.size === file.size
      );
      if (!fileOld) {
        this.uploadedFiles.push({
          fileId: null,
          fileName: file.name,
          size: file.size,
          idFake: _.size(this.uploadedFiles),
          file,
        });
      }
    }
    fileUpload.clear();
  }
}
