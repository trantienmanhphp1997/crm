import { CustomerAssignmentApi } from '../../apis';
import { global } from '@angular/compiler/src/util';
import { Component, HostBinding, OnInit, ViewChild, Injector } from '@angular/core';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import _ from 'lodash';
import {
  CommonCategory,
  Division,
  FunctionCode,
  functionUri,
  maxInt32,
  Scopes,
  SessionKey,
  typeExcel,
} from 'src/app/core/utils/common-constants';
import { FileService } from 'src/app/core/services/file.service';
import { Utils } from 'src/app/core/utils/utils';
import { BaseComponent } from 'src/app/core/components/base.component';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-assign-by-product',
  templateUrl: './assign-by-product.component.html',
  styleUrls: ['./assign-by-product.component.scss'],
})
export class AssignByProductComponent extends BaseComponent implements OnInit {
  @ViewChild('tableSuccess') tableSuccess: DatatableComponent;
  @ViewChild('tableError') tableError: DatatableComponent;

  constructor(injector: Injector, private api: CustomerAssignmentApi, private fileService: FileService) {
    super(injector);
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
    this.obj = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_ASSIGNMENT}`);
  }
  @HostBinding('class.app__right-content') appRightContent = true;
  notificationMessage: any;
  fields: any;
  obj: any;
  isLoading = false;
  fileName: string;
  isFile = false;
  fileImport: File;
  files: any;
  ColumnMode = ColumnMode;
  listDataSuccess = [];
  listDataError = [];
  prop: any;
  limit = global.userConfig.pageSize;
  pageSuccess: Pageable;
  pageError: Pageable;
  paramSuccess = {
    size: this.limit,
    page: 0,
    search: '',
    isSuccess: true,
    fileId: '',
  };
  prevSuccessParams = this.paramSuccess;
  textSearchSuccess = '';
  paramError = {
    size: this.limit,
    page: 0,
    search: '',
    isSuccess: false,
    fileId: '',
  };
  prevErrorParams = this.paramError;
  textSearchError = '';
  fileId: string;
  isUpload = false;
  searchSuccessDone = true;
  searchErrorDone = true;
  messages = global.messageTable;
  productCode = '';
  categoryCommon: any;
  listProduct = [];
  productType = [];
  customerOrContract = '';

  ngOnInit(): void {
    this.getDataFilter();
  }

  getDataFilter() {
    this.isLoading = true;
    this.categoryCommon = this.sessionService.getSessionData(FunctionCode.CUSTOMER_360_ASSIGN_BY_PRODUCT);
    if (!this.categoryCommon) {
      const listCommonCategory = [CommonCategory.PRODUCT_DELIVERED];
      this.commonService.getCommonCategoryByListCode(listCommonCategory).subscribe(
        (result) => {
          const listProduct = [];
          if (result?.length > 0) {
            result.forEach((category) => {
              if (category.commonCategoryCode == CommonCategory.PRODUCT_DELIVERED) {
                listProduct.push(category);
              }
            });
          }

          this.sessionService.setSessionData(FunctionCode.CUSTOMER_360_ASSIGN_BY_PRODUCT, {
            listProduct,
          });
          this.setDataFilter();
        },
        () => {
          this.isLoading = false;
        }
      );
    } else {
      this.setDataFilter();
    }
  }

  setDataFilter() {
    this.categoryCommon = this.sessionService.getSessionData(FunctionCode.CUSTOMER_360_ASSIGN_BY_PRODUCT);

    const listProductSort = this.categoryCommon?.listProduct;
    this.listProduct = this.sortArr(listProductSort) || [];
    if (listProductSort.length > 0) {
      this.productCode = listProductSort[0].value;
      this.customerOrContract = listProductSort[0].description;
    }

    this.isLoading = false;
  }

  changeProduct() {
    let proSelected = this.listProduct.filter((item)=> {
      return item.value === this.productCode
    });
    this.customerOrContract = proSelected[0].description;
  }

  sortArr(arr) {
    return arr.sort((a, b) => (a.orderNum > b.orderNum ? 1 : b.orderNum > a.orderNum ? -1 : 0));
  }

  searchSuccess(isSearch: boolean) {
    this.isLoading = true;
    this.searchSuccessDone = false;
    let params: any = {};
    if (isSearch) {
      this.paramSuccess.page = 0;
      this.textSearchSuccess = this.textSearchSuccess.trim();
      this.paramSuccess.search = this.textSearchSuccess;
      params = this.paramSuccess;
    } else {
      if (_.get(this.prevSuccessParams, 'size') === maxInt32) {
        _.set(this.prevSuccessParams, 'size', _.get(global, 'userConfig.pageSize'));
      }
      params = this.prevSuccessParams;
    }
    this.api.search(params).subscribe(
      (res) => {
        this.prevSuccessParams = params;
        this.listDataSuccess = _.get(res, 'content') || [];
        this.pageSuccess = {
          totalElements: _.get(res, 'totalElements'),
          totalPages: _.get(res, 'totalPages'),
          currentPage: _.get(res, 'number'),
          size: _.get(global, 'userConfig.pageSize'),
        };
        this.searchSuccessDone = true;
        if (this.searchErrorDone && this.searchSuccessDone) {
          this.isLoading = false;
        }
      },
      () => {
        this.searchSuccessDone = true;
        if (this.searchErrorDone && this.searchSuccessDone) {
          this.isLoading = false;
        }
      }
    );
  }

  searchError(isSearch: boolean) {
    this.isLoading = true;
    this.searchErrorDone = false;
    let params: any = {};
    if (isSearch) {
      this.paramError.page = 0;
      this.textSearchError = this.textSearchError.trim();
      this.paramError.search = this.textSearchError;
      params = this.paramError;
    } else {
      if (_.get(this.prevErrorParams, 'size') === maxInt32) {
        _.set(this.prevErrorParams, 'size', _.get(global, 'userConfig.pageSize'));
      }
      params = this.prevErrorParams;
    }
    this.api.search(params).subscribe(
      (res) => {
        this.listDataError = _.get(res, 'content') || [];
        this.pageError = {
          totalElements: _.get(res, 'totalElements'),
          totalPages: _.get(res, 'totalPages'),
          currentPage: _.get(res, 'number'),
          size: _.get(global, 'userConfig.pageSize'),
        };
        this.searchErrorDone = true;
        if (this.searchErrorDone && this.searchSuccessDone) {
          this.isLoading = false;
        }
      },
      () => {
        this.searchErrorDone = true;
        if (this.searchErrorDone && this.searchSuccessDone) {
          this.isLoading = false;
        }
      }
    );
  }

  setPage(pageInfo, type) {
    if (type === 'success') {
      _.set(this.paramSuccess, 'page', _.get(pageInfo, 'offset'));
      this.searchSuccess(false);
    } else {
      _.set(this.paramError, 'page', _.get(pageInfo, 'offset'));
      this.searchError(false);
    }
  }

  handleFileInput(files) {
    if (files?.item(0)?.size > 10485760) {
      this.messageService.warn(this.notificationMessage.ECRM005);
      return;
    }
    if (files?.item(0) && !typeExcel.includes(files?.item(0)?.type)) {
      this.messageService.error(this.notificationMessage.CANNOT_READ_DATA_FROM_FILE);
      return;
    }
    if (_.size(files) > 0) {
      this.isFile = true;
      this.fileImport = files.item(0);
      this.fileName = files.item(0).name;
    } else {
      this.isFile = false;
    }
  }

  clearFile() {
    this.isUpload = false;
    this.isFile = false;
    this.fileName = null;
    this.fileImport = null;
    this.files = null;
    this.listDataSuccess = [];
    this.listDataError = [];
    this.fileId = undefined;
    this.pageError = undefined;
    this.pageSuccess = undefined;
    this.textSearchSuccess = '';
    this.textSearchError = '';
    this.paramError.search = '';
    this.paramSuccess.search = '';

    this.prevErrorParams = this.paramError;
    _.set(this.prevErrorParams, 'fileId', '');
    this.prevSuccessParams = this.paramSuccess;
    _.set(this.prevSuccessParams, 'fileId', '');
  }

  importFile() {
    if (this.isFile && !this.isUpload) {
      this.isLoading = true;
      const formData: FormData = new FormData();
      formData.append('file', this.fileImport);
      const params = {
        rsId: _.get(this.obj, 'rsId'),
        scope: Scopes.IMPORT,
        productType: this.productCode,
      };
      // console.log(params);
      if (this.customerOrContract === '1') {
        this.api.importByProductLD(formData, params).subscribe(
          (res) => {
            this.fileId = _.get(res, 'requestId');
            this.paramError.fileId = this.fileId;
            this.paramSuccess.fileId = this.fileId;
            this.checkImportSuccess(this.fileId);
          },
          (e) => {
            if (e?.error) {
              this.messageService.error(e?.error?.description);
            } else {
              this.messageService.error(this.notificationMessage.error);
            }
            this.listDataError = [];
            this.listDataSuccess = [];
            this.isLoading = false;
          }
        );
      }

      if (this.customerOrContract === '2') {
        this.api.importByProduct(formData, params).subscribe(
          (res) => {
            this.fileId = _.get(res, 'requestId');
            this.paramError.fileId = this.fileId;
            this.paramSuccess.fileId = this.fileId;
            this.checkImportSuccess(this.fileId);
          },
          (e) => {
            if (e?.error) {
              this.messageService.error(e?.error?.description);
            } else {
              this.messageService.error(this.notificationMessage.error);
            }
            this.listDataError = [];
            this.listDataSuccess = [];
            this.isLoading = false;
          }
        );
      }
      
    }
  }

  confirmDialog() {
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.componentInstance.message = this.notificationMessage.confirm_assign_by_product;
    confirm.result
      .then((res) => {
        if (res) {
          this.save();
        }
      })
      .catch(() => {});
  }

  save() {
    if (!this.fileId || this.pageSuccess.totalElements === 0) {
      return;
    }
    this.isLoading = true;
    const rsId = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_ASSIGN_BY_PRODUCT}`)?.rsId;
    const data = {
      fileId: this.fileId,
      scope: Scopes.VIEW,
      rsId: rsId ? rsId : null,
    };
    if (this.customerOrContract === '1') {
      this.api.writeDataByProductLD(data).subscribe(
        () => {
          this.messageService.success(this.notificationMessage.success);
          this.isLoading = false;
          this.clearFile();
        },
        (e) => {
          if (e?.error) {
            this.messageService.warn(e?.error?.description);
          } else {
            this.messageService.error(this.notificationMessage.error);
          }
          this.isLoading = false;
        }
      );
    }

    if (this.customerOrContract === '2') {
      this.api.writeDataByProduct(data).subscribe(
        () => {
          this.messageService.success(this.notificationMessage.success);
          this.isLoading = false;
          this.clearFile();
        },
        (e) => {
          if (e?.error) {
            this.messageService.warn(e?.error?.description);
          } else {
            this.messageService.error(this.notificationMessage.error);
          }
          this.isLoading = false;
        }
      );
    }
  }

  checkImportSuccess(fileId: string) {
    this.api.checkFileImport(fileId).subscribe(
      (res) => {
        if (res?.status === 'COMPLETE') {
          this.isUpload = true;
          this.isLoading = false;
          this.searchError(true);
          this.searchSuccess(true);
        } else if (_.get(res, 'status') === 'FAIL') {
          if (res?.msgError?.includes('FILE_DOES_NOT_EXCEED_RECORDS')) {
            const maxRecord = res?.msgError?.replace('FILE_DOES_NOT_EXCEED_RECORDS_', '');
            this.translate.get('notificationMessage.FILE_LIMITED_RECORDS', { number: maxRecord }).subscribe((res) => {
              this.messageService.error(res);
            });
          } else if (res?.msgError === 'FILE_NO_CONTENT_EXCEPTION') {
            this.messageService.error(this.notificationMessage.FILE_NO_CONTENT_EXCEPTION);
          } else {
            this.messageService.error(this.notificationMessage.CANNOT_READ_DATA_FROM_FILE);
          }
          this.isLoading = false;
        } else if (_.get(res, 'status') === 'PENDING') {
          const timer = setTimeout(() => {
            this.checkImportSuccess(fileId);
            clearTimeout(timer);
          }, 5000);
        }
      },
      () => {
        this.messageService.error(this.notificationMessage.error);
        this.isLoading = false;
      }
    );
  }

  exportSuccess() {
    if (Utils.isArrayEmpty(this.listDataSuccess)) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    const params = this.prevSuccessParams;
    _.set(params, 'size', maxInt32);
    _.set(params, 'page', 0);
    this.isLoading = true;
    this.api.exportFile(params).subscribe(
      (result) => {
        this.download(true, result);
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }

  exportError() {
    if (Utils.isArrayEmpty(this.listDataError)) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    const params = this.prevErrorParams;
    _.set(params, 'size', maxInt32);
    _.set(params, 'page', 0);
    this.isLoading = true;
    this.api.exportFile(params).subscribe(
      (result) => {
        this.download(false, result);
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }

  download(type, fileId) {
    const fileName = type ? 'danh-sach-thanh-cong.xlsx' : 'danh-sach-loi.xlsx';
    this.fileService.downloadFile(fileId, fileName).subscribe(
      (res) => {
        this.isLoading = false;
        if (!res) {
          this.messageService.error(this.notificationMessage.error);
        }
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  downloadTemplate() {
    if (this.customerOrContract === '2') {
      window.open('/assets/template/account_gan_rm_template.xlsx', '_self');
    }
    if (this.customerOrContract === '1') {
      window.open('/assets/template/account_gan_rm_template_hd.xlsx', '_self');
    }
  }

  getValue(row, key) {
    return _.get(row, key);
  }
}
