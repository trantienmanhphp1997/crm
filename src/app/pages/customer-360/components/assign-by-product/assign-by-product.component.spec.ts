import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignByProductComponent } from './assign-by-product.component';

describe('AssignByProductComponent', () => {
  let component: AssignByProductComponent;
  let fixture: ComponentFixture<AssignByProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignByProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignByProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
