import { Component, ElementRef, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import _ from 'lodash';
import * as XLSX from 'xlsx';
import { formatNumber } from '@angular/common';
import { Utils } from 'src/app/core/utils/utils';
import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';
import * as moment from 'moment';

@Component({
  selector: 'customer-loan-balance',
  templateUrl: './customer-loan-balance.component.html',
  styleUrls: ['./customer-loan-balance.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CustomerLoanBalanceComponent implements OnInit {
  constructor(private messageService: NotifyMessageService) {}
  @ViewChild('customerLoanBalance') customerLoanBalance: ElementRef;
  @Input() model: any;
  @Input() notificationMessage: any;
  models: Array<any>;
  show_data: boolean;
  ngOnInit(): void {
    this.models = _.get(this.model, 'customerCICDTOS');
    this.show_data = Utils.isArrayEmpty(this.models) ? false : true;
  }

  export() {
    if (!this.show_data) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    } else {
      let fileName = 'Thông tin dư nợ vay';
      const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.customerLoanBalance.nativeElement, { raw: true });
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
      XLSX.writeFile(wb, `${fileName}.xlsx`);
    }
  }

  getValue(item, key) {
    return Utils.isNull(_.get(item, key)) ? '---' : _.get(item, key);
  }

  getCostValue(data, key) {
    return Utils.isNull(_.get(data, key)) ? '---' : formatNumber(_.get(data, key), 'en', '0.0-2');
  }

  getTruncateValue(item, key, limit) {
    return Utils.isStringNotEmpty(_.get(item, key)) ? Utils.truncate(_.get(item, key), limit) : '---';
  }

  getSubstringValue(item, key) {
    return Utils.isNull(_.get(item, key)) ? '---' : Utils.subString(_.get(item, key));
  }

  convertDateData(str) {
    const date = moment(str, 'DD-MMM-YY').toDate();
    return date;
  }
}
