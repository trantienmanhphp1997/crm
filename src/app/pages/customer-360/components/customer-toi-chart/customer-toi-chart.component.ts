import { Component, OnInit, Injector, Input, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import {CustomerType, EChartType, FunctionCode, Scopes} from 'src/app/core/utils/common-constants';
import { EChartsOption } from 'echarts';
import _ from 'lodash';
import { formatNumber } from '@angular/common';
import * as moment from 'moment';
import {CustomerApi} from "../../apis";

@Component({
  selector: 'app-customer-toi-chart',
  templateUrl: './customer-toi-chart.component.html',
  styleUrls: ['./customer-toi-chart.component.scss'],
})
export class CustomerToiChartComponent extends BaseComponent implements OnInit, OnChanges {
  listBranchCode = [];
  @Input() viewChange: EventEmitter<boolean> = new EventEmitter();
  data: any[];
  option: EChartsOption;
  title: string;

  constructor(injector: Injector,private customerApi: CustomerApi) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);

  }

  ngOnInit(): void {
    this.isLoading = true;
    const params = {
      customerTypes: [CustomerType.INDIV, CustomerType.NHS],
      rsId: this.objFunction?.rsId,
      scope: Scopes.VIEW,
    };
    this.customerApi.getTOIDataChart(params).subscribe(dataToi => {
      this.data = dataToi;
      const date = _.get(_.first(this.data), 'monthValue');
      this.title = `${_.get(this.fields, 'toi_chart')} ${
        !_.isEmpty(date?.trim()) ? moment(date, 'YYYY/MM').format('MM/YYYY') : '---'
      }`;
      this.data = this.data || [];
      let nameGapSize = 35;
      for (const item of this.data) {
        if (item.value?.toString()?.length >= 6) {
          nameGapSize = 70;
        } else if (item.value?.toString()?.length >= 4 && nameGapSize < 55) {
          nameGapSize = 55;
        }
      }
      this.handleMapData(nameGapSize);
      this.isLoading = false;
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    // const date = _.get(_.first(this.data), 'monthValue');
    // this.title = `${_.get(this.fields, 'toi_chart')} ${
    //   !_.isEmpty(date?.trim()) ? moment(date, 'YYYY/MM').format('MM/YYYY') : '---'
    // }`;
    // this.data = this.data || [];
    // let nameGapSize = 35;
    // for (const item of this.data) {
    //   if (item.value?.toString()?.length >= 6) {
    //     nameGapSize = 70;
    //   } else if (item.value?.toString()?.length >= 4 && nameGapSize < 55) {
    //     nameGapSize = 55;
    //   }
    // }
    // this.handleMapData(nameGapSize);
  }

  handleMapData(nameGapSize) {
    if (_.size(this.data) > 0) {
      this.option = {
        grid: {
          left: '10%',
          right: '0%',
          containLabel: true,
        },
        xAxis: {
          type: 'category',
          name: _.get(this.fields, 'millionUnit'),
          nameGap: 30,
          nameLocation: 'middle',
          nameTextStyle: {
            fontSize: 16,
            fontWeight: 'bold',
            fontFamily: 'Lato',
          },
          data: _.map(this.data, (item) => item.label) || [],
        },

        yAxis: {
          name: _.get(this.fields, 'customer'),
          nameGap: nameGapSize,
          // nameGap: 15,
          nameLocation: 'middle',
          nameTextStyle: {
            fontSize: 16,
            fontWeight: 'bold',
            fontFamily: 'Lato',
          },
          type: 'value',
          minInterval: 1,
        },
        series: [
          {
            data: _.map(this.data, (item) => item.value) || [],
            type: EChartType.Bar,
            barWidth: 28,
          },
        ],
        tooltip: {
          trigger: 'item',
          formatter: (params) => {
            return `${formatNumber(params.value || 0, 'en', '1.0-0')}`;
          },
        },
      };
    } else {
      this.data = undefined;
    }
  }
}
