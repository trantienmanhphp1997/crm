import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerToiChartComponent } from './customer-toi-chart.component';

describe('CustomerToiChartComponent', () => {
  let component: CustomerToiChartComponent;
  let fixture: ComponentFixture<CustomerToiChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerToiChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerToiChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
