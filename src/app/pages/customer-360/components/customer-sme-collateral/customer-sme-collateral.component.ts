import {
  Component,
  EventEmitter,
  ElementRef,
  Input,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { global } from '@angular/compiler/src/util';
import _ from 'lodash';
import { formatNumber } from '@angular/common';
import { Utils } from 'src/app/core/utils/utils';
import { CustomerApi } from '../../apis';
import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';
import * as XLSX from 'xlsx';
import * as moment from 'moment';

@Component({
  selector: 'customer-sme-collateral',
  templateUrl: './customer-sme-collateral.component.html',
  styleUrls: ['./customer-sme-collateral.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CustomerSmeCollateralComponent implements OnInit {
  constructor(private api: CustomerApi, private messageService: NotifyMessageService) {}

  @Input() code: string;
  @Input() registrationNumber: string;
  @ViewChild('smeCollateralTable') smeCollateralTable: ElementRef;
  @Input() loading: boolean;
  @Output() loadingChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() first: boolean;
  @Output() firstChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() notificationMessage: any;
  @Input() fields: any;
  messages = global.messageTable;
  models: Array<any>;
  @Input() showData: boolean;
  @Output() showDataChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() data: any;
  @Output() dataChange: EventEmitter<any> = new EventEmitter<any>();

  ngOnInit(): void {
    if (this.first) {
      this.loadingChange.emit(true);
      this.api.getCollateralInformation(this.registrationNumber).subscribe(
        (response) => {
          this.data = response;
          this.dataChange.emit(this.data);
          this.models = _.get(response, 'listCollateralBusiness');
          if (Utils.isArrayNotEmpty(this.models)) {
            this.showData = true;
            this.showDataChange.emit(this.showData);
          }
          this.first = false;
          this.loadingChange.emit(false);
          this.firstChange.emit(this.first);
        },
        () => {
          this.messageService.error(_.get(this.notificationMessage, 'E001'));
          this.loadingChange.emit(false);
        }
      );
    } else {
      this.models = _.get(this.data, 'listCollateralBusiness');
    }
  }

  export() {
    this.loadingChange.emit(true);
    if (!this.showData) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      this.loadingChange.emit(false);
      return;
    } else {
      let fileName = `tài sản đảm bảo_TCTD_${this.code}`;
      var data = [
        [
          `${_.get(this.fields, 'index')}`,
          `${_.get(this.fields, 'bank')}`,
          `${_.get(this.fields, 'realEstate')}`,
          '',
          `${_.get(this.fields, 'meansOfTransportation')}`,
          '',
          `${_.get(this.fields, 'machineDevices')}`,
          '',
          `${_.get(this.fields, 'inventory')}`,
          '',
          `${_.get(this.fields, 'debtClaims')}`,
          '',
          `${_.get(this.fields, 'valuablePapers')}`,
          '',
          `${_.get(this.fields, 'otherAsset')}`,
        ],
        [
          '',
          '',
          `${_.get(this.fields, 'sl')}`,
          `${_.get(this.fields, 'gt')}`,
          `${_.get(this.fields, 'sl')}`,
          `${_.get(this.fields, 'gt')}`,
          `${_.get(this.fields, 'sl')}`,
          `${_.get(this.fields, 'gt')}`,
          `${_.get(this.fields, 'sl')}`,
          `${_.get(this.fields, 'gt')}`,
          `${_.get(this.fields, 'sl')}`,
          `${_.get(this.fields, 'gt')}`,
          `${_.get(this.fields, 'sl')}`,
          `${_.get(this.fields, 'gt')}`,
          `${_.get(this.fields, 'sl')}`,
          `${_.get(this.fields, 'gt')}`,
        ],
      ];
      _.forEach(this.models, (x, index) => {
        data.push([
          `${index + 1}`,
          `${_.get(x, 'creditOrgName') || ''}`,
          `${_.get(x, 'amountRealEstate') || ''}`,
          `${_.get(x, 'valueRealEstate') || ''}`,
          `${_.get(x, 'amountTransport') || ''}`,
          `${_.get(x, 'valueTransport') || ''}`,
          `${_.get(x, 'amountMachine') || ''}`,
          `${_.get(x, 'valueMachine') || ''}`,
          `${_.get(x, 'amountInventories') || ''}`,
          `${_.get(x, 'valueInventories') || ''}`,
          `${_.get(x, 'amountPayable') || ''}`,
          `${_.get(x, 'valuePayable') || ''}`,
          `${_.get(x, 'amountPaperValuation') || ''}`,
          `${_.get(x, 'valuePaperValuation') || ''}`,
          `${_.get(x, 'amountOtherAssets') || ''}`,
          `${_.get(x, 'valueOtherAssets') || ''}`,
        ]);
      });
      data.push([
        '',
        `${_.get(this.fields, 'totalValue') || ''}`,
        `${_.get(this.data, 'totalAmountRealEstate') || ''}`,
        `${_.get(this.data, 'totalValueRealEstate') || ''}`,
        `${_.get(this.data, 'totalAmountTransport') || ''}`,
        `${_.get(this.data, 'totalValueTransport') || ''}`,
        `${_.get(this.data, 'totalAmountMachine') || ''}`,
        `${_.get(this.data, 'totalValueMachine') || ''}`,
        `${_.get(this.data, 'totalAmountInventories') || ''}`,
        `${_.get(this.data, 'totalValueInventories') || ''}`,
        `${_.get(this.data, 'totalAmountPayable') || ''}`,
        `${_.get(this.data, 'totalValuePayable') || ''}`,
        `${_.get(this.data, 'totalAmountPaperValuation') || ''}`,
        `${_.get(this.data, 'totalValuePaperValuation') || ''}`,
        `${_.get(this.data, 'totalAmountOtherAssets') || ''}`,
        `${_.get(this.data, 'totalValueOtherAssets') || ''}`,
      ]);

      var ws = XLSX.utils.aoa_to_sheet(data);
      /* add merges */
      ws['!merges'] = [
        { e: { r: 1, c: 0 }, s: { r: 0, c: 0 } },
        { e: { r: 1, c: 1 }, s: { r: 0, c: 1 } },
        { e: { r: 0, c: 3 }, s: { r: 0, c: 2 } },
        { e: { r: 0, c: 5 }, s: { r: 0, c: 4 } },
        { e: { r: 0, c: 7 }, s: { r: 0, c: 6 } },
        { e: { r: 0, c: 9 }, s: { r: 0, c: 8 } },
        { e: { r: 0, c: 11 }, s: { r: 0, c: 10 } },
        { e: { r: 0, c: 13 }, s: { r: 0, c: 12 } },
        { e: { r: 0, c: 15 }, s: { r: 0, c: 14 } },
      ];
      ws['!cols'] = [
        { width: 10 },
        { width: 30 },
        { width: 10 },
        { width: 20 },
        { width: 10 },
        { width: 20 },
        { width: 10 },
        { width: 20 },
        { width: 10 },
        { width: 20 },
        { width: 10 },
        { width: 20 },
        { width: 10 },
        { width: 20 },
        { width: 10 },
        { width: 20 },
      ];
      /* generate workbook */
      var wb = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'sheet1');
      XLSX.writeFile(wb, `${fileName}.xlsx`);
      this.loadingChange.emit(false);
    }
  }

  getValue(item, key) {
    return Utils.isNull(_.get(item, key)) ? '---' : _.get(item, key);
  }

  getCostValue(data, key) {
    return Utils.isNull(_.get(data, key)) ? '---' : formatNumber(_.get(data, key), 'en', '1.0-2');
  }

  getTruncateValue(item, key, limit) {
    return Utils.isStringNotEmpty(_.get(item, key)) ? Utils.truncate(_.get(item, key), limit) : '---';
  }

  convertDateData(str) {
    const date = moment(str, 'DD-MMM-YY').toDate();
    return date;
  }
}
