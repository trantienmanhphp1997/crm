import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { CustomerApi, RmApi } from '../../apis';
import { RevenueShareApi } from '../../apis/revenue-share.api';
import { CommonCategory, FunctionCode, maxInt32, REASON_STATUS, Scopes, SessionKey } from 'src/app/core/utils/common-constants';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import _ from 'lodash';
import { ModalRevenueSharingComponent } from '../modal-revenue-sharing/modal-revenue-sharing.component';
import * as moment from 'moment';
import { RmProfileService } from 'src/app/core/services/rm-profile.service';
import { formatNumber } from '@angular/common';
import { Utils } from 'src/app/core/utils/utils';

@Component({
  selector: 'revenue-sharing-approve-component',
  templateUrl: './revenue-sharing-approve.component.html',
  styleUrls: ['./revenue-sharing-approve.component.scss'],
})
export class RevenueSharingApproveComponent extends BaseComponent implements OnInit {
  isLoading = false;
  @ViewChild('table') table: DatatableComponent;
  @ViewChild('tableTarget') tableTarget: DatatableComponent;
  pageable: Pageable;
  screenType: any;
  listBranches = [];
  listDivision = [];
  listRmShare = [];
  listStatusShare = [];
  listRmManager = [];
  listTitleConfig = [];
  formCreated = this.fb.group({
    customerCode: [{ value: '', disabled: true }],
    customerName: [{ value: '', disabled: true }],
    rmCode: [{ value: '', disabled: true }],
    branchCode: [{ value: '', disabled: true }],
    currentFundraising: [{ value: '', disabled: true }],
    currentCredit: [{ value: '', disabled: true }],
    divisionCode: [{ value: '', disabled: true }],

    shareBranch: [{ value: '', disabled: true }],
    shareDivision: [{ value: '', disabled: true }],
    shareRmCode: [{ value: '', disabled: true }],

    shareStatus: [{ value: '', disabled: true }],
    note: [{ value: '', disabled: true }],
    startDate: [{ value: '', disabled: true }],
    endDate: [{ value: '', disabled: true }],
    shareTargetDTOList: [],
    reason: [{ value: '', disabled: true }],
  });
  code: string;
  minDate = new Date();
  paramSearchCustomer = {
    pageNumber: 0,
    pageSize: 10,
    rsId: '',
    scope: 'VIEW',
    search: '',
    searchFastType: 'CUSTOMER_CODE',
  };
  objFunctionCustomer: any;
  objFunctionRm: any;
  idRevenue: any;
  approvedList = false;
  

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private customerApi: CustomerApi,
    private rmApi: RmApi,
    private revenueShareApi: RevenueShareApi,
    private rmProfileService: RmProfileService
  ) {
    super(injector);
    this.isLoading = true;
    this.objFunctionCustomer = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.REVENUE_SHARE}`);
    this.objFunctionRm = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.idRevenue = _.get(this.route.snapshot.params, 'id');
  }
  listTarget = [];
  listDataHistory = [];

  ngOnInit() {
    forkJoin([
      this.categoryService
        .getBranchesOfUser(this.objFunctionCustomer?.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined))),
      this.revenueShareApi.detailRevenueShare(this.idRevenue).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.ACC_REVENUE_SHARING_ST).pipe(catchError(() => of(undefined))),
      this.commonService
        .getCommonCategory(CommonCategory.SHARE_REVENUE_ROLE_APPROVE_EMAIL)
        .pipe(catchError(() => of(undefined))),
      this.rmProfileService.getRolesByUser(this.currUser.username).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.SHARE_ITEM).pipe(catchError(() => of(undefined))),
    ]).subscribe(([listBranches, infoCustomer, listStatusShare, listRoleShare, listRoleUser, listShareItem]) => {
      this.listDivision =
        this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM)?.map((item) => {
          return {
            code: item.code,
            displayName: item.code + ' - ' + item.name,
          };
        }) || [];

      // chi nhánh
      this.listBranches = listBranches?.map((item) => {
        return { code: item.code, displayName: item.code + ' - ' + item.name };
      });

      listRoleShare = listRoleShare?.content;
      listRoleUser?.forEach((element) => {
        if (!_.isEmpty(listRoleShare?.find((item) => item.code === element.name))) {
          this.approvedList = true;
        }
      });
      if (_.isEmpty(infoCustomer)) {
        this.isLoading = false;
        return;
      } else {
        this.formCreated.get('customerCode').setValue(_.get(infoCustomer, 'customerCode'));
        this.formCreated.get('customerName').setValue(_.get(infoCustomer, 'customerName'));
        this.formCreated.get('rmCode').setValue(_.get(infoCustomer, 'rmCode'));
        this.formCreated.get('branchCode').setValue(_.get(infoCustomer, 'branchCode'));
        this.formCreated.get('currentFundraising').setValue(_.get(infoCustomer, 'currentFundraising') || 0);
        this.formCreated.get('currentCredit').setValue(_.get(infoCustomer, 'currentCredit') || 0);
        this.formCreated.get('divisionCode').setValue(_.get(infoCustomer, 'divisionCode'));
        this.formCreated.get('endDate').setValue(moment(_.get(infoCustomer, 'enDate'), 'DD-MM-YYYY').toDate());
        this.formCreated.get('startDate').setValue(moment(_.get(infoCustomer, 'startDate'), 'DD-MM-YYYY').toDate());
        this.formCreated.get('note').setValue(_.get(infoCustomer, 'note'));
        this.formCreated.get('shareDivision').setValue(_.get(infoCustomer, 'shareDivision'));
        this.formCreated.get('shareBranch').setValue(_.get(infoCustomer, 'shareBranch', ''));
        this.formCreated.get('shareRmCode').setValue(_.get(infoCustomer, 'shareRmCode'));
        this.formCreated.get('reason').setValue(_.get(infoCustomer, 'reason'));
      }
      this.listStatusShare = listStatusShare?.content.map((item) => {
        return { code: item.code, displayName: item.name, valueStatus: item.name };
      });

      this.getRmManager([], _.get(infoCustomer, 'shareDivision'), _.get(infoCustomer, 'shareRmCode'));

      this.listStatusShare.forEach((item) => {
        if (item.code === _.get(infoCustomer, 'shareStatus')) {
          this.formCreated.get('shareStatus').setValue(item?.code);
        }
      });
      let indexHistory = 1;
      const nameShareItem = listShareItem?.content.map((item) => {
        return {
          code: item.code,
          name: item.name,
        };
      });
      this.listTarget = _.get(infoCustomer, 'shareTargetDTOList')?.map((item) => {
        let nameShare = nameShareItem.filter((i) => i.code === item.target)[0]?.name || '';
        return { ...item, targetName: nameShare };
      });
      this.listDataHistory = _.get(infoCustomer, 'approvalHistoryDTOList').map((item) => {
        return {
          index: indexHistory++,
          ...item,
          action: this.listStatusShare.find((i) => i.code === item.action)?.displayName,
        };
      });
    });
  }

  isApproved(action: string) {
    const modalConfirm = this.modalService.open(ModalRevenueSharingComponent, {
      windowClass: 'confirm-approved-modal',
    });
    modalConfirm.componentInstance.action = action;
    modalConfirm.result
      .then((res) => {
        if (res !== false) {
          const paramApproved = {
            id: this.idRevenue,
            reason: res,
          };
          this.isLoading = true;
          const isApproved = action === 'approved' ? true : false;
          this.revenueShareApi.approveRevenueShare(isApproved, paramApproved).subscribe(
            () => {
              this.isLoading = false;
              this.messageService.success(this.notificationMessage.success);
              this.ngOnInit();
            },
            (e) => {
              if (!_.isEmpty(e.error)) {
                this.isLoading = false;
                this.messageService.error(e.error.description);
              } else {
                this.isLoading = false;
                this.messageService.error(this.notificationMessage.error);
              }
            }
          );
        }
      })
      .catch(() => {});
  }

  getRmManager(listBranch, rmBlock?: string, codeRmEdit?: string) {
    this.listRmManager = [];
    this.rmApi
      .post('findAll', {
        page: 0,
        size: maxInt32,
        crmIsActive: true,
        branchCodes: listBranch?.filter((code) => !Utils.isStringEmpty(code)),
        rmBlock: rmBlock || '',
        rsId: this.objFunctionCustomer?.rsId,
        scope: Scopes.VIEW,
        searchRevenue: true,
        customerBranchCode: this.formCreated.get('branchCode').value || '',
      })
      .subscribe((res) => {
        const listRm: any[] =
          res?.content?.map((item) => {
            return {
              code: item?.t24Employee?.employeeCode || '',
              displayName:
                Utils.trimNullToEmpty(item?.t24Employee?.employeeCode) +
                ' - ' +
                Utils.trimNullToEmpty(item?.hrisEmployee?.fullName),
            };
          }) || [];

        this.listRmManager = listRm.filter((item) => {
          return item.code !== this.currUser.code && item.code;
        });

        if (_.isEmpty(codeRmEdit)) {
          this.formCreated.controls.shareRmCode.setValue(null);
        } else {
          this.formCreated.controls.shareRmCode.setValue(codeRmEdit);
        }
        this.isLoading = false;
      });
  }

  endRevenueShare() {

    const modalConfirm = this.modalService.open(ModalRevenueSharingComponent, {
      windowClass: 'confirm-approved-modal',
    });
    modalConfirm.componentInstance.action = 'end';
    modalConfirm.componentInstance.maxlength = 50;
    modalConfirm.result
      .then((res) => {
        if (res) {
          console.log('res', res);
          
          const params = {
            reason: res
          }

          this.isLoading = true;
          this.revenueShareApi.endRevenueShare(this.idRevenue, params).subscribe(
            (res) => {
              this.isLoading = false;
              this.messageService.success(this.notificationMessage.success);
              this.back();
            },
            (e) => {
              this.isLoading = false;
              this.messageService.success(this.notificationMessage.error);
            }
          );
        }
      })
      .catch(() => { });
  }

  haveReason(): boolean {
    return REASON_STATUS.includes(this.formCreated.getRawValue().shareStatus);
  }
}
