import { Component, Injector, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CustomerDetailSmeApi } from '../../apis/customer.api';
import _ from 'lodash';
@Component({
  selector: 'app-risk-credit',
  templateUrl: './risk-credit.component.html',
  styleUrls: ['./risk-credit.component.scss']
})
export class RiskCreditComponent extends BaseComponent implements OnInit {
  
  @Input() type;

  isLoading = true;
  data = [];
  year = moment().year();
  year1 = moment().year() - 1;
  month1 = moment().month();
  month2 = moment().month() - 1;
  dateMonth1 = moment(moment(new Date(moment().year(), moment().month(), 1),).subtract(1, 'days').calendar('DD/MM/YYYY')).format('YYYY-MM-DD');
  dateMonth2 = moment(moment(new Date(moment().year(), moment().month() - 1, 1),).subtract(1, 'days').calendar('DD/MM/YYYY')).format('YYYY-MM-DD');
  dateYear1  = moment(moment(new Date(moment().year(), 0, 1),).subtract(1, 'days').calendar('DD/MM/YYYY')).format('YYYY-MM-DD');

  constructor(injector: Injector, private customerDetailSmeApi: CustomerDetailSmeApi) {
    super(injector);
  }

  ngOnInit() {
    const code = _.get(this.route.snapshot.params, 'code');
    this.getRwa(code);
  }

  getRwa(code) {
    const params = {
      lstTransactionDate: `${this.dateYear1},${this.dateMonth2},${this.dateMonth1}`,
      lstCustomerId: code
    }
    console.log(params);
    this.customerDetailSmeApi.getRwa(params).subscribe((res: any) => {
      this.data = res;
      this.isLoading = false;
    }, err => {
      this.isLoading = false;
    });
  }

  getRWByTime(time) {
    let RISKWEIGHT = ''
    switch (time) {
      case this.year1:
        RISKWEIGHT = this.data.find(item => item.TRANSACTION_DATE == this.dateYear1)?.RISKWEIGHT || '--'
        break;
      case this.month1:
        RISKWEIGHT = this.data.find(item => item.TRANSACTION_DATE == this.dateMonth1)?.RISKWEIGHT || '--'
        break;
      case this.month2:
        RISKWEIGHT = this.data.find(item => item.TRANSACTION_DATE == this.dateMonth2)?.RISKWEIGHT || '--'
        break;
      default:
        break;
    }
    return RISKWEIGHT;
  }

  getRWAByTime(time) {
    let RWA = ''
    switch (time) {
      case this.year1:
        RWA = this.data.find(item => item.TRANSACTION_DATE == this.dateYear1)?.RWA || '--'
        break;
      case this.month1:
        RWA = this.data.find(item => item.TRANSACTION_DATE == this.dateMonth1)?.RWA || '--'
        break;
      case this.month2:
        RWA = this.data.find(item => item.TRANSACTION_DATE == this.dateMonth2)?.RWA || '--'
        break;
      default:
        break;
    }
    return RWA;
  }
}
