import { Subscription } from 'rxjs';
import { animate, AUTO_STYLE, state, style, transition, trigger } from '@angular/animations';
import { global } from '@angular/compiler/src/util';
import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { FunctionCode, Scopes } from 'src/app/core/utils/common-constants';
import { getRole } from 'src/app/core/utils/function';
import { Utils } from 'src/app/core/utils/utils';
import { BaseComponent } from 'src/app/core/components/base.component';
import { ActivityService } from 'src/app/core/services/activity.service';

@Component({
  selector: 'app-customer-left-view',
  templateUrl: './customer-left-view.component.html',
  styleUrls: ['./customer-left-view.component.scss'],
  animations: [
    trigger('collapse', [
      state(
        'false',
        style({ height: AUTO_STYLE, visibility: AUTO_STYLE, paddingTop: '0.5rem', borderTop: '1px solid' })
      ),
      state('true', style({ height: '0', visibility: 'hidden' })),
      transition('false => true', animate(150 + 'ms ease-in')),
      transition('true => false', animate(150 + 'ms ease-out')),
    ]),
  ],
})
export class CustomerLeftViewComponent extends BaseComponent implements OnInit, OnDestroy {
  activityCalls = [];
  role: string;
  branchCode: string;
  timer = true;
  timeOutId: any;
  paramActivity = {
    page: 0,
    size: 3,
    parentId: '',
    scope: Scopes.VIEW,
    rsId: '',
    isLeftSide: true,
  };
  subscription: Subscription;
  constructor(
    injector: Injector,
    private activityService: ActivityService // private communicateService: CommunicateService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.role = getRole(global.roles);
    this.branchCode = this.currUser?.attributes?.branches ? this.currUser?.attributes.branches[0] : '';
    this.subscription = this.communicateService.request$.subscribe((req) => {
      if (req?.name === 'customer-left-view') {
        this.paramActivity.rsId = this.objFunction?.rsId;
        this.paramActivity.parentId = Utils.trimNullToEmpty(req.data.parentId);
        if (this.timeOutId) {
          clearTimeout(this.timeOutId);
        }
        if (!req?.data?.view) {
          this.activityCalls = [];
        } else {
          this.getData();
        }
      }
    });
  }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.activityService.search(this.paramActivity).subscribe((result) => {
      this.activityCalls = [];
      if (result) {
        result.content.forEach((activity) => {
          activity.isCollapsed = true;
          this.activityCalls.push(activity);
        });
        // this.timeOutId = setTimeout(() => {
        //   if (this.timer) {
        //     this.getData();
        //   }
        // }, 5000);
      }
    });
  }

  collapsedActivity(i: number) {
    this.activityCalls[i].isCollapsed = this.activityCalls[i].isCollapsed ? false : true;
  }

  identify(index, item) {
    return item ? item.id : undefined;
  }

  ngOnDestroy() {
    this.timer = false;
    clearTimeout(this.timeOutId);
    this.subscription.unsubscribe();
  }
}
