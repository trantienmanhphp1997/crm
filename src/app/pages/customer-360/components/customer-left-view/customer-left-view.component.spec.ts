import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerLeftViewComponent } from './customer-left-view.component';

describe('CustomerLeftViewComponent', () => {
  let component: CustomerLeftViewComponent;
  let fixture: ComponentFixture<CustomerLeftViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerLeftViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerLeftViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
