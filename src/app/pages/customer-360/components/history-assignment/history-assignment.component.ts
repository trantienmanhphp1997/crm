import { DatePipe } from '@angular/common';
import { global } from '@angular/compiler/src/util';
import { Component, Injector, OnInit, ViewChild, ViewEncapsulation, Input, HostBinding } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ExportExcelService } from 'src/app/core/services/export-excel.service';
import { Division, FunctionCode, maxInt32, Scopes } from 'src/app/core/utils/common-constants';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CustomerAssignmentApi, CustomerDetailApi } from '../../apis';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FileService } from 'src/app/core/services/file.service';

@Component({
  selector: 'app-history-assignment',
  templateUrl: './history-assignment.component.html',
  styleUrls: ['./history-assignment.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe],
})
export class HistoryAssignmentComponent extends BaseComponent implements OnInit {
  @HostBinding('class') elementClass = 'app-history-assignment';
  @ViewChild('table') table: DatatableComponent;
  listData = [];
  temp = [];
  isLoading = false;
  textFilter = '';
  customerName = '';
  messages = global.messageTable;
  hrsCode: string;
  customerTypeSale: number;
  customerType: string;
  manageType: number;
  contractNumber: any;
  contractNumberTitle: string;
  systemAssign: any;
  isShowColumnContractNumber: boolean;
  isShowBtnExportExcel: boolean;
  isTitleAssignLdRM: boolean;

  constructor(
    injector: Injector,
    private customerAssignmentApi: CustomerAssignmentApi,
    private customerDetailApi: CustomerDetailApi,
    private datePipe: DatePipe,
    private exportExcelService: ExportExcelService,
    private modalActive: NgbActiveModal,
    private fileService: FileService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
  }

  ngOnInit(): void {
    // const hrsCode = this.route.snapshot.paramMap.get('code');
    this.contractNumberTitle = this.contractNumber ? 'Mã hợp đồng ' + this.contractNumber + ' - ' : '';
    const hrsCode = this.hrsCode;
    if (hrsCode) {
      this.isLoading = true;
      const params = {
        contractNumber: this.contractNumber,
        systemAssign: this.systemAssign,
        customerCode: hrsCode,
        page: 0,
        size: maxInt32,
        rsId: this.objFunction?.rsId,
        scope: Scopes.VIEW,
      };
      let api;
      if (this.customerTypeSale === 2) {
        api = this.customerAssignmentApi.searchHistorySale(params);
      } else {
        if (this.manageType == 0 && this.customerType === Division.INDIV) {
          this.isShowBtnExportExcel = true;
          api = this.customerAssignmentApi.searchHistoryCredit(params, this.customerType === Division.INDIV);
        } else {
          api = this.customerAssignmentApi.searchHistory(params, this.customerType === Division.INDIV);
        }
      }
      api.subscribe(
        (listHistories) => {
          if (this.customerTypeSale === 2) {
            listHistories?.forEach((item) => {
              item.rmBranchCode = item.branchCode;
              item.rmBranchName = item.branchName;
              item.assignedDate = item.assignDate;
              item.updatedCustom = item.endBy;
              item.createdCustom = item.assignBy;
              item.updatedDate = item.endDate;
            });
            this.listData = listHistories || [];
            this.temp = listHistories || [];
          } else {
            this.listData = listHistories?.content || [];
            this.temp = listHistories?.content || [];
          }

          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
      this.customerDetailApi.getByCode(hrsCode).subscribe((customer) => {
        this.customerName =  hrsCode + ' - ' + customer?.customerInfo?.fullName;
      });
    }
  }

  ngAfterViewInit() {
    this.table.groupExpansionDefault = true;
  }

  filter() {
    this.textFilter = this.textFilter.trim();

    // filter our data
    const temp = this.temp.filter((d) => {
      return d.blockCode.toLowerCase().indexOf(this.textFilter) !== -1 || !this.textFilter;
    });

    // update the rows
    this.listData = temp;
  }

  toggleExpandGroup(group) {
    this.table.groupExpansionDefault = false;
    this.table.groupHeader.toggleExpandGroup(group);
  }

  parseDate(value) {
    if (value) {
      try {
        return this.datePipe.transform(value, 'dd/MM/yyyy');
      } catch (error) {
        return '';
      }
    }
    return '';
  }

  exportFile() {
    const data = [];
    let obj: any = {};
    if (this.listData.length > 0) {
      this.listData.forEach((item) => {
        obj = {};
        obj[this.fields.blockCode] = item.blockCode;
        obj[this.fields.titleGroupRM] = item.titleGroupName;
        obj[this.fields.levelRM] = item.levelRMName;
        obj[this.fields.syntheticKPI] = item.isSyntheticKPI ? this.fields.yes : this.fields.no;
        obj[this.fields.startDate] = this.parseDate(item.assignedDate);
        obj[this.fields.endDate] = this.parseDate(item.endDate);
        obj[this.fields.modifiedBy] = item.createdBy;
        obj[this.fields.modifiedDate] = this.parseDate(item.createdDate);
        data.push(obj);
      });
      this.exportExcelService.exportAsExcelFile(data, 'rm_' + this.customerName + '_history');
    } else {
      this.messageService.warn(this.notificationMessage.noRecord);
    }
  }

  closeModal() {
    this.modalActive.close();
  }

  exportFileHistory() {
    if (!this.maxExportExcel) {
      return;
    }
    if (this.listData.length === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    if (this.listData.length > 200000) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.isLoading = true;
    const params = {
      contractNumber: this.contractNumber,
      systemAssign: this.systemAssign,
      customerCode: this.hrsCode,
      page: 0,
      size: maxInt32,
      rsId: this.objFunction?.rsId,
      scope: Scopes.VIEW,
    };
    this.customerAssignmentApi.exportAssignProductHistory(params).subscribe(
      (fileId) => {
        if (fileId) {
          this.fileService.downloadFile(fileId, 'lich-su-phan-giao.xlsx').subscribe(
            (res) => {
              this.isLoading = false;
              if (!res) {
                this.messageService.error(this.notificationMessage.error);
              }
            },
            () => {
              this.isLoading = false;
            }
          );
        }
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }
}
