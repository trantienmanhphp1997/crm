import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryAssignmentComponent } from './history-assignment.component';

describe('HistoryAssignmentComponent', () => {
  let component: HistoryAssignmentComponent;
  let fixture: ComponentFixture<HistoryAssignmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryAssignmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
