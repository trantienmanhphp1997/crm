import {AfterViewInit, Component, Injector, OnInit} from "@angular/core";
import {BaseComponent} from "../../../../core/components/base.component";
import { global } from '@angular/compiler/src/util';
import {
  CommonCategory,
  Division,
  FunctionCode,
  Scopes,
  ScreenType,
  SessionKey,
  TaskType
} from "../../../../core/utils/common-constants";
import {RmModalComponent} from "../../../rm/components";
import * as _ from 'lodash';
import {Pageable} from '../../../../core/interfaces/pageable.interface';
import {ActivityActionComponent} from "../../../../shared/components";
import {CalendarAddModalComponent} from "../../../calendar-sme/components/calendar-add-modal/calendar-add-modal.component";
import {forkJoin, of} from "rxjs";
import {catchError} from "rxjs/operators";
import {CategoryService} from "../../../system/services/category.service";
import {CustomerApi} from "../../apis";
import * as moment from "moment";
import {Utils} from "../../../../core/utils/utils";
import {cleanDataForm} from "../../../../core/utils/function";
import {CreateTaskModalComponent} from "../../../tasks/components";
@Component({
  selector: 'app-birthday-warning',
  templateUrl: 'customer-birthday-warning.component.html',
  styleUrls: ['customer-birthday-warning.component.scss']
})
export class CustomerBirthdayWarningComponent extends BaseComponent implements OnInit, AfterViewInit{
  limit = global.userConfig.pageSize;
  listDivision = [];
  typeDivision = '';
  branchesOfUserIndiv: Array<any>;
  branchesCode = [];
  employeeCode = [];
  employeeCodeParams: Array<string>;
  termData = [];
  listData = [];
  pageable: Pageable;
  divisionOfUser: any[];
  dateChoose : Date;
  minDay: Date;
  maxDay: Date;
  formSearch = this.fb.group({
    rmCodes: [[]],
    search: '',
    branchCode: [[]],
    division: '',
    segment: '',
    indays: null
  });
  params : any= {
    page: 0,
    size: global?.userConfig?.pageSize,
    rsId: '',
    scope: Scopes.VIEW
  };
  privatePriority = [];
  isRM = false;
  objRmManagerFunc: any;
  constructor(injector: Injector,
              private categoryService: CategoryService,
              private customerApi: CustomerApi) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.objRmManagerFunc = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.divisionOfUser = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
  }
  ngOnInit(): void {
    // this.listDivision = this.divisionOfUser.map((item) => {
    //   return {
    //     code: item?.code,
    //     name: `${item?.code || ''} - ${item?.name || ''}`
    //   }
    // }) || [];
    this.isLoading = true;
    this.minDay = new Date();
    this.maxDay =  new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 6));
    this.employeeCode = _.get(this.fields, 'all');
    this.params.rsId = this.objFunction?.rsId;
    this.listDivision = [{
      code: 'INDIV',
      name: 'INDIV-Khách hàng cá nhân'
    }]
    this.typeDivision = this.listDivision[0].code;
    forkJoin([
      this.commonService.getCommonCategory(CommonCategory.SEGMENT_CUSTOMER_CONFIG).pipe(catchError(() => of(undefined))),
      this.categoryService.getBranchesOfUser(this.objRmManagerFunc?.rsId,Scopes.VIEW).pipe(catchError(() => of(undefined)))
    ]).subscribe(([listSegment,branchOfUser]) => {
      console.log('list branch: ', branchOfUser);
      this.branchesOfUserIndiv = branchOfUser || [];
      this.privatePriority = _.get(listSegment,'content');
      console.log('division: ', this.typeDivision);
     // this.privatePriority = _.find(_.get(listSegment,'content'),(item) => item?.value === this.typeDivision);
      this.privatePriority = this.privatePriority.filter((item) => item?.value === this.typeDivision);
      this.privatePriority.splice(0,0,{
        code: '',
        name: this.fields.all
      });
      this.formSearch.controls.segment.setValue(this.privatePriority[0].code);
      this.isRM = _.isEmpty(branchOfUser);
      this.search(true);
    });
  }
  ngAfterViewInit() {
    this.formSearch.controls.indays.valueChanges.subscribe((value) => {
      console.log('value: ',value);
    });
  }
  getBirthdayInfo(birthday){
    return birthday.substring(6,8) + '/' + birthday.substring(4,6) + '/' + birthday.substring(0,4);
  }
  search(isSearch){
    this.isLoading = true;
    if(isSearch){
      const p = this.formSearch.value;
      console.log('search: ',p);
      if(moment(p.indays).isValid()){
        p.indays = moment(p.indays).format('YYYY-MM-DD');
      }
      this.params = {
        ...this.params,
        ...p,
      }
      console.log('form: ', this.params);
      this.params.page = 0;
      this.params.branchCode = this.branchesCode;
      this.params.rmCodes = this.employeeCode !== this.fields.all ? this.employeeCode?.toString().split(',') : [];
      this.params.search = this.params.search.trim();
    }
    this.customerApi.getDataWarningBirthday(this.params).subscribe((res) => {
      this.listData = _.get(res,'content') || [];
      this.listData = this.listData.map((item) => {
        let rmInfo = '';
        let branchInfo = '';
        if(item?.rmCode){
          rmInfo += item?.rmCode;
        }
        if(item?.rmName){
          if(Utils.isStringNotEmpty(rmInfo)){
            rmInfo += '-' + item?.rmName;
          }
          else{
            rmInfo +=  item?.rmName;
          }
        }
        if(item?.branchCode){
          branchInfo += item?.branchCode;
        }
        if(item?.branchName){
          if(Utils.isStringNotEmpty(branchInfo)){
            branchInfo += '-' + item?.branchName;
          }
          else{
            branchInfo +=  item?.branchName;
          }
        }

        return {
          ...item,
          rmInfo: rmInfo,
          branchInfo: branchInfo,
          birthday: this.getBirthdayInfo(item.dateOfBirth)
        }
      })
      this.pageable = {
        totalElements: res.totalElements,
        totalPages: res.totalPages,
        currentPage: res.number,
        size: this.limit,
      };
      this.isLoading = false;
    }, (er) => {
      this.messageService.error(this.notificationMessage.error);
      this.isLoading = false;
    });

  }
  removeBranchSelected(){
    this.branchesCode = [];
    this.removeRMSelected();
  }
  changeBranch(event) {
    this.branchesCode = event;
    this.removeRMSelected();
  }
  removeRMSelected() {
    this.employeeCode = _.get(this.fields, 'all');
    this.employeeCodeParams = null;
    this.termData = [];
  }
  searchRM() {
    const formSearch = {
      crmIsActive: { value: 'true', disabled: true }
    };
    const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
    modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
    modal.componentInstance.dataSearch = formSearch;
    modal.componentInstance.branchCodes = this.branchesCode;
    modal.componentInstance.block = 'INDIV';
    modal.result
      .then((res) => {
        if (res) {
          this.employeeCode = [];
          this.employeeCodeParams = [];
          const listRMSelect = res.listSelected?.map((item) => {
            this.employeeCode.push(item?.t24Employee?.employeeCode);
            this.employeeCodeParams.push(item.t24Employee.employeeCode);
            return {
              hrsCode: item?.hrisEmployee?.employeeId,
              code: item?.t24Employee?.employeeCode,
              name: item?.hrisEmployee?.fullName,
            };
          });
          this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
          this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);

          this.translate.get('fields').subscribe((res) => {
            this.fields = res;
            const codes = this.employeeCode;
            const codeParams = this.employeeCodeParams;
            const countCode = (codes) => codes.reduce((a, b) => ({ ...a, [b]: (a[b] || 0) + 1 }), {});
            const countCodeParams = (codeParams) => codeParams.reduce((a, b) => ({ ...a, [b]: (a[b] || 0) + 1 }), {});

            this.employeeCode = Object.keys(countCode(codes));
            this.employeeCodeParams = Object.keys(countCodeParams(codeParams));
            this.employeeCode =
              _.size(this.employeeCode) === 0 ||
              (_.size(this.employeeCode) === 1 && this.employeeCode[0] === _.get(this.fields, 'all'))
                ? _.get(this.fields, 'all')
                : _.join(this.employeeCode, ', ');
          });

          if (this.employeeCode.length === 0) {
            this.removeRMSelected();
          }
        } else {
          this.removeRMSelected();
        }
      })
      .catch(() => {});
  }
  setPage(pageInfo){
    if (this.isLoading) {
      return;
    }
    this.params.page = pageInfo.offset;
    this.pageable.currentPage = pageInfo.offset;
    this.search(false);
  }

  createTaskTodo(row) {
    const data: any = {};
    data.parentId = row.customerCode;
    data.parentName = row.customerName;
    data.parentType = TaskType.CUSTOMER;
    const taskModal = this.modalService.open(CreateTaskModalComponent, {
      windowClass: 'create-task-modal',
      scrollable: true,
    });
    taskModal.componentInstance.data = data;
  }

  mailToINDIV(mail) {
    const url = `mailto:${mail}`;
    console.log(url);
    window.location.href = url;
  }

  createActivity(row) {
    const parent: any = {};
    parent.parentType = TaskType.CUSTOMER;
    parent.parentId = row?.customerCode;
    parent.parentName = row?.customerName;
    const modal = this.modalService.open(ActivityActionComponent, {
      windowClass: 'create-activity-modal',
      scrollable: true,
    });
    modal.componentInstance.parent = parent;
    modal.componentInstance.type = ScreenType.Create;
    modal.componentInstance.isShowFuture = true;
  }
  handleChangePageSize(event){
    this.params.page = 0;
    this.params.size = event;
    this.limit = event;
    this.search(false);
  }
}
