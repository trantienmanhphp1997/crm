import { Component, ElementRef, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import _ from 'lodash';
import * as XLSX from 'xlsx';
import { Utils } from 'src/app/core/utils/utils';
import { formatNumber } from '@angular/common';
import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';
import { ExportExcelService } from 'src/app/core/services/export-excel.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

@Component({
  selector: 'customer-sme-obligation-other',
  templateUrl: './customer-sme-obligation-other.component.html',
  styleUrls: ['./customer-sme-obligation-other.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CustomerSmeObligationOtherComponent implements OnInit {
  constructor(
    private messageService: NotifyMessageService,
    private exportExcelService: ExportExcelService,
    private translate: TranslateService
  ) {}
  @ViewChild('customerObligation') customerObligation: ElementRef;
  @Input() model: any;
  notificationMessage: any;
  fields: any;
  models: Array<any>;
  show_data: boolean;
  isLoading: boolean;
  excel_fields: any;
  excel_title: any;

  ngOnInit(): void {
    this.translate.get(['customer360', 'collateralMessage', 'fields']).subscribe((response) => {
      this.notificationMessage = _.get(response, 'notificationMessage');
      this.fields = _.get(response, 'fields');
      this.excel_fields = _.get(response, 'customer360.fields');
      this.excel_title = _.get(response, 'customer360.title');
    }),
      (this.models = _.get(this.model, 'customerCICDTOS'));
    this.show_data = Utils.isArrayEmpty(this.models) ? false : true;
  }

  export() {
    if (!this.show_data) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    } else {
      this.isLoading = true;
      var datas: Array<any> = [];
      _.forEach(this.models, (value, index) => {
        const obj = {};
        obj[_.get(this.fields, 'index')] = index + 1;
        obj[`${_.get(this.fields, 'bank')}`] = _.get(value, 'creditOrgName');
        obj[`${_.get(this.excel_fields, 'classify')}`] = `${_.get(this.fields, 'group')} ${_.get(
          value,
          'gprDebitLoan'
        )}`;
        obj[`${_.get(this.fields, 'card')}`] = Utils.isNotNull(_.get(value, 'cardAmt'))
          ? formatNumber(_.get(value, 'cardAmt'), 'en', '1.0-2')
          : null;
        obj[`${_.get(this.fields, 'bond')}`] = Utils.isNotNull(_.get(value, 'bondAmt'))
          ? formatNumber(_.get(value, 'bondAmt'), 'en', '1.0-2')
          : null;
        obj[`${_.get(this.fields, 'foreignCurrencyCommitment')}`] = Utils.isNotNull(_.get(value, 'commtAmt'))
          ? formatNumber(_.get(value, 'commtAmt'), 'en', '1.0-2')
          : null;
        datas.push(obj);
      });
      var data = {};
      data[_.get(this.fields, 'index')] = _.get(this.fields, 'total');
      data[`${_.get(this.fields, 'bank')}`] = null;
      data[`${_.get(this.excel_fields, 'classify')}`] = null;
      data[`${_.get(this.fields, 'card')}`] = Utils.isNotNull(_.get(this.model, 'totalCardAmt'))
        ? formatNumber(_.get(this.model, 'totalCardAmt'), 'en', '1.0-2')
        : null;
      data[`${_.get(this.fields, 'bond')}`] = Utils.isNotNull(_.get(this.model, 'totalBondAmt'))
        ? formatNumber(_.get(this.model, 'totalBondAmt'), 'en', '1.0-2')
        : null;
      data[`${_.get(this.fields, 'foreignCurrencyCommitment')}`] = Utils.isNotNull(_.get(this.model, 'totalCommtAmt'))
        ? formatNumber(_.get(this.model, 'totalCommtAmt'), 'en', '1.0-2')
        : null;
      datas.push(data);
      this.exportExcelService.exportAsExcelFile(datas, `Thông tin nghĩa vụ khác`, false);
    }
  }

  getTruncateValue(item, key, limit) {
    return Utils.isStringNotEmpty(_.get(item, key)) ? Utils.truncate(_.get(item, key), limit) : '---';
  }

  getValue(item, key) {
    return Utils.isNull(_.get(item, key)) ? '---' : _.get(item, key);
  }

  getCostValue(data, key) {
    return Utils.isNull(_.get(data, key)) ? '---' : formatNumber(_.get(data, key), 'en', '1.0-2');
  }

  getSubstringValue(item, key) {
    return Utils.isNull(_.get(item, key)) ? '---' : Utils.subString(_.get(item, key));
  }

  convertDateData(str) {
    const date = moment(str, 'DD-MMM-YY').toDate();
    return date;
  }
}
