import {Component, OnDestroy, OnInit, ElementRef, ViewEncapsulation, Input, Injector} from '@angular/core';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { TaskService } from 'src/app/pages/tasks/services/tasks.service';
import { AppConfig } from '../../../../core/interfaces/appconfig.interface';
import { AppConfigService } from '../../../../core/services/appconfig.service';
import {
  FunctionCode, Scopes,
  StatusTask
} from '../../../../core/utils/common-constants';
import * as _ from 'lodash';
import {CustomerLeadService} from '../../../lead/service/customer-lead.service';
import {CustomerAssignmentApi} from '../../apis';
import { BaseComponent } from 'src/app/core/components/base.component';

@Component({
  selector: 'app-activity-log',
  templateUrl: './activity-log.component.html',
  styleUrls: ['./activity-log.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ActivityLogComponent extends BaseComponent implements OnInit, OnDestroy {
  @Input() leadCode?: any = '';
  @Input() rsId?: any = '';
  @Input() customerCode?: any = '';
  active = true;
  showInput = false;
  inputFocus = '';
  outsideClickListener: any;
  listActions = ['Tất cả','Cuộc gọi','Tin nhắn','Email','Lịch hẹn']
  logTypeFilter = {
    all: true,
    call: false,
    sms: false,
    email: false,
    calendar: false,
  };
  config: AppConfig;
  listCustomer = [];
  date = moment();
  title: string;
  statusTask = StatusTask;
  subscription: Subscription;
  inputNote = '';
  isOpenNote = false;
  listActivityLog = [{
    content: '',
    createdBy: '',
    createdDate: '',
    leadCode: '',
    customerCode: '',
    id: '',
    type: '',
    calEndDate: '',
    rmBranchName : '',
    calStartDate: ''
  }]
  offset = 0;
  interval: any;
  isLoading = false;
  params: any = {
    size: 10,
    page: 0,
    leadCode: '',
    customerCode: '',
    type: '',
    rsId: '',
    scope: Scopes.VIEW
  };

  constructor(
    injector: Injector,
    private el: ElementRef,
    private configService: AppConfigService,
    private taskService: TaskService,
    private customerLeadService: CustomerLeadService,
    private customerAssignmentApi: CustomerAssignmentApi
  ) {
    super(injector);
  }

  ngOnInit() {
    this.params.rsId = this.rsId;
    this.getListActivityLog();
  }

  getListActivityLog() {
    this.params.leadCode = this.leadCode;
    this.params.customerCode = this.customerCode;
    this.customerLeadService.getActivityLogByCusCode(this.params).subscribe((value) => {
      this.listActivityLog = value.content;
    });

  }
  toggleConfigurator(event: Event) {
    this.active = !this.active;
    event.preventDefault();

    if (this.active) this.bindOutsideClickListener();
    else this.unbindOutsideClickListener();
  }

  openConfigurator(event: Event) {
    this.active = !this.active ;
  }

  hideConfigurator(event) {
    this.active = false;
    this.unbindOutsideClickListener();
    event.preventDefault();
  }

  onRippleChange() {
    this.configService.updateConfig(this.config);
  }

  bindOutsideClickListener() {
    if (!this.outsideClickListener) {
      this.outsideClickListener = (event) => {
        if (this.active && this.isOutsideClicked(event)) {
          this.active = false;
        }
      };
      document.addEventListener('click', this.outsideClickListener);
    }
  }

  unbindOutsideClickListener() {
    if (this.outsideClickListener) {
      document.removeEventListener('click', this.outsideClickListener);
      this.outsideClickListener = null;
    }
  }

  isOutsideClicked(event) {
    return !(this.el.nativeElement.isSameNode(event.target) || this.el.nativeElement.contains(event.target));
  }

  onScroll(e) {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.params.page += 1;
    this.customerLeadService
      .getActivityLogByCusCode(this.params)
      .subscribe(
        (value) => {
            this.listActivityLog.push.apply(this.listActivityLog, value.content);
            this.listCustomer = _.uniq(_.map(this.listActivityLog, 'createdBy'));
            this.isLoading = false;
            }, () => {
            this.isLoading = false;
            });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  getIconClass(type :string) {
    return ('icon-activity-' + type).toLowerCase();
  }

  blurLog(id) {
      this.inputFocus = id;
      const timer = setTimeout(() => {
        document.getElementById(id)?.focus();
        clearTimeout(timer);
      }, 100);
  }

  saveActivityLog(e,id) {
    const timer = setTimeout(() => {
    this.inputFocus = '';
    const data = _.find(this.listActivityLog,(item) => item.id === id);
    if (data.content !== e.target.value) {
      data.content = e.target.value;
      const dataUpdate = {
        leadCode: this.leadCode,
        customerCode: this.customerCode,
        content: e.target.value,
        logId: id,
        rsId: this.rsId,
        scope: Scopes.UPDATE
      }
      console.log(dataUpdate);
      this.customerAssignmentApi.saveNote(dataUpdate).subscribe(value => {
      });
    }
      clearTimeout(timer);
    }, 100);
  }

  // hoverLog(e,index) {
  //   document.getElementById('em' + index).className = 'float-right las icon-activity-note-black d-block';
  // }
  //
  // outHoverLog(e,index) {
  //   document.getElementById('em' + index).className = 'float-right las icon-activity-note-black d-none';
  // }

  handleLogType(type) {
    this.logTypeFilter = {
      all: type === 'all',
      call: type === 'call',
      sms: type === 'sms',
      email: type === 'email',
      calendar: type === 'calendar',
    };
    type === 'all' ? this.params.type = '' :this.params.type = type;
    this.params.page = 0;
    this.getListActivityLog();
  }

  openNoteInput() {
    this.isOpenNote = !this.isOpenNote;
    const timer = setTimeout(() => {
      document.getElementById('inputNote')?.focus();
      clearTimeout(timer);
    }, 100);
  }

  saveNote() {
    this.isOpenNote = false;
    const param = {
      leadCode: this.leadCode,
      customerCode: this.customerCode,
      content: this.inputNote,
      rsId: this.rsId,
      scope: Scopes.CREATE
    }
    this.customerAssignmentApi.saveNote(param).subscribe(item => {
      this.getListActivityLog();
      this.inputNote = '';
    });
  }
}
