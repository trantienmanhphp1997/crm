import { global } from '@angular/compiler/src/util';
import {
  AfterViewInit,
  EventEmitter,
  Injector,
  Input,
  Output,
  Component,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';
import { Utils } from 'src/app/core/utils/utils';
import { catchError } from 'rxjs/operators';
import { CustomerApi, CustomerAssignmentApi } from '../../apis';
import { cleanDataForm } from 'src/app/core/utils/function';
import {
  CommonCategory,
  ComponentType,
  ConfirmType,
  CustomerType,
  Division,
  ExportKey,
  FunctionCode,
  functionUri,
  ListCustomerType,
  maxInt32,
  Scopes,
  SessionKey,
  LEVEL_TTTM,
} from 'src/app/core/utils/common-constants';
import { SelectionType } from '@swimlane/ngx-datatable';
import { RmModalComponent } from 'src/app/pages/rm/components';
import { forkJoin, Observable, of, Subscription } from 'rxjs';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { RmApi, RmBlockApi } from 'src/app/pages/rm/apis';
import { FileService } from 'src/app/core/services/file.service';
import { ActivationEnd, ActivationStart } from '@angular/router';
import { HistoryAssignmentComponent } from '../history-assignment/history-assignment.component';
import { RevenueShareApi } from '../../apis/revenue-share.api';
import { SaleManagerApi } from 'src/app/pages/sale-manager/api/sale-manager.api';
import {AppFunction} from "../../../../core/interfaces/app-function.interface";
import {MbeeChatService} from "../../../../core/services/mbee-chat.service";
import { SaleSuggestApi } from './../../../sale-manager/api/sale-suggest.api';
import {createGroupChat11} from "../../../../core/utils/mb-chat";
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CustomerListComponent extends BaseComponent implements OnInit, AfterViewInit {
  listCustomerClassification = [
    {
      "name" : "Tất cả",
      "value": -1
    },{
      "name" : "Khách hàng trực tiếp",
      "value": 0
    },{
      "name" : "Khách hàng gián tiếp",
      "value": 1
    }
  ]
  textSearch = '';
  searchQuickType = '';
  isShowAdvance = true;
  isOpenMore = false;
  isCount = false;
  is8FirstDayInMonth = true;
  numberFirstDayInMonth = true;
  @Input() rsId: string;
  @Input() dataSearch: any;
  @Input() refreshTable: number;
  @Input() showAction: boolean;
  @Input() propData: any;
  @Input() type = ComponentType.Modal;
  @Input() listSelectedOld: any[];
  @Output() loading: EventEmitter<any> = new EventEmitter<any>();
  @Output() selectedCustomer: EventEmitter<any> = new EventEmitter<any>();
  listData = [];
  listTransactionFrequency = [];
  sectors: any[] = [];
  selectedSectors: any[] = [];
  listBranches = [];
  listDivision = [];
  listRmManager = [];
  listStatus = [];
  listSegmentAll = [];
  listSegment = [];
  listCustomerObject = [];
  listSearchType = [];
  listTitleConfig = [];
  phonePriority: string;
  emailPriority: string;
  pageable: Pageable;
  searchForm = this.fb.group({
    customerCode: [''],
    customerName: [''],
    phone: [''],
    idCard: [''],
    customerType: [''],
    transactionFrequency: [''],
    transactionFrequencyText: [this.fields.all],
    branchCode: [''],
    hrsCode: [''],
    businessRegistrationNumber: [''],
    taxCode: [''],
    swiftCode: [''],
    industry: [''],
    status: [''],
    segment: [''],
    sectorArray: [''],
    customerObjects: [''],
    customerTypeSale: [1],
    manageType: [1],
    productGroup: [''],
    rmCode: [''],
    customerClassification:[-1]
  });
  titleSegment: string =  'Phân khúc KH (DT gần nhất)';
  params: any = {
    pageNumber: 0,
    pageSize: global?.userConfig?.pageSize,
    rsId: '',
    scope: Scopes.VIEW,
    customerType: '',
  };
  prevParams: any = {};
  industryCode = '';
  checkAll = false;
  checkTransactionFrequencyGroupAll = true;
  countCheckedOnPage = 0;
  countDataTransactionFrequencyGroup = 0;
  searchAdvance: string;
  subCount: Subscription;
  currState: any;
  isSmeTable = false;
  disabledSearchAdvance = true;
  disabledSearchBasic = true;
  divisionConfig = [];
  listCustomerType = [];
  listManageType = [];
  isRMorUB = false;
  listProductType = [];
  listRmSale = [];
  isCustomerTypeSale = false;
  objFunctionRM: any;
  isOpenCodeDateShow = false;
  isShowBtnCampaign = false;
  scopes: Array<any>;
  isShowCreateOppBtn = false;
  isShowColumnContractNumber = false;
  systemAssign: string;
  flag: any;
  isLoadTable:boolean = true;
  priorityBlock: [];
  constructor(
    injector: Injector,
    private customerApi: CustomerApi,
    private customerAssignmentApi: CustomerAssignmentApi,
    private categoryService: CategoryService,
    private rmApi: RmApi,
    private fileService: FileService,
    private rmBlockApi: RmBlockApi,
    private revenueShareApi: RevenueShareApi,
    private saleManagerApi: SaleManagerApi,
    private mbeeChatService: MbeeChatService,
    private saleSuggestApi: SaleSuggestApi,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.objFunctionRM = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.objFunctionMBChat = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.APP_CHAT}`);
    this.scopes = _.get(this.objFunction, 'scopes');
    this.paramsRmSale.rsId = this.objFunctionRM.rsId;
    this.isLoading = true;
  }
  paramsRmSale: any = {
    divisionCode: 'SME',
    rsId: '',
    scope: Scopes.VIEW,
    page: 0,
    size: maxInt32,
    productGroup: '',
    branchCodes: '',
  };
  viewCustomerType = true;
  listDivisionSale = [];
  paramSearchDivision = {
    size: global.userConfig.pageSize,
    page: 0,
    name: '',
    code: '',
  };

  customerId: any;

  isShowChat = false;
  objFunctionMBChat: AppFunction;
  rowMenu: MenuItem[];
  itemSelected: any;
  isClickEvent = false;

  ngOnInit(): void {
    this.listDivision = _.map(this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM), (item) => {
      return {
        titleCode: item.titleCode,
        levelCode: item.levelCode,
        code: item.code,
        displayName: `${item.code} - ${item.name}`,
      };
    });
    if(_.head(this.listDivision)?.code === Division.INDIV){
      this.isLoadTable = false;
    }

    this.isShowChat = this.objFunctionMBChat?.scopes.includes('VIEW');

    this.customerId = JSON.parse(localStorage.getItem('DETAIL_CUS_ID'));
    if (this.customerId) {
      this.router.navigate([this.router.url, 'detail', 'indiv', this.customerId], {
        skipLocationChange: true,
      });
    } else {
      this.displayButtons();
      this.getManagerType();
    }
    this.titleSegment = this.params.customerType ==='SME' ? "Phân khúc KH (DTBQ 3 năm)":"Phân khúc KH (DT gần nhất)"

    this.rowMenu = [];
    if (this.objFunction?.rmGrant) {
      if (this.isShowChat) {
        this.rowMenu.push({
          label: 'Chat',
          command: (e) => {
            this.createGroupChat11(this.itemSelected);
          },
        });
      }

      if (this.isShowCreateOppBtn) {
        this.rowMenu.push({
          label: 'Tạo cơ hội bán',
          command: (e) => {
            this.createOppKHCN(this.itemSelected);
          },
        });
      }
    }
  }

  initData() {
    this.listCustomerType = ListCustomerType;
    this.isDownLoading = this.sessionService.getSessionData(ExportKey.Customer);
    this.searchQuickType = 'CUSTOMER_CODE';
    this.router.events.subscribe((e) => {
      if (e instanceof ActivationStart && e.snapshot?.data?.code !== FunctionCode.CUSTOMER_360_MANAGER) {
        this.prop = null;
        this.sessionService.setSessionData(FunctionCode.CUSTOMER_360_MANAGER, null);
      }
    });

    this.params.rsId = !this.rsId ? this.objFunction?.rsId : this.rsId;

    if (!this.listSelectedOld) {
      this.listSelectedOld = [];
    }

    this.currState = this.sessionService.getSessionData(`${FunctionCode.CUSTOMER_360_MANAGER}_STATE`);

    if (this.type !== ComponentType.Modal) {
      this.prop = this.sessionService.getSessionData(FunctionCode.CUSTOMER_360_MANAGER);
    }

    if (!this.prop) {
      if (!this.currState) {
        this.getDataCommon().subscribe(() => {
          this.search(true);
        });
      } else {
        this.mapDataCommon();
        this.getRmManager([], this.searchForm.controls.customerType.value);
        this.search(true);
      }
    } else {
      const timer = setTimeout(() => {
        this.isLoadTable = this.prop?.isLoadTable;
        this.isShowAdvance = this.prop?.isShowAdvance;
        this.isOpenMore = this.prop?.isOpenMore;
        this.prevParams = this.prop?.params;
        this.params.pageNumber = this.prop?.params?.pageNumber || 0;
        this.params.pageSize = this.prop?.params?.pageSize || global?.userConfig?.pageSize;
        this.params.customerType = this.prop?.params?.customerType || _.head(this.listDivision)?.code;
        this.industryCode = this.prop?.params?.industry || '';
        this.textSearch = this.prop?.params?.search || '';
        this.searchQuickType = this.prop?.params?.searchFastType || 'CUSTOMER_CODE';
        this.searchAdvance = this.prop?.searchAdvance;
        this.listRmSale = this.prop?.listRmSale;
        if (this.prop.params && this.searchAdvance === 'advance') {
          this.searchForm.patchValue(this.prop.dataForm);
        }
        this.mapDataCommon();

        this.listRmManager = this.prop?.listRmManager;
        this.listSegment = this.prop?.listSegment;
        if(this.prop?.params?.sectorArray){
          this.selectedSectors = [];
          this.prop.params.sectorArray.forEach(item => {
            let obj = {
              id: item
            };
            this.selectedSectors.push(obj);
          });
        }else{
          this.selectedSectors = this.sectors;
        }
        this.listCustomerObject = this.prop?.listCustomerObject;
        this.listTransactionFrequency?.forEach((item) => {
          item.isChecked = this.prop?.params?.transactionFrequency?.includes(item.code);
        });
        this.checkTransactionFrequencyGroupAll =
          this.prop?.params?.transactionFrequency?.length === this.listTransactionFrequency?.length;
        if (this.prop?.pageable) {
          this.pageable = this.prop.pageable;
          this.isCount = true;
        } else {
          this.getCountCustomer(this.prevParams);
        }
        this.isCustomerTypeSale = this.prop?.isCustomerTypeSale;
        this.isSmeTable = this.prop?.isSmeTable;
        this.listData = this.prop?.listData;
        this.toggleLoading(false);
        this.table?.recalculate();
        clearTimeout(timer);
      }, 800);
    }
    this.disabledSearchAdvance = this.isShowAdvance;
    this.disabledSearchBasic = !this.isShowAdvance;
  }

  // getManagerType() {
  //   this.categoryService
  //     .checkManageType(!this.rsId ? this.objFunction?.rsId : this.rsId, Scopes.VIEW)
  //     .subscribe((listManageType) => {
  //       if (listManageType && listManageType.length > 0) {
  //         this.listManageType = listManageType;
  //         if (listManageType.length === 1) {
  //           this.isRMorUB = true;
  //           this.searchForm.controls.manageType.setValue(listManageType[0].code);
  //         } else {
  //           this.searchForm.controls.manageType.setValue(1);
  //         }
  //       }
  //       this.initData();
  //     });
  // }

  getManagerType() {
    this.categoryService
      .checkManageType(!this.rsId ? this.objFunction?.rsId : this.rsId, Scopes.VIEW)
      .subscribe((listManageType) => {
        if (listManageType && listManageType.length > 0) {
          this.listManageType = listManageType;
          let itemSelected = listManageType.filter((item) => {
            return item.selected === true;
          });
          this.searchForm.controls.manageType.setValue(itemSelected[0].code);
        }
        this.initData();
      });
  }

  getDataCommon(): Observable<any> {
    return new Observable((observer) => {
      forkJoin([
        this.categoryService
          .getBranchesOfUser(!this.rsId ? this.objFunction?.rsId : this.rsId, Scopes.VIEW)
          .pipe(catchError(() => of(undefined))),
        this.commonService
          .getCommonCategory(CommonCategory.STATUS_CUSTOMER_CONFIG)
          .pipe(catchError(() => of(undefined))),
        this.commonService
          .getCommonCategory(CommonCategory.SEGMENT_CUSTOMER_CONFIG)
          .pipe(catchError(() => of(undefined))),
        this.commonService
          .getCommonCategory(CommonCategory.CUSTOMER_OBJECT_CONFIG)
          .pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.KH_PHONE_NO_CONFIG).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.KH_EMAIL_CONFIG).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.ACCOUNT_GROUP).pipe(catchError(() => of(undefined))),
        this.customerAssignmentApi.check8FirstDayInMonth().pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.QUICKSEARCH_TYPE).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.KHOI_PRIORITY).pipe(catchError(() => of(undefined))),
        this.commonService
          .getCommonCategory(CommonCategory.SHARE_REVENUE_ROLE_INPUT)
          .pipe(catchError(() => of(undefined))),
        this.rmBlockApi
          .fetch({ page: 0, size: maxInt32, hrsCode: this.currUser?.hrsCode, isActive: true })
          .pipe(catchError(() => of(undefined))),
        this.categoryService.searchBlocksCategory(this.paramSearchDivision).pipe(catchError(() => of(undefined))),
        this.saleSuggestApi.getSectorBySegment([]),
      ]).subscribe(
        ([
          listBranches,
          listStatus,
          listSegment,
          listCustomerObject,
          listConfigPhone,
          listConfigEmail,
          listAccountGroup,
          firstDayInMonth,
          listSearchType,
          listConfigDivision,
          listTitleConfig,
          divisionOfRm,
          allDivision,
          listSectorBySegment,
        ]) => {
          listConfigDivision = listConfigDivision?.content || [];
          listConfigDivision = _.orderBy(listConfigDivision, ['value']);

          allDivision = allDivision?.content?.map((item) => {
            return {
              code: item.code,
              displayName: item.code + ' - ' + item.name,
            };
          });

          this.listDivisionSale = [...listConfigDivision].map((item) => {
            return {
              code: item.code,
              displayName: _.filter(allDivision, (i) => item.code === i.code)[0]?.displayName,
            };
          });
          this.divisionConfig = listConfigDivision?.map((item) => item.code);
          listBranches = listBranches?.map((item) => {
            return { code: item.code, displayName: item.code + ' - ' + item.name };
          });
          listBranches?.unshift({ code: '', displayName: this.fields.all });
          listStatus = listStatus?.content || [];
          listStatus?.unshift({ code: '', name: this.fields.all });
          const listSegmentAll = listSegment?.content || [];
          listCustomerObject = listCustomerObject?.content || [];
          listCustomerObject?.unshift({ value: '', name: this.fields.all });
          listAccountGroup = listAccountGroup?.content || [];
          listAccountGroup?.unshift({ code: '', name: this.fields.no_group });
          listAccountGroup?.forEach((item) => {
            item.isChecked = true;
          });
          listSectorBySegment = listSectorBySegment || [];
          listSectorBySegment.forEach((item) => {
            let obj = {
              id: item
            };
            this.sectors.push(obj);
          });
          listConfigPhone = listConfigPhone?.content || [];
          listConfigEmail = listConfigEmail?.content || [];

          let listDivision = _.map(this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM), (item) => {
            return {
              titleCode: item.titleCode,
              levelCode: item.levelCode,
              code: item.code,
              displayName: `${item.code} - ${item.name}`,
            };
          });
          this.params.customerType = _.head(listDivision)?.code;
          // this.isShowBtnCampaign = this.params.customerType === Division.INDIV;
          if (_.find(listDivision, (item) => _.includes(LEVEL_TTTM, item.levelCode))) {
            _.remove(this.listCustomerType, (item) => item.code === 1);
            this.searchForm.get('customerTypeSale').setValue(2);
            this.onChangeCustomerTypeSale({ value: 2 });
          }
          listSearchType?.content?.forEach((item) => {
            if (_.find(listDivision, (i) => item.value.includes(i.code))) {
              this.listSearchType.push(item);
            }
          });

          this.listSearchType = _.uniqBy(this.listSearchType)?.map((item) => {
            return { code: item.code, name: item.name };
          });
          const listConfig = listTitleConfig?.content;

          this.currState = {
            listBranches,
            listStatus,
            listSegmentAll,
            listCustomerObject,
            listConfigPhone,
            listConfigEmail,
            listTransactionFrequency: listAccountGroup,
            firstDayInMonth,
            listDivision,
            listSearchType: this.listSearchType,
            listTitleConfig: listConfig,
            listProductType: this.listProductType,
            listDivisionSale: this.listDivisionSale,
            sectors: this.sectors,
          };
          console.log(this.currState)
          this.sessionService.setSessionData(`${FunctionCode.CUSTOMER_360_MANAGER}_STATE`, this.currState);
          this.mapDataCommon();
          this.getRmManager([], this.searchForm.controls.customerType.value);
          return observer.next(true);
        }
      );
    });
  }

  mapDataCommon() {
    if (this.dataSearch) {
      Object.keys(this.dataSearch).forEach((key) => {
        if (this.dataSearch[key].value) {
          this.searchForm.get(key)?.setValue(this.dataSearch[key].value);
        }
        if (this.dataSearch[key].disabled) {
          this.searchForm.get(key)?.disable();
        }
      });
    }
    this.listTitleConfig = this.currState?.listTitleConfig;
    this.is8FirstDayInMonth = this.currState?.firstDayInMonth?.value;
    this.numberFirstDayInMonth = this.currState?.firstDayInMonth?.key;
    this.listBranches = this.currState?.listBranches;
    this.listDivision = this.currState?.listDivision;
    this.listStatus = this.currState?.listStatus;
    this.listSegmentAll = this.currState?.listSegmentAll;
    this.listSegment = this.listSegmentAll?.filter((item) => {
      return item.value === this.searchForm.controls.customerType.value;
    });
    this.listSegment?.unshift({ code: '', name: this.fields.all });
    this.sectors = this.currState?.sectors;
    this.selectedSectors = this.sectors;
    this.listCustomerObject = this.currState?.listCustomerObject;
    this.listTransactionFrequency = this.currState?.listTransactionFrequency;
    this.phonePriority = this.currState?.listConfigPhone[0]?.value;
    this.emailPriority = this.currState?.listConfigEmail[0]?.value;
    this.listProductType = this.currState?.listProductType;
    this.listDivisionSale = this.currState?.listDivisionSale;

    if (_.isEmpty(this.searchForm.controls.customerType.value)) {
      this.searchForm.controls.customerType.setValue(_.first(this.listDivision)?.code);
    }
    if (_.isEmpty(this.params.customerType)) {
      this.params.customerType = _.first(this.listDivision)?.code;
      // this.isShowBtnCampaign = this.params.customerType === Division.INDIV;
    }
    if (this.listDivision?.length === 1 && this.listDivision[0].code === Division.INDIV) {
      this.viewCustomerType = false;
    } else {
      if (_.find(this.listDivision, (item) => _.includes(LEVEL_TTTM, item.levelCode))) {
        _.remove(this.listCustomerType, (item) => item.code === 1);
        this.searchForm.get('customerTypeSale').setValue(2);
        this.onChangeCustomerTypeSale({ value: 2 });
      }
    }
    this.listSearchType = this.currState?.listSearchType || [];
    this.flag = this.params.customerType;
    if (this.flag === 'INDIV') {
    } else {
      this.rowMenu.shift();
    }

  }

  ngAfterViewInit() {
    this.searchForm.controls.customerType.valueChanges.subscribe((value) => {
      if (this.searchForm.get('customerTypeSale').value === 2) {
        this.onChangeCustomerTypeSale({ value: 2 });
      }
      if (value) {
        this.listSegment = this.listSegmentAll?.filter((item) => {
          return item.value === value;
        });
        this.listSegment?.unshift({ code: '', name: this.fields.all });
      } else {
        this.listSegment = [{ code: '', name: this.fields.all }];
      }
      this.listSegment = _.uniqBy(this.listSegment, 'code');
      if (!this.isLoading) {
        if (value !== CustomerType.INDIV && value !== CustomerType.SME) {
          this.searchForm.controls.status.setValue('');
        }
        if (value !== CustomerType.SME) {
          this.searchForm.controls.customerObjects.setValue('');
        } else {
          this.searchForm.controls.idCard.setValue('');
          this.searchForm.controls.businessRegistrationNumber.setValue('');
        }
        this.searchForm.controls.segment.setValue('');
        this.searchForm.controls.hrsCode.setValue('');
        this.getRmManager([this.searchForm.controls.branchCode.value], value);
      }
      this.setOpenCodeDateShow();
    });
    this.searchForm.controls.branchCode.valueChanges.subscribe((value) => {
      if (!this.isLoading) {
        this.searchForm.controls.hrsCode.setValue('');
        if (this.searchForm.get('customerTypeSale').value === 1) {
          this.getRmManager([value], this.searchForm.controls.customerType.value);
        } else {
          this.getRMSaleProduct();
        }
      }
    });

    this.searchForm.controls.segment.valueChanges.subscribe((value) => {
      if (!this.isLoading && this.searchForm.controls.customerType.value == CustomerType.INDIV) {
        this.setSector();
      }
    });

    this.searchForm.valueChanges.subscribe((value) => {
      if (!this.isLoading) {
        this.disabledSearchAdvance = false;
      }
    });
  }

  search(isSearch?: boolean, type?: string) {
    if(!this.isLoadTable){
      this.toggleLoading(false);
      return;
    }
    this.toggleLoading(true);
    let params: any = {};
    if((this.searchForm.get('customerType').value === 'SME' && this.isShowAdvance) || (!this.isShowAdvance && this.params.customerType ==='SME')){
      this.titleSegment = "Phân khúc KH (DTBQ 3 năm)";
    }else{
      this.titleSegment = "Phân khúc KH (DT gần nhất)";
    }
    if (isSearch) {
      this.searchAdvance = type;
      this.params.pageNumber = 0;
      if (this.searchAdvance === 'advance') {
        const dataForm = cleanDataForm(this.searchForm);
        params = { ...dataForm };
        delete params.transactionFrequencyText;
        if (!this.checkTransactionFrequencyGroupAll) {
          params.transactionFrequency = [];
          this.listTransactionFrequency?.map((item) => {
            if (item.isChecked) {
              params.transactionFrequency.push(item.code);
            }
          });
        } else {
          delete params.transactionFrequency;
        }
        if (this.selectedSectors.length == this.sectors.length || this.selectedSectors.length === 0) {
          delete params.sectorArray;
        } else {
          params.sectorArray = [];
          this.selectedSectors?.map((item) => {
            params.sectorArray.push(item.id);
          });
        }
        params.branches = [params.branchCode];
        params.rsId = this.params.rsId;
        params.scope = this.params.scope;
        if (!_.isEmpty(_.trim(params.customerObjects))) {
          params.customerObjects = _.split(params.customerObjects, ',');
        } else {
          params.customerObjects = [];
        }
        delete params.branchCode;
      } else {
        params = this.params;
        this.textSearch = Utils.trimNullToEmpty(this.textSearch);
        params.search = this.textSearch;
        params.searchFastType = this.searchQuickType;
        params.manageType = this.searchForm.controls.manageType.value;
        params.customerTypeSale = this.listCustomerType?.length > 1 ? 1 : 2;
      }
    } else {
      params = this.prevParams;
    }
    this.isShowColumnContractNumber = this.searchForm.controls.manageType.value === 0 ? true : false;
    this.systemAssign = this.searchForm.controls.manageType.value === 0 ? '103' : '';
    this.listData = [];
    this.messages = { ...this.messages, emptyMessage: '' };
    params.pageNumber = this.params.pageNumber || 0;
    params.pageSize = this.params.pageSize;
    if (this.dataSearch) {
      Object.keys(this.dataSearch).forEach((key) => {
        if (this.dataSearch[key].value) {
          params[key] = this.dataSearch[key].value;
        }
      });
    }
    let apiSearch: Observable<any>;
    if (this.searchQuickType === 'BUSINESS_REGISTRATION_NUMBER' || params.customerType !== Division.INDIV) {
      params.rmCodeManager = _.find(this.listRmManager, (item) => item.code === params.hrsCode)?.rmCode;
      apiSearch = this.customerApi.searchSme(params);
      this.isSmeTable = true;
    } else {
      apiSearch = this.customerApi.search(params);
      this.isSmeTable = false;
    }

    if (params.customerTypeSale === 2) {
      if (this.searchAdvance === 'advance') {
        params.productGroup = this.searchForm.controls.productGroup.value;
      } else {
        params.rmCode = this.currUser?.code;
      }
      params.branches = _.filter(params.branches, (item) => !_.isEmpty(item));
      delete params.hrsCode;
      this.customerApi.searchSmeSale(params).subscribe(
        (listData) => {
          this.isCustomerTypeSale = true;
          this.mapRespondSearch(listData, params, true);
        },
        () => {
          this.toggleLoading(false);
        }
      );
    } else {
      apiSearch.subscribe(
        (listData) => {
          this.isCustomerTypeSale = false;
          this.mapRespondSearch(listData, params, false);
          if(this.route.snapshot.queryParams?.id){
            this.onActive({type: 'dblclick', row : listData[0]})
          }
        },
        () => {
          this.toggleLoading(false);
        }

      );
      this.getCountCustomer(params);
    }

    this.disabledSearchAdvance = this.searchAdvance === 'advance';
    this.disabledSearchBasic = this.searchAdvance !== 'advance';

    this.flag = params.customerType;
    if (this.flag === 'INDIV') {
      if (!_.find(this.rowMenu, (i) => i.label === 'Chat')) {
        this.rowMenu.unshift({
          label: 'Chat',
          command: (e) => {
            this.createGroupChat11(this.itemSelected);
          },
        })
      }
    } else {
      if (_.find(this.rowMenu, (i) => i.label === 'Chat')) {
        this.rowMenu.shift();
      }
    }
  }

  mapRespondSearch(data, params, isSale: boolean) {
    this.messages = { ...this.messages, emptyMessage: _.cloneDeep(global?.messageTable?.emptyMessage) };
    this.isLoading = false;
    this.listData = isSale ? data.content : data || [];
    this.countCheckedOnPage = 0;
    for (const item of this.listData) {
      if (
        this.listSelectedOld.findIndex((itemOld) => {
          return itemOld.customerCode === item.customerCode;
        }) > -1
      ) {
        this.countCheckedOnPage += 1;
      }
      const titleCodeConfig = this.listTitleConfig?.find((o) => o.code === `TITLE_${item.customerType}`)?.value;
      const titleOfDivision = this.listDivision?.find((o) => o.code === item.customerType)?.titleCode;
      item.showBtnRevenueShare =
        !_.isEmpty(item.manageRM) &&
        !_.isEmpty(titleCodeConfig) &&
        (this.objFunction.revenueShare || titleCodeConfig?.includes(titleOfDivision));
    }
    if (isSale) {
      this.pageable = {
        totalElements: data.totalElements,
        totalPages: data.totalPages,
        currentPage: data.number,
        size: global?.userConfig?.pageSize,
      };
      this.isCount = true;
    } else {
      if (!this.isCount) {
        this.pageable = {
          totalElements: this.listData.length,
          totalPages: 0,
          currentPage: this.params.pageNumber,
          size: this.params.pageSize,
        };
      }
    }
    this.pageable.currentPage = this.params?.pageNumber || 0;
    this.pageable.size = this.params?.pageSize || global?.userConfig?.pageSize;
    this.checkAll = this.listData.length > 0 && this.countCheckedOnPage === this.listData.length;
    this.prevParams = params;
    this.toggleLoading(false);
  }

  getCountCustomer(params) {
    this.isCount = false;
    this.subCount?.unsubscribe();
    let apiCount: Observable<any>;
    if (this.searchQuickType === 'BUSINESS_REGISTRATION_NUMBER' || params.customerType !== Division.INDIV) {
      apiCount = this.customerApi.countSme(params);
    } else {
      apiCount = this.customerApi.count(params);
    }
    apiCount.pipe(catchError(() => of(0))).subscribe((count) => {
      this.isCount = true;
      count = count || 0;
      this.pageable = {
        totalElements: count,
        totalPages: Math.floor(count / this.params.pageSize),
        currentPage: this.params?.pageNumber || 0,
        size: this.params.pageSize,
      };
    });
  }

  onClickSearch(isSearch?: boolean, type?: string){
    this.isLoadTable = true;
    this.search(isSearch, type);
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.pageNumber = pageInfo.offset;
    this.search(false);
  }

  handleChangePageSize(event) {
    this.params.pageSize = event;
    this.params.pageNumber = 0;
    if (this.isShowAdvance) {
      this.search(true, 'advance');
    } else {
      this.search(true, 'basic');
    }
  }

  switchSearch() {
    this.isShowAdvance = !this.isShowAdvance;
    this.setOpenCodeDateShow();

    if (!this.isRMorUB) {
      if (!this.isShowAdvance) {
        let itemSelected = this.listManageType.filter((item) => {
          return item.selected === true;
        });
        this.searchForm.controls.manageType.setValue(itemSelected[0].code);
      }
    } else {
      this.searchForm.controls.manageType.setValue(this.listManageType[0].code);
    }
  }

  openMore() {
    this.isOpenMore = !this.isOpenMore;
  }

  fillIndustry(value) {
    this.searchForm.controls.industry.setValue(value);
  }

  onBlur(e) {}

  historyAssign(item: any) {
    const modal = this.modalService.open(HistoryAssignmentComponent, {
      windowClass: 'customer-history-assignment',
    });
    modal.componentInstance.hrsCode = item?.customerCode;
    modal.componentInstance.contractNumber = item?.contractNumber === undefined ? '' : item?.contractNumber;
    modal.componentInstance.systemAssign = this.systemAssign;
    modal.componentInstance.isShowColumnContractNumber = this.isShowColumnContractNumber;
    modal.componentInstance.customerTypeSale = this.prevParams?.customerTypeSale;
    modal.componentInstance.customerType = this.prevParams?.customerType;
    modal.componentInstance.manageType = this.searchForm.controls.manageType.value;
  }

  onActive(event) {
    if (!this.showAction) {
      return;
    }

    if (event.type === 'dblclick') {
      const item = event.row;
      if (!_.isEmpty(_.trim(item.customerTypeMerge)) && !_.isEmpty(_.trim(item.customerCode))) {
        this.getPropData();
        this.router.navigate(["/customer360/customer-manager", 'detail', _.toLower(item.customerTypeMerge), item.customerCode], {
          skipLocationChange: true,
          queryParams: {
            branchCode: item.branchCode,
            manageRM: item.manageRM,
            showBtnRevenueShare: item.showBtnRevenueShare,
            isShowCreateOppBtn: this.isShowCreateOppBtn,
            taxCode:item.taxCode
          },
        });
      }
    }

    if (event.type === 'click') {
      this.itemSelected = event.row;
    }
  }

  getPropData() {
    this.prop = {
      isLoadTable: this.isLoadTable,
      isShowAdvance: this.isShowAdvance,
      isOpenMore: this.isOpenMore,
      searchAdvance: this.searchAdvance,
      listSegment: this.listSegment,
      sectors: this.sectors,
      listCustomerObject: this.listCustomerObject,
      listRmManager: this.listRmManager,
      listData: this.listData,
      pageable: this.pageable,
      params: this.prevParams,
      dataForm: this.searchForm.getRawValue(),
      isSmeTable: this.isSmeTable,
      listRmSale: this.listRmSale,
      isCustomerTypeSale: this.isCustomerTypeSale,
    };
    if (this.type !== ComponentType.Modal) {
      this.sessionService.setSessionData(FunctionCode.CUSTOMER_360_MANAGER, this.prop);
    }
  }

  checkHoliday(item) {
    /* datnv2.os - CRMCN-2824 - Bỏ check thời gian phân giao sau 8 ngày làm việc đầu tháng với KHCN */

    // if (!this.is8FirstDayInMonth && item.customerTypeMerge === Division.INDIV) {
    //   this.confirmService
    //     .warn(this.notificationMessage.ASSIGN007.replace('{{ days }}', this.numberFirstDayInMonth))
    //     .then(() => {
    //       this.assign(item);
    //     });
    // } else {
    //   this.assign(item);
    // }
    this.assign(item);
  }

  assign(item) {
    const formSearch = {
      isAssignRm: true,
      rmBlock: { value: item.customerTypeMerge, disabled: true },
    };
    const config = {
      selectionType: SelectionType.single,
    };

    const IS_ASSIGN_INDIV = item.customerTypeMerge === Division.INDIV;
    const ASSIGN_ERROR_CODE008 = IS_ASSIGN_INDIV ? 'ASSIGN_INDIV_008' : 'ASSIGN008';
    const ASSIGN_ERROR_CODE009 = IS_ASSIGN_INDIV ? 'ASSIGN_INDIV_009' : 'ASSIGN009';
    const ASSIGN_ERROR_CODE010 = IS_ASSIGN_INDIV ? 'ASSIGN_INDIV_010' : 'ASSIGN010';

    const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
    modal.componentInstance.config = config;
    modal.componentInstance.dataSearch = formSearch;
    modal.result
      .then((res) => {
        if (res?.listSelected?.length > 0) {
          if (Utils.trimNullToEmpty(res.listSelected[0]?.t24Employee?.employeeCode) === '') {
            this.confirmService.warn(this.notificationMessage[ASSIGN_ERROR_CODE010]);
            return;
          }
          let validateApis = [
            this.rmApi.checkRmAssign(res.listSelected[0].hrisEmployee?.employeeId).pipe(catchError((e) => of(false))),
            this.rmApi
              .checkRmNTSNotAssign([res.listSelected[0].t24Employee?.employeeCode])
              .pipe(catchError((e) => of(false))),
          ];

          if (IS_ASSIGN_INDIV) {
            validateApis.push(
              this.rmApi.checkAssignUB(res.listSelected[0].hrisEmployee?.employeeId).pipe(catchError((e) => of(false)))
            );
          }
          forkJoin(validateApis).subscribe(([checkRmAssign, listRm, checkAssignUB]) => {
            if (checkRmAssign || _.size(listRm) > 0 || (IS_ASSIGN_INDIV && !checkAssignUB)) {
              const messages = [];
              /* datnv2.os - CRMCN-2824 - Điều chỉnh độ ưu tiên của messages */
              // if (checkRmAssign) {
              //   messages.push(this.notificationMessage[ASSIGN_ERROR_CODE008]);
              // }
              // if (_.size(listRm) > 0) {
              //   messages.push(this.notificationMessage[ASSIGN_ERROR_CODE009]);
              // }
              // if (IS_ASSIGN_INDIV && !checkAssignUB) {
              //   messages.push(this.notificationMessage.ASSIGN_INDIV_011);
              // }
              if (IS_ASSIGN_INDIV && !checkAssignUB) {
                messages.push(this.notificationMessage.ASSIGN_INDIV_011);
              } else {
                if (checkRmAssign && !(_.size(listRm) > 0)) {
                  messages.push(this.notificationMessage[ASSIGN_ERROR_CODE008]);
                } else {
                  messages.push(this.notificationMessage[ASSIGN_ERROR_CODE009]);
                }
              }
              this.confirmService.error(messages);
              return;
            } else {
              this.toggleLoading(true);
              const data = {
                rm: res.listSelected[0].t24Employee?.employeeCode + ' - ' + res.listSelected[0].hrisEmployee?.fullName,
                hrsCode: res.listSelected[0].hrisEmployee?.employeeId,
                branchCode: res.listSelected[0].t24Employee?.branchCode,
                code: res.listSelected[0].t24Employee?.employeeCode,
                is8FirstDayInMonth: this.is8FirstDayInMonth,
                divisionCode: item.customerTypeMerge,
                customers: [
                  {
                    customerBranchCode: item.branchCode,
                    customerCode: item.customerCode,
                    customerName: item.customerName,
                    customerType: item.customerType,
                    sector: item.sector,
                    customerTypeSector: item.customerTypeSector,
                    customerTypeMerge: item.customerTypeMerge,
                  },
                ],
                rsId: this.objFunction?.rsId || null,
                scope: Scopes.VIEW,
              };
              let api;
              if (IS_ASSIGN_INDIV) {
                api = this.customerAssignmentApi.assignIndiv(data);
              } else {
                api = this.customerAssignmentApi.assignOtherIndiv(data);
              }
              api.subscribe(
                () => {
                  this.search(true);
                  this.messageService.success(this.notificationMessage.success);
                  this.loading.emit(false);
                },
                (e) => {
                  if (e?.error?.code) {
                    if (e.error.popup && e.error.data) {
                      let messCode = 'notificationMessage.' + e.error.code;
                      if (IS_ASSIGN_INDIV && e.error.code === 'ASSIGN_INDIV_004') {
                        messCode = 'notificationMessage.' + e.error.code + '_SINGLE';
                      }
                      this.translate
                        .get(messCode, {
                          customers: e.error.data.join(', '),
                          rm: data.rm,
                        })
                        .subscribe((res: string) => {
                          this.confirmService.error(res);
                        });
                    } else {
                      this.messageService.warn(this.notificationMessage[e.error.code]);
                    }
                  } else {
                    this.messageService.error(this.notificationMessage.error);
                  }
                  this.toggleLoading(false);
                }
              );
            }
          });
        }
      })
      .catch(() => {});
  }

  assignSale(item) {
    const formSearch = {
      rmBlock: { value: this.searchForm.controls.customerType.value, disabled: true },
      divisionCode: this.searchForm.controls.customerType.value,
      productGroup: this.searchForm.controls.productGroup.value,
    };
    const config = {
      selectionType: SelectionType.single,
    };
    const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
    modal.componentInstance.config = config;
    modal.componentInstance.dataSearch = formSearch;
    modal.componentInstance.isRmSale = true;
    modal.result
      .then((resData) => {
        if (resData?.listSelected?.length > 0) {
          if (resData.listSelected[0].t24Employee?.branchCode === item.branchCode) {
            const messageAssign = `Bạn đang thực hiện phân giao quản lý sale KH ${item.customerCode} cho RM ${resData.listSelected[0].t24Employee?.employeeCode}, xác nhận đồng ý để hoản tất phân giao`;
            this.confirmService.openModal(ConfirmType.Confirm, messageAssign).then((res) => {
              if (res) {
                this.toggleLoading(true);
                const paramAssign = {
                  rmCode: resData.listSelected[0].t24Employee?.employeeCode,
                  saleManagementId: item.id,
                };

                this.customerApi.assignRMSale(paramAssign).subscribe(
                  (resMessage) => {
                    this.toggleLoading(false);
                    this.messageService.success(this.notificationMessage.success);
                  },
                  (e) => {
                    this.toggleLoading(false);
                    if (e?.error?.description) {
                      this.messageService.error(e?.error?.description);
                    } else {
                      this.messageService.error(this.notificationMessage.error);
                    }
                  }
                );
              }
            });
          } else {
            this.messageService.error('RM phân giao  không thuộc cùng chi nhánh chốt hợp đồng');
          }
        }
      })
      .catch(() => {});
  }

  exportFile() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+(this.pageable?.totalElements || 0) === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.toggleLoading(true);
    let api;
    if (this.searchForm.get('customerTypeSale').value === 2 && this.isCustomerTypeSale) {
      api = this.customerApi.createFileSaleNew(this.prevParams);
    } else {
      if (this.searchQuickType === 'BUSINESS_REGISTRATION_NUMBER' || this.prevParams.customerType !== Division.INDIV) {
        api = this.customerApi.createFileSME(this.prevParams);
      } else {
        api = this.customerApi.createFile(this.prevParams);
      }
    }

    api.subscribe(
      (res) => {
        if (Utils.isStringNotEmpty(res)) {
          this.download(res);
        } else {
          this.messageService.error(this.notificationMessage?.export_error || '');
          this.toggleLoading(false);
        }
      },
      () => {
        this.messageService.error(this.notificationMessage?.export_error || '');
        this.toggleLoading(false);
      }
    );
  }

  getRmManager(listBranch, rmBlock?: string, isProp?: boolean) {
    this.listRmManager = [];
    this.rmApi
      .post('findAll', {
        page: 0,
        size: maxInt32,
        crmIsActive: true,
        branchCodes: listBranch?.filter((code) => !Utils.isStringEmpty(code)),
        rmBlock: rmBlock || '',
        rsId: this.objFunction?.rsId,
        scope: Scopes.VIEW,
        isAssignRm: _.get(this.dataSearch, 'isCheckGroupCBQL.value', false),
      })
      .subscribe((res) => {
        const listRm: any[] =
          res?.content?.map((item) => {
            return {
              code: item?.hrisEmployee?.employeeId || '',
              rmCode: item?.t24Employee?.employeeCode,
              displayName:
                Utils.trimNullToEmpty(item?.t24Employee?.employeeCode) +
                ' - ' +
                Utils.trimNullToEmpty(item?.hrisEmployee?.fullName),
            };
          }) || [];
        if (this.listBranches.length > 1) {
          this.listRmManager = [
            ...[
              { code: '', displayName: this.fields.all, rmCode: '' },
              { code: 'UN_ASSIGN', displayName: this.fields.unAssign, rmCode: 'UN_ASSIGN' },
            ],
            ...listRm,
          ];
        } else {
          this.listRmManager = listRm;
        }
        if (!this.listRmManager?.find((item) => item.code === this.currUser?.hrsCode)) {
          this.listRmManager.push({
            code: this.currUser?.hrsCode,
            displayName:
              Utils.trimNullToEmpty(this.currUser?.code) + ' - ' + Utils.trimNullToEmpty(this.currUser?.fullName),
          });
        }
        this.searchForm.controls.hrsCode.setValue(_.get(this.listRmManager, '[0].code', null));
      });
  }

  download(fileId: string) {
    this.isDownLoading = true;
    this.sessionService.setSessionData(ExportKey.Customer, true);
    this.fileService
      .downloadFile(fileId, 'crm_customer.xlsx', ExportKey.Customer)
      .pipe(catchError((e) => of(false)))
      .subscribe((res) => {
        this.toggleLoading(false);
        if (!res) {
          this.messageService.error(this.notificationMessage.error);
        }
        this.isDownLoading = false;
        this.sessionService.setSessionData(ExportKey.Customer, false);
      });
  }

  onCheckboxTransactionFrequencyFn(row, isChecked) {
    row.isChecked = isChecked;
    if (isChecked) {
      this.countDataTransactionFrequencyGroup += 1;
    } else {
      this.countDataTransactionFrequencyGroup -= 1;
    }
    this.checkTransactionFrequencyGroupAll =
      this.countDataTransactionFrequencyGroup === this.listTransactionFrequency.length;
    if (this.checkTransactionFrequencyGroupAll) {
      this.searchForm.controls.transactionFrequencyText.setValue(this.fields.all);
    } else {
      const list = this.listTransactionFrequency?.filter((item) => item.isChecked) || [];
      const text =
        list
          .map((x) => x.name)
          .join(', ')
          .substring(0, 30) + (list.length > 2 ? '...' : '');
      this.searchForm.controls.transactionFrequencyText.setValue(text);
    }
  }

  onCheckboxTransactionFrequencyAllFn(isChecked) {
    this.checkTransactionFrequencyGroupAll = isChecked;
    if (!isChecked) {
      this.countDataTransactionFrequencyGroup = 0;
    } else {
      this.countDataTransactionFrequencyGroup = this.listTransactionFrequency.length;
    }
    for (const item of this.listTransactionFrequency) {
      item.isChecked = isChecked;
    }
    if (this.checkTransactionFrequencyGroupAll) {
      this.searchForm.controls.transactionFrequencyText.setValue(this.fields.all);
    } else {
      const list = this.listTransactionFrequency?.filter((item) => item.isChecked) || [];
      const text =
        list
          .map((x) => x.name)
          .join(', ')
          .substring(0, 30) + (list.length > 2 ? '...' : '');
      this.searchForm.controls.transactionFrequencyText.setValue(text);
    }
  }

  setSector() {
    this.toggleLoading(true);
    //TODO: Remove when config segment VIP -> PRIORITY for search sectors
    let segmentValue = (this.searchForm.controls.segment.value === 'PRIORITY') ? 'VIP' : this.searchForm.controls.segment.value;

    this.saleSuggestApi.getSectorBySegment(segmentValue)
    .subscribe((res) => {
      if (res) {
        this.sectors = [];
        res.forEach((item) => {
          let obj = {
            id: item
          };
          this.sectors.push(obj);
        });
        this.selectedSectors = [];
        this.sectors.forEach(item => {
          this.selectedSectors.push(item)
        });
      }
    });
    this.toggleLoading(false);
  }

  toggleLoading(isLoading:boolean){
    this.loading.emit(isLoading);
    this.isLoading = isLoading;
  }

  isChecked(item) {
    return (
      this.listSelectedOld.findIndex((itemOld) => {
        return itemOld.customerCode === item.customerCode;
      }) > -1
    );
  }

  onCheckboxAllFn(isChecked) {
    this.checkAll = isChecked;
    if (!isChecked) {
      this.countCheckedOnPage = 0;
    } else {
      this.countCheckedOnPage = this.listData?.length;
    }
    for (const item of this.listData) {
      item.isChecked = isChecked;
      if (isChecked) {
        if (
          this.listSelectedOld.findIndex((itemOld) => {
            return itemOld.customerCode === item.customerCode;
          }) === -1
        ) {
          this.listSelectedOld.push(item);
        }
      } else {
        this.listSelectedOld = this.listSelectedOld.filter((itemOld) => {
          return itemOld.customerCode !== item.customerCode;
        });
      }
    }
    this.selectedCustomer.emit(this.listSelectedOld);
  }

  onCheckboxFn(item, isChecked) {
    if (isChecked) {
      this.listSelectedOld.push(item);
      this.countCheckedOnPage += 1;
    } else {
      this.listSelectedOld = this.listSelectedOld.filter((itemSelected) => {
        return itemSelected.customerCode !== item.customerCode;
      });
      this.countCheckedOnPage -= 1;
    }
    this.checkAll = this.countCheckedOnPage === this.listData.length;
    this.selectedCustomer.emit(this.listSelectedOld);
  }

  onChangeTextSearch(value) {
    this.setOpenCodeDateShow();

    if (!this.isLoading) {
      this.disabledSearchBasic = false;
    }
  }

  viewCreateRevenue(item) {
    this.isLoading = true;
    this.revenueShareApi.checkCutomerShare(item.customerCode).subscribe(
      (res) => {
        if (!res) {
          this.isLoading = false;
          this.router.navigate([functionUri.revenue_share, 'create', item.customerCode], {
            queryParams: {
              showBtnRevenueShare: item.showBtnRevenueShare,
            },
          });
        } else {
          this.isLoading = false;
          this.messageService.error(this.notificationMessage.customerNotShare);
        }
      },
      (e) => {
        this.messageService.error(_.get(this.notificationMessage, 'E001'));
        this.isLoading = false;
      }
    );
  }

  isShowRevenue(data) {
    const titleCodeConfig = this.listTitleConfig?.find((item) => item.code === `TITLE_${data.customerType}`)?.value;
    const titleOfDivision = this.listDivision?.find((item) => item.code === data.customerType)?.titleCode;
    data.showBtnRevenueShare =
      !_.isEmpty(data.manageRM) &&
      !_.isEmpty(titleCodeConfig) &&
      (this.objFunction.revenueShare || titleCodeConfig?.includes(titleOfDivision));
    return data.showBtnRevenueShare;
  }

  onChangeCustomerType(event) {
    if (!this.isLoading) {
      this.disabledSearchBasic = false;
    }
  }
  onChangeCustomerTypeAdvance(event) {
    this.params.customerType = event.value
  }

  getRMSaleProduct() {
    this.paramsRmSale.productGroup = this.searchForm.controls.productGroup.value;
    this.paramsRmSale.divisionCode = this.searchForm.controls.customerType.value;
    this.paramsRmSale.branchCodes = this.searchForm.controls.branchCode.value
      ? [this.searchForm.controls.branchCode.value]
      : [];
    this.searchForm.get('rmCode').disable();
    this.customerApi.getRMSale(this.paramsRmSale).subscribe((res) => {
      if (res) {
        this.listRmSale =
          res?.content?.map((item) => {
            return {
              code: item?.t24Employee?.employeeCode || '',
              displayName:
                Utils.trimNullToEmpty(item?.t24Employee?.employeeCode) +
                ' - ' +
                Utils.trimNullToEmpty(item?.hrisEmployee?.fullName),
            };
          }) || [];
        this.searchForm.get('rmCode').enable();
        if (this.listRmSale?.length > 1) {
          this.listRmSale.unshift({ code: '', displayName: this.fields.all });
        }
        this.searchForm.controls.rmCode.setValue(_.first(this.listRmSale)?.code);
      }
    });
  }

  onChangeProductType() {
    this.getRMSaleProduct();
  }

  onChangeCustomerTypeSale(event) {
    if (event.value === 2) {
      this.searchForm.get('rmCode').disable();
      this.searchForm.get('productGroup').disable();
      this.customerApi.getOppProductRule(this.searchForm.controls.customerType.value).subscribe(
        (res) => {
          if (res) {
            this.listProductType = res?.map((item) => {
              return {
                code: item.productGroup,
                displayName: item.productGroup + ' - ' + item.productName,
              };
            });
            this.searchForm.controls.productGroup.setValue(_.first(this.listProductType)?.code);
            if (!_.isEmpty(this.listProductType)) {
              this.getRMSaleProduct();
            } else {
              this.searchForm.get('rmCode').enable();
            }
            this.currState = {
              ...this.currState,
              listProductType: this.listProductType,
            };
            this.sessionService.setSessionData(`${FunctionCode.CUSTOMER_360_MANAGER}_STATE`, this.currState);
          }
          this.searchForm.get('productGroup').enable();
        },
        (e) => {
          this.searchForm.get('productGroup').enable();
          this.searchForm.get('rmCode').enable();
        }
      );
    }
  }

  onChangeManageType(event) {}

  checkDisableExcel() {
    if (this.searchForm.get('customerTypeSale').value === 1) {
      return !this.isCount || this.isDownLoading;
    } else {
      return !this.isCustomerTypeSale || this.isDownLoading;
    }
  }

  showManagerType() {
    let result = false;
    if (this.searchForm.get('customerType').value === 'INDIV') {
      result = true;
      if (this.searchForm.get('customerTypeSale').value === 2) {
        result = false;
      } else {
        result = true;
      }
    }
    return result;
  }

  setOpenCodeDateShow(): void {
    const paramCustomerType = this.params.customerType;
    const searchFormCustomerType = this.searchForm.get('customerType').value;
    let isSME = false;

    if (this.isShowAdvance) {
      isSME = searchFormCustomerType === CustomerType.SME;
    } else {
      isSME = paramCustomerType === CustomerType.SME;
    }

    this.isOpenCodeDateShow = isSME ? true : false;
  }

  // CRMCN-3672 - NAMNH - START
  createOppKHCN(row) {
    const url = this.router.serializeUrl(
      this.router.createUrlTree([functionUri.selling_opp_indiv, 'detail-v2'], {
        skipLocationChange: true,
        queryParams: {
          customerCode:  row.customerCode,
          code: null,
        }
      })
    );
    window.open(url, '_blank');
  }

  checkScopes(codes: Array<any>) {
    if (Utils.isArrayEmpty(codes)) return false;
    return Utils.isArrayNotEmpty(_.filter(this.scopes, (x) => codes.includes(x)));
  }

  checkIsDisableCreateOppBtn() {
    this.isShowCreateOppBtn = this.checkScopes(['CREATE_OPPORTUNITY_SALE']) ? true : false;
  }

  displayButtons() {
    this.checkIsDisableCreateOppBtn();
  }
  // CRMCN-3672 - NAMNH - END

  createGroupChat11(row) {
    this.toggleLoading(true);
    const customerCode = row.customerCode;
    this.mbeeChatService.createGroupChat(customerCode).subscribe(value => {
      if (value.data) {
        createGroupChat11(value.data);
      } else {
        this.messageService.error(value.message);
      }
      this.toggleLoading(false);
    }, error => {
      this.messageService.error(JSON.parse(error.error)?.messages.vn);
      this.toggleLoading(false);
    });

  }

  onShowMenu(e) {
    this.isClickEvent = true;
  }

  onHideMenu(e) {
    this.isClickEvent = false;
  }

  getDataFromParams(){
    if(this.route.snapshot.queryParams?.id){
      this.params.customerType = this.route.snapshot.queryParams.division;
      this.textSearch = this.route.snapshot.queryParams.id;
      this.onChangeTextSearch(0);
      this.isShowAdvance = false;
      this.disabledSearchBasic = false;
      this.search(true, 'basic');
    }
  }
}
