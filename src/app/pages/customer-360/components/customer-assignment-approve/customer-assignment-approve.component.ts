import { global } from '@angular/compiler/src/util';
import { Component, Injector, OnInit } from '@angular/core';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { ExportExcelService } from 'src/app/core/services/export-excel.service';
import { Division, FunctionCode, maxInt32, Scopes, SessionKey } from 'src/app/core/utils/common-constants';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { CustomerAssignmentApi } from '../../apis';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { ConfirmAssignmentModalComponent } from '../confirm-assignment-modal/confirm-assignment-modal.component';
import { catchError } from 'rxjs/operators';
import { forkJoin, of } from 'rxjs';
import { ListErrorComponent } from '../list-error-modal/list-error.component';
import { FileService } from 'src/app/core/services/file.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-customer-assignment-approve',
  templateUrl: './customer-assignment-approve.component.html',
  styleUrls: ['./customer-assignment-approve.component.scss'],
})
export class CustomerAssignmentApproveComponent extends BaseComponent implements OnInit {
  listData = [];
  listBlock = [];
  listSelected = [];
  limit = global.userConfig.pageSize;
  formSearch = this.fb.group({
    customerCode: [''],
    customerName: [''],
    blockCode: [''],
    rmCode: [''],
    rmName: [''],
  });
  paramSearch = {
    size: this.limit,
    page: 0,
    search: '',
    rsId: '',
    scope: Scopes.VIEW,
  };
  prevParams: any;
  branchesCode: string[];
  pageable: Pageable;
  isAdvance = false;
  messages = global.messageTable;
  listBranch = [];
  checkAll = false;
  countCheckedOnPage = 0;
  parentUrl = '';
  canApprove = false;

  constructor(
    injector: Injector,
    private exportExcelService: ExportExcelService,
    private customerAssignmentApi: CustomerAssignmentApi,
    private categoryService: CategoryService,
    private fileService: FileService,
    private activatedRoute: ActivatedRoute,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_ASSIGNMENT_APPROVED}`);
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
    if (this.prop) {
      this.prevParams = this.prop?.paramSearch;
      this.paramSearch.search = this.prop?.paramSearch?.search;
    }
    const rsIdOfCustomer360 = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`)?.rsId;
    this.paramSearch.rsId = rsIdOfCustomer360;
    this.isLoading = true;
  }

  ngOnInit(): void {
    if (this.objFunction) {
      this.isLoading = true;
      forkJoin([
        this.categoryService.getBranchesOfUser(this.paramSearch.rsId, Scopes.VIEW),
        this.categoryService.getBlocksCategoryByRm({ isAssignApprove: true }),
      ]).subscribe(
        ([listBranches, listBlock]) => {
          this.listBranch = listBranches;
          this.branchesCode = listBranches?.map((item) => item.code);
          const priority = this.sessionService.getSessionData(SessionKey.KHOI_PRIORITY);
          this.listBlock = listBlock.map((item) => {
            return {
              code: item.code,
              displayName: item.code + ' - ' + item.name,
              name: item.name,
              order:
                _.findIndex(priority, (i) => i.code === item.code) === -1
                  ? 99
                  : _.findIndex(priority, (i) => i.code === item.code),
            };
          });
          this.listBlock = _.orderBy(this.listBlock, 'order');
          this.formSearch.controls.blockCode.setValue(_.first(this.listBlock)?.code);

          this.search(true);
        },
        () => {
          this.isLoading = false;
        }
      );
    }
  }

  isShow() {
    this.isAdvance = !this.isAdvance;
  }

  search(isSearch: boolean) {
    let params: any;
    if (isSearch) {
      this.paramSearch.page = 0;
      const paramSearch = this.formSearch.getRawValue();
      params = { ...paramSearch, ...this.paramSearch };
      params.branchCodes = this.branchesCode;
      delete params.search;
    } else {
      if (!this.prevParams) {
        return;
      }
      params = { ...this.prevParams, page: this.paramSearch.page };
    }
    this.getData(params);
  }

  getData(params) {
    this.isLoading = true;
    this.customerAssignmentApi.searchPendingApproval(params).subscribe(
      (result) => {
        if (params.blockCode !== this.prevParams?.blockCode) {
          this.listSelected = [];
        }
        this.prevParams = params;
        this.countCheckedOnPage = 0;
        this.listData = result?.content || [];
        this.listData?.forEach((item) => {
          if (this.listSelected?.findIndex((itemSelected) => item.id === itemSelected.id) > -1) {
            this.countCheckedOnPage += 1;
          }
        });
        this.checkAll = this.listData.length > 0 && this.countCheckedOnPage === this.listData.length;
        this.pageable = {
          totalElements: result?.totalElements,
          totalPages: result?.totalPages,
          currentPage: result?.number,
          size: this.limit,
        };
        this.checkApprovePermission();
        this.isLoading = false;
      },
      () => {
        this.messageService.error(this.notificationMessage.E001);
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.page = pageInfo.offset;
    this.search(false);
  }

  isApproved(item, type) {
    this.save(type, [item]);
  }

  isApprovedList(listData: any[], type) {
    if (listData.length === 0) {
      this.messageService.warn(this.notificationMessage.noRecordSelected);
      return;
    }
    this.save(type, listData);
  }

  isApprovedAll(type) {
    if (this.pageable.totalElements === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    const modalConfirm = this.modalService.open(ConfirmAssignmentModalComponent, {
      windowClass: 'confirm-approved-modal',
    });
    modalConfirm.componentInstance.isApproved = type;
    modalConfirm.result
      .then((res) => {
        if (res) {
          const data: any = { ...this.prevParams };
          data.page = 0;
          data.size = maxInt32;
          data.note = res.note;
          data.isApprove = type;

          if (type) data.exceptionReason = res.exceptionReason;

          this.isLoading = true;

          this.customerAssignmentApi.approveOrRefuseAll(data).subscribe(
            (fileId) => {
              this.checkApprovedOrRefuseAll(fileId, 0, this.pageable.totalElements);
            },
            (e) => {
              if (e?.error) {
                this.messageService.error(e?.error?.description);
              } else {
                this.messageService.error(this.notificationMessage.error);
              }
              this.isLoading = false;
            }
          );
        }
      })
      .catch(() => { });
  }

  checkApprovedOrRefuseAll(fileId, count: number, countData: number, isAll: boolean = true) {
    this.customerAssignmentApi.checkFileApproveOfRefuse(fileId).subscribe(
      (res) => {
        if (res?.status === 'PENDING') {
          if (count >= 3000) {
            this.messageService.error(this.notificationMessage.error);
            this.isLoading = false;
            return;
          }
          const timer = setTimeout(() => {
            count += 1;
            this.checkApprovedOrRefuseAll(fileId, count, countData);
            clearTimeout(timer);
          }, 5000);
        } else if (res.status === 'FAIL') {
          this.messageService.error(this.notificationMessage.error);
          this.isLoading = false;
          this.search(false);
        } else {
          this.customerAssignmentApi
            .getDataApproveOfRefuseError(fileId)
            .pipe(catchError(() => of(undefined)))
            .subscribe((dataError) => {
              if (dataError?.length > 0) {
                this.openModalError(dataError, countData);
                this.customerAssignmentApi.exportFileErrorApprove(fileId).subscribe((requestId) => {
                  if (!_.isEmpty(requestId)) {
                    this.fileService
                      .downloadFile(requestId, 'danh-sach-loi.xlsx')
                      .pipe(catchError((e) => of(false)))
                      .subscribe((res) => {
                        if (!res) {
                          this.messageService.error(this.notificationMessage.error);
                        }
                      });
                  }
                });
              } else {
                this.messageService.success(this.notificationMessage.success);
              }
              this.paramSearch.page = isAll ? 0 : this.paramSearch.page;
              this.search(false);
              this.listSelected = [];
            });
        }
      },
      (e) => {
        this.messageService.error(this.notificationMessage.error);
        this.isLoading = false;
      }
    );
  }

  openModalError(data, countTotal: number) {
    const modalError = this.modalService.open(ListErrorComponent, { windowClass: 'list__customer-error' });
    modalError.componentInstance.dataError = data;
    modalError.componentInstance.countTotal = countTotal;
    modalError.result.then(() => { }).catch(() => { });
  }

  save(type: boolean, listData: any) {
    const modalConfirm = this.modalService.open(ConfirmAssignmentModalComponent, {
      windowClass: 'confirm-approved-modal',
    });
    modalConfirm.componentInstance.isApproved = type;
    modalConfirm.result
      .then((res) => {
        if (!res) {
          return;
        }
        let api, data;
        data = {
          note: res.note,
          ...type ? { exceptionReason: res.exceptionReason } : {},
          lstData: listData,
          rsId: this.paramSearch.rsId,
          scope: Scopes.VIEW,
          blockCode: this.prevParams?.blockCode,
        };
        if (type) {
          api = this.customerAssignmentApi.approveCustomer(data);
        } else {
          api = this.customerAssignmentApi.refuseCustomer(data);
        }
        this.isLoading = true;

        api.subscribe(
          (fileId) => {
            this.checkApprovedOrRefuseAll(fileId, 0, _.size(listData), false);
          },
          (e) => {
            if (e?.error) {
              this.messageService.error(e?.error?.description);
            } else {
              this.messageService.error(this.notificationMessage.error);
            }
            this.isLoading = false;
          }
        );
      })
      .catch(() => { });
  }

  onCheckboxAllFn(isChecked) {
    this.checkAll = isChecked;
    if (!isChecked) {
      this.countCheckedOnPage = 0;
    } else {
      this.countCheckedOnPage = this.listData.length;
    }
    for (const item of this.listData) {
      item.isChecked = isChecked;
      if (isChecked) {
        if (this.listSelected.findIndex((itemChoose) => itemChoose.id === item.id) === -1) {
          this.listSelected.push(item);
        }
      } else {
        this.listSelected = this.listSelected.filter((itemChoose) => itemChoose.id !== item.id);
      }
    }
  }

  isChecked(item) {
    return this.listSelected?.findIndex((itemSelected) => itemSelected.id === item.id) > -1;
  }

  onCheckboxFn(item, isChecked) {
    if (isChecked) {
      this.listSelected.push(item);
      this.countCheckedOnPage += 1;
    } else {
      this.listSelected = this.listSelected.filter((itemSelected) => itemSelected.id !== item.id);
      this.countCheckedOnPage -= 1;
    }
    this.checkAll = this.countCheckedOnPage === this.listData.length;
  }

  exportFile() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+(this.pageable?.totalElements || 0) === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    const data = [];
    let obj: any = {};
    const params = { ...this.prevParams };
    params.page = 0;
    params.size = maxInt32;
    this.customerAssignmentApi.searchPendingApproval(params).subscribe((result) => {
      if (result) {
        if (result.content.length === 0) {
          this.messageService.warn(this.notificationMessage.noRecord);
          return;
        }
        result.content.forEach((item, index) => {
          obj = {};
          obj[this.fields.order] = index + 1;
          obj[this.fields.customerCode] = item.customerCode;
          obj[this.fields.customerName] = item.customerName;
          obj[this.fields.branch] = item.customerBranchCode;
          obj[this.fields.rmCodeManagement] = item.assigningRmCode;
          obj[this.fields.rmNameManagement] = item.assigningRmName;
          obj[this.fields.rmCodeManagementNew] = item.rmCode;
          obj[this.fields.rmNameManagementNew] = item.rmName;
          obj[this.fields.description] = item.note;
          data.push(obj);
        });
        this.exportExcelService.exportAsExcelFile(data, 'danh-sach-cho-duyet');
      }
    });
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      if (this.formSearch.get('blockCode').value === Division.SME) {
        const item = event.row;
        this.router.navigate(['/customer360/customer-manager', 'detail', _.toLower(item.customerTypeMerge), item.customerCode], {
          skipLocationChange: true,
          queryParams: {
            manageRM: item.manageRM,
            showBtnRevenueShare: item.showBtnRevenueShare,
          },
        });
      }
    }
  }

  checkApprovePermission() {
    this.canApprove = this.formSearch.get('blockCode').value !== Division.SME ||
      (this.formSearch.get('blockCode').value === Division.SME && this.objFunction?.scopes.includes('APPROVE_SME'));
  }
}
