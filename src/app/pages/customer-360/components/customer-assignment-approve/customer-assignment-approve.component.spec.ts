import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerAssignmentApproveComponent } from './customer-assignment-approve.component';

describe('CustomerAssignmentApproveComponent', () => {
  let component: CustomerAssignmentApproveComponent;
  let fixture: ComponentFixture<CustomerAssignmentApproveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerAssignmentApproveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerAssignmentApproveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
