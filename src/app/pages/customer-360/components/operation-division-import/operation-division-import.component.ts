import { Observable } from 'rxjs';
import { OperationDivisionApi } from '../../apis/operation-division.api';
import { global } from '@angular/compiler/src/util';
import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { BaseComponent } from 'src/app/core/components/base.component';
import _ from 'lodash';
import { FunctionCode, functionUri, maxInt32, Scopes } from 'src/app/core/utils/common-constants';
import { ExportExcelService } from 'src/app/core/services/export-excel.service';
import { FileService } from 'src/app/core/services/file.service';

@Component({
  selector: 'app-operation-division-import',
  templateUrl: './operation-division-import.component.html',
  styleUrls: ['./operation-division-import.component.scss'],
})
export class OperationDivisionImportComponent extends BaseComponent implements OnInit {
  isLoading = false;
  fileName: string;
  isFile = false;
  fileImport: File;
  files: any;
  ColumnMode = ColumnMode;
  listDataSuccess = [];
  listDataError = [];
  prop: any;
  limit = global.userConfig.pageSize;
  pageSuccess: Pageable;
  pageError: Pageable;
  paramSuccess = {
    size: this.limit,
    page: 0,
    search: '',
    isTrue: true,
    fileId: '',
  };
  textSearchSuccess = '';
  paramError = {
    size: this.limit,
    page: 0,
    search: '',
    isTrue: false,
    fileId: '',
  };
  textSearchError = '';
  fileId: string;
  isUpload = false;
  isAssign: boolean;
  searchSuccessDone = true;
  searchErrorDone = true;
  messages = global.messageTable;
  timer: any;
  @ViewChild('tableSuccess') tableSuccess: DatatableComponent;
  @ViewChild('tableError') tableError: DatatableComponent;

  constructor(
    injector: Injector,
    private operationDivisionApi: OperationDivisionApi,
    private exportExcelService: ExportExcelService,
    private fileService: FileService
  ) {
    super(injector);
    this.isAssign = _.get(this.route.snapshot.data, 'isAssign') || false;
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');

    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.CUSTOMER_360_OPERATION_BLOCK_MANAGER}`
    );
    if (!this.objFunction?.assignKVH && !this.objFunction?.unAssignKVH) {
      this.router.navigateByUrl(`${functionUri.access_denied}`);
    }
  }

  ngOnInit(): void {}

  searchSuccess(isSearch: boolean) {
    this.isLoading = true;
    this.searchSuccessDone = false;
    if (isSearch) {
      this.paramSuccess.page = 0;
      this.textSearchSuccess = this.textSearchSuccess.trim();
      this.paramSuccess.search = this.textSearchSuccess;
    }
    let api: Observable<any>;
    if (this.isAssign) {
      api = this.operationDivisionApi.searchAssign(this.paramSuccess);
    } else {
      api = this.operationDivisionApi.searchUnAssign(this.paramSuccess);
    }
    api.subscribe(
      (listData) => {
        this.listDataSuccess = listData.content || [];
        this.pageSuccess = {
          totalElements: listData.totalElements,
          totalPages: listData.totalPages,
          currentPage: listData.number,
          size: this.limit,
        };
        this.searchSuccessDone = true;
        if (this.searchErrorDone && this.searchSuccessDone) {
          this.isLoading = false;
        }
      },
      () => {
        this.searchSuccessDone = true;
        if (this.searchErrorDone && this.searchSuccessDone) {
          this.isLoading = false;
        }
      }
    );
  }

  searchError(isSearch: boolean) {
    this.isLoading = true;
    this.searchErrorDone = false;
    if (isSearch) {
      this.paramError.page = 0;
      this.textSearchError = this.textSearchError.trim();
      this.paramError.search = this.textSearchError;
    }
    let api: Observable<any>;
    if (this.isAssign) {
      api = this.operationDivisionApi.searchAssign(this.paramError);
    } else {
      api = this.operationDivisionApi.searchUnAssign(this.paramError);
    }
    api.subscribe(
      (listData) => {
        this.listDataError = listData.content || [];
        this.listDataError?.forEach((item) => {
          item.errorType = this.notificationMessage[item.errorType]?.replace('$data', item.errorData);
        });
        this.pageError = {
          totalElements: listData.totalElements,
          totalPages: listData.totalPages,
          currentPage: listData.number,
          size: this.limit,
        };
        this.searchErrorDone = true;
        if (this.searchErrorDone && this.searchSuccessDone) {
          this.isLoading = false;
        }
      },
      () => {
        this.searchErrorDone = true;
        if (this.searchErrorDone && this.searchSuccessDone) {
          this.isLoading = false;
        }
      }
    );
  }

  setPage(pageInfo, type) {
    if (type === 'success') {
      this.paramSuccess.page = pageInfo.offset;
      this.searchSuccess(false);
    } else {
      this.paramError.page = pageInfo.offset;
      this.searchError(false);
    }
  }

  handleFileInput(files) {
    if (files && files.length > 0) {
      this.isFile = true;
      this.fileImport = files.item(0);
      this.fileName = files.item(0).name;
    } else {
      this.isFile = false;
    }
  }

  clearFile() {
    this.isUpload = false;
    this.isFile = false;
    this.fileName = null;
    this.fileImport = null;
    this.files = null;
    this.listDataSuccess = [];
    this.listDataError = [];
    this.fileId = undefined;
    this.pageError = undefined;
    this.pageSuccess = undefined;
    this.textSearchSuccess = '';
    this.textSearchError = '';
    this.paramError.search = '';
    this.paramSuccess.search = '';
  }

  importFile() {
    if (this.isFile && !this.isUpload) {
      this.isLoading = true;
      const formData: FormData = new FormData();
      formData.append('file', this.fileImport);
      const params = {
        isAssign: this.isAssign,
        rsId: this.objFunction.rsId,
        scope: this.isAssign ? Scopes.ASSIGN_KVH : Scopes.UNASSIGN_KVH,
      };
      this.operationDivisionApi.import(formData, params).subscribe(
        (res) => {
          this.fileId = res?.requestId;
          this.paramError.fileId = this.fileId;
          this.paramSuccess.fileId = this.fileId;
          this.checkImportSuccess(this.fileId);
        },
        (e) => {
          if (e?.error) {
            this.messageService.error(e?.error?.description);
          } else {
            this.messageService.error(this.notificationMessage.error);
          }
          this.listDataError = [];
          this.listDataSuccess = [];
          this.isLoading = false;
        }
      );
    }
  }

  save() {
    if (this.isLoading && !this.fileId || this.pageSuccess.totalElements === 0) {
      return;
    }
    this.isLoading = true;
    const data = {
      fileId: this.fileId,
      isAssigned: this.isAssign,
    };
    this.operationDivisionApi.writeData(data).subscribe(
      () => {
        this.messageService.success(this.notificationMessage.success);
        this.isLoading = false;
        this.fileId = undefined;
        this.listDataSuccess = [];
        this.listDataError = [];
        this.back();
      },
      (e) => {
        if (e?.error) {
          this.messageService.warn(e?.error?.description);
        } else {
          this.messageService.error(this.notificationMessage.error);
        }
        this.isLoading = false;
      }
    );
  }

  checkImportSuccess(fileId: string) {
    const interval = setInterval(() => {
      this.operationDivisionApi.checkFileImport(fileId).subscribe((res) => {
        if (res?.status === 'COMPLETE') {
          this.isUpload = true;
          this.isLoading = false;
          this.searchError(true);
          this.searchSuccess(true);
          clearInterval(interval);
        } else if (res?.status === 'FAIL') {
          if (res?.msgError?.includes('FILE_DOES_NOT_EXCEED_RECORDS')) {
            const maxRecord = res?.msgError?.replace('FILE_DOES_NOT_EXCEED_RECORDS_', '');
            const type = this.fileName?.split('.')[this.fileName?.split('.')?.length - 1];
            this.translate
              .get('notificationMessage.FILE_DOES_NOT_EXCEED_RECORDS', { number: maxRecord, type: type })
              .subscribe((res) => {
                this.messageService.error(res);
              });
          } else if (res?.msgError === 'FILE_NO_CONTENT_EXCEPTION') {
            this.messageService.error(this.notificationMessage.FILE_NO_CONTENT_EXCEPTION);
          } else {
            this.messageService.error(this.notificationMessage.CANNOT_READ_DATA_FROM_FILE);
          }
          console.log(res?.status);
          this.isLoading = false;
          clearInterval(interval);
        }
      });
    }, 5000);
  }

  exportExcel(type: boolean) {
    this.isLoading = true;
    const count = type ? this.pageSuccess.totalElements : this.pageError.totalElements;
    const params = type ? this.paramSuccess : this.paramError;
    if (!this.maxExportExcel) {
      return;
    }
    if (+count === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    if (_.lt(+this.maxExportExcel, +count)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.isLoading = true;
    this.operationDivisionApi.createFileExport(params, this.isAssign).subscribe(
      (fileId) => {
        if (fileId) {
          const fileName = type ? 'danh-sach-thanh-cong.xlsx' : 'danh-sach-loi.xlsx';
          this.fileService.downloadFile(fileId, fileName).subscribe(
            (res) => {
              this.isLoading = false;
              if (!res) {
                this.messageService.error(this.notificationMessage.error);
              }
            },
            () => {
              this.isLoading = false;
            }
          );
        }
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }

  exportSuccess() {
    this.exportExcel(true);
  }

  exportError() {
    this.exportExcel(false);
  }

  downloadTemplate() {
    window.open('/assets/template/operational_division_template.xlsx', '_self');
  }
}
