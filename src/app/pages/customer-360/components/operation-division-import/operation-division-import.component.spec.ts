import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationDivisionImportComponent } from './operation-division-import.component';

describe('OperationDivisionImportComponent', () => {
  let component: OperationDivisionImportComponent;
  let fixture: ComponentFixture<OperationDivisionImportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationDivisionImportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationDivisionImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
