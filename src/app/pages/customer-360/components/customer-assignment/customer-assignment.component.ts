import { global } from '@angular/compiler/src/util';
import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import {
  CommonCategory,
  Division,
  FunctionCode,
  functionUri,
  maxInt32,
  Scopes,
  SessionKey,
} from 'src/app/core/utils/common-constants';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { CustomerAssignmentApi } from '../../apis';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { CustomerModalComponent } from '../customer-modal/customer-modal.component';
import { RmModalComponent } from 'src/app/pages/rm/components';
import { Utils } from 'src/app/core/utils/utils';
import { ExportExcelService } from 'src/app/core/services/export-excel.service';
import { RmApi } from 'src/app/pages/rm/apis';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-customer-assignment',
  templateUrl: './customer-assignment.component.html',
  styleUrls: ['./customer-assignment.component.scss'],
})
export class CustomerAssignmentComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listCustomers = [];
  listTemp = [];
  listDivision = [];
  params = {
    page: 0,
    size: global.userConfig.pageSize,
    search: '',
  };
  pageable: Pageable;
  form = this.fb.group({
    rm: ['', [CustomValidators.required]],
    note: '',
    hrsCode: '',
    branchCode: '',
    code: '',
    is8FirstDayInMonth: true,
    divisionCode: ['', [CustomValidators.required]],
  });
  messages = global.messageTable;
  isSearch = true;
  textSearch = '';
  checkAll = false;
  listSelected = [];
  countCheckedOnPage = 0;
  isFirstDayInMonth: boolean;
  numberFirstDayInMonth = 0;
  isRmKVHOrRmKHCN = false;

  constructor(
    injector: Injector,
    private customerAssignmentApi: CustomerAssignmentApi,
    private exportExcelService: ExportExcelService,
    private rmApi: RmApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_ASSIGNMENT}`);
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
  }

  ngOnInit(): void {
    this.customerAssignmentApi.check8FirstDayInMonth().subscribe((res) => {
      if (res) {
        this.isFirstDayInMonth = res.value;
        this.numberFirstDayInMonth = res.key;
        this.form.controls.is8FirstDayInMonth.setValue(res.value);
      }
    });
    const list = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM);
    this.listDivision = _.map(list, (item) => {
      return {
        code: item.code,
        name: `${item.code} - ${item.name}`,
      };
    });
    this.form.controls.divisionCode.setValue(_.first(this.listDivision)?.code);
    this.form.controls.divisionCode.valueChanges.subscribe((value) => {
      this.clearData(false);
    });
  }

  confirmDialog() {
    if (this.form.valid) {
      if (this.listTemp.length === 0) {
        this.messageService.warn(this.notificationMessage.ASSIGN025);
        return;
      }

      /* datnv2.os - CRMCN-2824 - Bỏ check thời gian phân giao sau 8 ngày làm việc đầu tháng với KHCN */

      // if (!this.isFirstDayInMonth && this.form.controls.divisionCode.value === Division.INDIV) {
      //   if (this.isRmKVHOrRmKHCN) {
      //     const messages = [
      //       this.notificationMessage.ASSIGN006.replace('{{ days }}', this.numberFirstDayInMonth),
      //       this.notificationMessage.ASSIGN007.replace('{{ days }}', this.numberFirstDayInMonth),
      //     ];
      //     this.confirmService.warn(messages).then(() => {
      //       this.save();
      //     });
      //   } else {
      //     this.confirmService
      //       .warn(this.notificationMessage.ASSIGN007.replace('{{ days }}', this.numberFirstDayInMonth))
      //       .then(() => {
      //         this.save();
      //       });
      //     this.save();
      //   }
      // } else {
      //   this.save();
      // }
      this.save();
    } else {
      validateAllFormFields(this.form);
    }
  }

  save() {
    this.isLoading = true;
    const data = cleanDataForm(this.form);
    data.customers = this.listTemp.map((item) => {
      return {
        customerBranchCode: item.branchCode,
        customerCode: item.customerCode,
        customerName: item.customerName,
        customerType: item.customerType,
        sector: item.sector,
        customerTypeSector: item.customerTypeSector,
        customerTypeMerge: item.customerTypeMerge,
      };
    });
    const rsId = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_ASSIGNMENT_APPROVED}`)?.rsId;
    data.rsId = rsId ? rsId : null;
    data.scope = Scopes.VIEW;
    let api;
    if (this.form.get('divisionCode').value === Division.INDIV) {
      api = this.customerAssignmentApi.assignIndiv(data);
    } else {
      api = this.customerAssignmentApi.assignOtherIndiv(data);
    }
    api.subscribe(
      () => {
        this.messageService.success(this.notificationMessage.success);
        this.isLoading = false;
        this.clearData(true);
      },
      (e) => {
        if (e?.error?.code) {
          if (e.error.popup && e.error.data) {
            this.translate
              .get('notificationMessage.' + e.error.code, {
                customers: e.error.data.join(', '),
                /* datnv2.os - CRMCN-2824 - Thay đổi nội dung message: ASSIGN_INDIV_004*/
                // rm: data.rm,
              })
              .subscribe((res: string) => {
                this.confirmService.error(res);
              });
          } else {
            this.messageService.error(this.notificationMessage[e.error.code]);
          }
        } else {
          this.messageService.error(this.notificationMessage.error);
        }
        this.isLoading = false;
      }
    );
  }

  clearData(isDefaultDivision: boolean) {
    this.form.controls.hrsCode.setValue('');
    this.form.controls.branchCode.setValue('');
    this.form.controls.code.setValue('');
    this.form.controls.note.setValue('');
    this.form.controls.rm.setValue('');
    if (isDefaultDivision) {
      this.form.controls.divisionCode.setValue(_.first(this.listDivision)?.code, { emitEvent: false });
    }
    this.form.controls.is8FirstDayInMonth.setValue(this.isFirstDayInMonth);
    this.listTemp = [];
    this.listSelected = [];
    this.listCustomers = [];
    this.textSearch = '';
    this.pageable = undefined;
  }

  selectRm() {
    const formSearch = {
      crmIsActive: { value: 'true', disabled: true },
      isAssignRm: true,
      rmBlock: { value: this.form.controls.divisionCode.value, disabled: true },
    };
    const config = {
      selectionType: SelectionType.single,
    };

    //CRMCN-2948-START
    const IS_ASSIGN_INDIV = this.form.controls.divisionCode.value === Division.INDIV;
    const ASSIGN_ERROR_CODE008 = IS_ASSIGN_INDIV ? 'ASSIGN_INDIV_008' : 'ASSIGN008';
    const ASSIGN_ERROR_CODE009 = IS_ASSIGN_INDIV ? 'ASSIGN_INDIV_009' : 'ASSIGN009';
    const ASSIGN_ERROR_CODE010 = IS_ASSIGN_INDIV ? 'ASSIGN_INDIV_010' : 'ASSIGN010';

    const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
    modal.componentInstance.config = config;
    modal.componentInstance.dataSearch = formSearch;
    modal.componentInstance.listHrsCode = [this.form.controls.hrsCode.value];
    modal.result
      .then((res) => {
        if (res?.listSelected?.length > 0) {
          if (Utils.trimNullToEmpty(res.listSelected[0]?.t24Employee?.employeeCode) === '') {
            this.confirmService.warn(this.notificationMessage[ASSIGN_ERROR_CODE010]);
            return;
          }
          let validateApis = [
            this.rmApi.checkRmAssign(res.listSelected[0].hrisEmployee?.employeeId).pipe(catchError((e) => of(false))),
            this.rmApi
              .checkRmNTSNotAssign([res.listSelected[0].t24Employee?.employeeCode])
              .pipe(catchError((e) => of(false))),
          ];

          if (IS_ASSIGN_INDIV) {
            validateApis.push(
              this.rmApi.checkAssignUB(res.listSelected[0].hrisEmployee?.employeeId).pipe(catchError((e) => of(false)))
            );
          }

          forkJoin(validateApis).subscribe(([checkRmAssign, listRm, checkAssignUB]) => {
            if (checkRmAssign || _.size(listRm) > 0 || (IS_ASSIGN_INDIV && !checkAssignUB)) {
              const messages = [];
              /* datnv2.os - CRMCN-2824 - Điều chỉnh độ ưu tiên của messages */
              // if (checkRmAssign) {
              //   messages.push(this.notificationMessage[ASSIGN_ERROR_CODE008]);
              // }
              // if (_.size(listRm) > 0) {
              //   messages.push(this.notificationMessage[ASSIGN_ERROR_CODE009]);
              // }
              // if (IS_ASSIGN_INDIV && !checkAssignUB) {
              //   messages.push(this.notificationMessage.ASSIGN_INDIV_011);
              // }

              if (IS_ASSIGN_INDIV && !checkAssignUB) {
                messages.push(this.notificationMessage.ASSIGN_INDIV_011);
              } else {
                if (checkRmAssign && !(_.size(listRm) > 0)) {
                  messages.push(this.notificationMessage[ASSIGN_ERROR_CODE008]);
                } else {
                  messages.push(this.notificationMessage[ASSIGN_ERROR_CODE009]);
                }
              }
              //CRMCN-2948-END
              this.confirmService.warn(messages);
              return;
            } else {
              this.form.controls.rm.setValue(
                res.listSelected[0].t24Employee?.employeeCode + ' - ' + res.listSelected[0].hrisEmployee?.fullName
              );
              this.form.controls.hrsCode.setValue(res.listSelected[0].hrisEmployee?.employeeId);
              this.form.controls.branchCode.setValue(res.listSelected[0].t24Employee?.branchCode);
              this.form.controls.code.setValue(res.listSelected[0].t24Employee?.employeeCode);
            }
          });
          this.customerAssignmentApi
            .checkRmIsKVHOrIsKHCN(res.listSelected[0].hrisEmployee?.employeeId)
            .subscribe((res) => {
              if (res) {
                this.isRmKVHOrRmKHCN = true;
              } else {
                this.isRmKVHOrRmKHCN = false;
              }
            });
        }
      })
      .catch(() => {});
  }

  selectCustomers() {
    const modal = this.modalService.open(CustomerModalComponent, { windowClass: 'list__customer360-modal' });
    modal.componentInstance.listSelectedOld = this.listTemp;
    modal.componentInstance.dataSearch = {
      customerType: { value: this.form.controls.divisionCode.value, disabled: true },
    };
    modal.result
      .then((res) => {
        if (res) {
          this.listTemp =
            res.listSelected?.map((item) => {
              return {
                customerCode: item.customerCode,
                customerName: item.customerName,
                phone: item.phoneDisplay,
                email: item.emailDisplay,
                branch: item.branch ? item.branch : item.branchCode + ' - ' + item.branchName,
                branchCode: item.branchCode,
                customerType: item.customerType,
                sector: item.sector,
                customerTypeSector: item.customerTypeSector,
                customerTypeMerge: item.customerTypeMerge,
              };
            }) || [];
          this.search(true);
        }
      })
      .catch(() => {});
  }

  setPage(pageInfo) {
    this.params.page = pageInfo.offset;
    this.search(false);
  }

  search(isSearch: boolean) {
    if (isSearch) {
      this.params.page = 0;
      this.textSearch = this.textSearch.trim();
      this.params.search = this.textSearch;
    }
    const value = this.params.search.toLowerCase();
    const listResult = this.listTemp.filter((item) => {
      return (
        Utils.trimNullToEmpty(item.customerCode).toLowerCase().indexOf(value) !== -1 ||
        Utils.trimNullToEmpty(item.customerName).toLowerCase().indexOf(value) !== -1 ||
        Utils.trimNullToEmpty(item.phone).toLowerCase().indexOf(value) !== -1 ||
        Utils.trimNullToEmpty(item.email).toLowerCase().indexOf(value) !== -1 ||
        Utils.trimNullToEmpty(item.branch).toLowerCase().indexOf(value) !== -1
      );
    });
    this.countCheckedOnPage = 0;
    this.listCustomers = listResult.slice(
      this.params.page * this.params.size,
      this.params.page * this.params.size + this.params.size
    );
    this.listCustomers.forEach((item) => {
      if (
        this.listSelected.findIndex((itemChoose) => {
          return itemChoose.customerCode === item.customerCode;
        }) > -1
      ) {
        this.countCheckedOnPage += 1;
      }
    });
    this.checkAll = this.listCustomers.length > 0 && this.countCheckedOnPage === this.listCustomers.length;
    this.pageable = {
      totalElements: listResult.length,
      totalPages: listResult.length / this.params.size,
      currentPage: this.params.page,
      size: this.params.size,
    };
  }

  onCheckboxAllFn(isChecked) {
    this.checkAll = isChecked;
    if (!isChecked) {
      this.countCheckedOnPage = 0;
    } else {
      this.countCheckedOnPage = this.listCustomers.length;
    }
    for (const item of this.listCustomers) {
      item.isChecked = isChecked;
      if (isChecked) {
        if (
          this.listSelected.findIndex((itemChoose) => {
            return itemChoose.customerCode === item.customerCode;
          }) === -1
        ) {
          this.listSelected.push(item);
        }
      } else {
        this.listSelected = this.listSelected.filter((itemChoose) => {
          return itemChoose.customerCode !== item.customerCode;
        });
      }
    }
  }

  import() {
    this.router.navigateByUrl(`${functionUri.customer_360_assignment}/import`);
  }

  onCheckboxFn(item, isChecked) {
    item.isChecked = isChecked;
    if (isChecked) {
      this.listSelected.push(item);
      this.countCheckedOnPage += 1;
    } else {
      this.listSelected = this.listSelected.filter((itemChoose) => {
        return itemChoose.customerCode !== item.customerCode;
      });
      this.countCheckedOnPage -= 1;
    }
    this.checkAll = this.countCheckedOnPage === this.listCustomers.length;
  }

  removeCustomer(item) {
    this.listTemp = this.listTemp.filter((itemOld) => {
      return itemOld.customerCode !== item.customerCode;
    });
    this.listSelected = this.listSelected.filter((itemChoose) => {
      return itemChoose.customerCode !== item.customerCode;
    });
    this.search(false);
  }

  removeCustomers() {
    const listRemove = this.listCustomers.filter((item) => {
      return item.isChecked;
    });
    if (listRemove.length === 0) {
      this.messageService.warn(this.notificationMessage.noRecordSelected);
      return;
    }
    listRemove.forEach((item) => {
      this.listTemp = this.listTemp.filter((itemOld) => {
        return itemOld.customerCode !== item.customerCode;
      });
      this.listCustomers = this.listCustomers.filter((itemOld) => {
        return itemOld.customerCode !== item.customerCode;
      });
    });
    this.search(false);
  }

  exportFile() {
    if (this.listTemp.length === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    const data = [];
    let obj: any = {};
    const value = this.params.search.toLowerCase();
    const listResult = this.listTemp.filter((item) => {
      return (
        Utils.trimNullToEmpty(item.customerCode).toLowerCase().indexOf(value) !== -1 ||
        Utils.trimNullToEmpty(item.customerName).toLowerCase().indexOf(value) !== -1 ||
        Utils.trimNullToEmpty(item.phone).toLowerCase().indexOf(value) !== -1 ||
        Utils.trimNullToEmpty(item.email).toLowerCase().indexOf(value) !== -1 ||
        Utils.trimNullToEmpty(item.branch).toLowerCase().indexOf(value) !== -1
      );
    });
    if (listResult.length === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    listResult.forEach((item, index) => {
      obj = {};
      obj[this.fields.order] = index + 1;
      obj[this.fields.customerCode] = item.customerCode;
      obj[this.fields.customerName] = item.customerName;
      obj[this.fields.phone] = item.phone;
      obj[this.fields.email] = item.email;
      obj[this.fields.branch] = item.branch;
      data.push(obj);
    });
    this.exportExcelService.exportAsExcelFile(data, 'customer_assignment');
  }
}
