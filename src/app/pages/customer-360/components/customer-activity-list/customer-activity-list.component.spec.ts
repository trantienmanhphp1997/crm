import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerActivityListComponent } from './customer-activity-list.component';

describe('CustomerActivityListComponent', () => {
  let component: CustomerActivityListComponent;
  let fixture: ComponentFixture<CustomerActivityListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerActivityListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerActivityListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
