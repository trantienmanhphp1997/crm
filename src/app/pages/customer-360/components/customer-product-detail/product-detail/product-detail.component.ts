import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, Injector, Input } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
})
export class ProductDetailComponent extends BaseComponent implements OnInit {
  @Input() listFields: string[];
  @Input() code: string;
  @Input() data: any;
  title: string;

  constructor(injector: Injector, private modalActive: NgbActiveModal) {
    super(injector);
  }

  ngOnInit(): void {
    this.title = (this.data?.productId || '') + (this.data?.productName ? ' - ' + this.data?.productName : '');
  }

  closeModal() {
    this.modalActive.close(false);
  }
}
