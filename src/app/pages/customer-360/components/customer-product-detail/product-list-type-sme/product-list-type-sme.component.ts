import { forkJoin, of } from 'rxjs';
import { DatePipe, formatDate, formatNumber } from '@angular/common';
import _ from 'lodash';

import {
  Component,
  OnInit,
  Injector,
  Input,
  AfterViewInit,
  AfterViewChecked,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ViewChildren,
  QueryList,
  ElementRef,
} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import {CommonCategory, ExportKey, TypeFieldProduct} from 'src/app/core/utils/common-constants';
import { CustomerApi } from '../../../apis';
import { catchError } from 'rxjs/operators';
import { CommonCategoryService } from 'src/app/core/services/common-category.service';
import { ProductDetailSmeComponent } from '../product-detail-sme/product-detail-sme.component';
import {Utils} from '../../../../../core/utils/utils';
import {FileService} from '../../../../../core/services/file.service';

@Component({
  selector: 'app-product-list-type-sme',
  templateUrl: './product-list-type-sme.component.html',
  styleUrls: ['./product-list-type-sme.component.scss'],
  providers: [DatePipe],
})
export class ProductListTypeSMEComponent
  extends BaseComponent
  implements OnInit, OnChanges, AfterViewInit, AfterViewChecked
{
  @Input() type: string;
  @Input() data: any[];
  @Input() codeType: string[];
  @Input() resizeWindow: number;
  @Input() customerCode: string;
  @Input() title: string;
  @Input() rsId: string;
  @Output() loading: EventEmitter<any> = new EventEmitter<any>();
  // title: string;
  pageable: Pageable;
  listFields = [];
  rows = [];
  positionLeft = 300;
  creditProductGroup: any;
  isOpen = true;
  @ViewChildren('groupCode') groupCode: QueryList<any>;

  constructor(
    injector: Injector,
    private customerApi: CustomerApi,
    private fileService: FileService,
    private commonCategoryService: CommonCategoryService
  ) {
    super(injector);
    this.subscriptions.push(
      this.communicateService.request$.subscribe((res) => {
        if (res?.name === 'ExpandGroup' && res?.type === this.type) {
          this.isOpen = true;
          this.groupCode.forEach((el: ElementRef, i) => {
            if (this.table?.groupedRows[i] && !this.table.groupedRows[i].expanded) {
              (el.nativeElement as HTMLElement)?.click();
            }
          });
        }
      })
    );
  }

  ngOnInit(): void {
    this.getDataNew();
  }

  ngAfterViewInit() {
    this.table.groupExpansionDefault = true;
    this.table?.groupedRows?.forEach((item) => {
      item.expanded = true;
    });
  }

  ngAfterViewChecked() {
    if (
      this.table &&
      this.table.recalculate &&
      document
        .getElementById(`list-${this.type?.toLowerCase()}`)
        ?.querySelector('.datatable-row-center.datatable-row-group')
        ?.getElementsByClassName('datatable-body-cell')
    ) {
      const listClass = document
        .getElementById(`list-${this.type?.toLowerCase()}`)
        .getElementsByClassName('total-money');
      for (let index = 0; index < listClass?.length; index++) {
        const element = listClass?.item(index);
        element.setAttribute(
          'style',
          `left: ${
            (this.table?._internalColumns[0]?.width || 0) +
            (this.table?._internalColumns[1]?.width || 0) +
            (this.table?._internalColumns[2]?.width || 0) +
            (this.table?._internalColumns[3]?.width || 0)
          }px; width: ${this.table?._internalColumns[4]?.width}px`
        );
      }
    }
  }

  ngOnChanges(change: SimpleChanges) {}

  getDataNew() {
    this.listFields = [
      'order',
      'contractNumber',
      'currencyType',
      'currencyBalance',
      'redemptionBalance',
      'openDate',
      'dateDue',
      'dataDate',
    ];
    const countType: any = {};
    new Set(this.data.map((product) => product.type))?.forEach((code) => {
      countType[code] = 0;
    });
    this.data?.forEach((product) => {
      countType[product.type] += 1;
      product.order = countType[product.type];
      product.productCodeAndName = _.isEmpty(product.productCode)
        ? product.productName
        : product.productCode + ' - ' + product.productName;
      product.contractOrAccountNumber = product.contractNumber;
      product.currencyType = product?.currency;
      product.currencyBalance = product?.balance
        ? formatNumber(product?.balance || 0, 'en', '0.0-2')
        : product?.balance;
      product.redemptionBalance = product?.balanceQD
        ? formatNumber(product?.balanceQD || 0, 'en', '0.0-2')
        : product?.balanceQD;
      product.openDate = product.openDate ? formatDate(product.openDate, 'dd/MM/yyyy', 'en') : product.openDate;
      product.dateDue = product.expiredDate ? formatDate(product.expiredDate, 'dd/MM/yyyy', 'en') : '';
      product.dataDate = product.bussinessDate ? formatDate(product.bussinessDate, 'dd/MM/yyyy', 'en') : '';
    });
    this.rows = [...this.data];
  }

  getTotalByType(value) {
    if (
      this.rows?.filter((item) => {
        return item.productCodeAndName === value;
      }).length > 0
    ) {
      const total = this.rows
        ?.filter((item) => {
          return item.productCodeAndName === value;
        })
        .map((i) => i.balanceQD)
        .reduce((a, c) => {
          return a + c;
        });
      if (total === undefined) {
        return;
      }
      return formatNumber(total || 0, 'en', '0.0-2');
    }
    return 0;
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      this.loading.emit(true);
      this.customerApi
        .getProductDetailSme(this.customerCode, event.row?.contractNumber, event.row?.productTypeCode)
        .subscribe(
          (res) => {
            if (res) {
              this.loading.emit(false);
              const modal = this.modalService.open(ProductDetailSmeComponent, { windowClass: 'cus-product-modal' });
              modal.componentInstance.dataProduct = res;
            } else {
              this.loading.emit(false);
            }
          },
          (err) => {
            this.loading.emit(false);
          }
        );
    }
  }

  getLoanTermDesc(value: number) {
    if (value <= 12) {
      return this.fields.shortTerm;
    } else if (value > 12 && value <= 60) {
      return this.fields.mediumTerm;
    } else if (value > 60) {
      return this.fields.longTerm;
    } else {
      return '';
    }
  }

  getCreditProductGroupName(value) {
    return this.creditProductGroup[value] || this.creditProductGroup?.other;
  }

  toggleExpandGroup(group) {
    this.table.groupExpansionDefault = false;
    this.table.groupHeader.toggleExpandGroup(group);
    group.expanded = !group.expanded;
  }

  generateClassNameHeader(field: String) {
    if (
      field === 'redemptionBalance' ||
      field === 'currencyBalance' ||
      field === 'dateDue' ||
      field === 'openDate' ||
      field === 'dataDate' ||
      field === 'sales' ||
      field === 'dateOpen' ||
      field === 'dueDate' ||
      field === 'expiredDate'
    ) {
      return 'text-center';
    } else if (field === 'contractOrAccountNumber') {
      return '';
    }
    return field;
  }
  generateClassNameCell(field: String) {
    if (field === 'redemptionBalance' || field === 'currencyBalance' || field === 'sales') {
      return 'text-right';
    } else if (
      field === 'dueDate' ||
      field === 'dateDue' ||
      field === 'openDate' ||
      field === 'dataDate' ||
      field === 'dateOpen' ||
      field === 'expiredDate'
    ) {
      return 'text-center';
    }
    return field;
  }

  export(type) {
    this.isOpen = !this.isOpen;
    const data = {
      customerCode: this.customerCode,
      rsId: this.rsId,
      scope: 'VIEW',
      blockCode: 'SME'
    }
    const api = this.customerApi.exportProductListTypeSme(type, data);

    api.subscribe(
      (res) => {
        if (Utils.isStringNotEmpty(res)) {
          this.download(res);
        } else {
          this.messageService.error(this.notificationMessage?.export_error || '');
          this.loading.emit(false);
        }
      },
      () => {
        this.messageService.error(this.notificationMessage?.export_error || '');
        this.loading.emit(false);
      }
    );
  }

  download(fileId: string) {
    this.isDownLoading = true;
    this.sessionService.setSessionData(ExportKey.Customer, true);
    this.fileService
      .downloadFile(fileId, `${this.title}.xlsx`, ExportKey.Customer)
      .pipe(catchError((e) => of(false)))
      .subscribe((res) => {
        this.loading.emit(false);
        if (!res) {
          this.messageService.error(this.notificationMessage.error);
        }
        this.isDownLoading = false;
        this.sessionService.setSessionData(ExportKey.Customer, false);
      });
  }

  highlightPDLDExpired(row) {
    return {
      'text-danger': row.contractNumber.includes('PDLD')
    };
  }
}
