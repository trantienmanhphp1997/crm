import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductListTypeComponent } from './product-list-type.component';

describe('ProductListTypeComponent', () => {
  let component: ProductListTypeComponent;
  let fixture: ComponentFixture<ProductListTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductListTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductListTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
