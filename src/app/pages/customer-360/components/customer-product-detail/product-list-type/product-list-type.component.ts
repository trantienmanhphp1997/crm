import { forkJoin, of } from 'rxjs';
import { DatePipe, formatDate, formatNumber } from '@angular/common';
import _ from 'lodash';

import {
  Component,
  OnInit,
  Injector,
  Input,
  AfterViewInit,
  AfterViewChecked,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ViewChildren,
  QueryList,
  ElementRef,
} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { CommonCategory, CreditProductGroup, ServiceProductType } from 'src/app/core/utils/common-constants';
import { ProductDetailComponent } from '../product-detail/product-detail.component';
import { CustomerApi } from '../../../apis';
import { catchError } from 'rxjs/operators';
import { CommonCategoryService } from 'src/app/core/services/common-category.service';

@Component({
  selector: 'app-product-list-type',
  templateUrl: './product-list-type.component.html',
  styleUrls: ['./product-list-type.component.scss'],
  providers: [DatePipe],
})
export class ProductListTypeComponent
  extends BaseComponent
  implements OnInit, OnChanges, AfterViewInit, AfterViewChecked
{
  @Input() type: string;
  @Input() data: any[];
  @Input() codeType: string[];
  @Input() resizeWindow: number;
  @Input() customerCode: string;
  @Output() loading: EventEmitter<any> = new EventEmitter<any>();
  title: string;
  pageable: Pageable;
  listFields = [];
  rows = [];
  positionLeft = 300;
  creditProductGroup: any;
  isOpen = true;
  @ViewChildren('groupCode') groupCode: QueryList<any>;

  constructor(
    injector: Injector,
    private customerApi: CustomerApi,
    private commonCategoryService: CommonCategoryService
  ) {
    super(injector);
    this.subscriptions.push(
      this.communicateService.request$.subscribe((res) => {
        if (res?.name === 'ExpandGroup' && res?.type === this.type) {
          this.isOpen = true;
          this.groupCode.forEach((el: ElementRef, i) => {
            if (this.table?.groupedRows[i] && !this.table.groupedRows[i].expanded) {
              (el.nativeElement as HTMLElement)?.click();
            }
          });
        }
      })
    );
  }

  ngOnInit(): void {
    this.getData();
    this.translate.get('serviceProduct').subscribe((product) => {
      if (this.type) {
        this.title = product[this.type?.toLowerCase()];
      }
    });
    this.translate.get('creditProductGroup').subscribe((creditProductGroup) => {
      this.creditProductGroup = {};
      Object.keys(CreditProductGroup).forEach((key) => {
        this.creditProductGroup[CreditProductGroup[key]] = creditProductGroup[key.toLowerCase()];
      });
    });
  }

  ngAfterViewInit() {
    this.table.groupExpansionDefault = true;
    this.table?.groupedRows?.forEach((item) => {
      item.expanded = true;
    });
  }

  ngAfterViewChecked() {
    if (
      this.table &&
      this.table.recalculate &&
      document
        .getElementById(`list-${this.type?.toLowerCase()}`)
        ?.querySelector('.datatable-row-center.datatable-row-group')
        ?.getElementsByClassName('datatable-body-cell')
    ) {
      if (this.type === ServiceProductType.Mobilization || this.type === ServiceProductType.Credit) {
        const countType = new Set(this.rows?.map((i) => i.productCode));
        countType?.forEach((code) => {
          (
            document
              .getElementById(`list-${this.type?.toLowerCase()}`)
              .querySelector(`.${this.type?.toLowerCase()}_${code}`) as HTMLElement
          ).style.left = `${
            (this.table?._internalColumns[0]?.width || 0) +
            (this.table?._internalColumns[1]?.width || 0) +
            (this.table?._internalColumns[2]?.width || 0)
          }px`;

          (
            document
              .getElementById(`list-${this.type?.toLowerCase()}`)
              .querySelector(`.${this.type?.toLowerCase()}_${code}`) as HTMLElement
          ).style.width = `${this.table?._internalColumns[3]?.width}px`;
        });
      } else if (this.type === ServiceProductType.Group) {
        const listClass = document
          .getElementById(`list-${this.type?.toLowerCase()}`)
          .getElementsByClassName('total-money');
        for (let index = 0; index < listClass.length; index++) {
          const element = listClass.item(index);
          element.setAttribute(
            'style',
            `left: ${
              (this.table?._internalColumns[0]?.width || 0) + (this.table?._internalColumns[1]?.width || 0)
            }px; width: ${this.table?._internalColumns[2]?.width}px`
          );
        }
      }
    }
  }

  ngOnChanges(change: SimpleChanges) {}

  getData() {
    if (this.type === ServiceProductType.Mobilization || this.type === ServiceProductType.Credit) {
      this.listFields = [
        'order',
        'contractOrAccountNumber',
        'currencyBalance',
        'redemptionBalance',
        'openDate',
        'dateDue',
        'agency',
        'dataDate',
      ];
      const countType: any = {};
      new Set(this.data.map((product) => product.productCode))?.forEach((code) => {
        countType[code] = 0;
      });
      this.data?.forEach((product) => {
        countType[product.productCode] += 1;
        product.order = countType[product.productCode];
        product.productCodeAndName = product.productCode + ' - ' + product.productName;
        product.contractOrAccountNumber = product.contractNumber;
        product.currencyBalance =
          product?.balance && !product.formatted
            ? formatNumber(product?.balance || 0, 'en', '0.0-2') + ' ' + product.currency
            : product?.balance;
        product.redemptionBalance =
          product?.balanceQD && !product.formatted
            ? formatNumber(product?.balanceQD || 0, 'en', '0.0-2')
            : product?.balanceQD;
        product.openDate =
          product.openDate && !product.formatted ? formatDate(product.openDate, 'dd/MM/yyyy', 'en') : product.openDate;
        product.dateDue = product.expiredDate ? formatDate(product.expiredDate, 'dd/MM/yyyy', 'en') : '';
        product.dataDate = product.bussinessDate ? formatDate(product.bussinessDate, 'dd/MM/yyyy', 'en') : '';
        product.agency = product.branchCode + ' - ' + product.branchValue;
        product.formatted = true;
      });
      this.rows = [...this.data];
    } else if (this.type === ServiceProductType.Digital) {
      this.listFields = ['order', 'contractOrAccountNumber', 'openDate', 'agency', 'dataDate'];
      const countType: any = {};
      new Set(this.data.map((product) => product.type))?.forEach((code) => {
        countType[code] = 0;
      });
      this.data?.forEach((product) => {
        countType[product.type] += 1;
        product.order = countType[product.type];
        product.productCodeAndName = product.typeName;
        product.contractOrAccountNumber = product.contractNumberView;
        product.openDate =
          product.openDate && !product.formatted ? formatDate(product.openDate, 'dd/MM/yyyy', 'en') : product.openDate;
        product.agency = product.branchCode + ' - ' + product.branchValue;
        product.dataDate = product.bussinessDate ? formatDate(product.bussinessDate, 'dd/MM/yyyy', 'en') : '';
        product.formatted = true;
      });
      this.rows = [...this.data];
    } else if (this.type === ServiceProductType.Card) {
      //San pham the
      this.listFields = ['order', 'contractOrAccountNumber', 'openDate', 'dueDate', '_branch', 'dataDate'];
      const countType: any = {};
      new Set(this.data.map((product) => product.type))?.forEach((code) => {
        countType[code] = 0;
      });
      this.data?.forEach((product) => {
        countType[product.type] += 1;
        product.order = countType[product.type];
        if (product.type === '122') {
          product.productCodeAndName = 'Thẻ credit';
        } else if (product.type === '121') {
          product.productCodeAndName = 'Thẻ debit';
        }

        product.contractOrAccountNumber = product.contractNumber;
        product.openDate =
          product.openDate && !product.formatted ? formatDate(product.openDate, 'dd/MM/yyyy', 'en') : product.openDate;
        product.dueDate =
          product.expiredDate && !product.formatted
            ? formatDate(product.expiredDate, 'dd/MM/yyyy', 'en')
            : product.expiredDate;
        product._branch = product.branchCode + ' - ' + product.branchValue;
        product.dataDate = product.bussinessDate ? formatDate(product.bussinessDate, 'dd/MM/yyyy', 'en') : '';
        product.formatted = true;
      });
      this.rows = [...this.data];
    } else if (this.type === ServiceProductType.Group) {
      this.listFields = ['order', 'contractNumber', 'sales', 'dateOpen', 'expiredDate', 'agency', 'dataDate'];
      const countType: any = {};
      const countTypeMa: any = {};
      let countData = 0;
      new Set(this.data.map((product) => product.productCode))?.forEach((code) => {
        if (code !== undefined) {
          countType[code] = 0;
        }
      });
      new Set(this.data.map((product) => product.productName))?.forEach((code) => {
        countTypeMa[code] = 0;
      });

      this.data?.forEach((product) => {
        if (product.type === '172') {
          countType[product.productCode] += 1;
          product.order = countType[product.productCode];
        } else {
          countTypeMa[product.productName] += 1;
          product.order = countTypeMa[product.productName];
        }

        product.productCodeAndName = (product?.productCode ? product?.productCode + ' - ' : '') + product.productName;
        product.sales =
          product?.balance && !product.formatted
            ? formatNumber(product?.balance || 0, 'en', '0.0-2')
            : product?.balance;
        product.dateOpen =
          product.openDate && !product.formatted ? formatDate(product.openDate, 'dd/MM/yyyy', 'en') : product?.openDate;
        product.expiredDate =
          product.expiredDate && !product.formatted
            ? formatDate(product.expiredDate, 'dd/MM/yyyy', 'en')
            : product?.expiredDate;
        product.agency = product.branchCode + ' - ' + product.branchValue;
        product.dataDate = product.bussinessDate ? formatDate(product.bussinessDate, 'dd/MM/yyyy', 'en') : '';
        product.formatted = true;
      });
      this.rows = [...this.data];
    }
  }

  getTotalByType(value) {
    if (
      this.rows?.filter((item) => {
        return item.productCodeAndName === value;
      }).length > 0
    ) {
      const total = this.rows
        ?.filter((item) => {
          return item.productCodeAndName === value;
        })
        .map((i) => i.balanceQD)
        .reduce((a, c) => {
          return a + c;
        });
      return formatNumber(total || 0, 'en', '0.0-2');
    }
    return 0;
  }

  getTotalByTypeGroup(value) {
    if (
      this.rows?.filter((item) => {
        return item.productCodeAndName === value;
      }).length > 0
    ) {
      const total = this.rows
        ?.filter((item) => {
          return item.productCodeAndName === value;
        })
        .map((i) => i.balance)
        .reduce((a, c) => {
          return a + c;
        });
      return formatNumber(total || 0, 'en', '0.0-2');
    }
    return 0;
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      this.loading.emit(true);
      const params = {
        type: event?.row?.type,
        contractNumber: event.row?.contractNumber,
        customerCode: event.row?.clientId,
      };
      forkJoin([
        this.customerApi.getProductDetail(params),
        this.customerApi
          .getRmManagerByCustomerCode(this.customerCode, event?.row?.branchCode)
          .pipe(catchError((e) => of(undefined))),
        this.commonCategoryService.getCommonCategory(CommonCategory.SPDV_THE).pipe(catchError((e) => of(undefined))),
      ]).subscribe(
        ([product, rmManager, typeDetail]) => {
          const data: any = {};
          const listFields = [];
          if (event?.row?.type === '100') {
            data.customerCode = product?.customerId;
            data.customerName = product?.ipNm;
            data.accountNumber = product?.accountNo;
            data.status = 'Active';
            data.currencyBalance = product?.balFcy ? formatNumber(product?.balFcy || 0, 'en', '0.0-2') : undefined;
            data.currencyType = product?.currency;
            data.redemptionBalance = product?.balLcy ? formatNumber(Math.round(product?.balLcy) || 0, 'en') : undefined;
            data.openDatebalance = product?.balFcyPrev
              ? formatNumber(product?.balFcyPrev || 0, 'en', '0.0-2')
              : undefined;
            data.interest = product?.interest ? formatNumber(product?.interest || 0, 'en', '0.0-2') : undefined;
            data.openDate = product?.openDate;
            data.rmClosesContract = product?.rmCode;
            data.releasedBranch = product?.branchCode + ' - ' + product?.branchName;
          } else if (event?.row?.type === '101') {
            data.customerCode = product?.customerId;
            data.customerName = product?.cstNm;
            data.passbookNumber = product?.accountNo;
            data.status = 'Active';
            data.currencyBalance = product?.prinBalFcy
              ? formatNumber(product?.prinBalFcy || 0, 'en', '0.0-2')
              : undefined;
            data.currencyType = product?.currency;
            data.redemptionBalance = product?.prinBalLcy
              ? formatNumber(Math.round(product?.prinBalLcy) || 0, 'en')
              : undefined;
            data.openDatebalance = product?.balFcyPrev
              ? formatNumber(product?.balFcyPrev || 0, 'en', '0.0-2')
              : undefined;
            data.term = product?.arTermTp;
            data.interest = product?.interest ? formatNumber(product?.interest || 0, 'en', '0.0-2') : undefined;
            data.interestAccrued = product?.intAmtFcy
              ? formatNumber(product?.intAmtFcy || 0, 'en', '0.0-2')
              : undefined;
            data.openDate = product?.openDate;
            data.dateDue = product?.matDt;
            data.rmClosesContract = product?.rmCode;
            data.releasedBranch = (product?.ouCode || '') + (product?.branchName ? ' - ' + product?.branchName : '');
          } else if (event?.row?.type === '103') {
            data.customerCode = product?.customerId;
            data.customerName = product?.customerFullName;
            data.ldNumber = product?.accountNo;
            data.loanProduct = (product?.productId || '') + (product?.productName ? ' - ' + product?.productName : '');
            data.productGroup = this.getCreditProductGroupName(product?.prnGrp);
            data.status = 'Active';
            data.balance = product?.balFcy ? formatNumber(product?.balFcy || 0, 'en') : undefined;
            data.currencyType = product?.currency;
            data.term = product?.arTermTp;
            data.interest = product?.interest ? formatNumber(product?.interest || 0, 'en', '0.0-2') : undefined;
            data.loanDate = product?.openDate;
            data.dateDue = product?.endDt ? product?.endDt : product?.maturityDate;
            data.renewalDate = product?.rstcExnDt;
            data.originalBalance = product?.frstInstlAmt
              ? formatNumber(product?.frstInstlAmt || 0, 'en', '0.0-2')
              : undefined;
            data.openDatebalance = product?.balFcyPrev
              ? formatNumber(product?.balFcyPrev || 0, 'en', '0.0-2')
              : undefined;
            data.interestIncome = product?.intAccrualAmt
              ? formatNumber(product?.intAccrualAmt || 0, 'en', '0.0-2')
              : undefined;
            data.loanTerm = this.getLoanTermDesc(product?.arTermTp);
            data.groupDebt = product?.creditStatusCd;
            data.isCollateral = product?.secured === 'Y' ? this.fields.yes : this.fields.no;
            data.useOfLoan = product?.srcOfFnd;
            data.loanPurpose = product?.useOfLoan;
            data.channelOpenContract = product?.cnlOpen;
            data.rmClosesContract = product?.mainRmCode;
            data.releasedBranch =
              (product?.branchCode || '') + (product?.branchName ? ' - ' + product?.branchName : '');
          } else if (event?.row?.type === '141') {
            data.customerCode = product?.customerId;
            data.customerName = product?.customerName;
            data.product = 'EMB';
            data.codeUser = product?.userIdView;
            data.triggerchannel = product?.actvChannel;
            data.registrationDate = product?.effDt ? formatDate(product?.effDt, 'dd/MM/yyyy', 'en') : '';
            data.statusUser = product?.status;
            data.registeredBranch = (product?.ouId || '') + (product?.branchName ? ' - ' + product?.branchName : '');
            data.rmClosesContract = product?.mainRmCd;
          } else if (event?.row?.type === '142') {
            data.customerCode = product?.customerId;
            data.customerName = product?.customerName;
            data.product = 'SMS';
            data.registeredaccount = product?.acctId;
            data.registeredPhoneNumber = product?.mobile;
            data.registrationDate = product?.effDt ? formatDate(product?.effDt, 'dd/MM/yyyy', 'en') : '';
            data.source = product?.srcStm;
            data.registeredBranch = (product?.ouId || '') + (product?.branchName ? ' - ' + product?.branchName : '');
            data.rmClosesContract = product?.mainRmCd;
          } else if (event?.row?.type === '143') {
            data.customerCode = product?.customerId;
            data.customerName = product?.customerName;
            data.product = 'APP';
            data.codeUser = product?.userIdView;
            data.dateApp = product?.actvDt ? formatDate(product?.actvDt, 'dd/MM/yyyy', 'en') : '';
            data.triggerchannel = product?.actvChannel;
            data.releaseBranch = (product?.ouId || '') + (product?.branchName ? ' - ' + product?.branchName : '');
          }
          // sp thẻ
          //Debit
          else if (event?.row?.type === '121') {
            data.customerCode = product?.customerId;
            data.customerName = product?.customerName;
            data.productCode = product?.productCode;
            data.nameProduct = product?.productName.trim();

            data.cardNumber = product?.cardNbr;
            data.typeCard = product?.cardType;
            data.statusCard = product?.cardStatus;
            data.accountName = product?.cstNm;
            data.paymentAccount = product?.rbsNbr;
            data.surplus = product?.balLcy ? formatNumber(product?.balLcy || 0, 'en', '0.0-2') : undefined;
            data.currency = product?.currency;
            data.releasedBranch = (product?.ouCode || '') + (product?.branchName ? ' - ' + product?.branchName : '');
            data.rmClosesContract = product?.rmCode;
          } else if (event?.row?.type === '122') {
            data.customerCode = product?.customerId;
            data.customerName = product?.customerName;
            data.productCode = product?.pdCode;
            data.nameProduct = product?.pdName;
            data.cardNumber = product?.cardNumber;
            data.typeCard = product?.cardType;
            data.cardIssueDate = product?.dateAccountOpened
              ? formatDate(product?.dateAccountOpened, 'dd/MM/yyyy', 'en')
              : '';
            data.statusCard = product?.cardStatus;
            data.contractCode = product?.accountId;
            data.idCard = product?.cardId;
            data.contractLimit = product?.contractLimitAmt
              ? formatNumber(Math.round(product?.contractLimitAmt) || 0, 'en')
              : undefined;
            data.cardIssuanceLimit = product?.cardLimitAmt
              ? formatNumber(Math.round(product?.cardLimitAmt) || 0, 'en')
              : undefined;
            data.paymentAmount = product?.minimumPayment
              ? formatNumber(Math.round(product?.minimumPayment) || 0, 'en')
              : undefined;
            data.paymentPeriod = product?.customerPaymentAmount
              ? formatNumber(Math.round(product?.customerPaymentAmount) || 0, 'en')
              : undefined;

            data.overdueBalance = product?.arrearsAmount
              ? formatNumber(Math.round(product?.arrearsAmount) || 0, 'en')
              : undefined;
            data.originalOverdueBalance = product?.arrearsPrincipal
              ? formatNumber(Math.round(product?.arrearsPrincipal) || 0, 'en')
              : undefined;
            data.interestOverdueBalance = product?.arrearsInterest
              ? formatNumber(Math.round(product?.arrearsInterest) || 0, 'en')
              : undefined;
            data.releasedBranch = (product?.branchId || '') + (product?.branchName ? ' - ' + product?.branchName : '');
            data.rmClosesContract = product?.rmCode;
          }
          //San pham tap doan
          //BH
          else if (event?.row?.type === '171') {
            data.customerCode = product?.customerId;
            data.customerName = product?.customerName;
            data.typeInsurancePolicy = product?.alMainProductName;
            data.insuranceProducts = product?.alRiderProductName;
            data.contractNumber = product?.alPolicyNumber;
            data.nameBmbh = product?.alPolicyHolderName;
            data.cmtPassport = product?.alPolicyHolderId;
            data.periodicPayment = product?.alPremiumPaymentFrequency;
            data.paymentDeadline = product?.alPremiumTerm + ' tháng';
            data.insuranceFees = product?.alApeIc ? formatNumber(product?.alApeIc || 0, 'en', '0.0-2') : undefined;
            data.releaseDate = product?.alIssuedDate ? formatDate(product?.alIssuedDate, 'dd/MM/yyyy', 'en') : '';
            data.statusContract = product?.alPolicyStatus;
            data.saleChannel = product?.alSaleChannel;
            data.agency = (product?.mbSubBranchCode || '') + (product?.branchName ? ' - ' + product?.branchName : '');
            data.rmClosesContract = product?.mbReferalId;
            data.employeeCodeContract = product?.mbAgencyCode;
          } else if (event?.row?.type === '172') {
            data.customerCode = product?.customerId;
            data.customerName = product?.customerName;
            data.productCode = product?.productCode;
            data.productName = product?.prnPdNm;
            data.contractNumber = product?.accountId;
            data.insuranceSales = product?.turnoverAmt
              ? formatNumber(product?.turnoverAmt || 0, 'en', '0.0-2')
              : undefined;
            data.openDate = product?.effDt ? formatDate(product?.effDt, 'dd/MM/yyyy', 'en') : '';
            data.expiredDate = product?.matDt ? formatDate(product?.matDt, 'dd/MM/yyyy', 'en') : '';
            data.agency = (product?.branchCode || '') + (product?.branchName ? ' - ' + product?.branchName : '');
            data.rmClosesContract = product?.rmCode;
          }
          data.rmManager = rmManager;
          Object.keys(data).forEach((key) => {
            listFields.push(key);
          });
          switch (event?.row?.type) {
            case '172':
              data.productId = product?.productCode;
              break;
            case '171':
              data.productId = product?.alRiderProductName;
              break;
            case '121':
              data.productId = 'Thẻ debit';
              break;
            case '122':
              data.productId = 'Thẻ credit';
              break;
            case '141':
              data.productId = 'EMB';
              break;
            case '142':
              data.productId = 'SMS';
              break;
            case '143':
              data.productId = 'APP';
              break;
            default:
              data.productId = product?.productId;
              data.productName = product?.productName;
              break;
          }

          this.loading.emit(false);
          const modal = this.modalService.open(ProductDetailComponent, { windowClass: 'cus-product-modal' });
          modal.componentInstance.data = data;
          modal.componentInstance.type = this.type;
          modal.componentInstance.listFields = listFields;
        },
        () => {
          this.loading.emit(false);
        }
      );
    }
  }

  getLoanTermDesc(value: number) {
    if (value <= 12) {
      return this.fields.shortTerm;
    } else if (value > 12 && value <= 60) {
      return this.fields.mediumTerm;
    } else if (value > 60) {
      return this.fields.longTerm;
    } else {
      return '';
    }
  }

  getCreditProductGroupName(value) {
    return this.creditProductGroup[value] || this.creditProductGroup?.other;
  }

  toggleExpandGroup(group) {
    this.table.groupExpansionDefault = false;
    this.table.groupHeader.toggleExpandGroup(group);
    group.expanded = !group.expanded;
  }

  generateClassNameHeader(field: String) {
    if (
      field === 'redemptionBalance' ||
      field === 'currencyBalance' ||
      field === 'dateDue' ||
      field === 'openDate' ||
      field === 'dataDate' ||
      field === 'sales' ||
      field === 'dateOpen' ||
      field === 'dueDate' ||
      field === 'expiredDate'
    ) {
      return 'text-center';
    } else if (field === 'contractOrAccountNumber') {
      return '';
    }
    return field;
  }
  generateClassNameCell(field: String) {
    if (field === 'redemptionBalance' || field === 'currencyBalance' || field === 'sales') {
      return 'text-right';
    } else if (
      field === 'dueDate' ||
      field === 'dateDue' ||
      field === 'openDate' ||
      field === 'dataDate' ||
      field === 'dateOpen' ||
      field === 'expiredDate'
    ) {
      return 'text-center';
    }
    return field;
  }
}
