import { Component, OnInit, Injector, Input, EventEmitter } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import {
  CardChart,
  ComprodChart,
  CreditProductGroup,
  CustomerType,
  DigitalChart,
  EChartType,
  FunctionCode,
  MobilizationChart,
  ServiceProductType,
} from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { formatNumber } from '@angular/common';

@Component({
  selector: 'app-product-chart',
  templateUrl: './product-chart.component.html',
  styleUrls: ['./product-chart.component.scss'],
})
export class ProductChartComponent extends BaseComponent implements OnInit {
  listBranchCode = [];
  @Input() viewChange: EventEmitter<boolean> = new EventEmitter();
  @Input() type: string;
  @Input() data: any[];
  params = {
    customerTypes: [CustomerType.INDIV, CustomerType.NHS],
    branchs: [],
  };
  option: any;
  xAxisData: string[];
  dataBranchAll: any[];
  dataBranch: any[];
  colorBranchAll: string;
  colorBranch: string;
  title: string;
  branch: string;

  constructor(injector: Injector) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
  }

  ngOnInit(): void {
    this.translate.get('system.branchType.branch').subscribe((res) => {
      this.branch = res;
    });
    this.params.branchs = this.listBranchCode;
    this.xAxisData = [];
    this.dataBranchAll = [];
    this.dataBranch = [];
    if (this.type === ServiceProductType.Mobilization) {
      this.colorBranchAll = '#FFE5CE';
      this.colorBranch = '#FFA95A';
      this.translate.get('mobilizationChart').subscribe((mobilizationChart) => {
        this.xAxisData = [];
        this.dataBranchAll = [];
        this.dataBranch = [];
        Object.keys(MobilizationChart).forEach((key) => {
          this.xAxisData.push(mobilizationChart[key.toLowerCase()]);
          this.dataBranchAll.push(
            this.data?.find((item) => {
              return item.productType === MobilizationChart[key] && item.type === 'TOAN_HANG';
            })?.total || null
          );
          this.dataBranch.push(
            this.data?.find((item) => {
              return item.productType === MobilizationChart[key] && item.type === 'CHI_NHANH';
            })?.total || null
          );
        });
      });
    } else if (this.type === ServiceProductType.Credit) {
      this.colorBranchAll = '#FFD9DC';
      this.colorBranch = '#FF808B';
      this.translate.get('creditProductGroup').subscribe((creditProductGroup) => {
        this.xAxisData = [];
        this.dataBranchAll = [];
        this.dataBranch = [];
        this.data?.forEach((item) => { });
        Object.keys(CreditProductGroup).forEach((key) => {
          const item = this.data?.filter((item) => {
            return item.productType === CreditProductGroup[key];
          });
          if (item.length > 0) {
            this.xAxisData.push(creditProductGroup[key.toLowerCase()]);
            this.dataBranchAll.push(
              this.data?.find((item) => {
                return item.productType === CreditProductGroup[key] && item.type === 'TOAN_HANG';
              })?.total || null
            );
            this.dataBranch.push(
              this.data?.find((item) => {
                return item.productType === CreditProductGroup[key] && item.type === 'CHI_NHANH';
              })?.total || null
            );
          }
        });
      });
    } else if (this.type === ServiceProductType.Digital) {
      this.colorBranchAll = '#FFE5CE';
      this.colorBranch = '#FFA95A';
      this.translate.get('digitalChart').subscribe((digitalChart) => {
        this.xAxisData = [];
        this.dataBranch = [];
        this.dataBranchAll = [];
        Object.keys(DigitalChart).forEach((key) => {
          const item = this.data?.filter((item) => {
            return item.productType === DigitalChart[key];
          });
          if (item.length > 0) {
            this.xAxisData.push(digitalChart[key.toLowerCase()]);
            this.dataBranchAll.push(
              this.data?.find((item) => {
                return item.productType === DigitalChart[key] && item.type === 'TOAN_HANG';
              })?.count || null
            );
            this.dataBranch.push(
              this.data?.find((item) => {
                return item.productType === DigitalChart[key] && item.type === 'CHI_NHANH';
              })?.count || null
            );
          }
        });
      });
    } else if (this.type === ServiceProductType.Group) {
      this.colorBranchAll = '#FFD9DC';
      this.colorBranch = '#FF808B';
      this.translate.get('comprodChart').subscribe((comprodChart) => {
        this.xAxisData = [];
        this.dataBranch = [];
        this.dataBranchAll = [];
        Object.keys(ComprodChart).forEach((key) => {
          const item = this.data?.filter((item) => {
            return item.productType === ComprodChart[key];
          });
          if (item.length > 0) {
            this.xAxisData.push(comprodChart[key.toLowerCase()]);
            this.dataBranchAll.push(
              this.data?.find((item) => {
                return item.productType === ComprodChart[key] && item.type === 'TOAN_HANG';
              })?.total || null
            );
            this.dataBranch.push(
              this.data?.find((item) => {
                return item.productType === ComprodChart[key] && item.type === 'CHI_NHANH';
              })?.total || null
            );
          }
        });
      });
    } else if (this.type === ServiceProductType.Card) {
      this.colorBranchAll = '#CDF1DD';
      this.colorBranch = '#58CF8D';
      this.translate.get('creditCard').subscribe((creditCard) => {
        this.xAxisData = [];
        this.dataBranch = [];
        this.dataBranchAll = [];
        Object.keys(CardChart).forEach((key) => {
          const item = this.data?.filter((item) => {
            return item.productType === CardChart[key];
          });
          if (item.length > 0) {
            this.xAxisData.push(creditCard[key.toLowerCase()]);
            this.dataBranchAll.push(
              this.data?.find((item) => {
                return item.productType === CardChart[key] && item.type === 'TOAN_HANG';
              })?.count || null
            );
            this.dataBranch.push(
              this.data?.find((item) => {
                return item.productType === CardChart[key] && item.type === 'CHI_NHANH';
              })?.count || null
            );
          }
        });
      });
    }
    this.dataBranchAll?.forEach((value, i) => {
      // const valueAll = this.dataBranch[i] ? value - this.dataBranch[i] : value;
      this.dataBranchAll[i] = this.dataBranch[i] ? value - this.dataBranch[i] : value;
      // this.dataBranch[i] = this.dataBranch[i] ? formatNumber(this.dataBranch[i], 'en', '0.0-2') : null;
      // console.log(valueAll, value, this.dataBranch[i]);
    });
    this.handleMapData();
  }

  getData() { }

  handleMapData() {
    if (this.data) {
      this.option = {
        grid: {
          // left: '5%',
          // top: 0,
          right: '5%',
          bottom: 100,
          // containLabel: true,
        },
        tooltip: {
          formatter: (params) => {
            if (params.seriesName === this.fields.allBranch) {
              const total = formatNumber(
                parseFloat(params.data) +
                (this.dataBranch[params.dataIndex] ? parseFloat(this.dataBranch[params.dataIndex]) : 0),
                'en',
                '0.0-2'
              );
              return params.seriesName + ': ' + total;
            } else {
              return params.seriesName + ': ' + params.data;
            }
          },
        },
        legend: {
          bottom: 'bottom',
          borderRadius: 100,
        },
        xAxis: {
          data: this.xAxisData,
          axisLabel: {
            interval: 0,
            width: this.type === ServiceProductType.Credit ? 70 : 110,
            overflow: 'break',
          },
        },
        yAxis: {
          name: this.type === ServiceProductType.Card || this.type === ServiceProductType.Digital ? 'Số lượng' : 'trđ',
          nameGap: 20,
          // nameGap: this.type === ServiceProductType.Credit ? 60 : 35,
          nameLocation: 'end',
          nameTextStyle: {
            fontSize: 14,
            fontWeight: 'bold',
          },
          type: 'value',
          axisLabel: {
            formatter: (value) => {
              if (value.toString().indexOf('.') === -1) {
                return value;
              } else {
                return '';
              }
            },
          },
        },
        series: [
          {
            name: this.branch,
            data: this.dataBranch,
            type: EChartType.Bar,
            stack: 'one',
            barWidth: 35,
            color: this.colorBranch,
            label: {
              show: true,
              position: 'inside',
              formatter: (params) => {
                if (params.data && parseInt(params.data) === 0) {
                  return '';
                }
                return formatNumber(parseFloat(params.data), 'en', '0.0-2');
              },
            },
          },
          {
            name: this.fields.allBranch,
            data: this.dataBranchAll,
            type: EChartType.Bar,
            stack: 'one',
            barWidth: 35,
            color: this.colorBranchAll,
            label: {
              show: true,
              position: 'top',
              formatter: (params) => {
                return formatNumber(
                  parseFloat(params.data) +
                  (this.dataBranch[params.dataIndex] ? parseFloat(this.dataBranch[params.dataIndex]) : 0),
                  'en',
                  '0.0-2'
                );
              },
            },
          },
        ],
      };
    } else {
      this.data = undefined;
    }
  }
}
