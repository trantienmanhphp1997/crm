import { formatNumber } from '@angular/common';
import {
  Component,
  OnInit,
  EventEmitter,
  Injector,
  ViewEncapsulation,
  ViewChild,
  AfterViewInit,
  Input,
  Output,
} from '@angular/core';
import {
  DisplayGrid,
  GridsterComponent,
  GridsterConfig,
  GridsterItemComponentInterface,
  GridType,
} from 'angular-gridster2';
import { of, forkJoin } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import {
  FunctionCode,
  MobilizationChart,
  Scopes,
  ServiceProductType,
  SessionKey,
} from 'src/app/core/utils/common-constants';
import { CustomerApi } from '../../../apis';
import { ProductChartComponent } from '../product-chart/product-chart.component';

@Component({
  selector: 'app-customer-product',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ProductViewComponent extends BaseComponent implements OnInit, AfterViewInit {
  resizeEvent: EventEmitter<GridsterItemComponentInterface> = new EventEmitter<GridsterItemComponentInterface>();
  viewChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  component = ProductChartComponent;
  options: GridsterConfig;
  productsType: any[];
  timeResize: number;
  @Input() customerCode: string;
  @Input() listBranchCode: string[];
  @Output() loading: EventEmitter<any> = new EventEmitter();
  @ViewChild('gridProduct') gridProduct: GridsterComponent;
  listData: any[];
  dataCount: any;
  tab = 'list';
  first = true;

  constructor(injector: Injector, private customerApi: CustomerApi) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.options = {
      gridType: GridType.Fixed,
      displayGrid: DisplayGrid.None,
      // compactType: CompactType.CompactUp,
      margin: 10,
      outerMargin: true,
      outerMarginTop: 0,
      outerMarginRight: 0,
      outerMarginBottom: 0,
      outerMarginLeft: 0,
      mobileBreakpoint: 640,
      minCols: 1,
      minRows: 1,
      maxCols: 4,
      // maxRows: 9,
      defaultItemCols: 1,
      defaultItemRows: 1,
      // fixedColWidth: 271,
      fixedRowHeight: 125,
      draggable: {
        delayStart: 0,
        enabled: false,
      },
      resizable: {
        delayStart: 0,
        enabled: false,
      },
      itemInitCallback: (item, itemComponent) => {
        this.resizeEvent.emit(itemComponent);
      },
    };

    this.productsType = [
      {
        type: ServiceProductType.Mobilization,
        icon: 'assets/images/Product_HuyDongVon.png',
        codeType: ['100', '101','108'],
        item: {
          cols: 2,
          rows: 3,
          y: 0,
          x: 0,
        },
        listData: [],
        dataChart: [],
      },
      {
        type: ServiceProductType.Credit,
        icon: 'assets/images/Product_Tindung.png',
        codeType: ['103'],
        item: {
          cols: 2,
          rows: 3,
          y: 0,
          x: 0,
        },
        listData: [],
        dataChart: [],
      },
      {
        type: ServiceProductType.Card,
        codeType: ['121', '122'],
        icon: 'assets/images/Product_TheMB.png',
        item: {
          cols: 2,
          rows: 3,
          y: 0,
          x: 0,
        },
        listData: [],
        dataChart: [],
      },
      {
        type: ServiceProductType.Digital,
        icon: 'assets/images/Product_SanPhamSo.png',
        codeType: ['141', '142', '143'],
        item: {
          cols: 2,
          rows: 3,
          y: 0,
          x: 0,
        },
        listData: [],
        dataChart: [],
      },
      {
        type: ServiceProductType.Group,
        icon: 'assets/images/Product_SpTapDoan.png',
        codeType: ['171', '172','109'],
        item: {
          cols: 2,
          rows: 3,
          y: 0,
          x: 0,
        },
        listData: [],
        dataChart: [],
      },
    ];

    const prop = this.sessionService.getSessionData(SessionKey.PRODUCT_DETAIL_CUSTOMER);
    if (!prop || !prop.data || prop?.customerCode !== this.customerCode) {
      forkJoin([
        this.customerApi
          .getMobilizationProductChart({
            customerCode: this.customerCode,
            rsId: this.objFunction?.rsId,
            scope: Scopes.VIEW,
          })
          .pipe(catchError((e) => of(undefined))),

        this.customerApi
          .getCreditProductChart({
            customerCode: this.customerCode,
            rsId: this.objFunction?.rsId,
            scope: Scopes.VIEW,
          })
          .pipe(catchError((e) => of(undefined))),

        this.customerApi
          .getDigitalChart({
            customerCode: this.customerCode,
            rsId: this.objFunction?.rsId,
            scope: Scopes.VIEW,
          })
          .pipe(catchError((e) => of(undefined))),

        this.customerApi
          .getProductcomprodChart({
            customerCode: this.customerCode,
            rsId: this.objFunction?.rsId,
            scope: Scopes.VIEW,
          })
          .pipe(catchError((e) => of(undefined))),

        this.customerApi
          .getCreditCardChart({
            customerCode: this.customerCode,
            rsId: this.objFunction?.rsId,
            scope: Scopes.VIEW,
          })
          .pipe(catchError((e) => of(undefined))),
        this.customerApi
          .getProductByCustomerCode({
            customerCode: this.customerCode,
            rsId: this.objFunction?.rsId,
            scope: Scopes.VIEW,
          })
          .pipe(catchError((e) => of(undefined))),
      ]).subscribe(
        ([
          dataMobilizationChart,
          dataCreditChart,
          dataDigitalChart,
          dataComprodchart,
          dataCreditCardChart,
          listData,
        ]) => {
          const data = {
            dataMobilizationChart,
            dataCreditChart,
            dataDigitalChart,
            dataComprodchart,
            dataCreditCardChart,
            listData,
          };
          this.sessionService.setSessionData(SessionKey.PRODUCT_DETAIL_CUSTOMER, {
            customerCode: this.customerCode,
            data,
          });
          this.fillDataProp(data);
        }
      );
    } else {
      this.fillDataProp(prop.data);
    }
  }

  fillDataProp(data) {
    this.dataCount = {};
    this.dataCount[ServiceProductType.Mobilization] = data?.dataMobilizationChart;
    this.dataCount[ServiceProductType.Credit] = data?.dataCreditChart;
    this.dataCount[ServiceProductType.Digital] = data?.dataDigitalChart;
    this.dataCount[ServiceProductType.Group] = data?.dataComprodchart;
    this.dataCount[ServiceProductType.Card] = data?.dataCreditCardChart;
    const countCredit = new Set(data?.dataCreditChart?.map((i) => i.productType)).size || 0;
    this.productsType.find((i) => i.type === ServiceProductType.Credit).item.cols = countCredit > 5 ? 4 : 2;
    this.listData = data?.listData || [];
    this.handleMapData('CHI_NHANH');
    this.isLoading = false;
  }

  ngAfterViewInit() {
    this.setColWidth();
  }

  setColWidth() {
    const interval = setInterval(() => {
      if (this.gridProduct?.curWidth) {
        const magrin = (this.options.maxCols - 1) * this.options.margin;
        this.options.fixedColWidth = (this.gridProduct?.curWidth - magrin) / this.options.maxCols;
        this.gridProduct.optionsChanged();
        clearInterval(interval);
      }
    }, 100);
  }

  handleMapData(type: string) {
    this.productsType?.forEach((item) => {
      if (
        this.dataCount &&
        this.dataCount[item.type]?.filter((itemCount) => {
          return type === itemCount.type && itemCount.productType !== MobilizationChart.CasaBQ;
        }).length > 0
      ) {
        // if (item.type === ServiceProductType.Digital) {
        //   let countDigital = 0;
        //   this.dataCount[item.type]
        //   ?.filter((itemCount) => {
        //     return type === itemCount.type;
        //   }).filter((itemType) => {
        //       countDigital++;
        //     }
        //   });
        //   item.count = countDigital;

        // } else {
        item.count =
          this.dataCount[item.type]
            ?.filter((itemCount) => {
              return type === itemCount.type && itemCount.productType !== MobilizationChart.CasaBQ;
            })
            ?.map((i) => i.count || 0)
            .reduce((a, c) => {
              return a + c;
            }) || 0;
        // }
        item.total =
          this.dataCount[item.type]
            ?.filter((itemCount) => {
              return type === itemCount.type && itemCount.productType !== MobilizationChart.CasaBQ;
            })
            ?.map((i) => i.total || 0)
            .reduce((a, c) => {
              return a + c;
            }) || 0;
        item.total = formatNumber(item.total, 'en', '0.0-2');
        item.dataChart = this.dataCount[item.type];
      } else {
        item.count = 0;
        item.total = 0;
      }
      item.listData = this.listData?.filter((itemProduct) => {
        return item?.codeType?.indexOf(itemProduct.type) > -1;
      });
    });
  }

  onChangeTab(e) {
    if (e.index === 0) {
      this.tab = 'chart';
      this.handleMapData('TOAN_HANG');
      if (this.first && (document.getElementById('gridProduct').querySelector('.p-tabview-panel') as HTMLElement)) {
        const timer = setTimeout(() => {
          const heightGridster =
            this.gridProduct?.rows * this.options?.fixedRowHeight + (this.gridProduct?.rows - 1) * this.options?.margin;
          (
            document.getElementById('gridProduct').querySelector('.p-tabview-panel') as HTMLElement
          ).style.height = `${heightGridster}px`;
          this.gridProduct?.calculateLayout();
          this.first = false;
          clearTimeout(timer);
        });
      }
    } else {
      this.tab = 'list';
      this.handleMapData('CHI_NHANH');
    }
  }

  isShow(type) {
    return (
      type === ServiceProductType.Mobilization ||
      type === ServiceProductType.Credit ||
      type === ServiceProductType.Digital ||
      type === ServiceProductType.Group ||
      type === ServiceProductType.Card
    );
  }

  scrollTo(id: string) {
    this.communicateService.request({ name: 'ExpandGroup', type: id });
    const timer = setTimeout(() => {
      (document.getElementById(`${this.tab}-${id?.toLowerCase()}`) as HTMLElement)?.scrollIntoView();
      clearTimeout(timer);
    }, 200);
  }

  resizeWindow() {
    this.options?.api?.resize();
    this.timeResize = new Date().valueOf();
    this.setColWidth();
  }

  showLoading(isLoading) {
    // this.loading.emit(isLoading);
    this.isLoading = isLoading;
  }
}
