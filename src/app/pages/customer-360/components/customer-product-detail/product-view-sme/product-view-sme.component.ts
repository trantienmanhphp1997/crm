import { formatNumber } from '@angular/common';
import {
  Component,
  OnInit,
  EventEmitter,
  Injector,
  ViewEncapsulation,
  ViewChild,
  AfterViewInit,
  Input,
  Output,
} from '@angular/core';
import {
  DisplayGrid,
  GridsterComponent,
  GridsterConfig,
  GridsterItemComponentInterface,
  GridType,
} from 'angular-gridster2';
import { of, forkJoin } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import {
  FunctionCode,
  MobilizationChart,
  Scopes,
  ServiceProductType,
  SessionKey,
} from 'src/app/core/utils/common-constants';
import { CustomerApi } from '../../../apis';
import { ProductChartComponent } from '../product-chart/product-chart.component';
import _ from 'lodash';
import { Utils } from 'src/app/core/utils/utils';

@Component({
  selector: 'product-view-sme-component',
  templateUrl: './product-view-sme.component.html',
  styleUrls: ['./product-view-sme.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ProductViewSmeComponent extends BaseComponent implements OnInit, AfterViewInit {
  resizeEvent: EventEmitter<GridsterItemComponentInterface> = new EventEmitter<GridsterItemComponentInterface>();
  viewChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  component = ProductChartComponent;
  options: GridsterConfig;
  productsType: any[];
  timeResize: number;
  @Input() divisionProduct: any;
  @Input() customerCode: string;
  @Input() listBranchCode: string[];
  @Output() loading: EventEmitter<any> = new EventEmitter();
  @ViewChild('gridProduct') gridProduct: GridsterComponent;
  listData: any[];
  dataCount: any;
  tab = 'list';
  first = true;

  productMap = [];
  constructor(injector: Injector, private customerApi: CustomerApi) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    const paramProduct = {
      customerCode: this.customerCode,
      rsId: this.objFunction?.rsId,
      scope: Scopes.VIEW,
      blockCode: this.divisionProduct,
    };

    const prop = this.sessionService.getSessionData(SessionKey.PRODUCT_DETAIL_CUSTOMER);
    if (!prop || !prop.data || prop?.customerCode !== this.customerCode) {
      forkJoin([
        this.customerApi.getProductGroupTypeSme(this.divisionProduct).pipe(catchError((e) => of(undefined))),
        this.customerApi.getProductGroupCustomerSme(paramProduct).pipe(catchError((e) => of(undefined))),
      ]).subscribe(([listProductGroupType, listProductGroupCustomer]) => {
        this.productsType = listProductGroupType?.map((element) => {
          return {
            ...element,
            icon: 'assets/images/sme/' + element.icon + '.png',
            dataProduct: listProductGroupCustomer?.find((item) => item.groupCode === element.groupCode),
          };
        });
        this.isLoading = false;
      });
    } else {
      this.fillDataProp(prop.data);
    }
  }

  fillDataProp(data) {
    this.dataCount = {};
    this.dataCount[ServiceProductType.Mobilization] = data?.dataMobilizationChart;
    this.dataCount[ServiceProductType.Credit] = data?.dataCreditChart;
    this.dataCount[ServiceProductType.Digital] = data?.dataDigitalChart;
    this.dataCount[ServiceProductType.Group] = data?.dataComprodchart;
    this.dataCount[ServiceProductType.Card] = data?.dataCreditCardChart;
    const countCredit = new Set(data?.dataCreditChart?.map((i) => i.productType)).size || 0;
    this.productsType.find((i) => i.type === ServiceProductType.Credit).item.cols = countCredit > 5 ? 4 : 2;
    this.listData = data?.listData || [];
    this.isLoading = false;
  }

  ngAfterViewInit() {
    this.setColWidth();
  }

  setColWidth() {
    const interval = setInterval(() => {
      if (this.gridProduct?.curWidth) {
        const magrin = (this.options.maxCols - 1) * this.options.margin;
        this.options.fixedColWidth = (this.gridProduct?.curWidth - magrin) / this.options.maxCols;
        this.gridProduct.optionsChanged();
        clearInterval(interval);
      }
    }, 100);
  }

  scrollTo(id: string) {
    this.communicateService.request({ name: 'ExpandGroup', type: id });
    const timer = setTimeout(() => {
      (document.getElementById(`${this.tab}-${id?.toLowerCase()}`) as HTMLElement)?.scrollIntoView();
      clearTimeout(timer);
    }, 200);
  }

  resizeWindow() {
    this.options?.api?.resize();
    this.timeResize = new Date().valueOf();
    this.setColWidth();
  }

  showLoading(isLoading) {
    this.isLoading = isLoading;
  }

  isNotNull(data) {
    return Utils.isNotNull(data);
  }
}
