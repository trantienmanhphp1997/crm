import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, Injector, Input } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';

@Component({
  selector: 'app-product-detail-sme',
  templateUrl: './product-detail-sme.component.html',
  styleUrls: ['./product-detail-sme.component.scss'],
})
export class ProductDetailSmeComponent extends BaseComponent implements OnInit {
  @Input() dataProduct: any;
  title: string;
  listFieldProduct = [];

  constructor(injector: Injector, private modalActive: NgbActiveModal) {
    super(injector);
  }

  ngOnInit(): void {
    this.title = this.dataProduct?.name;
    this.listFieldProduct = this.dataProduct.data;
  }

  closeModal() {
    this.modalActive.close(false);
  }
}
