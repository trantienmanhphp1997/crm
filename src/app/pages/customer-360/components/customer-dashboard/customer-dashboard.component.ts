import { Component, OnInit, EventEmitter, Injector } from '@angular/core';
import { DisplayGrid, GridsterConfig, GridsterItem, GridsterItemComponentInterface, GridType } from 'angular-gridster2';
import * as _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CustomerStatusChartComponent } from '../customer-status-chart/customer-status-chart.component';
import { CustomerBscoreChartComponent } from '../customer-bscore-chart/customer-bscore-chart.component';
import { CustomerToiChartComponent } from '../customer-toi-chart/customer-toi-chart.component';
import { FunctionCode, functionUri } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'app-customer-dashboard',
  templateUrl: './customer-dashboard.component.html',
  styleUrls: ['./customer-dashboard.component.scss'],
})
export class CustomerDashboardComponent extends BaseComponent implements OnInit {
  options: GridsterConfig;
  dashboard: Array<GridsterItem>;
  resizeEvent: EventEmitter<GridsterItemComponentInterface> = new EventEmitter<GridsterItemComponentInterface>();
  viewChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(injector: Injector) {
    super(injector);
  }

  showSideBar() {
    const timer = setTimeout(() => {
      this.options?.api?.resize();
      this.viewChange.emit(true);
      clearTimeout(timer);
    }, 100);
  }

  ngOnInit(): void {
    this.options = {
      gridType: GridType.VerticalFixed,
      displayGrid: DisplayGrid.None,
      // compactType: CompactType.CompactUp,
      margin: 10,
      outerMargin: true,
      outerMarginTop: 0,
      outerMarginRight: 0,
      outerMarginBottom: 0,
      outerMarginLeft: 0,
      mobileBreakpoint: 640,
      minCols: 1,
      minRows: 1,
      maxCols: 3,
      // maxRows: 9,
      defaultItemCols: 1,
      defaultItemRows: 1,
      // fixedColWidth: 105,
      fixedRowHeight: 125,
      draggable: {
        delayStart: 0,
        enabled: false,
      },
      resizable: {
        delayStart: 0,
        enabled: false,
      },
      itemInitCallback: (item, itemComponent) => {
        this.resizeEvent.emit(itemComponent);
      },
    };
    this.dashboard = [
      {
        cols: 1,
        rows: 3,
        y: 0,
        x: 0,
        component: CustomerStatusChartComponent,
      },
      {
        cols: 1,
        rows: 3,
        y: 0,
        x: 0,
        component: CustomerBscoreChartComponent,
      },
      {
        cols: 1,
        rows: 3,
        y: 0,
        x: 0,
        component: CustomerToiChartComponent,
      },
    ];
  }

  redirectTo() {
    this.isLoading = true;
    this.router.navigateByUrl(`${functionUri.customer_360_manager}`);
  }
}
