import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerBscoreChartComponent } from './customer-bscore-chart.component';

describe('CustomerBscoreChartComponent', () => {
  let component: CustomerBscoreChartComponent;
  let fixture: ComponentFixture<CustomerBscoreChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerBscoreChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerBscoreChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
