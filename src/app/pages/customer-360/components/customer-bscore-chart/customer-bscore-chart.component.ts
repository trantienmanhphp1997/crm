import { Component, OnInit, Injector, Input, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import {CustomerType, EChartType, FunctionCode, Scopes} from 'src/app/core/utils/common-constants';
import { percentage } from 'src/app/core/utils/function';
import { EChartsOption } from 'echarts';
import { DatePipe, formatNumber } from '@angular/common';
import _ from 'lodash';
import * as moment from 'moment';
import {CustomerApi} from "../../apis";

@Component({
  selector: 'app-customer-bscore-chart',
  templateUrl: './customer-bscore-chart.component.html',
  styleUrls: ['./customer-bscore-chart.component.scss'],
  providers: [DatePipe],
})
export class CustomerBscoreChartComponent extends BaseComponent implements OnInit, OnChanges {
  isPieChart = true;
  listBranchCode = [];
  @Input() viewChange: EventEmitter<boolean> = new EventEmitter();
  data: any[];
  dataPercentage: string[] = [];
  option: EChartsOption;
  title: string;
  note: string;

  constructor(injector: Injector, private datePipe: DatePipe,private customerApi: CustomerApi) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);

  }

  ngOnInit(): void {
    this.isLoading = true;
    const params = {
      customerTypes: [CustomerType.INDIV, CustomerType.NHS],
      rsId: this.objFunction?.rsId,
      scope: Scopes.VIEW,
    };
    this.customerApi.getBScoreDataChart(params).subscribe(dataBScore => {
      this.data = dataBScore;
      const date = _.get(_.first(this.data), 'monthValue');
      this.title = `${_.get(this.fields, 'bscore_chart')} ${
        !_.isEmpty(date?.trim()) ? moment(date, 'YYYY/MM').format('MM/YYYY') : '---'
      }`;
      if (_.size(this.data) > 0) {
        const sum = _.reduce(
          _.map(this.data, (x) => x.value),
          (a, c) => {
            return a + c;
          }
        );
        this.translate.get('fields.number_customer_bscore', { number: sum }).subscribe((message) => {
          this.note = message;
        });
        _.forEach(this.data, (item) => {
          this.dataPercentage.push(percentage(item.value || 0, sum));
        });
        this.handleMapData(EChartType.Pie);
      }
      this.isLoading = false;
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    // const date = _.get(_.first(this.data), 'monthValue');
    // this.title = `${_.get(this.fields, 'bscore_chart')} ${
    //   !_.isEmpty(date?.trim()) ? moment(date, 'YYYY/MM').format('MM/YYYY') : '---'
    // }`;
    // if (_.size(this.data) > 0) {
    //   const sum = _.reduce(
    //     _.map(this.data, (x) => x.value),
    //     (a, c) => {
    //       return a + c;
    //     }
    //   );
    //   this.translate.get('fields.number_customer_bscore', { number: sum }).subscribe((message) => {
    //     this.note = message;
    //   });
    //   _.forEach(this.data, (item) => {
    //     this.dataPercentage.push(percentage(item.value || 0, sum));
    //   });
    //   this.handleMapData(EChartType.Pie);
    // }
  }

  handleMapData(type: string) {
    if (_.size(this.data) > 0) {
      if (type === EChartType.Pie) {
        this.option = {
          tooltip: {
            trigger: 'item',
            formatter: (params) => {
              return `${formatNumber(params.value || 0, 'en', '1.0-0')} (${params.percent})%`;
            },
          },
          legend: {
            show: false,
            bottom: 0,
          },
          series: [
            {
              // bottom: '40%',
              type: EChartType.Pie,
              radius: ['30%', '55%'],
              labelLine: {
                length: 5,
              },
              label: {
                formatter: '{d}%',
              },
              data: [],
            },
          ],
        };
        _.forEach(this.data, (item) => {
          if (item.value > 0) {
            this.option.series[0].data.push({
              value: item.value,
              name: item.label,
              itemStyle: { color: item.color },
            });
          }
        });
      } else {
        this.option = {
          xAxis: {
            type: 'category',
          },
          yAxis: {
            type: 'value',
          },
          legend: {
            show: false,
            bottom: 0,
          },
          grid: {
            left: '0%',
            right: '0%',
            containLabel: true,
          },
          tooltip: {
            trigger: 'item',
            formatter: (params) => {
              return `${formatNumber(params.value || 0, 'en', '1.0-0')} (${
                this.dataPercentage[params.seriesIndex] || 0
              })%`;
            },
          },
          series: [],
        };
        const data = [];
        _.forEach(this.data, (item) => {
          data.push({
            name: item.label,
            data: [item.value],
            type: EChartType.Bar,
            showBackground: false,
            itemStyle: {
              color: item.color,
            },
            barWidth: 28,
            barGap: '5%',
          });
        });
        this.option.series = data;
      }
    } else {
      this.data = undefined;
    }
  }

  switchChart() {
    if (this.isLoading) {
      return;
    }
    this.isPieChart = !this.isPieChart;
    if (this.isPieChart) {
      this.handleMapData(EChartType.Pie);
    } else {
      this.handleMapData(EChartType.Bar);
    }
  }
}
