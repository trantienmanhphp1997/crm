import {AfterViewInit, Component, Injector, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { global } from '@angular/compiler/src/util';
import {
  CommonCategory,
  Scopes,
  maxInt32,
  FunctionCode,
  functionUri,
  StatusWork,
  TaskType,
  ScreenType,
  Division, SessionKey,
} from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { empty, forkJoin, of } from 'rxjs';
import _ from 'lodash';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { FileService } from 'src/app/core/services/file.service';
import { cleanDataForm } from 'src/app/core/utils/function';
import { RmModalComponent } from 'src/app/pages/rm/components/rm-modal/rm-modal.component';
import { CustomerApi, CustomerDetailApi } from 'src/app/pages/customer-360/apis/customer.api';
import { RmApi } from 'src/app/pages/rm/apis';
import { Utils } from 'src/app/core/utils/utils';
import {HistoryAssignmentComponent} from '../history-assignment/history-assignment.component';
import {CustomValidators} from "../../../../core/utils/custom-validations";

@Component({
  selector: 'app-assign-ld-rm',
  templateUrl: './assign-ld-rm.component.html',
  styleUrls: ['./assign-ld-rm.component.scss'],
})
export class AssignLdRmComponent extends BaseComponent implements OnInit,AfterViewInit {

  isLoading = false;
  @ViewChild('table') table: DatatableComponent;
  limit = global.userConfig.pageSize;
  pageable: Pageable;
  params: any = {
    pageNumber: 0,
    pageSize: global?.userConfig?.pageSize,
    rsId: '',
    scope: Scopes.VIEW,
  };
  objFunctionRm: any;
  objFunctionCustomer: any;

  lastUpdateDate: any;
  categoryCommon: any;
  categoryCommonNotAssign: any;
  listData = [];
  total: number = 0;
  isOpenMore = true;
  listDivision = [{
    code: 'INDIV',
    displayName: 'INDIV - Khách hàng cá nhân'
  }];

  facilities: Array<any>;
  facilityCode: Array<string>;
  employeeCode: Array<string>;
  employeeCodeParams: Array<string>;
  isRm: boolean;
  model: any = {};
  termData = [];
  maxBranchSearch = 10;
  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private customerApi: CustomerApi,
    private fileService: FileService,
    private rmApi: RmApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.scopes = _.get(this.objFunction, 'scopes');
    this.prop = this.router.getCurrentNavigation()?.extras?.state;
  }
  formSearch = this.fb.group({
    customerCode: [''],
    customerName: [''],
    contractNumber: [''],
    systemAssign: [''],
    rmCodes: [''],
    rmBranchCodes: [''],
    isAssign: [true, CustomValidators.required],
  });

  customerType = '';
  listProduct = [];
  listProductAssign = [];
  listProductNotAssign = [];
  prevParams: any;
  prop: any;
  permissionOpp = false;
  allRm = true;
  listRuleView = [];
  allBranch = true;
  scopes: Array<any>;

  ngOnInit() {
    this.employeeCode = _.get(this.fields, 'all');
    this.customerType = this.listDivision[0].code;
    this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
    this.facilityCode = [];
    this.facilityCode.push(this.currUser.branch);
    this.getDataFilter();
    this.pageable = {
      totalElements: 0,
      totalPages: 1,
      currentPage: this.params.pageNumber,
      size: this.limit,
    };
    // this.formSearch.controls.isAssign.setValue(true);
    this.search(true);
  }
  ngAfterViewInit(): void {
    this.formSearch.controls.isAssign.valueChanges.subscribe((value) => {
      if(value === true){
        this.listProduct = this.listProductAssign;
      }
      else{
        this.listProduct = this.listProductNotAssign;

      }
      this.formSearch.controls.systemAssign.setValue(this.listProduct[0]?.value);
      this.formSearch.controls.customerCode.setValue('');
      this.formSearch.controls.customerName.setValue('');
      this.formSearch.controls.contractNumber.setValue('');
      // this.removeBranchSelected();
      // this.removeRMSelected();
    })
  }
  getDataFilter() {
    this.isLoading = true;
    this.prop = this.sessionService.getSessionData(FunctionCode.CUSTOMER_360_ASSIGN_LD_RM);
    if(!this.prop) {
      const listCommonCategory = [CommonCategory.PRODUCT_DELIVERED, CommonCategory.PRODUCT_DELIVERED_NOT_ASSIGN];
      const rsIdOfRm = this.objFunction?.rsId;

      forkJoin([
        this.commonService.getCommonCategoryByListCode(listCommonCategory),
        this.categoryService.getBranchesOfUser(rsIdOfRm, Scopes.VIEW).pipe(catchError(() => of(undefined))),
      ]) .subscribe(
        ([result,listBranchOfUser]) => {
          const listProduct = [];
          const listProductNotAssign = [];
          this.facilities = listBranchOfUser || [];
          if (result?.length > 0) {
            result.forEach((category) => {
              if (category.commonCategoryCode === CommonCategory.PRODUCT_DELIVERED) {
                listProduct.push(category);
              }
              else {
                listProductNotAssign.push(category);
              }
            });
          }
          const isRm = _.isEmpty(listBranchOfUser);
          this.isRm = isRm;
          this.prop = {
            listProduct,
            listProductNotAssign,
            isRm,
            listBranchOfUser
          }
          this.sessionService.setSessionData(FunctionCode.CUSTOMER_360_ASSIGN_LD_RM, this.prop);

          this.setDataFilter();
          this.listProduct = this.listProductAssign;
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
    } else {
      this.setDataFilter();
      this.prevParams = {...this.prop.prevParams};
      this.formSearch.patchValue(this.prevParams);
      this.facilities = this.prop.listBranchOfUser;
      this.employeeCode = this.prop.prevParams?.rmCodes;
      this.facilityCode = this.prop.prevParams?.rmBranchCodes;
      this.employeeCodeParams = this.employeeCode;
      this.formSearch.controls.isAssign.setValue(this.prevParams.isAssign);
      if(this.prevParams.isAssign === true){
        this.listProduct = this.listProductAssign;
      }
      else{
        this.listProduct = this.listProductNotAssign;
      }
    }
  }

  setDataFilter() {
    this.prop = this.sessionService.getSessionData(FunctionCode.CUSTOMER_360_ASSIGN_LD_RM);
    this.listProductAssign = this.prop.listProduct;
    this.listProductNotAssign = this.prop.listProductNotAssign;
    this.isRm = this.prop.isRm;
    if(this.isRm){
      this.formSearch.controls.isAssign.setValue(true);
      this.formSearch.get('isAssign').disable();
    }
    if (!this.listProductAssign.find((x) => x.code === '')) {
      this.listProductAssign.unshift({ code: '', name: this.fields.all });
    }
    // if(!this.listProductNotAssign.find((x) => x.code === '' )){
    //   this.listProductNotAssign.unshift({code: '', name: this.fields.all});
    // }
  }

  changeBranch(event) {
    const checkBranch = this.facilityCode ? this.facilityCode.length : this.facilities.length;
    if(checkBranch > this.maxBranchSearch){
      this.messageService.warn('Chỉ chọn tối đa ' + this.maxBranchSearch + ' chi nhánh');
      return;
    }
    this.facilityCode = event;
    this.removeRMSelected();
  }

  searchRM() {
    const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
    modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
    modal.componentInstance.branchCodes = this.facilityCode;
    modal.componentInstance.block = this.customerType;
    modal.result
      .then((res) => {
        if (res) {
          this.employeeCode = [];
          this.employeeCodeParams = [];
          const listRMSelect = res.listSelected?.map((item) => {
            this.employeeCode.push(item?.t24Employee?.employeeCode);
            this.employeeCodeParams.push(item.t24Employee.employeeCode);
            return {
              hrsCode: item?.hrisEmployee?.employeeId,
              code: item?.t24Employee?.employeeCode,
              name: item?.hrisEmployee?.fullName,
            };
          });
          this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
          this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);

          this.translate.get('fields').subscribe((res) => {
            this.fields = res;
            const codes = this.employeeCode;
            const codeParams = this.employeeCodeParams;
            const countCode = (codes) => codes.reduce((a, b) => ({ ...a, [b]: (a[b] || 0) + 1 }), {});
            const countCodeParams = (codeParams) => codeParams.reduce((a, b) => ({ ...a, [b]: (a[b] || 0) + 1 }), {});

            this.employeeCode = Object.keys(countCode(codes));
            this.employeeCodeParams = Object.keys(countCodeParams(codeParams));
            this.employeeCode =
              _.size(this.employeeCode) === 0 ||
              (_.size(this.employeeCode) === 1 && this.employeeCode[0] === _.get(this.fields, 'all'))
                ? _.get(this.fields, 'all')
                : _.join(this.employeeCode, ', ');
          });

          if (this.employeeCode.length === 0) {
            this.removeRMSelected();
          }
        } else {
          this.removeRMSelected();
        }
      })
      .catch(() => {});
  }

  removeRMSelected() {
    this.employeeCode = _.get(this.fields, 'all');
    this.employeeCodeParams = null;
    this.termData = [];
  }

  removeBranchSelected() {
    this.facilityCode = null;
    this.removeRMSelected();
  }

  checkEmployeeCode() {
    let show = false;
    this.translate.get('fields').subscribe((res) => {
      this.fields = res;
      if (this.employeeCode === _.get(this.fields, 'all')) {
        show = false;
      } else {
        show = true;
      }
    });
    return show;
  }

  search(isSearch: boolean) {
    const checkBranch = this.facilityCode ? this.facilityCode.length : this.facilities.length;
    if(checkBranch > this.maxBranchSearch){
      this.messageService.warn('Chỉ chọn tối đa ' + this.maxBranchSearch + ' chi nhánh');
      return;
    }
    this.isLoading = true;
    if (isSearch) {
      this.params.pageNumber = 0;
    }
    let paramsFilter = {
      ...cleanDataForm(this.formSearch),
      pageNumber: this.params.pageNumber,
      pageSize: this.limit,
      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW,
    };
    // paramsFilter.rmCodes = this.employeeCode === _.get(this.fields, 'all') ? null : this.employeeCode;
    paramsFilter.rmCodes = this.checkEmployeeCode() ? this.employeeCodeParams : null
    if (_.isEmpty(this.facilityCode) || this.facilityCode === _.get(this.fields, 'all')) {
      paramsFilter.rmBranchCodes = null;
    } else {
      paramsFilter.rmBranchCodes = this.facilityCode;
    }
    paramsFilter.systemAssign = this.formSearch.controls.systemAssign.value;
    console.log(paramsFilter);
    //  const branchesOfUser = this.categoryService.getBranchesOfUser(this.objFunction.rsId, Scopes.VIEW);

    forkJoin([this.customerApi.searchCustomerAssignRMCredit(paramsFilter),
      this.customerApi.countCustomerAssignRMCredit(paramsFilter)]).subscribe(
      ([listRmCredit, countRmCredit]) => {
        this.total = countRmCredit;
        let listDataTemp = listRmCredit?.content || [];
        if (listDataTemp.length > 0) {
          listDataTemp.forEach((item) => {
            if(paramsFilter.isAssign === true){
              item.isAssign = true;
            }
            else{
              item.isAssign = false;
            }
            if(item.rmCode){
              item.rmManager = item.rmCode + ' - ' + item.rmName;
            }
            else{
              item.rmManager = '';
            }
          });
        }
        this.listData = listDataTemp;

        this.pageable = {
          totalElements: this.total || 0,
          totalPages: Math.floor(this.total / this.params.pageSize),
          currentPage: this.params.pageNumber,
          size: this.params.pageSize,
        };
        this.prop.prevParams = paramsFilter;
        this.sessionService.setSessionData(FunctionCode.CUSTOMER_360_ASSIGN_LD_RM,this.prop);
        this.isLoading = false;
      },
      (e) => {
        console.log(e);
        this.messageService.error('Thực hiện không thành công');
        this.isLoading = false;
      }
    );
  }

  openMore() {
    this.isOpenMore = !this.isOpenMore;
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.pageNumber = pageInfo.offset;
    this.search(false);
  }

  handleChangePageSize(event) {
    this.params.pageNumber = 0;
    this.params.pageSize = event;
    this.limit = event;
    this.search(false);
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      const item = event.row;
      if (!_.isEmpty(_.trim(item.customerCode))) {
        this.router.navigate(['/customer360/customer-manager', 'detail', 'indiv', item.customerCode], {
          skipLocationChange: true,
          queryParams: {
            showBtnRevenueShare: false,
            isShowCreateOppBtn: this.checkScopes(['CREATE_OPPORTUNITY_SALE'])
          },
        });
      }
    }
  }

  exportFile() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+this.pageable?.totalElements <= 0) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.isLoading = true;
    const paramExport = {
      ...this.formSearch.value,
    };
    // paramExport.rmCodes = this.employeeCode === _.get(this.fields, 'all') ? null : this.employeeCode;
    paramExport.rmCodes = this.checkEmployeeCode() ? this.employeeCodeParams : null
    if (_.isEmpty(this.facilityCode) || this.facilityCode === _.get(this.fields, 'all')) {
      paramExport.rmBranchCodes = null;
    } else {
      paramExport.rmBranchCodes = this.facilityCode;
    }
    paramExport.rsId = this.objFunction.rsId;
    paramExport.scope = Scopes.VIEW;
    paramExport.pageNumber = 0;
    paramExport.pageSize = maxInt32;

    this.customerApi.exportCustomerAssignRMCredit(paramExport).subscribe(
      (res) => {
        if (res) {
          this.download(res);
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(_.get(this.notificationMessage, 'export_error'));
        this.isLoading = false;
      }
    );
  }

  download(fileId: string) {
    let todayExcel = new Date();
    let titleExcel =
      'phan-giao-ld-rm' +
      this.currUser.username +
      '-' +
      todayExcel.getFullYear().toString() +
      (todayExcel.getMonth() + 1).toString() +
      todayExcel.getDate().toString() +
      '.xlsx';
    this.fileService.downloadFile(fileId, titleExcel).subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }

  // CRMCN-3672 - NAMNH.OS - START
  historyAssign(item: any) {
    const modal = this.modalService.open(HistoryAssignmentComponent, {
      windowClass: 'customer-history-assignment',
    });
    modal.componentInstance.contractNumber = item?.contractNumber === undefined ? '' : item?.contractNumber;
    modal.componentInstance.hrsCode = item?.customerCode;
    modal.componentInstance.systemAssign = item?.systemAssign === undefined ? '' : item?.systemAssign;
    modal.componentInstance.isShowColumnContractNumber = false;
    modal.componentInstance.isTitleAssignLdRM = true;
    // modal.componentInstance.customerTypeSale = this.prevParams?.customerTypeSale;
    modal.componentInstance.customerType = Division.INDIV;
    modal.componentInstance.manageType = 0;
  }
  // CRMCN-3672 - NAMNH.OS - END

  checkScopes(codes: Array<any>) {
    if (Utils.isArrayEmpty(codes)) return false;
    return Utils.isArrayNotEmpty(_.filter(this.scopes, (x) => codes.includes(x)));
  }
}
