import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignLdRmComponent } from './assign-ld-rm.component';

describe('AssignLdRmComponent', () => {
  let component: AssignLdRmComponent;
  let fixture: ComponentFixture<AssignLdRmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignLdRmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignLdRmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
