import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerProductUsedChartComponent } from './customer-product-used-chart.component';

describe('CustomerProductUsedChartComponent', () => {
  let component: CustomerProductUsedChartComponent;
  let fixture: ComponentFixture<CustomerProductUsedChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerProductUsedChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerProductUsedChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
