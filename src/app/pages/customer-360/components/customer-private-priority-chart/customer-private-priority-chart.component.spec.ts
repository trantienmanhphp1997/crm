import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerPrivatePriorityChartComponent } from './customer-private-priority-chart.component';

describe('CustomerPrivatePriorityChartComponent', () => {
  let component: CustomerPrivatePriorityChartComponent;
  let fixture: ComponentFixture<CustomerPrivatePriorityChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerPrivatePriorityChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerPrivatePriorityChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
