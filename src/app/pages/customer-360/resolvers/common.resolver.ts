import { CommonCategory, maxInt32 } from 'src/app/core/utils/common-constants';
import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { CommonApi } from '../apis';

@Injectable()
export class CommonResolver implements Resolve<Object> {
  constructor(private router: Router, private api: CommonApi) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.api.get('commons', { page: 0, size: maxInt32, commonCategoryCode: CommonCategory.KH_PHONE_NO_CONFIG });
  }
}

@Injectable()
export class AgeGroupResolver implements Resolve<Object> {
  constructor(private router: Router, private api: CommonApi) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.api.get('commons', { page: 0, size: maxInt32, commonCategoryCode: CommonCategory.AGE_GROUP });
  }
}

@Injectable()
export class SectorResolver implements Resolve<Object> {
  constructor(private router: Router, private api: CommonApi) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.api.get('commons', {
      page: 0,
      size: maxInt32,
      commonCategoryCode: CommonCategory.PRIVATE_PRIORITY_CONFIG,
    });
  }
}

@Injectable()
export class EmailConfigResolver implements Resolve<Object> {
  constructor(private router: Router, private api: CommonApi) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.api.get('commons', { page: 0, size: maxInt32, commonCategoryCode: CommonCategory.KH_EMAIL_CONFIG });
  }
}

@Injectable()
export class CountriesResolver implements Resolve<Object> {
  constructor(private router: Router, private api: CommonApi) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.api.get('countries');
  }
}

@Injectable()
export class IndustriesResolver implements Resolve<Object> {
  constructor(private router: Router, private api: CommonApi) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.api.get('industries');
  }
}

@Injectable()
export class CustomerObjectConfigResolver implements Resolve<Object> {
  constructor(private router: Router, private api: CommonApi) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.api.get('commons', {
      page: 0,
      size: maxInt32,
      commonCategoryCode: CommonCategory.CUSTOMER_OBJECT_CONFIG,
    });
  }
}

@Injectable()
export class StatusCustomerConfigResolver implements Resolve<Object> {
  constructor(private router: Router, private api: CommonApi) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.api.get('commons', {
      page: 0,
      size: maxInt32,
      commonCategoryCode: CommonCategory.STATUS_CUSTOMER_CONFIG,
    });
  }
}

@Injectable()
export class SegmentCustomerConfigResolver implements Resolve<Object> {
  constructor(private router: Router, private api: CommonApi) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.api.get('commons', {
      page: 0,
      size: maxInt32,
      commonCategoryCode: CommonCategory.SEGMENT_CUSTOMER_CONFIG,
    });
  }
}

@Injectable()
export class AccountGroupConfigResolver implements Resolve<Object> {
  constructor(private router: Router, private api: CommonApi) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.api.get('commons', { page: 0, size: maxInt32, commonCategoryCode: CommonCategory.ACCOUNT_GROUP });
  }
}

@Injectable()
export class LimitExportConfigResolver implements Resolve<Object> {
  constructor(private router: Router, private api: CommonApi) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.api.get('commons', {
      page: 0,
      size: maxInt32,
      commonCategoryCode: CommonCategory.CONFIG_EXPORT_CUSTOMER_LIMIT_COUNT,
    });
  }
}

@Injectable()
export class AssetGroupConfigResolver implements Resolve<Object> {
  constructor(private router: Router, private api: CommonApi) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.api.get('commons', { page: 0, size: maxInt32, commonCategoryCode: CommonCategory.ASSET_GROUP });
  }
}

@Injectable()
export class AssetTypeConfigResolver implements Resolve<Object> {
  constructor(private router: Router, private api: CommonApi) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.api.get('commons', { page: 0, size: maxInt32, commonCategoryCode: 'ASSET_TYPE' });
  }
}
