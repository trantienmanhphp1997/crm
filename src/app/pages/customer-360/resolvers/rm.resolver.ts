import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { RmApi } from "../../rm/apis";

@Injectable()
export class RmManagerResolver implements Resolve<Object> {
    constructor(private router: Router, private api: RmApi) { }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.api.post('findAll', {
            page: 0,
            size: 10,
            isManagerRmCustomer360: true,
            search: "t24Employee.branchCode:[]"
        });
    }
}
