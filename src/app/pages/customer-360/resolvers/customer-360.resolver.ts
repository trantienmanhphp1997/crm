import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { CustomerDetailApi } from '../apis';
import _ from 'lodash';

@Injectable()
export class Customer360DetailResolver implements Resolve<Object> {
  constructor(private router: Router, private api: CustomerDetailApi) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const code = _.get(route.params, 'code');
    return this.api.getByCode(code);
  }
}

@Injectable()
export class CustomerCostResolver implements Resolve<Object> {
  constructor(private router: Router, private api: CustomerDetailApi) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const code = _.get(route.params, 'code');
    return this.api.get(`cost-collection/${code}`);
  }
}

@Injectable()
export class CollateralResolver implements Resolve<Object> {
  constructor(private router: Router, private api: CustomerDetailApi) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const code = _.get(route.params, 'code');
    return this.api.get(`collateral/${code}`);
    // return this.api.getCollateral();
  }
}

@Injectable()
export class BscoreResolver implements Resolve<Object> {
  constructor(private router: Router, private api: CustomerDetailApi) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const code = _.get(route.params, 'code');
    return this.api.get(`bscore/${code}`);
    // return this.api.getBscore();
  }
}
