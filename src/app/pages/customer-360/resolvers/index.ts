import {
  Customer360DetailResolver,
  CustomerCostResolver,
  CollateralResolver,
  BscoreResolver
} from './customer-360.resolver';
import {
  CommonResolver,
  AgeGroupResolver,
  SectorResolver,
  EmailConfigResolver,
  CountriesResolver,
  IndustriesResolver,
  CustomerObjectConfigResolver,
  StatusCustomerConfigResolver,
  SegmentCustomerConfigResolver,
  AccountGroupConfigResolver,
  LimitExportConfigResolver,
  AssetGroupConfigResolver,
  AssetTypeConfigResolver
} from './common.resolver';
import { RmManagerResolver } from './rm.resolver';

export {
  Customer360DetailResolver,
  CustomerCostResolver,
  CollateralResolver,
  BscoreResolver
} from './customer-360.resolver';
export {
  CommonResolver,
  AgeGroupResolver,
  SectorResolver,
  EmailConfigResolver,
  CountriesResolver,
  IndustriesResolver,
  CustomerObjectConfigResolver,
  StatusCustomerConfigResolver,
  SegmentCustomerConfigResolver,
  AccountGroupConfigResolver,
  LimitExportConfigResolver,
  AssetGroupConfigResolver,
  AssetTypeConfigResolver
} from './common.resolver';
export { RmManagerResolver } from './rm.resolver';

export const RESOLVERS = [
  Customer360DetailResolver,
  CommonResolver,
  AgeGroupResolver,
  SectorResolver,
  EmailConfigResolver,
  CustomerCostResolver,
  CountriesResolver,
  IndustriesResolver,
  CustomerObjectConfigResolver,
  StatusCustomerConfigResolver,
  SegmentCustomerConfigResolver,
  AccountGroupConfigResolver,
  RmManagerResolver,
  LimitExportConfigResolver,
  CollateralResolver,
  AssetGroupConfigResolver,
  AssetTypeConfigResolver,
  BscoreResolver
];
