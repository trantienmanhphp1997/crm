import { MatTabsModule } from '@angular/material/tabs';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Customer360RoutingModule } from './customer-360-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { TranslateModule } from '@ngx-translate/core';
import { APIS } from './apis';
import { RESOLVERS } from './resolvers';
import { SERVICES } from './services';
import { RadioButtonModule } from 'primeng/radiobutton';

import {
  BiddingComponent,
  ConfirmAssignComponent,
  Customer360Component,
  Customer360INDIVDetailComponent,
  CustomerActivityListComponent,
  CustomerAssignmentApproveComponent,
  CustomerAssignmentComponent,
  CustomerAssignmentImportComponent,
  CustomerBscoreChartComponent,
  CustomerDashboardComponent,
  CustomerLeftViewComponent,
  CustomerListComponent,
  CustomerModalComponent,
  CustomerPrivatePriorityChartComponent,
  CustomerProductUsedChartComponent,
  CustomerSegmentChartComponent,
  CustomerStatusChartComponent,
  CustomerToiChartComponent,
  HistoryAssignmentComponent,
  OperationDivisionImportComponent,
  OperationDivisionManagementComponent,
  ProductChartComponent,
  ProductDetailComponent,
  ProductListTypeComponent,
  ProductViewComponent,
  ConfirmAssignmentModalComponent,
  ListErrorComponent,
  Customer360SMEDetailComponent,
  CustomerSmeCollateralComponent,
  CustomerLoanBalanceComponent,
  ProductListTypeSMEComponent,
  ProductDetailSmeComponent,
  ModalRevenueSharingComponent,
  ListRevenueSharingComponent,
  CustomerSmeObligationOtherComponent,
  RevenueSharingCreateComponent,
  RevenueSharingApproveComponent,
  ProductViewSmeComponent,
  CustomerReportFinanceComponent,
  AssignByProductComponent,
  AssignLdRmComponent
} from './components';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxSelectModule } from 'ngx-select-ex';
import { RmModule } from '../rm/rm.module';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { NgxEchartsModule } from 'ngx-echarts';
import * as echarts from 'echarts';
import { GridsterModule } from 'angular-gridster2';
import { DynamicModule } from 'ng-dynamic-component';
import { AccordionModule } from 'primeng/accordion';
import { TabViewModule } from 'primeng/tabview';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { InputNumberModule } from 'primeng/inputnumber';
import { TableModule } from 'primeng/table';
import { MultiSelectModule } from 'primeng/multiselect';
import {MatMenuModule} from '@angular/material/menu';
import { AssignCustomerByRmTttmComponent } from './components/assign-customer-by-rm-tttm/assign-customer-by-rm-tttm.component';
import {ProductListTypeV2Component} from './components/customer-product-detail/product-list-type-v2/product-list-type-v2.component';
import {MenuModule} from 'primeng/menu';
import { RiskCreditComponent } from './components/risk-credit/risk-credit.component';
import {CustomerBirthdayWarningComponent} from "./components/customer-birthday-warning/customer-birthday-warning.component";
import { ActivityLogModule } from './activity-log.module';
import { CustomerManagementComponent } from './components/customer-management/customer-management.component';
import { CustomerDetailComponent } from './components/customer-detail/customer-detail.component';

const COMPONENTS = [
  BiddingComponent,
  Customer360Component,
  Customer360INDIVDetailComponent,
  OperationDivisionManagementComponent,
  OperationDivisionImportComponent,
  CustomerAssignmentComponent,
  CustomerAssignmentImportComponent,
  CustomerModalComponent,
  CustomerAssignmentApproveComponent,
  ConfirmAssignmentModalComponent,
  HistoryAssignmentComponent,
  CustomerLeftViewComponent,
  CustomerDashboardComponent,
  CustomerListComponent,
  CustomerPrivatePriorityChartComponent,
  CustomerProductUsedChartComponent,
  CustomerSegmentChartComponent,
  CustomerStatusChartComponent,
  CustomerBscoreChartComponent,
  CustomerToiChartComponent,
  ProductViewComponent,
  ProductListTypeComponent,
  ProductDetailComponent,
  ProductChartComponent,
  CustomerActivityListComponent,
  ConfirmAssignComponent,
  ListErrorComponent,
  Customer360SMEDetailComponent,
  ModalRevenueSharingComponent,
  ListRevenueSharingComponent,
  RevenueSharingCreateComponent,
  CustomerSmeCollateralComponent,
  CustomerLoanBalanceComponent,
  CustomerSmeCollateralComponent,
  CustomerSmeObligationOtherComponent,
  RevenueSharingApproveComponent,
  ProductViewSmeComponent,
  ProductListTypeSMEComponent,
  ProductDetailSmeComponent,
  CustomerReportFinanceComponent,
  AssignByProductComponent,
  AssignLdRmComponent,
  AssignCustomerByRmTttmComponent,
  ProductListTypeV2Component,
  RiskCreditComponent,
  CustomerBirthdayWarningComponent,
  CustomerManagementComponent,
  CustomerDetailComponent
];

@NgModule({
  declarations: [...COMPONENTS],
    imports: [
        CommonModule,
        SharedModule,
        NgbModule,
        DragDropModule,
        ReactiveFormsModule,
        FormsModule,
        InfiniteScrollModule,
        TranslateModule,
        Customer360RoutingModule,
        NgxDatatableModule,
        NgxSelectModule,
        RmModule,
        MatProgressBarModule,
        MatTabsModule,
        NgxEchartsModule.forRoot({
            echarts,
        }),
        GridsterModule,
        DynamicModule,
        AccordionModule,
        TabViewModule,
        ButtonModule,
        InputTextModule,
        DropdownModule,
        CalendarModule,
        RadioButtonModule,
        InputNumberModule,
        TableModule,
        MultiSelectModule,
        MatMenuModule,
        MenuModule,
        ActivityLogModule
    ],
  providers: [...APIS, ...RESOLVERS, ...SERVICES],
  exports: [...COMPONENTS],
  entryComponents: [...COMPONENTS],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class Customer360Module {}
