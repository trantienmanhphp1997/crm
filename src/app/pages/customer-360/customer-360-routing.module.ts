import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FunctionCode, Scopes, ScreenType } from 'src/app/core/utils/common-constants';
import {
  Customer360Component,
  Customer360INDIVDetailComponent,
  OperationDivisionImportComponent,
  CustomerAssignmentComponent,
  CustomerAssignmentImportComponent,
  CustomerAssignmentApproveComponent,
  OperationDivisionManagementComponent,
  CustomerLeftViewComponent,
  CustomerDashboardComponent,
  Customer360SMEDetailComponent,
  AssignByProductComponent,
  AssignLdRmComponent,
} from './components';
import { TrueResolve } from '../rm/resolvers';
import { RoleGuardService } from 'src/app/core/services/role-guard.service';
import { ConfirmAssignComponent } from './components/confirm-assign/confirm-assign.component';
import { ListRevenueSharingComponent } from './components/list-revenue-sharing/list-revenue-sharing.component';
import { RevenueSharingCreateComponent } from './components/revenue-sharing-create/revenue-sharing-create.component';
import { RevenueSharingApproveComponent } from './components/revenue-sharing-approve/revenue-sharing-approve.component';
import { AssignCustomerByRmTttmComponent } from './components/assign-customer-by-rm-tttm/assign-customer-by-rm-tttm.component';
import {CustomerBirthdayWarningComponent} from "./components/customer-birthday-warning/customer-birthday-warning.component";
import { CustomerManagementComponent } from './components/customer-management/customer-management.component';
import { CustomerDetailComponent } from './components/customer-detail/customer-detail.component';

const routes: Routes = [
  {
    path: '',
    component: CustomerLeftViewComponent,
    outlet: 'app-left-content',
  },
  {
    path: '',
    children: [
      {
        path: 'customer-manager',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.CUSTOMER_360_MANAGER,
        },
        children: [
          {
            path: '',
            component: Customer360Component,
            data: {
              scope: Scopes.VIEW,
            },
          },
          {
            path: 'detail/indiv/:code',
            component: Customer360INDIVDetailComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
          {
            path: 'detail/:type/:code',
            component: Customer360SMEDetailComponent,
            canActivate: [],
            data: {
              scope: Scopes.VIEW,
            },
          },
        ],
      },
      // {
      //   path: 'dashboard',
      //   canActivateChild: [RoleGuardService],
      //   data: {
      //     code: FunctionCode.DASHBOARD_CUSTOMER,
      //   },
      //   children: [
      //     {
      //       path: '',
      //       component: CustomerDashboardComponent,
      //       data: {
      //         scope: Scopes.VIEW,
      //       },
      //     },
      //   ],
      // },
      {
        path: 'operation-division',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.CUSTOMER_360_OPERATION_BLOCK_MANAGER,
        },
        children: [
          {
            path: '',
            component: OperationDivisionManagementComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
          {
            path: 'assign',
            component: OperationDivisionImportComponent,
            data: {
              scope: Scopes.ASSIGN_KVH,
              isAssign: true,
            },
          },
          {
            path: 'unassign',
            component: OperationDivisionImportComponent,
            data: {
              scope: Scopes.UNASSIGN_KVH,
              isAssign: false,
            },
          },
        ],
      },
      {
        path: 'customer-assignment',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.CUSTOMER_360_ASSIGNMENT,
        },
        children: [
          {
            path: '',
            component: CustomerAssignmentComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
          {
            path: 'import',
            component: CustomerAssignmentImportComponent,
            // data: {
            //   scope: Scopes.IMPORT,
            // },
          },
        ],
      },
      {
        path: 'customer-assignment-approve',
        component: CustomerAssignmentApproveComponent,
        canActivate: [RoleGuardService],
        data: {
          code: FunctionCode.CUSTOMER_360_ASSIGNMENT_APPROVED,
        },
      },
      {
        path: 'confirm-assign',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.CUSTOMER_360_CONFIRM_ASSIGN,
        },
        children: [
          {
            path: '',
            component: ConfirmAssignComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
        ],
      },
      {
        path: 'assign-ld-rm',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.CUSTOMER_360_ASSIGN_LD_RM,
        },
        children: [
          {
            path: '',
            component: AssignLdRmComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
        ],
      },
      {
        path: 'assign-by-product',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.CUSTOMER_360_ASSIGN_BY_PRODUCT,
        },
        children: [
          {
            path: '',
            component: AssignByProductComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
        ],
      },
      {
        path: 'revenue-sharing',
        children: [
          {
            path: '',
            canActivate: [RoleGuardService],
            data: {
              code: FunctionCode.REVENUE_SHARE,
              // scope: Scopes.VIEW,
            },
            component: ListRevenueSharingComponent,
          },
          {
            path: 'create/:code',
            component: RevenueSharingCreateComponent,
            data: {
              type: ScreenType.Create,
            },
          },
          {
            path: 'edit/:id',
            component: RevenueSharingCreateComponent,
            canActivate: [RoleGuardService],
            data: {
              type: ScreenType.Detail,
              // scope: Scopes.UPDATE,
              code: FunctionCode.REVENUE_SHARE,
            },
          },

          {
            path: 'approve/:id',
            component: RevenueSharingApproveComponent,
            canActivate: [RoleGuardService],
            data: {
              // scope: Scopes.APPROVE,
              code: FunctionCode.REVENUE_SHARE,
            },
          },
        ],
      },
      {
        path: 'assign-customer-by-rm-tttm',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.ASSIGN_CUSTOMER_BY_RM_TTTM,
        },
        children: [
          {
            path: '',
            component: AssignCustomerByRmTttmComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
        ],
      },
      {
        path: 'customer-warning-birthday',
        component: CustomerBirthdayWarningComponent,
        data: {
          scope: Scopes.VIEW,
          code: FunctionCode.BIRTHDAY_WARNING,
        }
      },
      {
        path: 'customer-management',
        canActivateChild: [],
        data: { code: FunctionCode.CUSTOMER_MANAGEMENT },
        children: [
          {
            path: '',
            component: CustomerManagementComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
          {
            path: 'detail/:id',
            component: CustomerDetailComponent,
            data: {
              scope: Scopes.UPDATE,
            },
          },
        ],
      },

    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Customer360RoutingModule {}
