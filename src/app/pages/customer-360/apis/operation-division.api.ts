import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class OperationDivisionApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_customer}/operation-block-management`);
  }

  findAll(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/operation-block-management/findAll`, params);
  }

  countAll(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/operation-block-management/countAll`, params, {
      responseType: 'text',
    });
  }

  import(data, params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/operation-block-management/import`, data, { params });
  }

  exportFile(params): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_customer}/operation-block-management/exportOperationDivisions`,
      params,
      { responseType: 'text' }
    );
  }

  searchAssign(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer}/operation-block-management/assign`, { params });
  }

  searchUnAssign(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer}/operation-block-management/unAssign`, { params });
  }

  writeData(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/operation-block-management/writeData`, data);
  }

  checkFileImport(fileId: string): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_customer}/operation-block-management/checkProcessImport?fileId=${fileId}`
    );
  }

  createFileExport(params, type): Observable<any> {
    const uri = `${environment.url_endpoint_customer}/operation-block-management/${
      type ? 'exports-assign' : 'exports-un-assign'
    }`;
    return this.http.get(uri, {
      params,
      responseType: 'text',
    });
  }
}
