import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend } from '@angular/common/http';

import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class RevenueShareApi {
  constructor(private http: HttpClient) {}

  createRevenueShare(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/shareRevenue`, params);
  }

  detailRevenueShare(id): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/shareRevenue/${id}`);
  }

  editRevenueShare(id, param): Observable<any> {
    return this.http.put(`${environment.url_endpoint_customer_sme}/shareRevenue/${id}`, param);
  }

  approveRevenueShare(isApprove, param): Observable<any> {
    return this.http.put(`${environment.url_endpoint_customer_sme}/shareRevenue/approve?isApprove=${isApprove}`, param);
  }

  endRevenueShare(id, params): Observable<any> {
    return this.http.put(`${environment.url_endpoint_customer_sme}/shareRevenue/end/${id}`, params);
  }

  searchRevenueShare(param, pageNumber, pageSize): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_customer_sme}/shareRevenue/search?page=${pageNumber}&size=${pageSize}`,
      param
    );
  }

  approveAllRevenueShare(param): Observable<any> {
    return this.http.put(`${environment.url_endpoint_customer_sme}/shareRevenue/quickApprove`, param);
  }

  excelRevenueShare(param): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/shareRevenue/export`, param, {
      responseType: 'text',
    });
  }

  checkCutomerShare(id) {
    return this.http.get(
      `${environment.url_endpoint_customer_sme}/shareRevenue/existByCustomerCode?customerCode=${id}`
    );
  }
}
