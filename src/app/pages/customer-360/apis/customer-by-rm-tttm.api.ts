import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpWrapper } from 'src/app/core/apis/http-wapper';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CustomerByRmTttmService extends HttpWrapper {

  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_sale}/sale-management/rm-tttm`);
  }

  getTemplate(): Observable<string> {
    return this.http.get(`${this.baseURL}/template`, { responseType: 'text' });
  }

  import(data): Observable<any> {
    return this.post('import', data);
  }

  write(data): Observable<any> {
    return this.post('save',data);
  }

  checkFileImport(fileId: string): Observable<any> {
    return this.get(`checkProcessImport?fileId=${fileId}`);
  }

  search(params): Observable<any> {
    return this.get(`findDataImportAssign`, params);
  }
}
