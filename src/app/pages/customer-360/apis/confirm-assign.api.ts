import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ConfirmAssignApi {
  private baseUrl = `${environment.url_endpoint_customer}/assignment-confirm`;
  constructor(private http: HttpClient) { }

  findAll(params): Observable<any> {
    return this.http.post(`${this.baseUrl}/findAll`, params);
  }

  getRmAssign(code: string, branchCode: string, businessDate: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/getRMAssignNew?customerCode=${code}&branchCode=${branchCode}&businessDate=${businessDate}`);
  }

  approve(data): Observable<any> {
    return this.http.post(`${this.baseUrl}/approve`, data);
  }

  exportFile(params): Observable<any> {
    return this.http.post(`${this.baseUrl}/export`, params, { responseType: 'text' });
  }
}
