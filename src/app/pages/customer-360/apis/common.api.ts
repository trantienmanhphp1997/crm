import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CommonApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_category}`);
  }
}
