import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend } from '@angular/common/http';

import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CustomerApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_rm_relationship}/graph-relationship/customers`);
  }

  getDataWarningBirthday(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_admin}/notify/list-customer-birthday`, params);
  }

  searchCustomerAssignRMCredit(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/customers-assignment-rm-credit/search`, params);
  }

  countCustomerAssignRMCredit(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/customers-assignment-rm-credit/count`, params);
  }

  exportCustomerAssignRMCredit(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/customers-assignment-rm-credit/export`, params, {
      responseType: 'text',
    });
  }

  search(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/customers/findAllFetch`, params);
  }

  count(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/customers/findAllCount`, params);
  }

  getRmManagerByCustomerCode(code, branchCode?): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_customer}/customers/rmManagement?customerCode=${code}&branchCode=${branchCode || ''}`,
      {
        responseType: 'text',
      }
    );
  }

  getSegmentDataChart(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/dashboard/phankhuc`, params);
  }

  getPrivatePriorityDataChart(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/dashboard/privatepriority`, params);
  }

  getProductUsedDataChart(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/dashboard/spdv`, params);
  }

  getStatusDataChart(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/dashboard/trangthai`, params);
  }

  getBScoreDataChart(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/dashboard/bscore`, params);
  }

  getAppActiveDataChart(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_report}/dashboard/app-summary-chart`, params);
  }

  getTOIDataChart(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/dashboard/toi`, params);
  }

  getProductByCustomerCode(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/products/customerproduct`, params);
  }

  getProductCountByCustomerCode(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/products/customerproductcount`, params);
  }

  getMobilizationProductChart(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/products/mobilizationchart`, data);
  }

  getCreditProductChart(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/products/creditchart`, data);
  }

  getProductDetail(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer}/products/product-detail`, {params});
  }

  getProductcomprodChart(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/products/comprodchart`, data);
  }

  getCreditCardChart(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/products/cardchart`, data);
  }

  createFile(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/customers/export`, data, {responseType: 'text'});
  }

  createFileSME(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/customers/export`, data, {responseType: 'text'});
  }

  getDigitalChart(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/products/nhschart`, data);
  }

  getCustomerById(id): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer}/customers/${id}`);
  }

  searchSme(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/customers/findAllFetch`, params);
  }

  countSme(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/customers/findAllCount`, params);
  }

  searchCustomerOnboarding(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/customers/find-customer-onboarding-biz`, data);
  }

  getCountCustomerOnboarding(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/customers/count-customer-onboarding-biz`, data);
  }

  getCreditInformation(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/credit-information`, {params});
  }

  getCreditInformation2(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/credit-information2`, {params});
  }

  // Product SME
  getProductGroupTypeSme(code): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/producttype/product-group-type?blockCode=${code}`);
  }

  getProductGroupCustomerSme(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/products/product-group-customer`, params);
  }

  getCollateralInformation(code: string): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_customer_sme}/customers/collateral-information?identifiedNumber=${code}`
    );
  }

  getBidding(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/bidding/info`, {params});
  }

  getProductDetailSme(customerCode, contractNumber, type): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_customer_sme}/products/product-detail-common?customerCode=${customerCode}&contractNumber=${contractNumber}&type=${type}`
    );
  }

  getOppProductRule(customerDivision): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_category}/oppProductRule/product-type?divisionCode=${customerDivision}`
    );
  }

  searchSmeSale(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sale-management`, params);
  }

  getRMSale(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/employees/findAllRmSale`, params);
  }

  assignRMSale(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sale-management/assign`, params);
  }

  createFileSale(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sale-management/export`, data, {responseType: 'text'});
  }

  createFileSaleNew(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sale-management/export-new`, data, {responseType: 'text'});
  }

  insuranceLife(params: any): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_customer}/customers/insurance-life-web`,
      params
    );
  }

  excelQueryMBAL(param): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/customers/insurance-life-excel`, param, {
      responseType: 'text',
    });
  }

  excelDetailQueryMBAL(param): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/customers/insurance-life-detail-excel`, param, {
      responseType: 'text',
    });
  }

  exportFileCustomerOnboarding(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/customers/exportCustomerOB`, data, {responseType: 'text'});
  }

  exportProductListTypeSme(groupCode, data): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_customer_sme}/products/export?groupCode=${groupCode}`, data, {responseType: 'text'}
    );
  }

  getMoneyTransfer(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/rpt-fi/search`, body);
  }

  getDetailMoneyTransfer(customerCode, type): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/rpt-fi/detail?customerCode=${customerCode}&type=${type}`);
  }

  exportExcelMoneyTransfer(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/rpt-fi/export`, body, {responseType: 'text'});

  }


  getImportAndExportSales(taxCode: string, year?: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/get-exim-turnover-amt?taxCode=${taxCode}${year ? '&year=' + year : ''}`);
  }

  getAttentionOwed(taxCode: string, year?: any): Observable<any> {
    // http://10.1.27.43:10186/crm19-customer-org/customers/get-debt-caution?taxCode=3702531173&year=2022
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/get-debt-caution?taxCode=${taxCode}${year ? '&year=' + year : ''}`);
  }

  getEximTurnoverTtqt(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/get-exim-turnover-ttqt`, {
      params: params
    });
  }

  getEximMiningRate(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/get-exim-mining-rate`, {
      params: params
    });
  }

  getDuNoKHCN(params): Observable<any> {
    // idCard
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/get-duno-KHCN`, {
      params: params
    });
  }

  getTotalCredit(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/get-total-credit-card-debt`, {
      params: params
    });
  }

  getStatusActiveMsCore(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/get-status-customer`, {
      params: params
    });
  }

  checkCustomerPermission(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer}/customers/check-customer-permission`, {
      params: params
    });
  }
  checkCustomerAssignRm(customerCode,taxCode): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers-assignment/check-customer-assigned-rm?customerCode=${customerCode}&taxCode=${taxCode}`);
  }

}

@Injectable({
  providedIn: 'root',
})
export class CustomerDetailApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_customer}/customers`);
  }

  getSegmentByCustomerCode(code: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer}/customers/segment/${code}`, {
      responseType: 'text',
    });
  }
}

@Injectable({
  providedIn: 'root',
})
export class CustomerDetailSmeApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_customer_sme}/customers`);
  }

  getSegmentByCustomerCode(code: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/segment/${code}`, {
      responseType: 'text',
    });
  }

  getReportFinanceByCustomerCode(code: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer}/customers/finance-report?customerCode=${code}`);
  }

  getOpportunityOfCustomer(page, size, customerCode): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_sale}/opportunities_onboarding/searchByCustCode?page=${page}&size=${size}&customerCode=${customerCode}`,
      {}
    );
  }

  getDataEarlyWarningModel(code: string): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_customer_sme}/customers/get-data-customer-map?customerCode=${code}`
    );
  }

  getDataCreditRatings(code: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/creditRatings?customerCode=${code}`);
  }

  exportFileCreditRatings(code: string): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_customer_sme}/customers/exportCreditRatings?customerCode=${code}`,
      {
        responseType: 'text',
      }
    );
  }

  exportFileCustomerExploitation(code: string): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_customer_sme}/customers/export-mining-situation?customerCode=${code}`,
      {},
      { responseType: 'text' }
    );
  }

  exportFileBusinessSituation(listDate: string, customerCode: string, branchCode: string): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_customer_sme}/customers/export-business-situation?listDate=${listDate}&customerCode=${customerCode}&branchCode=${branchCode}`,
      {},
      {responseType: 'text' }
    );
  }

  getClassification(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/classification`, {
      params,
      responseType: 'text',
    });
  }
  getToi12MKDL(code: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/get-toi-bq-kdl?customerCode=${code}`);
  }

  // Cam kết ngoại bảng
  getOffBalance(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/get-off-balance`, {
      params: params
    });
  }

  getCIC(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/get-cic`, {
      params: params
    });
  }

  getRwa(params){
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/get-rwa`, {
      params: params
    });
  }

  getDataCustomerRelation(listDate: string, customerCode: string, branchCode: string): Observable<any>{
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/get-customer-relation?listDate=${listDate}&customerCode=${customerCode}&branchCode=${branchCode}`);
  }

  getDttsrr(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/get-dttsrr`, {
      params: params
    });
  }
  getJourneyExploitCustomer(customerCode: string): Observable<any>{
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/get-journey-exploit-customer?customerCode=${customerCode}`);
  }

  exportJourneyExploitCustomer(code: string): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_customer_sme}/customers/export-journey-exploit-customer?customerCode=${code}`,
      { responseType: 'text' }
    );
  }
}
