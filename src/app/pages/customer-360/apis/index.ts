import { CustomerApi, CustomerDetailApi } from './customer.api';
import { CommonApi } from './common.api';
import { OperationDivisionApi } from './operation-division.api';
import { CustomerAssignmentApi } from './customer-assignment.api';
import { RmApi } from './rm.api';
import { DashboardApi } from './dashboard.api';
import { ConfirmAssignApi } from './confirm-assign.api';
import { RevenueShareApi } from './revenue-share.api';

export { CustomerApi, CustomerDetailApi } from './customer.api';
export { CommonApi } from './common.api';
export { OperationDivisionApi } from './operation-division.api';
export { CustomerAssignmentApi } from './customer-assignment.api';
export { RmApi } from './rm.api';
export { DashboardApi } from './dashboard.api';

export const APIS = [
  CustomerApi,
  CustomerDetailApi,
  CommonApi,
  OperationDivisionApi,
  CustomerAssignmentApi,
  RmApi,
  DashboardApi,
  ConfirmAssignApi,
  RevenueShareApi,
];
