import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';
import { Division } from 'src/app/core/utils/common-constants';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root',
})
export class CustomerAssignmentApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_customer}/customers-assignment`);
  }

  assignOtherIndiv(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/customers-assignment`, data);
  }

  assignIndiv(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/customers-assignment`, data);
  }

  check8FirstDayInMonth(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/holidays/check-8first-day`);
  }

  checkRmIsKVHOrIsKHCN(hrsCode): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/check-rm-title-group-block/${hrsCode}`);
  }

  searchPendingApproval(params): Observable<any> {
    return this.http.post(
      `${
        _.isEmpty(params.blockCode) || params.blockCode === Division.INDIV
          ? environment.url_endpoint_customer
          : environment.url_endpoint_customer_sme
      }/customers-assignment/findAll`,
      params
    );
  }

  searchHistory(params, type: boolean): Observable<any> {
    return this.http.get(
      `${type ? environment.url_endpoint_customer : environment.url_endpoint_customer_sme}/customers-assignment`,
      { params }
    );
  }

  searchHistoryCredit(params, type: boolean): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_customer}/customers-assignment-rm-credit/assign-credit-history`,
      { params }
    );
  }

  searchHistorySale(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/sale-management/${params.customerCode}`, { params });
  }

  approveCustomer(data): Observable<any> {
    return this.http.post(
      `${
        data.blockCode === Division.INDIV ? environment.url_endpoint_customer : environment.url_endpoint_customer_sme
      }/customers-assignment/approve`,
      data,
      {
        responseType: 'text',
      }
    );
  }

  refuseCustomer(data): Observable<any> {
    return this.http.post(
      `${
        data.blockCode === Division.INDIV ? environment.url_endpoint_customer : environment.url_endpoint_customer_sme
      }/customers-assignment/refuse`,
      data,
      {
        responseType: 'text',
      }
    );
  }

  approveOrRefuseAll(data): Observable<any> {
    return this.http.post(
      `${
        data.blockCode === Division.INDIV ? environment.url_endpoint_customer : environment.url_endpoint_customer_sme
      }/customers-assignment/acceptAll`,
      data,
      {
        responseType: 'text',
      }
    );
  }

  checkAcceptAll(data): Observable<any> {
    return this.http.post<boolean>(`${environment.url_endpoint_customer}/customers-assignment/checkAcceptAll`, data);
  }

  checkFileApproveOfRefuse(fileId: string): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_customer}/customers-assignment/checkHandleAcceptAll?requestId=${fileId}`
    );
  }

  getDataApproveOfRefuseError(fileId: string): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_customer}/customers-assignment/getDataAcceptAll?requestId=${fileId}`
    );
  }

  exportFileErrorApprove(fileId: string): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_customer}/customers-assignment/export-err-data-assign?requestId=${fileId}`,
      { responseType: 'text' }
    );
  }

  import(data, params): Observable<any> {
    return this.http.post(
      `${
        params.divisionCode === Division.INDIV
          ? environment.url_endpoint_customer
          : environment.url_endpoint_customer_sme
      }/customers-assignment/import`,
      data,
      { params }
    );
  }

  importByProduct(data, params): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_customer}/customers-assignment-rm-credit/import`,
      data,
      { params }
    );
  }

  importByProductLD(data, params): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_customer}/customers-assignment-rm-credit/import-ld`,
      data,
      { params }
    );
  }

  search(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer}/customers-assignment/findDataImportAssign`, { params });
  }

  writeData(data, divisionCode: string): Observable<any> {
    return this.http.post(
      `${
        divisionCode === Division.INDIV ? environment.url_endpoint_customer : environment.url_endpoint_customer_sme
      }/customers-assignment/saveDataImportAssign`,
      data
    );
  }

  writeDataByProduct(data): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_customer}/customers-assignment-rm-credit/save-data-import-assign-rm`,
      data
    );
  }

  writeDataByProductLD(data): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_customer}/customers-assignment-rm-credit/save-data-import-assign-rm-ld`,
      data
    );
  }

  checkFileImport(fileId: string): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_customer}/customers-assignment/checkProcessImport?fileId=${fileId}`
    );
  }

  exportFile(params: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer}/customers-assignment/exportDataImportAssign`, {
      params,
      responseType: 'text',
    });
  }

  exportAssignRmHistory(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer}/customers-assignment/exportAssignRmHistory`, {
      params,
      responseType: 'text',
    });
  }

  updateActivityLog(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/customer-activity-log/save`, data);
  }

  saveNote(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/customers/save-note`, {}, { params });
  }

  exportAssignProductHistory(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer}/customers-assignment-rm-credit/export-assigned-product-history`, {
      params,
      responseType: 'text',
    });
  }

}
