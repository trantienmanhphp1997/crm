import { MatTabsModule } from '@angular/material/tabs';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { TranslateModule } from '@ngx-translate/core';
import { APIS } from './apis';
import { RESOLVERS } from './resolvers';
import { SERVICES } from './services';
import { RadioButtonModule } from 'primeng/radiobutton';

import {
  ActivityLogComponent,
} from './components';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxSelectModule } from 'ngx-select-ex';
import { RmModule } from '../rm/rm.module';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { NgxEchartsModule } from 'ngx-echarts';
import * as echarts from 'echarts';
import { GridsterModule } from 'angular-gridster2';
import { DynamicModule } from 'ng-dynamic-component';
import { AccordionModule } from 'primeng/accordion';
import { TabViewModule } from 'primeng/tabview';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { InputNumberModule } from 'primeng/inputnumber';
import { TableModule } from 'primeng/table';
import { MultiSelectModule } from 'primeng/multiselect';
import { MatMenuModule } from '@angular/material/menu';
import { MenuModule } from 'primeng/menu';

const COMPONENTS = [
  ActivityLogComponent
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [
    CommonModule,
    SharedModule,
    NgbModule,
    DragDropModule,
    ReactiveFormsModule,
    FormsModule,
    InfiniteScrollModule,
    TranslateModule,
    NgxDatatableModule,
    NgxSelectModule,
    RmModule,
    MatProgressBarModule,
    MatTabsModule,
    NgxEchartsModule.forRoot({
      echarts,
    }),
    GridsterModule,
    DynamicModule,
    AccordionModule,
    TabViewModule,
    ButtonModule,
    InputTextModule,
    DropdownModule,
    CalendarModule,
    RadioButtonModule,
    InputNumberModule,
    TableModule,
    MultiSelectModule,
    MatMenuModule,
    MenuModule,
  ],
  providers: [...APIS, ...RESOLVERS, ...SERVICES],
  exports: [...COMPONENTS],
  entryComponents: [...COMPONENTS],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class ActivityLogModule { }
