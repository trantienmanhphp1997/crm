import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { global } from '@angular/compiler/src/util';
import { CommonCategory, FunctionCode } from 'src/app/core/utils/common-constants';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ConfigurationTargetService } from '../../services/configuration-target.service';

@Component({
  templateUrl: './configuration-target.component.html',
  styleUrls: ['./configuration-target.component.scss'],
})
export class ConfigurationTargetComponent extends BaseComponent implements OnInit {
  constructor(injector: Injector, private service: ConfigurationTargetService) {
    super(injector);
    this.isLoading = true;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CONFIG_AP}`);
  }

  paramSearch = {
    page: 0,
    search: '',
    itemLevel: '1',
    size: _.get(global, 'userConfig.pageSize'),
  };
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: _.get(global, 'userConfig.pageSize'),
  };
  prevParams: any;
  listData = [];
  commonData = {
    listLevel: [],
    listUnit: [],
    listAttribute: [],
    listParent: [],
    listType: [],
  };

  ngOnInit() {
    this.commonData.listAttribute = [
      { code: '1', name: 'Tỷ lệ thuận' },
      { code: '-1', name: 'Tỷ lệ nghịch' },
    ];
    this.commonData.listLevel = [{ name: '1' }, { name: '2' }, { name: '3' }, { name: '4' }, { name: '5' }];
    const state = this.sessionService.getSessionData(FunctionCode.CONFIG_AP);
    if (state) {
      this.commonData = state;
      if (this.prop) {
        this.paramSearch = _.cloneDeep(this.prop);
      }
      this.search(true);
    } else {
      forkJoin([
        this.commonService.getCommonCategory(CommonCategory.AP_TARGET).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.AP_KPI_UNIT).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.AP_TARGET_LEVEL).pipe(catchError(() => of(undefined))),
      ]).subscribe(([listType, listUnit, listLevel]) => {
        this.commonData.listType = listType?.content || [];
        this.commonData.listUnit = listUnit?.content || [];
        this.commonData.listLevel = listLevel?.content || [];
        this.paramSearch.itemLevel = _.head(this.commonData.listLevel)?.code;
        this.sessionService.setSessionData(FunctionCode.CONFIG_AP, this.commonData);
        this.search(true);
      });
    }
  }

  search(isSearch?: boolean) {
    this.isLoading = true;

    if (isSearch) {
      this.paramSearch.page = 0;
    }
    let params = _.cloneDeep(this.paramSearch);
    this.service.search(params).subscribe(
      (data) => {
        this.listData = data?.content || [];
        this.listData.forEach((item) => {
          item.unitName = `${item.unit} - ${_.find(this.commonData.listUnit, (u) => u.code === item.unit)?.name || ''}`;
        });
        this.pageable = {
          size: this.paramSearch.size,
          totalElements: data?.totalElements,
          totalPages: data?.totalPages,
          currentPage: this.paramSearch.page,
        };
        this.isLoading = false;
        this.prevParams = params;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.page = pageInfo.offset;
    this.search(false);
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      this.router.navigate([this.router.url, 'detail', item.id], {
        state: { params: this.prevParams, data: item },
        skipLocationChange: true,
      });
    }
  }

  create() {
    this.router.navigate([this.router.url, 'create'], { state: this.prevParams, skipLocationChange: true });
  }

  update(item) {
    this.router.navigate([this.router.url, 'update', item.id], {
      state: { params: this.prevParams, data: item },
      skipLocationChange: true,
    });
  }

  delete(item) {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.service.delete(item.id).subscribe(
          () => {
            this.search();
            this.messageService.success(this.notificationMessage.success);
          },
          (e) => {
            this.isLoading = false;
            if (!_.isEmpty(e?.error?.description)) {
              this.messageService.error(e.error.description);
            } else {
              this.messageService.error(this.notificationMessage.error);
            }
          }
        );
      }
    });
  }
}
