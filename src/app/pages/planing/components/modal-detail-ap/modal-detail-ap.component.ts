import {
  Component,
  OnInit,
  Injector,
  ViewEncapsulation,
  HostBinding,
  Input,
  ElementRef,
  ViewChild,
} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ApprovedPlanService } from '../../services/approved-plan.service';
import { TreeNode } from 'primeng/api';
import { ExportExcelService } from 'src/app/core/services/export-excel.service';
import * as XLSX from 'xlsx';
import { TreeTable } from 'primeng/treetable';

@Component({
  selector: 'modal-detail-ap',
  templateUrl: './modal-detail-ap.component.html',
  styleUrls: ['./modal-detail-ap.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ModalDetailAPComponent extends BaseComponent implements OnInit {
  @HostBinding('class.list__rm-content') appRightContent = true;
  @Input() customerCode: string;
  @Input() isLead: boolean;
  @Input() rmCode: string;
  @ViewChild('tableTree') tableTree: TreeTable;

  dataRows: TreeNode[] = [];

  constructor(
    injector: Injector,
    private modalActive: NgbActiveModal,
    private approvedPlanService: ApprovedPlanService,
    private exportExcelService: ExportExcelService
  ) {
    super(injector);
  }
  dataFake = [];

  ngOnInit() {
    this.isLoading = true;
    this.approvedPlanService.infoDetailsCustomer(this.customerCode, this.isLead, this.rmCode).subscribe(
      (res) => {
        this.isLoading = false;
        this.dataRows = this.mapTreeNode(res);
      },
      (e) => {
        this.isLoading = false;
        this.messageService.error('E001');
      }
    );
  }

  mapTreeNode(listData: any[]) {
    _.forEach(listData, (item) => {
      item.data = item;
      item.expanded = true;
    });
    const treeNode = _.filter(listData, (item) => !item.itemParent);
    _.forEach(treeNode, (item) => {
      this.getChildrenNode(item, listData);
    });
    return treeNode;
  }

  getChildrenNode(itemParent: any, listData: any[]) {
    const children = _.filter(listData, (item) => itemParent.itemId === +item.itemParent);
    itemParent.children = children;
    if (children.length > 0) {
      _.forEach(itemParent.children, (item) => {
        this.getChildrenNode(item, listData);
      });
    }
  }

  closeModal() {
    this.modalActive.close(false);
  }

  exportToExcel() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.tableTree?.el?.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'Chi tiết AP.xlsx');
  }
}
