import { formatDate } from '@angular/common';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { Component, OnInit, Injector, AfterViewInit, ViewEncapsulation } from '@angular/core';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { TableColumn } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import { Division, FunctionCode, maxInt32, Scopes } from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as moment from 'moment';
import { catchError } from 'rxjs/operators';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { ChooseBranchesModalComponent } from 'src/app/pages/system/components/choose-branches-modal/choose-branches-modal.component';
import { RmModalComponent } from 'src/app/pages/rm/components/rm-modal/rm-modal.component';
import { RmApi } from 'src/app/pages/rm/apis';
import { ApprovedPlanService } from '../../services/approved-plan.service';
import { Utils } from 'src/app/core/utils/utils';
import { FileService } from 'src/app/core/services/file.service';

@Component({
  selector: 'app-warning-ap',
  templateUrl: './warning-ap.component.html',
  styleUrls: ['./warning-ap.component.scss'],
})
export class WarningAPComponent extends BaseComponent implements OnInit, AfterViewInit {
  commonData = {
    listDivision: [],
    listBranch: [],
    minMonth: new Date(moment().set('month', -1).toDate()),
    maxMonth: new Date(moment().set('month', 11).toDate()),
  };
  textSearch: string;
  paramsTable = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };
  formSearch = this.fb.group({
    division: [{ value: '', disabled: true }],
    reportBy: 'CN',
    dateWarning: [new Date()],
    groupRM: '',
    period: '1',
    monthPlan: '',
  });
  dataTerm = {
    branch: {
      pageable: { ...this.pageable },
      term: [],
    },
    rm: {
      pageable: { ...this.pageable },
      term: [],
    },
  };
  tableType: string;
  termData = [];
  listDataTable = [];
  columns: TableColumn[];
  isSearch = false;
  objFunctionCustomer: any;
  paramSearchDivison = {
    size: maxInt32,
    page: 0,
    name: '',
    code: '',
  };

  listGroupRM = [
    { code: true, name: 'Nhóm cán bộ quản lý' },
    { code: false, name: 'Nhóm RM' },
  ];

  childHeader = [];
  parentHeader = [];
  dataRows = [];
  firstHeader = 0;
  lastHeader = 0;
  isViewTable = false;
  countCol = 0;
  rmCodesByBranch = [];
  listMonth = [
    {
      code: '00',
      name: '6 tháng đầu năm',
    },
    {
      code: '13',
      name: '6 tháng cuối năm ',
    },
  ];

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private rmApi: RmApi,
    private approvedPlanService: ApprovedPlanService,
    private fileService: FileService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.objFunctionCustomer = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.tableType = this.formSearch.get('reportBy').value;
    this.columns = [
      {
        name: this.fields._branchCode,
        prop: 'code',
      },
      {
        name: this.fields._branchName,
        prop: 'name',
      },
    ];
    forkJoin([
      this.categoryService.getBranchesOfUser(this.objFunction.rsId, Scopes.VIEW).pipe(catchError(() => of(undefined))),
      this.categoryService.searchBlocksCategory(this.paramSearchDivison).pipe(catchError(() => of(undefined))),
    ]).subscribe(([branchesOfUser, listDivision]) => {
      this.commonData.listBranch = branchesOfUser || [];

      this.commonData.listDivision = listDivision.content?.map((item) => {
        if (Division.CIB === item.code) {
          return {
            code: item.code,
            name: item.code + ' - ' + item.name,
          };
        }
      });
      this.formSearch.controls.division.setValue(_.first(this.commonData.listDivision)?.code);
      if (_.isEmpty(branchesOfUser)) {
        this.commonData.listBranch?.push({ code: this.currUser.branch, name: this.currUser.branchName });
        _.remove(this.listGroupRM, (item) => item.code);
        this.formSearch.controls.reportBy.setValue('RM');
        this.textSearch = this.currUser.code;
        if (this.currUser.code) {
          this.add();
        }
      }
      this.formSearch.controls.groupRM.setValue(_.first(this.listGroupRM)?.code);
      this.isLoading = false;
    });
  }

  fixWidth(count) {
    return +count * 150 + `px`;
  }

  ngAfterViewInit() {
    this.formSearch.controls.reportBy.valueChanges.subscribe((value) => {
      if (this.tableType !== value) {
        this.paramsTable.page = 0;
        this.dataTerm[this.tableType] = {
          pageable: { ...this.pageable },
          term: [...this.termData],
        };
        this.textSearch = '';
        if (value === 'CN') {
          this.columns[0].name = this.fields._branchCode;
          this.columns[1].name = this.fields._branchName;
          this.termData = [...this.dataTerm.branch.term];
          this.pageable = { ...this.dataTerm.branch.pageable };
        } else if (value === 'RM') {
          this.columns[0].name = this.fields.rmCode;
          this.columns[1].name = this.fields.rmName;
          this.termData = [...this.dataTerm.rm.term];
          this.pageable = { ...this.dataTerm.rm.pageable };
        }
        this.tableType = value;
        this.mapData();
      }
    });

    this.formSearch.controls.division.valueChanges.subscribe(() => {
      this.removeList();
    });

    this.formSearch.controls.groupRM.valueChanges.subscribe(() => {
      this.termData = [];
      this.mapData();
    });

    this.formSearch.controls.period.valueChanges.subscribe((value) => {
      if (+value === 1) {
        this.formSearch.controls.dateWarning.setValue(new Date());
      } else if (+value === 2) {
        this.formSearch.controls.monthPlan.setValue(_.first(this.listMonth).code);
      }
      this.termData = [];
      this.mapData();
    });
  }

  search() {
    const reportBy = this.formSearch.get('reportBy').value;
    if (reportBy === 'CN') {
      const modal = this.modalService.open(ChooseBranchesModalComponent, { windowClass: 'tree__branches-modal' });
      modal.componentInstance.listBranchOld = _.map(this.termData, (x) => x.code) || [];
      modal.componentInstance.listBranch = this.commonData.listBranch;
      modal.result
        .then((res) => {
          if (res) {
            this.termData = res;
            const params = {
              crmIsActive: true,
              scope: Scopes.VIEW,
              rmBlock: 'CIB',
              rsId: this.objFunction?.rsId,
              branchCodes: this.termData.map((item) => item.code),
              page: 0,
              size: maxInt32,
              manager: this.formSearch.get('groupRM').value,
              rm: !this.formSearch.get('groupRM').value,
              isReportByRm: !this.formSearch.get('groupRM').value,
            };
            this.findRmWarning(false, params);
            this.mapData();
          }
        })
        .catch(() => {});
    } else if (reportBy === 'RM') {
      const formSearch = {
        crmIsActive: { value: 'true', disabled: true },
        rmBlock: {
          value: 'CIB',
          disabled: true,
        },
        rm: !this.formSearch.get('groupRM').value,
        isReportByRm: !this.formSearch.get('groupRM').value,
      };
      const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
      modal.componentInstance.dataSearch = formSearch;
      modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
      modal.componentInstance.isManager = this.formSearch.get('groupRM').value;
      // modal.componentInstance.isWarningReport = this.formSearch.controls.groupRM.value;
      modal.result
        .then((res) => {
          if (res) {
            const listRMSelect = res.listSelected?.map((item) => {
              return {
                hrsCode: item?.hrisEmployee?.employeeId,
                code: item?.t24Employee?.employeeCode,
                name: item?.hrisEmployee?.fullName,
              };
            });
            this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
            this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);
            this.mapData();
          }
        })
        .catch(() => {});
    }
  }

  add() {
    const reportBy = this.formSearch.get('reportBy').value;
    this.textSearch = _.trim(this.textSearch);
    if (this.textSearch.length === 0) {
      this.isSearch = false;
      return;
    }
    if (_.findIndex(this.termData, (i) => i.code?.toUpperCase() === this.textSearch?.toUpperCase()) === -1) {
      if (reportBy === 'CN') {
        const itemResult = _.find(
          this.commonData.listBranch,
          (item) => item.code?.toUpperCase() === this.textSearch?.toUpperCase()
        );
        if (itemResult) {
          this.termData?.push(itemResult);
          const params = {
            crmIsActive: true,
            scope: Scopes.VIEW,
            rmBlock: 'CIB',
            rsId: this.objFunction?.rsId,
            branchCodes: this.termData.map((item) => item.code),
            page: 0,
            size: maxInt32,
            manager: this.formSearch.get('groupRM').value,
            rm: !this.formSearch.get('groupRM').value,
            isReportByRm: !this.formSearch.get('groupRM').value,
          };
          this.findRmWarning(false, params);
          this.mapData();
        } else {
          this.messageService.warn(_.get(this.notificationMessage, 'branchValidationPermission'));
        }
      } else {
        const params = {
          // rm: true,
          // manager: true,
          crmIsActive: true,
          scope: Scopes.VIEW,
          rmBlock: 'CIB',
          rsId: this.objFunction?.rsId,
          employeeCode: this.textSearch,
          page: 0,
          size: maxInt32,
          manager: this.formSearch.get('groupRM').value,
          rm: !this.formSearch.get('groupRM').value,
          isReportByRm: !this.formSearch.get('groupRM').value,
        };
        this.findRmWarning(true, params);
      }
    } else {
      const messageReport = this.formSearch.get('reportBy').value?.toString()?.toLowerCase() + 'IsExist';
      this.messageService.warn(_.get(this.notificationMessage, messageReport));
      this.isSearch = false;
      return;
    }
  }

  setPage(pageInfo) {
    if (!this.isLoading) {
      this.paramsTable.page = pageInfo.offset;
      this.mapData();
    }
  }

  mapData() {
    const total = this.termData.length;
    this.pageable.totalElements = total;
    this.pageable.totalPages = Math.floor(total / this.pageable.size);
    this.pageable.currentPage = this.paramsTable.page;
    const start = this.paramsTable.page * this.paramsTable.size;
    this.listDataTable = this.termData?.slice(start, start + this.paramsTable.size);
  }

  removeRecord(item) {
    _.remove(this.termData, (i) => i.code === item.code);
    _.remove(this.listDataTable, (i) => i.code === item.code);
    if (this.listDataTable.length === 0 && this.pageable.currentPage > 0) {
      this.paramsTable.page -= 1;
    }
    this.listDataTable = [...this.listDataTable];
    this.mapData();
  }

  viewApWarning() {
    let dateWarning = formatDate(this.formSearch.controls.dateWarning.value, 'MM-yyyy', 'en');
    const monthYear = _.split(dateWarning, '-');

    if (this.isLoading || this.isSearch) {
      return;
    }

    if (_.isEmpty(this.termData) && this.formSearch.get('reportBy').value === 'CN') {
      this.messageService.warn(_.get(this.notificationMessage, 'branchValidation'));
      return;
    } else if (_.isEmpty(this.termData) && this.formSearch.get('reportBy').value === 'RM') {
      this.messageService.warn(_.get(this.notificationMessage, 'rmValidation'));
      return;
    }
    const period = this.formSearch.get('period').value;
    const dataTable = this.termData?.map((i) => i.code);
    const params = {
      rmCodes: this.formSearch.get('reportBy').value === 'RM' ? dataTable : this.rmCodesByBranch,
      branchCodes: this.formSearch.get('reportBy').value === 'CN' ? dataTable : [],
      month: period === '1' ? monthYear[0] : this.formSearch.get('monthPlan').value.toString(),
      year: period === '1' ? monthYear[1] : moment().year().toString(),
      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW,
      page: 0,
      size: maxInt32,
    };

    this.isLoading = true;
    this.approvedPlanService.apWarning(params).subscribe(
      (res) => {
        if (!_.isEmpty(res)) {
          this.isLoading = false;
          this.isViewTable = true;

          this.childHeader = res?.childHeader?.cells || [];
          this.countCol = this.childHeader?.length;
          this.parentHeader = res?.parentHeader?.cells || [];
          this.firstHeader = this.childHeader?.filter(
            (item) => item.code === _.first(this.parentHeader)?.code
          )[0]?.column;
          this.lastHeader = _.last(
            this.childHeader.filter((item) => item?.code === _.last(this.parentHeader)?.code)
          )?.column;

          this.parentHeader = this.parentHeader?.map((item) => {
            let number = item.code ? this.childHeader.filter((i) => i.code === item.code)?.length : 0;
            return { ...item, countCode: number };
          });
          this.dataRows = res?.rows.content;
        }
      },
      (e) => {
        this.isViewTable = false;
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.E001);
      }
    );
  }

  removeList() {
    this.termData = [];
    this.paramsTable.page = 0;
    this.mapData();
  }

  downloadFile() {
    this.isLoading = true;
    let dateWarning = formatDate(this.formSearch.controls.dateWarning.value, 'MM-yyyy', 'en');
    const monthYear = _.split(dateWarning, '-');
    const dataTable = this.termData?.map((i) => i.code);
    const period = this.formSearch.get('period').value;
    const params = {
      rmCodes: this.formSearch.get('reportBy').value === 'RM' ? dataTable : this.rmCodesByBranch,
      branchCodes: this.formSearch.get('reportBy').value === 'CN' ? dataTable : [],
      month: period === '1' ? monthYear[0] : this.formSearch.get('monthPlan').value.toString(),
      year: period === '1' ? monthYear[1] : moment().year().toString(),
      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW,
      page: 0,
      size: maxInt32,
    };
    this.approvedPlanService.exportApWarning(params).subscribe((res) => {
      if (Utils.isStringNotEmpty(res)) {
        this.dowloadFile(res);
      } else {
        this.messageService.error(this.notificationMessage.fileDowloadExist);
        this.isLoading = false;
      }
    });
  }

  dowloadFile(fieId: string) {
    let nameFile = 'Cảnh báo AP.xlsx';
    this.fileService.downloadFile(fieId, nameFile).subscribe(
      (res) => {
        this.isLoading = false;
        if (!res) {
          this.isLoading = false;
          this.messageService.error(this.notificationMessage.error);
        }
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  findRmWarning(isSearch?, paramSearch?) {
    this.isSearch = true;
    // let apiRm: Observable<any>;
    // if (this.formSearch.controls.groupRM.value) {
    //   apiRm = this.rmApi.getRMGroupCbql(paramSearch);
    // } else {
    //   apiRm = this.rmApi.post('findAll', paramSearch);
    // }
    this.rmApi.post('findAll', paramSearch).subscribe(
      (data) => {
        if (_.isEmpty(isSearch)) {
          const itemResult = _.find(
            _.get(data, 'content'),
            (item) => item?.t24Employee?.employeeCode?.toUpperCase() === this.textSearch?.toUpperCase()
          );
          if (itemResult) {
            this.termData?.push({
              code: itemResult?.t24Employee?.employeeCode,
              name: itemResult?.hrisEmployee?.fullName,
              hrsCode: itemResult?.hrisEmployee?.employeeId,
            });
            this.mapData();
          } else {
            const itemResult = _.get(data, 'content');
            this.rmCodesByBranch = itemResult.map((item) => item?.t24Employee?.employeeCode);
          }
        } else {
          this.messageService.warn(_.get(this.notificationMessage, 'rmNotExistOrNotBranh'));
        }
        this.isSearch = false;
      },
      (e) => {
        this.messageService.error(this.notificationMessage.E001);
        this.isLoading = false;
        this.isSearch = false;
      }
    );
  }
}
