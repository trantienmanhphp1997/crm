import { ConfigurationTargetComponent } from './configuration-target/configuration-target.component';
import { ConfigurationTargetActionComponent } from './configuration-target-action/configuration-target-action.component';
import { PlanningListComponent } from './planning-list/planning-list.component';
import { PlanningImportComponent } from './planning-import/planning-import.component';
import { ApprovedPlanListComponent } from './approved-plan-list/approved-plan-list.component';
import { ApprovedPlanDetailComponent } from './approved-plan-detail/approved-plan-detail.component';
import { ModalDetailAPComponent } from './modal-detail-ap/modal-detail-ap.component';
import { WarningAPComponent } from './warning-ap/warning-ap.component';

export const COMPONENTS = [
  ConfigurationTargetComponent,
  ConfigurationTargetActionComponent,
  PlanningListComponent,
  PlanningImportComponent,
  ApprovedPlanListComponent,
  ApprovedPlanDetailComponent,
  ModalDetailAPComponent,
  WarningAPComponent,
];

export * from './configuration-target/configuration-target.component';
export * from './configuration-target-action/configuration-target-action.component';
export * from './planning-list/planning-list.component';
export * from './planning-import/planning-import.component';
export * from './approved-plan-list/approved-plan-list.component';
export * from './approved-plan-detail/approved-plan-detail.component';
export * from './modal-detail-ap/modal-detail-ap.component';
export * from './warning-ap/warning-ap.component';
