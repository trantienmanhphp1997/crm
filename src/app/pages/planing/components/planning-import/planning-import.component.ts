import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { ConfirmType, maxInt32, typeExcel } from 'src/app/core/utils/common-constants';
import { Utils } from 'src/app/core/utils/utils';
import { FileService } from 'src/app/core/services/file.service';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { PlanningImportService } from '../../services/planning-import.service';
import { global } from '@angular/compiler/src/util';

@Component({
  selector: 'planning-import',
  templateUrl: './planning-import.component.html',
  styleUrls: ['./planning-import.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PlanningImportComponent extends BaseComponent implements OnInit {
  dataPlanExport: any;
  disableApproval: boolean;
  constructor(
    injector: Injector,
    private fileService: FileService,
    private planningImportService: PlanningImportService
  ) {
    super(injector);
    this.dataPlanExport = JSON.parse(this.route.snapshot.queryParams.dataPlan);
    this.disableApproval = this.route.snapshot.queryParams.disableApproval === 'true';
  }
  limit = global.userConfig.pageSize;
  params: any = {
    pageNumber: 0,
    pageSize: global?.userConfig?.pageSize,
  };

  files: any;
  isUpload: boolean;
  fileName: string;
  isFile: boolean;
  listDataError = [];
  listDivision = [];
  fileImport: File;
  pageable: Pageable;
  viewListError = false;
  isSave = false;
  listDataSuccess = [];
  viewListSuccess = false;
  isSaveSuccess = false;

  ngOnInit() {}

  handleFileInput(files) {
    if (files && files.length > 0) {
      if (files?.item(0)?.size > 10485760) {
        this.messageService.warn(this.notificationMessage.ECRM005);
        return;
      }
      if (!typeExcel.includes(files?.item(0)?.type)) {
        this.messageService.error(this.notificationMessage.CANNOT_READ_DATA_FROM_FILE);
        return;
      }
      this.isFile = true;
      this.fileImport = files.item(0);
      this.fileName = files.item(0).name;
    } else {
      this.isFile = false;
    }
  }

  clearFile() {
    this.isUpload = false;
    this.isFile = false;
    this.fileName = null;
    this.fileImport = null;
    this.files = null;
    this.listDataError = [];
    this.viewListError = false;
  }

  importFile(isSearch: boolean, isSave?: boolean) {
    if (this.isFile && (!this.isUpload || this.viewListSuccess)) {
      this.isUpload = true;
      this.isSave = isSave;
      this.isLoading = true;
      if (isSearch) {
        this.params.pageNumber = 0;
      }
      this.isSaveSuccess = false;
      const formData: FormData = new FormData();
      formData.append('file', this.fileImport);
      formData.append('dto', new Blob([JSON.stringify(this.dataPlanExport)], { type: 'application/json' }));
      formData.append('page', new Blob([JSON.stringify(this.params.pageNumber)], { type: 'application/json' }));
      formData.append('size', new Blob([JSON.stringify(this.params.pageSize)], { type: 'application/json' }));
      formData.append('isSave', new Blob([JSON.stringify(this.isSave)], { type: 'application/json' }));
      this.planningImportService.importApTemplate(formData).subscribe(
        (result) => {
          if (!_.isEmpty(result) && result?.content.length > 0) {
            this.viewListError = true;
            this.viewListSuccess = false;
            this.listDataError = result.content;
            this.pageable = {
              totalElements: result.totalElements,
              totalPages: result.totalPages,
              currentPage: result.number,
              size: this.limit,
            };
            this.isLoading = false;
          } else {
            this.listDataSuccess = [];
            this.viewListSuccess = true;
            this.viewListError = false;
            this.isLoading = false;
            this.messageService.success(this.isSave ? 'Ghi file AP thành công' : 'Import file AP thành công');
            this.listDataSuccess = [...this.dataPlanExport].map((item) => {
              return {
                customerName: item.customerName,
                customerCode: item.customerCode,
              };
            });
            const messageSave = `Bạn có muốn ghi file AP `;
            this.confirmService.openModal(ConfirmType.Confirm, messageSave).then((resConfirm) => {
              if (resConfirm) {
                this.writeFile();
              }
            });
          }
        },
        (e) => {
          this.isLoading = false;
          if (e.error.code) {
            this.messageService.error(e.error.description);
          } else {
            this.messageService.error(this.notificationMessage.E001);
          }
        }
      );
    }
  }

  writeFile() {
    this.isLoading = true;
    const formData: FormData = new FormData();
    formData.append('file', this.fileImport);
    formData.append('dto', new Blob([JSON.stringify(this.dataPlanExport)], { type: 'application/json' }));
    formData.append('page', new Blob([JSON.stringify(this.params.pageNumber)], { type: 'application/json' }));
    formData.append('size', new Blob([JSON.stringify(this.params.pageSize)], { type: 'application/json' }));
    formData.append('isSave', new Blob([JSON.stringify(true)], { type: 'application/json' }));
    this.planningImportService.importApTemplate(formData).subscribe(
      (res) => {
        if (!_.isEmpty(res) && res?.content.length > 0) {
          this.viewListError = true;
          this.viewListSuccess = false;
          this.listDataError = res.content;
          this.pageable = {
            totalElements: res.totalElements,
            totalPages: res.totalPages,
            currentPage: res.number,
            size: this.limit,
          };
          this.isLoading = false;
        } else {
          this.messageService.success('Ghi file AP thành công');
          this.isLoading = false;
          this.disableApproval = true;
          this.isSaveSuccess = true;
        }
      },
      (e) => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.E001);
      }
    );
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.pageNumber = pageInfo.offset;
    this.isUpload = false;
    this.writeFile();
  }

  downloadTemplate() {
    this.isLoading = true;

    this.planningImportService.exportApTemplate(this.dataPlanExport).subscribe((res) => {
      if (Utils.isStringNotEmpty(res)) {
        this.dowloadFile(res, false);
      } else {
        this.messageService.error(this.notificationMessage.fileDowloadExist);
        this.isLoading = false;
      }
    });
  }

  dowloadFile(fieId: string, isFileData) {
    let nameFile = isFileData ? 'data-import-ap-cib-template.xlsx' : 'import-ap-cib-template.xlsx';
    this.fileService.downloadFile(fieId, nameFile).subscribe(
      (res) => {
        this.isLoading = false;
        if (!res) {
          this.isLoading = false;
          this.messageService.error(this.notificationMessage.error);
        }
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  requestApprovalAp() {
    this.isLoading = true;
    if (this.isSaveSuccess) {
      _.forEach(this.dataPlanExport, (i) => {
        i.status = 2;
      });
    }
    this.planningImportService.requestApprovalAp(this.dataPlanExport).subscribe(
      (res) => {
        this.isLoading = false;
        this.messageService.success(this.notificationMessage.success);
        this.back();
      },
      (e) => {
        this.isLoading = false;
        if (e.error.code) {
          this.messageService.error(e.error.description);
        } else {
          this.messageService.error(this.notificationMessage.E001);
        }
      }
    );
  }

  exportAPDataImported() {
    this.isLoading = true;
    this.planningImportService.exportAPDataImported(this.dataPlanExport).subscribe((res) => {
      if (Utils.isStringNotEmpty(res)) {
        this.dowloadFile(res, true);
      } else {
        this.messageService.error(this.notificationMessage.fileDowloadExist);
        this.isLoading = false;
      }
    });
  }
}
