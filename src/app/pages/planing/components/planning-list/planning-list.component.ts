import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { global } from '@angular/compiler/src/util';
import { CommonCategory, Scopes, maxInt32, FunctionCode, RoleUser } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { forkJoin, of } from 'rxjs';
import _ from 'lodash';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { Utils } from 'src/app/core/utils/utils';
import { RmApi } from 'src/app/pages/customer-360/apis';
import { RmProfileService } from 'src/app/core/services/rm-profile.service';
import * as moment from 'moment';
import { PlanningImportService } from '../../services/planning-import.service';

@Component({
  selector: 'planning-list-component',
  templateUrl: './planning-list.component.html',
  styleUrls: ['./planning-list.component.scss'],
})
export class PlanningListComponent extends BaseComponent implements OnInit {
  isLoading = false;
  @ViewChild('table') table: DatatableComponent;
  limit = global.userConfig.pageSize;
  pageable: Pageable;
  params: any = {
    pageNumber: 0,
    pageSize: global?.userConfig?.pageSize,
    rsId: '',
    scope: Scopes.VIEW,
  };
  propDetail: any;

  objFunctionCustomer: any;
  objFunctionRM: any;
  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private rmApi: RmApi,
    private rmProfileService: RmProfileService,
    private planningImportService: PlanningImportService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.ACCOUNT_PLANNING}`);
    this.objFunctionCustomer = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.objFunctionRM = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
  }

  formSearch = this.fb.group({
    customerCode: [''],
    customerName: [''],
    registrationNumber: [''],
    division: [''],
    rsId: '',
    scope: Scopes.VIEW,
    branchCode: [''],
    typeCustomer: [''],
    rmCode: [''],
    statusAp: [''],
    yearAp: [{ value: new Date().getFullYear(), disabled: true }],
  });

  listDivision = [];
  listData = [];
  listTypeCustomer = [
    { code: 1, displayName: 'Khách hàng 360' },
    { code: 0, displayName: 'Khách hàng tiềm năng' },
  ];

  listRmManager = [];
  listBranches = [];
  listApStatus = [];

  allBranch = true;
  checkAll = false;
  listSelected = [];
  countCheckedOnPage = 0;
  isRoleAdmin = false;
  countCustomer: any;
  disabledSearch = false;

  ngOnInit() {
    this.isLoading = true;
    forkJoin([
      this.rmProfileService.getRolesByUser(this.currUser.username).pipe(catchError(() => of(undefined))),
      this.categoryService
        .getBranchesOfUser(this.objFunctionRM?.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined))),
      this.categoryService.getCommonCategory(CommonCategory.AP_STATUS).pipe(catchError(() => of(undefined))),
      this.categoryService.getCommonCategory(CommonCategory.MAX_IMPORT_AP).pipe(catchError(() => of(undefined))),
    ]).subscribe(([listRoles, listBranches, listApStatus, maxCustomer]) => {
      this.countCustomer = _.get(maxCustomer, 'content[0].value', 0);
      this.listApStatus = listApStatus.content?.map((item) => {
        return { code: +item.code, displayName: item.name };
      });
      this.formSearch.controls.statusAp.setValue(1);

      this.listBranches = listBranches?.map((item) => {
        return { code: item.code, displayName: item.code + ' - ' + item.name };
      });

      if (!_.isEmpty(this.listBranches)) {
        this.listBranches?.unshift({ code: '', displayName: this.fields.all });
        this.isRoleAdmin = true;
      } else {
        this.listBranches.push({
          code: this.currUser.branch,
          displayName: this.currUser.branch + ' - ' + this.currUser.branchName,
        });
        this.formSearch.controls.branchCode.setValue(this.currUser.branch);
      }
      this.formSearch.controls.typeCustomer.setValue(1);

      this.getRmManager([], true);
    });
  }

  ngAfterViewInit() {
    this.formSearch.valueChanges.subscribe((value) => {
      if (!this.isLoading) {
        this.disabledSearch = false;
      }
    });

    this.formSearch.controls.branchCode.valueChanges.subscribe((value) => {
      this.getRmManager([value]);
    });
  }

  searchPlan(isSearch: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.params.pageNumber = 0;
    }

    const paramSearch = {
      rsId: this.objFunction?.rsId,
      scope: Scopes.VIEW,
      rmCode: this.formSearch.controls.rmCode.value,
      customerCode: this.formSearch.controls.customerCode.value,
      customerName: this.formSearch.controls.customerName.value,
      status: this.formSearch.controls.statusAp.value,
      year: this.formSearch.controls.yearAp.value,
      branchCodeList: this.formSearch.controls.branchCode.value ? [this.formSearch.controls.branchCode.value] : [],
      isLead: this.formSearch.controls.typeCustomer.value === 0,
      page: this.params.pageNumber,
      size: this.params.pageSize,
    };
    this.planningImportService.searchPlan(paramSearch).subscribe(
      (result) => {
        this.isLoading = false;
        this.listData =
          result.content.map((item) => {
            return {
              branchNamePlan: item.branchCode + ' - ' + item.branchName,
              statusText: this.listApStatus?.filter((i) => i.code === item.status)[0]?.displayName,
              ...item,
            };
          }) || [];
        this.countCheckedOnPage = 0;
        this.listData.forEach((item) => {
          if (
            this.listSelected.findIndex((itemChoose) => {
              return itemChoose.id === item.employeeAccountId;
            }) > -1
          ) {
            this.countCheckedOnPage += 1;
          }
        });
        this.checkAll = this.listData.length > 0 && this.countCheckedOnPage === this.listData.length;
        this.pageable = {
          totalElements: result.totalElements,
          totalPages: result.totalPages,
          currentPage: result.number,
          size: this.limit,
        };
      },
      (e) => {
        this.messageService.error(this.notificationMessage.E001);
        this.isLoading = false;
      }
    );
    this.disabledSearch = true;
  }

  getRmManager(listBranch, isSearch?) {
    this.formSearch.get('rmCode').disable();
    this.listRmManager = [];
    this.formSearch.controls.rmCode.setValue('');
    if (this.isRoleAdmin) {
      this.rmApi
        .post('findAll', {
          page: 0,
          size: maxInt32,
          crmIsActive: true,
          branchCodes: listBranch?.filter((code) => !Utils.isStringEmpty(code)),
          rmBlock: 'CIB',
          rsId: this.objFunctionRM?.rsId,
          scope: Scopes.VIEW,
        })
        .subscribe((res) => {
          const listRm: any[] =
            res?.content?.map((item) => {
              if (!_.isEmpty(item?.t24Employee?.employeeCode)) {
                return {
                  code: item?.t24Employee?.employeeCode || '',
                  displayName:
                    Utils.trimNullToEmpty(item?.t24Employee?.employeeCode) +
                    ' - ' +
                    Utils.trimNullToEmpty(item?.hrisEmployee?.fullName),
                };
              }
            }) || [];

          this.listRmManager = listRm;
          if (!_.isEmpty(this.listRmManager)) {
            this.listRmManager.unshift({ code: '', displayName: this.fields.all });
            this.formSearch.controls.rmCode.setValue('');
          }
          this.formSearch.get('rmCode').enable();
          if (isSearch) {
            this.searchPlan(true);
          }
        });
    } else {
      this.listRmManager.push({
        code: this.currUser.code,
        displayName: this.currUser.code + ' - ' + this.currUser.fullName,
      });
      this.formSearch.controls.rmCode.setValue(this.currUser.code);
      this.formSearch.get('rmCode').enable();
      if (isSearch) {
        this.searchPlan(true);
      }
    }
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.pageNumber = pageInfo.offset;
    this.searchPlan(false);
  }

  onCheckboxAllFn(isChecked) {
    this.checkAll = isChecked;
    if (!isChecked) {
      this.countCheckedOnPage = 0;
    } else {
      this.countCheckedOnPage = this.listData.length;
    }
    for (const item of this.listData) {
      if (!isChecked) {
        this.listSelected = this.listSelected.filter((itemChoose) => {
          return itemChoose.id !== item.employeeAccountId;
        });
      } else if (
        isChecked &&
        this.listSelected.findIndex((itemChoose) => itemChoose.id === item.employeeAccountId) === -1 &&
        item.status !== 3
      ) {
        this.listSelected.push({ id: item.employeeAccountId, ...item });
      }
    }
  }

  onCheckboxFn(item, isChecked) {
    if (isChecked) {
      this.listSelected.push({ id: item.employeeAccountId, ...item });
      this.countCheckedOnPage += 1;
    } else {
      this.listSelected = this.listSelected.filter((itemChoose) => {
        return itemChoose.id !== item.employeeAccountId;
      });
      this.countCheckedOnPage -= 1;
    }
    this.checkAll = this.countCheckedOnPage === this.listData.length;
  }

  isChecked(item) {
    return this.listSelected?.findIndex((itemSelected) => itemSelected.id === item.employeeAccountId) > -1;
  }

  importPlanning() {
    if (this.listSelected.length > this.countCustomer) {
      this.translate.get('notificationMessage.countCustomerPlan', { number: this.countCustomer }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    const paramExport = this.listSelected.map((item) => {
      return _.omit(item, ['id', 'branchNamePlan', 'statusText']);
    });

    this.router.navigate([this.router.url, 'import'], {
      skipLocationChange: true,
      queryParams: {
        dataPlan: JSON.stringify(paramExport),
        disableApproval: this.listSelected.length === this.listSelected.filter((item) => item.status === 2)?.length,
      },
    });
  }

  requestApprovalAp(dataPlan) {
    this.isLoading = true;
    dataPlan = _.omit(dataPlan, ['id', 'branchNamePlan', 'statusText']);
    this.planningImportService.requestApprovalAp([dataPlan]).subscribe(
      (res) => {
        this.isLoading = false;
        this.messageService.success(this.notificationMessage.success);
        this.searchPlan(true);
      },
      (e) => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }

  onActive(event) {
    const item = _.get(event, 'row');
    if (event.type === 'dblclick' && item.status === 2) {
      event.cellElement.blur();
      this.router.navigate([this.router.url, 'import'], {
        skipLocationChange: true,
        queryParams: {
          dataPlan: JSON.stringify([_.omit(item, ['id', 'branchNamePlan', 'statusText'])]),
          disableApproval: true,
        },
      });
    }
  }
}
