import { Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import {
  BRANCH_HO,
  ConfirmType,
  FunctionCode,
  LIST_STATUS_CUSTOMER_APPROVE,
  maxInt32,
  Scopes,
} from 'src/app/core/utils/common-constants';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import _ from 'lodash';
import * as moment from 'moment';
import { global } from '@angular/compiler/src/util';
import { ModalDetailAPComponent } from '../modal-detail-ap/modal-detail-ap.component';
import { ApprovedPlanService } from '../../services/approved-plan.service';

@Component({
  selector: 'approved-plan-detail-component',
  templateUrl: './approved-plan-detail.component.html',
  styleUrls: ['./approved-plan-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ApprovedPlanDetailComponent extends BaseComponent implements OnInit {
  formCreated = this.fb.group({
    rmCode: [{ value: '', disabled: true }],
    branchName: [{ value: '', disabled: true }],
    year: [{ value: '', disabled: true }],
    status: [{ value: '', disabled: true }],
  });

  objFunctionCustomer: any;
  objFunctionRM: any;

  isInfoRM: boolean;
  listDataSme = [];
  properties = [];
  listData = [];
  dataPlan: any;
  constructor(
    injector: Injector,
    private approvedPlanService: ApprovedPlanService,
    private categoryService: CategoryService
  ) {
    super(injector);
    this.objFunctionRM = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.dataPlan = JSON.parse(this.route.snapshot.queryParams.dataPlan);
  }
  checkAllCustomer360: boolean;
  checkAllPotential: boolean;
  listSelectedCustomer = [];
  countCheckedCustomer = 0;
  listSelectedPotential = [];
  countCheckedPotential = 0;
  limit = global.userConfig.pageSize;
  isCBQL = false;
  isRoleAdmin = false;

  childHeader = [];
  parentHeader = [];
  dataRows = [];

  firstHeader = 0;
  lastHeader = 0;
  paramSearchCustomer = {
    rmCode: '',
    year: new Date().getFullYear(),
    isLead: false,
    status: '',
    page: 0,
    size: maxInt32,
  };

  childHeader1 = [];
  parentHeader1 = [];
  granChildHeader1 = [];
  dataRows1 = [];

  soDau1 = 0;
  soCuoi1 = 0;
  soCotCuoi1 = 0;

  childHeader2 = [];
  parentHeader2 = [];
  granChildHeader2 = [];
  dataRows2 = [];

  soDau2 = 0;
  soCuoi2 = 0;
  soCotCuoi2 = 0;

  listStatusAdjusted = [
    LIST_STATUS_CUSTOMER_APPROVE[1].code,
    LIST_STATUS_CUSTOMER_APPROVE[2].code,
    LIST_STATUS_CUSTOMER_APPROVE[3].code,
  ];

  ngOnInit() {
    this.isLoading = true;
    this.isInfoRM = true;
    this.formCreated.controls.rmCode.setValue(this.dataPlan?.rmCode + ' - ' + this.dataPlan?.rmName);
    this.formCreated.controls.branchName.setValue(this.dataPlan?.branchCode + ' - ' + this.dataPlan?.branchName);
    this.formCreated.controls.year.setValue(this.dataPlan?.year);
    this.formCreated.controls.status.setValue(this.dataPlan?.statusCode ? this.dataPlan?.status : '');

    this.paramSearchCustomer.rmCode = this.dataPlan?.rmCode;
    this.paramSearchCustomer.status = this.dataPlan?.statusCode;

    forkJoin([
      this.approvedPlanService.viewDetailTotalPlan(this.dataPlan).pipe(catchError(() => of(undefined))),
      this.approvedPlanService.viewListCustomerAp(this.paramSearchCustomer).pipe(catchError(() => of(undefined))),
      this.approvedPlanService
        .viewListCustomerAp({ ...this.paramSearchCustomer, isLead: true })
        .pipe(catchError(() => of(undefined))),
      this.categoryService
        .getBranchesOfUser(this.objFunctionRM?.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined))),
    ]).subscribe(([listTotalPlan, listCustomer360, listPotential, listBranches]) => {
      this.isCBQL = !_.isEmpty(listBranches);
      this.isRoleAdmin = this.currUser.branch === BRANCH_HO;
      // Bảng 1
      this.firstHeader = listTotalPlan?.parentHeader?.cells.filter((item) => item.indexConfig)[0]?.column;
      this.childHeader = listTotalPlan?.childHeader?.cells;
      this.parentHeader = listTotalPlan?.parentHeader?.cells;
      this.parentHeader = this.parentHeader?.map((item) => {
        let number = item.indexConfig ? this.childHeader?.filter((i) => i.code === item.code).length : 0;
        return { ...item, countCode: number };
      });
      let number = 0;
      this.childHeader = this.childHeader?.map((item) => {
        if (this.parentHeader.filter((i) => i?.code === item?.code).length === 1) {
          return { ...item, numberCode: ++number };
        } else {
          number = 0;
          return { ...item };
        }
      });
      this.lastHeader = _.last(
        this.childHeader?.filter((item) => item?.code === _.last(this.parentHeader)?.code)
      )?.column;
      this.dataRows = [listTotalPlan?.kpiAssigned?.cells, listTotalPlan?.kpiMinimum?.cells];

      // Bảng 2
      this.parentHeader1 = listCustomer360?.parentHeader?.cells;
      this.childHeader1 = listCustomer360?.childHeader?.cells;
      this.granChildHeader1 = listCustomer360?.granChildHeader?.cells;
      this.parentHeader1 = this.parentHeader1?.map((item) => {
        let number = item?.indexConfig ? this.granChildHeader1?.filter((i) => i.code === item.code).length : 0;
        return { ...item, countCode: number };
      });

      this.childHeader1 = this.childHeader1?.map((item) => {
        let number = item.code ? this.granChildHeader1?.filter((i) => i.childCode === item.childCode).length : 0;
        return { ...item, countCode: number };
      });

      this.soDau1 = this.granChildHeader1?.filter(
        (item) => item.childCode === _.first(this.childHeader1)?.childCode
      )[0].column;
      this.soCuoi1 = _.last(
        this.granChildHeader1?.filter((item) => item.childCode === _.last(this.childHeader1)?.childCode)
      )?.column;
      this.soCotCuoi1 = _.last(this.granChildHeader1)?.column;
      this.dataRows1 = listCustomer360?.rows?.content || [];

      // Bảng 3

      this.parentHeader2 = listPotential?.parentHeader?.cells;
      this.childHeader2 = listPotential?.childHeader?.cells;
      this.granChildHeader2 = listPotential?.granChildHeader?.cells;
      this.parentHeader2 = this.parentHeader2?.map((item) => {
        let number = item.indexConfig ? this.granChildHeader2?.filter((i) => i.code === item.code).length : 0;
        return { ...item, countCode: number };
      });

      this.childHeader2 = this.childHeader2?.map((item) => {
        let number = item.code ? this.granChildHeader2?.filter((i) => i.childCode === item.childCode).length : 0;
        return { ...item, countCode: number };
      });

      this.soDau2 = this.granChildHeader2?.filter(
        (item) => item.childCode === _.first(this.childHeader2)?.childCode
      )[0]?.column;
      this.soCuoi2 = _.last(
        this.granChildHeader2?.filter((item) => item.childCode === _.last(this.childHeader2)?.childCode)
      )?.column;
      this.soCotCuoi2 = _.last(this.granChildHeader2)?.column;
      this.dataRows2 = listPotential?.rows?.content || [];
      this.isLoading = false;
    });
  }

  openInfoRM() {
    this.isInfoRM = !this.isInfoRM;
  }

  getValue(row, key) {
    return _.get(row, key);
  }

  // Customer
  onCheckboxAllFnCustomer360(isChecked) {
    this.checkAllCustomer360 = isChecked;
    if (!isChecked) {
      this.countCheckedCustomer = 0;
    } else {
      this.countCheckedCustomer = this.dataRows1.length;
    }
    for (const item of this.dataRows1) {
      if (isChecked) {
        if (
          this.listSelectedCustomer.findIndex((itemChoose) => {
            return itemChoose.customerCode === item.cells[1].value;
          }) === -1
        ) {
          this.listSelectedCustomer.push({ customerCode: item.cells[1].value, statusCode: item.cells[3].indexConfig });
        }
      } else {
        this.listSelectedCustomer = this.listSelectedCustomer.filter((itemChoose) => {
          return itemChoose.customerCode !== item.cells[1].value;
        });
      }
    }
  }

  isCheckedCustomer360(item) {
    return this.listSelectedCustomer?.findIndex((itemSelected) => itemSelected.customerCode === item[1].value) > -1;
  }

  onCheckboxFnCustomer360(item, isChecked) {
    if (isChecked) {
      this.listSelectedCustomer.push({ customerCode: item[1].value, statusCode: item[3].indexConfig });
      this.countCheckedCustomer += 1;
    } else {
      this.listSelectedCustomer = this.listSelectedCustomer.filter((itemChoose) => {
        return itemChoose.customerCode !== item[1].value;
      });
      this.countCheckedCustomer -= 1;
    }
    this.checkAllCustomer360 = this.countCheckedCustomer === this.dataRows1.length;
  }

  onCheckboxAllFnPotential(isChecked) {
    this.checkAllPotential = isChecked;
    if (!isChecked) {
      this.countCheckedPotential = 0;
    } else {
      this.countCheckedPotential = this.dataRows2.length;
    }
    for (const item of this.dataRows2) {
      if (isChecked) {
        if (
          this.listSelectedPotential.findIndex((itemChoose) => {
            return itemChoose.customerCode === item.cells[1].value;
          }) === -1
        ) {
          this.listSelectedPotential.push({ customerCode: item.cells[1].value, statusCode: item.cells[3].indexConfig });
        }
      } else {
        this.listSelectedPotential = this.listSelectedPotential.filter((itemChoose) => {
          return itemChoose.customerCode !== item.cells[1].value;
        });
      }
    }
  }

  isCheckedPotential(item) {
    return this.listSelectedPotential?.findIndex((itemSelected) => itemSelected.customerCode === item[1].value) > -1;
  }

  onCheckboxFnPotential(item, isChecked) {
    if (isChecked) {
      this.listSelectedPotential.push({ customerCode: item[1].value, statusCode: item[3].indexConfig });
      this.countCheckedPotential += 1;
    } else {
      this.listSelectedPotential = this.listSelectedPotential.filter((itemChoose) => {
        return itemChoose.customerCode !== item[1].value;
      });
      this.countCheckedPotential -= 1;
    }
    this.checkAllPotential = this.countCheckedPotential === this.dataRows2.length;
  }

  approveBranchOrHeadquarter(isApprove: boolean) {
    if (this.isCBQL) {
      if (_.isEmpty(this.listSelectedCustomer) && _.isEmpty(this.listSelectedPotential)) {
        this.messageService.warn('Không có bản ghi nào được chọn');
        return;
      }
      let actionApprove = this.isRoleAdmin ? 'APPROVE_HO' : 'APPROVE_BRANCH';
      const listCustomer = [...this.listSelectedCustomer, ...this.listSelectedPotential];
      if (isApprove) {
        if (this.isRoleAdmin && listCustomer.filter((item) => +item.statusCode !== 2).length > 0) {
          this.messageService.warn(_.get(this.notificationMessage, 'messageApprove'));
          return;
        } else if (!this.isRoleAdmin && listCustomer.filter((item) => +item.statusCode !== 1).length > 0) {
          this.messageService.warn(_.get(this.notificationMessage, 'messageApprove'));
          return;
        }
      } else {
        actionApprove = 'EDIT';
        if (listCustomer.filter((item) => !_.includes(this.listStatusAdjusted, +item.statusCode)).length > 0) {
          this.messageService.warn(_.get(this.notificationMessage, 'messageApprove'));
          return;
        }
      }

      const listCustomerApprove = listCustomer.map((item) => { return {
        ...item,
        rmCode: this.dataPlan?.rmCode,
      } })

      const messageApprove = 'Bạn có chắc chắn thực hiện?';
      this.confirmService.openModal(ConfirmType.Confirm, messageApprove).then((res) => {
        if (res) {
          this.isLoading = true;
          this.approvedPlanService.approveAccountPlanning(actionApprove, listCustomerApprove).subscribe(
            (res) => {
              this.isLoading = false;
              this.messageService.success('Thực hiện thành công');
              this.back();
            },
            (e) => {
              this.isLoading = false;
              this.messageService.error(this.notificationMessage.E001);
            }
          );
        }
      });
    }
  }

  fixWidth(count) {
    return +count * 150 + `px`;
  }

  handleSelect(data, isLead: boolean) {
    let customerCode = data?.cells[1].value || '';
    const modalConfirm = this.modalService.open(ModalDetailAPComponent, {
      windowClass: 'list__rm-modal',
    });
    modalConfirm.componentInstance.customerCode = customerCode;
    modalConfirm.componentInstance.isLead = isLead;
    modalConfirm.componentInstance.rmCode = this.dataPlan?.rmCode;
    modalConfirm.result
      .then((res) => {
        if (res) {
          return;
        }
      })
      .catch(() => {});
  }
}
