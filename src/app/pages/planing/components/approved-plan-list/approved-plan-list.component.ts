import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { global } from '@angular/compiler/src/util';
import { CommonCategory, Scopes, maxInt32, FunctionCode, RoleUser } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { forkJoin, of } from 'rxjs';
import _ from 'lodash';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { Utils } from 'src/app/core/utils/utils';
import { RmApi } from 'src/app/pages/customer-360/apis';
import { RmProfileService } from 'src/app/core/services/rm-profile.service';
import * as moment from 'moment';
import { ApprovedPlanService } from '../../services/approved-plan.service';
import { FileService } from 'src/app/core/services/file.service';

@Component({
  selector: 'approved-plan-list-component',
  templateUrl: './approved-plan-list.component.html',
  styleUrls: ['./approved-plan-list.component.scss'],
})
export class ApprovedPlanListComponent extends BaseComponent implements OnInit {
  isLoading = false;
  @ViewChild('table') table: DatatableComponent;
  limit = global.userConfig.pageSize;
  pageable: Pageable;
  params: any = {
    pageNumber: 0,
    pageSize: global?.userConfig?.pageSize,
    rsId: '',
    scope: Scopes.VIEW,
  };
  propDetail: any;

  objFunctionCustomer: any;
  objFunctionRM: any;
  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private rmApi: RmApi,
    private approvedPlanService: ApprovedPlanService,
    private fileService: FileService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.APPROVED_PLAN}`);
    this.objFunctionCustomer = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.objFunctionRM = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
  }

  formSearch = this.fb.group({
    rsId: '',
    scope: Scopes.VIEW,
    branchCode: [''],
    rmCode: [''],
    status: [''],
    year: [{ value: new Date().getFullYear(), disabled: true }],
  });

  listDivision = [];
  listData = [];
  listRmManager = [];
  listBranches = [];
  listStatusApproved = [
    { code: '', displayName: 'Tất cả' },
    { code: 1, displayName: 'Đang soạn thảo' },
    { code: 2, displayName: 'Đã phân giao' },
    { code: 3, displayName: 'Phê duyệt chi nhánh' },
    { code: 4, displayName: 'Phê duyệt hội sở' },
    { code: 5, displayName: 'Điều chỉnh phân giao' },
    { code: 6, displayName: 'Chưa phân giao' },
    { code: 7, displayName: 'Không cần phê duyệt' },
  ];

  listYear = [];
  isRoleAdmin = false;

  ngOnInit() {
    this.isLoading = true;
    forkJoin([
      this.categoryService
        .getBranchesOfUser(this.objFunctionRM?.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined))),
    ]).subscribe(([listBranches]) => {
      this.listBranches = listBranches?.map((item) => {
        return { code: item.code, displayName: item.code + ' - ' + item.name };
      });
      if (!_.isEmpty(this.listBranches)) {
        this.listBranches?.unshift({ code: '', displayName: this.fields.all });
        this.isRoleAdmin = true;
      }
      this.searchApprovedPlan(true);
      this.getRmManager([], true);
    });
  }

  ngAfterViewInit() {
    this.formSearch.controls.branchCode.valueChanges.subscribe((value) => {
      this.getRmManager([value]);
    });
  }

  searchApprovedPlan(isSearch: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.params.pageNumber = 0;
    }
    const paramSearch = {
      year: this.formSearch.controls.year.value,
      ...this.formSearch.value,
    };
    paramSearch.rsId = this.objFunctionRM?.rsId;
    paramSearch.page = this.params.pageNumber;
    paramSearch.size = this.params.pageSize;

    this.approvedPlanService.searchApprovedPlan(paramSearch).subscribe(
      (result) => {
        this.isLoading = false;
        this.listData = result.content;
        this.pageable = {
          totalElements: result.totalElements,
          totalPages: result.totalPages,
          currentPage: result.number,
          size: this.limit,
        };
      },
      (e) => {
        this.messageService.error(this.notificationMessage.E001);
        this.isLoading = false;
      }
    );
  }

  getRmManager(listBranch, isSearch?) {
    this.formSearch.get('rmCode').disable();
    this.listRmManager = [];
    this.formSearch.controls.rmCode.setValue('');
    if (this.isRoleAdmin) {
      this.rmApi
        .post('findAll', {
          page: 0,
          size: maxInt32,
          crmIsActive: true,
          branchCodes: listBranch?.filter((code) => !Utils.isStringEmpty(code)),
          rmBlock: 'CIB',
          rsId: this.objFunctionRM?.rsId,
          scope: Scopes.VIEW,
        })
        .subscribe((res) => {
          const listRm: any[] =
            res?.content?.map((item) => {
              if (!_.isEmpty(item?.t24Employee?.employeeCode)) {
                return {
                  code: item?.t24Employee?.employeeCode || '',
                  displayName:
                    Utils.trimNullToEmpty(item?.t24Employee?.employeeCode) +
                    ' - ' +
                    Utils.trimNullToEmpty(item?.hrisEmployee?.fullName),
                };
              }
            }) || [];

          this.listRmManager = listRm;
          if (!_.isEmpty(this.listRmManager)) {
            this.listRmManager.unshift({ code: '', displayName: this.fields.all });
            this.formSearch.controls.rmCode.setValue('');
          }
          this.formSearch.get('rmCode').enable();
          // if (isSearch) {
          //   this.searchApprovedPlan(true);
          // }
        });
    }
    //  else {
    //   this.listRmManager.push({
    //     code: this.currUser.code,
    //     displayName: this.currUser.code + ' - ' + this.currUser.fullName,
    //   });
    //   this.formSearch.controls.rmCode.setValue(this.currUser.code);
    //   this.formSearch.get('rmCode').enable();
    //   if (isSearch) {
    //     this.searchApprovedPlan(true);
    //   }
    // }
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.pageNumber = pageInfo.offset;
    this.searchApprovedPlan(false);
  }

  onActive(event) {
    const item = _.get(event, 'row');
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      this.router.navigate([this.router.url, 'detail'], {
        skipLocationChange: true,
        queryParams: {
          dataPlan: JSON.stringify(item),
        },
      });
    }
  }

  // requestApprovalAp(dataPlan) {
  //   this.isLoading = true;
  //   dataPlan = _.omit(dataPlan, ['id', 'branchNamePlan', 'statusText']);
  //   this.planningImportService.requestApprovalAp([dataPlan]).subscribe(
  //     (res) => {
  //       this.isLoading = false;
  //       this.messageService.success(this.notificationMessage.success);
  //       this.searchApprovedPlan(true);
  //     },
  //     (e) => {
  //       this.isLoading = false;
  //       this.messageService.error(this.notificationMessage.error);
  //     }
  //   );
  // }

  exportFile() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+this.pageable?.totalElements <= 0) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.isLoading = true;
    const paramExport = {
      year: this.formSearch.controls.year.value,
      ...this.formSearch.value,
    };
    paramExport.rsId = this.objFunction?.rsId;
    paramExport.page = 0;
    paramExport.size = maxInt32;
    this.approvedPlanService.excelApprovedPlan(paramExport).subscribe(
      (res) => {
        if (res) {
          this.download(res);
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(_.get(this.notificationMessage, 'export_error'));
        this.isLoading = false;
      }
    );
  }

  download(fileId: string) {
    let titleExcel = 'Danh sách RM lập kế hoạch.xlsx';
    this.fileService.downloadFile(fileId, titleExcel).subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }
}
