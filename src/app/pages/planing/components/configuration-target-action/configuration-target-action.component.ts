import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { FunctionCode, maxInt32, ScreenType } from 'src/app/core/utils/common-constants';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import * as _ from 'lodash';
import { ConfigurationTargetService } from '../../services/configuration-target.service';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  templateUrl: './configuration-target-action.component.html',
  styleUrls: ['./configuration-target-action.component.scss'],
})
export class ConfigurationTargetActionComponent extends BaseComponent implements OnInit {
  constructor(injector: Injector, private service: ConfigurationTargetService) {
    super(injector);
    this.isLoading = true;
    this.actionType = this.route?.snapshot?.data?.screenType;
    this.state = this.router.getCurrentNavigation()?.extras?.state?.params;
    this.data = this.router.getCurrentNavigation()?.extras?.state?.data;
  }

  form = this.fb.group({
    id: null,
    itemCode: ['', CustomValidators.required],
    itemName: ['', CustomValidators.required],
    itemLevel: [null, CustomValidators.required],
    itemParent: [{ value: '', disabled: true }, CustomValidators.required],
    attribute: ['', CustomValidators.required],
    unit: ['', CustomValidators.required],
    type: ['', CustomValidators.required],
    orderByLevel: null,
    formula: '',
    isHeader: false,
    isGeneral: false,
  });
  data: any;
  actionType: string;
  title = 'Thêm mới cấu hình chỉ tiêu AP';
  commonData = {
    listLevel: [],
    listUnit: [],
    listAttribute: [],
    listParent: [],
    listType: [],
  };
  isValidator = false;

  ngOnInit() {
    const state = this.sessionService.getSessionData(FunctionCode.CONFIG_AP);
    if (state) {
      this.commonData = _.cloneDeep(state);
    }

    this.form.get('itemLevel').valueChanges.subscribe((value) => {
      if (this.actionType !== ScreenType.Detail) {
        if (_.isEmpty(value) || value === '1') {
          this.form.get('itemParent').setValidators(null);
          this.form.get('itemParent').setValue('');
          this.form.get('itemParent').disable();
        } else {
          if (this.data.itemLevel !== value) {
            this.form.get('itemParent').setValue('');
          }
          this.form.get('itemParent').setValidators(CustomValidators.required);
          this.form.get('itemParent').enable();
        }
      }
      if (!_.isEmpty(value)) {
        this.data.itemLevel = value;
        this.getListParent();
      }
      this.form.updateValueAndValidity();
    });

    if (this.actionType === ScreenType.Detail) {
      this.form.disable();
      this.title = 'Chi tiết cấu hình chỉ tiêu AP';
    } else if (this.actionType === ScreenType.Update) {
      this.title = 'Cập nhật cấu hình chỉ tiêu AP';
    }

    if (this.data) {
      this.form.patchValue(this.data);
    }
    this.data = {};
    this.isLoading = false;
  }

  getListParent() {
    this.isLoading = true;
    const params = {
      page: 0,
      size: maxInt32,
      itemLevel: (+this.data.itemLevel - 1).toString(),
    };
    this.service
      .search(params)
      .pipe(catchError(() => of(undefined)))
      .subscribe((data) => {
        this.commonData.listParent = data?.content || [];
        this.isLoading = false;
      });
  }

  save() {
    if (this.form.valid) {
      this.confirmService.confirm().then((isConfirm) => {
        if (isConfirm) {
          const data = { ...cleanDataForm(this.form) };
          this.isLoading = true;
          if (this.actionType === ScreenType.Update) {
            this.service.update(data).subscribe(
              () => {
                this.messageService.success(this.notificationMessage.success);
                this.isLoading = false;
                this.router.navigateByUrl(`customer-plan/config-target/detail/${data.id}`, {
                  state: { params: this.state, data: data },
                  skipLocationChange: true,
                });
              },
              (e) => {
                this.isLoading = false;
                if (!_.isEmpty(e?.error?.description)) {
                  this.messageService.error(e?.error?.description);
                } else {
                  this.messageService.error(this.notificationMessage.error);
                }
              }
            );
          } else {
            this.service.create(data).subscribe(
              (id) => {
                this.messageService.success(this.notificationMessage.success);
                this.isLoading = false;
                data.id = id;
                this.router.navigateByUrl(`customer-plan/config-target/detail/${id}`, {
                  state: { params: this.state, data: data },
                  skipLocationChange: true,
                });
              },
              (e) => {
                e.error = JSON.parse(e.error);
                this.isLoading = false;
                if (!_.isEmpty(e?.error?.description)) {
                  this.messageService.error(e?.error?.description);
                } else {
                  this.messageService.error(this.notificationMessage.error);
                }
              }
            );
          }
        }
      });
    } else {
      this.isValidator = true;
      validateAllFormFields(this.form);
    }
  }

  back() {
    this.router.navigateByUrl('customer-plan/config-target', { state: this.state });
  }

  isRequiredParent() {
    return !_.isEmpty(this.form.get('itemLevel').value) && _.trim(this.form.get('itemLevel').value) !== '1';
  }

  getError(key: string) {
    return this.form.get(key)?.errors?.cusRequired && this.isValidator;
  }
}
