import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../../environments/environment';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ApprovedPlanService {
  constructor(private http: HttpClient) {}

  searchApprovedPlan(param): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/leads/searchRmApproveAp`, param);
  }

  excelApprovedPlan(param): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/leads/exportRmApproveAp`, param, {
      responseType: 'text',
    });
  }

  viewDetailTotalPlan(param): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/leads/viewDetailTotalPlan`, param);
  }

  viewListCustomerAp(param): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/leads/viewListCustomerAp`, param);
  }

  infoDetailsCustomer(customerCode, isLead, rmCode: string): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_customer_sme}/leads/info-details-customer/${customerCode}?isLead=${isLead}&rmCode=${rmCode}`
    );
  }

  approveAccountPlanning(action, param): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_customer_sme}/leads/approve-account-planning?action=${action}`,
      param
    );
  }

  apWarning(param): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/leads/ap-warning`, param);
  }

  exportApWarning(param): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/leads/ap-warning/export`, param, {
      responseType: 'text',
    });
  }
}
