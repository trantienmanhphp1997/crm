import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PlanningImportService {
  constructor(private http: HttpClient) {}

  searchPlan(param): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/customers-assignment/ap-acc-assign`, param);
  }

  exportApTemplate(param): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/leads/export-ap-template`, param, {
      responseType: 'text',
    });
  }

  importApTemplate(data: FormData): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/leads/import-ap`, data);
  }

  requestApprovalAp(param) {
    return this.http.post(`${environment.url_endpoint_customer_sme}/leads/ap-request-approval`, param);
  }

  exportAPDataImported(param) {
    return this.http.post(`${environment.url_endpoint_customer_sme}/leads/export-ap-data-imported`, param, {
      responseType: 'text',
    });
  }
}
