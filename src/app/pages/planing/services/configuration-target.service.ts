import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ConfigurationTargetService {
  baseUrl = `${environment.url_endpoint_customer_sme}/account-plan`;
  constructor(private http: HttpClient) {}

  search(params): Observable<any> {
    return this.http.get(`${this.baseUrl}`, { params });
  }

  create(data): Observable<any> {
    return this.http.post(`${this.baseUrl}`, data, { responseType: 'text' });
  }

  update(data): Observable<any> {
    return this.http.put(`${this.baseUrl}`, data);
  }

  delete(id: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }
}
