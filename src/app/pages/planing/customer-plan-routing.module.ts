import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGuardService } from 'src/app/core/services/role-guard.service';
import { FunctionCode, Scopes, ScreenType } from 'src/app/core/utils/common-constants';
import {
  ApprovedPlanDetailComponent,
  ApprovedPlanListComponent,
  ConfigurationTargetActionComponent,
  ConfigurationTargetComponent,
  PlanningImportComponent,
  WarningAPComponent,
} from './components';
import { PlanningListComponent } from './components/planning-list/planning-list.component';

const routes: Routes = [
  {
    path: 'config-target',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.CONFIG_AP,
    },
    component: ConfigurationTargetComponent,
  },
  {
    path: 'config-target/create',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.CONFIG_AP,
      scope: Scopes.CREATE,
      screenType: ScreenType.Create,
    },
    component: ConfigurationTargetActionComponent,
  },
  {
    path: 'config-target/update/:id',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.CONFIG_AP,
      scope: Scopes.UPDATE,
      screenType: ScreenType.Update,
    },
    component: ConfigurationTargetActionComponent,
  },
  {
    path: 'config-target/detail/:id',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.CONFIG_AP,
      scope: Scopes.UPDATE,
      screenType: ScreenType.Detail,
    },
    component: ConfigurationTargetActionComponent,
  },

  {
    path: 'planning-list',
    data: {
      code: FunctionCode.ACCOUNT_PLANNING,
    },
    canActivateChild: [RoleGuardService],
    children: [
      {
        path: '',
        component: PlanningListComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },

      {
        path: 'import',
        component: PlanningImportComponent,
      },
    ],
  },

  {
    path: 'approved-plan-list',
    data: {
      code: FunctionCode.APPROVED_PLAN,
    },
    canActivateChild: [RoleGuardService],
    children: [
      {
        path: '',
        component: ApprovedPlanListComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
      {
        path: 'detail',
        component: ApprovedPlanDetailComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
    ],
  },

  {
    path: 'warning-ap',
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.WARNING_AP,
    },
    component: WarningAPComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomerPlanRoutingModule {}
