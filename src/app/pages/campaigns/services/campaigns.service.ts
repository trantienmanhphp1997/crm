import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { maxInt32 } from 'src/app/core/utils/common-constants';

@Injectable({
  providedIn: 'root',
})
export class CampaignsService {
  constructor(private http: HttpClient) {}

  info = new BehaviorSubject({});

  getAll(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/campaigns`);
  }

  getById(id): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/campaigns/${id}`);
  }

  search(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/campaigns`, { params });
  }

  create(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/campaigns`, data, { responseType: 'text' });
  }

  update(data): Observable<any> {
    return this.http.put(`${environment.url_endpoint_sale}/campaigns/${data.id}`, data, { responseType: 'text' });
  }

  delete(id): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_sale}/campaigns/${id}`);
  }

  activated(id): Observable<any> {
    return this.http.put(`${environment.url_endpoint_sale}/campaigns/activate/${id}`, {});
  }

  import(data: FormData): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/campaigns/import`, data);
  }
  importByRm(data: FormData): Observable<any>{
    return this.http.post(`${environment.url_endpoint_sale}/campaigns/import-rm-campaign`, data);
  }
  checkFileImport(fileId: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/campaigns/checkProcessImport?fileId=${fileId}`);
  }

  writeDataImport(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/campaigns/saveDataCampaign`, data);
  }

  clearDataImport(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/campaigns/clearDataImport`, data);
  }

  clearDataCampaign(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/campaigns/clearDataCampaign`, params);
  }

  searchDataCampaign(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/campaigns/findDataCampaign`, { params });
  }

  searchImportError(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/campaigns/findDataError`, { params });
  }

  searchImportSuccess(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/campaigns/findDataSuccess`, { params });
  }

  getFieldCampaigns(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/fields`, { params });
  }

  // onCreate(): Observable<any> {
  //   return this.socketClient.on('/topic/campaign/created', ServiceType.SALE);
  // }

  // onUpdate(): Observable<any> {
  //   return this.socketClient.on('/topic/campaign/updated', ServiceType.SALE);
  // }

  findDataCampaign(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/leads/findAllByUser`, params);
  }

  findAllActivities(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/activities/findAll`, params);
  }

  getBlockMapping(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/blocks`, {
      params: {
        size: maxInt32.toString(),
        page: '0',
        name: '',
        code: '',
      },
    });
  }

  createFileExcel(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/campaigns/export`, { params, responseType: 'text' });
  }

  getSaleProcess(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_workflow}/processes/saleProcess`);
  }
  searchProductProcess(params: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer}/product-process`, { params });
  }

  createMappingProduct(data: any): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer}/product-process`, data, { responseType: 'text' });
  }

  findHistoryByCode(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/leads/findHistoryByCode`, { params });
  }

  findCampaignByRm(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/campaigns/findCampaignByRM`, { params });
  }

  importReAssign(data: FormData): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/campaigns/import-re-assign`, data);
  }

  writeDataReAssign(fileId: string, campaignId: string): Observable<any> {
    return this.http.post(
      `${environment.url_endpoint_sale}/campaigns/saveDataCampaignReAssign?fileId=${fileId}&campaignId=${campaignId}`,
      {}
    );
  }

  searchForPopup(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/campaigns/find`, data);
  }

  deleteLeadFromCampain(params): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_sale}/sale-potential/delete-campaign-lead`,{params, responseType: 'text'});
  }

  updateLeadFromCampain(data): Observable<any> {
    return this.http.put(`${environment.url_endpoint_sale}/sale-potential/update-campaign-lead`, data);
  }

  getDetailCustomerInCampaign(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/sale-potential/detail-campaign-lead`, { params });
  }

  getDropdownlistCampaign(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/campaigns`, { params });
  }

  createSME(body: any): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sme-campaign/create`, body);
  }

  getCampaignItems(blockCode: string, rsId: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/sme-campaign/campaign-item?blockCode=${blockCode}`);
  }

  getParentList(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/sme-campaign`, { params });
  }

  getSMECampaignDetail(id: string, rsId, scope): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/sme-campaign/findById/${id}?rsId=${rsId}&scope=${scope}`);
  }

  searchSME(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/sme-campaign`, { params });
  }

  updateSME(body: any): Observable<any> {
    return this.http.put(`${environment.url_endpoint_sale}/sme-campaign/${body.id}`, body, { responseType: 'text' });
  }

  deleteCampaign(id: string): Observable<any>{
    return this.http.delete(`${environment.url_endpoint_sale}/sme-campaign/${id}`);
  }
  importTargetSME(data: FormData): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sme-campaign/allocation/import`, data);
  }

  dowloadTemplateSME(id: string, params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/sme-campaign/template/${id}`, { responseType: 'text', params });
  }

  getHeaderAllocation(id : string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/sme-campaign/view/item/header/${id}`);
  }

  getListAllocation(body: any, page, size): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sme-campaign/view/item/allocation?page=${page}&size=${size}`, body);
  }

  dowloadAllocationError(fileId, campaignId): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/sme-campaign/export-allocation-error?fileId=${fileId}&campaignId=${campaignId}`, { responseType: 'text' });
  }

  importAssignCustomerSME(data: FormData): Observable<any>{
    return this.http.post(`${environment.url_endpoint_customer_sme}/customers/import/campaign/customer`, data , { responseType: 'text' });
  }

  downloadTemplateCustomerAssignmentSME(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/sme-campaign/template/customer/assignment`, { responseType: 'text' });
  }

  checkFileImportCustomer(fileId: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/customers/checkProcessImport?fileId=${fileId}`);
  }

  downloadErrorCustomerAssignmentSME(fileId: string, campaignId: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/sme-campaign/export-customer-assign-error?fileId=${fileId}&campaignId=${campaignId}`, { responseType: 'text' });
  }

  getSumAllocation(body: any): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/sme-campaign/view/item/sum`, body);
  }

  getListResult(id,body: any): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/sme-campaign/${id}/customer-assignment`, body);
  }
  updateResult(id,body: any): Observable<any> {
    return this.http.put(`${environment.url_endpoint_customer_sme}/sme-campaign/${id}/update-customer-assignment`, body);
  }
  getProductProcessByBlock(blockCode: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer}/product-process/findByBlock?blockCode=${blockCode}`);
  }


  exportAppoachingResult(id: string, params: any): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/sme-campaign/${id}/approaching-result/export`, params, {
      responseType: 'text'
    }
    )
  }
  exportCustomerTracking(id: string, params: any): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/sme-campaign/${id}/export-customer-assignment`, params, {
      responseType: 'text'
    }
    )
  }

  deleteReductionProposal(id: string): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_customer_sme}/reduction-proposal/${id}`);
  }

  searchReductionProposal(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/reduction-proposal/list`,  params );
  }

  getTableauReport(params): Observable<any> {
    console.log(params);
    return this.http.post(`${environment.url_endpoint_report}/report/getTableauReport`, params, { responseType: 'text' });
  }
  getApproachingResult(id: string, params: any): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/sme-campaign/${id}/approaching-result`, params);
  }

  activeCampaign(id): Observable<any> {
    return this.http.put(`${environment.url_endpoint_sale}/sme-campaign/active-campaign/${id}`, {});
  }

  searchForDashboard(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/campaigns/find-by-dashboard`, data);
  }


  getCampaignResultOverview(params: any, isbranchDimention: boolean, isRm: boolean): Observable<any> {
    let enpoint = `${environment.url_endpoint_sale}/dashboard-campaigns/result_v2`;
    if (isRm) {
      delete params.hrsCode;
      delete params.branchCode;
      delete params.listBranchCode;
      return this.http.post(enpoint, params);
    }

    if (isbranchDimention) {
      delete params.hrsCode;
      if (params.listBranchCode?.length > 1) {
        delete params.branchCode;
      } else {
        delete params.listBranchCode;
      }
    } else {
      delete params.branchCode;
      delete params.listBranchCode;
    }

    return this.http.post(enpoint, params);
  }

  getCampaignStageOverview(params: any, isbranchDimention: boolean, isRm: boolean): Observable<any> {
    let enpoint = `${environment.url_endpoint_sale}/dashboard-campaigns/process_v2`;
    if (isRm) {
      delete params.hrsCode;
      delete params.branchCode;
      delete params.listBranchCode;
      return this.http.post(enpoint, params);
    }

    if (isbranchDimention) {
      delete params.hrsCode;
      if (params.listBranchCode?.length > 1) {
        delete params.branchCode;
      } else {
        delete params.listBranchCode;
      }
    } else {
      delete params.branchCode;
      delete params.listBranchCode;
    }

    return this.http.post(enpoint, params);
  }

  getCampaignResultDetail(params : any): Observable<any> {
    delete params.hrsCode;
    if (params.listBranchCode?.length > 1) {
      delete params.branchCode;
    } else {
      delete params.listBranchCode;
    }
    return this.http.post(`${environment.url_endpoint_sale}/dashboard-campaigns/detail-filtered`, params);
  }

  getDashboardCampaignStageDetail(params): Observable<any>{
    return this.http.post(`${environment.url_endpoint_sale}/campaigns/rm-by-lane`,params);
  }
}
