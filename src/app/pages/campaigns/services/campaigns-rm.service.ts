import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CampaignsRmService {
  constructor(private http: HttpClient) {}

  getAll(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/campaigns`);
  }

  getById(id): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/campaigns/${id}`);
  }

  search(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/campaigns`, { params });
  }

  getProduct(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_sale}/products`, { params });
  }

  create(obj): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/campaigns`, obj, { responseType: 'text' });
  }

  update(obj, id): Observable<any> {
    return this.http.put(`${environment.url_endpoint_sale}/campaigns/${id}`, obj);
  }

  delete(id): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_sale}/campaigns/${id}`);
  }

  import(data: FormData): Observable<any> {
    return this.http.post(`${environment.url_endpoint_sale}/lead-previews/upload`, data);
  }

  getFieldCampaigns(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/fields`, { params });
  }

  // onCreate(): Observable<any> {
  //   return this.socketClient.on('/topic/campaign/created', ServiceType.SALE);
  // }

  // onUpdate(): Observable<any> {
  //   return this.socketClient.on('/topic/campaign/updated', ServiceType.SALE);
  // }
}
