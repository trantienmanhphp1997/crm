import {Component, OnInit, ViewEncapsulation, HostBinding, Input, Injector} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ConfirmDialogComponent} from '../../../../shared/components';
import {cleanDataForm} from '../../../../core/utils/function';
import {SaleSuggestApi} from '../../../sale-manager/api/sale-suggest.api';
import {BaseComponent} from '../../../../core/components/base.component';
import * as _ from 'lodash';

@Component({
  selector: 'app-campaign-rm-modal',
  templateUrl: './campaigns-rm-modal.component.html',
  styleUrls: ['./campaigns-rm-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CampaignsRmModalComponent extends BaseComponent implements OnInit {
  @HostBinding('class.list__campaigns-rm-content') listContent = true;

  isLoading = false;
  constructor(
    injector: Injector,
    private modalActive: NgbActiveModal,
    private saleSuggestApi: SaleSuggestApi)
  {
    super(injector);
  }
  data: any;
  @Input() paramsFilterOld: any;

  showLoading(isShowLoading) {
    this.isLoading = isShowLoading;
  }

  ngOnInit() {
  }

  closeModal() {
    this.modalActive.close(false);
  }

  choose() {
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.componentInstance.message = 'Bạn có muốn đẩy tập Khách hàng vào chiến dịch này?';
    confirm.result
      .then((res) => {
        if (res) {
          const params = this.paramsFilterOld;
          params.campaignId = _.first(this.data)?.id;
          this.isLoading = true;
          this.saleSuggestApi.addCustomerToCampaign(params).subscribe(item => {
            this.confirmService.success(this.notificationMessage.add_customer_to_campaign_success);
            this.isLoading = false;
            this.closeModal();
          }, error => {
            this.confirmService.success(this.notificationMessage.add_customer_to_campaign_success);
            this.isLoading = false;
            this.closeModal();
          });
        }
      })
      .catch(() => {});
  }

  onChangeCampain(event?: any) {
    this.data = [];
    if (event) {
      this.data.push(event);
    }
  }
}
