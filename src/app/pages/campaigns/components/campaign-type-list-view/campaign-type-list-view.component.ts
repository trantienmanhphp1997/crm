import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { global } from '@angular/compiler/src/util';
import { CampaignsService } from '../../services/campaigns.service';
import { Utils } from 'src/app/core/utils/utils';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';

@Component({
  selector: 'app-campaign-type-list-view',
  templateUrl: './campaign-type-list-view.component.html',
  styleUrls: ['./campaign-type-list-view.component.scss'],
})
export class CampaignTypeListViewComponent extends BaseComponent implements OnInit {
  listData = [];
  paramSearch = {
    size: global.userConfig.pageSize,
    page: 0,
    search: '',
  };
  prevParams = this.paramSearch;
  pageable: Pageable;
  constructor(
    injector: Injector,
    private campaignService: CampaignsService,
  ) {
    super(injector);
  }

  ngOnInit(): void {}

  search(isSearch?: boolean) {
    this.isLoading = true;
    let params: any;
    if (isSearch) {
      this.paramSearch.page = 0;

      this.paramSearch.search = Utils.trimNullToEmpty(this.paramSearch.search);
      params = this.paramSearch;
    } else {
      params = this.prevParams;
      params.page = this.paramSearch.page;
    }
    this.campaignService.search(params).subscribe(
      (result) => {
        if (result) {
          this.listData = result.content || [];
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: global.userConfig.pageSize,
          };
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    this.paramSearch.page = pageInfo.offset;
    this.search(false);
  }

  create() {
    this.router.navigate([this.router.url, 'create'], { state: { paramSearch: this.paramSearch } });
  }
}
