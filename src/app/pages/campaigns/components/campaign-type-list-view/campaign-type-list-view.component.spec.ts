import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignTypeListViewComponent } from './campaign-type-list-view.component';

describe('CampaignTypeListViewComponent', () => {
  let component: CampaignTypeListViewComponent;
  let fixture: ComponentFixture<CampaignTypeListViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignTypeListViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignTypeListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
