import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignListViewComponent } from './campaign-list-view.component';

describe('CampaignListViewComponent', () => {
  let component: CampaignListViewComponent;
  let fixture: ComponentFixture<CampaignListViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignListViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
