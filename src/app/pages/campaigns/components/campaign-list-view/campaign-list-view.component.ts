import { forkJoin, of } from 'rxjs';
import { global } from '@angular/compiler/src/util';
import { AfterViewInit, Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';
import { cleanDataForm } from 'src/app/core/utils/function';
import { CampaignsService } from '../../services/campaigns.service';
import {
  BRANCH_HO,
  CampaignStatus,
  CommonCategory,
  Division,
  FunctionCode,
  Scopes,
  SessionKey,
  maxInt32,
  ScreenType
} from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { Utils } from 'src/app/core/utils/utils';
import * as moment from 'moment';
import { CampaignsCreateModal } from '../campaign-create-modal/campaigns-create-modal';
import { ChooseCreateModalComponent } from '../choose-create-modal/choose-create-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { RmApi } from 'src/app/pages/rm/apis';
import { RmProfileService } from 'src/app/core/services/rm-profile.service';
import { UserService } from 'src/app/pages/system/services/users.service';

@Component({
  selector: 'app-campaign-list-view',
  templateUrl: './campaign-list-view.component.html',
  styleUrls: ['./campaign-list-view.component.scss'],
})
export class CampaignListViewComponent extends BaseComponent implements OnInit, AfterViewInit {
  isLoading = false;
  listData = [];
  isRM = false;

  listDataSME = [];
  listDivision = [];
  listRmManager = [];
  listBranchesManager = [];
  listRmManagerTerm = [];
  customerType: any;
  allBranch = {
    "branchCode": '',
    "code": '',
    "name": "Tất cả"
  }
  allRM = {
    "code": '',
    "displayName": "Tất cả"
  }
  formSearch = this.fb.group({
    code: '',
    name: '',
    status: '',
    typeId: '',
    purpose: '',
    formId: '',
    isCampaignActive: 'true',
    startDateFirst: null,
    startDateLast: null,
    endDateFirst: null,
    endDateLast: null,
    branchCodes: [''],
    isRmScr: '',
    customerType: 'SME',
    createdBranch: "",
    createdUsername: "",
    parentCampaignId: ""
  });
  paramSearch = {
    size: global.userConfig.pageSize,
    page: 0,
    rsId: '',
    scope: Scopes.VIEW,
  };
  prevParams: any;
  pageable: Pageable;
  prop: any;
  listStatus = [];
  listStatusSME = [
    {
      "code": "",
      "name": "Tất cả"
    },
    {
      "code": "IN_PROGRESS",
      "name": "Đang soạn thảo"
    }, {
      "code": "ACTIVATED",
      "name": "Đã kích hoạt"
    }, {
      "code": "EXPIRED",
      "name": "Đã hết hạn"
    },
  ];
  listPurpose = [];
  listFormality = [];
  statusCampaign = [];
  minStartDateLast: Date;
  maxStartDateFirst: Date;
  minEndDateLast: Date;
  minEndDateCheck: Date;
  maxEndDateFirst: Date;
  @Input() isViewFromPopup = false;
  isSelectedRadio = [];
  @Output() onChangeCampain = new EventEmitter();
  @Input() listBranchChoose: Array<any>;

  constructor(
    injector: Injector,
    private campaignService: CampaignsService,
    public dialog: MatDialog,
    private rmApi: RmApi,
    private categoryService: CategoryService,
    private userService: UserService,
    private rmProfileService: RmProfileService
  ) {
    super(injector);
    this.isLoading = true;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CAMPAIGN}`);
    this.paramSearch.rsId = this.objFunction?.rsId;

  }

  ngOnInit(): void {
    // destroy current campaign info
    if (!this.campaignService.info.closed) {
      this.campaignService.info.next({});
      this.campaignService.info.complete(); // destroy info
    }
    this.statusCampaign.push({ code: '', name: this.fields.all });
    this.statusCampaign.push({ code: 'true', name: this.fields.effective });
    this.statusCampaign.push({ code: 'false', name: this.fields.expire });
    this.formSearch.controls.isCampaignActive.setValue(this.statusCampaign[1].code);
    this.sessionService.clearSessionData(`${FunctionCode.CAMPAIGN}_${ScreenType.Detail}`);
    const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
    var prevBlock = this.sessionService.getSessionData(SessionKey.COMMON_DATA_CAMPAIGN_DETAILS);
    if(prevBlock == undefined)
      prevBlock = 'INDIV'

    this.listDivision =
      divisionOfUser?.map((item) => {
        return { code: item.code, displayName: `${item.code || ''} - ${item.name || ''}` };
      }).filter(item => [Division.INDIV, Division.SME].includes(item.code)) || [];
    if (this.listDivision.length > 0) {
      this.formSearch.controls.customerType.setValue(this.listDivision[0].code);
      this.customerType = this.listDivision[0].code
    }
    if (prevBlock) {
      this.sessionService.clearSessionData(SessionKey.COMMON_DATA_CAMPAIGN_DETAILS)
      if(this.listDivision.length == 1 && this.listDivision[0].code == 'SME'){
        this.formSearch.controls.customerType.setValue(this.listDivision[0].code);
        this.customerType = this.listDivision[0].code;
      }else{
        this.formSearch.controls.customerType.setValue(prevBlock);
        this.customerType = prevBlock;
      }
    }
    if (global?.previousUrl && global?.previousUrl.includes('campaigns/campaign-management/detail')) {
      this.prop = this.sessionService.getSessionData(FunctionCode.CAMPAIGN);
    }
    if (this.prop) {
      this.prevParams = { ...this.prop?.prevParams };
      this.paramSearch.page = this.prevParams?.page;
      this.formSearch.patchValue(this.prevParams);
      this.formSearch.controls.startDateFirst.setValue(
        this.prevParams?.startDateFirst ? new Date(this.prevParams?.startDateFirst) : null
      );
      this.formSearch.controls.startDateLast.setValue(
        this.prevParams?.startDateLast ? new Date(this.prevParams?.startDateLast) : null
      );
      this.formSearch.controls.endDateFirst.setValue(
        this.prevParams?.endDateFirst ? new Date(this.prevParams?.endDateFirst) : null
      );
      this.formSearch.controls.endDateLast.setValue(
        this.prevParams?.endDateLast ? new Date(this.prevParams?.endDateLast) : null
      );
      this.listFormality = this.prop?.listFormality || [];
      this.statusCampaign = this.prop?.statusCampaign || [];
      this.listPurpose = this.prop?.listPurpose || [];
      this.listStatus = this.prop?.listStatus || [];
      this.isRM = this.prop?.isRM;
      this.formSearch.controls.isRmScr.setValue(this.isRM);
      this.search();
    } else {
      let paramsUser = {
        page: 0,
        isActive:1,
        size: maxInt32,
        searchAdvanced: '',
      };
      forkJoin([
        this.commonService.getCommonCategory(CommonCategory.CAMPAIGN_FORM).pipe(catchError((e) => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.CAMPAIGN_TYPE).pipe(catchError((e) => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.CAMPAIGN_MONTH_CONFIG_BEFORE).pipe(catchError((e) => of(undefined))),
        this.translate.get('campaign').pipe(catchError((e) => of(undefined))),
        this.categoryService
          .getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW)
          .pipe(catchError(() => of(undefined))),
        // this.rmApi
        //   .post('findAll', {
        //      page: 0,
        //      size: maxInt32,
        //      crmIsActive: true,
        //      branchCodes: [],
        //      rmBlock: "SME",
        //      rsId: this.objFunction?.rsId,
        //      scope: Scopes.VIEW,
        //   })
        //   .pipe(catchError(() => of(undefined))),
        this.userService.searchUser(paramsUser).pipe(catchError((e) => of(undefined))),
        this.categoryService
          .getAllBranches(0, maxInt32)
          .pipe(catchError(() => of(undefined))),
      ]).subscribe(([listForm, listPurpose, backdate, campaign, branchOfUser, listRmManagement,allBranch]) => {
        this.listFormality = listForm?.content || [];
        this.listFormality.unshift({ code: '', name: this.fields.all });
        this.listPurpose = listPurpose?.content || [];
        this.listPurpose.unshift({ code: '', name: this.fields.all });
        this.listStatus.push({ code: '', name: this.fields.all });

        Object.keys(CampaignStatus).forEach((key) => {
          this.listStatus.push({ code: CampaignStatus[key], name: campaign.status[CampaignStatus[key].toLowerCase()] });
        });
        this.isRM = _.isEmpty(branchOfUser);
        this.prop = {
          listFormality: this.listFormality,
          listPurpose: this.listPurpose,
          listStatus: this.listStatus,
          statusCampaign: this.statusCampaign,
          isRM: _.isEmpty(branchOfUser)
        };
        this.sessionService.setSessionData(FunctionCode.CAMPAIGN, this.prop);

        // -----------------------
        const listRm = [];
         listRmManagement?.content?.forEach((item) => {
            if (!_.isEmpty(item?.code)) {
               listRm.push({
                  code: _.trim(item?.username),
                  displayName: _.trim(item?.code) + ' - ' + _.trim(item?.fullName),
                  branchCode: _.trim(item?.branch),
               });
            }
         });
         if (!listRm?.find((item) => item.code === this.currUser?.code)) {
            listRm.push({
               code: this.currUser?.code,
               displayName:
                  Utils.trimNullToEmpty(this.currUser?.code) + ' - ' + Utils.trimNullToEmpty(this.currUser?.fullName),
               branchCode: this.currUser?.branch,
            });
         }
         this.listRmManagerTerm = [...listRm];
         this.listRmManager = [...listRm];
        this.listBranchesManager =
        allBranch?.content?.map((item) => {
            return {
              code: item.code,
              name: `${item.code} - ${item.name}`,
            };
          }) || [];
        if (!_.find(this.listBranchesManager, (item) => item.code === this.currUser?.branch)) {
          this.listBranchesManager.push({
            code: this.currUser?.branch,
            name: `${this.currUser?.branch} - ${this.currUser?.branchName}`,
          });
        }
        if (!_.find(this.listBranchesManager, (item) => item.code === this.currUser?.branch)) {
          this.listBranchesManager.push({
            code: this.currUser?.branch,
            name: `${this.currUser?.branch} - ${this.currUser?.branchName}`,
          });
        }

         if (this.listRmManager.length > 1)
            this.listRmManager.unshift(this.allRM);
        else {
           if (this.listRmManager.length == 1) {
              let rm = this.listRmManager[0];
              this.formSearch.get('rmCode').setValue(rm.code);
           }
        }
        if (this.listBranchesManager.length >= 1)
          this.listBranchesManager.unshift(this.allBranch);
         else {
            if (this.listBranchesManager.length == 1) {
               let branch = this.listBranchesManager[0];
               this.formSearch.get('branchCodes').setValue(branch.code);
            }
         }
        this.formSearch.controls.isRmScr.setValue(this.isRM);
          var endDateCheck = backdate?.content || [];
          if (endDateCheck.length > 0) {
            var backdateCheck = new Date();
            backdateCheck.setMonth(new Date().getMonth() - endDateCheck[0].value);
            backdateCheck.setDate(1);
            this.minEndDateCheck = backdateCheck;
            if(this.minEndDateLast == null || this.minEndDateLast < this.minEndDateCheck)
              this.minEndDateLast = this.minEndDateCheck;
          }
        this.search(true);
      },
      (e) => {
         this.messageService.error(e.error.description);
         this.isLoading = false;
      });
    }
    this.formSearch.get('createdBranch').valueChanges.subscribe((value) => {
      this.formSearch.get('createdUsername').setValue('');
      if (value == null || value == '') {
        this.listRmManager = this.listRmManagerTerm;
      } else {
        this.listRmManager = _.filter(
          this.listRmManagerTerm,
          (item) => item.branchCode === value
        );
      }
       if (this.listRmManager.length > 1) {
          this.listRmManager.unshift(this.allRM);
       } else {
          if (this.listRmManager.length == 1) {
             let rm = this.listRmManager[0];
             this.formSearch.get('createdUsername').setValue(rm.code);
          }
       }
    });
  }

  ngAfterViewInit() {
    this.formSearch.controls.startDateFirst.valueChanges.subscribe((value) => {
      if (moment(value).isValid()) {
        const startDateLast = this.formSearch.controls.startDateLast.value;
        if (moment(startDateLast).isValid() && moment(value).isAfter(moment(startDateLast))) {
          this.formSearch.controls.startDateLast.setValue(value);
        }
        this.minStartDateLast = value;
      } else {
        this.minStartDateLast = null;
      }
    });
    this.formSearch.controls.startDateLast.valueChanges.subscribe((value) => {
      if (moment(value).isValid()) {
        const startDateFirst = this.formSearch.controls.startDateFirst.value;
        if (moment(startDateFirst).isValid() && moment(value).isBefore(moment(startDateFirst))) {
          this.formSearch.controls.startDateFirst.setValue(value);
        }
        this.maxStartDateFirst = value;
      } else {
        this.maxStartDateFirst = null;
      }
    });
    this.formSearch.controls.endDateFirst.valueChanges.subscribe((value) => {
      if (moment(value).isValid()) {
        const endDateLast = this.formSearch.controls.endDateLast.value;
        if (moment(endDateLast).isValid() && moment(value).isAfter(moment(endDateLast))) {
          this.formSearch.controls.endDateLast.setValue(value);
        }
        this.minEndDateLast = value;
      } else {
        this.minEndDateLast = null;
      }
      if(this.minEndDateLast == null || this.minEndDateLast < this.minEndDateCheck)
        this.minEndDateLast = this.minEndDateCheck;
    });
    this.formSearch.controls.endDateLast.valueChanges.subscribe((value) => {
      if (moment(value).isValid()) {
        const endDateFirst = this.formSearch.controls.endDateFirst.value;
        if (moment(endDateFirst).isValid() && moment(value).isBefore(moment(endDateFirst))) {
          this.formSearch.controls.endDateFirst.setValue(value);
        }
        this.maxEndDateFirst = value;
      } else {
        this.maxEndDateFirst = null;
      }
    });
  }

  search(isSearch?: boolean) {
    this.isLoading = true;
    let params: any = {};

    if (isSearch) {
      this.onCheckRadio();
      this.paramSearch.page = 0;
      params = cleanDataForm(this.formSearch);

      if (moment(params.startDateFirst).isValid()) {
        params.startDateFirst = moment(params.startDateFirst).format('YYYY-MM-DD');
      } else {
        delete params.startDateFirst;
      }

      if (moment(params.startDateLast).isValid()) {
        params.startDateLast = moment(params.startDateLast).format('YYYY-MM-DD');
      } else {
        delete params.startDateLast;
      }

      if (moment(params.endDateFirst).isValid()) {
        params.endDateFirst = moment(params.endDateFirst).format('YYYY-MM-DD');
      } else {
        delete params.endDateFirst;
      }

      if (moment(params.endDateLast).isValid()) {
        params.endDateLast = moment(params.endDateLast).format('YYYY-MM-DD');
      } else {
        delete params.endDateLast;
      }
    } else {
      if (!this.prevParams) {
        return;
      }
      params = this.prevParams;
    }
    Object.keys(this.paramSearch).forEach((key) => {
      params[key] = this.paramSearch[key];
    });

    if (this.isViewFromPopup) {
      this.customerType = 'INDIV';
      this.formSearch.controls.customerType.setValue('INDIV');
      this.formSearch.controls.customerType.disable();
      params.branchCodes = this.listBranchChoose;
      params.customerType = 'INDIV';
      params.isGyb = true;
    } else {
      params.branchCodes = null;
    }
    params.isRmScr = this.isRM;

    if (this.customerType === 'INDIV') {
      params.customerType = 'INDIV';
      this.campaignService.searchForPopup(params).subscribe(
        (result) => {
          if (result) {
            this.prevParams = params;
            this.prop.prevParams = params;
            result?.content?.forEach((item) => {
              item.campaignLevel = Utils.trimNullToEmpty(item.scope) !== '' ? this.fields[item.scope.toLowerCase()] : '';
            });
            this.listData = result?.content || [];
            this.pageable = {
              totalElements: result.totalElements,
              totalPages: result.totalPages,
              currentPage: result.number,
              size: global.userConfig.pageSize,
            };
          }
          this.sessionService.setSessionData(FunctionCode.CAMPAIGN, this.prop);
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
    } else if (this.customerType === 'SME') {
      params.customerType = 'SME';
      if(params.createdUsername == undefined)
        params.createdUsername = ''
      this.campaignService.searchSME(params).subscribe(
        (result) => {
          if (result) {
            this.prevParams = params;
            this.prop.prevParams = params;
            result?.content?.forEach((item) => {
              item.campaignLevel = Utils.trimNullToEmpty(item.scope) !== '' ? this.fields[item.scope.toLowerCase()] : '';
            });
            this.listDataSME = result?.content || [];
            this.pageable = {
              totalElements: result.totalElements,
              totalPages: result.totalPages,
              currentPage: result.number,
              size: global.userConfig.pageSize,
            };
          }
          this.sessionService.setSessionData(FunctionCode.CAMPAIGN, this.prop);
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
    }
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.page = pageInfo.offset;
    this.search(false);
  }

  create() {

    if (this.isViewFromPopup) {
      const modal = this.modalService.open(CampaignsCreateModal, { windowClass: 'campaigns-create-modal' });
      modal.componentInstance.listBranchChoose = this.listBranchChoose;
      modal.result
        .then((res) => {
          if (res) {
            this.search(true);
          }
        })
        .catch(() => { });
    } else {
      if (this.isINDIV && this.isSME) {
        if (this.isRM) {
          this.router.navigate([this.router.url, 'create']);
          return;
        }
        const dialogRef = this.dialog.open(ChooseCreateModalComponent);

        dialogRef.afterClosed().subscribe(data => {
          if (!data) return;

          const link = data.divisionCode === Division.INDIV ? 'create' : 'create-sme';

          if (data.divisionCode === Division.SME && this.isRM) {
            this.messageService.warn(this.notificationMessage.no_permission_to_create_campaign);
            return;
          }

          this.router.navigate([this.router.url, link]);
        });
        return;
      }

      if (this.isINDIV) {
        this.router.navigate([this.router.url, 'create']);

      } else if (this.isSME) {
        if (this.isRM) {
          this.messageService.warn(this.notificationMessage.no_permission_to_create_campaign);
          return;
        }

        this.router.navigate([this.router.url, 'create-sme']);
      }
    }
  }

  update(item) {
    switch (item.blockCode) {
      case Division.SME:
        const isHO = this.isHO || this.currUser?.branch === BRANCH_HO;
        const isNotCreator = item.createdUsername !== this.currUser?.username;
        if (isNotCreator && !isHO) {
          this.messageService.warn(this.notificationMessage.no_permission_for_update_campaign)
          return;
        }
        this.router.navigate([this.router.url, 'update-sme', item.id], {
          queryParams: {
            campaignCode: item.code,
          },
        });
        break;
      case Division.INDIV:
        this.router.navigate([this.router.url, 'update', item.id]);
        break;
    }
  }

  getPurposeDesc(value) {
    return (
      this.listPurpose?.find((item) => {
        return item.code === value;
      })?.name || ''
    );
  }

  getStatusDesc(value) {
    return (
      this.listStatus?.find((item) => {
        return item.code === value;
      })?.name || ''
    );
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      if (this.isViewFromPopup) {
        this.onCheckRadio(item);
        return;
      } else {
        if (item.blockCode === 'SME') {
          this.router.navigate([this.router.url, 'detail-sme', item.id], {
            queryParams: {
              campaignCode: item.code,
            },
          });
        } else {
          this.router.navigate([this.router.url, 'detail', item.id], {
            queryParams: {
              // showBtnAddCampaign: item.typeId === 'PTKH' && item.status === 'ACTIVATED'
              showBtnAddCampaign: (item.isRmInsert === true || this.currUser?.branch === item?.createdByBranchCode) && item.status === 'ACTIVATED'
                && item.blockCode === 'INDIV' && new Date((new Date()).setHours(0, 0, 0, 0)) <= new Date(item.endDate),
              listBranch: item.branchCodes
            }
          });
        }

      }
    }
  }


  isUpdate(item) {
    return (
      this.objFunction?.update &&
      item.status === CampaignStatus.InProgress &&
      ((item.branchCode === BRANCH_HO && this.currUser?.branch === BRANCH_HO) ||
        (item.createdBy === this.currUser?.username && item.branchCode !== BRANCH_HO))
    );
  }

  isUpdateSME(item) {
    return (
      this.objFunction?.updateSME &&
      item.blockCode === Division.SME &&
      [CampaignStatus.InProgress, CampaignStatus.Activated].includes(item.status)
      // (this.isHO || item.createdBy === this.currUser?.username)
    );
  }

  onCheckRadio(item?: any) {
    this.isSelectedRadio = [];
    if (item) {
      this.isSelectedRadio.push(item);
    }
    this.onChangeCampain.emit(item);
  }

  isCheckedRadio(item) {
    return this.isSelectedRadio?.filter((element) => element.id === item.id).length > 0;
  }

  get isSME(): boolean {
    return this.listDivision.find(item => item.code === Division.SME);
  }

  get isINDIV(): boolean {
    return this.listDivision.find(item => item.code === Division.INDIV);
  }

  changeBlock(event) {
    this.customerType = event.value;
  }

  getRMInfo(row) {
    if(row?.createdRMCode)
      return row?.createdRMCode?.concat(' - ').concat(row?.createdFullName)
    else
      return row?.createdFullName
  }
  getBranchInfo(row) {
    return row?.branchCode?.concat(' - ').concat(row?.branchName)
  }

  get isHO(): boolean {
    return this.objFunction.HO;
  }

  deleteCampaign(item) {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.campaignService.deleteCampaign(item.id).subscribe(() => {
          this.listData = this.listData?.filter((i) => i.id !== item.id);
          this.search();
          this.messageService.success(this.notificationMessage.success);
        }, (e) => {
          this.isLoading = false;
          if (!_.isEmpty(e?.error?.description)) {
            this.messageService.error(e.error.description);
          } else {
            this.messageService.error(this.notificationMessage.error);
          }
        });
      }
    });
  }

  canDeleteSME(row: any): boolean {
    return this.objFunction.updateSME &&
    [CampaignStatus.InProgress].includes(row.status) &&
      (
        this.isHO ||
        (
          row.branchCode === BRANCH_HO &&
          this.currUser?.branch === BRANCH_HO
        ) ||
        row.createdUsername === this.currUser?.username
      )
  }

  getStatusSME(value) {
    return (
      this.listStatusSME?.find((item) => {
        return item.code === value;
      })?.name || ''
    );
  }

  canCreate(): boolean {

    if (this.isHO || this.currUser?.branch === BRANCH_HO) {
      return true;
    }

    if (this.isSME && this.isINDIV) {
      return this.objFunction?.updateSME && this.objFunction?.create;
    }

    if (this.isSME) {
      return this.objFunction?.updateSME;
    }

    if (this.isINDIV) {
      return this.objFunction?.create;
    }
  }
}
