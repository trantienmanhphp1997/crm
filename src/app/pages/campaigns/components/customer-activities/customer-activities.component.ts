import {
  Component,
  OnInit,
  SimpleChanges,
  OnChanges,
  ViewChild,
  Input,
  HostBinding,
  Output,
  EventEmitter,
} from '@angular/core';
import {CampaignsService} from '../../services/campaigns.service';
import {CommonCategoryService} from 'src/app/core/services/common-category.service';
import _ from 'lodash';
import {Pageable} from 'src/app/core/interfaces/pageable.interface';
import {Utils} from '../../../../core/utils/utils';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {global} from '@angular/compiler/src/util';
import {Scopes, CommonCategory, ScreenType, SessionKey} from 'src/app/core/utils/common-constants';
import {TranslateService} from '@ngx-translate/core';
import {forkJoin, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {ActivityRecordComponent} from '../activity.record/activity.record.component';
import {ActivityService} from 'src/app/core/services/activity.service';
import {ConfirmDialogComponent} from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import {NotifyMessageService} from '../../../../core/components/notify-message/notify-message.service';
import {WorkflowService} from 'src/app/core/services/workflow.service';
import {SessionService} from 'src/app/core/services/session.service';
import * as moment from 'moment';

@Component({
  selector: 'customer-activities',
  templateUrl: './customer-activities.component.html',
  styleUrls: ['./customer-activities.component.scss'],
})
export class CustomerActivitiesComponent implements OnInit, OnChanges {
  constructor(
    private service: CampaignsService,
    private modalService: NgbModal,
    private translate: TranslateService,
    private commonService: CommonCategoryService,
    private activityService: ActivityService,
    private messageService: NotifyMessageService,
    private campaignService: CampaignsService,
    private workflowService: WorkflowService,
    private session: SessionService
  ) {
    this.currUser = this.session.getSessionData(SessionKey.USER_INFO);
    this.messages = global.messageTable;
    this.translate.get(['fields', 'notificationMessage', 'confirmMessage']).subscribe((result) => {
      this.notificationMessage = result.notificationMessage;
      this.fields = result.fields;
    });
  }

  @ViewChild('table') table: DatatableComponent;
  @HostBinding('class.app__right-content') appRightContent = true;
  @ViewChild('customers') customers: any;
  @Input() campaignId: string;
  @Input() rsId: string;
  @Input() customerId: string;
  @Input() isReload: boolean;
  @Input() model: any;
  @Input() isHistory: boolean;
  @Output() modelChange = new EventEmitter();
  @Output() loading: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() backHours: number;

  currUser: any;
  models: Array<any>;
  pageable: Pageable;
  customerTypes: Array<any> = [];
  campaign: any;
  isFisrt: boolean = true;
  isLoading: boolean;
  fields: any;
  messages: any;
  textSearch: string;
  activity_results: Array<any>;
  notificationMessage: any;
  activities: Array<any> = [];
  activity_types: Array<any> = [];
  entity: any = {
    activityType: '',
  };
  listLanes = [];

  paramSearch = {
    size: global.userConfig.pageSize,
    page: 0,
    rsId: '',
    scope: Scopes.VIEW,
  };
  prevParams = this.paramSearch;

  ngOnInit(): void {
    forkJoin([
      this.commonService
        .getCommonCategory(CommonCategory.CONFIG_ACTIVITY_RESULT)
        .pipe(catchError((e) => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.ACTIVITY_NEXT_TYPE).pipe(catchError((e) => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.CONFIG_TYPE_ACTIVITY).pipe(catchError((e) => of(undefined))),
      this.campaignService.getById(this.campaignId).pipe(catchError((e) => of(undefined))),
      this.workflowService.findAllProcess().pipe(catchError((e) => of(undefined))),
    ]).subscribe(([results, activities, types, itemCampaign, listLanes]) => {
      this.campaign = itemCampaign;
      this.listLanes = listLanes;
      this.activity_results = _.get(results, 'content') || [];
      this.activities = _.get(activities, 'content') || [];
      this.activity_types = _.get(types, 'content') || [];
      this.activity_types = [
        {
          code: '',
          name: _.get(this.fields, 'all'),
        },
        ...this.activity_types,
      ];
    });
    this.reload();
  }

  ngOnChanges(change: SimpleChanges) {
    if (Utils.isNotNull(_.get(change, 'isReload.currentValue'))) {
      this.reload(true);
    }
  }

  setPage(pageInfo) {
    if (this.isFisrt) {
      return;
    }
    this.paramSearch.page = pageInfo.offset;
    this.reload(false);
  }

  search() {
    this.reload(true);
  }

  reload(isSearch?: boolean) {
    this.isLoading = true;
    this.loading.emit(true);
    let params: any = {};
    if (isSearch) {
      this.paramSearch.page = 0;
      if (Utils.isStringNotEmpty(this.textSearch)) {
        this.paramSearch = _.merge(this.paramSearch, {
          search: this.textSearch,
        });
      }
      if (Utils.isStringNotEmpty(this.entity.activityType)) {
        this.paramSearch = _.merge(this.paramSearch, {
          activityType: _.get(this.entity, 'activityType'),
        });
      } else {
        this.paramSearch = _.merge(this.paramSearch, {
          activityType: '',
        });
      }
      if (Utils.isNotNull(_.get(this.entity, 'dateStart'))) {
        this.paramSearch = _.merge(this.paramSearch, {
          dateStart: _.get(this.entity, 'dateStart'),
        });
      } else {
        this.paramSearch = _.merge(this.paramSearch, {
          dateStart: '',
        });
      }
    } else {
      if (!this.prevParams) {
        return;
      }
      params = this.prevParams;
    }
    Object.keys(this.paramSearch).forEach((key) => {
      params[key] = this.paramSearch[key];
    });
    params.campaignId = this.campaignId;
    params.rsId = this.rsId;
    params.parentId = this.customerId;
    this.service.findAllActivities(params).subscribe(
      (result) => {
        if (result) {
          this.models = _.get(result, 'content') || [];
          this.models.forEach((item) => {
            let performer = item?.rmCode || '';
            if (item?.rmCode) {
              performer += ' - ' + item?.rmFullName;
            } else {
              performer += item?.rmFullName;
            }
            console.log('performer: ', performer);
            item.performer = performer;
          })
          this.prevParams = params;
          this.pageable = {
            totalElements: _.get(result, 'totalElements'),
            totalPages: _.get(result, 'totalPages'),
            currentPage: _.get(result, 'number'),
            size: _.get(global, 'userConfig.pageSize'),
          };
        }
        this.isLoading = false;
        this.loading.emit(false);
        this.isFisrt = false;
      },
      () => {
        this.isLoading = false;
        this.loading.emit(false);
        this.isFisrt = false;
      }
    );
  }

  getValue(row, key) {
    return _.get(row, key);
  }

  getFeatureActivity(row, key) {
    let value = _.get(row, key);
    return _.get(
      _.chain(this.activities)
        .filter((x) => x.code == value)
        .first()
        .value(),
      'name'
    );
  }

  checkValue(row, key) {
    return Utils.isStringNotEmpty(_.get(row, key));
  }

  getActivityResultName(row, key) {
    return _.get(
      _.chain(this.activity_results)
        .filter((x) => x.code == _.get(row, key))
        .first()
        .value(),
      'name'
    );
  }

  getActivityTypeName(row, key) {
    return _.get(
      _.chain(this.activity_types)
        .filter((x) => x.code == _.get(row, key))
        .first()
        .value(),
      'name'
    );
  }

  onActive($event) {
    if ($event.type === 'dblclick') {
      let username = _.get($event, 'row.rmUserName');
      if (_.get(this.currUser, 'username') !== username) {
        return;
      }
      this.activityService
        .getByCode(_.get($event, 'row.id'))
        .pipe(catchError((e) => undefined))
        .subscribe((data) => {
          this.modelChange.emit(data);
        });
    }
  }

  edit(row) {
    const params = {
      campaignId: this.campaignId
    }
    this.activityService
      .getByCodeAndCampaignId(_.get(row, 'id'), params)
      .pipe(catchError((e) => undefined))
      .subscribe((data) => {
        this.modelChange.emit(data);
      });
  }

  delete(row) {
    const params = {
      campaignId: this.campaignId
    }
    const confirm = this.modalService.open(ConfirmDialogComponent, {windowClass: 'confirm-dialog'});
    confirm.result
      .then((res) => {
        if (res) {
          this.isLoading = true;
          this.loading.emit(true);
          this.activityService.deleteByCodeAndCampaignId(_.get(row, 'id'), params).subscribe(
            () => {
              this.messageService.success(this.notificationMessage.success);
              this.isLoading = false;
              this.loading.emit(false);
              this.modelChange.emit(null);
              this.reload();
            },
            (e) => {
              this.messageService.error(this.notificationMessage.error);
              this.isLoading = false;
              this.loading.emit(false);
            }
          );
        }
      })
      .catch(() => {
      });
  }

  recordActivity(row: any) {
    const parent: any = {};
    parent.parentType = _.get(row, 'parentType');
    parent.parentId = _.get(row, 'parentId');
    parent.parentName = _.get(row, 'parentName');
    parent.campaignId = this.campaignId;
    parent.rsId = this.rsId;
    this.activityService
      .getByCode(_.get(row, 'id'))
      .pipe(catchError((e) => undefined))
      .subscribe((data) => {
        parent.data = this.convertData(data);
        const modal = this.modalService.open(ActivityRecordComponent, {
          windowClass: 'create-activity-modal',
          scrollable: true,
        });
        modal.componentInstance.parent = parent;
        modal.componentInstance.type = ScreenType.Update;
        modal.componentInstance.isShowFuture = true;
        modal.result.then((res) => {
          if (res) {
            this.reload();
          }
        });
      });
  }

  showPermission(row) {
    return _.get(this.currUser, 'username') === _.get(row, 'rmUserName');
  }

  convertData(data) {
    return {
      preId: _.get(data, 'id'),
      id: _.get(data, 'futureActivity.id'),
      parentName: _.get(data, 'parentName'),
      parentType: _.get(data, 'parentType'),
      activityType: _.get(data, 'futureActivity.activityType'),
      dateStart: new Date(),
      activityResult: _.get(data, 'futureActivity.activityResult'),
      location: _.get(data, 'futureActivity.location'),
      note: _.get(data, 'futureActivity.note'),
    };
  }

  checkDate(row) {
    let start = moment(_.get(row, 'dateStart')).add('hour', this.backHours).format('DD-MM-YYYY HH:mm');
    let now = moment().format('DD-MM-YYYY HH:mm');
    return start > now;
  }

  isDelete(row) {
    return (
      row.createdBy === this.currUser?.username &&
      moment(row.dateStart)
        .add(+this.backHours, 'hour')
        .isSameOrAfter(moment().startOf('day'))
    );
  }

  isUpdate(row) {
    return (
      row.createdBy === this.currUser?.username &&
      moment(row.dateStart)
        .add(+this.backHours, 'hour')
        .isSameOrAfter(moment().startOf('day'))
    );
  }
}
