import {Component, OnInit, ViewEncapsulation, HostBinding, Input} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ConfirmDialogComponent} from "../../../../shared/components";
import {cleanDataForm} from "../../../../core/utils/function";

@Component({
  selector: 'app-campaign-rm-modal',
  templateUrl: './campaigns-create-modal.html',
  styleUrls: ['./campaigns-create-modal.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CampaignsCreateModal implements OnInit {
  @HostBinding('class.campaigns-create-content') listContent = true;
  @Input() listBranchChoose: Array<any>;

  isLoading = false;
  constructor(private modalActive: NgbActiveModal,
              private modalService: NgbModal) {}
  data = [];

  showLoading(isShowLoading) {
    this.isLoading = isShowLoading;
  }

  ngOnInit() {
  }

  closeModal(value? :boolean) {
    this.modalActive.close(value);
  }
}
