import {Component, Injector, OnInit} from "@angular/core";
import {BaseComponent} from "../../../../core/components/base.component";
import {catchError} from "rxjs/operators";
import {forkJoin, of} from "rxjs";
import {BRANCH_HO, CampaignProcessType, FunctionCode, Scopes} from "../../../../core/utils/common-constants";
import {CategoryService} from "../../../system/services/category.service";
import _ from 'lodash';
import {CampaignsService} from "../../services/campaigns.service";
import {ListImportErrorComponent} from "../list-import-error/list-import-error.component";
@Component({
  selector: 'app-campaign-import-customer',
  templateUrl: 'campaign-import-customer.component.html',
  styleUrls: ['campaign-import-customer.component.scss']
})
export class CampaignImportCustomerComponent extends BaseComponent implements OnInit{
  fileName: string;
  fileImport: File;
  files: any;
  fileId: string;
  hasImportError = false;
  listProcessAssignType = [];
  processAssignType: string;
  processTypeConfig : any;
  objFunction: any;
  prevParam: any;
  isHO = false;
  isRM = false;
  isCBQL = false;
  COMPLETE = 'COMPLETE';
  FAIL = 'FAIL';
  campaign: any;
  constructor(injector:Injector, private categoryService: CategoryService, private campaignService: CampaignsService) {
    super(injector);
    this.prevParam = this.router.getCurrentNavigation()?.extras?.state;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${this.prevParam?.functionCode}`);
  }
  ngOnInit(): void {
    console.log('objFunction: ', this.objFunction);
    forkJoin([
      this.translate.get('campaign').pipe(catchError(() => of(undefined))),
      this.campaignService.getById(this.prevParam.id).pipe(catchError(() => of(undefined))),
      this.categoryService
        .getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined)))
    ])
      .subscribe(([res,campaign,branchOfUser]) => {
        this.processTypeConfig = res?.processType;
        this.isRM = _.isEmpty(branchOfUser);
        this.campaign = campaign;
        if(this.currUser?.branch === BRANCH_HO){
          this.isHO = true;
        }
        else if(branchOfUser.length > 0){
          this.isCBQL = true;
        }
        this.handleMapData();
        this.processAssignType = this.listProcessAssignType[0]?.code;
      });
  }
  handleMapData(){
    if (this.isRM) {
      Object.keys(CampaignProcessType).forEach((key) => {
        if (CampaignProcessType[key] === CampaignProcessType.RM) {
          this.listProcessAssignType.push({
            code: CampaignProcessType[key],
            name: this.processTypeConfig[key.toLowerCase()],
          });
        }
      });
    } else if (this.isHO) {
      Object.keys(CampaignProcessType).forEach((key) => {
        this.listProcessAssignType.push({
          code: CampaignProcessType[key],
          name: this.processTypeConfig[key.toLowerCase()],
        });
      });
      console.log('listProcessAssignType: ', this.listProcessAssignType);
    } else if (this.isCBQL) {
      Object.keys(CampaignProcessType).forEach((key) => {
        if (CampaignProcessType[key] !== CampaignProcessType.MB247) {
          this.listProcessAssignType.push({
            code: CampaignProcessType[key],
            name: this.processTypeConfig[key.toLowerCase()],
          });
        }
      });
      console.log('listProcessAssignType: ', this.listProcessAssignType);
    }
  }
  onchangeFile(inputFile){
    if(this.fileName){
      return;
    }
    inputFile.click();
  }
  handleFileInput(file){
    if(file && file.length > 0){
      this.fileImport = file.item(0);
      this.fileName = file.item(0).name;
    }
  }
  clearFile(){
    this.fileName = undefined;
    this.fileImport = undefined;
    this.files = undefined;
    this.fileId = undefined;
    this.hasImportError = false;
  }
  importFile(){
    if(this.fileImport && this.fileName){
      this.isLoading = true;
      const formData: FormData = new FormData();
      formData.append('file', this.fileImport);
      formData.append('campaignId', this.prevParam?.id);
      if(this.isRM){
        this.campaignService.importByRm(formData).subscribe((res) => {
          this.fileId = res?.id;
          this.checkImportSuccess(this.fileId);
        }, (e) => {
          if(e?.error){
            this.messageService.warn(e?.error?.description);
          }
          else{
            this.messageService.error(this.notificationMessage.error);
          }
          this.isLoading = false;
        });
      }
      else{
        this.campaignService.import(formData).subscribe((res) => {
          this.fileId = res?.id;
          this.checkImportSuccess(this.fileId);
        }, (e) => {
          if(e?.error){
            this.messageService.warn(e?.error.description);
          }
          else{
            this.messageService.error(this.notificationMessage.error);
          }
          this.isLoading = false;
        });
      }
    }
  }
  checkImportSuccess(fileId){
    if(fileId){
      const countInterval = 0;
      const interval = setInterval(() => {
        this.campaignService.checkFileImport(fileId).subscribe((res) => {
          if(res?.status === this.COMPLETE){
            this.campaignService.searchImportError({fileId: this.fileId, campaignId: this.prevParam.id}).subscribe(
              (list) => {
                this.isLoading = false;
                if(list?.content?.length > 0){
                  this.hasImportError = true;
                  this.openModalError();
                }
                else{
                  this.messageService.success(this.notificationMessage.success);
                }
              }, () => {
                this.isLoading = false;
              }
            )
            clearInterval(interval);
          }
          else if(res?.status === this.FAIL){
            if (res?.msgError?.includes('FILE_DOES_NOT_EXCEED_RECORDS')) {
              const maxRecord = res?.msgError?.replace('FILE_DOES_NOT_EXCEED_RECORDS_', '');
              const type = this.fileName?.split('.')[this.fileName?.split('.')?.length - 1];
              this.translate
                .get('notificationMessage.FILE_DOES_NOT_EXCEED_RECORDS', {number: maxRecord, type})
                .subscribe((message) => {
                  this.messageService.error(message);
                });
            } else if (res?.msgError === 'FILE_NO_CONTENT_EXCEPTION') {
              this.messageService.error(this.notificationMessage.FILE_NO_CONTENT_EXCEPTION);
            } else {
              this.messageService.error(this.notificationMessage.CANNOT_READ_DATA_FROM_FILE);
            }
            this.isLoading = false;
            clearInterval(interval);
          }
        })
      }, 5000);
      if(countInterval > 360){
        clearInterval(interval);
      }
    }

  }
  openModalError(){
    const modalError = this.modalService.open(ListImportErrorComponent, {windowClass: 'list__campaign-error'});
    modalError.componentInstance.fileId = this.fileId;
    modalError.componentInstance.campaignId = this.prevParam.id;
    modalError.result
      .then(() => {
        const formData: FormData = new FormData();
        formData.append('fileId', this.fileId);
        this.campaignService.clearDataImport(formData).subscribe(() => {
        });
        this.clearFile();
      })
      .catch(() => {
      });
  }
  downloadTemplate(){
    if (this.isRM) {
      window.open('/assets/template/template_Danh_sach_khach_hang_muc_tieu_CRM_RM.xls', '_self');
    } else {
      window.open('/assets/template/template_Danh_sach_khach_hang_muc_tieu_CRM.xls', '_self');
    }
  }
  save(){
    this.isLoading = true;
    const formData: FormData = new FormData();
    formData.append('fileId', this.fileId);
    formData.append('campaignId', this.prevParam.id);
    formData.append('assignProcessType', this.processAssignType);
    formData.append('isRm', `${this.isRM}`);
    if (this.fileId) {
      this.campaignService.writeDataImport(formData).subscribe(
        () => {
          this.isLoading = false;
          this.messageService.success(this.notificationMessage.success);
          this.back();
        },
        (e) => {
          this.isLoading = false;
          if (e?.status === 0) {
            this.messageService.error(this.notificationMessage.E001);
          } else if (e?.error?.description) {
            this.messageService.error(e?.error?.description);
          } else {
            this.messageService.error(this.notificationMessage.error);
          }
        }
      );
    } else {
      this.isLoading = false;
      this.back();
      this.messageService.success(this.notificationMessage.success);
    }
  }
  back(){
    this.isLoading = true;
    if(this.prevParam?.functionCode === FunctionCode.CAMPAIGN){
      this.router.navigate(['/campaigns/campaign-management/detail/', this.prevParam.id], {
        queryParams: {
          showBtnAddCampaign:  this.campaign.status === 'ACTIVATED'
            && this.campaign.blockCode === 'INDIV' && new Date((new Date()).setHours(0, 0, 0, 0)) <= new Date(this.campaign.endDate),
          listBranch: this.campaign.branchCodes
        }
      });
    }
    else if(this.prevParam?.functionCode === FunctionCode.CAMPAIGN_RM){
      this.router.navigate(['/campaigns/campaign-rm/detail/', this.prevParam.id], {
        queryParams: {
          // showBtnAddCampaign: this.campaign.typeId === 'PTKH' && this.campaign.status === 'ACTIVATED'
          showBtnAddCampaign: (this.campaign.isRmInsert === true || this.currUser?.branch === this.campaign?.createdByBranchCode) && this.campaign.status === 'ACTIVATED'
            && this.campaign.blockCode === 'INDIV' && new Date((new Date()).setHours(0, 0, 0, 0)) <= new Date(this.campaign.endDate),
          listBranch: this.campaign.branchCodes
        }
      });
    }
  }
}
