import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import _ from 'lodash';

@Component({
  selector: 'app-customer-campaign-modal',
  templateUrl: './customer-campaign-modal.component.html',
  styleUrls: ['./customer-campaign-modal.component.scss'],
})
export class CustomerCampaignModalComponent implements OnInit {
  constructor(private modalActive: NgbActiveModal, private ref: ChangeDetectorRef) { }
  @Input() parent: any;
  customerCode: string;
  rsId: string;
  disabled: boolean = false;
  titleHeader: string;
  isLoading: boolean = false;

  ngOnInit(): void {
    this.customerCode = _.get(this.parent, 'parentId');
    this.rsId = _.get(this.parent, 'rsId');
    this.titleHeader = 'title.historyCampaignCustomer';
  }

  closeModal() {
    this.modalActive.close(false);
  }

  change($event) {
    this.isLoading = $event;
    this.ref.detectChanges();
  }
}
