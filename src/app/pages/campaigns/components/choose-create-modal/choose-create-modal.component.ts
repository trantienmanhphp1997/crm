import { Component, HostBinding, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';
import { Division } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'app-choose-create-modal',
  templateUrl: './choose-create-modal.component.html',
  styleUrls: ['./choose-create-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ChooseCreateModalComponent implements OnInit {
  @HostBinding('choose-create-modal') classCreateCalendar = true;
  constructor(
    private router: Router,
    private dialogRef: MatDialogRef<ChooseCreateModalComponent>
  ) { }

  ngOnInit(): void {
  }

  create(divisionCode: string): void {
    this.dialogRef.close({divisionCode});
    
  }
}
