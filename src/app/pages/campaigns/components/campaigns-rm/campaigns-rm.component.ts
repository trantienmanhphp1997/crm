import { forkJoin, of } from 'rxjs';
import { global } from '@angular/compiler/src/util';
import {AfterViewInit, Component, EventEmitter, Injector, Input, OnInit, Output} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';
import { cleanDataForm } from 'src/app/core/utils/function';
import { CampaignsService } from '../../services/campaigns.service';
import {CampaignStatus, FunctionCode, CommonCategory, Scopes, SessionKey} from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { Utils } from 'src/app/core/utils/utils';
import * as moment from 'moment';

@Component({
  selector: 'app-campaigns-rm',
  templateUrl: './campaigns-rm.component.html',
  styleUrls: ['./campaigns-rm.component.scss'],
})
export class CampaignsRmComponent extends BaseComponent implements OnInit, AfterViewInit {
  isLoading = false;
  listData = [];
  formSearch = this.fb.group({
    code: [''],
    name: [''],
    // status: ['ACTIVATED'],
    typeId: [''],
    purpose: [''],
    formId: [''],
    isCampaignActive: true,
    startDateFirst: [''],
    startDateLast: [''],
    endDateFirst: [''],
    endDateLast: [''],
  });
  paramSearch = {
    size: global.userConfig.pageSize,
    page: 0,
    rsId: '',
    scope: Scopes.VIEW,
    isRm: true,
    status: 'ACTIVATED',
  };
  prevParams = {
    ...this.paramSearch,
    isCampaignActive: true,
  };
  pageable: Pageable;
  prop: any;
  listStatus = [];
  statusCampaign = [];
  listPurpose = [];
  listFormality = [];
  minStartDateLast: Date;
  maxStartDateFirst: Date;
  minEndDateLast: Date;
  maxEndDateFirst: Date;
  minEndDateCheck: Date;
  @Input() isViewFromPopup = false;
  isSelectedRadio = [];
  @Output() onChangeCampain = new EventEmitter();

  constructor(injector: Injector, private campaignService: CampaignsService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CAMPAIGN_RM}`);
    this.paramSearch.rsId = _.get(this.objFunction, 'rsId');
    this.prop = this.router.getCurrentNavigation()?.extras?.state;
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.sessionService.clearSessionData(SessionKey.CUSTOMER_TARGETS);
    if (global?.previousUrl && global?.previousUrl.includes('campaigns/campaign-rm/detail')) {
      this.prop = this.sessionService.getSessionData(FunctionCode.CAMPAIGN_RM);
    }
    this.statusCampaign.push({code: null, name: this.fields.all});
    this.statusCampaign.push({code: true, name: this.fields.effective});
    this.statusCampaign.push({code: false, name: this.fields.expire});
    this.formSearch.controls.isCampaignActive.setValue(this.statusCampaign[1].code);
    if (this.prop) {
      this.prevParams = this.prop;
      this.paramSearch.page = this.prevParams?.page;
      this.prop.startDateFirst =
        this.prop?.startDateFirst && moment(this.prop?.startDateFirst).isValid()
          ? new Date(this.prop?.startDateFirst)
          : '';
      this.prop.startDateLast =
        this.prop?.startDateLast && moment(this.prop?.startDateLast).isValid()
          ? new Date(this.prop?.startDateLast)
          : '';
      this.prop.endDateFirst =
        this.prop?.endDateFirst && moment(this.prop?.endDateFirst).isValid() ? new Date(this.prop?.endDateFirst) : '';
      this.prop.endDateLast =
        this.prop?.endDateLast && moment(this.prop?.endDateLast).isValid() ? new Date(this.prop?.endDateLast) : '';
      this.formSearch.patchValue(this.prop);
    }
    forkJoin([
      this.commonService.getCommonCategory('CAMPAIGN_FORM').pipe(catchError((e) => of(undefined))),
      this.commonService.getCommonCategory('CAMPAIGN_TYPE').pipe(catchError((e) => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.CAMPAIGN_MONTH_CONFIG_BEFORE).pipe(catchError((e) => of(undefined))),
      this.translate.get('campaign').pipe(catchError((e) => of(undefined))),
    ]).subscribe(
      ([listForm, listPurpose, backdate, campaign]) => {
        this.listFormality = _.get(listForm, 'content') || [];
        this.listFormality.unshift({ code: '', name: this.fields.all });
        this.listPurpose = _.get(listPurpose, 'content') || [];
        this.listPurpose.unshift({ code: '', name: this.fields.all });
        this.listStatus.push({ code: '', name: this.fields.all });
        Object.keys(CampaignStatus).forEach((key) => {
          this.listStatus.push({ code: CampaignStatus[key], name: campaign.status[CampaignStatus[key].toLowerCase()] });
        });
        var endDateCheck = backdate?.content || [];
        if (endDateCheck.length > 0) {
          var backdateCheck = new Date();
          backdateCheck.setMonth(new Date().getMonth() - endDateCheck[0].value);
          backdateCheck.setDate(1);
          this.minEndDateCheck = backdateCheck;
          if (this.minEndDateLast == null || this.minEndDateLast < this.minEndDateCheck)
            this.minEndDateLast = this.minEndDateCheck;
        }
        this.search(this.prevParams ? false : true);
      },
      () => {
      }
    );
  }

  ngAfterViewInit() {
    this.formSearch.controls.startDateFirst.valueChanges.subscribe((value) => {
      if (moment(value).isValid()) {
        const startDateLast = this.formSearch.controls.startDateLast.value;
        if (moment(startDateLast).isValid() && moment(value).isAfter(moment(startDateLast))) {
          this.formSearch.controls.startDateLast.setValue(value);
        }
        this.minStartDateLast = value;
      } else {
        this.minStartDateLast = null;
      }
      if(this.minEndDateLast == null || this.minEndDateLast < this.minEndDateCheck)
        this.minEndDateLast = this.minEndDateCheck;
    });
    this.formSearch.controls.startDateLast.valueChanges.subscribe((value) => {
      if (moment(value).isValid()) {
        const startDateFirst = this.formSearch.controls.startDateFirst.value;
        if (moment(startDateFirst).isValid() && moment(value).isBefore(moment(startDateFirst))) {
          this.formSearch.controls.startDateFirst.setValue(value);
        }
        this.maxStartDateFirst = value;
      } else {
        this.maxStartDateFirst = null;
      }
    });
    this.formSearch.controls.endDateFirst.valueChanges.subscribe((value) => {
      if (moment(value).isValid()) {
        const endDateLast = this.formSearch.controls.endDateLast.value;
        if (moment(endDateLast).isValid() && moment(value).isAfter(moment(endDateLast))) {
          this.formSearch.controls.endDateLast.setValue(value);
        }
        this.minEndDateLast = value;
      } else {
        this.minEndDateLast = null;
      }
    });
    this.formSearch.controls.endDateLast.valueChanges.subscribe((value) => {
      if (moment(value).isValid()) {
        const endDateFirst = this.formSearch.controls.endDateFirst.value;
        if (moment(endDateFirst).isValid() && moment(value).isBefore(moment(endDateFirst))) {
          this.formSearch.controls.endDateFirst.setValue(value);
        }
        this.maxEndDateFirst = value;
      } else {
        this.maxEndDateFirst = null;
      }
    });
  }

  search(isSearch?: boolean) {
    this.isLoading = true;
    let params: any = {};
    params.isRm = true;
    if (isSearch) {
      this.onCheckRadio();
      this.paramSearch.page = 0;
      params = cleanDataForm(this.formSearch);
      console.log('form: ', this.formSearch.value);
      if (moment(params.startDateFirst).isValid()) {
        params.startDateFirst = moment(params.startDateFirst).format('YYYY-MM-DD');
      } else {
        params.startDateFirst = '';
      }
      if (moment(params.startDateLast).isValid()) {
        params.startDateLast = moment(params.startDateLast).format('YYYY-MM-DD');
      } else {
        params.startDateLast = '';
      }
      if (moment(params.endDateFirst).isValid()) {
        params.endDateFirst = moment(params.endDateFirst).format('YYYY-MM-DD');
      } else {
        params.endDateFirst = '';
      }
      if (moment(params.endDateLast).isValid()) {
        params.endDateLast = moment(params.endDateLast).format('YYYY-MM-DD');
      } else {
        params.endDateLast = '';
      }

      console.log('param search true: ', params);
    } else {
      if (!this.prevParams) {
        return;
      }
      params = this.prevParams;
      //  params.isCampaignActive = this.formSearch.controls.isCampaignActive.value;
      // params.isCampaignActive = true;
    }
    Object.keys(this.paramSearch).forEach((key) => {
      params[key] = this.paramSearch[key];
    });
    params.branchCodes = null;
    this.campaignService.searchForPopup(params).subscribe(
      (result) => {
        if (result) {
          this.prevParams = params;
          _.forEach(_.get(result, 'content'), (item) => {
            item.campaignLevel = Utils.trimNullToEmpty(item.scope) !== '' ? this.fields[item.scope.toLowerCase()] : '';
          });
          this.listData = result?.content || [];
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: global.userConfig.pageSize,
          };
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.page = _.get(pageInfo, 'offset');
    this.search(false);
  }

  getStatusDesc(value) {
    return _.get(
      _.chain(this.listStatus)
        .filter((x) => x.code == value)
        .first()
        .value(),
      'name'
    );
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      if (this.isViewFromPopup) {
        this.onCheckRadio(item);
        return;
      } else {
        this.sessionService.setSessionData(FunctionCode.CAMPAIGN_RM, this.prevParams);
        this.router.navigate([this.router.url, 'detail', item.id], {
          state: this.prevParams,
          queryParams: {
            // showBtnAddCampaign: item.typeId === 'PTKH' && item.status === 'ACTIVATED'
            showBtnAddCampaign: (item.isRmInsert === true || this.currUser?.branch === item?.createdByBranchCode) && item.status === 'ACTIVATED'
              && item.blockCode === 'INDIV' && new Date((new Date()).setHours(0, 0, 0, 0)) <= new Date(item.endDate),
            listBranch: item.branchCodes
          }
        });
      }
    }
  }

  isUpdate(item) {
    return this.objFunction?.update && item.status === CampaignStatus.InProgress;
  }

  onCheckRadio(item?: any) {
    this.isSelectedRadio = [];
    if (item) {
      this.isSelectedRadio.push(item);
    }
    this.onChangeCampain.emit(item);
  }

  isCheckedRadio(item) {
    return this.isSelectedRadio?.filter((element) => element.id === item.id).length > 0;
  }

}
