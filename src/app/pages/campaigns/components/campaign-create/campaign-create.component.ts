import { forkJoin, of } from 'rxjs';
import {
  Component,
  OnInit,
  AfterViewInit,
  Injector,
  ViewEncapsulation,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { CampaignsService } from '../../services/campaigns.service';
import { BaseComponent } from 'src/app/core/components/base.component';
import { validateAllFormFields, cleanDataForm } from 'src/app/core/utils/function';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { catchError } from 'rxjs/operators';
import {
  BRANCH_HO,
  CampaignSize,
  CommonCategory, CustomerType,
  FunctionCode,
  ProductLevel,
  Scopes,
  ScreenType,SessionKey,
} from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as moment from 'moment';
import { Utils } from 'src/app/core/utils/utils';
import { Validators } from '@angular/forms';
import { ProductService } from 'src/app/core/services/product.service';
import * as _ from 'lodash';
import { FileService } from 'src/app/core/services/file.service';

@Component({
  selector: 'app-campaign-create',
  templateUrl: './campaign-create.component.html',
  styleUrls: ['./campaign-create.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CampaignCreateComponent extends BaseComponent implements OnInit, AfterViewInit {
  form = this.fb.group({
    id: [''],
    name: ['', CustomValidators.required],
    code: ['', CustomValidators.required],
    status: [{value: '', disabled: true}, CustomValidators.required],
    startDate: [new Date(), CustomValidators.required],
    endDate: ['', CustomValidators.required],
    productCode: ['', CustomValidators.required],
    typeId: ['', CustomValidators.required],
    process: ['', CustomValidators.required],
    scope: [CampaignSize.TOAN_HANG, CustomValidators.required],
    areaCode: [],
    branchCodes: [],
    bizLine: ['', CustomValidators.required],
    channel: ['ALL', CustomValidators.required],
    blockCode: ['', CustomValidators.required],
    formId: ['', CustomValidators.required],
    sourceId: ['', CustomValidators.required],
    content: [''],
    slaManager: [null,[CustomValidators.required, Validators.min(1)]],
    slaRm: [null, [CustomValidators.required, Validators.min(1)]],
    isOutreachProcess: [false],
    isRmInsert: [false],
    fileId: null,
    rsId: '',
  });
  minEndDate = new Date();
  isLoading = false;
  isValidator = false;
  isRegion = false;
  isHO = false;
  isRm = false;
  listStatus = [];
  listProduct = [];
  listPurpose = [];
  listFormality = [];
  listBlock = [];
  listChannel = [];
  listProcess = [];
  listRegion = [];
  listBranch = [];
  listBranchByRegion = [];
  listBizLine = [];
  listSourceData = [];
  listBizLineByBlock = [];
  branchesCode: string[];
  avatarUploading: boolean;
  avatarFileName: string;
  @Input() isViewFromPopup = false;
  @Output() onClosePopup: EventEmitter<any> = new EventEmitter<any>();
  @Input() listBranchChoose: Array<any>;

  branchUser: any;
  listDivision = [];
  listProductProcess = [];

  constructor(
    injector: Injector,
    private campaignService: CampaignsService,
    private categoryService: CategoryService,
    private productService: ProductService,
    private fileService: FileService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CAMPAIGN}`);
    this.listDivision = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM);
    this.form.controls.rsId.setValue(this.objFunction?.rsId);
  }

  ngOnInit(): void {
    if (this.isViewFromPopup) {
      this.branchesCode = this.listBranchChoose;
    }
    this.avatarFileName = this.fields?.chooseFile;
    this.isLoading = true;
    this.isHO = this.currUser?.branch === BRANCH_HO;
    this.prop = this.sessionService.getSessionData(`${FunctionCode.CAMPAIGN}_${ScreenType.Create}`);
    if (!this.prop) {
      forkJoin([
        this.commonService
          .getCommonCategory(CommonCategory.EXT_CAMPAIGNS_CHANNELS)
          .pipe(catchError((e) => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.CAMPAIGN_TYPE).pipe(catchError((e) => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.CAMPAIGN_FORM).pipe(catchError((e) => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.SOURCE_DATA).pipe(catchError((e) => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.CAMPAIGN_DIVISION).pipe(catchError((e) => of(undefined))),
        this.productService.getProductByLevel(ProductLevel.Lvl1).pipe(catchError((e) => of(undefined))),
        this.categoryService.getBlocksCategoryByRm().pipe(catchError((e) => of(undefined))),
        this.translate.get('campaign').pipe(catchError((e) => of(undefined))),
        this.categoryService
          .getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW)
          .pipe(catchError((e) => of(undefined))),
        this.categoryService.getRegion().pipe(catchError((e) => of(undefined))),
        this.categoryService.getBranchByCode(this.currUser?.branch).pipe(catchError((e) => of(undefined))),
        this.campaignService.getProductProcessByBlock(CustomerType.INDIV).pipe(catchError((e) => of(undefined))),
      ]).subscribe(
        ([
           listSaleChannel,
           listPurpose,
           listFormality,
           listSourceData,
           listBizLine,
           listProduct,
           listBlock,
           campaign,
           branchesOfUser,
           listRegion,
           branchUser,
           listProductProcess
         ]) => {
          this.prop = {
            listSaleChannel,
            listPurpose,
            listFormality,
            listSourceData,
            listBizLine,
            listProduct,
            listBlock,
            campaign,
            branchesOfUser,
            listRegion,
            branchUser,
            listProductProcess
          };
          this.isRm = _.isEmpty(branchesOfUser);
          this.prop.isRm = this.isRm;
          if (this.isRm) {
            branchesOfUser.push(branchUser);
            this.form.controls.slaManager.disable();
            this.form.controls.branchCodes.setValue(branchesOfUser);
            this.form.controls.branchCodes.disable();
            this.form.controls.slaManager.setValidators(null);
          }
          this.mapData();
          this.sessionService.setSessionData(`${FunctionCode.CAMPAIGN}_${ScreenType.Create}`, this.prop);
        }
      );
    } else {
      const timer = setTimeout(() => {
        this.isRm = this.prop.isRm;
        this.mapData();
        clearTimeout(timer);
      }, 500);
    }
  }

  mapData() {
    this.listProductProcess = this.prop?.listProductProcess || [];
    this.form.controls.process.setValue(_.get(_.first(this.listProductProcess), 'id'));
    this.listPurpose = this.prop?.listPurpose?.content || [];
    this.listFormality = this.prop?.listFormality?.content || [];
    this.listProduct =
      this.prop?.listProduct
        ?.filter((i) => i.level === ProductLevel.Lvl1)
        ?.map((item) => {
          return { code: item.idProductTree, name: item.idProductTree + ' - ' + item.description };
        }) || [];
    this.listBlock =
      this.prop?.listBlock?.map((item) => {
        return { code: item.code, name: item.code + ' - ' + item.name };
      }) || [];



    this.form.controls.blockCode.setValue(this.listBlock.find((item) => item.code  === CustomerType.INDIV )?.code);
    this.form.controls.blockCode.disable();

    this.listChannel = this.prop?.listSaleChannel?.content || [];
    this.listChannel.unshift({ code: 'ALL', name: this.fields?.all });
    this.listChannel = _.unionBy(this.listChannel, 'code');
    this.listSourceData = this.prop?.listSourceData?.content || [];
    this.form.controls.sourceId.setValue(this.listSourceData.find((item) => item.isDefault)?.code);
    this.listBizLine = this.prop?.listBizLine?.content || [];
    Object.keys(this.prop?.campaign?.status).forEach((key) => {
      this.listStatus.push({ code: key.toUpperCase(), name: this.prop?.campaign?.status[key] });
    });

    this.listBizLineByBlock = this.listBizLine.filter((item) => item.value?.includes(CustomerType.INDIV));

    this.form.controls.status.setValue(this.listStatus[0]?.code);
    this.listBranch = this.prop?.branchesOfUser;
    this.listBranchByRegion = this.prop?.branchesOfUser;
    if (!this.isViewFromPopup) {
      this.branchesCode = this.prop?.branchesOfUser?.map((item) => item.code);
    }
    if (this.isHO) {
      this.listRegion = this.prop?.listRegion;
      if (this.isViewFromPopup) {
        const region = _.filter(this.listBranchByRegion, (item) => this.listBranchChoose.includes(item.code)).map(i => i.khuVucM);
        // this.form.controls.areaCode.setValue(_.filter(this.listRegion, (item) => region.includes(item.locationCode)), { emitEvent: false });
        this.form.controls.areaCode.setValue(_.uniq(region), { emitEvent: false });
        this.listBranchByRegion = this.listBranch.filter((item) => region.includes(item.khuVucM));
      }
    } else {
      const areaCodes = [];
      this.prop?.listRegion?.forEach((item) => {
        if (
          this.prop?.branchesOfUser?.findIndex((i) => i.khuVucM === item?.locationCode) !== -1 &&
          this.listRegion.findIndex((r) => r.locationCode === item?.locationCode) === -1
        ) {
          this.listRegion.push(item);
          areaCodes.push(item?.locationCode);
        }
      });
      this.form.controls.scope.setValue(CampaignSize.CHI_NHANH);
      this.form.controls.scope.disable();
      this.form.controls.areaCode.setValue(areaCodes, { emitEvent: false });
      this.form.controls.areaCode.disable({emitEvent: false});
    }
    if(this.isRm){
      this.form.controls.slaManager.setValidators(null);
    }
    this.isLoading = false;
  }

  ngAfterViewInit() {
    this.form.controls.scope.valueChanges.subscribe((value) => {
      this.isRegion = value === CampaignSize.CHI_NHANH;
      if (this.isRegion) {
        this.form.controls.areaCode.setValidators(CustomValidators.required);
        this.form.controls.branchCodes.setValidators(CustomValidators.required);
      } else {
        this.form.controls.areaCode.clearValidators();
        this.form.controls.branchCodes.clearValidators();
      }
      this.form.updateValueAndValidity({ emitEvent: false });
    });
    // this.form.controls.productCode.valueChanges.subscribe((value) => {
    //   this.getProcess(value, this.form.controls.blockCode.value);
    // });
    // this.form.controls.blockCode.valueChanges.subscribe((value) => {
    //   this.getProcess(this.form.controls.productCode.value, value);
    // });
    this.form.controls.startDate.valueChanges.subscribe((value) => {
      if (value && value !== '') {
        if (moment(value).isBefore(moment())) {
          this.minEndDate = new Date();
        } else {
          this.minEndDate = value;
        }
      }
    });
    this.form.controls.areaCode.valueChanges.subscribe((value) => {
      if (value) {
        this.listBranchByRegion = this.listBranch.filter((item) => value.includes(item.khuVucM));
        this.branchesCode = [];
      }
    });
    this.form.controls.startDate.valueChanges.subscribe((value) => {
      const endDate = this.form.controls.endDate.value;
      if (value !== '' && endDate !== '' && moment(value).isAfter(moment(endDate))) {
        this.form.controls.endDate.setValue('');
      }
    });
    this.form.controls.blockCode.valueChanges.subscribe((value) => {
      this.listBizLineByBlock = this.listBizLine.filter((item) => item.value?.includes(value));
    });
  }

  save() {
    this.form.controls.branchCodes.setValue(this.branchesCode);
    if (this.form.valid) {
      this.confirmService.confirm().then((res) => {
        if (res) {
          this.isLoading = true;
          const data = cleanDataForm(this.form);
          if (Utils.isStringEmpty(data.process)) {
            this.messageService.warn(this.notificationMessage.CAMPAIGN_E102);
            this.isLoading = false;
            return;
          }
          data.branchCodes = this.branchesCode;
          if (data.scope === CampaignSize.TOAN_HANG) {
            delete data.areaCode;
            delete data.branchCodes;
          }
          this.campaignService.create(data).subscribe(
            () => {
              this.isLoading = false;
              this.messageService.success(this.notificationMessage.success);
              if (!this.isViewFromPopup) {
                this.back();
              } else {
                this.onClosePopup.emit(true);
              }
            },
            (e) => {
              this.isLoading = false;
              if (e?.status === 0) {
                this.messageService.error(this.notificationMessage.E001);
                return;
              }
              this.messageService.error(JSON.parse(e?.error)?.description);
            }
          );
        }
      });
    } else {
      this.isValidator = true;
      validateAllFormFields(this.form);
    }
  }

  getProcess(productCode, blockCode) {
    if (Utils.trimNullToEmpty(productCode) !== '' && Utils.trimNullToEmpty(blockCode) !== '') {
      this.productService.getProcessByProductAndBlock(productCode, blockCode).subscribe((listData) => {
        if (listData?.content[0]) {
          this.form.controls.process.setValue(listData?.content[0]?.process);
        }
      });
    }
  }

  uploadAvatar(event, fileUpload) {
    if (!_.startsWith(_.get(event, 'files[0].type'), 'image/')) {
      this.avatarFileName = this.fields?.chooseFile;
      fileUpload.clear();
      this.messageService.error(this.notificationMessage.E124);
      return;
    }
    if (_.get(event, 'files[0].size') > 512000) {
      this.messageService.error(this.notificationMessage.max500kb);
      this.avatarFileName = this.fields?.chooseFile;
      fileUpload.clear();
      return;
    }
    this.avatarUploading = true;
    const formData = new FormData();
    formData.append('file', event.files[0]);
    this.avatarFileName = event.files[0].name;
    this.fileService
      .uploadFile(formData)
      .pipe(catchError(() => of(undefined)))
      .subscribe((res) => {
        this.avatarUploading = false;
        if (!res) {
          this.messageService.error(this.notificationMessage.uploadFail);
          this.avatarFileName = this.fields?.chooseFile;
        }
        fileUpload.clear();
        this.form.controls.fileId.setValue(res || null);
      });
  }

  retype() {
    this.form.reset();
    this.branchesCode = [];
    if (!this.isHO) {
      const areaCodes = this.listRegion?.map((i) => i.locationCode);
      this.form.controls.scope.setValue(CampaignSize.CHI_NHANH);
      this.form.controls.scope.disable();
      this.form.controls.areaCode.setValue(areaCodes);
      this.form.controls.areaCode.disable();
    }
  }
}
