import { catchError } from 'rxjs/operators';
import { forkJoin, of } from 'rxjs';
import { AfterContentChecked, Component, Injector, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { MenuItem, PrimeIcons } from 'primeng/api';
import { CampaignsService } from '../../services/campaigns.service';
import {
  ActivityType,
  CommonCategory,
  ConfigBackDate,
  FunctionCode,
  functionUri,
  LeadProsessName,
  maxInt32,
  ProcessType,
  ProductLevel,
  Scopes,
  ScreenType,
  StatusLead,
  TaskType,
} from 'src/app/core/utils/common-constants';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { ActivityService } from 'src/app/core/services/activity.service';
import { ActivityActionComponent } from '../activity.action/activity.action.component';
import { CalendarAddModalComponent } from 'src/app/pages/calendar/components/calendar-add-modal/calendar-add-modal.component';
import { CustomerCampaignModalComponent } from '../customer-campaign-modal/customer-campaign-modal.component';
import * as moment from 'moment';
import { ActivityRecordComponent } from '../activity.record/activity.record.component';
import { WorkflowService } from 'src/app/core/services/workflow.service';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { CustomerApi } from 'src/app/pages/customer-360/apis';
import { ProductService } from 'src/app/core/services/product.service';
import * as _ from 'lodash';
import { LeadService } from 'src/app/core/services/lead.service';
import {global} from '@angular/compiler/src/util';
import {CategoryService} from '../../../system/services/category.service';
import {AppFunction} from '../../../../core/interfaces/app-function.interface';
import {CampaignChangeStageComponent} from '../campaign-change-stage/campaign-change-stage.component';

export interface TimeLineWorkflow {
  icon?: string;
  background: string;
  name: string;
  lane: string;
  status: string;
}

@Component({
  selector: 'app-campaign-rm-customer',
  templateUrl: './campaign-rm-customer.component.html',
  styleUrls: ['./campaign-rm-customer.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CampaignRmCustomerComponent extends BaseComponent implements OnInit, AfterContentChecked {
  tabIndex: number;
  expired: boolean;
  customer: any = {};
  customerType: string;
  rowMenu: MenuItem[];
  itemCampaign: any;
  workflowType: string;
  listCampaign = [];
  listActivity = [];
  listSourceData = [];
  listProduct = [];
  listProcess = [];
  listProductChild = [];
  workflow: TimeLineWorkflow[];
  lanes: any[] = [];
  paramsLead = {
    campaignId: '',
    customerCode: '',
    type: '',
  };
  paramsActivity = {
    campaignId: '',
    page: 0,
    parentId: '',
    rsId: '',
    scope: Scopes.VIEW,
    size: maxInt32,
  };
  isPermissionCustomer: boolean;
  backDate: number;
  itemActivityFocus: any;
  form = this.fb.group({
    status: [null, CustomValidators.required],
    product: '',
  });
  isValidator: boolean;
  showForm: boolean;
  isDone: boolean;
  componentLoaded: boolean;
  showFormProduct: boolean;
  customerTypeMerge: string;
  @ViewChild('stageModal') stageModal: any;
  objFunction360: AppFunction;
  activity_types: Array<any> = [];

  constructor(
    injector: Injector,
    private campaignService: CampaignsService,
    private leadService: LeadService,
    private activityService: ActivityService,
    private workflowService: WorkflowService,
    private customerService: CustomerApi,
    private productService: ProductService,
    private categoryService: CategoryService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CAMPAIGN_RM}`);
    this.objFunction360 = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.paramsActivity.rsId = this.objFunction?.rsId;
  }

  ngOnInit(): void {
    const customerCode = this.route.snapshot.paramMap.get('code');
    const customerType = this.route.snapshot.paramMap.get('type');
    const campaignId = this.route.snapshot.paramMap.get('campaignId');
    this.customerType = customerType?.toUpperCase();
    this.paramsLead.customerCode = customerCode;
    this.paramsLead.campaignId = campaignId;
    this.paramsLead.type = customerType?.toUpperCase();
    this.paramsActivity.parentId = customerCode;
    this.paramsActivity.campaignId = campaignId;
    this.translate.get('btnAction').subscribe((btnAction) => {
      this.rowMenu = [
        {
          label: btnAction?.delete,
          command: (e) => {
            this.deleteActivity();
          },
        },
        {
          label: btnAction?.update,
          command: () => {
            this.updateActivity();
          },
        },
        {
          label: btnAction?.separate,
          command: () => {
            this.separateActivity();
          },
        },
      ];
    });
    this.isLoading = true;
    const rsIdCustomer = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`)?.rsId;
    forkJoin([
      this.commonService.getCommonCategory(CommonCategory.SOURCE_DATA).pipe(catchError(() => of(undefined))),
      this.productService.getProductByLevel(ProductLevel.Lvl1).pipe(catchError(() => of(undefined))),
      this.campaignService.findCampaignByRm({ campaignId, customerCode }).pipe(catchError(() => of(undefined))),
      this.leadService.getDetailLead(this.paramsLead).pipe(catchError(() => of(undefined))),
      this.activityService.search(this.paramsActivity).pipe(catchError(() => of(undefined))),
      this.workflowService.findAllProcess().pipe(catchError(() => of(undefined))),
      this.categoryService.checkManageType(this.objFunction360?.rsId, Scopes.VIEW).pipe(catchError(() => of(undefined))),
      this.categoryService.getBranchesOfUser(this.objFunction360?.rsId, Scopes.VIEW).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.CONFIG_TYPE_ACTIVITY).pipe(catchError((e) => of(undefined))),
    ]).subscribe(
      ([
        listSourceData,
        listProduct,
        listCampaign,
        itemLead,
        listActivity,
        listProcess,
         listManageType,
         listBranches,
         types
      ]) => {
        this.activity_types = _.get(types, 'content') || [];
        this.listProcess = _.orderBy(listProcess, ['index'], ['asc']);
        this.listCampaign = listCampaign || [];
        this.tabIndex = _.findIndex(this.listCampaign, (item) => item.id === campaignId);
        this.itemCampaign = _.get(this.listCampaign, `[${this.tabIndex}]`);
        this.expired = moment(this.itemCampaign?.endDate).endOf('day').isBefore(moment());
        this.getProductChildren();
        this.listSourceData = listSourceData?.content || [];
        this.listActivity = listActivity?.content || [];
        this.listProduct = listProduct?.map((item) => {
          return { code: item.idProductTree, name: `${item.idProductTree} - ${item.description}` };
        });
        if (itemLead) {
          this.customer = itemLead;
          this.form.controls.product.setValue(this.customer?.opportunity?.productCodeSale);
          this.customer.source = listSourceData?.content?.find(
            (itemSource) => this.customer.source === itemSource.code
          )?.name;
          this.customer.productName = this.listProduct?.find(
            (itemProduct) => this.customer.productCode === itemProduct.code
          )?.name;
          this.mapWorkFlow(true);
        } else {
          this.isLoading = false;
          this.componentLoaded = true;
        }
        if (listManageType && listManageType.length > 0) {
          const itemSelected = listManageType.filter((item) => {
            return item.selected === true;
          });
          const paramsCustomer = {
            rsId: rsIdCustomer,
            scope: Scopes.VIEW,
            hrsCode: _.isEmpty(listBranches) ? this.currUser?.hrsCode : '',
            customerCode,
            pageNumber: 0,
            pageSize: 10,
            manageType: itemSelected[0].code,
        //    manageType: ''
          };
          this.isLoading = true;
          this.customerService.search(paramsCustomer).subscribe(listCustomers => {
            this.isLoading = false;
            this.isPermissionCustomer = listCustomers?.length > 0;
            if (this.isPermissionCustomer) {
              this.customerTypeMerge = _.get(listCustomers, '[0].customerTypeMerge');
            }
          });
        }
      },
      () => {
        this.isLoading = false;
        this.componentLoaded = true;
        this.messageService.error(this.notificationMessage.error);
      }
    );
    this.commonService
      .getCommonCategory(CommonCategory.CONFIG_BACK_DATE, ConfigBackDate.BACK_DATE_ACTIVITY)
      .pipe(catchError(() => of(undefined)))
      .subscribe((backDate) => {
        this.backDate = backDate?.content[0]?.value || 0;
      });
    this.form.controls.status.valueChanges.subscribe((value) => {
      this.showFormProduct = value === 'CLOSING_SALES';
    });
  }

  ngAfterContentChecked() {
    this.ref.detectChanges();
  }

  checkItem(item) {
    return _.isEmpty(_.get(item, 'preActivityId'));
  }

  getDataByCampaign() {
    this.isLoading = true;
    forkJoin([
      this.leadService.getDetailLead(this.paramsLead),
      this.activityService.search(this.paramsActivity),
    ]).subscribe(
      ([itemLead, listActivity]) => {
        if (itemLead) {
          this.customer = itemLead;
          this.form.controls.product.setValue(this.customer?.opportunity?.productCodeSale);
          this.mapWorkFlow(true);
          this.customer.source = this.listSourceData?.find(
            (itemSource) => this.customer.source === itemSource.code
          )?.name;
          this.customer.productName = this.listProduct?.find((item) => item.code === this.customer.productCode)?.name;
        }
        this.listActivity = listActivity?.content || [];
        this.getProductChildren();
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }

  getProductChildren() {
    this.productService.getProductTreeSale(this.itemCampaign?.productCode).subscribe((list) => {
      this.listProductChild = list?.map((item) => {
        return { code: item.idProductTree, name: `${item.idProductTree} - ${item.description}` };
      });
    });
  }

  // openModalChangeStage() {
  //   this.modalService.open(this.stageModal, { windowClass: 'modal-process-stage' });
  //   const timer = setTimeout(() => {
  //     this.showForm = true;
  //     clearTimeout(timer);
  //   }, 100);
  // }
  openModalChangeStage() {
    // this.modalService.open(this.stageModal, { windowClass: 'modal-process-stage' });
    // const timer = setTimeout(() => {
    //   this.showForm = true;
    //   clearTimeout(timer);
    // }, 100);
    const form = this.modalService.open(CampaignChangeStageComponent, {
      windowClass: 'campaign-change-stage'
    });
    form.componentInstance.customer = this.customer;
    form.componentInstance.itemCampaign = this.itemCampaign;
    form.result.then((res) => {
      // if(res){
      //   this.customer = res.customer;
      //   console.log('cus info: ', this.customer);
      // }
      // this.mapWorkFlow();
      if(res.customer){
        this.getDataByCampaign();
        if (res.modalRef) {
          res.modalRef.result
            .then((confirmed: boolean) => {
              if (confirmed) {
                this.createActivity();
              }
              // debugger
              // const data = {
              //   customer: this.customer
              // }
              // this.modalActive.close(data);
            })
            .catch(() => {
            });
        }
      }
    }).catch(() => {});
  }

  changeStage(modal) {
    if (this.form.valid) {
      modal.close();
      this.isLoading = true;
      const dataForm = this.form.getRawValue();
      this.form.controls.product.reset();
      if (this.workflowType === ProcessType.LEAD) {
        this.workflowService.processQualify(this.customer.id, dataForm?.status).subscribe(
          () => {
            if (this.lanes?.findIndex((item) => item.status === dataForm?.status) === this.workflow.length - 1) {
              this.createOpportunity();
            } else {
              this.customer.status = dataForm?.status;
              this.mapWorkFlow();
              this.isLoading = false;
              this.messageService.success(this.notificationMessage.success);
            }
          },
          () => {
            this.isLoading = false;
            this.messageService.error(this.notificationMessage.error);
          }
        );
      } else {
        //bay h se roi vao case nay
        const status = this.lanes?.find((item) => item.status === dataForm?.status)?.lane;
        this.workflowService.processOpportunity(this.customer?.opportunity?.id, status, dataForm?.product).subscribe(
          () => {
            if (this.showFormProduct) {
              this.form.controls.product.setValue(dataForm?.product);
            }
            this.customer.opportunity.status = dataForm?.status;
            this.mapWorkFlow();
            this.isLoading = false;
            this.messageService.success(this.notificationMessage.success);
            if (this.lanes?.findIndex((item) => item.status === dataForm?.status) === this.workflow.length - 1) {
              this.communicateService.request({ name: 'FireWorks' });
              this.isDone = true;
            }
          },
          () => {
            this.isLoading = false;
            this.messageService.error(this.notificationMessage.error);
          }
        );
      }
    } else {
      this.isValidator = true;
      validateAllFormFields(this.form);
    }
  }

  createOpportunity() {
    this.workflowService.convertOpportunity(this.customer?.id).subscribe((opportunity) => {
      this.customer.opportunity = opportunity;
      this.workflowType = ProcessType.OPPORTUNITY;
      this.customer.status = StatusLead.CONVERTED;
      this.mapWorkFlow(true);
      this.messageService.success(this.notificationMessage.success);
    });
  }

  detailCustomer() {
    if (!this.isPermissionCustomer) {
      this.confirmService.warn(this.notificationMessage.ECUS001).then();
      return;
    }
    this.router.navigateByUrl(
      `${functionUri.customer_360_manager}/detail/${this.customerTypeMerge?.toLowerCase()}/${
        this.customer?.customerCode
      }`,
      {
        skipLocationChange: true,
      }
    );
  }

  onTabChanged(e) {
    this.tabIndex = e?.index;
    this.customer.remainDayRmAccost = null;
    this.itemCampaign = this.listCampaign[e?.index];
    this.expired = moment(this.itemCampaign?.endDate).endOf('day').isBefore(moment());
    this.paramsActivity.campaignId = this.itemCampaign?.id;
    this.paramsLead.campaignId = this.itemCampaign?.id;
    this.getDataByCampaign();
  }

  historyCampaign() {
    const parent: any = {};
    parent.parentId = this.customer?.customerCode;
    parent.parentName = this.customer?.customerName;
    parent.campaignId = this.itemCampaign?.id;
    parent.rsId = this.objFunction?.rsId;
    const modal = this.modalService.open(CustomerCampaignModalComponent, {
      windowClass: 'create-activity-modal',
      scrollable: true,
    });
    modal.componentInstance.parent = parent;
  }

  getActivity() {
    this.activityService.search(this.paramsActivity).subscribe((listActivity) => {
      this.listActivity = listActivity?.content || [];
    });
  }

  createCalendar() {
    this.modalService.open(CalendarAddModalComponent, { windowClass: 'create-calendar-modal' });
  }

  createActivity() {
    const parent: any = {
      parentType: TaskType.LEAD,
      parentId: this.customer?.customerCode,
      parentName: this.customer?.customerName,
      campaignId: this.itemCampaign?.id,
      rsId: this.objFunction?.rsId,
      productCode: this.itemCampaign?.productCode,
    };
    const modal = this.modalService.open(ActivityActionComponent, {
      windowClass: 'create-activity-modal',
      scrollable: true,
    });
    modal.componentInstance.parent = parent;
    modal.componentInstance.type = ScreenType.Create;
    modal.result.then(() => {
      this.getActivity();
    });
  }

  mailTo() {
    window.location.href = `mailto:${this.customer?.email}`;
  }

  getIcon(item) {
    if (item?.activityType === ActivityType.Call) {
      return 'las la-phone';
    } else if (item?.activityType === ActivityType.Email) {
      return 'las la-envelope';
    } else if (item?.activityType === ActivityType.Meeting) {
      return 'las la-users';
    } else if (item?.activityType === ActivityType.SMS) {
      return 'las la-sms';
    } else if (item?.activityType === ActivityType.Chat) {
      return 'las la-chat-small';
    } else {
      return '';
    }
  }

  showMenu(item) {
    return (
      item?.createdBy === this.currUser?.username &&
      !this.expired &&
      item?.dateStart &&
      moment(item?.dateStart)
        .add(+this.backDate * 24, 'hour')
        .isSameOrAfter(moment().startOf('day'))
    );
  }

  getItemActivity(item) {
    this.itemActivityFocus = item;
  }

  onShowMenu() {
    if (
      moment(this.itemActivityFocus?.dateStart)
        .add(+this.backDate * 24, 'hour')
        .isSameOrAfter(moment().startOf('day'))
    ) {
      this.rowMenu[0].styleClass = '';
      this.rowMenu[1].styleClass = '';
      this.rowMenu[2].styleClass = this.itemActivityFocus.preActivityId ? '' : 'd-none';
    }
     else {
      this.rowMenu[0].styleClass = 'd-none';
      this.rowMenu[1].styleClass = 'd-none';
      this.rowMenu[2].styleClass = 'd-none';
    }
  }

  separateActivity() {
    if (!this.itemActivityFocus) {
      return;
    }
    const parent: any = {};
    parent.parentType = this.itemActivityFocus?.parentType;
    parent.parentId = this.itemActivityFocus?.parentId;
    parent.parentName = this.itemActivityFocus?.parentName;
    parent.productCode = this.itemActivityFocus?.productCode;
    parent.campaignId = this.itemCampaign?.id;
    parent.rsId = this.objFunction?.rsId;
    const params = {
      campaignId: this.route.snapshot.paramMap.get('campaignId')
    }
    this.activityService
      .getByCodeAndCampaignId(this.itemActivityFocus?.id, params)
      .pipe(catchError(() => undefined))
      .subscribe((data) => {
        parent.data = this.convertData(data);
        const modal = this.modalService.open(ActivityRecordComponent, {
          windowClass: 'create-activity-modal',
          scrollable: true,
        });
        modal.componentInstance.parent = parent;
        modal.componentInstance.type = ScreenType.Update;
        modal.componentInstance.isShowFuture = true;
        modal.result.then((res) => {
          if (res) {
            this.getActivity();
          }
        });
      });
  }

  updateActivity() {
    if (!this.itemActivityFocus) {
      return;
    }
    const params = {
      campaignId: this.route.snapshot.paramMap.get('campaignId')
    }
    this.activityService
      .getByCodeAndCampaignId(this.itemActivityFocus?.id, params)
      .pipe(catchError(() => undefined))
      .subscribe((data) => {
        const parent: any = {};
        parent.parentType = TaskType.LEAD;
        parent.parentId = this.customer?.customerCode;
        parent.parentName = this.customer?.customerName;
        parent.campaignId = this.itemCampaign?.id;
        parent.rsId = this.objFunction?.rsId;
        parent.productCode = this.itemCampaign?.productCode;
        const activityModal = this.modalService.open(ActivityActionComponent, {
          windowClass: 'create-activity-modal',
          scrollable: true,
        });
        activityModal.componentInstance.parent = parent;
        activityModal.componentInstance.data = data;
        activityModal.result
          .then((res) => {
            if (res) {
              this.getActivity();
            }
          })
          .catch(() => {});
      });
  }

  deleteActivity() {
    if (!this.itemActivityFocus) {
      return;
    }
    this.confirmService
      .confirm()
      .then((isConfirm) => {
        if (isConfirm) {
          const params = {
            campaignId: this.route.snapshot.paramMap.get('campaignId')
          }
          this.isLoading = true;
          this.activityService.deleteByCodeAndCampaignId(this.itemActivityFocus?.id, params).subscribe(
            () => {
              this.messageService.success(this.notificationMessage.success);
              this.isLoading = false;
              this.getActivity();
            },
            () => {
              this.messageService.error(this.notificationMessage.error);
              this.isLoading = false;
            }
          );
        }
      })
      .catch(() => {});
  }

  convertData(data) {
    return {
      preId: data?.id,
      id: data?.futureActivity?.id,
      parentName: data?.parentName,
      parentType: data?.parentType,
      activityType: data?.futureActivity?.activityType,
      dateStart: new Date(),
      activityResult: data?.futureActivity?.activityResult,
      location: data?.futureActivity?.location,
      note: data?.futureActivity?.note,
    };
  }

  closeModal(modal) {
    modal?.close();
  }

  mapWorkFlow(isReload?: boolean) {
    let currStatus = '';
    if (isReload) {
      this.workflowType = this.customer?.status === StatusLead.CONVERTED ? ProcessType.OPPORTUNITY : ProcessType.LEAD;
      currStatus =
        this.workflowType === ProcessType.LEAD ? this.customer?.status : this.customer?.opportunity?.status;
      const nameProcess = this.workflowType === ProcessType.LEAD ? LeadProsessName : this.itemCampaign?.process;
      const lanes = this.listProcess?.filter((i) => i.nameProcessDefine === nameProcess);
      this.lanes = [];
      this.workflow = [];
      lanes?.forEach((itemLane, i) => {
        if (lanes.length > 0 && lanes.length - 1 === i) {
          if (currStatus === itemLane.status) {
            this.workflow[this.workflow.length - 1].name = `${i}. ${itemLane.title}`;
          } else if (currStatus !== this.workflow[this.workflow.length - 1].status) {
            this.workflow[this.workflow.length - 1].name += '/ ' + itemLane.title;
          }
        } else {
          this.workflow?.push({
            background: 'bg-grey',
            name: `${i + 1}. ${itemLane.title}`,
            lane: itemLane?.lane,
            status: itemLane?.status,
          });
        }
        this.lanes.push({
          name: itemLane.title,
          lane: itemLane?.lane,
          status: itemLane?.status,
          disabled: i === 0,
        });
      });
    } else {
      currStatus =
        this.workflowType === ProcessType.LEAD ? this.customer?.status : this.customer?.opportunity?.status;
    }
    this.form.controls.status.setValue(currStatus);
    const index = this.lanes?.findIndex((item) => item.status === currStatus);
    this.isDone = index === this.workflow?.length - 1;
    this.workflow?.forEach((item, i) => {
      item.background = i <= index ? 'bg-green' : 'bg-grey';
      item.icon = PrimeIcons.CHECK;
    });
    if (this.workflow[this.workflow?.length - 1]) {
      if (index === this.workflow?.length - 1) {
        this.workflow[this.workflow?.length - 1].name = `${index + 1}. ${this.lanes[index].name}`;
      } else if (index > this.workflow?.length - 1) {
        this.workflow[this.workflow?.length - 1].name = `${index}. ${this.lanes[index].name}`;
        this.workflow[this.workflow?.length - 1].icon = PrimeIcons.TIMES;
        this.workflow[this.workflow?.length - 1].background = 'bg-danger';
      }
    }
    if (!currStatus) {
      this.messageService.error(this.notificationMessage.E001);
      this.isDone = true;
    }
    this.isLoading = false;
    this.componentLoaded = true;
  }

  generateClassName(count) {
    if (this.isLoading) {
      return 'text-white';
    }
    if (count > 1) {
      return 'text-green';
    } else if (count === 1) {
      return 'text-orange';
    } else {
      return 'text-danger';
    }
  }

  back() {
    if (global?.previousUrl) {
      if (global?.previousUrl.includes('campaigns/campaign-rm')) {
        this.router.navigateByUrl(global.previousUrl, { state: this.prop ? this.prop : this.state });
      } else {
        window.history.go(-1);
      }
    } else {
      this.location.back();
    }
  }

  getActivityTypeName(row, key) {
    return _.get(
      _.chain(this.activity_types)
        .filter((x) => x.code === _.get(row, key))
        .first()
        .value(),
      'name'
    );
  }
}
