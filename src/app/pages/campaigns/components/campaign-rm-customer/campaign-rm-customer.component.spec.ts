import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignRmCustomerComponent } from './campaign-rm-customer.component';

describe('CampaignRmCustomerComponent', () => {
  let component: CampaignRmCustomerComponent;
  let fixture: ComponentFixture<CampaignRmCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignRmCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignRmCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
