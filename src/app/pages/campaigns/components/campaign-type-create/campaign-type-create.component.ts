import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { CampaignsTypeService } from '../../services/campaigns-type.service';
import { CampaignsService } from '../../services/campaigns.service';
import { validateAllFormFields, cleanDataForm } from 'src/app/core/utils/function';
import { HttpRespondCode } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'app-campaign-type-create',
  templateUrl: './campaign-type-create.component.html',
  styleUrls: ['./campaign-type-create.component.scss'],
})
export class CampaignTypeCreateComponent extends BaseComponent implements OnInit {
  isLoading = false;
  form = this.fb.group({
    // code: ['', [CustomValidators.required, CustomValidators.code]],
    name: ['', [CustomValidators.required]],
    // description: [''],
  });
  constructor(
    injector: Injector,
    private campaignService: CampaignsService,
    private campaignsTypeService: CampaignsTypeService
  ) {
    super(injector);
  }

  ngOnInit(): void {}

  confirmDialog() {
    if (this.form.valid) {
      const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result
        .then((res) => {
          if (res) {
            this.isLoading = true;
            const data = cleanDataForm(this.form);
            this.campaignService.create(data).subscribe(
              () => {
                this.location.back();
                this.messageService.success(this.notificationMessage.success);
              },
              (e) => {
                if (e && e.error && e.error.code === HttpRespondCode.EXIST_CODE) {
                  this.messageService.warn(this.notificationMessage.existCode);
                } else {
                  this.messageService.error(this.notificationMessage.error);
                }
                this.isLoading = false;
              }
            );
          }
        })
        .catch(() => {});
    } else {
      validateAllFormFields(this.form);
    }
  }

  back() {
    this.location.back();
  }
}
