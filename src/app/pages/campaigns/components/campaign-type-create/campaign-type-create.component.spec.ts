import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignTypeCreateComponent } from './campaign-type-create.component';

describe('CampaignTypeCreateComponent', () => {
  let component: CampaignTypeCreateComponent;
  let fixture: ComponentFixture<CampaignTypeCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignTypeCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignTypeCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
