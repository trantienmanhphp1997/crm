import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { global } from '@angular/compiler/src/util';
import { cleanDataForm } from 'src/app/core/utils/function';
import { Utils } from 'src/app/core/utils/utils';
import { CampaignsService } from '../../services/campaigns.service';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { FunctionCode, ProductLevel } from 'src/app/core/utils/common-constants';
import { ProductService } from 'src/app/core/services/product.service';
import * as moment from "moment";

@Component({
  selector: 'app-campaign-mapping-list-view',
  templateUrl: './campaign-mapping-list-view.component.html',
  styleUrls: ['./campaign-mapping-list-view.component.scss'],
})
export class CampaignMappingListViewComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listData = [];
  limit = global.userConfig.pageSize;
  listProduct = [];
  listBlock = [];
  listProcedure = [];
  paramSearch = {
    size: global.userConfig.pageSize,
    page: 0,
    search: '',
  };

  formSearch = this.fb.group({
    productId: [''],
    blockCode: [''],
    process: [''],
    approvedDate: [''],
  });
  searchType = false;
  isAdvance = false;
  prevParams = this.paramSearch;
  pageable: Pageable;

  constructor(injector: Injector, private campaignService: CampaignsService, private productService: ProductService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CAMPAIGN_MAPPING}`);
  }

  ngOnInit(): void {
    forkJoin([
      this.productService.getProductByLevel(ProductLevel.Lvl1).pipe(catchError((e) => of(undefined))),
      this.campaignService.getBlockMapping().pipe(catchError((e) => of(undefined))),
      this.campaignService.getSaleProcess().pipe(catchError((e) => of(undefined))),
    ]).subscribe(([listProduct, listBlock, listProcedure]) => {
      this.listProduct =
        listProduct?.map((item) => {
          return { code: item.idProductTree, name: item.idProductTree + ' - ' + item.description };
        }) || [];
      this.listProduct.unshift({ code: '', name: this.fields.all });
      this.listBlock =
        listBlock?.content.map((item) => {
          return { code: item.code, name: item.code + ' - ' + item.name };
        }) || [];
      this.listBlock.unshift({ code: '', name: this.fields.all });
      this.listProcedure = listProcedure || [];
      this.listProcedure.unshift({ id: '', name: this.fields.all });
      this.search(true);
    });
  }

  search(isSearch?: boolean) {
    this.isLoading = true;
    let params: any = {};
    if (isSearch) {
      this.paramSearch.page = 0;
        params = cleanDataForm(this.formSearch);
        delete params.search;
        this.paramSearch.search = Utils.trimNullToEmpty(this.paramSearch.search);
        params = {...this.paramSearch, ...params};
        this.prevParams = params;
    } else {
      params = this.prevParams;
      params.page = this.paramSearch.page;
    }
    if (params.approvedDate) {
      params.approvedDate = moment(params.approvedDate).format('YYYY-MM-DD');
    } else {
      delete params.approvedDate;
    }
    this.campaignService.searchProductProcess(params).subscribe(
      (result) => {
        if (result) {
          this.listData = result?.content || [];
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: global.userConfig.pageSize,
          };
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  searchBasic(isSearch?: boolean) {
    this.searchType = false;
    this.search(isSearch);
  }

  searchAdvance(isSearch?: boolean) {
    this.searchType = true;
    this.search(isSearch);
  }
  isShow() {
    this.isAdvance = !this.isAdvance;
  }

  onActive(event) {
    // if (event.type === 'dblclick') {
    //   event.cellElement.blur();
    //   const item = _.get(event, 'row');
    //   this.router.navigate([this.router.url, 'detail', item.id], { state: { paramSearch: this.paramSearch } });
    // }
  }

  setPage(pageInfo) {
    this.paramSearch.page = pageInfo.offset;
    if (this.searchType) {
      this.searchAdvance(false);
    } else {
      this.searchBasic(false);
    }
  }

  delete(item) {
    this.confirmService.confirm().then((res) => {
      if (res) {
        this.isLoading = true;
        this.campaignService.delete(item.id).subscribe(
          () => {
            this.messageService.success(this.notificationMessage.success);
            if (this.searchType) {
              this.searchAdvance(false);
            } else {
              this.search(false);
            }
          },
          () => {
            this.messageService.error(this.notificationMessage.error);
            this.isLoading = false;
          }
        );
      }
    });
  }

  create() {
    this.router.navigate([this.router.url, 'create'], { state: { paramSearch: this.paramSearch } });
  }
}
