import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignMappingListViewComponent } from './campaign-mapping-list-view.component';

describe('CampaignMappingListViewComponent', () => {
  let component: CampaignMappingListViewComponent;
  let fixture: ComponentFixture<CampaignMappingListViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignMappingListViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignMappingListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
