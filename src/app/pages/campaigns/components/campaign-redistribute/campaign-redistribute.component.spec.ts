import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignRedistributeComponent } from './campaign-redistribute.component';

describe('CampaignRedistributeComponent', () => {
  let component: CampaignRedistributeComponent;
  let fixture: ComponentFixture<CampaignRedistributeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignRedistributeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignRedistributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
