import {
  Component,
  OnInit,
  ViewEncapsulation,
  HostBinding,
  Input,
  ViewChild,
  ViewChildren,
  Injector
} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ConfirmDialogComponent} from '../../../../shared/components';
import {CampaignListViewComponent} from '../campaign-list-view/campaign-list-view.component';
import {SaleSuggestApi} from '../../../sale-manager/api/sale-suggest.api';
import {BaseComponent} from '../../../../core/components/base.component';
import * as _ from 'lodash';
import {FunctionCode} from '../../../../core/utils/common-constants';

@Component({
  selector: 'app-campaign-rm-modal',
  templateUrl: './campaigns-modal.component.html',
  styleUrls: ['./campaigns-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CampaignsModalComponent extends BaseComponent implements OnInit {
  @HostBinding('class.list__campaigns-content') listContent = true;
  // @ViewChild(CampaignListViewComponent) listCampaign: CampaignListViewComponent;

  isLoading = false;
  constructor(
    injector: Injector,
    private modalActive: NgbActiveModal,
    private saleSuggestApi: SaleSuggestApi)
  {
    super(injector);
  }
  data: any
  @Input() listBranchChoose: Array<any>;
  @Input() paramsFilterOld: any;

  showLoading(isShowLoading) {
    this.isLoading = isShowLoading;
  }

  ngOnInit() {
  }

  closeModal() {
    this.sessionService.clearSessionData(FunctionCode.CAMPAIGN);
    this.modalActive.close(false);
  }

  choose() {
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.componentInstance.message = 'Bạn có muốn đẩy tập Khách hàng vào chiến dịch này?';
    confirm.result
      .then((res) => {
        if (res) {
          const params = this.paramsFilterOld;
          params.campaignId = _.first(this.data)?.id;
          params.branchCodes = this.listBranchChoose;
          this.isLoading = true;
          this.saleSuggestApi.addCustomerToCampaign(params).subscribe(item => {
            this.confirmService.success(this.notificationMessage.add_customer_to_campaign_success);
            this.isLoading = false;
            this.closeModal();
          }, error => {
            this.confirmService.success(this.notificationMessage.add_customer_to_campaign_success);
            this.isLoading = false;
            this.closeModal();
          });
        }
      })
      .catch(() => {});
  }

  onChangeCampain(event?: any) {
    this.data = [];
    if (event) {
      this.data.push(event);
    }
  }
}
