import { Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { FunctionCode, Scopes } from 'src/app/core/utils/common-constants';
import { get } from 'lodash';
import { DetailResultRmComponent } from '../detail-result-rm/detail-result-rm.component';
import { global } from '@angular/compiler/src/util';
@Component({
  selector: 'app-detail-result-branch',
  templateUrl: './detail-result-branch.component.html',
  styleUrls: ['./detail-result-branch.component.scss']
})
export class DetailResultBranchComponent extends BaseComponent implements OnInit {

  @Input() prevParams: any;
  @Input() listData: any = [];
  @Output() onSearch = new EventEmitter();

  @Input() campaignId = '';
  @Input() pageable: Pageable;
  @Input() isRM = false;

  constructor(
    injector: Injector
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CAMPAIGN}`);
  }

  ngOnInit(): void {
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }

    let page = pageInfo.offset;
    this.onSearch.emit(page);
  }

  onActive(event) {
    const item = get(event, 'row');
    if (event.type === 'dblclick' && !this.isRM) {
      event.cellElement.blur();
      const modal = this.modalService.open(DetailResultRmComponent, {
        windowClass: 'detail-result-rm',
      });

      modal.componentInstance.campaignId = this.campaignId;
      modal.componentInstance.params = {
        branchCodes: [
          item.branchCode
        ],
        regions: [
          item.region
        ],
        rsId: this.objFunction?.rsId,
        scope: Scopes.VIEW,
        isRm: true,
        size: get(global, 'userConfig.pageSize'),
        campaignId: this.campaignId
      }

    }
  }
}
