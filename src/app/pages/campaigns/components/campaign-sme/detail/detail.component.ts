import { Component, Injector, OnInit } from '@angular/core';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import { ProductService } from 'src/app/core/services/product.service';
import { BRANCH_HO, CampaignSize, CommonCategory, FunctionCode, ProductLevel, SessionKey, Scopes, Division, CampaignStatus, functionUri } from 'src/app/core/utils/common-constants';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { ConfirmDialogComponent } from 'src/app/shared/components';
import { CampaignsService } from '../../../services/campaigns.service';
import { get, unionBy, startsWith } from 'lodash';
import { Validators } from '@angular/forms';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { FileService } from 'src/app/core/services/file.service';
import * as moment from 'moment';
import { Utils } from 'src/app/core/utils/utils';
import { MenuItem } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';
import { KpiReportApi } from 'src/app/pages/report/apis/kpi-report.api';


@Component({
  selector: 'app-sme-campaign-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailSaleCampaignComponent extends BaseComponent implements OnInit {
  form = this.fb.group({
    id: [''],
    name: ['', CustomValidators.required],
    code: ['', CustomValidators.required],
    status: [{ value: '', disabled: true }, CustomValidators.required],
    startDate: [new Date(), CustomValidators.required],
    endDate: ['', CustomValidators.required],
    productCode: ['', CustomValidators.required],
    typeId: ['', CustomValidators.required],
    process: [''],
    scope: [CampaignSize.TOAN_HANG, CustomValidators.required],
    areaCode: [],
    branchCodes: [],
    bizLine: ['', CustomValidators.required],
    channel: ['ALL', CustomValidators.required],
    blockCode: ['', CustomValidators.required],
    formId: ['', CustomValidators.required],
    sourceId: ['', CustomValidators.required],
    content: [''],
    slaManager: [null, [CustomValidators.required, Validators.min(1)]],
    slaRm: [null, [CustomValidators.required, Validators.min(1)]],
    isOutreachProcess: [false],
    fileId: null,
  });
  minEndDate = new Date();
  isLoading = true;
  isValidator = false;
  isRegion = false;
  isHO = false;
  listStatus = [];
  listProduct = [];
  listPurpose = [];
  listFormality = [];
  listBlock = [];
  listChannel = [];
  listProcess = [];
  listRegion = [];
  listBranch = [];
  listBranchByRegion = [];
  listBizLine = [];
  listSourceData = [];
  listBizLineByBlock = [];
  branchesCode: string[];
  avatarUploading: boolean;
  avatarFileName: string;
  allowActive: boolean = false;
  campaign = {
    id: "",
    code: "",
    name: "",
    parentCampaign: "",
    status: "",
    statusDisplay: "",
    createdBy: "",
    branchCode: "",
    branchName: "",
    scope: "",
    startDate: "",
    endDate: "",
    formatedStartDate: "",
    formatedEndDate: "",
    slaRm: 0,
    slaManager: 0,
    content: "",
    areaCode: "",
    campaignAttachments: [],
    campaignBranchCodes: [],
    campaignRmLevels: [],
    participants: '',
    campaignItemAllocations: [],
    itemName: '',
    itemCode: '',
    rmCode: '',
    fullName: '',
    branchDisplay: ''
  };

  // new var
  status = '';
  campaignCode = '';
  notificationType = '';
  created_by = '';
  branch = '';
  campaignId: string;
  detailCampaign: any;

  items: MenuItem[];
  tabIndex = 0;

  constructor(
    injector: Injector,
    private campaignService: CampaignsService,
    private categoryService: CategoryService,
    private productService: ProductService,
    private fileService: FileService,
    protected translate: TranslateService,
    private kpiReportApi: KpiReportApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CAMPAIGN}`);
    this.campaignCode = this.route.snapshot.queryParams.campaignCode;
    this.prop = get(this.router.getCurrentNavigation(), 'extras.state');
    console.log(this.prop);
    
  }

  ngOnInit(): void {
    this.campaignId = _.get(this.route.snapshot.params, 'id');
    if (this.prop?.hasOwnProperty('tabIndex')) {
      this.tabIndex = this.prop.tabIndex;
    }
    if (!this.campaignId && this.prop?.hasOwnProperty('tabIndex')) {
      this.campaignId = this.prop.campaignId;
    }
    this.avatarFileName = this.fields?.chooseFile;

    this.sessionService.setSessionData(SessionKey.COMMON_DATA_CAMPAIGN_DETAILS, 'SME')
    this.items = [
      { label: 'Home', icon: 'pi pi-fw pi-home' },
      { label: 'Calendar', icon: 'pi pi-fw pi-calendar' },
      { label: 'Edit', icon: 'pi pi-fw pi-pencil' },
      { label: 'Documentation', icon: 'pi pi-fw pi-file' },
      { label: 'Settings', icon: 'pi pi-fw pi-cog' }
    ];

    this.isLoading = true;
    this.isHO = this.currUser?.branch === BRANCH_HO;

    forkJoin([
      this.commonService
        .getCommonCategory(CommonCategory.EXT_CAMPAIGNS_CHANNELS)
        .pipe(catchError((e) => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.CAMPAIGN_TYPE).pipe(catchError((e) => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.CAMPAIGN_FORM).pipe(catchError((e) => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.SOURCE_DATA).pipe(catchError((e) => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.CAMPAIGN_DIVISION).pipe(catchError((e) => of(undefined))),
      this.productService.getProductByLevel(ProductLevel.Lvl1).pipe(catchError((e) => of(undefined))),
      this.categoryService.getBlocksCategoryByRm().pipe(catchError((e) => of(undefined))),
      this.translate.get('campaign').pipe(catchError((e) => of(undefined))),
      this.categoryService
        .getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW)
        .pipe(catchError((e) => of(undefined))),
      this.categoryService.getRegion().pipe(catchError((e) => of(undefined))),
      this.campaignService.getSMECampaignDetail(this.campaignId,this.objFunction?.rsId, Scopes.VIEW).pipe(catchError((e) => of(undefined))),
    ]).subscribe(
      ([
        listSaleChannel,
        listPurpose,
        listFormality,
        listSourceData,
        listBizLine,
        listProduct,
        listBlock,
        campaign,
        branchesOfUser,
        listRegion,
        detailCampaign,
      ]) => {
        
        this.prop = {
          ...this.prop,
          listSaleChannel,
          listPurpose,
          listFormality,
          listSourceData,
          listBizLine,
          listProduct,
          listBlock,
          campaign,
          branchesOfUser,
          listRegion,
        };

        if (!this.prop?.tabIndex) {
          this.tabIndex = this.route.snapshot.queryParams.tabIndex ? this.route.snapshot.queryParams.tabIndex : 0;
        }

        this.allowActive = this.isUpdateSME(detailCampaign);
        this.campaignCode = detailCampaign?.code
        this.detailCampaign = detailCampaign;
        this.mapData();
      }
    );
  }

  mapData() {
    this.listPurpose = this.prop?.listPurpose?.content || [];
    this.listFormality = this.prop?.listFormality?.content || [];
    this.listProduct =
      this.prop?.listProduct
        ?.filter((i) => i.level === ProductLevel.Lvl1)
        ?.map((item) => {
          return { code: item.idProductTree, name: item.idProductTree + ' - ' + item.description };
        }) || [];
    this.listBlock =
      this.prop?.listBlock?.map((item) => {
        return { code: item.code, name: item.code + ' - ' + item.name };
      }) || [];
    this.listChannel = this.prop?.listSaleChannel?.content || [];
    this.listChannel.unshift({ code: 'ALL', name: this.fields?.all });
    this.listChannel = unionBy(this.listChannel, 'code');
    this.listSourceData = this.prop?.listSourceData?.content || [];
    this.form.controls.sourceId.setValue(this.listSourceData.find((item) => item.isDefault)?.code);
    this.listBizLine = this.prop?.listBizLine?.content || [];

    Object.keys(this.prop?.campaign?.status).forEach((key) => {
      this.listStatus.push({ code: key.toUpperCase(), name: this.prop?.campaign?.status[key] });
    });
    this.form.controls.status.setValue(this.listStatus[0]?.code);
    this.listBranch = this.prop?.branchesOfUser;
    this.listBranchByRegion = this.prop?.branchesOfUser;
    this.branchesCode = this.prop?.branchesOfUser?.map((item) => item.code);
    if (this.isHO) {
      this.listRegion = this.prop?.listRegion;
    } else {
      const areaCodes = [];
      this.prop?.listRegion?.forEach((item) => {
        if (
          this.prop?.branchesOfUser?.findIndex((i) => i.khuVucM === item?.locationCode) !== -1 &&
          this.listRegion.findIndex((r) => r.locationCode === item?.locationCode) === -1
        ) {
          this.listRegion.push(item);
          areaCodes.push(item?.locationCode);
        }
      });
      this.form.controls.scope.setValue(CampaignSize.CHI_NHANH);
      this.form.controls.scope.disable();
      this.form.controls.areaCode.setValue(areaCodes);
      this.form.controls.areaCode.disable();

    }
    this.isLoading = false;
  }

  ngAfterViewInit() {
    this.form.controls.scope.valueChanges.subscribe((value) => {
      this.isRegion = value === CampaignSize.CHI_NHANH;
      if (this.isRegion) {
        this.form.controls.areaCode.setValidators(CustomValidators.required);
        this.form.controls.branchCodes.setValidators(CustomValidators.required);
      } else {
        this.form.controls.areaCode.clearValidators();
        this.form.controls.branchCodes.clearValidators();
      }
      this.form.updateValueAndValidity({ emitEvent: false });
    });
    this.form.controls.productCode.valueChanges.subscribe((value) => {
      this.getProcess(value, this.form.controls.blockCode.value);
    });
    this.form.controls.blockCode.valueChanges.subscribe((value) => {
      this.getProcess(this.form.controls.productCode.value, value);
    });
    this.form.controls.startDate.valueChanges.subscribe((value) => {
      if (value && value !== '') {
        if (moment(value).isBefore(moment())) {
          this.minEndDate = new Date();
        } else {
          this.minEndDate = value;
        }
      }
    });
    this.form.controls.areaCode.valueChanges.subscribe((value) => {
      if (value) {
        this.listBranchByRegion = this.listBranch.filter((item) => value.includes(item.khuVucM));
        this.branchesCode = [];
      }
    });
    this.form.controls.startDate.valueChanges.subscribe((value) => {
      const endDate = this.form.controls.endDate.value;
      if (value !== '' && endDate !== '' && moment(value).isAfter(moment(endDate))) {
        this.form.controls.endDate.setValue('');
      }
    });
    this.form.controls.blockCode.valueChanges.subscribe((value) => {
      this.listBizLineByBlock = this.listBizLine.filter((item) => item.value?.includes(value));
    });
  }

  save() {
    this.form.controls.branchCodes.setValue(this.branchesCode);
    if (this.form.valid) {
      this.confirmService.confirm().then((res) => {
        if (res) {
          this.isLoading = true;
          const data = cleanDataForm(this.form);
          if (Utils.isStringEmpty(data.process)) {
            this.messageService.warn(this.notificationMessage.CAMPAIGN_E102);
            this.isLoading = false;
            return;
          }
          data.branchCodes = this.branchesCode;
          if (data.scope === CampaignSize.TOAN_HANG) {
            delete data.areaCode;
            delete data.branchCodes;
          }
          this.campaignService.create(data).subscribe(
            () => {
              this.isLoading = false;
              this.back();
              this.messageService.success(this.notificationMessage.success);
            },
            (e) => {
              this.isLoading = false;
              if (e?.status === 0) {
                this.messageService.error(this.notificationMessage.E001);
                return;
              }
              this.messageService.error(JSON.parse(e?.error)?.description);
            }
          );
        }
      });
    } else {
      this.isValidator = true;
      validateAllFormFields(this.form);
    }
  }

  getProcess(productCode, blockCode) {
    if (Utils.trimNullToEmpty(productCode) !== '' && Utils.trimNullToEmpty(blockCode) !== '') {
      this.productService.getProcessByProductAndBlock(productCode, blockCode).subscribe((listData) => {
        if (listData?.content[0]) {
          this.form.controls.process.setValue(listData?.content[0]?.process);
        }
      });
    }
  }


  retype() {
    this.form.reset();
    this.branchesCode = [];
    if (!this.isHO) {
      const areaCodes = this.listRegion?.map((i) => i.locationCode);
      this.form.controls.scope.setValue(CampaignSize.CHI_NHANH);
      this.form.controls.scope.disable();
      this.form.controls.areaCode.setValue(areaCodes);
      this.form.controls.areaCode.disable();
    }
  }

  onTabChanged($event) {
    this.tabIndex = get($event, 'index');
  }

  changeInfo(data): void {
    this.campaign = data;
    this.campaign.createdBy = (data?.rmCode === undefined ? '' : data?.rmCode + ' - ') + data?.fullName;
    this.campaign.branchDisplay = (data?.branchCode === undefined ? '' : data?.branchCode + ' - ') + data?.branchName;
    switch (data?.status?.toLowerCase()) {
      case 'activated':
        this.campaign.statusDisplay = "Đã kích hoạt";
        break;
      case 'in_progress':
        this.campaign.statusDisplay = "Đang soạn thảo";
        break;
      case 'expired':
        this.campaign.statusDisplay = "Hết hạn";
        break;

    }
  }

  activeCampaign() {
    this.isLoading = true;
    this.campaignService.activeCampaign(this.campaignId).subscribe((result) => {
      this.isLoading = false;
      this.campaign.status = "ACTIVATED"
      this.messageService.success("Kích hoạt chiến dịch thành công!");
      // if (listData?.content[0]) {
      //   this.form.controls.process.setValue(listData?.content[0]?.process);
      // }
    }, (e) => {
      this.isLoading = false;
      this.messageService.error(e?.error?.description);
    });
  }

  isUpdateSME(item) {
    const isHO = this.currUser?.branch === BRANCH_HO;
    const isNotCreator = item.userName !== this.currUser?.username;
    return (
      (CampaignStatus.InProgress === item.status  && (!isNotCreator || isHO))
    );
  }

  changeLoading(isLoading = false): void {
    this.isLoading = isLoading;
    this.ref.detectChanges();
  }

  backToList(): void {
    this.router.navigateByUrl(functionUri.campaign);
  }
}
