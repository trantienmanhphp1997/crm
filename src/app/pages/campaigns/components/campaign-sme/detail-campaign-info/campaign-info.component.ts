import { Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { ControlContainer, FormControl, FormGroup } from '@angular/forms';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import { AppFunction } from 'src/app/core/interfaces/app-function.interface';
import { FileService } from 'src/app/core/services/file.service';
import { CommonCategory, FunctionCode } from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { CampaignsService } from '../../../services/campaigns.service';
import {get} from 'lodash';
import { formatDate } from '@angular/common';
import { Utils } from 'src/app/core/utils/utils';

@Component({
  selector: 'app-sme-campaign-info',
  templateUrl: './campaign-info.component.html',
  styleUrls: ['./campaign-info.component.scss']
})
export class CampaignInfoComponent extends BaseComponent implements OnInit {
  @Output() changeInfo = new EventEmitter();
  @Output() changeLoading = new EventEmitter();

  public objFunction: AppFunction;

  campaign = {
      id: "",
      code: "",
      name: "",
      parentCampaign: "",
      status: "",
      createdBy: "",
      branchCode: "",
      branchName: "",
      scope: "",
      startDate: "",
      endDate: "",
      formatedStartDate: "",
      formatedEndDate: "",
      slaRm: 0,
      slaManager: 0,
      content: "",
      areaCode: "",
      campaignAttachments: [],
      campaignBranchCodes: [],
      campaignRmLevels: [],
      participants: '',
      campaignItemAllocations: [],
      itemName: '',
      itemCode: '',
      rmCode: '',
      fullName: '',
      branchs: ''
  };

  isLoading = true;
  unit = {};
  scopeRegion = '';


  constructor(
    injector: Injector,
    private campaignService: CampaignsService,
    private categoryService: CategoryService,
    // private workflowService: WorkflowService,
    // private datePipe: DatePipe,
    // private productService: ProductService,
    private fileService: FileService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CAMPAIGN}`);
   }

  ngOnInit(): void {
    this.campaign.id = get(this.route.snapshot.params, 'id');
    this.changeLoading.emit(true);
    forkJoin([
      this.campaignService.getSMECampaignDetail(this.campaign.id, this.objFunction?.rsId, 'VIEW').pipe(catchError((e) => of(undefined))),
    ]).subscribe(([detail]) => {
      this.campaign = detail || {};
      if(this.campaign?.endDate !== undefined && this.campaign?.endDate !== null){
        this.campaign.formatedEndDate = formatDate(this.campaign.endDate, 'dd/MM/yyyy', 'en');
      }
      if(this.campaign?.startDate !== undefined && this.campaign?.startDate !== null){
        this.campaign.formatedStartDate = formatDate(this.campaign.startDate, 'dd/MM/yyyy', 'en');
      }
      if(this.campaign?.startDate !== undefined && this.campaign?.startDate !== null){
        this.campaign.formatedStartDate = formatDate(this.campaign.startDate, 'dd/MM/yyyy', 'en');
      }     
      if(this.campaign?.campaignBranchCodes !== undefined && this.campaign?.campaignBranchCodes !== null){
        let branchs = '';
        this.campaign.campaignBranchCodes?.map(item => {
          branchs += item.branchCode + ' - ' + item.branchName + '\n';
        })
        this.campaign.branchs = branchs;
      }
      this.scopeRegion = this.campaign?.scope;
      
      this.campaign.participants = '';
      this.campaign.campaignRmLevels?.map(item => {
        if(item.rmLevelName)
        this.campaign.participants += item.rmLevelName + '\n';
      });
      let area = '';
      this.campaign?.areaCode?.split(',')?.map(item=>{
        area += item.trim() + '\n'; 
      });
      this.campaign.areaCode = area;
      this.campaign.content = Utils.isNull(this.campaign.content) ? '' : this.campaign.content;
      this.changeInfo.emit(this.campaign);
      this.changeLoading.emit(false);
    });
  }

  downloadFile(item) {
    this.fileService
      .downloadFile(item.fileId, item.fileName)
      .pipe(catchError((e) => of(false)))
      .subscribe((res) => {
        this.changeLoading.emit(false);
        if (!res) {
          this.messageService.error(this.notificationMessage.error);
        }
      });
  }

}
