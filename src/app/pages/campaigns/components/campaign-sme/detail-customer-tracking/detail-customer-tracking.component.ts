import { BaseComponent } from 'src/app/core/components/base.component';
import { Component, OnInit, Injector, ViewChildren, QueryList, ChangeDetectorRef, Input, SimpleChanges, Output, EventEmitter} from '@angular/core';
import * as _ from 'lodash';
import { global } from '@angular/compiler/src/util';
import { forkJoin, of } from 'rxjs';
import {
	CommonCategory,
	FunctionCode,
	functionUri,
	maxInt32,
	Scopes
} from 'src/app/core/utils/common-constants';
import { catchError, finalize } from 'rxjs/operators';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { RmApi } from 'src/app/pages/rm/apis';
import { Utils } from 'src/app/core/utils/utils';
import { CampaignsService } from '../../../services/campaigns.service';
import { CalendarAddModalComponent } from 'src/app/pages/calendar-sme/components/calendar-add-modal/calendar-add-modal.component';
import { CustomerLeadService } from 'src/app/pages/lead/service/customer-lead.service';
import { MatMenuTrigger } from '@angular/material/menu';
import { CustomerAssignmentApi } from 'src/app/pages/customer-360/apis';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { PopupUpdateNoteComponent } from '../popup-update-note/popup-update-note.component';
import { FileService } from 'src/app/core/services/file.service';

@Component({
	selector: 'app-detail-customer-tracking',
	templateUrl: './detail-customer-tracking.component.html',
	styleUrls: ['./detail-customer-tracking.component.scss']
})
export class DetailCustomerTrackingComponent extends BaseComponent implements OnInit {
	@ViewChildren(MatMenuTrigger) trigger: QueryList<any>;
	@Input() campaignCode: string;
	@Input() campaignId: string;
	@Output() changeInfo = new EventEmitter();
	@Output() changeLoading = new EventEmitter();
	listType: any = [
		{
			"value": null,
			"name": "Tất cả"
		}, {
			"value": 1,
			"name": "Khách hàng có code"
		}, {
			"value": 0,
			"name": "Khách hàng chưa code"
		}
	]
	listResult: any = [];
	listResultSearch = [{
		"code": null,
		"name": "Tất cả"
	}];
	listContactPosition: any[];
	listData: any[];
	prevParams: any;
	isRM: boolean = true;
	pageable: Pageable;
	objFunctionCustomer: any;
	listRmManager: any[];
	listRmManagerTerm: any[];
	listBranchesManager: any[];
	customerType = 'SME';
	size: number = _.get(global, 'userConfig.pageSize');
	objFunctionRm: any;
	form = this.fb.group({
		customerType: [null],
		customerCode: [null],
		taxCode: [null],
		campaignId: [null],
		approachingResult: [null],
		branchCodes: [null],
		rmCodes: [null],
		page: 0,
		size: this.size,
		rsId: this.objFunction?.rsId,
		scope: Scopes.VIEW,
	});
	listEmail = [];
	viewMode = 1;

	info: any = {};

	constructor(
		public categoryService: CategoryService,
		private campaignService: CampaignsService,
		private service: CustomerLeadService,
		private customerAssignmentApi: CustomerAssignmentApi,
		private rmApi: RmApi,
		injector: Injector,
		private fileService: FileService
	) {
		super(injector);
		this.objFunctionRm = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
		this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CAMPAIGN}`);
		this.objFunctionCustomer = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
	}

	ngOnInit(): void {
		this.info = this.campaignService.info.value; // get info from service
		this.campaignId = _.get(this.route.snapshot.params, 'id', this.campaignId) || this.info.campaignId;
		this.form.get('campaignId').setValue(this.campaignId);
		this.form.get('rsId').setValue(this.objFunction?.rsId);

		this.initData();


	}

	initData() {
		this.changeLoading.emit(true);
		let params = this.form.value;
		this.commonService.getCommonCategory(CommonCategory.CAMPAIGN_APPROACHING_RESULT).subscribe(
			(data) => {
				let results = data?.content || [];
				this.listResult.push({
					"code": null,
					"name": "--"
				})
				this.listResultSearch.push(...results);
				this.listResult.push(...results);
			}
		)
		forkJoin([
			this.categoryService
				.branchesByUserPermission(this.objFunction?.rsId, Scopes.VIEW)
				.pipe(catchError(() => of(undefined))),
			this.rmApi
				.post('findAll', {
					page: 0,
					size: maxInt32,
					crmIsActive: true,
					branchCodes: [],
					rmBlock: this.customerType,
					rsId: this.objFunctionRm?.rsId,
					scope: Scopes.VIEW,
				})
				.pipe(catchError(() => of(undefined))),
			this.campaignService.getListResult(this.campaignId, params).pipe(catchError(() => of(undefined))),
			this.categoryService.getCommonCategory(CommonCategory.CONTACT_POSITION).pipe(catchError(() => of(undefined))),
			this.campaignService.getSMECampaignDetail(this.campaignId,this.objFunction?.rsId, Scopes.VIEW).pipe(catchError((e) => of(undefined))),
		]).subscribe(([ branchOfUser, listRmManagement, listData, listContactPosition, campaign]) => {
			this.changeInfo.emit(campaign);
			this.listData = listData?.content || [];
			this.pageable = {
				totalElements: listData?.totalElements,
				totalPages: listData?.totalPages,
				currentPage: 0,
				size: this.size,
			};
			if (_.isEmpty(branchOfUser)) {
				this.isRM = true;
			}else{
				this.isRM = false;
			}
			this.listContactPosition = listContactPosition?.content || [];
			// -----------------------
			const listRm = [];
			listRmManagement?.content?.forEach((item) => {
				if (!_.isEmpty(item?.t24Employee?.employeeCode)) {
					listRm.push({
						code: _.trim(item?.t24Employee?.employeeCode),
						displayName: _.trim(item?.t24Employee?.employeeCode) + ' - ' + _.trim(item?.hrisEmployee?.fullName),
						branchCode: _.trim(item?.t24Employee?.branchLevel2),
					});
				}
			});
			if (!listRm?.find((item) => item.code === this.currUser?.code)) {
				listRm.push({
					code: this.currUser?.code,
					displayName:
						Utils.trimNullToEmpty(this.currUser?.code) + ' - ' + Utils.trimNullToEmpty(this.currUser?.fullName),
					branchCode: this.currUser?.branch,
				});
			}
			this.listRmManager = [...listRm];
			this.listRmManagerTerm = [...listRm]
			this.listBranchesManager =
				branchOfUser?.map((item) => {
					return {
						code: item.code,
						name: `${item.code} - ${item.name}`,
					};
				}) || [];
			if (!_.find(this.listBranchesManager, (item) => item.code === this.currUser?.branch)) {
				this.listBranchesManager.push({
					code: this.currUser?.branch,
					name: `${this.currUser?.branch} - ${this.currUser?.branchName}`,
				});
			}
			if (!_.find(this.listBranchesManager, (item) => item.code === this.currUser?.branch)) {
				this.listBranchesManager.push({
					code: this.currUser?.branch,
					name: `${this.currUser?.branch} - ${this.currUser?.branchName}`,
				});
			}

			this.changeLoading.emit(false);
		}, (e) => {
			this.messageService.error(e.error.description);
			this.changeLoading.emit(false);
		});
		this.form.get('branchCodes').valueChanges.subscribe((value) => {
			// this.formSearch.get('listRmManager').setValue('');
			if (value) {
				let listRmManager = [];
				value.forEach(element => {
					let listRMInBranch = _.filter(
						this.listRmManagerTerm,
						(item) => item.branchCode === element
					);
					listRmManager.push(...listRMInBranch);
				});
				this.listRmManager = [...listRmManager];
			} else {
				this.listRmManager = this.listRmManagerTerm;
			}
		});
	}

	search(page) {
		this.form.get('rsId').setValue(this.objFunction?.rsId);
		var dataSearch = this.form.value;
		var listRmCode = dataSearch.rmCodes;
		if (listRmCode?.length == this.listRmManagerTerm?.length && listRmCode?.length > 1000) {
			dataSearch.rmCodes = [];
		} else {
			if (listRmCode?.length > 1000) {
				this.messageService.error("Vui lòng chọn ít hơn 1000 RM hoặc chọn tất cả !");
				return;
			}
		}
		let params = this.form.value;
		params.page = page;
		this.changeLoading.emit(true);

		if (this.viewMode === 1) {
			this.getResultList(page, params);
		} else if (this.viewMode === 2) {
			this.getApproachingResult(page, params);
		}
	}

	getResultList(page, params): void {

		if (!page) this.listData = [];

		this.campaignService.getListResult(this.campaignId, params).subscribe((result) => {
			if (result) {
				this.listData = result?.content || [];
				this.pageable = {
					totalElements: result?.totalElements,
					totalPages: result?.totalPages,
					currentPage: page,
					size: this.size,
				};
			}
			this.changeLoading.emit(false);
		},
			(e) => {
				if (e.error.code) {
					this.messageService.error(e.error.description);
					this.changeLoading.emit(false);
					return;
				}
				this.messageService.error(this.notificationMessage.error);
				this.changeLoading.emit(false);
			});
	}

	getApproachingResult(page, params): void {

		if (!page) this.listData = [];

		params.isRm = this.isRM;
		this.campaignService.getApproachingResult(this.campaignId, params).pipe(
			finalize(() => {
				this.changeLoading.emit(false);
			})
		).subscribe((result) => {
			if (!result) return;

			this.listData = result?.content || [];
			this.pageable = {
				totalElements: result?.totalElements,
				totalPages: result?.totalPages,
				currentPage: page,
				size: this.size,
			};
		})
	}

	onActive(event) {

		if (event.type === 'dblclick') {
			const item = _.get(event, 'row');
			if (!_.isEmpty(_.trim(item.customerCode))) {
				this.router.navigate([functionUri.customer_360_manager, 'detail', _.toLower(item.block), item.customerCode], {
					skipLocationChange: true,
					queryParams: {
						manageRM: item.rm,
						tabIndex: 0,
						showBtnRevenueShare: false,
					},
				})
			} else if (!_.isEmpty(_.trim(item.leadCode))) {
				event.cellElement.blur();
				const item = _.get(event, 'row');
				this.router.navigate([functionUri.lead_management, 'detail', item.leadCode], { state: this.prevParams });
			}
		}
	}


	onChange(event) {
		const modal = this.modalService.open(PopupUpdateNoteComponent, { windowClass: 'popup-update-note' });
		modal.componentInstance.data = event;
		modal.componentInstance.campaignId = this.campaignId;
		modal.result
			.then((res) => {
				this.search(0);
				if (res) {

				} else {
				}
			})
			.catch(() => { });
	}

	detectChange(event) {
		this.ref.detectChanges();
		// console.log("detectChange : ",event)
		// const modal = this.modalService.open(PopupUpdateNoteComponent, { windowClass: 'popup-update-note' });
		// modal.componentInstance.data = event;
		// modal.componentInstance.campaignId = this.campaignId;
	}

	setPage(pageInfo) {
		if (this.isLoading) {
			return;
		}
		let page = pageInfo.offset;
		this.search(page);
	}
	createTaskTodo(row) {
		const modal = this.modalService.open(CalendarAddModalComponent, { windowClass: 'create-calendar-modal' });
		modal.componentInstance.customerCode = row.customerCode;
		modal.componentInstance.customerName = row.customerName;
		modal.componentInstance.isDetailLead = true;
		modal.result
			.then((res) => {
				if (res) {
				} else {
				}
			})
			.catch(() => {
			});
	}

	getAllMailAndPosition(row) {
		this.listEmail = [];
		const data = {
			leadCode: row.customerCode ? '' : row.leadCode ? row.leadCode : '',
			customerCode: row.customerCode ? row.customerCode : '',
			rsId: this.objFunction.rsId,
			scope: 'VIEW'
		}
		this.service.getAllMail(data).subscribe(value => {
			if (value) {
				this.listEmail = value.filter(e => e.mail !== undefined);
				if (_.isEmpty(this.listEmail)) {
					this.mailTo(row, undefined)
				}
				this.listContactPosition.forEach(item => {
					this.listEmail.forEach(e => {
						if (item.code === e.position) {
							e.position = item.name;
						}
					})
				});
			}
		}, () => {
			this.trigger.forEach(item => {
				item.closeMenu();
			});
			this.messageService.error(this.notificationMessage.error);
		});
	}

	mailTo(row, mail) {
		if (_.isEmpty(mail)) {
			this.trigger.forEach(item => {
				item.closeMenu();
			});
			this.messageService.error('Không có thông tin Email');
		} else {
			const data = {
				type: 'EMAIL',
				customerCode: row.customerCode ? row.customerCode : '',
				leadCode: row.leadCode ? row.leadCode : '',
				scope: Scopes.VIEW,
				rsId: this.objFunctionCustomer.rsId
			}
			this.customerAssignmentApi.updateActivityLog(data).subscribe(() => {
			});
			const url = `mailto:${mail}`;
			window.location.href = url;
		}
	}
	getNameResult(value) {
		console.log("getNameResult : ",value)
		let result = this.listResult?.find((item) => item.code === value)
		return result?.name;
	}
	createFromFile() {
		this.router.navigate([functionUri.campaign, 'assign-customer-sme-import-file'],
			{
				state: {
					campaignId: this.form.value.campaignId,
					campaignCode: this.campaignCode
				}, skipLocationChange: true
			});
	}
	getRMInfo(row) {
		let rmCode = "";
		let rmName = "";
		rmCode = row?.rmCode == undefined ? "" : row?.rmCode;
		rmName = row?.rmName == undefined ? "" : row?.rmName;

		return rmCode.concat(' - ').concat(rmName)
	}

	exportApproachingResultFile() {
		if (!this.maxExportExcel) {
			return;
		}

		if (+(this.pageable?.totalElements || 0) === 0) {
			this.messageService.warn(this.notificationMessage.noRecord);
			return;
		}

		if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
			this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
				this.messageService.warn(res);
			});
			return;
		}

		this.changeLoading.emit(true);
		if(this.viewMode === 2){
			this.campaignService.exportAppoachingResult(this.campaignId, this.form.value).pipe(
				finalize(() => {
					this.changeLoading.emit(false);
				})
			).subscribe(
				(fileId) => {
					if (!fileId) return;
	
					this.downloadFile(fileId);
				}, () => {
					this.messageService.error(this.notificationMessage.error);
				}
			);
		}else{
			this.campaignService.exportCustomerTracking(this.campaignId, this.form.value).pipe(
				finalize(() => {
					this.changeLoading.emit(false);
				})
			).subscribe(
				(fileId) => {
					if (!fileId) return;
	
					this.fileService.downloadFile(fileId, `Danh_sach_theo_doi_KH.xlsx`).pipe(
						finalize(() => {
							this.changeLoading.emit(false);
						})
					).subscribe(res => {
						if (!res) {
							this.messageService.error(this.notificationMessage.error);
						}
					});
				}, () => {
					this.messageService.error(this.notificationMessage.error);
				}
			);
		}
		
	}

	downloadFile(fileId: string): void {
		const side = this.isRM ? 'RM' : 'chi_nhanh';
		this.fileService.downloadFile(fileId, `dsach_KH_tiep_can_chieu_${side}.xlsx`).pipe(
			finalize(() => {
				this.changeLoading.emit(false);
			})
		).subscribe(res => {
			if (!res) {
				this.messageService.error(this.notificationMessage.error);
			}
		});
	}
	allowUpdate(row){
		return this.isRM || row?.rmCode == this.currUser?.code;
	}
}
