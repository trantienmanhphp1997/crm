import { Component, EventEmitter, HostBinding, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import _ from 'lodash';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { Utils } from 'src/app/core/utils/utils';
import { global } from '@angular/compiler/src/util';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { TranslateService } from '@ngx-translate/core';
import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';
import { ChooseBranchesModalComponent } from 'src/app/pages/system/components/choose-branches-modal/choose-branches-modal.component';

@Component({
  selector: 'kpi-report-branch-table',
  templateUrl: './kpi-report-branch-table.component.html',
  styleUrls: ['./kpi-report-branch-table.component.scss'],
})
export class KpiReportBranchTableComponent implements OnInit {
  constructor(
    private modalService: NgbModal,
    private messageService: NotifyMessageService,
    private translate: TranslateService
  ) {
    this.messages = global.messageTable;
    this.translate.get(['fields', 'notificationMessage']).subscribe((result) => {
      this.notificationMessage = _.get(result, 'notificationMessage');
    });
  }
  notificationMessage: any;
  listTemp: Array<any> = [];
  @Input() listBranch: Array<any>;
  params = {
    page: 0,
    size: global.userConfig.pageSize,
    search: '',
  };
  @HostBinding('class.app__right-content') appRightContent = true;
  @Input() model: Array<any>;
  pageable: Pageable;
  isSearch = true;
  textSearch = '';
  prev_block: string;
  branches: Array<any>;
  isLoading: boolean;
  messages: any;
  models: Array<any>;
  @ViewChild('table') table: DatatableComponent;
  search_value: string;
  @Output() modelChange = new EventEmitter();

  ngOnInit(): void {
    this.listTemp = this.model || [];
    this.calculatePageble(this.model);
  }

  selectMembers() {
    const modal = this.modalService.open(ChooseBranchesModalComponent, { windowClass: 'tree__branches-modal' });
    modal.componentInstance.listBranchOld = _.map(this.model, (x) => x.code) || [];
    modal.componentInstance.listBranch = this.listBranch;
    modal.result
      .then((res) => {
        if (res) {
          this.modelChange.emit(res);
          const listSelected = _.map(res, (x) => {
            return {
              code: _.get(x, 'code'),
              name: _.get(x, 'name'),
            };
          });
          this.listTemp = [...listSelected];
          res.forEach((code) => {
            this.listTemp = this.listTemp.filter((item) => {
              return item.code !== code;
            });
          });
          this.search();
        }
      })
      .catch(() => {});
  }

  setPage(pageInfo) {
    this.params.page = pageInfo.offset;
    this.search();
  }

  deleteMember(row) {
    _.remove(this.listTemp, (x) => x.code === _.get(row, 'code'));
    this.search();
    this.modelChange.emit(this.listTemp);
  }

  deleteAll() {
    this.listTemp = [];
    this.calculatePageble(this.listTemp);
    this.modelChange.emit(this.listTemp);
  }

  getValue(row, key) {
    return _.get(row, key);
  }

  add() {
    if (Utils.isStringNotEmpty(this.search_value)) {
      this.search_value = Utils.isStringNotEmpty(this.search_value) ? Utils.trim(this.search_value) : '';
      const check = _.size(_.find(this.listTemp, (x) => x.code.toLowerCase() === this.search_value.toLowerCase()));
      if (check === 0) {
        const branch = _.find(this.listBranch, (x) => x.code.toLowerCase() === this.search_value.toLowerCase());
        if (Utils.isNotNull(branch)) {
          // if (!this.validate(_.size(this.listTemp) + 1)) return;
          this.listTemp.push(branch);
          this.search();
          this.modelChange.emit(this.listTemp);
          this.search_value = '';
        } else {
          this.messageService.warn('Chi nhánh không tồn tại hoặc không thuộc chi nhánh quản lý');
        }
      } else {
        this.messageService.warn(_.get(this.notificationMessage, 'branchCodeIsExist'));
        return;
      }
    } else {
      this.messageService.warn(_.get(this.notificationMessage, 'branchCodeEmpty'));
      return;
    }
  }

  search() {
    const codes = _.map(this.listTemp, (x) => x.id);
    const datas = _.chain(this.listTemp)
      .filter((item) => codes.includes(item.id))
      .value();
    this.calculatePageble(datas);
  }

  validate(count) {
    if (count > 10) {
      this.messageService.warn('Số lượng đơn vị không được vượt quá 10');
      return false;
    }
    return true;
  }

  calculatePageble(datas: Array<any>) {
    this.branches = _.slice(
      datas,
      this.params.page * this.params.size,
      this.params.page * this.params.size + this.params.size
    );
    this.pageable = {
      totalElements: _.size(datas),
      totalPages: _.size(datas) / this.params.size,
      currentPage: _.get(this.params, 'page'),
      size: _.get(this.params, 'size'),
    };
  }
}
