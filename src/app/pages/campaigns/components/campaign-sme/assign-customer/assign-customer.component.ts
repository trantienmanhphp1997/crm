import { Component, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { ControlContainer, FormGroup } from '@angular/forms';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { BaseComponent } from 'src/app/core/components/base.component';
import { FunctionCode, functionUri, Scopes, SessionKey, TARGET_PICK_BY } from 'src/app/core/utils/common-constants';
import { global } from '@angular/compiler/src/util';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { cleanDataForm } from 'src/app/core/utils/function';
import { get, find } from 'lodash';

@Component({
  selector: 'app-assign-customer-sme',
  templateUrl: './assign-customer.component.html',
  styleUrls: ['./assign-customer.component.scss']
})
export class AssignCustomerComponent extends BaseComponent implements OnInit {
  @Input() data;
  @ViewChild('table') table: DatatableComponent;

  form: FormGroup;

  TARGET_PICK_BY = TARGET_PICK_BY;

  limit = global.userConfig.pageSize;
  commonData = {
    listRmManagementTerm: [],
    listRmManagement: [],
    listStatus: [],
    listCustomerType: [],
    listBranch: [],
    listScreeningResults: [],
    listSourceCustomer: [],
    listBusiness: [],
    listSegment: [],
    potentialLevelConfig: [],
    potentialResourceConfig: [],
    listSlas: [
      {
        value: '3',
      },
      {
        value: '2',
      },
      {
        value: '1',
      },
    ],
  };
  searchForm = this.fb.group({
    leadCode: null,
    fullName: '',
    customerType: '1',
    businessRegistrationNumber: '',
    taxCode: '',
    phone: '',
    listBranchSearch: [],
    lstRmCode: [],
    filters: [],
    sourceLead: '',
    status: '',
    industry: '',
    segment: '',
    slas: [],
  });

  commonDataKHCN = {
    listRmManagementTerm: [],
    listRmManagement: [],
    listStatus: [],
    listCustomerType: [],
    listBranch: [],
    potentialLevelConfig: [],
    potentialResourceConfig: [],
  };

  searchFormKHCN = this.fb.group({
    leadCode: null,
    customerName: '',
    customerType: '',
    idCard: '',
    phone: '',
    email: '',
    status: '',
    listBranch: [],
    listRM: [],
    campaign: [],
  });

  isOpenMore: boolean;
  industryCode = '';
  listData = [];
  selected = [];
  pageable: Pageable;
  user: any;
  paramSearch = {
    page: 0,
    size: get(global, 'userConfig.pageSize'),
    rsId: '',
    scope: Scopes.VIEW,
  };
  prevParams: any;
  prevParamsKHCN: any;
  checkAll = false;

  isFirst = false;
  scopes: Array<any>;
  scope = 'branch';

  @Input() dataSearch: any;
  @Input() listSelected = [];
  @Input() isModal = false;
  @Input() modalActive: NgbModalRef;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    protected translate: TranslateService,
    public controlContainer: ControlContainer,
  ) {
    super(injector);

    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CAMPAIGN}`);
    this.scopes = get(this.objFunction, 'scopes');
    this.paramSearch.rsId = this.objFunction?.rsId;
    this.user = this.sessionService.getSessionData(SessionKey.USER_INFO);
  }

  ngOnInit() {
    this.form = this.controlContainer.control as FormGroup;

    this.pageable = {
      totalElements: 0,
      totalPages: 1,
      currentPage: this.paramSearch.page,
      size: this.limit,
    };

    this.initData();
  }

  initData() {
    this.isLoading = true;
    this.listData = [];
    forkJoin([
      this.categoryService
        .getBranchesOfUser(this.paramSearch.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined))),

    ]).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(
      ([
        branchOfUser
      ]) => {
        this.commonData.listBranch =
          branchOfUser?.map((item) => {
            return {
              code: item.code,
              name: `${item.code} - ${item.name}`,
            };
          }) || [];
        if (!find(this.commonData.listBranch, (item) => item.code === this.currUser?.branch)) {
          this.commonData.listBranch.push({
            code: this.currUser?.branch,
            name: `${this.currUser?.branch} - ${this.currUser?.branchName}`,
          });
        }
        this.search(true);
      }
    );
  }

  search(isSearch?: boolean) {
    // this.isLoading = true;
    let params = cleanDataForm(this.searchForm);
    if (isSearch) {
      this.paramSearch.page = 0;
    }
    params = { ...params, ...this.paramSearch };
    // this.listData = [{rmName: 'a'}, {rmName: 'a'},{rmName: 'a'},{rmName: 'a'},{rmName: 'a'}]
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.page = pageInfo.offset;
    this.search(false);
  }

  onActive() {

  }

  createFromFile() {
    this.router.navigate([functionUri.campaign, 'assign-customer-sme-import-file'],
      {
        state: {
          campaignId: this.form.value.campaignId,
          campaignCode: this.data?.info?.code
        }, skipLocationChange: true
      });
  }



  handleChangePageSize(event) {
    this.paramSearch.page = 0;
    this.paramSearch.size = event;
    this.limit = event;
    this.search(false);
  }

}
