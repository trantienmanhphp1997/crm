import { Component, OnInit, HostBinding, Injector, ViewEncapsulation, Input } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {
  CommonCategory,
  Format,
  FunctionCode,
  ListFrequencyCalendar,
  PriorityTaskOrCalendar,
  Scopes,
} from 'src/app/core/utils/common-constants';
import { finalize } from 'rxjs/operators';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as _ from 'lodash';
import { CampaignsService } from '../../../services/campaigns.service';

@Component({
	selector: 'app-popup-update-note',
	templateUrl: './popup-update-note.component.html',
	styleUrls: ['./popup-update-note.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class PopupUpdateNoteComponent extends BaseComponent implements OnInit {

	@Input() data?: any;
  @Input() campaignId?: any;
	listSuggest = [];
	form = this.fb.group({
		"campaignId": [null],
		"note": [null],
		"approachingResult": [null],
		"id": 0,
		"type": [null],
		"rmCode": [null],
		"rmBranchCode": [null]
	});

	constructor(injector: Injector,
		private  categoryService :CategoryService,
		private campaignService: CampaignsService,
		private modalActive: NgbActiveModal,
	){
    super(injector);
  }

	ngOnInit() {
		this.categoryService.getCommonCategory(CommonCategory.HINT_NOTE_CAMPAIGN).subscribe((result) => {
			if(this.data?.approachingResult){
				result?.content?.forEach(item => {
					if(item.value === this.data?.approachingResult)
						this.listSuggest.push(item.name);
				});
			}
		},
		(e) => {
			 if (e.error.code) {
					this.messageService.error(e.error.description);
					return;
			 }
			 this.messageService.error(this.notificationMessage.error);
		});
		this.form.controls.campaignId.setValue(this.campaignId);
		if(this.data?.approachingResult)
			this.form.controls.approachingResult.setValue(this.data?.approachingResult);
		if(this.data?.note)
			this.form.controls.note.setValue(this.data?.note);
		// this.form.controls.type.setValue(2);
		this.form.controls.rmCode.setValue(this.data?.rmCode);
		this.form.controls.rmBranchCode.setValue(this.data?.rmBranchCode);
		this.form.controls.id.setValue(this.data?.id);

	}
	closeModal() {
    this.modalActive.close(false);
  }
	onChangeSuggest(i){
		this.form.get('note').setValue(this.listSuggest[i]);
	}
	confirmDialog(){
		this.isLoading = true;
		let params = this.form.value;
		this.campaignService.updateResult(this.campaignId, params).subscribe((result) => {
			this.isLoading = false;
			this.messageService.success("Cập nhật dữ liệu thành công.");
			this.closeModal();
		},
		(e) => {
			 if (e.error.code) {
					this.messageService.error(e.error.description);
					this.isLoading = false;
					return;
			 }
			 this.messageService.error(this.notificationMessage.error);
			 this.isLoading = false;
		});
	}
}