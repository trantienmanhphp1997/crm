import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { catchError, finalize } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CampaignSize, CAMPAIGN_SCOPES, Division, FunctionCode, functionUri, TARGET_PICK_BY, SessionKey } from 'src/app/core/utils/common-constants';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { CampaignsService } from '../../../services/campaigns.service';
import { get, pick, isEmpty } from 'lodash';
import { FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { MenuItem } from 'primeng/api';
import { LevelApi } from 'src/app/pages/kpis/apis';
import { DashboardService } from 'src/app/pages/dashboard/services/dashboard.service';
import { DeclareCampaignComponent } from '../declare-campaign/declare-campaign.component';
import { Utils } from 'src/app/core/utils/utils';
import { MatDialog } from '@angular/material/dialog';
import { forkJoin, of } from 'rxjs';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateSaleCampaignComponent extends BaseComponent implements OnInit {

  @ViewChild('declareCampaign') declareComp: DeclareCampaignComponent;

  form = this.fb.group({
    info: this.fb.group({
      itemId: [''],
      parentCampaignId: [''],
      parentId: ['', CustomValidators.required],
      code: ['', CustomValidators.onlyNormalAndSpecialChar],
      campaignId: [''],
      name: ['', CustomValidators.required],
      startDate: [new Date(), CustomValidators.required],
      endDate: [''],
      scope: [CampaignSize.TOAN_HANG, CustomValidators.required],
      content: [''],
      slaManager: [null, Validators.compose([CustomValidators.required, CustomValidators.onlyInteger])], // phan giao
      slaRm: [null, Validators.compose([CustomValidators.required, CustomValidators.onlyInteger])], // tiep can
      titleGroupDTOS: [[]], // doi tuong tham gia
      campaignItems: [[]], // chi tieu
      files: [[]],
      imgList: [[]],
      fileList: [[]],
      blockCode: Division.SME,
      branchCodes: [[]],
      areaCodes: [[]],
      planValueForm: this.fb.array([])
    }),
    target: this.fb.group({
      pickBy: [TARGET_PICK_BY.MULTI],
      file: [''],
      campaignId: ''
    }),
    assignCustomer: this.fb.group({
      pickBy: [TARGET_PICK_BY.MULTI],
      file: [''],
      campaignId: ''
    })
  });
  isLoading = false;
  isFirtLoading = false;
  campaignId = '';
  status = 'Đang soạn thảo';
  campaignCode = '';
  created_by = '';
  branch = '';

  items: MenuItem[];
  tabIndex = 0;
  rmLevelList = [];
  campaign: any;

  constructor(
    injector: Injector,
    private campaignService: CampaignsService,
    public dialog: MatDialog,
    private levelApi: LevelApi,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CAMPAIGN}`);
    this.prop = get(this.router.getCurrentNavigation(), 'extras.state');
    if (this.prop?.hasOwnProperty('campaignId')) {
      this.campaignId = this.prop.campaignId;
    }
  }

  ngOnInit(): void {
    this.isFirtLoading = true;

    if (this.prop?.hasOwnProperty('tabIndex')) {
      this.tabIndex = this.prop.tabIndex;
    }

    this.infoForm.controls.scope.setValue(this.isHO ? CampaignSize.TOAN_HANG : CampaignSize.CHI_NHANH);
    this.sessionService.setSessionData(SessionKey.COMMON_DATA_CAMPAIGN_DETAILS, 'SME')
    forkJoin([
      this.levelApi
        .searchLevelByBlockCode(Division.SME)
        .pipe(catchError(() => of(undefined))),
    ]).pipe(
      finalize(() => {
        this.isFirtLoading = false;
      })
    ).subscribe(
      ([
        rmLevels,
      ]) => {
        this.rmLevelList = rmLevels?.content || [];
        this.infoForm.controls.titleGroupDTOS.setValue(this.rmLevelList.map(item => item.id));
      }
    );


  }

  ngAfterViewInit() {

    this.infoForm.controls.slaManager.valueChanges.subscribe((value) => {
      if (!isEmpty(value)) {
        const reg = new RegExp(/^[0-9]*$/g);
        value = value.toString().replace(/,/g, '');
        if (!reg.test(value)) {
          value = value.replace(/\D/g, '');
        }
        this.infoForm.controls.slaManager.setValue(Utils.numberWithCommas(value).substr(0, 50), { emitEvent: false });
      }
    });

    this.infoForm.controls.slaRm.valueChanges.subscribe((value) => {
      if (!isEmpty(value)) {
        const reg = new RegExp(/^[0-9]*$/g);
        value = value.toString().replace(/,/g, '');
        if (!reg.test(value)) {
          value = value.replace(/\D/g, '');
        }
        this.infoForm.controls.slaRm.setValue(Utils.numberWithCommas(value).substr(0, 50), { emitEvent: false });
      }
    })
  }

  save(isForward = false) {
    if (this.tabIndex > 0) {
      this.backToList();
      return;
    }
    if (this.form.invalid) {
      validateAllFormFields(this.form);
      this.infoForm.controls.planValueForm.markAllAsTouched();
      console.log('this.form.invalid')
      console.log('this.form:')
      console.log(this.form)
      return;
    }

    // validate info
    const { info } = cleanDataForm(this.form);

    if (info.scope === CAMPAIGN_SCOPES.CHI_NHANH) {
      if (!info.branchCodes?.length) {
        this.messageService.warn(`Chưa chọn chi nhánh!`);
        return;
      }

      if (!info.areaCodes?.length) {
        this.messageService.warn(`Chưa chọn vùng!`);
        return;
      }
    }

    if (!info.titleGroupDTOS?.length) {
      this.messageService.warn(`Chưa đối tượng tham gia!`);
      return;
    }

    if (moment(info.startDate).isAfter(moment(info.endDate))) {
      this.messageService.warn(this.notificationMessage.startDateMoreThanEndDate);
      return;
    }


    // active loading child 1
    this.declareComp.isLoading = true;

    info.titleGroupDTOS = this.getRmLevelList(info.titleGroupDTOS);
    info.files = this.getFileList(info.files);
    info.slaManager = info.slaManager.toString().replace(/,/g, '');
    info.slaRm = info.slaRm.toString().replace(/,/g, '');

    info.campaignId = info.code;

    const data = pick(info, [
      'campaignId',
      'code',
      'parentCampaignId',
      'name',
      'startDate',
      'endDate',
      'campaignItems',
      'titleGroupDTOS',
      'files',
      'slaManager',
      'slaRm',
      'scope',
      'content',
      'blockCode',
      'areaCodes',
      'branchCodes'
    ]);

    this.campaignService.createSME(data).pipe(
      finalize(() => {
        this.declareComp.isLoading = false;
      })
    ).subscribe(res => {
      this.messageService.success(this.notificationMessage.success);
      this.targetForm.controls.campaignId.setValue(res?.id);
      this.campaignId = res?.id;
      this.campaignCode = this.infoForm.value.code;
      this.campaignService.info.next({ campaignId: this.campaignId, campaignCode: this.campaignCode });

      if (!isForward) {
        this.backToList();
      } else {
        this.tabIndex++;
      }
    },
      (e) => {

        if (e?.status === 0) {
          this.messageService.error(this.notificationMessage.E001);
          return;
        }
        this.messageService.error(e?.error?.description);
      }
    );
  }

  getRmLevelList(idList: string[]): any[] {
    return this.rmLevelList.filter(item => idList.includes(item.id)).map(item => ({
      id: item.id,
      code: item.code,
      name: item.name,
      blockCode: item.blockCode
    }))
  }

  getFileList(fileObj): any[] {
    return Object.keys(fileObj).map(id => ({
      fileId: id,
      fileName: fileObj[id].name,
      filePath: fileObj[id].path,
      type: fileObj[id].type,
    }));
  }

  forwardToUpdate(): void {
    this.router.navigateByUrl(`${functionUri.campaign}/update-sme/${this.campaignId}?campaignCode=${this.infoForm.value.code}`);
  }


  onTabChanged($event) {
    this.tabIndex = get($event, 'index');
    if (this.tabIndex === 0 && this.isCreatedCampaign) {
      this.forwardToUpdate()
    }
  }

  getDetail(id: string): void {
    this.isLoading = true;
    this.campaignService.getSMECampaignDetail(id, this.objFunction?.rsId, 'VIEW').pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(campaign => {

      this.campaign = campaign;
      this.targetForm.controls.campaignId.setValue(campaign.id);
    })
  }

  backToList(): void {
    this.router.navigateByUrl(functionUri.campaign);
  }

  backStep(): void {
    if(this.tabIndex === 0){
      this.backToList();
    }else if(this.tabIndex === 1){
      this.forwardToUpdate();
    }else{
      this.tabIndex--;
    }
  }

  next(): void {
    if (this.tabIndex === 0) {
      this.save(true);
    } else {
      this.tabIndex++;
    }
  }

  get infoForm(): FormGroup {
    return this.form.get('info') as FormGroup;
  }

  get isHO(): boolean {
    return this.objFunction.HO;
  }

  get targetForm(): FormGroup {
    return this.form.get('target') as FormGroup;
  }

  get assignCustomerForm(): FormGroup {
    return this.form.get('assignCustomer') as FormGroup;
  }

  get isCreatedCampaign(): boolean {
    return !isEmpty(this.campaignService.info.value);
  }

  get isLastTab(): boolean {
    return this.tabIndex === 2;
  }

  changeLoading(isLoading = false): void {
    this.isLoading = isLoading;
    this.ref.detectChanges();
  }

}
