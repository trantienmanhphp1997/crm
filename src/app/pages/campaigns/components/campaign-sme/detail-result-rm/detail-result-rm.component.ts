import { Component, HostBinding, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CampaignsService } from '../../../services/campaigns.service';
import { get, lt } from 'lodash';
import { global } from '@angular/compiler/src/util';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { finalize } from 'rxjs/internal/operators/finalize';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FileService } from 'src/app/core/services/file.service';

@Component({
  selector: 'app-detail-result-rm',
  templateUrl: './detail-result-rm.component.html',
  styleUrls: ['./detail-result-rm.component.scss']
})
export class DetailResultRmComponent extends BaseComponent implements OnInit {
  @HostBinding('class') elementClass = 'app-detail-result-rm';
  campaignId = '';
  params: any = {};

  pageable: Pageable;

  listData = [];
  size: number = get(global, 'userConfig.pageSize');

  constructor(
    injector: Injector,
    private campaignService: CampaignsService,
    private modalActive: NgbActiveModal,
		private fileService: FileService
  ) {
    super(injector);

  }

  ngOnInit(): void {
  }

  search(page) {
    this.isLoading = true;

    this.params.page = page;

    this.campaignService.getApproachingResult(this.campaignId, this.params).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(result => {
      if (!result) return;

      this.listData = result?.content || [];
      this.pageable = {
        totalElements: result?.totalElements,
        totalPages: result?.totalPages,
        currentPage: page,
        size: this.size,
      };

    });
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }

    let page = pageInfo.offset;

    this.search(page);
  }

  closeModal() {
    this.modalActive.close();
  }

  exportApproachingResultFile() {
		if (!this.maxExportExcel) {
			return;
		}

		if (+(this.pageable?.totalElements || 0) === 0) {
			this.messageService.warn(this.notificationMessage.noRecord);
			return;
		}

		if (lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
			this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
				this.messageService.warn(res);
			});
			return;
		}

		this.isLoading = true;
		this.campaignService.exportAppoachingResult(this.campaignId, this.params).pipe(
      finalize(() => {
				this.isLoading = false;
			})
    ).subscribe(
			(fileId) => {
				if (!fileId) return;

				this.downloadFile(fileId);
			},
			() => {
				this.messageService.error(this.notificationMessage.error);
			}
		);
	}

  downloadFile(fileId: string): void {
		this.fileService.downloadFile(fileId, 'dsach_KH_tiep_can_chieu_RM.xlsx').pipe(
      finalize(() => {
				this.isLoading = false;
			})
    ).subscribe(res => {
				if (!res) {
					this.messageService.error(this.notificationMessage.error);
				}
			}
    );
	}
}
