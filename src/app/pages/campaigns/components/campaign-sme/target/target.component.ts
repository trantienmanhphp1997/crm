import { Component, Injector, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup, ControlContainer } from '@angular/forms';
import { DatatableComponent, TableColumn } from '@swimlane/ngx-datatable';
import { BaseComponent } from 'src/app/core/components/base.component';
import { FunctionCode, functionUri, Scopes, SessionKey, TARGET_PICK_BY } from 'src/app/core/utils/common-constants';
import { global } from '@angular/compiler/src/util';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { TranslateService } from '@ngx-translate/core';
import { FileService } from 'src/app/core/services/file.service';
import * as _ from 'lodash';
import { forkJoin, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { cleanDataForm } from 'src/app/core/utils/function';
import { CampaignsService } from '../../../services/campaigns.service';

@Component({
  selector: 'app-campaign-target-sme',
  templateUrl: './target.component.html',
  styleUrls: ['./target.component.scss']
})
export class CampaignTargetComponent extends BaseComponent implements OnInit {

  @ViewChild('table') table: DatatableComponent;
  @ViewChild('orderTmpl', { static: true }) orderTmpl: TemplateRef<any>;

  TARGET_PICK_BY = TARGET_PICK_BY;
  commonData = {
    listRmManagementTerm: [],
    listRmManagement: [],
    listStatus: [],
    listCustomerType: [],
    listBranch: [],
    listScreeningResults: [],
    listSourceCustomer: [],
    listBusiness: [],
    listSegment: [],
    potentialLevelConfig: [],
    potentialResourceConfig: [],
    listSlas: [
      {
        value: '3',
      },
      {
        value: '2',
      },
      {
        value: '1',
      },
    ],
  };
  typeCampaign = '2';
  form = this.fb.group({
    typeCampaign: [this.typeCampaign]
  });

  commonDataKHCN = {
    listRmManagementTerm: [],
    listRmManagement: [],
    listStatus: [],
    listCustomerType: [],
    listBranch: [],
    potentialLevelConfig: [],
    potentialResourceConfig: [],
  };

  searchFormKHCN = this.fb.group({
    leadCode: null,
    customerName: '',
    customerType: '',
    idCard: '',
    phone: '',
    email: '',
    status: '',
    listBranch: [],
    listRM: [],
    campaign: [],
  });

  campaignId = '';
  itemSelected: any;
  body: any;
  page: number = 0;
  limit = global.userConfig.pageSize;
  columns: TableColumn[];
  isOpenMore: boolean;
  industryCode = '';
  listData = [];
  selected = [];
  pageable: Pageable;
  user: any;
  paramSearch = {
    page: 0,
    size: _.get(global, 'userConfig.pageSize'),
    rsId: '',
    scope: Scopes.VIEW,
  };
  prevParams: any;
  prevParamsKHCN: any;
  checkAll = false;

  isFirst = false;
  scopes: Array<any>;
  scope = 'branch';

  @Input() dataSearch: any;
  @Input() listSelected = [];
  @Input() isModal = false;
  @Input() modalActive: NgbModalRef;
  isRM = false;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    protected translate: TranslateService,
    public controlContainer: ControlContainer,
    private campaignService: CampaignsService
  ) {
    super(injector);

    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CAMPAIGN}`);
    this.scopes = _.get(this.objFunction, 'scopes');
    this.paramSearch.rsId = this.objFunction?.rsId;
    this.user = this.sessionService.getSessionData(SessionKey.USER_INFO);
  }

  ngOnInit() {
    this.search(0);
  }

  search(page) {
    this.isLoading = true;
    this.campaignId = _.get(this.route.snapshot.params, 'id');
    this.body = {
      "campaignId": this.campaignId,
      "rsId": this.objFunction.rsId,
      "scope": "VIEW",
      "type": Number(this.typeCampaign),
    }
    forkJoin([
      this.campaignService.getHeaderAllocation(this.campaignId).pipe(catchError((e) => of(undefined))),
      this.campaignService.getListAllocation(this.body, this.page, this.limit)
        .pipe(catchError((e) => of(undefined))),
      this.campaignService.getSumAllocation(this.body)
        .pipe(catchError((e) => of(undefined))),
      this.categoryService
        .getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined))),
    ]).subscribe(([listHeader, listData, sumData, branchesOfUser]) => {
      this.getColumns(listHeader);
      let listAllocation = listData?.content;
      let listView = []
      if (listAllocation) {
        this.pageable = {
          totalElements: listData?.totalElements,
          totalPages: listData?.totalPages,
          currentPage: page,
          size: this.limit,
        };

        if (sumData && listHeader) {
          if (listData?.content?.length > 0) {
            let item = {
              "rmCode": "SUM",
              "rmName": "",
              "branchCode": this.typeCampaign === '3' ? "" : "SUM",
              "branchName": "",
              "parentBranchName": "",
              "region": "",
            }
            let count = 0;
            if (sumData[0]) {
              sumData[0].forEach(element => {
                item[listHeader[count]?.itemCode] = element;
                count++;
              });
            }
            listView.push(item);
          }

        }
        listAllocation.forEach(element => {
          let item = {
            "rmCode": element?.rmCode,
            "rmName": element?.rmName,
            "branchCode": element?.branchCode,
            "branchName": element?.branchName,
            "parentBranchName": element?.parentBranchName,
            "region": element?.region,
          }
          let listValue = element?.value;
          if (listValue && listHeader) {
            let count = 0;
            listValue.forEach(element => {
              item[listHeader[count]?.itemCode] = element;
              count++;
            });
          }
          listView.push(item);
        });
      }

      this.isRM = _.isEmpty(branchesOfUser);

      this.listData = [...listView];
      this.isLoading = false;
    },
      (e) => {
        this.messageService.error(e.error.description);
        this.isLoading = false;
      });
  }

  getColumns(listHeader) {
    this.columns = [
      {
        name: 'STT',
        cellTemplate: this.orderTmpl,
        sortable: false,
        width: 10,
        draggable: false,
        headerClass: 'text-center',
      }];
    if (this.typeCampaign === '3') {
      let infoRm = [{
        name: 'Mã RM',
        prop: 'rmCode',
        sortable: false,
        draggable: false,
      },
      {
        name: 'Tên RM',
        prop: 'rmName',
        sortable: false,
        draggable: false,
      }, {
        name: 'Mã chi nhánh cấp 2',
        prop: 'branchCode',
        sortable: false,
        draggable: false,
      }, {
        name: 'Tên chi nhánh cấp 2',
        prop: 'branchName',
        sortable: false,
        draggable: false,
      }]
      this.columns.push(...infoRm);
    } else {
      let infoCBQL = [{
        name: 'Mã chi nhánh cấp 2',
        prop: 'branchCode',
        sortable: false,
        draggable: false,
      }, {
        name: 'Tên chi nhánh cấp 2',
        prop: 'branchName',
        sortable: false,
        draggable: false,
      }, {
        name: 'Tên chi nhánh cấp 1',
        prop: 'parentBranchName',
        sortable: false,
        draggable: false,
      }, {
        name: 'Vùng',
        prop: 'region',
        sortable: false,
        draggable: false,
      }]
      this.columns.push(...infoCBQL);

    }
    if (listHeader) {
      let header = [];
      listHeader.forEach(element => {
        let item = {
          name: element?.itemName?.concat("\n (").concat(element?.unitName).concat(")"),
          prop: element?.itemCode,
          sortable: false,
          draggable: false,
        }
        header.push(item);
      });
      this.columns.push(...header);
    }
  }

  createFromFile() {
    this.router.navigate([functionUri.campaign, 'target-sme-import-file'],
      {
        state: {
          campaignId: this.form.value.campaignId,
        }, skipLocationChange: true
      });
  }


  changeType() {
    let typeCampaign = this.form.get('typeCampaign').value;
    if (typeCampaign != this.typeCampaign) {
      this.typeCampaign = typeCampaign;
      this.search(0);
    }

  }

  onActive(event) {
    if (event.type === 'click') {
      this.itemSelected = event.row;
    }
  }

  setPage(pageInfo) {
    this.search(pageInfo?.offset);
  }

}
