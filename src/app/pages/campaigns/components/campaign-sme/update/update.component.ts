import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { forkJoin, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CampaignSize, CAMPAIGN_SCOPES, Division, FunctionCode, functionUri, maxInt32, Scopes, TARGET_PICK_BY, SessionKey, BRANCH_HO, CampaignStatus } from 'src/app/core/utils/common-constants';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { CampaignsService } from '../../../services/campaigns.service';
import { get, pick, orderBy, groupBy, find, cloneDeep, isEmpty } from 'lodash';
import { FormArray, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { MenuItem } from 'primeng/api';
import { LevelApi } from 'src/app/pages/kpis/apis';
import { DashboardService } from 'src/app/pages/dashboard/services/dashboard.service';
import { Utils } from 'src/app/core/utils/utils'
import { DeclareCampaignComponent } from '../declare-campaign/declare-campaign.component';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class CampaignSMEUpdateComponent extends BaseComponent implements OnInit {
	@ViewChild('declareCampaign') declareCampaign: DeclareCampaignComponent;

  form = this.fb.group({
    info: this.fb.group({
      parentCampaignId: [''],
      parentId: [{value:'', disabled: true}, CustomValidators.required],
      code: [{value:'', disabled: true}, CustomValidators.onlyNormalAndSpecialChar],
      campaignId: [''],
      name: ['', CustomValidators.required],
      startDate: [new Date(), CustomValidators.required],
      endDate: [''],
      scope: [CampaignSize.TOAN_HANG, CustomValidators.required],
      content: [''],
      slaManager: [null, Validators.compose([CustomValidators.required, CustomValidators.onlyInteger])], // phan giao
      slaRm: [null, Validators.compose([CustomValidators.required, CustomValidators.onlyInteger])], // tiep can
      titleGroupDTOS: [[]], // doi tuong tham gia
      campaignItems: [[]], // chi tieu
      files: [[]],
      imgList: [[]],
      fileList: [[]],
      blockCode: Division.SME,
      branchCodes: [[]],
      areaCodes: [[]],
      planValueForm: this.fb.array([]), // chi tieu - entrust list
      itemId: [''],
      id: ['']
    }),
    target: this.fb.group({
      pickBy: [TARGET_PICK_BY.MULTI],
      file: [''],
      campaignId: ''
    }),
    assignCustomer: this.fb.group({
      pickBy: [TARGET_PICK_BY.MULTI],
      file: [''],
      campaignId: ''
    })
  });
  isLoading = false;
  isFirtLoading = false;

  status = 'Đang soạn thảo';
  campaignCode = '';
  created_by = '';
  branch = '';

  items: MenuItem[];
  tabIndex = 0;

  rmLevelList = [];
  campaignItems: any = [];
  campaign: any;
  currentCampaign;
  fileListForm = [];
  imgListForm = [];
  fileInfo =  {};
  isAllocated = true;
  itemList = [];

  constructor(
    injector: Injector,
    private campaignService: CampaignsService,
    private levelApi: LevelApi,
    private dashboardService: DashboardService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CAMPAIGN}`);
    this.prop = get(this.router.getCurrentNavigation(), 'extras.state');  
    this.campaignCode = this.route.snapshot.queryParams.campaignCode;  
  }

  ngOnInit(): void {
    this.isFirtLoading = true;
    this.sessionService.setSessionData(SessionKey.COMMON_DATA_CAMPAIGN_DETAILS, 'SME')
    if (this.prop?.hasOwnProperty('tabIndex')) {
      this.tabIndex = this.prop.tabIndex;
    }
    this.infoForm.controls.scope.setValue(this.isHO ? CampaignSize.TOAN_HANG : CampaignSize.CHI_NHANH);

    const parentParams = {
      rsId: this.objFunction?.rsId,
      scope: Scopes.VIEW,
      page: 0,
      size: maxInt32,
      status: 'ACTIVATED'
    }
    const id = this.route.snapshot.paramMap.get('id');

    forkJoin([
      this.levelApi
        .searchLevelByBlockCode(Division.SME)
        .pipe(catchError(() => of(undefined))),
      this.campaignService.getSMECampaignDetail(id,this.objFunction?.rsId,'VIEW').pipe(catchError(() => of(undefined))),
      this.campaignService.getCampaignItems('SME', this.objFunction.rsId).pipe(catchError(() => of(undefined))),
      this.campaignService.getCampaignItems('SME', this.objFunction.rsId).pipe(catchError(() => of(undefined))),
     ]).pipe(
      finalize(() => {
      })
    ).subscribe(
      ([
        rmLevels,
        currentCampaign,
        campaignItems
      ]) => {
        this.isFirtLoading = false;
        this.isUpdateSME(currentCampaign);
        this.campaignItems = campaignItems;
        this.rmLevelList = rmLevels?.content || [];
        this.infoForm.controls.titleGroupDTOS.setValue(this.rmLevelList.map(item => item.id));

        this.currentCampaign = currentCampaign;
        this.currentCampaign.createdBy = (currentCampaign?.rmCode === undefined ? '' : currentCampaign?.rmCode + ' - ')  + currentCampaign?.fullName;
        this.currentCampaign.branchDisplay = (currentCampaign?.branchCode === undefined ? '' : currentCampaign?.branchCode + ' - ')  + currentCampaign?.branchName;
        this.initData({info: this.currentCampaign});
      }
    );

  }

  ngAfterViewInit() {

    this.infoForm.controls.slaManager.valueChanges.subscribe((value) => {
      if (!isEmpty(value)) {
        const reg = new RegExp(/^[0-9]*$/g);
        value = value.toString().replace(/,/g, '');
        if (!reg.test(value)) {
          value = value.replace(/\D/g, '');
        }
        this.infoForm.controls.slaManager.setValue(Utils.numberWithCommas(value).substr(0, 50), { emitEvent: false });
      }
    });

    this.infoForm.controls.slaRm.valueChanges.subscribe((value) => {
      if (!isEmpty(value)) {
        const reg = new RegExp(/^[0-9]*$/g);
        value = value.toString().replace(/,/g, '');
        if (!reg.test(value)) {
          value = value.replace(/\D/g, '');
        }
        this.infoForm.controls.slaRm.setValue(Utils.numberWithCommas(value).substr(0, 50), { emitEvent: false });
      }
    })
  }

  initData(data: any): void {
    const { info } = data;
    // at this time, just patch info form
    this.initInfo(info);
  }

  initInfo(info: any): void {

    //// info
    info.parentCampaignId = info.parentCampaignId;
    info.startDate = new Date(info.startDate);
    info.endDate = info?.endDate ? new Date(info?.endDate) : '';
    info.areaCodes = info.areaCode ? info.areaCode.split(',') : [];
    info.parentId = info.itemId;
    info.branchCodes = info.campaignBranchCodes ? info.campaignBranchCodes.map(item => item.branchCode) : [];
    
    if (!info.campaignRmLevels) info.campaignRmLevels = [];

    // set entrust value list
    if (!info.campaignItemAllocations) info.campaignItemAllocations = [];
    this.itemList = info?.campaignItemAllocations;
    info?.campaignItemAllocations.forEach(item => {
      this.planValueForm.push(
        this.fb.group({
          entrustValue: [item.entrustValue, CustomValidators.required],
          id: item.itemId,
          itemCode: item.itemCode,
          itemName: item.itemName,
          itemLevel: item.itemLevel,
          itemParent: info.itemId,
          unit: item.unit
        })
      )
    });

    info.campaignItems = this.planValueForm.value;

    const parentItem = this.campaignItems.find(item => item.campaignItemDto?.id === info.parentId);

    if (parentItem) {
      parentItem.campaignItemLevel = {[info.itemId]:this.planValueForm.value};

    }

    // attach file
    const fileInfo = {};
    let fileListForm = [];
    let imgListForm = [];
    if (!info.campaignAttachments) info.campaignAttachments = [];

    info.campaignAttachments.forEach(item => {
      fileInfo[item.fileId] = {
        name: item.fileName,
        path: item.filePath,
        type: item.type,
      };

      if (item.type === 'file') {
        fileListForm = [...fileListForm, item.fileId];
      } else {
        imgListForm = [...imgListForm, item.fileId];
      }
    });
    this.fileListForm = fileListForm;
    this.imgListForm = imgListForm;
    this.fileInfo = fileInfo;

    info.files = fileInfo;

    // patch
    this.infoForm.patchValue(info);
    this.infoForm.controls.titleGroupDTOS.setValue(info.campaignRmLevels.map(item => item.rmLevelId));
    this.targetForm.controls.campaignId.setValue(info.id);
    this.assignCustomerForm.controls.campaignId.setValue(info.id);
  }

  save() {
    if (this.form.invalid) {
      validateAllFormFields(this.form);
      this.infoForm.controls.planValueForm.markAllAsTouched();

      return;
    }

    // validate info
    const { info } = cleanDataForm(this.form);

    if (info.scope === CAMPAIGN_SCOPES.CHI_NHANH) {
      if (!info.branchCodes?.length) {
        this.messageService.warn(`Chưa chọn chi nhánh!`);
        return;
      }

      if (!info.areaCodes?.length) {
        this.messageService.warn(`Chưa chọn vùng!`);
        return;
      }
    }

    if (!info.titleGroupDTOS?.length) {
      this.messageService.warn(`Chưa đối tượng tham gia!`);
      return;
    }

    if (moment(info.startDate).isAfter(moment(info.endDate))) {
      this.messageService.warn(this.notificationMessage.startDateMoreThanEndDate);
      return;
    }

    this.isLoading = true;

    info.titleGroupDTOS = this.getRmLevelList(info.titleGroupDTOS);
    info.files = this.getFileList(info.files);
    info.slaManager = info.slaManager.toString().replace(/,/g, '');
    info.slaRm = info.slaRm.toString().replace(/,/g, '');

    info.campaignId = info.code;

    const data = pick(info, [
      'campaignId',
      'code',
      'parentCampaignId',
      'name',
      'startDate',
      'endDate',
      'campaignItems',
      'titleGroupDTOS',
      'files',
      'slaManager',
      'slaRm',
      'scope',
      'content',
      'blockCode',
      'areaCodes',
      'branchCodes',
      'itemId',
      'id'
    ]);

    this.campaignService.updateSME(data).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(res => {
      this.messageService.success(this.notificationMessage.success);
      this.router.navigate([functionUri.campaign, 'detail-sme', data.id], {});
    },
      (e) => {

        if (e?.status === 0) {
          this.messageService.error(this.notificationMessage.E001);
          return;
        }
        this.messageService.error(JSON.parse(e?.error)?.description);
      }
    );
  }

  cancel(): void {
    this.backToList();
  }

  getRmLevelList(idList: string[]): any[] {
    return this.rmLevelList.filter(item => idList.includes(item.id)).map(item => ({
      id: item.id,
      code: item.code,
      name: item.name,
      blockCode: item.blockCode
    }))
  }

  getFileList(fileObj): any[] {
    return Object.keys(fileObj).map(id => ({
      fileId: id,
      fileName: fileObj[id].name,
      filePath: fileObj[id].path,
      type: fileObj[id].type,
    }));
  }


  onTabChanged($event) {
    this.tabIndex = get($event, 'index');
  }

  getDetail(id: string): void {
    this.isLoading = true;
    this.campaignService.getSMECampaignDetail(id,this.objFunction?.rsId,'VIEW').pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(campaign => {

      this.campaign = campaign;
    })
  }

  backToList(): void {
    this.router.navigateByUrl(functionUri.campaign);
  }

  isUpdateSME(item) {
    const isHO = this.isHO || this.currUser?.branch === BRANCH_HO;
    const isNotCreator = item.userName !== this.currUser?.username;
    const allowRoles = this.objFunction?.updateSME &&
    item.blockCode === Division.SME &&
    [CampaignStatus.InProgress, CampaignStatus.Activated].includes(item.status);
    
    if (
      isNotCreator &&
      !isHO &&
      !allowRoles
    ) {
      this.messageService.warn(this.notificationMessage.no_permission_for_update_campaign)
      this.backToList();
      return;
    }
  }

  changeLoading(isLoading = false): void {
    this.isLoading = isLoading;
    this.ref.detectChanges();
  }

  get infoForm(): FormGroup {
    return this.form.get('info') as FormGroup;
  }

  get isHO(): boolean {
    return this.objFunction.HO;
  }

  get planValueForm(): FormArray {
    return this.infoForm.controls.planValueForm as FormArray;
  }

  get targetForm(): FormGroup {
    return this.form.get('target') as FormGroup;
  }
  get assignCustomerForm(): FormGroup {
    return this.form.get('assignCustomer') as FormGroup;
  }
}
