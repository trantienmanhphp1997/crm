import { Component, EventEmitter, Injector, TemplateRef, OnInit, ViewChild, Input, Output } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { TableColumn } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { forkJoin, of } from 'rxjs';
import { CampaignsService } from '../../../services/campaigns.service';
import { FunctionCode, functionUri, Scopes } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import * as _ from 'lodash';
import { Utils } from 'src/app/core/utils/utils';
import { CategoryService } from 'src/app/pages/system/services/category.service';

@Component({
  selector: 'app-detail-campaign-allocation',
  templateUrl: './detail-campaign-allocation.component.html',
  styleUrls: ['./detail-campaign-allocation.component.scss']
})
export class DetailCampaignAllocationComponent extends BaseComponent implements OnInit {
  @ViewChild('orderTmpl', { static: true }) orderTmpl: TemplateRef<any>;
  @ViewChild('cellTmpl', { static: true }) cellTmpl: TemplateRef<any>;
  @ViewChild('buttonsTemplate', { static: true }) buttonsTemplate: TemplateRef<any>;

  @Input() campaignId;
  @Output() changeLoading = new EventEmitter();
  @Output() changeInfo = new EventEmitter();

  columns: TableColumn[];
  listData = [];
  page: number = 0;
  size: number = _.get(global, 'userConfig.pageSize');
  typeCampaign = '2';
  body: any;
  type: any;
  itemSelected: any;
  pageable: Pageable;
  form = this.fb.group({
    typeCampaign: [this.typeCampaign],
    branchCode: [null],
    branchName: [null],
    region: [null],
    allocationName: [null],
    page: 0,
    size: global?.userConfig?.pageSize,
  });
  isRM = true;

  constructor(
    injector: Injector,
    private campaignService: CampaignsService,
    private categoryService: CategoryService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CAMPAIGN}`);
  }

  ngOnInit(): void {

    this.search(0);
  }
  search(page) {
    this.changeLoading.emit(true);
    if (!this.campaignId) {
      this.campaignId = _.get(this.route.snapshot.params, 'id', this.campaignId);
    }

    if (this.typeCampaign === '3')
      this.type = 3;
    else
      this.type = 2;
    this.body = {
      "campaignId": this.campaignId,
      "rsId": this.objFunction.rsId,
      "scope": "VIEW",
      "type": this.type,
    }
    forkJoin([
      this.campaignService.getHeaderAllocation(this.campaignId).pipe(catchError((e) => of(undefined))),
      this.campaignService.getListAllocation(this.body, page, this.size)
        .pipe(catchError((e) => of(undefined))),
      this.campaignService.getSumAllocation(this.body)
        .pipe(catchError((e) => of(undefined))),
      this.categoryService
        .getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined))),
      this.campaignService.getSMECampaignDetail(this.campaignId, this.objFunction?.rsId, Scopes.VIEW).pipe(catchError((e) => of(undefined))),
    ]).subscribe(([listHeader, listData, sumData, branchesOfUser, campaign]) => {
      this.changeInfo.emit(campaign);
      this.getColumns(listHeader);
      let listAllocation = listData?.content;
      let listView = []
      if (listAllocation) {
        this.pageable = {
          totalElements: listData?.totalElements,
          totalPages: listData?.totalPages,
          currentPage: page,
          size: this.size,
        };

        if (sumData && listHeader) {
          if (listData?.content?.length > 0) {
            let item = {
              "rmCode": "SUM",
              "rmName": "",
              "branchCode": this.typeCampaign === '3' ? "" : "SUM",
              "branchName": "",
              "parentBranchName": "",
              "region": "",
            }
            let count = 0;
            if (sumData[0]) {
              sumData[0].forEach(element => {
                let formated = element;
                if (Number(element)) {
                  formated = Number(element).toLocaleString('en-US', {
                    style: 'currency',
                    currency: 'VND',
                  });
                  formated = formated.substr(1, formated.length);
                }
                item[listHeader[count]?.itemCode] = formated;
                count++;
              });
            }
            listView.push(item);
          }

        }
        listAllocation.forEach(element => {
          let item = {
            "rmCode": element?.rmCode,
            "rmName": element?.rmName,
            "branchCode": element?.branchCode,
            "branchName": element?.branchName,
            "parentBranchName": element?.parentBranchName,
            "region": element?.region,
          }
          let listValue = element?.value;
          if (listValue && listHeader) {
            let count = 0;
            listValue.forEach(element => {
              let formated = element;
              if (Number(element)) {
                formated = Number(element).toLocaleString('en-US', {
                  style: 'currency',
                  currency: 'VND',
                });
                formated = formated.substr(1, formated.length);
              }
              item[listHeader[count]?.itemCode] = formated;
              count++;
            });
          }
          listView.push(item);
        });
      }

      this.isRM = _.isEmpty(branchesOfUser);

      this.listData = [...listView];
      this.changeLoading.emit(false);
    },
      (e) => {
        this.messageService.error(e.error.description);
        this.changeLoading.emit(false);
      });
  }
  getColumns(listHeader) {
    this.columns = [
      {
        name: 'STT',
        cellTemplate: this.orderTmpl,
        sortable: false,
        width: 10,
        draggable: false,
        headerClass: 'text-center',
      }];
    if (this.typeCampaign === '3') {
      let infoRm = [{
        name: 'Mã RM',
        prop: 'rmCode',
        sortable: false,
        draggable: false,
      },
      {
        name: 'Tên RM',
        prop: 'rmName',
        sortable: false,
        draggable: false,
      }, {
        name: 'Mã chi nhánh cấp 2',
        prop: 'branchCode',
        sortable: false,
        draggable: false,
      }, {
        name: 'Tên chi nhánh cấp 2',
        prop: 'branchName',
        sortable: false,
        draggable: false,
      }]
      this.columns.push(...infoRm);
    } else {
      let infoCBQL = [{
        name: 'Mã chi nhánh cấp 2',
        prop: 'branchCode',
        sortable: false,
        draggable: false,
      }, {
        name: 'Tên chi nhánh cấp 2',
        prop: 'branchName',
        sortable: false,
        draggable: false,
      }, {
        name: 'Tên chi nhánh cấp 1',
        prop: 'parentBranchName',
        sortable: false,
        draggable: false,
      }, {
        name: 'Vùng',
        prop: 'region',
        sortable: false,
        draggable: false,
      }]
      this.columns.push(...infoCBQL);

    }
    if (listHeader) {
      let header = [];
      listHeader.forEach(element => {
        let item = {
          name: '<p class="m-0">'.concat(element?.itemName).concat("<br>\n (").concat(element?.unitName).concat(")").concat('</p>'),
          prop: element?.itemCode,
          sortable: false,
          draggable: false,
        }
        header.push(item);
      });
      this.columns.push(...header);
    }
  }


  //   {
  //     name: this.fields.customerType,
  //       prop: 'customerType',
  //         sortable: false,
  //           draggable: false,
  //     },

  //   {
  //     name: this.fields.action,
  //       prop: 'action',
  //         cellTemplate: this.buttonsTemplate,
  //           sortable: false,
  //             draggable: false,
  //               frozenRight: true,
  //                 width: 130,
  //                   resizeable: false,
  //     },
  // }
  exportFile() {

  }
  onActive(event) {
    if (event.type === 'click') {
      this.itemSelected = event.row;
    }
  }
  setPage(pageInfo) {
    this.search(pageInfo?.offset);
  }
  changeType() {
    let typeCampaign = this.form.get('typeCampaign').value;
    if (typeCampaign != this.typeCampaign) {
      this.typeCampaign = typeCampaign;
      this.search(0);
    }
  }
  createFromFile() {
    this.router.navigate([functionUri.campaign, 'target-sme-import-file'],
      {
        state: {
          campaignId: this.campaignId,
        }
      });
  }

}