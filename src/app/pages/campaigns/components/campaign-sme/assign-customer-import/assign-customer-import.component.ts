import { global } from '@angular/compiler/src/util';
import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import * as _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { FileService } from 'src/app/core/services/file.service';
import { FunctionCode, Scopes, typeExcel } from 'src/app/core/utils/common-constants';
import { CampaignsService } from '../../../services/campaigns.service';
import { Utils } from '../../../../../core/utils/utils';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-assign-customer-import',
  templateUrl: './assign-customer-import.component.html',
  styleUrls: ['./assign-customer-import.component.scss']
})
export class AssignCustomerSMEImportComponent extends BaseComponent implements OnInit {
  isLoading = false;
  fileName: string;
  isFile = false;
  fileImport: File;
  files: any;
  ColumnMode = ColumnMode;
  listDataSuccess = [];
  listDataError = [];
  prop: any;
  limit = global.userConfig.pageSize;
  pageSuccess: Pageable;
  pageError: Pageable;
  paramSuccess = {
    size: this.limit,
    page: 0,
    search: '',
    status: true,
    requestId: ''
  };
  textSearchSuccess = '';
  paramError = {
    size: this.limit,
    page: 0,
    search: '',
    status: false,
    requestId: ''
  };
  textSearchError = '';
  fileId: string;
  isUpload = false;
  isBusiness = true;
  searchSuccessDone = true;
  searchErrorDone = true;
  messages = global.messageTable;
  @ViewChild('tableSuccess') tableSuccess: DatatableComponent;
  @ViewChild('tableError') tableError: DatatableComponent;

  constructor(injector: Injector, private service: CampaignsService, private fileService: FileService) {
    super(injector);
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');

    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.CAMPAIGN}`
    );
  }

  ngOnInit(): void {
  }

  handleFileInput(files) {
    console.log('handleFileInput', files);

    if (files && files.length > 0) {
      if (!typeExcel.includes(files?.item(0)?.type)) {
        this.messageService.error(this.notificationMessage.CANNOT_READ_DATA_FROM_FILE);
        return;
      }
      this.isFile = true;
      this.fileImport = files.item(0);
      this.fileName = files.item(0).name;
    } else {
      this.isFile = false;
    }
  }

  backStep() {
    // this.router.navigateByUrl(functionUri.lead_management, { state: this.prop ? this.prop : this.state });
    this.prop.tabIndex = 2;
    this.back();
  }

  clearFile() {
    this.isUpload = false;
    this.isFile = false;
    this.fileName = null;
    this.fileImport = null;
    this.files = null;
    this.listDataSuccess = [];
    this.listDataError = [];
    this.fileId = undefined;
    this.pageError = undefined;
    this.pageSuccess = undefined;
    this.textSearchSuccess = '';
    this.textSearchError = '';
    this.paramError.search = '';
    this.paramSuccess.search = '';
  }

  importFile() {
    if (this.isFile && !this.isUpload) {
      this.isLoading = true;
      const formData: FormData = new FormData();
      formData.append('file', this.fileImport);
      formData.append('campaignCode', this.prop.campaignCode);
      formData.append('campaignId', this.prop.campaignId);
      formData.append('rsId', this.objFunction?.rsId);
      formData.append('scope', Scopes.VIEW);
      this.service.importAssignCustomerSME(formData).subscribe(
        (fileId) => {
          this.fileId = fileId;
          this.paramError.requestId = this.fileId;
          this.paramSuccess.requestId = this.fileId;
          this.checkImportSuccess(this.fileId);
        },
        (e) => {
          console.log('Lỗi', e);
          const error = JSON.parse(e.error);
          if (error?.description) {
            this.messageService.error(error?.description);
          } else {
            this.messageService.error(this.notificationMessage.error);
          }
          this.listDataError = [];
          this.listDataSuccess = [];
          this.isLoading = false;
        }
      );
    }
  }

  checkImportSuccess(fileId: string) {
    const interval = setInterval(() => {
      this.service.checkFileImportCustomer(fileId).subscribe((res) => {

        if (res?.status === 'PENDING') return;

        if (res?.status === 'COMPLETE') {
          this.service.downloadErrorCustomerAssignmentSME(fileId, this.prop.campaignId).subscribe((res2) => {

            if (Utils.isEmpty(res2)) {
              this.messageService.success(this.notificationMessage.import_campaign_allocation_success);
              this.isLoading = false;
              setTimeout(() => {
                this.backStep();
              }, 3000);
            } else {
              this.messageService.error(this.notificationMessage.import_campaign_allocation_error);
              this.downloadFile(res2);
            }
          });
        } else if (res?.status === 'FAIL') {
          this.messageService.error(res?.msgError);
          this.isLoading = false;
        }
        clearInterval(interval);
      });
    }, 5000);
  }

  downloadFile(res2): void {
    this.fileService.downloadFile(res2, 'import-error-customer-assign.xlsx').subscribe(res3 => {
      if (!res3) {
        this.messageService.error(this.notificationMessage.error);
      }else{
        setTimeout(() => {
          this.backStep();
        }, 3000);
      }
      this.isLoading = false;
    }, () => {
      this.isLoading = false;
    });
  }

  downloadTemplate() {
    this.isLoading = true;
    this.service.downloadTemplateCustomerAssignmentSME().subscribe(
      (fileId) => {
        this.fileService.downloadFile(fileId, 'template-import-phan-bo-khach-hang.xlsx').subscribe(
          (res) => {
            this.isLoading = false;
            if (!res) {
              this.messageService.error(this.notificationMessage.error);
            }
          },
          () => {
            this.isLoading = false;
          }
        );
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }
}
