import { Component, EventEmitter, Injector, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { ControlContainer, FormArray, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { forkJoin, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import { FileService } from 'src/app/core/services/file.service';
import { CampaignSize, Division, FunctionCode, maxInt32, Scopes } from 'src/app/core/utils/common-constants';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { DashboardService } from 'src/app/pages/dashboard/services/dashboard.service';
import { LevelApi } from 'src/app/pages/kpis/apis';
import {  RmFluctuateApi } from 'src/app/pages/rm/apis';
import { CampaignsService } from '../../../services/campaigns.service';
import { orderBy, cloneDeep, find, groupBy } from 'lodash';
import { Utils } from 'src/app/core/utils/utils';
import { TrueResolve } from 'src/app/pages/system/resolvers';

@Component({
  selector: 'app-declare-campaign',
  templateUrl: './declare-campaign.component.html',
  styleUrls: ['./declare-campaign.component.scss']
})
export class DeclareCampaignComponent extends BaseComponent implements OnInit {

  @Input() rmLevelList: any[] = [];
  @Input() regionList: any[] = [];
  @Input() itemList: any[] = [];
  @Input() fileListForm: any[] = [];
  @Input() imgListForm: any[] = [];
  @Input() fileInfo = {};
  @Input() isUpdate = false;

  @Output() changeLoading = new EventEmitter();

  form: FormGroup;
  minEndDate = new Date();
  isLoading = false;
  isRegion = false;
  listRegion = [];
  branchesCode: string[];
  CampaignSize = CampaignSize;

  targetParentList = [];
  target = {};
  virtualList = [{}]; // virtual scroll primeng is super lag, use list with 1 elm instead
  curType = 'file';

  fileType = [
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/pdf',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation'
  ];

  fileTypeFlat = this.fileType.join(';');
  imgTypeFlat = 'image/*';
  acceptType = null;
  parent = null;
  oneMB = 1048576;
  filterValue = '';
  regions = {};
  allBranchLv1 = [];
  allBranchLv2 = [];
  branchLv1List = [];
  campaignItems: any[] = [];
  parentList: any[] = [];
  listBranch: any[] = [];
  isAllocated = true;
  isChangeParentCampaign = false;
  itemAllocation: any[];

  constructor(
    injector: Injector,
    public controlContainer: ControlContainer,
    private rmFluctuateApi: RmFluctuateApi,
    private fileService: FileService,
    private campaignService: CampaignsService,
    private levelApi: LevelApi,
    private dashboardService: DashboardService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CAMPAIGN}`);

  }

  ngOnChanges(changes: SimpleChanges): void {

  }

  ngOnInit(): void {
    this.changeLoading.emit(true);

    this.form = this.controlContainer.control as FormGroup;
    this.setValueChanges();

    const parentParams = {
      rsId: this.objFunction?.rsId,
      scope: Scopes.VIEW,
      page: 0,
      size: maxInt32,
      status: 'ACTIVATED'
    }

    forkJoin([
      this.campaignService.getCampaignItems('SME', this.objFunction.rsId).pipe(catchError(() => of(undefined))),
      this.campaignService.getParentList(parentParams).pipe(catchError(() => of(undefined))),
      this.dashboardService.getBranchByDomain({
        rsId: this.objFunction?.rsId,
        scope: Scopes.VIEW
      }),
      this.getAllocation(2).pipe(catchError(() => of(undefined))),
      this.getAllocation(3).pipe(catchError(() => of(undefined))),
    ]).pipe(
      finalize(() => {
        this.changeLoading.emit(false);
      })
    ).subscribe(
      ([
        campaignItems,
        parentList,
        branchesByDomain,
        branchAllocation,
        rmAllocation
      ]) => {
        const branchAllocate = branchAllocation?.content || [];
        const rmAllocate = rmAllocation?.content || [];
        if(Utils.isArrayEmpty(branchAllocate) && Utils.isArrayEmpty(rmAllocate)){
          this.isAllocated = false;
        }
        this.campaignItems = campaignItems;
        this.getTargetList(this.campaignItems);

        this.parentList = parentList?.content.map(item => ({ ...item, displayName: `${item?.code} - ${item?.name}` })) || [];
        this.listBranch = branchesByDomain;
        this.initRegionList(branchesByDomain);
        this.getTargetList(campaignItems);
      }
    );
  }

  getAllocation(typeNum: number){
    const id = this.form.controls?.id?.value;
    return this.campaignService.getListAllocation({ "campaignId": id, "rsId": this.objFunction?.rsId, "scope": "VIEW", "type": typeNum }, 0, 1);
  }

  ngAfterViewInit(): void {
    console.log('DeclareCampaignComponent');
    this.itemAllocation = this.itemList;
  }


  changeFile(fileEvent): void {
    const file = fileEvent.target.files[0];

    if (!file) return;

    // img, file - max size: 2mb
    const size = file.size / this.oneMB;
    const maxSize = 2;

    if (this.curType === 'img') {
      if (!this.isValidImgType(file.type)) {
        this.messageService.warn('Ảnh sai định dạng!');
        return;
      }

      if (this.imgListForm.length === 2) {
        this.messageService.warn(`Ảnh vượt quá số lượng được up`);
        return;
      }
    }

    if (this.curType === 'file') {
      if (!this.isValidFileType(file.type)) {
        this.messageService.warn('File sai định dạng!');
        return;
      }

      if (this.fileListForm.length === 5) {
        this.messageService.warn(`Tài liệu vượt quá số lượng được up`);
        return;
      }
    }

    if (size > maxSize) {
      this.messageService.warn(`Dung lượng vượt quá ${maxSize}MB`);
      return;
    }

    this.upload(file);
  }

  upload(file): void {
    this.changeLoading.emit(true);
    const formData = new FormData();
    formData.append('file', file);
    this.rmFluctuateApi.uploadFile(formData).subscribe(id => {

      this.saveInfo(id, file.size);
    })
  }

  saveInfo(id: string, size): void {
    this.changeLoading.emit(true);
    this.rmFluctuateApi.getFileInfo(id).pipe(
      finalize(() => {
        this.changeLoading.emit(false);
      })
    ).subscribe(res => {
      let formatedSize;
      const s = Number(size / this.oneMB);
      if(s.toString().includes('.')){
        formatedSize = Number(s).toFixed(2);
      } else {
        formatedSize = s;
      }
      // const size = 
      this.fileInfo[res.id] = {
        name: res.fileName,
        size: `${formatedSize} MB`,
        path: res.filePath,
        type: this.curType,
        sizeNum: formatedSize
      };

      if (this.curType === 'file') {
        this.fileListForm = [...this.fileListForm, id];
      } else {
        this.imgListForm = [...this.imgListForm, id];
      }

      this.form.controls.files.setValue(this.fileInfo);

    });
  }

  download(fileId: string) {
    const title = this.fileInfo[fileId].name;
    this.fileService.downloadFile(fileId, title).subscribe((res) => {
      this.changeLoading.emit(false);
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }

  removeFile(id: string): void {
    const info = this.fileInfo[id];
    if (!info) return;

    if (info.type === 'file') {
      this.fileListForm = this.fileListForm.filter(itemId => itemId !== id);
    } else {
      this.imgListForm = this.imgListForm.filter(itemId => itemId !== id);
    }
    delete this.fileInfo[id];

  }

  getTargetList(raw: any[] = []): void {
    this.targetParentList = [];
    this.target = {};
    
    raw.forEach(item => {
      this.targetParentList.push(item.campaignItemDto);
      this.target = { ...this.target, ...item.campaignItemLevel };
    });
    if (!this.targetParentList.length) return;

  }

  setValueChanges(): void {
    this.form.controls.parentId.valueChanges.subscribe(id => {
      if (!this.target[id] || this.isChangeParentCampaign) return;

      this.itemAllocation = this.target[id];
      this.planValueForm.clear();
      this.target[id].map(item => {

        this.planValueForm.push(
          this.fb.group({
            entrustValue: [null, CustomValidators.required],
            id: item.id,
            itemCode: item.itemCode,
            itemName: item.itemName,
            itemLevel: item.itemLevel,
            itemParent: item.itemParent,
            unit: item.unit
          })
        )
      });
    });
    this.planValueForm.valueChanges.subscribe(() => {
      this.form.controls.campaignItems.setValue(this.planValueForm.value);
    });

    this.form.controls.parentCampaignId.valueChanges.subscribe(id => {
      if (this.isUpdate) return;

      if (id) {
        this.isChangeParentCampaign = true;
        const parent = this.parentList.find(parent => parent.id === id);

        if (parent?.startDate) {
          this.form.controls.startDate.setValue(moment(parent.startDate).toDate());
        }

        if (parent?.endDate) {
          this.form.controls.endDate.setValue(moment(parent.endDate).toDate());
        }

        this.form.controls.name.setValue(parent.name);

        this.setMinEndDate();

        this.getDetail(id);
        return;
      }

      this.form.patchValue({
        name: '',
        startDate: new Date(),
        endDate: '',
        parentId: ''
      })
    });

    this.form.controls.startDate.valueChanges.subscribe(() => {
      this.setMinEndDate();
    })

    this.form.controls.areaCodes.valueChanges.subscribe(() => {
      this.setBranchLv2List();
    });

    this.form.controls.scope.valueChanges.subscribe(value => {
      this.ref.detectChanges();
      if (value === CampaignSize.TOAN_HANG) {
        this.form.patchValue({
          branchCodes: [],
          areaCodes: []
        })
      }
    })
  }

  isInvalidPattern(controlName, rowIndex): boolean {
    
    return this.planValueForm.controls[rowIndex].get(controlName).errors?.cusRequired;
  }

  changeArea(): void {
    this.setBranchLv2List();
  }

  setMinEndDate(): void {
    const startDate = this.form.controls.startDate.value;

    this.minEndDate = moment(startDate).isBefore(moment()) ? new Date() : moment(startDate).toDate();
  }

  isValidImgType(type: string): boolean {
    return type.includes('image/');
  }

  isValidFileType(type: string): boolean {
    return this.fileType.includes(type);
  }

  openDownloadFilePopup(inputFile): void {
    this.curType = 'file';
    inputFile.value = '';
    inputFile.click();
  }

  openDownloadImgPopup(inputImg): void {
    this.curType = 'img';
    inputImg.value = '';
    inputImg.click();
  }

  getDetail(id: string): void {
    this.changeLoading.emit(true);
    this.campaignService.getSMECampaignDetail(id,this.objFunction?.rsId,'VIEW').pipe(
      finalize(() => {
        this.changeLoading.emit(false);
        this.isChangeParentCampaign = false;
      })
    ).subscribe(campaign => {
      this.parent = campaign;
      const targetParent = this.targetParentList.find(item => item.itemCode === campaign.itemCode);

      this.form.controls.parentId.setValue(targetParent?.id);   

      const itemCodes : any[] = campaign?.campaignItemAllocations?.map(item => {return item.itemCode});
      this.itemAllocation =  this.target[targetParent?.id]?.filter(item => itemCodes.includes(item.itemCode));
      this.planValueForm.clear();
      this.target[targetParent?.id]?.filter(item => itemCodes.includes(item.itemCode)).map(item => {
        this.planValueForm.push(
          this.fb.group({
            entrustValue: [null, CustomValidators.required],
            id: item.id,
            itemCode: item.itemCode,
            itemName: item.itemName,
            itemLevel: item.itemLevel,
            itemParent: item.itemParent,
            unit: item.unit
          })
        )
      });  
      this.planValueForm.patchValue(campaign?.campaignItemAllocations);
    })
  }

  initRegionList(branchesByDomain: any): void {
    const regionObj = groupBy(branchesByDomain, 'region');
    const regionList = Object.keys(regionObj).map(key => ({ locationCode: key, locationName: key }));

    Object.keys(regionObj).forEach(key => {
      const branchLv1 = groupBy(regionObj[key], 'parentCode');
      const branchLv1List = Object.keys(branchLv1).map(branchKey => {
        return {
          code: branchLv1[branchKey][0].parentCode,
          name: branchLv1[branchKey][0].parentName,
          displayName: `${branchLv1[branchKey][0].parentCode} - ${branchLv1[branchKey][0].parentName}`,
          khuVucM: key,
          branch_lv2: branchLv1[branchKey].map(branch => {
            return { code: branch.code, name: branch.name, displayName: `${branch.code} - ${branch.name}`, khuVucM: key, parentCode: branchLv1[branchKey][0].parentCode }
          })
        }
      })
      if (!this.regions[key]) {
        this.regions[key] = {
          branch_lv1: branchLv1List
        }
      }
    });

    Object.keys(this.regions).forEach(key => {
      this.regions[key].branch_lv1.forEach(item => {
        this.allBranchLv2.push(...item.branch_lv2);
      });
    });

    this.listBranch = cloneDeep(this.allBranchLv2);
    if (!find(this.listBranch, (item) => item.code === this.currUser?.branch)) {
      this.listBranch.push({
        code: this.currUser?.branch,
        displayName: `${this.currUser?.branch} - ${this.currUser?.branchName}`,
      });
    }

    if (this.isHO) {
      this.regionList = regionList;
    } else {
      regionList?.forEach((item) => {
        if (
          this.listBranch?.findIndex((i) => i.khuVucM === item?.locationCode) !== -1 &&
          this.regionList.findIndex((r) => r.locationCode === item?.locationCode) === -1
        ) {
          this.regionList.push(item);
        }
      });
    }

    // sort region list
    this.regionList = orderBy(this.regionList, [region => region.locationName], ['asc']);

    this.setBranchLv2List();

  }

  setBranchLv2List(): void {
    let branchLv2List = [];
    const { areaCodes } = this.form.getRawValue();

    if (areaCodes.length) {
      const list = areaCodes.map(code => this.regions[code].branch_lv1);
      list.forEach(branchLv1List => {
        branchLv1List.filter(branch => branch.code).forEach(item => {
          branchLv2List.push(...item.branch_lv2);
        });
      });
    } else {
      branchLv2List = this.allBranchLv2;
    }

    this.listBranch = cloneDeep(branchLv2List);

    this.listBranch = orderBy(this.listBranch, [branch => branch.displayName], ['asc']);

  }

  get filesForm(): FormGroup {
    return this.form.get('files') as FormGroup;
  }

  get fileIdList(): string[] {
    return Object.keys(this.fileInfo);
  }

  get planValueForm(): FormArray {
    return this.form.controls.planValueForm as FormArray;
  }

  get isHO(): boolean {
    return this.objFunction.HO;
  }
}
