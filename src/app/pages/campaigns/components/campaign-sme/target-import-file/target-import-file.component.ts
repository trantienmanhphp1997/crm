
import { global } from '@angular/compiler/src/util';
import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { FileService } from 'src/app/core/services/file.service';
import { FunctionCode, functionUri, Scopes, typeExcel } from 'src/app/core/utils/common-constants';
import { Utils } from 'src/app/core/utils/utils';
import { CampaignsService } from '../../../services/campaigns.service';

@Component({
  selector: 'app-target-import-file',
  templateUrl: './target-import-file.component.html',
  styleUrls: ['./target-import-file.component.scss']
})
export class TargetImportFileComponent extends BaseComponent implements OnInit {
  isLoading = false;
  fileName: string;
  isFile = false;
  fileImport: File;
  files: any;
  ColumnMode = ColumnMode;
  listDataSuccess = [];
  listDataError = [];
  prop: any;
  limit = global.userConfig.pageSize;
  pageSuccess: Pageable;
  pageError: Pageable;
  paramSuccess = {
    size: this.limit,
    page: 0,
    search: '',
    status: true,
    requestId: '',
  };
  textSearchSuccess = '';
  paramError = {
    size: this.limit,
    page: 0,
    search: '',
    status: false,
    requestId: '',
  };
  textSearchError = '';
  fileId: string;
  isUpload = false;
  isBusiness = true;
  searchSuccessDone = true;
  searchErrorDone = true;
  messages = global.messageTable;
  @ViewChild('tableSuccess') tableSuccess: DatatableComponent;
  @ViewChild('tableError') tableError: DatatableComponent;

  constructor(injector: Injector, private service: CampaignsService, private fileService: FileService) {
    super(injector);
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
    console.log(this.prop);
    
    this.objFunction = this.sessionService.getSessionData(
      `FUNCTION_${FunctionCode.CAMPAIGN}`
    );
  }

  ngOnInit(): void { }

  handleFileInput(files) {
    console.log('handleFileInput', files);

    if (files && files.length > 0) {
      if (!typeExcel.includes(files?.item(0)?.type)) {
        this.messageService.error(this.notificationMessage.CANNOT_READ_DATA_FROM_FILE);
        return;
      }
      this.isFile = true;
      this.fileImport = files.item(0);
      this.fileName = files.item(0).name;
    } else {
      this.isFile = false;
    }
  }

  clearFile() {
    this.isUpload = false;
    this.isFile = false;
    this.fileName = null;
    this.fileImport = null;
    this.files = null;
    this.listDataSuccess = [];
    this.listDataError = [];
    this.fileId = undefined;
    this.pageError = undefined;
    this.pageSuccess = undefined;
    this.textSearchSuccess = '';
    this.textSearchError = '';
    this.paramError.search = '';
    this.paramSuccess.search = '';
  }

  importFile() {
    if (this.isFile && !this.isUpload) {
      this.isLoading = true;
      const formData: FormData = new FormData();
      formData.append('file', this.fileImport);
      formData.append('rsId', this.objFunction.rsId);
      formData.append('scope', Scopes.CREATE);
      formData.append('campaignId', this.prop.campaignId);

      this.service.importTargetSME(formData).subscribe(
        (res) => {
          this.fileId = res?.id;
          this.paramError.requestId = this.fileId;
          this.paramSuccess.requestId = this.fileId;
          this.checkImportSuccess(this.fileId);
        },
        (e) => {
          const error = JSON.parse(e.error);
          if (error?.description) {
            this.messageService.error(error?.description);
          } else {
            this.messageService.error(this.notificationMessage.error);
          }
          this.listDataError = [];
          this.listDataSuccess = [];
          this.isLoading = false;
        }
      );
    }
  }

  save() {
  }

  backStep() {
    this.prop.tabIndex = 1;
    this.back();
  }

  checkImportSuccess(fileId: string) {
    const interval = setInterval(() => {
      this.service.checkFileImport(fileId).subscribe((res) => {
        if (res?.status === 'PENDING') return;

        if (res?.status === 'COMPLETE') {
          this.service.dowloadAllocationError(fileId, this.prop.campaignId).subscribe((res2) => {

            if (Utils.isEmpty(res2)) {
              this.messageService.success(this.notificationMessage.import_campaign_allocation_success);
              this.isLoading = false;
              setTimeout(() => {
                this.backStep();
              }, 3000);
            } else {
              this.messageService.error(this.notificationMessage.import_campaign_allocation_error);
              this.downloadFile(res2);
            }
          });
        } else if (res?.status === 'FAIL') {
          let message = '';
          switch (res?.msgError) {
            case 'FILE_NO_CONTENT_EXCEPTION':
              message = this.notificationMessage.FILE_NO_CONTENT_EXCEPTION;
              break;
            case 'TEMPLATE_INCORRECT':
            default:
              message = this.notificationMessage.file_not_valid;
          }
          this.messageService.error(message);
          this.isLoading = false;
        }
        clearInterval(interval);
      });
    }, 5000);
  }

  downloadFile(res2): void {
    this.fileService.downloadFile(res2, 'import-campaign-sme-allocation-errors.xlsx').subscribe(res3 => {
      if (!res3) {
        this.messageService.error(this.notificationMessage.error);
      } else {
        setTimeout(() => {
          this.backStep();
        }, 3000);
      }
      this.isLoading = false;
    }, () => {
      this.isLoading = false;
    });
  }

  downloadTemplate() {
    this.isLoading = true;
    const params = {rsId: this.objFunction?.rsId, scope: Scopes.VIEW};
    this.service.dowloadTemplateSME(this.prop.campaignId, params).subscribe(
      (fileId) => {
        this.fileService.downloadFile(fileId, 'template-target-campaign-sme.xlsx').subscribe(
          (res) => {
            this.isLoading = false;
            if (!res) {
              this.messageService.error(this.notificationMessage.error);
            }
          },
          () => {
            this.isLoading = false;
          }
        );
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }
}
