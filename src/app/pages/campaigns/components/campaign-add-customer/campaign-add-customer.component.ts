import {AfterViewInit, Component, Injector, OnInit, ViewEncapsulation} from '@angular/core';
import {BaseComponent} from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import {CommonCategory, FunctionCode, Scopes, ScreenType,} from 'src/app/core/utils/common-constants';
import {cleanDataForm, validateAllFormFields} from 'src/app/core/utils/function';
import {CustomValidators} from 'src/app/core/utils/custom-validations';
import * as moment from 'moment';
import {catchError} from 'rxjs/operators';
import {forkJoin, of} from 'rxjs';
import {Utils} from 'src/app/core/utils/utils';
import {CategoryService} from 'src/app/pages/system/services/category.service';
import {RmApi} from 'src/app/pages/rm/apis';
import {AppFunction} from 'src/app/core/interfaces/app-function.interface';
import {Validators} from '@angular/forms';
import {LeadService} from 'src/app/core/services/lead.service';
import {CustomerLeadService} from '../../../lead/service/customer-lead.service';
import {CampaignsService} from '../../services/campaigns.service';

@Component({
  selector: 'app-campaign-add-customer',
  templateUrl: './campaign-add-customer.html',
  styleUrls: ['./campaign-add-customer.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CampaignAddCustomerComponent extends BaseComponent implements OnInit, AfterViewInit {

  form = this.fb.group({
    customerType: '0',
    customerName: ['', [CustomValidators.required, Validators.maxLength(200)]],
    idCard: [null, [CustomValidators.required, Validators.maxLength(50)]],
    birthday: null,
    gender: ['1'],
    national: 'VN',
    email: ['', [Validators.maxLength(50), CustomValidators.email]],
    phone: [null, [CustomValidators.onlyNumber, Validators.maxLength(15)]],
    address: ['', [Validators.maxLength(255)]],
    potentialLevel: '',
    job: ['', [Validators.maxLength(255)]],
    incomePerMonth: null,
    campaign: {value: '', disabled: true},
    customerResources: '',
    leadCode: { value: '', disabled: true },
    customerTypeName: { value: '', disabled: true },
    branchCode: null,
    rmCode: this.currUser?.code,
    branchUpload: { value: '', disabled: true },
    createdDate: { value: moment().format('DD/MM/YYYY'), disabled: true },
    hrsCode: this.currUser?.hrsCode,
    additinalInfor1: ['', [Validators.maxLength(255)]],
    additinalInfor2: ['', [Validators.maxLength(255)]],
    additinalInfor3: ['', [Validators.maxLength(255)]],
    additinalInfor4: ['', [Validators.maxLength(255)]],
    additinalInfor5: ['', [Validators.maxLength(255)]],
    value: ['', [Validators.maxLength(255)]],
  });

  commonData = {
    listCountry: [
      { code: 'VN', name: 'Việt Nam' },
      { code: 'NN', name: 'Nước ngoài' },
    ],
    listCustomerType: [],
    listRmManager: [],
    listRmManagerTerm: [],
    listBranchManager: [],

    // KHCN
    listPotentialLevel: [],
    listCampaign: [],
    listCustomerResources: [],
  };
  actionType: string;
  objFunctionRM: AppFunction;
  objFunction360: AppFunction;

  listBranch : any;
  typeName : string;
  typeId : string;
  campaignId : string;
  campaignName : string;
  today = new Date();
  type = ScreenType.Create;
  queryParamsDetail: any;

  constructor(
    injector: Injector,
    private service: CustomerLeadService,
    private categoryService: CategoryService,
    private rmApi: RmApi,
    private leadService: LeadService,
    private campaignsService: CampaignsService,
  ) {
    super(injector);
    this.isLoading = true;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.LEAD}`);
    this.objFunctionRM = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.objFunction360 = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);
    this.listBranch = this.route.snapshot.queryParams.listBranch;
    this.campaignId = this.route.snapshot.queryParams.campaignId;
    this.campaignName = this.route.snapshot.queryParams.campaignName;
    this.typeId = this.route.snapshot.queryParams.typeId;
    this.queryParamsDetail = this.route.snapshot.queryParams;
    this.type = this.route.snapshot.queryParams.isScreenUpdate ? this.route.snapshot.queryParams.isScreenUpdate : ScreenType.Create;
  }

  ngAfterViewInit() {
    this.form.get('branchCode').valueChanges.subscribe((value) => {
      if (!this.isLoading) {
        this.form.get('rmCode').setValue('');
        this.commonData.listRmManager = _.filter(
          this.commonData.listRmManagerTerm,
          (item) => item.branchCode === value
        );
      }
    });

    this.form.get('email').valueChanges.subscribe((value) => {
      this.form.get('email').setValue(value?.trim(), { emitEvent: false });
    });

    this.form.get('rmCode').valueChanges.subscribe((value) => {
      this.form.controls.hrsCode.setValue(_.first(_.filter(this.commonData.listRmManager, item => item.code === value))?.hrsCode);
    });

  }

  ngOnInit() {
    if (this.type === ScreenType.Update) {
      this.campaignsService.getDetailCustomerInCampaign(this.queryParamsDetail).subscribe(value => {
        if (value) {
          this.form.patchValue({
            customerName: value.customerName,
            customerType: value.customerType,
            birthday: value.birthday ? new Date(value.birthday) : null,
            gender: value.gender,
            national: value.national,
            email: value.email,
            phone: value.phone,
            address: value.address,
            potentialLevel: value.potentialLevel,
            job: value.job,
            incomePerMonth: value.incomePerMonth,
            customerResources: value.customerResources,
            leadCode: value.leadCode,
            idCard: value.idCard,
            createdDate: moment(value.createdDate).format('DD/MM/YYYY'),
            branchCode: value.branchCode,
            rmCode: value.rmCode ? value.rmCode : '',
            additinalInfor1: value.additinalInfor1,
            additinalInfor2: value.additinalInfor2,
            additinalInfor3: value.additinalInfor3,
            additinalInfor4: value.additinalInfor4,
            additinalInfor5: value.additinalInfor5,
            value: value.value
          })
        }
      }, error => {
        this.messageService.error(this.notificationMessage.error);
      });
    }

    this.form.get('branchUpload').setValue(`${this.currUser?.branch} - ${this.currUser?.branchName}`);

    forkJoin([
      this.leadService.getRmByBranch({
        rsId: this.objFunctionRM?.rsId,
        scope: Scopes.VIEW,
      }).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.LEAD_TYPE).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.POTENTIAL_LEVEL).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.POTENTIAL_RESOURCE).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.CAMPAIGN_TYPE).pipe(catchError(() => of(undefined))),
      this.categoryService
        .getBranchesOfUser(this.objFunctionRM?.rsId, Scopes.VIEW)
        .pipe(catchError(() => of(undefined))),
    ]).subscribe(([listRmManagement, leadTypeConfig, listPotentialLevel, listCustomerResources,listPurpose, branchOfUser]) => {
      this.typeName = _.get(
        _.chain(_.get(listPurpose, 'content'))
          .filter((x) => x.code === this.typeId)
          .first()
          .value(),
        'name'
      );
      this.commonData.listCampaign.push({code:this.typeId, name: this.typeName});
      this.form.get('campaign').setValue(this.campaignName);
      const listRm = [];
      listRmManagement?.forEach((item) => {
        if (!_.isEmpty(item?.code)) {
          listRm.push({
            code: item?.code,
            displayName: item?.code + ' - ' + item?.fullName,
            branchCode: item?.branch,
            hrsCode: item?.hrsCode,
          });
        }
      });
      if (!listRm?.find((item) => item.code === this.currUser?.code)) {
        listRm.push({
          code: this.currUser?.code,
          displayName:
            Utils.trimNullToEmpty(this.currUser?.code) + ' - ' + Utils.trimNullToEmpty(this.currUser?.fullName),
          branchCode: this.currUser?.branch,
          hrsCode: this.currUser?.hrsCode,
        });
      }
      this.commonData.listRmManagerTerm = [...listRm];
      this.commonData.listRmManager = [...listRm];
      this.commonData.listBranchManager =
        branchOfUser?.reduce((filtered, item) => {
          if(_.isEmpty(this.listBranch) || _.includes(this.listBranch, item.code)) {
            const newItem = {
              code: item.code,
              name: `${item.code} - ${item.name}`,
            }
            filtered.push(newItem);
          }
          return filtered
        }, []);
      if (!_.find(this.commonData.listBranchManager, (item) => item.code === this.currUser?.branch)
        && (_.isEmpty(this.listBranch) || _.find(this.listBranch, (i) => i ===  this.currUser?.branch))) {
        this.commonData.listBranchManager.push({
          code: this.currUser?.branch,
          name: `${this.currUser?.branch} - ${this.currUser?.branchName}`,
        });
      }
      if (this.type === ScreenType.Create) {
        (_.isEmpty(this.listBranch) || _.find(this.listBranch, (i) => i.code ===  this.currUser?.branch))
          ? this.form.controls.branchCode.setValue(this.currUser?.branch)
          : this.form.controls.branchCode.setValue(_.head(this.commonData.listBranchManager)?.code);
      }

      this.commonData.listPotentialLevel = listPotentialLevel?.content || [];
      this.commonData.listCustomerResources = listCustomerResources?.content || [];
      this.commonData.listCustomerType = leadTypeConfig?.content || [];

      if (this.actionType === ScreenType.Update) {
        // this.getLeadDetail();
      } else {
        this.form.controls.customerTypeName.setValue(
          _.find(this.commonData.listCustomerType, (item) => item.code === this.form.controls.customerType.value)?.name
        );
        // if (_.find(this.commonData.listBranchManager, item => item.code === this.currUser?.branch)) {
          this.commonData.listRmManager = _.filter(
            this.commonData.listRmManagerTerm,
            (item) => item.branchCode === this.form.controls.branchCode.value
          );
        this.isLoading = false;
      }
    });

    this.form.controls.customerType.valueChanges.subscribe((value) => {
      this.form.controls.customerTypeName.setValue(this.form.controls.customerType.value);
    });

  }

  // back() {
  //   this.router.navigateByUrl(functionUri.lead_management, { state: this.state });
  // }

  convertDateToString(dateString) {
    const date = new Date(dateString);
    return (
      date.getFullYear() +
      '-' +
      (date.getMonth() > 8 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1)) +
      '-' +
      (date.getDate() > 9 ? date.getDate() : '0' + date.getDate())
    );
  }

  saveKHCN() {
    if (this.form.valid) {
      this.confirmService.confirm().then((isConfirm) => {
        if (isConfirm) {
          this.isLoading = true;
          const data = {...cleanDataForm(this.form)};
          data.rsId = this.objFunction?.rsId;
          data.sourceLead = data.sourceLeadCode;
          data.scope = 'VIEW';
          if (this.type === ScreenType.Create) {
            data.createdDate = new Date();
          } else {
            data.createdDate = moment(data.createdDate, 'DD/MM/YYYY').toDate();
          }
          data.campaignId = this.campaignId;
          data.campaignId = this.campaignId;
          data.branchUpload = this.form.controls.branchUpload.value;
          data.branchCode = this.form.controls.branchCode.value;
          data.additinalInfor1 = this.form.controls.additinalInfor1.value;
          data.additinalInfor2 = this.form.controls.additinalInfor2.value;
          data.additinalInfor3 = this.form.controls.additinalInfor3.value;
          data.additinalInfor4 = this.form.controls.additinalInfor4.value;
          data.additinalInfor5 = this.form.controls.additinalInfor5.value;
          data.value = this.form.controls.value.value;
          if (this.type === ScreenType.Create) {
            this.leadService.createCampaignLead(data).subscribe(value => {
              this.messageService.success(this.notificationMessage.success);
              this.isLoading = false;
              this.back();
            }, error => {
              this.isLoading = false;
              this.confirmService.warn(error.error.messages.vn);
            });
          } else {
            this.campaignsService.updateLeadFromCampain(data).subscribe(value => {
              this.messageService.success(this.notificationMessage.success);
              this.isLoading = false;
              this.back();
            }, error => {
              this.isLoading = false;
              this.confirmService.warn(error.error.messages.vn);
            });
          }
        }
      });
    } else {
      validateAllFormFields(this.form);
    }
  }

  cancelKHCN() {
    this.back();
  }

  searchKH() {
    const idCard = this.form.get('idCard').value;
    if (_.isEmpty(idCard)) {
      return;
    }
    this.form.controls.customerType.setValue('0');
    this.form.controls.national.setValue('VN');
    this.form.controls.gender.setValue('1');
    this.form.controls.incomePerMonth.setValue(null);
    this.form.controls.customerName.reset();
    this.form.controls.birthday.reset();
    this.form.controls.email.reset();
    this.form.controls.phone.reset();
    this.form.controls.address.reset();
    this.form.controls.leadCode.reset();
    this.form.controls.potentialLevel.reset();
    this.form.controls.customerResources.reset();
    this.form.controls.job.reset();
    this.form.controls.additinalInfor1.reset();
    this.form.controls.additinalInfor2.reset();
    this.form.controls.additinalInfor3.reset();
    this.form.controls.additinalInfor4.reset();
    this.form.controls.additinalInfor5.reset();
    this.form.controls.value.reset();
    const data = {
      branchCode: this.form.controls.branchCode.value,
      idCard: this.form.get('idCard').value,
      campaignId: this.campaignId,
    }
    this.leadService.searchKHCampaignByIdCard(data).subscribe(value => {
      if(value) {
        this.form.patchValue({
          customerName: value.customerName,
          customerType: value.customerType,
          birthday: value.birthday ? new Date(value.birthday) : null,
          gender: value.gender,
          national: value.national,
          email: value.email,
          phone: value.phone,
          address: value.address,
          potentialLevel: value.potentialLevel,
          job: value.job,
          incomePerMonth: value.incomePerMonth,
          customerResources: value.customerResources,
          leadCode: value.leadCode,
          additinalInfor1: value.additinalInfor1,
          additinalInfor2: value.additinalInfor2,
          additinalInfor3: value.additinalInfor3,
          additinalInfor4: value.additinalInfor4,
          additinalInfor5: value.additinalInfor5,
          value: value.value,
        });
      }
    }, error => {
      this.confirmService.warn(error.error.messages.vn);
    })
  }
}
