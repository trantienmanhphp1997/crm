import { forkJoin, of, Observable } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, Injector, AfterViewInit, Input, OnChanges, ViewChild } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { ActivityType, CommonCategory, ConfigBackDate, ScreenType } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { ActivityService } from 'src/app/core/services/activity.service';
import * as moment from 'moment';
import _ from 'lodash';
import {SaleManagerApi} from "../../../sale-manager/api";
import {ProductService} from "../../../../core/services/product.service";

@Component({
  selector: 'app-activity.action',
  templateUrl: './activity.action.component.html',
  styleUrls: ['./activity.action.component.scss'],
})
export class ActivityActionComponent extends BaseComponent implements OnInit, AfterViewInit, OnChanges {
  @Input() parent: any;
  @Input() isActionHeader: boolean;
  @Input() type = ScreenType.Create;
  @Input() data: any;
  @Input() id: string;
  titleHeader: string;
  today = new Date();
  backDate: number;
  form = this.fb.group({
    id: [''],
    parentId: [''],
    parentName: [''],
    parentType: [''],
    activityType: ['', CustomValidators.required],
    dateStart: [this.today, CustomValidators.required],
    activityResult: ['', CustomValidators.required],
    location: [''],
    note: [''],
    isFutureActivity: [''],
    activityParentResult: [''],
    futureActivity: this.fb.group({
      id: [''],
      activityType: [''],
      dateStart: [''],
      futureActivityType: [''],
      location: [''],
      note: [''],
    }),
    productCode: ''
  });
  nextActivityMinDate = this.today;
  activityMinDate = this.today;
  activityMaxDate = new Date();
  listType = [];
  listNextTask = [];
  listResult = [];
  resultItems: Array<any>;
  isShowLocation = false;
  isShowLocationFuture = false;
  isValidator = false;
  itemCustomer: any;
  backHours = 0;
  rsId = '';
  campaignId = '';
  customerId = '';
  isReload = false;
  isShowCheckbox = true;
  model: any;
  @ViewChild('listView') listView: any;
  listProduct: any;


  constructor(
    injector: Injector,
    private modalActive: NgbActiveModal,
    private activityService: ActivityService,
    private saleManagerApi: SaleManagerApi,
    private productService: ProductService,
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.rsId = _.get(this.parent, 'rsId');
    this.campaignId = _.get(this.parent, 'campaignId');
    this.customerId = _.get(this.parent, 'parentId');
    if (!this.type || this.type === ScreenType.Create) {
      this.titleHeader = 'title.createActivity';
      this.form.controls.futureActivity.disable();
    } else {
      this.titleHeader = 'title.detailActivity';
    }
    forkJoin([
      this.productService.getProductTreeSale(this.parent.productCode).pipe(catchError((e) => of(undefined))),
      this.commonService
        .getCommonCategory(CommonCategory.CONFIG_ACTIVITY_RESULT)
        .pipe(catchError((e) => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.CONFIG_TYPE_ACTIVITY).pipe(catchError((e) => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.ACTIVITY_NEXT_TYPE).pipe(catchError((e) => of(undefined))),
      this.commonService
        .getCommonCategory(CommonCategory.CONFIG_BACK_DATE, ConfigBackDate.BACK_DATE_ACTIVITY)
        .pipe(catchError((e) => of(undefined))),
    ]).subscribe(([listProduct, listResult, listType, listNextTask, backDate]) => {
      this.listProduct = listProduct?.map((item) => {
        return { code: item.idProductTree, displayName: item.idProductTree + ' - ' + item.description };
      });
      this.resultItems = _.get(listResult, 'content');
      _.forEach(_.get(listResult, 'content'), (item) => {
        if (item.code === item.value) {
          item.children = _.chain(_.get(listResult, 'content'))
            .filter((itemChild) => itemChild.value === item.code && itemChild.code !== itemChild.value)
            .value();
          if (item.children.length === 0) {
            delete item.children;
          }
          this.listResult.push(item);
        }
      });
      this.listType = _.get(listType, 'content') || [];
      this.listNextTask = _.get(listNextTask, 'content') || [];
      this.backDate = _.get(_.chain(_.get(backDate, 'content')).first().value(), 'value');
      this.backHours = this.backDate * 24;
      this.activityMinDate = new Date(moment().add(-this.backHours, 'hour').startOf('day').valueOf());
      if (this.data) {
        this.onModelChange(this.data);
      }
    });
  }

  ngOnChanges() { }

  ngAfterViewInit() {
    this.form.controls.dateStart.valueChanges.subscribe((value) => {
      if (moment(value).isAfter(moment())) {
        this.form?.controls?.activityResult?.clearValidators();
      } else {
        this.form?.controls?.activityResult?.setValidators(CustomValidators.required);
      }
      this.form?.controls?.activityResult?.updateValueAndValidity({ emitEvent: false });
    });
    this.form.controls.activityResult.valueChanges.subscribe((value) => {
      if (value === 'KHAC') {
        this.form.controls.note.setValidators(CustomValidators.required);
      } else {
        this.form.controls.note.clearValidators();
      }
      this.form.controls.note.updateValueAndValidity({ emitEvent: false });
    });
    this.form.valueChanges.subscribe((data) => {
      this.isShowLocation = _.get(data, 'activityType') === ActivityType.Meeting;
      this.isShowLocationFuture = _.get(data, 'futureActivity.activityType') === ActivityType.Meeting;
      if (data?.isFutureActivity) {
        this.form?.get('futureActivity.activityType')?.setValidators(CustomValidators.required);
        this.form?.get('futureActivity.dateStart')?.setValidators(CustomValidators.required);
        this.form?.get('futureActivity.futureActivityType')?.setValidators(CustomValidators.required);
      } else {
        this.form?.get('futureActivity.activityType')?.clearValidators();
        this.form?.get('futureActivity.dateStart')?.clearValidators();
        this.form?.get('futureActivity.futureActivityType')?.clearValidators();
      }
      this.form?.controls?.futureActivity?.updateValueAndValidity({ emitEvent: false });
    });

    this.form.controls.activityResult.valueChanges.subscribe((value) => {
      const val = _.chain(this.resultItems)
        .filter((x) => x.code === value)
        .first()
        .value();
      this.form.controls.activityParentResult.setValue(_.get(val, 'value'));
    });
  }

  save() {
    this.isReload = !this.isReload;
    this.isValidator = true;
    if (this.form.valid) {
      this.isLoading = true;
      const data = cleanDataForm(this.form);
      if (this.parent) {
        data.parentId = _.get(this.parent, 'parentId');
        data.parentName = _.get(this.parent, 'parentName');
        data.parentType = _.get(this.parent, 'parentType');
        data.campaignId = _.get(this.parent, 'campaignId');
      }
      let api: Observable<any>;
      if (this.type === ScreenType.Create) {
        api = this.activityService.create(data);
      } else {
        data.futureActivityId = _.get(this.data, 'futureActivityId');
        api = this.activityService.update(data);
      }
      api.subscribe(
        () => {
          this.isLoading = false;
          this.communicateService.request({ name: 'refreshActivityList' });
          this.messageService.success(_.get(this.notificationMessage, 'success'));
          this.clean();
          this.form.controls.futureActivity.reset();
          this.listView?.reload();
        },
        () => {
          this.isLoading = false;
          this.messageService.error(_.get(this.notificationMessage, 'error'));
          this.clean();
        }
      );
    } else {
      validateAllFormFields(this.form);
    }
  }

  isCreateNextActivity(e) {
    if (_.get(e, 'checked')) {
      this.form.controls.futureActivity.enable();
    } else {
      this.form.controls.futureActivity.disable();
    }
  }

  closeModal() {
    this.modalActive.close(false);
  }

  clean() {
    this.type = ScreenType.Create;
    this.form?.enable();
    this.form?.controls?.futureActivity?.disable();
    this.form.reset();
    this.activityMaxDate = new Date();
  }

  onModelChange(item) {
    this.clean();
    if (!item) {
      return;
    }
    this.data = item;
    this.type = ScreenType.Update;
    this.form.controls.dateStart.disable();
    this.form.controls.activityType.disable();
    this.form.controls.activityResult.disable();
    this.form.controls.note.disable();
    this.form.controls.location.disable();
    this.activityMaxDate = new Date();
    if (this.data?.dateStart) {
      this.data.dateStart = new Date(this.data.dateStart);
      const date = moment(this.data.dateStart).add(this.backDate, 'day');
      if (moment(date).isAfter(moment().startOf('day'))) {
        this.form.controls.activityResult.enable();
        this.form.controls.note.enable();
        this.form.controls.location.enable();
        this.form.controls.activityType.enable();
        this.form.controls.dateStart.enable();
      }
      if (this.data.futureActivity?.dateStart) {
        this.form.controls.futureActivity.enable();
        this.data.futureActivity.dateStart = new Date(this.data.futureActivity?.dateStart);
        if (
          moment(this.data.futureActivity.dateStart)
            .add(this.backDate, 'day')
            .isBefore(moment().startOf('day'))
        ) {
          this.form.controls.futureActivity.disable();
        }
        this.isShowCheckbox = false;
      }
      this.data.productCode = this.data.productCode;
      this.form.patchValue(this.data);
    }
  }

  showLoading(isLoading) {
    this.isLoading = isLoading;
    this.ref.detectChanges();
  }

  clearFilter() {
    this.form.controls.productCode.setValue('');
  }
}
