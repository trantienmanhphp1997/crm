import { trigger, state, style, transition, animate, AUTO_STYLE } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/pages/dashboard/services/dashboard.service';

@Component({
  selector: 'app-campaigns-left-view',
  templateUrl: './campaigns-left-view.component.html',
  styleUrls: ['./campaigns-left-view.component.scss'],
  animations: [
    trigger('collapse', [
      state(
        'false',
        style({ height: AUTO_STYLE, visibility: AUTO_STYLE, paddingTop: '0.5rem', borderTop: '1px solid' })
      ),
      state('true', style({ height: '0', visibility: 'hidden' })),
      transition('false => true', animate(150 + 'ms ease-in')),
      transition('true => false', animate(150 + 'ms ease-out')),
    ]),
  ],
})
export class CampaignsLeftViewComponent implements OnInit {
  activityCalls: any[];
  role: string;
  branchCode: string;
  constructor(private dashboardService: DashboardService) {
    // this.role = getRole(global.roles);
    // this.branchCode = global?.user?.branch || '';
  }

  ngOnInit(): void {
    this.getData();
    // if (this.role !== Roles.RGM) {
    //   this.dashboardService.onUpdateData().subscribe((res) => {
    //     if (res && res.branchCode === this.branchCode) {
    //       this.getData();
    //     }
    //   });
    // } else {
    //   setInterval(() => {
    //     this.getData();
    //   }, 90000);
    // }
  }

  getData() {
    this.dashboardService.getActivityCallsByRole().subscribe((result) => {
      if (result) {
        this.activityCalls = [];
        // result.forEach((activity) => {
        //   activity.isCollapsed = true;
        //   this.activityCalls.push(activity);
        // });
      }
    });
  }

  collapsedActivity(i: number) {
    this.activityCalls[i].isCollapsed = !this.activityCalls[i].isCollapsed;
  }

  identify(index, item) {
    return item ? item.id : undefined;
  }
}
