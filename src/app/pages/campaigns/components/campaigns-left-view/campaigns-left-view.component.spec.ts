import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignsLeftViewComponent } from './campaigns-left-view.component';

describe('CampaignsLeftViewComponent', () => {
  let component: CampaignsLeftViewComponent;
  let fixture: ComponentFixture<CampaignsLeftViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignsLeftViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignsLeftViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
