import { Component, HostBinding, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonCategoryService } from 'src/app/core/services/common-category.service';
import { CampaignsService } from '../../services/campaigns.service';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import {
  CampaignStatus,
  CommonCategory,
  maxInt32,
  ProductLevel,
  FunctionCode, SessionKey,
} from 'src/app/core/utils/common-constants';
import _ from 'lodash';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Utils } from '../../../../core/utils/utils';
import * as moment from 'moment';
import { NotifyMessageService } from '../../../../core/components/notify-message/notify-message.service';
import { AppFunction } from 'src/app/core/interfaces/app-function.interface';
import { ProductService } from 'src/app/core/services/product.service';
import { SessionService } from 'src/app/core/services/session.service';
import { TranslateService } from '@ngx-translate/core';
import { FileService } from 'src/app/core/services/file.service';

@Component({
  selector: 'app-campaigns-rm-detail',
  templateUrl: './campaigns-rm-detail.component.html',
  styleUrls: ['./campaigns-rm-detail.component.scss'],
})
export class CampaignsRmDetailComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private commonService: CommonCategoryService,
    private campaignService: CampaignsService,
    private categoryService: CategoryService,
    private messageService: NotifyMessageService,
    private productService: ProductService,
    private sessionService: SessionService,
    private translate: TranslateService,
    private fileService: FileService
  ) {
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
    this.showBtnAddCampaign = this.route.snapshot.queryParams.showBtnAddCampaign === 'true';
    this.stage = this.route.snapshot.queryParams.stage;
    this.listBranch = this.route.snapshot.queryParams.listBranch;
    this.activityResult = this.route.snapshot.queryParams.activityResult;
    this.obj = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CAMPAIGN_RM}`);
    this.rsId = _.get(this.obj, 'rsId');
  }
  showBtnAddCampaign: boolean;
  rsId: any;
  model: any = {};
  tabIndex = 0;
  isLoading: boolean;
  isSlide: boolean;
  prop: any;
  scope: string;
  isOutreachProcess: boolean;
  isRmInsert: boolean;
  isRegion: boolean;
  showAction: boolean;
  campaignTrans: any;
  fields: any;
  id: string;
  obj: AppFunction;
  name: string;
  notificationMessage: any;
  region_name: string;
  bizLine_name: string;
  listBranch: any;
  activityResult: string;
  stage = '';
  @HostBinding('class.app__right-content') appRightContent = true;

  ngOnInit(): void {

    this.id = _.get(this.route.snapshot.params, 'id');
    this.isLoading = true;
    forkJoin([
      this.commonService.getCommonCategory(CommonCategory.EXT_CAMPAIGNS_CHANNELS).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.CAMPAIGN_TYPE).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.CAMPAIGN_FORM).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.SOURCE_DATA).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.CAMPAIGN_DIVISION).pipe(catchError(() => of(undefined))),
      this.productService.getProductByLevel(ProductLevel.Lvl1).pipe(catchError(() => of(undefined))),
      this.categoryService.getBlocksCategoryByRm().pipe(catchError(() => of(undefined))),
      this.categoryService.searchBranches({ page: 0, size: maxInt32 }).pipe(catchError(() => of(undefined))),
      this.translate.get(['campaign', 'fields', 'notificationMessage']).pipe(catchError(() => of(undefined))),
      this.campaignService.getById(this.id).pipe(catchError(() => of(undefined))),
      this.categoryService.getRegion().pipe(catchError(() => of(undefined))),
    ]).subscribe(
      ([
        listSaleChannel,
        listPurpose,
        listFormality,
        listSourceDate,
        listBizLine,
        listProduct,
        listBlock,
        listBranch,
        field,
        model,
        listRegion,
      ]) => {
        if (Utils.isNotNull(model)) {
          this.model = model;
          this.fields = _.get(field, 'fields');
          this.notificationMessage = _.get(field, 'notificationMessage');
          this.scope = _.get(this.model, 'scope');
          this.isOutreachProcess = _.get(this.model, 'isOutreachProcess');
          this.isRmInsert = _.get(this.model, 'isRmInsert');
          this.isRegion = this.scope === 'CHI_NHANH';
          this.model.status = _.get(field, 'campaign').status[_.get(this.model, 'status').toLowerCase()];
          this.model.content = Utils.isStringNotEmpty(_.get(this.model, 'content'))
            ? _.get(this.model, 'content')
            : '---';
          this.showAction =
            _.get(model, 'status') === CampaignStatus.InProgress &&
            moment(_.get(this.model, 'endDate')).isAfter(moment().endOf('day'));
          this.model.typeName = _.get(
            _.chain(_.get(listPurpose, 'content'))
              .filter((x) => x.code === _.get(this.model, 'typeId'))
              .first()
              .value(),
            'name'
          );
          this.model.formName = _.get(
            _.chain(_.get(listFormality, 'content'))
              .filter((x) => x.code === _.get(this.model, 'formId'))
              .first()
              .value(),
            'name'
          );
          this.model.productName = listProduct
            ?.map((item) => {
              return { code: item.idProductTree, name: `${item.idProductTree} - ${item.description}` };
            })
            ?.find((item) => item.code === this.model?.productCode)?.name;
          this.model.blockName = _.get(
            _.chain(listBlock)
              .filter((x) => x.code === _.get(this.model, 'blockCode'))
              .first()
              .value(),
            'name'
          );
          this.model.channelName =
            _.get(
              _.chain(_.get(listSaleChannel, 'content'))
                .filter((x) => x.code === _.get(this.model, 'channel'))
                .first()
                .value(),
              'name'
            ) || _.get(this.fields, 'all');
          this.model.sourceName = _.get(
            _.chain(_.get(listSourceDate, 'content'))
              .filter((x) => x.code === _.get(this.model, 'sourceId'))
              .first()
              .value(),
            'name'
          );
          this.model.bizLine = _.map(
            _.chain(_.get(listBizLine, 'content'))
              .filter((x) => _.get(this.model, 'bizLine').includes(x.code))
              .value(),
            (x) => x.name
          );
          this.bizLine_name = _.join(_.get(this.model, 'bizLine'), ', ');
          this.campaignTrans = _.get(field, 'campaign');
          this.model.region = _.map(
            _.chain(listRegion)
              .filter(
                (x) =>
                  Utils.isArrayNotEmpty(_.get(this.model, 'areaCode')) &&
                  _.get(this.model, 'areaCode').includes(x.locationCode)
              )
              .value(),
            (x) => x.locationCode
          );
          this.region_name = _.join(_.get(this.model, 'region'), ', ');
          this.model.branch = _.map(
            _.chain(_.get(listBranch, 'content'))
              .filter(
                (itemBranch) =>
                  Utils.isArrayNotEmpty(_.get(this.model, 'branchCodes')) &&
                  _.get(this.model, 'branchCodes').includes(itemBranch.code)
              )
              .value(),
            (x) => `${x.code} - ${x.name}`
          );
        }

        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
        this.messageService.error(_.get(this.notificationMessage, 'E001'));
      }
    );
  }


  onTabChanged($event) {}

  getValue(model, key) {
    return Utils.isStringNotEmpty(_.get(model, key)) ? _.get(model, key) : '---';
  }

  back() {
    this.sessionService.clearSessionData(SessionKey.CUSTOMER_TARGETS);
    this.router.navigateByUrl('/campaigns/campaign-rm', { state: this.prop });
  }

  downloadFile() {
    if (Utils.isStringNotEmpty(_.get(this.model, 'fileId')) && Utils.isStringNotEmpty(_.get(this.model, 'fileName'))) {
      this.fileService.downloadFile(_.get(this.model, 'fileId'), _.get(this.model, 'fileName')).subscribe((res) => {
        if (!res) {
          this.messageService.error(_.get(this.notificationMessage, 'error'));
        }
      });
    }
  }

  getDate(item, value) {
    return _.get(item, value);
  }
}
