import { forkJoin, of } from 'rxjs';
import { Component, OnInit, ViewChild, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { CampaignsService } from '../../services/campaigns.service';
import _ from 'lodash';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import { LeadProsessName, Scopes } from 'src/app/core/utils/common-constants';
import { TranslateService } from '@ngx-translate/core';
import { catchError } from 'rxjs/operators';
import { WorkflowService } from 'src/app/core/services/workflow.service';
import { ProductService } from 'src/app/core/services/product.service';
import { Utils } from 'src/app/core/utils/utils';

@Component({
  selector: 'customer-campaigns',
  templateUrl: './customer-campaigns.component.html',
  styleUrls: ['./customer-campaigns.component.scss'],
})
export class CustomerCampaignsComponent implements OnInit {
  constructor(
    private service: CampaignsService,
    private translate: TranslateService,
    private workflowService: WorkflowService,
    private productService: ProductService
  ) {
    this.messages = global.messageTable;
    this.translate.get(['fields', 'notificationMessage', 'confirmMessage']).subscribe((result) => {
      this.notificationMessage = result.notificationMessage;
      this.fields = result.fields;
    });
  }

  @ViewChild('table') table: DatatableComponent;
  @HostBinding('class.app__right-content') appRightContent = true;
  @Input() code: string;
  @Input() rsId: string;
  @Input() model: any;
  @Output() modelChange = new EventEmitter();
  models: Array<any>;
  pageable: Pageable;
  isFisrt: boolean = true;
  isLoading: boolean;
  fields: any;
  messages: any;
  notificationMessage: any;
  paramSearch = {
    size: global.userConfig.pageSize,
    page: 0,
    rsId: '',
    scope: Scopes.VIEW,
  };
  prevParams = this.paramSearch;
  listProcess = [];
  products: Array<any>;

  ngOnInit(): void {
    this.isLoading = true;
    this.paramSearch.rsId = this.rsId;
    this.reload();
  }

  setPage(pageInfo) {
    if (this.isFisrt) {
      return;
    }
    this.paramSearch.page = pageInfo.offset;
    this.reload(false);
  }

  search() {
    this.reload(true);
  }

  reload(isSearch?: boolean) {
    this.isLoading = true;
    this.modelChange.emit(true);
    let params: any = {};
    if (isSearch) {
      this.paramSearch.page = 0;
    } else {
      if (!this.prevParams) {
        return;
      }
      params = this.prevParams;
    }
    Object.keys(this.paramSearch).forEach((key) => {
      params[key] = this.paramSearch[key];
    });
    params.customerCode = this.code;
    params.rsId = this.rsId;
    forkJoin([
      this.service.findHistoryByCode(params).pipe(catchError((e) => of(undefined))),
      this.workflowService.findAllProcess().pipe(catchError((e) => of(undefined))),
      this.productService.findAll().pipe(catchError((e) => of(undefined))),
    ]).subscribe(
      ([listHistory, listProcess, products]) => {
        this.products = products;
        this.listProcess = listProcess;
        this.models = _.get(listHistory, 'content') || [];
        _.forEach(this.models, (item) => {
          item.stageName = _.get(
            _.find(
              this.listProcess,
              (i) => i.nameProcessDefine === (item.opt ? item.nameProcess : LeadProsessName) && i.status === item.status
            ),
            'title'
          );
        });

        this.prevParams = params;
        this.pageable = {
          totalElements: _.get(listHistory, 'totalElements'),
          totalPages: _.get(listHistory, 'totalPages'),
          currentPage: _.get(listHistory, 'number'),
          size: _.get(global, 'userConfig.pageSize'),
        };
        this.isLoading = false;
        this.isFisrt = false;
        this.model = false;
        this.modelChange.emit(this.model);
      },
      () => {
        this.isLoading = false;
        this.isFisrt = false;
        this.model = false;
        this.modelChange.emit(this.model);
      }
    );
  }

  getProduct(row, key) {
    var val = _.get(row, key);
    if (Utils.isStringNotEmpty(val)) {
      return _.get(
        _.chain(this.products)
          .filter((x) => x.idProductTree === val)
          .first()
          .value(),
        'description'
      );
    }
    return '';
  }

  getValue(row, key) {
    return _.get(row, key);
  }
}
