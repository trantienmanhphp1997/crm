import {AfterViewInit, Component, Injector, OnInit, ViewEncapsulation} from "@angular/core";
import {BaseComponent} from "../../../../core/components/base.component";
import {CustomValidators} from "../../../../core/utils/custom-validations";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {
  FunctionCode,
  LeadProsessName, maxInt32,
  ProcessType,
  ProductLevel, Scopes, ScreenType,
  StatusLead,
  TaskType
} from "../../../../core/utils/common-constants";
import {validateAllFormFields} from "../../../../core/utils/function";
import {WorkflowService} from "../../../../core/services/workflow.service";
import {TimeLineWorkflow} from "../campaign-rm-customer/campaign-rm-customer.component";
import {PrimeIcons} from "primeng/api";
import {ProductService} from "../../../../core/services/product.service";
import {forkJoin, of} from "rxjs";
import {catchError} from "rxjs/operators";
import * as _ from 'lodash';
import {ActivityActionComponent} from "../activity.action/activity.action.component";
import {ConfirmDialogComponent} from "../../../../shared/components";
import {ActivityService} from "../../../../core/services/activity.service";
import {LeadService} from "../../../../core/services/lead.service";
@Component({
  selector: 'app-campaign-change-stage',
  templateUrl: 'campaign-change-stage.component.html',
  styleUrls: ['campaign-change-stage.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CampaignChangeStageComponent extends BaseComponent implements OnInit,AfterViewInit {
  showForm: boolean;
  form = this.fb.group({
    status: [null, CustomValidators.required],
    product: null,
  });
  lanes: any[] = [];
  isValidator: boolean;
  workflowType: string;
  showFormProduct: boolean;
  listProductChild = [];
  listProduct = [];
  customer: any = {};
  workflow: TimeLineWorkflow[];
  isDone: boolean;
  componentLoaded: boolean;
  listProcess = [];
  itemCampaign: any;
  paramsActivity = {
    campaignId: '',
    page: 0,
    parentId: '',
    rsId: '',
    scope: Scopes.VIEW,
    size: maxInt32,
  };
  isLoading = true;

  constructor(injector: Injector,
              private modalActive: NgbActiveModal,
              private workflowService: WorkflowService,
              private productService: ProductService,
              private activityService: ActivityService,
              private leadService: LeadService,) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CAMPAIGN_RM}`);
    this.paramsActivity.rsId = this.objFunction?.rsId;
  }

  ngOnInit(): void {
    const paramsLead = {
      campaignId: this.itemCampaign.id,
      customerCode: this.customer.customerCode,
      type: this.customer.type,
    };
    forkJoin([
      this.productService.getProductByLevel(ProductLevel.Lvl1).pipe(catchError(() => of(undefined))),
      this.workflowService.findAllProcess().pipe(catchError(() => of(undefined))),
      this.productService.getProductTreeSale(this.itemCampaign?.productCode),
      this.leadService.getDetailLead(paramsLead)])
      .subscribe(
        ([listProduct,listProcess,list, lead]) => {
          this.listProcess = _.orderBy(listProcess, ['index'], ['asc']);
          this.listProduct = listProduct?.map((item) => {
            return { code: item.idProductTree, name: `${item.idProductTree} - ${item.description}` };
          });
          this.listProductChild = list?.map((item) => {
            return { code: item.idProductTree, name: `${item.idProductTree} - ${item.description}` };
          });
          this.customer = lead;
          this.isLoading = false;
          if(this.customer?.opportunity?.productCodeSale){
            this.form.controls.product.setValue(this.customer?.opportunity?.productCodeSale);
          }
          this.paramsActivity.parentId = this.customer.customerCode;
          this.paramsActivity.campaignId = this.itemCampaign.id;
          this.mapWorkFlow(true);

        }
      );
  }
  ngAfterViewInit(){
    this.form.controls.status.valueChanges.subscribe((value) => {
      this.showFormProduct = (value !== 'NEW' && value !== 'WORKING');
      if(this.showFormProduct){
        console.log('show product');
        this.form.controls.product.setValidators(CustomValidators.required);
      }
      else{
        console.log('not show product');
        this.form.controls.product.clearValidators();
      }
      this.form.controls.product.updateValueAndValidity({ emitEvent: false });
    });
  }
  closeModal() {
    this.modalActive.close();
  }
  changeStage() {
    if (this.form.valid) {
      this.isLoading = true;
      const dataForm = this.form.getRawValue();
      console.log('dataform: ', dataForm);
      this.form.controls.product.reset();
      if (this.workflowType === ProcessType.LEAD) {
        this.workflowService.processQualify(this.customer.id, dataForm?.status).subscribe(
          () => {
            if (this.lanes?.findIndex((item) => item.status === dataForm?.status) === this.workflow.length - 1) {
              this.createOpportunity();
            } else {
              this.customer.status = dataForm?.status;
              this.mapWorkFlow();
              this.isLoading = false;
              this.messageService.success(this.notificationMessage.success);
            }
          },
          () => {
            this.isLoading = false;
            this.messageService.error(this.notificationMessage.error);
          }
        );
      } else {
        const status = this.lanes?.find((item) => item.status === dataForm?.status)?.lane;
        console.log('status:', status);
        let product = dataForm?.product;
        if(status === 'status_new' || status === 'status_working'){
          product = null;
        }
        this.workflowService.processOpportunity(this.customer?.opportunity?.id, status, product).subscribe(
          () => {
            if (this.showFormProduct) {
              this.form.controls.product.setValue(dataForm?.product);
            }
            this.customer.opportunity.status = dataForm?.status;
            this.customer.opportunity.productCodeSale = dataForm?.product;
            this.mapWorkFlow();
            this.messageService.success(this.notificationMessage.success);
            if (this.lanes?.findIndex((item) => item.status === dataForm?.status) === this.workflow.length - 1) {
              this.communicateService.request({ name: 'FireWorks' });
              this.isDone = true;
            }
            this.isLoading = false;
            //confirm tao hoat dong
            const modalRef = this.confirmCreateActivity();
            const data = {
              customer: this.customer,
              modalRef
            }
            this.modalActive.close(data);
          },
          () => {
            this.modalActive.close();
            this.isLoading = false;
            this.messageService.error(this.notificationMessage.error);
          }
        );
      }
    } else {
      this.isValidator = true;
      validateAllFormFields(this.form);
    }
  }
  confirmCreateActivity(){
    const confirmSave = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirmSave.componentInstance.message = 'Bạn có muốn tạo hoạt động tương tác với Khách hàng ?';
    confirmSave.componentInstance.title = '';
    // confirmSave.result
    //   .then((confirmed: boolean) => {
    //     if (confirmed) {
    //       this.createActivity();
    //     }
    //     // debugger
    //     // const data = {
    //     //   customer: this.customer
    //     // }
    //     // this.modalActive.close(data);
    //   })
    //   .catch(() => {
    //   });
    return confirmSave;
  }
  createOpportunity() {
    this.workflowService.convertOpportunity(this.customer?.id).subscribe((opportunity) => {
      this.customer.opportunity = opportunity;
      this.workflowType = ProcessType.OPPORTUNITY;
      this.customer.status = StatusLead.CONVERTED;
      this.mapWorkFlow(true);
      this.messageService.success(this.notificationMessage.success);
    });
  }
  mapWorkFlow(isReload?: boolean) {
    let currStatus = '';
    if (isReload) {
      this.workflowType = this.customer?.status === StatusLead.CONVERTED ? ProcessType.OPPORTUNITY : ProcessType.LEAD;
      currStatus =
        this.workflowType === ProcessType.LEAD ? this.customer?.status : this.customer?.opportunity?.status;
      const nameProcess = this.workflowType === ProcessType.LEAD ? LeadProsessName : this.itemCampaign?.process;
      const lanes = this.listProcess?.filter((i) => i.nameProcessDefine === nameProcess);
      this.lanes = [];
      this.workflow = [];
      lanes?.forEach((itemLane, i) => {
        if (lanes.length > 0 && lanes.length - 1 === i) {
          if (currStatus === itemLane.status) {
            this.workflow[this.workflow.length - 1].name = `${i}. ${itemLane.title}`;
          } else if (currStatus !== this.workflow[this.workflow.length - 1].status) {
            this.workflow[this.workflow.length - 1].name += '/ ' + itemLane.title;
          }
        } else {
          this.workflow?.push({
            background: 'bg-grey',
            name: `${i + 1}. ${itemLane.title}`,
            lane: itemLane?.lane,
            status: itemLane?.status,
          });
        }
        this.lanes.push({
          name: itemLane.title,
          lane: itemLane?.lane,
          status: itemLane?.status,
          disabled: i === 0,
        });
      });
    } else {
      currStatus =
        this.workflowType === ProcessType.LEAD ? this.customer?.status : this.customer?.opportunity?.status;
    }
    this.form.controls.status.setValue(currStatus);
    const index = this.lanes?.findIndex((item) => item.status === currStatus);
    this.isDone = index === this.workflow?.length - 1;
    this.workflow?.forEach((item, i) => {
      item.background = i <= index ? 'bg-green' : 'bg-grey';
      item.icon = PrimeIcons.CHECK;
    });
    if (this.workflow[this.workflow?.length - 1]) {
      if (index === this.workflow?.length - 1) {
        this.workflow[this.workflow?.length - 1].name = `${index + 1}. ${this.lanes[index].name}`;
      } else if (index > this.workflow?.length - 1) {
        this.workflow[this.workflow?.length - 1].name = `${index}. ${this.lanes[index].name}`;
        this.workflow[this.workflow?.length - 1].icon = PrimeIcons.TIMES;
        this.workflow[this.workflow?.length - 1].background = 'bg-danger';
      }
    }
    if (!currStatus) {
      this.messageService.error(this.notificationMessage.E001);
      this.isDone = true;
    }
    this.isLoading = false;
    this.componentLoaded = true;
  }
  createActivity() {
    const parent: any = {
      parentType: TaskType.LEAD,
      parentId: this.customer?.customerCode,
      parentName: this.customer?.customerName,
      campaignId: this.itemCampaign?.id,
      rsId: this.objFunction?.rsId,
      productCode: this.itemCampaign?.productCode,
    };
    const modal = this.modalService.open(ActivityActionComponent, {
      windowClass: 'create-activity-modal',
      scrollable: true,
    });
    modal.componentInstance.parent = parent;
    modal.componentInstance.type = ScreenType.Create;
    modal.result.then(() => {
      //xu li sau
      //  this.getActivity();
    });
  }
  // getActivity() {
  //   this.activityService.search(this.paramsActivity).subscribe((listActivity) => {
  //     this.listActivity = listActivity?.content || [];
  //   });
  // }
}
