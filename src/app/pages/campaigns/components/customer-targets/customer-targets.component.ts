import { forkJoin, of } from 'rxjs';
import {Component, OnInit, ViewChild, Input, HostBinding, Injector} from '@angular/core';
import { CampaignsService } from '../../services/campaigns.service';
import { CommonCategoryService } from 'src/app/core/services/common-category.service';
import _ from 'lodash';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import {
  defaultExportExcel, FunctionCode,
  functionUri,
  LeadProsessName,
  ProcessType,
  ScreenType,
  SessionKey,
  StatusLead,
  TaskType,
} from '../../../../core/utils/common-constants';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { global } from '@angular/compiler/src/util';
import { Scopes, CommonCategory, CustomerImportType } from 'src/app/core/utils/common-constants';
import { TranslateService } from '@ngx-translate/core';
import { ActivityActionComponent } from '../activity.action/activity.action.component';
import { catchError } from 'rxjs/operators';
import * as moment from 'moment';
import { CustomerCampaignModalComponent } from '../customer-campaign-modal/customer-campaign-modal.component';
import { Router } from '@angular/router';
import { WorkflowService } from 'src/app/core/services/workflow.service';
import { ProductService } from 'src/app/core/services/product.service';
import { SessionService } from 'src/app/core/services/session.service';
import { FileService } from 'src/app/core/services/file.service';
import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';
import {MenuItem} from 'primeng/api';
import {ConfirmDialogComponent} from '../../../../shared/components';
import {AppFunction} from '../../../../core/interfaces/app-function.interface';
import {MbeeChatService} from '../../../../core/services/mbee-chat.service';
import {createGroupChat11} from '../../../../core/utils/mb-chat';
import {BaseComponent} from "../../../../core/components/base.component";
import {CampaignChangeStageComponent} from "../campaign-change-stage/campaign-change-stage.component";
import {LeadService} from "../../../../core/services/lead.service";
import {CategoryService} from "../../../system/services/category.service";

@Component({
  selector: 'customer-targets',
  templateUrl: './customer-targets.component.html',
  styleUrls: ['./customer-targets.component.scss'],
})
export class CustomerTargetsComponent  implements OnInit {
  paramSearch: any = {
    pageSize: global?.userConfig?.pageSize,
    pageNumber: 0,
    rsId: '',
    scope: Scopes.VIEW,
    campaignId: '',
    type: 'ASSIGN',
    campaignRm: true,
    customerCode: '',
    customerName: '',
    customerType: '',
    result: '',
    status: '',
  };
  showBtnImportCus = false;
  currUser:any;

  @ViewChild('table') table: DatatableComponent;
  @HostBinding('class.app__right-content') appRightContent = true;
  @ViewChild('customers') customers: any;
  @Input() campaignId: string;
  @Input() campaignName: string;
  @Input() isSlide: boolean;
  @Input() rsId: string;
  @Input() endDate: Date;
  // @Input() showBtnAddCampaign: boolean;
  @Input() listBranch: any;
  @Input() productCode: any;
  @Input() activityResult: string;
  @Input() stage: string;
  showBtnAddCampaign: boolean;
  expiredMessage: string;
  listProduct = [];
  activityResults: Array<any> = [];
  activityResultsParent: Array<any> = [];
  models: Array<any>;
  pageable: Pageable;
  customerTypes: Array<any> = [];
  campaign: any;
  isLoading: boolean;
  fields: any;
  notificationMessage: any;
  activities: Array<any> = [];
  activity_types: Array<any> = [];
  messages: any;
  isExpired: boolean;
  listStage: Array<any> = [];
  listLanes = [];
  lstBranchUser = [];
  itemCampaign: any;
  prop: any;
  maxExportExcel = defaultExportExcel;

  prevParams: any = {...this.paramSearch};
  rowMenu: MenuItem[];
  isClickEvent = false;
  itemSelected: any;

  isShowChat = false;
  objFunctionMBChat: AppFunction;
  objFunctionCampaignRM: AppFunction;
  limit = global.userConfig.pageSize;

  constructor(
    private service: CampaignsService,
    private modalService: NgbModal,
    private translate: TranslateService,
    private commonService: CommonCategoryService,
    private router: Router,
    private workflowService: WorkflowService,
    private productService: ProductService,
    private sessionService: SessionService,
    private fileService: FileService,
    private messageService: NotifyMessageService,
    private campaignService: CampaignsService,
    private mbeeChatService: MbeeChatService,
    private leadService: LeadService,
    private categoryService: CategoryService,
  ) {
    this.objFunctionMBChat = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.APP_CHAT}`);
    this.objFunctionCampaignRM = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CAMPAIGN_RM}`);
    this.messages = global.messageTable;
    this.isLoading = true;
  }
  ngOnInit(): void {
    if (this.activityResult) {
      this.paramSearch.result = this.activityResult;
    }
    if(this.stage){
      this.paramSearch.status = this.stage;
    }
    this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
    this.isShowChat = this.objFunctionMBChat?.scopes.includes('VIEW');
    this.paramSearch.rsId = this.rsId;
    this.paramSearch.campaignId = this.campaignId;
    this.maxExportExcel = +(this.sessionService.getSessionData(SessionKey.LIMIT_EXPORT) || defaultExportExcel);
    this.prop = this.sessionService.getSessionData(SessionKey.CUSTOMER_TARGETS);
    if (!this.prop) {
      forkJoin([
        this.translate.get(['campaign', 'fields', 'notificationMessage']).pipe(catchError((e) => of(undefined))),
        this.commonService
          .getCommonCategory(CommonCategory.CONFIG_ACTIVITY_RESULT)
          .pipe(catchError((e) => of(undefined))),
        this.productService.findAll().pipe(catchError((e) => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.ACTIVITY_NEXT_TYPE).pipe(catchError((e) => of(undefined))),
        this.commonService
          .getCommonCategory(CommonCategory.CONFIG_TYPE_ACTIVITY)
          .pipe(catchError((e) => of(undefined))),
        this.workflowService.findAllProcess().pipe(catchError((e) => of(undefined))),
        this.service.getById(this.campaignId).pipe(catchError((e) => of(undefined))),
        this.categoryService
          .getBranchesOfUser(this.objFunctionCampaignRM?.rsId, Scopes.VIEW)
      ]).subscribe(
        ([translateMessage, activityResults, listProduct, activities, types, listLanes, itemCampaign, lstBranchUser]) => {
          this.prop = {
            translateMessage,
            activityResults,
            listProduct,
            activities,
            types,
            listLanes,
            itemCampaign,
            lstBranchUser,
          };
          // if(itemCampaign.status === 'ACTIVATED'
          //   && itemCampaign.blockCode === 'INDIV' && new Date((new Date()).setHours(0, 0, 0, 0)) <= new Date(itemCampaign.endDate)
          //   && itemCampaign.createdBy === this.currUser?.username){
          //   this.showBtnImportCus = true;
          // }
          this.checkShowBtnImportCus(itemCampaign);
          this.showBtnAddCampaign = this.showBtnImportCus;
          this.sessionService.setSessionData(SessionKey.CUSTOMER_TARGETS, this.prop);
          this.handleMapDataInit(true);
          this.lstBranchUser = lstBranchUser.map(i => i.code);
        }
      );
    } else {
      this.service
        .getById(this.campaignId)
        .pipe(catchError((e) => of(undefined)))
        .subscribe((itemCampaign) => {
          this.prop.itemCampaign = itemCampaign;
          this.paramSearch = {...this.paramSearch, ...this.prop?.prevParams};
          this.prevParams = this.prop?.prevParams;
          // if(itemCampaign.status === 'ACTIVATED'
          //   && itemCampaign.blockCode === 'INDIV' && new Date((new Date()).setHours(0, 0, 0, 0)) <= new Date(itemCampaign.endDate)
          //   && itemCampaign.createdBy === this.currUser?.username){
          //   this.showBtnImportCus = true;
          // }
          this.checkShowBtnImportCus(itemCampaign);
          this.showBtnAddCampaign = this.showBtnImportCus;
          this.handleMapDataInit(false);
        });
    }
  }
  checkShowBtnImportCus(itemCampaign){
    if(itemCampaign.status === 'ACTIVATED'
      && itemCampaign.blockCode === 'INDIV' && new Date((new Date()).setHours(0, 0, 0, 0)) <= new Date(itemCampaign.endDate)){
      if(itemCampaign.isRmInsert){
        this.showBtnImportCus = true;
      }
      else{
        if(this.currUser?.branch === itemCampaign?.createdByBranchCode){
          this.showBtnImportCus = true;
        }
      }
    }
  }

  handleMapDataInit(flag? : boolean) {
    this.campaign = _.get(this.prop, 'translateMessage.campaign');
    this.fields = _.get(this.prop, 'translateMessage.fields');
    this.notificationMessage = _.get(this.prop, 'translateMessage.notificationMessage');
    this.lstBranchUser = this.prop?.branchesOfUser;
    this.expiredMessage = _.get(this.fields, 'expired');
    this.customerTypes.push({ code: '', name: _.get(this.fields, 'all') });
    this.customerTypes.push({
      code: CustomerImportType.KH360,
      name: _.get(this.campaign, 'customerImportType.kh360'),
    });
    this.customerTypes.push({ code: CustomerImportType.TN, name: _.get(this.campaign, 'customerImportType.tn') });
    this.customerTypes = _.uniqBy(this.customerTypes, 'code');
    if (!this.prop) {
      this.paramSearch.customerType = this.customerTypes[0].code;
    }
    this.itemCampaign = _.get(this.prop, 'itemCampaign');
    this.listBranch = _.get(this.itemCampaign, 'branchCodes');
    this.isExpired = moment(_.get(this.itemCampaign, 'endDate')).endOf('day').isBefore(moment());
    this.listLanes = this.prop?.listLanes;
    this.listStage = _.chain(_.get(this.prop, 'listLanes'))
      .filter(
        (i) => i.type === ProcessType.OPPORTUNITY && i.nameProcessDefine === _.get(this.prop, 'itemCampaign.process')
      )
      .value();
    this.listStage.unshift({ status: '', title: this.fields.all });
    this.activityResults = _.get(this.prop, 'activityResults.content') || [];
    this.activityResultsParent = this.convert_data(_.get(this.prop, 'activityResults.content'));
    this.listProduct = this.prop?.listProduct || [];
    this.activityResultsParent = [{ code: '', name: _.get(this.fields, 'all') }, ...this.activityResultsParent];
    this.activities = _.get(this.prop, 'activities.content') || [];
    this.activity_types = _.get(this.prop, 'types.content') || [];
    this.reload(flag);
 //   this.reload(true);

    this.translate.get('btnAction').subscribe((btnAction) => {
      this.rowMenu = [];
      // nếu có nút chat và nút tạo hoạt động thì đẩy nút lịch sử chiến dịch vào trong rowMenu
      if (this.isShowChat && this.showBtnAddCampaign) {
        this.rowMenu.push({
          label: 'Lịch sử chiến dịch',
          command: (e) => {
            this.historyCampaign(this.itemSelected);
          },
        });
      }
      this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
      this.rowMenu.push({
        label: btnAction?.edit,
        command: (e) => {
          this.update();
        },
      });
      this.rowMenu.push({
        label: btnAction?.delete,
        command: (e) => {
          this.delete();
        },
      });
    });
  }

  convert_data(data: Array<any>) {
    return _.chain(data)
      .filter((x) => x.code === x.value)
      .value();
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.pageNumber = pageInfo.offset;
    this.reload(false);
  }

  search() {
    this.reload(true);
  }

  reload(isSearch?: boolean) {
    this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
    this.isLoading = true;
    let params: any = {};
    if (isSearch) {
      this.paramSearch.pageNumber = 0;
      params = this.paramSearch;
      Object.keys(params).forEach((key) => {
        if (typeof params[key] === 'string') {
          params[key] = _.trim(params[key]);
        }
      });
    } else {
      if (!this.prevParams) {
        return;
      }
      params = this.prevParams;
      params.pageNumber = this.paramSearch.pageNumber;
    }
    params.campaignId = this.campaignId;
    this.service.findDataCampaign(params).subscribe(
      (result) => {
        if (result) {
          this.prevParams = params;
          this.models = _.get(result, 'content') || [];
          this.models?.forEach((item) => {
            item.menu = this.getMenuItem(item);
            item.productRequiredToSale = this.listProduct?.find(
              (product) => product.idProductTree === item.productCode
            )?.description;
            item.productKeySale = this.listProduct?.find(
              (product) => product.idProductTree === item.productCodeSale
            )?.description;
            item.assignedByCampaingn = item.assignedBy;
            item.stageName = _.chain(this.listLanes)
              .find(
                (i) =>
                  i.status === item.status &&
                  i.nameProcessDefine === (item.opt ? this.itemCampaign?.process : LeadProsessName)
              )
              .get('title')
              .value();
            item.resultActivityName = _.get(
              _.find(this.activityResults, (i) => i.code === item.resultActivity),
              'name'
            );
            item.featureActivityTypeName = _.get(
              _.find(this.activities, (i) => i.code === item.featureActivityType),
              'name'
            );
          });
          this.pageable = {
            totalElements: _.get(result, 'totalElements'),
            totalPages: _.get(result, 'totalPages'),
            currentPage: _.get(result, 'number'),
            size: _.get(result, 'size'),
          };
          this.prop.prevParams = this.prevParams;
          this.sessionService.setSessionData(SessionKey.CUSTOMER_TARGETS, this.prop);
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  onActive(event) {
    if (event.type === 'click') {
      this.itemSelected = event.row;
    }
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      this.router.navigateByUrl(
        `${functionUri.campaign_rm}/customer/${this.itemCampaign?.id}/${item?.type?.toLowerCase()}/${
          item?.customerCode
        }`
      );
    }
  }

  getCustomerType(row, key) {
    let value = _.get(row, key);
    if (value === 'TN' || value === 'tn') {
      return _.get(this.campaign, 'customerImportType.tn');
    } else {
      return value;
    }
  }

  getMenuItem(row) {
    row.showMenu = false;
    this.translate.get('btnAction').subscribe((btnAction) => {
      this.rowMenu = [];
      if ((row.createdBy === this.currUser.username)) {
        row.showMenu = true;
        this.rowMenu.push({
          label: btnAction?.edit,
          command: (e) => {
            this.update();
          },
        });
        this.rowMenu.push({
          label: btnAction?.delete,
          command: (e) => {
            this.delete();
          },
        });
      }
      // nếu có nút chat và nút tạo hoạt động thì đẩy nút lịch sử chiến dịch vào trong rowMenu
      if ((this.isShowChat && row.type !== 'TN') || row.showMenu === true) {
        row.showMenu = true;
        this.rowMenu.unshift({
          label: 'Lịch sử chiến dịch',
          command: (e) => {
            this.historyCampaign(this.itemSelected);
          },
        });
      }
      // this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);

      if (this.isShowChat && row.type !== 'TN' && !this.isExpired) {
        row.showMenu = true;
        this.rowMenu.push({
          label: 'Tạo hoạt động',
          command: (e) => {
            this.createActivity(row);
          },
        });
      }
    });
    return this.rowMenu;
  }

  createActivity(row: any) {
    const parent: any = {};
    parent.parentType = TaskType.LEAD;
    parent.parentId = _.get(row, 'customerCode');
    parent.parentName = _.get(row, 'customerName');
    parent.campaignId = this.campaignId;
    parent.campaignName = this.campaignName;
    parent.rsId = this.rsId;
    parent.productCode = this.productCode;
    const modal = this.modalService.open(ActivityActionComponent, {
      windowClass: 'create-activity-modal',
      scrollable: true,
    });
    modal.componentInstance.parent = parent;
    modal.componentInstance.type = ScreenType.Create;
    modal.componentInstance.isShowFuture = true;
  }

  historyCampaign(row: any) {
    const parent: any = {};
    parent.parentId = _.get(row, 'customerCode');
    parent.parentName = _.get(row, 'customerName');
    parent.rsId = this.rsId;
    const modal = this.modalService.open(CustomerCampaignModalComponent, {
      windowClass: 'create-activity-modal',
      scrollable: true,
    });
    modal.componentInstance.parent = parent;
  }

  getValue(row, key) {
    return _.get(row, key);
  }

  getActivityTypeName(row, key) {
    return _.get(
      _.chain(this.activity_types)
        .filter((x) => x.code == _.get(row, key))
        .first()
        .value(),
      'name'
    );
  }

  checkRemainDayRmAccost(row) {
    // return row.opt && row.status !== StatusLead.NEW;
    return row.result || (row.status && row.status !== StatusLead.NEW);
  }

  checkExpired() {
    return this.isExpired;
  }

  generateClass(value) {
    if (this.isExpired) return 'text-center text-error font-weight-bold';
    if (value < 1) {
      return 'text-center text-error font-weight-bold';
    } else if (value === 1) {
      return 'text-center text-warring font-weight-bold';
    } else {
      return 'text-center ';
    }
  }

  exportExcel() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+(this.pageable?.totalElements || 0) === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    const params = { ...this.prevParams };
    params.campaignRm = true;
    this.isLoading = true;
    this.service.createFileExcel(params).subscribe(
      (fileId) => {
        if (fileId) {
          this.fileService.downloadFile(fileId, 'danh_sach_khach_hang_muc_tieu.xlsx').subscribe(
            (res) => {
              this.isLoading = false;
              if (!res) {
                this.messageService.error(this.notificationMessage.error);
              }
            },
            () => {
              this.isLoading = false;
            }
          );
        }
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }
  addCustomer() {
    this.router.navigate(['/campaigns/campaign-rm/', 'add-customer'],{
      state: this.prevParams,
      queryParams: {
        campaignName: this.campaignName,
        listBranch: this.listBranch,
        campaignId: this.prop.itemCampaign.id,
        typeId: this.prop.itemCampaign.typeId,
      },
    });
  }

  onShowMenu(e) {
    this.isClickEvent = true;
  }

  onHideMenu(e) {
    this.isClickEvent = false;
  }

  update() {
    this.router.navigate(['/campaigns/campaign-rm','add-customer'], {
      skipLocationChange: true,
      queryParams: {
        campaignName: this.campaignName,
        leadId: this.itemSelected.id,
        assignedUserId: this.itemSelected.assignedUserId ? this.itemSelected.assignedUserId : '',
        campaignId: this.campaignId,
        isScreenUpdate: ScreenType.Update,
        typeId: this.prop.itemCampaign.typeId,
      }});
  }

  delete() {
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.componentInstance.message = 'Xóa khách hàng này khỏi chiến dịch?';
    confirm.result
      .then((res) => {
        if (res) {
          const params = {
            leadId: this.itemSelected.id,
            rsId: this.rsId,
            scope: 'VIEW'
          }
          this.isLoading = true;
          this.campaignService.deleteLeadFromCampain(params).subscribe(value => {
            if (value) {
              this.messageService.success(this.notificationMessage.success);
              this.isLoading = false;
              this.search();
            }
          }, error => {
            this.messageService.error(JSON.parse(error.error)?.messages.vn);
            this.isLoading = false;
          });
        }
      })
      .catch(() => {});
  }


  handleChangePageSize(event) {
    this.pageable.currentPage = 0;
    this.pageable.size = event;
    this.limit = event;
    this.paramSearch.pageSize = event;
    this.paramSearch.pageNumber = 0;
    this.reload(true);
  }

  createGroupChat11(row) {
    this.isLoading = true;
    const customerCode = row.customerCode;
    this.mbeeChatService.createGroupChat(customerCode).subscribe(value => {
      if (value.data) {
        createGroupChat11(value.data)
      } else {
        this.messageService.error(value.message);
      }
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
      this.messageService.error(error.error?.description);
    });

  }
  importCustomer(){
    const data = {
      functionCode: FunctionCode.CAMPAIGN_RM,
      id: this.paramSearch.campaignId
    }
    this.router.navigate(['campaigns/campaign-management/import-customer'],{
      state: data
    });
  }
  changeStage(row){
    const form = this.modalService.open(CampaignChangeStageComponent, {
      windowClass: 'campaign-change-stage'
    });
    form.componentInstance.customer = row;
    form.componentInstance.itemCampaign = this.itemCampaign;
    form.result.then((res) => {
      if(res.customer){
        this.search();
        if (res.modalRef) {
          res.modalRef.result
            .then((confirmed: boolean) => {
              if (confirmed) {
                this.createActivity(row);
              }
              // debugger
              // const data = {
              //   customer: this.customer
              // }
              // this.modalActive.close(data);
            })
            .catch(() => {
            });
        }
      }
    }).catch(() => {});
  }

  checkViewStatus(value){
    if(value === 'WON'){
      return 'text-success';
    }
    if(value === 'LOST'){
      return 'text-error';
    }
    return '';
  }
}
