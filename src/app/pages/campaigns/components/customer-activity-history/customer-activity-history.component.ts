import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, Injector, Input} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import _ from 'lodash';

@Component({
  selector: 'app-customer-activity-history',
  templateUrl: './customer-activity-history.component.html',
  styleUrls: ['./customer-activity-history.component.scss'],
})
export class CustomerActivityHistoryComponent extends BaseComponent implements OnInit {
  constructor(injector: Injector, private modalActive: NgbActiveModal) {
    super(injector);
  }
  @Input() parent: any;
  @Input() isActionHeader: boolean;
  @Input() data: any;
  @Input() id: string;
  rsId = '';
  campaignId = '';
  customerId = '';
  isReload = false;
  model: any;
  disabled = false;
  titleHeader: string;

  ngOnInit(): void {
    this.rsId = _.get(this.parent, 'rsId');
    this.campaignId = _.get(this.parent, 'campaignId');
    this.customerId = _.get(this.parent, 'parentId');
    this.titleHeader = 'title.detailActivityCustomer';
  }

  closeModal() {
    this.modalActive.close(false);
  }
}
