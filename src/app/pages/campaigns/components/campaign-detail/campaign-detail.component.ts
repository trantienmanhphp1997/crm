import { DatePipe } from '@angular/common';
import { cleanDataForm } from 'src/app/core/utils/function';
import { forkJoin, of } from 'rxjs';
import {Component, OnInit, Injector, ViewChild, TemplateRef, AfterViewInit} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { CampaignsService } from '../../services/campaigns.service';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import {
  CampaignStatus,
  CommonCategory,
  FunctionCode,
  maxInt32,
  Scopes,
  CampaignSize,
  CustomerImportType,
  TaskType,
  ScreenType,
  LeadProsessName,
  StatusLead,
  functionUri, ProcessType, SessionKey, Division,
} from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import * as moment from 'moment';
import { TableColumn } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import _ from 'lodash';
import { CustomerActivityHistoryComponent } from '../customer-activity-history/customer-activity-history.component';
import { CustomerCampaignModalComponent } from '../customer-campaign-modal/customer-campaign-modal.component';
import { WorkflowService } from 'src/app/core/services/workflow.service';
import { ProductService } from 'src/app/core/services/product.service';
import { FileService } from 'src/app/core/services/file.service';
import { Utils } from 'src/app/core/utils/utils';
import {MenuItem} from 'primeng/api';
import {ConfirmDialogComponent} from '../../../../shared/components';
import * as queryString from 'querystring';
import {RmApi} from "../../../rm/apis";
import {AppFunction} from "../../../../core/interfaces/app-function.interface";
import {RmModalComponent} from "../../../rm/components";

@Component({
  selector: 'app-campaign-detail',
  templateUrl: './campaign-detail.component.html',
  styleUrls: ['./campaign-detail.component.scss'],
  providers: [DatePipe],
})
export class CampaignDetailComponent extends BaseComponent implements OnInit , AfterViewInit{
  @ViewChild('orderTmpl', { static: true }) orderTmpl: TemplateRef<any>;
  @ViewChild('cellTmpl', { static: true }) cellTmpl: TemplateRef<any>;
  @ViewChild('buttonsTemplate', { static: true }) buttonsTemplate: TemplateRef<any>;
  data: any;
  isOutreachProcess: boolean;
  isRmInsert: boolean;
  isRegion: boolean;
  showAction: boolean;
  campaignTrans: any;

  columns: TableColumn[];
  listData = [];
  pageable: Pageable;
  listBranch = [];
  listBranchCode = [];
  listCustomerType = [];
  listStatusAssign = [];
  listProduct = [];
  activityResults = [];
  lstBranchUser = [];
  formSearch = this.fb.group({
    customerCode: '',
    customerType: '',
    branchCode: '',
    isAssign: 'true',
    page: 0,
    size: global?.userConfig?.pageSize,
    campaignId: '',
    scope: Scopes.VIEW,
    rsId: '',
    result: '',
    status: ''
  });
  prevParams: any;
  expiredMessage: string;
  region_name: string;
  bizLine_name: string;
  showBtnAddCampaign: boolean;
  showBtnImportCus = false;
  activityResultsParent: Array<any> = [];
  listStage: Array<any> = [];
  isClickEvent = false;
  rowMenu: MenuItem[];
  itemSelected: any;
  limit = global.userConfig.pageSize;
  objFunctionRM360: AppFunction;
  isRm = false;
  maxRm = 50;
  hrsCode: string;
  rmCode: string;
  branchCode: string;
  activityResult: '';
  stage: '';
  // paramSearchRm = {
  //   page: 0,
  //   size: maxInt32,
  //   crmIsActive: true,
  //   scope: Scopes.VIEW,
  //   rsId: '',
  //   employeeCode: '',
  //   employeeId: '',
  //   fullName: '',
  //   email: '',
  //   mobile: '',
  //   rmBlock: 'INDIV',
  //   productGroup: '',
  //   divisionCode: '',
  //   branchCodes: [],
  // }
  employeeCode = [];
  employeeCodeParams: Array<string>;
  termData = [];
  employeeId = [];
  branchSearch = [];
  constructor(
    injector: Injector,
    private campaignService: CampaignsService,
    private categoryService: CategoryService,
    private workflowService: WorkflowService,
    private datePipe: DatePipe,
    private productService: ProductService,
    private fileService: FileService,
    private api: RmApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CAMPAIGN}`);
    this.objFunctionCampaignRM = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CAMPAIGN_RM}`);
    this.objFunctionRM360 = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.expiredMessage = _.get(this.fields, 'expired');
    this.showBtnAddCampaign = this.route.snapshot.queryParams.showBtnAddCampaign === 'true';
    this.listBranch = this.route.snapshot.queryParams.listBranch;
    this.hrsCode = this.route.snapshot.queryParams.hrsCode;
    this.rmCode = this.route.snapshot.queryParams.rmCode;
    this.branchCode = this.route.snapshot.queryParams.branchCode;
    this.activityResult = this.route.snapshot.queryParams.activityResult;
    this.stage = this.route.snapshot.queryParams.stage;
  }

  ngOnInit(): void {

    this.employeeCode = _.get(this.fields, 'all');
    this.employeeId = _.get(this.fields,'all');

    this.translate.get('btnAction').subscribe((btnAction) => {
      this.rowMenu = [];
        this.rowMenu.push({
          label: btnAction?.edit,
          command: (e) => {
            this.update();
          },
        });
        this.rowMenu.push({
          label: btnAction?.delete,
          command: (e) => {
            this.delete();
          },
        });
    });
    this.columns = [
      {
        name: this.fields.order,
        cellTemplate: this.orderTmpl,
        sortable: false,
        width: 40,
        draggable: false,
        headerClass: 'text-center',
      },
      {
        name: this.fields.customerType,
        prop: 'customerType',
        sortable: false,
        draggable: false,
      },
      {
        name: this.fields.customerCode,
        prop: 'customerCode',
        sortable: false,
        draggable: false,
      },
      {
        name: this.fields.customerName,
        prop: 'customerName',
        sortable: false,
        draggable: false,
      },
      {
        name: this.fields.phone,
        prop: 'phone',
        sortable: false,
        draggable: false,
      },
      {
        name: this.fields.email,
        prop: 'email',
        sortable: false,
        draggable: false,
      },
      {
        name: this.fields.branchOpenCode,
        prop: 'customerBranchCode',
        sortable: false,
        draggable: false,
      },
      {
        name: this.fields.productRequiredToSale,
        prop: 'productRequiredToSale',
        sortable: false,
        draggable: false,
      },
      {
        name: this.fields.productKeySale,
        prop: 'productKeySale',
        sortable: false,
        draggable: false,
      },
      {
        name: this.fields.campaignImplementationRm,
        prop: 'rmPerform',
        sortable: false,
        draggable: false,
      },
      {
        name: this.fields.stage,
        prop: 'stage',
        sortable: false,
        draggable: false,
      },
      {
        name: this.fields.result,
        prop: 'resultActivityName',
        sortable: false,
        draggable: false,
      },
      {
        name: this.fields.note,
        prop: 'note',
        sortable: false,
        draggable: false,
      },
      {
        name: this.fields.dateHOAssign,
        prop: 'dateHOAssign',
        sortable: false,
        draggable: false,
      },
      {
        name: this.fields.daysRemainingAssignRm,
        prop: 'remainDayAssign',
        cellTemplate: this.cellTmpl,
        sortable: false,
        draggable: false,
      },
      {
        name: this.fields.dateCBQLAssignRm,
        prop: 'dateCBQLAssignRm',
        sortable: false,
        draggable: false,
      },
      {
        name: this.fields.daysRemainingRmApproachCustomer,
        prop: 'remainDayRmAccost',
        cellTemplate: this.cellTmpl,
        sortable: false,
        draggable: false,
      },
      {
        name: this.fields.assignedByCampaingn,
        prop: 'assignedByCampaingn',
        sortable: false,
        draggable: false,
      },
      {
        name: this.fields.cmnd_passport,
        prop: 'customerIdCard',
        sortable: false,
        draggable: false,
      },
      {
        name: this.fields.campaignImplementationBranch,
        prop: 'branchPerform',
        sortable: false,
        draggable: false,
      },
      {
        name: 'Giá trị',
        prop: 'value',
        sortable: false,
        draggable: false,
      },
      {
        name: 'Thông tin bổ sung 1',
        prop: 'additinalInfor1',
        sortable: false,
        draggable: false,
      },
      {
        name: 'Thông tin bổ sung 2',
        prop: 'additinalInfor2',
        sortable: false,
        draggable: false,
      },
      {
        name: 'Thông tin bổ sung 3',
        prop: 'additinalInfor3',
        sortable: false,
        draggable: false,
      },
      {
        name: 'Thông tin bổ sung 4',
        prop: 'additinalInfor4',
        sortable: false,
        draggable: false,
      },
      {
        name: 'Thông tin bổ sung 5',
        prop: 'additinalInfor5',
        sortable: false,
        draggable: false,
      },
      {
        name: this.fields.action,
        prop: 'action',
        cellTemplate: this.buttonsTemplate,
        sortable: false,
        draggable: false,
        frozenRight: true,
        width: 130,
        resizeable: false,
      },
    ];

    const id = this.route.snapshot.paramMap.get('id');
    this.formSearch.controls.campaignId.setValue(id);
    this.formSearch.controls.rsId.setValue(this.objFunction?.rsId);
    this.isLoading = true;
    this.prop = this.sessionService.getSessionData(`${FunctionCode.CAMPAIGN}_${ScreenType.Detail}`);
    if (!this.prop) {
      forkJoin([
        this.commonService
          .getCommonCategory(CommonCategory.EXT_CAMPAIGNS_CHANNELS)
          .pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.CAMPAIGN_TYPE).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.CAMPAIGN_FORM).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.SOURCE_DATA).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.CAMPAIGN_DIVISION).pipe(catchError(() => of(undefined))),
        this.productService.findAll().pipe(catchError(() => of(undefined))),
        this.categoryService.getBlocksCategoryByRm().pipe(catchError(() => of(undefined))),
        this.categoryService.searchBranches({ page: 0, size: maxInt32 }).pipe(catchError(() => of(undefined))),
        this.translate.get('campaign').pipe(catchError(() => of(undefined))),
        this.campaignService.getById(id).pipe(catchError(() => of(undefined))),
        this.categoryService
          .getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW)
          .pipe(catchError(() => of(undefined))),
        this.categoryService.getRegion().pipe(catchError(() => of(undefined))),
        this.workflowService.findAllProcess().pipe(catchError(() => of(undefined))),
        this.commonService
          .getCommonCategory(CommonCategory.CONFIG_ACTIVITY_RESULT)
          .pipe(catchError((e) => of(undefined))),
        this.categoryService
          .getCommonCategory(CommonCategory.REPORTS_MAX_LENGTH_RM)
          .pipe(catchError(() => of(undefined))),
      ]).subscribe(
        ([
          listSaleChannel,
          listPurpose,
          listFormality,
          listSourceData,
          listBizLine,
          listProduct,
          listBlock,
          listBranch,
          campaign,
          itemCampaign,
          branchesOfUser,
          listRegion,
          listLanes,
          activityResults, configReport
        ]) => {
          this.isRm = _.isEmpty(branchesOfUser);
          this.maxRm =
            _.find(_.get(configReport, 'content'), (item) => item.code === CommonCategory.MAX_RM_SEARCH_DETAIL_CAMPAIGN)?.value ||
            0;
          this.prop = {
            listSaleChannel,
            listPurpose,
            listFormality,
            listSourceData,
            listBizLine,
            listProduct,
            listBlock,
            listBranch,
            campaign,
            itemCampaign,
            branchesOfUser,
            listRegion,
            listLanes,
            activityResults,
            isRM: _.isEmpty(branchesOfUser),
            maxRm: this.maxRm
          };
          // if(itemCampaign.status === 'ACTIVATED'
          //   && itemCampaign.blockCode === 'INDIV' && new Date((new Date()).setHours(0, 0, 0, 0)) <= new Date(itemCampaign.endDate)
          //   && itemCampaign.createdBy === this.currUser?.username){
          //   this.showBtnImportCus = true;
          // }
          this.checkShowBtnImportCus(itemCampaign);
          this.showBtnAddCampaign = this.showBtnImportCus;
          this.sessionService.setSessionData(`${FunctionCode.CAMPAIGN}_${ScreenType.Detail}`, this.prop);
          this.lstBranchUser = branchesOfUser.map(i => i.code);
          this.handleMapData(true);

        }
      );
    } else {
      this.campaignService.getById(id).subscribe((itemCampaign) => {
        this.prop.itemCampaign = itemCampaign;
        if (this.prop?.prevParams) {
          this.formSearch.patchValue(this.prop?.prevParams);
        }
        // if(itemCampaign.status === 'ACTIVATED'
        //   && itemCampaign.blockCode === 'INDIV' && new Date((new Date()).setHours(0, 0, 0, 0)) <= new Date(itemCampaign.endDate)
        //   && itemCampaign.createdBy === this.currUser?.username){
        //   this.showBtnImportCus = true;
        // }
        this.checkShowBtnImportCus(itemCampaign);
        this.showBtnAddCampaign = this.showBtnImportCus;
        this.formSearch.controls.campaignId.setValue(id);
        this.prevParams = this.prop?.prevParams;
        this.isRm = this.prop?.isRM;
        this.handleMapData(false);
      });
    }
  }

  private setRedirectParams() {
    console.log('nhay vao day');
    if (this.hrsCode) {
    //  this.formSearch.controls.hrsCodelst.patchValue([this.hrsCode]);
      this.employeeCode = [this.hrsCode];
      this.employeeId = [this.rmCode];
      console.log('em: ', this.employeeCode);
    }

    if (this.branchCode) {
      this.formSearch.controls.branchCode.patchValue(this.branchCode);
    }

    if (this.activityResult) {
      this.formSearch.controls.result.patchValue(this.activityResult);
    }

    if(this.stage){
      this.formSearch.controls.status.patchValue(this.stage);
    }
    this.search(true);
  }

  ngAfterViewInit(): void {
    this.formSearch.controls.branchCode.valueChanges.subscribe((value) => {
        if(value === ''){
          this.branchSearch = [];
        }
        else{
          this.branchSearch = [value];
        }
        this.removeRMSelected();
        if(this.hrsCode){
          this.employeeCode = [this.hrsCode];
          this.employeeId = [this.rmCode];
          this.hrsCode = null;
        }
    });
    this.formSearch.controls.isAssign.valueChanges.subscribe((value) => {
      this.removeRMSelected();
    })
  }
  checkShowBtnImportCus(itemCampaign){
    if(itemCampaign.status === 'ACTIVATED'
      && itemCampaign.blockCode === 'INDIV' && new Date((new Date()).setHours(0, 0, 0, 0)) <= new Date(itemCampaign.endDate)){
      if(itemCampaign.isRmInsert){
        this.showBtnImportCus = true;
      }
      else{
        if(this.currUser?.branch === itemCampaign?.createdByBranchCode){
          this.showBtnImportCus = true;
        }
      }
    }
  }
  handleMapData(flag? : boolean) {
    if (this.prop?.itemCampaign) {
      this.maxRm = this.prop?.maxRm;
      this.data = this.prop?.itemCampaign;
      this.lstBranchUser = this.prop?.branchesOfUser.map(i => i.code);
      this.isOutreachProcess = this.data?.isOutreachProcess;
      this.isRmInsert = this.data?.isRmInsert;
      this.isRegion = this.data?.scope === 'CHI_NHANH';

      this.data.content = Utils.isStringEmpty(this.data.content) ? '---' : _.get(this.data, 'content');

      // this.region_name = _.get(this.data, 'region').join(', ');
      this.showAction =
        this.data?.status === CampaignStatus.InProgress && moment(this.data.endDate).isAfter(moment().endOf('day'));
      this.data.typeName = this.prop?.listPurpose?.content?.find(
        (itemType) => this.data.typeId === itemType.code
      )?.name;
      this.data.formName = this.prop?.listFormality?.content?.find(
        (itemForm) => this.data.formId === itemForm.code
      )?.name;
      this.listProduct = this.prop?.listProduct || [];
      const product = this.prop?.listProduct?.find(
        (itemProduct) => this.data.productCode === itemProduct.idProductTree?.toString()
      );
      this.data.productName = product?.idProductTree + ' - ' + product?.description;
      this.data.blockName = this.prop?.listBlock?.find((itemBlock) => this.data.blockCode === itemBlock.code)?.name;
      this.data.channelName =
        this.prop?.listSaleChannel?.content?.find((itemChannel) => this.data.channel === itemChannel.code)?.name ||
        this.fields.all;
      this.data.status = this.prop?.campaign?.status[this.data?.status?.toLowerCase()];
      this.listStatusAssign.push({ code: 'true', name: this.prop?.campaign?.statusAssign?.assigned });
      this.listStatusAssign.push({ code: 'false', name: this.prop?.campaign?.statusAssign?.yetAssign });
      this.campaignTrans = this.prop?.campaign;
      this.data.sourceName = this.prop?.listSourceData?.content?.find(
        (itemSource) => this.data.sourceId === itemSource.code
      )?.name;
      this.data.bizLine = this.prop?.listBizLine?.content
        ?.filter((itemBizLine) => this.data?.bizLine?.includes(itemBizLine.code))
        ?.map((i) => i.name);
      this.bizLine_name = _.join(_.get(this.data, 'bizLine'), ', ');
      this.data.region = this.prop?.listRegion
        ?.filter((item) => this.data?.areaCode?.includes(item.locationCode))
        .map((i) => i.locationCode);
      this.region_name = _.join(_.get(this.data, 'region'), ', ');
      this.data.branch = this.prop?.listBranch?.content
        ?.filter((itemBranch) => this.data?.branchCodes?.includes(itemBranch.code))
        ?.map((i) => i.code + ' - ' + i.name);
      if (this.data.scope === CampaignSize.TOAN_HANG) {
        this.listBranch = this.prop?.branchesOfUser?.map((item) => {
          return { code: item.code, name: item.code + ' - ' + item.name };
        });
        this.listBranchCode = [];
      } else {
        this.listBranch = this.prop?.branchesOfUser
          ?.filter((branch) => this.data?.branchCodes?.indexOf(branch.code) > -1)
          ?.map((item) => {
            return { code: item.code, name: item.code + ' - ' + item.name };
          });
        this.listBranchCode = this.listBranch.map((item) =>  {
          return item?.code;
        })
      }

      this.listBranch.unshift({ code: '', name: this.fields.all });
    }
    this.listCustomerType.push({ code: '', name: this.fields.all });
    this.listCustomerType.push({
      code: CustomerImportType.KH360,
      name: this.prop?.campaign?.customerImportType?.kh360,
    });
    this.listCustomerType.push({ code: CustomerImportType.TN, name: this.prop?.campaign?.customerImportType?.tn });
    this.activityResults = _.get(this.prop, 'activityResults.content', []);
    this.activityResultsParent = this.convert_data(_.get(this.prop, 'activityResults.content', []));
    this.listStage = _.chain(_.get(this.prop, 'listLanes'))
      .filter(
        (i) => i.type === ProcessType.OPPORTUNITY && i.nameProcessDefine === _.get(this.prop, 'itemCampaign.process')
      )
      .value();
    this.listStage.unshift({ status: '', title: this.fields.all });
    this.activityResultsParent.unshift({ code: '', name: this.fields.all });

    if(this.activityResult || this.hrsCode || this.branchCode) {
      this.setRedirectParams();
    } else {
      this.search(flag);
    }

  }

  convert_data(data: Array<any>) {
    return _.chain(data)
      .filter((x) => x.code === x.value)
      .value();
  }

  setPage(pageInfo) {
    this.formSearch.controls.page.setValue(pageInfo?.offset);
    this.search(false);
  }

  search(isSearch?: boolean) {
    this.isLoading = true;
    let params: any;
    if (isSearch) {
      this.formSearch.controls.page.setValue(0);
      params = cleanDataForm(this.formSearch);
    } else {
      if (!this.prevParams) {
        this.isLoading = false;
        return;
      }
      params = this.prevParams;
      params.page = this.formSearch.controls.page.value;
      cleanDataForm(this.formSearch);
    }
    console.log('employee: ', this.employeeCode);
    params.hrsCodelst = this.employeeCode !== this.fields.all ? this.employeeCode?.toString() : '';
    this.campaignService.searchDataCampaign(params).subscribe(
      (listData) => {
        this.isLoading = false;
        this.prevParams = params;
        this.listData = listData?.content || [];
        this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
        this.listData?.forEach((item) => {
          item.menu = this.getMenuItem(item);
          item.customerType = this.listCustomerType?.find((type) => type.code === item.type)?.name;
          item.assignedByCampaingn = item.assignedBy;
          item.productRequiredToSale = this.listProduct?.find(
            (product) => product.idProductTree === item.productCode
          )?.description;
          item.productKeySale = this.listProduct?.find(
            (product) => product.idProductTree === item.productCodeSale
          )?.description;
          item.dateHOAssign = this.datePipe.transform(item.activatedDate, 'dd/MM/yyyy');
          item.dateCBQLAssignRm = this.datePipe.transform(item.assignedDate, 'dd/MM/yyyy');
          item.stage = this.prop?.listLanes?.find(
            (i) => i.status === item.status && i.nameProcessDefine === (item.opt ? this.data?.process : LeadProsessName)
          )?.title;
          item.branchPerform = _.isEmpty(item.branchPerform) ? item.branchCode : item.branchPerform;
          item.resultActivityName = _.get(
            _.find(this.activityResults, (i) => i.code === item.resultActivity),
            'name'
          );
        });
        this.pageable = {
          totalElements: listData?.totalElements,
          totalPages: listData?.totalPages,
          currentPage: listData?.number,
          size: listData.size === 0 ? global.userConfig.pageSize : listData.size,
        };
        this.prop.prevParams = this.prevParams;
        this.sessionService.setSessionData(`${FunctionCode.CAMPAIGN}_${ScreenType.Detail}`, this.prop);
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  active() {
    if (this.data?.id) {
      this.confirmService.confirm(this.confirmMessage?.activedCampaign).then((res) => {
        if (res) {
          this.isLoading = true;
          this.campaignService.activated(this.data.id).subscribe(
            () => {
              this.isLoading = false;
              this.showAction = false;
              this.data.status = this.campaignTrans?.status?.activated;
              // if (this.data.typeId === 'PTKH' && this.data.blockCode === 'INDIV') {
              if ((this.data.isRmInsert === true || this.currUser?.branch === this.data?.createdByBranchCode) && this.data.blockCode === 'INDIV') {
                this.showBtnAddCampaign = true;
                const q = queryString.parse(location.search);
                q.showBtnAddCampaign = 'true';
                location.search = queryString.stringify(q);
              }
              // if(this.data.isActive === true
              //   && this.data.blockCode === 'INDIV' && new Date((new Date()).setHours(0, 0, 0, 0)) <= new Date(this.data.endDate)
              //   && this.data.createdBy === this.currUser?.username){
              //   this.showBtnImportCus = true;
              // }
              this.data.status = 'ACTIVATED';
              this.checkShowBtnImportCus(this.data);
              this.showBtnAddCampaign = this.showBtnImportCus;
              this.isLoading = false;
              this.search(true);
              this.messageService.success(this.notificationMessage.success);
            },
            (error: any) => {
              if (_.get(error, 'error.code') === 'err.api.errorcode.CRM133') {
                this.isLoading = false;
                this.messageService.error(_.get(error, 'error.description'));
                return;
              }
              this.isLoading = false;
              this.messageService.error(this.notificationMessage.error);
            }
          );
        }
      });
    }
  }

  clearData() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        const formData: FormData = new FormData();
        formData.append('campaignId', this.data?.id);
        this.isLoading = true;
        this.campaignService.clearDataCampaign(formData).subscribe(
          () => {
            this.isLoading = false;
            this.search(true);
            this.messageService.success(this.notificationMessage.success);
          },
          () => {
            this.isLoading = false;
            this.messageService.error(this.notificationMessage.error);
          }
        );
      }
    });
  }

  historyActivity(row: any) {
    const parent: any = {};
    parent.parentType = TaskType.LEAD;
    parent.parentId = _.get(row, 'customerCode');
    parent.parentName = _.get(row, 'customerName');
    parent.campaignId = _.get(this.data, 'id');
    parent.campaignName = _.get(this.data, 'name');
    parent.rsId = _.get(this.objFunction, 'rsId');
    const modal = this.modalService.open(CustomerActivityHistoryComponent, {
      windowClass: 'create-activity-modal',
      scrollable: true,
    });
    modal.componentInstance.parent = parent;
  }

  historyCampaign(row: any) {
    const parent: any = {};
    parent.parentId = _.get(row, 'customerCode');
    parent.parentName = _.get(row, 'customerName');
    parent.rsId = _.get(this.objFunction, 'rsId');
    const modal = this.modalService.open(CustomerCampaignModalComponent, {
      windowClass: 'create-activity-modal',
      scrollable: true,
    });
    modal.componentInstance.parent = parent;
  }

  generateClass(value) {
    if (moment(this.data.endDate).endOf('day').isBefore(moment())) return 'text-error font-weight-bold';
    if (value < 1) {
      return 'text-error font-weight-bold';
    } else if (value === 1) {
      return 'text-warring font-weight-bold';
    } else {
      return '';
    }
  }

  generateAssignClass(value) {
    if (moment(this.data.endDate).endOf('day').isBefore(moment())) return 'text-error font-weight-bold';
    if (value < 1) {
      return 'text-error font-weight-bold';
    } else if (value === 1) {
      return 'text-warring font-weight-bold';
    } else {
      return '';
    }
  }

  checkExpired() {
    return moment(this.data?.endDate).endOf('day').isBefore(moment());
  }

  checkRemainDayRmAccost(row) {
    // return row.opt && row.status !== StatusLead.NEW;
    return row.result || (row.status && row.status !== StatusLead.NEW);
  }

  checkRemainDayAssign(row) {
    return row?.dateHOAssign;
  }

  exportExcel() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+(this.pageable?.totalElements || 0) === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    const params = { ...this.prevParams };
    params.campaignRm = false;
    this.isLoading = true;
    this.campaignService.createFileExcel(params).subscribe(
      (fileId) => {
        if (fileId) {
          this.fileService.downloadFile(fileId, 'danh_sach_khach_hang_muc_tieu.xlsx').subscribe(
            (res) => {
              this.isLoading = false;
              if (!res) {
                this.messageService.error(this.notificationMessage.error);
              }
            },
            () => {
              this.isLoading = false;
            }
          );
        }
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }

  downloadFile() {
    if (this.data?.fileId && this.data?.fileName) {
      this.fileService.downloadFile(this.data.fileId, this.data.fileName).subscribe((res) => {
        if (!res) {
          this.messageService.error(this.notificationMessage.error);
        }
      });
    }
  }

  redistribute() {
    this.router.navigateByUrl(`${functionUri.campaign}/redistributed/${this.data?.id}`);
  }

  back() {
    this.sessionService.clearSessionData(`${FunctionCode.CAMPAIGN}_${ScreenType.Detail}`);
    this.router.navigateByUrl(functionUri.campaign);
  }

  addCustomer() {
    const list = [];
    if (this.data?.scope !== 'TOAN_HANG') {
      this.listBranch?.forEach(item => {
        if (item.code) {
          list.push(item.code);
        }
      });
    }
    this.router.navigate(['/campaigns/campaign-rm/', 'add-customer'],{
      state: this.prevParams,
      queryParams: {
        listBranch: list,
        campaignId: this.prop.itemCampaign.id,
        typeId: this.prop.itemCampaign.typeId,
        campaignName: this.data?.name
      },
    });
  }

  onShowMenu(e) {
    this.isClickEvent = true;
  }

  onHideMenu(e) {
    this.isClickEvent = false;
  }

  update() {
    this.router.navigate(['/campaigns/campaign-management','add-customer'], {
      skipLocationChange: true,
      queryParams: {
        leadId: this.itemSelected.id,
        assignedUserId: this.itemSelected.assignedUserId ? this.itemSelected.assignedUserId : '',
        campaignId: this.route.snapshot.paramMap.get('id'),
        isScreenUpdate: ScreenType.Update,
        typeId: this.prop.itemCampaign.typeId,
        campaignName: this.data?.name
      }});
  }

  delete() {
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.componentInstance.message = 'Xóa khách hàng này khỏi chiến dịch?';
    confirm.result
      .then((res) => {
        if (res) {
          const params = {
            leadId: this.itemSelected.id,
            rsId: this.objFunction?.rsId,
            scope: 'VIEW'
          }
          this.isLoading = true;
          this.campaignService.deleteLeadFromCampain(params).subscribe(value => {
            if (value) {
              this.messageService.success(this.notificationMessage.success);
              this.isLoading = false;
              this.search(false);
            }
          }, error => {
            this.messageService.error(JSON.parse(error.error)?.messages.vn);
            this.isLoading = false;
          });
          }
      })
      .catch(() => {});
  }

  onActive(event) {
    if (event.type === 'click') {
      this.itemSelected = event.row;
    }
  }

  handleChangePageSize(event) {
    this.pageable.currentPage = 0;
    this.pageable.size = event;
    this.limit = event;
    this.formSearch.controls.size.setValue(event);
    this.formSearch.controls.page.setValue(0);
    this.search(true);
  }
  importCustomer(){
    const data = {
      functionCode: FunctionCode.CAMPAIGN,
      id: this.route.snapshot.paramMap.get('id')
    }
    this.router.navigate(['campaigns/campaign-management/import-customer'],{
      state: data
    });
  }

  getMenuItem(row) {
    this.translate.get('btnAction').subscribe((btnAction) => {
      this.rowMenu = [];
      if ((row.createdBy === this.currUser.username) || (!_.isEmpty(this.lstBranchUser))) {
        row.showMenu = true;
        this.rowMenu.push({
          label: btnAction?.edit,
          command: (e) => {
            this.update();
          },
        });
      }
      if ((row.createdBy === this.currUser.username) || this.lstBranchUser.includes(row.createdByBranchCode)) {
        row.showMenu = true;
        this.rowMenu.push({
          label: btnAction?.delete,
          command: (e) => {
            this.delete();
          },
        });
      }
    });
    return this.rowMenu;
  }
  removeRMSelected(){
    this.employeeCode = _.get(this.fields, 'all');
    this.employeeId = _.get(this.fields,'all');
    this.employeeCodeParams = null;
    this.termData = [];
  }

  searchRM(){
    const formSearch = {
      crmIsActive: { value: 'true', disabled: true },
      isRmNotNull: true
    };

    const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
    modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
    modal.componentInstance.dataSearch = formSearch;
    modal.componentInstance.branchCodes = _.isEmpty(this.branchSearch) ? this.listBranchCode : this.branchSearch ;
    modal.componentInstance.block = 'INDIV';
    modal.result
      .then((res) => {
        if (res) {
          if(res.listSelected.length > this.maxRm){
            this.messageService.warn('Chỉ chọn tối đa ' + this.maxRm + ' RM');
            return;
          }
          this.employeeCode = []; //list Hrs code
          this.employeeId = []; //list Rm code
          this.employeeCodeParams = [];
          console.log('maxRM: ', this.maxRm);
          console.log('listSelected: ', res.listSelected);
          const listRMSelect = res.listSelected?.map((item) => {
            this.employeeCode.push(item?.hrisEmployee?.employeeId);
            this.employeeCodeParams.push(item.hrisEmployee.employeeId);
            return {
              hrsCode: item?.hrisEmployee?.employeeId,
              code: item?.t24Employee?.employeeCode,
              name: item?.hrisEmployee?.fullName,
            };
          });
          this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
          this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);

          this.translate.get('fields').subscribe((res) => {
            this.fields = res;
            const codes = this.employeeCode;
            const codeParams = this.employeeCodeParams;
            const countCode = (codes) => codes.reduce((a, b) => ({ ...a, [b]: (a[b] || 0) + 1 }), {});
            const countCodeParams = (codeParams) => codeParams.reduce((a, b) => ({ ...a, [b]: (a[b] || 0) + 1 }), {});

            this.employeeCode = Object.keys(countCode(codes));
            this.employeeCodeParams = Object.keys(countCodeParams(codeParams));
            this.employeeCode =
              _.size(this.employeeCode) === 0 ||
              (_.size(this.employeeCode) === 1 && this.employeeCode[0] === _.get(this.fields, 'all'))
                ? _.get(this.fields, 'all')
                : _.join(this.employeeCode, ', ');
          });
          if (this.employeeCode.length === 0) {
            this.removeRMSelected();
          }
          else{
            this.termData.forEach((i) => {
              if(this.employeeCode.includes(i?.hrsCode)){
                this.employeeId.push(i?.code);
              }
            });
            console.log('employeeid: ', this.employeeId);
          }
        } else {
          this.removeRMSelected();
        }
      })
      .catch(() => {});
  }


}
