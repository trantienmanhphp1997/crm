import { forkJoin, of, Observable } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, Injector, AfterViewInit, Input, OnChanges } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { ActivityType, CommonCategory, ConfigBackDate, ScreenType } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { ActivityService } from 'src/app/core/services/activity.service';
import * as moment from 'moment';
import { Utils } from 'src/app/core/utils/utils';
import _ from 'lodash';
import {SaleManagerApi} from "../../../sale-manager/api";

@Component({
  selector: 'app-activity.record',
  templateUrl: './activity.record.component.html',
  styleUrls: ['./activity.record.component.scss'],
})
export class ActivityRecordComponent extends BaseComponent implements OnInit, AfterViewInit, OnChanges {
  @Input() parent: any;
  @Input() isActionHeader: boolean;
  @Input() type = ScreenType.Update;
  @Input() data: any;
  @Input() id: string;
  dataForm: any;
  titleHeader: string;
  today = new Date();
  form = this.fb.group({
    id: [''],
    parentId: [''],
    parentName: [''],
    parentType: [''],
    activityType: ['', CustomValidators.required],
    dateStart: [this.today, CustomValidators.required],
    activityResult: ['', CustomValidators.required],
    location: [''],
    note: [''],
    isFutureActivity: [''],
    activityParentResult: [''],
    futureActivity: this.fb.group({
      id: [''],
      activityType: [''],
      dateStart: [''],
      futureActivityType: [''],
      location: [''],
      note: [''],
    }),
    productCode: ''
  });
  nextActivityMinDate = this.today;
  activityMinDate = this.today;
  activityMaxDate = new Date(moment().endOf('day').valueOf());
  listType = [];
  listNextTask = [];
  listResult = [];
  isShowLocation = false;
  isShowLocationFuture = false;
  isValidator = false;
  backHours = 0;
  rsId = '';
  campaignId = '';
  customerId = '';
  isReload = false;
  model: any;
  resultItems: Array<any>;
  listProduct: any;

  constructor(injector: Injector, private modalActive: NgbActiveModal, private activityService: ActivityService, private saleManagerApi: SaleManagerApi) {
    super(injector);
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.rsId = _.get(this.parent, 'rsId');
    this.campaignId = _.get(this.parent, 'campaignId');
    this.customerId = _.get(this.parent, 'parentId');
    this.dataForm = _.get(this.parent, 'data');
    if (!this.type || this.type === ScreenType.Create) {
      this.titleHeader = 'title.createActivity';
      this.form.controls.futureActivity.disable();
    } else {
      this.titleHeader = 'title.recordActivity';
      this.form.controls.futureActivity.disable();
    }
    forkJoin([
      this.saleManagerApi.getProductByLevel().pipe(catchError((e) => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.CONFIG_ACTIVITY_RESULT).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.CONFIG_TYPE_ACTIVITY).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.ACTIVITY_NEXT_TYPE).pipe(catchError(() => of(undefined))),
      this.commonService
        .getCommonCategory(CommonCategory.CONFIG_BACK_DATE, ConfigBackDate.BACK_DATE_ACTIVITY)
        .pipe(catchError(() => of(undefined))),
    ]).subscribe(([listProduct, listResult, listType, listNextTask, backDate]) => {
      this.listProduct = listProduct?.map((item) => {
        return { code: item.idProductTree, displayName: item.idProductTree + ' - ' + item.description };
      });
      this.resultItems = _.get(listResult, 'content');
      _.forEach(_.get(listResult, 'content'), (item) => {
        if (item.code === item.value) {
          item.children = _.chain(_.get(listResult, 'content'))
            .filter((itemChild) => itemChild.value === item.code && itemChild.code !== itemChild.value)
            .value();
          if (item.children.length === 0) {
            delete item.children;
          }
          this.listResult.push(item);
        }
      });
      this.listType = _.get(listType, 'content') || [];
      this.listNextTask = _.get(listNextTask, 'content') || [];
      this.backHours = _.get(_.chain(_.get(backDate, 'content')).first().value(), 'value') * 24;
      if (this.type === this.screenType.create) {
        this.activityMinDate = new Date(moment().hour(-this.backHours).startOf('day').valueOf());
      }
      this.dataForm.dateStart = new Date(_.get(this.dataForm, 'dateStart'));
      if (Utils.isNotNull(_.get(this.dataForm, 'futureActivity.dateStart'))) {
        this.dataForm.futureActivity.dateStart = new Date(this.dataForm.futureActivity.dateStart);
      }
      this.form.patchValue(this.dataForm);
      if (_.get(this.parent, 'productCode')) {
        this.form.controls.productCode.setValue(+ _.get(this.parent, 'productCode'));
      }
    });
    this.isLoading = false;
  }

  ngOnChanges() {}

  ngAfterViewInit() {
    this.form.controls.activityResult.valueChanges.subscribe((value) => {
      if (value === 'KHAC') {
        this.form.controls.note.setValidators(CustomValidators.required);
      } else {
        this.form.controls.note.clearValidators();
      }
      this.form.controls.note.updateValueAndValidity({ emitEvent: false });
    });
    this.form.controls.dateStart.valueChanges.subscribe((value) => {
      if (moment(value).isAfter(moment())) {
        this.form?.controls?.activityResult?.clearValidators();
      } else {
        this.form?.controls?.activityResult?.setValidators(CustomValidators.required);
      }
      this.form?.controls?.activityResult?.updateValueAndValidity({ emitEvent: false });
    });
    this.form.valueChanges.subscribe((data) => {
      this.isShowLocation = _.get(data, 'activityType') === ActivityType.Meeting;
      this.isShowLocationFuture = _.get(data, 'futureActivity.activityType') === ActivityType.Meeting;
      if (data?.isFutureActivity) {
        this.form?.get('futureActivity.activityType')?.setValidators(CustomValidators.required);
        this.form?.get('futureActivity.dateStart')?.setValidators(CustomValidators.required);
        this.form?.get('futureActivity.futureActivityType')?.setValidators(CustomValidators.required);
      } else {
        this.form?.get('futureActivity.activityType')?.clearValidators();
        this.form?.get('futureActivity.dateStart')?.clearValidators();
        this.form?.get('futureActivity.futureActivityType')?.clearValidators();
      }
      this.form?.controls?.futureActivity?.updateValueAndValidity({ emitEvent: false });
    });
    this.form.controls.activityResult.valueChanges.subscribe((value) => {
      const parentResult = _.chain(this.resultItems)
        .filter((x) => x.code === value)
        .first()
        .value();
      this.form.controls.activityParentResult.setValue(_.get(parentResult, 'value'));
    });
  }

  save() {
    this.isReload = !this.isReload;
    this.isValidator = true;
    if (this.form.valid) {
      this.isLoading = true;
      const data = cleanDataForm(this.form);
      if (this.parent) {
        data.parentId = _.get(this.parent, 'parentId');
        data.parentName = _.get(this.parent, 'parentName');
        data.parentType = _.get(this.parent, 'parentType');
        data.campaignId = _.get(this.parent, 'campaignId');
        data.activityParentId = _.get(this.parent, 'data.preId');
      }
      let api: Observable<any>;
      if (this.type === ScreenType.Create) {
        api = this.activityService.create(data);
      } else {
        data.futureActivityId = _.get(this.data, 'futureActivityId');
        api = this.activityService.record(data);
      }
      api.subscribe(
        () => {
          this.isLoading = false;
          this.communicateService.request({ name: 'refreshActivityList' });
          this.messageService.success(_.get(this.notificationMessage, 'success'));
          this.modalActive.close(true);
        },
        () => {
          this.isLoading = false;
          this.messageService.error(_.get(this.notificationMessage, 'error'));
        }
      );
    } else {
      validateAllFormFields(this.form);
    }
  }

  isCreateNextActivity(e) {
    if (_.get(e, 'checked')) {
      this.form.controls.futureActivity.enable();
    } else {
      this.form.controls.futureActivity.reset();
      this.form.controls.futureActivity.disable();
    }
  }

  closeModal() {
    this.modalActive.close(false);
  }
}
