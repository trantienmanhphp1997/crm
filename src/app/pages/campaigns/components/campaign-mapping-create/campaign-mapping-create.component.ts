import {AfterViewInit, Component, Injector, OnInit} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { CampaignsService } from '../../services/campaigns.service';
import { validateAllFormFields, cleanDataForm } from 'src/app/core/utils/function';
import {ProcessType, ProductLevel} from 'src/app/core/utils/common-constants';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { forkJoin, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import _ from 'lodash';
import { ProductService } from 'src/app/core/services/product.service';
import {WorkflowService} from '../../../../core/services/workflow.service';

@Component({
  selector: 'app-campaign-mapping-create',
  templateUrl: './campaign-mapping-create.component.html',
  styleUrls: ['./campaign-mapping-create.component.scss'],
})
export class CampaignMappingCreateComponent extends BaseComponent implements OnInit, AfterViewInit {
  listProduct = [];
  listBlock = [];
  listProcedure = [];
  listLanes = [];
  listProcess: any = [];
  isValidator = false;
  translateObj: any;
  form = this.fb.group({
    productId: [''],
    blockCode: ['', CustomValidators.required],
    process: ['', CustomValidators.required],
    approvedDate: ['', CustomValidators.required]
  });
  constructor(injector: Injector,
              private campaignService: CampaignsService,
              private productService: ProductService,
              private workflowService: WorkflowService) {
    super(injector);
  }

  ngOnInit(): void {
    this.translate.get(['fields', 'notificationMessage']).subscribe((res) => {
      this.translateObj = res;
    });
    forkJoin([
      this.productService.getProductByLevel(ProductLevel.Lvl1).pipe(catchError(() => of(undefined))),
      this.campaignService.getBlockMapping().pipe(catchError(() => of(undefined))),
      this.campaignService.getSaleProcess().pipe(catchError(() => of(undefined))),
      this.workflowService.findAllProcess().pipe(catchError((e) => of(undefined))),
    ]).subscribe(([listProduct, listBlock, listProcedure, listLanes]) => {
      this.listLanes = listLanes;
      this.listProduct =
        listProduct?.map((item) => {
          return { code: item.idProductTree, name: item.idProductTree + ' - ' + item.description };
        }) || [];
      this.listBlock =
        listBlock?.content.map((item) => {
          return { code: item.code, name: item.code + ' - ' + item.name };
        }) || [];
      this.listProcedure = listProcedure || [];
    });
  }

  ngAfterViewInit() {
    this.form.controls.process.valueChanges.subscribe(value => {
      this.listProcess = _.filter(this.listLanes, (i) => i.type === ProcessType.OPPORTUNITY && i.nameProcessDefine === value);
    });
  }

  confirmDialog() {
    if (this.form.valid) {
      const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result.then((res) => {
        if (res) {
          this.isLoading = true;
          const data = cleanDataForm(this.form);
          this.campaignService.createMappingProduct(data).subscribe(
            () => {
              this.isLoading = false;
              this.location.back();
              this.messageService.success(this.notificationMessage.success);
            },
            (e) => {
              this.isLoading = false;
              if (e?.status === 0) {
                this.messageService.error(this.notificationMessage.E001);
                return;
              }
              const error = JSON.parse(_.get(e, 'error'));
              if (e && e.error && error) {
                this.confirmService.warn(_.get(error, 'description'));
              } else {
                this.messageService.error(this.notificationMessage.error);
              }
            }
          );
        }
      });
    } else {
      this.isValidator = true;
      validateAllFormFields(this.form);
    }
  }

  back() {
    this.location.back();
  }
}
