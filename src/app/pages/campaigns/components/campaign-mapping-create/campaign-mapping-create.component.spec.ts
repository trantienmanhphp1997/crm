import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignMappingCreateComponent } from './campaign-mapping-create.component';

describe('CampaignMappingCreateComponent', () => {
  let component: CampaignMappingCreateComponent;
  let fixture: ComponentFixture<CampaignMappingCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignMappingCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignMappingCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
