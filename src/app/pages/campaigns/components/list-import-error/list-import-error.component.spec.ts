import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListImportErrorComponent } from './list-import-error.component';

describe('ListImportErrorComponent', () => {
  let component: ListImportErrorComponent;
  let fixture: ComponentFixture<ListImportErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListImportErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListImportErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
