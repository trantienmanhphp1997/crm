import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, Injector, HostBinding, Input, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { global } from '@angular/compiler/src/util';
import { CampaignsService } from '../../services/campaigns.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-list-import-error',
  templateUrl: './list-import-error.component.html',
  styleUrls: ['./list-import-error.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ListImportErrorComponent extends BaseComponent implements OnInit {
  @HostBinding('class.list__error-content') listContent = true;
  @Input() fileId: string;
  @Input() campaignId: string;
  listDataError = [];
  pageError: Pageable;
  params = {
    page: 0,
    size: global?.userConfig?.pageSize,
    fileId: '',
    campaignId: '',
  };

  constructor(injector: Injector, private modalActive: NgbActiveModal, private campaignService: CampaignsService) {
    super(injector);
  }

  ngOnInit(): void {}

  setPage(pageInfo) {
    this.params.page = pageInfo?.offset;
    this.params.fileId = this.fileId;
    this.params.campaignId = this.campaignId;
    this.isLoading = true;
    this.campaignService
      .searchImportError(this.params)
      .pipe(catchError((e) => of(undefined)))
      .subscribe((listError) => {
        this.listDataError = listError?.content || [];
        this.pageError = {
          totalElements: listError.totalElements,
          totalPages: listError.totalPages,
          currentPage: listError.number,
          size: this.params.size,
        };
        this.isLoading = false;
      });
  }

  closeModal() {
    this.modalActive.close();
  }
}
