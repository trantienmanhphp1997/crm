import { Component, OnInit, OnDestroy, AfterViewInit, Injector, ViewEncapsulation } from '@angular/core';
import { CampaignsService } from '../../services/campaigns.service';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { forkJoin, of } from 'rxjs';
import {
  BRANCH_HO,
  CampaignProcessType,
  CampaignSize,
  CommonCategory, CustomerType,
  FunctionCode,
  ProductLevel,
  Scopes,
  ScreenType,
} from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as moment from 'moment';
import { ListImportErrorComponent } from '../list-import-error/list-import-error.component';
import { Utils } from 'src/app/core/utils/utils';
import { Validators } from '@angular/forms';
import { ProductService } from 'src/app/core/services/product.service';
import * as _ from 'lodash';
import { FileService } from 'src/app/core/services/file.service';

@Component({
  selector: 'app-campaign-update',
  templateUrl: './campaign-update.component.html',
  styleUrls: ['./campaign-update.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CampaignUpdateComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
  data: any;
  processAssignType: string;
  listProcessAssignType = [];
  fileName: string;
  fileImport: File;
  files: any;
  fileId: string;
  form = this.fb.group({
    id: [''],
    name: [{value: ''}, CustomValidators.required],
    code: [{value: '', disabled: true}, CustomValidators.required],
    status: [{value: '', disabled: true}, CustomValidators.required],
    startDate: [new Date(), CustomValidators.required],
    endDate: ['', CustomValidators.required],
    productCode: ['', CustomValidators.required],
    typeId: ['', CustomValidators.required],
    process: [''],
    scope: ['', CustomValidators.required],
    areaCode: [],
    branchCodes: [],
    bizLine: [{ value: [], disabled: true }, CustomValidators.required],
    channel: ['', CustomValidators.required],
    blockCode: [{ value: '', disabled: true }, CustomValidators.required],
    formId: ['', CustomValidators.required],
    sourceId: [{ value: '', disabled: true }, CustomValidators.required],
    content: [''],
    slaManager: [null, [CustomValidators.required, Validators.min(1)]],
    slaRm: [null, [CustomValidators.required, Validators.min(1)]],
    isOutreachProcess: [false],
    isRmInsert: [false],
    fileId: null,
  });
  minEndDate = new Date();
  isLoading = false;
  isValidator = false;
  isRegion = false;
  isHO = false;
  isCBQL = false;
  hasImportError = false;
  listStatus = [];
  listProduct = [];
  listPurpose = [];
  listFormality = [];
  listBlock = [];
  listChannel = [];
  listProcess = [];
  listRegion = [];
  listBranch = [];
  listBranchByRegion = [];
  listBizLine = [];
  listBizLineByBlock = [];
  listSourceData = [];
  branchesCode: string[];
  selectedItemsLabel = 'ellipsis';
  avatarUploading: boolean;
  avatarName: string;
  isRm = false;
  rsId: string;
  listProductProcess = [];

  constructor(
    injector: Injector,
    private campaignService: CampaignsService,
    private categoryService: CategoryService,
    private productService: ProductService,
    private fileService: FileService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CAMPAIGN}`);
    this.rsId = this.objFunction?.rsId;
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.isHO = this.currUser?.branch === BRANCH_HO;
    const id = this.route.snapshot.paramMap.get('id');
    this.prop = this.sessionService.getSessionData(`${FunctionCode.CAMPAIGN}_${ScreenType.Update}`);
    if (!this.prop) {
      forkJoin([
        this.campaignService.getById(id),
        this.commonService
          .getCommonCategory(CommonCategory.EXT_CAMPAIGNS_CHANNELS)
          .pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.CAMPAIGN_TYPE).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.CAMPAIGN_FORM).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.SOURCE_DATA).pipe(catchError(() => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.CAMPAIGN_DIVISION).pipe(catchError(() => of(undefined))),
        this.productService.getProductByLevel(ProductLevel.Lvl1).pipe(catchError(() => of(undefined))),
        this.categoryService.getBlocksCategoryByRm().pipe(catchError(() => of(undefined))),
        this.translate.get('campaign').pipe(catchError(() => of(undefined))),
        this.categoryService
          .getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW)
          .pipe(catchError(() => of(undefined))),
        this.categoryService.getRegion().pipe(catchError(() => of(undefined))),
        this.categoryService.getBranchByCode(this.currUser?.branch).pipe(catchError((e) => of(undefined))),
        this.campaignService.getProductProcessByBlock(CustomerType.INDIV).pipe(catchError((e) => of(undefined))),
      ]).subscribe(
        ([
           itemCampaign,
           listSaleChannel,
           listPurpose,
           listFormality,
           listSourceData,
           listBizLine,
           listProduct,
           listBlock,
           campaign,
           branchesOfUser,
           listRegion,
           branchUser,
           listProductProcess
         ]) => {
          this.prop = {
            itemCampaign,
            listSaleChannel,
            listPurpose,
            listFormality,
            listSourceData,
            listBizLine,
            listProduct,
            listBlock,
            campaign,
            branchesOfUser,
            listRegion,
            branchUser,
            listProductProcess
          };
          console.log('listRegion: ', listRegion);
          this.isRm = _.isEmpty(branchesOfUser);
          this.prop.isRm = this.isRm;
          if (this.isRm) {
            branchesOfUser.push(branchUser);
            this.form.controls.slaManager.setValidators(null);
            this.form.controls.slaManager.disable();
          }

          this.sessionService.setSessionData(`${FunctionCode.CAMPAIGN}_${ScreenType.Update}`, this.prop);
          this.handleMapData();
        }
      );
    } else {
      this.campaignService.getById(id).subscribe((itemCampaign) => {
        this.prop.itemCampaign = itemCampaign;
        this.isRm = this.prop.isRm;
        if (this.isRm) {
          this.form.controls.slaManager.setValidators(null);
          this.form.controls.slaManager.disable();
        }

        this.handleMapData();
      });
    }
  }

  handleMapData() {
    this.isCBQL = this.prop?.branchesOfUser?.length > 0;
    console.log('campaign: ',this.prop?.campaign);
    if (this.isRm) {
      Object.keys(CampaignProcessType).forEach((key) => {
        if (CampaignProcessType[key] === CampaignProcessType.RM) {
          this.listProcessAssignType.push({
            code: CampaignProcessType[key],
            name: this.prop?.campaign?.processType[key.toLowerCase()],
          });
        }
      });
    } else if (this.isHO) {
      Object.keys(CampaignProcessType).forEach((key) => {
        this.listProcessAssignType.push({
          code: CampaignProcessType[key],
          name: this.prop?.campaign?.processType[key.toLowerCase()],
        });
      });
    } else if (this.isCBQL) {
      Object.keys(CampaignProcessType).forEach((key) => {
        if (CampaignProcessType[key] !== CampaignProcessType.MB247) {
          this.listProcessAssignType.push({
            code: CampaignProcessType[key],
            name: this.prop?.campaign?.processType[key.toLowerCase()],
          });
        }
      });
    }

    this.listProductProcess = this.prop?.listProductProcess || [];
    this.processAssignType = this.listProcessAssignType[0]?.code;
    this.listPurpose = this.prop?.listPurpose?.content || [];
    this.listFormality = this.prop?.listFormality?.content || [];
    this.listProduct =
      this.prop?.listProduct
        ?.filter((i) => i.level === ProductLevel.Lvl1)
        ?.map((item) => {
          return { code: item.idProductTree, name: item.idProductTree + ' - ' + item.description };
        }) || [];
    this.listBlock =
      this.prop?.listBlock?.map((item) => {
        return { code: item.code, name: item.code + ' - ' + item.name };
      }) || [];
    this.listChannel = this.prop?.listSaleChannel?.content || [];
    this.listChannel.unshift({ code: 'ALL', name: this.fields.all });
    this.listChannel = _.unionBy(this.listChannel, 'code');
    this.listSourceData = this.prop?.listSourceData?.content || [];
    this.listBizLine = this.prop?.listBizLine?.content || [];
    Object.keys(this.prop?.campaign?.status).forEach((key) => {
      this.listStatus.push({ code: key.toUpperCase(), name: this.prop?.campaign?.status[key] });
    });
    this.data = this.prop?.itemCampaign;
    this.avatarName = this.data?.fileName || this.fields?.chooseFile;
    this.data.startDate = new Date(this.data.startDate);
    this.data.endDate = new Date(this.data.endDate);
    this.branchesCode = this.data?.branchCodes;
    this.listBranch = this.prop?.branchesOfUser;
    this.form.patchValue(this.data);

    if (this.isHO) {
      this.listRegion = this.prop?.listRegion;
    } else {
      this.prop?.listRegion?.forEach((item) => {
        if (
          this.prop?.branchesOfUser?.findIndex((i) => i.khuVucM === item?.locationCode) !== -1 &&
          this.listRegion.findIndex((r) => r.locationCode === item?.locationCode) === -1
        ) {
          this.listRegion.push(item);
        }
      });
      this.form.controls.scope.disable();
      this.form.controls.areaCode.disable();
    }

    this.isLoading = false;
  }

  ngAfterViewInit() {
    this.form.controls.scope.valueChanges.subscribe((value) => {
      this.isRegion = value === CampaignSize.CHI_NHANH;
      if (this.isRegion) {
        this.form.controls.areaCode.setValidators(CustomValidators.required);
        this.form.controls.branchCodes.setValidators(CustomValidators.required);
      } else {
        this.form.controls.areaCode.clearValidators();
        this.form.controls.branchCodes.clearValidators();
      }
      this.form.updateValueAndValidity({ emitEvent: false });
    });
    // this.form.controls.productCode.valueChanges.subscribe((value) => {
    //   this.getProcess(value, this.form.controls.blockCode.value);
    // });
    // this.form.controls.blockCode.valueChanges.subscribe((value) => {
    //   this.getProcess(this.form.controls.productCode.value, value);
    // });
    this.form.controls.startDate.valueChanges.subscribe((value) => {
      if (value && value !== '') {
        if (moment(value).isBefore(moment())) {
          this.minEndDate = new Date();
        } else {
          this.minEndDate = value;
        }
      }
    });
    this.form.controls.areaCode.valueChanges.subscribe((value) => {
      if (value) {
        this.listBranchByRegion = this.listBranch.filter((item) => value.includes(item.khuVucM));
        if (!this.isLoading) {
          this.branchesCode = [];
        }
      }
    });
    this.form.controls.startDate.valueChanges.subscribe((value) => {
      const endDate = this.form.controls.endDate.value;
      if (value !== '' && endDate !== '' && moment(value).isAfter(moment(endDate))) {
        this.form.controls.endDate.setValue('');
      }
    });
    this.form.controls.blockCode.valueChanges.subscribe((value) => {
      this.listBizLineByBlock = this.listBizLine.filter((item) => item.value?.includes(value));
    });
  }

  save() {
    if (this.isLoading) {
      return;
    }
    this.form.controls.branchCodes.setValue(this.branchesCode);
    if (this.form.valid) {
      if (this.hasImportError) {
        this.openModalError();
        return;
      }
      this.confirmService.confirm().then((res) => {
        if (res) {
          this.isLoading = true;
          const formData: FormData = new FormData();
          formData.append('fileId', this.fileId);
          formData.append('campaignId', this.data.id);
          formData.append('assignProcessType', this.processAssignType);
          formData.append('isRm', `${this.isRm}`);

          const data = cleanDataForm(this.form);
          if (Utils.isStringEmpty(data.process)) {
            this.messageService.warn(this.notificationMessage.CAMPAIGN_E102);
            this.isLoading = false;
            return;
          }
          data.branchCodes = this.branchesCode;
          if (data.scope === 'TOAN_HANG') {
            delete data.areaCode;
            delete data.branchCodes;
          }

          this.campaignService.update(data).subscribe(
            () => {
              if (this.fileId) {
                this.campaignService.writeDataImport(formData).subscribe(
                  () => {
                    this.isLoading = false;
                    this.back();
                    this.messageService.success(this.notificationMessage.success);
                  },
                  (e) => {
                    this.isLoading = false;
                    if (e?.status === 0) {
                      this.messageService.error(this.notificationMessage.E001);
                    } else if (e?.error?.description) {
                      this.messageService.error(e?.error?.description);
                    } else {
                      this.messageService.error(this.notificationMessage.error);
                    }
                  }
                );
              } else {
                this.isLoading = false;
                this.back();
                this.messageService.success(this.notificationMessage.success);
              }
            },
            (e) => {
              this.isLoading = false;
              if (e?.status === 0) {
                this.messageService.error(this.notificationMessage.E001);
              } else {
                this.messageService.error(this.notificationMessage.error);
              }
            }
          );
        }
      });
    } else {
      this.isValidator = true;
      validateAllFormFields(this.form);
    }
  }

  getProcess(productCode, blockCode) {
    this.productService.getProcessByProductAndBlock(productCode, blockCode).subscribe((listData) => {
      if (listData?.content[0]) {
        this.form.controls.process.setValue(listData?.content[0]?.process);
      } else {
        this.form.controls.process.reset();
      }
    });
  }

  handleFileInput(files) {
    if (files && files.length > 0) {
      this.fileImport = files.item(0);
      this.fileName = files.item(0).name;
    }
  }

  onchangeFile(inputFile) {
    if (this.fileName) {
      return;
    }
    inputFile.click();
  }

  clearFile() {
    this.fileName = undefined;
    this.fileImport = undefined;
    this.files = undefined;
    this.fileId = undefined;
    this.hasImportError = false;
  }

  importFile() {
    if (this.fileImport && this.fileName) {
      this.isLoading = true;
      const formData: FormData = new FormData();
      formData.append('file', this.fileImport);
      formData.append('campaignId', this.data.id);
      if (this.isRm) {
        this.campaignService.importByRm(formData).subscribe(
          (res) => {
            this.fileId = res?.id;
            this.checkImportSuccess(this.fileId);
          },
          (e) => {
            if (e?.error) {
              this.messageService.warn(e?.error?.description);
            } else {
              this.messageService.error(this.notificationMessage.error);
            }
            this.isLoading = false;
          }
        );
      } else {
        this.campaignService.import(formData).subscribe(
          (res) => {
            this.fileId = res?.id;
            this.checkImportSuccess(this.fileId);
          },
          (e) => {
            if (e?.error) {
              this.messageService.warn(e?.error?.description);
            } else {
              this.messageService.error(this.notificationMessage.error);
            }
            this.isLoading = false;
          }
        );
      }
    }
  }

  checkImportSuccess(fileId: string) {
    const countInterval = 0;
    const interval = setInterval(() => {
      this.campaignService.checkFileImport(fileId).subscribe((res) => {
        if (res?.status === 'COMPLETE') {
          clearInterval(interval);
          this.campaignService.searchImportError({ fileId: this.fileId, campaignId: this.data?.id }).subscribe(
            (list) => {
              this.isLoading = false;
              if (list?.content?.length > 0) {
                this.hasImportError = true;
                this.openModalError();
              } else {
                this.messageService.success(this.notificationMessage.success);
              }
            },
            () => {
              this.isLoading = false;
            }
          );
        } else if (res?.status === 'FAIL') {
          if (res?.msgError?.includes('FILE_DOES_NOT_EXCEED_RECORDS')) {
            const maxRecord = res?.msgError?.replace('FILE_DOES_NOT_EXCEED_RECORDS_', '');
            const type = this.fileName?.split('.')[this.fileName?.split('.')?.length - 1];
            this.translate
              .get('notificationMessage.FILE_DOES_NOT_EXCEED_RECORDS', { number: maxRecord, type })
              .subscribe((message) => {
                this.messageService.error(message);
              });
          } else if (res?.msgError === 'FILE_NO_CONTENT_EXCEPTION') {
            this.messageService.error(this.notificationMessage.FILE_NO_CONTENT_EXCEPTION);
          } else {
            this.messageService.error(this.notificationMessage.CANNOT_READ_DATA_FROM_FILE);
          }
          this.isLoading = false;
          clearInterval(interval);
        }
      });
    }, 5000);
    if (countInterval > 360) {
      clearInterval(interval);
    }
  }

  openModalError() {
    const modalError = this.modalService.open(ListImportErrorComponent, { windowClass: 'list__campaign-error' });
    modalError.componentInstance.fileId = this.fileId;
    modalError.componentInstance.campaignId = this.data.id;
    modalError.result
      .then(() => {
        const formData: FormData = new FormData();
        formData.append('fileId', this.fileId);
        this.campaignService.clearDataImport(formData).subscribe(() => {});
        this.clearFile();
      })
      .catch(() => {});
  }

  downloadTemplate() {
    if (this.isRm) {
      window.open('/assets/template/template_Danh_sach_khach_hang_muc_tieu_CRM_RM.xls', '_self');
    } else {
      window.open('/assets/template/template_Danh_sach_khach_hang_muc_tieu_CRM.xls', '_self');
    }

  }

  uploadAvatar(event, fileUpload) {
    if (!_.startsWith(_.get(event, 'files[0].type'), 'image/')) {
      this.avatarName = this.fields?.chooseFile;
      fileUpload.clear();
      this.messageService.error(this.notificationMessage.E124);
      return;
    }
    if (_.get(event, 'files[0].size') > 512000) {
      this.messageService.error(this.notificationMessage.max500kb);
      this.avatarName = this.fields?.chooseFile;
      fileUpload.clear();
      return;
    }
    this.avatarUploading = true;
    const formData = new FormData();
    formData.append('file', event.files[0]);
    this.avatarName = event?.files[0]?.name;
    this.fileService
      .uploadFile(formData)
      .pipe(catchError(() => of(undefined)))
      .subscribe((res) => {
        this.avatarUploading = false;
        if (!res) {
          this.messageService.error(this.notificationMessage.uploadFail);
          this.avatarName = this.fields?.chooseFile;
        }
        fileUpload.clear();
        this.form.controls.fileId.setValue(res || null);
      });
  }
}
