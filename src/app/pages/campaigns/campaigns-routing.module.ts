import { CampaignListViewComponent } from './components/campaign-list-view/campaign-list-view.component';
import { CampaignDetailComponent } from './components/campaign-detail/campaign-detail.component';
import { CampaignUpdateComponent } from './components/campaign-update/campaign-update.component';
import { CampaignCreateComponent } from './components/campaign-create/campaign-create.component';
import { CampaignsLeftViewComponent } from './components/campaigns-left-view/campaigns-left-view.component';
import { CampaignsRmComponent } from './components/campaigns-rm/campaigns-rm.component';
import { CampaignsRmDetailComponent } from './components/campaigns-rm-detail/campaigns-rm-detail.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FunctionCode, Scopes, ScreenType} from 'src/app/core/utils/common-constants';
import { CampaignMappingListViewComponent } from './components/campaign-mapping-list-view/campaign-mapping-list-view.component';
import { CampaignMappingCreateComponent } from './components/campaign-mapping-create/campaign-mapping-create.component';
import { CampaignRmCustomerComponent } from './components/campaign-rm-customer/campaign-rm-customer.component';
import { RoleGuardService } from 'src/app/core/services/role-guard.service';
import { CampaignRedistributeComponent } from './components/campaign-redistribute/campaign-redistribute.component';
import {CampaignAddCustomerComponent} from './components';
import { CreateSaleCampaignComponent } from './components/campaign-sme/create/create.component';
import { DetailSaleCampaignComponent } from './components/campaign-sme/detail/detail.component';
import { CampaignSMEUpdateComponent } from './components/campaign-sme/update/update.component';
import { TargetImportFileComponent } from './components/campaign-sme/target-import-file/target-import-file.component';
import { AssignCustomerSMEImportComponent } from './components/campaign-sme/assign-customer-import/assign-customer-import.component';
import {CampaignImportCustomerComponent} from './components/campaign-import-customer/campaign-import-customer.component';
const routes: Routes = [
  {
    path: '',
    component: CampaignsLeftViewComponent,
    outlet: 'app-left-content',
  },
  {
    path: 'campaign-management',
    data: {
      code: FunctionCode.CAMPAIGN,
    },
    canActivateChild: [RoleGuardService],
    children: [
      {
        path: '',
        component: CampaignListViewComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
      {
        path: 'create',
        component: CampaignCreateComponent,
        data: {
          scope: Scopes.CREATE,
        },
      },
      {
        path: 'create-sme',
        component: CreateSaleCampaignComponent,
        data: {
          scope: Scopes.CREATE,
        },
      },
      {
        path: 'update/:id',
        component: CampaignUpdateComponent,
        data: {
          scope: Scopes.UPDATE,
        },
      },
      {
        path: 'update-sme/:id',
        component: CampaignSMEUpdateComponent,
        data: {
          scope: Scopes.UPDATE,
        },
      },
      {
        path: 'detail/:id',
        component: CampaignDetailComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
      {
        path: 'redistributed/:id',
        data: {
          scope: Scopes.VIEW,
        },
        component: CampaignRedistributeComponent,
      },
      {
        path: 'add-customer',
        component: CampaignAddCustomerComponent,
        data: {
          scope: Scopes.CREATE,
        },
      },
      {
        path: 'detail-sme/:id',
        component: DetailSaleCampaignComponent,
        data: {
          scope: Scopes.CREATE,
        },
      },
      {
        path: 'target-sme-import-file',
        component: TargetImportFileComponent,
        canActivate: [],
        data: {
          scope: Scopes.CREATE,
        },
      },
      {
        path: 'assign-customer-sme-import-file',
        component: AssignCustomerSMEImportComponent,
        canActivate: [],
        data: {
          scope: Scopes.CREATE,
        },
      },
      {
        path: 'import-customer',
        component: CampaignImportCustomerComponent,
        canActivate: [],
        data: {
          scope: Scopes.CREATE
        }
      },
    ],
  },

  {
    path: 'campaign-rm',
    data: {
      code: FunctionCode.CAMPAIGN_RM,
    },
    canActivateChild: [RoleGuardService],
    children: [
      {
        path: '',
        component: CampaignsRmComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
      {
        path: 'detail/:id',
        component: CampaignsRmDetailComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
      {
        path: 'customer/:campaignId/:type/:code',
        component: CampaignRmCustomerComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
      {
        path: 'add-customer',
        component: CampaignAddCustomerComponent,
        data: {
          scope: Scopes.CREATE,
        },
      },
    ],
  },
  {
    path: 'campaign-mapping',
    data: {
      code: FunctionCode.CAMPAIGN_MAPPING,
    },
    canActivateChild: [RoleGuardService],
    children: [
      {
        path: '',
        component: CampaignMappingListViewComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
      {
        path: 'create',
        component: CampaignMappingCreateComponent,
        data: {
          scope: Scopes.CREATE,
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CampaignsRoutingModule {}
