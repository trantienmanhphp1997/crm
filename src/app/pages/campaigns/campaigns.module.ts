import {
  CampaignCreateComponent,
  CampaignUpdateComponent,
  CampaignDetailComponent,
  CampaignListViewComponent,
  CampaignsLeftViewComponent,
  CampaignTypeListViewComponent,
  CampaignTypeCreateComponent,
  CampaignMappingListViewComponent,
  CampaignMappingCreateComponent,
  CampaignsRmComponent,
  CampaignsRmDetailComponent,
  CustomerTargetsComponent,
  ListImportErrorComponent,
  ActivityActionComponent,
  CustomerActivitiesComponent,
  ActivityRecordComponent,
  CustomerActivityHistoryComponent,
  CustomerCampaignsComponent,
  CustomerCampaignModalComponent,
  CampaignRmCustomerComponent,
  CampaignRedistributeComponent,
  CampaignAddCustomerComponent,
  CampaignsRmModalComponent,
  CampaignsModalComponent,
  CampaignsCreateModal
} from './components';
import { SharedModule } from '../../shared/shared.module';
import { CampaignsRoutingModule } from './campaigns-routing.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { RadioButtonModule } from 'primeng/radiobutton';
import { CheckboxModule } from 'primeng/checkbox';
import { InputNumberModule } from 'primeng/inputnumber';
import { NgxSelectModule } from 'ngx-select-ex';
import { TableModule } from 'primeng/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MultiSelectModule } from 'primeng/multiselect';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { BadgeModule } from 'primeng/badge';
import { ChipModule } from 'primeng/chip';
import { AvatarModule } from 'primeng/avatar';
import { CascadeSelectModule } from 'primeng/cascadeselect';
import { TimelineModule } from 'primeng/timeline';
import { MenuModule } from 'primeng/menu';
import { FileUploadModule } from 'primeng/fileupload';
//
import {TabMenuModule} from 'primeng/tabmenu';
import {EditorModule} from 'primeng/editor';
import { CreateSaleCampaignComponent } from './components/campaign-sme/create/create.component';
import { DeclareCampaignComponent } from './components/campaign-sme/declare-campaign/declare-campaign.component';
import { ChooseCreateModalComponent } from './components/choose-create-modal/choose-create-modal.component';
import { DetailSaleCampaignComponent } from './components/campaign-sme/detail/detail.component';
import { CampaignInfoComponent } from './components/campaign-sme/detail-campaign-info/campaign-info.component';
import { CampaignSMEUpdateComponent } from './components/campaign-sme/update/update.component';
import { SafeHtml } from './pipes/safe-html.pipe';
import { CampaignTargetComponent } from './components/campaign-sme/target/target.component';
import { TargetImportFileComponent } from './components/campaign-sme/target-import-file/target-import-file.component';
import { AssignCustomerComponent } from './components/campaign-sme/assign-customer/assign-customer.component';
import { DetailCampaignAllocationComponent } from './components/campaign-sme/detail-campaign-allocation/detail-campaign-allocation.component';
import { AssignCustomerSMEImportComponent } from './components/campaign-sme/assign-customer-import/assign-customer-import.component';
import { DetailCustomerTrackingComponent } from './components/campaign-sme/detail-customer-tracking/detail-customer-tracking.component';
import { MatMenuModule } from '@angular/material/menu';
import { PopupUpdateNoteComponent } from './components/campaign-sme/popup-update-note/popup-update-note.component';
import { DetailResultBranchComponent } from './components/campaign-sme/detail-result-branch/detail-result-branch.component';
import { DetailResultRmComponent } from './components/campaign-sme/detail-result-rm/detail-result-rm.component';
import { MatIconModule } from '@angular/material/icon';
import { CampaignResultComponent } from './components/campaign-sme/camping-result/campaign-result';
import { KpiReportBranchTableComponent } from './components/campaign-sme/kpi-report-branch-table/kpi-report-branch-table.component';
import { TreeTableModule } from 'primeng/treetable';
import {CampaignImportCustomerComponent} from "./components/campaign-import-customer/campaign-import-customer.component";
import {CampaignChangeStageComponent} from "./components/campaign-change-stage/campaign-change-stage.component";

const COMPONENTS = [
  CampaignCreateComponent,
  CampaignUpdateComponent,
  CampaignDetailComponent,
  CampaignListViewComponent,
  CampaignsLeftViewComponent,
  CampaignTypeListViewComponent,
  CampaignTypeCreateComponent,
  CampaignMappingListViewComponent,
  CampaignMappingCreateComponent,
  CampaignsRmComponent,
  CampaignsRmComponent,
  CampaignsRmDetailComponent,
  CustomerTargetsComponent,
  ListImportErrorComponent,
  ActivityActionComponent,
  CustomerActivitiesComponent,
  ActivityRecordComponent,
  CustomerActivityHistoryComponent,
  CustomerCampaignsComponent,
  CustomerCampaignModalComponent,
  CampaignRmCustomerComponent,
  CampaignRedistributeComponent,
  CampaignAddCustomerComponent,
  CampaignsRmModalComponent,
  CampaignsModalComponent,
  CampaignsCreateModal,
  CampaignSMEUpdateComponent,
  DetailCampaignAllocationComponent,
  SafeHtml,
  CampaignTargetComponent,
  TargetImportFileComponent,
  AssignCustomerComponent,
  DetailCustomerTrackingComponent,
  PopupUpdateNoteComponent,
  AssignCustomerSMEImportComponent,
  KpiReportBranchTableComponent,
  CampaignInfoComponent,
  DetailSaleCampaignComponent,
  CreateSaleCampaignComponent,
  DeclareCampaignComponent,
  ChooseCreateModalComponent,
  CampaignResultComponent,
  CampaignImportCustomerComponent,
  CampaignChangeStageComponent,
];

@NgModule({
  declarations: [...COMPONENTS, DetailResultBranchComponent, DetailResultRmComponent],
  imports: [
    CommonModule,
    CampaignsRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    SharedModule,
    NgbModule,
    TranslateModule,
    NgxDatatableModule,
    InputTextModule,
    InputTextareaModule,
    DropdownModule,
    CalendarModule,
    RadioButtonModule,
    CheckboxModule,
    InputNumberModule,
    NgxSelectModule,
    TableModule,
    MatTabsModule,
    MultiSelectModule,
    AvatarModule,
    ButtonModule,
    RippleModule,
    BadgeModule,
    ChipModule,
    CascadeSelectModule,
    TimelineModule,
    MenuModule,
    FileUploadModule,
    TabMenuModule,
    EditorModule,
    MatMenuModule,
    MatIconModule,
    TreeTableModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class CampaignsViewModule {}
