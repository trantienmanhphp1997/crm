import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpWrapper } from 'src/app/core/apis/http-wapper';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WarningService extends HttpWrapper {

  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_customer_sme}/warning-product`);
  }

  getAll(params: any): Observable<any> {
    return this.post('get-list-warning', params);
  }

  export(params: any): Observable<any> {
    return this.http.post(`${environment.url_endpoint_customer_sme}/warning-product/export-list-warning`, params, {responseType: 'text'});
  }

}
