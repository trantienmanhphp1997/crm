import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WarningRoutingModule } from './warning.routing.';
import { SharedModule } from 'src/app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { WarningComponent } from './warning.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputNumberModule } from 'primeng/inputnumber';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatMenuModule } from '@angular/material/menu';
import { MatTabsModule } from '@angular/material/tabs';
import { NgxSelectModule } from 'ngx-select-ex';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { MultiSelectModule } from 'primeng/multiselect';
import { RadioButtonModule } from 'primeng/radiobutton';
import { SelectButtonModule } from 'primeng/selectbutton';
import { TableModule } from 'primeng/table';
import { TooltipModule } from 'primeng/tooltip';
import { InputTextModule } from 'primeng/inputtext';
import { TagModule } from 'primeng/tag';

@NgModule({
  declarations: [WarningComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    NgbModule,
    TranslateModule,
    NgxDatatableModule,
    InputTextModule,
    InputTextareaModule,
    DropdownModule,
    RadioButtonModule,
    CheckboxModule,
    InputNumberModule,
    NgxSelectModule,
    TableModule,
    MatTabsModule,
    MultiSelectModule,
    ButtonModule,
    SelectButtonModule,
    TooltipModule,
    WarningRoutingModule,
    TagModule,
    MatMenuModule,
  ]
})
export class WarningModule { }
