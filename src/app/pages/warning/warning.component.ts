import {AfterViewInit, Component, Injector, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { forkJoin, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { FileService } from 'src/app/core/services/file.service';
import {
  Scopes,
  FunctionCode,
  maxInt32,
  CommonCategory,
  DUE_STATUS_LIST,
  WARNING_TYPE,
  functionUri,
  SessionKey, TaskType, ScreenType
} from 'src/app/core/utils/common-constants';
import { RmApi } from '../rm/apis';
import { SaleManagerApi } from '../sale-manager/api';
import { CategoryService } from '../system/services/category.service';
import { global } from '@angular/compiler/src/util';
import * as _ from 'lodash';
import { Utils } from 'src/app/core/utils/utils';
import { WarningService } from './services/warning.service';
import { CalendarAddModalComponent } from '../calendar-sme/components/calendar-add-modal/calendar-add-modal.component';
import { MatMenuTrigger } from '@angular/material/menu';
import { CustomerLeadService } from '../lead/service/customer-lead.service';
import { CustomerAssignmentApi } from '../customer-360/apis';
import {RmModalComponent} from '../rm/components';
import {ProductService} from '../../core/services/product.service';
import {ActivityActionComponent} from "../../shared/components";
import {cleanData, cleanDataForm} from 'src/app/core/utils/function';


@Component({
  selector: 'app-warning',
  templateUrl: './warning.component.html',
  styleUrls: ['./warning.component.scss']
})
export class WarningComponent extends BaseComponent implements OnInit, AfterViewInit {

  @ViewChild('table') table: DatatableComponent;
  @ViewChildren(MatMenuTrigger) trigger: QueryList<any>;

  isLoading = false;
  limit = global.userConfig.pageSize;
  pageable: Pageable;
  params: any = {
    pageNumber: 0,
    pageSize: global?.userConfig?.pageSize,
    rsId: '',
    scope: Scopes.VIEW,
  };


  formSearch = this.fb.group({
    listRm: [[]],
    rsId: '',
    scope: Scopes.VIEW,
    searchAdvanced: [''],
    status: [null],
    typeWarning: [null],
    dueDate: [null],
    listBranch: [[]],
    groupByCustomer: [false],
    blockCode: ''
  });

  formSearchINDIV = this.fb.group({
    listRm: [[]],
    rsId: '',
    scope: Scopes.VIEW,
    searchAdvanced: [''],
    customerStatus: [null],
    typeWarning: [],
    listBranch: [[]],
    groupByCustomer: [false],
    privatePriority: [[]],
    groupDebt: [null],
    soLanTraNoTreHan: null
  });

  commonDataSME = {
    dueStatusList: DUE_STATUS_LIST,
    warningTypeList: [
      { code: null, name: 'Tất cả' },
    ],
    dueDateList: [
      { value: null, name: 'Tất cả' },
    ],
  }
  commonDataINDIV = {
    dueStatusList: [
      { code: null, name: 'Tất cả' },
    ],
    warningTypeList: [
      { code: null, name: 'Tất cả' },
    ],
    groupDebt: [
      { value: null, name: 'Tất cả' },
    ],
    privatePriority: [],
  }

  listDivision = [];
  branchesOfUser = [];
  branchesOfUserIndiv: Array<any>;
  typeName = {};
  typeConfig = WARNING_TYPE;

  listEmail = [];
  listContactPosition = [];

  listData = [];

  listRmManager = [];
  listBranch = [];
  isRM = true;
  objRmManagerFunc = null;
  divisionOfUser: any[];
  branchesCode: string[];
  employeeCode: Array<string>;
  employeeCodeParams: Array<string>;
  termData = [];
  type = 'INDIV';
  typeDivision = '';
  showGroupDebtDr = false;


  rmParams = {
    page: 0,
    size: maxInt32,
    crmIsActive: true,
    rsId: null,
    scope: Scopes.VIEW,
    branchCodes: [],
  };

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private fileService: FileService,
    private rmApi: RmApi,
    private warningService: WarningService,
    private customerLeadService: CustomerLeadService,
    private customerAssignmentApi: CustomerAssignmentApi,
    private productService: ProductService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.WARNING}`);
    this.objRmManagerFunc = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.formSearch.get('rsId').setValue(this.objFunction.rsId);
    this.rmParams.rsId = this.objRmManagerFunc.rsId;
    this.divisionOfUser = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];

  }


  ngOnInit() {
    this.employeeCode = _.get(this.fields, 'all');
    this.prop = this.sessionService.getSessionData(FunctionCode.WARNING);
    this.listDivision =
      this.divisionOfUser?.map((item) => {
        return { code: item.code, name: `${item.code || ''} - ${item.name || ''}` };
      }) || [];
    this.typeDivision = this.listDivision.find(item => item.code === 'INDIV') ? 'INDIV' : 'SME';
    this.getTypeName();
    this.isLoading = true;
    forkJoin([
      this.categoryService.getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.DUE_DATE_WARNING).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.WARNING_TYPE_LIST).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.CONTACT_POSITION).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.WARNING_TYPE_LIST_INDIV).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.DUE_STATUS_LIST_INDIV).pipe(catchError(() => of(undefined))),
      this.rmApi.post('findAll', this.rmParams).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.SEGMENT_SUGGEST_SELLING_PRODUCT).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.GROUP_DEBT).pipe(catchError(() => of(undefined))),
    ]).subscribe(([branchesOfUser, dueDateList, warningTypeList, position,  warningTypeListIndiv, dueDateStatus, rmList, privatePriority, groupDebt]) => {

      this.commonDataINDIV.dueStatusList.push(..._.get(dueDateStatus, 'content'));
      this.commonDataINDIV.groupDebt.push(..._.get(groupDebt, 'content'));
      this.commonDataINDIV.privatePriority = _.get(privatePriority, 'content');
      this.formSearchINDIV.controls.privatePriority.setValue(_.get(privatePriority, 'content').map(item => item.code));
      this.commonDataINDIV.warningTypeList.push(..._.get(warningTypeListIndiv, 'content'));
      this.branchesOfUserIndiv = branchesOfUser;
      this.branchesOfUser = branchesOfUser?.map((item) => {
        return {
          code: item.code,
          name: `${item.code} - ${item.name}`,
        };
      }) || [];
      this.isRM = _.isEmpty(branchesOfUser);

      // due date
      this.commonDataSME.dueDateList.push(...dueDateList.content?.map(item => ({ value: item.value, name: item.name })));

      // type
      this.commonDataSME.warningTypeList.push(...warningTypeList.content?.map(item => ({ code: item.code, name: item.name })))

      // contact position
      this.listContactPosition = position?.content;

      // rm List
      this.setRmList(rmList?.content);

      // is rm
      if (this.isRM) {
        this.branchesOfUser.push({
          code: this.currUser.branch,
          name: `${this.currUser.branch} - ${this.currUser.branchName}`,
        });
        this.formSearch.controls.listBranch.setValue([this.currUser.branch]);
        this.formSearch.controls.listBranch.disable();
      }

      this.typeDivision !== 'INDIV' ? this.search(true) : this.searchINDIV(true);
    });

    this.setValueChanges();
  }

  ngAfterViewInit() {
    this.formSearchINDIV.controls.typeWarning.valueChanges.subscribe((value => {
      this.showGroupDebtDr = _.find(this.commonDataINDIV.warningTypeList, item => item.code === value).description === 'true';
      this.formSearchINDIV.controls.groupDebt.setValue(null);
    }));
  }

  getTypeName(): void {
    this.commonDataSME.warningTypeList.filter(item => item.code).forEach(item => {
      this.typeName[item.code] = item;
    });
  }

  search(isSearch: boolean) {
    this.isLoading = true;

    if (isSearch) {
      this.params.pageNumber = 0;
    }
    const params = {
      ...this.formSearch.getRawValue(),
      page: this.params.pageNumber,
      size: this.params.pageSize,
    };

    this.warningService.getAll(params).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(
      (result) => {
        if (result) {
          this.isLoading = false;
          this.listData = result.content || [];

          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: this.limit,
          };
        }
      });
  }

  searchINDIV(isSearch: boolean) {
    console.log('INDIV');
    this.isLoading = true;

    if (isSearch) {
      this.params.pageNumber = 0;
    }
    this.employeeCode !== this.fields.all ? this.formSearchINDIV.controls.listRm.setValue(this.employeeCode?.toString().split(',')) :
      this.formSearchINDIV.controls.listRm.setValue([]);
    this.formSearchINDIV.controls.listBranch.setValue(this.branchesCode);
    const params = {
      ...this.formSearchINDIV.getRawValue(),
      pageNumber: this.params.pageNumber,
      pageSize: this.params.pageSize,
      rsId: this.objFunction.rsId,
      scope: 'VIEW'
    };
    params.dueDate = '';
    params.privatePriority = this.formSearchINDIV.controls.privatePriority.value.length === this.commonDataINDIV.privatePriority.length
      ? []
      : this.formSearchINDIV.controls.privatePriority.value;
    cleanDataForm(this.formSearchINDIV);
    this.productService.getAllWarningIndiv(params).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(
      (result) => {
        if (result) {
          this.isLoading = false;
          this.mapDataTree(_.get(result,'content'));
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: this.params.pageNumber,
            size: this.params.pageSize,
          };
        }
      });
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.pageNumber = pageInfo.offset;
    this.typeDivision === 'INDIV' ? this.searchINDIV( false) : this.search(false);
  }

  onActive(event) {

  }

  exportFile() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+this.pageable?.totalElements <= 0) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }

    this.isLoading = true;

    const paramExport = {
      ...this.formSearch.getRawValue(),
      rsId: this.objFunction.rsId,
      pageNumber: 0,
      pageSize: maxInt32,
    };

    this.warningService.export(paramExport).subscribe(
      (res) => {
        if (res) {
          this.download(res);
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(_.get(this.notificationMessage, 'export_error'));
        this.isLoading = false;
      }
    );
  }

  download(fileId: string) {
    const titleExcel =
      'canhbao-SPDV' +
      '.xlsx';
    this.fileService.downloadFile(fileId, titleExcel).subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }

  groupByCustomer(): void {

  }

  getRmList(): void {
    const listRmControl = this.formSearch.get('listRm');
    listRmControl.setValue([]);
    listRmControl.disable();
    this.rmParams.branchCodes = this.formSearch.get('listBranch').value;
    this.rmApi.post('findAll', this.rmParams).pipe(
      finalize(() => {
        listRmControl.enable();
      })
    ).subscribe(res => {
      this.setRmList(res?.content);
    })
  }

  setRmList(list: any[]): void {
    // rm List
    const listRm: any[] =
      list.map((item) => {
        if (!_.isEmpty(item?.t24Employee?.employeeCode)) {
          return {
            code: item?.t24Employee?.employeeCode || '',
            name:
              Utils.trimNullToEmpty(item?.t24Employee?.employeeCode) +
              ' - ' +
              Utils.trimNullToEmpty(item?.hrisEmployee?.fullName),
          };
        }
      }) || [];

    this.listRmManager = listRm;

    if (this.isRM) {
      this.formSearch.get('listRm').setValue([this.currUser.code]);
      this.formSearch.get('listRm').disable();
    }
  }

  getAllMailAndPosition(row) {
    this.listEmail = [];
    const data = {
      leadCode: '',
      customerCode: row.customerCode ? row.customerCode : '',
      rsId: this.objFunction.rsId,
      scope: 'VIEW'
    }
    this.isLoading = true;
    this.customerLeadService.getAllMail(data).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(value => {
        if (value) {
          this.listEmail = value.filter(e => e.mail !== undefined);
          if (_.isEmpty(this.listEmail)) {
            this.mailTo(row, undefined)
          }
          this.listContactPosition.forEach(item => {
            this.listEmail.forEach(e => {
              if (item.code === e.position) {
                e.position = item.name;
              }
            })
          });
        }
      }, () => {
        this.trigger.forEach(item => {
          item.closeMenu();
        });
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }

  createTaskTodo(row) {
    const modal = this.modalService.open(CalendarAddModalComponent, { windowClass: 'create-calendar-modal' });
    modal.componentInstance.customerCode = row.customerCode;
    modal.componentInstance.customerName = row.customerName;
    modal.componentInstance.isDetailLead = true;
    modal.result
      .then(() => {
        // if (res) {
        // } else {
        // }
      })
      .catch(() => {
      });
  }

  mailTo(row, mail) {
    if (_.isEmpty(mail)) {
      this.trigger.forEach(item => {
        item.closeMenu();
      });
      this.messageService.error('Không có thông tin Email');
    } else {
      const data = {
        type: 'EMAIL',
        customerCode: row.customerCode ? row.customerCode : '',
        leadCode: '',
        scope: Scopes.VIEW,
        rsId: this.objFunction.rsId
      }

      this.customerAssignmentApi.updateActivityLog(data).subscribe(() => {
      });

      const url = `mailto:${mail}`;
      window.location.href = url;
    }
  }

  viewDetail(item) {
    const url = this.router.serializeUrl(
      this.router.createUrlTree([`${functionUri.customer_360_manager}/detail/sme/${item.customerCode}`], {
        queryParams: {
          tab:2
        }
      })
    );

    window.open(url, '_blank');
  }

  setValueChanges(): void {
    this.formSearch.controls.listBranch.valueChanges.subscribe(() => {
      this.getRmList();
    })
  }

  removeRMSelected() {
    this.employeeCode = _.get(this.fields, 'all');
    this.employeeCodeParams = null;
    this.termData = [];
  }

  searchRM() {
    const formSearch = {
      crmIsActive: { value: 'true', disabled: true }
    };
    const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
    modal.componentInstance.listHrsCode = this.termData?.map((item) => item.hrsCode);
    modal.componentInstance.dataSearch = formSearch;
    modal.componentInstance.branchCodes = this.branchesCode;
    modal.componentInstance.branchCodes = this.branchesCode;
    modal.componentInstance.block = 'INDIV';
    modal.result
      .then((res) => {
        if (res) {
          this.employeeCode = [];
          this.employeeCodeParams = [];
          const listRMSelect = res.listSelected?.map((item) => {
            this.employeeCode.push(item?.t24Employee?.employeeCode);
            this.employeeCodeParams.push(item.t24Employee.employeeCode);
            return {
              hrsCode: item?.hrisEmployee?.employeeId,
              code: item?.t24Employee?.employeeCode,
              name: item?.hrisEmployee?.fullName,
            };
          });
          this.termData = _.uniqBy([...this.termData, ...listRMSelect], (i) => i.hrsCode);
          this.termData = _.remove(this.termData, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);

          this.translate.get('fields').subscribe((res) => {
            this.fields = res;
            const codes = this.employeeCode;
            const codeParams = this.employeeCodeParams;
            const countCode = (codes) => codes.reduce((a, b) => ({ ...a, [b]: (a[b] || 0) + 1 }), {});
            const countCodeParams = (codeParams) => codeParams.reduce((a, b) => ({ ...a, [b]: (a[b] || 0) + 1 }), {});

            this.employeeCode = Object.keys(countCode(codes));
            this.employeeCodeParams = Object.keys(countCodeParams(codeParams));
            this.employeeCode =
              _.size(this.employeeCode) === 0 ||
              (_.size(this.employeeCode) === 1 && this.employeeCode[0] === _.get(this.fields, 'all'))
                ? _.get(this.fields, 'all')
                : _.join(this.employeeCode, ', ');
          });

          if (this.employeeCode.length === 0) {
            this.removeRMSelected();
          }
        } else {
          this.removeRMSelected();
        }
      })
      .catch(() => {});
  }

  changeDivision(event) {
    if (this.typeDivision !== 'INDIV' && event.value !== 'INDIV') {
      this.search(true);
    }
    this.typeDivision = event.value;
  }

  handleChangePageSize(event) {
    // this.pageable.currentPage = 0;
    // this.pageable.size = event;
    this.limit = event;
    this.params.pageNumber = 0;
    this.params.pageSize = event;
    this.searchINDIV(true);
  }

  mailToINDIV(mail) {
    const url = `mailto:${mail}`;
    console.log(url);
    window.location.href = url;
  }

  createActivity(row) {
    const parent: any = {};
    parent.parentType = TaskType.CUSTOMER;
    parent.parentId = _.get(row,'id');
    // parent.parentName = this.customerName;
    const modal = this.modalService.open(ActivityActionComponent, {
      windowClass: 'create-activity-modal',
      scrollable: true,
    });
    modal.componentInstance.parent = parent;
    modal.componentInstance.type = ScreenType.Create;
    modal.componentInstance.isShowFuture = true;
  }

  removeBranchSelected() {
    this.branchesCode = null;
    this.removeRMSelected();
  }

  changeBranch(event) {
    this.branchesCode = event;
    this.removeRMSelected();
  }

  onTreeAction(event: any) {
    const row = event.row;

    if (row.treeStatus === 'collapsed') {
      row.treeStatus = 'expanded';
    } else {
      row.treeStatus = 'collapsed';
    }

    this.listData = [...this.listData];
    this.ref.detectChanges();
  }

  mapDataTree(treeLv1) {
    let treeLv1Clone = _.cloneDeep(treeLv1);
    // thêm id tăng dần cho data tree lv1
    treeLv1Clone = _.map(treeLv1Clone, (data, i) => ({id: 'id' + i + 1, ...data}));

    // tạo tree lv0 với id = customerCode
    const treeLv0 = _.chain(treeLv1Clone)
      .groupBy('customerCode')
      .map((value, key) => (
        { id: key ,
          treeStatus: 'expanded',
          customerName: value[0].customerName,
          isTreeLv0: true,
          email: value[0].email,
          slaMin: value[0].sla,
          duNoTaiNgaySaoKe:_.sumBy(value, 'duNoTaiNgaySaoKe'), // tính tổng theo từng customerCode
          laiDuChi:_.sumBy(value, 'laiDuChi'),
          lai_PHAT_PHI_PHAT:_.sumBy(value, 'lai_PHAT_PHI_PHAT')
        }))
      .sortBy('slaMin')
      .map((data, i) => ({index: i + 1 , ...data}))  // thêm stt cho tree lv0
      .value();


    // thêm stt cho tree lv1
    const tempList:any = [];
    treeLv1Clone.forEach(item => {
      const  groupByCodeList = _.values(_.groupBy(tempList)).map(d => ({name: d[0], count: d.length}));
      item.index = _.find(groupByCodeList, (i) => i.name === item.customerCode)
        ? _.find(groupByCodeList, (i) => i.name === item.customerCode)?.count + 1
        : 1;
      tempList.push( item.customerCode);
    });

    this.listData = [...treeLv0, ...treeLv1Clone];
  }

  generateWarningType(type) {
    switch (type) {
      case 'DEPOSIT':
        return 'Tiết kiệm';
        break;
      case 'CREDIT_CARD':
        return 'Thẻ tín dụng';
        break;
      case 'LOAN':
        return 'Tín dụng';
        break;
      default:
        return type;
        break;
    }
  }

  getTotalPay(row) {
    return (row.duNoTaiNgaySaoKe ? row.duNoTaiNgaySaoKe : 0)
      + (row.laiDuChi ? row.laiDuChi : 0)
      + (row.lai_PHAT_PHI_PHAT ? row.lai_PHAT_PHI_PHAT : 0)
  }

  generateClassWarningType(type) {
    console.log(type)
    switch (type) {
      case 'DEPOSIT':
        return 'light-sky-blue-tag';
        break;
      case 'CREDIT_CARD':
        return 'orange-tag';
        break;
      case 'LOAN':
        return 'purple-tag';
        break;
      default:
        return '';
        break;
    }
  }

}
