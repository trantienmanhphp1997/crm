import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGuardService } from 'src/app/core/services/role-guard.service';
import { FunctionCode, Scopes } from 'src/app/core/utils/common-constants';
import { WarningComponent } from './warning.component';


const routes: Routes = [
  {
    path: '',
    component: WarningComponent,
    canActivate: [RoleGuardService],
    data: {
      code: FunctionCode.WARNING,
      scope: Scopes.VIEW
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WarningRoutingModule { }
