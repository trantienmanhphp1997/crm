import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGuardService } from 'src/app/core/services/role-guard.service';
import { FunctionCode, Scopes, ScreenType } from 'src/app/core/utils/common-constants';
import { DeviceDetailComponent } from './components/device-detail/device-detail.component';
import { DeviceLeftContentComponent } from './components/device-left-content/device-left-content.component';
import { DeviceListComponent } from './components/device-list/device-list.component';

const routes: Routes = [
  // {
  //   path: '',
  //   component: DeviceLeftContentComponent,
  //   outlet: 'app-left-content',
  // },
  {
    path: 'device-list',
    data: {
      code: FunctionCode.DEVICE_MANAGEMENT,
    },
    canActivateChild: [RoleGuardService],
    children: [
      {
        path: '',
        component: DeviceListComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
      {
        path: 'update',
        component: DeviceDetailComponent,
        data: {
          scope: Scopes.UPDATE,
          type: ScreenType.Update,
        },
      },
      {
        path: 'detail',
        component: DeviceDetailComponent,
        data: {
          scope: Scopes.VIEW,
          type: ScreenType.Detail,
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeviceSmartRoutingModule {}
