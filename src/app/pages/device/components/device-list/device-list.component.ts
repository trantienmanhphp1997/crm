import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';
import { CommonCategory, FunctionCode, maxInt32, Scopes } from 'src/app/core/utils/common-constants';
import { global } from '@angular/compiler/src/util';
import { cleanDataForm } from 'src/app/core/utils/function';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { DeviceSmartService } from '../../services/device-smart.service';
import { Utils } from 'src/app/core/utils/utils';
import { FileService } from 'src/app/core/services/file.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.scss'],
  providers: [DatePipe],
})
export class DeviceListComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listData = [];
  pageable: Pageable;
  limit = global.userConfig.pageSize;
  paramSearch = {
    pageSize: this.limit,
    pageNumber: 0,
    scope: Scopes.VIEW,
    rsId: '',
  };
  formSearch = this.fb.group({
    userName: [''],
    rmCode: [''],
    appCode: [''],
  });
  checkAll = false;
  countCheckedOnPage = 0;
  @ViewChild('table') table: DatatableComponent;
  listSelected = [];
  listDateNoActive = [];
  listAppCode = [];
  onlyRM: boolean;

  constructor(
    injector: Injector,
    private deviceSmartService: DeviceSmartService,
    private fileService: FileService,
    private datePipe: DatePipe
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DEVICE_MANAGEMENT}`);
    this.paramSearch.rsId = this.objFunction?.rsId;
    this.prop = this.router?.getCurrentNavigation()?.extras?.state;
    this.isLoading = true;
    this.route.data.subscribe((data) => {
      this.onlyRM = data?.onlyRM;
      if (this.prop) {
        this.formSearch.controls.userName.setValue(this.prop?.params.userName);
        this.formSearch.controls.rmCode.setValue(this.prop?.params.rmCode);
      }
      if (this.onlyRM) {
        this.formSearch.controls.userName.setValue(this.currUser.username);
      }
      this.search(true);
    });
  }

  ngOnInit(): void {
    this.deviceSmartService.getAppCode(CommonCategory.APP_MANAGEMENT).subscribe((result) => {
      if (result && result.content.length > 0) {
        this.listAppCode = result.content.map((item) => {
          return {
            code: item.code,
            name: item.name,
          };
        });
        this.listAppCode.unshift({
          code: '',
          name: this.fields.all,
        });
      }
    });

    // this.search(true);
  }

  search(isSearch?: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.paramSearch.pageNumber = 0;
      const paramSearch = cleanDataForm(this.formSearch);
      Object.keys(paramSearch).forEach((key) => {
        this.paramSearch[key] = paramSearch[key];
      });
    }
    const params = this.paramSearch;
    this.deviceSmartService.search(params).subscribe(
      (result) => {
        if (result) {
          this.countCheckedOnPage = 0;
          this.listData = result.content || [];
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: this.limit,
          };
          this.isLoading = false;
        }
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.pageNumber = pageInfo.offset;
    this.search(false);
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      this.router.navigate([this.router.url, 'detail'], {
        skipLocationChange: true,
        state: { params: this.paramSearch },
        queryParams: {
          deviceId: item.deviceId,
          rmCode: item.rmCode,
          appCode: item.appCode,
          userName: item.userName,
        },
      });
    }
  }

  onCheckboxBlockFn(item) {
    let status = !item.status;
    item.status = status;
    this.deviceSmartService.activeDevice(this.objFunction?.rsId, Scopes.UPDATE, item).subscribe(
      (result) => {
        if (result === null) {
          this.messageService.success(this.notificationMessage.success);
          this.search(true);
        }
      },
      (e) => {
        if (e?.error?.code) {
          this.messageService.error(e?.error?.description);
          this.search(true);
        } else {
          this.messageService.error(this.notificationMessage.error);
        }
      }
    );
  }

  deleteDevice(row: any) {
    this.confirmService.confirm().then((res) => {
      if (res) {
        this.isLoading = true;
        this.deviceSmartService.deleteDevice(this.objFunction?.rsId, Scopes.DELETE, row).subscribe(
          () => {
            this.isLoading = false;
            this.search(true);
            this.messageService.success(this.notificationMessage.success);
          },
          (e) => {
            this.isLoading = false;
            if (e?.error) {
              this.messageService.error(e?.error?.description);
            } else {
              this.messageService.error(this.notificationMessage.error);
            }
          }
        );
      }
    });
  }
  update(item) {
    this.router.navigate([this.router.url, 'update'], {
      skipLocationChange: true,
      state: { params: this.paramSearch },
      queryParams: {
        deviceId: item.deviceId,
        rmCode: item.rmCode,
        appCode: item.appCode,
        userName: item.userName,
      },
    });
  }

  exportFile() {
    if (Utils.isArrayEmpty(this.listData)) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    this.isLoading = true;
    const params = this.paramSearch;
    let pageSizeOld = params.pageSize;
    params.pageSize = maxInt32;
    params.pageNumber = 0;
    params.rsId = this.objFunction?.rsId;
    this.deviceSmartService.exportDevice(params).subscribe(
      (res) => {
        if (Utils.isStringNotEmpty(res)) {
          this.download(res);
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
        this.paramSearch.pageSize = pageSizeOld;
      },
      () => {
        this.messageService.error(_.get(this.notificationMessage, 'export_error'));
        this.isLoading = false;
        this.paramSearch.pageSize = pageSizeOld;
      }
    );
  }

  download(fileId: string) {
    this.fileService.downloadFile(fileId, 'danh sach thiet bi.xlsx').subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }

  formatDateTime(time) {
    return this.datePipe.transform(time, 'dd-MM-yyyy HH:mm:ss') || '';
  }
}
