import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { FunctionCode, Scopes, ScreenType } from 'src/app/core/utils/common-constants';
import * as _ from 'lodash';
import { DeviceSmartService } from '../../services/device-smart.service';
import { DatePipe } from '@angular/common';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';

@Component({
  selector: 'app-device-detail',
  templateUrl: './device-detail.component.html',
  styleUrls: ['./device-detail.component.scss'],
  providers: [DatePipe],
})
export class DeviceDetailComponent extends BaseComponent implements OnInit {
  isLoading = false;
  isDetail = true;
  screenType: any;
  formSearch = this.fb.group({
    status: [''],
    phone: ['', CustomValidators.phoneMobileVN],
  });
  deviceId: any;
  rmCode: any;
  appCode: any;
  dataDetail: any;
  active: any;
  numberPhone: any;
  userName: any;

  constructor(
    injector: Injector,
    private datePipe: DatePipe,
    private deviceSmartService: DeviceSmartService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.DEVICE_MANAGEMENT}`);
    this.route?.data?.subscribe((data) => {
      this.screenType = data?.type;
    });
    this.deviceId = this.route.snapshot.queryParams.deviceId;
    this.rmCode = this.route.snapshot.queryParams.rmCode;
    this.appCode = this.route.snapshot.queryParams.appCode;
    this.userName = this.route.snapshot.queryParams.userName;
  }

  ngOnInit(): void {
    const param = {
      deviceId: this.deviceId,
      userName: this.userName,
      appCode: this.appCode,
      rsId: this.objFunction?.rsId,
      scope: Scopes.VIEW,
    };
    if (this.screenType !== ScreenType.Detail) {
      this.isDetail = false;
    }
    this.deviceSmartService.detailDevice(param).subscribe((result) => {
      if (result) {
        this.dataDetail = result || [];
        this.active = this.dataDetail.status;
        this.formSearch.controls.phone.setValue(this.dataDetail.phoneDevice);
      }
    });
  }

  save() {
    if (this.formSearch.valid) {
      const data = cleanDataForm(this.formSearch);
      this.dataDetail.status = this.active;
      this.dataDetail.phoneDevice = data.phone;
      this.deviceSmartService.activeDevice(this.objFunction?.rsId, Scopes.UPDATE, this.dataDetail).subscribe(
        (result) => {
          if (result === null) {
            this.messageService.success(this.notificationMessage.success);
            this.back();
          }
        },
        (e) => {
          if (e?.error?.code) {
            this.messageService.error(e?.error?.description);
          } else {
            this.messageService.error(this.notificationMessage.error);
          }
          this.back();
        }
      );
    } else {
      validateAllFormFields(this.formSearch);
    }
  }

  formatDateTime(time) {
    return this.datePipe.transform(time, 'dd-MM-yyyy HH:mm:ss') || '';
  }
}
