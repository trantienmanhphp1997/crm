import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceLeftContentComponent } from './device-left-content.component';

describe('DeviceLeftContentComponent', () => {
  let component: DeviceLeftContentComponent;
  let fixture: ComponentFixture<DeviceLeftContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceLeftContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceLeftContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
