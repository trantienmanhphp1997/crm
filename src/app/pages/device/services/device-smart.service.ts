import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DeviceSmartService {

  constructor(private http: HttpClient, private client: HttpClient) { }


  // searchKpiRmProcess(params): Observable<any> {
  //   return this.http.get(`${environment.url_endpoint_category}/`, params);
  // }


  search(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/device/get-active-list`, { params });
  }

  activeDevice(rsId,action,params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/device/update?rsId=${rsId}&scope=${action}`, params);
  }

  deleteDevice(rsId,action,params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_category}/device/delete?rsId=${rsId}&scope=${action}`, params);
  }

  detailDevice(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/device/detail`, { params });
  }

  exportDevice(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/device/export-active-list`, {params,responseType:'text'});
  }

  dowloadFile(params): Observable<any> {
    return this.client
      .get(`${environment.url_endpoint_rm}/files/download/${params}`, {
        responseType: 'arraybuffer',
        observe: 'response',
      })
  }
  getAppCode(param: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/commons?commonCategoryCode=${param}`);
  }
}
