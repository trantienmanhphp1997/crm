import { TestBed } from '@angular/core/testing';

import { DeviceSmartService } from './device-smart.service';

describe('DeviceSmartService', () => {
  let service: DeviceSmartService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DeviceSmartService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
