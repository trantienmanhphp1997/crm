import { CommonModule } from '@angular/common';

import { DeviceSmartRoutingModule } from './device-smart-routing.module';
import { DeviceListComponent, DeviceDetailComponent, DeviceLeftContentComponent } from './components';
import { SharedModule } from '../../shared/shared.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { RadioButtonModule } from 'primeng/radiobutton';
import { CheckboxModule } from 'primeng/checkbox';
import { InputNumberModule } from 'primeng/inputnumber';
import { NgxSelectModule } from 'ngx-select-ex';
import { TableModule } from 'primeng/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MultiSelectModule } from 'primeng/multiselect';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { BadgeModule } from 'primeng/badge';
import { ChipModule } from 'primeng/chip';
import { AvatarModule } from 'primeng/avatar';
import { TimelineModule } from 'primeng/timeline';
import { MenuModule } from 'primeng/menu';

const COMPONENTS = [DeviceListComponent, DeviceDetailComponent, DeviceLeftContentComponent];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [
    CommonModule,
    DeviceSmartRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    // HttpClientModule,
    SharedModule,
    NgbModule,
    TranslateModule,
    NgxDatatableModule,
    InputTextModule,
    InputTextareaModule,
    DropdownModule,
    CalendarModule,
    RadioButtonModule,
    CheckboxModule,
    InputNumberModule,
    NgxSelectModule,
    TableModule,
    MatTabsModule,
    MultiSelectModule,
    AvatarModule,
    ButtonModule,
    RippleModule,
    BadgeModule,
    ChipModule,
    TimelineModule,
    MenuModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class DeviceSmartModule {}
