import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend } from '@angular/common/http';

import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';
import { Utils } from '../../../core/utils/utils';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BranchApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_category}/branches`);
  }

  getBranchesByFunction(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/branches/branchesByUser`, { params });
  }

  selectize(rsId: string = '', scope: string = '') {
    return this.get(`branchesByUser`, { rsId: rsId, scope: scope });
  }
}
