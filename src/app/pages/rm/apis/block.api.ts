import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend } from '@angular/common/http';

import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BlockApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_category}/blocks`);
  }

  selectize(): Observable<any> {
    return this.http.get(`${this.baseURL}/findBlockByRM`);
  }
}
