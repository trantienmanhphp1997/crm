import { RmApi } from './rm.api';
import { BranchApi } from './branch.api';
import { BlockApi } from './block.api';
import { TitleGroupApi } from './title-group.api';
import { LevelApi } from './level.api';
import { RmBlockApi } from './rm-block.api';
import { RmGroupApi } from './rm-group.api';
import { RmFluctuateApi } from './rm-fluctuate.api';
import { CommonApi } from './common.api';
import { RmUnassignApi } from './rm-unassign.api';
import {HuddleGroupApi} from "./huddle-group.api";

export { RmApi } from './rm.api';
export { BranchApi } from './branch.api';
export { BlockApi } from './block.api';
export { TitleGroupApi } from './title-group.api';
export { LevelApi } from './level.api';
export { RmBlockApi } from './rm-block.api';
export { RmGroupApi } from './rm-group.api';
export { RmFluctuateApi } from './rm-fluctuate.api';
export { CommonApi } from './common.api';
export { RmUnassignApi } from './rm-unassign.api';

export const APIS = [
  RmApi,
  BranchApi,
  BlockApi,
  TitleGroupApi,
  LevelApi,
  RmBlockApi,
  RmGroupApi,
  RmFluctuateApi,
  CommonApi,
  RmUnassignApi,
  HuddleGroupApi,
];
