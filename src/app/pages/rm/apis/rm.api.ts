import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';
import { Observable, of } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class RmApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_rm}/employees`);
  }

  checkRmAssign(hrsCode): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_rm}/employees/check-not-allowed-to-choose-to-assign-rm/${hrsCode}`
    );
  }

  checkRmNTSNotAssign(data: string[]): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/employees/getRmNTSByRmCodes`, data);
  }

  getRmAssignOrFollow(isAssignee): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/getAssigneeToFollower/${isAssignee}`);
  }
  getRmAssign(param,isAssignee): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/employees/getAssigneeToFollower/v2/${isAssignee}`,param);
  }
  getAllRmInBranch(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/getAllRmInBranch`);
  }

  createFileExcel(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/employees/exportsRm`, params, { responseType: 'text' });
  }

  dowloadTemplate(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/template`, { responseType: 'text' });
  }

  importFile(param): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/employees/import`, param);
  }

  checkImportSuccess(fileId): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/checkProcessImport?fileId=${fileId}`);
  }

  searchDataError(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/findDataError`, { params });
  }

  searchDataSuccess(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/findDataSuccess`, { params });
  }

  importFileUpdateHris(param): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/employees/import-change-hrs`, param, {
      responseType: 'text',
    });
  }

  searchDataErrorSuccess(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/view-data-hrs-change`, { params });
  }

  createRmCode(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/employees/createRmCode`, data);
  }

  updateRmT24(data, rmCode: string): Observable<any> {
    return this.http.put(`${environment.url_endpoint_rm}/employees/updateRmT24?rmCode=${rmCode}`, data);
  }

  getRmInfoByHrsCode(hrsCode: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/hris-employee-info?hrsCode=${hrsCode}`);
  }

  getRmInfoByRmCode(rmCode: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/employee-info-t24?rmCode=${rmCode}`);
  }

  checkAssignUB(hrsCode): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/check-assign-ub/${hrsCode}`);
  }

  getRMSale(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/employees/findAllRmSale`, params);
  }

  getRMGroupCbql(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/employees/findAllCBQLByDivision`, params);
  }

  createTargetSaleConfig(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/target-sale-config`, body);
  }

  getAllTargetSaleConfig(body): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/target-sale-config/findAll`, body);
  }

  getTargetSaleConfig(paramKey): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/target-sale-config/${paramKey}`);
  }

  deleteTargetSaleConfig(id): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_rm}/target-sale-config/${id}`);
  }

  updateTargetSaleConfig(data): Observable<any> {
    return this.http.put(`${environment.url_endpoint_rm}/target-sale-config`, data);
  }

  findRMInfo(hrsCode): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/${hrsCode}`);
  }

  getDetailRMByRmCode(rmCode : string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/rm-code/${rmCode}`)
  }

  exportRmblock(body){
    return this.http.post(`${environment.url_endpoint_rm}/employees/export-rm`, body, { responseType: 'text' });
  }
}
