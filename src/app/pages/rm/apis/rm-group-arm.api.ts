import { Observable } from 'rxjs';
import { HttpWrapper } from '../../../core/apis/http-wapper';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RmGroupArmApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_rm}/arm`);
  }

  searchGroupById(id, params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/arm/${id}`, { params });
  }

  search(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/arm/search`, { params });
  }

  exportListGroup(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/arm/exportGroup`, { params, responseType: 'text' });
  }

  exportGroupById(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/arm/exporthistorymember/${params.id}`, {
      params,
      responseType: 'text',
    });
  }
}
