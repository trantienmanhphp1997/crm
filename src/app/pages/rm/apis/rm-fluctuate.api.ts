import { HttpWrapper } from './../../../core/apis/http-wapper';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class RmFluctuateApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_rm_relationship}/fluctuations`);
  }

  search(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm_relationship}/fluctuations/findAll`, params);
  }

  approve(id, data): Observable<any> {
    return this.http.put(`${environment.url_endpoint_rm_relationship}/fluctuations/approve/${id}`, data);
  }

  reject(id, data): Observable<any> {
    return this.http.put(`${environment.url_endpoint_rm_relationship}/fluctuations/reject/${id}`, data);
  }

  checkBeforeCreate(hrsCode, params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm_relationship}/fluctuations/checkBeforeCreate/${hrsCode}`, {
      params,
    });
  }

  checkFile(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm_relationship}/fluctuations/checkFileUpload`, data);
  }

  uploadFile(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/files/upload`, data, { responseType: 'text' });
  }

  updateT24(id): Observable<any> {
    return this.http.put(`${environment.url_endpoint_rm_relationship}/fluctuations/updateBranchT24/${id}`, {});
  }

  createFileExcel(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm_relationship}/fluctuations/exports`, params, {
      responseType: 'text',
    });
  }

  checkRestore(id): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm_relationship}/fluctuations/checkRestoreManual/${id}`, {});
  }

  restore(id): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm_relationship}/fluctuations/restoreManual/${id}`, {});
  }

  getFileInfo(id: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/files/getById/${id}`);
  }
}
