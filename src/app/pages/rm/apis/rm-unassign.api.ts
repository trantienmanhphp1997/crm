import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class RmUnassignApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_rm}/employee-account-types`);
  }

  createFile(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/employee-account-types/exportRmUnassignedGroupList`, data, { responseType: 'text' });
  }
}