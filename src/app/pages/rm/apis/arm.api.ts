import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpWrapper } from 'src/app/core/apis/http-wapper';
import { environment } from 'src/environments/environment';
import { Utils as util } from 'src/app/core/utils/utils';

@Injectable({
  providedIn: 'root'
})
export class ArmService extends HttpWrapper {

  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_rm}/arm`);
  }

  getList(params): Observable<any> {
    return this.post('getaRMUnassignedGroupList', params);
  }

  createExcelFile(params): Observable<any> {
    return this.createFile('exportArmUnassignedGroupList', params, { responseType: 'text' });
  }
}
