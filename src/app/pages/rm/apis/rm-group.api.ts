import { Observable } from 'rxjs';
import { HttpWrapper } from './../../../core/apis/http-wapper';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable()
export class RmGroupApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_rm}/groups`);
  }

  searchGroupById(id, params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/groups/${id}`, { params });
  }

  search(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/groups/findAll`, params);
  }

  createExcelFile(params): Observable<any> {
    return this.createFile('exportListRmGroup', params, { responseType: 'text' });
  }
}
