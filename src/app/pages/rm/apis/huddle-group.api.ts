import {HttpWrapper} from "../../../core/apis/http-wapper";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";

@Injectable()
export class HuddleGroupApi extends HttpWrapper{
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_rm}/huddleGroup`);
  }
  search(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/huddleGroup/findAll`, params);
  }
  deleteGroup(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/huddleGroup/delGroup`, params);
  }
  exportGroup(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/huddleGroup/exportGroup`, params, { responseType: 'text' });
  }
  createGroup(params): Observable<any>{
    return this.http.post(`${environment.url_endpoint_rm}/huddleGroup/createGroup`,params);
  }
  detailGroup(params): Observable<any>{
    return this.http.post(`${environment.url_endpoint_rm}/huddleGroup/findAllMember`,params);
  }
  updateGroup(params):Observable<any>{
    return this.http.post(`${environment.url_endpoint_rm}/huddleGroup/updateGroup`,params);
  }
}
