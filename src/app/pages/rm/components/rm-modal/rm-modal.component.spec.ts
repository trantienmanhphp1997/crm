import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmModalComponent } from './rm-modal.component';

describe('RmModalComponent', () => {
  let component: RmModalComponent;
  let fixture: ComponentFixture<RmModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
