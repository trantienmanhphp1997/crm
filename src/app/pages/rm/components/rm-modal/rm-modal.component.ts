import { forkJoin, Observable } from 'rxjs';
import { Component, OnInit, Injector, ViewChild, ViewEncapsulation, HostBinding, Input } from '@angular/core';
import { Validators } from '@angular/forms';
import { DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { global } from '@angular/compiler/src/util';
import { Division, FunctionCode, Scopes, SessionKey } from 'src/app/core/utils/common-constants';
import { RmService } from '../../services';
import { Utils } from 'src/app/core/utils/utils';
import { cleanDataForm } from 'src/app/core/utils/function';
import { BlockApi, RmApi } from '../../apis';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoryService } from 'src/app/pages/system/services/category.service';

@Component({
  selector: 'app-rm-modal',
  templateUrl: './rm-modal.component.html',
  styleUrls: ['./rm-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class RmModalComponent extends BaseComponent implements OnInit {
  limit = global.userConfig.pageSize;
  isLoading = false;
  @ViewChild('tablerm') public tablerm: DatatableComponent;
  @HostBinding('class.list__rm-content') appRightContent = true;
  @Input() dataSearch: any;
  @Input() isManager: boolean;
  @Input() isRm: boolean;
  @Input() rsId: string;
  @Input() typeAssignKHTN: string;
  @Input() isRmSale: boolean;
  @Input() branchCodes: string[];
  @Input() block: string;
  isDisableBranch = false;
  isDisableBlock = false;
  showSearchBasic = true;
  obj: any;
  branch_codes: Array<any> = [];
  search_form = this.fb.group({
    employeeCode: ['', Validators.maxLength(20)],
    employeeId: ['', Validators.maxLength(20)],
    fullName: ['', Validators.maxLength(120)],
    email: ['', Validators.maxLength(50)],
    mobile: ['', Validators.maxLength(30)],
    crmIsActive: ['true'],
    rmBlock: [''],
    productGroup: [''],
    divisionCode: [''],
  });
  search_value: string;
  show: boolean;
  facilities: Array<any>;
  grades: Array<any>;
  customer_ranges: Array<any> = [];
  facilityCode: Array<string>;
  prevSearch: string;
  // model = [];
  models = [];
  count: number; // total
  offset: number; // page
  params: any = {
    page: 0,
    size: global?.userConfig?.pageSize,
    crmIsActive: true,
    scope: Scopes.VIEW,
    rsId: '',
  };
  searchType: boolean;

  checkAll = false;
  listSelected = [];
  listHrsCode = [];
  listHrsCodeOld = [];
  countCheckedOnPage = 0;
  @Input() config: any;
  showCheckBox = true;

  constructor(
    injector: Injector,
    private service: RmService,
    private api: RmApi,
    private blockApi: BlockApi,
    private categoryService: CategoryService,
    private modalActive: NgbActiveModal
  ) {
    super(injector);
    this.obj = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.params.rsId = this.obj?.rsId;
  }

  ngOnInit() {
    this.isLoading = true;
    this.customer_ranges.push({ code: '', name: this.fields.all });
    this.customer_ranges.push({ code: 'true', name: this.fields.effective });
    this.customer_ranges.push({ code: 'false', name: this.fields.expire });
    this.params.rsId = _.get(this.obj, 'rsId');
    if (this.dataSearch) {
      Object.keys(this.dataSearch).forEach((key) => {
        if (this.dataSearch[key].value) {
          this.search_form.get(key)?.setValue(this.dataSearch[key].value);
        }
        if (this.dataSearch[key].disabled) {
          this.search_form.get(key)?.disable();
        }
      });
    }
    const divisionOfUser = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM);
    this.grades = _.map(divisionOfUser, (x) => ({ ...x, value_to_search: `${x.code} - ${x.name}` }));
    if (this.typeAssignKHTN === 'DN') {
      this.grades = _.filter(this.grades, (x) => x.code === Division.CIB || x.code === Division.SME);
    } else if (this.typeAssignKHTN === 'CN') {
      this.grades = _.filter(this.grades, (x) => x.code === Division.INDIV);
    }
    if (this.grades?.length > 0) {
      this.grades = [
        {
          id: '',
          code: '',
          value_to_search: _.get(this.fields, 'all'),
        },
        ...this.grades,
      ];
    }

    this.categoryService.getBranchesOfUser(this.params.rsId, this.params.scope).subscribe(
      (branchesOfUser) => {
        this.facilities = branchesOfUser || [];
        if (this.branchCodes || this.block) {
          this.showSearchBasic = false;
          if (this.block === 'INDIV') {
            this.isDisableBlock = true;
            this.search_form.controls.rmBlock.setValue(this.block);
          }
          if (this.branchCodes) {
            this.isDisableBranch = true;
            this.facilityCode = this.branchCodes;
          }
          this.searchType = true;
          this.show_advance_search();
          this.reload(true);
        } else {
          this.searchType = false;
          this.facilityCode = branchesOfUser?.map((item) => item.code);
          this.reload();
        }
      },
      () => {
        this.isLoading = false;
      }
    );
    this.listHrsCodeOld = [...this.listHrsCode];
    if (this.config?.selectionType === SelectionType.single) {
      this.showCheckBox = false;
    }
  }

  show_advance_search() {
    this.show = true;
    this.search_value = undefined;
  }

  show_quick_search() {
    this.show = false;
    this.facilityCode = [];
  }

  changeBranch(event) {}

  paging(event) {
    if (!this.isLoading) {
      this.params.page = _.get(event, 'page') ? _.get(event, 'page') - 1 : 0;
      this.reload();
    }
  }

  searchBasic() {
    this.searchType = false;
    this.reload(true);
  }

  searchAdvance() {
    this.searchType = true;
    this.reload(true);
  }

  reload(isPaging?: boolean) {
    let params: any;
    if (isPaging) {
      this.params.page = 0;
      params = { ...this.params };
      if (this.searchType) {
        const data = cleanDataForm(this.search_form);
        Object.keys(data).forEach((key) => {
          params[key] = data[key];
        });
        params.branchCodes = this.facilityCode;
      } else {
        this.search_value = Utils.trimNullToEmpty(this.search_value);
        params.search = this.search_value;
      }
    } else {
      params = this.prevSearch ? this.prevSearch : this.params;
      params.page = this.params.page;
      params.size = this.params.size;
    }
    this.isLoading = true;
    if (!this.isManager) {
      params.isRmGroup = this.dataSearch?.isRmGroup;
    } else {
      params.rmBlock = 'INDIV';
    }
    _.set(params, 'manager', this.isManager);
    if (this.dataSearch) {
      _.forEach(this.dataSearch, (item, key) => {
        params[key] = _.has(item, 'value') ? item.value : item;
      });
    }
    if (this.typeAssignKHTN) {
      params.typeAssignKHTN = this.typeAssignKHTN;
    }
    let apiSearch: Observable<any>;
    if (!this.isRmSale) {
      apiSearch = this.api.post('findAll', params);
    } else {
      params.divisionCode = '';
      apiSearch = this.api.getRMSale(params);
    }
    apiSearch.subscribe(
      (response) => {
        this.prevSearch = params;
        this.models = _.get(response, 'content');
        this.count = _.get(response, 'totalElements');
        this.offset = _.get(response, 'number');
        this.countCheckedOnPage = 0;
        for (const item of this.models) {
          if (
            this.listHrsCode.findIndex((hrsCode) => {
              return hrsCode === item.hrisEmployee.employeeId;
            }) > -1
          ) {
            this.listSelected.push(item);
            this.countCheckedOnPage += 1;
          }
        }
        this.checkAll = this.models.length > 0 && this.countCheckedOnPage === this.models.length;
        this.isLoading = false;
      },
      () => {
        this.messageService.error('Kết nối hệ thống không thành công. Vui lòng thử lại sau');
        this.isLoading = false;
      }
    );
  }

  onCheckboxRmAllFn(isChecked) {
    this.checkAll = isChecked;
    if (!isChecked) {
      this.countCheckedOnPage = 0;
    } else {
      this.countCheckedOnPage = this.models.length;
    }
    for (const item of this.models) {
      item.isChecked = isChecked;
      if (isChecked) {
        if (
          this.listHrsCode.findIndex((hrsCode) => {
            return hrsCode === item.hrisEmployee?.employeeId;
          }) === -1
        ) {
          this.listHrsCode.push(item.hrisEmployee?.employeeId);
          this.listSelected.push(item);
        }
      } else {
        this.listHrsCode = this.listHrsCode.filter((hrsCode) => {
          return hrsCode !== item.hrisEmployee?.employeeId;
        });
        this.listSelected = this.listSelected.filter((itemChoose) => {
          return itemChoose.hrisEmployee?.employeeId !== item.hrisEmployee?.employeeId;
        });
      }
    }
  }

  onCheckboxRmFn(item, isChecked) {
    if (isChecked) {
      this.listSelected.push(item);
      this.listHrsCode.push(item.hrisEmployee?.employeeId);
      this.countCheckedOnPage += 1;
    } else {
      this.listSelected = this.listSelected.filter((itemSelected) => {
        return itemSelected.hrisEmployee?.employeeId !== item.hrisEmployee?.employeeId;
      });
      this.listHrsCode = this.listHrsCode.filter((hrsCode) => {
        return hrsCode !== item.hrisEmployee?.employeeId;
      });
      this.countCheckedOnPage -= 1;
    }
    this.checkAll = this.countCheckedOnPage === this.models.length;
  }

  isChecked(item) {
    return (
      this.listHrsCode.findIndex((hrsCode) => {
        return hrsCode === item.hrisEmployee.employeeId;
      }) > -1
    );
  }

  choose() {
    const listRemove = this.listHrsCodeOld?.filter(
      (codeOld) => this.listHrsCode?.findIndex((code) => code === codeOld) === -1
    );
    const data = {
      listSelected: this.listSelected,
      listRemove,
    };
    this.modalActive.close(data);
  }

  closeModal() {
    this.modalActive.close(false);
  }

  handleChangePageSize(event) {
    this.params.page = 0;
    this.params.size = event;
    this.limit = event;
    this.reload();
  }
}
