import { global } from '@angular/compiler/src/util';
import { Component, OnInit, Injector } from '@angular/core';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { ARM_STATUS_LIST, EXPORT_LIMIT, FunctionCode, maxInt32, Scopes } from 'src/app/core/utils/common-constants';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { Utils } from 'src/app/core/utils/utils';
import { RmGroupArmApi } from '../../apis/rm-group-arm.api';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { FileService } from 'src/app/core/services/file.service';
import * as moment from 'moment';


@Component({
  selector: 'app-arm-management',
  templateUrl: './arm-management.component.html',
  styleUrls: ['./arm-management.component.scss'],
})
export class ArmManagementComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listData = [];
  listBranch = [];
  limit = global.userConfig.pageSize;
  paramSearch = {
    size: this.limit,
    page: 0,
    search: '',
    rsId: '',
    scope: Scopes.VIEW,
    isActive: 1
  };
  textSearch = '';
  pageable: Pageable;
  statusList = ARM_STATUS_LIST;
  EXPORT_LIMIT = EXPORT_LIMIT;

  constructor(
    injector: Injector,
    private rmGroupArmApi: RmGroupArmApi,
    private categoryService: CategoryService,
    private fileService: FileService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.GROUP_ARM}`);
    this.paramSearch.rsId = this.objFunction?.rsId;
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
    if (this.prop) {
      this.paramSearch = this.prop.paramSearch;
      this.textSearch = this.paramSearch.search;
    }
    const rsIdOfRm = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`)?.rsId;
    this.paramSearch.rsId = rsIdOfRm;
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.categoryService
      .searchBranches({ page: 0, size: maxInt32 })
      .pipe(catchError(() => of(undefined)))
      .subscribe((data) => {
        this.listBranch = data?.content;
        this.search(true);
      });
  }

  search(isSearch: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.paramSearch.page = 0;
      this.textSearch = this.textSearch.trim();
      this.paramSearch.search = this.textSearch;
    }
    this.rmGroupArmApi.search(this.paramSearch).subscribe(
      (result) => {
        if (result) {
          this.listData = _.orderBy(result?.content || [], [
              item => moment(new Date(item.createdDate), 'DD/MM/YYYY').startOf('day').valueOf(),
              item => moment(new Date(item.updatedDate), 'DD/MM/YYYY').startOf('day').valueOf()],
            ['desc', 'desc']
          );
          this.listData.forEach((item) => {
            item.branchName = _.find(this.listBranch, (i) => i.code === item.branchCode)?.name;
          });
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: this.limit,
          };
          this.isLoading = false;
        }
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    if (!this.isLoading) {
      this.paramSearch.page = pageInfo.offset;
      this.search(false);
    }
  }

  create() {
    this.router.navigate([this.router.url, 'create'], {
      state: { paramSearch: this.paramSearch },
      skipLocationChange: true,
    });
  }

  update(item) {
    this.router.navigate([this.router.url, 'update', item.id], {
      state: { paramSearch: this.paramSearch },
      skipLocationChange: true,
    });
  }

  onActive($event) {
    const type = _.get($event, 'type');
    const id = _.get($event, 'row.id');
    if (type === 'dblclick') {
      if (Utils.isStringNotEmpty(id)) {
        this.router.navigate([this.router.url, 'view', id], {
          state: { paramSearch: this.paramSearch },
          skipLocationChange: true,
        });
      }
    }
  }

  delete(item) {
    this.confirmService.confirm().then((res) => {
      if (res) {
        this.rmGroupArmApi.delete(item.id).subscribe(
          () => {
            this.search(false);
            this.messageService.success(this.notificationMessage.success);
          },
          (e) => {
            if (e?.error) {
              this.messageService.warn(e?.error?.description);
              this.search(false);
            } else {
              this.messageService.error(this.notificationMessage.error);
            }
          }
        );
      }
    });
  }

  exportFile() {
    if (this.pageable.totalElements === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    this.isLoading = true;

    const params = { ...this.paramSearch, ...this.EXPORT_LIMIT };

    this.rmGroupArmApi
      .exportListGroup(params)
      .pipe(catchError(() => of(undefined)))
      .subscribe((fileId) => {
        if (Utils.isStringNotEmpty(fileId)) {
          this.fileService.downloadFile(fileId, 'list_arm.xlsx').subscribe((res) => {
            this.isLoading = false;
            if (!res) {
              this.messageService.error(this.notificationMessage.error);
            }
          });
        } else {
          this.messageService.error(this.notificationMessage?.export_error || '');
          this.isLoading = false;
        }
      });
  }
}
