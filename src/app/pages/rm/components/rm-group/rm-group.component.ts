import { DatatableComponent } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { EXPORT_LIMIT, FunctionCode, maxInt32, RM_GROUP_STATUS_LIST, Scopes } from 'src/app/core/utils/common-constants';
import { BaseComponent } from 'src/app/core/components/base.component';
import { RmGroupApi } from '../../apis';
import * as _ from 'lodash';
import { Utils } from 'src/app/core/utils/utils';
import { FileService } from 'src/app/core/services/file.service';

declare const $: any;

@Component({
  selector: 'app-rm-group',
  templateUrl: './rm-group.component.html',
  styleUrls: ['./rm-group.component.scss'],
})
export class RmGroupComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listData = [];
  limit = global.userConfig.pageSize;
  paramSearch = {
    size: this.limit,
    page: 0,
    search: '',
    rsId: '',
    scope: Scopes.VIEW,
    isActive: 1
  };
  textSearch = '';
  tooltip: any;
  pageable: Pageable;
  isSearch = false;
  isFisrt = true;
  prop: any;
  messages = global.messageTable;
  @ViewChild('table') table: DatatableComponent;

  statusList = RM_GROUP_STATUS_LIST;
  EXPORT_LIMIT = EXPORT_LIMIT;

  constructor(
    injector: Injector,
    private rmGroupApi: RmGroupApi,
    private fileService: FileService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_GROUP}`);
    const rsIdOfRm = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`)?.rsId;
    this.paramSearch.rsId = rsIdOfRm;
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
    if (this.prop) {
      this.paramSearch = this.prop.paramSearch;
      this.textSearch = this.paramSearch.search;
    }
    //
  }

  ngOnInit(): void {
    this.search(true);
  }

  isClickSearch() {
    this.isSearch = false;
  }

  search(isSearch: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.paramSearch.page = 0;
      this.isSearch = isSearch;
      this.textSearch = this.textSearch.trim();
      this.paramSearch.search = this.textSearch;
    }
    this.rmGroupApi.search(this.paramSearch).subscribe(
      (result) => {
        if (result) {
          this.listData = result.content;
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: this.limit,
          };
          this.isLoading = false;
          this.isFisrt = false;
        }
      },
      () => {
        this.isLoading = false;
        this.isFisrt = false;
      }
    );
  }

  setPage(pageInfo) {
    this.paramSearch.page = pageInfo.offset;
    if (!this.isFisrt) {
      this.search(false);
    }
  }

  create() {
    this.router.navigate([this.router.url, 'create'], { state: { paramSearch: this.paramSearch } });
  }

  update(item) {
    this.router.navigate([this.router.url, 'update', item.id], {
      state: { paramSearch: this.paramSearch },
      skipLocationChange: true,
    });
  }

  onActive($event) {
    const type = _.get($event, 'type');
    const id = _.get($event, 'row.id');
    if (type === 'dblclick') {
      if (Utils.isStringNotEmpty(id)) {
        this.router.navigate([this.router.url, 'detail', id], {
          state: { paramSearch: this.paramSearch },
          skipLocationChange: true,
        });
      }
    }
  }

  delete(item) {
    this.confirmService.confirm().then((res) => {
      if (res) {
        this.rmGroupApi.delete(item.id).subscribe(
          () => {
            this.search(false);
            this.messageService.success(this.notificationMessage.success);
          },
          (e) => {
            if (e?.error) {
              this.messageService.warn(e?.error?.description);
              this.search(false);
            } else {
              this.messageService.error(this.notificationMessage.error);
            }
          }
        );
      }
    });
  }

  exportFile() {

    if (Utils.isArrayEmpty(this.listData)) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    this.isLoading = true;
    const params = { ...this.paramSearch, ...this.EXPORT_LIMIT };

    this.rmGroupApi.createExcelFile(params).subscribe(
      (res) => {
        if (Utils.isStringNotEmpty(res)) {
          this.download(res);
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(_.get(this.notificationMessage, 'export_error'));
        this.isLoading = false;
      }
    );
  }

  download(fileId: string) {
    const filename = `rm_group_${new Date().getTime()}`;
    this.fileService.downloadFile(fileId, `${filename}.xlsx`).subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(_.get(this.notificationMessage, 'error'));
      }
    });
  }
}
