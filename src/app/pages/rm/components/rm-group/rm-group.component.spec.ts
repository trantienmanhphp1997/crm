import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmGroupComponent } from './rm-group.component';

describe('RmGroupComponent', () => {
  let component: RmGroupComponent;
  let fixture: ComponentFixture<RmGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
