import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnDestroy,
  ChangeDetectorRef,
  HostBinding,
  ViewChildren,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import _ from 'lodash';
import { RmService } from '../../services';
import { Utils } from '../../../../core/utils/utils';
import { ViewImageComponent } from '../view-image/view-image.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FunctionCode } from 'src/app/core/utils/common-constants';
import { SessionService } from 'src/app/core/services/session.service';
import { RmApi } from '../../apis';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-rm-detail',
  templateUrl: './rm-detail.component.html',
  styleUrls: ['./rm-detail.component.scss'],
})
export class RmDetailComponent implements OnInit, OnDestroy {
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private service: RmService,
    private modalService: NgbModal,
    private router: Router,
    private ref: ChangeDetectorRef,
    private sessionService: SessionService,
    private rmApi: RmApi
  ) {
    this.isLoading = true;
    const obj = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.isUpdate = _.get(obj, 'update');
    this.hrsCode = this.route.snapshot.paramMap.get('code');
    this.rmCode = this.route.snapshot.queryParams?.rmCode;
    if (Utils.isStringNotEmpty(_.get(this.model, 'image'))) {
      this.hyper_link = `${this.model?.hrisEmployee?.employeeId}_${this.model?.t24Employee?.employeeCode}_${this.model?.hrisEmployee?.userName}`;
    }
    const navigation = this.router?.getCurrentNavigation();
    const state = navigation?.extras?.state;
    this.param = state;
    this.tab_index = state?.tabIndex || 0;
  }
  isRm360Detail = true;
  @HostBinding('class.app__right-content') appRightContent = true;
  hrsCode: string;
  rmCode: string;
  hyper_link: string;
  model: any;
  emodel: any;
  roles: Array<string>;
  isUpdate: boolean;
  facility_name: string;
  form = this.fb.group({});

  is_show = true;
  is_edit: boolean;
  is_show_form: boolean;
  @Output() emittedPost = new EventEmitter();
  tab_index = 0;
  param: any;
  isLoading = false;
  @ViewChildren('rmCustomers') rmCustomers: any;

  view() {
    const updateModal = this.modalService.open(ViewImageComponent, {
      scrollable: true,
    });
    updateModal.componentInstance.data = this.model?.image;
    updateModal.componentInstance.hyper_link = this.hyper_link;
  }

  edit_form() {
    this.is_edit = !this.is_edit;
    if (this.is_edit) {
      this.emodel = this.model;
    }
  }

  show() {
    this.is_show = true;
  }

  close() {
    this.is_show = false;
  }

  onTabChanged(e) {
    if (this.isLoading) {
      return;
    }
    this.tab_index = _.get(e, 'index');
  }

  show_form() {
    this.is_show_form = !this.is_show_form;
  }

  collaps() {
    this.is_show = !this.is_show;
  }

  update() {
    this.is_edit = !this.is_edit;
  }

  cancel() {
    this.is_edit = !this.is_edit;
  }

  ngOnInit() {
    if (this.param?.rm360Infos) {
      this.rmCode = this.param?.rm360Infos[0]?.rmCode;
    }
    this.service.set_profile(this.model);
    this.rmApi
      .getByCode(this.hrsCode)
      .pipe(catchError(() => of(undefined)))
      .subscribe((data) => {
        if (data) {
          this.model = data;
        }
        this.isLoading = false;
      });
  }

  ngOnDestroy() {
    this.service.set_profile(undefined);
  }

  back() {
    this.router.navigate([`/rm360/rm-manager`], { state: this.param });
  }

  showLoading(event) {
    // this.isLoading = event;
    // this.ref.detectChanges();
  }
}
