import { Component, Injector, ChangeDetectorRef, ViewEncapsulation, ViewChild } from '@angular/core';
import { BaseTableComponent } from '../../../../shared/base-table.component';
import { LevelApi } from '../../apis';
import { FormBuilder } from '@angular/forms';
import { maxInt32, Scopes, FunctionCode } from 'src/app/core/utils/common-constants';
import { ExportExcelService } from 'src/app/core/services/export-excel.service';
import * as _ from 'lodash';
import { RmService } from '../../services';
import { Utils } from '../../../../core/utils/utils';
import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ConfirmDialogComponent } from '../../../../shared/components/confirm-dialog/confirm-dialog.component';
import { AppFunction } from 'src/app/core/interfaces/app-function.interface';
import { SessionService } from 'src/app/core/services/session.service';

@Component({
  selector: 'app-level',
  templateUrl: './level.component.html',
  styleUrls: ['./level.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class LevelComponent extends BaseTableComponent<LevelApi> {
  formSearch = this.fb.group({
    code: [''],
    name: [''],
    blockCode: [''],
    titleGroupId: [''],
    isActive: ['true'],
  });
  isSearch = true;

  constructor(
    injjector: Injector,
    api: LevelApi,
    private service: RmService,
    private ref: ChangeDetectorRef,
    private fb: FormBuilder,
    private exportExcelService: ExportExcelService,
    private sessionService: SessionService
  ) {
    super(injjector, api);
    this.isLoading = true;
    this.translate.get(['fields', 'messageTable']).subscribe((result) => {
      this.fields = result.fields;
      this.messages = result.messageTable;
      this.status.push({ code: '', name: this.fields.all });
      this.status.push({ code: 'true', name: this.fields.effective });
      this.status.push({ code: 'false', name: this.fields.expire });
    });

    this.translate.get(['fields', 'notificationMessage', 'rm']).subscribe((result) => {
      this.error_fields = result.fields;
      this.notificationMessage = result.notificationMessage;
    });

    let val = _.get(this.route.snapshot.data, 'titleGroups.content');
    let titleVals = _.map(val, (x) => ({ ...x, value_to_search: `${x.blockCode} - ${x.name}` }));
    this.titleGroups = [
      {
        id: '',
        code: '',
        value_to_search: _.get(this.fields, 'all') || '',
      },
      ...titleVals,
    ];
    this.titleGroupValues = [...this.titleGroups];
    this.obj = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_LEVEL}`);
    this.rsId = _.get(this.obj, 'rsId');
    this.scope = Scopes.VIEW;
    this.service.blocks().then((res: Array<any>) => {
      if (Utils.isArrayNotEmpty(res)) {
        this.formSearch = this.fb.group({
          code: [''],
          name: [''],
          blockCode: ['all'],
          titleGroupId: [''],
          isActive: ['true'],
        });
        let values = _.map(res, (x) => ({ ...x, value_to_search: `${x.code} - ${x.name}` }));
        this.blocks = [
          {
            id: 'all',
            code: 'all',
            value_to_search: _.get(this.fields, 'all') || '',
          },
          ...values,
        ];
        this.onBlockChanged('all');
        this.reload();
      } else {
        this.blocks = [];
        this.onBlockChanged('');
        this.reload();
      }
    });

    this.formSearch.valueChanges.subscribe(() => {
      this.isSearch = false;
    });

    const navigation = this.router.getCurrentNavigation();

    if (Utils.isNotNull(_.get(navigation, 'extras.state.query'))) {
      var query = _.get(navigation, 'extras.state.query');
      this.parmasPrev = query;
      this.previous_query = query;
      this.reload();
    }
    this.search_value = _.get(navigation, 'extras.state.query_search');
    this.previous_search = _.get(navigation, 'extras.state.query_search');
    const timer = setTimeout(() => {
      if (Utils.isNotNull(_.get(navigation, 'extras.state.form'))) {
        const form = _.get(navigation, 'extras.state.form');
        this.previous_param = form;
        this.formSearch = this.fb.group({
          code: [`${_.get(form, 'code')}`],
          name: [`${_.get(form, 'name')}`],
          blockCode: [`${_.get(form, 'blockCode')}`],
          titleGroupId: [`${_.get(form, 'titleGroupId')}`],
          isActive: ['true'],
        });
        this.onBlockChanged(_.get(form, 'blockCode'));
      }
      clearTimeout(timer);
    }, 150);
  }

  @ViewChild('tablerm') public tablerm: DatatableComponent;
  obj: AppFunction;
  rsId: string;
  scope: string;
  blocks: Array<any> = [];
  error_fields: any;
  notificationMessage: any;

  status: Array<any> = [];
  tooltipSearch: string;
  actions: Array<any>;
  search_value: string;
  pageSize: number;
  isLoading: boolean = false;
  first: boolean = false;
  model: Array<any> = [];
  titleGroups: Array<any> = [];
  titleGroupValues: Array<any> = [];
  fields: any;
  parmasPrev: any;
  messages: any;
  isSearchSuccess: boolean;
  previous_query: any;
  previous_param: any;
  previous_search: string;
  exportFile() {
    this.translate.get('rm.fields').subscribe((fields) => {
      const fieldLabels = fields;
      let data = [];
      let obj: any = {};
      const parameters = this.parameters();
      let params = {
        size: maxInt32,
        page: 0,
      };
      if (Utils.isStringNotEmpty(_.get(parameters, 'code'))) {
        params = _.merge(params, {
          code: _.get(parameters, 'code') || '',
        });
      }
      if (Utils.isStringNotEmpty(_.get(parameters, 'name'))) {
        params = _.merge(params, {
          name: _.get(parameters, 'name') || '',
        });
      }

      params = _.merge(params, {
        blockCode: _.get(parameters, 'blockCode') || '',
      });
      if (Utils.isStringNotEmpty(_.get(parameters, 'titleGroupId'))) {
        params = _.merge(params, {
          titleGroupId: _.get(parameters, 'titleGroupId') || '',
        });
      }
      if (Utils.isStringNotEmpty(_.get(parameters, 'isActive'))) {
        params = _.merge(params, {
          isActive: _.get(parameters, 'isActive') || '',
        });
      }
      this.api.fetch(params).subscribe((response) => {
        if (Utils.isArrayNotEmpty(_.get(response, 'content'))) {
          _.get(response, 'content').forEach((item) => {
            obj = {};
            obj[fieldLabels.index] = _.get(response, 'content').indexOf(item) + 1;
            obj[fieldLabels.grade] = _.get(item, 'blockCode');
            obj[fieldLabels.rm_title_group_name] = _.get(item, 'titleGroupName');
            obj[fieldLabels.rm_level_code] = _.get(item, 'code');
            obj[fieldLabels.rm_level_name] = _.get(item, 'name');
            obj[fieldLabels.description] = _.get(item, 'description');
            data.push(obj);
          });
          this.exportExcelService.exportAsExcelFile(data, 'title-group');
        } else {
          this.messageService.error(this.notificationMessage.noRecord);
        }
      });
    });
  }

  search() {
    this.reload(true);
    this.isSearchSuccess = true;
  }

  onActive($event) {
    let type = _.get($event, 'type');
    let id = _.get($event, 'row.id');
    if (type === 'dblclick') {
      if (Utils.isStringNotEmpty(id)) {
        var url = `/rm360/level-category/view/${id}`;
        this.navigate(url);
      }
    }
  }

  ftime: boolean = true;
  paging($event) {
    if (this.ftime) {
      this.ftime = false;
      return;
    }
    this.params.page = _.get($event, 'page') - 1;
    if (Utils.isNotNull(this.parmasPrev)) {
      this.parmasPrev.page = _.get($event, 'page') - 1;
    }
    this.reload();
  }

  onBlockChanged(event) {
    if (event.value && event.value !== 'all') {
      let vals = _.chain(this.titleGroups)
        .filter((x) => x.blockCode === event.value)
        .value();
      if (Utils.isArrayNotEmpty(vals)) {
        this.titleGroupValues = [
          {
            id: '',
            code: 'all',
            value_to_search: _.get(this.fields, 'all'),
          },
          ...vals,
        ];
      } else {
        this.titleGroupValues = [...vals];
      }
    } else {
      this.titleGroupValues = [...this.titleGroups];
    }
    this.ref.detectChanges();
  }

  parameters() {
    const params = {
      q: this.params.size, // quantity
      p: this.params.page, // page (from 0)
      n:
        this.params.name === null || this.params.name === undefined || this.params.name === ''
          ? undefined
          : this.params.name, // name
      c:
        this.params.code === null || this.params.code === undefined || this.params.code === ''
          ? undefined
          : this.params.code, // code
      s:
        this.params.sort === null || this.params.sort === undefined || this.params.sort === 'code,asc'
          ? undefined
          : this.params.sort, // sorting
    };
    return _.merge(params, {
      code: Utils.trim(this.formSearch.get('code').value),
      name: Utils.trim(this.formSearch.get('name').value),
      blockCode: this.formSearch.get('blockCode').value,
      titleGroupId: this.formSearch.get('titleGroupId').value,
      isActive: Utils.trim(this.formSearch.get('isActive').value),
    });
  }

  add() {
    var url = '/rm360/level-category/create';
    this.navigate(url);
  }

  navigate(url: string) {
    if (this.isSearchSuccess) {
      this.router.navigate([`${url}`], {
        state: {
          form: {
            code: Utils.trim(this.formSearch.get('code').value),
            name: Utils.trim(this.formSearch.get('name').value),
            blockCode: this.formSearch.get('blockCode').value,
            titleGroupId: this.formSearch.get('titleGroupId').value,
            isActive: Utils.trim(this.formSearch.get('isActive').value),
          },
          query: this.parmasPrev,
          query_search: this.search_value,
        },
      });
    } else {
      if (
        Utils.isNotNull(this.previous_param) ||
        Utils.isNotNull(this.previous_query) ||
        Utils.isStringNotEmpty(this.previous_search)
      ) {
        this.router.navigate([`${url}`], {
          state: {
            form: this.previous_param || {
              code: '',
              name: '',
              blockCode: 'all',
              titleGroupId: '',
            },
            query: this.previous_query || {},
            query_search: this.previous_search || '',
          },
        });
      } else {
        this.router.navigate([`${url}`], {
          state: {
            form: {
              code: '',
              name: '',
              blockCode: 'all',
              titleGroupId: '',
            },
            query: this.parmasPrev,
            query_search: '',
          },
        });
      }
    }
  }

  update(value: any) {
    var url = `/rm360/level-category/update/${this.parse_code(value)}`;
    this.navigate(url);
    // this.router.navigate([`/rm360/level-category/update/${this.parse_code(value)}`], {
    //   state: {
    //     form: {
    //       code: Utils.trim(this.formSearch.get('code').value),
    //       name: Utils.trim(this.formSearch.get('name').value),
    //       blockCode: this.formSearch.get('blockCode').value,
    //       titleGroupId: this.formSearch.get('titleGroupId').value,
    //       isActive: Utils.trim(this.formSearch.get('isActive').value),
    //     },
    //     query: this.parmasPrev,
    //     query_search: this.search_value
    //   }
    // })
  }

  parse_code(value) {
    return _.get(value, 'id');
  }

  reload(isSearch?: boolean) {
    this.isLoading = true;
    const parameters = this.parameters();
    let params: any = {};
    if (isSearch) {
      params = this.getParams(parameters, params);
      params.page = 0;
    } else {
      params = this.parmasPrev;
    }
    if (Utils.isNull(params)) {
      params = this.getParams(parameters, params);
      params.page = _.get(parameters, 'p') === undefined ? 0 : _.get(parameters, 'p');
    }

    params.size = _.get(parameters, 'q') === undefined ? 10 : _.get(parameters, 'q');
    params.isActive = true;
    this.isSearch = true;
    this.api.fetch(params).subscribe(
      (response) => {
        this.parmasPrev = params;
        this.models = _.get(response, 'content');
        this.count = _.get(response, 'totalElements');
        this.offset = _.get(response, 'number');
        this.ref.detectChanges();
        this.isLoading = false;
      },
      () => {
        this.messageService.error('Kết nối hệ thống không thành công. Vui lòng thử lại sau');
        this.isLoading = false;
      }
    );
  }

  getParams(parameters, params) {
    if (Utils.isStringNotEmpty(_.get(parameters, 'code'))) {
      params = _.merge(params, {
        code: _.get(parameters, 'code') || '',
      });
    }
    if (Utils.isStringNotEmpty(_.get(parameters, 'name'))) {
      params = _.merge(params, {
        name: _.get(parameters, 'name') || '',
      });
    }

    params = _.merge(params, {
      blockCode: _.get(parameters, 'blockCode') || '',
    });

    if (Utils.isStringNotEmpty(_.get(parameters, 'titleGroupId'))) {
      params = _.merge(params, {
        titleGroupId: _.get(parameters, 'titleGroupId') || '',
      });
    }
    return Utils.isNotNull(params) ? params : {};
  }

  delete(value: any) {
    const id = this.parse_code(value);
    const confirm = this.modal.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.result
      .then((response) => {
        if (response) {
          this.api.delete(id).subscribe(
            () => {
              this.messageService.success(this.notificationMessage?.success || '');
              setTimeout(() => {
                this.reload();
              }, 300);
              this.isLoading = false;
            },
            (e) => {
              this.messageService.error(e?.error?.description);
              this.isLoading = false;
            }
          );
        }
      })
      .finally(() => {
        const timer = setTimeout(() => {
          if (Utils.isArrayEmpty(this.models)) {
            this.params.page = this.offset - 1 > 0 ? this.offset - 1 : 0;
            if (Utils.isNotNull(this.parmasPrev)) {
              this.parmasPrev.page = this.offset - 1 > 0 ? this.offset - 1 : 0;
            }
            this.reload();
          }
          clearTimeout(timer);
        }, 500);
      });
  }
}
