import {Component, Injector, OnInit, ViewChild} from "@angular/core";
import {BaseComponent} from "../../../../core/components/base.component";
import {Pageable} from "../../../../core/interfaces/pageable.interface";
import {global} from "@angular/compiler/src/util";
import {DatatableComponent} from "@swimlane/ngx-datatable";
import {RM_GROUP_STATUS_LIST, EXPORT_LIMIT, FunctionCode, Scopes,SessionKey} from "../../../../core/utils/common-constants";
import {RmGroupApi} from "../../apis";
import {FileService} from "../../../../core/services/file.service";
import {HuddleGroupApi} from "../../apis/huddle-group.api";
import * as _ from 'lodash';
import {CategoryService} from "../../../system/services/category.service";
import {Utils} from "../../../../core/utils/utils";
@Component({
  selector: 'app-huddle-group',
  templateUrl: './huddle-group.component.html',
  styleUrls: ['./huddle-group.component.scss'],
})
export class HuddleGroupComponent extends BaseComponent implements OnInit{
  isLoading = true;
  listData = [];
  pageable: Pageable;
  limit = global.userConfig.pageSize;
  paramSearch = {
    size: this.limit,
    page: 0,
    search: '',
    rsId: '',
    scope: Scopes.VIEW,
    isActived: 1
  };
  paramDelete = {
    groupCode: '',
    rsId: '',
    scope: 'VIEW'
  }
  textSearch = '';
  isSearch = true;
  isFirst = true;
  isRm = false;
  currUser: any;
  @ViewChild('table') table: DatatableComponent;

  statusList = RM_GROUP_STATUS_LIST;
  EXPORT_LIMIT = EXPORT_LIMIT;


  constructor(
    injector: Injector,
    private huddleGroupApi: HuddleGroupApi,
    private fileService: FileService,
    private categoryService: CategoryService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HUDDLE_GROUP}`);
    const rsIdOfRm = this.objFunction?.rsId;
    this.paramSearch.rsId = rsIdOfRm;
    this.paramDelete.rsId = rsIdOfRm;
    // this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
    // if (this.prop) {
    //   this.paramSearch = this.prop.paramSearch;
    // //  this.paramDelete = this.prop.paramDelete;
    //   this.textSearch = this.paramSearch.search;
    // }
  }
  ngOnInit(): void {
    if (global?.previousUrl && global?.previousUrl.includes('rm360/huddle-group')) {
      this.prop = this.sessionService.getSessionData(FunctionCode.HUDDLE_GROUP);
    }
    if(this.prop){
      console.log('prop: ',this.prop);
      this.isRm = this.prop.isRm;
      this.paramSearch = this.prop.paramSearch;
      this.textSearch = this.prop.textSearch
      if(this.paramSearch.isActived === null){
        this.paramSearch.isActived = -1;
      }
      this.search(true);
    }
    else{
     // console.log('chay vao day!');
      this.categoryService.getBranchesOfUser(this.paramSearch.rsId, Scopes.VIEW).subscribe(
        (branchesOfUser) => {
          this.isRm = Utils.isArrayEmpty(branchesOfUser);
          this.prop = {
            isRm : this.isRm,
            paramSearch: this.paramSearch,
            textSearch : this.textSearch
          }
          this.sessionService.setSessionData(FunctionCode.HUDDLE_GROUP, this.prop);
          this.search(true);
        }
      )
    }
  }

  search(isSearch: boolean){
    this.isLoading = true;
    if(isSearch){
      this.paramSearch.page = 0;
      this.isSearch = isSearch;
      this.textSearch = this.textSearch.trim();
      this.paramSearch.search = this.textSearch;
      if(this.paramSearch.isActived === -1){
        this.paramSearch.isActived = null;
      }
    }
    this.huddleGroupApi.search(this.paramSearch).subscribe(
      (result) => {
        if(result){
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: this.limit,
          }
          this.currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
          console.log('user_name: ',this.currUser);
          const user = this.currUser?.username;
          console.log('user: ',user);
          // if(row.createdBy === user){
          //   return true;
          // }
          result?.content.forEach((i) => {
            if(!this.isRm){
              i.isUpdateOrDelete = true
            }
            else{
              console.log('createBy: ',i?.createdBy );
              console.log('=? ',i?.createdBy === user.toString());
              if(  i?.createdBy === user.toString()){
                i.isUpdateOrDelete = true
              }
              else{
                i.isUpdateOrDelete = false
              }
            }
          });
          this.listData = result?.content;
          this.prop.paramSearch = this.paramSearch;
          this.prop.textSearch = this.textSearch;
          this.sessionService.setSessionData(FunctionCode.HUDDLE_GROUP, this.prop);
          this.isLoading = false;
          this.isFirst = false;
        }
      }, () => {
        this.isLoading = false;
        this.isFirst = false;
      }
    );
  }
  setPage(pageInfo){
    this.paramSearch.page = pageInfo.offset;
    if(!this.isFirst){
      this.search(false);
    }
  }
  isClickSearch(){
    this.isSearch = false;
  }
  // isUpdateOrDelete(row){
  //   console.log('rm : ', this.isRm);
  //   if(!this.isRm){
  //     return true;
  //   }
  //   const currUser = this.sessionService.getSessionData(SessionKey.USER_INFO);
  //   const user = currUser?.userName;
  //   console.log('user: ',user);
  //   if(row.createdBy === user){
  //     return true;
  //   }
  //   return false;
  // }
  exportFile(){
    this.isLoading = true;
    // this.paramSearch.page = 0;
    // this.isSearch = isSearch;
    if(Utils.isArrayEmpty(this.listData)){
      this.isLoading = false;
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    this.textSearch = this.textSearch.trim();
    this.paramSearch.search = this.textSearch;
    if(this.paramSearch.isActived === -1){
      this.paramSearch.isActived = null;
    }
    this.huddleGroupApi.exportGroup(this.paramSearch).subscribe(
      (res) => {
        if(Utils.isStringNotEmpty(res)){
          this.downloadFile(res);
        }
        else{
          this.isLoading = false;
          this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
          return;
        }
      }, () => {
        this.isLoading = false;
        this.messageService.error(_.get(this.notificationMessage,'export_error'));
      }
    )
  }
  downloadFile(fileId: string){
    const filename = `huddle_group_${new Date().getTime()}`;
    this.fileService.downloadFile(fileId,`${filename}.xlsx`).subscribe(
      (res) => {
        this.isLoading = false;
        if(!res){
          this.messageService.error(_.get(this.notificationMessage, 'error'))
        }
      }
    );
  }
  delete(row){
    console.log('xoa: ',row);
    this.confirmService.confirm().then(
      (res) => {
        if(res){
          this.isLoading = true;
          console.log('row: ',row.groupCode);
          console.log('paramDelete: ',this.paramDelete);
          this.paramDelete.groupCode = row.groupCode;
          this.huddleGroupApi.deleteGroup(this.paramDelete).subscribe(
            (result) => {
              this.isLoading = false;
              this.search(false);
              this.messageService.success(this.notificationMessage.success);
            }, (e) => {
              this.isLoading = false;
              if(e?.errorCode){
                this.messageService.warn(e?.messages?.vn)
              }
              else{
                this.messageService.error(this.notificationMessage.error);
              }
            }
          )
        }
      }
    )
  }
  create(){
    this.router.navigate([this.router.url,'create'],{state: {paramSearch: this.paramSearch}});
  }
  handleChangePageSizeHuddle(event){
    this.paramSearch.page = 0;
    this.paramSearch.size = event;
    this.limit = event;
    this.search(false);
  }
  update(row){
    this.router.navigate([this.router.url, 'update', row.huddleGroupId], {
      queryParams: { ...this.paramSearch,
              groupCode: row.groupCode,
              groupName: row.groupName,
              blockCode: row.blockCode},
      skipLocationChange: true,
    });
  }
  onActive($event){
    const  type = _.get($event,'type');
    if(type === 'dblclick'){
      const row = _.get($event,'row');
      this.router.navigate([this.router.url, 'detail', row.huddleGroupId], {
        queryParams: { ...this.paramSearch,
          groupCode: row.groupCode,
          groupName: row.groupName,
          blockCode: row.blockCode},
        skipLocationChange: true,
      });
    }
  }
}
