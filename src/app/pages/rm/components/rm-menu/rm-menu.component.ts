import { ChangeDetectorRef, NgZone } from '@angular/core';
import { Component, OnInit, Output, EventEmitter, AfterViewInit, OnChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import _ from 'lodash';
import { RmService } from '../../services';

@Component({
  selector: 'app-rm-menu',
  templateUrl: './rm-menu.component.html',
  styleUrls: ['./rm-menu.component.scss'],
})
export class RmMenuComponent {
  public menus = [
    {
      code: 'rm360',
      routerLink: '/rm360',
    },
  ];
  profile: any;
  has_profile: boolean;
  constructor(
    private router: ActivatedRoute,
    private service: RmService,
    private zone: NgZone,
    private ref: ChangeDetectorRef
  ) {
    service._profile_changed.subscribe(() => {
      zone.run(() => {
        this.profile = service.get_profile();
        this.has_profile = this.profile !== null && this.profile !== undefined;
        ref.detectChanges();
      });
    });
  }
}
