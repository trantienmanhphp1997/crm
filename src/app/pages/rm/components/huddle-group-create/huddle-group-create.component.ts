import {Component, Injector, OnInit} from "@angular/core";
import {BaseComponent} from "../../../../core/components/base.component";
import {CustomValidators} from "../../../../core/utils/custom-validations";
import {Pageable} from "../../../../core/interfaces/pageable.interface";
import {RmType, SessionKey} from "../../../../core/utils/common-constants";
import * as _ from 'lodash';
import {RmModalComponent} from "../rm-modal/rm-modal.component";
import {global} from "@angular/compiler/src/util";
import {Utils} from "../../../../core/utils/utils";
import {HuddleGroupApi} from "../../apis/huddle-group.api";
import {ConfirmDialogComponent} from "../../../../shared/components";
import {cleanDataForm} from "../../../../core/utils/function";
import {number} from "echarts";
@Component({
  selector: 'app-huddle-group-create',
  templateUrl: './huddle-group-create.component.html',
  styleUrls: ['./huddle-group-create.component.scss'],
})
export class HuddleGroupCreateComponent extends BaseComponent implements OnInit{
  listBlock = [];
  listTemp = [];
  form = this.fb.group({
    groupCode: [{ value: '', disabled: true }, [CustomValidators.required]],
    groupName: [{ value: '', disabled: true }, [CustomValidators.required]],
    blockCode: ['', [CustomValidators.required]],
  });
  params = {
    page: 0,
    size: global.userConfig.pageSize,
    search: '',
  };
  textSearch = '';
  pageable:Pageable;
  listRm = [];
  datas = [];
  rmType :string;
  lead: any = {hrsCode: null};
  groupCode : string;
  groupName : string;
  prev_block : string;
  CODE_WARNING_BRANCH = 'CRM-HUDDLE-RM009';
  constructor(injector: Injector,private huddleGroupApi: HuddleGroupApi) {
    super(injector);
  }
  ngOnInit(): void {
    console.log('them moi');
    this.rmType = RmType.RM;
    // this.listBlock = _.map(this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM),(item) =>{
    //   return {
    //     code: item.code,
    //     displayName: item.code + ' - ' + item.name,
    //     name: item.name,
    //   }
    // })
    this.listBlock = [
      {
            code: 'INDIV',
            displayName: 'INDIV - Khách hàng cá nhân',
            name: 'Khách hàng cá nhân'
      }
    ];
    this.form.controls.blockCode.setValue(_.get(this.listBlock,'[0].code',''));
    this.form.controls.blockCode.disable();
  }
  selectBlock(value){
    if(value !== this.prev_block){
      this.listTemp = [];
      this.search(false);
    }
    this.prev_block = value;
    this.genInfoLeaderGroup();
  }
  selectMembers(){
    if(this.form.controls.blockCode.value === ''){
      return;
    }
    const formSearch = {
      crmIsActive: { value: 'true', disabled: true },
      rmBlock: { value: this.form.controls.blockCode.value, disabled: true },
      isHuddleGroup: true
    }
    const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
    modal.componentInstance.dataSearch = formSearch;
    modal.componentInstance.listHrsCode = this.listTemp.map((item) => {
      return item.hrsCode;
    });
    modal.result
      .then((res) => {
        if (res) {
          const listSelected = res.listSelected.map((item) => {
            return {
              hrsCode: item.hrisEmployee?.employeeId,
              rmCode: item.t24Employee?.employeeCode,
              rmFullName: item.hrisEmployee?.fullName,
              isLeader: (this.lead.hrsCode === item.hrisEmployee?.employeeId) ? true : false,
              branchCode: item.t24Employee?.branchCode,
              branchName: item.t24Employee?.branchName,
            };
          });
          console.log('listSelected: ',listSelected);
          console.log('tem: ',this.listTemp);
          this.listTemp = _.uniqBy([...this.listTemp, ...listSelected], (i) => i.hrsCode);
          console.log('listtemp111: ',this.listTemp);
          this.listTemp = _.remove(this.listTemp, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);
          console.log('listtemp: ',this.listTemp);
          this.search(true);
        }
      })
      .catch(() => {});
  }
  search(isSearch: boolean ){
    if(isSearch){
      this.textSearch = this.textSearch.trim();
      this.params.search = this.textSearch;
      this.params.page = 0;
    }
    const list = _.map(this.listTemp,(item) => ({
        ...item,
        name: Utils.parseToEnglish(`${item.rmCode} ${item.rmFullName}`),
        id: item.id
      })
    );
    if(Utils.isStringEmpty(this.textSearch)){
      this.datas = [...list];
      this.listRm = _.slice(
        this.datas,
        _.get(this.params,'page') * _.get(this.params,'size'),
        _.get(this.params,'page') * _.get(this.params,'size') + _.get(this.params,'size')
        );
    }
    else{
      const search = Utils.parseToEnglish(this.textSearch);
      const  listRms = _.filter(list, (item) => {
        return item.name.indexOf(search) !== -1
      });
      this.datas = [...listRms];
      this.listRm = _.slice(this.datas,
        _.get(this.params,'size') * _.get(this.params,'page'),
        _.get(this.params,'size') * _.get(this.params,'page') + _.get(this.params,'size'));
    }
    this.pageable = {
      totalElements: _.size(this.datas),
      totalPages: _.size(this.datas) / _.get(this.params, 'size'),
      currentPage: _.get(this.params, 'page'),
      size: _.get(this.params, 'size'),
    }
  }
  setPage(pageInfo){
    this.params.page = pageInfo.offset;
    console.log('page: ',this.params.page);
    this.search(false);
  }
  deleteMember(row){
    if(row.hrsCode === this.lead?.hrsCode){
      this.lead = {hrsCode: null};
      this.genInfoLeaderGroup();
    }
    this.listTemp = _.filter(this.listTemp, (item) => {
      return item.hrsCode !== row.hrsCode
    });
    this.search(false);
  }
  onSelectedFn(item, event){
    this.lead = event.checked ? {...item} : {hrsCode : null};
    this.listTemp.forEach((itemOld) => {
      itemOld.isLeader = itemOld.hrsCode === this.lead.hrsCode;
    });
    this.listRm?.forEach((itemRm) => {
      itemRm.isLeader = false;
    });
    item.isLeader = event.checked;
    this.genInfoLeaderGroup();
  }
  confirmDialog(){
   // if(this.form.valid){
      if(this.listTemp.length < 1){
        this.confirmService.warn(this.notificationMessage.RM003);
        return;
      }
      if(this.lead.hrsCode === null){
        this.confirmService.warn(this.notificationMessage.RM001);
        return;
      }
      // if(this.listTemp.length > 50){
      //   this.confirmService.error(this.notificationMessage.RM006);
      //   return;
      // }
      const confirm = this.modalService.open(ConfirmDialogComponent,{ windowClass: 'confirm-dialog'});
      confirm.result.then((res) => {
        if(res){
          this.isLoading = true;
          const listMem = this.listTemp.map((i) => {
            return i.hrsCode
          });
          const dataCreate = {
            groupCode : this.groupCode,
            groupName : this.groupName,
            blockCode : this.form.controls.blockCode.value,
            leadGroup : this.lead?.hrsCode,
            memberList : listMem
          }
          cleanDataForm(this.form);
          console.log('form: ', this.form.controls.blockCode.value);
          this.huddleGroupApi.createGroup(dataCreate).subscribe((result:any) => {
           // if(Utils.isStringNotEmpty(_.get(result,'')))
            console.log('result: ',result);
            this.isLoading = false;
            const today = new Date();
            this.listTemp.forEach((item) => {
              item.dateOfJoin = today;
            });
            this.messageService.success(this.notificationMessage.success);
            const id = _.get(result, 'huddleGroupId');
            console.log('id: ',id);
            this.router.navigate(['rm360/huddle-group/detail', id], {
              queryParams: {
                groupCode: this.groupCode,
                groupName: this.groupName,
                blockCode: this.form.controls.blockCode.value}
            });
          }, (e) => {
            if(e?.error){
              console.log('e: ',e);
              if(_.get(e, 'error.errorCode') === this.CODE_WARNING_BRANCH){
                const confirmSave = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
                confirmSave.componentInstance.message = _.get(e, 'error.messages.vn') + '. Bạn muốn tiếp tục lưu?';
                confirmSave.componentInstance.title = 'Cảnh báo';
                confirmSave.result
                  .then((confirmed: boolean) => {
                    if (confirmed) {
                      this.isLoading = true;
                      const listMember = this.listTemp.map((i) => {
                        return i.hrsCode
                      });
                      const dataCreateNew = {
                        groupCode : this.groupCode,
                        groupName : this.groupName,
                        blockCode : this.form.controls.blockCode.value,
                        leadGroup : this.lead?.hrsCode,
                        memberList : listMember,
                        isSave: true
                      };
                      this.huddleGroupApi.createGroup(dataCreateNew).subscribe((resNew:any) => {
                        this.isLoading = false;
                        const today = new Date();
                        this.listTemp.forEach((item) => {
                          item.dateOfJoin = today;
                        });
                        this.messageService.success(this.notificationMessage.success);
                        const idNew = _.get(resNew, 'huddleGroupId');
                        console.log('id: ',idNew);
                        this.router.navigate(['rm360/huddle-group/detail', idNew], {
                          queryParams: {
                            groupCode: this.groupCode,
                            groupName: this.groupName,
                            blockCode: this.form.controls.blockCode.value}
                        });
                      }, (er) => {
                        this.isLoading = false;
                        if (er?.error) {
                          if(_.get(er, 'error.messages.vn')){
                            this.confirmService.warn(_.get(er, 'error.messages.vn'));
                          }
                          else{
                            this.messageService.error(this.notificationMessage.error);
                          }
                        } else {
                          this.messageService.error(this.notificationMessage.error);
                        }
                      });
                    }
                  })
                  .catch(() => {
                  });
              }
              else{
                if(_.get(e, 'error.messages.vn')){
                  this.confirmService.warn(_.get(e, 'error.messages.vn'));
                }
                else{
                  this.messageService.error(this.notificationMessage.error);
                }
              }
            }
            else {
              this.messageService.error(this.notificationMessage.error);
            }
            this.isLoading = false;
          });
        }
      })
  //  }
  }
  genInfoLeaderGroup(){
    if(Utils.trimNullToEmpty(this.form.controls.blockCode.value) !== '' ){
      this.groupCode = this.form.controls.blockCode.value ;
      this.groupName = this.form.controls.blockCode.value ;
    }
    if(Utils.trimNullToEmpty(this.lead?.branchCode) !== ''){
      this.groupCode += '_' + this.lead.branchCode;
      this.groupName += '_' + this.lead.branchCode;
    }
    if(Utils.trimNullToEmpty(this.lead?.hrsCode) !== ''){
      this.groupCode += '_' + this.lead.hrsCode;
    }
    if(Utils.trimNullToEmpty(this.lead?.rmFullName) !== ''){
      this.groupName += '_' + this.lead.rmFullName;
    }
    this.form.controls.groupCode.setValue(this.groupCode);
    this.form.controls.groupName.setValue(this.groupName);
  }
  handleChangePageSizeMember(value){
    this.params.page = 0;
    this.params.size = value;
    this.search(false);
  }
  back() {
    this.router.navigate(['rm360/huddle-group'], { state: this.prop });
  }
}
