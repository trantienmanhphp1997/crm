import { forkJoin, of } from 'rxjs';
import { DatePipe } from '@angular/common';
import { global } from '@angular/compiler/src/util';
import { Component, Injector, OnInit, ViewChild, ViewEncapsulation, Input, HostBinding } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ExportExcelService } from 'src/app/core/services/export-excel.service';
import { maxInt32 } from 'src/app/core/utils/common-constants';
import { BaseComponent } from 'src/app/core/components/base.component';
import { RmApi, RmBlockApi } from '../../apis';
import { catchError } from 'rxjs/operators';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-rm-division-history',
  templateUrl: './rm-division-history.component.html',
  styleUrls: ['./rm-division-history.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe],
})
export class RmDivisionHistoryComponent extends BaseComponent implements OnInit {
  @ViewChild('table') table: DatatableComponent;
  listData = [];
  temp = [];
  itemRm: any;
  infoRm: string;
  isLoading = false;
  textFilter = '';
  messages = global.messageTable;
  param: any;
  @Input() hrsCode: string;
  @HostBinding('class.app-rm-division-history') rmDivisionHistory = true;

  constructor(
    injector: Injector,
    private rmBlockApi: RmBlockApi,
    private rmApi: RmApi,
    private datePipe: DatePipe,
    private exportExcelService: ExportExcelService,
    private modalActive: NgbActiveModal
  ) {
    super(injector);
  }

  ngOnInit(): void {
    const hrsCode = this.hrsCode;
    if (hrsCode) {
      this.isLoading = true;
      const params = {
        hrsCode,
        page: 0,
        size: maxInt32,
      };
      forkJoin([
        this.rmBlockApi.fetch(params),
        this.rmApi.getByCode(hrsCode).pipe(catchError((e) => of(undefined))),
      ]).subscribe(
        ([listHistories, itemRm]) => {
          this.listData = listHistories.content || [];
          this.temp = listHistories.content || [];
          this.itemRm = itemRm;
          this.infoRm = (itemRm?.t24Employee?.employeeCode || '') + ' - ' + (itemRm?.hrisEmployee?.fullName || '');
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
    }
  }

  ngAfterViewInit() {
    this.table.groupExpansionDefault = true;
  }

  filter() {
    this.textFilter = this.textFilter.trim();

    // filter our data
    const temp =
      this.temp?.filter((d) => {
        return d.blockCode.toLowerCase().indexOf(this.textFilter?.toLocaleLowerCase()) !== -1 || !this.textFilter;
      }) || [];

    // update the rows
    this.listData = temp;
  }

  toggleExpandGroup(group) {
    this.table.groupExpansionDefault = false;
    this.table.groupHeader.toggleExpandGroup(group);
  }

  parseDate(value) {
    if (value) {
      try {
        return this.datePipe.transform(value, 'dd/MM/yyyy');
      } catch (error) {
        return '';
      }
    }
    return '';
  }

  exportFile() {
    const data = [];
    let obj: any = {};
    if (this.listData.length > 0) {
      this.listData.forEach((item) => {
        obj = {};
        obj[this.fields.blockCode] = item.blockCode;
        obj[this.fields.titleGroupRM] = item.titleGroupName;
        obj[this.fields.levelRM] = item.levelRMName;
        obj[this.fields.syntheticKPI] = item.isSyntheticKPI ? this.fields.yes : this.fields.no;
        obj[this.fields.startDate] = this.parseDate(item.assignedDate);
        obj[this.fields.endDate] = this.parseDate(item.endDate);
        obj[this.fields.modifiedBy] = item.createdBy;
        obj[this.fields.modifiedDate] = this.parseDate(item.createdDate);
        data.push(obj);
      });
      this.exportExcelService.exportAsExcelFile(
        data,
        'rm_' +
        (this.itemRm?.t24Employee?.employeeCode ? this.itemRm?.t24Employee?.employeeCode + '_' : '') +
        this.itemRm?.hrisEmployee?.fullName +
        '_history'
      );
    } else {
      this.messageService.warn(this.notificationMessage.noRecord);
    }
  }

  back() {
    // this.location.back();
    this.router.navigate([`/rm360/rm-manager`], { state: this.param });
  }

  closeModal() {
    this.modalActive.close();
  }
}
