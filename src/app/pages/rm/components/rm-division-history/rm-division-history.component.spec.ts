import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmDivisionHistoryComponent } from './rm-division-history.component';

describe('RmDivisionHistoryComponent', () => {
  let component: RmDivisionHistoryComponent;
  let fixture: ComponentFixture<RmDivisionHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RmDivisionHistoryComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmDivisionHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
