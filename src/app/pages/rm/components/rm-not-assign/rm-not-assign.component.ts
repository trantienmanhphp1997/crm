import { forkJoin, of } from 'rxjs';
import { Component, OnInit, ViewChild, HostBinding, ChangeDetectorRef } from '@angular/core';
import _ from 'lodash';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { Utils } from '../../../../core/utils/utils';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { FunctionCode, maxInt32 } from '../../../../core/utils/common-constants';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { global } from '@angular/compiler/src/util';
import { Scopes } from 'src/app/core/utils/common-constants';
import { TranslateService } from '@ngx-translate/core';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { catchError } from 'rxjs/operators';
import { RmUnassignApi } from '../../apis';
import * as moment from 'moment';
import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';
import { AppFunction } from 'src/app/core/interfaces/app-function.interface';
import { SessionService } from 'src/app/core/services/session.service';
import { FileService } from 'src/app/core/services/file.service';

@Component({
  selector: 'app-rm-not-assign',
  templateUrl: './rm-not-assign.component.html',
  styleUrls: ['./rm-not-assign.component.scss'],
})
export class RmNotAssignComponent implements OnInit {
  constructor(
    private modalService: NgbModal,
    private translate: TranslateService,
    private categoryService: CategoryService,
    private api: RmUnassignApi,
    private messageService: NotifyMessageService,
    private ref: ChangeDetectorRef,
    private sessionService: SessionService,
    private fileService: FileService
  ) {
    this.messages = global.messageTable;

    this.translate.get(['fields', 'notificationMessage']).subscribe((result) => {
      this.notificationMessage = _.get(result, 'notificationMessage');
      this.fields = _.get(result, 'fields');
    });
  }

  @ViewChild('table') table: DatatableComponent;
  @HostBinding('class.app__right-content') appRightContent = true;
  models: Array<any>;
  pageable: Pageable;
  isFisrt = true;
  isLoading: boolean;
  fields: any;
  messages: any;
  branches: Array<any>;
  blocks: Array<any>;
  date: Date = new Date();
  notificationMessage: any;

  paramSearch = {
    blockCodes: '',
    branchCodes: [],
    businessDate: new Date(this.date.getFullYear(), this.date.getMonth(), 1),
    pageSize: global.userConfig.pageSize,
    pageNumber: 0,
    rsId: '',
    scope: Scopes.VIEW,
  };
  prevParams: any;
  obj: AppFunction;

  ngOnInit(): void {
    this.obj = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.GROUP_RM_UNASSIGN}`);
    const rsIdOfRm = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`)?.rsId;
    this.paramSearch.rsId = rsIdOfRm;
    forkJoin([
      this.categoryService.getBranchesOfUser(rsIdOfRm, Scopes.VIEW).pipe(catchError((e) => of(undefined))),
      this.categoryService.getBlocksCategoryByRm().pipe(catchError((e) => of(undefined))),
    ]).subscribe(([branches, blocks]) => {
      this.branches = branches;
      this.blocks = _.map(blocks, (x) => ({ ...x, value_to_search: `${x.code} - ${x.name}` }));
      this.blocks = [
        {
          code: '',
          value_to_search: `Tất cả`,
        },
        ...this.blocks,
      ];
      this.paramSearch.branchCodes = _.map(this.branches, (x) => x.code);
      this.search();
    });
  }

  setPage(pageInfo) {
    if (this.isFisrt) {
      return;
    }
    this.paramSearch.pageNumber = _.get(pageInfo, 'offset');
    this.reload(false);
  }

  search() {
    this.reload(true);
  }

  exportFile() {
    if (Utils.isArrayEmpty(this.models)) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    this.isLoading = true;
    const params = this.prevParams;
    _.set(params, 'pageSize', maxInt32);
    _.set(params, 'pageNumber', 0);
    this.api.createFile(params).subscribe(
      (res) => {
        if (Utils.isStringNotEmpty(res)) {
          this.download(res);
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(_.get(this.notificationMessage, 'export_error'));
        this.isLoading = false;
      }
    );
  }

  download(fileId: string) {
    this.fileService.downloadFile(fileId, 'danh-sach-rm-chua-gan-nhom.xlsx').subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(_.get(this.notificationMessage, 'error'));
      }
    });
  }

  onActive($event) {}

  monthChange($event) {
    if (Utils.isNull(_.get(this.paramSearch, 'businessDate'))) {
      _.set(this.paramSearch, 'businessDate', new Date(this.date.getFullYear(), this.date.getMonth(), 1));
    }
    this.ref.detectChanges();
  }

  reload(isSearch?: boolean) {
    this.isLoading = true;
    let params: any = {};
    if (isSearch) {
      this.paramSearch.pageNumber = 0;
      Object.keys(this.paramSearch).forEach((key) => {
        if (key === 'businessDate') {
          if (Utils.isNull(_.get(this.paramSearch, 'businessDate'))) {
            _.set(this.paramSearch, 'businessDate', new Date(this.date.getFullYear(), this.date.getMonth(), 1));
          }
          params[key] = moment(_.get(this.paramSearch, 'businessDate')).format('DD/MM/YYYY');
        } else {
          params[key] = this.paramSearch[key];
        }
      });
    } else {
      if (!this.prevParams) {
        return;
      }
      if (_.get(this.prevParams, 'pageSize') === maxInt32) {
        _.set(this.prevParams, 'pageSize', _.get(global, 'userConfig.pageSize'));
      }
      params = this.prevParams;
      params.pageNumber = this.paramSearch.pageNumber;
    }
    // Object.keys(this.paramSearch).forEach((key) => {
    //   params[key] = this.paramSearch[key];
    // });
    this.api.post('getRmUnassignedGroupList', params).subscribe(
      (result) => {
        if (result) {
          this.prevParams = params;
          this.models = _.get(result, 'content') || [];
          this.pageable = {
            totalElements: _.get(result, 'totalElements'),
            totalPages: _.get(result, 'totalPages'),
            currentPage: _.get(result, 'number'),
            size: _.get(global, 'userConfig.pageSize'),
          };
        }
        this.isLoading = false;
        this.isFisrt = false;
      },
      () => {
        this.isLoading = false;
        this.isFisrt = false;
      }
    );
  }

  getValue(row, key) {
    return _.get(row, key);
  }
}
