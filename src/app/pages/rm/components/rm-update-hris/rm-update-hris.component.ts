import { Component, Injector, OnInit, ViewChild } from "@angular/core";
import { BaseComponent } from "src/app/core/components/base.component";
import { FileService } from "src/app/core/services/file.service";
import { Utils } from "src/app/core/utils/utils";
import { RmApi } from "../../apis";
import { global } from "@angular/compiler/src/util";
import { Pageable } from "src/app/core/interfaces/pageable.interface";
import { ColumnMode, DatatableComponent } from "@swimlane/ngx-datatable";
import { maxInt32, typeExcel } from "src/app/core/utils/common-constants";
import { ExportExcelService } from "src/app/core/services/export-excel.service";

@Component({
  selector: "app-rm-update-hris",
  templateUrl: "./rm-update-hris.component.html",
  styleUrls: ["./rm-update-hris.component.scss"],
})
export class RmUpdateHrisComponent extends BaseComponent implements OnInit {
  isLoading = false;
  files: any;
  fileName: string;
  isFile = false;
  fileImport: File;
  limit = global.userConfig.pageSize;
  pageSuccess: Pageable;
  pageError: Pageable;
  listDataError = [];
  listDataSuccess = [];
  ColumnMode = ColumnMode;
  paramError = {
    isSuccess: false,
    size: this.limit,
    page: 0,
    fileId: "",
  };
  paramSuccess = {
    isSuccess: true,
    size: this.limit,
    page: 0,
    fileId: "",
  };
  fileId: string;
  isUpload = false;
  @ViewChild("tableError") tableError: DatatableComponent;
  @ViewChild("tableSuccess") tableSuccess: DatatableComponent;

  constructor(
    injector: Injector,
    private api: RmApi,
    private fileService: FileService,
    private exportExcelService: ExportExcelService
  ) {
    super(injector);
  }

  ngOnInit(): void {}

  searchSuccess(isSearch: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.paramSuccess.page = 0;
    }
    this.api.searchDataErrorSuccess(this.paramSuccess).subscribe(
      (listData) => {
        if (listData) {
          this.listDataSuccess = listData.content || [];
          this.pageSuccess = {
            totalElements: listData.totalElements,
            totalPages: listData.totalPages,
            currentPage: listData.number,
            size: this.limit,
          };
          this.isLoading = false;
        }
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  searchError(isSearch: boolean) {
    this.isLoading = true;

    if (isSearch) {
      this.paramError.page = 0;
    }

    this.api.searchDataErrorSuccess(this.paramError).subscribe(
      (listData) => {
        if (listData) {
          this.listDataError = listData.content || [];
          this.pageError = {
            totalElements: listData.totalElements,
            totalPages: listData.totalPages,
            currentPage: listData.number,
            size: this.limit,
          };
          this.isLoading = false;
        }
      },
      () => {
        this.isLoading = false;
      }
    );
  }
  setPage(pageInfo, type) {
    if (type === "success") {
      this.paramSuccess.page = pageInfo.offset;
      this.searchSuccess(false);
    } else {
      this.paramError.page = pageInfo.offset;
      this.searchError(false);
    }
  }

  downloadTemplate() {
    window.open("/assets/template/hris_code_update_template.xlsx", "_self");
  }

  clearFile() {
    this.isUpload = false;
    this.isFile = false;
    this.fileName = null;
    this.fileImport = null;
    this.files = null;
    this.listDataError = [];
    this.listDataSuccess = [];
    this.pageError = undefined;
    this.fileId = undefined;
  }

  importFile() {
    if (this.isFile && !this.isUpload) {
      this.listDataError = [];
      this.listDataSuccess = [];
      this.isLoading = true;
      const formData: FormData = new FormData();
      formData.append("file", this.fileImport);
      this.api.importFileUpdateHris(formData).subscribe(
        (res) => {
          this.fileId = res;
          this.paramError.fileId = this.fileId;
          this.paramSuccess.fileId = this.fileId;
          this.checkImportSuccess(this.fileId);
          // this.isLoading = false;   
        },
        (e) => {
          if (e?.error) {
            this.messageService.error(e?.error?.description);
          } else {
            this.messageService.error(this.notificationMessage.error);
          }
          this.listDataError = [];
          this.listDataSuccess = [];
          this.isLoading = false;
        }
      );
    }
  }

  handleFileInput(files) {
    if (files && files.length > 0) {
      if (files?.item(0)?.size > 10485760) {
        this.messageService.warn(this.notificationMessage.ECRM005);
        return;
      }
      if (!typeExcel.includes(files?.item(0)?.type)) {
        this.messageService.error(
          this.notificationMessage.CANNOT_READ_DATA_FROM_FILE
        );
        return;
      }
      this.isFile = true;
      this.fileImport = files.item(0);
      this.fileName = files.item(0).name;
    } else {
      this.isFile = false;
    }
  }

  checkImportSuccess(fileId: string) {
    let countInterval = 0;
    const interval = setInterval(() => {
      countInterval++;
      this.api.checkImportSuccess(fileId).subscribe((res) => {
        if (res?.status === "COMPLETE") {
          this.isUpload = true;
          this.isLoading = false;
          this.searchSuccess(true);
          this.searchError(true);
          clearInterval(interval);
        } else if (res?.status === "FAIL") {
          if (res?.msgError === "FILE_NO_CONTENT_EXCEPTION") {
            this.messageService.error(
              this.notificationMessage.FILE_NO_CONTENT_EXCEPTION
            );
          } else if (res?.msgError === "CANNOT_READ_DATA_FROM_FILE") {
            this.messageService.error(
              this.notificationMessage.CANNOT_READ_DATA_FROM_FILE
            );
          } else if (res?.msgError === "FILE_DOES_NOT_EXCEED_RECORDS_1000") {
            const maxRecord = res?.msgError?.replace(
              "FILE_DOES_NOT_EXCEED_RECORDS_",
              ""
            );
            this.translate
              .get("notificationMessage.FILE_HRIS_DOES_NOT_EXCEED_RECORDS", {
                number: maxRecord,
              })
              .subscribe((res) => {
                this.messageService.error(res);
              });
          }
          this.isLoading = false;
          clearInterval(interval);
        }
      });

      if (countInterval > 144) {
        clearInterval(interval);
        this.messageService.error(this.notificationMessage.E001);
        this.isLoading = false;
      }
    }, 2500);
  }

  exportError() {
    let data = [];
    let obj: any = {};
    const params = JSON.parse(JSON.stringify(this.paramError));
    params.page = 0;
    params.size = maxInt32;
    this.isLoading = true;
    this.api.searchDataErrorSuccess(params).subscribe(
      (result) => {
        this.isLoading = false;
        if (result) {
          if (result.content.length === 0) {
            this.messageService.warn(this.notificationMessage.noRecord);
            return;
          }
          result.content.forEach((item, index) => {
            obj = {};
            obj[this.fields.order] = index + 1;
            obj[this.fields.row] = item.rowNum;
            obj[this.fields.column] = item.errorColumn;
            obj[this.fields.typeError] = item.errorType;
            obj[this.fields.dataError] = item.errorCode;
            obj[this.fields.infoError] = item.description;
            data.push(obj);
          });
          this.exportExcelService.exportAsExcelFile(data, "Danh sách lỗi");
        }
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }

  exportSuccess() {
    let data = [];
    let obj: any = {};
    const params = JSON.parse(JSON.stringify(this.paramSuccess));
    params.page = 0;
    params.size = maxInt32;
    this.isLoading = true;
    this.api.searchDataErrorSuccess(params).subscribe(
      (result) => {
        this.isLoading = false;
        if (result) {
          if (result.content.length === 0) {
            this.messageService.warn(this.notificationMessage.noRecord);
            return;
          }
          result.content.forEach((item, index) => {
            obj = {};
            obj[this.fields.order] = index + 1;
            obj[this.fields.hrisCodeOld] = item.hrsCodeOld;
            obj[this.fields.hrsCodeNew] = item.hrsCodeNew;
            // obj[this.fields.hrsCode] = item.hrsCode;

            // obj[this.fields.dataError] = item.value;
            // obj[this.fields.infoError] = item.description;
            data.push(obj);
          });
          this.exportExcelService.exportAsExcelFile(
            data,
            "Danh sách thành công"
          );
        }
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }
}
