import { forkJoin } from 'rxjs';
import { global } from '@angular/compiler/src/util';
import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { ColumnMode, SelectionType, DatatableComponent } from '@swimlane/ngx-datatable';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { Division, FunctionCode, maxInt32, RespondCodeError, Scopes } from 'src/app/core/utils/common-constants';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { UserService } from 'src/app/pages/system/services/users.service';
import { LevelApi, RmBlockApi, TitleGroupApi } from '../../apis';
import { Utils } from 'src/app/core/utils/utils';

@Component({
  selector: 'app-rm-division',
  templateUrl: './rm-division.component.html',
  styleUrls: ['./rm-division.component.scss'],
})
export class RmDivisionComponent extends BaseComponent implements OnInit {
  isLoading = false;
  limit = global.userConfig.pageSize;
  columnsUser = [{ prop: 'username' }, { prop: 'hrsCode' }, { prop: 'code' }, { prop: 'fullName' }, { prop: 'branch' }];
  columnsRole = [{ prop: 'name' }, { prop: 'description' }];
  listRM = [];
  listBlock = [];
  listBlockAll = [];
  blockSelected = [];
  rmSelected: any;
  checkAll = false;
  selected = [];
  ColumnMode = ColumnMode;
  SelectionType = SelectionType;
  rmSearch = '';
  note = '';
  paramsRM = {
    page: 0,
    size: this.limit,
    search: '',
    rsId: '',
    scope: Scopes.VIEW,
  };
  pageableRM: Pageable;
  paramsBlock = {
    page: 0,
    size: this.limit,
  };
  pageableBlock: Pageable;
  listBlocksOfUserOld = [];
  listTitleGroupRM = [];
  listLevelRM = [];
  isFirstSearch = true;
  countCheckedOnPage = 0;

  messErrorConvert = '';  //CRMCN-2823

  @ViewChild('tableRM') public tableRM: DatatableComponent;
  @ViewChild('tableBlock') public tableBlock: DatatableComponent;

  constructor(
    injector: Injector,
    private userService: UserService,
    private categoryService: CategoryService,
    private titleGroupApi: TitleGroupApi,
    private levelApi: LevelApi,
    private rmBlockApi: RmBlockApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_BLOCK}`);
    this.paramsRM.rsId = this.objFunction?.rsId;
  }

  ngOnInit(): void {
    this.isLoading = true;
    forkJoin([
      this.categoryService.searchBlocksCategory(this.paramsBlock),
      this.userService.getRmByPermission(this.paramsRM),
      this.titleGroupApi.fetch({ page: 0, size: maxInt32, isSetUp: true, isActive: true }),
      this.levelApi.fetch({ page: 0, size: maxInt32, isSetUp: true, isActive: true }),
      this.categoryService.searchBlocksCategory({ page: 0, size: maxInt32 }),
    ]).subscribe(
      ([listBlock, listRm, listTitile, listLevel, listBlockAll]) => {
        this.listLevelRM = listLevel.content.sort((a, b) => {
          if (a.name.toLowerCase() < b.name.toLowerCase()) {
            return -1;
          }
          if (a.name.toLowerCase() > b.name.toLowerCase()) {
            return 1;
          }
          return 0;
        });
        this.listTitleGroupRM = listTitile.content.sort((a, b) => {
          if (a.name.toLowerCase() < b.name.toLowerCase()) {
            return -1;
          }
          if (a.name.toLowerCase() > b.name.toLowerCase()) {
            return 1;
          }
          return 0;
        });
        this.listBlockAll = listBlockAll.content;
        for (const itemBlock of listBlock.content) {
          if (itemBlock.titleGroupId) {
            itemBlock.listLevelRM = this.listLevelRM.filter((itemOld) => {
              return itemOld.titleGroupId === itemBlock.titleGroupId;
            });
          }
          itemBlock.listTitleGroupRM = this.listTitleGroupRM.filter((itemOld) => {
            return itemOld.blockCode === itemBlock.code;
          });
          itemBlock.titleGroupId = null;
          itemBlock.levelRMId = null;
        }
        this.listBlock = listBlock.content || [];
        this.pageableBlock = {
          totalElements: listBlock.totalElements,
          totalPages: listBlock.totalPages,
          currentPage: listBlock.number,
          size: this.limit,
        };
        this.listRM = listRm.content || [];
        if (this.listRM.length > 0) {
          this.selected = [this.listRM[0]];
          this.rmSelected = this.selected[0].hrsCode;
          this.getBlockOfUser(this.rmSelected);
        }
        this.pageableRM = {
          totalElements: listRm.totalElements,
          totalPages: listRm.totalPages,
          currentPage: listRm.number,
          size: this.limit,
        };
        this.isFirstSearch = false;
        this.isLoading = false;
      },
      () => {
        this.isFirstSearch = false;
        this.isLoading = false;
      }
    );
  }

  ngAfterViewInit() {}

  searchRM(isSearch: boolean) {
    this.isLoading = true;
    this.rmSearch = this.rmSearch.trim();
    this.paramsRM.search = this.rmSearch;
    if (isSearch) {
      this.paramsRM.page = 0;
    }
    this.userService.getRmByPermission(this.paramsRM).subscribe(
      (result) => {
        if (result) {
          this.listRM = result.content || [];
          this.pageableRM = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: this.limit,
          };
          if (this.listRM.length > 0) {
            this.selected = [this.listRM[0]];
            this.rmSelected = this.selected[0].hrsCode;
            this.getBlockOfUser(this.rmSelected);
          }
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  searchBlock(isSearch: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.paramsBlock.page = 0;
      this.paramsBlock.size = this.limit;
    }
    this.categoryService.searchBlocksCategory(this.paramsBlock).subscribe(
      (result) => {
        this.listBlock = [];
        this.countCheckedOnPage = 0;
        for (const itemBlock of result.content) {
          const itemChoose = this.blockSelected.find((itemOld) => {
            return itemOld.blockCode === itemBlock.code;
          });
          if (!Utils.isNull(itemChoose)) {
            this.countCheckedOnPage += 1;
          }
          itemBlock.titleGroupId = Utils.trimToNull(itemChoose?.titleGroupId);
          itemBlock.levelRMId = Utils.trimToNull(itemChoose?.levelRMId);
          itemBlock.isChecked = !Utils.isNull(itemChoose);
          itemBlock.isSyntheticKPI = itemChoose?.isSyntheticKPI;
          itemBlock.assignedDate = Utils.trimToNull(itemChoose?.assignedDate);
          if (itemBlock.titleGroupId) {
            itemBlock.listLevelRM = this.listLevelRM.filter((itemOld) => {
              return itemOld.titleGroupId === itemBlock.titleGroupId;
            });
          }
          itemBlock.listTitleGroupRM = this.listTitleGroupRM.filter((itemOld) => {
            return itemOld.blockCode === itemBlock.code;
          });
        }
        //CRMCN-2823
        const titleGroupName = this.getTitleGroupName(result.content);
        if (titleGroupName) {
          this.translate.get("notificationMessage.errorConvertUB", { name: titleGroupName }).subscribe((value)=> {
            this.messErrorConvert = value;
          });
        }
        //CRMCN-2823
        this.listBlock = result.content || [];
        this.checkAll = this.listBlock.length > 0 && this.countCheckedOnPage === this.listBlock.length;
        this.pageableBlock = {
          totalElements: result.totalElements,
          totalPages: result.totalPages,
          currentPage: result.number,
          size: this.limit,
        };
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  getBlockOfUser(hrsCode, flag?: boolean) {
    this.isLoading = true;
    const params = {
      isActive: true,
      hrsCode: hrsCode,
      page: 0,
      size: maxInt32,
    };
    this.rmBlockApi.fetch(params).subscribe(
      (listBlocks) => {
        if (listBlocks) {
          this.note = '';
          this.blockSelected = listBlocks.content.map((item) => {
            if (item.note) {
              this.note = item.note;
            } else {
              this.note = '';
            }
            return {
              blockCode: item.blockCode,
              levelRMId: item.levelRMId,
              titleGroupId: item.titleGroupId,
              isSyntheticKPI: item.isSyntheticKPI,
              assignedDate: item.assignedDate,
            };
          });
          this.listBlock = [];
          if (!flag) {
            this.searchBlock(true);
          } else {
            this.searchBlock(false);
          }
        } else {
          this.isLoading = false;
        }
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo, type) {
    if (this.isFirstSearch) {
      return;
    }
    if (type === 'rm') {
      this.paramsRM.page = pageInfo.offset;
      this.searchRM(false);
    } else if (type === 'block') {
      this.paramsBlock.page = pageInfo.offset;
      this.searchBlock(false);
      this.checkAll = false;
    }
  }

  save() {
    if (!(this.objFunction && this.objFunction.update)) {
      return;
    }
    if (this.rmSelected) {
      const data = {
        hrsCode: this.selected.length > 0 ? this.selected[0].hrsCode : '',
        userProfileId: this.rmSelected,
        lstMaps: this.blockSelected,
        note: this.note,
        isRMDepartment: true    //CRMCN-2823
      };
      this.rmBlockApi.create(data).subscribe(
        () => {
          this.blockSelected = [];
          this.getBlockOfUser(this.rmSelected, true);
          this.messageService.success(this.notificationMessage.success);
          this.note = '';
        },
        (error) => {
          //CRMCN-2823
          if (error.error.code === RespondCodeError.CRM6192) {
            // this.messageService.error(this.messErrorConvert);
            this.confirmService.error(this.messErrorConvert);
          } else {
            this.messageService.error(this.notificationMessage.error);
          }
          //CRMCN-2823
          this.isLoading = false;
        }
      );
    }
  }

  //CRMCN-2823
  getTitleGroupName (data) {
    let titleGroupName = '';
    let titleGroupRM = [];
    let titleGroupId = '';
    let titleGroupRMSelected = [];
    const blockINDIV = data.filter((value) => value.code === Division.INDIV);
    if (blockINDIV.length > 0) {
      titleGroupRM = blockINDIV[0].listTitleGroupRM;
      titleGroupId = blockINDIV[0].titleGroupId;
    }
    if (titleGroupId) {
      titleGroupRMSelected = titleGroupRM.filter((value) => value.id === titleGroupId);
    }
    if (titleGroupRMSelected[0] && titleGroupRMSelected[0].name) {
      titleGroupName = titleGroupRMSelected[0].name;
    }
    return titleGroupName;
  }
  //CRMCN-2823

  onCheckboxBlockAllFn(isChecked) {
    this.checkAll = isChecked;
    if (!isChecked) {
      this.listBlock.forEach((item) => {
        this.blockSelected = this.blockSelected.filter((itemChoose) => {
          return itemChoose.blockCode !== item.code;
        });
      });
      this.countCheckedOnPage = 0;
    } else {
      this.countCheckedOnPage = this.listBlock.length;
    }
    for (const item of this.listBlock) {
      item.isChecked = isChecked;
      if (
        isChecked &&
        this.blockSelected.findIndex((itemOld) => {
          return itemOld.blockCode === item.code;
        }) === -1
      ) {
        this.blockSelected.push({
          blockCode: item.code,
          levelRMId: item.levelRMId,
          titleGroupId: item.titleGroupId,
          isSyntheticKPI: item.isSyntheticKPI,
          assignedDate: item.assignedDate,
        });
      }
    }
  }

  onCheckboxBlockFn(row, isChecked) {
    row.isChecked = isChecked;
    if (isChecked) {
      const item = {
        blockCode: row.code,
        levelRMId: row.levelRMId,
        titleGroupId: row.titleGroupId,
        isSyntheticKPI: row.isSyntheticKPI,
        assignedDate: row.assignedDate,
      };
      this.blockSelected.push(item);
      this.countCheckedOnPage += 1;
    } else {
      this.blockSelected = this.blockSelected.filter((itemOld) => {
        return itemOld.blockCode !== row.code;
      });
      this.countCheckedOnPage -= 1;
    }
    this.checkAll = this.countCheckedOnPage === this.listBlock.length;
  }

  onCheckboxSyntheticFn(row, isChecked) {
    row.isSyntheticKPI = !row.isSyntheticKPI;
    for (const item of this.blockSelected) {
      if (item.blockCode === row.code) {
        item.titleGroupId = row.titleGroupId;
        item.levelRMId = row.levelRMId;
        item.isSyntheticKPI = row.isSyntheticKPI;
        item.assignedDate = row.assignedDate;
        return;
      }
    }
  }

  onSelectRM({ selected }) {
    if (selected[0].hrsCode === this.rmSelected) {
      return;
    }
    this.rmSelected = selected[0].hrsCode;
    this.blockSelected = [];
    if (this.rmSelected) {
      this.checkAll = false;
      this.getBlockOfUser(this.rmSelected);
    }
  }

  chooseTitleGroupRM(event, row) {
    event.originalEvent.stopPropagation();
    row.listLevelRM =
      this.listLevelRM?.filter((itemOld) => {
        return itemOld.titleGroupId === event.value;
      }) || [];
    if (
      row.listLevelRM.findIndex((item) => {
        return item.id === row.levelRMId;
      }) === -1
    ) {
      row.levelRMId = row.listLevelRM[0]?.id || null;
    }
    for (const item of this.blockSelected) {
      if (item.blockCode === row.code) {
        item.titleGroupId = row.titleGroupId;
        item.levelRMId = row.levelRMId;
        item.isSyntheticKPI = row.isSyntheticKPI;
        item.assignedDate = row.assignedDate;
        break;
      }
    }
  }

  chooseLevelRM(event, row) {
    event.originalEvent.stopPropagation();
    for (const item of this.blockSelected) {
      if (item.blockCode === row.code) {
        item.titleGroupId = row.titleGroupId;
        item.levelRMId = row.levelRMId;
        item.isSyntheticKPI = row.isSyntheticKPI;
        item.assignedDate = row.assignedDate;
        return;
      }
    }
  }
}
