import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmDivisionComponent } from './rm-division.component';

describe('RmDivisionComponent', () => {
  let component: RmDivisionComponent;
  let fixture: ComponentFixture<RmDivisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RmDivisionComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmDivisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
