import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { BaseComponent } from 'src/app/core/components/base.component';
import { RmModalComponent } from '../rm-modal/rm-modal.component';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { global } from '@angular/compiler/src/util';
import { FunctionCode, RmType, Scopes, SessionKey } from 'src/app/core/utils/common-constants';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { Utils } from 'src/app/core/utils/utils';
import { RmGroupApi } from '../../apis';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';

@Component({
  selector: 'app-rm-group-create',
  templateUrl: './rm-group-create.component.html',
  styleUrls: ['./rm-group-create.component.scss'],
})
export class RmGroupCreateComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listRm = [];
  listTemp = [];
  listBlock = [];
  leader: any = { hrsCode: null };
  groupCode = '';
  groupName = '';
  params = {
    page: 0,
    size: global.userConfig.pageSize,
    search: '',
  };
  pageable: Pageable;
  form = this.fb.group({
    code: [{ value: '', disabled: true }, [CustomValidators.required]],
    name: [{ value: '', disabled: true }, [CustomValidators.required]],
    blockCode: ['', [CustomValidators.required]],
  });
  messages = global.messageTable;
  isSearch = true;
  textSearch = '';
  rmType: string;
  prev_block: string;
  datas: Array<any>;
  @ViewChild('table') table: DatatableComponent;

  constructor(injector: Injector, private categoryService: CategoryService, private rmGroupApi: RmGroupApi) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_GROUP}`);
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
  }

  ngOnInit(): void {
    this.rmType = RmType.RM;
    this.listBlock = _.map(this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM), (item) => {
      return {
        code: item.code,
        displayName: item.code + ' - ' + item.name,
        name: item.name,
      };
    });
    this.form.controls.blockCode.setValue(_.get(this.listBlock, '[0].code', ''));
  }

  confirmDialog() {
    if (this.form.valid) {
      if (this.listTemp.length < 1) {
        this.confirmService.error(this.notificationMessage.RM003);
        return;
      }
      if (!this.leader) {
        this.confirmService.error(this.notificationMessage.RM001);
        return;
      }
      const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result
        .then((res) => {
          if (res) {
            this.isLoading = true;
            const data = cleanDataForm(this.form);
            data.branchCode = this.leader.branchCode;
            data.branchName = this.leader.branchName;
            data.rsId = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`)?.rsId;
            data.scope = Scopes.VIEW;
            data.lstMaps = this.listTemp;
            this.rmGroupApi.create(data).subscribe(
              (response: any) => {
                if (Utils.isStringNotEmpty(_.get(response, 'errorValue'))) {
                  this.confirmService.error(_.get(response, 'errorValue'));
                  this.isLoading = false;
                  return;
                }
                const today = new Date();
                this.listTemp.forEach((item) => {
                  item.dateOfJoin = today;
                });
                this.messageService.success(this.notificationMessage.success);
                this.isLoading = false;
                const id = _.get(response, 'id');
                this.router.navigate(['rm360/rm-group/detail', id]);
              },
              (e) => {
                if (e?.error) {
                  this.confirmService.warn(_.get(e, 'error.description'));
                } else {
                  this.messageService.error(this.notificationMessage.error);
                }
                this.isLoading = false;
              }
            );
          }
        })
        .catch(() => {});
    } else {
      validateAllFormFields(this.form);
    }
  }

  selectMembers() {
    if (this.form.controls.blockCode.value === '') {
      return;
    }
    const formSearch = {
      crmIsActive: { value: 'true', disabled: true },
      rmBlock: { value: this.form.controls.blockCode.value, disabled: true },
      isRmGroup: true,
    };
    const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
    modal.componentInstance.dataSearch = formSearch;
    modal.componentInstance.listHrsCode = this.listTemp.map((item) => {
      return item.hrsCode;
    });
    modal.result
      .then((res) => {
        if (res) {
          const listSelected = res.listSelected.map((item) => {
            return {
              hrsCode: item.hrisEmployee?.employeeId,
              rmCode: item.t24Employee?.employeeCode,
              rmFullName: item.hrisEmployee?.fullName,
              isLeader: item.isLeader ? true : false,
              branchCode: item.t24Employee?.branchCode,
              branchName: item.t24Employee?.branchName,
            };
          });
          this.listTemp = _.uniqBy([...this.listTemp, ...listSelected], (i) => i.hrsCode);
          this.listTemp = _.remove(this.listTemp, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);
          this.search(true);
        }
      })
      .catch(() => {});
  }

  setPage(pageInfo) {
    this.params.page = pageInfo.offset;
    this.search(false);
  }

  search(isSearch: boolean) {
    if (isSearch) {
      this.params.page = 0;
      this.textSearch = this.textSearch.trim();
      this.params.search = this.textSearch;
    }
    if (isSearch) {
      this.params.page = 0;
      this.textSearch = this.textSearch.trim();
      this.params.search = this.textSearch;
    }
    const list = _.map(this.listTemp, (x) => ({
      ...x,
      name: Utils.parseToEnglish(`${x.rmCode} ${x.rmFullName}`),
      id: x.id,
    }));
    if (Utils.isStringEmpty(this.params.search)) {
      this.datas = [...list];
      this.listRm = _.slice(
        list,
        _.get(this.params, 'page') * _.get(this.params, 'size'),
        _.get(this.params, 'page') * _.get(this.params, 'size') + _.get(this.params, 'size')
      );
    } else {
      const search = Utils.parseToEnglish(this.params.search);
      const listRms = _.filter(list, (item) => {
        return Utils.trimNullToEmpty(item.name).toLowerCase().indexOf(search) !== -1;
      });
      this.datas = [...listRms];
      this.listRm = _.slice(
        listRms,
        _.get(this.params, 'page') * _.get(this.params, 'size'),
        _.get(this.params, 'page') * _.get(this.params, 'size') + _.get(this.params, 'size')
      );
    }

    this.pageable = {
      totalElements: _.size(this.datas),
      totalPages: _.size(this.datas) / _.get(this.params, 'size'),
      currentPage: _.get(this.params, 'page'),
      size: _.get(this.params, 'size'),
    };
  }

  onSelectedFn(item, event) {
    this.leader = event.checked ? { ...item } : { hrsCode: null };
    this.listTemp.forEach((itemOld) => {
      itemOld.isLeader = itemOld.hrsCode === this.leader.hrsCode;
    });
    this.listRm?.forEach((itemRm) => {
      itemRm.isLeader = false;
    });

    item.isLeader = event.checked;
    this.generateGroupCodeAndName();
  }

  selectBlock(value) {
    if (Utils.isStringNotEmpty(this.prev_block)) {
      if (this.prev_block !== value) {
        this.listTemp = [];
        this.search(false);
      }
    }
    this.prev_block = value;
    this.generateGroupCodeAndName();
  }

  deleteMember(item) {
    if (item.hrsCode === this.leader?.hrsCode) {
      this.leader = { hrsCode: null };
      this.generateGroupCodeAndName();
    }
    this.listTemp = this.listTemp.filter((itemOld) => {
      return itemOld.hrsCode !== item.hrsCode;
    });
    this.search(false);
  }

  generateGroupCodeAndName() {
    if (Utils.trimNullToEmpty(this.form.controls.blockCode.value) !== '') {
      this.groupCode = this.form.controls.blockCode.value;
      this.groupName = this.form.controls.blockCode.value;
    }
    if (Utils.trimNullToEmpty(this.leader?.branchCode) !== '') {
      this.groupCode += '_' + this.leader.branchCode;
      this.groupName += '_' + this.leader.branchCode;
    }
    if (Utils.trimNullToEmpty(this.leader?.hrsCode) !== '') {
      this.groupCode += '_' + this.leader.hrsCode;
    }
    if (Utils.trimNullToEmpty(this.leader?.rmFullName) !== '') {
      this.groupName += '_' + this.leader.rmFullName;
    }
    this.form.controls.code.setValue(this.groupCode);
    this.form.controls.name.setValue(this.groupName);
  }
}
