import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmGroupCreateComponent } from './rm-group-create.component';

describe('RmGroupCreateComponent', () => {
  let component: RmGroupCreateComponent;
  let fixture: ComponentFixture<RmGroupCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmGroupCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmGroupCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
