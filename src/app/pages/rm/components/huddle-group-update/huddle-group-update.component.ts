import {Component, Injector, OnInit} from '@angular/core';
import {BaseComponent} from '../../../../core/components/base.component';
import {CustomValidators} from '../../../../core/utils/custom-validations';
import {Pageable} from '../../../../core/interfaces/pageable.interface';
import {HuddleGroupApi} from '../../apis/huddle-group.api';
import {global} from '@angular/compiler/src/util';
import {FunctionCode, maxInt32, RmType, Scopes, SessionKey} from '../../../../core/utils/common-constants';
import _ from 'lodash';
import {Utils} from '../../../../core/utils/utils';
import {FileService} from '../../../../core/services/file.service';
import {ExportExcelService} from '../../../../core/services/export-excel.service';
import * as moment from 'moment';
import {DatePipe} from '@angular/common';
import {RmModalComponent} from '../rm-modal/rm-modal.component';
import {ConfirmDialogComponent} from '../../../../shared/components';
import {cleanDataForm, validateAllFormFields} from '../../../../core/utils/function';

@Component({
  selector: 'app-huddle-group-update',
  templateUrl: './huddle-group-update.component.html',
  styleUrls: ['./huddle-group-update.component.scss'],
  providers: [DatePipe],
})
export class HuddleGroupUpdateComponent extends BaseComponent implements OnInit {
  isLoading = false;
  isUpdate: boolean;
  lead: any  = {hrisCode: null};
  form = this.fb.group({
    groupCode: [{value: '', disabled: true}, [CustomValidators.required]],
    groupName: [{value: '', disabled: true}, [CustomValidators.required]],
    blockCode: ['', [CustomValidators.required]],
  });
  param = {
    groupCode: '',
    groupId: null,
    page: 0,
    size: global.userConfig.pageSize,
    search: '',
    isHistory: false,
    rsId: '',
    scope: Scopes.VIEW
  };
  paramSearch = {
    page: 0,
    size: global.userConfig.pageSize,
    search: '',
  };
  listBlock = [];
  listRm = [];
  listTemp = [];
  listMemberOld = [];
  past = true;
  textSearch = '';
  pageable: Pageable;
  rmType: string;
  groupCode = '';
  groupName = '';
  datas: Array<any>;
  groupCodeOld = '';
  groupId = null;
  CODE_WARNING_BRANCH = 'CRM-HUDDLE-RM009';
  constructor(
    injector: Injector,
    private huddleApi: HuddleGroupApi,
    private fileService: FileService,
    private exportExcelService: ExportExcelService,
    private datePipe: DatePipe
    ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HUDDLE_GROUP}`);
    this.param.rsId = this.objFunction?.rsId;
    this.groupId = this.route.snapshot.paramMap.get('id');
    this.isUpdate = _.get(this.route.snapshot.data, 'isUpdate') || false;
    this.groupName = _.get(this.route.snapshot.queryParams, 'groupName');
    this.groupCode = _.get(this.route.snapshot.queryParams, 'groupCode');
    this.groupCodeOld = _.get(this.route.snapshot.queryParams, 'groupCode');
    //   this.form.controls.blockCode.setValue(_.get(this.route.snapshot.queryParams,'blockCode'));
    this.form.controls.groupName.setValue(this.groupName);
    this.form.controls.groupCode.setValue(this.groupCode);
    this.form.controls.blockCode.disable();
    this.past = true;
    if (!this.isUpdate) {
      this.form.disable();
    }
    console.log('name: ', this.groupName);
  }

  ngOnInit(): void {
    this.rmType = RmType.RM;
    this.isLoading = true;
    this.listBlock = _.map(this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM), (item) => {
      return {
        code: item.code,
        displayName: item.code + ' - ' + item.name,
        name: item.name,
      }
    })
    this.form.controls.blockCode.setValue(_.get(this.route.snapshot.queryParams, 'blockCode'));
    this.param.size = maxInt32;
    this.param.groupCode = this.groupCode;
    this.param.groupId = this.groupId;
    this.huddleApi.detailGroup(this.param).subscribe((res) => {
      if (res) {
        this.form.patchValue(res);
        this.listTemp = res?.content || [];
        this.listMemberOld = this.listTemp.map((item) => item.hrisCode);;
        console.log('list: ', this.listTemp);
        this.lead = this.listTemp.find((item) => item.isLeader === true) || {hrisCode: null};
        this.pageable = {
          totalElements: res.totalElements || 0,
          totalPages: res.totalPages || 0,
          currentPage: res.number || 0,
          size: this.param.size,
        };
        this.isLoading = false;
        this.search(false);
      }
    }, () => {
      this.isLoading = false
    });
  }

  selectBlock(value) {
  }

  selectMember() {
    if(this.form.controls.blockCode.value === ''){
      return;
    }
    const formSearch = {
      crmIsActive: { value: 'true', disabled: true },
      rmBlock: { value: this.form.controls.blockCode.value, disabled: true },
      isHuddleGroup: true,
      huddleGroupCode: this.groupCodeOld,
    }
    const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
    modal.componentInstance.dataSearch = formSearch;
    modal.componentInstance.listHrsCode = this.listTemp.map((item) => item.hrisCode);
    modal.result.then((res) => {
      if(res){
        console.log('res: ', res);
        const listSelected = res.listSelected.map((item) => {
          return {
            hrisCode: item.hrisEmployee?.employeeId,
            rmCode: item.t24Employee?.employeeCode,
            rmName: item.hrisEmployee?.fullName,
           // isLeader: item.isLeader ? true : false,
            isLeader: (this.lead.hrisCode === item.hrisEmployee?.employeeId) ? true : false,
            branchCode: item.t24Employee?.branchCode,
            // branchName: item.t24Employee?.branchName,
            // hrsCodeOld: item.hrisEmployee?.hrsCode,
          };
        });
        console.log('listSelected: ',listSelected);
        this.listTemp = _.uniqBy([ ...this.listTemp, ...listSelected],(item) => item.hrisCode);
        console.log('listSelected1: ',listSelected);
        this.listTemp = _.remove(this.listTemp,(i) => _.indexOf(res.listRemove,i.hrisCode) === -1);
        console.log('list tem: ',this.listTemp);
        this.search(true);
      }
    }).catch(() => {});
  }

  onChangePast(value: boolean) {
    if (!value !== this.param.isHistory) {
      this.param.isHistory = !value;
      this.param.page = 0;
      this.param.size = maxInt32;
      this.huddleApi.detailGroup(this.param).subscribe((res) => {
        if (res) {
          this.form.patchValue(res);
          this.listTemp = res?.content || [];
          this.pageable = {
            totalElements: res.totalElements || 0,
            totalPages: res.totalPages || 0,
            currentPage: res.number || 0,
            size: this.param.size,
          };
          this.lead = this.listTemp.find((item) => item.isLeader === true) || {hrisCode: null};
          this.search(true);
        }
      });
    }
  }

  search(isSearch: boolean) {
  //  console.log('temp: ',this.listTemp);
    if (isSearch) {
      this.textSearch = this.textSearch.trim().toLowerCase();
      this.paramSearch.search = this.textSearch;
    }
    const list = _.map(this.listTemp, (x) => ({
      ...x,
      name: Utils.parseToEnglish(`${x.rmCode} ${x.rmName}`),
      id: x.huddleMemberId
    }));
    if (Utils.isStringEmpty(this.textSearch)) {
      this.datas = [...list];
      this.listRm = _.slice(list,
        _.get(this.paramSearch, 'page') * _.get(this.paramSearch, 'size'),
        _.get(this.paramSearch, 'page') * _.get(this.paramSearch, 'size') + _.get(this.paramSearch, 'size'));
    } else {
      const search = Utils.parseToEnglish(this.textSearch);
    //  console.log('text search: ',search);
      this.datas = _.filter(list, (i) => {
        return i.name.toLowerCase().indexOf(search) !== -1;
      });
      this.listRm = _.slice(this.datas,
        _.get(this.paramSearch, 'page') * _.get(this.paramSearch, 'size'),
        _.get(this.paramSearch, 'page') * _.get(this.paramSearch, 'size') + _.get(this.paramSearch, 'size'));
    }
    this.pageable = {
      totalElements: this.datas.length || 0,
      totalPages: this.datas.length / this.paramSearch.size,
      currentPage: this.paramSearch.page,
      size: this.paramSearch.size,
    }
  }

  exportFile() {
    if (_.size(this.datas) === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    const data = [];
    let obj: any = {};
    _.forEach(this.datas, (item, index) => {
      obj = {};
      obj[this.fields.order] = index + 1;
      if (this.past) {
        obj[this.fields.type] = this.rmType;
      }
      obj[this.fields.rmCode] = item.rmCode;
      obj[this.fields.rmName] = item.rmName;
      obj[this.fields.leader] = item.isLeader ? 'X' : '';
      obj[this.fields.effectiveDate] = this.datePipe.transform(item.joinDate, 'dd/MM/yyyy');
      if (!this.past) {
        obj[this.fields.endDate] = this.datePipe.transform(item.leftDate, 'dd/MM/yyyy');
        // obj[this.fields.endDate] = moment(item.leftDate).format('dd/MM/yyyy');
      }
      data.push(obj);
    });
    this.exportExcelService.exportAsExcelFile(
      data,
      `${this.past ? 'Nhóm' : 'Lịch sử nhóm'} Huddle_${this.groupCode}`
    );
  }

  setPage(pageInfo) {
    this.paramSearch.page = pageInfo.offset;
    this.search(false);
  }

  onSelectedFn(item, event) {
    if (this.isUpdate) {
      this.lead = event.checked ? {...item} : {hrisCode: null};
      this.listTemp.forEach((itemOld) => {
        itemOld.isLeader = itemOld.hrisCode === this.lead.hrisCode;
      });
      this.listRm?.forEach((itemRm) => {
        itemRm.isLeader = false;
      });

      item.isLeader = event.checked;
      this.genInfoLeaderGroup();
    }
  }

  deleteMember(item) {
    if (item.hrisCode === this.lead?.hrisCode) {
      this.lead = { hrisCode: null };
      this.genInfoLeaderGroup();
    }
    console.log(this.listTemp);
    this.listTemp = this.listTemp.filter((itemOld) => {
      return itemOld.hrisCode !== item.hrisCode;
    });
    this.search(false);
  }

  handleChangePageSizeMember(value) {
    this.paramSearch.page = 0;
    this.paramSearch.size = value;
    this.search(false);
  }

  confirmDialog() {
      if (this.listTemp.length < 1) {
        this.confirmService.warn(this.notificationMessage.RM003);
        return;
      }
      if (this.lead.hrisCode === null) {
        this.confirmService.warn(this.notificationMessage.RM001);
        return;
      }
      // if(this.listTemp.length > 50){
      //   this.confirmService.warn(this.notificationMessage.RM006);
      //   return;
      // }
      const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result
        .then((res) => {
          if (res) {
            this.isLoading = true;
            const data = cleanDataForm(this.form);
            const listMemberNew = this.listTemp.map((item) => item.hrisCode);
            data.groupId = this.groupId;
            data.leadGroup = this.lead.hrisCode;
            data.rsId = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.HUDDLE_GROUP}`)?.rsId;
            data.scope = Scopes.VIEW;
            data.memberListDel = this.getElement(this.listMemberOld, listMemberNew);
            data.memberList = this.getElement( listMemberNew, this.listMemberOld);
            console.log('them moi: ',data.memberList);
            console.log('xoa: ',data.memberListDel);
            this.huddleApi.updateGroup(data).subscribe(
              (response) => {
                if (Utils.isStringNotEmpty(_.get(response, 'errorValue'))) {
                  this.confirmService.error(_.get(response, 'errorValue'));
                  this.isLoading = false;
                  return;
                }
                this.messageService.success(this.notificationMessage.success);
                this.isLoading = false;
              //  const id = _.get(data, 'huddleGroupId');
                this.router.navigate(['rm360/huddle-group/detail', this.groupId], {
                  queryParams: {
                    groupCode: this.groupCode,
                    groupName: this.groupName,
                    blockCode: this.form.controls.blockCode.value}
                });
              },
              (e) => {
                this.isLoading = false;
                if (e?.error) {
                  if(_.get(e, 'error.errorCode') === this.CODE_WARNING_BRANCH){
                    const confirmSave = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
                    confirmSave.componentInstance.message = _.get(e, 'error.messages.vn') + '. Bạn muốn tiếp tục lưu?';
                    confirmSave.componentInstance.title = 'Cảnh báo';
                    confirmSave.result
                      .then((confirmed: boolean) => {
                        if (confirmed) {
                          this.isLoading = true;
                          data.isSave = true;
                          this.huddleApi.updateGroup(data).subscribe((resNew:any) => {
                            if (Utils.isStringNotEmpty(_.get(resNew, 'errorValue'))) {
                              this.confirmService.error(_.get(resNew, 'errorValue'));
                              this.isLoading = false;
                              return;
                            }
                            this.messageService.success(this.notificationMessage.success);
                            this.isLoading = false;
                          //  const id = _.get(data, 'huddleGroupId');
                            this.router.navigate(['rm360/huddle-group/detail', this.groupId], {
                              queryParams: {
                                groupCode: this.groupCode,
                                groupName: this.groupName,
                                blockCode: this.form.controls.blockCode.value}
                            });
                          }, (er) => {
                            this.isLoading = false;
                            if (er?.error) {
                              if(_.get(er, 'error.messages.vn')){
                                this.confirmService.warn(_.get(er, 'error.messages.vn'));
                              }
                              else{
                                this.messageService.error(this.notificationMessage.error);
                              }
                            } else {
                              this.messageService.error(this.notificationMessage.error);
                            }
                          });
                        }
                      })
                      .catch(() => {
                        this.messageService.error(this.notificationMessage.error);
                      });
                  }
                  else{
                    if(_.get(e, 'error.messages.vn')){
                      this.confirmService.warn(_.get(e, 'error.messages.vn'));
                    }
                    else{
                      this.messageService.error(this.notificationMessage.error);
                    }
                  }
                } else {
                  this.messageService.error(this.notificationMessage.error);
                }
              }
            );
          }
        })
        .catch(() => {
          this.messageService.error(this.notificationMessage.error);
        });
  }
  back() {
    this.router.navigate(['rm360/huddle-group'], { state: this.prop });
  }
  genInfoLeaderGroup(){
    if(Utils.trimNullToEmpty(this.form.controls.blockCode.value) !== '' ){
      this.groupCode = this.form.controls.blockCode.value ;
      this.groupName = this.form.controls.blockCode.value ;
    }
    if(Utils.trimNullToEmpty(this.lead?.branchCode) !== ''){
      this.groupCode += '_' + this.lead.branchCode;
      this.groupName += '_' + this.lead.branchCode;
    }
    if(Utils.trimNullToEmpty(this.lead?.hrisCode) !== ''){
      this.groupCode += '_' + this.lead.hrisCode;
    }
    if(Utils.trimNullToEmpty(this.lead?.rmName) !== ''){
      this.groupName += '_' + this.lead.rmName;
    }
    this.form.controls.groupCode.setValue(this.groupCode);
    this.form.controls.groupName.setValue(this.groupName);
  }
  getElement(listMemberFirst,listMemberSecond){
    const newList = listMemberFirst.filter((i) => !listMemberSecond.includes(i));
    return newList;
  }
}
