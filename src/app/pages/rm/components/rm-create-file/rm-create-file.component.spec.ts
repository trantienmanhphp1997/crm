import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmCreateFileComponent } from './rm-create-file.component';

describe('RmCreateFileComponent', () => {
  let component: RmCreateFileComponent;
  let fixture: ComponentFixture<RmCreateFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmCreateFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmCreateFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
