import { CustomerAssignmentApi } from './../../../customer-360/apis/customer-assignment.api';
import { global } from '@angular/compiler/src/util';
import { Component, Injector, Input, OnInit, ViewChild, ViewEncapsulation, HostBinding } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { ExportExcelService } from 'src/app/core/services/export-excel.service';
import { CustomerType, maxInt32 } from 'src/app/core/utils/common-constants';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { RmApi } from '../../apis';
import { DatePipe } from '@angular/common';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FileService } from 'src/app/core/services/file.service';

@Component({
  selector: 'app-rm-customer-history',
  templateUrl: './rm-customer-history.component.html',
  styleUrls: ['./rm-customer-history.component.scss'],
  providers: [DatePipe],
  encapsulation: ViewEncapsulation.None,
})
export class RmCustomerHistoryComponent extends BaseComponent implements OnInit {
  @HostBinding('class') hostClass = 'app-rm-customer-history';
  isLoading = false;
  listData = [];
  limit = global.userConfig.pageSize;
  paramSearch = {
    size: this.limit,
    page: 0,
    search: '',
    hrsCode: '',
  };
  textSearch = '';
  pageable: Pageable;
  isSearch = false;
  param: any;
  messages = global.messageTable;
  infoRm: string;
  @ViewChild('table') table: DatatableComponent;
  @Input() hrsCode: string;
  @Input() rmBlock: string;

  constructor(
    injector: Injector,
    private exportExcelService: ExportExcelService,
    private customerAssignmentApi: CustomerAssignmentApi,
    private rmApi: RmApi,
    private datePipe: DatePipe,
    private modalActive: NgbActiveModal,
    private fileService: FileService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    if (this.param) {
      this.paramSearch = this.prop.paramSearch;
      this.textSearch = this.paramSearch.search;
    }
    const hrsCode = this.hrsCode;
    if (hrsCode) {
      this.paramSearch.hrsCode = hrsCode;
      this.rmApi.getByCode(hrsCode).subscribe((itemRm) => {
        this.infoRm = (itemRm?.t24Employee?.employeeCode || '') + ' - ' + (itemRm?.hrisEmployee?.fullName || '');
      });
    }
  }

  isClickSearch() {
    this.isSearch = false;
  }

  search(isSearch: boolean) {
    this.isLoading = true;
    if (isSearch) {
      this.paramSearch.page = 0;
      this.isSearch = isSearch;
      this.textSearch = this.textSearch.trim();
      this.paramSearch.search = this.textSearch;
    }
    if (this.rmBlock === '' || this.rmBlock === CustomerType.INDIV) {
      this.customerAssignmentApi.searchHistory(this.paramSearch, true).subscribe(
        (result) => {
          if (result) {
            this.listData = result.content;
            this.pageable = {
              totalElements: result.totalElements,
              totalPages: result.totalPages,
              currentPage: result.number,
              size: this.limit,
            };
            this.isLoading = false;
          }
        },
        () => {
          this.isLoading = false;
        }
      );
    } else {
      this.customerAssignmentApi.searchHistory(this.paramSearch, false).subscribe(
        (result) => {
          if (result) {
            this.listData = result.content;
            this.pageable = {
              totalElements: result.totalElements,
              totalPages: result.totalPages,
              currentPage: result.number,
              size: this.limit,
            };
            
            this.isLoading = false;
          }
        },
        () => {
          this.isLoading = false;
        }
      );
    }
  }

  setPage(pageInfo) {
    this.paramSearch.page = pageInfo.offset;
    this.search(false);
  }

  exportFile() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+(this.pageable?.totalElements || 0) === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.isLoading = true;
    this.customerAssignmentApi.exportAssignRmHistory(this.paramSearch).subscribe(
      (fileId) => {
        if (fileId) {
          this.fileService.downloadFile(fileId, 'lich-su-phan-giao.xlsx').subscribe(
            (res) => {
              this.isLoading = false;
              if (!res) {
                this.messageService.error(this.notificationMessage.error);
              }
            },
            () => {
              this.isLoading = false;
            }
          );
        }
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }

  back() {
    this.router.navigate([`/rm360/rm-manager`], { state: this.prop });
  }

  closeModal() {
    this.modalActive.close();
  }
}
