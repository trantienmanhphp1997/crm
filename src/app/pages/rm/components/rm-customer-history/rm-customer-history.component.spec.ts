import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmCustomerHistoryComponent } from './rm-customer-history.component';

describe('RmCustomerHistoryComponent', () => {
  let component: RmCustomerHistoryComponent;
  let fixture: ComponentFixture<RmCustomerHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmCustomerHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmCustomerHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
