import { Component, OnInit, Injector, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { FunctionCode, Scopes } from 'src/app/core/utils/common-constants';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { RmFluctuateApi } from '../../apis';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-rm-fluctuate-check',
  templateUrl: './rm-fluctuate-check.component.html',
  styleUrls: ['./rm-fluctuate-check.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class RmFluctuateCheckComponent extends BaseComponent implements OnInit {
  formCode = this.fb.group({
    code: ['', CustomValidators.required],
  });
  messageError: string;
  isError: boolean;

  constructor(
    injector: Injector,
    private rmFuluctuateApi: RmFluctuateApi,
    private categoryService: CategoryService,
    private modalActive: NgbActiveModal
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_FLUCTUATE}`);
  }

  ngOnInit(): void {
    this.formCode.valueChanges.subscribe((value) => {
      this.isError = false;
    });
  }

  checkRm() {
    if (this.formCode.valid) {
      this.isError = false;
      this.isLoading = true;
      this.rmFuluctuateApi
        .checkBeforeCreate(this.formCode.controls.code.value?.trim(), {
          rsId: this.objFunction?.rsId,
          scope: Scopes.VIEW,
        })
        .subscribe(
          (itemRm) => {
            this.isLoading = false;
            this.modalActive.close(itemRm);
          },
          (e) => {
            this.isLoading = false;
            this.isError = true;
            this.messageError = e?.error?.description;
          }
        );
    } else {
      this.isError = true;
      validateAllFormFields(this.formCode);
    }
  }

  closeModal() {
    setTimeout(() => {
      this.modalActive.close(false);
    }, 100);
  }
}
