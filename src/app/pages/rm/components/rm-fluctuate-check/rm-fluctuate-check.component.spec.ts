import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmFluctuateCheckComponent } from './rm-fluctuate-check.component';

describe('RmFluctuateCheckComponent', () => {
  let component: RmFluctuateCheckComponent;
  let fixture: ComponentFixture<RmFluctuateCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmFluctuateCheckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmFluctuateCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
