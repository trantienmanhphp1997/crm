import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmGroupUpdateComponent } from './rm-group-update.component';

describe('RmGroupUpdateComponent', () => {
  let component: RmGroupUpdateComponent;
  let fixture: ComponentFixture<RmGroupUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmGroupUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmGroupUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
