import { DatePipe } from '@angular/common';
import { ExportExcelService } from 'src/app/core/services/export-excel.service';
import { global } from '@angular/compiler/src/util';
import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { FunctionCode, maxInt32, RmType, Scopes } from 'src/app/core/utils/common-constants';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { Utils } from 'src/app/core/utils/utils';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { RmGroupApi } from '../../apis';
import { RmModalComponent } from '../rm-modal/rm-modal.component';
import _ from 'lodash';

@Component({
  selector: 'app-rm-group-update',
  templateUrl: './rm-group-update.component.html',
  styleUrls: ['./rm-group-update.component.scss'],
  providers: [DatePipe],
})
export class RmGroupUpdateComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listRm = [];
  listTemp = [];
  listBlock = [];
  leader: any = { hrsCode: null };
  groupCode = '';
  groupName = '';
  params = {
    page: 0,
    size: global.userConfig.pageSize,
    search: '',
    isHistory: false,
  };
  pageable: Pageable;
  form = this.fb.group({
    id: [''],
    code: [{ value: '', disabled: true }, [CustomValidators.required]],
    name: [{ value: '', disabled: true }, [CustomValidators.required]],
    blockCode: ['', [CustomValidators.required]],
  });
  isUpdate: boolean;
  groupId: string;
  messages = global.messageTable;
  isFisrt = true;
  textSearch = '';
  rmType: string;
  datas: Array<any>;
  past = true;
  @ViewChild('table') table: DatatableComponent;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private rmGroupApi: RmGroupApi,
    private exportExcelService: ExportExcelService,
    private datePipe: DatePipe
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_GROUP}`);

    this.groupId = this.route.snapshot.paramMap.get('id');
    this.isUpdate = _.get(this.route.snapshot.data, 'isUpdate') || false;
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
    this.form.controls.blockCode.disable();
    if (!this.isUpdate) {
      this.form.disable();
    }
  }

  ngOnInit(): void {
    this.rmType = RmType.RM;
    if (!this.groupId) {
      return;
    }
    this.isLoading = true;
    const params = JSON.parse(JSON.stringify(this.params));
    params.size = maxInt32;
    this.rmGroupApi.searchGroupById(this.groupId, params).subscribe(
      (result) => {
        if (result) {
          this.form.patchValue(result);
          this.listTemp = result.rmList?.content || [];
          this.leader = this.listTemp.find((item) => item.isLeader) || { hrsCode: null };
          this.pageable = {
            totalElements: result.rmList?.totalElements || 0,
            totalPages: result.rmList?.totalPages || 0,
            currentPage: result.rmList?.number || 0,
            size: this.params.size,
          };
          this.isFisrt = false;
          this.search(true);
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
    this.categoryService.getBlocksCategoryByRm().subscribe((res) => {
      this.listBlock = res.map((item) => {
        return {
          code: item.code,
          displayName: item.code + ' - ' + item.name,
          name: item.name,
        };
      });
    });
  }

  confirmDialog() {
    if (this.form.valid) {
      if (this.listTemp.length < 1) {
        this.confirmService.error(this.notificationMessage.RM003);
        return;
      }
      if (!this.leader) {
        this.confirmService.error(this.notificationMessage.RM001);
        return;
      }
      const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result
        .then((res) => {
          if (res) {
            this.isLoading = true;
            const data = cleanDataForm(this.form);
            data.branchCode = this.leader.branchCode;
            data.branchName = this.leader.branchName;
            data.rsId = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`)?.rsId;
            data.scope = Scopes.VIEW;
            data.lstMaps = this.listTemp;
            this.rmGroupApi.update(data).subscribe(
              (response) => {
                if (Utils.isStringNotEmpty(_.get(response, 'errorValue'))) {
                  this.confirmService.error(_.get(response, 'errorValue'));
                  this.isLoading = false;
                  return;
                }
                this.messageService.success(this.notificationMessage.success);
                this.isLoading = false;
                const id = _.get(data, 'id');
                this.router.navigate(['rm360/rm-group/detail', id]);
              },
              (e) => {
                if (e?.error) {
                  this.confirmService.warn(_.get(e, 'error.description'));
                } else {
                  this.messageService.error(this.notificationMessage.error);
                }
                this.isLoading = false;
              }
            );
          }
        })
        .catch(() => {});
    } else {
      validateAllFormFields(this.form);
    }
  }

  selectMembers() {
    if (this.form.controls.blockCode.value === '') {
      return;
    }
    const formSearch = {
      crmIsActive: { value: 'true', disabled: true },
      rmBlock: { value: this.form.controls.blockCode.value, disabled: true },
      isRmGroup: true,
      groupId: this.form.controls.id.value,
    };
    const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
    modal.componentInstance.dataSearch = formSearch;
    modal.componentInstance.listHrsCode = this.listTemp.map((item) => item.hrsCode);
    modal.result
      .then((res) => {
        if (res) {
          const listSelected = res.listSelected.map((item) => {
            return {
              hrsCode: item.hrisEmployee?.employeeId,
              rmCode: item.t24Employee?.employeeCode,
              rmFullName: item.hrisEmployee?.fullName,
              isLeader: item.isLeader ? true : false,
              branchCode: item.t24Employee?.branchCode,
              branchName: item.t24Employee?.branchName,
              hrsCodeOld: item.hrisEmployee?.hrsCode,
            };
          });
          this.listTemp = _.uniqBy([...this.listTemp, ...listSelected], (i) => i.hrsCode);
          this.listTemp = _.remove(this.listTemp, (i) => _.indexOf(res.listRemove, i.hrsCode) === -1);
          this.search(true);
        }
      })
      .catch(() => {});
  }

  setPage(pageInfo) {
    this.params.page = pageInfo.offset;
    this.search(false);
  }

  search(isSearch: boolean) {
    if (isSearch) {
      this.params.page = 0;
      this.textSearch = this.textSearch.trim();
      this.params.search = this.textSearch;
    }
    const list = _.map(this.listTemp, (x) => ({
      ...x,
      name: Utils.parseToEnglish(`${x.rmCode} ${x.rmFullName}`),
      id: x.id,
    }));
    if (Utils.isStringEmpty(this.params.search)) {
      this.datas = [...list];
      this.listRm = _.slice(
        list,
        _.get(this.params, 'page') * _.get(this.params, 'size'),
        _.get(this.params, 'page') * _.get(this.params, 'size') + _.get(this.params, 'size')
      );
    } else {
      const search = Utils.parseToEnglish(this.params.search);
      const listRms = _.filter(list, (item) => {
        return Utils.trimNullToEmpty(item.name).toLowerCase().indexOf(search) !== -1;
      });
      this.datas = [...listRms];
      this.listRm = _.slice(
        listRms,
        _.get(this.params, 'page') * _.get(this.params, 'size'),
        _.get(this.params, 'page') * _.get(this.params, 'size') + _.get(this.params, 'size')
      );
    }

    this.pageable = {
      totalElements: _.size(this.datas),
      totalPages: _.size(this.datas) / _.get(this.params, 'size'),
      currentPage: _.get(this.params, 'page'),
      size: _.get(this.params, 'size'),
    };
  }

  onSelectedFn(item, event) {
    if (this.isUpdate) {
      this.leader = event.checked ? { ...item } : { hrsCode: null };
      this.listTemp.forEach((itemOld) => {
        itemOld.isLeader = itemOld.hrsCode === this.leader.hrsCode;
      });
      this.listRm?.forEach((itemRm) => {
        itemRm.isLeader = false;
      });

      item.isLeader = event.checked;
      this.generateGroupCodeAndName();
    }
  }

  selectBlock(value) {
    this.generateGroupCodeAndName();
  }

  deleteMember(item) {
    if (item.hrsCode === this.leader?.hrsCode) {
      this.leader = { hrsCode: null };
      this.generateGroupCodeAndName();
    }
    console.log(this.listTemp);
    this.listTemp = this.listTemp.filter((itemOld) => {
      return itemOld.hrsCode !== item.hrsCode;
    });
    this.search(false);
  }

  back() {
    this.router.navigate(['rm360/rm-group'], { state: this.prop });
  }

  generateGroupCodeAndName() {
    if (Utils.trimNullToEmpty(this.form.controls.blockCode.value) !== '') {
      this.groupCode = this.form.controls.blockCode.value;
      this.groupName = this.form.controls.blockCode.value;
    }
    if (Utils.trimNullToEmpty(this.leader?.branchCode) !== '') {
      this.groupCode += '_' + this.leader.branchCode;
      this.groupName += '_' + this.leader.branchCode;
    }
    if (Utils.trimNullToEmpty(this.leader?.hrsCode) !== '') {
      this.groupCode += '_' + this.leader.hrsCode;
    }
    if (Utils.trimNullToEmpty(this.leader?.rmFullName) !== '') {
      this.groupName += '_' + this.leader.rmFullName;
    }
    this.form.controls.code.setValue(this.groupCode);
    this.form.controls.name.setValue(this.groupName);
  }

  exportFile() {
    if (_.size(this.datas) === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    const data = [];
    let obj: any = {};
    _.forEach(this.datas, (item, index) => {
      obj = {};
      obj[this.fields.order] = index + 1;
      if (this.past) {
        obj[this.fields.type] = this.rmType;
      }
      obj[this.fields.rmCode] = item.rmCode;
      obj[this.fields.rmName] = item.rmFullName;
      obj[this.fields.leader] = item.isLeader ? 'X' : '';
      obj[this.fields.effectiveDate] = this.datePipe.transform(item.dateOfJoin, 'dd/MM/yyyy');
      if (!this.past) {
        obj[this.fields.endDate] = this.datePipe.transform(item.endDate, 'dd/MM/yyyy');
      }
      data.push(obj);
    });
    this.exportExcelService.exportAsExcelFile(
      data,
      `${this.past ? 'Nhóm' : 'Lịch sử nhóm'} RMCBQL_${this.form.controls.code.value}`
    );
  }

  onChangePast(value: boolean) {
    if (!value !== this.params.isHistory) {
      this.params.page = 0;
      this.params.isHistory = !value;
      const params = {
        ...this.params,
        size: maxInt32,
      };
      if (!this.groupId) {
        return;
      }
      this.isLoading = true;
      this.listRm = [];
      this.pageable = {
        totalElements: 0,
        totalPages: 0,
        currentPage: 0,
        size: this.params.size,
      };
      this.rmGroupApi.searchGroupById(this.groupId, params).subscribe(
        (result) => {
          if (result) {
            this.form.patchValue(result);
            this.listTemp = result.rmList?.content || [];
            this.pageable = {
              totalElements: result.rmList?.totalElements || 0,
              totalPages: result.rmList?.totalPages || 0,
              currentPage: result.rmList?.number || 0,
              size: this.params.size,
            };
            this.isFisrt = false;
            this.search(true);
          }
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
    }
  }
}
