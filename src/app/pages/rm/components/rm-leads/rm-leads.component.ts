import { of } from 'rxjs';
import { global } from '@angular/compiler/src/util';
import {
  Component,
  Injector,
  Input,
  OnInit,
  Output,
  EventEmitter,
  ViewEncapsulation,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { CommonCategory, FunctionCode, functionUri, Scopes, SessionKey } from 'src/app/core/utils/common-constants';
import { FileService } from 'src/app/core/services/file.service';
import * as _ from 'lodash';
import { cleanDataForm } from 'src/app/core/utils/function';
import { CustomerLeadService } from 'src/app/pages/lead/service/customer-lead.service';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'rm-leads',
  templateUrl: './rm-leads.component.html',
  styleUrls: ['./rm-leads.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class RmLeadsComponent extends BaseComponent implements OnInit, OnChanges {
  listData = [];
  pageable: Pageable;
  searchForm = this.fb.group({
    page: 0,
    size: _.get(global, 'userConfig.pageSize'),
    rsId: '',
    scope: Scopes.VIEW,
    leadCode: '',
    fullName: '',
    rmCode: '',
  });
  prevParams: any;
  @Input() rmCode: string;
  @Output() loading: EventEmitter<any> = new EventEmitter<any>();
  listCustomerType = [];
  loadCommon: boolean = false;

  constructor(injector: Injector, private service: CustomerLeadService, private fileService: FileService) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.LEAD}`);
    this.searchForm.controls.rsId.setValue(this.objFunction?.rsId);
    this.isLoading = true;
  }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges) {
    if (this.rmCode) {
      const propData = this.sessionService.getSessionData(SessionKey.LIST_LEAD_OF_RM);
      if (propData?.rmCode === this.rmCode) {
        this.searchForm.patchValue(propData);
      }
      this.searchForm.controls.rmCode.setValue(this.rmCode);
      if (!this.loadCommon) {
        this.commonService
          .getCommonCategory(CommonCategory.LEAD_TYPE)
          .pipe(catchError(() => of(undefined)))
          .subscribe((leadTypeConfig) => {
            this.loadCommon = true;
            this.listCustomerType = leadTypeConfig?.content || [];
            this.search();
          });
      } else {
        this.search();
      }
    }
  }

  search(isSearch?: boolean) {
    this.isLoading = true;
    let params = cleanDataForm(this.searchForm);
    if (isSearch) {
      this.searchForm.controls.page.setValue(0);
      params.page = 0;
    }
    this.service.searchLeadByRm(params).subscribe(
      (data) => {
        this.listData = data?.content || [];
        this.listData?.forEach((item) => {
          if (item.customerType === '0') {
            item.customerTypeName = _.find(this.listCustomerType, (i) => i.code === '0')?.name;
          } else if (item.customerType === '1' && item.isFi) {
            item.customerTypeName = _.find(this.listCustomerType, (i) => i.code === '2')?.name;
          } else if (item.customerType === '1') {
            item.customerTypeName = _.find(this.listCustomerType, (i) => i.code === '1')?.name;
          }
        });
        this.pageable = {
          size: params.size,
          totalElements: data?.totalElements,
          totalPages: data?.totalPages,
          currentPage: params.page,
        };
        this.isLoading = false;
        this.prevParams = params;
        this.sessionService.setSessionData(SessionKey.LIST_LEAD_OF_RM, this.prevParams);
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.searchForm.controls.page.setValue(pageInfo.offset);
    this.search(false);
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      this.router.navigate([functionUri.lead_management, 'detail', item.leadCode]);
    }
  }

  exportFile() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+(this.pageable?.totalElements || 0) === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.isLoading = true;
    this.service.exportFileLeadByRm(this.prevParams).subscribe(
      (fileId) => {
        if (!_.isEmpty(fileId)) {
          this.fileService
            .downloadFile(fileId, 'crm_lead.xlsx')
            .pipe(catchError((e) => of(false)))
            .subscribe((res) => {
              this.isLoading = false;
              if (!res) {
                this.messageService.error(this.notificationMessage.error);
              }
            });
        } else {
          this.messageService.error(this.notificationMessage?.export_error || '');
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(this.notificationMessage?.export_error || '');
        this.isLoading = false;
      }
    );
  }
}
