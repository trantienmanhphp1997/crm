import {forkJoin, Observable, of, Subscription} from 'rxjs';
import { global } from '@angular/compiler/src/util';
import { Component, Injector, Input, OnInit, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import {
  CommonCategory,
  Division,
  FunctionCode,
  functionUri,
  Scopes,
  SessionKey,
} from 'src/app/core/utils/common-constants';
import { Utils } from 'src/app/core/utils/utils';
import { CustomerApi } from 'src/app/pages/customer-360/apis';
import { FileService } from 'src/app/core/services/file.service';
import * as _ from 'lodash';
import { HistoryAssignmentComponent } from 'src/app/pages/customer-360/components';
import {catchError} from "rxjs/operators";
import {CategoryService} from "../../../system/services/category.service";

@Component({
  selector: 'rm-customers',
  templateUrl: './rm-customers.component.html',
  styleUrls: ['./rm-customers.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class RmCustomersComponent extends BaseComponent implements OnInit {
  listData = [];
  listSearchType = [];
  pageable: Pageable;
  params = {
    pageNumber: 0,
    pageSize: global?.userConfig?.pageSize,
    rsId: '',
    scope: Scopes.VIEW,
    hrsCode: '',
    search: '',
    searchFastType: '',
    orderBy: 'ASSIGNED_DATE',
    customerType: '',
    manageType: ''
  };
  prevParams: any;
  backDate: number;
  isCount = false;
  subCount: Subscription;
  @Input() propData: any;
  @Input() hrsCode: string;
  @Input() rmCode: string;
  @Output() loading: EventEmitter<any> = new EventEmitter<any>();
  listDivision = [];
  divisionConfig = [];
  isSmeTable = false;

  constructor(injector: Injector, private customerApi: CustomerApi, private fileService: FileService,
              private categoryService: CategoryService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CUSTOMER_360_MANAGER}`);

    this.params.rsId = this.objFunction?.rsId;
    this.isLoading = true;
  }

  ngOnInit(): void {
    forkJoin([
      this.commonService.getCommonCategory(CommonCategory.KHOI_PRIORITY).pipe(catchError((e) => of(undefined))),
      this.categoryService.checkManageType(this.objFunction?.rsId , Scopes.VIEW).pipe(catchError((e) => of(undefined))),
    ]).subscribe(([result, listManageType]) => {
      if (listManageType && listManageType.length > 0) {
        const itemSelected = listManageType.filter((item) => {
          return item.selected === true;
        });
        this.params.manageType = itemSelected[0].code;
      }

      let listConfigDivision = _.get(result, 'content') || [];
      listConfigDivision = _.orderBy(listConfigDivision, ['value']);
      this.divisionConfig = listConfigDivision?.map((item) => item.code);
      let listDivision =
        this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM)?.map((item) => {
          return {
            code: item.code,
            displayName: item.code + ' - ' + item.name,
            order:
              _.findIndex(this.divisionConfig, (value) => value === item.code) === -1
                ? 99
                : _.findIndex(this.divisionConfig, (value) => value === item.code),
          };
        }) || [];
      listDivision = _.orderBy(listDivision, ['order']);
      this.listDivision = listDivision;
      this.params.customerType = _.first(this.listDivision)?.code;

      this.listSearchType = [
        { code: 'CUSTOMER_CODE', name: this.fields.customerCode },
        { code: 'CUSTOMER_NAME', name: this.fields.customerName },
      ];
      this.params.searchFastType = 'CUSTOMER_CODE';
      this.params.hrsCode = this.hrsCode;
      const propData = this.sessionService.getSessionData(SessionKey.LIST_CUSTOMER_OF_RM);
      if (propData?.params?.hrsCode === this.params.hrsCode) {
        this.params.searchFastType = propData.params.searchFastType;
        this.params.customerType = propData.params.customerType;
        this.params.search = propData.params.search;
      }
      this.loading.emit(true);
      this.search(true);
    });

    this.commonService.getCommonCategory(CommonCategory.KHOI_PRIORITY).subscribe((result) => {

    });
  }

  search(isSearch?: boolean) {
    this.isLoading = true;
    let params: any = {};
    if (isSearch) {
      this.params.pageNumber = 0;
      this.params.search = Utils.trimNullToEmpty(this.params.search);
      params = this.params;
    } else {
      if (!this.prevParams) {
        return;
      }
      params = this.prevParams;
    }
    params.pageNumber = this.params.pageNumber;
    let apiSearch: Observable<any>;
    if (params.customerType !== Division.INDIV) {
      params.rmCodeManager = this.rmCode;
      apiSearch = this.customerApi.searchSme(params);
      this.isSmeTable = true;
    } else {
      apiSearch = this.customerApi.search(params);
      this.isSmeTable = false;
    }
    apiSearch.subscribe(
      (listData) => {
        this.listData = listData || [];
        for (const item of this.listData) {
          item.branchManager = (item.branchCode || '') + ' - ' + (item.branchName || '');
        }
        if (!this.isCount) {
          this.pageable = {
            totalElements: this.listData.length,
            totalPages: 0,
            currentPage: 0,
            size: this.params.pageSize,
          };
        }
        this.pageable.currentPage = this.params.pageNumber;
        this.prevParams = params;
        this.sessionService.setSessionData(SessionKey.LIST_CUSTOMER_OF_RM, {
          params: this.prevParams,
          pageable: this.pageable,
        });
        this.isLoading = false;
      },
      (e) => {
        this.isLoading = false;
      }
    );
    if (isSearch) {
      this.getCountCustomer(params);
    }
  }

  getCountCustomer(params) {
    this.isCount = false;
    this.subCount?.unsubscribe();
    let apiCount: Observable<any>;
    if (params.customerType !== Division.INDIV) {
      apiCount = this.customerApi.countSme(params);
    } else {
      apiCount = this.customerApi.count(params);
    }
    this.subCount = apiCount.subscribe(
      (count) => {
        this.isCount = true;
        this.pageable = {
          totalElements: count,
          totalPages: Math.floor(count / this.params.pageSize),
          currentPage: this.params.pageNumber,
          size: this.params.pageSize,
        };
        this.sessionService.setSessionData(SessionKey.LIST_CUSTOMER_OF_RM, {
          params: this.prevParams,
          pageable: this.pageable,
        });
      },
      () => {
        this.isCount = true;
        this.pageable = {
          totalElements: 0,
          totalPages: 0,
          currentPage: this.params.pageNumber,
          size: this.params.pageSize,
        };
        this.sessionService.setSessionData(SessionKey.LIST_CUSTOMER_OF_RM, {
          params: this.prevParams,
          pageable: this.pageable,
        });
      }
    );
  }

  setPage(pageInfo) {
    if (!this.isLoading) {
      this.params.pageNumber = pageInfo.offset;
      this.search(false);
    }
  }

  historyAssign(item: any) {
    const modal = this.modalService.open(HistoryAssignmentComponent, { windowClass: 'customer-history-assignment' });
    modal.componentInstance.hrsCode = item?.customerCode;
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      const item = event.row;
      if (!_.isEmpty(_.trim(item.customerTypeMerge)) && !_.isEmpty(_.trim(item.customerCode))) {
        this.router.navigateByUrl(
          `${functionUri.customer_360_manager}/detail/${_.toLower(item.customerTypeMerge)}/${item.customerCode}`,
          {
            state: this.getPropData(),
            skipLocationChange: true,
          }
        );
      }
    }
  }

  getPropData() {
    return {
      prevParams: this.prevParams,
      listData: this.listData,
      pageable: this.pageable,
      tabIndex: 1,
    };
  }

  exportFile() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+(this.pageable?.totalElements || 0) === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.isLoading = true;
    let apiSearch: Observable<any>;
    if (this.params.customerType !== Division.INDIV) {
      apiSearch = this.customerApi.createFileSME(this.prevParams);
    } else {
      apiSearch = this.customerApi.createFile(this.prevParams);
    }
    apiSearch.subscribe(
      (res) => {
        if (Utils.isStringNotEmpty(res)) {
          this.download(res);
        } else {
          this.messageService.error(this.notificationMessage?.export_error || '');
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(this.notificationMessage?.export_error || '');
        this.isLoading = false;
      }
    );
  }

  download(fileId: string) {
    this.fileService.downloadFile(fileId, 'crm_customer.xlsx').subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }
}
