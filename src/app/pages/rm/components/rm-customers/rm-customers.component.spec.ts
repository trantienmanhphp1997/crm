import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmCustomersComponent } from './rm-customers.component';

describe('RmCustomersComponent', () => {
  let component: RmCustomersComponent;
  let fixture: ComponentFixture<RmCustomersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmCustomersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmCustomersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
