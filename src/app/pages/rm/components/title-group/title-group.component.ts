import { Component, ViewEncapsulation, ViewChild, OnInit, HostBinding } from '@angular/core';
import { TitleGroupApi } from '../../apis';
import { maxInt32, Scopes, FunctionCode } from 'src/app/core/utils/common-constants';
import { ExportExcelService } from 'src/app/core/services/export-excel.service';
import * as _ from 'lodash';
import { RmService } from '../../services';
import { Utils } from '../../../../core/utils/utils';
import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import { ConfirmDialogComponent } from '../../../../shared/components/confirm-dialog/confirm-dialog.component';
import { AppFunction } from 'src/app/core/interfaces/app-function.interface';
import { SessionService } from 'src/app/core/services/session.service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-title-group',
  templateUrl: './title-group.component.html',
  styleUrls: ['./title-group.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class TitleGroupComponent implements OnInit {
  constructor(
    private api: TitleGroupApi,
    private service: RmService,
    private exportExcelService: ExportExcelService,
    private sessionService: SessionService,
    private translate: TranslateService,
    private router: Router,
    private messageService: NotifyMessageService,
    private modal: NgbModal,
  ) {
    this.isLoading = true;
    this.obj = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_TITLE}`);
    this.rsId = _.get(this.obj, 'rsId');
    this.scope = Scopes.VIEW;
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
  }

  @ViewChild('tablerm') public tablerm: DatatableComponent;
  @HostBinding('class.app__right-content') appRightContent = true;
  obj: AppFunction;
  rsId: string;
  scope: string;
  blocks: Array<any>;
  models: Array<any>;
  pageable: Pageable;
  status: Array<any> = [];
  isLoading: boolean = false;
  model: Array<any> = [];
  fields: any;
  notificationMessage: any;
  messages = global.messageTable;
  params: any = {
    size: global.userConfig.pageSize,
    page: 0,
  };
  prevParams = this.params;
  prop: any;
  isFisrt: boolean = true;

  ngOnInit() {
    this.isLoading = false;

    if (Utils.isNotNull(this.prop)) {
      this.params = this.prop;
    }
    this.service.blocks().then((response) => {
      this.blocks = _.map(response, (x) => ({ ...x, value_to_search: `${x.code} - ${x.name}` }));
      if (Utils.isNull(this.prop)) {
        this.params.blockCode = _.get(_.chain(this.blocks).first().value(), 'code');
        this.reload();
      }
    });
    this.translate.get(['fields', 'notificationMessage']).subscribe((result) => {
      this.fields = result.fields;
      this.notificationMessage = result.notificationMessage;
      this.status.push({ code: '', name: this.fields.all });
      this.status.push({ code: 'true', name: this.fields.effective });
      this.status.push({ code: 'false', name: this.fields.expire });
    });
    this.reload(true);
  }

  exportFile() {
    this.translate.get('rm.fields').subscribe((fields) => {
      const fieldLabels = fields;
      let data = [];
      let obj: any = {};
      var params = this.prevParams;
      _.set(params, 'size', maxInt32);
      _.set(params, 'page', 0);
      this.api.fetch(params).subscribe((response) => {
        if (Utils.isArrayNotEmpty(_.get(response, 'content'))) {
          _.get(response, 'content').forEach((item) => {
            obj = {};
            obj[fieldLabels.index] = _.get(response, 'content').indexOf(item) + 1;
            obj[fieldLabels.grade] = _.get(item, 'blockCode');
            obj[fieldLabels.rm_group_code] = _.get(item, 'code');
            obj[fieldLabels.rm_group_name] = _.get(item, 'name');
            obj[fieldLabels.description] = _.get(item, 'description');
            data.push(obj);
          });
          this.exportExcelService.exportAsExcelFile(data, 'title-group');
        } else {
          this.messageService.error(this.notificationMessage.noRecord);
        }
      });
    });
  }

  ftime: boolean = true;
  paging($event) {
    if (this.isFisrt) {
      return;
    }
    this.params.page = _.get($event, 'offset');
    this.reload(false);
  }

  onActive($event) {
    let type = _.get($event, 'type');
    let id = _.get($event, 'row.id');
    if (type === 'dblclick') {
      if (Utils.isStringNotEmpty(id)) {
        this.router.navigate([`/rm360/title-category/view/${id}`], { state: this.prevParams });
      }
    }
  }

  search() {
    this.reload(true);
  }

  add() {
    this.router.navigate([`/rm360/title-category/create`], { state: this.prevParams });
  }

  update(value: any) {
    this.router.navigate([`/rm360/title-category/update/${this.parse_code(value)}`], { state: this.prevParams });
  }

  parse_code(value) {
    return _.get(value, 'id');
  }

  reload(isSearch?: boolean) {
    this.isLoading = true;
    let params: any = {};
    if (isSearch) {
      this.params.page = 0;
    } else {
      if (_.get(this.prevParams, 'size') === maxInt32) {
        _.set(this.prevParams, 'size', _.get(global, 'userConfig.pageSize'));
      }
      params = this.prevParams;
    }
    Object.keys(this.params).forEach((key) => {
      params[key] = this.params[key];
    });
    params.page = this.params.page;
    params.size = this.params.size;
    this.api.fetch(params).subscribe(
      (response) => {
        this.isFisrt = false;
        this.isLoading = false;
        this.models = _.get(response, 'content');
        this.pageable = {
          totalElements: _.get(response, 'totalElements'),
          totalPages: _.get(response, 'totalPages'),
          currentPage: _.get(response, 'number'),
          size: _.get(global, 'userConfig.pageSize'),
        };
        this.prevParams = params;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  delete(value: any) {
    const id = this.parse_code(value);
    const confirm = this.modal.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.result
      .then((response) => {
        if (response) {
          this.api.delete(id).subscribe(
            () => {
              this.messageService.success(_.get(this.notificationMessage, 'success') || '');
              this.reload(true);
              this.isLoading = false;
            },
            (e) => {
              this.messageService.error(_.get(e, 'error.description'));
              this.isLoading = false;
            }
          );
        }
      })
  }
}
