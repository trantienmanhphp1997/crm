import { Component, ChangeDetectorRef, HostBinding } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { LevelApi } from '../../apis';
import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { TranslateService } from '@ngx-translate/core';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { Utils } from 'src/app/core/utils/utils';

import _ from 'lodash';
import { RmService } from '../../services';

@Component({
  selector: 'app-level.action',
  templateUrl: './level.action.component.html',
  styleUrls: ['./level.action.component.scss'],
})
export class LevelActionComponent {
  form = this.fb.group({
    id: [''],
    code: ['', CustomValidators.required],
    name: ['', CustomValidators.required],
    // blockId: ['', CustomValidators.required],
    // titleGroupId: ['', CustomValidators.required],
    groupRmOnHRIS: [''],
    levelRmOnHRIS: [''],
    description: [''],
  });

  constructor(
    private fb: FormBuilder,
    private api: LevelApi,
    private messageService: NotifyMessageService,
    private tranService: TranslateService,
    private modal: NgbModal,
    route: ActivatedRoute,
    private ref: ChangeDetectorRef,
    private configModal: NgbModalConfig,
    private service: RmService,
    private router: Router
  ) {
    let val = _.get(route.snapshot.data, 'titleGroups.content');
    let titleVals = _.map(val, (x) => ({ ...x, value_to_search: `${x.blockCode} - ${x.name}` }));
    this.titleGroups = titleVals;
    this.titleGroupValues = [...titleVals];
    let blcks = _.get(route.snapshot.data, 'blocks');
    this.blocks = _.map(blcks, (x) => ({ ...x, value_to_search: `${x.code} - ${x.name}` }));
    this.isCreate = _.get(route.snapshot.data, 'isCreate');
    this.isUpdate = _.get(route.snapshot.data, 'isUpdate');
    this.isView = _.get(route.snapshot.data, 'isView');
    this.data = _.get(route.snapshot.data, 'data') || {};

    const navigation = this.router.getCurrentNavigation();
    const state = navigation?.extras?.state;
    this.param = state;
    this.tranService.get(['fields', 'notificationMessage', 'validationMessage', 'rm']).subscribe((result) => {
      this.fields = result.fields;
      this.notificationMessage = result.notificationMessage;
      this.validationMessage = result.validationMessage;
    });
    this.configModal.backdrop = 'static';
    this.configModal.keyboard = false;
    this.init();
  }

  param: any;
  fields: any;
  notificationMessage: any;
  title: string;
  status: Array<any>;
  data: any;
  facilities: Array<any> = [];
  message: any;
  isLoading: boolean;
  isCreate: boolean;
  isUpdate: boolean;
  isView: boolean;
  blocks: Array<any>;
  grade_mes: string;
  show_mes: boolean;
  title_category_mes: string;
  show_title_mes: boolean;
  titleGroups: Array<any>;
  titleGroupValues: Array<any>;
  title_group_code: string;
  show: boolean = true;
  block_code: string;
  title_code: string;
  validationMessage: any;
  @HostBinding('class.app__right-content') appRightContent = true;

  init() {
    if (this.isCreate) {
      this.convert_title('level_create');
      let block = _.chain(this.blocks).first().value();
      let bcode = _.get(block, 'code');
      this.block_code = bcode;
      if (Utils.isStringNotEmpty(bcode)) {
        this.titleGroupValues = _.chain(this.titleGroups)
          .filter((x) => x.blockCode === bcode)
          .value();
        this.title_code = _.get(_.chain(this.titleGroupValues).first().value(), 'id');
      } else {
        let codes = _.map(this.block_code, (x) => x.code);
        var vals = _.chain(this.titleGroups)
          .filter((x) => codes.includes(x.blockCode))
          .value();
        this.titleGroupValues = [...vals];
        if (Utils.isArrayNotEmpty(this.titleGroupValues)) {
          this.title_code = _.get(_.chain(this.titleGroupValues).first().value(), 'id');
        } else {
          this.title_code = '';
        }
      }
    }
    if (this.isUpdate) {
      this.block_code = _.get(this.data, 'blockCode');
      console.log(this.block_code);
      this.titleGroupValues = _.chain(this.titleGroups)
        .filter((x) => x.blockCode === this.block_code)
        .value();
      this.title_code = _.get(this.data, 'titleGroupId');
      var val = _.chain(this.titleGroupValues)
        .filter((x) => x.id === this.title_code)
        .value();
      if (Utils.isArrayEmpty(val)) {
        this.title_code = undefined;
      }
      this.convert_title('level_update');
    }
    if (this.isView) {
      this.block_code = _.get(this.data, 'blockCode');
      this.titleGroupValues = _.chain(this.titleGroups)
        .filter((x) => x.blockCode === this.block_code)
        .value();
      this.title_code = _.get(this.data, 'titleGroupId');
      this.convert_title('level_view');
      this.form.disable();
    }
    this.form.patchValue(this.data);
  }

  convert_title(value) {
    this.tranService.get(`rm.menu.${value}`).subscribe((res) => {
      this.title = res;
    });
  }

  onTitleCategoryChange($event) {
    if (Utils.isStringNotEmpty($event.value)) {
      this.show_title_mes = false;
      this.title_category_mes = '';
    } else {
      this.title_category_mes = _.get(this.validationMessage, 'required');
      this.show_title_mes = true;
    }
  }

  save() {
    if (Utils.isStringEmpty(this.block_code)) {
      this.grade_mes = _.get(this.validationMessage, 'required');
      this.show_mes = true;
      validateAllFormFields(this.form);
      return;
    } else {
      this.grade_mes = '';
      this.show_mes = false;
    }

    if (Utils.isStringEmpty(this.title_code)) {
      this.title_category_mes = _.get(this.validationMessage, 'required');
      this.show_title_mes = true;
      validateAllFormFields(this.form);
      return;
    } else {
      this.title_category_mes = '';
      this.show_title_mes = false;
    }
    if (this.form.valid) {
      const confirm = this.modal.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result
        .then((res) => {
          if (res) {
            this.isLoading = true;
            const fdata = cleanDataForm(this.form);
            const data = {
              id: _.get(this.data, 'id'),
              code: Utils.trim(_.get(fdata, 'code')),
              blockCode: this.block_code,
              titleGroupId: this.title_code,
              name: Utils.trim(_.get(fdata, 'name')),
              groupRmOnHRIS: Utils.trim(_.get(fdata, 'groupRmOnHRIS')),
              levelRmOnHRIS: Utils.trim(_.get(fdata, 'levelRmOnHRIS')),
              description: Utils.trim(_.get(fdata, 'description')),
            };
            if (this.isCreate) {
              this.create(data);
            }
            if (this.isUpdate) {
              this.update(data);
            }
          }
        })
        .catch(() => {
          this.isLoading = false;
        });
    } else {
      validateAllFormFields(this.form);
    }
  }

  onBlockChanged($event) {
    this.show = false;
    if (Utils.isStringNotEmpty($event.value)) {
      this.show_mes = false;
      this.grade_mes = '';
    } else {
      this.grade_mes = _.get(this.validationMessage, 'required');
      this.show_mes = true;
    }
    this.titleGroupValues = _.chain(this.titleGroups)
      .filter((x) => x.blockCode === $event.value)
      .value();
    if (Utils.isArrayNotEmpty(this.titleGroupValues)) {
      const timer = setTimeout(() => {
        this.title_code = _.get(_.chain(this.titleGroupValues).first().value(), 'id');
        clearTimeout(timer);
      }, 10);
    } else {
      this.title_code = undefined;
    }
    this.ref.detectChanges();
    this.show = true;
  }

  create(data) {
    this.api.create(data).subscribe(
      (result) => {
        this.messageService.success(this.notificationMessage?.success || '');
        this.router.navigateByUrl(`/rm360/level-category`);
        this.isLoading = false;
      },
      (e) => {
        if (e?.error) {
          this.messageService.error(e?.error?.description);
        } else {
          // this.messageService.error('Thêm mới không thành công')
          this.messageService.error(this.notificationMessage.error);
        }
        this.isLoading = false;
      }
    );
  }

  update(data) {
    this.api.update(data).subscribe(
      (result) => {
        this.messageService.success(this.notificationMessage?.success || '');
        this.router.navigateByUrl(`/rm360/level-category`);
        this.isLoading = false;
      },
      (e) => {
        if (e?.error) {
          this.messageService.error(e?.error?.description);
        } else {
          this.messageService.error(this.notificationMessage.error);
        }
        this.isLoading = false;
      }
    );
  }

  back() {
    this.isLoading = true;
    setTimeout(() => {
      this.router.navigate([`/rm360/level-category`], { state: this.param });
    }, 100);
    this.isLoading = false;
  }
}
