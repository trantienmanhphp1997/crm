import { cleanDataForm } from 'src/app/core/utils/function';
import { Component, OnInit, Injector } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { BaseTableComponent } from 'src/app/shared/base-table.component';
import { LevelApi } from '../../apis';
import { FunctionCode } from 'src/app/core/utils/common-constants';
import { SessionService } from 'src/app/core/services/session.service';

@Component({
  selector: 'app-rm-level',
  templateUrl: './rm-level.component.html',
  styleUrls: ['./rm-level.component.scss'],
})
export class RmLevelComponent extends BaseTableComponent<LevelApi> implements OnInit {
  formSearch = this.fb.group({
    code: [''],
    name: [''],
    blockCode: [''],
    titleGroupId: [''],
    isActive: ['true'],
  });
  listData = [];
  pageable: Pageable;
  objFunction: any;
  paramSearch = {
    page: 0,
    size: this.limit,
  };
  titleGroupValues = [];
  blocks = [];
  status = [];

  constructor(injjector: Injector, api: LevelApi, private fb: FormBuilder, private sessionService: SessionService) {
    super(injjector, api);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_TITLE}`);
  }

  ngOnInit(): void {
    const params = cleanDataForm(this.formSearch);
    params.page = this.paramSearch.page;
    params.size = this.paramSearch.size;
    this.api.fetch(params).subscribe((result) => {
      this.listData = result.content;
      this.pageable = {
        totalElements: result.totalElements,
        totalPages: result.totalPages,
        currentPage: result.number,
        size: this.limit,
      };
    });
  }

  setPage(event) {
    this.paramSearch.page = event.pageInfo;
    this.ngOnInit();
  }

  reload(isSearch?: boolean) {}

  update(item) {}

  delete(item) {}

  exportFile() {}

  onBlockChanged(e) {}
}
