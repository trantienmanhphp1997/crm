import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmLevelComponent } from './rm-level.component';

describe('RmLevelComponent', () => {
  let component: RmLevelComponent;
  let fixture: ComponentFixture<RmLevelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmLevelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmLevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
