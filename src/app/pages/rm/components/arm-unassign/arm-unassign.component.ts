import { Component, HostBinding, Injector, OnInit, ViewChild } from '@angular/core';
import { global } from '@angular/compiler/src/util';

import { forkJoin, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';

import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { FileService } from 'src/app/core/services/file.service';
import { Scopes, FunctionCode, maxInt32, EXPORT_LIMIT } from 'src/app/core/utils/common-constants';
import { Utils } from 'src/app/core/utils/utils';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import * as moment from 'moment';
import * as _ from 'lodash';
import { AppFunction } from 'src/app/core/interfaces/app-function.interface';
import { ArmService } from '../../apis/arm.api';

@Component({
  selector: 'app-arm-unassign',
  templateUrl: './arm-unassign.component.html',
  styleUrls: ['./arm-unassign.component.scss']
})
export class ArmUnassignComponent extends BaseComponent implements OnInit {
  
  models: Array<any>;
  pageable: Pageable;
  isFisrt = true;
  isLoading = true;
  fields: any;
  messages: any;
  branches: Array<any>;
  blocks: Array<any>;
  date: Date = new Date();
  notificationMessage: any;

  paramSearch = {
    blockCodes: '',
    branchCodes: [],
    businessDate: new Date(this.date.getFullYear(), this.date.getMonth(), 1),
    pageSize: global.userConfig.pageSize,
    pageNumber: 0,
    rsId: '',
    scope: Scopes.VIEW,
  };
  prevParams: any;
  obj: AppFunction;
  EXPORT_LIMIT = EXPORT_LIMIT;
  
  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private armService: ArmService,
    private fileService: FileService
  ) {
    super(injector);
    this.messages = global.messageTable;

    this.translate.get(['fields', 'notificationMessage']).subscribe((result) => {
      this.notificationMessage = _.get(result, 'notificationMessage');
      this.fields = _.get(result, 'fields');
    });
  }

  ngOnInit(): void {
    this.obj = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.GROUP_ARM_UNASSIGN}`);
    this.paramSearch.rsId = this.obj.rsId;

    forkJoin([
      this.categoryService.getBranchesOfUser(this.obj.rsId, Scopes.VIEW).pipe(catchError(() => of(undefined))),
      this.categoryService.getBlocksCategoryByRm().pipe(catchError(() => of(undefined))),
    ]).subscribe(([branches, blocks]) => {
      this.branches = branches;
      this.blocks = _.map(blocks, (x) => ({ ...x, value_to_search: `${x.code} - ${x.name}` }));
      this.blocks = [
        {
          code: '',
          value_to_search: `Tất cả`,
        },
        ...this.blocks,
      ];
      this.paramSearch.branchCodes = _.map(this.branches, (x) => x.code);
      this.search();
    });
  }

  setPage(pageInfo) {
    if (this.isFisrt) {
      return;
    }
    this.paramSearch.pageNumber = _.get(pageInfo, 'offset');
    this.reload(false);
  }

  search() {
    this.reload(true);
  }

  exportFile() {
    if (Utils.isArrayEmpty(this.models)) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    this.isLoading = true;
    const params = { ...this.prevParams };
    _.set(params, 'pageSize', this.EXPORT_LIMIT.size);
    _.set(params, 'pageNumber', 0);
    
    this.armService.createExcelFile(params).subscribe(
      (res) => {
        if (Utils.isStringNotEmpty(res)) {
          this.download(res);
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(_.get(this.notificationMessage, 'export_error'));
        this.isLoading = false;
      }
    );
  }

  download(fileId: string) {
    this.fileService.downloadFile(fileId, 'danh-sach-arm-chua-gan-nhom.xlsx').subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(_.get(this.notificationMessage, 'error'));
      }
    });
  }

  monthChange() {
    if (Utils.isNull(_.get(this.paramSearch, 'businessDate'))) {
      _.set(this.paramSearch, 'businessDate', new Date(this.date.getFullYear(), this.date.getMonth(), 1));
    }
    this.ref.detectChanges();
  }

  reload(isSearch?: boolean) {
    this.isLoading = true;
    let params: any = {};

    if (isSearch) {
      this.paramSearch.pageNumber = 0;
      Object.keys(this.paramSearch).forEach((key) => {
        if (key === 'businessDate') {
          if (Utils.isNull(_.get(this.paramSearch, 'businessDate'))) {
            _.set(this.paramSearch, 'businessDate', new Date(this.date.getFullYear(), this.date.getMonth(), 1));
          }
          params[key] = moment(_.get(this.paramSearch, 'businessDate')).format('DD/MM/YYYY');
        } else {
          params[key] = this.paramSearch[key];
        }
      });
    } else {
      if (!this.prevParams) {
        return;
      }
      if (_.get(this.prevParams, 'pageSize') === maxInt32) {
        _.set(this.prevParams, 'pageSize', _.get(global, 'userConfig.pageSize'));
      }
      params = this.prevParams;
      params.pageNumber = this.paramSearch.pageNumber;
    }

    this.armService.getList(params).pipe(
      finalize(() => {
        this.isLoading = false;
        this.isFisrt = false;
      })
    ).subscribe((result) => {
      if (result) {
        this.prevParams = params;
        this.models = _.get(result, 'content') || [];
        this.pageable = {
          totalElements: _.get(result, 'totalElements'),
          totalPages: _.get(result, 'totalPages'),
          currentPage: _.get(result, 'number'),
          size: _.get(global, 'userConfig.pageSize'),
        };
      }
    });
  }

  getValue(row, key) {
    return _.get(row, key);
  }

}
