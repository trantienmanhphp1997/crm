import { forkJoin } from 'rxjs';
import {
  Component,
  OnInit,
  Injector,
  ChangeDetectorRef,
  ViewEncapsulation,
  ViewChild,
  HostBinding,
} from '@angular/core';
import { BaseTableComponent } from '../../../../shared/base-table.component';
import { BlockApi, RmApi } from '../../apis';
import { FormBuilder, Validators } from '@angular/forms';
import { defaultExportExcel, functionUri, Scopes, SessionKey } from 'src/app/core/utils/common-constants';
import * as _ from 'lodash';
import { Utils } from '../../../../core/utils/utils';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import { FunctionCode } from 'src/app/core/utils/common-constants';
import { cleanDataForm } from 'src/app/core/utils/function';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { AppFunction } from 'src/app/core/interfaces/app-function.interface';
import { SessionService } from 'src/app/core/services/session.service';
import { CommonCategoryService } from 'src/app/core/services/common-category.service';
import { FileService } from 'src/app/core/services/file.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RmCustomerHistoryComponent } from '../rm-customer-history/rm-customer-history.component';
import { RmDivisionHistoryComponent } from '../rm-division-history/rm-division-history.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';

@Component({
  selector: 'app-rm',
  templateUrl: './rm.component.html',
  styleUrls: ['./rm.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class RmComponent extends BaseTableComponent<RmApi> implements OnInit {
  pageable: Pageable;
  constructor(
    injector: Injector,
    api: RmApi,
    private ref: ChangeDetectorRef,
    private fb: FormBuilder,
    private blockApi: BlockApi,
    private categoryService: CategoryService,
    private sessionService: SessionService,
    private commonService: CommonCategoryService,
    private fileService: FileService,
    private modalService: NgbModal
  ) {
    super(injector, api);
    this.isLoading = true;
    this.obj = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.paramSearch.rsId = this.obj?.rsId;
    this.translate.get(['fields', 'messageTable', 'notificationMessage']).subscribe((result) => {
      this.fields = result.fields;
      this.messages = result.messageTable;
      this.notificationMessage = result.notificationMessage;
      // this.customer_ranges.push({ code: '', name: this.fields.all });
      this.customer_ranges.push({ code: 'true', name: this.fields.effective });
      this.customer_ranges.push({ code: 'false', name: this.fields.expire });
    });
    this.prop = this.router?.getCurrentNavigation()?.extras?.state;
  }
  @HostBinding('class') hostClass = 'app-rm';

  @ViewChild('tablerm') public tablerm: DatatableComponent;
  obj: AppFunction;
  branch_codes: Array<any> = [];
  search_form = this.fb.group({
    employeeCode: ['', Validators.maxLength(20)],
    employeeId: ['', Validators.maxLength(20)],
    fullName: ['', Validators.maxLength(120)],
    email: ['', Validators.maxLength(50)],
    mobile: ['', Validators.maxLength(30)],
    crmIsActive: ['true'],
    rmBlock: [''],
  });
  fields: any;
  search_value: string;
  show: boolean;
  facilities: Array<any>;
  grades: Array<any>;
  customer_ranges: Array<any> = [];
  facilityCode: Array<string>;
  isLoading = false;
  prevSearch: any;
  paramSearch = {
    page: 0,
    size: global?.userConfig?.pageSize,
    scope: Scopes.VIEW,
    rsId: '',
  };
  messages: any;
  notificationMessage: any;
  prop: any;
  searchType: boolean;
  maxExportExcel = defaultExportExcel;

  init_navigation() {}

  changeBranch(event) {}

  ngOnInit() {
    this.pageable = {
      totalElements: 0,
      totalPages: 1,
      currentPage: 0,
      size: this.limit,
    };
    if (this.prop) {
      if (this.prop?.form) {
        this.search_form.patchValue(this.prop?.form);
        this.facilityCode = this.prop?.params?.branchCodes || '';
      }
      this.prevSearch = this.prop?.params;
      this.search_value = this.prevSearch?.search || '';
      this.paramSearch.page = this.prevSearch?.page || 0;
      this.paramSearch.size = this.prevSearch?.size || global?.userConfig?.pageSize;
      this.show = this.prop?.show;
    } else {
      this.prevSearch = { ...this.paramSearch, crmIsActive: 'true' };
    }
    this.reload(false);
    forkJoin([
      this.blockApi.selectize(),
      this.categoryService.getBranchesOfUser(this.paramSearch.rsId, this.paramSearch.scope),
    ]).subscribe(([listBlock, branchesOfUser]) => {
      const blcks = _.map(listBlock, (x) => ({ ...x, value_to_search: `${x.code} - ${x.name}` }));
      this.grades = [
        {
          id: '',
          code: '',
          value_to_search: _.get(this.fields, 'all'),
        },
        ...blcks,
      ];
      this.facilities = branchesOfUser || [];
    });
    this.maxExportExcel = +(this.sessionService.getSessionData(SessionKey.LIMIT_EXPORT) || defaultExportExcel);
  }

  show_advance_search() {
    this.show = true;
    this.search_value = undefined;
  }

  show_quick_search() {
    this.show = false;
    this.search_form.patchValue({
      rmCode: '',
      rm_name: '',
      email: '',
      phone: '',
      employee_code_hris: '',
      rangeCode: 'true',
      gradeCode: '',
    });
  }

  paging(event) {
    if (!this.isLoading) {
      this.paramSearch.page = _.get(event, 'page') ? _.get(event, 'page') - 1 : 0;
      this.reload();
    }
  }

  handleChangePageSize(event) {
    this.paramSearch.size = event;
    this.limit = event;
    if (this.show) {
      this.searchAdvance();
    } else {
      this.searchBasic();
    }
  }

  searchBasic() {
    this.searchType = false;
    this.reload(true);
  }

  searchAdvance() {
    this.searchType = true;
    this.reload(true);
  }

  navigate(url: string) {
    this.router.navigateByUrl(url, {
      state: {
        form: this.search_form.getRawValue(),
        params: this.prevSearch,
        show: this.show,
      },
      skipLocationChange: true,
    });
  }

  add() {
    this.navigate(`${functionUri.rm_360_manager}/create`);
  }

  edit(value: any) {
    this.navigate(`/rm360/detail/${this.parse_code(value)}`);
  }

  view(value: any) {
    this.navigate(`${functionUri.rm_360_manager}/detail/${this.parse_code(value)}`);
  }

  historyBlock(value: any) {
    const modal = this.modalService.open(RmDivisionHistoryComponent, { windowClass: 'rm-division-history-modal' });
    modal.componentInstance.hrsCode = this.parse_code(value);
  }

  historyAssignCustomer(value: any) {
    const modal = this.modalService.open(RmCustomerHistoryComponent, { windowClass: 'rm-customer-history-modal' });
    modal.componentInstance.hrsCode = this.parse_code(value);
    modal.componentInstance.rmBlock = this.search_form.value.rmBlock;
  }

  addFile() {
    this.navigate(`${functionUri.rm_360_manager}/create-file`);
  }

  parse_code(value) {
    return _.get(value, 'hrisEmployee.employeeId');
  }

  exportFile() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+this.count === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.count)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.isLoading = true;
    this.api.createFileExcel(this.prevSearch).subscribe(
      (fileId) => {
        if (fileId) {
          this.fileService.downloadFile(fileId, 'danh_sach_rm.xlsx').subscribe(
            (res) => {
              this.isLoading = false;
              if (!res) {
                this.messageService.error(this.notificationMessage.error);
              }
            },
            () => {
              this.isLoading = false;
            }
          );
        }
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.error);
      }
    );
  }

  reload(isPaging?: boolean) {
    let params: any = {};
    if (isPaging) {
      this.paramSearch.page = 0;
      if (this.searchType) {
        params = cleanDataForm(this.search_form);
        if (!_.isEmpty(this.facilityCode)) {
          params.branchCodes = this.facilityCode;
        }
      } else {
        this.search_value = Utils.trimNullToEmpty(this.search_value);
        params.search = this.search_value;
        params.crmIsActive = 'true';
      }
      params = { ...params, ...this.paramSearch };
    } else {
      params = this.prevSearch;
      params.page = this.paramSearch.page;
    }
    this.isLoading = true;
    this.api.post('findAll', params).subscribe(
      (response) => {
        this.prevSearch = params;
        this.models = _.get(response, 'content');
        this.count = _.get(response, 'totalElements');
        this.offset = _.get(response, 'number');
        this.pageable = {
          totalElements: response.totalElements,
          totalPages: response.totalPages,
          currentPage: response.number,
          size: response.size || this.prevSearch.size,
        };
        this.ref.detectChanges();
        this.isLoading = false;
      },
      () => {
        this.messageService.error('Kết nối hệ thống không thành công. Vui lòng thử lại sau');
        this.isLoading = false;
      }
    );
  }

  onActive($event) {
    const type = _.get($event, 'type');
    const id = _.get($event, 'row.hrisEmployee.employeeId');
    if (type === 'dblclick') {
      $event.cellElement.blur();
      if (Utils.isStringNotEmpty(id)) {
        const url = `${functionUri.rm_360_manager}/detail/${id}?rmCode=${_.get(
          $event,
          'row.t24Employee.employeeCode'
        )}`;
        this.navigate(url);
      }
    }
  }

  updateHris() {
    this.navigate(`${functionUri.rm_360_manager}/update-hris`);
  }

  createRmCode() {
    this.navigate(`${functionUri.rm_360_manager}/rmcode-create`);
  }

  updateRmCode() {
    this.navigate(`${functionUri.rm_360_manager}/rmcode-update`);
  }
}
