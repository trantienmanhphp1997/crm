import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmFluctuateActionComponent } from './rm-fluctuate-action.component';

describe('RmFluctuateActionComponent', () => {
  let component: RmFluctuateActionComponent;
  let fixture: ComponentFixture<RmFluctuateActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmFluctuateActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmFluctuateActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
