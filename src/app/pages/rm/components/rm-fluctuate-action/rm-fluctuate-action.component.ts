import { forkJoin, of } from 'rxjs';
import { Component, OnInit, Injector, AfterViewInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CommonCategory, FunctionCode, functionUri, maxInt32, Scopes } from 'src/app/core/utils/common-constants';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { LevelApi, RmBlockApi, RmFluctuateApi, TitleGroupApi } from '../../apis';
import { catchError } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { FileService } from 'src/app/core/services/file.service';
import { ConfirmAssignmentModalComponent } from 'src/app/pages/customer-360/components/confirm-assignment-modal/confirm-assignment-modal.component';

@Component({
  selector: 'app-rm-fluctuate-action',
  templateUrl: './rm-fluctuate-action.component.html',
  styleUrls: ['./rm-fluctuate-action.component.scss'],
  providers: [DatePipe],
})
export class RmFluctuateActionComponent extends BaseComponent implements OnInit, AfterViewInit {
  isCreate: boolean;
  isShowFile: boolean;
  isDetail = false;
  isShowTable = false;
  title: string;
  form = this.fb.group({
    id: [''],
    employeeCode: [{ value: '', disabled: true }],
    employeeHrsCode: [{ value: '', disabled: true }],
    employeeUserName: [{ value: '', disabled: true }],
    employeeName: [{ value: '', disabled: true }],
    employeeEmail: [{ value: '', disabled: true }],
    blockCode: [{ value: '', disabled: true }],
    branchCode: [{ value: '', disabled: true }],
    branchName: [{ value: '', disabled: true }],
    status: [{ value: '', disabled: true }],
    statusWork: ['', CustomValidators.required],
    hrsNote: [{ value: '', disabled: true }],
    attachments: [{ value: '', disabled: true }],
    hrsLevelNote: [{ value: '', disabled: true }],
    note: [{ value: '', disabled: true }],
    reason: [{ value: '', disabled: true }],
    createdDate: [{ value: '', disabled: true }],
    updatedDate: [{ value: '', disabled: true }],
    effectiveDate: [{ value: '', disabled: true }],
  });
  listBlock = [];
  listBlockOld = [];
  listStatusWork = [];
  listStatus = [];
  listBranchCode = [];
  listLevel = [];
  listTitleGroup = [];
  listBranch = [];
  isReason = false;
  type: string;
  itemRm: any;
  rmId: string;
  fileName: string;
  fileUpload: File;
  files: any;
  statusWorkCDV = 'CDV';
  branchOld: string;
  branchNew: string;
  backToList = true;
  isRestore: boolean;

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private rmFluctuateApi: RmFluctuateApi,
    private levelApi: LevelApi,
    private titleGroupApi: TitleGroupApi,
    private rmBlockApi: RmBlockApi,
    private datePipe: DatePipe,
    private fileService: FileService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_FLUCTUATE}`);
    this.type = this.route.snapshot.paramMap.get('type');
    if (this.type !== 'detail' && !this.objFunction[this.type]) {
      this.router.navigateByUrl(functionUri.access_denied);
    }

    this.rmId = this.route.snapshot.paramMap.get('id');
    if (this.type === 'create') {
      this.prop = this.router.getCurrentNavigation()?.extras?.state?.prop;
      this.itemRm = this.router?.getCurrentNavigation()?.extras?.state?.itemRm;
      this.isCreate = true;
      this.isShowFile = true;
      this.form.controls.note.enable();
      this.title = 'rm.title.createFluctuate';
    } else if (this.type === 'update') {
      this.title = 'rm.title.updateFluctuate';
      this.prop = this.router.getCurrentNavigation()?.extras?.state;
      this.isCreate = false;
      this.form.controls.note.enable();
    } else {
      this.title = 'rm.title.detailFluctuate';
      this.prop = this.router.getCurrentNavigation()?.extras?.state;
      this.form.disable();
      this.isDetail = true;
      this.isCreate = false;
    }
  }

  ngOnInit(): void {
    if (this.isCreate) {
      if (this.itemRm) {
        this.form.patchValue(this.itemRm);
      }
      this.getDataCreate();
    } else {
      this.getDataDetail();
    }
  }

  ngAfterViewInit() {
    this.form.controls.status.valueChanges.subscribe((value) => {
      this.isReason = value === '2';
    });
    this.form.controls.statusWork.valueChanges.subscribe((value) => {
      if (value === 'CDVTCD' || value === 'CK') {
        this.isShowTable = true;
      } else {
        this.isShowTable = false;
      }
    });
  }

  getDataCreate() {
    this.isLoading = true;
    forkJoin([
      this.commonService.getCommonCategory(CommonCategory.EMPLOYEE_CHANGE).pipe(catchError(() => of(undefined))),
      this.categoryService.searchBlocksCategory({ page: 0, size: maxInt32 }).pipe(catchError(() => of(undefined))),
      this.translate.get('status').pipe(catchError(() => of(undefined))),
      this.rmBlockApi
        .fetch({
          isActive: true,
          userProfileId: this.itemRm?.employeeId,
          page: 0,
          size: maxInt32,
        })
        .pipe(catchError(() => of(undefined))),
      this.levelApi
        .fetch({ page: 0, size: maxInt32, isSetUp: true, isActive: true })
        .pipe(catchError(() => of(undefined))),
      this.titleGroupApi
        .fetch({ page: 0, size: maxInt32, isSetUp: true, isActive: true })
        .pipe(catchError(() => of(undefined))),
      this.categoryService.searchBranches({ page: 0, size: maxInt32 }).pipe(catchError(() => of(undefined))),
    ]).subscribe(
      ([listStatusWork, listBlock, listStatus, listBlockOld, listLevel, listTitleGroup, listBranch]) => {
        this.listBranch = listBranch?.content || [];
        const itemBranchOld = this.listBranch?.find((i) => i.code === this.itemRm?.branchCode);
        this.branchOld = `${itemBranchOld?.code || ''} - ${itemBranchOld?.name || ''}`;
        this.listStatusWork = listStatusWork?.content?.map((item) => {
          return { code: item.code, name: item.name };
        });
        this.listBlock = listBlock?.content || [];
        this.listBlockOld = this.itemRm?.lstAccountTypes || [];
        this.listStatus.push({ code: '0', name: listStatus?.pendingApprove });
        this.listStatus.push({ code: '1', name: listStatus?.approved });
        this.listStatus.push({ code: '2', name: listStatus?.rejected });
        this.listLevel = listLevel?.content || [];
        this.listTitleGroup = listTitleGroup?.content || [];
        listBlock?.content?.forEach((item) => {
          const itemOld = this.listBlockOld?.find((itemBlock) => {
            return item.code === itemBlock.blockCode;
          });
          if (itemOld) {
            item.checked = true;
            item.isSyntheticKPI = itemOld.isSyntheticKPI;
            item.titleGroupId = itemOld.titleGroupId;
            item.listLevel =
              this.listLevel?.filter((itemLevel) => {
                return itemLevel.titleGroupId === itemOld.titleGroupId;
              }) || [];
            item.levelId = itemOld.levelRMId;
          } else {
            item.titleGroupId = '';
            item.levelId = '';
            item.checked = false;
            item.isSyntheticKPI = false;
          }
          item.listTitle =
            this.listTitleGroup?.filter((itemTitle) => {
              return item.code === itemTitle.blockCode;
            }) || [];
        });
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  getDataDetail() {
    this.isLoading = true;
    const rsIdOfRm = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`)?.rsId;
    forkJoin([
      this.rmFluctuateApi.getByCode(this.rmId),
      this.categoryService.searchBranches({ page: 0, size: maxInt32 }).pipe(catchError(() => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.EMPLOYEE_CHANGE).pipe(catchError(() => of(undefined))),
      this.translate.get('status').pipe(catchError(() => of(undefined))),
      this.levelApi
        .fetch({ page: 0, size: maxInt32, isSetUp: true, isActive: true })
        .pipe(catchError(() => of(undefined))),
      this.titleGroupApi
        .fetch({ page: 0, size: maxInt32, isSetUp: true, isActive: true })
        .pipe(catchError(() => of(undefined))),
      this.rmFluctuateApi.checkRestore(this.rmId).pipe(catchError(() => of(undefined))),
      this.categoryService.getBranchesOfUser(rsIdOfRm, Scopes.VIEW).pipe(catchError(() => of(undefined))),
    ]).subscribe(
      ([itemRm, listBranch, listStatusWork, listStatus, listLevel, listTitleGroup, isRestore, branchOfUser]) => {
        if (branchOfUser?.length <= 0 || branchOfUser?.findIndex((i) => i.code === itemRm.branchCode) === -1) {
          this.messageService.error(this.notificationMessage.E101);
          this.router.navigateByUrl(functionUri.rm_fluctuate);
        }
        this.listBranch = listBranch?.content || [];
        this.isRestore = isRestore;
        this.listStatusWork = listStatusWork?.content?.map((item) => {
          return { code: item.code, name: item.name };
        });
        this.listBlock = itemRm?.blocks || [];
        this.listBlockOld = itemRm?.lstAccountTypes || [];
        this.listStatus.push({ code: '0', name: listStatus?.pendingApprove });
        this.listStatus.push({ code: '1', name: listStatus?.approved });
        this.listStatus.push({ code: '2', name: listStatus?.rejected });
        this.listLevel = listLevel?.content || [];
        this.listTitleGroup = listTitleGroup?.content || [];
        this.listBlock?.forEach((item) => {
          const itemOld = this.listBlockOld?.find((itemBlock) => {
            return item.code === itemBlock.blockCode;
          });
          if (itemOld) {
            item.checked = true;
            item.isSyntheticKPI = itemOld.isSyntheticKPI;
            item.titleGroupId = itemOld.titleGroupId;
            item.listLevel =
              this.listLevel?.filter((itemLevel) => {
                return itemLevel.titleGroupId === itemOld.titleGroupId;
              }) || [];
            item.levelId = itemOld.levelRMId;
          } else {
            item.titleGroupId = '';
            item.levelId = '';
            item.checked = false;
            item.isSyntheticKPI = false;
          }
          item.listTitle =
            this.listTitleGroup?.filter((itemTitle) => {
              return item.code === itemTitle.blockCode;
            }) || [];
        });
        if (itemRm) {
          itemRm.createdDate = this.datePipe.transform(itemRm.createdDate, 'dd/MM/yyyy');
          itemRm.updatedDate = this.datePipe.transform(itemRm.updatedDate, 'dd/MM/yyyy');
          itemRm.effectiveDate = this.datePipe.transform(itemRm.effectiveDate, 'dd/MM/yyyy');
          this.itemRm = itemRm;
          this.branchNew = itemRm.t24companycodeNew;
          this.fileName = itemRm.fileName;
          const itemBranchOld = this.listBranch?.find((i) => i.code === itemRm.branchCode);
          this.branchOld = `${itemBranchOld?.code || ''} - ${itemBranchOld?.name || ''}`;
          this.form.patchValue(this.itemRm);
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  submit() {
    if (this.form.valid) {
      this.isLoading = true;
      const data = cleanDataForm(this.form);
      if (data?.statusWork === 'CDVTCD' || data?.statusWork === 'CK') {
        data.lstAccountTypes = this.listBlock
          ?.filter((item) => {
            return item.checked === true;
          })
          ?.map((itemSelected) => {
            return {
              blockCode: itemSelected.code || '',
              levelRMId: itemSelected.levelId || '',
              titleGroupId: itemSelected.titleGroupId || '',
              isSyntheticKPI: itemSelected.isSyntheticKPI || false,
            };
          });
      }
      if (this.fileUpload) {
        const formData: FormData = new FormData();
        formData.append('file', this.fileUpload);
        this.rmFluctuateApi.checkFile(formData).subscribe(
          () => {
            this.rmFluctuateApi.uploadFile(formData).subscribe(
              (fileId) => {
                data.fileId = fileId;
                this.save(data);
              },
              (e) => {
                this.isLoading = false;
                this.messageService.error(this.notificationMessage.error);
              }
            );
          },
          (e) => {
            this.isLoading = false;
            this.messageService.error(e?.error?.description);
          }
        );
      } else {
        data.fileName = this.fileName ? this.fileName : null;
        this.save(data);
      }
    } else {
      validateAllFormFields(this.form);
    }
  }

  save(data) {
    let api: any;
    data.t24companycodeNew = this.branchNew;
    if (this.type === 'create') {
      api = this.rmFluctuateApi.create(data);
    } else {
      api = this.rmFluctuateApi.update(data);
    }
    api.subscribe(
      () => {
        this.isLoading = false;
        this.messageService.success(this.notificationMessage.success);
        if (this.backToList) {
          this.back();
        } else {
          this.title = 'rm.title.detailFluctuate';
          this.form.disable();
          this.isDetail = true;
          this.isCreate = false;
        }
      },
      (e) => {
        this.isLoading = false;
        if (e?.error) {
          this.messageService.error(e?.error?.description);
        } else {
          this.messageService.error(this.notificationMessage.error);
        }
      }
    );
  }

  handleApproveOrReject(isApprove: boolean) {
    if (isApprove) {
      this.confirmService.confirm().then((isConfirm) => {
        if (isConfirm) {
          this.isLoading = true;
          this.rmFluctuateApi.approve(this.itemRm?.id, {}).subscribe(
            (res) => {
              this.isLoading = false;
              this.messageService.success(this.notificationMessage.success);
              if (this.itemRm?.statusWork === 'CDVTCD' || this.itemRm?.statusWork === 'CK') {
                this.confirmService.confirm(this.notificationMessage.ECRM009).then((isRedirect) => {
                  if (isRedirect) {
                    this.router.navigateByUrl(`${functionUri.admin_user_role}`, {
                      state: { hrsCode: this.itemRm?.employeeHrsCode },
                    });
                  } else {
                    this.back();
                  }
                });
              } else if (this.itemRm?.statusWork === this.statusWorkCDV) {
                if (res?.error) {
                  this.confirmService.warn(res?.error);
                } else {
                  this.confirmService.success(this.notificationMessage.S001);
                }
                this.back();
              } else {
                this.back();
              }
            },
            (e) => {
              this.isLoading = false;
              if (e?.error) {
                this.messageService.error(e?.error?.description);
              } else {
                this.messageService.error(this.notificationMessage.error);
              }
            }
          );
        }
      });
    } else {
      const modalConfirm = this.modalService.open(ConfirmAssignmentModalComponent, {
        windowClass: 'confirm-approved-modal',
      });
      modalConfirm.componentInstance.isApproved = isApprove;
      modalConfirm.componentInstance.maxlength = 1000;
      modalConfirm.result
        .then((res) => {
          if (res) {
            const data = {
              reason: res.note,
            };
            this.isLoading = true;
            this.rmFluctuateApi.reject(this.itemRm?.id, data).subscribe(
              () => {
                this.isLoading = false;
                this.messageService.success(this.notificationMessage.success);
                this.back();
              },
              (e) => {
                this.isLoading = false;
                if (e?.error) {
                  this.messageService.error(e?.error?.description);
                } else {
                  this.messageService.error(this.notificationMessage.error);
                }
              }
            );
          }
        })
        .catch(() => {});
    }
  }

  downloadFile() {
    this.fileService.downloadFile(this.itemRm?.fileId, this.itemRm?.i).subscribe(() => {});
  }

  handleFileInput(files) {
    if (files && files.length > 0) {
      this.fileUpload = files.item(0);
      this.fileName = files.item(0).name;
    }
  }

  onchangeFile(inputFile) {
    if (this.fileName) {
      return;
    }
    inputFile.click();
  }

  clearFile() {
    this.fileName = undefined;
    this.fileUpload = undefined;
    this.files = undefined;
  }

  onChangeTitle(event, index) {
    event.originalEvent.stopPropagation();
    if (event.value) {
      this.listBlock[index].levelId = '';
      this.listBlock[index].listLevel = this.listLevel?.filter((item) => {
        return item.titleGroupId === event?.value && item.blockCode === this.listBlock[index].code;
      });
    } else {
      this.listBlock[index].listLevel = [];
    }
  }

  onBlur(e) {
    this.ref.detectChanges();
  }

  back() {
    this.router.navigateByUrl(`${functionUri.rm_fluctuate}`, { state: this.prop });
  }

  updateT24() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.rmFluctuateApi.updateT24(this.itemRm.id).subscribe(
          (res) => {
            this.isLoading = false;
            if (res?.error) {
              this.confirmService.warn(res?.error);
            } else {
              this.confirmService.success(this.notificationMessage.S001);
            }
          },
          () => {
            this.isLoading = false;
            this.messageService.error(this.notificationMessage.error);
          }
        );
      }
    });
  }

  deleteFluctuate() {
    this.confirmService.confirm().then((res) => {
      if (res) {
        this.isLoading = true;
        this.rmFluctuateApi.delete(this.itemRm?.id).subscribe(
          () => {
            this.isLoading = false;
            this.messageService.success(this.notificationMessage.success);
            this.back();
          },
          (e) => {
            this.isLoading = false;
            if (e?.error) {
              this.messageService.error(e?.error?.description);
            } else {
              this.messageService.error(this.notificationMessage.error);
            }
          }
        );
      }
    });
  }

  edit() {
    this.title = 'rm.title.updateFluctuate';
    this.isDetail = false;
    this.backToList = false;
    this.form.controls.note.enable();
    this.form.controls.statusWork.enable();
  }

  restore() {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.rmFluctuateApi.restore(this.rmId).subscribe(
          () => {
            this.isLoading = false;
            this.isRestore = !this.isRestore;
            this.messageService.success(this.notificationMessage.success);
          },
          () => {
            this.isLoading = false;
            this.messageService.error(this.notificationMessage.error);
          }
        );
      }
    });
  }
}
