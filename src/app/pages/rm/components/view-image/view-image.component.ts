import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { TranslateService } from '@ngx-translate/core';
import { HttpRespondCode } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'app-view-image',
  templateUrl: './view-image.component.html',
  styleUrls: ['./view-image.component.scss'],
})
export class ViewImageComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private modalService: NgbModal,
    private modalActive: NgbActiveModal,
    private messageService: NotifyMessageService,
    private tranService: TranslateService
  ) {}

  form = this.fb.group({
    image: [''],
    hyper_link: [''],
  });
  data: any;
  hyper_link: any;

  ngOnInit() {
    this.form.patchValue(this.data);
    this.form.patchValue(this.hyper_link);
  }

  closeModal() {
    this.modalActive.close(false);
  }
}
