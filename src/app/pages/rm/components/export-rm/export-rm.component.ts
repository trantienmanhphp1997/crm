import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import _ from 'lodash';
import { SessionKey } from 'src/app/core/utils/common-constants';
import * as moment from 'moment';
import { RmApi } from '../../apis';
import { FileService } from 'src/app/core/services/file.service';

@Component({
  selector: 'app-export-rm',
  templateUrl: './export-rm.component.html',
  styleUrls: ['./export-rm.component.scss']
})
export class ExportRmComponent extends BaseComponent implements OnInit {
  listDivision = _.map(this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM), (item) => {
    return {
      titleCode: item.titleCode,
      levelCode: item.levelCode,
      code: item.code,
      displayName: `${item.code} - ${item.name}`,
    };
  });
  selectValue = 'month';
  minDate = new Date(moment().subtract(1, 'year').format('MM/DD/YYYY'));
  maxDate = new Date(moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY'));
  minMonth = new Date(moment().subtract(13, 'month').format('MM/DD/YYYY'));
  form = this.fb.group({
    blockCode: this.listDivision[0].code,
    fromDate: null,
    toDate: moment().subtract(1, 'month').endOf('month').format('DD/MM/YYYY'),
    month: moment().subtract(1, 'month').format('MM/YYYY')
  })
  constructor(injector: Injector, private rmApi: RmApi, private fileService: FileService,
  ) {
    super(injector);
  }

  ngOnInit() {
    console.log(this.form.value);
  }

  export() {
    this.form.markAllAsTouched();
    const formValue = this.form.value;
    if(this.selectValue == 'select' && !formValue.fromDate) {
      return;
    }
    const body: any = {
      blockCode: formValue.blockCode,
      isMonth: this.selectValue == 'month',
    };
    if (this.selectValue == 'month') {
      body.fromDate = moment(formValue.month, 'MM/YYYY').startOf('month').format('YYYY-MM-DD');
      body.toDate = moment(formValue.month, 'MM/YYYY').endOf('month').format('YYYY-MM-DD');
    } else {
      body.fromDate = moment(formValue.fromDate, 'DD/MM/YYYY').format('YYYY-MM-DD');
      body.toDate = moment(formValue.toDate, 'DD/MM/YYYY').format('YYYY-MM-DD');
    }
    this.isLoading = true;
    this.rmApi.exportRmblock(body).subscribe(res => {
      if (res) {
        this.fileService.downloadFile(res, `export-rm-by-division-from${body.fromDate}to${body.toDate}.xlsx`).subscribe(val => {
          this.isLoading = false;
        }, error => {
          this.messageService.error(this.notificationMessage.error);
          this.isLoading = false;
        });
      }
    }, err => {
      this.messageService.error(this.notificationMessage.error);
      this.isLoading = false;
    });
  }
}
