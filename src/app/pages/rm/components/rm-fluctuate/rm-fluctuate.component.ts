import { MenuItem } from 'primeng/api';
import { DatePipe } from '@angular/common';
import { forkJoin, of } from 'rxjs';
import { Component, OnInit, Injector, ViewChild, AfterViewInit } from '@angular/core';
import { RmFluctuateApi } from '../../apis';
import { Utils } from '../../../../core/utils/utils';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { global } from '@angular/compiler/src/util';
import { _getOptionScrollPosition } from '@angular/material/core';
import { CommonCategory, FunctionCode, functionUri, SessionKey } from 'src/app/core/utils/common-constants';
import { Scopes } from 'src/app/core/utils/common-constants';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { catchError } from 'rxjs/operators';
import { cleanData } from 'src/app/core/utils/function';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { RmFluctuateCheckComponent } from '../rm-fluctuate-check/rm-fluctuate-check.component';
import * as _ from 'lodash';
import { FileService } from 'src/app/core/services/file.service';
import { ConfirmAssignmentModalComponent } from 'src/app/pages/customer-360/components/confirm-assignment-modal/confirm-assignment-modal.component';

@Component({
  selector: 'app-rm-fluctuate',
  templateUrl: './rm-fluctuate.component.html',
  styleUrls: ['./rm-fluctuate.component.scss'],
  providers: [DatePipe],
})
export class RmFluctuateComponent extends BaseComponent implements OnInit, AfterViewInit {
  @ViewChild('table') public table: DatatableComponent;
  listData = [];
  listStatus: any[];
  listStatusWork: any[];
  listBranch = [];
  listBlock = [];
  listStatusUpdateT24 = [];
  rowMenu: MenuItem[];
  itemSelected: any;
  prevParams: any;
  pageable: Pageable;
  params = {
    search: '',
    page: 0,
    size: global?.userConfig?.pageSize,
    rsId: '',
    scope: Scopes.VIEW,
    employeeCode: '',
    employeeName: '',
    status: '',
    statusWork: '',
    startDate: null,
    endDate: null,
    statusUpdateBranchT24: '',
    branches: [],
    blockCode: '',
    hrisCode: '',
  };
  endDateMin: Date;
  today = new Date();
  showAdvance = false;
  searchAdvance: string;
  isClickEvent = false;
  statusWorkCDV = 'CDV';

  constructor(
    injector: Injector,
    private rmFluctuateApi: RmFluctuateApi,
    private categoryService: CategoryService,
    private datePipe: DatePipe,
    private fileService: FileService
  ) {
    super(injector);
    this.isLoading = true;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_FLUCTUATE}`);
    this.prop = this.router.getCurrentNavigation()?.extras?.state;
  }

  ngOnInit() {
    this.messages = { ...this.messages, emptyMessage: this.notificationMessage.noDataFluctuate };
    const rsIdOfRm = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`)?.rsId;
    this.params.rsId = rsIdOfRm;
    const lstDivision = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM);

    if (this.prop) {
      this.showAdvance = this.prop?.showAdvance;
      this.listStatus = this.prop?.listStatus;
      this.listStatusWork = this.prop?.listStatusWork;
      this.listBranch = this.prop?.listBranch;
      this.listBlock = _.get(this.prop, 'listBlock', []);
      this.prevParams = this.prop?.prevParams;
      if (this.prevParams) {
        const prevParams = { ...this.prevParams };
        prevParams.startDate = prevParams.startDate ? new Date(prevParams.startDate) : null;
        prevParams.endDate = prevParams.endDate ? new Date(prevParams.endDate) : null;
        this.params = prevParams;
      }
      this.searchAdvance = this.prop?.searchAdvance;
      this.search(true, this.searchAdvance);
    } else {
      this.listBlock = _.map(lstDivision, (item) => {
        return { code: item.code, name: `${item.code} - ${item.name}` };
      });
      this.listBlock.unshift({ code: '', name: this.fields.all });

      forkJoin([
        this.categoryService.getBranchesOfUser(rsIdOfRm, Scopes.VIEW).pipe(catchError((error) => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.EMPLOYEE_CHANGE).pipe(catchError((e) => of(undefined))),
        this.translate.get('status').pipe(catchError((error) => of(undefined))),
      ]).subscribe(([listBranch, listStatusWork, status]) => {
        if (listBranch) {
          this.params.branches = listBranch.map((item) => item.code);
          this.listBranch = listBranch;
        }
        this.listStatusWork = listStatusWork?.content || [];
        this.listStatusWork?.unshift({ code: '', name: this.fields.all });
        this.listStatus = [
          {
            code: '',
            name: this.fields?.all,
          },
          {
            code: '0',
            name: status?.pendingApprove,
          },
          {
            code: '1',
            name: status?.approved,
          },
          {
            code: '2',
            name: status?.rejected,
          },
        ];
        this.search(true);
      });
    }

    this.listStatusUpdateT24.push({ code: '', name: this.fields.all });
    this.listStatusUpdateT24.push({ code: 'SUCCESS', name: this.fields.success });
    this.listStatusUpdateT24.push({ code: 'FAIL', name: this.fields.notSuccess });

    this.translate.get('btnAction').subscribe((btnAction) => {
      this.rowMenu = [];
      if (this.objFunction?.update) {
        this.rowMenu.push({
          label: btnAction?.edit,
          command: (e) => {
            this.update();
          },
        });
      }
      if (this.objFunction?.delete) {
        this.rowMenu.push({
          label: btnAction?.delete,
          command: (e) => {
            this.delete();
          },
        });
      }
    });
  }

  ngAfterViewInit() {}

  search(isSearch?: boolean, type?: string) {
    this.isLoading = true;
    let params: any = {};
    if (isSearch) {
      this.searchAdvance = type;
      this.params.page = 0;
      cleanData(this.params);
      if (this.searchAdvance === 'advance') {
        params = { ...this.params };
        params.startDate = _.trim(this.datePipe.transform(params.startDate, 'yyyy-MM-dd'));
        params.endDate = _.trim(this.datePipe.transform(params.endDate, 'yyyy-MM-dd'));
        delete params.search;
      } else {
        params.page = this.params.page;
        params.size = this.params.size;
        params.rsId = this.params.rsId;
        params.scope = this.params.scope;
        params.search = this.params.search;
      }
    } else {
      if (!this.prevParams) {
        return;
      }
      params = this.prevParams;
      params.page = this.params.page;
    }
    this.rmFluctuateApi.search(params).subscribe(
      (result) => {
        this.prevParams = params;
        result?.content?.forEach((item) => {
          item.statusWorkDisplay = this.listStatusWork?.find((status) => {
            return status.code === item.statusWork;
          })?.name;
          item.statusDisplay = this.listStatus?.find((status) => {
            return status.code === item.status;
          })?.name;
        });
        this.listData = result?.content || [];
        this.pageable = {
          totalPages: result?.totalPages,
          totalElements: result?.totalElements,
          currentPage: result?.number,
          size: global?.userConfig?.pageSize,
        };
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
        this.messageService.error(this.notificationMessage.E001);
      }
    );
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.page = pageInfo.offset;
    this.search(false);
  }

  switchSearch() {
    this.showAdvance = !this.showAdvance;
  }

  exportFile() {
    if (Utils.isArrayEmpty(this.listData)) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    this.isLoading = true;
    const params = this.prevParams;
    this.rmFluctuateApi.createFileExcel(params).subscribe(
      (res) => {
        if (Utils.isStringNotEmpty(res)) {
          this.download(res);
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(_.get(this.notificationMessage, 'export_error'));
        this.isLoading = false;
      }
    );
  }

  download(fileId: string) {
    this.fileService.downloadFile(fileId, 'danh-sach-bien-dong-rm.xlsx').subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }

  create() {
    const modal = this.modalService.open(RmFluctuateCheckComponent, { windowClass: 'rm-fluctuate-check' });
    modal.result
      .then((res) => {
        if (res) {
          this.router.navigateByUrl(`${functionUri.rm_fluctuate}/create/${res?.employeeId}`, {
            state: { itemRm: res, prop: this.getPropData() },
            skipLocationChange: true,
          });
        }
      })
      .catch(() => {});
  }

  update() {
    this.router.navigateByUrl(`${functionUri.rm_fluctuate}/update/${this.itemSelected?.id}`, {
      state: this.getPropData(),
      skipLocationChange: true,
    });
  }

  onActive(event) {
    if (event.type === 'click') {
      this.itemSelected = event.row;
    }
    if (event.type === 'dblclick' && !this.isClickEvent) {
      if (Utils.isStringNotEmpty(event.row?.id)) {
        this.router.navigateByUrl(`${functionUri.rm_fluctuate}/detail/${event.row?.id}`, {
          state: this.getPropData(),
          skipLocationChange: true,
        });
      }
    }
  }

  getPropData() {
    return {
      showAdvance: this.showAdvance,
      listStatus: this.listStatus,
      listStatusWork: this.listStatusWork,
      listBranch: this.listBranch,
      listBlock: this.listBlock,
      prevParams: this.prevParams,
      searchAdvance: this.searchAdvance,
    };
  }

  handleApproveOrReject(itemSelected: any, isApprove: boolean) {
    if (isApprove) {
      this.confirmService.confirm().then((isConfirm) => {
        if (isConfirm) {
          this.isLoading = true;
          this.rmFluctuateApi.approve(_.get(itemSelected, 'id'), {}).subscribe(
            (res) => {
              this.isLoading = false;
              this.messageService.success(this.notificationMessage.success);
              if (_.get(itemSelected, 'statusWork') === 'CDVTCD' || _.get(itemSelected, 'statusWork') === 'CK') {
                this.confirmService.confirm(this.notificationMessage.ECRM009).then((isRedirect) => {
                  if (isRedirect) {
                    this.router.navigateByUrl(`${functionUri.admin_user_role}`, {
                      state: { hrsCode: _.get(itemSelected, 'employeeHrsCode') },
                    });
                  } else {
                    this.search();
                  }
                });
              } else if (_.get(itemSelected, 'statusWork') === this.statusWorkCDV) {
                if (res?.error) {
                  this.confirmService.warn(res?.error);
                } else {
                  this.confirmService.success(this.notificationMessage.S001);
                }
                this.search();
              } else {
                this.search();
              }
            },
            (e) => {
              this.isLoading = false;
              if (e?.error) {
                this.messageService.error(e?.error?.description);
              } else {
                this.messageService.error(this.notificationMessage.error);
              }
            }
          );
        }
      });
    } else {
      const modalConfirm = this.modalService.open(ConfirmAssignmentModalComponent, {
        windowClass: 'confirm-approved-modal',
      });
      modalConfirm.componentInstance.isApproved = isApprove;
      modalConfirm.componentInstance.maxlength = 1000;
      modalConfirm.result
        .then((res) => {
          if (res) {
            const data = {
              reason: res.note,
            };
            this.isLoading = true;
            this.rmFluctuateApi.reject(_.get(itemSelected, 'id'), data).subscribe(
              () => {
                this.isLoading = false;
                this.search();
                this.messageService.success(this.notificationMessage.success);
              },
              (e) => {
                this.isLoading = false;
                if (e?.error) {
                  this.messageService.error(e?.error?.description);
                } else {
                  this.messageService.error(this.notificationMessage.error);
                }
              }
            );
          }
        })
        .catch(() => {});
    }
  }

  delete() {
    this.confirmService.confirm().then((res) => {
      if (res) {
        this.isLoading = true;
        this.rmFluctuateApi.delete(this.itemSelected?.id).subscribe(
          () => {
            this.isLoading = false;
            this.search();
            this.messageService.success(this.notificationMessage.success);
          },
          (e) => {
            this.isLoading = false;
            if (e?.error) {
              this.messageService.error(e?.error?.description);
            } else {
              this.messageService.error(this.notificationMessage.error);
            }
          }
        );
      }
    });
  }

  restore(item) {
    this.confirmService.confirm().then((isConfirm) => {
      if (isConfirm) {
        this.isLoading = true;
        this.rmFluctuateApi.restore(item?.id).subscribe(
          () => {
            this.search();
            this.isLoading = false;
            this.messageService.success(this.notificationMessage.success);
          },
          () => {
            this.isLoading = false;
            this.messageService.error(this.notificationMessage.error);
          }
        );
      }
    });
  }

  downloadFile(item) {
    if (this.isClickEvent || !item?.fileId) {
      return;
    }
    this.isClickEvent = true;
    this.fileService.downloadFile(item.fileId, item.fileName).subscribe((res) => {
      this.isClickEvent = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }

  onShowMenu(e) {
    this.isClickEvent = true;
  }

  onHideMenu(e) {
    this.isClickEvent = false;
  }

  getStatusUpdateBranchT24(value) {
    if (value === 'FAIL') {
      return this.fields.notSuccess;
    } else if (value === 'SUCCESS') {
      return this.fields.success;
    } else {
      return '';
    }
  }

  selectedDate(value) {
    this.endDateMin = new Date(value);
  }

  changeStatusWork(event) {
    if (event?.value !== this.statusWorkCDV) {
      // this.params.statusUpdateBranchT24 = '';
    }
  }

  trimParam(event): void {
    event.target.value = event.target.value.trim();
  }
}
