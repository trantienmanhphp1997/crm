import { DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { RmModalComponent } from '../rm-modal/rm-modal.component';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { global } from '@angular/compiler/src/util';
import { FunctionCode, functionUri, maxInt32, SessionKey } from 'src/app/core/utils/common-constants';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { Utils } from 'src/app/core/utils/utils';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';
import { RmGroupArmApi } from '../../apis/rm-group-arm.api';
import { FileService } from 'src/app/core/services/file.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { CustomValidators } from 'src/app/core/utils/custom-validations';

@Component({
  selector: 'app-arm-management-action',
  templateUrl: './arm-management-action.component.html',
  styleUrls: ['./arm-management-action.component.scss'],
})
export class ArmManagementActionComponent extends BaseComponent implements OnInit {
  listRm = [];
  listTemp = [];
  listBlock = [];
  leader: any = {};
  groupCode = '';
  groupName = '';
  params = {
    page: 0,
    size: global.userConfig.pageSize,
    search: '',
    groupId: '',
    check: true,
  };
  pageable: Pageable;
  form = this.fb.group({
    id: '',
    code: [{ value: '', disabled: true }, [CustomValidators.required]],
    name: [{ value: '', disabled: true }, [CustomValidators.required]],
    divisionCode: ['', [CustomValidators.required]],
    branchCode: '',
    listGroupRms: [],
  });
  isSearch = true;
  textSearch = '';
  @ViewChild('table') table: DatatableComponent;
  past: boolean;
  title: string;
  isCreate: boolean;
  isUpdate: boolean;
  isView: boolean;
  listRmOld = [];

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private rmGroupArmApi: RmGroupArmApi,
    private fileService: FileService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_GROUP}`);
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
    this.isCreate = _.get(this.route.snapshot.data, 'isCreate');
    this.isUpdate = _.get(this.route.snapshot.data, 'isUpdate');
    this.isView = _.get(this.route.snapshot.data, 'isView');
    if (this.isCreate) {
      this.title = 'Thêm mới nhóm aRM';
    } else {
      this.form.controls.divisionCode.disable();
    }
    if (this.isUpdate) {
      this.title = 'Cập nhật nhóm aRM';
    }
    if (this.isView) {
      this.title = 'Xem thông tin nhóm aRM';
    }
  }

  ngOnInit(): void {
    this.past = true;
    this.listBlock = _.map(this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM), (item) => {
      return {
        code: item.code,
        displayName: item.code + ' - ' + item.name,
        name: item.name,
      };
    });

    if (!this.isCreate) {
      this.params.groupId = _.get(this.route.snapshot.params, 'id');
      if (!this.params.groupId) {
        return;
      }
      this.isLoading = true;
      const params = { ...this.params, size: maxInt32 };
      this.rmGroupArmApi.searchGroupById(this.params.groupId, params).subscribe(
        (result) => {
          if (result) {
            this.form.patchValue(result);
            this.listTemp = _.orderBy(_.get(result, 'rmList.content', []), 'isARm', 'desc');
            this.listRmOld = [...this.listTemp];
            this.leader = _.find(this.listTemp, (item) => item.isARm);
            this.pageable = {
              totalElements: _.get(result, 'rmList.totalElements') || 0,
              totalPages: _.get(result, 'rmList.totalPages') || 0,
              currentPage: _.get(result, 'rmList.number') || 0,
              size: _.get(this.params, 'size'),
            };
            this.search(true);
          }
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
    } else {
      this.form.controls.divisionCode.setValue(_.get(this.listBlock, '[0].code', ''), { emitEvent: false });
      this.generateGroupCodeAndName();
    }
  }

  confirmDialog() {
    if (this.listTemp.length < 1) {
      this.confirmService.error(this.notificationMessage.RM003);
      return;
    }
    if (!_.find(this.listTemp, (item) => item.isARm === true)) {
      this.confirmService.error(this.notificationMessage.ARM001);
      return;
    }
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.result
      .then((res) => {
        if (res) {
          this.isLoading = true;

          const dataForm = {
            ...this.form.getRawValue(),
            listGroupRms: this.listTemp,
            branchCode: this.leader?.branchCode,
            branchName: this.leader?.branchName,
          };
          let api: any;
          if (this.isCreate) {
            api = this.rmGroupArmApi.create(dataForm);
          } else {
            const listRmNew = [];
            _.differenceBy(this.listTemp, this.listRmOld, 'hrsCode').forEach((item) => {
              item.isAction = true;
              listRmNew.push(item);
            });
            _.differenceBy(this.listRmOld, this.listTemp, 'hrsCode').forEach((item) => {
              item.isAction = false;
              listRmNew.push(item);
            });
            dataForm.listGroupRms = listRmNew;
            dataForm.listGroupRmsOld = this.listRmOld;
            api = this.rmGroupArmApi.update(dataForm);
          }
          api.subscribe(
            (response: any) => {
              if (Utils.isStringNotEmpty(_.get(response, 'errorValue'))) {
                this.confirmService.error(_.get(response, 'errorValue'));
                this.isLoading = false;
                return;
              }
              this.messageService.success(this.notificationMessage.success);
              this.isLoading = false;
              const id = this.isCreate ? _.get(response, 'id') : this.params.groupId;
              this.router.navigateByUrl(`${functionUri.rm_group_arm}/view/${id}`, {
                state: this.prop,
                skipLocationChange: true,
              });
            },
            (e) => {
              if (e?.error) {
                this.confirmService.warn(_.get(e, 'error.description'));
              } else {
                this.messageService.error(this.notificationMessage.error);
              }
              this.isLoading = false;
            }
          );
        }
      })
      .catch(() => {});
  }

  selectMembers(isARm = false) {
    if (Utils.isStringEmpty(this.form.controls.divisionCode.value)) {
      return;
    }
    const formSearch: any = {
      crmIsActive: { value: 'true', disabled: true },
      rmBlock: { value: this.form.controls.divisionCode.value, disabled: true },
    };
    if (isARm) {
      formSearch.isArm = true;
    } else {
      formSearch.isRmGroup = true;
      formSearch.isRmArmGroup = true;
    }
    const modal = this.modalService.open(RmModalComponent, { windowClass: 'list__rm-modal' });
    if (isARm) {
      modal.componentInstance.config = { selectionType: SelectionType.single };
    }
    modal.componentInstance.dataSearch = formSearch;
    modal.componentInstance.listHrsCode = _.filter(this.listTemp, (item) => item.isARm === isARm).map((i) => i.hrsCode);
    modal.result
      .then((res) => {
        if (res) {
          const listSelected = _.map(_.get(res, 'listSelected'), (item) => {
            return {
              hrsCode: _.get(item, 'hrisEmployee.employeeId'),
              rmCode: _.get(item, 't24Employee.employeeCode'),
              rmFullName: _.get(item, 'hrisEmployee.fullName'),
              isARm,
              branchCode: _.get(item, 't24Employee.branchCode'),
            };
          });
          if (isARm && listSelected.length > 0) {
            _.remove(this.listTemp, (item) => item.isARm);
            this.leader = _.get(listSelected, '[0]');
            this.generateGroupCodeAndName();
          }
          this.listTemp = _.uniqBy([...this.listTemp, ...listSelected], 'hrsCode');
          if (_.size(res.listRemove) > 0) {
            _.remove(this.listTemp, (i) => _.indexOf(res.listRemove, i.hrsCode) > -1);
          }
          this.listTemp = _.orderBy(this.listTemp, 'isARm', 'desc');
          this.search(true);
        }
      })
      .catch(() => {});
  }

  setPage(pageInfo) {
    _.set(this.params, 'page', _.get(pageInfo, 'offset'));
    this.search(false);
  }

  search(isSearch: boolean) {
    if (isSearch) {
      this.params.page = 0;
      this.textSearch = this.textSearch.trim();
      this.params.search = this.textSearch;
    }
    _.forEach(this.listTemp, (x) => {
      x.name = Utils.parseToEnglish(`${x.rmCode} ${x.rmFullName}`);
    });
    if (Utils.isStringEmpty(this.params.search)) {
      this.listRm = _.slice(
        this.listTemp,
        _.get(this.params, 'page') * _.get(this.params, 'size'),
        _.get(this.params, 'page') * _.get(this.params, 'size') + _.get(this.params, 'size')
      );
    } else {
      const search = Utils.parseToEnglish(this.params.search);
      const listRms = _.filter(this.listTemp, (item) => {
        return Utils.trimNullToEmpty(item.name).toLowerCase().indexOf(search) !== -1;
      });
      this.listRm = _.slice(
        listRms,
        _.get(this.params, 'page') * _.get(this.params, 'size'),
        _.get(this.params, 'page') * _.get(this.params, 'size') + _.get(this.params, 'size')
      );
    }
    if (this.listRm.length === 0 && this.params.page > 0) {
      this.params.page -= 1;
      this.listRm = _.slice(
        this.listTemp,
        _.get(this.params, 'page') * _.get(this.params, 'size'),
        _.get(this.params, 'page') * _.get(this.params, 'size') + _.get(this.params, 'size')
      );
    }

    this.pageable = {
      totalElements: _.size(this.listTemp),
      totalPages: _.size(this.listTemp) / _.get(this.params, 'size'),
      currentPage: _.get(this.params, 'page'),
      size: _.get(this.params, 'size'),
    };
  }

  selectBlock(value) {
    this.listTemp = [];
    this.leader = null;
    this.search(false);
    this.generateGroupCodeAndName();
  }

  deleteMember(item) {
    if (item.hrsCode === _.get(this.leader, 'hrsCode')) {
      this.leader = { hrsCode: null };
      this.generateGroupCodeAndName();
    }
    this.listTemp = _.filter(this.listTemp, (itemOld) => itemOld.hrsCode !== item.hrsCode);
    this.search(false);
  }

  generateGroupCodeAndName() {
    if (Utils.isStringNotEmpty(Utils.trimNullToEmpty(this.form.controls.divisionCode?.value))) {
      this.groupCode = this.form.controls.divisionCode?.value;
      this.groupName = this.form.controls.divisionCode?.value;
    }
    if (Utils.isStringNotEmpty(Utils.trimNullToEmpty(_.get(this.leader, 'branchCode')))) {
      this.groupCode += '_' + _.get(this.leader, 'branchCode');
      this.groupName += '_' + _.get(this.leader, 'branchCode');
    }
    if (Utils.isStringNotEmpty(Utils.trimNullToEmpty(_.get(this.leader, 'hrsCode')))) {
      this.groupCode += '_' + _.get(this.leader, 'hrsCode');
    }
    if (Utils.isStringNotEmpty(Utils.trimNullToEmpty(_.get(this.leader, 'rmFullName')))) {
      this.groupName += '_' + _.get(this.leader, 'rmFullName');
    }
    this.form.controls.code.setValue(this.groupCode);
    this.form.controls.name.setValue(this.groupName);
  }

  onChangePast(value: boolean) {
    this.params.page = 0;
    this.params.check = value;
    const params = {
      ...this.params,
      size: maxInt32,
    };
    this.isLoading = true;
    this.rmGroupArmApi.searchGroupById(this.params.groupId, params).subscribe(
      (result) => {
        if (result) {
          if (value) {
            this.listTemp = _.orderBy(_.get(result, 'rmList.content', []), 'isARm', 'desc');
          } else {
            this.listTemp = _.get(result, 'rmList.content', []);
          }
          this.leader = _.find(this.listTemp, (item) => item.isARm);
          this.pageable = {
            totalElements: _.get(result, 'rmList.totalElements') || 0,
            totalPages: _.get(result, 'rmList.totalPages') || 0,
            currentPage: _.get(result, 'rmList.number') || 0,
            size: _.get(this.params, 'size'),
          };
          this.search(true);
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  exportFile() {
    if (this.listTemp.length === 0) {
      this.messageService.warn(this.notificationMessage.noRecord);
      return;
    }
    this.isLoading = true;
    const param = { id: this.params.groupId, check: this.params.check };
    this.rmGroupArmApi
      .exportGroupById(param)
      .pipe(catchError(() => of(undefined)))
      .subscribe((fileId) => {
        if (Utils.isStringNotEmpty(fileId)) {
          this.fileService.downloadFile(fileId, 'list_arm.xlsx').subscribe((res) => {
            this.isLoading = false;
            if (!res) {
              this.messageService.error(this.notificationMessage.error);
            }
          });
        } else {
          this.messageService.error(this.notificationMessage?.export_error || '');
          this.isLoading = false;
        }
      });
  }

  isHasARm() {
    return this.listTemp.findIndex((item) => item.isARm) > -1;
  }

  back() {
    this.router.navigateByUrl(`${functionUri.rm_group_arm}`, { state: this.prop });
  }
}
