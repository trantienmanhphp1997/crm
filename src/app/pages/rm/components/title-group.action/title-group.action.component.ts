import { Component, HostBinding, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { TitleGroupApi } from '../../apis';
import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { TranslateService } from '@ngx-translate/core';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { Utils } from '../../../../core/utils/utils';

import _ from 'lodash';
import { RmService } from '../../services';
import { HttpErrorResponse } from '@angular/common/http';
import { RespondCodeError } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'app-title-group.action',
  templateUrl: './title-group.action.component.html',
  styleUrls: ['./title-group.action.component.scss'],
})
export class TitleGroupActionComponent {
  form = this.fb.group({
    id: [''],
    code: ['', CustomValidators.required],
    name: ['', CustomValidators.required],
    blockCode: [''],
    isActive: [true],
    isManagerGroup: [false],
    description: [''],
  });
  fields: any;
  notificationMessage: any;
  rmTitle: any;
  param: any;

  constructor(
    private fb: FormBuilder,
    private api: TitleGroupApi,
    private messageService: NotifyMessageService,
    private tranService: TranslateService,
    private modal: NgbModal,
    route: ActivatedRoute,
    private service: RmService,
    private router: Router,
    private configModal: NgbModalConfig
  ) {
    this.configModal.backdrop = 'static';
    this.configModal.keyboard = false;
    this.isCreate = _.get(route.snapshot.data, 'isCreate');
    this.isUpdate = _.get(route.snapshot.data, 'isUpdate');
    this.isView = _.get(route.snapshot.data, 'isView');
    this.data = _.get(route.snapshot.data, 'data') || {};
    let blcks = _.get(route.snapshot.data, 'blocks') || [];
    this.blocks = _.map(blcks, (x) => ({ ...x, value_to_search: `${x.code} - ${x.name}` }));
    this.tranService.get(['fields', 'notificationMessage', 'rm']).subscribe((result) => {
      this.fields = result.fields;
      this.notificationMessage = result.notificationMessage;
      this.rmTitle = result.rm.title;
      this.status.push({ code: true, name: this.fields.effective });
      this.status.push({ code: false, name: this.fields.expire });
    });

    this.init();
    const navigation = this.router.getCurrentNavigation();
    const state = navigation?.extras?.state;
    this.param = state;
  }
  init() {
    if (this.isCreate) {
      this.title_des = this.rmTitle.rmGroupTitleCreate;
      this.convert_title('title_category_create');
      let block = _.chain(this.blocks).first().value();
      let bcode = _.get(block, 'code');
      this.blockCode = bcode;
      this.form.get('blockCode').setValue(this.blockCode);
      this.form.get('isActive').disable();
    }
    if (this.isUpdate) {
      this.title_des = this.rmTitle.rmGroupTitleCreate;
      this.blockCode = _.get(this.data, 'blockCode');
      this.convert_title('title_category_update');
    }
    if (this.isView) {
      this.title_des = this.rmTitle.rmGroupTitleCreate;
      this.blockCode = _.get(this.data, 'blockCode');
      this.convert_title('title_category_view');
    }

    this.form.patchValue(this.data);
    if (this.isView) {
      this.form.disable();
    }
  }
  title: string;
  status: Array<any> = [];
  data: any;
  facilities: Array<any> = [];
  // message: any;
  isLoading: boolean;
  isCreate: boolean;
  isUpdate: boolean;
  isView: boolean;
  blocks: Array<any>;
  blockCode: string;
  title_des: string;
  @HostBinding('class.app__right-content') appRightContent = true;

  convert_title(value) {
    this.tranService.get(`rm.menu.${value}`).subscribe((res) => {
      this.title = res;
    });
  }

  save() {
    if (this.form.valid) {
      const confirm = this.modal.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result
        .then((res) => {
          if (res) {
            this.isLoading = true;
            const fdata = cleanDataForm(this.form);
            if (this.isCreate) {
              this.create(fdata);
            }
          }
        })
        .catch(() => {
          this.isLoading = false;
        });
    } else {
      validateAllFormFields(this.form);
    }
  }

  create(data) {
    this.api.create(data).subscribe(
      (result) => {
        this.messageService.success(this.notificationMessage?.success || '');
        this.router.navigateByUrl(`/rm360/title-category`);
        this.isLoading = false;
      },
      (e) => {
        if (e?.error) {
          this.messageService.error(e?.error?.description);
        } else {
          this.messageService.error(this.notificationMessage.error);
        }
        this.isLoading = false;
      }
    );
  }

  update() {
    if (this.form.valid) {
      const fdata = cleanDataForm(this.form);
      this.api.post('checkUpdate', fdata).subscribe((response: boolean) => {
        if (response) {
          const confirm = this.modal.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
          confirm.result
            .then((res) => {
              if (res) {
                this.isLoading = true;
                this.api.update(fdata).subscribe(
                  () => {
                    this.messageService.success(this.notificationMessage?.success || '');
                    this.router.navigateByUrl(`/rm360/title-category`);
                    this.isLoading = false;
                  },
                  (e) => {
                    this.messageService.error(e?.error?.description);
                    this.isLoading = false;
                  }
                );
              }
            })
            .catch(() => {
              this.isLoading = false;
            });
        } else {
          const confirm = this.modal.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
          confirm.componentInstance.message = this.notificationMessage.CDRM003;
          confirm.result
            .then((res) => {
              if (res) {
                this.isLoading = true;
                this.api.update(fdata).subscribe(
                  () => {
                    this.messageService.success(this.notificationMessage?.success || '');
                    this.router.navigateByUrl(`/rm360/title-category`);
                    this.isLoading = false;
                  },
                  (e) => {
                    this.messageService.error(e?.error?.description);
                    this.isLoading = false;
                  }
                );
              }
            })
            .catch(() => {
              this.isLoading = false;
            });
        }
      });
    } else {
      validateAllFormFields(this.form);
    }
  }

  back() {
    this.router.navigate([`/rm360/title-category`], { state: this.param });
  }
}
