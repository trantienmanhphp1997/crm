import { Component, HostBinding } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { RmApi } from '../../apis';
import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { TranslateService } from '@ngx-translate/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { Utils } from '../../../../core/utils/utils';
import _ from 'lodash';
import { RmService } from '../../services';
import { FunctionCode } from 'src/app/core/utils/common-constants';
import { AppFunction } from 'src/app/core/interfaces/app-function.interface';
import { SessionService } from 'src/app/core/services/session.service';
import { ConfirmService } from 'src/app/core/services/confirm.service';

@Component({
  selector: 'app-rm.action',
  templateUrl: './rm.action.component.html',
  styleUrls: ['./rm.action.component.scss'],
})
export class RmActionComponent {
  form = this.fb.group({
    username: ['', CustomValidators.required],
    hrisCode: ['', CustomValidators.required],
    t24EmployeeCode: [''],
    rmName: [''],
    email: [''],
    phone: [''],
    phoneNumber: [''],
    gender: [''],
    title: [''],
    dateOfBirth: [''],
    startPosDate: [''],
    position: [''],
    jointDate: [''],
    division: [''],
  });
  facility: any = {};

  constructor(
    private fb: FormBuilder,
    private api: RmApi,
    private messageService: NotifyMessageService,
    private tranService: TranslateService,
    private modal: NgbModal,
    route: ActivatedRoute,
    private service: RmService,
    private router: Router,
    private sessionService: SessionService,
    private confirmService: ConfirmService
  ) {
    this.tranService.get(['fields', 'notificationMessage', 'validationMessage', 'rm']).subscribe((result) => {
      this.fields = result.fields;
      this.notificationMessage = result.notificationMessage;
      this.validationMessage = result.validationMessage;
    });
    this.obj = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.rsId = _.get(this.obj, 'rsId');
    this.scope = 'CREATE';
    this.init_branches();
    const navigation = this.router.getCurrentNavigation();
    const state = _.get(navigation, 'extras.state');
    this.param = state;
  }

  init_branches() {
    this.service.branches(this.rsId, this.scope).then((data: Array<any>) => {
      this.facilities = data;
    });
  }

  check($event) {
    if (Utils.isStringEmpty($event)) {
      this.facility_mes = _.get(this.fields, 'required');
      this.show_mes = true;
    } else {
      this.facility_mes = '';
      this.show_mes = false;
    }
  }
  @HostBinding('class.app__right-content') appRightContent = true;

  facilities: Array<any> = [];
  message: any;
  isLoading: boolean;
  facilityCode: any;
  facility_mes: string;
  show_mes: boolean;
  obj: AppFunction;
  rsId: string;
  scope: string;
  param: any;
  fields: any;
  notificationMessage: any;
  validationMessage: any;

  save() {
    if (Utils.isStringEmpty(this.facilityCode)) {
      this.show_mes = true;
      validateAllFormFields(this.form);
      return;
    } else {
      this.show_mes = false;
    }
    if (this.form.valid) {
      const confirm = this.modal.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
      confirm.result
        .then((res) => {
          if (res) {
            this.isLoading = true;
            const fdata = cleanDataForm(this.form);
            const data = {
              insertedAll: true,
              branchCode: Utils.trim(this.facilityCode),
              hrsCode: Utils.trim(_.get(fdata, 'hrisCode')),
              rmCode: Utils.trim(_.get(fdata, 't24EmployeeCode')),
              username: Utils.trim(_.get(fdata, 'username')),
              rmName: Utils.trim(_.get(fdata, 'rmName')),
              email: Utils.trim(_.get(fdata, 'email')),
              phone: Utils.trim(_.get(fdata, 'phone')),
              gender: Utils.trim(_.get(fdata, 'gender')),
              title: Utils.trim(_.get(fdata, 'title')),
              dateOfBirth: _.get(fdata, 'dateOfBirth'),
              startPosDate: _.get(fdata, 'startPosDate'),
              position: Utils.trim(_.get(fdata, 'position')),
              jointDate: _.get(fdata, 'jointDate'),
              division: Utils.trim(_.get(fdata, 'division')),
              rm360Infos: [
                {
                  branchCode: Utils.trim(this.facilityCode),
                  hrisCode: Utils.trim(_.get(fdata, 'hrisCode')),
                  rmCode: Utils.trim(_.get(fdata, 't24EmployeeCode')),
                  userName: Utils.trim(_.get(fdata, 'username')),
                },
              ],
              user: 'crm_use1',
            };
            let rmCode =  data?.rm360Infos?data?.rm360Infos[0]?.rmCode:"";
            this.api.createNewRm(data).subscribe(
              (result) => {
                this.isLoading = false;
                if (_.head(result)?.errorCode) {
                  this.confirmService.error(this.notificationMessage[_.head(result).errorCode]);
                  return;
                }
                this.messageService.success(_.get(this.notificationMessage, 'success'));
                this.router.navigateByUrl(`/rm360/rm-manager/detail/${_.get(fdata, 'hrisCode')}?rmCode=${rmCode}`);
              },
              (e) => {
                if (e?.error) {
                  this.messageService.error(e?.error?.description);
                } else {
                  this.messageService.error(_.get(this.notificationMessage, 'error'));
                }
                this.isLoading = false;
              }
            );
          }
        })
        .catch(() => {});
    } else {
      validateAllFormFields(this.form);
    }
  }

  back() {
    this.router.navigate([`/rm360/rm-manager`], { state: this.param });
  }
}
