import { maxInt32 } from 'src/app/core/utils/common-constants';
import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { CommonApi } from '../apis';
import _ from 'lodash';

@Injectable()
export class WorkStatusResolver implements Resolve<Object> {
  constructor(private router: Router, private api: CommonApi) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.api.get('commons', { page: 0, size: maxInt32, commonCategoryCode: 'EMPLOYEE_CHANGE' });
  }
}
