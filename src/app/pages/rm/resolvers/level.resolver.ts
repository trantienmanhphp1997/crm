import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { LevelApi } from '../apis';
import _ from 'lodash';

@Injectable()
export class LevelResolver implements Resolve<Object> {
  constructor(private router: Router, private api: LevelApi) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.api.fetch({ size: 10, page: 0, isActive: true });
  }
}


@Injectable()
export class LevelDetailResolver implements Resolve<Object> {
  constructor(private router: Router, private api: LevelApi) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const code = _.get(route.params, 'code');
    return this.api.getByCode(code);
  }
}
