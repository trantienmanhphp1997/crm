import { BranchResolver } from './branch.resolver';
import { BlockResolver } from './block.resolver';
import { FalseResolve, TrueResolve } from './boolean.resolver';
import { TitleGroupDetailResolver, TitleGroupSelectizeResolver } from './title-group.resolver';
import { LevelDetailResolver, LevelResolver } from './level.resolver';
import { RmFluctuateDetailResolver } from './rm-fluctuate.resolver';
import { WorkStatusResolver } from './common.resolver';

export { BranchResolver } from './branch.resolver';
export { BlockResolver } from './block.resolver';
export { FalseResolve, TrueResolve } from './boolean.resolver';
export { TitleGroupDetailResolver, TitleGroupSelectizeResolver } from './title-group.resolver';
export { LevelDetailResolver, LevelResolver } from './level.resolver';
export { RmFluctuateDetailResolver } from './rm-fluctuate.resolver';
export { WorkStatusResolver } from './common.resolver';

export const RESOLVERS = [
  BranchResolver,
  BlockResolver,
  FalseResolve,
  TrueResolve,
  TitleGroupDetailResolver,
  LevelDetailResolver,
  LevelResolver,
  TitleGroupSelectizeResolver,
  RmFluctuateDetailResolver,
  WorkStatusResolver,
];
