import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import _ from 'lodash';
import { RmService } from '../services';

@Injectable()
export class BranchResolver implements Resolve<Object> {
  constructor(private router: Router, private service: RmService) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.service.branches();
    return undefined
  }
}
