import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { TitleGroupApi } from '../apis';
import _ from 'lodash';

@Injectable()
export class TitleGroupDetailResolver implements Resolve<Object> {
  constructor(private router: Router, private api: TitleGroupApi) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const code = _.get(route.params, 'code');
    return this.api.getByCode(code);
  }
}

@Injectable()
export class TitleGroupSelectizeResolver implements Resolve<Object> {
  constructor(private router: Router, private api: TitleGroupApi) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.api.selectize();
  }
}
