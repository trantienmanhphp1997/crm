import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { BlockApi } from '../apis';
import _ from 'lodash';
import { RmService } from '../services';

@Injectable()
export class BlockResolver implements Resolve<Object> {
  constructor(private router: Router, private service: RmService, private api: BlockApi) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.api.selectize();
  }
}
