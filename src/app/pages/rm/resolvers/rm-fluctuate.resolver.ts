import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { RmFluctuateApi } from '../apis';
import _ from 'lodash';

@Injectable()
export class RmFluctuateDetailResolver implements Resolve<Object> {
  constructor(private router: Router, private api: RmFluctuateApi) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const code = _.get(route.params, 'code');
    return this.api.getByCode(code);
  }
}
