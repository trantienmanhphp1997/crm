import { ChangeDetectorRef, Injectable, Injector } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { BlockApi, BranchApi } from '../apis';

@Injectable({
  providedIn: 'root'
})
export class RmService {
  _blocks_loading: boolean;
  _blocks: Object;
  _blocks_emitter = new EventEmitter();
  _branches_loading: boolean;
  _branches: Object;
  _branches_emitter = new EventEmitter();

  private _profile: Object;
  public _profile_changed = new EventEmitter();
  scope: string;

  constructor(private injector: Injector) {}

  set_profile(value: Object) {
    this._profile = value;
    this._profile_changed.next();
  }

  get_profile() {
    return this._profile;
  }

  blocks() {
    const api = this.injector.get(BlockApi);
    return new Promise((resolve, reject) => {
      api
        .selectize()
        .toPromise()
        .then(
          (response) => {
            this._blocks = response;
            this._blocks_emitter.emit();
            resolve(this._blocks);
          },
          () => {
            this._blocks = undefined;
            this._blocks_emitter.error({});
            reject();
          }
        )
        .finally(() => {
          this._blocks_loading = false;
        });
    });
  }

  branches(rsId: string = '', scope: string = '') {
    // if (this.scope === scope) {
    //   if (this._branches !== null && this._branches !== undefined) {
    //     return Promise.resolve(this._branches);
    //   }

    // } else {
    // if (this._branches_loading) {
    //   return new Promise((resolve, reject) => {
    //     this._branches_emitter.subscribe(() => {
    //       resolve(this._branches)
    //     }, () => {
    //       reject();
    //     })
    //   })
    // }
    // this._branches_loading = true;
    const api = this.injector.get(BranchApi);

    return new Promise((resolve, reject) => {
      api
        .selectize(rsId, scope)
        .toPromise()
        .then(
          (response) => {
            this._branches = response;
            this._branches_emitter.emit();
            resolve(this._branches);
          },
          () => {
            this._branches = [];
            this._branches_emitter.emit([]);
            reject();
          }
        )
        .catch(() => {
          this._branches = [];
          this._branches_emitter.emit([]);
        })
        .finally(() => {
          // this._branches_loading = false;
          this.scope = scope;
        });
    });
    // }
  }
}
