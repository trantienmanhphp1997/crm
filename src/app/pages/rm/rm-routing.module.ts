import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGuardService } from 'src/app/core/services/role-guard.service';
import { FunctionCode, Scopes, ScreenType } from 'src/app/core/utils/common-constants';
import { DeviceDetailComponent } from '../device/components';
import { DeviceListComponent } from '../device/components/device-list/device-list.component';
import { ArmUnassignComponent } from './components/arm-unassign/arm-unassign.component';

import {
  RmMenuComponent,
  RmComponent,
  RmActionComponent,
  RmDetailComponent,
  RmDivisionComponent,
  RmGroupComponent,
  RmGroupCreateComponent,
  RmGroupUpdateComponent,
  TitleGroupComponent,
  TitleGroupActionComponent,
  LevelComponent,
  LevelActionComponent,
  RmFluctuateComponent,
  RmFluctuateActionComponent,
  RmFluctuateCheckComponent,
  RmNotAssignComponent,
  RmCreateFileComponent,
  RmUpdateHrisComponent,
  ArmManagementComponent,
  ArmManagementActionComponent,
  // RmCodeCreateComponent,
} from './components';

import {
  BlockResolver,
  TitleGroupDetailResolver,
  FalseResolve,
  TrueResolve,
  LevelDetailResolver,
  TitleGroupSelectizeResolver,
} from './resolvers';
import {HuddleGroupComponent} from "./components/huddle-group/huddle-group.component";
import {HuddleGroupCreateComponent} from "./components/huddle-group-create/huddle-group-create.component";
import {HuddleGroupUpdateComponent} from "./components/huddle-group-update/huddle-group-update.component";
import { ExportRmComponent } from './components/export-rm/export-rm.component';

const routes: Routes = [
  {
    path: '',
    component: RmMenuComponent,
    outlet: 'app-left-content',
  },
  {
    path: '',
    children: [
      {
        path: 'rm-manager',
        canActivateChild: [RoleGuardService],
        data: { code: FunctionCode.RM_MANAGER },
        children: [
          {
            path: '',
            component: RmComponent,
            runGuardsAndResolvers: 'always',
            data: {
              scope: Scopes.VIEW,
            },
          },
          {
            path: 'create',
            component: RmActionComponent,
            data: {
              scope: Scopes.CREATE,
            },
            runGuardsAndResolvers: 'always',
          },
          {
            path: 'detail/:code',
            component: RmDetailComponent,
            data: {
              scope: Scopes.VIEW,
            },
            runGuardsAndResolvers: 'always',
          },
          {
            path: 'create-file',
            component: RmCreateFileComponent,
            data: {
              scope: Scopes.CREATE,
            },
            runGuardsAndResolvers: 'always',
          },
          {
            path: 'update-hris',
            component: RmUpdateHrisComponent,
            runGuardsAndResolvers: 'always',
          },
          // {
          //   path: 'rmcode-create',
          //   component: RmCodeCreateComponent,
          //   data: {
          //     scope: Scopes.ONLY_OPN,
          //     screenType: ScreenType.Create,
          //   },
          //   runGuardsAndResolvers: 'always',
          // },
          // {
          //   path: 'rmcode-update',
          //   component: RmCodeCreateComponent,
          //   data: {
          //     scope: Scopes.ONLY_OPN,
          //     screenType: ScreenType.Update,
          //   },
          //   runGuardsAndResolvers: 'always',
          // },
        ],
      },
      {
        path: 'rm-division',
        canActivateChild: [RoleGuardService],
        data: { code: FunctionCode.RM_BLOCK },
        children: [
          {
            path: '',
            component: RmDivisionComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
        ],
      },
      {
        path: 'rm-fluctuate',
        canActivateChild: [RoleGuardService],
        data: { code: FunctionCode.RM_FLUCTUATE },
        children: [
          {
            path: '',
            component: RmFluctuateComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
          {
            path: 'create',
            component: RmFluctuateCheckComponent,
            data: {
              scope: Scopes.CREATE,
            },
          },
          {
            path: ':type/:id',
            component: RmFluctuateActionComponent,
          },
        ],
      },
      {
        path: 'rm-group',
        canActivateChild: [RoleGuardService],
        data: { code: FunctionCode.RM_GROUP },
        children: [
          {
            path: '',
            component: RmGroupComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
          {
            path: 'create',
            component: RmGroupCreateComponent,
            data: {
              scope: Scopes.CREATE,
            },
          },
          {
            path: 'update/:id',
            component: RmGroupUpdateComponent,
            resolve: {
              isUpdate: TrueResolve,
            },
            data: {
              scope: Scopes.UPDATE,
            },
          },
          {
            path: 'detail/:id',
            component: RmGroupUpdateComponent,
            resolve: {
              isDetail: TrueResolve,
            },
            data: {
              scope: Scopes.VIEW,
            },
          },
        ],
      },
      {
        path: 'rm-unassign',
        component: RmNotAssignComponent,
        canActivate: [RoleGuardService],
        data: {
          code: FunctionCode.GROUP_RM_UNASSIGN,
        },
      },
      {
        path: 'arm',
        canActivateChild: [RoleGuardService],
        data: {
          code: FunctionCode.GROUP_ARM,
        },
        children: [
          {
            path: '',
            component: ArmManagementComponent,
          },
          {
            path: 'create',
            component: ArmManagementActionComponent,
            resolve: {
              isCreate: TrueResolve,
            },
            data: {
              scope: Scopes.CREATE,
            },
          },
          {
            path: 'update/:id',
            component: ArmManagementActionComponent,
            resolve: {
              isUpdate: TrueResolve,
            },
            data: {
              scope: Scopes.UPDATE,
            },
          },
          {
            path: 'view/:id',
            component: ArmManagementActionComponent,
            resolve: {
              isView: TrueResolve,
            },
            data: {
              scope: Scopes.VIEW,
            },
          },
        ],
      },
      {
        path: 'title-category',
        canActivateChild: [RoleGuardService],
        data: { code: FunctionCode.RM_TITLE },
        children: [
          {
            path: '',
            component: TitleGroupComponent,
            data: {
              scope: Scopes.VIEW,
            },
          },
          {
            path: 'create',
            component: TitleGroupActionComponent,
            resolve: {
              blocks: BlockResolver,
              isCreate: TrueResolve,
              isUpdate: FalseResolve,
              isView: FalseResolve,
            },
            data: {
              scope: Scopes.CREATE,
            },
            runGuardsAndResolvers: 'always',
          },
          {
            path: 'view/:code',
            component: TitleGroupActionComponent,
            resolve: {
              blocks: BlockResolver,
              data: TitleGroupDetailResolver,
              isCreate: FalseResolve,
              isUpdate: FalseResolve,
              isView: TrueResolve,
            },
            data: {
              scope: Scopes.CREATE,
            },
            runGuardsAndResolvers: 'always',
          },
          {
            path: 'update/:code',
            component: TitleGroupActionComponent,
            resolve: {
              blocks: BlockResolver,
              data: TitleGroupDetailResolver,
              isCreate: FalseResolve,
              isUpdate: TrueResolve,
              isView: FalseResolve,
            },
            data: {
              scope: Scopes.VIEW,
            },
            runGuardsAndResolvers: 'always',
          },
        ],
      },
      {
        path: 'level-category',
        canActivateChild: [RoleGuardService],
        data: { code: FunctionCode.RM_LEVEL },
        children: [
          {
            path: '',
            component: LevelComponent,
            resolve: { titleGroups: TitleGroupSelectizeResolver },
            data: {
              scope: Scopes.VIEW,
            },
          },
          {
            path: 'create',
            component: LevelActionComponent,
            resolve: {
              titleGroups: TitleGroupSelectizeResolver,
              blocks: BlockResolver,
              isCreate: TrueResolve,
              isUpdate: FalseResolve,
              isView: FalseResolve,
            },
            data: {
              scope: Scopes.CREATE,
            },
            runGuardsAndResolvers: 'always',
          },
          {
            path: 'view/:code',
            component: LevelActionComponent,
            resolve: {
              titleGroups: TitleGroupSelectizeResolver,
              blocks: BlockResolver,
              data: LevelDetailResolver,
              isCreate: FalseResolve,
              isUpdate: FalseResolve,
              isView: TrueResolve,
            },
            data: {
              scope: Scopes.CREATE,
            },
            runGuardsAndResolvers: 'always',
          },
          {
            path: 'update/:code',
            component: LevelActionComponent,
            resolve: {
              blocks: BlockResolver,
              titleGroups: TitleGroupSelectizeResolver,
              data: LevelDetailResolver,
              isCreate: FalseResolve,
              isUpdate: TrueResolve,
              isView: FalseResolve,
            },
            data: {
              scope: Scopes.VIEW,
            },
            runGuardsAndResolvers: 'always',
          },
        ],
      },
      {
        path: 'arm-unassign',
        component: ArmUnassignComponent,
        canActivate: [RoleGuardService],
        data: {
          code: FunctionCode.GROUP_ARM_UNASSIGN,
        },
      },
    ],
  },
  {
    path: 'device',
    data: {
      onlyRM: true,
    },
    children: [
      {
        path: '',
        component: DeviceListComponent,
      },
      {
        path: 'update',
        component: DeviceDetailComponent,
        data: {
          type: ScreenType.Update,
        },
      },
      {
        path: 'detail',
        component: DeviceDetailComponent,
        data: {
          type: ScreenType.Detail,
        },
      },
    ],
  },
  {
    path: 'huddle-group',
    canActivateChild: [RoleGuardService],
    data: { code: FunctionCode.HUDDLE_GROUP },
    children: [
      {
        path: '',
        component: HuddleGroupComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
      {
        path: 'create',
        component: HuddleGroupCreateComponent,
        data: {
          scope: Scopes.CREATE,
        },
      },
      {
        path: 'update/:id',
        component: HuddleGroupUpdateComponent,
        resolve: {
          isUpdate: TrueResolve,
        },
        data: {
          scope: Scopes.UPDATE,
        },
      },
      {
        path: 'detail/:id',
        component: HuddleGroupUpdateComponent,
        resolve: {
          isDetail: TrueResolve,
        },
        data: {
          scope: Scopes.VIEW,
        },
      },
    ],
  },
  {
    path: 'export',
    component: ExportRmComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RmRoutingModule {}
