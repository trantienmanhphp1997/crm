import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RmRoutingModule } from './rm-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { TranslateModule } from '@ngx-translate/core';
import { APIS } from './apis';
import { RESOLVERS } from './resolvers';
import { SERVICES } from './services';
import { RadioButtonModule } from 'primeng/radiobutton';

import {
  LevelActionComponent,
  LevelComponent,
  RmActionComponent,
  RmComponent,
  RmCreateFileComponent,
  RmCustomerHistoryComponent,
  RmDetailComponent,
  RmDivisionComponent,
  RmDivisionHistoryComponent,
  RmFluctuateActionComponent,
  RmFluctuateCheckComponent,
  RmFluctuateComponent,
  RmGroupComponent,
  RmGroupCreateComponent,
  RmGroupUpdateComponent,
  RmLevelComponent,
  RmMenuComponent,
  RmModalComponent,
  RmNotAssignComponent,
  TitleGroupActionComponent,
  TitleGroupComponent,
  ViewImageComponent,
  RmUpdateHrisComponent,
  ArmManagementComponent,
  ArmManagementActionComponent,
  // RmCodeCreateComponent,
  RmLeadsComponent,
} from './components';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxSelectModule } from 'ngx-select-ex';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { MenuModule } from 'primeng/menu';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { CheckboxModule } from 'primeng/checkbox';
import { MatTabsModule } from '@angular/material/tabs';
import { InputTextModule } from 'primeng/inputtext';
import { RmCustomersComponent } from '../rm/components/rm-customers/rm-customers.component';
import { InputNumberModule } from 'primeng/inputnumber';
import { ArmUnassignComponent } from './components/arm-unassign/arm-unassign.component';
import {HuddleGroupComponent} from "./components/huddle-group/huddle-group.component";
import {HuddleGroupCreateComponent} from "./components/huddle-group-create/huddle-group-create.component";
import {HuddleGroupUpdateComponent} from "./components/huddle-group-update/huddle-group-update.component";
import { ExportRmComponent } from './components/export-rm/export-rm.component';


const COMPONENTS = [
  RmMenuComponent,
  RmComponent,
  RmActionComponent,
  RmDetailComponent,
  ViewImageComponent,
  TitleGroupComponent,
  RmDivisionComponent,
  RmGroupComponent,
  RmGroupCreateComponent,
  RmGroupUpdateComponent,
  TitleGroupActionComponent,
  LevelComponent,
  LevelActionComponent,
  RmModalComponent,
  RmLevelComponent,
  RmDivisionHistoryComponent,
  RmCustomerHistoryComponent,
  RmFluctuateComponent,
  RmFluctuateActionComponent,
  RmFluctuateCheckComponent,
  RmCustomersComponent,
  RmNotAssignComponent,
  RmCreateFileComponent,
  RmUpdateHrisComponent,
  ArmManagementComponent,
  ArmManagementActionComponent,
  // RmCodeCreateComponent,
  RmLeadsComponent,
  ArmUnassignComponent,
  HuddleGroupComponent,
  HuddleGroupCreateComponent,
  HuddleGroupUpdateComponent,
  ExportRmComponent
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [
    CommonModule,
    SharedModule,
    NgbModule,
    DragDropModule,
    ReactiveFormsModule,
    FormsModule,
    InfiniteScrollModule,
    TranslateModule,
    RmRoutingModule,
    NgxDatatableModule,
    NgxSelectModule,
    DropdownModule,
    CalendarModule,
    MenuModule,
    ButtonModule,
    TableModule,
    CheckboxModule,
    MatTabsModule,
    InputTextModule,
    RadioButtonModule,
    InputNumberModule,
  ],
  providers: [...APIS, ...RESOLVERS, ...SERVICES],
  entryComponents: [...COMPONENTS],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  exports: [...COMPONENTS],
})
export class RmModule {}
