import {Component, HostBinding, Injector, OnInit} from '@angular/core';
import { SessionService } from '../../../core/services/session.service';
import { SessionKey } from '../../../core/utils/common-constants';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';
import {environment} from '../../../../environments/environment';
import {BaseComponent} from "../../../core/components/base.component";

@Component({
  selector: 'app-embed',
  templateUrl: './embed.component.html',
  styleUrls: ['./embed.component.scss'],
})
export class EmbedComponent extends BaseComponent implements OnInit {
  source: SafeUrl;
  @HostBinding('class.app__right-content') appRightContent = true;

  constructor(
    injector: Injector,
    private sanitizer: DomSanitizer
  ) {
    super(injector)
  }

  ngOnInit() {
    this.source = this.sanitizer.bypassSecurityTrustResourceUrl(environment.url_endpoint_embedded
      + `/dashboard?username=${this.currUser.username}&token=${this.sessionService.getSessionData(SessionKey.TOKEN)}`);
  }

}
