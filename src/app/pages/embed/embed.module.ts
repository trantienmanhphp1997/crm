import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { DynamicModule } from 'ng-dynamic-component';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxEchartsModule } from 'ngx-echarts';
import * as echarts from 'echarts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {EmbedComponent} from './components/embed.component';
import {EmbedRoutingModule} from './embed-routing.module';

@NgModule({
  declarations: [
    EmbedComponent
  ],
  imports: [
    EmbedRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts,
    }),
    NgbModule,
    NgxDatatableModule,
    TranslateModule,
    DynamicModule,
    DropdownModule,
    ButtonModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class EmbedModule {}
