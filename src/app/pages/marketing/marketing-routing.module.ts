import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { FunctionCode, Scopes } from 'src/app/core/utils/common-constants';
import { RoleGuardService } from 'src/app/core/services/role-guard.service';
import { CreateTemplateComponent, ListTemplateComponent, UpdateTemplateComponent } from './components';

const routes: Routes = [
  // {
  //   path: '',
  //   outlet: 'app-left-content',
  // },
  {
    path: 'templates',
    data: {
      code: FunctionCode.MARKETING,
    },
    canActivateChild: [RoleGuardService],
    children: [
      {
        path: '',
        component: ListTemplateComponent,
        data: {
          scope: Scopes.VIEW,
        },
      },
      {
        path: 'create',
        component: CreateTemplateComponent,
        data: {
          scope: Scopes.CREATE,
        },
      },
      {
        path: 'update/:id',
        component: UpdateTemplateComponent,
        data: {
          scope: Scopes.UPDATE,
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MarketingRoutingModule {}
