import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { EmailEditorModule } from 'angular-email-editor';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import { FileUploadModule } from 'primeng/fileupload';
import { InputTextModule } from 'primeng/inputtext';
import { SharedModule } from 'src/app/shared/shared.module';
import { CreateTemplateComponent, ListTemplateComponent, UpdateTemplateComponent } from './components';
import { MarketingRoutingModule } from './marketing-routing.module';

@NgModule({
  declarations: [ListTemplateComponent, CreateTemplateComponent, UpdateTemplateComponent],
  imports: [
    CommonModule,
    MarketingRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgxDatatableModule,
    TranslateModule,
    EmailEditorModule,
    CalendarModule,
    DropdownModule,
    InputTextModule,
    FileUploadModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class MarketingModule {}
