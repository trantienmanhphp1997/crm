import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { FunctionCode } from 'src/app/core/utils/common-constants';
import { global } from '@angular/compiler/src/util';

@Component({
  selector: 'app-list-template',
  templateUrl: './list-template.component.html',
  styleUrls: ['./list-template.component.scss'],
})
export class ListTemplateComponent extends BaseComponent implements OnInit {
  commonData = {
    listFormality: [
      { code: 'email', name: 'Email' },
      { code: 'sms', name: 'SMS' },
      { code: 'message', name: 'Chat Message' },
      { code: 'notification', name: 'Notification' },
    ],
    listCategories: [
      { code: '1', name: 'Ngày kỷ niệm' },
      { code: '2', name: 'Chiến dịch' },
      { code: '3', name: 'Hỗ trợ KH' },
    ],
    listObjects: [
      { code: 'public', name: 'Tất cả mọi người được sử dụng' },
      { code: 'private', name: 'Chỉ người tạo được sử dụng' },
    ],
    listStatus: [],
  };
  listData = [];
  pageable: Pageable;
  formSearch = this.fb.group({
    code: '',
    name: '',
    formality: '',
    category: '',
    status: '',
    object: '',
    createdDateFrom: '',
    createdDateTo: '',
    updatedDateFrom: '',
    updatedDateTo: '',
  });
  params = {
    page: 0,
    size: global?.userConfig?.pageSize,
  };
  prevParams: any;

  constructor(injector: Injector) {
    super(injector);
    this.isLoading = true;
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.MARKETING}`);
  }

  ngOnInit(): void {
    this.search(true);
  }

  search(isSearch?: boolean) {
    this.isLoading = false;
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.page = pageInfo.offset;
  }

  onActive(e) {}

  create() {
    this.router.navigateByUrl(`${this.router.url}/create`, {
      state: { dataForm: this.formSearch.getRawValue(), params: this.prevParams },
    });
  }

  update(row) {}

  delete(row) {}

  copy() {}
}
