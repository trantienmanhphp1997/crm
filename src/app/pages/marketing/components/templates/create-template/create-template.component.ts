import { Component, Injector, OnInit, ViewEncapsulation, ViewChild, AfterViewInit } from '@angular/core';
import { EmailEditorComponent, UnlayerOptions } from 'angular-email-editor';
import { BaseComponent } from 'src/app/core/components/base.component';

@Component({
  selector: 'app-create-template',
  templateUrl: './create-template.component.html',
  styleUrls: ['./create-template.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CreateTemplateComponent extends BaseComponent implements OnInit, AfterViewInit {
  commonData = {
    listFormality: [
      { code: 'email', name: 'Email' },
      { code: 'sms', name: 'SMS' },
      { code: 'message', name: 'Chat Message' },
      { code: 'notification', name: 'Notification' },
    ],
    listCategories: [
      { code: '1', name: 'Ngày kỷ niệm' },
      { code: '2', name: 'Chiến dịch' },
      { code: '3', name: 'Hỗ trợ KH' },
    ],
    listObjects: [
      { code: 'public', name: 'Tất cả mọi người được sử dụng' },
      { code: 'private', name: 'Chỉ người tạo được sử dụng' },
    ],
  };
  form = this.fb.group({
    formality: '',
    category: '',
    subject: '',
    name: '',
    object: 'public',
  });
  fileName: string;
  isValidator = false;
  options: UnlayerOptions = {
    // locale: 'vi-VN',
    tools: {
      html: {
        enabled: false,
      },
      menu: {
        enabled: false,
      },
    },
    appearance: {
      panels: {
        tools: {
          dock: 'left',
        },
      },
    },
  };
  sample = {
    body: {
      values: {
        contentWidth: '900px',
      },
    },
  };

  @ViewChild(EmailEditorComponent)
  private emailEditor: EmailEditorComponent;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.fileName = this.fields?.chooseFile;
  }

  ngAfterViewInit() {}

  editorLoaded(event) {
    // this.emailEditor.editor.loadDesign(this.sample);
  }

  save() {
    this.emailEditor.editor.exportHtml((data) => console.log('exportHtml', data));
  }

  saveAndActive() {}

  uploadFile(event, el) {}

  retype() {
    this.form.reset();
  }
}
