import { MatTabsModule } from '@angular/material/tabs';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { kpisRoutingModule } from './kpis-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { TranslateModule } from '@ngx-translate/core';
import { APIS } from './apis';

import {
  KpiLeftViewComponent,
  KpiManagerByDayComponent,
  DetailKpiManagerByDayComponent,
  KpiTargetTableComponent,
  KpiRmListComponent,
  KpiRmDetailComponent,
  KpiCategoryComponent,
  KpiCategoryCreateComponent,
  KpiCategoryUpdateComponent,
  KpiBranchTimeComponent,
  KpiBranchTimeCreateComponent,
  KpiRegionFactorComponent,
  KpiRegionFactorCreateComponent,
  KpiRegionFactorUpdateComponent,
  KpiRmTimeFactorComponent,
  KpiRmTimeFactorCreateComponent,
  KpiRmTimeFactorUpdateComponent,
  KpiCategoryByTitleComponent,
  KpiCategoryByTitleCreateComponent,
  KpiCategoryByTitleUpdateComponent,
  KpiStandardRmComponent,
  KpiStandardCbqlImportComponent,
  KpiStandardCbqlComponent,
  KpiStandardCbqlViewComponent,
  KpiStandardCbqlDetailComponent,
  KpiAssignmentRmSearchComponent,
  KpiAssignmentCbqlSearchComponent,
  KpiAssignmentRmUpdateComponent,
  KpiAssignmentCbqlUpdateComponent,
  KpiAssignmentReceiveComponent,
  StandardKpiCBQLComponent,
  StandardKpiCBQLImportComponent,
  KpiRmDetailSmeComponent,
  KpiManagerDetailSmeComponent,
  TargetSaleConfigListComponent,
  TargetSaleConfigErrorModalComponent,
  TargetSaleConfigEditModalComponent, TargetSaleConfigUpdateComponent, TargetSaleConfigCreateComponent,
} from './components';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxSelectModule } from 'ngx-select-ex';
import { RmModule } from '../rm/rm.module';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { NgxEchartsModule } from 'ngx-echarts';
import * as echarts from 'echarts';
import { GridsterModule } from 'angular-gridster2';
import { DynamicModule } from 'ng-dynamic-component';
import { AccordionModule } from 'primeng/accordion';
import { TabViewModule } from 'primeng/tabview';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
import { CheckboxModule } from 'primeng/checkbox';
import { MultiSelectModule } from 'primeng/multiselect';
import { InputNumberModule } from 'primeng/inputnumber';
import { KpiTargetChartComponent } from './components/kpi-target-chart/kpi-target-chart.component';
import { KpiTotalPointChartComponent } from './components/kpi-total-point-chart/kpi-total-point-chart.component';
import {ProgressBarModule} from 'primeng/progressbar';
import { TargetChartComponent } from './components/kpi-target-chart/chart/chart.component';
import { StandardKpiCBQLBranchV2Component } from './components/kpi-standard-cbql-branch-v2/list/kpi-standard-cbql.component';
import { StandardKpiCBQLBranchV2ImportComponent } from './components/kpi-standard-cbql-branch-v2/import/kpi-standard-cbql-import.component';
import { KpiStandardCbqlBranchV2DetailComponent } from './components/kpi-standard-cbql-branch-v2/detail/kpi-standard-cbql-detail.component';

const COMPONENTS = [
  KpiLeftViewComponent,
  KpiManagerByDayComponent,
  DetailKpiManagerByDayComponent,
  KpiTargetTableComponent,
  KpiRmListComponent,
  KpiRmDetailComponent,
  KpiCategoryComponent,
  KpiCategoryCreateComponent,
  KpiCategoryUpdateComponent,
  KpiRegionFactorComponent,
  KpiRegionFactorCreateComponent,
  KpiRegionFactorUpdateComponent,
  KpiBranchTimeComponent,
  KpiBranchTimeCreateComponent,
  KpiRmTimeFactorComponent,
  KpiRmTimeFactorCreateComponent,
  KpiRmTimeFactorUpdateComponent,
  KpiCategoryByTitleComponent,
  KpiCategoryByTitleCreateComponent,
  KpiCategoryByTitleUpdateComponent,
  KpiStandardRmComponent,
  KpiStandardCbqlComponent,
  KpiStandardCbqlImportComponent,
  KpiStandardCbqlViewComponent,
  KpiStandardCbqlDetailComponent,
  KpiAssignmentRmSearchComponent,
  KpiAssignmentCbqlSearchComponent,
  KpiAssignmentRmUpdateComponent,
  KpiAssignmentCbqlUpdateComponent,
  KpiAssignmentReceiveComponent,
  StandardKpiCBQLComponent,
  StandardKpiCBQLImportComponent,
  KpiRmDetailSmeComponent,
  KpiManagerDetailSmeComponent,
  KpiTargetChartComponent,
  KpiTotalPointChartComponent,
  TargetChartComponent,
  TargetSaleConfigListComponent,
  TargetSaleConfigErrorModalComponent,
  TargetSaleConfigEditModalComponent,
  TargetSaleConfigUpdateComponent,
  TargetSaleConfigCreateComponent,
  StandardKpiCBQLBranchV2Component,
  StandardKpiCBQLBranchV2ImportComponent,
  KpiStandardCbqlBranchV2DetailComponent
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [
    CommonModule,
    SharedModule,
    NgbModule,
    DragDropModule,
    ReactiveFormsModule,
    FormsModule,
    InfiniteScrollModule,
    TranslateModule,
    kpisRoutingModule,
    NgxDatatableModule,
    NgxSelectModule,
    RadioButtonModule,
    CheckboxModule,
    RmModule,
    MatProgressBarModule,
    MatTabsModule,
    NgxEchartsModule.forRoot({
      echarts,
    }),
    GridsterModule,
    DynamicModule,
    AccordionModule,
    TabViewModule,
    ButtonModule,
    InputTextModule,
    DropdownModule,
    CalendarModule,
    TableModule,
    MultiSelectModule,
    InputNumberModule,
    ProgressBarModule
  ],
  providers: [...APIS],
  entryComponents: [...COMPONENTS],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class KpisModule {}
