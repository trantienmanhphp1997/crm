import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';

@Injectable()
export class KpiRmApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_kpi}`);
  }

  searchKpiRmProcess(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi/kpi-rm-indiv`, params);
  }
  getDateRm(param: any): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_kpi}/kpi/recently-business-date?viewRm=true&period=${param}&dayView=false&typeView=false`
    );
  }
  getMonthRm(param: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/month?viewRm=true&year=${param}`);
  }
  getCategoryRmByBlock(params: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/groups/title-groups-config`, { params });
  }
  createFileRM(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi/export-kpi-rm-indiv`, data, { responseType: 'text' });
  }
  exportKPIRmDay(id, rsId, scope, past): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_kpi}/kpi/export-kpi-rm-indiv-detail-day/${id}?rsId=${rsId}&scope=${scope}&past=${past}`,
      { responseType: 'text' }
    );
  }
  exportKPIRmMonth(id, rsId, scope, past): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_kpi}/kpi/export-kpi-rm-indiv-detail-month/${id}?rsId=${rsId}&scope=${scope}&past=${past}`,
      { responseType: 'text' }
    );
  }
  checkDataInMonth(businessData, blockCode) {
    return this.http.get(
      `${environment.url_endpoint_kpi}/kpi/data-info?viewRm=true&businessDate=${businessData}&blockCode=${blockCode}`,
      { responseType: 'text' }
    );
  }
  searchDataSme(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi-corp/rm`, params);
  }

  getDateSme(period, year, blockCode): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi-corp/month?period=true&year=${year}&blockCode=${blockCode}`);
  }

  createFileRMSme(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi-corp/export-rm`, data, { responseType: 'text' });
  }

  createFileDetailRMSme(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi-corp/export-detail-rm`, data, { responseType: 'text' });
  }

  searchDataSmeDaily(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi-corp/manager-daily`, params);
  }

  createFileSmeByDay(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi-corp/export-kpi-daily`, data, { responseType: 'text' });
  }

  createFileDetailSmeByDay(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi-corp/export-kpi-detail-daily`, data, { responseType: 'text' });
  }
}
