import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpWrapper } from 'src/app/core/apis/http-wapper';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class KpiDashboardService extends HttpWrapper {

  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_kpi}`);
  }

  getTimePoint(params: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/dashboard/timePoint`, { params });
  }

  // period: month
  getRankingCategories(params: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/dashboard/summary`, { params });
  }

  getAvgPointEntrust(params: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/dashboard/avg`, { params });
  }

  getKpiTargets(params: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/dashboard/detail`, { params });
  }

  getSummaryFourMonths(params: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/dashboard/summary/4month`, { params });
  }

  // period: day
  getRankingCategoriesDailies(params: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/dashboard/summary-daily`, { params });
  }

  getAvgPointEntrustDailies(params: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/dashboard/avg-daily`, { params });
  }

  getKpiTargetsDailies(params: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/dashboard/detail-daily`, { params });
  }

  getSummaryFourDays(params: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/dashboard/summary/4days`, { params });
  }
  
}
