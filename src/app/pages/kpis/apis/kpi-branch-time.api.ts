import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HttpWrapper} from '../../../core/apis/http-wapper';
import {environment} from '../../../../environments/environment';

@Injectable()
export class KpiBranchTimeApi extends HttpWrapper {
	constructor(http: HttpClient) {
		super(http, `${environment.url_endpoint_kpi}`);
	}

	getKpiBranchTimeByBlockCode(params): Observable<any> {
		return this.http.get(`${environment.url_endpoint_kpi}/kpi/kpi-time-org-factor`, {params});
	}

	saveKpiTimeOrgFactor(data): Observable<any> {
		return this.http.post(`${environment.url_endpoint_kpi}/kpi/kpi-time-org-factor`, data);
	}
}
