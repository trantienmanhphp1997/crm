import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend } from '@angular/common/http';
import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';
import { Observable, of } from 'rxjs';
import { maxInt32 } from 'src/app/core/utils/common-constants';

@Injectable()
export class TitleGroupApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_category}/title-groups`);
  }
  searchGroupByBlockCode(code): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/title-groups`, {
      params: {
        blockCode: code,
        page: '0',
        size: maxInt32.toString(),
        isActive: 'true',
      },
    });
  }

  search(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_rm}/groups/findAll`, params);
  }
}
