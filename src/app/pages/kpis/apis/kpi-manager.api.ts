import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';

@Injectable()
export class KpiManagerApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_kpi}`);
  }

  createFile(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi/export-kpi-manager-indiv`, data, {
      responseType: 'text',
    });
  }

  createDetailFile(id: string, rsId: string, scope: string): Observable<any> {
    return this.http.get(
      `${environment.url_endpoint_kpi}/kpi/export-kpi-manager-indiv-detail/${id}?rsId=${rsId}&scope=${scope}`,
      { responseType: 'text' }
    );
  }

  fetch(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi/kpi-manager-indiv`, params);
  }

  getBusinessDate(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/recently-business-date`, { params });
  }

  getMonthData(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/data-info`, { params, responseType: 'text' });
  }

  searchDataSme(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi-corp/manager`, params);
  }

  getDateSme(period, year, blockCode): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi-corp/month?period=${period}&year=${year}&blockCode=${blockCode}`);
  }

  createFileRMSme(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi-corp/export-manager`, data, { responseType: 'text' });
  }

  createFileDetailRMSme(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi-corp/export-detail-manager`, data, {
      responseType: 'text',
    });
  }

  searchDataSmeDaily(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi-corp/manager-daily`, params);
  }

  createFileSmeByDay(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi-corp/export-kpi-daily`, data, { responseType: 'text' });
  }

  createFileDetailSmeByDay(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi-corp/export-kpi-detail-daily`, data, { responseType: 'text' });
  }
}
