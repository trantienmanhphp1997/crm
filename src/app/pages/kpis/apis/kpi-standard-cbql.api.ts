import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';
import { maxInt32 } from 'src/app/core/utils/common-constants';

@Injectable()
export class KpiStandardCbqlApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_kpi}`);
  }

  importStandard(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi/import/kpi-standard`, data);
  }

  updateKpiStandardCbql(data): Observable<any> {
    return this.http.put(`${environment.url_endpoint_kpi}/kpi/kpi-standard-cbql`, data);
  }

  exportKPIStandardTemplate(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/export-kpi-standard-template-import`, {
      params,
      responseType: 'text',
    });
  }

  searchKpiStandardCbql(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/kpi-standard-cbql`, { params });
  }

  searchCbql(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/infoCBQL`, { params: { ...params, isSyntheticKPI: 1 } });
  }

  getBranchesOfUser(rsId, scope): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/branches/branchesByUser?rsId=${rsId}&scope=${scope}`);
  }

  // Nhập KPI cbql chi nhánh, vùng v2
  importKpiBranch(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi/import-kpi-branch`, data, { responseType: 'text' });
  }

  saveKpiBranchArea(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/save-kpi-branch-area`, { params, responseType: 'text' });
  }

  getListPassBranchArea(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/get-list-pass-branch-area`, { params });
  }

  getListFailBranchArea(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/get-list-fail-branch-area`, { params });
  }

  exportListFailBranchArea(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/export-list-fail-branch-area`, { params, responseType: 'text' });
  }

  searchKpiBranchArea(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi/kpi-standard-branch-area/search`, data);
  }

  detailKpiBranchArea(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi/kpi-standard-branch-area/detail`, data);
  }

  exportDetailKpiBranchArea(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi/kpi-standard-branch-area/export`, data, { responseType: 'text' });
  }

  exportTemplatteKpiBranchArea(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi/export-kpi-standard-branch-template`, data, { responseType: 'text' });
  }
}
