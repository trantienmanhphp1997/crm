import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import { maxInt32 } from 'src/app/core/utils/common-constants';

@Injectable({
  providedIn: 'root'
})
export class LevelApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_category}/levels`);
  }

  searchLevelByGroupCode(titleGroupId, blockCode): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/levels`, {
      params: {
        blockCode,
        titleGroupId,
        page: '0',
        size: maxInt32.toString(),
        isActive: 'true',
      },
    });
  }

  searchLevelByBlockCode(blockCode: string): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/levels`, {
      params: {
        blockCode,
        page: '0',
        size: maxInt32.toString(),
        isActive: 'true',
      },
    });
  }
}
