import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';

@Injectable()
export class KpiAssignmentApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_kpi}`);
  }

  searchRM(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/getActiveRM`, {
      params: { ...params, isSyntheticKPI: 1 },
    });
  }

  searchKpiAssign(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/kpi-assign`, { params });
  }

  searchMyKpiAssign(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/me/kpi-assign`, { params });
  }

  saveKpiAssign(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi/kpi-assign`, data);
  }

  // api này lấy ra ngày tháng gần nhất đã tổng hợp kpi
  getAggregteMonthMax(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/aggregate-month-max`);
  }

  createFile(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_rm}/employees/exportKpiAssignment`, {
      params: { ...params, isSyntheticKPI: 1 },
      responseType: 'text',
    });
  }

  listTitleEdit(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_customer_sme}/account-plan/getAllKpiItemId`);
  }
}
