import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import { maxInt32 } from 'src/app/core/utils/common-constants';

@Injectable()
export class CommonCategoryApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_category}/commons`);
  }

  getCommonCategory(parentCode, code?): Observable<any> {
    return this.http.get(`${environment.url_endpoint_category}/commons`, {
      params: {
        page: '0',
        size: maxInt32.toString(),
        commonCategoryCode: parentCode,
        code: code || '',
      },
    });
  }
}
