import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';

@Injectable()
export class KpiCategoryByTitleApi extends HttpWrapper {
	constructor(http: HttpClient) {
		super(http, `${environment.url_endpoint_kpi}`);
	}

	searchKpiRmGroupLevel(blockCode): Observable<any> {
		return this.http.get(`${environment.url_endpoint_kpi}/kpi/kpi-rm-level`, { params: { blockCode } });
	}

	searchKpiAccordingTitles(params): Observable<any> {
		return this.http.get(`${environment.url_endpoint_kpi}/kpi/kpi-according-titles`, { params });
	}

	deleteOneHistoryProperty(params): Observable<any> {
		return this.http.delete(`${environment.url_endpoint_kpi}/kpi/kpi-item-rm-level`, { params });
	}

	deleteAllHistoryProperty(params): Observable<any> {
		return this.http.delete(`${environment.url_endpoint_kpi}/kpi/kpi-item-rm-level`, { params });
	}

	createKpiItemRmLevel(data): Observable<any> {
		return this.http.post(`${environment.url_endpoint_kpi}/kpi/kpi-item-rm-level`, data);
	}

	updateKpiItemRmLevel(data): Observable<any> {
		return this.http.put(`${environment.url_endpoint_kpi}/kpi/kpi-item-rm-level`, data);
	}
}
