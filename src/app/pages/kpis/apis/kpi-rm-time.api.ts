import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';

@Injectable()
export class KpiRmTimeApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_kpi}`);
  }

  searchKpiTimeFactorSet(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/kpi-time-factor-set`, { params });
  }

  getKpiTimeFactorSetDetail(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/kpi-time-factor-set-detail`, { params });
  }

  getKpiTimeFactors(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/kpi-time-factor`, { params });
  }

  postKpiTimeFactors(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi/kpi-time-factor`, data);
  }

  getKpiRegionFactors(kpiFactorSetId): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/kpi-factor`, { params: { kpiFactorSetId } });
  }

  getDetailFactorSet(id): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/kpi-factor-set-detail`, { params: { id } });
  }
}
