import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';

@Injectable()
export class KpiDetailApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_kpi}/kpi/kpi-manager-indiv-detail`);
  }
}