import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';

@Injectable()
export class KpiMonthApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_kpi}/month`);
  }

  getMonthWithYear(params) {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/month`, { params })
  }

  getByIdMonth(id: any, rsId: any, scope: any,past: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/kpi-rm-indiv-detail-month/${id}?rsId=${rsId}&scope=${scope}&past=${past}`);
  }
  getByIdDay(id: any, rsId: any, scope: any,past: any): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/kpi-rm-indiv-detail-day/${id}?rsId=${rsId}&scope=${scope}&past=${past}`);
  }


}