import {KpiManagerApi} from './kpi-manager.api';
import {CommonCategoryApi} from './common-category.api';
import {TitleGroupApi} from './title-group.api';
import {LevelApi} from './rm-level.api';
import {KpiMonthApi} from './kpi-month.api';
import {RmProfileApi} from './rm-profile.api';
import {KpiDetailApi} from './kpi-detail.api';
import {KpiRmApi} from './kpi-rm.api';
import {KpiCategoryApi} from './kpi-category.api';
import {KpiBranchTimeApi} from './kpi-branch-time.api';
import {KpiRegionApi} from './kpi-region.api';
import {KpiStandardRmApi} from './kpi-standard-rm.api';
import {KpiCategoryByTitleApi} from './kpi-category-by-title.api';
import { KpiRmTimeApi } from './kpi-rm-time.api';
import { KpiStandardCbqlApi } from './kpi-standard-cbql.api';
import { KpiAssignmentApi } from './kpi-assignment.api';

export {KpiManagerApi} from './kpi-manager.api';
export {CommonCategoryApi} from './common-category.api';
export {TitleGroupApi} from './title-group.api';
export {LevelApi} from './rm-level.api';
export {KpiMonthApi} from './kpi-month.api';
export {RmProfileApi} from './rm-profile.api';
export {KpiDetailApi} from './kpi-detail.api';
export {KpiRmApi} from './kpi-rm.api';
export {KpiCategoryApi} from './kpi-category.api';
export {KpiStandardRmApi} from './kpi-standard-rm.api';
export {KpiCategoryByTitleApi} from './kpi-category-by-title.api';
export { KpiRmTimeApi } from './kpi-rm-time.api';
export { KpiStandardCbqlApi } from './kpi-standard-cbql.api';
export { KpiAssignmentApi } from './kpi-assignment.api';

export const APIS = [
	KpiManagerApi,
	CommonCategoryApi,
	TitleGroupApi,
	LevelApi,
	KpiMonthApi,
	RmProfileApi,
	KpiDetailApi,
	KpiRmApi,
	KpiCategoryApi,
	KpiBranchTimeApi,
	KpiRegionApi,
	KpiStandardRmApi,
	KpiCategoryByTitleApi,
	KpiRmTimeApi,
	KpiStandardCbqlApi,
	KpiAssignmentApi
];