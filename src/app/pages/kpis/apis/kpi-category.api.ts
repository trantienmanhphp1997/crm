import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';

@Injectable()
export class KpiCategoryApi extends HttpWrapper {
  constructor(http: HttpClient) {
    super(http, `${environment.url_endpoint_kpi}`);
  }

  search(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/kpi-items`, { params });
  }

  getItemResource(): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/kpi-item-resource`);
  }

  createKpiItem(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi/kpi-item`, data);
  }

  getKpiItemById(id): Observable<any> {
    return this.http.get(`${environment.url_endpoint_kpi}/kpi/kpi-item`, { params: { id } });
  }

  updateKpiItem({ data, id }): Observable<any> {
    return this.http.put(`${environment.url_endpoint_kpi}/kpi/kpi-item`, data, { params: { id } });
  }

  deleteKpiItem(id): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_kpi}/kpi/kpi-item`, { params: { id } });
  }
}
