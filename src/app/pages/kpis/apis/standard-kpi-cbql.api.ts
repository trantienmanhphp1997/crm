import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend } from '@angular/common/http';

import { HttpWrapper } from '../../../core/apis/http-wapper';
import { environment } from '../../../../environments/environment';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StandardKpiCbqlApi {
  constructor(private http: HttpClient) {}

  exportKpiStandardManagerTemplate(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi/export-kpi-standard-manager-template`, params, {
      responseType: 'text',
    });
  }

  importKpiStandardManagerTemplate(params: FormData): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi/import-kpi-standard-manager-template`, params);
  }

  saveKpiStandardManagerTemplate(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi/save-kpi-standard-manager-template`, params);
  }

  searchStandardKpiCBQL(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi/kpi-standard-manager-by-branch`, params);
  }

  saveStandardKpiCBQL(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_kpi}/kpi/save-kpi-standard-manager-by-branch`, params);
  }
}
