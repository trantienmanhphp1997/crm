import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGuardService } from 'src/app/core/services/role-guard.service';

import { FunctionCode, Scopes } from 'src/app/core/utils/common-constants';
import {
  KpiLeftViewComponent,
  KpiManagerByDayComponent,
  DetailKpiManagerByDayComponent,
  KpiRmListComponent,
  KpiCategoryCreateComponent,
  KpiCategoryUpdateComponent,
  KpiBranchTimeComponent,
  KpiRegionFactorComponent,
  KpiBranchTimeCreateComponent,
  KpiRmTimeFactorComponent,
  KpiRmTimeFactorCreateComponent,
  KpiRmTimeFactorUpdateComponent,
  KpiRegionFactorCreateComponent,
  KpiRegionFactorUpdateComponent,
  KpiCategoryByTitleCreateComponent,
  KpiCategoryByTitleComponent,
  KpiStandardRmComponent,
  KpiStandardCbqlComponent,
  KpiStandardCbqlImportComponent,
  KpiStandardCbqlViewComponent,
  KpiStandardCbqlDetailComponent,
  KpiAssignmentRmSearchComponent,
  KpiAssignmentCbqlSearchComponent,
  KpiAssignmentRmUpdateComponent,
  KpiAssignmentCbqlUpdateComponent,
  KpiAssignmentReceiveComponent,
  StandardKpiCBQLComponent,
  KpiCategoryComponent,
  KpiRmDetailComponent,
  StandardKpiCBQLImportComponent,
  KpiRmDetailSmeComponent,
  KpiManagerDetailSmeComponent,
  TargetSaleConfigCreateComponent, TargetSaleConfigListComponent, TargetSaleConfigUpdateComponent,
} from './components';
import { KpiStandardCbqlBranchV2DetailComponent } from './components/kpi-standard-cbql-branch-v2/detail/kpi-standard-cbql-detail.component';
import { StandardKpiCBQLBranchV2ImportComponent } from './components/kpi-standard-cbql-branch-v2/import/kpi-standard-cbql-import.component';
import { StandardKpiCBQLBranchV2Component } from './components/kpi-standard-cbql-branch-v2/list/kpi-standard-cbql.component';
const routes: Routes = [
  {
    path: '',
    component: KpiLeftViewComponent,
    outlet: 'app-left-content',
  },
  {
    path: '',
    children: [
      {
        path: 'kpi-management',
        children: [
          {
            path: 'kpi-assignment',
            children: [
              {
                path: 'kpi-assignment-rm',
                canActivateChild: [RoleGuardService],
                data: {
                  code: FunctionCode.KPI_ASSIGNMENT_RM,
                },
                children: [
                  {
                    path: '',
                    component: KpiAssignmentRmSearchComponent,
                    data: {
                      scope: Scopes.VIEW,
                    },
                  },
                  {
                    path: 'update/:id',
                    component: KpiAssignmentRmUpdateComponent,
                    data: {
                      scope: Scopes.UPDATE,
                    },
                  },
                ],
              },
              {
                path: 'kpi-assignment-cbql',
                canActivateChild: [RoleGuardService],
                data: {
                  code: FunctionCode.KPI_ASSIGNMENT_CBQL,
                },
                children: [
                  {
                    path: '',
                    component: KpiAssignmentCbqlSearchComponent,
                    data: {
                      scope: Scopes.VIEW,
                    },
                  },
                  {
                    path: 'update/:id',
                    component: KpiAssignmentCbqlUpdateComponent,
                    data: {
                      scope: Scopes.UPDATE,
                    },
                  },
                ],
              },
              {
                path: 'kpi-assignment-receive',
                canActivateChild: [RoleGuardService],
                data: {
                  code: FunctionCode.KPI_ASSIGNMENT_RECEIVE,
                },
                children: [
                  {
                    path: '',
                    component: KpiAssignmentReceiveComponent,
                    data: {
                      scope: Scopes.VIEW,
                    },
                  },
                ],
              },
            ],
          },
          {
            path: 'kpi-track',
            children: [
              {
                path: 'kpi-manager-by-day',
                canActivateChild: [RoleGuardService],
                data: {
                  code: FunctionCode.KPI_CBQL,
                },
                children: [
                  {
                    path: '',
                    component: KpiManagerByDayComponent,
                    data: {
                      scope: Scopes.VIEW,
                    },
                  },
                  {
                    path: 'detail/:code',
                    component: DetailKpiManagerByDayComponent,
                    data: {
                      scope: Scopes.VIEW,
                    },
                  },
                  {
                    path: 'detail-sme',
                    component: KpiManagerDetailSmeComponent,
                    data: {
                      scope: Scopes.VIEW,
                    },
                  },
                ],
              },
              {
                path: 'kpi-rm',
                canActivateChild: [RoleGuardService],
                data: {
                  code: FunctionCode.KPI_RM,
                },
                children: [
                  {
                    path: '',
                    component: KpiRmListComponent,
                    data: {
                      scope: Scopes.VIEW,
                    },
                  },
                  {
                    path: 'detail/:id',
                    component: KpiRmDetailComponent,
                    data: {
                      scope: Scopes.VIEW,
                    },
                  },
                  {
                    path: 'detail-sme',
                    component: KpiRmDetailSmeComponent,
                    data: {
                      scope: Scopes.VIEW,
                    },
                  },
                ],
              },
            ],
          },
          {
            path: 'kpi-config',
            children: [
              {
                path: 'kpi-category',
                canActivateChild: [RoleGuardService],
                data: {
                  code: FunctionCode.KPI_ITEM,
                },
                children: [
                  {
                    path: '',
                    component: KpiCategoryComponent,
                    data: {
                      scope: Scopes.VIEW,
                    },
                  },
                  {
                    path: 'create',
                    component: KpiCategoryCreateComponent,
                    data: {
                      scope: Scopes.CREATE,
                    },
                  },
                  {
                    path: 'update/:id',
                    component: KpiCategoryUpdateComponent,
                    data: {
                      scope: Scopes.UPDATE,
                    },
                  },
                ],
              },
              {
                path: 'kpi-category-by-title',
                canActivateChild: [RoleGuardService],
                data: {
                  code: FunctionCode.KPI_CATEGORY_BY_TITLE,
                },
                children: [
                  {
                    path: '',
                    component: KpiCategoryByTitleComponent,
                    data: {
                      scope: Scopes.VIEW,
                    },
                  },
                  {
                    path: 'create',
                    component: KpiCategoryByTitleCreateComponent,
                    data: {
                      scope: Scopes.CREATE,
                    },
                  },
                  {
                    path: 'update/:id',
                    component: KpiCategoryUpdateComponent,
                    data: {
                      scope: Scopes.UPDATE,
                    },
                  },
                ],
              },
              {
                path: 'kpi-standard-rm',
                canActivateChild: [RoleGuardService],
                data: {
                  code: FunctionCode.KPI_STANDARD_RM,
                },
                children: [
                  {
                    path: '',
                    component: KpiStandardRmComponent,
                    data: {
                      scope: Scopes.VIEW,
                    },
                  },
                  {
                    path: 'create',
                    component: KpiCategoryCreateComponent,
                    data: {
                      scope: Scopes.CREATE,
                    },
                  },
                  {
                    path: 'update/:id',
                    component: KpiCategoryUpdateComponent,
                    data: {
                      scope: Scopes.UPDATE,
                    },
                  },
                ],
              },
              {
                path: 'kpi-standard-cbql',
                canActivateChild: [RoleGuardService],
                data: {
                  code: FunctionCode.KPI_STANDARD_CBQL,
                },
                children: [
                  {
                    path: '',
                    component: KpiStandardCbqlComponent,
                    data: {
                      scope: Scopes.VIEW,
                    },
                  },
                  {
                    path: 'create',
                    component: KpiStandardCbqlImportComponent,
                    data: {
                      scope: Scopes.CREATE,
                    },
                  },
                ],
              },
              {
                path: 'kpi-standard-cbql-branch-v2',
                canActivateChild: [RoleGuardService],
                data: {
                  code: FunctionCode.KPI_STANDARD_CBQL_BRANCH_V2,
                },
                children: [
                  {
                    path: '',
                    component: StandardKpiCBQLBranchV2Component,
                    data: {
                      scope: Scopes.VIEW,
                    },
                  },
                  {
                    path: 'detail',
                    component: KpiStandardCbqlBranchV2DetailComponent,
                    data: {
                      scope: Scopes.VIEW,
                    },
                  },
                  {
                    path: 'import',
                    component: StandardKpiCBQLBranchV2ImportComponent,
                    data: {
                      scope: Scopes.IMPORT,
                    },
                  },
                ],
              },
              {
                path: 'standard-kpi-cbql',
                // canActivateChild: [RoleGuardService],
                // data: {
                //   code: FunctionCode.KPI_STANDARD_CBQL,
                // },
                children: [
                  {
                    path: '',
                    component: StandardKpiCBQLComponent,
                    // data: {
                    //   scope: Scopes.VIEW,
                    // },
                  },
                  {
                    path: 'import',
                    component: StandardKpiCBQLImportComponent,
                    data: {
                      scope: Scopes.CREATE,
                    },
                  },
                ],
              },
              {
                path: 'view-kpi-standard-cbql',
                canActivateChild: [RoleGuardService],
                data: {
                  code: FunctionCode.VIEW_KPI_STANDARD_CBQL,
                },
                children: [
                  {
                    path: '',
                    component: KpiStandardCbqlViewComponent,
                    data: {
                      scope: Scopes.VIEW,
                    },
                  },
                  {
                    path: 'detail/:id',
                    component: KpiStandardCbqlDetailComponent,
                    data: {
                      scope: Scopes.VIEW,
                    },
                  },
                ],
              },
              {
                path: 'kpi-factor',
                canActivateChild: [RoleGuardService],
                data: {
                  code: FunctionCode.KPI_FACTOR,
                },
                children: [
                  {
                    path: 'kpi-region',
                    canActivateChild: [RoleGuardService],
                    data: {
                      code: FunctionCode.KPI_FACTOR_AREA,
                    },
                    children: [
                      {
                        path: '',
                        component: KpiRegionFactorComponent,
                        data: {
                          scope: Scopes.VIEW,
                        },
                      },
                      {
                        path: 'create',
                        component: KpiRegionFactorCreateComponent,
                        data: {
                          scope: Scopes.CREATE,
                        },
                      },
                      {
                        path: 'update/:id',
                        component: KpiRegionFactorUpdateComponent,
                        data: {
                          scope: Scopes.UPDATE,
                        },
                      },
                    ],
                  },
                  {
                    path: 'kpi-time',
                    canActivateChild: [RoleGuardService],
                    data: {
                      code: FunctionCode.KPI_FACTOR_BRANCHTIME,
                    },
                    children: [
                      {
                        path: '',
                        component: KpiBranchTimeComponent,
                        data: {
                          scope: Scopes.VIEW,
                        },
                      },
                      {
                        path: 'create',
                        component: KpiBranchTimeCreateComponent,
                        data: {
                          scope: Scopes.CREATE,
                        },
                      },
                      {
                        path: 'update/:blockCode',
                        component: KpiBranchTimeCreateComponent,
                        data: {
                          scope: Scopes.UPDATE,
                        },
                      },
                    ],
                  },
                  {
                    path: 'kpi-time-rm',
                    canActivateChild: [RoleGuardService],
                    data: {
                      code: FunctionCode.KPI_TIME_FACTOR,
                    },
                    children: [
                      {
                        path: '',
                        component: KpiRmTimeFactorComponent,
                        data: {
                          scope: Scopes.VIEW,
                        },
                      },
                      {
                        path: 'create',
                        component: KpiRmTimeFactorCreateComponent,
                        data: {
                          scope: Scopes.CREATE,
                        },
                      },
                      {
                        path: 'update/:id',
                        component: KpiRmTimeFactorUpdateComponent,
                        data: {
                          scope: Scopes.UPDATE,
                        },
                      },
                    ],
                  },
                ],
              },
              {
                path: 'target-sale',
                canActivateChild: [RoleGuardService],
                data: {
                  code: FunctionCode.TARGET_SALE_CONFIG,
                },
                children: [{
                  path: '',
                  component: TargetSaleConfigListComponent,
                  data: {
                    scope: Scopes.VIEW,
                  },
                },{
                  path: 'create',
                  component: TargetSaleConfigCreateComponent,
                  data: {
                    scope: Scopes.VIEW,
                  },
                },{
                  path: 'detail/:paramKey',
                  component: TargetSaleConfigUpdateComponent,
                  data: {
                    scope: Scopes.VIEW,
                  },
                },{
                  path: 'update/:paramKey',
                  component: TargetSaleConfigUpdateComponent,
                  data: {
                    scope: Scopes.VIEW,
                  },
                }]
              }
            ],
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class kpisRoutingModule {}
