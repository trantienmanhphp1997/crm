import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiCategoryByTitleComponent } from './kpi-category-by-title.component';

describe('KpiCategoryByTitleComponent', () => {
  let component: KpiCategoryByTitleComponent;
  let fixture: ComponentFixture<KpiCategoryByTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiCategoryByTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiCategoryByTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
