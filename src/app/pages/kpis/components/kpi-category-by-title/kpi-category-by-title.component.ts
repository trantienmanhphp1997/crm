import { forkJoin, of } from 'rxjs';
import { ChangeDetectorRef, Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { CampaignsService } from 'src/app/pages/campaigns/services/campaigns.service';
import { FunctionCode } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { SOURCE_INPUT } from '../../constant';
import { KpiCategoryByTitleApi, LevelApi, TitleGroupApi } from '../../apis';
import { KpiCategoryByTitleUpdateComponent } from '../kpi-category-by-title-update/kpi-category-by-title-update.component';
@Component({
	selector: 'app-kpi-category-by-title',
	templateUrl: './kpi-category-by-title.component.html',
	styleUrls: ['./kpi-category-by-title.component.scss'],
})
export class KpiCategoryByTitleComponent extends BaseComponent implements OnInit {
	isLoading = false;
	listBlock = [];
	formSearch = this.fb.group({
		blockCode: '',
		title: '', // title code
		level: '', // level code
		isActive: true,
		kpiItemName: '',
	});
	listGroup = [];
	listLevel = [];
	listKpiItem = [];
	listGroupByCategoryType = [];
	listKpiItemMapping = [];
	allRmLevel = [];

	constructor(
		injector: Injector,
		private campaignService: CampaignsService,
		private kpiCategoryByTitleApi: KpiCategoryByTitleApi,
		private cd: ChangeDetectorRef,
		private titleGroupApi: TitleGroupApi,
		private rmLevelApi: LevelApi
	) {
		super(injector);
		this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.KPI_CATEGORY_BY_TITLE}`);
		this.isLoading = true;
	}

	ngOnInit(): void {
		forkJoin([
			this.campaignService.getBlockMapping().pipe(catchError((e) => of(undefined))),
			// Get all rm level
			this.rmLevelApi.searchLevelByGroupCode('', 'all').pipe(catchError((e) => of(undefined))),
		]).subscribe(([listBlock, allRmLevel]) => {
			// map list block
			this.listBlock =
				listBlock?.content.map((item) => {
					return { code: item.code, name: item.code + ' - ' + item.name };
				}) || [];
			this.allRmLevel = allRmLevel?.content || [];
			this.isLoading = false;
			this.searchKpiAccordingTitles();
		});
	}

	getGroupAndLevelBlockCode = async (code) => {
		const [listGroup, listLevel] = await Promise.all([
			this.titleGroupApi
				.searchGroupByBlockCode(code)
				.pipe(catchError((e) => of(undefined)))
				.toPromise(),
			this.rmLevelApi
				.searchLevelByGroupCode('', code)
				.pipe(catchError((e) => of(undefined)))
				.toPromise(),
		]);

		return { listGroup: listGroup?.content || [], listLevel: listLevel?.content || [] };
	};

	async onChangeBlockCode() {
		const blockCode = this.formSearch.value.blockCode;
		const data = await this.getGroupAndLevelBlockCode(blockCode);
		this.listGroup = this.mapListGroup(data.listGroup);
		this.listLevel = this.mapListLevel(data.listLevel);
		// Re-set value title and level
		const objectData = {
			title: '',
			level: '',
		};
		this.formSearch.setValue({ ...this.formSearch.value, ...objectData });
	}

	async onChangeGroup() {
		const blockCode = this.formSearch.value.blockCode;
		const groupCode = this.formSearch.value.title;
		const groupSelected = this.listGroup.find((group) => group.code === groupCode);
		const listLevel =
			(
				await this.rmLevelApi
					.searchLevelByGroupCode(groupSelected?.id || '', blockCode)
					.pipe(catchError((e) => of(undefined)))
					.toPromise()
			)?.content || [];
		this.listLevel = this.mapListLevel(listLevel);
		// Re-set value level
		const objectData = {
			level: '',
		};
		this.formSearch.setValue({ ...this.formSearch.value, ...objectData });
	}

	mapListGroup = (listGroup) => {
		const listGroupTemp =
			listGroup?.map((item) => {
				return { ...item, name: item.blockCode + ' - ' + item.name };
			}) || [];
		return [{ code: '', name: 'Tất cả', kpiRmLevelOutputs: [] }, ...listGroupTemp];
	};

	mapListLevel = (listLevel) => {
		return [{ code: '', name: 'Tất cả' }, ...listLevel];
	};

	onTreeAction(event: any) {
		const row = event.row;

		if (row.treeStatus === 'collapsed') {
			row.treeStatus = 'expanded';
		} else {
			row.treeStatus = 'collapsed';
		}

		this.listKpiItemMapping = [...this.listKpiItemMapping];
		this.cd.detectChanges();
	}

	searchKpiAccordingTitles = async () => {
		const params = this.formSearch.value;
		this.isLoading = true;

		const listKpiByTitle =
			(await this.kpiCategoryByTitleApi
				.searchKpiAccordingTitles(params)
				.pipe(catchError((e) => of(undefined)))
				.toPromise())?.sort((a, b) => (a.categoryCode >= b.categoryCode) ? 1 : -1) || [];
		this.listKpiItemMapping = [];
		// group by categoryCode
		const listGroupByCategoryType = Object.values(
			listKpiByTitle?.reduce(
				(r, a) => ({
					...r,
					[a.categoryCode]: [...(r[a.categoryCode] || []), a],
				}),
				{}
			)
		);

		listGroupByCategoryType.forEach((e: Array<any>) => {
			// first level nested
			this.listKpiItemMapping.push({
				idGroup: e[0]?.categoryCode,
				name: e[0]?.categoryName,
				treeStatus: this.formSearch.value.level ? 'expanded' : 'collapsed',
				parentId: null,
			});
			// group by kpiItemId each CategoryType
			const listGroupByKpiItem = Object.values(
				e?.reduce(
					(r, a) => ({
						...r,
						[a.kpiItemId]: [...(r[a.kpiItemId] || []), a],
					}),
					{}
				)
			);

			listGroupByKpiItem.forEach((element: Array<any>) => {
				// second level nested
				this.listKpiItemMapping.push({
					idGroup: element[0]?.kpiItemId,
					name: element[0]?.kpiItemName,
					treeStatus: this.formSearch.value.level ? 'expanded' : 'collapsed',
					parentId: element[0]?.categoryCode,
				});

				element.forEach((i) => {
					const PREFIX_KPI_TITLE = 'PREFIX_KPI_TITLE';
					const frequencyNames = [];
					i.frequencies?.length &&
						i.frequencies.forEach((element) => frequencyNames.push(element.frequencyName));
					const sourceInputName = SOURCE_INPUT.find((source) => source.code == i.sourceInput)?.name || ''; // == cause different type
					const rmLevelName = this.allRmLevel.find((item) => item.code === i.rmLevelCode)?.name || '';
					// last level nested
					this.listKpiItemMapping.push({
						...i,
						// idGroup is required cause prevent render error
						// idGroup need prefix cause prevent equal idGroup of parent
						idGroup: `${PREFIX_KPI_TITLE}_${i.kpiRmLevelId}`,
						name: '',
						rate: i.rate.toString().replace('.', ','),
						limitFloor: i.limitFloor ? i.limitFloor.toString().replace('.', ',') : '',
						limitCeiling: i.limitCeiling ? i.limitCeiling.toString().replace('.', ',') : '',
						ceilingRatio: i.ceilingRatio ? i.ceilingRatio.toString().replace('.', ',') : '',
						treeStatus: 'disabled',
						parentId: i.kpiItemId,
						frequency: frequencyNames.join(', '),
						sourceInput: sourceInputName,
						rmLevelName,
					});
				});
			});
		});

		this.isLoading = false;
	};

	isBoolean = (value) => typeof value === 'boolean';

	create() {
		this.router.navigate([this.router.url, 'create']);
	}

	async update(item) {
		const params = {
			blockCode: item.blockCode,
			title: item.rmTitleCode,
			level: item.rmLevelCode,
			kpiItemId: item.kpiItemId,
			kpiItemName: item.kpiItemName
    };

		const listKpiByTitle =
			(await this.kpiCategoryByTitleApi
				.searchKpiAccordingTitles(params)
				.pipe(catchError((e) => of(undefined)))
				.toPromise()) || [];

		const updateModal = this.modalService.open(KpiCategoryByTitleUpdateComponent, {
			scrollable: true,
			windowClass: 'kpi-category-by-title-update',
		});

		updateModal.componentInstance.listKpiByTitle = listKpiByTitle;
		updateModal.componentInstance.params = params;
		updateModal.componentInstance.listBlock = this.listBlock;
		updateModal.result
			.then((data) => {
				this.searchKpiAccordingTitles();
			})
			.catch(() => {});
	}

	delete(item) {
		this.confirmService
			.confirm()
			.then((res) => {
				console.log('res', res);
				if (res) {
					this.isLoading = true;
					this.kpiCategoryByTitleApi
						.deleteAllHistoryProperty({
							kpiItemId: item.kpiItemId,
							rmLevelCode: item.rmLevelCode,
							rmTitleCode: item.rmTitleCode,
							blockCode: item.blockCode,
						})
						.subscribe(
							() => {
								this.isLoading = false;
								this.searchKpiAccordingTitles();
								this.messageService.success(this.notificationMessage.success);
							},
							(e) => {
								if (e?.error) {
									this.messageService.warn(e?.error?.description);
								} else {
									this.messageService.error(this.notificationMessage.error);
								}
								this.isLoading = false;
							}
						);
				}
			})
			.catch(() => {});
	}
}
