import { of } from 'rxjs';
import { ChangeDetectorRef, Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { catchError } from 'rxjs/operators';
import { CODE_FIRST_6M, LIST_MONTH, LIST_SIX_MONTH, LIST_YEAR } from '../../constant';
import { KpiStandardRmApi } from '../../apis';
import * as moment from 'moment';
import { FunctionCode, functionUri } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'app-kpi-standard-cbql-detail',
  templateUrl: './kpi-standard-cbql-detail.component.html',
  styleUrls: ['./kpi-standard-cbql-detail.component.scss'],
})
export class KpiStandardCbqlDetailComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listBlock = [];
  formSearch = this.fb.group({
    isMonth: true,
    month: parseInt(moment().format('MM')),
    year: parseInt(moment().format('yyyy')),
  });
  listGroup = [];
  listLevel = [];
  listKpiItem = [];
  listGroupByCategoryType = [];
  listKpiItemMapping = [];
  listYear = LIST_YEAR;
  listMonth = LIST_MONTH;
  dataFromView: any;

  constructor(injector: Injector, private cd: ChangeDetectorRef, private kpiStandardRmApi: KpiStandardRmApi) {
    super(injector);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.dataFromView = this.sessionService.getSessionData(FunctionCode.KPI_STANDARD_CBQL_DETAIL);

    this.formSearch.controls.isMonth.valueChanges.subscribe((value) => {
      if (value) {
        this.listMonth = LIST_MONTH;
        this.formSearch.controls.month?.setValue(parseInt(moment().format('MM')));
      } else {
        this.listMonth = LIST_SIX_MONTH;
        this.formSearch.controls.month?.setValue(CODE_FIRST_6M);
      }
    });

    this.isLoading = false;
    this.search();
  }

  onTreeAction(event: any) {
    const row = event.row;

    if (row.treeStatus === 'collapsed') {
      row.treeStatus = 'expanded';
    } else {
      row.treeStatus = 'collapsed';
    }

    this.listKpiItemMapping = [...this.listKpiItemMapping];
    this.cd.detectChanges();
  }

  search = async () => {
    let formData = this.formSearch.value;
    const params = {
      blockCode: this.dataFromView?.block?.code,
      rmTitleCode: this.dataFromView?.title?.code,
      rmLevelCode: this.dataFromView?.level?.code,
      month: formData.month,
      year: formData.year,
      hrisCode: this.dataFromView?.hrisCode,
      isInputKpiRm: true,
    };
    this.isLoading = true;

    let listKpiTimeFactors =
      (await this.kpiStandardRmApi
        .searchKpiStandard(params)
        .pipe(catchError((e) => of(undefined)))
        .toPromise()) || [];
    this.listKpiItemMapping = [];
    // group by categoryCode
    let listGroupByCategoryType = Object.values(
      listKpiTimeFactors?.reduce(
        (r, a) => ({
          ...r,
          [a.categoryCode]: [...(r[a.categoryCode] || []), a],
        }),
        {}
      )
    );

    listGroupByCategoryType.forEach((e: Array<any>) => {
      // first level nested
      this.listKpiItemMapping.push({
        idGroup: e[0]?.categoryCode,
        name: e[0]?.categoryName,
        treeStatus: 'expanded',
        parentId: null,
      });

      e.forEach((i) => {
        const frequencyNames = [];
        i.frequencies?.length && i.frequencies.forEach((element) => frequencyNames.push(element.frequencyName));
        const value = i.targets?.[0]?.value || '';
        // last level nested
        this.listKpiItemMapping.push({
          ...i,
          // idGroup is required cause prevent render error
          idGroup: i.kpiItemRmLevelId,
          name: i.kpiItemName,
          treeStatus: 'disabled',
          parentId: i.categoryCode,
          frequency: frequencyNames.join(', '),
          value,
        });
      });
    });
    this.isLoading = false;
  };
}
