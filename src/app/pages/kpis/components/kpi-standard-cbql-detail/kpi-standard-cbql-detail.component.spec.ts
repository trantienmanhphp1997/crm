import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiStandardCbqlDetailComponent } from './kpi-standard-cbql-detail.component';

describe('KpiStandardCbqlDetailComponent', () => {
  let component: KpiStandardCbqlDetailComponent;
  let fixture: ComponentFixture<KpiStandardCbqlDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiStandardCbqlDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiStandardCbqlDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
