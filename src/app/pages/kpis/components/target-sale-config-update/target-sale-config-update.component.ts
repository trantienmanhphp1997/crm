import {AfterViewInit, Component, Injector, OnInit, ViewEncapsulation} from '@angular/core';
import {BaseComponent} from 'src/app/core/components/base.component';
import {StandardKpiCbqlApi} from '../../apis/standard-kpi-cbql.api';
import {forkJoin, of} from 'rxjs';
import {CommonCategory, ConfirmType, Division, FunctionCode, Scopes} from '../../../../core/utils/common-constants';
import {CategoryService} from '../../../system/services/category.service';
import {KpiRmApi} from '../../apis';
import {catchError} from 'rxjs/operators';
import * as _ from 'lodash';
import {formatDate} from '@angular/common';
import {ColumnMode, DatatableComponent} from '@swimlane/ngx-datatable';
import {RmApi} from '../../../rm/apis';
import {AppFunction} from '../../../../core/interfaces/app-function.interface';
import {Utils} from '../../../../core/utils/utils';
import {TargetSaleConfigEditModalComponent} from '../target-sale-config-edit-modal/target-sale-config-edit-modal.component';
import {TargetSaleConfigErrorModalComponent} from '../target-sale-config-list-error-modal/target-sale-config-error-modal.component';
import {ConfirmDialogComponent} from '../../../../shared/components';


@Component({
  selector: 'app-target-sale-config-update',
  templateUrl: './target-sale-config-update.component.html',
  styleUrls: ['./target-sale-config-update.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TargetSaleConfigUpdateComponent extends BaseComponent implements OnInit, AfterViewInit {

  listWeeksData: any = [];
  listMonthsData: any = [];
  allotmentValue = 'MONTH';
  ColumnMode = ColumnMode;
  autoAllocation = false;
  public objFunctionTagetSale: AppFunction;
  screen = this.screenType.detail;
  listDeleteData: any = [];

  commonData = {
    listDivision: [{name: 'INDIV', code: 'INDIV'}],
    listTitleGroup: [],
    listLevelRM: [],
    listAllotmentValue: [{name: 'Tháng', code: 'MONTH'}, {name: 'Tuần', code: 'WEEK'}, {name: 'Tất cả', code: 'ALL'}],
    listProduct: [],
    listBranch: []
  };

  constructor(
    injector: Injector,
    private standardKpiCbqlApi: StandardKpiCbqlApi,
    private categoryService: CategoryService,
    private api: KpiRmApi,
    private rmApi: RmApi,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.objFunctionTagetSale = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.TARGET_SALE_CONFIG}`);
    this.screen = this.route.snapshot.routeConfig.path.includes('update') ? this.screenType.update : this.screenType.detail;
  }

  ngOnInit() {
    this.isLoading = true;
    this.prop = this.sessionService.getSessionData(FunctionCode.TARGET_SALE_CONFIG);
    if (!this.prop) {
      forkJoin([
        this.api.getCategoryRmByBlock({
          viewRm: true,
          blockCode: 'INDIV',
          rsId: this.objFunction?.rsId,
          scope: Scopes.VIEW,
          past: 1,
        }),
        this.categoryService
          .getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW)
          .pipe(catchError((e) => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.TARGET_SALE_CONFIG_PRODUCT).pipe(catchError(() => of(undefined))),
      ]).subscribe(([listTitleGroupByDivision, branchesOfUser, targetSaleProductConfig]) => {
        this.commonData.listProduct = _.get(targetSaleProductConfig, 'content')
        this.commonData.listBranch = branchesOfUser?.map((item) => {
          return {code: item.code, displayName: item.code + ' - ' + item.name};
        });
        listTitleGroupByDivision?.forEach((item) => {
          item.name = `${item.blockCode} - ${item.name}`;
        });
        this.commonData.listTitleGroup = _.sortBy(listTitleGroupByDivision, 'id') || [];
      });
    } else {
      this.commonData = this.prop.commonData;
    }
    this.getDetail();
  }

  ngAfterViewInit() {
  }


  generateRowHeight(row) {
    if (row?.targetSaleConfigDetail.length === 1) {
      return 50;
    }
    return 38 * row?.targetSaleConfigDetail.length;
  }

  update(row) {
    const modal = this.modalService.open(TargetSaleConfigEditModalComponent, {windowClass: 'target-sale-config-edit-modal'});
    modal.componentInstance.data = row;
    modal.componentInstance.commonData = this.commonData;
    modal.componentInstance.screen = this.screen;
    modal.result
      .then((res) => {
        if (res) {
          this.editAction(row, res);
        } else {
          row.startDate = formatDate(row.startDate, 'dd/MM/yyyy', 'en');
          row.endDate = formatDate(row.endDate, 'dd/MM/yyyy', 'en');
        }
      })
      .catch(() => {
      });
  }

  editAction(oldObj, newObj) {
    oldObj.isUpdate = true;
    oldObj.value = newObj.value;
    oldObj.autoAllocation = newObj.autoAllocation;
    const indexMonth = _.findIndex(this.listMonthsData, i => (i.rmTitleId === oldObj.rmTitleId && i.rmLevelId === oldObj.rmLevelId && i.branchCode === oldObj.branchCode && i.product === oldObj.product));
    const indexWeek = _.findIndex(this.listWeeksData, i => (i.rmTitleId === oldObj.rmTitleId && i.rmLevelId === oldObj.rmLevelId && i.branchCode === oldObj.branchCode && i.product === oldObj.product));
    if (indexMonth !== -1) {
      const dataMonth: any = {
        ...oldObj,
        allotmentValue: 'MONTH',
        targetSaleConfigDetail: this.generateDetailsTypeMonth(oldObj.startDate, oldObj.endDate, newObj.value, newObj.autoAllocation, this.listMonthsData[indexMonth]?.targetSaleConfigDetail),
        // startDate: formatDate(newObj.startDate, 'dd/MM/yyyy', 'en'),
        // endDate: formatDate(newObj.endDate, 'dd/MM/yyyy', 'en')
      };
      dataMonth.startDate = formatDate(newObj.startDate, 'dd/MM/yyyy', 'en');
      dataMonth.endDate = formatDate(newObj.endDate, 'dd/MM/yyyy', 'en');
      this.listMonthsData[indexMonth] = dataMonth;
      const tempM = Object.assign([], this.listMonthsData);
      this.listMonthsData = tempM;
    }
    if (indexWeek !== -1) {
      const dataWeek: any = {
        ...oldObj,
        allotmentValue: 'WEEK',
        targetSaleConfigDetail: this.generateDetailsTypeWeek(oldObj.startDate, oldObj.endDate, newObj.value, newObj.autoAllocation, this.listWeeksData[indexWeek]?.targetSaleConfigDetail),
        // startDate: formatDate(newObj.startDate, 'dd/MM/yyyy', 'en'),
        // endDate: formatDate(newObj.endDate, 'dd/MM/yyyy', 'en')
      };
      dataWeek.startDate = formatDate(newObj.startDate, 'dd/MM/yyyy', 'en');
      dataWeek.endDate = formatDate(newObj.endDate, 'dd/MM/yyyy', 'en');
      this.listWeeksData[indexWeek] = dataWeek;
      const tempW = Object.assign([], this.listWeeksData);
      this.listWeeksData = tempW;
    }
  }

  delete(indexData, isMonth?: boolean) {
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.result
      .then((res) => {
        if (res) {
          if (isMonth) {
            this.listMonthsData = this.listMonthsData.filter((item, index) => {
              if (index === indexData) {
                this.listDeleteData.push(item);
              }
              return index !== indexData;
            });
          } else {
            this.listWeeksData = this.listWeeksData.filter((item, index) => {
              if (index === indexData) {
                this.listDeleteData.push(item);
              }
              return index !== indexData;
            });
          }
        }
      })
      .catch(() => {});
  }

  changeValue(chilIndex, parentIndex, isMonth: boolean) {
    if (!isMonth) {
      // tổng giá trị trong chi tiết
      let totalValueDetail = 0;
      this.listWeeksData[parentIndex].isUpdate = true;
      this.listWeeksData[parentIndex].targetSaleConfigDetail.forEach(item => {
        item.isUpdate = true;
        totalValueDetail += item.value;
      });
      // phần dư
      let remainingValue = totalValueDetail - this.listWeeksData[parentIndex].value;
      let i = this.listWeeksData[parentIndex].targetSaleConfigDetail.length - 1;
      // vòng lặp
      // lặp arr từ index cuối lên đầu
      while (i >= 0) {
        if (i !== chilIndex) {
          // nếu phần tử ko đủ giá trị để trừ đi phần dư
          if (remainingValue >= this.listWeeksData[parentIndex].targetSaleConfigDetail[i].value) {
            // tính lại phần dư
            remainingValue = remainingValue - this.listWeeksData[parentIndex].targetSaleConfigDetail[i].value;
            // gán phần từ thành 0
            this.listWeeksData[parentIndex].targetSaleConfigDetail[i].value = 0;
          } else {
            // trừ nốt phần dư
            this.listWeeksData[parentIndex].targetSaleConfigDetail[i].value = this.listWeeksData[parentIndex].targetSaleConfigDetail[i].value - remainingValue;
            break;
          }
        }
        i--;
      }
    } else {
      // tổng giá trị trong chi tiết
      let totalValueDetail = 0;
      this.listMonthsData[parentIndex].isUpdate = true;
      this.listMonthsData[parentIndex].targetSaleConfigDetail.forEach(item => {
        item.isUpdate = true;
        totalValueDetail += item.value;
      });
      // phần dư
      let remainingValue = totalValueDetail - this.listMonthsData[parentIndex].value;
      let i = this.listMonthsData[parentIndex].targetSaleConfigDetail.length - 1;
      // vòng lặp
      // lặp arr từ index cuối lên đầu
      while (i >= 0) {
        if (i !== chilIndex) {
          // nếu phần tử ko đủ giá trị để trừ đi phần dư
          if (remainingValue >= this.listMonthsData[parentIndex].targetSaleConfigDetail[i].value) {
            // tính lại phần dư
            remainingValue = remainingValue - this.listMonthsData[parentIndex].targetSaleConfigDetail[i].value;
            // gán phần từ thành 0
            this.listMonthsData[parentIndex].targetSaleConfigDetail[i].value = 0;
          } else {
            // trừ nốt phần dư
            this.listMonthsData[parentIndex].targetSaleConfigDetail[i].value = this.listMonthsData[parentIndex].targetSaleConfigDetail[i].value - remainingValue;
            break;
          }
        }
        i--;
      }
    }
  }


  convertValue(value) {
    return Utils.numberWithCommas(value);
  }

  saveAll() {
    this.isLoading = true;
    this.listDeleteData.forEach(item => {
      item.isActive = false;
      item.targetSaleConfigDetail.forEach(chil => {
        chil.isActive = false;
      })
    });
    const arrData = [..._.filter(this.listMonthsData, i => i.isUpdate), ..._.filter(this.listWeeksData, i => i.isUpdate), ...this.listDeleteData];
    const data = {
      rsId: this.objFunctionTagetSale.rsId,
      scope: 'VIEW',
      data: arrData
    }
    console.log(data);
    this.rmApi.updateTargetSaleConfig(data).subscribe(listDataError => {
      if (!_.isEmpty(listDataError)) {
        this.isLoading = false;
        this.openModalError(listDataError);
      } else {
        this.isLoading = false;
        this.messageService.success(this.notificationMessage.success);
        this.back();
      }
    }, error => {
      this.isLoading = false;
      this.messageService.error(this.notificationMessage.error);
    });
  }

  getDetail() {
    this.rmApi.getTargetSaleConfig(this.route.snapshot.params.paramKey).subscribe(data => {
      const tempM = [];
      const tempW = [];
      data.forEach(item => {
        if (item.targetSaleConfigDetail[0].type === 'WEEK') {
          item.allotmentValue = 'WEEK';
          tempW.push(item);
        } else {
          item.allotmentValue = 'MONTH';
          tempM.push(item);
        }
      });
      this.listWeeksData = tempW;
      this.listMonthsData = tempM;

      console.log(data);
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
      this.messageService.error(this.notificationMessage.error);
    });
  }

  generateDetailsTypeMonth(startDate, endDate, totalValue, autoAllocation, listChild) {
    const dates = [];
    let tempValue = 0;

    const totalDate = this.caculatorDate(endDate, startDate);
    const averageValue = totalValue / totalDate;
    let currentDate = new Date(startDate);
    currentDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
    let index = 0;
    while (currentDate <= endDate) {
      const endMonthDate = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0);
      let value;
      let startDateValue;
      let endDateValue;
      if (startDate.getMonth() === endDate.getMonth() && startDate.getFullYear() === endDate.getFullYear()) {
        startDateValue = formatDate(startDate, 'dd/MM/yyyy', 'en');
        endDateValue = formatDate(endDate, 'dd/MM/yyyy', 'en');
        value = autoAllocation ? totalValue - tempValue : totalValue;
      } else if (startDate > currentDate) {
        startDateValue = formatDate(startDate, 'dd/MM/yyyy', 'en');
        endDateValue = formatDate(endMonthDate, 'dd/MM/yyyy', 'en');
        value = autoAllocation ? Math.round(averageValue * (endMonthDate.getDate() - startDate.getDate() + 1)) : 0;
        tempValue += value;
      } else if (endDate <= endMonthDate) {
        startDateValue = formatDate(currentDate, 'dd/MM/yyyy', 'en');
        endDateValue = formatDate(endDate, 'dd/MM/yyyy', 'en');
        value = autoAllocation ? totalValue - tempValue : totalValue;
      } else {
        startDateValue = formatDate(currentDate, 'dd/MM/yyyy', 'en');
        endDateValue = formatDate(endMonthDate, 'dd/MM/yyyy', 'en');
        // value = autoAllocation ? Math.round(averageValue * (endMonthDate.getDate() - currentDate.getDate() + 1)) : 0;
        // startDateValue = formatDate(currentDate, 'dd/MM/yyyy', 'en');
        // endDateValue = formatDate(endWeekDate, 'dd/MM/yyyy', 'en');
        value = (autoAllocation && ((tempValue + Math.round(averageValue * (endMonthDate.getDate() - currentDate.getDate() + 1))) <= totalValue))
          ? Math.round(averageValue * (endMonthDate.getDate() - currentDate.getDate() + 1))
          : 0;
        tempValue += value;
      }
      dates.push({
        id: listChild[index].id,
        createdDate: listChild[index].createdDate,
        createdBy: listChild[index].createdBy,
        startDate: startDateValue,
        endDate: endDateValue,
        value,
        type: 'MONTH',
        isActive: true,
        isUpdate: true
      });
      currentDate = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 1);
      index ++;
    }
    return dates;
  };

  generateDetailsTypeWeek(startDate, endDate, totalValue, autoAllocation, listChild) {
    const dates = [];

    const totalDate = parseInt(String((endDate - startDate) / (1000 * 60 * 60 * 24)), 10) + 1;
    // tính giá trị trung bình mỗi ngày
    const averageValue = totalValue / totalDate;
    let currentDate: any = new Date(startDate);

    if (currentDate.getDay() !== 1) {
      // not a tuesday
      currentDate.setDate(currentDate.getDate() - currentDate.getDay() + 1);
    }
    let tempValue = 0;
    let index = 0;
    while (currentDate <= endDate) {
      const endWeekDate: any = this.addDays(currentDate, 6);
      let value;
      let startDateValue;
      let endDateValue;
      if (this.isSameWeek(startDate, endDate)) {
        // Ngày bắt đầu và kết thúc trong cùng 1 tuần
        startDateValue = formatDate(startDate, 'dd/MM/yyyy', 'en');
        endDateValue = formatDate(endDate, 'dd/MM/yyyy', 'en');
        value = autoAllocation ? totalValue - tempValue : totalValue;

      } else if (startDate > currentDate) {
        // tuần đầu tiên
        startDateValue = formatDate(startDate, 'dd/MM/yyyy', 'en');
        endDateValue = formatDate(endWeekDate, 'dd/MM/yyyy', 'en');
        value = autoAllocation ? Math.round(averageValue * this.caculatorDate(endWeekDate, startDate)) : 0;
        tempValue += value;
      } else if (endDate <= endWeekDate) {
        // tuần cuối cùng
        startDateValue = formatDate(currentDate, 'dd/MM/yyyy', 'en');
        endDateValue = formatDate(endDate, 'dd/MM/yyyy', 'en');
        value = autoAllocation ? totalValue - tempValue : totalValue;
      } else {
        // các tuần ở giữa
        startDateValue = formatDate(currentDate, 'dd/MM/yyyy', 'en');
        endDateValue = formatDate(endWeekDate, 'dd/MM/yyyy', 'en');
        value = (autoAllocation && ((tempValue + Math.round(averageValue * this.caculatorDate(endWeekDate, currentDate))) <= totalValue))
          ? Math.round(averageValue * this.caculatorDate(endWeekDate, currentDate))
          : 0;
        tempValue += value;

      }
      dates.push({
        id: listChild[index].id,
        createdDate: listChild[index].createdDate,
        createdBy: listChild[index].createdBy,
        startDate: startDateValue,
        endDate: endDateValue,
        value,
        type: 'WEEK',
        isActive: true,
        isUpdate: true
      });
      currentDate = this.addDays(currentDate, 7);
      index ++;
    }
    return dates;
  };

  caculatorDate(endDate, startDate) {
    return parseInt(String((endDate - startDate) / (1000 * 60 * 60 * 24)), 10) + 1;
  }

  isSameWeek(dateA, dateB) {
    return this.getWeek(dateA) === this.getWeek(dateB);
  }

  addDays(currentDate, days) {
    const date = new Date(currentDate);
    date.setDate(date.getDate() + days);
    return date;
  };

  getWeek(date) {
    const janFirst = new Date(date.getFullYear(), 0, 1);
    return Math.ceil((((date.getTime() - janFirst.getTime()) / 86400000) + janFirst.getDay() + 1) / 7);
  }

  openModalError(listDataError) {
    const modalError = this.modalService.open(TargetSaleConfigErrorModalComponent, {windowClass: 'list__target-sale-config-error'});
    modalError.componentInstance.dataError = listDataError;
    modalError.result
      .then(() => {

      })
      .catch(() => {
      });
  }

}
