import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { forkJoin, of } from 'rxjs';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Utils } from 'src/app/core/utils/utils';
import { KpiMonthApi, KpiRmApi } from '../../apis';
import * as _ from 'lodash';
import { FunctionCode, Scopes } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { Table } from 'primeng/table';
import { formatNumber } from '@angular/common';
import { FileService } from 'src/app/core/services/file.service';

@Component({
  selector: 'app-kpi-rm-detail',
  templateUrl: './kpi-rm-detail.component.html',
  styleUrls: ['./kpi-rm-detail.component.scss'],
})
export class KpiRmDetailComponent extends BaseComponent implements OnInit {
  @ViewChild('tableRm') pTableRef: Table;
  isLoading = false;
  customers: any;
  customersInfo: any;
  rowGroupMetadata: any;
  paramKmDetail: any;
  idKmDetail: any;
  models: Array<any>;
  rows: Array<any>;
  count: any;
  period: any;
  rsId: any;
  dateRmKpi: any;
  expandedRows: {} = {};
  past: any;
  // isLoadingMessage = false;
  dateMonth: any;

  constructor(
    injector: Injector,
    private monthApi: KpiMonthApi,
    private api: KpiRmApi,
    private fileService: FileService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.rsId = this.objFunction?.rsId;
    const state = this.router.getCurrentNavigation()?.extras?.state;
    this.idKmDetail = this.route.snapshot?.paramMap.get('id');
    this.period = state?.period;
    this.past = state?.past;
    this.dateMonth = state?.businessDate;
  }

  ngOnInit(): void {
    this.isLoading = true;
    if (this.period === 0) {
      forkJoin([
        this.monthApi
          .getByIdDay(this.idKmDetail, this.objFunction?.rsId, Scopes.VIEW, this.past)
          .pipe(catchError((e) => of(undefined))),
        this.api.getDateRm(0).pipe(catchError((e) => of(undefined))),
      ]).subscribe(
        ([dataDetail, dateRmKpi]) => {
          if (dataDetail === undefined) {
            const listClass = document.getElementById('tableRm').getElementsByClassName('p-datatable-tbody');
            listClass.item(1).setAttribute('style', 'text-align: center;display: table-caption;');
            this.messageService.error(this.notificationMessage.E001);
            this.isLoading = false;
          }
          this.dateRmKpi = dateRmKpi;
          this.customers = dataDetail.details;
          this.customersInfo = dataDetail;
          this.customers = this.customers.filter((item) => Object.keys(item).length !== 0);
          if (document.getElementById('tableRm') && this.customers.length === 0) {
            const listClass = document.getElementById('tableRm').getElementsByClassName('p-datatable-tbody');
            listClass.item(1).setAttribute('style', 'text-align: center;display: table-caption;');
          }
          const thisRef = this;
          this.customers.forEach(function (car) {
            thisRef.expandedRows[car.groupParentName] = true;
          });

          this.expandedRows = Object.assign({}, this.expandedRows);
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    } else {
      forkJoin([
        this.monthApi
          .getByIdMonth(this.idKmDetail, this.objFunction?.rsId, Scopes.VIEW, this.past)
          .pipe(catchError((e) => of(undefined))),
        this.api.getDateRm(1).pipe(catchError((e) => of(undefined))),
      ]).subscribe(
        ([dataDetail, dateRmKpi]) => {
          if (dataDetail === undefined) {
            const listClass = document.getElementById('tableRm').getElementsByClassName('p-datatable-tbody');
            listClass.item(1).setAttribute('style', 'text-align: center;display: table-caption;');
            this.messageService.error(this.notificationMessage.E001);
            this.isLoading = false;
          }
          this.dateRmKpi = dateRmKpi;
          this.customers = dataDetail.details;
          this.customersInfo = dataDetail;
          this.customers = this.customers.filter((item) => Object.keys(item).length !== 0);
          // this.customers = [];
          if (document.getElementById('tableRm') && this.customers.length === 0) {
            const listClass = document.getElementById('tableRm').getElementsByClassName('p-datatable-tbody');
            listClass.item(1).setAttribute('style', 'text-align: center;display: table-caption;');
          }
          const thisRef = this;
          this.customers.forEach(function (car) {
            thisRef.expandedRows[car.groupParentName] = true;
          });
          this.expandedRows = Object.assign({}, this.expandedRows);
          this.isLoading = false;
        },
        () => {
          this.messageService.error(this.notificationMessage.E001);
          this.isLoading = false;
        }
      );
    }

    // this.isLoadingMessage = true;
  }

  onSort() {
    this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    let count = 1;
    if (this.customers) {
      for (let i = 0; i < this.customers.length; i++) {
        const rowData = this.customers[i];
        const representativeName = rowData.groupParentName;
        if (i == 0) {
          this.rowGroupMetadata[representativeName] = { index: 0, size: 1 };
          this.customers[i].index = count;
          count++;
        } else {
          const previousRowData = this.customers[i - 1];
          const previousRowGroup = previousRowData.groupParentName;
          if (representativeName === previousRowGroup) {
            if (i === 1) {
              this.customers[i].index = 2;
              this.rowGroupMetadata[representativeName].size++;
            } else {
              count++;
              this.customers[i].index = count;
              this.rowGroupMetadata[representativeName].size++;
            }
          } else {
            count = 1;
            this.customers[i].index = count;
            this.rowGroupMetadata[representativeName] = { index: i, size: 1 };
          }
        }
      }
    }
  }

  exportFile() {
    if (Utils.isArrayEmpty(this.customers)) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    this.isLoading = true;
    if (this.period === 1) {
      this.api.exportKPIRmMonth(this.idKmDetail, this.rsId, Scopes.VIEW, this.past).subscribe(
        (res) => {
          if (Utils.isStringNotEmpty(res)) {
            this.download(res);
          } else {
            this.messageService.error(_.get(this.notificationMessage, 'export_error'));
            this.isLoading = false;
          }
        },
        (err) => {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      );
    } else {
      this.api.exportKPIRmDay(this.idKmDetail, this.rsId, Scopes.VIEW, this.past).subscribe(
        (res) => {
          if (Utils.isStringNotEmpty(res)) {
            this.download(res);
          } else {
            this.messageService.error(_.get(this.notificationMessage, 'export_error'));
            this.isLoading = false;
          }
        },
        () => {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      );
    }
  }

  download(fileId: string) {
    this.fileService.downloadFile(fileId, 'danh sach kpi-rm.xlsx').subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }

  formatValue(param) {
    if (param !== undefined) {
      return formatNumber(param, 'en', '1.0-4');
    }
    return;
  }
}
