import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiRmDetailComponent } from './kpi-rm-detail.component';

describe('KpiRmDetailComponent', () => {
  let component: KpiRmDetailComponent;
  let fixture: ComponentFixture<KpiRmDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiRmDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiRmDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
