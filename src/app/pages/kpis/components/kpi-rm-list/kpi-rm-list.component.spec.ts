import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiRmListComponent } from './kpi-rm-list.component';

describe('KpiRmListComponent', () => {
  let component: KpiRmListComponent;
  let fixture: ComponentFixture<KpiRmListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiRmListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiRmListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
