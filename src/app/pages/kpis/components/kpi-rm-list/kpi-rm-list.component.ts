import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';
import {
  CommonCategory,
  Division,
  FunctionCode,
  maxInt32,
  Scopes,
  SessionKey,
} from 'src/app/core/utils/common-constants';
import { global } from '@angular/compiler/src/util';
import { catchError, finalize } from 'rxjs/operators';
import { forkJoin, Observable, of } from 'rxjs';
import * as moment from 'moment';
import { CommonCategoryApi, KpiRmApi } from '../../apis';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { formatNumber } from '@angular/common';
import { FileService } from 'src/app/core/services/file.service';
import { cleanDataForm } from 'src/app/core/utils/function';
import { Utils } from 'src/app/core/utils/utils';

@Component({
  selector: 'app-kpi-rm-list',
  templateUrl: './kpi-rm-list.component.html',
  styleUrls: ['./kpi-rm-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class KpiRmListComponent extends BaseComponent implements OnInit {
  commonData = {
    listDivision: [],
    listTitleGroup: [],
    listLevelRM: [],
    listBranches: [],
    listYears: [],
    listMonths: [],
    minDate: new Date(),
    maxDate: new Date(),
    lasterDateBusiness: new Date(),
    isRM: false,
    yearConfig: null ,
  };
  listData = [];
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize,
  };
  paramSearch = {
    pageSize: global?.userConfig?.pageSize,
    pageNumber: 0,
    branches: [],
    scope: Scopes.VIEW,
    rsId: '',
  };
  prevParams: any;
  formSearch = this.fb.group({
    rmCode: '',
    hrsCode: '',
    rmName: '',
    rmLevelId: '',
    bizLine: '',
    groupTitleId: '',
    past: 1,
    period: 0,
    yearRm: '',
    monthRm: '',
    businessDate: '',
  });
  objFunctionKpi: any;
  itemRow = [{}, {}];
  listDataSme = [];
  properties = [];
  isSme = false;
  yearConfig: any;
  isArm = false;
  isFilterLoading = false;
  highPrioritySort = [Division.INDIV, Division.SME, Division.CIB];
  today = moment().add(-1, 'day').toDate(); // T-1
  minDate = moment().add(-45, 'day').toDate();

  constructor(
    injector: Injector,
    private api: KpiRmApi,
    private commonCategoryApi: CommonCategoryApi,
    private categoryService: CategoryService,
    private fileService: FileService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.objFunctionKpi = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.KPI_RM}`);

    this.paramSearch.rsId = this.objFunction?.rsId;

    this.prop = this.router.getCurrentNavigation()?.extras?.state;
    this.isLoading = true;
  }

  ngOnInit(): void {
   // this.formSearch.get('businessDate').disable();
   // this.formSearch.get('yearRm').disable();
  //  this.formSearch.get('monthRm').disable();
    this.formSearch.get('bizLine').valueChanges.subscribe((value) => {
      this.getTitleGroupAndLevel();
    });
    this.formSearch.get('groupTitleId').valueChanges.subscribe((value) => {
      const itemTitleGroup = this.commonData.listTitleGroup?.find((item) => item.id === value);
      if (itemTitleGroup) {
        this.commonData.listLevelRM = itemTitleGroup?.levels || [];
        if (this.commonData.listLevelRM.length > 0 && this.commonData.listLevelRM[0]?.id !== '') {
          if (
            this.formSearch.get('bizLine').value === Division.SME ||
            this.formSearch.get('bizLine').value === Division.CIB
          ) {
            this.setDefaultLevelId();
          } else {
            this.setDefaultLevelId();
            if (this.commonData.listLevelRM.length > 1) {
              //this.setDefaultLevelId();
            //  this.commonData.listLevelRM.unshift({ id: '', name: this.fields.all });
            }
          }
         // console.log('common: ',this.commonData);
          this.sessionService.setSessionData(FunctionCode.KPI_RM, this.commonData);
        }
        else if(this.commonData.listLevelRM.length > 0 && this.commonData.listLevelRM[0]?.id === ''
          && this.formSearch.get('bizLine').value === Division.INDIV){
          this.formSearch.get('rmLevelId').setValue(this.commonData.listLevelRM[0]?.id);
        }
      }
    });
    // this.formSearch.get('past').valueChanges.subscribe((value) => {
    //   if (value === 0) {
    //     this.formSearch.get('businessDate').setValue(new Date(this.commonData.lasterDateBusiness));
    //     this.formSearch.get('businessDate').disable({ emitEvent: false });
    //     this.formSearch.get('yearRm').disable({ emitEvent: false });
    //     this.formSearch.get('monthRm').disable({ emitEvent: false });
    //     this.formSearch.get('yearRm').setValue(this.commonData.listYears[0].value);
    //     this.commonData.listMonths = this.commonData.listYears[0].listMonths;
    //     this.formSearch.get('monthRm').setValue(_.last(this.commonData.listMonths)?.value);
    //     this.getTitleGroupAndLevel();
    //   } else {
    //     this.formSearch.get('businessDate').enable({ emitEvent: false });
    //     this.formSearch.get('yearRm').enable({ emitEvent: false });
    //     this.formSearch.get('monthRm').enable({ emitEvent: false });
    //     this.getLastDateDataByMonth();
    //     this.getTitleGroupAndLevel();
    //   }
    // });
    this.formSearch.get('monthRm').valueChanges.subscribe((value) => {
      if (value) {
        this.getLastDateDataByMonth();
      }
    });
    this.formSearch.get('yearRm').valueChanges.subscribe((value) => {
      if (value) {
        this.commonData.listMonths = _.find(this.commonData.listYears, (item) => item.value === value)?.listMonths;
        this.formSearch.get('monthRm').setValue(_.last(this.commonData.listMonths)?.value);
      }
    });
    this.formSearch.get('period').valueChanges.subscribe((value) => {
      if (!value && this.isLoading) {
        return;
      }

      //this.formSearch.controls.businessDate.setValue(this.today);
      // console.log('value=',value);
      // console.log('bussinessDate=',this.commonData.lasterDateBusiness);
      this.formSearch.controls.businessDate.setValue(new Date(this.commonData.lasterDateBusiness));
      this.formSearch.get('yearRm').setValue(this.commonData?.listYears[0]?.value);
      this.formSearch.get('monthRm').setValue(_.last(this.commonData?.listYears[0]?.listMonths)?.value);
      this.search(true);
    });
    const prop = this.sessionService.getSessionData(FunctionCode.KPI_RM);
    //const prop = '';
    if (prop) {
      this.commonData = prop;
      this.initData();
      if (!this.prop) {
        this.formSearch.get('yearRm').setValue(this.commonData?.listYears[0]?.value);
        this.formSearch.get('monthRm').setValue(_.last(this.commonData?.listYears[0]?.listMonths)?.value);
      }
    } else {
      const divisionOfUser = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM);
      this.commonData.listDivision = divisionOfUser;
      this.commonData.listDivision = this.commonData.listDivision
        ?.map((item) => {
          return {
            code: item.code,
            name: `${item.code || ''} - ${item.name || ''}`,
            listTitleGroup: {},
          };
        })
        ?.sort((a, b) => {
          if (a.code.toLowerCase() < b.code.toLowerCase()) {
            return -1;
          }
          if (a.code.toLowerCase() > b.code.toLowerCase()) {
            return 1;
          }
          return 0;
        });

      this.sortDivision();

      forkJoin([
        this.commonCategoryApi.getCommonCategory(CommonCategory.KPI_YEARS).pipe(catchError((e) => of(undefined))),
        this.categoryService
          .getBranchesOfUser(this.objFunction?.rsId, Scopes.VIEW)
          .pipe(catchError(() => of(undefined))),
        this.api.getDateRm(0).pipe(catchError(() => of(undefined))),
        this.api.getDateRm(1).pipe(catchError(() => of(undefined))),
        this.api.getCategoryRmByBlock({
          viewRm: true,
          blockCode: this.commonData.listDivision[0]?.code,
          rsId: this.objFunction?.rsId,
          scope: Scopes.VIEW,
          past: this.formSearch.get('past').value,
        }),
      ]).subscribe(([kpiDate, listBranches, dateRm, dateRmMonth, listTitleGroupByDivision]) => {
        const yearConfig = _.get(_.chain(_.get(kpiDate, 'content')).first().value(), 'value') || 0;
        this.commonData.minDate = new Date(
          moment()
            .add(-(yearConfig > 0 ? yearConfig : 1), 'year')
            .startOf('year')
            .valueOf()
        );
        this.commonData.maxDate = new Date(moment().endOf('year').valueOf());
        this.commonData.lasterDateBusiness = dateRm;
        this.commonData.yearConfig = yearConfig;
        this.initYears(yearConfig);
        this.commonData.listBranches = listBranches || [];
        this.commonData.isRM = _.isEmpty(listBranches);

        this.paramSearch.branches = listBranches?.map((branch) => branch.code) || [];
        listTitleGroupByDivision?.forEach((item) => {
          item.name = `${item.blockCode} - ${item.name}`;
        });
        this.commonData.listDivision[0].listTitleGroup[this.formSearch.controls.past.value] =
          listTitleGroupByDivision || [];
        this.commonData.listTitleGroup =
          this.commonData.listDivision[0].listTitleGroup[this.formSearch.controls.past.value];
        this.setListLevelRM();
        this.initData();
        this.sessionService.setSessionData(FunctionCode.KPI_RM, this.commonData);
      });
    }
  }

  initData() {
    // console.log('this.commonData.listDivision[: ',this.commonData.listDivision);
    if (this.prop) {
      let businessDate: Date;
      if (this.prop.period === 1) {
        businessDate = new Date(this.commonData.lasterDateBusiness);
        this.prop.monthRm = moment(this.prop.businessDate).format('MM');
        this.prop.yearRm = moment(this.prop.businessDate).format('YYYY');
      } else {
        businessDate = _.isEmpty(_.trim(this.prop.businessDate))
          ? new Date(this.commonData.lasterDateBusiness)
          : new Date(this.prop.businessDate);
        this.formSearch.get('yearRm').setValue(this.commonData.listYears[0].value);
        this.formSearch.get('monthRm').setValue(_.last(this.commonData.listYears[0].listMonths)?.value);
      }
      if (this.prop.past === 1) {
        this.formSearch.get('businessDate').enable({ emitEvent: false });
        this.formSearch.get('yearRm').enable({ emitEvent: false });
        this.formSearch.get('monthRm').enable({ emitEvent: false });
      }
      delete this.prop.businessDate;
      this.prop.yearRm = +this.prop.yearRm;
      const params = { ...this.prop, businessDate };
      this.formSearch.patchValue(params, { emitEvent: false });
      this.paramSearch.branches = this.prop.branches;
    } else {
      this.formSearch.get('businessDate').setValue(new Date(this.commonData.lasterDateBusiness));
      this.formSearch
        .get('bizLine')
        .setValue(this.commonData.listDivision[0].code, { emitEvent: this.prop ? false : true });
      this.formSearch
        .get('groupTitleId')
        .setValue(this.commonData.listTitleGroup[0]?.id, { emitEvent: this.prop ? false : true });
      this.setDefaultLevelId(false);
      if (
        this.commonData.listDivision[0].code === Division.SME ||
        this.commonData.listDivision[0].code === Division.CIB
      ) {
        this.formSearch.controls.past.setValue(1);
      }
    }
    this.search(true);
  }

  initYears(range: number) {
    this.commonData.listMonths = [];
    this.commonData.listYears = [];
    const year = moment().add(-1, 'month').year();
    const apis: Observable<any>[] = [];
    for (let i = 0; i < range; i++) {
      if (!_.isEmpty(this.formSearch.get('bizLine').value)) {
        if (
          this.formSearch.get('bizLine').value === Division.SME ||
          this.formSearch.get('bizLine').value === Division.CIB
        ) {
          apis.push(this.api.getDateSme(this.formSearch.get('period').value, year - i,this.formSearch.get('bizLine').value));
          this.commonData.listYears.push({
            value: year - i,
            listMonths: [],
          });
        } else {
          apis.push(this.api.getMonthRm(year - i));
          this.commonData.listYears.push({
            value: year - i,
            listMonths: [],
          });
        }
      }
    }
    if (apis.length > 0) {
      forkJoin(apis).subscribe((data) => {
        for (let i = 0; i < data.length; i++) {
          this.commonData.listYears[i].listMonths = data[i]?.map((month) => {
            return { value: month.trim() };
          });
        }
        this.formSearch.get('yearRm').setValue(this.commonData.listYears[0]?.value);
        this.commonData.listMonths = this.commonData.listYears[0].listMonths || [];
        this.formSearch.get('monthRm').setValue(_.last(this.commonData.listMonths)?.value);
        this.sessionService.setSessionData(FunctionCode.KPI_RM, this.commonData);
      });
    }
  }
  addValueAll(listGroupTitle){
    if(listGroupTitle.length > 1 && listGroupTitle[0].id === ''){
      return;
    }
    let listLevel = [];
    if(listGroupTitle.length > 1){
      listGroupTitle.forEach((item) => {
        if(item?.levels && item.levels.length > 0){
          listLevel = _.uniqBy([ ...listLevel, ...item?.levels]);
          if(item?.levels.length > 1){
            item?.levels.splice(0,0,{
              id: '',
              name: this.fields.all
            });
          }
        }
      });
      if(listLevel.length > 1){
        listLevel.splice(0,0,{
          id: '',
          name: this.fields.all
        });
      }
      listGroupTitle.splice(0,0,{
        id: '',
        name: this.fields.all,
        levels: listLevel
      });
    }
    console.log('title check ', listGroupTitle);
  }

  getTitleGroupAndLevel() {
    const divisionCode = this.formSearch.get('bizLine').value;
    const dataType = this.formSearch.get('past').value;
    if (divisionCode === Division.SME || divisionCode === Division.CIB) {
      this.formSearch.get('period').setValue(1);
    }
    if (divisionCode && dataType?.toString()) {
      const itemDivision = this.commonData.listDivision?.find((item) => item.code === divisionCode);
      if (itemDivision?.listTitleGroup[dataType]) {
        this.commonData.listTitleGroup = itemDivision?.listTitleGroup[dataType] || [];
        if(divisionCode === Division.INDIV){
          this.addValueAll(this.commonData.listTitleGroup);
        }
        this.setTitleGroupAndLevel({});
      } else {
        this.isFilterLoading = true;
        const param = {
          viewRm: true,
          blockCode: divisionCode,
          rsId: this.objFunction?.rsId,
          scope: Scopes.VIEW,
          past: dataType,
        };
        this.api
          .getCategoryRmByBlock(param)
          .pipe(
            finalize(() => {
              this.isFilterLoading = false;
            })
          )
          .subscribe((data) => {
            data?.forEach((item) => {
              item.name = `${item.blockCode} - ${item.name}`;
            });
            itemDivision.listTitleGroup[dataType] = data;
            this.commonData.listTitleGroup = itemDivision?.listTitleGroup[dataType] || [];
            if(divisionCode === Division.INDIV){
              this.addValueAll(this.commonData.listTitleGroup);
            }
            this.setTitleGroupAndLevel({});
            let isAlreadySetLevel = false;
            // vc    console.log('level list: ',this.commonData.listLevelRM);
            if (
              this.commonData.listTitleGroup[0]?.levels?.length > 0 &&
              this.commonData.listTitleGroup[0]?.levels[0]?.id !== ''
            ) {
              if (
                this.formSearch.get('bizLine').value === Division.SME ||
                this.formSearch.get('bizLine').value === Division.CIB
              ) {
                this.setDefaultLevelId();
              } else {
                this.setDefaultLevelId();
                if (this.commonData.listLevelRM.length > 1) {
                  //this.setDefaultLevelId();
               //   this.commonData.listTitleGroup[0]?.levels?.unshift({ id: '', name: this.fields.all });

                }
              }
              isAlreadySetLevel = true;
            }
            else if(this.commonData.listLevelRM.length > 1 && this.commonData.listLevelRM[0]?.id === ''
              && this.formSearch.get('bizLine').value === Division.INDIV){
              this.formSearch.get('rmLevelId').setValue(this.commonData.listLevelRM[0]?.id);
            }
            this.setTitleGroupAndLevel({ setLevel: isAlreadySetLevel ? false : true });
          });
      }
    }
  }

  getLastDateDataByMonth() {
    const month = this.commonData.listMonths?.find((i) => i.value === this.formSearch.get('monthRm').value);
    if (month && !month?.lastDataDate) {
      const strDate = `${this.formSearch.get('yearRm').value}/${this.formSearch.get('monthRm').value}/01`;
      const lastDateInMonth = moment(strDate).endOf('month').format('YYYYMMDD');
      this.api.checkDataInMonth(lastDateInMonth, this.formSearch.get('bizLine').value).subscribe((res) => {
        month.lastDataDate = res;
      });
    }
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.pageNumber = pageInfo.offset;
    this.search(false);
  }

  search(isSearch: boolean) {
    this.isLoading = true;
    let params: any;
    if (isSearch) {
      this.paramSearch.pageNumber = 0;
      const dataForm = cleanDataForm(this.formSearch);
      params = { ...this.paramSearch, ...dataForm };
      if (this.formSearch.get('period').value === 1) {
        const strDate = `${this.formSearch.get('yearRm').value}/${this.formSearch.get('monthRm').value}/01`;
        params.businessDate = moment(strDate).endOf('month').format('YYYY-MM-DD');
      } else {
        params.businessDate = moment(params.businessDate).format('YYYY-MM-DD');
      }
      delete params.monthRm;
      delete params.yearRm;
    } else {
      params = { ...this.prevParams, pageNumber: this.paramSearch.pageNumber };
    }
    let apiSearch: Observable<any>;
    if (params.bizLine === Division.SME || params.bizLine === Division.CIB) {
      params.rmTitleCode = this.commonData.listTitleGroup?.find((item) => item.id === params.groupTitleId)?.code || '';
      params.rmLevelCode = this.commonData.listLevelRM?.find((item) => item.id === params.rmLevelId)?.code || '';
      params.rmTitleId = this.formSearch.controls.groupTitleId.value;
      params.blockCode = this.formSearch.controls.bizLine.value;
      apiSearch = this.formSearch.get('period').value === 0 ? this.api.searchDataSmeDaily(params) : this.api.searchDataSme(params);

      if (this.formSearch.get('period').value === 0) {
        params.rm = 1;
      }

      this.isSme = true;
    } else {
      apiSearch = this.api.searchKpiRmProcess(params);
      this.isSme = false;
    }

    apiSearch.subscribe(
      (data) => {
        if (params.bizLine == 'SME' || params.bizLine == 'CIB') {
          this.isArm = data.isArm;
          const body = this.formSearch.get('period').value === 0 ? data : data?.page;
          this.properties = body?.content?.map((item) => item.properties)[0] || [];
          this.listDataSme = body?.content || [];
          this.pageable.totalElements = body?.totalElements;
          this.pageable.totalPages = body?.totalPages;
        } else {
          this.listData = data?.content || [];
          this.pageable.totalElements = data.totalElements;
          this.pageable.totalPages = data.totalPages;
        }

        this.pageable.currentPage = this.paramSearch.pageNumber;
        this.prevParams = params;
        this.isLoading = false;
        const timer = setTimeout(() => {
          this.table?.recalculate();
          clearTimeout(timer);
        }, 100);
      },
      () => {
        this.listData = [];
        this.pageable = {
          totalElements: 0,
          totalPages: 0,
          currentPage: 0,
          size: global?.userConfig?.pageSize,
        };
        this.isLoading = false;
      }
    );
  }

  getValue(row, key) {
    return _.get(row, key);
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      if (this.formSearch.get('bizLine').value === 'SME' || this.formSearch.get('bizLine').value === 'CIB') {
        this.router.navigate([this.router.url, 'detail-sme'], {
          skipLocationChange: true,
          queryParams: { dataKpiRm: JSON.stringify(item), isArm: this.isArm },
          state: this.prevParams,
        });
      } else {
        this.router.navigate([this.router.url, 'detail', item?.id], { state: this.prevParams });
      }
    }
  }

  exportFile() {
    if (!this.maxExportExcel) {
      return;
    }
    if (+this.pageable?.totalElements <= 0) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    if (_.lt(+this.maxExportExcel, +this.pageable?.totalElements)) {
      this.translate.get('notificationMessage.DATA_EXCEL_LARGE', { number: this.maxExportExcel }).subscribe((res) => {
        this.messageService.warn(res);
      });
      return;
    }
    this.isLoading = true;
    const paramExport = { ...this.prevParams };
    paramExport.pageNumber = 0;
    paramExport.pageSize = maxInt32;
    let apiExcel: Observable<any>;
    if (paramExport.bizLine === Division.SME || paramExport.bizLine === Division.CIB) {
      apiExcel = paramExport.period ? this.api.createFileRMSme(paramExport) : this.api.createFileSmeByDay(paramExport);

      if (!paramExport.period) {
        paramExport.rm = 1;
      }

    } else {
      apiExcel = this.api.createFileRM(paramExport);
    }
    apiExcel.subscribe(
      (res) => {
        if (res) {
          this.download(res);
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(_.get(this.notificationMessage, 'export_error'));
        this.isLoading = false;
      }
    );
  }

  download(fileId: string) {
    this.fileService.downloadFile(fileId, 'danh sach kpi-rm.xlsx').subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }

  formatValue(param) {
    if (param !== undefined) {
      return formatNumber(param, 'en', '0.0-2');
    }
    return;
  }

  getLastDateByMonth(month) {
    return this.commonData.listMonths?.find((item) => item.value === month)?.lastDataDate;
  }

  isDataTypeMoment() {
    return (
      this.objFunctionKpi?.scopes.includes('VIEW_KPI_BY_DAY') ||
      this.objFunctionKpi?.scopes.includes('VIEW_KPI_BY_MONTH')
    );
  }

  isDataTypeHistory() {
    return (
      this.objFunctionKpi?.scopes.includes('VIEW_HISTORY_KPI_BY_DAY') ||
      this.objFunctionKpi?.scopes.includes('VIEW_HISTORY_KPI_BY_MONTH')
    );
  }

  isPeriodDay() {
    return (
      this.objFunctionKpi?.scopes.includes('VIEW_KPI_BY_DAY') ||
      this.objFunctionKpi?.scopes.includes('VIEW_HISTORY_KPI_BY_DAY')
    );
  }

  isPeriodMonth() {
    return (
      this.objFunctionKpi?.scopes.includes('VIEW_KPI_BY_MONTH') ||
      this.objFunctionKpi?.scopes.includes('VIEW_HISTORY_KPI_BY_MONTH')
    );
  }

  ngAfterViewInit() {
    this.formSearch.controls.bizLine.valueChanges.subscribe((value) => {
      this.initYears(this.commonData.yearConfig);
      if (value === Division.SME || value === Division.CIB) {
        this.formSearch.get('businessDate').enable();
        this.formSearch.get('yearRm').enable();
        this.formSearch.get('monthRm').enable();
        this.formSearch.controls.past.setValue(1);
      }
    });
  }

  hiddenTypeData(value) {
    if (value === 'SME' || value === 'CIB') {
      return false;
    } else {
      return true;
    }
  }

  setListLevelRM(): void {
    const { listTitleGroup, isRM } = this.commonData;
    let result = [];
    const groupTitleId = this.formSearch.get('groupTitleId').value;
    const titleIndex = listTitleGroup.findIndex((item) => groupTitleId === item.id);
    if (titleIndex > -1) {
      result = listTitleGroup[titleIndex]?.levels || [];
    } else {
      result = this.commonData.listTitleGroup[0]?.levels || [];
    }

    this.commonData.listLevelRM = result;
  }

  setTitleGroupAndLevel({ setLevel = true }): void {
    this.setListLevelRM();
    const divisionList = this.commonData.listDivision[0];
    const listTitleGroup =  divisionList?.listTitleGroup[this.formSearch.controls.past.value];
 //   const listTitleGroup =  this.commonData.listDivision[0].listTitleGroup[0];
    const listLevelRM = listTitleGroup[0]?.levels;
    const { isRM } = this.commonData;
    let defaultData = {
      groupTitleId: listTitleGroup[0]?.id,
      rmLevelId: listLevelRM[0]?.id,
    };

    if (isRM && this.formSearch.get('bizLine').value !== Division.INDIV) {
      const divisionOfUser = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM)[0];
      const title = listTitleGroup.find((item) => divisionOfUser.titleId === item.id);

      if (title) {
        defaultData.groupTitleId = title.id;
      }

      const level = listLevelRM.find((item) => divisionOfUser.levelId === item.id);
      if (level) {
        defaultData.rmLevelId = level.id;
      }
    }

   // this.formSearch.get('groupTitleId').setValue(defaultData.groupTitleId, { emitEvent: false });
    this.formSearch.get('groupTitleId').setValue(defaultData.groupTitleId);
    if (setLevel) this.formSearch.get('rmLevelId').setValue(defaultData.rmLevelId, { emitEvent: false });

    this.sessionService.setSessionData(FunctionCode.KPI_RM, this.commonData);
  }

  setDefaultLevelId(emitEvent = true): void {
    const { isRM, listTitleGroup } = this.commonData;
    let result = null;

    this.setListLevelRM();

    if (isRM) {
      const groupTitleId = this.formSearch.get('groupTitleId').value;
      const level = this.commonData.listLevelRM.find((item) => groupTitleId === item.titleGroupId);

      if (level) result = level.id;
    } else {
      result = this.commonData.listLevelRM[0]?.id;
    }

    this.formSearch.get('rmLevelId').setValue(result);
  }

  sortDivision(): void {
    const currentDisivionList: any[] = _.cloneDeep(this.commonData.listDivision);
    const result = [];
    this.highPrioritySort.forEach(code => {
      const index = currentDisivionList.findIndex(item => item.code === code);
      if (index > -1) {
        result.push(currentDisivionList[index]);
        currentDisivionList.splice(index, 1);
      }
    });

    result.push(...currentDisivionList);
    this.commonData.listDivision = result;
  }
}
