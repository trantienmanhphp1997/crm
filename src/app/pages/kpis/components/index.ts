import { StandardKpiCBQLComponent } from './standard-kpi-cbql/standard-kpi-cbql.component';

export { KpiLeftViewComponent } from './kpi-left-view/kpi-left-view.component';
export { KpiManagerByDayComponent } from './kpi-manager-by-day/kpi-manager-by-day.component';
export { DetailKpiManagerByDayComponent } from './detail-kpi-manager-by-day/detail-kpi-manager-by-day.component';
export { KpiTargetTableComponent } from './kpi-target-table/kpi-target-table.component';
export { KpiRmListComponent } from './kpi-rm-list/kpi-rm-list.component';
export { KpiRmDetailComponent } from './kpi-rm-detail/kpi-rm-detail.component';
export { KpiCategoryComponent } from './kpi-category/kpi-category.component';
export { KpiCategoryCreateComponent } from './kpi-category-create/kpi-category-create.component';
export { KpiCategoryUpdateComponent } from './kpi-category-update/kpi-category-update.component';
export { KpiBranchTimeComponent } from './kpi-branch-time/kpi-branch-time.component';
export { KpiBranchTimeCreateComponent } from './kpi-branch-time-create/kpi-branch-time-create.component';
export { KpiRegionFactorComponent } from './kpi-region-factor/kpi-region-factor.component';
export { KpiRegionFactorCreateComponent } from './kpi-region-factor-create/kpi-region-factor-create.component';
export { KpiRegionFactorUpdateComponent } from './kpi-region-factor-update/kpi-region-factor-update.component';
export { KpiRmTimeFactorComponent } from './kpi-rm-time-factor/kpi-rm-time-factor.component';
export { KpiRmTimeFactorCreateComponent } from './kpi-rm-time-factor-create/kpi-rm-time-factor-create.component';
export { KpiRmTimeFactorUpdateComponent } from './kpi-rm-time-factor-update/kpi-rm-time-factor-update.component';
export { KpiCategoryByTitleComponent } from './kpi-category-by-title/kpi-category-by-title.component';
export { KpiCategoryByTitleCreateComponent } from './kpi-category-by-title-create/kpi-category-by-title-create.component';
export { KpiCategoryByTitleUpdateComponent } from './kpi-category-by-title-update/kpi-category-by-title-update.component';
export { KpiStandardRmComponent } from './kpi-standard-rm/kpi-standard-rm.component';
export { KpiStandardCbqlComponent } from './kpi-standard-cbql/kpi-standard-cbql.component';
export { KpiStandardCbqlImportComponent } from './kpi-standard-cbql-import/kpi-standard-cbql-import.component';
export { KpiStandardCbqlViewComponent } from './kpi-standard-cbql-view/kpi-standard-cbql-view.component';
export { KpiStandardCbqlDetailComponent } from './kpi-standard-cbql-detail/kpi-standard-cbql-detail.component';
export { KpiAssignmentRmSearchComponent } from './kpi-assignment-rm-search/kpi-assignment-rm-search.component';
export { KpiAssignmentCbqlSearchComponent } from './kpi-assignment-cbql-search/kpi-assignment-cbql-search.component';
export { KpiAssignmentRmUpdateComponent } from './kpi-assignment-rm-update/kpi-assignment-rm-update.component';
export { KpiAssignmentCbqlUpdateComponent } from './kpi-assignment-cbql-update/kpi-assignment-cbql-update.component';
export { KpiAssignmentReceiveComponent } from './kpi-assignment-receive/kpi-assignment-receive.component';
export { StandardKpiCBQLComponent } from './standard-kpi-cbql/standard-kpi-cbql.component';
export { StandardKpiCBQLImportComponent } from './standard-kpi-cbql-import/standard-kpi-cbql-import.component';
export { KpiRmDetailSmeComponent } from './kpi-rm-detail-sme/kpi-rm-detail-sme.component';
export { KpiManagerDetailSmeComponent } from './kpi-manager-detail-sme/kpi-manager-detail-sme.component';
export { TargetSaleConfigCreateComponent } from './target-sale-config-create/target-sale-config-create.component';
export { TargetSaleConfigEditModalComponent } from './target-sale-config-edit-modal/target-sale-config-edit-modal.component';
export { TargetSaleConfigErrorModalComponent } from './target-sale-config-list-error-modal/target-sale-config-error-modal.component';
export { TargetSaleConfigListComponent } from './target-sale-config-list/target-sale-config-list.component';
export { TargetSaleConfigUpdateComponent } from './target-sale-config-update/target-sale-config-update.component';
