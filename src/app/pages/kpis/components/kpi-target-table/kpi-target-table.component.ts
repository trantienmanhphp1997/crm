import { Component, HostBinding, Input, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Utils } from '../../../../core/utils/utils';
import _ from 'lodash';
import { global } from '@angular/compiler/src/util';
import { TranslateService } from '@ngx-translate/core';
import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';
import { KpiManagerApi } from '../../apis';
import { FileService } from 'src/app/core/services/file.service';

@Component({
  selector: 'kpi-target-table',
  templateUrl: './kpi-target-table.component.html',
  styleUrls: ['./kpi-target-table.component.scss'],
})
export class KpiTargetTableComponent implements OnInit {
  constructor(
    private translate: TranslateService,
    private messageService: NotifyMessageService,
    private api: KpiManagerApi,
    private fileService: FileService
  ) {
    this.message = global.messageTable;
    this.translate.get(['fields', 'notificationMessage']).subscribe((result) => {
      this.notificationMessage = _.get(result, 'notificationMessage');
      this.field = _.get(result, 'fields');
    });
  }

  @ViewChild('table') table: DatatableComponent;
  @HostBinding('class.app__right-content') appRightContent = true;
  isLoading: boolean;
  phone_configs: Array<any>;
  email_configs: Array<any>;
  @Input() id: string;
  @Input() models: Array<any>;
  rows: Array<any>;
  @Input() rsid: string;
  @Input() scope: string;
  search_value: string;
  message: any;
  notificationMessage: any;
  field: any;
  products: Array<any>;
  product_values: Array<any>;

  ngOnInit(): void {
    this.convert_data(this.models);
  }

  convert_data(datas: Array<any>) {
    this.products = [];
    let pValues = [];
    if (Utils.isArrayNotEmpty(datas)) {
      var values = _.chain(datas)
        .groupBy((x) => x.groupParentCode)
        .map((item) => item)
        .value();
      // console.log(values)
      if (Utils.isArrayNotEmpty(values)) {
        _.forEach(values, (x) => {
          var parent = _.chain(x).first().value();
          var newParent = {
            productCode: _.get(parent, 'groupParentCode'),
            productName: _.get(parent, 'groupParentName'),
          };
          let v = _.map(x, (y) => ({ ...y }));
          pValues.push(newParent);
          pValues.push(v);
        });
        this.products = _.flattenDeep(pValues);
      }
    }
    this.convert_to_rows(this.products);
  }

  exportFile() {
    if (Utils.isArrayEmpty(this.models)) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    this.isLoading = true;
    this.api.createDetailFile(this.id, this.rsid, this.scope).subscribe(
      (res) => {
        if (Utils.isStringNotEmpty(res)) {
          this.download(res);
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(_.get(this.notificationMessage, 'export_error'));
        this.isLoading = false;
      }
    );
  }

  download(fileId: string) {
    this.fileService.downloadFile(fileId, 'danh sach kpi-cbql.xlsx').subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }

  onTreeAction(event: any) {
    const row = _.get(event, 'row');
    if (row.treeStatus === 'collapsed') {
      row.treeStatus = 'expanded';
    } else {
      row.treeStatus = 'collapsed';
    }
    this.rows = [...this.rows];
  }

  convert_to_rows(datas: Array<any>) {
    let listChildren: Array<any>;
    this.rows = [];
    if (Utils.isArrayNotEmpty(datas)) {
      datas.forEach((item) => {
        if (item.groupParentCode === item.productCode) {
          delete item.groupParentCode;
          listChildren = datas.filter((value) => {
            return value.groupParentCode === item.productCode;
          });
        } else {
          listChildren = datas.filter((value) => {
            return value.groupParentCode === item.productCode;
          });
        }
        item['treeStatus'] = listChildren.length ? 'expanded' : 'disabled';
        this.rows.push(item);
      });
    } else {
      this.rows = [];
    }
  }

  getValue(row, key) {
    return _.get(row, key);
  }
}
