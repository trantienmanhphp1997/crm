import {AfterViewInit, Component, HostBinding, Injector, Input, OnInit, ViewEncapsulation} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { StandardKpiCbqlApi } from '../../apis/standard-kpi-cbql.api';
import {forkJoin, of} from 'rxjs';
import {CommonCategory, Division, FunctionCode, Scopes} from '../../../../core/utils/common-constants';
import {CategoryService} from '../../../system/services/category.service';
import {KpiRmApi} from '../../apis';
import {catchError} from 'rxjs/operators';
import * as _ from 'lodash';
import {formatDate} from '@angular/common';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import {CustomValidators} from '../../../../core/utils/custom-validations';
import {cleanDataForm, validateAllFormFields} from '../../../../core/utils/function';
import {RmApi} from '../../../rm/apis';
import {AppFunction} from '../../../../core/interfaces/app-function.interface';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';


@Component({
  selector: 'app-target-sale-config-edit-modal',
  templateUrl: './target-sale-config-edit-modal.component.html',
  styleUrls: ['./target-sale-config-edit.component.modal.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TargetSaleConfigEditModalComponent extends BaseComponent implements OnInit, AfterViewInit {
  @HostBinding('class.target-sale-config-edit-modal') classEditModal = true;
  @Input() data?: any;
  @Input() commonData?: any;
  @Input() screen?: any;

  formSearch = this.fb.group({
    id: null,
    division: [{value: 'INDIV', disabled: true}],
    rmLevelId: [{value: 'ALL', disabled: true}],
    rmTitleId: [{value: '', disabled: true}],
    startDate: ['', CustomValidators.required],
    endDate: ['', CustomValidators.required],
    product: [{value: 'Casa', disabled: true}],
    value: ['', CustomValidators.required],
    branchCode: [{value: '', disabled: true}],
    isActive: true,
  });
  branchesCode: string[];
  listBranch = [];

  listWeeksData:any = [];
  listMonthsData:any = [];
  prevParams: any = [];
  allotmentValue = 'MONTH';
  ColumnMode = ColumnMode;
  autoAllocation = false;
  public objFunctionTagetSale: AppFunction;
  minDate = new Date();
  flag = false;

  constructor(
    injector: Injector,
    private standardKpiCbqlApi: StandardKpiCbqlApi,
    private categoryService: CategoryService,
    private api: KpiRmApi,
    private rmApi: RmApi,
    private modalActive: NgbActiveModal,

  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.objFunctionTagetSale = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.TARGET_SALE_CONFIG}`);
  }

  ngOnInit() {
    console.log('data', this.data);
    this.autoAllocation = this.data.autoAllocation;
    this.allotmentValue = this.data.allotmentValue;
    this.data.startDate = moment(this.data.startDate, 'DD/MM/YYYY').toDate();
    this.data.endDate = moment(this.data.endDate, 'DD/MM/YYYY').toDate();
    this.formSearch.patchValue(this.data);
    if (this.screen === 'UPDATE') {
      this.formSearch.controls.startDate.disable();
      this.formSearch.controls.endDate.disable();
    }
    const itemTitleGroup = _.find(this.commonData.listTitleGroup, (item) => item.id === this.formSearch.controls.rmTitleId.value);
    this.commonData.listLevelRM = itemTitleGroup?.levels;
  }

  ngAfterViewInit() {
  }

  closeModal() {
    this.modalActive.close(false);
  }

  save() {
    const data = this.formSearch.getRawValue();
    // data.startDate = formatDate(data.startDate, 'dd/MM/yyyy', 'en');
    // data.endDate = formatDate(data.endDate, 'dd/MM/yyyy', 'en');
    data.autoAllocation = this.autoAllocation;
    data.allotmentValue = this.allotmentValue;
    this.modalActive.close(data);
  }

  checkRequired() {
    return !(this.formSearch.status === 'VALID' && !this.flag);
  }

  onChangeValue(event) {
    this.flag = event.value === '-' || event.value < 1 || event.value === null;
  }

}
