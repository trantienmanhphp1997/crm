import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { forkJoin, Observable, of } from 'rxjs';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Utils } from 'src/app/core/utils/utils';
import { CommonCategoryApi, KpiMonthApi, KpiRmApi } from '../../apis';
import * as _ from 'lodash';
import { CommonCategory, FunctionCode, Scopes } from 'src/app/core/utils/common-constants';
import { Table } from 'primeng/table';
import { formatNumber } from '@angular/common';
import { FileService } from 'src/app/core/services/file.service';
import { catchError, finalize } from 'rxjs/operators';

@Component({
  selector: 'app-kpi-rm-detail-sme',
  templateUrl: './kpi-rm-detail-sme.component.html',
  styleUrls: ['./kpi-rm-detail-sme.component.scss'],
})
export class KpiRmDetailSmeComponent extends BaseComponent implements OnInit {
  @ViewChild('tableRmSme') pTableRef: Table;
  isLoading = false;
  customers: any;
  customersInfo: any;
  rowGroupMetadata: any;
  paramKmDetail: any;
  models: Array<any>;
  rows: Array<any>;
  count: any;
  period: any;
  rsId: any;
  dateRmKpi: any;
  expandedRows: {} = {};
  past: any;
  dateMonth: any;
  dataDetail: any;
  isArm: boolean;
  categoryKpiList = [];

  constructor(
    injector: Injector,
    private monthApi: KpiMonthApi,
    private api: KpiRmApi,
    private fileService: FileService,
    private commonCategoryApi: CommonCategoryApi,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.rsId = this.objFunction?.rsId;
    const state = this.router.getCurrentNavigation()?.extras?.state;
    this.period = state?.period;
    this.past = state?.past;
    this.dateMonth = state?.businessDate;
    this.dataDetail = this.route.snapshot.queryParams.dataKpiRm;
    this.dataDetail = JSON.parse(this.dataDetail);
    this.isArm = this.route.snapshot.queryParams.isArm === 'false' ? false : true;
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.customers = this.dataDetail.properties;

    forkJoin(
      [
        this.commonCategoryApi.getCommonCategory(CommonCategory.CATEGORY_KPI).pipe(catchError((e) => of(undefined)))
      ]
    ).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(([categoryKpiList]) => {
      this.categoryKpiList = categoryKpiList?.content || [];

      this.customersInfo = this.dataDetail;
      this.customers = this.customers.filter((item) => Object.keys(item).length !== 0);
      if (document.getElementById('tableRmSme') && this.customers.length === 0) {
        const listClass = document.getElementById('tableRmSme').getElementsByClassName('p-datatable-tbody');
        listClass.item(1).setAttribute('style', 'text-align: center;display: table-caption;');
      }
      const thisRef = this;
      this.customers.forEach(function (car) {
        thisRef.expandedRows[car.kpiItemGroupName] = true;
      });

      this.expandedRows = Object.assign({}, this.expandedRows);
      this.updateRowGroupMetaData();
    })

  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata =  this.customers.reduce((item, currentValue) => {

      const { kpiItemGroupName } = currentValue;
      const list = item[kpiItemGroupName] ? item[kpiItemGroupName].list : [];

      if (!item[kpiItemGroupName]) {
        item[kpiItemGroupName] = {}
      }
      item[kpiItemGroupName].index = 0;

      item[kpiItemGroupName].list = [...list, currentValue];

      return item;

    }, {});


    this.customers = [];

    // sort
    const rowGroup = {};
    this.categoryKpiList.forEach(item => {
      if (this.rowGroupMetadata[item.value]) {
        rowGroup[item.value] = this.rowGroupMetadata[item.value];
      }
    });

    this.rowGroupMetadata = rowGroup;

    Object.keys(this.rowGroupMetadata).forEach((key, index) => {
      const preKey = Object.keys(this.rowGroupMetadata)[index - 1];
      const preGroup: any = this.rowGroupMetadata[preKey];
      this.rowGroupMetadata[key].index = preGroup ? preGroup.index + preGroup.list.length : 0;
      this.rowGroupMetadata[key].list = this.rowGroupMetadata[key].list.map((item, i) => {
        item.index = i + 1;
        return item;
      })

      this.customers.push(...this.rowGroupMetadata[key].list);
    })



    this.ref.detectChanges();
  }

  exportFile() {
    if (_.isEmpty(this.dataDetail)) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    this.isLoading = true;
    let apiExcel: Observable<any>;

    this.dataDetail = { ...this.dataDetail, isArm: this.isArm };

    apiExcel = this.period ? this.api.createFileDetailRMSme(this.dataDetail) : this.api.createFileDetailSmeByDay(this.dataDetail);

    apiExcel.subscribe(
      (res) => {
        if (Utils.isStringNotEmpty(res)) {
          this.download(res);
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      },
      (err) => {
        this.messageService.error(_.get(this.notificationMessage, 'export_error'));
        this.isLoading = false;
      }
    );
  }

  download(fileId: string) {
    this.fileService.downloadFile(fileId, 'chi tiet kpi-RM.xlsx').subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }

  formatValue(param) {
    if (param !== undefined) {
      return formatNumber(param, 'en', '1.0-4');
    }
    return;
  }

  getValue(row, key) {
    return _.get(row, key);
  }
}
