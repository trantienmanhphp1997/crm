import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiStandardCbqlViewComponent } from './kpi-standard-cbql-view.component';

describe('KpiStandardCbqlViewComponent', () => {
  let component: KpiStandardCbqlViewComponent;
  let fixture: ComponentFixture<KpiStandardCbqlViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiStandardCbqlViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiStandardCbqlViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
