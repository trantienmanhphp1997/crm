import { forkJoin, of } from 'rxjs';
import { global } from '@angular/compiler/src/util';
import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';
import { CampaignsService } from 'src/app/pages/campaigns/services/campaigns.service';
import { FunctionCode, Scopes } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { LIST_ALL_KPI_ASSESSMENT, LIST_ALL_KPI_STATUS } from '../../constant';
import { KpiStandardCbqlApi, LevelApi, TitleGroupApi } from '../../apis';

@Component({
  selector: 'app-kpi-standard-cbql-view',
  templateUrl: './kpi-standard-cbql-view.component.html',
  styleUrls: ['./kpi-standard-cbql-view.component.scss'],
})
export class KpiStandardCbqlViewComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listData = [];
  listBlock = [];
  listKpiStatus = LIST_ALL_KPI_STATUS;
  listKpiAssessment = LIST_ALL_KPI_ASSESSMENT;
  formSearch = this.fb.group({
    rmCode: '',
    rmName: '',
    blockCode: '',
    branchCode: '',
    titleId: '',
    levelId: '',
  });
  paramSearch = {
    pageSize: global.userConfig.pageSize,
    pageNumber: 0,
  };
  prevParams: any;
  pageable: Pageable;
  prop: any;
  listGroup = [];
  listLevel = [];
  listBranches = [];

  constructor(
    injector: Injector,
    private campaignService: CampaignsService,
    private kpiStandardCbqlApi: KpiStandardCbqlApi,
    private titleGroupApi: TitleGroupApi,
    private rmLevelApi: LevelApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.VIEW_KPI_STANDARD_CBQL}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.prop = this.sessionService.getSessionData(FunctionCode.VIEW_KPI_STANDARD_CBQL);
    if (this.prop) {
      this.prevParams = { ...this.prop?.prevParams };
      this.paramSearch.pageNumber = this.prevParams?.pageNumber;
      this.formSearch.patchValue(this.prevParams);
      this.listBlock = this.prop?.listBlock || [];
      this.listBranches = this.prop?.listBranches || [];
      this.listGroup = this.prop?.listGroup || [];
      this.listLevel = this.prop?.listLevel || [];
      this.search();
    } else {
      forkJoin([
        this.kpiStandardCbqlApi.getBranchesOfUser(_.get(this.objFunction, 'rsId'), Scopes.VIEW).pipe(catchError(() => of(undefined))),
        this.campaignService.getBlockMapping().pipe(catchError((e) => of(undefined))),
      ]).subscribe(async ([resBranches, listBlock]) => {
        // map list branch
        const listBranches = resBranches?.map((item) => {
          return { code: item.code, displayName: item.code + ' - ' + item.name };
        });
        this.listBranches = [{ code: '', displayName: this.fields.all }, ...listBranches];

        // map list block
        this.listBlock =
          listBlock?.content?.map((item) => {
            return { code: item.code, name: item.code + ' - ' + item.name };
          }) || [];

        // map list level & title
        let data;
        if (this.listBlock.length) {
          data = await this.getGroupAndLevelBlockCode(this.listBlock[0].code);
        }
        this.listGroup = this.mapListGroup(data.listGroup);
        this.listLevel = data.listLevel;

        // Set default value
        const objectData = {
          blockCode: this.listBlock?.[0]?.code,
          titleId: this.listGroup?.[0]?.id,
          levelId: this.listLevel?.[0]?.id,
        };
        this.formSearch.setValue({ ...this.formSearch.value, ...objectData });

        this.prop = {
          listBlock: this.listBlock,
          listBranches: this.listBranches,
          listGroup: this.listGroup,
          listLevel: this.listLevel,
        };

        this.sessionService.setSessionData(FunctionCode.VIEW_KPI_STANDARD_CBQL, this.prop);
        this.search(true);
      });
    }
  }

  getGroupAndLevelBlockCode = async (code) => {
    let listGroup = [];
    let listLevel = [];
    listGroup =
      (
        await this.titleGroupApi
          .searchGroupByBlockCode(code)
          .pipe(catchError((e) => of(undefined)))
          .toPromise()
      )?.content?.filter((item) => item.isManagerGroup) || [];
    if (listGroup?.length) {
      listLevel =
        (
          await this.rmLevelApi
            .searchLevelByGroupCode(listGroup[0].id, code)
            .pipe(catchError((e) => of(undefined)))
            .toPromise()
        )?.content || [];
    }
    console.log(listGroup, listLevel);

    return { listGroup, listLevel };
  };

  async onChangeBlockCode() {
    const blockCode = this.formSearch.value.blockCode;
    const data = await this.getGroupAndLevelBlockCode(blockCode);
    this.listGroup = this.mapListGroup(data.listGroup);
    this.listLevel = data.listLevel;
    // Re-set value titleId, and levelId
    const objectData = {
      titleId: this.listGroup?.[0]?.id || '',
      levelId: this.listLevel?.[0]?.id || '',
    };
    this.formSearch.setValue({ ...this.formSearch.value, ...objectData });
  }

  async onChangeGroup() {
    const blockCode = this.formSearch.value.blockCode;
    const titleId = this.formSearch.value.titleId;
    this.listLevel =
      (
        await this.rmLevelApi
          .searchLevelByGroupCode(titleId, blockCode)
          .pipe(catchError((e) => of(undefined)))
          .toPromise()
      )?.content || [];
    // Re-set value titleId, levelId,
    const objectData = {
      titleId,
      levelId: this.listLevel?.[0]?.id || '',
    };
    this.formSearch.setValue({ ...this.formSearch.value, ...objectData });
  }

  onChangeLevel() {
    const levelId = this.formSearch.value.levelId;
    // Re-set value levelId
    const objectData = {
      levelId,
    };
    this.formSearch.setValue({ ...this.formSearch.value, ...objectData });
  }

  mapListGroup = (listGroup) => {
    return (
      listGroup?.map((item) => {
        return { ...item, name: item.blockCode + ' - ' + item.name };
      }) || []
    );
  };

  search(isSearch?: boolean) {
    let params: any = {};
    if (isSearch) {
      this.paramSearch.pageNumber = 0;
      const formData = this.formSearch.value;
      params = {
        rmCode: formData.rmCode,
        rmName: formData.rmName,
        blockCode: formData.blockCode,
        branchCodes: [formData.branchCode],
        titleId: formData.titleId,
        levelId: formData.levelId,
      };
    } else {
      if (!this.prevParams) {
        return;
      }
      params = this.prevParams;
    }
    Object.keys(this.paramSearch).forEach((key) => {
      params[key] = this.paramSearch[key];
    });

    params.rsId = _.get(this.objFunction, 'rsId'),
      params.scope = Scopes.VIEW

    // Move these line out of block success cause server throw error 500 when search empty list
    // ----------
    this.prevParams = params;
    this.prop.prevParams = params;
    this.prop.listGroup = this.listGroup;
    this.prop.listLevel = this.listLevel;
    this.sessionService.setSessionData(FunctionCode.VIEW_KPI_STANDARD_CBQL, this.prop);
    // ----------

    this.isLoading = true;

    this.kpiStandardCbqlApi.searchCbql(params).subscribe(
      (result) => {
        if (result) {
          this.listData =
            result?.content?.map((item) => {
              return {
                ...item,
              };
            }) || [];
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: global.userConfig.pageSize,
          };
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      const item = event.row;
      const formData = this.formSearch.value;
      const block = this.listBlock.find((block) => block.code === formData.blockCode);
      const level = this.listLevel.find((level) => level.id === formData.levelId);
      const title = this.listGroup.find((group) => group.id === formData.titleId);
      this.sessionService.setSessionData(FunctionCode.KPI_STANDARD_CBQL_DETAIL, { ...item, block, level, title });
      this.router.navigate([this.router.url, 'detail', item.hrisCode], {
        skipLocationChange: true,
      });
    }
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.pageNumber = pageInfo.offset;
    this.search(false);
  }
}
