import {forkJoin, of} from 'rxjs';
import {global} from '@angular/compiler/src/util';
import {Component, Injector, OnInit} from '@angular/core';
import {BaseComponent} from 'src/app/core/components/base.component';
import {Pageable} from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';
import {cleanDataForm} from 'src/app/core/utils/function';
import {CampaignsService} from 'src/app/pages/campaigns/services/campaigns.service';
import {FunctionCode} from 'src/app/core/utils/common-constants';
import {catchError} from 'rxjs/operators';
import {CLIENT_DATE_FORMAT, LIST_ALL_KPI_ASSESSMENT, LIST_ALL_KPI_STATUS, SERVER_DATE_FORMAT} from '../../constant';
import {KpiRegionApi} from '../../apis/kpi-region.api';
import * as moment from 'moment';

@Component({
	selector: 'app-kpi-region-factor',
	templateUrl: './kpi-region-factor.component.html',
	styleUrls: ['./kpi-region-factor.component.scss'],
})
export class KpiRegionFactorComponent extends BaseComponent implements OnInit {
	isLoading = false;
	listData = [];
	kpiCategoryOutputs = [];
	listBlock = [];
	listKpiStatus = LIST_ALL_KPI_STATUS;
	listKpiAssessment = LIST_ALL_KPI_ASSESSMENT;
	formSearch = this.fb.group({
		blockCode: '', // Ma khoi
		efficientDate: '', // Ngay bat dau
		expireDate: '', // Ngay ket thuc
	});
	paramSearch = {
		pageSize: global.userConfig.pageSize,
		pageNumber: 0,
	};
	prevParams: any;
	pageable: Pageable;
	prop: any;
	maxEfficientDate: any;
	minExpireDate: any;

	constructor(injector: Injector, private campaignService: CampaignsService, private kpiRegionApi: KpiRegionApi) {
		super(injector);
		this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.KPI_FACTOR_AREA}`);
		this.isLoading = true;
	}

	ngOnInit(): void {
		this.prop = this.sessionService.getSessionData(FunctionCode.KPI_FACTOR_AREA);
		this.maxEfficientDate = '';
		this.minExpireDate = '';
		if (this.prop) {
			this.prevParams = {...this.prop?.prevParams};
			this.paramSearch.pageNumber = this.prevParams?.pageNumber;
			this.formSearch.patchValue(this.prevParams);
			this.kpiCategoryOutputs = this.prop?.kpiCategoryOutputs || [];
			this.listBlock = this.prop?.listBlock || [];
			this.search();
		} else {
			forkJoin([this.campaignService.getBlockMapping().pipe(catchError((e) => of(undefined)))]).subscribe(
				([listBlock]) => {
					// map list block
					const blockTypes =
						listBlock?.content.map((item) => {
							return {code: item.code, name: item.code + ' - ' + item.name};
						}) || [];
					this.listBlock = [{code: '', name: 'Tất cả'}, ...blockTypes];

					this.prop = {
						listBlock: this.listBlock,
						kpiCategoryOutputs: this.kpiCategoryOutputs,
					};

					this.formSearch.controls.efficientDate.valueChanges.subscribe((value) => {
						this.minExpireDate = new Date(moment(value).startOf('day').valueOf());
					});
					this.formSearch.controls.expireDate.valueChanges.subscribe((value) => {
						this.maxEfficientDate = new Date(moment(value).startOf('day').valueOf());
					});

					this.sessionService.setSessionData(FunctionCode.KPI_FACTOR_AREA, this.prop);
					this.search(true);
				}
			);
		}
	}

	onChangeEndDate = (event) => {
		const endOfMonth = new Date(moment(event).endOf('month').valueOf());
		this.formSearch.controls?.expireDate?.setValue(endOfMonth);
	};

	search(isSearch?: boolean) {
		this.isLoading = true;
		let params: any = {};
		if (isSearch) {
			this.paramSearch.pageNumber = 0;
			params = cleanDataForm(this.formSearch);
		} else {
			if (!this.prevParams) {
				return;
			}
			params = this.prevParams;
		}
		Object.keys(this.paramSearch).forEach((key) => {
			params[key] = this.paramSearch[key];
		});

		// remove fields date empty or format
		if (params.efficientDate) {
			params.efficientDate = moment(params.efficientDate).format(SERVER_DATE_FORMAT);
		} else {
			delete params.efficientDate;
		}
		if (params.expireDate) {
			params.expireDate = moment(params.expireDate).format(SERVER_DATE_FORMAT);
		} else {
			delete params.expireDate;
		}

		this.kpiRegionApi.searchKpiRegion(params).subscribe(
			(result) => {
				if (result) {
					this.prevParams = params;
					this.prop.prevParams = params;
					this.listData =
						result?.content?.map((item) => {
							const efficientDate = item?.efficientDate
								? moment(item.efficientDate, SERVER_DATE_FORMAT).format(CLIENT_DATE_FORMAT)
								: '';
							const expireDate = item?.expireDate
								? moment(item.expireDate, SERVER_DATE_FORMAT).format(CLIENT_DATE_FORMAT)
								: '';
							return {
								...item,
								efficientDate,
								expireDate,
							};
						}) || [];
					this.pageable = {
						totalElements: result.totalElements,
						totalPages: result.totalPages,
						currentPage: result.number,
						size: global.userConfig.pageSize,
					};
				}
				this.sessionService.setSessionData(FunctionCode.KPI_FACTOR_AREA, this.prop);
				this.isLoading = false;
			},
			() => {
				this.isLoading = false;
			}
		);
	}

	setPage(pageInfo) {
		if (this.isLoading) {
			return;
		}
		this.paramSearch.pageNumber = pageInfo.offset;
		this.search(false);
	}

	create() {
		this.router.navigate([this.router.url, 'create']);
	}

	update(item) {
		this.router.navigate([this.router.url, 'update', item.kpiFactorSetId]);
	}
}
