import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiRegionFactorComponent } from './kpi-region-factor.component';

describe('KpiRegionFactorComponent', () => {
  let component: KpiRegionFactorComponent;
  let fixture: ComponentFixture<KpiRegionFactorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiRegionFactorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiRegionFactorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
