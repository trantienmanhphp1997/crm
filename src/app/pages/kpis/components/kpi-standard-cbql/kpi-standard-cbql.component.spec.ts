import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiStandardCbqlComponent } from './kpi-standard-cbql.component';

describe('KpiStandardCbqlComponent', () => {
  let component: KpiStandardCbqlComponent;
  let fixture: ComponentFixture<KpiStandardCbqlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiStandardCbqlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiStandardCbqlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
