import { forkJoin, of } from 'rxjs';
import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { cleanDataForm } from 'src/app/core/utils/function';
import { CampaignsService } from 'src/app/pages/campaigns/services/campaigns.service';
import { FunctionCode } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import {
  CODE_FIRST_6M,
  LIST_ALL_KPI_ASSESSMENT,
  LIST_ALL_KPI_STATUS,
  LIST_MONTH,
  LIST_SIX_MONTH,
  LIST_YEAR,
} from '../../constant';
import * as moment from 'moment';
import { KpiAssignmentApi, KpiStandardCbqlApi, LevelApi, TitleGroupApi } from '../../apis';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-kpi-standard-cbql',
  templateUrl: './kpi-standard-cbql.component.html',
  styleUrls: ['./kpi-standard-cbql.component.scss'],
})
export class KpiStandardCbqlComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listData = [];
  listBlock = [];
  listKpiStatus = LIST_ALL_KPI_STATUS;
  listKpiAssessment = LIST_ALL_KPI_ASSESSMENT;
  formSearch = this.fb.group({
    blockCode: '',
    rmTitleCode: '',
    rmLevelCode: '',
    isMonth: true,
    month: parseInt(moment().format('MM')),
    year: parseInt(moment().format('yyyy')),
    // Extra data
    rmLevelId: '',
    rmTitleId: '',
    hrisCode: '',
  });
  prevParams: any;
  prop: any;
  maxEfficientDate: any;
  minExpireDate: any;
  listYear = LIST_YEAR;
  listMonth = LIST_MONTH;
  listGroup = [];
  listLevel = [];
  listHeaders = [];
  formCreate = this.fb.array([]);
  editing = {};

  constructor(
    injector: Injector,
    private campaignService: CampaignsService,
    private kpiStandardCbqlApi: KpiStandardCbqlApi,
    private titleGroupApi: TitleGroupApi,
    private rmLevelApi: LevelApi,
    private kpiAssignmentApi: KpiAssignmentApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.KPI_STANDARD_CBQL}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.prop = this.sessionService.getSessionData(FunctionCode.KPI_STANDARD_CBQL);
    this.maxEfficientDate = '';
    this.minExpireDate = '';
    if (this.prop) {
      this.prevParams = { ...this.prop?.prevParams };
      this.formSearch.patchValue(this.prevParams);
      if (!this.prevParams.isMonth) {
        this.listMonth = LIST_SIX_MONTH;
        this.formSearch.controls.month?.setValue(CODE_FIRST_6M);
      }
      this.listBlock = this.prop?.listBlock || [];
      this.listLevel = this.prop?.listLevel || [];
      this.listGroup = this.prop?.listGroup || [];
      this.onSearchKpiStandardCbql();
    } else {
      forkJoin([this.campaignService.getBlockMapping().pipe(catchError((e) => of(undefined)))]).subscribe(
        async ([listBlock]) => {
          // map list block
          this.listBlock =
            listBlock?.content?.map((item) => {
              return { code: item.code, name: item.code + ' - ' + item.name };
            }) || [];
          let data;
          if (this.listBlock.length) {
            data = await this.getGroupAndLevelBlockCode(this.listBlock[0].code);
          }
          this.listGroup = this.mapListGroup(data.listGroup);
          this.listLevel = data.listLevel;

          // Set default value
          const objectData = {
            blockCode: this.listBlock?.[0]?.code,
            rmTitleCode: this.listGroup?.[0]?.code,
            rmLevelCode: this.listLevel?.[0]?.code,
            rmLevelId: this.listLevel?.[0]?.id,
            rmTitleId: this.listGroup?.[0]?.id,
          };
          this.formSearch.setValue({ ...this.formSearch.value, ...objectData });

          this.onSearchKpiStandardCbql(true);
          this.isLoading = false;
        }
      );
    }

    this.formSearch.controls.isMonth.valueChanges.subscribe((value) => {
      if (value) {
        this.listMonth = LIST_MONTH;
        this.formSearch.controls.month?.setValue(parseInt(moment().format('MM')));
      } else {
        this.listMonth = LIST_SIX_MONTH;
        this.formSearch.controls.month?.setValue(CODE_FIRST_6M);
      }
    });
  }

  onClickButtonSearch = () => {
    this.isLoading = true;
    this.kpiAssignmentApi.getAggregteMonthMax().subscribe(
      (res) => {
        this.isLoading = false;
        const formValue = this.formSearch.value;
        const isNeedCheck = formValue.month >= 1 && formValue.month <= 12;
        if (
          isNeedCheck &&
          (res.year > formValue.year || (res.year == formValue.year && formValue.month <= res.month))
        ) {
          this.confirmService.confirm(this.fields.confirm_aggregte).then((res) => {
            if (res) {
              this.onSearchKpiStandardCbql(true);
            }
          });
        } else {
          this.onSearchKpiStandardCbql(true);
        }
      },
      (e) => {
        this.isLoading = false;
        if (e?.status === 0) {
          this.messageService.error(this.notificationMessage.E001);
          return;
        }
        this.messageService.error(e?.error?.description);
      }
    );
  };

  getGroupAndLevelBlockCode = async (code) => {
    let listGroup = [];
    let listLevel = [];
    listGroup =
      (
        await this.titleGroupApi
          .searchGroupByBlockCode(code)
          .pipe(catchError((e) => of(undefined)))
          .toPromise()
      )?.content?.filter((item) => item.isManagerGroup) || [];
    if (listGroup?.length) {
      listLevel =
        (
          await this.rmLevelApi
            .searchLevelByGroupCode(listGroup[0].id, code)
            .pipe(catchError((e) => of(undefined)))
            .toPromise()
        )?.content || [];
    }
    return { listGroup, listLevel };
  };

  async onChangeBlockCode() {
    const blockCode = this.formSearch.value.blockCode;
    const data = await this.getGroupAndLevelBlockCode(blockCode);
    this.listGroup = this.mapListGroup(data.listGroup);
    this.listLevel = data.listLevel;
    // Re-set value rmTitleCode, rmTitleId, rmLevelId and rmLevelCode
    const objectData = {
      rmTitleCode: this.listGroup?.[0]?.code || '',
      rmLevelCode: this.listLevel?.[0]?.code || '',
      rmLevelId: this.listLevel?.[0]?.id || '',
      rmTitleId: this.listGroup?.[0]?.id || '',
    };
    this.formSearch.setValue({ ...this.formSearch.value, ...objectData });
  }

  async onChangeGroup() {
    const blockCode = this.formSearch.value.blockCode;
    const groupCode = this.formSearch.value.rmTitleCode;
    const groupSelected = this.listGroup.find((group) => group.code === groupCode);
    this.listLevel =
      (
        await this.rmLevelApi
          .searchLevelByGroupCode(groupSelected?.id, blockCode)
          .pipe(catchError((e) => of(undefined)))
          .toPromise()
      )?.content || [];
    // Re-set value rmTitleId, rmLevelCode, rmLevelId
    const objectData = {
      rmTitleId: groupSelected?.id,
      rmLevelCode: this.listLevel?.[0]?.code || '',
      rmLevelId: this.listLevel?.[0]?.id || '',
    };
    this.formSearch.setValue({ ...this.formSearch.value, ...objectData });
  }

  onChangeLevel() {
    const levelCode = this.formSearch.value.rmLevelCode;
    const levelSelected = this.listLevel.find((level) => level.code === levelCode);
    // Re-set value rmLevelId
    const objectData = {
      rmLevelId: levelSelected?.id,
    };
    this.formSearch.setValue({ ...this.formSearch.value, ...objectData });
  }

  mapListGroup = (listGroup) => {
    return (
      listGroup?.map((item) => {
        return { ...item, name: item.blockCode + ' - ' + item.name };
      }) || []
    );
  };

  onSearchKpiStandardCbql(isSearch?: boolean) {
    this.isLoading = true;
    let params: any = {};
    if (isSearch) {
      params = cleanDataForm(this.formSearch);
    } else {
      if (!this.prevParams) {
        return;
      }
      params = this.prevParams;
    }

    let paramsSearch = {
      blockCode: params.blockCode,
      rmTitleCode: params.rmTitleCode,
      rmLevelCode: params.rmLevelCode,
      month: params.month,
      year: params.year,
      hrisCode: params.hrisCode,
    };

    this.kpiStandardCbqlApi.searchKpiStandardCbql(paramsSearch).subscribe(
      (result) => {
        if (result) {
          this.prevParams = params;
          this.prop = {
            ...this.prop,
            prevParams: params,
            listBlock: this.listBlock,
            listGroup: this.listGroup,
            listLevel: this.listLevel,
          };

          this.listData = result.entries;

          if (this.listData.length) {
            let groupByHrisCode = _.chain(this.listData)
              .groupBy((item) => item.hrisCode)
              .map((item) => item)
              .value();
            let listHeaders = groupByHrisCode[0].map((item) => ({
              kpiItemCode: item.kpiItemCode,
              kpiItemName: item.kpiItemName,
            }));

            this.listData = groupByHrisCode;
            this.listHeaders = listHeaders;
            // console.log('groupByHrisCode', groupByHrisCode);
          }
        }
        this.sessionService.setSessionData(FunctionCode.KPI_STANDARD_CBQL, this.prop);
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  isEdit = (rowIndex) => {
    return this.editing[rowIndex];
  };

  getHeader = (item) => {
    return `${item.kpiItemName}\n${item.kpiItemCode}`;
  };

  getValue = (row, kpiItemCode) => {
    return row.find((item) => item.kpiItemCode === kpiItemCode)?.value;
  };

  create() {
    this.router.navigate([this.router.url, 'create']);
  }

  update(row, index) {
    this.formCreate.clear();
    this.editing = { [index]: true };
    row.forEach((cell) => {
      this.formCreate.push(
        this.fb.group({
          ...cell,
          success: true,
        })
      );
    });
  }

  clear() {
    this.formCreate.clear();
    this.editing = {};
  }

  save = () => {
    const formValue = this.formCreate.value;
    // console.log('formValue', formValue);
    let data = formValue;

    this.confirmService.confirm().then((res) => {
      if (res) {
        this.isLoading = true;
        this.kpiStandardCbqlApi.updateKpiStandardCbql(data).subscribe(
          (value) => {
            this.isLoading = false;
            let failed = value?.failed || {};
            let failedKeys = Object.keys(failed);

            if (failedKeys.length) {
              this.messageService.error(this.notificationMessage.error);
              let itemFailed: any = Object.values(failed)[0]; // Cause only one item in case edit

              itemFailed.forEach((element, index) => {
                (this.formCreate.controls[index] as FormGroup)?.controls.success.setValue(element.success);
              });
              // console.log('failed', failed);
            } else {
              this.editing = {};
              this.formCreate.clear();
              this.messageService.success(this.notificationMessage.success);
              this.onSearchKpiStandardCbql();
            }
          },
          (e) => {
            this.isLoading = false;
            if (e?.status === 0) {
              this.messageService.error(this.notificationMessage.E001);
              return;
            }
            this.messageService.error(e?.error?.description);
          }
        );
      }
    });
  };
}
