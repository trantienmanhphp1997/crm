import { forkJoin, of } from 'rxjs';
import { ChangeDetectorRef, Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { CampaignsService } from 'src/app/pages/campaigns/services/campaigns.service';
import { catchError } from 'rxjs/operators';
import {
  KPI_RM_TIME_FACTOR_CREATE_DEFAULT,
  NO_MONTH_TIME_FACTOR,
  NO_WORKING_MONTHS_KPI_TIME_FACTOR,
  SERVER_DATE_FORMAT,
} from '../../constant';
import { KpiRmTimeApi, LevelApi, TitleGroupApi } from '../../apis';
import * as moment from 'moment';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { FormGroup } from '@angular/forms';
import { CommonCategoryService } from 'src/app/core/services/common-category.service';

@Component({
  selector: 'app-kpi-rm-time-factor-update',
  templateUrl: './kpi-rm-time-factor-update.component.html',
  styleUrls: ['./kpi-rm-time-factor-update.component.scss'],
})
export class KpiRmTimeFactorUpdateComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listBlock = [];
  formSearch = this.fb.group({
    blockCode: { value: KPI_RM_TIME_FACTOR_CREATE_DEFAULT.BLOCK_CODE, disabled: true },
    titleCode: { value: KPI_RM_TIME_FACTOR_CREATE_DEFAULT.TITLE, disabled: true },
    levelCode: { value: KPI_RM_TIME_FACTOR_CREATE_DEFAULT.LEVEL, disabled: true },
    configSetName: { value: KPI_RM_TIME_FACTOR_CREATE_DEFAULT.CONFIG_SET_NAME, disabled: true },
    efficientDate: { value: '', disabled: true },
    expireDate: '',
  });
  formCreate = this.fb.array([]);
  listGroup = [];
  listLevel = [];
  listKpiItem = [];
  listGroupByCategoryType = [];
  listKpiItemMapping = [];
  listColumnValue = [];
  minExpireDate: any;
  numberOfMonth: any;
  isValidator = false;
  kpiFactorSetId: any;
  kpiTimeFactorSetDetail: any;
  efficientDate = '';
  index = 0;

  constructor(
    injector: Injector,
    private campaignService: CampaignsService,
    private cd: ChangeDetectorRef,
    private titleGroupApi: TitleGroupApi,
    private rmLevelApi: LevelApi,
    private kpiRmTimeApi: KpiRmTimeApi,
    private commonCategoryService: CommonCategoryService
  ) {
    super(injector);
    this.isLoading = true;
  }

  ngOnInit(): void {
    const kpiFactorSetId = this.route.snapshot.paramMap.get('id');
    forkJoin([
      this.campaignService.getBlockMapping().pipe(catchError((e) => of(undefined))),
      this.titleGroupApi
        .searchGroupByBlockCode(KPI_RM_TIME_FACTOR_CREATE_DEFAULT.BLOCK_CODE)
        .pipe(catchError((e) => of(undefined))),
      this.rmLevelApi
        .searchLevelByGroupCode('', KPI_RM_TIME_FACTOR_CREATE_DEFAULT.BLOCK_CODE)
        .pipe(catchError((e) => of(undefined))),
      this.commonCategoryService
        .getCommonCategory(NO_WORKING_MONTHS_KPI_TIME_FACTOR, NO_MONTH_TIME_FACTOR)
        .pipe(catchError((e) => of(undefined))),
      this.kpiRmTimeApi.getKpiTimeFactorSetDetail({ id: kpiFactorSetId }).pipe(catchError((e) => of(undefined))),
    ]).subscribe(async ([listBlock, listGroup, listLevel, dataMonths, kpiTimeFactorSetDetail]) => {
      console.log('kpiTimeFactorSetDetail', kpiTimeFactorSetDetail);

      // map list block
      this.listBlock =
        listBlock?.content.map((item) => {
          return { ...item, name: item.code + ' - ' + item.name };
        }) || [];
      // map list group
      this.listGroup =
        listGroup?.content.map((item) => {
          return { ...item, name: item.blockCode + ' - ' + item.name };
        }) || [];
      // map list level
      this.listLevel = listLevel?.content || [];

      this.kpiFactorSetId = kpiFactorSetId;
      this.kpiTimeFactorSetDetail = kpiTimeFactorSetDetail;

      // map date to form search
      let efficientDate = kpiTimeFactorSetDetail.efficientDate
        ? new Date(moment(kpiTimeFactorSetDetail.efficientDate, SERVER_DATE_FORMAT).valueOf())
        : '';
      this.efficientDate = kpiTimeFactorSetDetail.efficientDate;
      let expireDate = kpiTimeFactorSetDetail.expireDate
        ? new Date(moment(kpiTimeFactorSetDetail.expireDate, SERVER_DATE_FORMAT).valueOf())
        : '';
      this.minExpireDate = efficientDate;
      this.formSearch.controls.expireDate.setValue(expireDate);
      this.formSearch.controls.efficientDate.setValue(efficientDate);

      this.numberOfMonth = dataMonths?.content[0]?.value || 0;
      for (let index = 1; index <= this.numberOfMonth; index++) {
        this.listColumnValue.push({
          name: index,
          value: `value${index}`,
        });
      }

      this.formSearch.controls.efficientDate.valueChanges.subscribe((value) => {
        this.minExpireDate = new Date(moment(value).startOf('day').valueOf());
      });

      this.isLoading = false;
      this.onSearchKpiTimeFactors();
    });
  }

  onTreeAction(event: any) {
    const row = event.row;

    if (row.treeStatus === 'collapsed') {
      row.treeStatus = 'expanded';
    } else {
      row.treeStatus = 'collapsed';
    }

    this.listKpiItemMapping = [...this.listKpiItemMapping];
    this.cd.detectChanges();
  }

  getCusRequire = (value, rowIndex) =>
    (this.formCreate?.controls[rowIndex] as FormGroup)?.controls?.[value]?.errors?.cusRequired && this.isValidator;

  getErrValue = (value, rowIndex) =>
    (this.formCreate?.controls[rowIndex] as FormGroup)?.controls?.[value]?.errors?.value;

  onSearchKpiTimeFactors = async () => {
    this.formCreate.clear();
    const params = {
      kpiFactorSetId: this.kpiFactorSetId,
      rmLevelCode: KPI_RM_TIME_FACTOR_CREATE_DEFAULT.LEVEL,
    };
    this.isLoading = true;

    let listKpiTimeFactors =
      (await this.kpiRmTimeApi
        .getKpiTimeFactors(params)
        .pipe(catchError((e) => of(undefined)))
        .toPromise())?.sort((a, b) => (a.categoryCode >= b.categoryCode) ? 1 : -1) || [];
    this.listKpiItemMapping = [];
    // group by categoryCode
    let listGroupByCategoryType = Object.values(
      listKpiTimeFactors?.reduce(
        (r, a) => ({
          ...r,
          [a.categoryCode]: [...(r[a.categoryCode] || []), a],
        }),
        {}
      )
    );

    listGroupByCategoryType.forEach((e: Array<any>) => {
      // first level nested
      this.listKpiItemMapping.push({
        idGroup: e[0]?.categoryCode,
        name: e[0]?.categoryName,
        treeStatus: 'expanded',
        parentId: null,
        index: this.index
      });
      this.index = this.index + 1;
      this.formCreate.push(this.fb.group({}));

      e.forEach((i) => {
        // last level nested
        this.listKpiItemMapping.push({
          ...i,
          // idGroup is required cause prevent render error
          idGroup: i.kpiItemRmLevelId,
          name: i.kpiItemName,
          treeStatus: 'disabled',
          parentId: i.categoryCode,
          index: this.index
        });
        this.index = this.index + 1;

        this.formCreate.push(
          this.fb.group({
            value1: this._getDefaultValue(1, i.value1),
            value2: this._getDefaultValue(2, i.value2),
            value3: this._getDefaultValue(3, i.value3),
            value4: this._getDefaultValue(4, i.value4),
            value5: this._getDefaultValue(5, i.value5),
            value6: this._getDefaultValue(6, i.value6),
            value7: this._getDefaultValue(7, i.value7),
            value8: this._getDefaultValue(8, i.value8),
            value9: this._getDefaultValue(9, i.value9),
            value10: this._getDefaultValue(10, i.value10),
            value11: this._getDefaultValue(11, i.value11),
            value12: this._getDefaultValue(12, i.value12),
            // extra data
            kpiItemRmLevelId: i.kpiItemRmLevelId,
            kpiTimeFactorId: i.kpiTimeFactorId,
          })
        );
      });
    });
    this.isLoading = false;
  };

  onChangeEndDate = (event) => {
    const endOfMonth = new Date(moment(event).endOf('month').valueOf());
    this.formSearch.controls?.expireDate?.setValue(endOfMonth);
  };

  _getDefaultValue = (month, value) => {
    let valueFormat = typeof value === 'number' ? value.toString().replace('.', ',') : '';
    if (month <= this.numberOfMonth) {
      return [valueFormat, [CustomValidators.required, CustomValidators.valueRmTime]];
    } else {
      return '';
    }
  };

  back = () => {
    this.location.back();
  };

  save() {
    console.log('this.formCreate', this.formCreate);

    if (this.formCreate.valid) {
      let valueGeneral = this.formSearch.value;

      let kpiFactorDtos = [];
      this.formCreate.value.forEach((element) => {
        if (element.kpiItemRmLevelId) {
          kpiFactorDtos.push({
            kpiItemRmLevelId: element.kpiItemRmLevelId,
            kpiTimeFactorId: element.kpiTimeFactorId,
            value1: parseFloat(element.value1.replace(',', '.')),
            value2: parseFloat(element.value2.replace(',', '.')),
            value3: parseFloat(element.value3.replace(',', '.')),
            value4: parseFloat(element.value4.replace(',', '.')),
            value5: parseFloat(element.value5.replace(',', '.')),
            value6: parseFloat(element.value6.replace(',', '.')),
            value7: parseFloat(element.value7.replace(',', '.')),
            value8: parseFloat(element.value8.replace(',', '.')),
            value9: parseFloat(element.value9.replace(',', '.')),
            value10: parseFloat(element.value10.replace(',', '.')),
            value11: parseFloat(element.value11.replace(',', '.')),
            value12: parseFloat(element.value12.replace(',', '.')),
          });
        }
      });

      let data = {
        kpiTimeFactorSetId: this.kpiTimeFactorSetDetail.kpiFactorSetId,
        blockCode: KPI_RM_TIME_FACTOR_CREATE_DEFAULT.BLOCK_CODE,
        efficientDate: this.efficientDate,
        expireDate: moment(new Date(valueGeneral?.expireDate)).format(SERVER_DATE_FORMAT),
        rmLevelCode: KPI_RM_TIME_FACTOR_CREATE_DEFAULT.LEVEL,
        kpiFactorDtos,
      };

      this.confirmService.confirm().then((res) => {
        if (res) {
          this.isLoading = true;
          this.kpiRmTimeApi.postKpiTimeFactors(data).subscribe(
            () => {
              this.isLoading = false;
              this.back();
              this.messageService.success(this.notificationMessage.success);
            },
            (e) => {
              this.isLoading = false;
              if (e?.status === 0) {
                this.messageService.error(this.notificationMessage.E001);
                return;
              }
              this.messageService.error(e?.error?.description);
            }
          );
        }
      });
    } else {
      this.isValidator = true;
      this.formCreate.controls.forEach((element: FormGroup) => {
        validateAllFormFields(element);
      });
    }
  }
}
