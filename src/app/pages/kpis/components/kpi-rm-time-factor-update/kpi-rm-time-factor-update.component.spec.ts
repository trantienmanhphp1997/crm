import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiRmTimeFactorUpdateComponent } from './kpi-rm-time-factor-update.component';

describe('KpiRmTimeFactorUpdateComponent', () => {
  let component: KpiRmTimeFactorUpdateComponent;
  let fixture: ComponentFixture<KpiRmTimeFactorUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiRmTimeFactorUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiRmTimeFactorUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
