import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {KpiAssignmentReceiveComponent} from './kpi-assignment-receive.component';

describe('KpiAssignmentReceiveComponent', () => {
	let component: KpiAssignmentReceiveComponent;
	let fixture: ComponentFixture<KpiAssignmentReceiveComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [KpiAssignmentReceiveComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(KpiAssignmentReceiveComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
