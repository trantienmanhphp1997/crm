import { forkJoin, of } from 'rxjs';
import { ChangeDetectorRef, Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { catchError } from 'rxjs/operators';
import { LIST_COLUMN_FIRST, LIST_COLUMN_LAST, LIST_MONTH, LIST_YEAR, SOURCE_INPUT } from '../../constant';
import { KpiAssignmentApi } from '../../apis';
import * as moment from 'moment';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { maxInt32, SessionKey } from 'src/app/core/utils/common-constants';
import { RmBlockApi } from 'src/app/pages/rm/apis';

@Component({
  selector: 'app-kpi-assignment-receive',
  templateUrl: './kpi-assignment-receive.component.html',
  styleUrls: ['./kpi-assignment-receive.component.scss'],
})
export class KpiAssignmentReceiveComponent extends BaseComponent implements OnInit {
  isFirstHalf = parseInt(moment().format('MM')) <= 6;
  isLoading = false;
  listBlock = [];
  formSearch = this.fb.group({
    blockCode: '',
    rmTitleCode: '',
    rmLevelCode: '',
    month: parseInt(moment().format('MM')),
    year: parseInt(moment().format('yyyy')),
  });
  formCreate = this.fb.array([]);
  listLevel = [];
  listGroup = [];
  listKpiItem = [];
  listGroupByCategoryType = [];
  listKpiItemMapping = [];
  listYear = LIST_YEAR;
  listMonth = LIST_MONTH;
  year = moment().format('YYYY');
  listColumnValue = this.isFirstHalf ? LIST_COLUMN_FIRST : LIST_COLUMN_LAST;
  codeSelected = '';
  isPressSave = false;
  blockData = '';
  blockSelected: any = {};
  levelRMName = '';
  titleGroupName = '';

  constructor(
    injector: Injector,
    private cd: ChangeDetectorRef,
    private kpiAssignmentApi: KpiAssignmentApi,
    private rmBlockApi: RmBlockApi
  ) {
    super(injector);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.isLoading = false;
    const hrsCode = this.sessionService.getSessionData(SessionKey.USER_INFO)?.hrsCode;
    if (hrsCode) {
      this.isLoading = true;
      const params = {
        hrsCode,
        page: 0,
        size: maxInt32,
      };
      forkJoin([this.rmBlockApi.fetch(params)]).subscribe(
        async ([blockData]) => {
          // map list block
          this.listBlock = blockData?.content?.filter((item) => item.isActive) || [];

          this.blockSelected = this.listBlock[0];

          this.titleGroupName = this.blockSelected?.titleGroupName || '';
          this.levelRMName = this.blockSelected?.levelRMName || '';

          this.formSearch.setValue({
            ...this.formSearch.value,
            blockCode: this.blockSelected?.blockCode,
          });
          this.onSearchKpiAssign();
        },
        () => {
          this.isLoading = false;
        }
      );
    }
  }

  async onChangeBlockCode() {
    const formValue = this.formSearch.value;
    this.blockSelected = this.listBlock.find((item) => item.blockCode === formValue.blockCode);
    this.titleGroupName = this.blockSelected?.titleGroupName || '';
    this.levelRMName = this.blockSelected?.levelRMName || '';
    this.onSearchKpiAssign();
  }

  getColName = (colObject) => {
    if (colObject.code === 'N') {
      return `${colObject.name} ${this.year}`;
    } else {
      return colObject.name;
    }
  };

  onTreeAction(event: any) {
    const row = event.row;

    if (row.treeStatus === 'collapsed') {
      row.treeStatus = 'expanded';
    } else {
      row.treeStatus = 'collapsed';
    }

    this.listKpiItemMapping = [...this.listKpiItemMapping];
    this.cd.detectChanges();
  }

  isExist = (value) => value !== null && value !== undefined && value !== '';

  onSearchKpiAssign = async () => {
    this.formCreate.clear();
    const formValue = this.formSearch.value;
    this.listColumnValue = parseInt(formValue.month) <= 6 ? LIST_COLUMN_FIRST : LIST_COLUMN_LAST;
    this.isLoading = true;

    let params = {
      blockCode: formValue.blockCode,
      rmTitleCode: this.blockSelected?.titleGroupCode || '',
      rmLevelCode: this.blockSelected?.levelRMCode || '',
      month: formValue.month,
      year: formValue.year,
    };

    const listKpiByTitle =
      (await this.kpiAssignmentApi
        .searchMyKpiAssign(params)
        .pipe(catchError((e) => of(undefined)))
        .toPromise())?.sort((a, b) => (a.categoryCode >= b.categoryCode) ? 1 : -1) || [];
    this.listKpiItemMapping = [];
    // group by categoryCode
    const listGroupByCategoryType = Object.values(
      listKpiByTitle?.reduce(
        (r, a) => ({
          ...r,
          [a.categoryCode]: [...(r[a.categoryCode] || []), a],
        }),
        {}
      )
    );

    listGroupByCategoryType.forEach((e: Array<any>) => {
      // first level nested
      this.listKpiItemMapping.push({
        idGroup: e[0]?.categoryCode,
        name: e[0]?.categoryName,
        treeStatus: 'expanded',
        parentId: null,
      });
      this.formCreate.push(this.fb.group({}));

      e.forEach((i) => {
        const frequencyNames = [];
        i.frequencies?.length && i.frequencies.forEach((element) => frequencyNames.push(element.frequencyName));
        const sourceInputName = SOURCE_INPUT.find((source) => source.code == i.sourceInput)?.name || ''; // == cause different type
        // last level nested
        this.listKpiItemMapping.push({
          ...i,
          // idGroup is required cause prevent render error
          idGroup: i.kpiItemRmLevelId,
          name: i.kpiItemName,
          treeStatus: 'disabled',
          parentId: i.categoryCode,
          frequency: frequencyNames.join(', '),
          rate: i.rate.toString().replace('.', ','),
          sourceInput: sourceInputName,
          mappingTargets: this.listColumnValue.map((col) => {
            const data = i.targets?.find(
              (target) => col.month === target.standardMonth && formValue.year == target.standardYear
            ); // == cause different type
            return {
              ...col,
              ...data,
              standardValue: data?.standardValue ? data?.standardValue.toString().replace('.', ',') : '',
            };
          }),
        });

        console.log('this.listKpiItemMapping', this.listKpiItemMapping);

        this.formCreate.push(
          this.fb.group({
            value: [0, [CustomValidators.required, CustomValidators.assignKpi]],
            // extra data
            kpiStandardId: i.kpiStandardId,
            kpiItemRmLevelId: i.kpiItemRmLevelId,
            kpiItemId: i.kpiItemId,
          })
        );
      });
    });
    this.isLoading = false;
  };

  isBoolean = (value) => typeof value === 'boolean';
}
