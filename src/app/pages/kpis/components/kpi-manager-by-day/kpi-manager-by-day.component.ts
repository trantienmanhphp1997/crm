import { Component, OnInit, ViewChild, HostBinding, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { KpiManagerApi, CommonCategoryApi, KpiMonthApi, RmProfileApi } from '../../apis';
import { Router } from '@angular/router';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Division, FunctionCode, maxInt32, Scopes } from 'src/app/core/utils/common-constants';
import _ from 'lodash';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { Utils } from '../../../../core/utils/utils';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import * as moment from 'moment';
import { global } from '@angular/compiler/src/util';
import { CommonCategory } from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import { TranslateService } from '@ngx-translate/core';
import { NotifyMessageService } from 'src/app/core/components/notify-message/notify-message.service';
import { EncrDecrService } from '../../../../core/services/encr-decr.service';
import { SessionService } from 'src/app/core/services/session.service';
import { FileService } from 'src/app/core/services/file.service';

@Component({
  selector: 'app-kpi-manager-by-day',
  templateUrl: './kpi-manager-by-day.component.html',
  styleUrls: ['./kpi-manager-by-day.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class KpiManagerByDayComponent implements OnInit {
  constructor(
    private api: KpiManagerApi,
    private ref: ChangeDetectorRef,
    private commonCategoryApi: CommonCategoryApi,
    private categoryService: CategoryService,
    private rmProfileApi: RmProfileApi,
    private monthApi: KpiMonthApi,
    private router: Router,
    private translate: TranslateService,
    private fileService: FileService,
    private messageService: NotifyMessageService,
    private enc: EncrDecrService,
    private sessionService: SessionService
  ) {
    this.messages = global.messageTable;
    this.obj = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.KPI_CBQL}`);
    this.scopes = _.get(this.obj, 'scopes');
    this.translate.get(['fields', 'notificationMessage']).subscribe((result) => {
      this.notificationMessage = _.get(result, 'notificationMessage');
      this.field = _.get(result, 'fields');
    });
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
  }
  maxDate = new Date();
  minDate = moment().add(-45, 'day').toDate();
  scopes: Array<any>;
  field: any;
  prop: any;
  obj: any;
  branches: Array<any>;
  blocks: Array<any>;
  groups: Array<any>;
  levels: Array<any>;
  models: Array<any>;
  pageable: Pageable;
  isFisrt = true;
  isLoading: boolean;
  searchAdvance: boolean;
  textSearch: string;
  notificationMessage: any;
  params: any = {
    pageSize: global.userConfig.pageSize,
    pageNumber: 0,
    rsId: '',
    scope: Scopes.VIEW,
    period: false,
    past: false,
    dayView: false,
    typeView: true
  };
  prevParams = this.params;
  months: Array<any>;
  years: Array<any>;
  valueTypes: Array<any>;
  width = 650;
  colspan = 5;
  messages: any;
  year_config: number;
  periods: Array<any>;
  pasts: Array<any>;
  dayViews: Array<any>;
  view_value = 'TUDOANH';
  data_of_date: Date;
  date: Date = new Date();
  @ViewChild('table') table: DatatableComponent;
  lastModifiedDate: string;
  showLastModifiedDate: boolean;
  countChangeMonth = 0;
  countChangeYear = 0;
  listDataSme = [];
  properties = [];
  isSme = false;
  highPrioritySort = [Division.INDIV, Division.SME, Division.CIB];

  @HostBinding('class.app__right-content') appRightContent = true;

  ngOnInit(): void {
    this.init();
    const rsIdOfRm = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`)?.rsId;
    this.params.rsId = rsIdOfRm;
    forkJoin([
      this.commonCategoryApi.getCommonCategory(CommonCategory.KPI_YEARS).pipe(catchError((e) => of(undefined))),
      this.categoryService.getBranchesOfUser(rsIdOfRm, Scopes.VIEW).pipe(catchError((e) => of(undefined))),
      this.categoryService.getBlocksCategoryByRm().pipe(catchError((e) => of(undefined))),
      this.api
        .getBusinessDate({
          viewRm: false,
          period: _.get(this.params, 'period'),
          dayView: _.get(this.params, 'dayView'),
          typeView: _.get(this.params, 'typeView'),
        })
        .pipe(catchError((e) => of(undefined))),
    ]).subscribe(([kpis, branches, blocks, date]) => {
      this.year_config = _.get(_.first(_.get(kpis, 'content')), 'value');
      this.init_year(this.year_config);
      this.branches = branches;
      _.set(
        this.params,
        'branches',
        _.map(this.branches, (x) => x.code)
      );
      this.blocks = _.map(blocks, (x) => ({ ...x, value_to_search: `${x.code} - ${x.name}` }));
      this.blocks = _.orderBy(this.blocks, ['value_to_search'], ['asc', 'desc']);

      this.sortDivision();

      this.params.bizLine = _.get(_.first(this.blocks), 'code');
      if (!this.checkDisabledDate(this.params.bizLine)) {
        this.params.period = true;
      }
      this.data_of_date = date;
      if (Utils.isNotNull(date)) {
        _.set(this.params, 'businessDate', new Date(date));
      }
      if (Utils.isNotNull(this.prop)) {
        this.params = this.prop;
        this.months = this.params.listMonth;
        this.rmProfileApi
          .get('title-groups-config', {
            viewRm: false,
            blockCode: _.get(this.params, 'bizLine'),
            rsId: this.params.rsId,
            scope: 'VIEW',
          })
          .subscribe((response: Array<any>) => {
            this.groups = _.map(response, (x) => ({ ...x, value_to_search: `${x.blockCode} - ${x.name}` }));
            this.groups = _.orderBy(this.groups, ['value_to_search'], ['asc', 'desc']);
            this.params.groupTitleId = _.get(this.params, 'groupTitleId');
            if (Utils.isStringNotEmpty(this.params.groupTitleId)) {
              if (Utils.isStringEmpty(_.get(this.params, 'groupTitleId'))) {
                this.levels = [];
                this.ref.detectChanges();
              } else {
                const blcks = _.get(
                  _.find(this.groups, (x) => x.id === _.get(this.params, 'groupTitleId')),
                  'levels'
                );
                if (Utils.isArrayNotEmpty(blcks)) {
                  this.levels = _.map(blcks, (x) => ({ ...x, value_to_search: `${x.blockCode} - ${x.name}` }));
                  this.levels = _.orderBy(this.levels, ['value_to_search'], ['asc', 'desc']);
                  this.levels = [
                    {
                      id: '',
                      name: 'Tất cả',
                      value_to_search: 'Tất cả',
                    },
                    ...this.levels,
                  ];
                  this.params.rmLevelId = _.get(this.params, 'rmLevelId');
                  this.ref.detectChanges();
                }
              }
            }
            this.reload(true);
          });
      } else {
        this.onBlockChange(this.params.bizLine, true);
      }
    });
  }

  checkScopes(codes: Array<any>) {
    if (Utils.isArrayEmpty(codes)) return false;
    return Utils.isArrayNotEmpty(_.filter(this.scopes, (x) => codes.includes(x)));
  }

  init() {
    this.valueTypes = [
      {
        code: false,
        name: 'Tự doanh',
      },
      {
        code: true,
        name: 'Tổng hợp theo ngày',
      },
    ];

    this.dayViews = [
      {
        code: true,
        name: 'Năng suất',
      },
    ];
  }

  init_year(range: number) {
    this.years = [];
    const year = moment().add('month', -1).year();
    for (let i = 0; i < range; i++) {
      this.years.push({
        code: year - i,
        name: year - i,
      });
    }
    this.params.year = _.get(_.first(this.years), 'code');
    this.onYearChange(this.params.year);
  }

  init_date() {
    this.api
      .getBusinessDate({
        viewRm: false,
        period: _.get(this.params, 'period'),
        dayView: _.get(this.params, 'dayView'),
        typeView: _.get(this.params, 'typeView'),
      })
      .subscribe((response: Date) => {
        if (Utils.isNotNull(response)) {
          this.data_of_date = response;
          this.params.year = moment(response).year();
          this.onYearChange(this.params.year);
          // day - auto t-1
          if (this.params.period) _.set(this.params, 'businessDate', new Date(response));
        }
      });
  }

  onYearChange($event) {
    this.months = [];
    let apiMonth: Observable<any>;
    if (_.get(this.params, 'bizLine') === Division.SME || _.get(this.params, 'bizLine') === Division.CIB) {
      apiMonth = this.api.getDateSme(this.params.period, $event, _.get(this.params, 'bizLine'));
    } else {
      apiMonth = this.monthApi.getMonthWithYear({
        year: $event,
        viewRm: false,
      });
    }
    apiMonth.subscribe((response) => {
      _.forEach(response, (item) => {
        this.months.push({ name: item, code: item });
      });
      this.params.month = _.get(_.last(this.months), 'code');
      if (_.get(this.params, 'period') === true) {
        this.getMonthData($event, _.get(this.params, 'month'));
      }
    });

    this.ref.detectChanges();
  }

  onMonthChange($event) {
    if (_.get(this.params, 'period') === true) {
      this.getMonthData(_.get(this.params, 'year'), $event);
    }
  }

  onValueTypeChange($event) {
    // this.init_date();
    this.view_value = $event === false ? 'TUDOANH' : 'THTN';
    this.colspan = 5;
    this.width = 650;
    this.api
      .getBusinessDate({
        viewRm: false,
        period: _.get(this.params, 'period'),
        dayView: _.get(this.params, 'dayView'),
        typeView: _.get(this.params, 'typeView'),
      })
      .subscribe((response: Date) => {
        if (Utils.isNotNull(response)) {
          this.data_of_date = response;
          this.params.year = moment(response).year();
          this.onYearChange(this.params.year);
          if (this.params.period) _.set(this.params, 'businessDate', new Date(response));
        }
        this.reload(true);
      });
    this.ref.detectChanges();
  }

  onTypeViewChange($event) {
    if ($event) {
      this.view_value = 'NS';
      this.colspan = 7;
      this.width = 650;
      this.reload(true);
    }
    this.ref.detectChanges();
  }

  onPeriodChangeDay($event) {
    if (this.countChangeYear === 0) {
      this.init_date();
      this.params.dayView = _.get(_.first(this.valueTypes), 'code');
      this.onValueTypeChange(this.params.dayView);
      this.countChangeYear++;
      this.countChangeMonth = 0;
    }
    this.params.businessDate = moment().add(-1, 'day').toDate();
    this.ref.detectChanges();
  }

  onPeriodchangeMonth($event) {
    if (this.countChangeMonth === 0) {
      this.init_date();
      this.getMonthData(_.get(this.params, 'year'), _.get(this.params, 'month'));
      this.params.typeView = true;
      this.onTypeViewChange(true);
      this.countChangeMonth++;
      this.countChangeYear = 0;
    }
    this.ref.detectChanges();
  }

  getMonthData(year, month) {
    const date = new Date(year, month, 0).getDate();
    const businessDate = moment(new Date(year, month - 1, date)).format('YYYYMMDD');
    this.api
      .getMonthData({
        viewRm: false,
        businessDate,
        blockCode: _.get(this.params, 'bizLine'),
      })
      .subscribe((response) => {
        if (Utils.isStringNotEmpty(response)) {
          this.lastModifiedDate = response;
          this.showLastModifiedDate = true;
        } else {
          this.showLastModifiedDate = false;
        }
      });
  }

  onPastchange($event) {
    if ($event === false) {
      this.params.businessDate = new Date(this.data_of_date);
      this.params.year = moment(this.data_of_date).year();
      this.params.month = moment(this.data_of_date).month();
      this.onYearChange(this.params.year);
    } else {
      this.params.year = moment(this.data_of_date).year();
      this.onYearChange(this.params.year);
    }
    this.ref.detectChanges();
  }

  onBlockChange($event, isFirst?) {
    if (!this.checkDisabledDate($event)) {
      this.params.period = true;
    }
    this.init_year(this.year_config);
    if (Utils.isStringEmpty($event)) {
      this.groups = [];
      this.ref.detectChanges();
    } else {
      if (_.get(this.params, 'period') === true) {
        this.getMonthData(_.get(this.params, 'year'), _.get(this.params, 'month'));
      }
      this.rmProfileApi
        .get('title-groups-config', { viewRm: false, blockCode: $event, rsId: this.params.rsId, scope: 'VIEW' })
        .subscribe((response: Array<any>) => {
          this.groups = _.map(response, (x) => ({ ...x, value_to_search: `${x.blockCode} - ${x.name}` }));
          this.groups = _.orderBy(this.groups, ['value_to_search'], ['asc', 'desc']);
          setTimeout(() => {
            this.params.groupTitleId = _.get(_.first(this.groups), 'id');
            this.ref.detectChanges();
            if (Utils.isStringNotEmpty(this.params.groupTitleId)) {
              this.onGroupTitleChange(this.params.groupTitleId);
              if (isFirst) {
                this.reload(true);
              }
            }
          }, 100);
        });
      this.ref.detectChanges();
    }
  }

  onGroupTitleChange($event) {
    if (Utils.isStringEmpty($event)) {
      this.levels = [];
      this.ref.detectChanges();
    } else {
      const blcks = _.get(
        _.find(this.groups, (x) => x.id === $event),
        'levels'
      );
      if (Utils.isArrayNotEmpty(blcks)) {
        this.levels = _.map(blcks, (x) => ({ ...x, value_to_search: `${x.blockCode} - ${x.name}` }));
        this.levels = _.orderBy(this.levels, ['value_to_search'], ['asc', 'desc']);
        if (this.params.bizLine === Division.SME || this.params.bizLine === Division.CIB) {
          this.params.rmLevelId = _.get(_.first(this.levels), 'id');
        } else {
          this.levels = [
            {
              id: '',
              name: 'Tất cả',
              value_to_search: 'Tất cả',
            },
            ...this.levels,
          ];
          this.params.rmLevelId = '';
        }

        this.ref.detectChanges();
      }
    }
  }

  exportFile() {
    if (Utils.isArrayEmpty(this.models)) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    this.isLoading = true;
    const params = this.prevParams;
    _.set(params, 'pageSize', maxInt32);
    _.set(params, 'pageNumber', 0);
    let apiExcel: Observable<any>;
    if (params.bizLine === Division.SME || params.bizLine === Division.CIB) {
      apiExcel = params.period ? this.api.createFileRMSme(params) : this.api.createFileSmeByDay(params);

      if (!params.period) {
        params.rm = 0;
      }

    } else {
      apiExcel = this.api.createFile(params);
    }

    apiExcel.subscribe(
      (res) => {
        if (Utils.isStringNotEmpty(res)) {
          this.download(res);
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(_.get(this.notificationMessage, 'export_error'));
        this.isLoading = false;
      }
    );
  }

  download(fileId: string) {
    this.fileService.downloadFile(fileId, 'danh sach kpi-cbql.xlsx').subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }

  setPage(pageInfo) {
    if (this.isFisrt) {
      return;
    }
    this.params.pageNumber = pageInfo.offset;
    this.reload(false);
  }

  search() {
    this.reload(true);
  }

  reload(isSearch?: boolean) {
    this.isLoading = true;
    let params: any = {};
    if (isSearch) {
      this.params.pageNumber = 0;
      this.params.businessDate = Utils.isNotNull(this.params.businessDate)
        ? new Date(moment(this.params.businessDate).format('YYYY-MM-DD'))
        : new Date(this.data_of_date);
      if (this.params.period) {
        this.params.businessDate = undefined;
        if (Utils.isNotNull(this.params.month) && Utils.isNotNull(this.params.year)) {
          const date = new Date(this.params.year, this.params.month, 0).getDate();
          const month = _.get(this.params, 'month') - 1;
          this.params.businessDate = moment(new Date(this.params.year, month, date)).format('YYYY-MM-DD');
        }
      }
    } else {
      if (_.get(this.prevParams, 'pageSize') === maxInt32) {
        _.set(this.prevParams, 'pageSize', _.get(global, 'userConfig.pageSize'));
      }
      params = this.prevParams;
    }
    Object.keys(this.params).forEach((key) => {
      params[key] = this.params[key];
    });
    params.pageNumber = this.params.pageNumber;
    params.pageSize = this.params.pageSize;
    params.rsId = this.params.rsId;
    params.scope = 'VIEW';
    if (this.params.bizLine === Division.SME || this.params.bizLine === Division.CIB) {
      params.rmTitleCode = this.groups?.find((item) => item.id === params.groupTitleId).code || '';
      params.rmLevelCode = this.levels?.find((item) => item.id === params.rmLevelId).code || '';
      params.rmTitleId = this.params.groupTitleId;
      params.blockCode = this.params.bizLine;
      params.businessDate = moment(this.params.businessDate).format('YYYY-MM-DD');
      const apiSearch = !this.params.period ? this.api.searchDataSmeDaily(params) : this.api.searchDataSme(params);

      if (!this.params.period) {
        params.rm = 0;
      }

      apiSearch.subscribe(
        (res) => {
          if (res) {
            this.isSme = true;
            this.isFisrt = false;
            this.isLoading = false;
            const data = res;
            this.models = data?.content || [];
            this.properties = data?.content?.map((item) => item.properties)[0];
            this.pageable = {
              totalElements: data?.totalElements,
              totalPages: data?.totalPages,
              currentPage: this.params.pageNumber,
              size: global.userConfig.pageSize,
            };
            this.prevParams = { ...params, listMonth: this.months };
          }
        },
        (e) => {
          this.isLoading = false;
        }
      );
    } else {
      this.api.fetch(params).subscribe(
        (response) => {
          this.isSme = false;
          this.isFisrt = false;
          this.isLoading = false;
          this.models = _.get(response, 'content') || [];
          this.pageable = {
            totalElements: _.get(response, 'totalElements'),
            totalPages: _.get(response, 'totalPages'),
            currentPage: _.get(response, 'number'),
            size: _.get(global, 'userConfig.pageSize'),
          };
          this.prevParams = { ...params, listMonth: this.months };
        },
        () => {
          this.isLoading = false;
        }
      );
    }
  }

  onActive($event) {
    const type = _.get($event, 'type');
    const id = _.get($event, 'row.id');
    if (_.get(this.prevParams, 'dayView') === false && _.get(this.prevParams, 'period') === false) {
      if (type === 'dblclick') {
        if (Utils.isStringNotEmpty(id)) {
          const name = `${id}-${_.get(this.prevParams, 'period')}-${_.get(this.prevParams, 'past')}-${moment(
            this.data_of_date
          ).format('YYYY/MM/DD')}`;
          this.router.navigate([`/kpis/kpi-management/kpi-track/kpi-manager-by-day/detail/${this.enc.set(name)}`], {
            state: this.prevParams,
          });
        }
      }
    }
  }

  getValue(row, key) {
    return _.get(row, key);
  }

  onActiveSme(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      this.router.navigate([this.router.url, 'detail-sme'], {
        skipLocationChange: true,
        queryParams: { dataKpiRm: JSON.stringify(item) },
        state: this.prevParams,
      });
    }
  }

  checkDisabledDate(value) {
    if (value === Division.SME || value === Division.CIB) {
      return false;
    } else {
      return true;
    }
  }

  sortDivision(): void {
    const currentDisivionList: any[] = _.cloneDeep(this.blocks);
    const result = [];
    this.highPrioritySort.forEach(code => {
      const index = currentDisivionList.findIndex(item => item.code === code);
      if (index > -1) {
        result.push(currentDisivionList[index]);
        currentDisivionList.splice(index, 1);
      }
    });

    result.push(...currentDisivionList);
    this.blocks = result;
  }
}
