import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiCategoryByTitleCreateComponent } from './kpi-category-by-title-create.component';

describe('KpiCategoryByTitleCreateComponent', () => {
  let component: KpiCategoryByTitleCreateComponent;
  let fixture: ComponentFixture<KpiCategoryByTitleCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiCategoryByTitleCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiCategoryByTitleCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
