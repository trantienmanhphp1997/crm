import { forkJoin, of } from 'rxjs';
import { Component, OnInit, Injector, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { catchError } from 'rxjs/operators';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as _ from 'lodash';
import {
	LIST_KPI_ASSESSMENT,
	LIST_KPI_FEATURE_NATURE,
	SERVER_DATE_FORMAT,
	SOURCE_INPUT,
	UN_TOUCH_KPI_ITEM_NUMBER,
} from '../../constant';
import { KpiCategoryApi, KpiCategoryByTitleApi, LevelApi, TitleGroupApi } from '../../apis';
import { FormArray, FormGroup } from '@angular/forms';
import * as moment from 'moment';

@Component({
	selector: 'app-kpi-category-by-title-create',
	templateUrl: './kpi-category-by-title-create.component.html',
	styleUrls: ['./kpi-category-by-title-create.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class KpiCategoryByTitleCreateComponent extends BaseComponent implements OnInit {
	formSearch = this.fb.group({
		rmTitleCode: ['', CustomValidators.required],
		rmLevelCode: ['', CustomValidators.required],
		blockCode: ['', CustomValidators.required],
		// Extra data
		rmLevelId: '',
		rmTitleId: '',
	});
	formCreate = this.fb.array([]);
	kpiCategoryOutputs = [];
	listKpiAssessment = LIST_KPI_ASSESSMENT;
	kpiUnitOutputs = [];
	kpiFeatures = LIST_KPI_FEATURE_NATURE;
	listSourceInput = SOURCE_INPUT;
	kpiFrequencyOutputs = [];
	isLoading = false;
	isValidator = false;
	listBlock = [];
	listGroup = [];
	listLevel = [];
	listKpiItem = [];
	branchesCode: string[];
	minDate = new Date(moment().subtract(1, 'month').startOf('month').valueOf());
	allKpiItem = [];

	constructor(
		injector: Injector,
		private categoryService: CategoryService,
		private kpiCategoryByTitleApi: KpiCategoryByTitleApi,
		private kpiCategoryApi: KpiCategoryApi,
		private titleGroupApi: TitleGroupApi,
		private rmLevelApi: LevelApi
	) {
		super(injector);
	}

	ngOnInit(): void {
		this.isLoading = true;
		forkJoin([
			this.categoryService.getBlocksCategoryByRm().pipe(catchError((e) => of(undefined))),
			this.kpiCategoryApi.getItemResource().pipe(catchError((e) => of(undefined))),
			this.kpiCategoryApi
				.search({ pageSize: UN_TOUCH_KPI_ITEM_NUMBER, pageNumber: 0 })
				.pipe(catchError((e) => of(undefined))),
		]).subscribe(async ([listBlock, itemResources, allKpiItem]) => {
			let data;
			if (listBlock?.length) {
				data = await this.getGroupAndLevelBlockCode(listBlock[0].code);
			}
			this.prop = {
				listBlock,
				itemResources,
				listGroup: data.listGroup,
				listLevel: data.listLevel,
				allKpiItem,
			};
			this.mapData();
		});
	}

	getGroupAndLevelBlockCode = async (code) => {
		let listGroup = [];
		let listLevel = [];
		listGroup =
			(
				await this.titleGroupApi
					.searchGroupByBlockCode(code)
					.pipe(catchError((e) => of(undefined)))
					.toPromise()
			)?.content || [];
		if (listGroup?.length) {
			listLevel =
				(
					await this.rmLevelApi
						.searchLevelByGroupCode(listGroup[0].id, code)
						.pipe(catchError((e) => of(undefined)))
						.toPromise()
				)?.content || [];
		}
		console.log(listGroup, listLevel);

		return { listGroup, listLevel };
	};

	onChangeCategoryCode = async (index) => {
		const categoryCode = this.formCreate.value[index]?.categoryCode;
		const blockCode = this.formSearch.value?.blockCode;
		const res = await this.kpiCategoryApi
			.search({ categoryCode, blockCode, isActive: true, pageSize: UN_TOUCH_KPI_ITEM_NUMBER, pageNumber: 0 })
			.pipe(catchError((e) => of(undefined)))
			.toPromise();
		this.listKpiItem[index] = res?.content || [];
	};

	mapData() {
		this.kpiCategoryOutputs = this.prop?.itemResources?.kpiCategoryOutputs || [];
		this.kpiUnitOutputs = this.prop?.itemResources?.kpiUnitOutputs || [];
		this.kpiFrequencyOutputs = this.prop?.itemResources?.kpiFrequencyOutputs || [];
		this.allKpiItem = this.prop.allKpiItem?.content || [];
		this.listBlock =
			this.prop.listBlock?.map((item) => {
				return { ...item, name: item.code + ' - ' + item.name };
			}) || [];
		this.listGroup = this.mapListGroup(this.prop.listGroup);
		this.listLevel = this.prop.listLevel;
		// Set default value
		const objectData = {
			blockCode: this.listBlock?.[0]?.code,
			rmTitleCode: this.listGroup?.[0]?.code,
			rmLevelCode: this.listLevel?.[0]?.code,
			rmLevelId: this.listLevel?.[0]?.id,
			rmTitleId: this.listGroup?.[0]?.id,
		};
		this.formSearch.setValue({ ...objectData });

		// Search old data
		this.searchKpiAccordingTitles({
			blockCode: objectData.blockCode,
			title: objectData.rmTitleCode,
			level: objectData.rmLevelCode,
		});
	}

	mapFormControl = () => {
		this.formCreate.controls.forEach((fb: FormGroup, i) => {
			if (i !== this.formCreate.controls.length - 1) {
				return;
			}
			fb.controls.efficientDate.valueChanges.subscribe((value) => {
				fb.controls.minExpireDate.setValue(new Date(moment(value).valueOf()));
			});
			fb.controls.expireDate.valueChanges.subscribe((value) => {
				fb.controls.maxEfficientDate.setValue(new Date(moment(value).valueOf()));
			});
		});
	};

	searchKpiAccordingTitles = async ({ blockCode, title, level }) => {
		this.isLoading = true;
		const listOldKpiByTitle =
			(await this.kpiCategoryByTitleApi
				.searchKpiAccordingTitles({ blockCode, title, level })
				.pipe(catchError((e) => of(undefined)))
				.toPromise()) || [];
		// console.log('listOldKpiByTitle', listOldKpiByTitle);
		// clear data before push
		this.formCreate.clear();
		this.listKpiItem = [];
		// Set time out to prevent format err
		const timer = setTimeout(() => {
			listOldKpiByTitle.forEach((element) => {
				(this.formCreate as FormArray).push(this.addOldItem(element));
				this.listKpiItem.push(this.allKpiItem.filter((item) => item.categoryCode === element.categoryCode));
			});
			clearTimeout(timer);
		}, 0);

		this.isLoading = false;
	};

	async onChangeBlockCode() {
		const blockCode = this.formSearch.value.blockCode;
		const data = await this.getGroupAndLevelBlockCode(blockCode);
		this.listGroup = this.mapListGroup(data.listGroup);
		this.listLevel = data.listLevel;
		// Re-set value rmTitleCode, rmTitleId, rmLevelId and rmLevelCode
		const objectData = {
			rmTitleCode: this.listGroup?.[0]?.code || '',
			rmLevelCode: this.listLevel?.[0]?.code || '',
			rmLevelId: this.listLevel?.[0]?.id || '',
			rmTitleId: this.listGroup?.[0]?.id || '',
		};
		this.formSearch.setValue({ ...this.formSearch.value, ...objectData });
		// Search old data
		this.searchKpiAccordingTitles({
			blockCode: this.formSearch.value.blockCode,
			title: objectData.rmTitleCode,
			level: objectData.rmLevelCode,
		});
	}

	async onChangeGroup() {
		const blockCode = this.formSearch.value.blockCode;
		const groupCode = this.formSearch.value.rmTitleCode;
		const groupSelected = this.listGroup.find((group) => group.code === groupCode);
		this.listLevel =
			(
				await this.rmLevelApi
					.searchLevelByGroupCode(groupSelected?.id, blockCode)
					.pipe(catchError((e) => of(undefined)))
					.toPromise()
			)?.content || [];
		// Re-set value rmTitleId, rmLevelCode, rmLevelId
		const objectData = {
			rmTitleId: groupSelected?.id,
			rmLevelCode: this.listLevel?.[0]?.code || '',
			rmLevelId: this.listLevel?.[0]?.id || '',
		};
		this.formSearch.setValue({ ...this.formSearch.value, ...objectData });
		// Search old data
		this.searchKpiAccordingTitles({
			blockCode: this.formSearch.value.blockCode,
			title: this.formSearch.value.rmTitleCode,
			level: objectData.rmLevelCode,
		});
	}

	onChangeLevel() {
		const levelCode = this.formSearch.value.rmLevelCode;
		const levelSelected = this.listLevel.find((level) => level.code === levelCode);
		// Re-set value rmLevelId
		const objectData = {
			rmLevelId: levelSelected?.id,
		};
		this.formSearch.setValue({ ...this.formSearch.value, ...objectData });
		// Search old data
		this.searchKpiAccordingTitles({
			blockCode: this.formSearch.value.blockCode,
			title: this.formSearch.value.rmTitleCode,
			level: this.formSearch.value.rmLevelCode,
		});
	}

	mapListGroup = (listGroup) => {
		return (
			listGroup?.map((item) => {
				return { ...item, name: item.blockCode + ' - ' + item.name };
			}) || []
		);
	};

	addItem = () => {
		if (this.formCreate.valid) {
			(this.formCreate as FormArray).push(this.createItem());
			this.mapFormControl();
		} else {
			this.isValidator = true;
			this.formCreate.controls.forEach((element: FormGroup) => {
				validateAllFormFields(element);
			});
		}
	};

	onChangeEndDate = (event, index) => {
		const endOfMonth = new Date(moment(event).endOf('month').valueOf());
		(this.formCreate.controls[index] as FormGroup).controls?.expireDate?.setValue(endOfMonth);
	};

	getErrRequire(rowIndex, fieldName) {
		const controlValue = (this.formCreate.controls[rowIndex] as FormGroup).controls;
		return controlValue?.[fieldName]?.errors?.cusRequired && controlValue?.[fieldName]?.touched && this.isValidator;
	}

	getErrValidate(rowIndex, fieldName) {
		const controlValue = (this.formCreate.controls[rowIndex] as FormGroup).controls;
		if (fieldName === 'limitFloor') {
			return controlValue?.[fieldName]?.errors?.value || controlValue?.[fieldName]?.errors?.maxLimitFloor;
		} else if (fieldName === 'limitCeiling') {
			return controlValue?.[fieldName]?.errors?.value || controlValue?.[fieldName]?.errors?.minLimitCeiling;
		}
		return controlValue?.[fieldName]?.errors?.value;
	}

	createItem = () => {
		return this.fb.group({
			categoryCode: ['', CustomValidators.required],
			kpiItemId: ['', CustomValidators.required],
			efficientDate: ['', CustomValidators.required],
			expireDate: '',
			isActive: true,
			rate: ['', [CustomValidators.required, CustomValidators.rate]],
			limitFloor: ['', CustomValidators.valueByTitle],
			limitCeiling: ['', CustomValidators.valueByTitle],
			ceilingRatio: ['', CustomValidators.valueByTitle],
			isTimeOrgFactor: false,
			isAreaFactor: false,
			isKpi: true,
			sourceInput: ['', CustomValidators.required],
			isShare: true,
			// extra data
			minExpireDate: '',
			maxEfficientDate: '',
		});
	};

	addOldItem = (item) => {
		return this.fb.group({
      categoryCode: { value: item.categoryCode, disabled: true },
      kpiItemId: { value: item.kpiItemId, disabled: true },
      efficientDate: {
        value: new Date(moment(item.efficientDate, SERVER_DATE_FORMAT).valueOf()),
        disabled: true,
      },
      expireDate: {
        value: item?.expireDate ? new Date(moment(item.expireDate, SERVER_DATE_FORMAT).valueOf()) : '',
        disabled: true,
      },
      isActive: { value: item.isActive, disabled: true },
      rate: { value: item.rate.toString().replace('.', ','), disabled: true },
      limitFloor: { value: (item.limitFloor || '').toString().replace('.', ','), disabled: true },
      limitCeiling: { value: (item.limitCeiling || '').toString().replace('.', ','), disabled: true },
      ceilingRatio: { value: (item.ceilingRatio || '').toString().replace('.', ','), disabled: true },
      isAreaFactor: { value: item.isAreaFactor, disabled: true },
      isKpi: { value: item.isKpi, disabled: true },
      isShare: { value: item.isShare, disabled: true },
      // 2 line below cause server not save 2 these field
      // --------
      sourceInput: { value: item.sourceInput, disabled: true },
      isTimeOrgFactor: { value: !!item.isTimeOrgFactor, disabled: true },
      // --------
      // extra data
      isOld: true,
    });
	};

	delete = (rowIndex) => {
		const formCreateTemp = this.fb.array([]);
		this.formCreate.controls.forEach((e) => {
			formCreateTemp.push(e);
		});
		// Set time out to prevent format err
		const timerA = setTimeout(() => {
			this.formCreate.clear();
			const timerB = setTimeout(() => {
				formCreateTemp.controls.forEach((e, i) => {
					if (i !== rowIndex) {
						this.formCreate.push(e);
					}
				});
				clearTimeout(timerB);
			});
			clearTimeout(timerA);
		}, 0);
		// delete list kpi item
		this.listKpiItem.splice(rowIndex, 1);
	};

	save() {
		console.log('this.formCreate', this.formCreate);

		if (this.formCreate.valid && this.formSearch.valid) {
			const listKpiItemInput = [];
			const listAddNew = this.formCreate.value
				?.filter((item) => !item.isOld)
				?.reduce((r, a) => {
					r[a.kpiItemId] = [...(r[a.kpiItemId] || []), a];
					return r;
				}, {});
			// console.log('listAddNew', listAddNew);
			Object.values(listAddNew).forEach((listAddNewGroupByItem: Array<any>) => {
				const targetAttributes = [];
				listAddNewGroupByItem.forEach((element) => {
					targetAttributes.push({
            rate: parseFloat(element.rate.replace(',', '.')),
            limitFloor: element.limitFloor ? parseFloat(element.limitFloor.replace(',', '.')) : element.limitFloor,
            limitCeiling: element.limitCeiling
              ? parseFloat(element.limitCeiling.replace(',', '.'))
              : element.limitCeiling,
            ceilingRatio: element.ceilingRatio
              ? parseFloat(element.ceilingRatio.replace(',', '.'))
              : element.ceilingRatio,
            sourceInput: element.sourceInput,
            expireDate: element.expireDate ? moment(element.expireDate).format(SERVER_DATE_FORMAT) : '',
            efficientDate: moment(element.efficientDate).format(SERVER_DATE_FORMAT),
            isActive: element.isActive,
            isAreaFactor: element.isAreaFactor,
            isKpi: element.isKpi,
            isShare: element.isShare,
            isTimeOrgFactor: element.isTimeOrgFactor,
          });
				});
				// map targetAttributes to kpiItemInput
				const kpiItemInput = {
					kpiItemId: parseFloat(listAddNewGroupByItem[0].kpiItemId),
					categoryCode: listAddNewGroupByItem[0].categoryCode,
					targetAttributes,
				};
				// map kpiItemInput to listKpiItemInput
				listKpiItemInput.push(kpiItemInput);
			});

			const data = {
				...this.formSearch.value,
				kpiItemInput: listKpiItemInput,
			};
			this.confirmService.confirm().then((res) => {
				if (res) {
					this.isLoading = true;
					this.kpiCategoryByTitleApi.createKpiItemRmLevel(data).subscribe(
						() => {
							this.isLoading = false;
							this.back();
							this.messageService.success(this.notificationMessage.success);
						},
						(e) => {
							this.isLoading = false;
							if (e?.status === 0) {
								this.messageService.error(this.notificationMessage.E001);
								return;
							}
							this.messageService.error(e?.error?.description);
						}
					);
				}
			});
		} else {
			this.isValidator = true;
			this.formCreate.controls.forEach((element: FormGroup) => {
				validateAllFormFields(element);
			});
		}
	}
}
