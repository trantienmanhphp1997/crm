import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiRegionFactorCreateComponent } from './kpi-region-factor-create.component';

describe('KpiRegionFactorCreateComponent', () => {
  let component: KpiRegionFactorCreateComponent;
  let fixture: ComponentFixture<KpiRegionFactorCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiRegionFactorCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiRegionFactorCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
