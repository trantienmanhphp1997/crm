import { forkJoin, of } from 'rxjs';
import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { CampaignsService } from 'src/app/pages/campaigns/services/campaigns.service';
import { CommonCategory, FunctionCode } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import * as moment from 'moment';
import { FormArray, FormGroup } from '@angular/forms';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { KpiRegionApi } from '../../apis/kpi-region.api';
import { SERVER_DATE_FORMAT } from '../../constant';

@Component({
  selector: 'app-kpi-region-factor-create',
  templateUrl: './kpi-region-factor-create.component.html',
  styleUrls: ['./kpi-region-factor-create.component.scss'],
})
export class KpiRegionFactorCreateComponent extends BaseComponent implements OnInit {
  blockCode: String;
  isValidator = false;
  listData = [];
  listBranchTypeKPI = [];
  isLoading = false;
  listBlock = [];
  formCreate = this.fb.group({
    kpiFactorSetName: ['Bộ hệ số vùng miền', CustomValidators.required],
    blockCode: 'SME',
    efficientDate: ['', CustomValidators.required],
    expireDate: '',
    kpiFactorDtos: this.fb.array([]),
  });

  minEfficientDate: any;
  maxEfficientDate: any;
  minExpireDate: any;
  isCreate = true;
  listRegion = [];

  constructor(injector: Injector, private campaignService: CampaignsService, private kpiRegionApi: KpiRegionApi) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.KPI_FACTOR_AREA}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.minEfficientDate = new Date(moment().subtract(1, 'month').startOf('month').valueOf());
    this.minExpireDate = new Date(moment().subtract(1, 'month').startOf('month').valueOf());
    this.maxEfficientDate = '';
    forkJoin([
      // get list branch type kpi
      this.commonService.getCommonCategory(CommonCategory.KPI_SYS_ORG_TYPE).pipe(catchError((e) => of(undefined))),
      // get list block
      this.campaignService.getBlockMapping().pipe(catchError((e) => of(undefined))),
      // get list region
      this.commonService.getCommonCategory(CommonCategory.KPI_LOCATION).pipe(catchError((e) => of(undefined))),
    ]).subscribe(([listBranchTypeKPI, listBlock, listRegion]) => {
      this.isLoading = false;
      this.listRegion = listRegion?.content || [];
      this.listBranchTypeKPI = listBranchTypeKPI?.content || [];
      // map list block
      this.listBlock =
        listBlock?.content.map((item) => {
          return { code: item.code, name: item.code + ' - ' + item.name };
        }) || [];
      this.mapData();
    });
  }

  getIndex(col, row) {
    return row * this.listRegion.length + col;
  }

  onChangeEndDate = (event) => {
		const endOfMonth = new Date(moment(event).endOf('month').valueOf());
		this.formCreate.controls?.expireDate?.setValue(endOfMonth);
	};

  mapData() {
    this.listBranchTypeKPI.forEach((branchTypeKPI) => {
      this.listRegion.forEach((region) => {
        (this.formCreate.controls.kpiFactorDtos as FormArray).push(this.createItem(branchTypeKPI, region));
      });
    });

    this.formCreate.controls.efficientDate.valueChanges.subscribe((value) => {
      this.minExpireDate = new Date(moment(value).startOf('day').valueOf());
    });
    this.formCreate.controls.expireDate.valueChanges.subscribe((value) => {
      this.maxEfficientDate = new Date(moment(value).startOf('day').valueOf());
    });
  }

  getErrRequireValue(indexRegion, rowIndex) {
    const controlValue = (
      (this.formCreate.controls.kpiFactorDtos as FormArray).controls[this.getIndex(indexRegion, rowIndex)] as FormGroup
    ).controls;
    return controlValue?.value?.errors?.cusRequired && controlValue.value?.touched && this.isValidator;
  }

  getErrValidateValue(indexRegion, rowIndex) {
    const controlValue = (
      (this.formCreate.controls.kpiFactorDtos as FormArray).controls[this.getIndex(indexRegion, rowIndex)] as FormGroup
    ).controls;
    return controlValue?.value?.errors?.value;
  }

  createItem(branchType, region) {
    return this.fb.group({
      value: ['', [CustomValidators.required, CustomValidators.valueFactor]],
      locationCode: region.code,
      kpiSysOrgTypeCode: branchType.code,
    });
  }

  create() {
    if (this.formCreate.valid) {
      const formData = this.formCreate.value;
      const kpiFactorDtos = formData?.kpiFactorDtos.map((kpiFactorDto) => ({
        ...kpiFactorDto,
        value: parseFloat(kpiFactorDto.value.replace(',', '.')),
      }));
      const data = {
        ...formData,
        efficientDate: moment(new Date(formData?.efficientDate)).format(SERVER_DATE_FORMAT),
        expireDate: formData?.expireDate ? moment(new Date(formData?.expireDate)).format(SERVER_DATE_FORMAT) : '',
        kpiFactorDtos,
      };
      // console.log('data', data);
      this.confirmService.confirm().then((res) => {
        if (res) {
          this.isLoading = true;
          this.kpiRegionApi.saveKpiFactorSet(data).subscribe(
            () => {
              this.isLoading = false;
              this.back();
              this.messageService.success(this.notificationMessage.success);
            },
            (e) => {
              this.isLoading = false;
              if (e?.status === 0) {
                this.messageService.error(this.notificationMessage.E001);
                return;
              }
              this.messageService.error(e?.error?.description);
            }
          );
        }
      });
    } else {
      this.isValidator = true;
      validateAllFormFields(this.formCreate);
      (this.formCreate.controls.kpiFactorDtos as FormArray).controls.forEach((element: FormGroup) => {
        validateAllFormFields(element);
      });
    }
  }
}
