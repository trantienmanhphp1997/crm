import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiCategoryCreateComponent } from './kpi-category-create.component';

describe('KpiCategoryCreateComponent', () => {
  let component: KpiCategoryCreateComponent;
  let fixture: ComponentFixture<KpiCategoryCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiCategoryCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiCategoryCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
