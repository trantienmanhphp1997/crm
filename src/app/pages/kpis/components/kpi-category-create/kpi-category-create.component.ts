import { forkJoin, of } from 'rxjs';
import { Component, OnInit, Injector, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { validateAllFormFields, cleanDataForm } from 'src/app/core/utils/function';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { catchError } from 'rxjs/operators';
import {
  FunctionCode,
  ScreenType,
} from 'src/app/core/utils/common-constants';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as _ from 'lodash';
import { KpiCategoryApi } from '../../apis';
import { LIST_KPI_ASSESSMENT, LIST_KPI_FEATURE_NATURE } from '../../constant';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-kpi-category-create',
  templateUrl: './kpi-category-create.component.html',
  styleUrls: ['./kpi-category-create.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class KpiCategoryCreateComponent extends BaseComponent implements OnInit {
  form = this.fb.group({
    code: ['', [CustomValidators.required, CustomValidators.codeKpi]], // Ma chi tieu
    name: ['', CustomValidators.required], // Ten chi tieu
    categoryCode: ['', CustomValidators.required], // Ma loai chi tieu
    blockCode: ['', CustomValidators.required], // Ma khoi
    type: ['', CustomValidators.required], // Cach danh gia
    unitCode: ['', CustomValidators.required], // Don vi tinh
    feature: ['', CustomValidators.required], // Tinh chat chi tieu
    frequencyCode: ['', CustomValidators.required], // Tan suat
    description: [''], // Mo ta,
    shortedName: ['', [Validators.maxLength(50)]] // Ten viet tat
  });
  kpiCategoryOutputs = [];
  listKpiAssessment = LIST_KPI_ASSESSMENT;
  kpiUnitOutputs = [];
  kpiFeatures = LIST_KPI_FEATURE_NATURE;
  kpiFrequencyOutputs = [];
  isLoading = false;
  isValidator = false;
  listBlock = [];
  branchesCode: string[];

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private kpiCategoryApi: KpiCategoryApi
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.prop = this.sessionService.getSessionData(`${FunctionCode.KPI_ITEM}_${ScreenType.Create}`);
    if (!this.prop) {
      forkJoin([
        this.categoryService.getBlocksCategoryByRm().pipe(catchError((e) => of(undefined))),
        this.kpiCategoryApi.getItemResource().pipe(catchError((e) => of(undefined))),
      ]).subscribe(
        ([
          listBlock,
          itemResources,
        ]) => {
          this.prop = {
            listBlock,
            itemResources,
          };
          this.mapData();
          this.sessionService.setSessionData(`${FunctionCode.KPI_ITEM}_${ScreenType.Create}`, this.prop);
        }
      );
    } else {
      const timer = setTimeout(() => {
        this.mapData();
        clearTimeout(timer);
      }, 500);
    }
  }

  mapData() {
    this.kpiCategoryOutputs = this.prop?.itemResources?.kpiCategoryOutputs || [];
    this.kpiUnitOutputs = this.prop?.itemResources?.kpiUnitOutputs || [];
    this.kpiFrequencyOutputs = this.prop?.itemResources?.kpiFrequencyOutputs || [];
    this.listBlock =
      this.prop?.listBlock?.map((item) => {
        return { code: item.code, name: item.code + ' - ' + item.name };
      }) || [];
    this.isLoading = false;
  }

  save() {

    if (this.form.valid) {
      this.confirmService.confirm().then((res) => {
        if (res) {
          this.isLoading = true;
          const data = cleanDataForm(this.form);
          this.kpiCategoryApi.createKpiItem(data).subscribe(
            () => {
              this.isLoading = false;
              this.back();
              this.messageService.success(this.notificationMessage.success);
            },
            (e) => {
              this.isLoading = false;
              if (e?.status === 0) {
                this.messageService.error(this.notificationMessage.E001);
                return;
              }
              this.messageService.error(e?.error?.description);
            }
          );
        }
      });
    } else {
      this.isValidator = true;
      validateAllFormFields(this.form);
    }
  }
}
