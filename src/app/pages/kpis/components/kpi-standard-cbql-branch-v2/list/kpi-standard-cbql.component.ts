import { forkJoin, of } from 'rxjs';
import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { cleanDataForm } from 'src/app/core/utils/function';
import { CampaignsService } from 'src/app/pages/campaigns/services/campaigns.service';
import { FunctionCode, Scopes, SessionKey } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { KpiStandardCbqlApi } from '../../../apis';
import { CategoryService } from 'src/app/pages/system/services/category.service';

@Component({
  selector: 'app-kpi-standard-cbql-branch-v2',
  templateUrl: './kpi-standard-cbql.component.html',
  styleUrls: ['./kpi-standard-cbql.component.scss'],
})
export class StandardKpiCBQLBranchV2Component extends BaseComponent implements OnInit {
  isLoading = false;
  listData = [];
  listBlock = [];
  formSearch = this.fb.group({
    blockCode: '',
    unitType: '3',
    unit: []
  });
  prevParams: any;
  prop: any;
  listHeaders = [];
  formCreate = this.fb.array([]);
  unitTypes = [
    { text: 'Chi nhánh cấp 2', code: '2' },
    { text: 'Chi nhánh cấp 1', code: '1' },
    { text: 'Vùng', code: '3' },
  ];
  units = [];
  listBranches = [];
  tblHeader = [];

  constructor(
    injector: Injector,
    private categoryService: CategoryService,
    private campaignService: CampaignsService,
    private kpiStandardCbqlApi: KpiStandardCbqlApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.KPI_STANDARD_CBQL_BRANCH_V2}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.prop = this.sessionService.getSessionData(FunctionCode.KPI_STANDARD_CBQL_BRANCH_V2);
    if (this.prop) {
      this.prevParams = { ...this.prop?.prevParams };
      this.formSearch.patchValue(this.prevParams);
      this.listBlock = this.prop?.listBlock || [];
      this.listBranches = this.prop?.listBranches || [];
      this.initUnits();
      this.search();
    } else {
      this.initUnits();
      forkJoin([
        this.campaignService.getBlockMapping().pipe(catchError((e) => of(undefined))),
        this.categoryService.getBranchesOfUser(this.objFunction.rsId, Scopes.VIEW).pipe(catchError((e) => of(undefined))),
      ]).subscribe(([listBlock, branchesOfUser]) => {
        const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
        this.listBlock =
          divisionOfUser?.sort((it1, it2) => it1.order - it2.order)?.map((item) => {
            return { code: item.code, name: `${item.code || ''} - ${item.name || ''}` };
          }) || [];
        this.formSearch.controls.blockCode.setValue(_.head(this.listBlock)?.code);
        this.listBranches = branchesOfUser;
        this.prop = {
          ...this.prop,
          listBlock: this.listBlock,
          listBranches: this.listBranches
        }
        this.sessionService.setSessionData(FunctionCode.KPI_STANDARD_CBQL_BRANCH_V2, this.prop);
        this.search(true);
      }
      );
    }
  }

  ngAfterViewInit(): void {
    this.formSearch.controls.unitType.valueChanges.subscribe((value) => {
      this.initUnits();
      this.listData = [];
      this.search(true);
    });
  }

  initUnits() {
    this.formSearch.controls.unit.setValue([]);
    this.initTableHeader();
    let { unitType } = this.formSearch.getRawValue();
    let existed = [];
    let res = [];
    this.listBranches.forEach((item) => {
      let parentCode = item.parentCode;
      let code = item.code;
      let name = item.name;
      let region = item.khuVucM;
      let unit = { code: code, name: code + ' - ' + name };

      switch (unitType) {
        case '3':
          unit = { code: region, name: region };
          if (!existed.includes(region)) {
            existed.push(region);
            res.push(unit);
          }
          break;
        case '1':
          if (code === parentCode) {
            res.push(unit);
          }
          break;
        case '2':
          res.push(unit);
          break;
      }
    });
    this.units = res;
  }

  initTableHeader() {
    let { unitType } = this.formSearch.getRawValue();
    this.tblHeader = unitType === '3' ?
      [
        this.fields.hrisCode,
        this.fields._directorName
      ]
      : [
        this.fields._branchCode,
        this.fields._branchName
      ];
    this.tblHeader.push(this.fields.block);
  }

  search(isSearch?: boolean) {
    this.isLoading = true;
    let params: any = {};
    if (isSearch) {
      params = cleanDataForm(this.formSearch);
    } else {
      if (!this.prevParams) {
        return;
      }
      params = this.prevParams;
    }

    let paramsSearch = {
      rsId: this.objFunction.rsId,
      scope: Scopes.VIEW,
      blockCode: _.get(params, 'blockCode'),
      type: Number(_.get(params, 'unitType')),
      branchCodes: _.get(params, 'unit') || []
    }

    this.prevParams = params;
    this.kpiStandardCbqlApi.searchKpiBranchArea(paramsSearch).subscribe(
      (result) => {
        if (result) {
          this.prop = {
            ...this.prop,
            prevParams: params,
            listBlock: this.listBlock,
          };
          this.listData = result;
        }
        this.sessionService.setSessionData(FunctionCode.KPI_STANDARD_CBQL_BRANCH_V2, this.prop);
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  import() {
    this.router.navigate([this.router.url, 'import'], {state: this.prevParams});
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      this.prop = {
        ...this.prop,
        item,
      }
      this.sessionService.setSessionData(FunctionCode.KPI_STANDARD_CBQL_BRANCH_V2, this.prop);
      this.router.navigate([this.router.url, 'detail']);
    }
  }

  get allowImport() {
    return this.objFunction.scopes.includes(Scopes.IMPORT);
  }

}
