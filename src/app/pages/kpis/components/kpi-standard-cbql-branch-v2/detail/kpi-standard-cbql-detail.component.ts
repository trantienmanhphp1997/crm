import { of } from 'rxjs';
import { ChangeDetectorRef, Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { catchError } from 'rxjs/operators';
import { CODE_FIRST_6M, LIST_MONTH, LIST_SIX_MONTH, LIST_YEAR } from '../../../constant';
import { KpiStandardCbqlApi, KpiStandardRmApi } from '../../../apis';
import * as moment from 'moment';
import { FunctionCode, functionUri } from 'src/app/core/utils/common-constants';
import { Utils } from 'src/app/core/utils/utils';
import { FileService } from 'src/app/core/services/file.service';
import { formatNumber } from '@angular/common';

@Component({
  selector: 'app-kpi-standard-cbql-branch-v2-detail',
  templateUrl: './kpi-standard-cbql-detail.component.html',
  styleUrls: ['./kpi-standard-cbql-detail.component.scss'],
})
export class KpiStandardCbqlBranchV2DetailComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listBlock = [];
  formSearch = this.fb.group({
    year: parseInt(moment().format('yyyy')),
  });
  listGroup = [];
  listLevel = [];
  listKpiItem = [];
  listGroupByCategoryType = [];
  listKpiItemMapping = [];
  listYear = LIST_YEAR;
  listMonth = LIST_MONTH;
  dataFromView: any;
  pageInfo;

  constructor(
    injector: Injector,
    private cd: ChangeDetectorRef,
    private kpiStandardCbqlApi: KpiStandardCbqlApi,
    private fileService: FileService
  ) {
    super(injector);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.dataFromView = this.sessionService.getSessionData(FunctionCode.KPI_STANDARD_CBQL_BRANCH_V2)?.item;
    this.search();
  }

  ngAfterViewInit(): void {
    this.formSearch.controls.year.valueChanges.subscribe(() => {
      this.search();
    });
  }

  search = async () => {
    let { year } = this.formSearch.getRawValue();
    let params = {
      ... this.dataFromView,
      year
    }
    delete params.name;
    this.isLoading = true;
    this.kpiStandardCbqlApi.detailKpiBranchArea(params).subscribe(
      (res) => {
        this.listKpiItemMapping = [];
        // group by categoryCode
        let listGroupByCategory = Object.values(
          res?.reduce(
            (r, a) => ({
              ...r,
              [a.categoryCode]: [...(r[a.categoryCode] || []), a],
            }),
            {}
          )
        );
        listGroupByCategory.forEach((e: Array<any>) => {
          // first level nested
          this.listKpiItemMapping.push({
            idGroup: e[0]?.categoryCode,
            name: e[0]?.categoryName,
            treeStatus: 'expanded',
            parentId: null,
          });


          e.forEach((i) => {
            // last level nested
            let kpis = {};
            for (const key in i) {
              if (key.includes('m')) {
                kpis[key] = _.get(i, key, '');
              }
            }
            this.listKpiItemMapping.push({
              ...i,
              // idGroup is required cause prevent render error
              idGroup: i.itemCode,
              name: i.itemName,
              treeStatus: 'disabled',
              parentId: i.categoryCode,
              unit: i?.unit,
              kpis,
            });
          });
        });
        this.isLoading = false;
      },
      (err) => {
        const message = err.error ? JSON.parse(err.error).description : _.get(this.notificationMessage, 'error');
        this.messageService.error(message);
        this.isLoading = false;
      },
    );
  };

  onTreeAction(event: any) {
    const row = event.row;

    if (row.treeStatus === 'collapsed') {
      row.treeStatus = 'expanded';
    } else {
      row.treeStatus = 'collapsed';
    }

    this.listKpiItemMapping = [...this.listKpiItemMapping];
    this.cd.detectChanges();
  }

  export() {
    if (!this.isLoading) {
      let { year } = this.formSearch.getRawValue();
      let params = {
        ... this.dataFromView,
        year
      }
      delete params.name;
      this.isLoading = true;

      this.kpiStandardCbqlApi.exportDetailKpiBranchArea(params).subscribe(
        (res) => {
          if (Utils.isStringNotEmpty(res)) {
            this.download(res, 'Chi tiết KPI tiêu chuẩn');
          } else {
            this.messageService.error(_.get(this.notificationMessage, 'export_error'));
            this.isLoading = false;
          }
        }, (err) => {
          const message = err.error ? JSON.parse(err.error).description : _.get(this.notificationMessage, 'export_error');
          this.messageService.error(message);
          this.isLoading = false;
        }
      );
    }
  }

  download(fileId: string, fileName: string) {
    this.fileService.downloadFile(fileId, `${fileName}.xlsx`).subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    }, (err) => {
      const message = err.error ? JSON.parse(err.error).description : _.get(this.notificationMessage, 'error');
      this.messageService.error(message);
    });
  }

  formatKpiValue(value) {
    return Utils.isNull(value) ? '' : formatNumber(value, 'en', '1.0-2');
  }

}
