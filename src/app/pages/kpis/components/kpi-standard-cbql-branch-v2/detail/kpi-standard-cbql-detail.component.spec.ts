import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiStandardCbqlBranchV2DetailComponent } from './kpi-standard-cbql-detail.component';

describe('KpiStandardCbqlDetailComponent', () => {
  let component: KpiStandardCbqlBranchV2DetailComponent;
  let fixture: ComponentFixture<KpiStandardCbqlBranchV2DetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiStandardCbqlBranchV2DetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiStandardCbqlBranchV2DetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
