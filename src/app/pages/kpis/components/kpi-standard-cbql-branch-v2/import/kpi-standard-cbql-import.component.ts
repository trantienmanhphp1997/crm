import { formatDate } from '@angular/common';
import { global } from '@angular/compiler/src/util';
import { Component, HostBinding, Injector, OnInit, ViewChild } from '@angular/core';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import _ from 'lodash';
import { BaseComponent } from 'src/app/core/components/base.component';
import { FileService } from 'src/app/core/services/file.service';
import { SessionKey, typeExcel } from 'src/app/core/utils/common-constants';
import { Utils } from 'src/app/core/utils/utils';
import { KpiStandardCbqlApi } from '../../../apis';

@Component({
  selector: 'standard-kpi-cbql-branch-v2-import',
  templateUrl: './kpi-standard-cbql-import.component.html',
  styleUrls: ['./kpi-standard-cbql-import.component.scss'],
})
export class StandardKpiCBQLBranchV2ImportComponent extends BaseComponent implements OnInit {
  @ViewChild('tableSuccess') tableSuccess: DatatableComponent;
  @ViewChild('tableError') tableError: DatatableComponent;

  constructor(
    injector: Injector,
    private kpiStandardCbqlApi: KpiStandardCbqlApi,
    private fileService: FileService,
  ) {
    super(injector);
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
  }
  @HostBinding('class.app__right-content') appRightContent = true;
  notificationMessage: any;
  isLoading = false;
  fileName: string;
  isFile = false;
  fileImport: File;
  files: any;
  ColumnMode = ColumnMode;

  prop: any;
  limit = global.userConfig.pageSize;
  fileId: string;
  isUpload = false;
  uploaded = false;
  messages = global.messageTable;
  listBlock = [];
  formSearch = this.fb.group({
    blockCode: '',
    unitType: '3',
    time: new Date()
  });
  formCreate = this.fb.array([]);
  unitTypes = [
    { text: 'Chi nhánh cấp 2', code: '2' },
    { text: 'Chi nhánh cấp 1', code: '1' },
    { text: 'Vùng', code: '3' },
  ];
  listLevel = [];
  // Data from file
  listSizeHeaders = [];
  listHeaders = [];
  listData = [];
  allData = [];
  generalInfo: any;
  tblHeader = [];

  searchSuccessDone;
  paramSuccess = {
    size: this.limit,
    page: 0,
    requestId: '',
  };
  listDataSuccess = [];
  pageSuccess;
  prevSuccessParams = this.paramSuccess;

  paramError = {
    size: this.limit,
    page: 0,
    requestId: '',
  };
  listDataError = [];
  searchErrorDone;
  pageError;
  prevErrorParams;

  ngOnInit(): void {
    this.initHeader();
    this.generalInfo = {};
    const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
    this.listBlock =
      divisionOfUser?.sort((it1, it2) => it1.order - it2.order)?.map((item) => {
        return { code: item.code, name: `${item.code || ''} - ${item.name || ''}` };
      }) || [];

    let blockCode = _.get(this.prop, 'blockCode', '');
    let type = _.get(this.prop, 'unitType', '');

    this.formSearch.controls.blockCode.setValue(blockCode ? blockCode : _.head(this.listBlock)?.code);
    if(type){
      this.formSearch.controls.unitType.setValue(type);
    }
  }
  ngAfterViewInit(): void {
    this.formSearch.controls.unitType.valueChanges.subscribe((val) => {
      this.initHeader();
    });

  }

  handleFileInput(files) {
    if (files?.item(0)?.size > 10485760) {
      this.messageService.warn(this.notificationMessage.ECRM005);
      return;
    }
    if (files?.item(0) && !typeExcel.includes(files?.item(0)?.type)) {
      this.messageService.error(this.notificationMessage.CANNOT_READ_DATA_FROM_FILE);
      return;
    }

    if (_.size(files) > 0) {
      this.isFile = true;
      this.fileImport = files.item(0);
      this.fileName = files.item(0).name;
    } else {
      this.isFile = false;
    }
  }

  downloadTemplate() {
    if (!this.isLoading) {
      const { blockCode, unitType, time } = this.formSearch.getRawValue();
      let dateImport = formatDate(time, 'MM/yyyy', 'en')?.split('/');
      JSON.stringify({ month: dateImport[0], year: dateImport[1], blockCode: blockCode, type: unitType })
      let params = {
        month: Number(dateImport[0]),
        year: Number(dateImport[1]),
        blockCode: blockCode,
        branchCodeList: [],
        type: Number(unitType)
      }
      this.isLoading = true;

      this.kpiStandardCbqlApi.exportTemplatteKpiBranchArea(params).subscribe(
        (res) => {
          if (Utils.isStringNotEmpty(res)) {
            this.download(res, `Template nhập KPI tiêu chuẩn CBQL (${this.unitTypes.find(item => item.code == unitType)?.text})`);
          } else {
            this.messageService.error(_.get(this.notificationMessage, 'export_error'));
            this.isLoading = false;
          }
        }, (err) => {
          const message = err.error ? JSON.parse(err.error).description : _.get(this.notificationMessage, 'export_error');
          this.messageService.error(message);
          this.isLoading = false;
        }
      );
    }
  }

  importFile() {
    if (this.isFile && !this.isUpload) {
      const { blockCode, unitType, time } = this.formSearch.getRawValue();
      this.isUpload = true;
      this.isLoading = true;
      const formData: FormData = new FormData();
      let dateImport = formatDate(time, 'MM/yyyy', 'en')?.split('/');
      formData.append('file', this.fileImport);
      formData.append(
        'dto',
        JSON.stringify({ month: dateImport[0], year: dateImport[1], blockCode: blockCode, type: unitType })
      );
      this.kpiStandardCbqlApi.importKpiBranch(formData).subscribe(
        (res) => {
          this.uploaded = true;
          let parsed;
          try {
            parsed = JSON.parse(res);
          } catch (error) { }

          if (!parsed) {
            // Import & validate thành công
            this.paramSuccess.requestId = res;
            this.paramError.requestId = res;
            this.searchSuccess(true);
            this.searchError(true);
            this.isUpload = false;
          } else {
            this.isLoading = false;
            this.messageService.error('Lỗi');
          }
        }, (err) => {
          const message = err.error ? JSON.parse(err.error).description : _.get(this.notificationMessage, 'error');
          this.messageService.error(message);
          this.isLoading = false;
        }
      );
    }
  }


  searchSuccess(isSearch: boolean) {
    this.isLoading = true;
    this.searchSuccessDone = false;
    let params: any = this.paramSuccess;
    if (isSearch) {
      this.paramSuccess.page = 0;
      params = this.paramSuccess;
    }
    this.kpiStandardCbqlApi.getListPassBranchArea(params).subscribe(
      (res) => {
        this.prevSuccessParams = params;
        this.listDataSuccess = _.get(res, 'content') || [];
        this.pageSuccess = {
          totalElements: _.get(res, 'totalElements'),
          totalPages: _.get(res, 'totalPages'),
          currentPage: _.get(res, 'number'),
          size: _.get(global, 'userConfig.pageSize'),
        };
        this.searchSuccessDone = true;
        if (this.searchErrorDone && this.searchSuccessDone) {
          this.isLoading = false;
          this.uploaded = true;
        }
      },
      () => {
        this.searchSuccessDone = true;
        if (this.searchErrorDone && this.searchSuccessDone) {
          this.isLoading = false;
        }
      }
    );
  }

  searchError(isSearch: boolean) {
    this.isLoading = true;
    this.searchErrorDone = false;
    let params: any = this.paramError;
    if (isSearch) {
      this.paramError.page = 0;
      params = this.paramError;
    }
    this.kpiStandardCbqlApi.getListFailBranchArea(params).subscribe(
      (res) => {
        this.listDataError = _.get(res, 'content') || [];
        this.pageError = {
          totalElements: _.get(res, 'totalElements'),
          totalPages: _.get(res, 'totalPages'),
          currentPage: _.get(res, 'number'),
          size: _.get(global, 'userConfig.pageSize')
        };

        this.searchErrorDone = true;
        if (this.searchErrorDone && this.searchSuccessDone) {
          this.isLoading = false;
          this.uploaded = true;
        }
      },
      () => {
        this.searchErrorDone = true;
        if (this.searchErrorDone && this.searchSuccessDone) {
          this.isLoading = false;
        }
      }
    );
  }

  exportError() {
    if (!this.isLoading) {
      this.isLoading = true;

      this.kpiStandardCbqlApi.exportListFailBranchArea({
        requestId: this.paramSuccess.requestId
      }).subscribe(
        (res) => {
          if (Utils.isStringNotEmpty(res)) {
            this.download(res, 'Danh sách lỗi');
          } else {
            this.messageService.error(_.get(this.notificationMessage, 'export_error'));
            this.isLoading = false;
          }
        }, (err) => {
          const message = err.error ? JSON.parse(err.error).description : _.get(this.notificationMessage, 'export_error');
          this.messageService.error(message);
          this.isLoading = false;
        }
      );
    }
  }

  download(fileId: string, fileName: string) {
    this.fileService.downloadFile(fileId, `${fileName}.xlsx`).subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    }, (err) => {
      const message = err.error ? JSON.parse(err.error).description : _.get(this.notificationMessage, 'error');
      this.messageService.error(message);
    });
  }

  clearFile() {
    this.isUpload = false;
    this.isFile = false;
    this.fileName = null;
    this.fileImport = null;
    this.files = null;
    this.listDataError = [];
    this.fileId = undefined;

    // Data before write
    this.listSizeHeaders = [];
    this.listHeaders = [];
    this.listData = [];
    this.allData = [];
    this.generalInfo = {};
  }


  save() {
    this.kpiStandardCbqlApi.saveKpiBranchArea({
      requestId: this.paramSuccess.requestId
    }).subscribe((res) => {
      this.messageService.success(_.get(this.notificationMessage, 'success'));
      if (this.listDataError.length === 0) {
        setTimeout(() => {
          this.back();
        }, 1500);
      }
    }, (err) => {
      const message = err.error ? JSON.parse(err.error).description : _.get(this.notificationMessage, 'error');
      this.messageService.error(message);
      this.isLoading = false;
    });
  }

  back() {
    this.location.back();
  }

  initHeader() {
    let { unitType } = this.formSearch.getRawValue();
    this.tblHeader = unitType === '3' ?
      [
        this.fields.hrisCode,
        this.fields._directorName
      ]
      : [
        this.fields._branchCode,
        this.fields._branchName
      ];
    this.tblHeader.push(this.fields.block);
  }
  get isRegion() {
    let { unitType } = this.formSearch.getRawValue();
    return unitType === '3';
  }

  setPageSuccessTbl(pageInfo) {
    this.paramSuccess.page = pageInfo?.offset;
    this.searchSuccess(false);
  }

  setPageErrorTbl(pageInfo) {
    this.paramError.page = pageInfo?.offset;
    this.searchError(false);
  }

}
