import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {KpiStandardRmComponent} from './kpi-standard-rm.component';

describe('KpiStandardRmComponent', () => {
	let component: KpiStandardRmComponent;
	let fixture: ComponentFixture<KpiStandardRmComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [KpiStandardRmComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(KpiStandardRmComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
