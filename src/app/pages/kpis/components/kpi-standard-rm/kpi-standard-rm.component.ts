import { forkJoin, of } from 'rxjs';
import { ChangeDetectorRef, Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { CampaignsService } from 'src/app/pages/campaigns/services/campaigns.service';
import { FunctionCode } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { LIST_COLUMN_VALUE, LIST_MONTH, LIST_YEAR, SERVER_DATE_FORMAT, SOURCE_INPUT } from '../../constant';
import { KpiAssignmentApi, KpiStandardRmApi, LevelApi, TitleGroupApi } from '../../apis';
import * as moment from 'moment';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-kpi-standard-rm',
  templateUrl: './kpi-standard-rm.component.html',
  styleUrls: ['./kpi-standard-rm.component.scss'],
})
export class KpiStandardRmComponent extends BaseComponent implements OnInit {
  isLoading = false;
  yearAfterSearch = moment().year();
  listBlock = [];
  formSearch = this.fb.group({
    blockCode: '',
    rmTitleCode: '',
    rmLevelCode: '',
    month: parseInt(moment().format('MM')),
    year: parseInt(moment().format('yyyy')),
  });
  formCreate = this.fb.array([]);
  listGroup = [];
  listLevel = [];
  listKpiItem = [];
  listGroupByCategoryType = [];
  listKpiItemMapping = [];
  listYear = LIST_YEAR;
  listMonth = LIST_MONTH;
  listColumnValue = [];
  codeSelected = '';
  listKpiFrequency = ['KPI_FREQUENCY_1', 'KPI_FREQUENCY_2'];

  constructor(
    injector: Injector,
    private campaignService: CampaignsService,
    private kpiStandardRmApi: KpiStandardRmApi,
    private cd: ChangeDetectorRef,
    private titleGroupApi: TitleGroupApi,
    private rmLevelApi: LevelApi,
    private kpiAssignmentApi: KpiAssignmentApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.KPI_STANDARD_RM}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    forkJoin([this.campaignService.getBlockMapping().pipe(catchError((e) => of(undefined)))]).subscribe(
      async ([listBlock]) => {
        // map list block
        this.listBlock =
          listBlock?.content?.map((item) => {
            return { code: item.code, name: item.code + ' - ' + item.name };
          }) || [];
        let data;
        if (this.listBlock.length) {
          data = await this.getGroupAndLevelBlockCode(this.listBlock[0].code);
        }
        this.listGroup = this.mapListGroup(data.listGroup);
        this.listLevel = data.listLevel;

        // Set default value
        const objectData = {
          blockCode: this.listBlock?.[0]?.code,
          rmTitleCode: this.listGroup?.[0]?.code,
          rmLevelCode: this.listLevel?.[0]?.code,
        };
        this.formSearch.setValue({ ...this.formSearch.value, ...objectData });

        this.isLoading = false;
        this.onSearchKpiStandardRm();
      }
    );
  }

  getColName = (colObject) => {
    if (colObject.code === 'N') {
      return `${colObject.name} ${this.yearAfterSearch}`;
    } else {
      return colObject.name;
    }
  };

  getGroupAndLevelBlockCode = async (code) => {
    let listGroup = [];
    let listLevel = [];
    listGroup =
      (
        await this.titleGroupApi
          .searchGroupByBlockCode(code)
          .pipe(catchError((e) => of(undefined)))
          .toPromise()
      )?.content?.filter((item) => !item.isManagerGroup) || [];
    if (listGroup?.length) {
      listLevel =
        (
          await this.rmLevelApi
            .searchLevelByGroupCode(listGroup[0].id, code)
            .pipe(catchError((e) => of(undefined)))
            .toPromise()
        )?.content || [];
    }

    return { listGroup, listLevel };
  };

  async onChangeBlockCode() {
    const blockCode = this.formSearch.value.blockCode;
    const data = await this.getGroupAndLevelBlockCode(blockCode);
    this.listGroup = this.mapListGroup(data.listGroup);
    this.listLevel = data.listLevel;
    // Re-set value title and level
    const objectData = {
      rmTitleCode: this.listGroup[0]?.code || '',
      rmLevelCode: this.listLevel[0]?.code || '',
    };
    this.formSearch.setValue({ ...this.formSearch.value, ...objectData });
  }

  async onChangeGroup() {
    const blockCode = this.formSearch.value.blockCode;
    const groupCode = this.formSearch.value.rmTitleCode;
    const groupSelected = this.listGroup.find((group) => group.code === groupCode);
    this.listLevel =
      (
        await this.rmLevelApi
          .searchLevelByGroupCode(groupSelected?.id, blockCode)
          .pipe(catchError((e) => of(undefined)))
          .toPromise()
      )?.content || [];
    // Re-set value rmLevelCode
    const objectData = {
      rmLevelCode: this.listLevel[0]?.code || '',
    };
    this.formSearch.setValue({ ...this.formSearch.value, ...objectData });
  }

  mapListGroup = (listGroup) => {
    return listGroup?.map((item) => ({ ...item, name: item.blockCode + ' - ' + item.name }));
  };

  onTreeAction(event: any) {
    const row = event.row;

    if (row.treeStatus === 'collapsed') {
      row.treeStatus = 'expanded';
    } else {
      row.treeStatus = 'collapsed';
    }

    this.listKpiItemMapping = [...this.listKpiItemMapping];
    this.cd.detectChanges();
  }

  editColumn = (code) => {
    this.isLoading = true;
    this.kpiAssignmentApi.getAggregteMonthMax().subscribe(
      (res) => {
        this.isLoading = false;
        const month = LIST_COLUMN_VALUE.find((item) => item.code === code).month;
        const isNeedCheck = month >= 1 && month <= 12;
        if (
          isNeedCheck &&
          (res.year > this.yearAfterSearch || (res.year == this.yearAfterSearch && month <= res.month))
        ) {
          this.confirmService.confirm(this.fields.confirm_aggregte).then((res) => {
            if (res) {
              this.confirmEditColumn(code);
            }
          });
        } else {
          this.confirmEditColumn(code);
        }
      },
      (e) => {
        this.isLoading = false;
        if (e?.status === 0) {
          this.messageService.error(this.notificationMessage.E001);
          return;
        }
        this.messageService.error(e?.error?.description);
      }
    );
  };

  confirmEditColumn = (code) => {
    console.log('Ádasd', this.codeSelected);
    this.codeSelected = code;
    this.formCreate.controls.forEach((item: FormGroup, index) => {
      const target = this.listKpiItemMapping[index]?.mappingTargets?.find(
        (target: any) => target.code === this.codeSelected
      );
      target && item.setValue({ ...item.value, value: target?.value || 0 });
    });
  };

  onSearchKpiStandardRm = async () => {
    this.formCreate.clear();
    const params = this.formSearch.value;
    params.isInputKpiRm = true;
    this.isLoading = true;
    this.listColumnValue = LIST_COLUMN_VALUE.map((i) => {
      return { ...i, divisionCode: this.formSearch.controls.blockCode.value };
    });

    const listKpiByTitle =
      (
        await this.kpiStandardRmApi
          .searchKpiStandard(params)
          .pipe(catchError((e) => of(undefined)))
          .toPromise()
      )?.sort((a, b) => (a.categoryCode >= b.categoryCode ? 1 : -1)) || [];
    this.listKpiItemMapping = [];
    // group by categoryCode
    const listGroupByCategoryType = Object.values(
      listKpiByTitle?.reduce(
        (r, a) => ({
          ...r,
          [a.categoryCode]: [...(r[a.categoryCode] || []), a],
        }),
        {}
      )
    );

    listGroupByCategoryType.forEach((e: Array<any>) => {
      // first level nested
      this.listKpiItemMapping.push({
        idGroup: e[0]?.categoryCode,
        name: e[0]?.categoryName,
        treeStatus: 'expanded',
        parentId: null,
      });
      this.formCreate.push(this.fb.group({}));

      e.forEach((i) => {
        const frequencyNames = [];
        i.frequencies?.length && i.frequencies.forEach((element) => frequencyNames.push(element.frequencyName));
        const sourceInputName = SOURCE_INPUT.find((source) => source.code == i.sourceInput)?.name || ''; // == cause different type
        // last level nested
        this.listKpiItemMapping.push({
          ...i,
          // idGroup is required cause prevent render error
          idGroup: i.kpiItemRmLevelId,
          name: i.kpiItemName,
          treeStatus: 'disabled',
          parentId: i.categoryCode,
          frequency: frequencyNames.join(', '),
          rate: i.rate.toString().replace('.', ','),
          sourceInput: sourceInputName,
          mappingTargets: this.listColumnValue.map((col) => {
            const data = i.targets?.find((target) => col.month === target.month);
            return {
              ...col,
              ...data,
              value: data?.value.toString().replace('.', ','),
            };
          }),
        });
        console.log('this.listKpiItemMapping', this.listKpiItemMapping[0].mappingTargets);

        this.formCreate.push(
          this.fb.group({
            value: [0, [CustomValidators.required, CustomValidators.standardKpi]],
            // extra data
            kpiStandardId: i.kpiStandardId,
            kpiItemRmLevelId: i.kpiItemRmLevelId,
            kpiItemId: i.kpiItemId,
            rmLevelId: i.rmLevelId,
            rmLevelCode: i.rmLevelCode,
            rmTitleId: i.rmTitleId,
            rmTitleCode: i.rmTitleCode,
          })
        );
      });
    });
    this.isLoading = false;
    this.yearAfterSearch = this.formSearch.value.year;
  };

  isBoolean = (value) => typeof value === 'boolean';

  back() {
    this.codeSelected = '';
  }

  getIsInActiveTime(row, col) {
    const efficientYear = parseInt(moment(row.efficientDate, SERVER_DATE_FORMAT).format('YYYY'));
    const expireYear = row.expireDate ? parseInt(moment(row.expireDate, SERVER_DATE_FORMAT).format('YYYY')) : 0;
    let efficientMonth = parseInt(moment(row.efficientDate, SERVER_DATE_FORMAT).format('MM'));
    let expireMonth = row.expireDate ? parseInt(moment(row.expireDate, SERVER_DATE_FORMAT).format('MM')) : 0;

    // Big case 1: this.yearAfterSearch > efficientYear
    if (this.yearAfterSearch > efficientYear) {
      if (!expireYear || this.yearAfterSearch < expireYear) {
        return true;
      } else if (this.yearAfterSearch == expireYear) {
        efficientMonth = 0;
      } else {
        return false;
      }
      // Big case 2: this.yearAfterSearch = efficientYear
    } else if (this.yearAfterSearch == efficientYear) {
      if (!expireYear || this.yearAfterSearch < expireYear) {
        expireMonth = 0;
      } else if (this.yearAfterSearch == expireYear) {
        // efficientMonth & expireMonth not change
      } else {
        return false;
      }
      // Big case 3: this.yearAfterSearch < efficientYear
    } else {
      return false;
    }

    const month = LIST_COLUMN_VALUE.find((item) => col.code === item.code)?.month;
    if (!month) return true;
    // 6 month first
    if (month === 13) {
      return efficientMonth <= 6;
    }
    // 6 month last
    if (month === 14) {
      return !expireMonth || expireMonth > 6;
    }
    // each month
    return efficientMonth <= month && (!expireMonth || expireMonth >= month);
  }

  save() {
    if (this.formCreate.valid) {
      const data = [];
      this.formCreate.value.forEach((element, index) => {
        if (
          element.kpiItemRmLevelId &&
          this.getIsInActiveTime(this.listKpiItemMapping[index], { code: this.codeSelected })
        ) {
          data.push({
            ...element,
            value: typeof element.value === 'number' ? element.value : parseFloat(element.value.replace(',', '.')),
            month: LIST_COLUMN_VALUE.find((item) => item.code === this.codeSelected)?.month,
            year: this.yearAfterSearch,
          });
        }
      });

      this.confirmService.confirm().then((res) => {
        if (res) {
          this.isLoading = true;
          this.kpiStandardRmApi.createKpiStandard(data).subscribe(
            () => {
              this.isLoading = false;
              this.back();
              this.onSearchKpiStandardRm();
              this.messageService.success(this.notificationMessage.success);
            },
            (e) => {
              this.isLoading = false;
              if (e?.status === 0) {
                this.messageService.error(this.notificationMessage.E001);
                return;
              }
              this.messageService.error(e?.error?.description);
            }
          );
        }
      });
    } else {
      this.formCreate.controls.forEach((element: FormGroup) => {
        validateAllFormFields(element);
      });
    }
  }

  checkEditCib(row) {
    if (row.divisionCode !== 'CIB') {
      return row.month !== 13 && row.month !== 14;
    } else {
      return true;
    }
  }

  checkEditKPI(row, value) {
    if (
      row?.frequencies?.filter((item) => this.listKpiFrequency.includes(item.frequencyCode))?.length > 0 &&
      value?.code !== '6T1' &&
      value?.code !== '6T2'
    ) {
      return true;
    } else if (
      row?.frequencies[0]?.frequencyCode === 'KPI_FREQUENCY_4' &&
      (value?.code === '6T1' || value?.code === '6T2')
    ) {
      return true;
    } else if (value?.code === 'N') {
      return true;
    } else {
      return false;
    }
  }
}
