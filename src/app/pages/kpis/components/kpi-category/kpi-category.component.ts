import { forkJoin, of } from 'rxjs';
import { global } from '@angular/compiler/src/util';
import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';
import { cleanDataForm } from 'src/app/core/utils/function';
import { CampaignsService } from 'src/app/pages/campaigns/services/campaigns.service';
import { FunctionCode } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { LIST_ALL_KPI_ASSESSMENT, LIST_ALL_KPI_STATUS, LIST_KPI_ASSESSMENT } from '../../constant';
import { KpiCategoryApi } from '../../apis/kpi-category.api';

@Component({
  selector: 'app-kpi-category',
  templateUrl: './kpi-category.component.html',
  styleUrls: ['./kpi-category.component.scss'],
})
export class KpiCategoryComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listData = [];
  kpiCategoryOutputs = [];
  listBlock = [];
  listKpiStatus = LIST_ALL_KPI_STATUS;
  listKpiAssessment = LIST_ALL_KPI_ASSESSMENT;
  formSearch = this.fb.group({
    code: '', // Ma chi tieu
    name: '', // Ten chi tieu
    categoryCode: '', // Ma loai chi tieu
    blockCode: '', // Ma khoi
    type: '', // Cach danh gia
    isActive: '', // Trang Thai
  });
  paramSearch = {
    pageSize: global.userConfig.pageSize,
    pageNumber: 0,
  };
  prevParams: any;
  pageable: Pageable;
  prop: any;

  constructor(injector: Injector, private campaignService: CampaignsService, private kpiCategoryApi: KpiCategoryApi) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.KPI_ITEM}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.prop = this.sessionService.getSessionData(FunctionCode.KPI_ITEM);
    if (this.prop) {
      this.prevParams = { ...this.prop?.prevParams };
      this.paramSearch.pageNumber = this.prevParams?.pageNumber;
      this.formSearch.patchValue(this.prevParams);
      this.kpiCategoryOutputs = this.prop?.kpiCategoryOutputs || [];
      this.listBlock = this.prop?.listBlock || [];
      this.search();
    } else {
      forkJoin([
        this.campaignService.getBlockMapping().pipe(catchError((e) => of(undefined))),
        this.kpiCategoryApi.getItemResource().pipe(catchError((e) => of(undefined))),
      ]).subscribe(([listBlock, itemResources]) => {
        this.kpiCategoryOutputs = [{ code: '', name: 'Tất cả' }, ...(itemResources?.kpiCategoryOutputs || [])];
        // map list block
        const blockTypes =
          listBlock?.content.map((item) => {
            return { code: item.code, name: item.code + ' - ' + item.name };
          }) || [];
        this.listBlock = [{ code: '', name: 'Tất cả' }, ...blockTypes];

        this.prop = {
          listBlock: this.listBlock,
          kpiCategoryOutputs: this.kpiCategoryOutputs,
        };

        this.sessionService.setSessionData(FunctionCode.KPI_ITEM, this.prop);
        this.search(true);
      });
    }
  }

  search(isSearch?: boolean) {
    this.isLoading = true;
    let params: any = {};
    if (isSearch) {
      this.paramSearch.pageNumber = 0;
      params = cleanDataForm(this.formSearch);
    } else {
      if (!this.prevParams) {
        return;
      }
      params = this.prevParams;
    }
    Object.keys(this.paramSearch).forEach((key) => {
      params[key] = this.paramSearch[key];
    });
    if (params.isActive === '') {
      delete params.isActive;
    }
    this.kpiCategoryApi.search(params).subscribe(
      (result) => {
        if (result) {
          this.prevParams = params;
          this.prop.prevParams = params;
          this.listData =
            result?.content?.map((item) => {
              const block = this.listBlock?.find((element) => element.code === item.blockCode);
              const assessment = LIST_KPI_ASSESSMENT.find(
                (element) => element.code?.toString() === item.type?.toString()
              );
              return { ...item, blockName: block?.name, assessmentName: assessment?.name };
            }) || [];
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: global.userConfig.pageSize,
          };
        }
        this.sessionService.setSessionData(FunctionCode.KPI_ITEM, this.prop);
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.pageNumber = pageInfo.offset;
    this.search(false);
  }

  create() {
    this.router.navigate([this.router.url, 'create']);
  }

  update(item) {
    this.router.navigate([this.router.url, 'update', item.kpiItemId]);
  }

  delete(item) {
    this.confirmService
      .confirm()
      .then((res) => {
        console.log('res', res);
        if (res) {
          this.isLoading = true;
          this.kpiCategoryApi.deleteKpiItem(item.kpiItemId).subscribe(
            () => {
              this.isLoading = false;
              this.search();
              this.messageService.success(this.notificationMessage.success);
            },
            (e) => {
              if (e?.error) {
                this.messageService.warn(e?.error?.description);
              } else {
                this.messageService.error(this.notificationMessage.error);
              }
              this.isLoading = false;
            }
          );
        }
      })
      .catch(() => {});
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      this.router.navigate([this.router.url, 'detail', item.id]);
    }
  }
}
