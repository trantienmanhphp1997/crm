import { Component, HostBinding, OnInit, ViewChild } from '@angular/core';
import { FunctionCode } from 'src/app/core/utils/common-constants';
import _ from 'lodash';
import { ActivatedRoute, Router } from '@angular/router';
import { KpiDetailApi } from '../../apis';
import { EncrDecrService } from '../../../../core/services/encr-decr.service';
import { SessionService } from 'src/app/core/services/session.service';

@Component({
  selector: 'app-detail-kpi-manager-by-day',
  templateUrl: './detail-kpi-manager-by-day.component.html',
  styleUrls: ['./detail-kpi-manager-by-day.component.scss'],
})
export class DetailKpiManagerByDayComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private api: KpiDetailApi,
    private router: Router,
    private enc: EncrDecrService,
    private sessionService: SessionService
  ) {
    let list = this.enc.get(route.snapshot.params.code).split('-');
    this.code = list[0];
    this.period = list[1];
    this.past = list[2];
    this.businessDate = new Date(list[3]);
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
  }
  businessDate: Date;
  periods: Array<any>;
  pasts: Array<any>;
  period: boolean;
  past: boolean;
  prop: any;
  model: any = {};
  isLoading: boolean;
  obj: any;
  code: string;
  rsid: string;
  @HostBinding('class.app__right-content') appRightContent = true;
  isShow = true;

  ngOnInit(): void {
    this.isShow = false;
    this.isLoading = true;
    this.obj = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.rsid = _.get(this.obj, 'rsId');
    this.api.get(`${this.code}?rsId=${_.get(this.obj, 'rsId')}&scope=VIEW`).subscribe(
      (response) => {
        this.model = response;
        this.isLoading = false;
        this.isShow = true;
      },
      () => {
        this.isLoading = false;
        this.isShow = true;
      }
    );
  }

  getValue(item, key) {
    return _.get(item, key);
  }

  back() {
    this.router.navigate(['/kpis/kpi-management/kpi-track/kpi-manager-by-day'], { state: this.prop });
  }
}
