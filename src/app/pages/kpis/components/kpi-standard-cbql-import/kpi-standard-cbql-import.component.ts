import { KpiAssignmentApi, KpiStandardCbqlApi, LevelApi, TitleGroupApi } from '../../apis';
import { global } from '@angular/compiler/src/util';
import { Component, HostBinding, OnInit, ViewChild, Injector } from '@angular/core';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import _ from 'lodash';
import { typeExcel } from 'src/app/core/utils/common-constants';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CODE_FIRST_6M, LIST_MONTH, LIST_SIX_MONTH, LIST_YEAR } from '../../constant';
import * as moment from 'moment';
import { forkJoin, of } from 'rxjs';
import { CampaignsService } from 'src/app/pages/campaigns/services/campaigns.service';
import { catchError } from 'rxjs/operators';
import { Utils } from 'src/app/core/utils/utils';
import { FileService } from 'src/app/core/services/file.service';

@Component({
  selector: 'app-kpi-standard-cbql-import',
  templateUrl: './kpi-standard-cbql-import.component.html',
  styleUrls: ['./kpi-standard-cbql-import.component.scss'],
})
export class KpiStandardCbqlImportComponent extends BaseComponent implements OnInit {
  @ViewChild('tableSuccess') tableSuccess: DatatableComponent;
  @ViewChild('tableError') tableError: DatatableComponent;

  constructor(
    injector: Injector,
    private kpiStandardCbqlApi: KpiStandardCbqlApi,
    private campaignService: CampaignsService,
    private titleGroupApi: TitleGroupApi,
    private rmLevelApi: LevelApi,
    private fileService: FileService,
    private kpiAssignmentApi: KpiAssignmentApi
  ) {
    super(injector);
    this.prop = _.get(this.router.getCurrentNavigation(), 'extras.state');
  }
  @HostBinding('class.app__right-content') appRightContent = true;
  notificationMessage: any;
  isLoading = false;
  fileName: string;
  isFile = false;
  fileImport: File;
  files: any;
  ColumnMode = ColumnMode;
  listDataError = [];
  prop: any;
  limit = global.userConfig.pageSize;
  fileId: string;
  isUpload = false;
  messages = global.messageTable;
  // add
  listYear = LIST_YEAR;
  listMonth = LIST_MONTH;
  listBlock = [];
  formSearch = this.fb.group({
    blockCode: '',
    rmTitleCode: '',
    rmLevelCode: '',
    isMonth: true,
    month: parseInt(moment().format('MM')),
    year: parseInt(moment().format('yyyy')),
    // Extra data
    rmLevelId: '',
    rmTitleId: '',
  });
  formCreate = this.fb.array([]);
  listGroup = [];
  listLevel = [];
  // Data from file
  listSizeHeaders = [];
  listHeaders = [];
  listData = [];
  allData = [];
  generalInfo: any;
  listTextMonth = [
    { text: 'Kỳ 6 tháng đầu năm', code: 13 },
    { text: 'Kỳ 6 tháng cuối năm', code: 14 },
  ];

  ngOnInit(): void {
    this.generalInfo = {};
    forkJoin([this.campaignService.getBlockMapping().pipe(catchError((e) => of(undefined)))]).subscribe(
      async ([listBlock]) => {
        // map list block
        this.listBlock =
          listBlock?.content?.map((item) => {
            return { code: item.code, name: item.code + ' - ' + item.name };
          }) || [];
        let data;
        if (this.listBlock.length) {
          data = await this.getGroupAndLevelBlockCode(this.listBlock[0].code);
        }
        this.listGroup = this.mapListGroup(data.listGroup);
        this.listLevel = data.listLevel;

        // Set default value
        const objectData = {
          blockCode: this.listBlock?.[0]?.code,
          rmTitleCode: this.listGroup?.[0]?.code,
          rmLevelCode: this.listLevel?.[0]?.code,
          rmLevelId: this.listLevel?.[0]?.id,
          rmTitleId: this.listGroup?.[0]?.id,
        };
        this.formSearch.setValue({ ...this.formSearch.value, ...objectData });

        this.formSearch.controls.isMonth.valueChanges.subscribe((value) => {
          if (value) {
            this.listMonth = LIST_MONTH;
            this.formSearch.controls.month?.setValue(parseInt(moment().format('MM')));
          } else {
            this.listMonth = LIST_SIX_MONTH;
            this.formSearch.controls.month?.setValue(CODE_FIRST_6M);
          }
        });

        this.isLoading = false;
        // this.onSearchKpiStandardRm();
      }
    );
  }

  getGroupAndLevelBlockCode = async (code) => {
    let listGroup = [];
    let listLevel = [];
    listGroup =
      (
        await this.titleGroupApi
          .searchGroupByBlockCode(code)
          .pipe(catchError((e) => of(undefined)))
          .toPromise()
      )?.content?.filter((item) => item.isManagerGroup) || [];
    if (listGroup?.length) {
      listLevel =
        (
          await this.rmLevelApi
            .searchLevelByGroupCode(listGroup[0].id, code)
            .pipe(catchError((e) => of(undefined)))
            .toPromise()
        )?.content || [];
    }

    return { listGroup, listLevel };
  };

  async onChangeBlockCode() {
    const blockCode = this.formSearch.value.blockCode;
    const data = await this.getGroupAndLevelBlockCode(blockCode);
    this.listGroup = this.mapListGroup(data.listGroup);
    this.listLevel = data.listLevel;
    // Re-set value rmTitleCode, rmTitleId, rmLevelId and rmLevelCode
    const objectData = {
      rmTitleCode: this.listGroup?.[0]?.code || '',
      rmLevelCode: this.listLevel?.[0]?.code || '',
      rmLevelId: this.listLevel?.[0]?.id || '',
      rmTitleId: this.listGroup?.[0]?.id || '',
    };
    this.formSearch.setValue({ ...this.formSearch.value, ...objectData });
  }

  async onChangeGroup() {
    const blockCode = this.formSearch.value.blockCode;
    const groupCode = this.formSearch.value.rmTitleCode;
    const groupSelected = this.listGroup.find((group) => group.code === groupCode);
    this.listLevel =
      (
        await this.rmLevelApi
          .searchLevelByGroupCode(groupSelected?.id, blockCode)
          .pipe(catchError((e) => of(undefined)))
          .toPromise()
      )?.content || [];
    // Re-set value rmTitleId, rmLevelCode, rmLevelId
    const objectData = {
      rmTitleId: groupSelected?.id,
      rmLevelCode: this.listLevel?.[0]?.code || '',
      rmLevelId: this.listLevel?.[0]?.id || '',
    };
    this.formSearch.setValue({ ...this.formSearch.value, ...objectData });
  }

  onChangeLevel() {
    const levelCode = this.formSearch.value.rmLevelCode;
    const levelSelected = this.listLevel.find((level) => level.code === levelCode);
    // Re-set value rmLevelId
    const objectData = {
      rmLevelId: levelSelected?.id,
    };
    this.formSearch.setValue({ ...this.formSearch.value, ...objectData });
  }

  mapListGroup = (listGroup) => {
    return (
      listGroup?.map((item) => {
        return { ...item, name: item.blockCode + ' - ' + item.name, nameExcel: item.name };
      }) || []
    );
  };

  handleFileInput(files) {
    if (files?.item(0)?.size > 10485760) {
      this.messageService.warn(this.notificationMessage.ECRM005);
      return;
    }
    if (files?.item(0) && !typeExcel.includes(files?.item(0)?.type)) {
      this.messageService.error(this.notificationMessage.CANNOT_READ_DATA_FROM_FILE);
      return;
    }

    // Dissection data from excel file
    import('xlsx').then((xlsx) => {
      let workBook = null;
      let jsonData = null;
      const reader = new FileReader();
      // const file = ev.target.files[0];
      reader.onload = (event) => {
        const data = reader.result;
        workBook = xlsx.read(data, { type: 'binary' });

        jsonData = workBook.SheetNames.reduce((initial, name) => {
          const sheet = workBook.Sheets[name];
          initial[name] = xlsx.utils.sheet_to_json(sheet);
          return initial;
        }, {});
        let allData = jsonData[Object.keys(jsonData)[0]];
        let indexFirstFullHeader = 0;
        let indexGeneralInfo = 2;

        for (let index = 0; index < allData.length; index++) {
          const element = allData[index];
          if (element.__EMPTY) {
            indexFirstFullHeader = index;
            break;
          }
        }

        this.listSizeHeaders = Object.values(allData[indexFirstFullHeader - 1]);
        this.listHeaders = Object.values(allData[indexFirstFullHeader]);
        this.listData = allData.splice(indexFirstFullHeader + 1).map((item) => {
          return Object.values(item).map((v, i) =>
            i >= 3 && Number(v) % 1 != 0 ? (Math.round(Number(v) * 100) / 100).toFixed(2) : v
          );
        });
        this.allData = allData;
        this.listDataError = [];
        this.formCreate.clear();

        this.generalInfo = {
          blockCode: allData[indexGeneralInfo].__EMPTY_1,
          rmTitleCode: allData[indexGeneralInfo + 1].__EMPTY_1,
          rmLevelCode: allData[indexGeneralInfo + 2].__EMPTY_1,
          month: allData[indexGeneralInfo + 3].__EMPTY_1,
          year: allData[indexGeneralInfo + 4].__EMPTY_1,
        };
      };
      reader.readAsBinaryString(files[0]);
    });

    if (_.size(files) > 0) {
      this.isFile = true;
      this.fileImport = files.item(0);
      this.fileName = files.item(0).name;
    } else {
      this.isFile = false;
    }
  }

  exportFile() {
    this.isLoading = true;
    this.kpiAssignmentApi.getAggregteMonthMax().subscribe(
      (res) => {
        this.isLoading = false;
        const formValue = this.formSearch.value;
        const isNeedCheck = formValue.month >= 1 && formValue.month <= 12;
        if (
          isNeedCheck &&
          (res.year > formValue.year || (res.year == formValue.year && formValue.month <= res.month))
        ) {
          this.confirmService.confirm(this.fields.confirm_aggregte).then((res) => {
            if (res) {
              this.confirmExportFile();
            }
          });
        } else {
          this.confirmExportFile();
        }
      },
      (e) => {
        this.isLoading = false;
        if (e?.status === 0) {
          this.messageService.error(this.notificationMessage.E001);
          return;
        }
        this.messageService.error(e?.error?.description);
      }
    );
  }

  confirmExportFile = () => {
    this.isLoading = true;
    const formValue = this.formSearch.value;
    let groupSelected = this.listGroup.find((item) => item.code === formValue.rmTitleCode);
    let levelSelected = this.listLevel.find((item) => item.code === formValue.rmLevelCode);
    let params = {
      month: formValue.month,
      year: formValue.year,
      titleCode: formValue.rmTitleCode,
      levelCode: formValue.rmLevelCode,
      blockCode: formValue.blockCode,
      titleId: formValue.rmTitleId,
      levelId: formValue.rmLevelId,
      titleName: groupSelected?.nameExcel,
      levelName: levelSelected?.name,
    };
    this.kpiStandardCbqlApi.exportKPIStandardTemplate(params).subscribe(
      (res) => {
        if (Utils.isStringNotEmpty(res)) {
          this.download(res);
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      },
      (err) => {
        const message = err.error ? JSON.parse(err.error).description : _.get(this.notificationMessage, 'export_error');
        this.messageService.error(message);
        this.isLoading = false;
      }
    );
  };

  download(fileId: string) {
    this.fileService.downloadFile(fileId, 'Template Import KPI Tieu chuan CBQL.xlsx').subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }

  clearFile() {
    this.isUpload = false;
    this.isFile = false;
    this.fileName = null;
    this.fileImport = null;
    this.files = null;
    this.listDataError = [];
    this.fileId = undefined;

    // Data before write
    this.listSizeHeaders = [];
    this.listHeaders = [];
    this.listData = [];
    this.allData = [];
    this.generalInfo = {};
  }

  getIndex = (row, col) => {
    // Cause 3 first col is title;
    let realNumberCol = this.listHeaders.length - 3;
    let realCol = col - 3;
    //---------------------------
    return row * realNumberCol + realCol;
  };

  save() {
    const formValue = this.formSearch.value;
    if (formValue.month > 12) {
      const monthValue = this.listTextMonth.filter((item) => item.text === this.generalInfo.month)[0].code;
      this.generalInfo.month = monthValue;
    }
    if (
      formValue.blockCode != this.generalInfo.blockCode ||
      formValue.rmLevelCode != this.generalInfo.rmLevelCode ||
      formValue.rmTitleCode != this.generalInfo.rmTitleCode ||
      formValue.month != this.generalInfo.month ||
      formValue.year != this.generalInfo.year
    ) {
      this.messageService.error(this.notificationMessage.file_not_valid);
    } else {
      this.saveAfterValid(formValue);
    }
  }

  saveAfterValid = (formValue) => {
    let data = {};
    let entries = [];
    let firstUpdate = true;
    if (this.listDataError.length) {
      firstUpdate = false;
      this.formCreate.value.forEach((element) => {
        entries.push(element);
      });
    } else {
      this.listData.forEach((row) => {
        for (let index = 3; index < this.listHeaders.length; index++) {
          const kpiItemCode = this.listHeaders[index];
          entries.push({
            branchCode: row[0],
            hrisCode: row[2],
            kpiItemCode,
            value: row[index],
          });
        }
      });
    }

    data = {
      blockCode: formValue.blockCode,
      month: formValue.month,
      year: formValue.year,
      rmLevelId: formValue.rmLevelId,
      rmLevelCode: formValue.rmLevelCode,
      rmTitleId: formValue.rmTitleId,
      rmTitleCode: formValue.rmTitleCode,
      firstUpdate,
      entries,
    };

    this.confirmService.confirm().then((res) => {
      if (res) {
        this.formCreate.clear();
        this.listDataError = [];
        this.isLoading = true;
        this.kpiStandardCbqlApi.importStandard(data).subscribe(
          (value) => {
            this.isLoading = false;
            let failed = value.failed;
            let failedKeys = Object.keys(failed);

            if (failedKeys.length) {
              this.translate
                .get('notificationMessage.note_success_apart', { number: Object.values(value.success).length })
                .subscribe((resNotification) => {
                  this.messageService.success(resNotification);
                });
              this.listData.forEach((row) => {
                if (failedKeys.indexOf(row[2]) >= 0) {
                  this.listDataError.push({ ...row });
                }
              });
              this.listDataError.forEach((row) => {
                for (let index = 3; index < this.listHeaders.length; index++) {
                  const kpiItemCode = this.listHeaders[index];
                  const itemFailed = failed[row[2]]?.find((item) => item.kpiItemCode === kpiItemCode);
                  this.formCreate.push(
                    this.fb.group({
                      branchCode: row[0],
                      hrisCode: row[2],
                      kpiItemCode,
                      value: itemFailed?.value,
                      success: itemFailed?.success,
                    })
                  );
                }
              });
            } else {
              this.back();
              this.messageService.success(this.notificationMessage.success);
            }
          },
          (e) => {
            this.isLoading = false;
            if (e?.status === 0) {
              this.messageService.error(this.notificationMessage.E001);
              return;
            }
            this.messageService.error(e?.error?.description);
          }
        );
      }
    });
  };

  isText = (i) => {
    return i <= 2;
  };

  back() {
    this.location.back();
  }

  getValue(row, key) {
    return _.get(row, key);
  }

  getHeader = (item, index) => {
    if (index < 3) {
      return item;
    } else {
      let sizeHeader = this.listSizeHeaders[index - 3];
      return `${sizeHeader}\n${item}`;
    }
  };
}
