import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiCategoryUpdateComponent } from './kpi-category-update.component';

describe('KpiCategoryUpdateComponent', () => {
  let component: KpiCategoryUpdateComponent;
  let fixture: ComponentFixture<KpiCategoryUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiCategoryUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiCategoryUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
