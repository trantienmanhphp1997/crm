import { Component, OnInit, OnDestroy, Injector, ViewEncapsulation } from '@angular/core';
import { cleanDataForm, validateAllFormFields } from 'src/app/core/utils/function';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { forkJoin, of } from 'rxjs';
import { FunctionCode, ScreenType } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as _ from 'lodash';
import { LIST_KPI_ASSESSMENT, LIST_KPI_FEATURE_NATURE } from '../../constant';
import { KpiCategoryApi } from '../../apis';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-kpi-category-update',
  templateUrl: './kpi-category-update.component.html',
  styleUrls: ['./kpi-category-update.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class KpiCategoryUpdateComponent extends BaseComponent implements OnInit, OnDestroy {
  data: any;
  id = null;
  form = this.fb.group({
    code: [{ value: '', disabled: true }, [CustomValidators.required, CustomValidators.codeKpi]], // Ma chi tieu
    name: ['', CustomValidators.required], // Ten chi tieu
    categoryCode: ['', CustomValidators.required], // Ma loai chi tieu
    blockCode: ['', CustomValidators.required], // Ma khoi
    type: ['', CustomValidators.required], // Cach danh gia
    unitCode: ['', CustomValidators.required], // Don vi tinh
    feature: ['', CustomValidators.required], // Tinh chat chi tieu
    frequencyCode: ['', CustomValidators.required], // Tan suat
    isActive: ['', CustomValidators.required], // Hieu luc
    description: [''], // Mo ta
    shortedName: ['', [Validators.maxLength(50)]] // Ten viet tat
  });
  kpiCategoryOutputs = [];
  listKpiAssessment = LIST_KPI_ASSESSMENT;
  kpiUnitOutputs = [];
  kpiFeatures = LIST_KPI_FEATURE_NATURE;
  kpiFrequencyOutputs = [];
  isLoading = false;
  isValidator = false;
  listBlock = [];
  branchesCode: string[];

  constructor(injector: Injector, private categoryService: CategoryService, private kpiCategoryApi: KpiCategoryApi) {
    super(injector);
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.id = this.route.snapshot.paramMap.get('id');
    this.prop = this.sessionService.getSessionData(`${FunctionCode.KPI_ITEM}_${ScreenType.Update}`);
    if (!this.prop) {
      forkJoin([
        this.kpiCategoryApi.getKpiItemById(this.id).pipe(catchError((e) => of(undefined))),
        this.categoryService.getBlocksCategoryByRm().pipe(catchError((e) => of(undefined))),
        this.kpiCategoryApi.getItemResource().pipe(catchError((e) => of(undefined))),
      ]).subscribe(([kpiItem, listBlock, itemResources]) => {
        this.prop = {
          kpiItem,
          listBlock,
          itemResources,
        };
        this.mapData();
        this.sessionService.setSessionData(`${FunctionCode.KPI_ITEM}_${ScreenType.Update}`, this.prop);
      });
    } else {
      const timer = setTimeout(() => {
        this.kpiCategoryApi.getKpiItemById(this.id).subscribe((kpiItem) => {
          this.prop.kpiItem = kpiItem;
          this.mapData();
        });
        clearTimeout(timer);
      }, 500);
    }
  }

  mapData() {
    this.kpiCategoryOutputs = this.prop?.itemResources?.kpiCategoryOutputs || [];
    this.kpiUnitOutputs = this.prop?.itemResources?.kpiUnitOutputs || [];
    this.kpiFrequencyOutputs = this.prop?.itemResources?.kpiFrequencyOutputs || [];
    this.listBlock =
      this.prop?.listBlock?.map((item) => {
        return { code: item.code, name: item.code + ' - ' + item.name };
      }) || [];
    // map old data
    this.data = this.prop?.kpiItem;
    this.data.type = this.data.type.toString(); // for map data
    this.form.patchValue(this.data);
    this.isLoading = false;
  }

  save() {
    if (this.form.valid) {
      this.confirmService.confirm().then((res) => {
        if (res) {
          this.isLoading = true;
          const data = cleanDataForm(this.form);
          this.kpiCategoryApi.updateKpiItem({ data, id: this.id }).subscribe(
            () => {
              this.isLoading = false;
              this.back();
              this.messageService.success(this.notificationMessage.success);
            },
            (e) => {
              this.isLoading = false;
              if (e?.status === 0) {
                this.messageService.error(this.notificationMessage.E001);
                return;
              }
              this.messageService.error(e?.error?.description);
            }
          );
        }
      });
    } else {
      this.isValidator = true;
      validateAllFormFields(this.form);
    }
  }
}
