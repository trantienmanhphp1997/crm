import { forkJoin, of } from 'rxjs';
import { global } from '@angular/compiler/src/util';
import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';
import { CampaignsService } from 'src/app/pages/campaigns/services/campaigns.service';
import { FunctionCode, maxInt32, Scopes } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import { LIST_ALL_KPI_ASSESSMENT, LIST_ALL_KPI_STATUS } from '../../constant';
import { KpiAssignmentApi, KpiStandardCbqlApi, LevelApi, TitleGroupApi } from '../../apis';
import { Utils } from 'src/app/core/utils/utils';
import { FileService } from 'src/app/core/services/file.service';

@Component({
  selector: 'app-kpi-assignment-cbql-search',
  templateUrl: './kpi-assignment-cbql-search.component.html',
  styleUrls: ['./kpi-assignment-cbql-search.component.scss'],
})
export class KpiAssignmentCbqlSearchComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listData = [];
  listBlock = [];
  listKpiStatus = LIST_ALL_KPI_STATUS;
  listKpiAssessment = LIST_ALL_KPI_ASSESSMENT;
  formSearch = this.fb.group({
    rmCode: '',
    rmName: '',
    blockCode: '',
    titleId: '',
    levelId: '',
    hrisCode: '',
  });
  paramSearch = {
    pageSize: global.userConfig.pageSize,
    pageNumber: 0,
  };
  prevParams: any;
  pageable: Pageable;
  prop: any;
  listGroup = [];
  listLevel = [];
  listBranches = [];
  branchCodes = [];

  constructor(
    injector: Injector,
    private campaignService: CampaignsService,
    private kpiStandardCbqlApi: KpiStandardCbqlApi,
    private titleGroupApi: TitleGroupApi,
    private rmLevelApi: LevelApi,
    private kpiAssignmentApi: KpiAssignmentApi,
    private fileService: FileService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.KPI_ASSIGNMENT_CBQL}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.prop = this.sessionService.getSessionData(FunctionCode.KPI_ASSIGNMENT_CBQL);
    if (this.prop) {
      this.prevParams = { ...this.prop?.prevParams };
      this.paramSearch.pageNumber = this.prevParams?.pageNumber;
      this.formSearch.patchValue({
        ...this.prevParams,
        titleId: this.prevParams.titleId[0],
        levelId: this.prevParams.levelId[0],
      });
      this.listBlock = this.prop?.listBlock || [];
      this.listBranches = this.prop?.listBranches || [];
      this.listGroup = this.prop?.listGroup || [];
      this.listLevel = this.prop?.listLevel || [];
      this.search();
    } else {
      forkJoin([
        this.kpiStandardCbqlApi.getBranchesOfUser(_.get(this.objFunction, 'rsId'), Scopes.VIEW).pipe(catchError(() => of(undefined))),
        this.campaignService.getBlockMapping().pipe(catchError((e) => of(undefined))),
      ]).subscribe(async ([resBranches, listBlock]) => {
        // map list branch
        this.listBranches = resBranches || [];

        // map list block
        this.listBlock =
          listBlock?.content?.map((item) => {
            return { code: item.code, name: item.code + ' - ' + item.name };
          }) || [];

        // map list level & title
        let data;
        if (this.listBlock.length) {
          data = await this.getGroupAndLevelBlockCode(this.listBlock[0].code);
        }
        this.listGroup = this.mapListGroup(data.listGroup);
        this.listLevel = this.mapListLevel(data.listLevel);

        // Set default value
        const objectData = {
          blockCode: this.listBlock?.[0]?.code,
          titleId: this.listGroup?.[0]?.id,
          levelId: this.listLevel?.[0]?.id,
        };
        this.formSearch.setValue({ ...this.formSearch.value, ...objectData });

        this.prop = {
          listBlock: this.listBlock,
          listBranches: this.listBranches,
          listGroup: this.listGroup,
          listLevel: this.listLevel,
        };

        this.sessionService.setSessionData(FunctionCode.KPI_ASSIGNMENT_CBQL, this.prop);
        this.search(true);
      });
    }

    this.setValueChanges();
  }

  getGroupAndLevelBlockCode = async (code) => {
    let listGroup = [];
    let listLevel = [];
    listGroup =
      (
        await this.titleGroupApi
          .searchGroupByBlockCode(code)
          .pipe(catchError((e) => of(undefined)))
          .toPromise()
      )?.content?.filter((item) => item.isManagerGroup) || [];
    if (listGroup?.length) {
      listLevel =
        (
          await this.rmLevelApi
            .searchLevelByGroupCode(listGroup[0].id, code)
            .pipe(catchError((e) => of(undefined)))
            .toPromise()
        )?.content || [];
    }
    console.log(listGroup, listLevel);

    return { listGroup, listLevel };
  };

  async onChangeBlockCode() {
    const blockCode = this.formSearch.value.blockCode;
    const data = await this.getGroupAndLevelBlockCode(blockCode);
    this.listGroup = this.mapListGroup(data.listGroup);
    this.listLevel = this.mapListLevel(data.listLevel);
    // Re-set value titleId, and levelId
    const objectData = {
      titleId: this.listGroup?.[0]?.id || '',
      levelId: this.listLevel?.[0]?.id || '',
    };
    this.formSearch.setValue({ ...this.formSearch.value, ...objectData });
  }

  async onChangeGroup() {
    const blockCode = this.formSearch.value.blockCode;
    const titleId = this.formSearch.value.titleId;
    this.listLevel = this.mapListLevel(
      (
        await this.rmLevelApi
          .searchLevelByGroupCode(titleId, blockCode)
          .pipe(catchError((e) => of(undefined)))
          .toPromise()
      )?.content || []
    );
    // Re-set value titleId, levelId,
    const objectData = {
      titleId,
      levelId: this.listLevel?.[0]?.id || '',
    };
    this.formSearch.setValue({ ...this.formSearch.value, ...objectData });
  }

  onChangeLevel() {
    const levelId = this.formSearch.value.levelId;
    // Re-set value levelId
    const objectData = {
      levelId,
    };
    this.formSearch.setValue({ ...this.formSearch.value, ...objectData });
  }

  mapListGroup = (listGroup) => {
    let newListGroup =
      listGroup?.map((item) => {
        return { ...item, name: item.blockCode + ' - ' + item.name };
      }) || [];
    return [{ id: '', name: 'Tất cả' }, ...newListGroup];
  };

  mapListLevel = (listLevel) => {
    return [{ id: '', name: 'Tất cả' }, ...listLevel];
  };

  search(isSearch?: boolean) {
    let params: any = {};
    if (isSearch) {
      this.paramSearch.pageNumber = 0;
      const formData = this.formSearch.value;
      let listAllTitleId = this.listGroup.filter((item) => !!item.id)?.map((item) => item.id);
      params = {
        rmCode: formData.rmCode,
        rmName: formData.rmName,
        blockCode: formData.blockCode,
        branchCodes: this.branchCodes,
        titleId: formData.titleId ? [formData.titleId] : listAllTitleId,
        levelId: [formData.levelId],
        hrisCode: formData.hrisCode,
      };
    } else {
      if (!this.prevParams) {
        return;
      }
      params = this.prevParams;
    }

    params = {
      ...params, ...this.paramSearch, rsId: _.get(this.objFunction, 'rsId'),
      scope: Scopes.VIEW
    };

    // ----------
    this.prevParams = params;
    this.prop.prevParams = params;
    this.prop.listGroup = this.listGroup;
    this.prop.listLevel = this.listLevel;
    this.sessionService.setSessionData(FunctionCode.KPI_ASSIGNMENT_CBQL, this.prop);
    // ----------

    this.isLoading = true;

    this.kpiAssignmentApi.searchRM(params).subscribe(
      (result) => {
        if (result) {
          this.listData =
            result?.content?.map((item) => {
              return {
                ...item,
              };
            }) || [];
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: global.userConfig.pageSize,
          };
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      const item = event.row;
      const formData = this.formSearch.value;
      const block = this.listBlock.find((block) => block.code === formData.blockCode);
      this.sessionService.setSessionData(FunctionCode.KPI_STANDARD_CBQL_DETAIL, { ...item, block });
      this.router.navigate([this.router.url, 'update', item.hrisCode], {
        skipLocationChange: true,
      });
    }
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.pageNumber = pageInfo.offset;
    this.search(false);
  }

  exportFile() {
    if (Utils.isArrayEmpty(this.listData)) {
      this.messageService.warn(_.get(this.notificationMessage, 'noRecord'));
      return;
    }
    this.isLoading = true;
    const params = this.prevParams;
    _.set(params, 'pageSize', maxInt32);
    _.set(params, 'pageNumber', 0);

    this.kpiAssignmentApi.createFile(params).subscribe(
      (res) => {
        if (Utils.isStringNotEmpty(res)) {
          this.download(res);
        } else {
          this.messageService.error(_.get(this.notificationMessage, 'export_error'));
          this.isLoading = false;
        }
      },
      () => {
        this.messageService.error(_.get(this.notificationMessage, 'export_error'));
        this.isLoading = false;
      }
    );
  }

  download(fileId: string) {
    this.fileService.downloadFile(fileId, 'danh_sach_phan_giao_kpi_CBQL.xlsx').subscribe((res) => {
      this.isLoading = false;
      if (!res) {
        this.messageService.error(this.notificationMessage.error);
      }
    });
  }

  setValueChanges(): void {
    this.formSearch.controls.hrisCode.valueChanges.subscribe(value => {
      this.formSearch.controls.hrisCode.setValue(value.trim(), { emitEvent: false });
    })
  }
}
