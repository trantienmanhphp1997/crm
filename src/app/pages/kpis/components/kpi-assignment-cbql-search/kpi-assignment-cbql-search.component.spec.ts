import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiAssignmentCbqlSearchComponent } from './kpi-assignment-cbql-search.component';

describe('KpiAssignmentCbqlSearchComponent', () => {
  let component: KpiAssignmentCbqlSearchComponent;
  let fixture: ComponentFixture<KpiAssignmentCbqlSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiAssignmentCbqlSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiAssignmentCbqlSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
