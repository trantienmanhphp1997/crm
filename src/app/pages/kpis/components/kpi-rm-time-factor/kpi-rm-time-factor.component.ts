import { forkJoin, of } from 'rxjs';
import { global } from '@angular/compiler/src/util';
import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';
import { cleanDataForm } from 'src/app/core/utils/function';
import { CampaignsService } from 'src/app/pages/campaigns/services/campaigns.service';
import { FunctionCode } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import {
  CLIENT_DATE_FORMAT,
  KPI_RM_TIME_FACTOR_DEFAULT,
  LIST_ALL_KPI_ASSESSMENT,
  LIST_ALL_KPI_STATUS,
  SERVER_DATE_FORMAT,
} from '../../constant';
import * as moment from 'moment';
import { KpiRmTimeApi, LevelApi, TitleGroupApi } from '../../apis';

@Component({
  selector: 'app-kpi-rm-time-factor',
  templateUrl: './kpi-rm-time-factor.component.html',
  styleUrls: ['./kpi-rm-time-factor.component.scss'],
})
export class KpiRmTimeFactorComponent extends BaseComponent implements OnInit {
  isLoading = false;
  listData = [];
  listBlock = [];
  listGroup = [];
  listLevel = [];
  listKpiStatus = LIST_ALL_KPI_STATUS;
  listKpiAssessment = LIST_ALL_KPI_ASSESSMENT;
  formSearch = this.fb.group({
    blockCode: { value: KPI_RM_TIME_FACTOR_DEFAULT.BLOCK_CODE, disabled: true }, // Ma khoi
    rmTitleCode: { value: KPI_RM_TIME_FACTOR_DEFAULT.TITLE, disabled: true },
    rmLevelCode: { value: KPI_RM_TIME_FACTOR_DEFAULT.LEVEL, disabled: true },
    efficientDate: '', // Ngay bat dau
    expireDate: '', // Ngay ket thuc
  });
  paramSearch = {
    pageSize: global.userConfig.pageSize,
    pageNumber: 0,
  };
  prevParams: any;
  pageable: Pageable;
  prop: any;
  maxEfficientDate: any;
  minExpireDate: any;

  constructor(
    injector: Injector,
    private campaignService: CampaignsService,
    private titleGroupApi: TitleGroupApi,
    private rmLevelApi: LevelApi,
    private kpiRmTimeApi: KpiRmTimeApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.KPI_TIME_FACTOR}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.prop = this.sessionService.getSessionData(FunctionCode.KPI_TIME_FACTOR);
    this.maxEfficientDate = '';
    this.minExpireDate = '';
    if (this.prop) {
      this.prevParams = { ...this.prop?.prevParams };
      this.paramSearch.pageNumber = this.prevParams?.pageNumber;
      this.formSearch.patchValue(this.prevParams);
      this.listBlock = this.prop?.listBlock || [];
      this.listGroup = this.prop?.listGroup || [];
      this.listLevel = this.prop?.listLevel || [];
      this.search();
    } else {
      forkJoin([
        this.campaignService.getBlockMapping().pipe(catchError((e) => of(undefined))),
        this.titleGroupApi
          .searchGroupByBlockCode(KPI_RM_TIME_FACTOR_DEFAULT.BLOCK_CODE)
          .pipe(catchError((e) => of(undefined))),
        this.rmLevelApi
          .searchLevelByGroupCode('', KPI_RM_TIME_FACTOR_DEFAULT.BLOCK_CODE)
          .pipe(catchError((e) => of(undefined))),
      ]).subscribe(([listBlock, listGroup, listLevel]) => {
        // map list block
        this.listBlock =
          listBlock?.content.map((item) => {
            return { ...item, name: item.code + ' - ' + item.name };
          }) || [];
        // map list group
        this.listGroup =
          listGroup?.content.map((item) => {
            return { ...item, name: item.blockCode + ' - ' + item.name };
          }) || [];
        // map list level
        this.listLevel = listLevel?.content || [];
        this.prop = {
          listBlock: this.listBlock,
          listGroup: this.listGroup,
          listLevel: this.listLevel,
        };

        this.formSearch.controls.efficientDate.valueChanges.subscribe((value) => {
          this.minExpireDate = new Date(moment(value).startOf('day').valueOf());
        });
        this.formSearch.controls.expireDate.valueChanges.subscribe((value) => {
          this.maxEfficientDate = new Date(moment(value).startOf('day').valueOf());
        });

        this.sessionService.setSessionData(FunctionCode.KPI_TIME_FACTOR, this.prop);
        this.search(true);
      });
    }
  }

  onChangeEndDate = (event) => {
		const endOfMonth = new Date(moment(event).endOf('month').valueOf());
		this.formSearch.controls?.expireDate?.setValue(endOfMonth);
	};

  search(isSearch?: boolean) {
    this.isLoading = true;
    let params: any = {};
    if (isSearch) {
      this.paramSearch.pageNumber = 0;
      params.efficientDate = this.formSearch.value.efficientDate;
      params.expireDate = this.formSearch.value.expireDate;
    } else {
      if (!this.prevParams) {
        return;
      }
      params = this.prevParams;
    }
    Object.keys(this.paramSearch).forEach((key) => {
      params[key] = this.paramSearch[key];
    });

    // remove fields date empty or format
    if (params.efficientDate) {
      params.efficientDate = moment(params.efficientDate).format(SERVER_DATE_FORMAT);
    } else {
      delete params.efficientDate;
    }
    if (params.expireDate) {
      params.expireDate = moment(params.expireDate).format(SERVER_DATE_FORMAT);
    } else {
      delete params.expireDate;
    }

    this.kpiRmTimeApi.searchKpiTimeFactorSet(params).subscribe(
      (result) => {
        if (result) {
          this.prevParams = {
            ...params,
            // keep format of date when save to session storage
            efficientDate: this.formSearch.value.efficientDate,
            expireDate: this.formSearch.value.expireDate,
          };
          this.prop.prevParams = this.prevParams;
          this.listData =
            result?.content?.map((item) => {
              const efficientDate = item?.efficientDate
                ? moment(item.efficientDate, SERVER_DATE_FORMAT).format(CLIENT_DATE_FORMAT)
                : '';
              const expireDate = item?.expireDate
                ? moment(item.expireDate, SERVER_DATE_FORMAT).format(CLIENT_DATE_FORMAT)
                : '';
              return {
                ...item,
                efficientDate,
                expireDate,
              };
            }) || [];
          this.pageable = {
            totalElements: result.totalElements,
            totalPages: result.totalPages,
            currentPage: result.number,
            size: global.userConfig.pageSize,
          };
        }
        this.sessionService.setSessionData(FunctionCode.KPI_TIME_FACTOR, this.prop);
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.pageNumber = pageInfo.offset;
    this.search(false);
  }

  getIsShowEdit = (value) => {
    let expireDate = moment(value, CLIENT_DATE_FORMAT).startOf('day');
    let currentDate = moment().startOf('day');
    if (expireDate.diff(currentDate, 'days') >= 0) {
      return true;
    } else {
      return false;
    }
  };

  create() {
    this.router.navigate([this.router.url, 'create']);
  }

  update(item) {
    this.router.navigate([this.router.url, 'update', item.kpiFactorSetId]);
  }
}
