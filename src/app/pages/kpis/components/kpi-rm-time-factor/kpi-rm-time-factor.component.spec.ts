import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiRmTimeFactorComponent } from './kpi-rm-time-factor.component';

describe('KpiRmTimeFactorComponent', () => {
  let component: KpiRmTimeFactorComponent;
  let fixture: ComponentFixture<KpiRmTimeFactorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiRmTimeFactorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiRmTimeFactorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
