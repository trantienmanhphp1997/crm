import {AfterViewInit, Component, Injector, OnInit, ViewEncapsulation} from '@angular/core';
import {BaseComponent} from 'src/app/core/components/base.component';
import {StandardKpiCbqlApi} from '../../apis/standard-kpi-cbql.api';
import {forkJoin, of} from 'rxjs';
import {CommonCategory, ConfirmType, Division, FunctionCode, Scopes} from '../../../../core/utils/common-constants';
import {CategoryService} from '../../../system/services/category.service';
import {KpiRmApi} from '../../apis';
import {catchError} from 'rxjs/operators';
import * as _ from 'lodash';
import {formatDate} from '@angular/common';
import {ColumnMode, DatatableComponent} from '@swimlane/ngx-datatable';
import {CustomValidators} from '../../../../core/utils/custom-validations';
import {cleanDataForm, validateAllFormFields} from '../../../../core/utils/function';
import {RmApi} from '../../../rm/apis';
import {AppFunction} from '../../../../core/interfaces/app-function.interface';
import {TargetSaleConfigEditModalComponent} from '../target-sale-config-edit-modal/target-sale-config-edit-modal.component';
import {TargetSaleConfigErrorModalComponent} from '../target-sale-config-list-error-modal/target-sale-config-error-modal.component';
import {ConfirmDialogComponent} from '../../../../shared/components';


@Component({
  selector: 'app-target-sale-config',
  templateUrl: './target-sale-config-create.component.html',
  styleUrls: ['./target-sale-config-create.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TargetSaleConfigCreateComponent extends BaseComponent implements OnInit, AfterViewInit {
  commonData = {
    listDivision: [{name: 'INDIV - Khách hàng cá nhân', code: 'INDIV'}],
    listTitleGroup: [],
    listLevelRM: [],
    listAllotmentValue: [{name: 'Tháng', code: 'MONTH'}, {name: 'Tuần', code: 'WEEK'}, {name: 'Tất cả', code: 'ALL'}],
    listProduct: [],
    listBranch: []
  };

  formSearch = this.fb.group({
    division: [{value: 'INDIV', disabled: true}],
    rmLevelId: 'ALL',
    rmTitleId: '',
    startDate: ['', CustomValidators.required],
    endDate: ['', CustomValidators.required],
    product: 'Casa',
    value: [''],
    branchCode: ['', CustomValidators.required],
    productCode: '',
  });

  listWeeksData: any = [];
  listMonthsData: any = [];
  allotmentValue = 'MONTH';
  ColumnMode = ColumnMode;
  autoAllocation = false;
  public objFunctionTagetSale: AppFunction;
  minDate =  new Date((new Date()).setHours(0,0,0,0));
  flag = true;

  constructor(
    injector: Injector,
    private standardKpiCbqlApi: StandardKpiCbqlApi,
    private categoryService: CategoryService,
    private api: KpiRmApi,
    private rmApi: RmApi,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.objFunctionTagetSale = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.TARGET_SALE_CONFIG}`);
  }

  ngOnInit() {
    this.isLoading = true;
    forkJoin([
      this.api.getCategoryRmByBlock({
        viewRm: true,
        blockCode: 'INDIV',
        rsId: this.objFunction?.rsId,
        scope: Scopes.VIEW,
        past: 1,
      }),
      this.categoryService
        .getBranchesOfUser(this.objFunctionTagetSale?.rsId, Scopes.VIEW)
        .pipe(catchError((e) => of(undefined))),
      this.commonService.getCommonCategory(CommonCategory.TARGET_SALE_CONFIG_PRODUCT).pipe(catchError(() => of(undefined))),
    ]).subscribe(([listTitleGroupByDivision, branchesOfUser, targetSaleProductConfig]) => {
      this.commonData.listProduct = _.get(targetSaleProductConfig, 'content')
      this.formSearch.controls.product.setValue(_.get(_.find(this.commonData.listProduct, (i) => i.isDefault), 'name'));

      this.commonData.listBranch = branchesOfUser?.map((item) => {
        return {code: item.code, displayName: item.code + ' - ' + item.name};
      });
      listTitleGroupByDivision?.forEach((item) => {
        item.name = `${item.blockCode} - ${item.name}`;
      });
      this.commonData.listTitleGroup = _.sortBy(listTitleGroupByDivision, 'id') || [];
      this.formSearch.controls.rmTitleId.setValue(this.commonData.listTitleGroup[0]?.id);
      this.isLoading = false;
    });
  }

  ngAfterViewInit() {
    this.formSearch.controls.rmTitleId.valueChanges.subscribe((id) => {
      if (id) {
        const itemTitleGroup = _.find(this.commonData.listTitleGroup, (item) => item.id === id);
        this.commonData.listLevelRM = itemTitleGroup?.levels;
        if (itemTitleGroup?.levels.length > 1 && !_.find(itemTitleGroup?.levels, (item) => item.id === 'ALL')) {
          this.commonData.listLevelRM.unshift({id: 'ALL', name: this.fields.all});
        }
        this.formSearch.controls.rmLevelId.setValue(_.head(this.commonData.listLevelRM).id);
      }
    });
    this.formSearch.controls.product.valueChanges.subscribe((name) => {
      if (name) {
        this.formSearch.controls.productCode.setValue(_.find(this.commonData.listProduct, i => i.name === name).code);
        console.log(' this.formSearch',  this.formSearch.controls.productCode.value);
      }
    });
  }

  addAction() {
    if (this.checkExistWithKeys()) return;
    this.isLoading = true;
    let option: any = [];
    // nếu chức danh RM có nhiều level
    // Chọn tất cả level
    if (this.formSearch.controls.rmLevelId.value === 'ALL') {
      // xóa toàn bộ dữ liệu của các level khác cùng chỉ tiêu
      this.clearData();
      // add từng level
      // chia đều value cho các level
      let tempValue = 0;
      this.commonData.listLevelRM.forEach((item, index) => {
        if (item.id !== 'ALL') {
          if (this.commonData.listLevelRM.length - 1 > index) {
            option.push({
              autoAllocation: this.autoAllocation,
              allotmentValue: this.allotmentValue,
              branchCode: this.formSearch.controls.branchCode.value,
              ...cleanDataForm(this.formSearch),
              rmLevelId: item.id,
              value: Math.round(this.formSearch.controls.value.value / (this.commonData.listLevelRM.length - 1))
            });
            tempValue += Math.round(this.formSearch.controls.value.value / (this.commonData.listLevelRM.length - 1))
          } else {
            // phần tử cuối trong list level
            option.push({
              autoAllocation: this.autoAllocation,
              allotmentValue: this.allotmentValue,
              branchCode: this.formSearch.controls.branchCode.value,
              ...cleanDataForm(this.formSearch),
              rmLevelId: item.id,
              value: this.formSearch.controls.value.value - tempValue
            });
          }
          if (this.hasDataDifferentAllotmentType(this.allotmentValue)) {
            _.remove(this.allotmentValue === 'MONTH' ? this.listWeeksData : this.listMonthsData, this.hasDataDifferentAllotmentType(this.allotmentValue));
            option[index - 1].allotmentValue = 'ALL';
          }
        }
      });
    } else {
      option = [{
        autoAllocation: this.autoAllocation,
        allotmentValue: this.allotmentValue,
        branchCode: this.formSearch.controls.branchCode.value,
        ...cleanDataForm(this.formSearch)
      }];
      if (this.hasDataDifferentAllotmentType(this.allotmentValue)) {
        _.remove(this.allotmentValue === 'MONTH' ? this.listWeeksData : this.listMonthsData, this.hasDataDifferentAllotmentType(this.allotmentValue));
        option[0].allotmentValue = 'ALL';
      }
    }
    // thay đổi ngày phân giao?
    if (this.isChangeDate()) {
      // thay đổi
      // set lại ngày phân bổ của toàn bộ dữ liệu cũ
      const oldData = [...this.listMonthsData, ...this.listWeeksData];
      this.listMonthsData = [];
      this.listWeeksData = [];
      oldData.forEach(item => {
        item.startDate = this.formSearch.get('startDate').value;
        item.endDate = this.formSearch.get('endDate').value;
      });
      option = [...oldData, ...option];
    }
    for (const temp of option) {
      // thêm mới
      if ((temp.allotmentValue === 'WEEK' || temp.allotmentValue === 'ALL')) {
        const data: any = {
          isActive: 1,
          targetSaleConfigDetail: this.generateDetailsTypeWeek(temp.startDate, temp.endDate, temp.value, temp.autoAllocation),
        };
        data.startDate = formatDate(temp.startDate, 'dd/MM/yyyy', 'en');
        data.endDate = formatDate(temp.endDate, 'dd/MM/yyyy', 'en');
        if (!_.find(this.listWeeksData, {...temp, ...data, allotmentValue: 'WEEK'})) {
          this.listWeeksData.push({...temp, ...data, allotmentValue: 'WEEK'})
          const tempData = Object.assign([], this.listWeeksData);
          this.listWeeksData = tempData;
        }
        console.log('listWeeksData', this.listWeeksData);
      }
      if ((temp.allotmentValue === 'MONTH' || temp.allotmentValue === 'ALL')) {
        const data: any = {
          isActive: 1,
          targetSaleConfigDetail: this.generateDetailsTypeMonth(temp.startDate, temp.endDate, temp.value, temp.autoAllocation),
        };
        data.startDate = formatDate(temp.startDate, 'dd/MM/yyyy', 'en');
        data.endDate = formatDate(temp.endDate, 'dd/MM/yyyy', 'en');
        if (!_.find(this.listMonthsData, {...temp, ...data, allotmentValue: 'MONTH'})) {
          this.listMonthsData.push({...temp, ...data, allotmentValue: 'MONTH'});
          const tempData = Object.assign([], this.listMonthsData);
          this.listMonthsData = tempData;
        }
        console.log('listMonthsData', this.listMonthsData);
      }
    }
    this.formSearch.controls.value.setValue(0);
    this.flag = true;
    this.isLoading = false;
  }

  generateDetailsTypeWeek(startDate, endDate, totalValue, autoAllocation) {
    const dates = [];

    const totalDate = parseInt(String((endDate - startDate) / (1000 * 60 * 60 * 24)), 10) + 1;
    // tính giá trị trung bình mỗi ngày
    const averageValue = totalValue / totalDate;
    let currentDate: any = new Date(startDate);

    if (currentDate.getDay() !== 1) {
      // not a tuesday
      currentDate.setDate(currentDate.getDate() - currentDate.getDay() + 1);
    }
    let tempValue = 0;

    do {
      const endWeekDate: any = this.addDays(currentDate, 6);
      let value;
      let startDateValue;
      let endDateValue;
      if (this.isSameWeek(startDate, endDate)) {
        // Ngày bắt đầu và kết thúc trong cùng 1 tuần
        startDateValue = formatDate(startDate, 'dd/MM/yyyy', 'en');
        endDateValue = formatDate(endDate, 'dd/MM/yyyy', 'en');
        value = autoAllocation ? totalValue - tempValue : totalValue;

      } else if (startDate > currentDate) {
        // tuần đầu tiên
        startDateValue = formatDate(startDate, 'dd/MM/yyyy', 'en');
        endDateValue = formatDate(endWeekDate, 'dd/MM/yyyy', 'en');
        value = autoAllocation ? Math.round(averageValue * this.caculatorDate(endWeekDate, startDate)) : 0;
        tempValue += value;
      } else if (endDate <= endWeekDate) {
        // tuần cuối cùng
        startDateValue = formatDate(currentDate, 'dd/MM/yyyy', 'en');
        endDateValue = formatDate(endDate, 'dd/MM/yyyy', 'en');
        value = autoAllocation ? totalValue - tempValue : totalValue;
      } else {
        // các tuần ở giữa
        startDateValue = formatDate(currentDate, 'dd/MM/yyyy', 'en');
        endDateValue = formatDate(endWeekDate, 'dd/MM/yyyy', 'en');
        value = (autoAllocation && ((tempValue + Math.round(averageValue * this.caculatorDate(endWeekDate, currentDate))) <= totalValue))
          ? Math.round(averageValue * this.caculatorDate(endWeekDate, currentDate))
          : 0;
        tempValue += value;

      }
      dates.push({
        startDate: startDateValue,
        endDate: endDateValue,
        value,
        type: 'WEEK',
        isActive: 1
      });
      currentDate = this.addDays(currentDate, 7);
    }
    while (currentDate <= endDate);
    return dates;
  };

  getWeek(date) {
    const janFirst = new Date(date.getFullYear(), 0, 1);
    return Math.ceil((((date.getTime() - janFirst.getTime()) / 86400000) + janFirst.getDay() + 1) / 7);
  }

  isSameWeek(dateA, dateB) {
    return this.getWeek(dateA) === this.getWeek(dateB);
  }

  addDays(currentDate, days) {
    const date = new Date(currentDate);
    date.setDate(date.getDate() + days);
    return date;
  };

  generateRowHeight(row) {
    if (row?.targetSaleConfigDetail.length === 1) {
      return 50;
    }
    return 38 * row?.targetSaleConfigDetail.length;
  }

  checkRequired() {
    // return !(this.formSearch.status === 'VALID' && this.formSearch.value.value !== 0);
    return !(this.formSearch.status === 'VALID' && !this.flag);
  }

  generateDetailsTypeMonth(startDate, endDate, totalValue, autoAllocation) {
    const dates = [];
    let tempValue = 0;

    const totalDate = this.caculatorDate(endDate, startDate);
    const averageValue = totalValue / totalDate;
    let currentDate = new Date(startDate);
    currentDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
    while (currentDate <= endDate) {
      const endMonthDate = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0);
      let value;
      let startDateValue;
      let endDateValue;
      if (startDate.getMonth() === endDate.getMonth() && startDate.getFullYear() === endDate.getFullYear()) {
        startDateValue = formatDate(startDate, 'dd/MM/yyyy', 'en');
        endDateValue = formatDate(endDate, 'dd/MM/yyyy', 'en');
        value = autoAllocation ? totalValue - tempValue : totalValue;
      } else if (startDate > currentDate) {
        startDateValue = formatDate(startDate, 'dd/MM/yyyy', 'en');
        endDateValue = formatDate(endMonthDate, 'dd/MM/yyyy', 'en');
        value = autoAllocation ? Math.round(averageValue * (endMonthDate.getDate() - startDate.getDate() + 1)) : 0;
        tempValue += value;
      } else if (endDate <= endMonthDate) {
        startDateValue = formatDate(currentDate, 'dd/MM/yyyy', 'en');
        endDateValue = formatDate(endDate, 'dd/MM/yyyy', 'en');
        value = autoAllocation ? totalValue - tempValue : totalValue;
      } else {
        startDateValue = formatDate(currentDate, 'dd/MM/yyyy', 'en');
        endDateValue = formatDate(endMonthDate, 'dd/MM/yyyy', 'en');
        // value = autoAllocation ? Math.round(averageValue * (endMonthDate.getDate() - currentDate.getDate() + 1)) : 0;
        value = (autoAllocation && ((tempValue + Math.round(averageValue * (endMonthDate.getDate() - currentDate.getDate() + 1))) <= totalValue))
          ? Math.round(averageValue * (endMonthDate.getDate() - currentDate.getDate() + 1))
          : 0;
        tempValue += value;
      }
      dates.push({
        startDate: startDateValue,
        endDate: endDateValue,
        value,
        type: 'MONTH',
        isActive: 1
      });
      currentDate = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 1);
    }
    return dates;
  };

  caculatorDate(endDate, startDate) {
    return parseInt(String((endDate - startDate) / (1000 * 60 * 60 * 24)), 10) + 1;
  }

  delete(indexData, isMonth?: boolean) {
    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.result
      .then((res) => {
        if (res) {
          if (isMonth) {
            this.listMonthsData = this.listMonthsData.filter((item, index) => {
              return index !== indexData;
            });
          } else {
            this.listWeeksData = this.listWeeksData.filter((item, index) => {
              return index !== indexData;
            });
          }
        }
      })
      .catch(() => {});
  }

  update(row) {
    const modal = this.modalService.open(TargetSaleConfigEditModalComponent, {windowClass: 'target-sale-config-edit-modal'});
    modal.componentInstance.data = row;
    modal.componentInstance.commonData = this.commonData;
    modal.result
      .then((res) => {
        if (res) {
          this.editAction(row, res);
        } else {
        }
      })
      .catch(() => {
      });
  }

  saveAll() {
    this.isLoading = true;
    const month = Object.assign([], this.listMonthsData);
    const week = Object.assign([], this.listWeeksData);
    const arr = [...month, ...week];
    // const groupBy = arr.reduce((acc, cur) => {
    //   (acc[cur.product] && acc[cur.rmTitleId]) ? acc[cur.product].targetSaleConfigDetail
    //     = [ ...acc[cur.product].targetSaleConfigDetail, ...cur.targetSaleConfigDetail ] : acc[cur.product] = cur;
    //   return acc;
    // }, {});
    const data = {
      rsId: this.objFunctionTagetSale.rsId,
      scope: 'VIEW',
      data: arr
    }
    this.rmApi.createTargetSaleConfig(data).subscribe(listDataError => {
      if (!_.isEmpty(listDataError)) {
        this.isLoading = false;
        this.openModalError(listDataError);
      } else {
        this.isLoading = false;
        this.messageService.success(this.notificationMessage.success);
        // this.back();
        this.router.navigateByUrl('/kpis/kpi-management/kpi-config/target-sale', { state: this.prop ? this.prop : this.state });
      }
    }, error => {
      this.isLoading = false;
      this.messageService.error(this.notificationMessage.error);
    });
  }

  checkExistWithKeys(type?) {
    // keys
    const rmTitleId = this.formSearch.controls.rmTitleId.value;
    const rmLevelId = this.formSearch.controls.rmLevelId.value;
    const branchCode = this.formSearch.controls.branchCode.value;
    const product = this.formSearch.controls.product.value;
    const listData = this.allotmentValue === 'ALL' ? [...this.listWeeksData, ...this.listMonthsData] : this.allotmentValue === 'MONTH' ? this.listMonthsData : this.listWeeksData;
    let flag = false;
    // TH chọn ALL level RM
    if (rmLevelId === 'ALL') {
      let count = 0;
      this.commonData.listLevelRM.forEach(item => {
        if (item.id !== 'ALL') {
          const temp = _.filter(listData,
            i => (i.rmTitleId === rmTitleId && i.rmLevelId === item.id && i.branchCode === branchCode && i.product === product));
          count += temp.length;
        }
      });
      if (count && count === this.commonData.listLevelRM.length - 1 && this.allotmentValue !== 'ALL') {
        flag = true;
      } else if (count && count === (this.commonData.listLevelRM.length - 1) * 2 && this.allotmentValue === 'ALL') {
        flag = true;
      }
      if (flag) {
        this.translate
          .get('notificationMessage.target_sale_config_duplicate',
            {
              product, title: _.find(this.commonData.listTitleGroup, i => i.id === rmTitleId).name,
              level: _.find(this.commonData.listLevelRM, i => i.id === rmLevelId).name,
              branch: _.find(this.commonData.listBranch, i => i.code === branchCode).displayName,
              allot: _.find(this.commonData.listAllotmentValue, i => i.code === this.allotmentValue).name
            })
          .subscribe((data) => {
            this.openPopupInfo(data);
          });
        return true;
      }
    }
    // TH chọn 1 level RM
    const data = _.filter(listData, i => (i.rmTitleId === rmTitleId && i.rmLevelId === rmLevelId && i.branchCode === branchCode && i.product === product));
    let dataNoti = '';
    this.translate
      .get('notificationMessage.target_sale_config_duplicate',
        {
          product, title: _.find(this.commonData.listTitleGroup, i => i.id === rmTitleId).name,
          level: _.find(this.commonData.listLevelRM, i => i.id === rmLevelId).name,
          branch: _.find(this.commonData.listBranch, i => i.code === branchCode).displayName,
          allot: _.find(this.commonData.listAllotmentValue, i => i.code === this.allotmentValue).name
        })
      .subscribe((res) => {
        dataNoti = res;
      });
    if (data && data.length > 0 && this.allotmentValue !== 'ALL') {
      this.openPopupInfo(dataNoti);
      return true;
    } else if (data && data.length > 1 && this.allotmentValue === 'ALL') {
      this.openPopupInfo(dataNoti);
      return true;
    }
    return false;
  }

  openModalError(listDataError) {
    const modalError = this.modalService.open(TargetSaleConfigErrorModalComponent, {windowClass: 'list__target-sale-config-error'});
    modalError.componentInstance.dataError = listDataError;
    modalError.result
      .then(() => {

      })
      .catch(() => {
      });
  }

  editAction(oldObj, newObj) {
    const indexMonth = _.findIndex(this.listMonthsData, i => (i.rmTitleId === oldObj.rmTitleId && i.rmLevelId === oldObj.rmLevelId && i.branchCode === oldObj.branchCode && i.product === oldObj.product));
    const indexWeek = _.findIndex(this.listWeeksData, i => (i.rmTitleId === oldObj.rmTitleId && i.rmLevelId === oldObj.rmLevelId && i.branchCode === oldObj.branchCode && i.product === oldObj.product));
    if (oldObj.startDate === newObj.startDate && oldObj.endDate === oldObj.startDate) {
      const dataMonth: any = {
        ...newObj,
        targetSaleConfigDetail: this.generateDetailsTypeMonth(newObj.startDate, newObj.endDate, newObj.value, newObj.autoAllocation),
        startDate: formatDate(newObj.startDate, 'dd/MM/yyyy', 'en'),
        endDate: formatDate(newObj.endDate, 'dd/MM/yyyy', 'en')
      };
      const dataWeek: any = {
        ...newObj,
        targetSaleConfigDetail: this.generateDetailsTypeWeek(newObj.startDate, newObj.endDate, newObj.value, newObj.autoAllocation),
        startDate: formatDate(newObj.startDate, 'dd/MM/yyyy', 'en'),
        endDate: formatDate(newObj.endDate, 'dd/MM/yyyy', 'en')
      };
      if (indexMonth !== -1) {
        this.listMonthsData[indexMonth] = dataMonth;
        const tempM = Object.assign([], this.listMonthsData);
        this.listMonthsData = tempM;
      }
      if (indexWeek !== -1) {
        this.listWeeksData[indexWeek] = dataWeek;
        const tempW = Object.assign([], this.listWeeksData);
        this.listWeeksData = tempW;
      }
      console.log('dataMonth', dataMonth);
      console.log('dataWeek', dataWeek);
    } else {
      if (indexWeek !== -1) {
        this.listWeeksData[indexWeek].value = newObj.value;
        this.listWeeksData[indexWeek].autoAllocation = newObj.autoAllocation;
      }
      if (indexMonth !== -1) {
        this.listMonthsData[indexMonth].value = newObj.value;
        this.listMonthsData[indexMonth].autoAllocation = newObj.autoAllocation;
      }
      // thay đổi
      // set lại ngày phân bổ của toàn bộ dữ liệu cũ
      const oldData = [...this.listMonthsData, ...this.listWeeksData];
      this.listMonthsData = [];
      this.listWeeksData = [];
      oldData.forEach(item => {
        item.startDate = newObj.startDate;
        item.endDate = newObj.endDate;
      });
      for (const temp of oldData) {
        // thêm mới
        if ((temp.allotmentValue === 'WEEK' || temp.allotmentValue === 'ALL')) {
          const data: any = {
            isActive: 1,
            targetSaleConfigDetail: this.generateDetailsTypeWeek(temp.startDate, temp.endDate, temp.value, temp.autoAllocation),
          };
          data.startDate = formatDate(temp.startDate, 'dd/MM/yyyy', 'en');
          data.endDate = formatDate(temp.endDate, 'dd/MM/yyyy', 'en');
          this.listWeeksData.push({...temp, ...data, allotmentValue: 'WEEK'})
          const tempData = Object.assign([], this.listWeeksData);
          this.listWeeksData = tempData;
          console.log('listWeeksData', this.listWeeksData);
        }
        if ((temp.allotmentValue === 'MONTH' || temp.allotmentValue === 'ALL')) {
          const data: any = {
            isActive: 1,
            targetSaleConfigDetail: this.generateDetailsTypeMonth(temp.startDate, temp.endDate, temp.value, temp.autoAllocation),
          };
          data.startDate = formatDate(temp.startDate, 'dd/MM/yyyy', 'en');
          data.endDate = formatDate(temp.endDate, 'dd/MM/yyyy', 'en');
          this.listMonthsData.push({...temp, ...data, allotmentValue: 'MONTH'});
          const tempData = Object.assign([], this.listMonthsData);
          this.listMonthsData = tempData;
          console.log('listMonthsData', this.listMonthsData);
        }
      }
    }
  }

  openPopupInfo(message) {
    const confirm = this.modalService.open(ConfirmDialogComponent, {windowClass: 'confirm-dialog'});
    confirm.componentInstance.type = ConfirmType.CusError;
    confirm.componentInstance.message = message;
    confirm.result
      .then((res) => {
        if (res) {
          console.log(res);
        }
      })
      .catch(() => {
      });
  }

  changeValue(chilIndex, parentIndex, isMonth: boolean) {
    if (!isMonth) {
      // tổng giá trị trong chi tiết
      let totalValueDetail = 0;
      this.listWeeksData[parentIndex].targetSaleConfigDetail.forEach(item => {
        totalValueDetail += item.value;
      });
      // phần dư
      let remainingValue = totalValueDetail - this.listWeeksData[parentIndex].value;
      let i = this.listWeeksData[parentIndex].targetSaleConfigDetail.length - 1;
      // vòng lặp
      // lặp arr từ index cuối lên đầu
      while (i >= 0) {
        if (i !== chilIndex) {
          // nếu phần tử ko đủ giá trị để trừ đi phần dư
          if (remainingValue >= this.listWeeksData[parentIndex].targetSaleConfigDetail[i].value) {
            // tính lại phần dư
            remainingValue = remainingValue - this.listWeeksData[parentIndex].targetSaleConfigDetail[i].value;
            // gán phần từ thành 0
            this.listWeeksData[parentIndex].targetSaleConfigDetail[i].value = 0;
          } else {
            // trừ nốt phần dư
            this.listWeeksData[parentIndex].targetSaleConfigDetail[i].value = this.listWeeksData[parentIndex].targetSaleConfigDetail[i].value - remainingValue;
            break;
          }
        }
        i--;
      }
    } else {
      // tổng giá trị trong chi tiết
      let totalValueDetail = 0;
      this.listMonthsData[parentIndex].targetSaleConfigDetail.forEach(item => {
        totalValueDetail += item.value;
      });
      // phần dư
      let remainingValue = totalValueDetail - this.listMonthsData[parentIndex].value;
      let i = this.listMonthsData[parentIndex].targetSaleConfigDetail.length - 1;
      // vòng lặp
      // lặp arr từ index cuối lên đầu
      while (i >= 0) {
        if (i !== chilIndex) {
          // nếu phần tử ko đủ giá trị để trừ đi phần dư
          if (remainingValue >= this.listMonthsData[parentIndex].targetSaleConfigDetail[i].value) {
            // tính lại phần dư
            remainingValue = remainingValue - this.listMonthsData[parentIndex].targetSaleConfigDetail[i].value;
            // gán phần từ thành 0
            this.listMonthsData[parentIndex].targetSaleConfigDetail[i].value = 0;
          } else {
            // trừ nốt phần dư
            this.listMonthsData[parentIndex].targetSaleConfigDetail[i].value = this.listMonthsData[parentIndex].targetSaleConfigDetail[i].value - remainingValue;
            break;
          }
        }
        i--;
      }
    }
  }

  getRMLevelName(rmLevelId, rmTitleId) {
    const title = _.find(this.commonData.listTitleGroup, item => (item.id === rmTitleId));
    return _.find(title.levels, i => i.id === rmLevelId).name;
  }

  isChangeDate() {
    const listData = [...this.listWeeksData, ...this.listMonthsData];
    return (listData.length > 0)
      && (listData[0].startDate !== formatDate(this.formSearch.get('startDate').value, 'dd/MM/yyyy', 'en')
        || listData[0].endDate !== formatDate(this.formSearch.get('endDate').value, 'dd/MM/yyyy', 'en'));
  }

  clearData() {
    const rmTitleId = this.formSearch.controls.rmTitleId.value;
    const branchCode = this.formSearch.controls.branchCode.value;
    const product = this.formSearch.controls.product.value;
    const allotmentValue = this.allotmentValue;

    if (allotmentValue === 'ALL') {
      _.remove(this.listMonthsData, i => (i.rmTitleId === rmTitleId && i.branchCode === branchCode && i.product === product));
      _.remove(this.listWeeksData, i => (i.rmTitleId === rmTitleId && i.branchCode === branchCode && i.product === product));
    } else if (allotmentValue === 'MONTH') {
      _.remove(this.listMonthsData, i => (i.rmTitleId === rmTitleId && i.branchCode === branchCode && i.product === product));
    } else {
      _.remove(this.listWeeksData, i => (i.rmTitleId === rmTitleId && i.branchCode === branchCode && i.product === product));
    }
  }

  hasDataDifferentAllotmentType(allotment) {
    // lấy
    const data = allotment === 'MONTH' ? this.listWeeksData : this.listMonthsData;
    const rmTitleId = this.formSearch.controls.rmTitleId.value;
    const branchCode = this.formSearch.controls.branchCode.value;
    const product = this.formSearch.controls.product.value;
    return _.find(data, i => (i.rmTitleId === rmTitleId && i.branchCode === branchCode && i.product === product));

  }

  onChangeValue(event) {
    this.flag = event.value === '-' || event.value < 1 || event.value === null;
  }
}

