import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiAssignmentRmSearchComponent } from './kpi-assignment-rm-search.component';

describe('KpiAssignmentRmSearchComponent', () => {
  let component: KpiAssignmentRmSearchComponent;
  let fixture: ComponentFixture<KpiAssignmentRmSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiAssignmentRmSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiAssignmentRmSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
