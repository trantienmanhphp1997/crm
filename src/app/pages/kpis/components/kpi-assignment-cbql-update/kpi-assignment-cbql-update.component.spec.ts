import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {KpiAssignmentCbqlUpdateComponent} from './kpi-assignment-cbql-update.component';

describe('KpiAssignmentCbqlUpdateComponent', () => {
	let component: KpiAssignmentCbqlUpdateComponent;
	let fixture: ComponentFixture<KpiAssignmentCbqlUpdateComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [KpiAssignmentCbqlUpdateComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(KpiAssignmentCbqlUpdateComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
