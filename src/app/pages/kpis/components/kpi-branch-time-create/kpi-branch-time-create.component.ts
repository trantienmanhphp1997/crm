import {forkJoin, of} from 'rxjs';
import {Component, Injector, OnInit} from '@angular/core';
import {BaseComponent} from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import {CampaignsService} from 'src/app/pages/campaigns/services/campaigns.service';
import {CommonCategory, FunctionCode} from 'src/app/core/utils/common-constants';
import {catchError} from 'rxjs/operators';
import {KpiBranchTimeApi} from '../../apis/kpi-branch-time.api';
import * as moment from 'moment';
import {FormGroup} from '@angular/forms';
import {cleanDataForm, validateAllFormFields} from 'src/app/core/utils/function';
import {CustomValidators} from 'src/app/core/utils/custom-validations';

@Component({
	selector: 'app-kpi-branch-time-create',
	templateUrl: './kpi-branch-time-create.component.html',
	styleUrls: ['./kpi-branch-time-create.component.scss'],
})
export class KpiBranchTimeCreateComponent extends BaseComponent implements OnInit {
	blockCode: String;
	isValidator = false;
	listData = [];
	listBranchTypeKPI = [];
	isLoading = false;
	listBlock = [];
	formSearch: FormGroup;
	formCreate = this.fb.array([]);
	minDate: any;
	isCreate = true;

	constructor(
		injector: Injector,
		private campaignService: CampaignsService,
		private kpiBranchTimeApi: KpiBranchTimeApi
	) {
		super(injector);
		this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.KPI_FACTOR_BRANCHTIME}`);
		this.isLoading = true;
	}

	ngOnInit(): void {
		this.blockCode = this.route.snapshot.paramMap.get('blockCode');
		if (this.blockCode) {
			this.formSearch = this.fb.group({
				blockCode: {value: this.blockCode, disabled: true}, // Ma khoi edit
			});
			this.isCreate = false;
		} else {
			this.formSearch = this.fb.group({
				blockCode: 'SME', // Ma khoi - Mac dinh la SME
			});
		}

		this.minDate = new Date(moment().subtract(1, 'month').startOf('month').valueOf());
		const params = cleanDataForm(this.formSearch);
		forkJoin([
			this.commonService
				.getCommonCategory(CommonCategory.KPI_SYS_ORG_TYPE)
				.pipe(catchError((e) => of(undefined))),
			this.campaignService.getBlockMapping().pipe(catchError((e) => of(undefined))),
			this.kpiBranchTimeApi.getKpiBranchTimeByBlockCode(params).pipe(catchError((e) => of(undefined))),
		]).subscribe(([listBranchTypeKPI, listBlock, listData]) => {
			this.isLoading = false;
			this.listBranchTypeKPI = listBranchTypeKPI?.content || [];
			// map list block
			this.listBlock =
				listBlock?.content.map((item) => {
					return {code: item.code, name: item.code + ' - ' + item.name};
				}) || [];
			this.listData = listData;
			this.mapData();
		});
	}

	mapData() {
		this.formCreate = this.fb.array([]);
		this.listBranchTypeKPI.forEach((branchType, i) => {
			const oldData = this.listData?.find((e) => e.kpiSysOrgTypeCode === branchType.code);
			if (oldData) {
				this.formCreate.push(this.updateItem(oldData));
				// Map minExpireDate
				if (oldData?.efficientDate) {
					this.listBranchTypeKPI[i].minExpireDate = new Date(
						moment(oldData?.efficientDate).startOf('day').valueOf()
					);
				}
				// Map maxEfficientDate
				if (oldData?.expireDate) {
					this.listBranchTypeKPI[i].maxEfficientDate = new Date(
						moment(oldData?.expireDate).startOf('day').valueOf()
					);
				}
			} else {
				this.formCreate.push(this.createItem(branchType));
			}
		});

		this.formCreate.controls.forEach((fb: FormGroup, i) => {
			fb.controls.efficientDate.valueChanges.subscribe((value) => {
				const minExpireDate = new Date(moment(value).startOf('day').valueOf());
				this.listBranchTypeKPI[i].minExpireDate = minExpireDate;
			});
			fb.controls.expireDate.valueChanges.subscribe((value) => {
				const maxEfficientDate = new Date(moment(value).startOf('day').valueOf());
				this.listBranchTypeKPI[i].maxEfficientDate = maxEfficientDate;
			});
		});
	}

	onChangeBlockCode() {
		const params = cleanDataForm(this.formSearch);
		forkJoin([
			this.kpiBranchTimeApi.getKpiBranchTimeByBlockCode(params).pipe(catchError((e) => of(undefined))),
		]).subscribe(([listData]) => {
			this.listData = listData;
			this.mapData();
		});
	}

	createItem(branchType) {
		return this.fb.group({
			value: ['', [CustomValidators.required, CustomValidators.valueFactor]],
			efficientDate: ['', CustomValidators.required],
			expireDate: '',
			kpiSysOrgTypeCode: branchType.code,
			kpiTimeOrgFactorId: '',
		});
	}

	updateItem(oldData) {
		return this.fb.group({
			...oldData,
			value: [
				oldData.value.toString()?.replace('.', ','),
				[CustomValidators.required, CustomValidators.valueFactor],
			],
			efficientDate: [
				new Date(moment(oldData.efficientDate, 'YYYY-MM-DD')?.startOf('day').valueOf()),
				CustomValidators.required,
			],
			expireDate: oldData?.expireDate
				? new Date(moment(oldData.expireDate, 'YYYY-MM-DD')?.startOf('day').valueOf())
				: '',
		});
	}

	// log(val) {
	// 	console.log('val', val);
	// }

	create() {
		// console.log(this.formCreate);
		if (this.formCreate.valid) {
			const data = [];
			this.formCreate.value.forEach((element) => {
				data.push({
					...element,
					expireDate: element.expireDate ? moment(element.expireDate).format('YYYY-MM-DD') : '',
					efficientDate: moment(element.efficientDate).format('YYYY-MM-DD'),
					blockCode: this.formSearch.value.blockCode,
					value: parseFloat(element.value.replace(',', '.')),
				});
			});

			this.confirmService.confirm().then((res) => {
				if (res) {
					this.isLoading = true;
					this.kpiBranchTimeApi.saveKpiTimeOrgFactor(data).subscribe(
						() => {
							this.isLoading = false;
							this.back();
							this.messageService.success(this.notificationMessage.success);
						},
						(e) => {
							this.isLoading = false;
							if (e?.status === 0) {
								this.messageService.error(this.notificationMessage.E001);
								return;
							}
							this.messageService.error(e?.error?.description);
						}
					);
				}
			});
		} else {
			this.isValidator = true;
			this.formCreate.controls.forEach((element: FormGroup) => {
				validateAllFormFields(element);
			});
		}
	}
}
