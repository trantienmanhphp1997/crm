import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiBranchTimeCreateComponent } from './kpi-branch-time-create.component';

describe('KpiBranchTimeCreateComponent', () => {
  let component: KpiBranchTimeCreateComponent;
  let fixture: ComponentFixture<KpiBranchTimeCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiBranchTimeCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiBranchTimeCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
