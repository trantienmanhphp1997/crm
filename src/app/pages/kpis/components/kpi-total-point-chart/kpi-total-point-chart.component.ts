import { Component, HostListener, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
import { ProgressBarClass } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'app-kpi-total-point-chart',
  templateUrl: './kpi-total-point-chart.component.html',
  styleUrls: ['./kpi-total-point-chart.component.scss']
})
export class KpiTotalPointChartComponent implements OnInit {
  @Input('data') data: any;

  isLoading = true;
  title = "kpi.title.kpi_targets";

  chartOptions: any;
  gaugeNeedle = {};

  stardardCoreOptions = {
    grid: {
      containLabel: true
    },
    series: [
      {
        type: 'gauge',
        min: 0,
        max: 200,
        splitNumber: 100 ,
        axisLine: {
          lineStyle: {
            width: 40,
            color: [
              [0.2, '#67e0e3'],
              [0.3, '#37a2da'],
              [0.5, 'red'],
              [0.6, 'green'],
              [1, 'grey']
            ]
          }
        },
        pointer: {
          itemStyle: {
            color: 'auto'
          }
        },
        axisTick: {
          distance: -30,
          length: 8,
          lineStyle: {
            color: '#fff',
            width: 0
          }
        },
        splitLine: {
          distance: -30,
          length: 30,
          lineStyle: {
            color: '#fff',
            width: 0
          }
        },
        axisLabel: {
          color: 'auto',
          distance: -20,
          fontSize: 0
        },
        detail: {
          valueAnimation: true,
          formatter: (value) => {
            return this.rankData?.sumPointEntrust || 0;
          },
          color: 'auto',
          fontSize: this.fontSize
        },
        data: [
          {
            value: 0
          }
        ]
      }
    ]
  };

  distributionPointOptions = {
    grid: {
      containLabel: true
    },
    xAxis: {
      type: 'category',
      data: []
    },
    yAxis: {
      type: 'value'
    },
    series: [
      {
        data: [],
        type: 'line',
        areaStyle: {
          color: '#D5FDE7',
          opacity: 0.5
        },
        label: {
          show: true,
          position: 'top',
          formatter: (data) => { return `${data.value}%` }
        },
      }
    ]
  };

  avgPointEntrust: any;
  rankData: any;
  type;


  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.setSmallDesktopOptions();
  }

  constructor() { 
    this.setSmallDesktopOptions();
  }

  ngOnInit(): void {
    this.getData();
    
  }

  get generateClassProgressBar(): string {
    return ProgressBarClass[4];
  }

  getData(): void {
    const { avg, sum4months, rank, type } = this.data;
    this.avgPointEntrust = avg?.avgPointEntrust;
    this.rankData = { ...rank };
    this.type = type;

    if (Object.keys(this.rankData).length !== 0) {
      if(this.rankData?.kpiRankingCategories && this.rankData?.kpiRankingCategories.length){
        this.rankData.list = this.rankData?.kpiRankingCategories?.map(data => {
          return { color: data.color, value: this.calculateRank(data.hightValue) }
        });
      }

      // init chart data
      this.stardardCoreColor = this.rankData.list;
      this.mainPoint = this.rankData.sumPointEntrust;
    }

    // summary 4 months
    if (sum4months && sum4months.length !== 0) {
      this.distributionPointOptions.xAxis.data = sum4months?.map(data => { return moment(data.bussinessDate).format(type === 'month' ? 'MM/YY' : 'DD/MM') });
      this.distributionPointOptions.series[0].data = sum4months?.map(data => { return data.sumPointEntrust });
    }

    this.stardardCoreOptions = { ...this.stardardCoreOptions };
    this.isLoading = false;
  }

  // max 200 point
  calculateRank(value: number): number {
    return value / 200;
  }

  setSmallDesktopOptions(): void {
    let lineStyle = this.stardardCoreOptions.series[0].axisLine.lineStyle; 
    
    if (window.innerWidth < 1500) {
      lineStyle.width = 35;
    } else {
      lineStyle.width = 40;
    }
    this.stardardCoreOptions.series[0].detail.fontSize = this.fontSize;
    this.stardardCoreOptions = {...this.stardardCoreOptions};
  }

  set stardardCoreColor(value: any[]) {
    if(value && value.length){
      this.stardardCoreOptions.series[0].axisLine.lineStyle.color = value?.map(data => { return [data.value, data.color] });
    }
  }

  set mainPoint(value: number) {
    this.stardardCoreOptions.series[0].data[0].value = value;
  }

  get isEmpty(): boolean {
    const isEmptyData = Object.keys(this.data).every(key => {
      const v = this.data[key]
      return Array.isArray(v) ? v.length === 0 : v ? Object.keys(v).length === 0 : true
    });
    return !this.isLoading && isEmptyData;
  }

  get isRankDataEmpty(): boolean {
    return Object.keys(this.rankData).length === 0;
  }

  get isSum4MonthsEmpty(): boolean {
    return this.data.sum4months.length === 0;
  }

  get fontSize(): number {
    return window.innerWidth < 1500 ? 15 : 20;
  }
}
