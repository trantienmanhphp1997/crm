import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiTotalPointChartComponent } from './kpi-total-point-chart.component';

describe('KpiTotalPointChartComponent', () => {
  let component: KpiTotalPointChartComponent;
  let fixture: ComponentFixture<KpiTotalPointChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiTotalPointChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiTotalPointChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
