import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { SessionKey } from 'src/app/core/utils/common-constants';
import * as _ from 'lodash';
import { maxInt32, typeExcel } from 'src/app/core/utils/common-constants';
import * as moment from 'moment';
import { StandardKpiCbqlApi } from '../../apis/standard-kpi-cbql.api';
import { formatDate } from '@angular/common';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Utils } from 'src/app/core/utils/utils';
import { FileService } from 'src/app/core/services/file.service';

@Component({
  selector: 'standard-kpi-cbql-import',
  templateUrl: './standard-kpi-cbql-import.component.html',
  styleUrls: ['./standard-kpi-cbql-import.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class StandardKpiCBQLImportComponent extends BaseComponent implements OnInit {
  constructor(injector: Injector, private standardKpiCbqlApi: StandardKpiCbqlApi, private fileService: FileService) {
    super(injector);
  }
  form = this.fb.group({
    divisionCode: [''],
    month: [new Date()],
  });

  files: any;
  isUpload: boolean;
  fileName: string;
  isFile: boolean;
  listData = [];
  listDivision = [];
  fileImport: File;
  listDataTable = [];
  hiddenTable: boolean;

  ngOnInit() {
    this.hiddenTable = false;
    const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
    this.listDivision =
      divisionOfUser?.map((item) => {
        return { code: item.code, displayName: `${item.code || ''} - ${item.name || ''}` };
      }) || [];
    this.form.controls.divisionCode.setValue(_.first(this.listDivision)?.code);
  }

  handleFileInput(files) {
    if (files && files.length > 0) {
      if (files?.item(0)?.size > 10485760) {
        this.messageService.warn(this.notificationMessage.ECRM005);
        return;
      }
      if (!typeExcel.includes(files?.item(0)?.type)) {
        this.messageService.error(this.notificationMessage.CANNOT_READ_DATA_FROM_FILE);
        return;
      }
      this.isFile = true;
      this.fileImport = files.item(0);
      this.fileName = files.item(0).name;
    } else {
      this.isFile = false;
    }
  }

  clearFile() {
    this.isUpload = false;
    this.isFile = false;
    this.fileName = null;
    this.fileImport = null;
    this.files = null;
    this.listData = [];
    this.listDataTable = [];
    this.hiddenTable = false;
  }

  importFile() {
    if (this.isFile && !this.isUpload) {
      this.isUpload = true;
      this.isLoading = true;
      const formData: FormData = new FormData();
      let dateImport = formatDate(this.form.get('month').value, 'MM/yyyy', 'en')?.split('/');
      formData.append('file', this.fileImport);
      formData.append(
        'dto',
        JSON.stringify({ month: dateImport[0], year: dateImport[1], blockCode: this.form.get('divisionCode').value })
      );
      this.standardKpiCbqlApi.importKpiStandardManagerTemplate(formData).subscribe(
        (res) => {
          this.isLoading = false;
          if (res.errorType === 'FORMAT') {
            this.hiddenTable = true;
            this.tableTemplate(res);
          } else if (!res.error) {
            this.saveImport(res);
          } else if (res.errorType === 'TEMPLATE') {
            this.messageService.error(res.errorMessage);
          }
        },
        (error) => {
          this.isLoading = false;
          this.messageService.error('Lỗi');
        }
      );
    }
  }

  tableTemplate(dateTemplate) {
    this.listData.push(dateTemplate);
    for (let index = 0; index < this.listData[0]?.data?.length; index++) {
      if (this.listData[0].data[index].row > 1 && this.listData[0].data[index].column > 1) {
        if (_.isString(Number(this.listData[0].data[index].value))) {
          this.listData[0].data[index].value = null;
          this.listData[0].data[index].error = true;
        }
      }
    }
    const dataGroupBy = _.groupBy(_.orderBy(this.listData[0].data, 'row'), (item) => item.row);
    Object.keys(dataGroupBy).forEach((key) => {
      this.listDataTable.push(dataGroupBy[key]);
    });
  }

  saveImport(param) {
    this.isLoading = true;
    this.standardKpiCbqlApi.saveKpiStandardManagerTemplate(param).subscribe(
      (res) => {
        if (res) {
          this.isLoading = false;
          this.messageService.success(this.notificationMessage.success);
          this.hiddenTable = true;
          this.tableTemplate(param);
        }
      },
      (error) => {
        this.messageService.error(this.notificationMessage.error);
        this.isLoading = false;
      }
    );
  }

  save() {
    const reg = new RegExp(/[^\d.-]/g);
    const reg_decimal_limit_2digits = /^[0-9]+\.\d{1,2}$/;

    for (let index = 0; index < this.listData[0].data.length; index++) {
      if (this.listData[0].data[index].row > 1 && this.listData[0].data[index].column > 1) {
        if (!this.listData[0].data[index].value.toString()) {
          this.messageService.error(
            'Chưa nhập dữ liệu dòng ' + this.listData[0].data[index].row + ' cột ' + this.listData[0].data[index].column
          );
          this.listData[0].data[index].error = true;
          return;
        } else {
          let value = this.listData[0].data[index].value;
          const invalidNumber = reg.test(value);
          // max 2 digits to the right of decimal point
          const invalidDecimal = value % 1 != 0 && !reg_decimal_limit_2digits.test(value);
          
          if (invalidNumber || invalidDecimal) {
            this.messageService.error('Nhập sai định dạng dòng ' + this.listData[0].data[index].row + ' cột ' + this.listData[0].data[index].column);
            this.listData[0].data[index].error = true;
            return;
          } else {
            this.listData[0].data[index].error = false;
          }
        }
      }
    }
    this.confirmService.confirm().then((res) => {
      if (res) {
        this.isLoading = true;
        this.listData[0].data = [];
        this.listDataTable?.forEach((item) => {
          this.listData[0].data = [...this.listData[0].data, ...item];
        });
        this.listData[0].error = false;
        this.standardKpiCbqlApi.saveKpiStandardManagerTemplate(this.listData[0]).subscribe(
          (res) => {
            if (res) {
              this.isLoading = false;
              this.messageService.success(this.notificationMessage.success);
              this.back();
            }
          },
          (error) => {
            this.messageService.error(this.notificationMessage.error);
            this.isLoading = false;
          }
        );
      }
    });
  }

  downloadTemplate() {
    this.isLoading = true;
    let dateImport = formatDate(this.form.get('month').value, 'MM/yyyy', 'en')?.split('/');
    const paramExport = { month: dateImport[0], year: dateImport[1], blockCode: this.form.get('divisionCode').value };
    this.standardKpiCbqlApi
      .exportKpiStandardManagerTemplate(paramExport)
      .pipe(catchError(() => of(undefined)))
      .subscribe((res) => {
        if (Utils.isStringNotEmpty(res)) {
          this.dowloadFile(res);
        } else {
          this.messageService.error(this.notificationMessage.error);
          this.isLoading = false;
        }
      });
  }

  dowloadFile(fieId: string) {
    this.fileService.downloadFile(fieId, 'Template Import KPI Tieu chuan CBQL.xlsx').subscribe(
      (res) => {
        this.isLoading = false;
        if (!res) {
          this.messageService.error(this.notificationMessage.error);
        }
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  selectedData(value) {
    this.clearFile();
  }
}
