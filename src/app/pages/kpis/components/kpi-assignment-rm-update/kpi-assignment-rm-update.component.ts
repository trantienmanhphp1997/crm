import { of } from 'rxjs';
import { ChangeDetectorRef, Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { FunctionCode } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import {
  CODE_FIRST_6M,
  CODE_LAST_6M,
  LIST_COLUMN_FIRST,
  LIST_COLUMN_LAST,
  LIST_COLUMN_VALUE,
  LIST_SIX_MONTH,
  LIST_YEAR,
  SOURCE_INPUT,
} from '../../constant';
import { KpiAssignmentApi } from '../../apis';
import * as moment from 'moment';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-kpi-assignment-rm-update',
  templateUrl: './kpi-assignment-rm-update.component.html',
  styleUrls: ['./kpi-assignment-rm-update.component.scss'],
})
export class KpiAssignmentRmUpdateComponent extends BaseComponent implements OnInit {
  yearAfterSearch = moment().year();
  isFirstHalf = parseInt(moment().format('MM')) <= 6;
  isLoading = false;
  listBlock = [];
  formSearch = this.fb.group({
    blockCode: '',
    rmTitleCode: '',
    rmLevelCode: '',
    month: this.isFirstHalf ? CODE_FIRST_6M : CODE_LAST_6M,
    year: parseInt(moment().format('yyyy')),
  });
  formCreate = this.fb.array([]);
  listLevel = [];
  listKpiItem = [];
  listGroupByCategoryType = [];
  listKpiItemMapping = [];
  listYear = LIST_YEAR;
  listMonth = LIST_SIX_MONTH;
  listColumnValue = this.isFirstHalf ? LIST_COLUMN_FIRST : LIST_COLUMN_LAST;
  codeSelected = '';
  dataFromView: any = {};
  isPressSave = false;

  constructor(injector: Injector, private cd: ChangeDetectorRef, private kpiAssignmentApi: KpiAssignmentApi) {
    super(injector);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.dataFromView = this.sessionService.getSessionData(FunctionCode.KPI_ASSIGNMENT_RM_DETAIL);
    this.isLoading = false;
    this.onSearchKpiAssign();
  }

  getColName = (colObject) => {
    return colObject.name;
  };

  onTreeAction(event: any) {
    const row = event.row;

    if (row.treeStatus === 'collapsed') {
      row.treeStatus = 'expanded';
    } else {
      row.treeStatus = 'collapsed';
    }

    this.listKpiItemMapping = [...this.listKpiItemMapping];
    this.cd.detectChanges();
  }

  editColumn = (code) => {
    this.isLoading = true;
    this.kpiAssignmentApi.getAggregteMonthMax().subscribe(
      (res) => {
        this.isLoading = false;
        const month = LIST_COLUMN_VALUE.find((item) => item.code === code).month;
        const isNeedCheck = month >= 1 && month <= 12;
        if (
          isNeedCheck &&
          (res.year > this.yearAfterSearch || (res.year == this.yearAfterSearch && month <= res.month))
        ) {
          this.confirmService.confirm(this.fields.confirm_aggregte).then((res) => {
            if (res) {
              this.confirmEditColumn(code);
            }
          });
        } else {
          this.confirmEditColumn(code);
        }
      },
      (e) => {
        this.isLoading = false;
        if (e?.status === 0) {
          this.messageService.error(this.notificationMessage.E001);
          return;
        }
        this.messageService.error(e?.error?.description);
      }
    );
  };

  confirmEditColumn = (code) => {
    this.isPressSave = false;
    this.codeSelected = code;
    this.formCreate.controls.forEach((item: FormGroup, index) => {
      const target = this.listKpiItemMapping[index]?.mappingTargets?.find(
        (target: any) => target.code === this.codeSelected
      );

      target && item.setValue({ ...item.value, value: target.kpiStandardId ? target?.assignValue || '' : 0 });
    });
  };

  isExist = (value) => value !== null && value !== undefined && value !== '';

  onSearchKpiAssign = async () => {
    this.formCreate.clear();
    const formValue = this.formSearch.value;
    this.isLoading = true;
    this.listColumnValue = formValue.month === CODE_FIRST_6M ? LIST_COLUMN_FIRST : LIST_COLUMN_LAST;

    let params = {
      branchCode: this.dataFromView.branchCode,
      blockCode: this.dataFromView.block.code,
      rmTitleCode: this.dataFromView.rmTitleCode,
      rmLevelCode: this.dataFromView.rmLevelCode,
      hrisCode: this.dataFromView.hrisCode,
      rmCode: this.dataFromView.rmCode,
      month: formValue.month,
      year: formValue.year,
      sysOrgTypeCode: '',
      locationCode: '',
      isRM: true,
      assignDate: this.dataFromView.assignedDate,
      endAssignDate: this.dataFromView.endDate ? this.dataFromView.endDate : '',
      isInputKpiRm: true,
    };

    const listKpiByTitle =
      (
        await this.kpiAssignmentApi
          .searchKpiAssign(params)
          .pipe(catchError((e) => of(undefined)))
          .toPromise()
      )?.sort((a, b) => (a.categoryCode >= b.categoryCode ? 1 : -1)) || [];
    this.listKpiItemMapping = [];
    // group by categoryCode
    const listGroupByCategoryType = Object.values(
      listKpiByTitle?.reduce(
        (r, a) => ({
          ...r,
          [a.categoryCode]: [...(r[a.categoryCode] || []), a],
        }),
        {}
      )
    );

    listGroupByCategoryType.forEach((e: Array<any>) => {
      // first level nested
      this.listKpiItemMapping.push({
        idGroup: e[0]?.categoryCode,
        name: e[0]?.categoryName,
        treeStatus: 'expanded',
        parentId: null,
      });
      this.formCreate.push(this.fb.group({}));

      e.forEach((i) => {
        const frequencyNames = [];
        i.frequencies?.length && i.frequencies.forEach((element) => frequencyNames.push(element.frequencyName));
        const sourceInputName = SOURCE_INPUT.find((source) => source.code == i.sourceInput)?.name || ''; // == cause different type
        // last level nested
        this.listKpiItemMapping.push({
          ...i,
          // idGroup is required cause prevent render error
          idGroup: i.kpiItemRmLevelId,
          name: i.kpiItemName,
          treeStatus: 'disabled',
          parentId: i.categoryCode,
          frequency: frequencyNames.join(', '),
          rate: i.rate.toString().replace('.', ','),
          sourceInput: sourceInputName,
          mappingTargets: this.listColumnValue.map((col) => {
            const data = i.targets?.find(
              (target) => col.month === target.standardMonth && formValue.year == target.standardYear
            ); // == cause different type
            return {
              ...col,
              ...data,
              standardValue: data?.standardValue
                ? data?.standardValue.toString().replace('.', ',')
                : data?.standardValue,
              assignValue: data?.assignValue?.toString().replace('.', ','),
            };
          }),
        });

        this.formCreate.push(
          this.fb.group({
            value: [0, [CustomValidators.required, CustomValidators.assignKpi]],
            // extra data
            kpiStandardId: i.kpiStandardId,
            kpiItemRmLevelId: i.kpiItemRmLevelId,
            kpiItemId: i.kpiItemId,
            rmCode: this.dataFromView.rmCode,
            hrisCode: this.dataFromView.hrisCode,
          })
        );
      });
    });
    this.isLoading = false;
    this.yearAfterSearch = this.formSearch.value.year;
  };

  isBoolean = (value) => typeof value === 'boolean';

  skip() {
    this.codeSelected = '';
  }

  save() {
    if (this.formCreate.valid) {
      const data = [];
      for (let index = 0; index < this.formCreate.value.length; index++) {
        const element = this.formCreate.value[index];
        if (element.kpiItemRmLevelId) {
          const limitCeiling = this.listKpiItemMapping[index].limitCeiling;
          const limitFloor = this.listKpiItemMapping[index].limitFloor;
          const month = this.listColumnValue.find((item) => item.code === this.codeSelected)?.month;
          const itemMappingTarget = this.listKpiItemMapping[index].mappingTargets.find((item) => item.month === month);
          const value = typeof element.value === 'number' ? element.value : parseFloat(element.value.replace(',', '.'));
          const standardValue =
            typeof itemMappingTarget.standardValue === 'number'
              ? itemMappingTarget.standardValue
              : parseFloat(itemMappingTarget.standardValue.replace(',', '.'));
          if (itemMappingTarget?.kpiStandardId) {
            if (typeof limitFloor === 'number' && value < (standardValue * limitFloor) / 100) {
              this.messageService.error(`KPI phân giao phải >= ${(standardValue * limitFloor) / 100}`);
              return;
            }
            if (typeof limitCeiling === 'number' && value > (standardValue * limitCeiling) / 100) {
              this.messageService.error(`KPI phân giao phải <= ${(standardValue * limitCeiling) / 100}`);
              return;
            }
            data.push({
              ...element,
              value,
              month,
              year: this.yearAfterSearch,
              kpiStandardId: itemMappingTarget?.kpiStandardId,
              entrustId: itemMappingTarget?.entrustId,
            });
          }
        }
      }

      if (!data.length) {
        this.messageService.error(this.notificationMessage.not_have_assign);
        return;
      }

      this.confirmService.confirm().then((res) => {
        if (res) {
          this.isLoading = true;
          this.kpiAssignmentApi.saveKpiAssign(data).subscribe(
            () => {
              this.isLoading = false;
              this.skip();
              this.onSearchKpiAssign();
              this.messageService.success(this.notificationMessage.success);
            },
            (e) => {
              this.isLoading = false;
              if (e?.status === 0) {
                this.messageService.error(this.notificationMessage.E001);
                return;
              }
              this.messageService.error(e?.error?.description);
            }
          );
        }
      });
    } else {
      this.isPressSave = true;
      this.formCreate.controls.forEach((element: FormGroup) => {
        validateAllFormFields(element);
      });
    }
  }
}
