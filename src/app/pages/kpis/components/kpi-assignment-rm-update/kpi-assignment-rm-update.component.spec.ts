import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {KpiAssignmentRmUpdateComponent} from './kpi-assignment-rm-update.component';

describe('KpiAssignmentRmUpdateComponent', () => {
	let component: KpiAssignmentRmUpdateComponent;
	let fixture: ComponentFixture<KpiAssignmentRmUpdateComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [KpiAssignmentRmUpdateComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(KpiAssignmentRmUpdateComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
