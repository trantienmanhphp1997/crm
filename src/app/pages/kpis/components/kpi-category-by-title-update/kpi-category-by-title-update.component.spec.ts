import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiCategoryByTitleUpdateComponent } from './kpi-category-by-title-update.component';

describe('KpiCategoryByTitleUpdateComponent', () => {
  let component: KpiCategoryByTitleUpdateComponent;
  let fixture: ComponentFixture<KpiCategoryByTitleUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiCategoryByTitleUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiCategoryByTitleUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
