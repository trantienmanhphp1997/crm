import { forkJoin, of } from 'rxjs';
import { Component, OnInit, Injector, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { catchError } from 'rxjs/operators';
import { CategoryService } from 'src/app/pages/system/services/category.service';
import * as _ from 'lodash';
import {
	CLIENT_DATE_FORMAT,
	LIST_KPI_ASSESSMENT,
	LIST_KPI_FEATURE_NATURE,
	SERVER_DATE_FORMAT,
	SOURCE_INPUT,
	UN_TOUCH_KPI_ITEM_NUMBER,
} from '../../constant';
import { KpiCategoryApi, KpiCategoryByTitleApi, LevelApi, TitleGroupApi } from '../../apis';
import * as moment from 'moment';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CustomValidators } from 'src/app/core/utils/custom-validations';

@Component({
	selector: 'app-kpi-category-by-title-update',
	templateUrl: './kpi-category-by-title-update.component.html',
	styleUrls: ['./kpi-category-by-title-update.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class KpiCategoryByTitleUpdateComponent extends BaseComponent implements OnInit {
	formSearch = this.fb.group({
		rmTitleCode: { value: '', disabled: true },
		rmLevelCode: { value: '', disabled: true },
		blockCode: { value: '', disabled: true },
	});
	formCreate = this.fb.array([]);
	listKpiAssessment = LIST_KPI_ASSESSMENT;
	kpiFeatures = LIST_KPI_FEATURE_NATURE;
	listSourceInput = SOURCE_INPUT;
	isLoading = false;
	listBlock = [];
	listGroup = [];
	listLevel = [];
	listKpiItem = [];
	branchesCode: string[];
	minDate = new Date();
	allKpiItem = [];
	listOldKpiByTitle = [];
	editing = {};
	indexEditing: any;
	selectedItem: any;
	// passed data
	listKpiByTitle = [];
	params: any;

	formRow = this.fb.group({
		efficientDate: ['', CustomValidators.required],
		expireDate: '',
		isActive: true,
		rate: ['', [CustomValidators.required, CustomValidators.rate]],
		limitFloor: ['', CustomValidators.valueByTitle],
		limitCeiling: ['', CustomValidators.valueByTitle],
		ceilingRatio: ['', CustomValidators.valueByTitle],
		isTimeOrgFactor: false,
		isAreaFactor: false,
		isKpi: true,
		sourceInput: ['', CustomValidators.required],
		isShare: true,
		// extra data
		minExpireDate: '',
		maxEfficientDate: '',
	});

	constructor(
		injector: Injector,
		private categoryService: CategoryService,
		private kpiCategoryByTitleApi: KpiCategoryByTitleApi,
		private kpiCategoryApi: KpiCategoryApi,
		private modalActive: NgbActiveModal,
		private titleGroupApi: TitleGroupApi,
		private rmLevelApi: LevelApi
	) {
		super(injector);
	}

	ngOnInit(): void {
		this.isLoading = true;
		forkJoin([
			this.categoryService.getBlocksCategoryByRm().pipe(catchError((e) => of(undefined))),
			this.kpiCategoryApi.getItemResource().pipe(catchError((e) => of(undefined))),
			this.kpiCategoryApi
				.search({ pageSize: UN_TOUCH_KPI_ITEM_NUMBER, pageNumber: 0 })
				.pipe(catchError((e) => of(undefined))),
		]).subscribe(async ([itemResources, allKpiItem]) => {
			const data = await this.getGroupAndLevelBlockCode(this.params.blockCode);
			this.listGroup = this.mapListGroup(data.listGroup);
			this.listLevel = data.listLevel;

			this.prop = {
				itemResources,
				allKpiItem,
			};
			this.mapData();
			this.mapFormControl();
		});
	}

	closeModal() {
		this.modalActive.close(false);
	}

	mapFormControl = () => {
		this.formRow.controls.efficientDate.valueChanges.subscribe((value) => {
			this.formRow.controls.minExpireDate.setValue(new Date(moment(value).valueOf()));
		});
		this.formRow.controls.expireDate.valueChanges.subscribe((value) => {
			this.formRow.controls.maxEfficientDate.setValue(new Date(moment(value).valueOf()));
		});
	};

	getGroupAndLevelBlockCode = async (code) => {
		const [listGroup, listLevel] = await Promise.all([
			this.titleGroupApi
				.searchGroupByBlockCode(code)
				.pipe(catchError((e) => of(undefined)))
				.toPromise(),
			this.rmLevelApi
				.searchLevelByGroupCode('', code)
				.pipe(catchError((e) => of(undefined)))
				.toPromise(),
		]);

		return { listGroup: listGroup?.content || [], listLevel: listLevel?.content || [] };
	};

	mapData() {
		this.allKpiItem = this.prop.allKpiItem?.content || [];
		// Set default value
		const objectData = {
			blockCode: this.params.blockCode,
			rmTitleCode: this.params.title,
			rmLevelCode: this.params.level,
		};
		this.formSearch.setValue({ ...objectData });
		// format listOldKpiByTitle
		this.listOldKpiByTitle = this.listKpiByTitle.map((item) => ({
			...item,
			efficientDate: moment(item?.efficientDate, SERVER_DATE_FORMAT).format(CLIENT_DATE_FORMAT),
			expireDate: item?.expireDate ? moment(item.expireDate, SERVER_DATE_FORMAT).format(CLIENT_DATE_FORMAT) : '',
			rate: item.rate?.toString()?.replace('.', ','),
			limitFloor: item.limitFloor ? item.limitFloor.toString().replace('.', ',') : '',
			limitCeiling: item.limitCeiling ? item.limitCeiling.toString().replace('.', ',') : '',
			ceilingRatio: item.ceilingRatio ? item.ceilingRatio.toString().replace('.', ',') : '',
			}));
		this.isLoading = false;
	}

	mapListGroup = (listGroup) => {
		return (
			listGroup?.map((item) => {
				return { ...item, name: item.blockCode + ' - ' + item.name };
			}) || []
		);
	};

	onChangeEndDate = (event) => {
		const endOfMonth = new Date(moment(event).endOf('month').valueOf());
		this.formRow.controls?.expireDate?.setValue(endOfMonth);
	};

	update(item, index) {
		// console.log('item', item);
		this.editing = { [index]: true };
		this.selectedItem = item;
		this.indexEditing = index;

		const efficientDate = new Date(moment(item.efficientDate, CLIENT_DATE_FORMAT).valueOf());
		const expireDate = item?.expireDate ? new Date(moment(item.expireDate, CLIENT_DATE_FORMAT).valueOf()) : '';
		const rate = item.rate.toString().replace('.', ',');

		this.formRow.setValue({
      efficientDate,
      expireDate,
      isActive: item.isActive,
      rate,
      limitFloor: item.limitFloor ? item.limitFloor.toString().replace('.', ',') : '',
      limitCeiling: item.limitCeiling ? item.limitCeiling.toString().replace('.', ',') : '',
      ceilingRatio: item.ceilingRatio ? item.ceilingRatio.toString().replace('.', ',') : '',
      isTimeOrgFactor: item.isTimeOrgFactor,
      isAreaFactor: item.isAreaFactor,
      isKpi: item.isKpi,
      sourceInput: item.sourceInput,
      isShare: item.isShare,
      // extra data
      minExpireDate: efficientDate,
      maxEfficientDate: expireDate,
    });
	}

	delete(item, index) {
		// console.log('item', item);
		const params = { kpiItemRmLevelId: item.kpiRmLevelId };
		this.confirmService
			.confirm()
			.then((res) => {
				if (res) {
					this.isLoading = true;
					this.kpiCategoryByTitleApi.deleteOneHistoryProperty(params).subscribe(
						() => {
							this.isLoading = false;
							this.listOldKpiByTitle.splice(index, 1);
							this.messageService.success(this.notificationMessage.success);
						},
						(e) => {
							if (e?.error) {
								this.messageService.warn(e?.error?.description);
							} else {
								this.messageService.error(this.notificationMessage.error);
							}
							this.isLoading = false;
						}
					);
				}
			})
			.catch(() => {});
	}

	getSourceInputName = (code) => SOURCE_INPUT.find((source) => source.code == code)?.name || '';

	getErrRequire(fieldName) {
		return this.formRow?.controls?.[fieldName]?.errors?.cusRequired;
	}

	getErrValidate(fieldName) {
		if (fieldName === 'limitFloor') {
			return (
				this.formRow?.controls?.[fieldName]?.errors?.value ||
				this.formRow?.controls?.[fieldName]?.errors?.maxLimitFloor
			);
		} else if (fieldName === 'limitCeiling') {
			return (
				this.formRow?.controls?.[fieldName]?.errors?.value ||
				this.formRow?.controls?.[fieldName]?.errors?.minLimitCeiling
			);
		}
		return this.formRow?.controls?.[fieldName]?.errors?.value;
	}

	save() {
		console.log(this.formRow);

		const dataFormRow = this.formRow.value;
		const item = this.selectedItem;
		const index = this.indexEditing;

		if (this.formRow.valid) {
			const data = {
        id: item.kpiRmLevelId,
        rate: parseFloat(dataFormRow.rate.replace(',', '.')),
        limitFloor: dataFormRow.limitFloor
          ? parseFloat(dataFormRow.limitFloor.replace(',', '.'))
          : dataFormRow.limitFloor,
        limitCeiling: dataFormRow.limitCeiling
          ? parseFloat(dataFormRow.limitCeiling.replace(',', '.'))
          : dataFormRow.limitCeiling,
        ceilingRatio: dataFormRow.ceilingRatio
          ? parseFloat(dataFormRow.ceilingRatio.replace(',', '.'))
          : dataFormRow.ceilingRatio,
        sourceInput: dataFormRow.sourceInput,
        expireDate: dataFormRow.expireDate ? moment(dataFormRow.expireDate).format(SERVER_DATE_FORMAT) : '',
        efficientDate: moment(dataFormRow.efficientDate).format(SERVER_DATE_FORMAT),
        isActive: dataFormRow.isActive,
        isAreaFactor: dataFormRow.isAreaFactor,
        isKpi: dataFormRow.isKpi,
        isShare: dataFormRow.isShare,
        isTimeOrgFactor: dataFormRow.isTimeOrgFactor,
      };
			this.confirmService.confirm().then((res) => {
				if (res) {
					this.isLoading = true;
					this.kpiCategoryByTitleApi.updateKpiItemRmLevel(data).subscribe(
						() => {
							this.isLoading = false;
							this.editing = {};
							this.indexEditing = null;
							this.listOldKpiByTitle[index] = {
								...this.listOldKpiByTitle[index],
								...dataFormRow,
								efficientDate: moment(dataFormRow.efficientDate).format(CLIENT_DATE_FORMAT),
								expireDate: dataFormRow.expireDate
									? moment(dataFormRow.expireDate).format(CLIENT_DATE_FORMAT)
									: '',
							};
							// For re-render
							this.listOldKpiByTitle = [...this.listOldKpiByTitle];

							this.messageService.success(this.notificationMessage.success);
						},
						(e) => {
							this.isLoading = false;
							if (e?.status === 0) {
								this.messageService.error(this.notificationMessage.E001);
								return;
							}
							this.messageService.error(e?.error?.description);
						}
					);
				}
			});
		} else {
			validateAllFormFields(this.formRow);
		}
	}
}
