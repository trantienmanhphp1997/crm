import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import * as _ from 'lodash';
import { global } from '@angular/compiler/src/util';
import { FunctionCode, Scopes, SessionKey } from 'src/app/core/utils/common-constants';
import { StandardKpiCbqlApi } from '../../apis/standard-kpi-cbql.api';
import { formatDate, formatNumber } from '@angular/common';
import { AppFunction } from 'src/app/core/interfaces/app-function.interface';
import { CategoryService } from 'src/app/pages/system/services/category.service';

@Component({
  selector: 'standard-kpi-cbql',
  templateUrl: './standard-kpi-cbql.component.html',
  styleUrls: ['./standard-kpi-cbql.component.scss'],
})
export class StandardKpiCBQLComponent extends BaseComponent implements OnInit {
  constructor(
    injector: Injector,
    private standardKpiCbqlApi: StandardKpiCbqlApi,
    private categoryService: CategoryService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.KPI_STANDARD_CBQL_BRANCH}`);
    this.objFunctionRm = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
  }
  limit = global.userConfig.pageSize;
  objFunctionRm: AppFunction;
  formSearch = this.fb.group({
    divisionCode: [''],
    month: new Date(),
    branchesCode: [''],
  });
  pageable: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: _.get(global, 'userConfig.pageSize', 10),
  };
  listDivision;
  listBranchTerm = [];
  listBranch = [];
  listData = [];
  selected = [];
  isEdit = false;
  params = {
    branchCodeList: [''],
    month: '',
    year: '',
    blockCode: '',
    pageNumber: 0,
    pageSize: global?.userConfig?.pageSize,
  };
  childHeader = [];
  parentHeader = [];
  dataRows = [];
  hiddenTable = false;
  dataTable: any;
  dataRowsOld = [];
  listBranches = [];

  ngOnInit() {
    this.isLoading = true;
    const divisionOfUser: any[] = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
    this.listDivision =
      divisionOfUser?.map((item) => {
        return { code: item.code, displayName: `${item.code || ''} - ${item.name || ''}` };
      }) || [];
    this.formSearch.controls.divisionCode.setValue(_.first(this.listDivision)?.code);
    this.categoryService.getBranchesOfUser(this.objFunctionRm.rsId, Scopes.VIEW).subscribe((res) => {
      if (res) {
        this.listBranches = res.map((item) => {
          return { code: item.code, displayName: item.code + ' - ' + item.name };
        });
        this.listBranches.unshift({ code: '', displayName: this.fields.all });
        this.search(true);
      }
    });
  }

  search(isSearch: boolean) {
    this.isLoading = true;
    this.hiddenTable = false;
    if (isSearch) {
      this.params.pageNumber = 0;
    }
    let dateSearch = formatDate(this.formSearch.get('month').value, 'MM/yyyy', 'en')?.split('/');
    this.params.month = dateSearch[0];
    this.params.year = dateSearch[1];
    this.params.blockCode = this.formSearch.controls.divisionCode.value;
    this.params.month = dateSearch[0];
    let branchCodeList = [];
    branchCodeList.push(this.formSearch.controls.branchesCode.value);
    this.params.branchCodeList = _.isEmpty(this.formSearch.controls.branchesCode.value) ? [] : branchCodeList;
    this.standardKpiCbqlApi.searchStandardKpiCBQL(this.params).subscribe(
      (res) => {
        if (res) {
          this.isLoading = false;
          this.listBranch = [...res.content];
          this.pageable = {
            totalElements: res.totalElements,
            totalPages: res.totalPages,
            currentPage: res.number,
            size: this.limit,
          };
        } else {
          this.messageService.error(this.notificationMessage.error);
          this.isLoading = false;
        }
      },
      (error) => {
        this.messageService.error(this.notificationMessage.E001);
        this.isLoading = false;
      }
    );
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.params.pageNumber = pageInfo.offset;
    this.search(false);
  }

  onSelect(branch) {
    this.isEdit = false;
    this.hiddenTable = true;
    this.dataTable = this.listBranch.filter((item) => item.branchCode === branch.selected[0].branchCode)[0];
    this.childHeader = this.dataTable.childHeader.cells;
    this.parentHeader = this.dataTable.parentHeader.cells;
    this.dataRows = [...this.dataTable.rows.map((item) => item.cells)];
    _.forEach(this.dataRows, (item) => {
      _.forEach(item, (cel) => {
        cel.showInput = cel?.value ? true : false;
      });
    });
    this.dataRowsOld = [...this.dataTable.rows.map((item) => item.cells)];
  }

  save() {
    const reg = new RegExp(/[^\d.-]/g);
    for (let index = 0; index < this.dataRows.length; index++) {
      let showInputRow = this.dataRows[index].filter((item) => !item.showInput && item.isRate).length;
      let dataValidate = this.dataRows[index]?.filter((i) => i.isRate && i.value && reg.test(i.value));
      if (dataValidate.length > 0) {
        this.messageService.error('Nhập sai định dạng');
        return;
      }
      let totalKpi = formatNumber(
        _.reduce(
          this.dataRows[index]?.filter((i) => i.isRate && i.value).map((item) => +item.value),
          (a, c) => {
            return a + c;
          }
        ) || 0,
        'en',
        '0.0-2'
      )?.replace(',', '');
      if (showInputRow === this.parentHeader.length - 2) {
        totalKpi = '100';
      }
      if (+totalKpi > 100.1 || +totalKpi < 99.9) {
        this.messageService.error('Tổng tỷ phải nằm trong khoảng 99.9% - 100.1%');
        return;
      }
    }

    this.isLoading = true;
    this.standardKpiCbqlApi.saveStandardKpiCBQL(this.dataTable).subscribe(
      (res) => {
        if (res) {
          this.isEdit = false;
          this.isLoading = false;
          this.messageService.success(this.notificationMessage.success);
        } else {
          this.messageService.error(this.notificationMessage.error);
          this.isLoading = false;
        }
      },
      (error) => {
        this.messageService.error(this.notificationMessage.E001);
        this.isLoading = false;
      }
    );
  }

  showEdit() {
    this.isEdit = true;
  }

  import() {
    this.router.navigateByUrl(`/kpis/kpi-management/kpi-config/standard-kpi-cbql/import`);
  }

  editProportion(dataCol, dataRow) {
    const reg = new RegExp(/[^\d.-]/g);
    if (reg.test(dataCol.value)) {
      this.messageService.error('Nhập sai định dạng');
      return;
    }
    if (+dataCol.value > 100) {
      this.messageService.error('Tỷ trọng phải nhỏ hơn 100');
      return;
    } else {
      dataRow[dataCol.column - 1].value = (dataRow[1].value * dataCol.value) / 100;
      dataRow[dataCol.column - 1].isChanged = true;
    }
  }
}
