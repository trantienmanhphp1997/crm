import {AfterViewInit, Component, Injector, OnInit, ViewEncapsulation} from '@angular/core';
import {BaseComponent} from 'src/app/core/components/base.component';
import {StandardKpiCbqlApi} from '../../apis/standard-kpi-cbql.api';
import {forkJoin, of} from 'rxjs';
import {
  BRANCH_HO,
  CommonCategory,
  ConfirmType,
  Division,
  FunctionCode,
  Scopes, SessionKey
} from '../../../../core/utils/common-constants';
import {CategoryService} from '../../../system/services/category.service';
import {KpiRmApi} from '../../apis';
import {catchError} from 'rxjs/operators';
import * as _ from 'lodash';
import {CustomValidators} from '../../../../core/utils/custom-validations';
import {RmApi} from '../../../rm/apis';
import {AppFunction} from '../../../../core/interfaces/app-function.interface';
import {Pageable} from '../../../../core/interfaces/pageable.interface';
import {global} from '@angular/compiler/src/util';
import {formatDate} from '@angular/common';
import {Utils} from '../../../../core/utils/utils';
import {ConfirmDialogComponent} from '../../../../shared/components';



@Component({
  selector: 'app-target-sale-config-list',
  templateUrl: './target-sale-config-list.component.html',
  styleUrls: ['./target-sale-config-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TargetSaleConfigListComponent extends BaseComponent implements OnInit, AfterViewInit {
  commonData = {
    listDivision: [{name: 'INDIV - Khách hàng cá nhân', code: 'INDIV'}],
    listTitleGroup: [],
    listLevelRM: [],
    listAllotmentValue: [{name: 'Tháng', code: 'MONTH'}, {name: 'Tuần', code: 'WEEK'}, {name: 'Tất cả', code: 'ALL'}],
    listProduct: [],
    listBranch: []
  };

  formSearch = this.fb.group({
    division: [{value: 'INDIV', disabled: true}],
    rmLevelId: null,
    rmTitleId: '',
    startDate: ['', CustomValidators.required],
    endDate: ['', CustomValidators.required],
    value: ['', CustomValidators.required],
    branchCodeList: [[], CustomValidators.required],
  });

  paramSearch = {
    pageSize: global.userConfig.pageSize,
    pageNumber: 0,
    rsId: '',
    scope: Scopes.VIEW,
  };

  public objFunctionTagetSale: AppFunction;
  pageable: Pageable;
  listData = [];
  limit = global.userConfig.pageSize;
  isHO = false;
  divisionOfUser: any[];

  constructor(
    injector: Injector,
    private standardKpiCbqlApi: StandardKpiCbqlApi,
    private categoryService: CategoryService,
    private api: KpiRmApi,
    private rmApi: RmApi,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.RM_MANAGER}`);
    this.objFunctionTagetSale = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.TARGET_SALE_CONFIG}`);
  }

  ngOnInit() {
    this.divisionOfUser = this.sessionService.getSessionData(SessionKey.DIVISION_OF_RM) || [];
    this.isHO = this.currUser?.branch === BRANCH_HO;
    if (!this.isUserInDiv() && !this.isHO) {
      this.commonData.listDivision = [];
      this.listData = [];
      return;
    }
    this.isLoading = true;
    this.prop = this.sessionService.getSessionData(FunctionCode.TARGET_SALE_CONFIG);
    if (!this.prop) {
      forkJoin([
        this.api.getCategoryRmByBlock({
          viewRm: true,
          blockCode: 'INDIV',
          rsId: this.objFunction?.rsId,
          scope: Scopes.VIEW,
          past: 1,
        }),
        this.categoryService
          .getBranchesOfUser(this.objFunctionTagetSale?.rsId, Scopes.VIEW)
          .pipe(catchError((e) => of(undefined))),
        this.commonService.getCommonCategory(CommonCategory.TARGET_SALE_CONFIG_PRODUCT).pipe(catchError(() => of(undefined))),
      ]).subscribe(([listTitleGroupByDivision, branchesOfUser, targetSaleProductConfig]) => {
        this.commonData.listProduct = _.get(targetSaleProductConfig, 'content')

        this.commonData.listBranch = branchesOfUser?.map((item) => {
          return {code: item.code, displayName: item.code + ' - ' + item.name};
        });
        this.formSearch.controls.branchCodeList.setValue(branchesOfUser.map(item => item.code));
        listTitleGroupByDivision?.forEach((item) => {
          item.name = `${item.blockCode} - ${item.name}`;
        });
        this.commonData.listTitleGroup = _.sortBy(listTitleGroupByDivision, 'id') || [];
        this.formSearch.controls.rmTitleId.setValue(this.commonData.listTitleGroup[0]?.id);
        this.prop = {
          commonData: this.commonData,
        }
        this.sessionService.setSessionData(FunctionCode.TARGET_SALE_CONFIG, this.prop);
        this.search(true);
      });
    } else {
      this.commonData = _.get(this.prop, 'commonData');
      this.formSearch.patchValue(_.get(this.prop, 'form'));
      this.paramSearch = _.get(this.prop, 'paramSearch');
      this.search(false);
    }
  }

  ngAfterViewInit() {
    this.formSearch.controls.rmTitleId.valueChanges.subscribe((id) => {
      if (id) {
        const itemTitleGroup = _.find(this.commonData.listTitleGroup, (item) => item.id === id);
        this.commonData.listLevelRM = itemTitleGroup?.levels;
        if (itemTitleGroup?.levels.length > 1 && !_.find(itemTitleGroup?.levels, (item) => item.id === null)) {
          this.commonData.listLevelRM.unshift({id: null, name: this.fields.all});
        }
        this.formSearch.controls.rmLevelId.setValue(_.head(this.commonData.listLevelRM).id);
      }
    });
  }

  onActive(event) {
    if (event.type === 'dblclick') {
      event.cellElement.blur();
      const item = _.get(event, 'row');
      this.router.navigate([this.router.url, 'detail', item.id]);
    }
  }

  setPage(pageInfo) {
    if (this.isLoading) {
      return;
    }
    this.paramSearch.pageNumber = pageInfo.offset;
    this.search(false);
  }

  update(value) {
    this.router.navigate([this.router.url, 'update', value.id]);
  }

  delete(row) {

    const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
    confirm.result
      .then((res) => {
        if (res) {
          this.isLoading = true;
          this.rmApi.deleteTargetSaleConfig(_.get(row, 'id')).subscribe(
            () => {
              this.messageService.success(this.notificationMessage.success);
              this.isLoading = false;
              this.search(true);
            },
            (e) => {
              this.messageService.error(this.notificationMessage.error);
              this.isLoading = false;
            }
          );
        }
      })
      .catch(() => {});
  }

  search(isSearch?: boolean) {
    if (!this.isUserInDiv() && !this.isHO) {
      this.commonData.listDivision = [];
      this.listData = [];
      return;
    }
    if (isSearch) {
      this.paramSearch.pageNumber = 0;
    }
    this.isLoading = true;
    const data = {...this.formSearch.getRawValue(),...this.paramSearch};
    data.startDate = data.startDate ? formatDate(data.startDate, 'dd/MM/yyyy', 'en') : null;
    data.endDate = data.endDate ? formatDate(data.endDate, 'dd/MM/yyyy', 'en') : null;
    data.rsId = this.objFunctionTagetSale.rsId;
    this.prop.paramSearch = this.paramSearch;
    this.prop.form = this.formSearch.getRawValue();
    this.sessionService.setSessionData(FunctionCode.TARGET_SALE_CONFIG, this.prop);
    this.rmApi.getAllTargetSaleConfig(data).subscribe(result => {
      this.listData = _.get(result, 'content');
      this.pageable = {
        totalElements: result.totalElements,
        totalPages: result.totalPages,
        currentPage: this.paramSearch.pageNumber,
        size: this.paramSearch.pageSize,
      };
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
      this.messageService.error(this.notificationMessage.error);
    });
  }

  create() {
    this.router.navigate([this.router.url, 'create']);
  }

  convertValue(value) {
    return Utils.numberWithCommas(value);
  }

  handleChangePageSize(event) {
    this.paramSearch.pageNumber = 0;
    this.paramSearch.pageSize = event;
    this.limit = event;
    this.search(true);
  }

  isUserInDiv() {
    return _.find(this.divisionOfUser, i => i.code === 'INDIV');
  }
}
