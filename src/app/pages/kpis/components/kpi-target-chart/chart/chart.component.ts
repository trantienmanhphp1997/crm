import { ChangeDetectorRef, Component, HostListener, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { EChartsOption } from 'echarts';

@Component({
  selector: 'app-target-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class TargetChartComponent implements OnInit, OnChanges {

  @Input() chartData: any;
  @Input() selectedChartType: string;

  options = {
    grid: {
      containLabel: false
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'cross',
        crossStyle: {
          color: '#999'
        }
      },
      formatter: (data) => {
        const [bar, line] = data;
        const { list, unitName } = this.chartData;
        let lineValue = this.getThousandSeparator(list[line.dataIndex].lineValue);
        let lineName = 'Kết quả';
        let lineUnit = unitName;
        if (this.selectedChartType === 'point') {
          lineValue = list[line.dataIndex].linePoint;
          lineName = '%HTKH';
          lineUnit = '%'
        }

        return (
          list[bar.dataIndex].barName +
          '<br/>' +
          "<ul>" +
          "<li class='a'>" + 'Phân giao: ' + list[bar.dataIndex].barValue + ' ' + unitName + "</li>" +
          "<li>" + lineName + ': ' + lineValue + ' ' + lineUnit + "</li>" +
          "</ul>"
        )
      }
    },
    legend: {
      data: ['Phân giao', '%HTKH'],
      icon: 'circle',
      itemWidth: 9,
      itemGap: 0,
      top: 15,
      left: 120
    },
    xAxis: [
      {
        type: 'category',
        data: [],
        axisPointer: {
          type: 'shadow'
        },
        axisTick: {
          show: false,
        }
      }
    ],
    yAxis: [
      {
        type: 'value',
        name: '',
        min: 0,
        max: 0,
        axisLabel: {
          formatter: this.getKFormatter
        },
        nameTextStyle: {
          align: 'left',
          verticalAlign: 'top',
          padding: [-25, 0, 0, -30],
        },
        axisLine: {show: true}
      },
      {
        type: 'value',
        name: '',
        min: 0,
        max: 0,
        axisLabel: {
          formatter: ''
        }
      }
    ],
    series: [
      {
        name: 'Phân giao',
        type: 'bar',
        data: [],
        barWidth: 40
      },
      {
        name: '%HTKH',
        type: 'line',
        yAxisIndex: 1,
        data: [],
        itemStyle: {
          color: '#002EDB'
        },
        label: {
          show: true,
          position: 'top',
          textBorderColor: '#ffffff',
          textBorderWidth: 0,
          color: '#002EDB',
          formatter: (data) => {
            switch (this.selectedChartType) {
              case 'point':
                return this.chartData.list[data.dataIndex].linePoint + '%';
              default:
                return this.getThousandSeparator(data.value);
            }
          }
        },
      }
    ]
  };

  lineName = {
    'point': '%HTKH',
    'value': 'Kết quả'
  }

  colorList = ['#1FB266', '#FF1A12', '#FFD300', '#B4A5FA', '#62B3FF', '#FAA21B', '#A0D2FF', '#9BE6C8'];
  currentColorBar = '';

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.setSmallDesktopOptions();
  }

  constructor(
    private cdk: ChangeDetectorRef
  ) {
    this.setSmallDesktopOptions();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.selectedChartType && changes.selectedChartType.currentValue) {
      const name = this.lineName[changes.selectedChartType.currentValue];
      this.options.legend.data[1] = name;
      this.options.series[1].name = name;
      this.options = { ...this.options };
    }
    this.cdk.detectChanges();
  }

  ngOnInit(): void {
    const { list, unitName, maxValue, colorIndex, minValue } = this.chartData;

    this.currentColorBar = this.colorList[colorIndex];
    this.options.series[0]['itemStyle'] = { color: this.currentColorBar };
    //
    this.options.xAxis[0].data = list.map(data => { return data.barName });
    //
    this.options.series[0].data = list.map(data => { return data.barValue });
    this.options.series[1].data = list.map(data => { return data.lineValue });
    //
    this.options.yAxis[0].name = unitName;
  
    this.options.yAxis[0].max = maxValue;
    this.options.yAxis[1].max = maxValue;
  
    this.options.yAxis[0].min = minValue;
    this.options.yAxis[1].min = minValue;

    this.options = { ...this.options };
    this.cdk.detectChanges();
  }

  ngAfterViewChecked(): void {
    this.cdk.detectChanges();
  }

  getKFormatter(num: number): any {
    return Math.abs(num) > 999 ? Math.sign(num) * Number((Math.abs(num) / 1000).toFixed(1)) + 'k' : Math.sign(num) * Math.abs(num)
  }

  getThousandSeparator(num: number): string {
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  setSmallDesktopOptions(): void {
    let bar = this.options.series[0];
    let grid = this.options.grid;
    let legend = this.options.legend;
    if (window.innerWidth < 1500) {
      bar.barWidth = 20;
      grid.containLabel = true;
      legend.left = 115;
      legend.itemGap = 6;
    } else {
      bar.barWidth = 30;
      grid.containLabel = false;
      legend.left = 120;
      legend.itemGap = 10;
    }
    this.options = { ...this.options };
  }
}
