import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiTargetChartComponent } from './kpi-target-chart.component';

describe('KpiTargetChartComponent', () => {
  let component: KpiTargetChartComponent;
  let fixture: ComponentFixture<KpiTargetChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiTargetChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiTargetChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
