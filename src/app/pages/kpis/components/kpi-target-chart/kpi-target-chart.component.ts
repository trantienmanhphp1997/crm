import { AfterViewChecked, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-kpi-target-chart',
  templateUrl: './kpi-target-chart.component.html',
  styleUrls: ['./kpi-target-chart.component.scss']
})
export class KpiTargetChartComponent implements OnInit, AfterViewChecked {

  @Input() data: any;

  chartOptions: any;

  isLoading = false;

  title = "kpi.title.kpi_targets";

  chartType = ['point', 'value'];

  selectedChartType = this.chartType[1];

  constructor(
    private cdk: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.initData();
  }


  initData(): void {

  }

  ngAfterViewChecked(): void {
    this.cdk.detectChanges();
  }

  setChartType(type: string): void {
    this.selectedChartType = type;
  }

  get isEmpty(): boolean {
    return this.data?.chartList.length === 0;
  }
}
