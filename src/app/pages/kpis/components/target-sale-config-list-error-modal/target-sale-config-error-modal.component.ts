import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, Injector, HostBinding, Input, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { Pageable } from 'src/app/core/interfaces/pageable.interface';
import { global } from '@angular/compiler/src/util';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-list-import-error',
  templateUrl: './target-sale-config-error-modal.component.html',
  styleUrls: ['./target-sale-config-error-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TargetSaleConfigErrorModalComponent extends BaseComponent implements OnInit {
  @HostBinding('class.list__error-content') listContent = true;
  @Input() dataError: any;
  listDataError = [];

  pageError: Pageable = {
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    size: global?.userConfig?.pageSize
  };

  constructor(injector: Injector, private modalActive: NgbActiveModal) {
    super(injector);
  }

  ngOnInit(): void {}

  setPage(pageInfo) {
    this.pageError.currentPage = pageInfo?.offset;
    const total = this.dataError?.length || 0;
    const start = this.pageError.currentPage * this.pageError.size;
    this.listDataError = this.dataError?.slice(start, start + this.pageError.size);
    this.pageError.totalElements = total;
    this.pageError.totalPages = Math.floor(total / this.pageError.size);
  }

  closeModal() {
    this.modalActive.close();
  }
}
