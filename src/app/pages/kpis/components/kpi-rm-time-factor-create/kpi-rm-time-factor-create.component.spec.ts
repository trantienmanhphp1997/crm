import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiRmTimeFactorCreateComponent } from './kpi-rm-time-factor-create.component';

describe('KpiRmTimeFactorCreateComponent', () => {
  let component: KpiRmTimeFactorCreateComponent;
  let fixture: ComponentFixture<KpiRmTimeFactorCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiRmTimeFactorCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiRmTimeFactorCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
