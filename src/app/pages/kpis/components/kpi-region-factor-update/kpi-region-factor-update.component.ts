import { forkJoin, of } from 'rxjs';
import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { CampaignsService } from 'src/app/pages/campaigns/services/campaigns.service';
import { CommonCategory, FunctionCode } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import * as moment from 'moment';
import { FormArray, FormGroup } from '@angular/forms';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { KpiRegionApi } from '../../apis/kpi-region.api';
import { SERVER_DATE_FORMAT } from '../../constant';

@Component({
  selector: 'app-kpi-region-factor-update',
  templateUrl: './kpi-region-factor-update.component.html',
  styleUrls: ['./kpi-region-factor-update.component.scss'],
})
export class KpiRegionFactorUpdateComponent extends BaseComponent implements OnInit {
  blockCode: String;
  isValidator = false;
  listData = [];
  listBranchTypeKPI = [];
  isLoading = false;
  listBlock = [];
  formCreate = this.fb.group({
    kpiFactorSetName: ['', CustomValidators.required],
    blockCode: ['', CustomValidators.required],
    efficientDate: ['', CustomValidators.required],
    expireDate: '',
  });

  formCell = this.fb.group({
    value: ['', [CustomValidators.required, CustomValidators.valueFactor]],
  });

  minEfficientDate: any;
  maxEfficientDate: any;
  minExpireDate: any;
  isCreate = true;
  listRegion = [];
  editing = {};
  listRegionFactor = [];
  listReshapeRegionFactor = [];
  valueCell: any;
  kpiFactorDtoId: any;
  indexEditing: any;

  constructor(injector: Injector, private campaignService: CampaignsService, private kpiRegionApi: KpiRegionApi) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.KPI_FACTOR_AREA}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    const kpiFactorDtoId = this.route.snapshot.paramMap.get('id');
    this.minEfficientDate = new Date(moment().subtract(1, 'month').startOf('month').valueOf());
    this.minExpireDate = new Date(moment().subtract(1, 'month').startOf('month').valueOf());
    this.maxEfficientDate = '';
    forkJoin([
      // get list branch type kpi
      this.commonService.getCommonCategory(CommonCategory.KPI_SYS_ORG_TYPE).pipe(catchError((e) => of(undefined))),
      // get list block
      this.campaignService.getBlockMapping().pipe(catchError((e) => of(undefined))),
      // get list region
      this.commonService.getCommonCategory(CommonCategory.KPI_LOCATION).pipe(catchError((e) => of(undefined))),
      // get list kpi region factor by kpiFactorSetId
      this.kpiRegionApi.getKpiRegionFactors(kpiFactorDtoId).pipe(catchError((e) => of(undefined))),
      // get detail factor set
      this.kpiRegionApi.getDetailFactorSet(kpiFactorDtoId).pipe(catchError((e) => of(undefined))),
    ]).subscribe(([listBranchTypeKPI, listBlock, listRegion, listRegionFactor, detailFactorSet]) => {
      // console.log('listRegionFactor', detailFactorSet);
      this.isLoading = false;
      this.listRegion = listRegion?.content || [];
      this.listBranchTypeKPI = listBranchTypeKPI?.content || [];
      this.listRegionFactor = listRegionFactor || [];
      this.kpiFactorDtoId = kpiFactorDtoId;
      // map list block
      this.listBlock =
        listBlock?.content.map((item) => {
          return { code: item.code, name: item.code + ' - ' + item.name };
        }) || [];
      this.mapData();
      // Map old value
      this.formCreate.setValue({
        kpiFactorSetName: detailFactorSet.kpiFactorSetName || '',
        blockCode: detailFactorSet.blockCode || this.listBlock[0]?.code || '',
        efficientDate: detailFactorSet?.efficientDate
          ? new Date(moment(detailFactorSet.efficientDate, SERVER_DATE_FORMAT)?.startOf('day').valueOf())
          : '',
        expireDate: detailFactorSet?.expireDate
          ? new Date(moment(detailFactorSet.expireDate, SERVER_DATE_FORMAT)?.startOf('day').valueOf())
          : '',
      });
    });
  }

  getIndex(col, row) {
    return row * this.listRegion.length + col;
  }

  mapData() {
    const listReshapeRegionFactor = [];
    this.listBranchTypeKPI.forEach((branchTypeKPI) => {
      this.listRegion.forEach((region) => {
        // re-shape data
        const regionFactor = this.listRegionFactor.find(
          (item) => item.kpiSysOrgTypeCode === branchTypeKPI.code && item.locationCode === region.code
        );
        listReshapeRegionFactor.push({
          ...regionFactor,
          value: regionFactor?.value?.toString()?.replace('.', ','),
        });
      });
    });

    this.listReshapeRegionFactor = listReshapeRegionFactor;

    this.formCreate.controls.efficientDate.valueChanges.subscribe((value) => {
      this.minExpireDate = new Date(moment(value).startOf('day').valueOf());
    });
    this.formCreate.controls.expireDate.valueChanges.subscribe((value) => {
      this.maxEfficientDate = new Date(moment(value).startOf('day').valueOf());
    });
  }

  onChangeEndDate = (event) => {
    const endOfMonth = new Date(moment(event).endOf('month').valueOf());
    this.formCreate.controls?.expireDate?.setValue(endOfMonth);
  };

  getErrRequireValue(indexRegion, rowIndex) {
    const controlValue = (
      (this.formCreate.controls.kpiFactorDtos as FormArray).controls[this.getIndex(indexRegion, rowIndex)] as FormGroup
    ).controls;
    return controlValue?.value?.errors?.cusRequired && controlValue.value?.touched && this.isValidator;
  }

  getErrValidateValue(indexRegion, rowIndex) {
    const controlValue = (
      (this.formCreate.controls.kpiFactorDtos as FormArray).controls[this.getIndex(indexRegion, rowIndex)] as FormGroup
    ).controls;
    return controlValue?.value?.errors?.value;
  }

  createItem(branchType, region) {
    return this.fb.group({
      value: ['', [CustomValidators.required, CustomValidators.valueFactor]],
      locationCode: region.code,
      kpiSysOrgTypeCode: branchType.code,
    });
  }

  onDbClickCell(index) {
    this.editing[index] = true;
    this.valueCell = this.listReshapeRegionFactor[index];
    this.formCell.controls.value.setValue(this.valueCell?.value);
    this.indexEditing = index;
  }

  endEdit(event) {
    if (event?.target?.id === 'inputValue' || event?.target?.id === 'imgCheck') return;
    this.editing = {};
    this.indexEditing = null;
  }

  updateValue(event) {
    event.target.closest('datatable-body-cell').blur(); // Fix err ExpressionChangedAfterItHasBeenCheckedError
    if (this.formCell.valid) {
      const value = this.formCell.value?.value;
      const kpiFactorDtos = [
        {
          ...this.valueCell,
          value: parseFloat(value?.replace(',', '.')),
        },
      ];
      const data = {
        kpiFactorSetId: this.kpiFactorDtoId,
        kpiFactorDtos,
        type: 2,
      };
      // console.log('data', data);
      this.confirmService.confirm().then((res) => {
        if (res) {
          this.isLoading = true;
          this.kpiRegionApi.saveKpiFactorSet(data).subscribe(
            () => {
              this.isLoading = false;
              this.editing = {};
              this.listReshapeRegionFactor[this.indexEditing].value = value;
              this.indexEditing = null;
              this.messageService.success(this.notificationMessage.success);
            },
            (e) => {
              this.isLoading = false;
              if (e?.status === 0) {
                this.messageService.error(this.notificationMessage.E001);
                return;
              }
              this.messageService.error(e?.error?.description);
            }
          );
        }
      });
    } else {
      this.isValidator = true;
      validateAllFormFields(this.formCell);
    }
  }

  create() {
    // console.log('this.formCreate', this.formCreate);
    if (this.formCreate.valid) {
      const formData = this.formCreate.value;
      const data = {
        kpiFactorSetId: this.kpiFactorDtoId,
        ...formData,
        efficientDate: moment(new Date(formData?.efficientDate)).format(SERVER_DATE_FORMAT),
        expireDate: formData?.expireDate ? moment(new Date(formData?.expireDate)).format(SERVER_DATE_FORMAT) : '',
        kpiFactorDtos: [],
        type: 1,
      };
      // console.log('data', data);
      this.confirmService.confirm().then((res) => {
        if (res) {
          this.isLoading = true;
          this.kpiRegionApi.saveKpiFactorSet(data).subscribe(
            () => {
              this.isLoading = false;
              this.back();
              this.messageService.success(this.notificationMessage.success);
            },
            (e) => {
              this.isLoading = false;
              if (e?.status === 0) {
                this.messageService.error(this.notificationMessage.E001);
                return;
              }
              this.messageService.error(e?.error?.description);
            }
          );
        }
      });
    } else {
      this.isValidator = true;
      validateAllFormFields(this.formCreate);
    }
  }
}
