import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiRegionFactorUpdateComponent } from './kpi-region-factor-update.component';

describe('KpiRegionFactorUpdateComponent', () => {
  let component: KpiRegionFactorUpdateComponent;
  let fixture: ComponentFixture<KpiRegionFactorUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiRegionFactorUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiRegionFactorUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
