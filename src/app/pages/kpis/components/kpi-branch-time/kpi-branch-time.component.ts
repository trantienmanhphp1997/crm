import { forkJoin, of } from 'rxjs';
import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import * as _ from 'lodash';
import { cleanDataForm } from 'src/app/core/utils/function';
import { CampaignsService } from 'src/app/pages/campaigns/services/campaigns.service';
import { CommonCategory, FunctionCode } from 'src/app/core/utils/common-constants';
import { catchError } from 'rxjs/operators';
import {
  CLIENT_DATE_FORMAT,
  LIST_ALL_KPI_ASSESSMENT,
  LIST_ALL_KPI_STATUS,
  LIST_KPI_ASSESSMENT,
  SERVER_DATE_FORMAT,
} from '../../constant';
import { KpiBranchTimeApi } from '../../apis/kpi-branch-time.api';
import * as moment from 'moment';

@Component({
  selector: 'app-kpi-branch-time',
  templateUrl: './kpi-branch-time.component.html',
  styleUrls: ['./kpi-branch-time.component.scss'],
})
export class KpiBranchTimeComponent extends BaseComponent implements OnInit {
  listBranchTypeKPI: any;
  isLoading = false;
  listData = [];
  kpiCategoryOutputs = [];
  listBlock = [];
  listKpiStatus = LIST_ALL_KPI_STATUS;
  listKpiAssessment = LIST_ALL_KPI_ASSESSMENT;
  formSearch = this.fb.group({
    blockCode: 'SME', // Ma khoi - Mac dinh la SME
  });
  prevParams: any;
  prop: any;

  constructor(
    injector: Injector,
    private campaignService: CampaignsService,
    private kpiBranchTimeApi: KpiBranchTimeApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.KPI_FACTOR_BRANCHTIME}`);
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.prop = this.sessionService.getSessionData(FunctionCode.KPI_FACTOR_BRANCHTIME);
    if (this.prop) {
      this.prevParams = { ...this.prop?.prevParams };
      this.formSearch.patchValue(this.prevParams);
      this.listBlock = this.prop?.listBlock || [];
      this.listBranchTypeKPI = this.prop?.listBranchTypeKPI || [];
      this.search();
    } else {
      forkJoin([
        this.commonService.getCommonCategory(CommonCategory.KPI_SYS_ORG_TYPE).pipe(catchError((e) => of(undefined))),
        this.campaignService.getBlockMapping().pipe(catchError((e) => of(undefined))),
      ]).subscribe(([listBranchTypeKPI, listBlock]) => {
        this.listBranchTypeKPI = listBranchTypeKPI?.content || [];
        // map list block
        this.listBlock =
          listBlock?.content.map((item) => {
            return { code: item.code, name: item.code + ' - ' + item.name };
          }) || [];

        this.prop = {
          listBlock: this.listBlock,
          listBranchTypeKPI: this.listBranchTypeKPI,
        };

        this.sessionService.setSessionData(FunctionCode.KPI_FACTOR_BRANCHTIME, this.prop);
        this.search();
      });
    }
  }

  onChangeBlockCode() {
    this.search();
  }

  search() {
    this.isLoading = true;
    let params: any = {};
    params = cleanDataForm(this.formSearch);

    this.kpiBranchTimeApi.getKpiBranchTimeByBlockCode(params).subscribe(
      (result) => {
        if (result) {
          this.prevParams = params;
          this.prop.prevParams = params;
          this.listData =
            result?.map((item) => {
              const kpiSysOrgType = this.listBranchTypeKPI?.find((element) => element.code === item.kpiSysOrgTypeCode);
              return {
                ...item,
                efficientDate: item.efficientDate
                  ? moment(item.efficientDate, SERVER_DATE_FORMAT).format(CLIENT_DATE_FORMAT)
                  : '',
                expireDate: item.expireDate
                  ? moment(item.expireDate, SERVER_DATE_FORMAT).format(CLIENT_DATE_FORMAT)
                  : '',
                kpiSysOrgTypeName: kpiSysOrgType?.name,
              };
            }) || [];
        }
        this.sessionService.setSessionData(FunctionCode.KPI_FACTOR_BRANCHTIME, this.prop);
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  create() {
    this.router.navigate([this.router.url, 'create']);
  }

  update() {
    this.router.navigate([this.router.url, 'update', this.formSearch.value.blockCode]);
  }
}
