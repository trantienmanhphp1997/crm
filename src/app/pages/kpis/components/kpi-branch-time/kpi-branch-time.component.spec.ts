import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiBranchTimeComponent } from './kpi-branch-time.component';

describe('KpiBranchTimeComponent', () => {
  let component: KpiBranchTimeComponent;
  let fixture: ComponentFixture<KpiBranchTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpiBranchTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiBranchTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
