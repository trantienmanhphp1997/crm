import {Component, EventEmitter, Injector, Input, OnInit, Output} from '@angular/core';
import {FunctionCode, ListFrequencyCalendar, PriorityTaskOrCalendar} from '../../../../core/utils/common-constants';
import {ItemDetailComponent} from '../calendar-item-detail-modal/item-detail.component';
import {BaseComponent} from '../../../../core/components/base.component';
import {CalendarEditModalComponent} from '../calendar-add-modal/calendar-edit-modal/calendar-edit-modal.component';
import {CalendarService} from '../../services/calendar.service';
import {finalize} from 'rxjs/operators';
import {ConfirmDialogComponent} from '../../../../shared/components';
import {CalendarAlertInitService} from '../../../../core/services/calendar-alert-init.service';
import {ConfirmCalendarDialogComponent} from '../confirm-calendar-dialog/confirm-calendar-dialog.component';

@Component({
  selector: 'app-job-item',
  templateUrl: './job-item.component.html',
  styleUrls: ['./job-item.component.css']
})
export class JobItemComponent extends BaseComponent implements OnInit {

  isUpdateAll = false;
  dataFake: any;
  @Input() job?: any;
  @Input() index?: 0;
  @Input() isViewPersonal?: false;
  @Input() listEventType?: any;
  @Output()  onChangeSth: EventEmitter<any> = new EventEmitter<any>();
  @Output()  onChangeLoading: EventEmitter<any> = new EventEmitter<any>();
  data: {};
  constructor(injector: Injector,
              // private modalActive: NgbActiveModal,
              private calendarService: CalendarService,
              private  calendarAlertInitService :CalendarAlertInitService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CALENDARSME}`);
  }

  ngOnInit(): void {
    this.handleTimeDisplay();
  }
  handleTimeDisplay() {
    if (this.job.startDisplay === undefined) {
      this.job.startDisplay = this.job.start;
      this.job.endDisplay = this.job.end;

    }
  }
  getLimitLength(str?: string, length = 20, noWhiteSpace = 10): string {
    // nếu string không có khoảng trắng thì sẽ lấy length
    if (!!str && str.indexOf(' ') === -1) {
      return str.length <= length
        ? str
        : str.slice(0, noWhiteSpace) + '...';
    }
    return !!str
      ? str.length <= length
        ? str
        : str.slice(0, length) + '...'
      : '';
  }

  getStatusClass(): string {
    let statusClass = '';
    switch (this.job.status) {
      case 'NEW':
        statusClass = 'badge-primary';
        break;
      case 'DONE':
        statusClass = 'badge-success';
        break;
      case 'WAIT':
        statusClass = 'badge-blur';
        break;
      case 'REJECT':
        statusClass = 'badge-blur';
        break;
      default:
        statusClass = 'badge-danger';
        break;
    }
    return statusClass;
  }

  getTitleClass(): string {
    let statusClass = '';
    switch (this.job.status) {
      case 'NEW':
        statusClass = 'badge-primary-title';
        break;
      case 'DONE':
        statusClass = 'badge-success-title';
        break;
      case 'REJECT':
        statusClass = 'badge-blur-title';
        break;
      case 'WAIT':
        statusClass = 'badge-blur-title';
        break;
      default:
        statusClass = 'badge-danger-title';
        break;
    }
    // if (this.isShowBtnStar()) {
    //   statusClass += ' badge-border';
    // }
    return statusClass;
  }
  public isShowBtnStar(): boolean {
    return this.job.level === PriorityTaskOrCalendar.High; // Custom lại loic show button star
  }

  public isShowBtnChat(): boolean {
    return (this.job.comment !== undefined && this.job.comment !== ''); // Custom lại loic show button chat
  }

  public getImageFillByStatus(): string {
    let imageFill = '';
    switch (this.job.status) {
      case 'NEW':
        imageFill = 'primary';
        break;
      case 'DONE':
        imageFill = 'success';
        break;
      case 'WAIT':
        imageFill = 'blur';
        break;
      case 'REJECT':
        imageFill = 'blur';
        break;
      default:
        imageFill = 'danger';
        break;
    }
    return imageFill;
  }

  openDialogConfirm() {
    // phần này là cải tiến lịch chọn update 1 hay tất cả lịch, tạm thời pending
    // let value = '';
    // this.translate
    //   .get('calendar.chooseDialog')
    //   .subscribe((data) => {
    //     value = data
    //   });
    // const confirm = this.modalService.open(ConfirmCalendarDialogComponent, { windowClass: 'confirm-dialog' });
    // confirm.result
    //   .then((confirmed: number) => {
    //     if (confirmed !== 0) {
    //       if (confirmed === 2) {
    //         this.isUpdateAll = true;
    //       }
    //       this.getDetailEventLoop();
    //     }
    //   })
    //   .catch(() => {});

    this.isUpdateAll = true;
    this.getDetailEventLoop();
  }

  getDetailEventLoop() {
    const param = {rsId:this.objFunction.rsId, scope:'VIEW', idRoot: this.job.idRoot, startDate: this.job.startDisplay
      , endDate: this.job.endDisplay };
    this.onChangeLoading.emit(true);
    this.calendarService.getDetailEventsLoop(param)
    .subscribe(value => {
      if (value) {
        this.dataFake = value;
        this.dataFake.listUser = [{
          hrsCodeAssigns: value?.userHrsCode,
          name:value?.userCode + ' - ' + value?.fullNameUser,
          status: value?.status,
          reason: value?.reason,
        },...value.listEventAssigns.map(item => {
          return {
            hrsCodeAssigns: item?.userHrsCode,
            name:item?.userCode + ' - ' + item?.fullNameUser,
            status: item?.status,
            reason: item?.reason,
          };
        })];
        this.dataFake.start = new Date( this.dataFake.start);
        this.dataFake.end = new Date( this.dataFake.end);

        this.onChangeLoading.emit(false);
        const isOriginalRecord = (this.dataFake.idEventAssign === '' || this.dataFake.idEventAssign === undefined);
        const isPersonalRecord = (isOriginalRecord === true && this.dataFake.listUser.length === 1);
        const modal = this.modalService.open(CalendarEditModalComponent, { windowClass: 'create-calendar-modal' });
        modal.componentInstance.isDetailEvent = true;
        // modal.componentInstance.data = this.job;
        modal.componentInstance.data = this.dataFake;
        modal.componentInstance.isUpdateAll = this.isUpdateAll;
        modal.componentInstance.isOriginalRecord = isOriginalRecord;
        modal.componentInstance.isPersonalRecord = isPersonalRecord;
        modal.componentInstance.listEventType = this.listEventType;
        modal.result
          .then((result) => {
            this.isLoading = true;
            if (!result.isDelete) {
              if (result) {
                this.onChangeLoading.emit(true);
                result.value.status = this.updateStatus(result.value.status);
                this.data = result.value;
                if (this.isUpdateAll) {
                  this.calendarService.updateSme(this.data).subscribe(
                    (item) => {
                      this.onChangeLoading.emit(false);
                      this.messageService.success(this.notificationMessage.success);
                      this.onChangeSth.emit();
                    },
                    () => {
                      this.onChangeLoading.emit(false);
                      this.messageService.error(this.notificationMessage.error);
                    }
                  );
                } else {
                  this.calendarService.updateSmeOneRecord(this.data).subscribe(
                    (item) => {
                      this.onChangeLoading.emit(false);
                      this.messageService.success(this.notificationMessage.success);
                      this.onChangeSth.emit();
                    },
                    () => {
                      this.onChangeLoading.emit(false);
                      this.messageService.error(this.notificationMessage.error);
                    }
                  );
                }
              }
            } else {
              this.onChangeSth.emit();
            }
          })
      }
    },() => {
      this.messageService.error(this.notificationMessage.error);
    });
  }

  openPopupComment() {
    const modal = this.modalService.open(ItemDetailComponent, {
      windowClass: 'item-detail-calendar-modal',
    });
    modal.componentInstance.job = this.job;
    modal.componentInstance.isViewPersonal = this.isViewPersonal;
    modal.componentInstance.isUpdateAll = this.isUpdateAll;
    modal.result
      .then((res) => {
        if (res) {
          this.onChangeSth.emit();
        }
      })
      .catch(() => {});
  }

  openDetail() {
    // nếu là quản lý xem chi tiết của RM
    if (!this.isViewPersonal) {
      if (this.job.id === '') {
        let value = '';
        this.translate
          .get('calendar.chooseDialog')
          .subscribe((data) => {
            value = data
          });
        const confirm = this.modalService.open(ConfirmDialogComponent, { windowClass: 'confirm-dialog' });
        confirm.componentInstance.message = value;
        confirm.componentInstance.isConfirm = false;
        confirm.componentInstance.isChoose = true;
        confirm.componentInstance.title = 'Gia hạn SLA';
        confirm.result
          .then((confirmed: boolean) => {
            if (confirmed) {
              this.isUpdateAll = true;
            }
            this.openPopupComment();
          })
          .catch(() => {});
      } else {
        this.openPopupComment();
      }

    } else {
      // this.job.start = new Date( this.job.start);
      // this.job.end = new Date( this.job.end);
      // nếu là event lặp
      if (this.job.id === '') {
        this.openDialogConfirm();
      }
      else {
        const param = {rsId:this.objFunction.rsId, scope:'VIEW' };
        this.onChangeLoading.emit(true);
        this.calendarService.getDetailEvents(this.job.id, param).pipe(
          finalize(() => {
            this.onChangeLoading.emit(false);
            const isOriginalRecord = this.dataFake.idEventAssign === undefined ? true : false;
            const isPersonalRecord = (isOriginalRecord === true && this.dataFake.listUser.length === 1);
            const modal = this.modalService.open(CalendarEditModalComponent, { windowClass: 'create-calendar-modal' });
            modal.componentInstance.isDetailEvent = true;
            // modal.componentInstance.data = this.job;
            modal.componentInstance.data = this.dataFake;
            modal.componentInstance.isOriginalRecord = isOriginalRecord;
            modal.componentInstance.isUpdateAll = true;
            modal.componentInstance.isPersonalRecord = isPersonalRecord;
            modal.componentInstance.listEventType = this.listEventType;
            modal.result
              .then((result) => {
                this.isLoading = true;
                if (!result.isDelete) {
                  if (result) {
                    this.onChangeLoading.emit(true);
                    result.value.status = this.updateStatus(result.value.status);
                    this.data = result.value;
                    this.calendarService.updateSme(this.data).subscribe(
                      (item) => {
                        this.onChangeLoading.emit(false);
                        this.messageService.success(this.notificationMessage.success);
                        this.onChangeSth.emit();

                        // update alert calendar
                        // this.calendarAlertInitService.getCalendarOfTheDay();
                      },
                      () => {
                        this.messageService.error(this.notificationMessage.error);
                        this.onChangeLoading.emit(false);
                      }
                    );
                  }
                } else {
                  this.onChangeSth.emit();
                  // update alert calendar
                  // this.calendarAlertInitService.getCalendarOfTheDay();
                }
              })
          })
        ).subscribe(value => {
          if (value) {
            this.dataFake = value;
            this.dataFake.listUser = [{
              hrsCodeAssigns: value?.userHrsCode,
              name:value?.userCode + ' - ' + value?.fullNameUser,
              status: value?.status,
              reason: value?.reason,
            },...value.listEventAssigns.map(item => {
              return {
                hrsCodeAssigns: item?.userHrsCode,
                name:item?.userCode + ' - ' + item?.fullNameUser,
                status: item?.status,
                reason: item?.reason,
              };
            })];
            this.dataFake.start = new Date( this.dataFake.start);
            this.dataFake.end = new Date( this.dataFake.end);
          }
        },() => {
          this.messageService.error(this.notificationMessage.error);
        });
        // this.dataFake = this.fakeDataDetail;

      }
    }

  }
  updateStatus(status: string) {
    if (status !== 'DONE' && status !== 'WAIT' && status !== 'REJECT') {
      status = 'NEW';
    }
    return status;
  }
}
