import { ActivityService } from 'src/app/core/services/activity.service';
import { finalize} from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import {
  Component,
  OnInit,
  Injector,
  ViewEncapsulation,
  Input,
  EventEmitter,
} from '@angular/core';
import { CalendarService } from '../../services/calendar.service';
import { BaseComponent } from 'src/app/core/components/base.component';
import { RmApi } from 'src/app/pages/rm/apis';
import { CalendarOptions } from '@fullcalendar/angular';
import { CalendarAddModalComponent } from '../../components/calendar-add-modal/calendar-add-modal.component';
import {
  CommonCategory,
  FunctionCode, ListEventTypeCalendar,
  Scopes,
} from 'src/app/core/utils/common-constants';
import { Dropdown } from 'primeng/dropdown';
import {CategoryService} from '../../../system/services/category.service';
import {UserService} from '../../../system/services/users.service';
import {CalendarListEventModalComponent} from '../calendar-list-event/calendar-list-event-modal.component';
import * as _ from 'lodash';
import {CalendarAlertInitService} from '../../../../core/services/calendar-alert-init.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe],
})

export class CalendarComponent extends BaseComponent implements OnInit {
  paramsRM = {
    rsId: '',
    scope: Scopes.VIEW,
  };
  totalItem: 0;
  isViewPersonal = true;
  listData = [];
  isLoading = false;
  monday: Date;
  tuesday: Date;
  wednesday: Date;
  thursday: Date;
  friday: Date;
  saturday: Date;
  sunday: Date;
  listEventType = ListEventTypeCalendar;
  params = {
    status: 0,
    rsId: '',
    hrsCode: [''],
    code: '',
    scope: Scopes.VIEW,
    startDate: '',
    endDate: '',
    pageNumber: 0,
    pageSize: 3,
  };
  // listRm = [{
  //   code: this.currUser?.hrsCode,
  //   name: `${this.currUser?.code || ''} - ${this.currUser?.fullName || ''}`,
  // }];
  listRm = [];
  rmSelected: any;
  eventTypeSelected: any;
  eventStatusFilter = {
    outDate: false,
    finished: false,
    unfinished: false,
    all: true,
    wait: false,
  };
  configForm = this.fb.group({
    dateData: [new Date()]
  });

  constructor(
    injector: Injector,
    private calendarService: CalendarService,
    private rmApi: RmApi,
    private activityService: ActivityService,
    private  categoryService :CategoryService,
    private  calendarAlertInitService :CalendarAlertInitService,
    private userService :UserService
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CALENDARSME}`);
    this.isLoading = true;
  }

  ngOnInit() {
    if (this.isViewPersonal) {
      this.params.hrsCode[0] = this.currUser?.hrsCode;
    }
    this.isLoading = true;
    this.paramsRM.rsId = this.objFunction.rsId;
    this.params.rsId = this.objFunction.rsId;
    // get dropdown list RM
    this.userService.getRmByCBQL(this.paramsRM).subscribe((values => {
      if(values) {
        values.content.forEach(value => {
          this.listRm.push({
            code: value.hrsCode,
            name: `${value.rmCode || ''} - ${value.rmName || ''}`,
          });
        })
      }
    }))
    // get dropdown list event type
    this.categoryService.getCommonCategory(CommonCategory.CALENDAR_CONFIG).subscribe((value => {
      if (value.content) {
        Object.keys(value.content).forEach((key) => {
          this.listEventType.push({code:value.content[key].code, name:value.content[key].value
            , listSuggest: JSON.parse(value.content[key].description)})
        });
      }
    }))
    this.getDayOfWeek(new Date());
    this.getEvents();
  }

  getEvents() {
    this.isLoading = true;
    this.calendarService.getAllEventsForCBQL(this.params).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(value => {
      if (value.content) {
        this.totalItem = value.totalElements;
        this.listData = value.content;
        this.getListData();

        // cảnh báo lích
        this.calendarAlertInitService.handleListAlert(value.content[0]?.listEvent)
        // const calendarAlert = this.sessionService.getSessionData('CALENDAR_ALERT');
        // if (calendarAlert === undefined) {
        //   this.calendarAlertInitService.getCalendarOfTheDay();
        // }
      }
    },() => {
    });
  }

  changeLoading(value: boolean) {
    this.isLoading = value;
  }
  create() {
    const modal = this.modalService.open(CalendarAddModalComponent, { windowClass: 'create-calendar-modal' });
    modal.result
      .then((res) => {
        if (res) {
          this.getEvents();
        } else{
        }
      })
      .catch(() => {});
  }

  onChangeEventType() {
    this.params.code =  this.eventTypeSelected;
    this.getEvents();
  }

  onChangeRM() {
    if (!_.isEqual(this.params.hrsCode, this.rmSelected)) {
      this.params.hrsCode = this.rmSelected;
      this.getEvents();
    }
  }

  onBlur(dropdown: Dropdown) {
    this.ref.detectChanges();
  }

  handleEventStatus(value: number) {
    this.eventStatusFilter = {
      outDate: value === 3,
      all: value === 0,
      finished: value === 2,
      unfinished: value === 1,
      wait: value === 4,
    };
    this.params.status = value
    this.getEvents();
  }

  triggerDateFilterClick(e) {
    e.stopPropagation();
  }

  getListData() {
    let local = [];
    let tempArr = [{nameRm: '', jobs: []}];
    tempArr.pop();
    this.listData.forEach(i => {
      local = [...local, {nameRm:i.rmCode + ' - ' + i.rmName, jobs: i.listEvent || []}];
      tempArr = [...tempArr, {nameRm:i.rmCode + ' - ' + i.rmName, jobs: []}];
    });
    local.filter((e,index) => {
       e.jobs.filter(job => {
         job.fullNameUser = job.userCode + ' - ' + job.fullNameUser;
         this.updateStatusOfEvent(job);
         // với các event lặp
         if (job.id === undefined) {
           job.id = '';
         }
        if (!this.isSameDay(new Date(job.start), new Date(job.end))) {
          const start = new Date(job.start);
          const end = new Date(job.end);
          const n = this.dateDiffInDays(start,end)
          /// tách event vắt ngày thành nhiều event
          for (let i = 0; i <= n; i ++) {
            /// startNew, endNew chỉ để hiển thị, update thì dùng start, end
            let startNew = '';
            if (i === 0) {
              startNew = start.toJSON();
            } else {
              startNew = new Date(new Date(start.setDate(start.getDate() + 1)).setHours(0,0,0)).toJSON();
            }
            let endNew = ''
            if (!this.isSameDay(new Date(startNew), end)) {
              endNew = (new Date(new Date(startNew).setHours(23,59,59))).toJSON();
            } else {
              endNew = end.toJSON();
            }
            const data = {id: job.id,startDisplay: startNew, endDisplay: endNew, start : job.start, idRoot: job.idRoot,
              end: job.end, description: job.description, code: job.code, location: job.location, result: job.result,comment: job.comment
              , status: job.status,level: job.level, name: job.name, fullNameUser: job.fullNameUser, isSeVeralDay: true}
            tempArr[index].jobs.push(data);
          }
        }
        else {
          tempArr[index].jobs.push(job);
        }
      })
    })
    // sau khi tách event thành các event nhỏ thì xóa event gốc
    tempArr.filter(e => {
      e.jobs = e.jobs.filter(i => {
         return ((this.isSameDay(new Date(i.start), new Date(i.end))) || i.startDisplay !== undefined)
      })
    });
    // order time
    tempArr.forEach(i => {
      i.jobs = _.orderBy(i.jobs, ['start'], ['asc']);
    })
    this.listData = tempArr;
  }

  updateStatusOfEvent(i : any) {
    if (i.status === 'NEW' && (new Date(i.end) < new Date())) {
      i.status = 'EXPIRE';
    }
  }
  getDayOfWeek(d) {
    d = new Date(d);
    const  day = d.getDay();
    const diff = d.getDate() - day + (day === 0 ? -6 : 1); // adjust when day is sunday
    this.monday =  new Date(d.setDate(diff));
    this.tuesday =  new Date(d.setDate(this.monday.getDate() + 1));
    this.wednesday =  new Date(d.setDate(this.tuesday.getDate() + 1));
    this.thursday =  new Date(d.setDate(this.wednesday.getDate() + 1));
    this.friday =  new Date(d.setDate(this.thursday.getDate() + 1));
    this.saturday =  new Date(d.setDate(this.friday.getDate() + 1));
    this.sunday =  new Date(d.setDate(this.saturday.getDate() + 1));
    this.params.startDate = new Date(this.monday.setHours(0,0,0,0)).toISOString();
    this.params.endDate = new Date(this.sunday.setHours(23,59,59,0)).toISOString();
  }

  isSameDay(d1, d2) {
    return d1.getFullYear() === d2.getFullYear() &&
      d1.getMonth() === d2.getMonth() &&
      d1.getDate() === d2.getDate();
  }

  onChangeDate() {
    this.getDayOfWeek(this.configForm.get('dateData').value);
    this.getEvents();
  }

  getJobByDate(date1: Date, jobs: any []) {
    const job = jobs.filter(item => {
      if (item.startDisplay === undefined) {
        return this.isSameDay(date1, new Date(item.start));
      } else {
        return this.isSameDay(date1, new Date(item.startDisplay));
      }
    });
    return job;
  }

  goToDay() {
    this.getDayOfWeek(new Date());
    this.configForm.get('dateData').setValue(new Date());
    this.getEvents();
  }

  showAllEvent(data: any []) {
    const modal = this.modalService.open(CalendarListEventModalComponent, { windowClass: 'create-calendar-modal' });
    modal.componentInstance.listData = data;
    modal.result
      .then((res) => {
        if (res) {
          this.getEvents();
        } else {
        }
      })
      .catch(() => {});
  }

  changeTypeView() {
    this.listData = [];
    this.isLoading = true;
    this.rmSelected = [];
    if (this.isViewPersonal) {
      this.params.hrsCode = [];
    } else {
      this.params.hrsCode = [];
      this.params.hrsCode[0] = this.currUser.hrsCode;
    }
    this.isViewPersonal = !this.isViewPersonal;
    this.getEvents();
  }

  changePage(event) {
    this.params.pageNumber = event.pageIndex;
    this.params.pageSize = event.pageSize;
    this.getEvents();
  }

  getHeadClass(d1 : Date) {
    const d2 = new Date();
    if (d1.getFullYear() === d2.getFullYear() &&
      d1.getMonth() === d2.getMonth() &&
      d1.getDate() === d2.getDate()) {
      return 'background-head'
    }
  }

  dateDiffInDays(d1, d2) {
    const oneDay = 24 * 60 * 60 * 1000;
    // Discard the time and time-zone information.
    const utc1 = Date.UTC(d1.getFullYear(), d1.getMonth(), d1.getDate());
    const utc2 = Date.UTC(d2.getFullYear(), d2.getMonth(), d2.getDate());

    return Math.floor((utc2 - utc1) / oneDay);
  }

  nextWeek() {
    this.getDayOfWeek(this.sunday.setDate(this.sunday.getDate() + 1));
    this.getEvents();
  }

  preWeek() {
    this.getDayOfWeek(this.monday.setDate(this.monday.getDate() - 1));
    this.getEvents();
  }
}
