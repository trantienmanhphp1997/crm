import { Component, OnInit, HostBinding, Injector, ViewEncapsulation, Input } from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { CalendarService } from '../../services/calendar.service';
import { CategoryService } from '../../../system/services/category.service';
import {
  CommonCategory,
  Format,
  FunctionCode,
  ListFrequencyCalendar,
  PriorityTaskOrCalendar,
  Scopes,
} from 'src/app/core/utils/common-constants';
import { finalize } from 'rxjs/operators';
import { CalendarEditModalComponent } from './calendar-edit-modal/calendar-edit-modal.component';
import * as _ from 'lodash';
import { RmModalComponent } from '../../../rm/components';
import { ChooseUserComponent } from '../../../tasks/components';
import { RmApi } from '../../../rm/apis';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-calendar-add-modal',
  templateUrl: './calendar-add-modal.component.html',
  styleUrls: ['./calendar-add-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CalendarAddModalComponent extends BaseComponent implements OnInit {
  @HostBinding('class.app-create-calendar') classCreateCalendar = true;
  @Input() leadCode?: any;
  @Input() leadName?: any;
  @Input() customerCode?: any;
  @Input() customerName?: any;
  formData: any;
  isDetailLead = false;
  listUser = [];
  isLoading = false;
  now = new Date();
  listUsersFollow = [];
  listEventType = [];
  listSuggest = [];
  listData = [];
  listFrequency = ListFrequencyCalendar;
  form = this.fb.group({
    name: ['', CustomValidators.required],
    description: ['', CustomValidators.required],
    location: [''],
    repeat: [0],
    start: [this.now, CustomValidators.required],
    end: [this.now, CustomValidators.required],
    level: PriorityTaskOrCalendar.Medium,
    code: '',
    hrsCodeAssigns: [[]],
    listUser: [[]],
    leadCode: '',
    leadName: '',
    customerCode: '',
    customerName: '',
    scope: Scopes.VIEW,
    rsId: '',
  });
  initialValues = this.form.value;
  optCreateDate = {
    format: Format.DateTimeUp,
    minDate: this.now,
  };
  formSearch = this.fb.group({
    division: '',
    reportBy: 'rm',
    downloadReport: 'HTML',
    businessDate: [new Date(), CustomValidators.required],
  });
  optDueDate = {
    format: Format.DateTimeUp,
    minDate: this.now,
  };
  constructor(
    injector: Injector,
    private modalActive: NgbActiveModal,
    private calendarService: CalendarService,
    private categoryService: CategoryService,
    private rmApi: RmApi
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CALENDARSME}`);
  }

  ngOnInit() {
    this.form.get('rsId').setValue(this.objFunction.rsId);
    this.getLeadCodeActivityCode();
    this.isLoading = true;

    forkJoin([
      this.categoryService.getCommonCategory(CommonCategory.CALENDAR_CONFIG),
      this.rmApi.getAllRmInBranch()
    ]).pipe(
      finalize(() => {
        this.isLoading = false;
      })
    ).subscribe(([calendarConfig, listFollow]) => {
      if (calendarConfig.content) {
        Object.keys(calendarConfig.content).forEach((key) => {
          // tslint:disable-next-line:max-line-length
          this.listEventType.push({code:calendarConfig.content[key].code, name:calendarConfig.content[key].value, listSuggest: JSON.parse(calendarConfig.content[key].description) });
          this.form.get('name').setValue(_.get(_.head(this.listEventType), 'name'), {emitEvent: false})
          this.form.get('code').setValue(_.get(_.head(this.listEventType), 'code'), {emitEvent: false})
          this.onChangeWork();
        });
      }
      this.listUsersFollow = listFollow;

      this.initData();
    })
  }

  ngAfterViewInit() {
    this.form.controls.start.valueChanges.subscribe((value) => {
      this.optDueDate.minDate = value;
      if (value.valueOf() > this.form.controls.end.value.valueOf()) {
        this.form.controls.end.setValue(value);
      }
    });
  }

  getLeadCodeActivityCode() {
    if (this.leadCode !== null) {
      this.form.get('leadCode').setValue(this.leadCode);
      this.form.get('leadName').setValue(this.leadName);
    }
    if (this.customerCode !== null) {
      this.form.get('customerCode').setValue(this.customerCode);
      this.form.get('customerName').setValue(this.customerName);
    }
  }

  confirmDialog() {
    if (this.form.valid || this.listData.length > 0) {
      if (this.listData.length === 0) {
        this.addJob();
      }
      this.isLoading = true;
      this.calendarService.createSme(this.listData).subscribe(
        (item) => {
          this.messageService.success(this.notificationMessage.success);
          this.modalActive.close(item);
        },
        () => {
          this.messageService.success(this.notificationMessage.success);
          this.modalActive.close(true);
        }
      );
    } else {
      validateAllFormFields(this.form);
    }
  }

  closeModal() {
    this.modalActive.close(false);
  }

  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }

  addJob() {
    if (this.form.valid) {
      this.handleForm();
      this.form.get('listUser').setValue(this.listUser);
      this.listData.push(this.form.getRawValue());
      this.form.reset(this.initialValues);
      this.getLeadCodeActivityCode();
      this.listUser = [];
      this.form.get('rsId').setValue(this.objFunction.rsId);
      this.form.get('name').setValue(_.get(_.head(this.listEventType), 'name'), { emitEvent: false });
      this.form.get('code').setValue(_.get(_.head(this.listEventType), 'code'), { emitEvent: false });
      this.onChangeWork();

      this.initData();
    } else {
      validateAllFormFields(this.form);
    }
  }
  handleForm() {
    this.form.get('code').setValue(this.listEventType.find((i) => i.name === this.form.get('name').value).code);
  }
  onChangeWork() {
    this.listSuggest = this.listEventType.filter((i) => i.name === this.form.get('name').value)[0].listSuggest;
  }
  onChangeSuggest(i: number) {
    this.form.get('description').setValue(this.listSuggest[i]);
  }
  onDelete(i: number) {
    this.listData = this.listData.filter((item) => item !== this.listData[i]);
  }

  onChangeLevel() {
    if (this.form.get('level').value === PriorityTaskOrCalendar.Medium) {
      this.form.get('level').setValue(PriorityTaskOrCalendar.High);
    } else {
      this.form.get('level').setValue(PriorityTaskOrCalendar.Medium);
    }
  }

  onEdit(i: number) {
    const modal = this.modalService.open(CalendarEditModalComponent, { windowClass: 'create-calendar-modal' });
    modal.componentInstance.data = this.listData[i];
    // modal.componentInstance.listUser = this.listData[i]
    modal.componentInstance.isDetailEvent = false;
    modal.componentInstance.isDetailLead = this.isDetailLead;
    modal.componentInstance.listUsersFollow = this.listUsersFollow;
    modal.componentInstance.listEventType = this.listEventType;
    modal.componentInstance.disableList = ['name'];
    modal.result.then((result) => {
      if (result) {
        this.listData[i] = result.getRawValue();
      }
    });
  }

  isDisableRepeat() {
    if (this.isDetailLead) {
      return true;
    }
    if (this.form.get('start').value === '' || this.form.get('end').value === '') {
      this.form.get('repeat').setValue(0);
      return true;
    } else if (
      this.form.get('end').value !== null &&
      this.form.get('end').value.getDate() !== this.form.get('start').value.getDate()
    ) {
      this.form.get('repeat').setValue(0);
      return true;
    } else {
      return false;
    }
  }

  onDeleteUser(index: number) {
    _.remove(this.listUser, this.listUser[index]);
    this.updateHrsCode();
  }

  search() {
    const chooseModal = this.modalService.open(ChooseUserComponent, { windowClass: 'choose-user-modal' });
    chooseModal.componentInstance.data = this.listUsersFollow?.filter((item) => {
      return (
        this.listUser?.findIndex((i) => {
          return i.hrsCodeAssigns === item.hrsCode;
        }) === -1
      );
    });
    chooseModal.result
      .then((result) => {
        if (result) {
          this.listUser.push({ hrsCodeAssigns: result?.hrsCode, name: result?.code + ' - ' + result?.fullName });
          this.updateHrsCode();
        }
      })
      .catch(() => {});
  }

  updateHrsCode() {
    if (
      this.listUser.length > 1 ||
      (this.listUser.length === 1 && this.listUser[0].hrsCodeAssigns !== this.currUser?.hrsCode)
    ) {
      this.form.get('hrsCodeAssigns').setValue(this.listUser.map((item) => item.hrsCodeAssigns));
    }
  }

  getLimitLength(str?: string, length = 20, noWhiteSpace = 10): string {
    // nếu string không có khoảng trắng thì sẽ lấy length
    if (!!str && str.indexOf(' ') === -1) {
      return str.length <= length ? str : str.slice(0, noWhiteSpace) + '...';
    }
    return !!str ? (str.length <= length ? str : str.slice(0, length) + '...') : '';
  }

  // init data from other place
  initData() {
    if (!this.formData) return;
    let { listUser, isPersional } = this.formData;
    switch (this.formData?.function) {
      case FunctionCode.COACHING_SUGGEST:
        const activity = this.listEventType.find((item) => item.code === this.formData.activity);

        this.form.get('name').disable();
        this.form.get('name').setValue(activity.name);
        this.form.get('code').setValue(activity.code);

        if (listUser.length >= 2) {
          // remove all use is not in listUsersFollow
          for (let i = 0; i < listUser.length; i++) {
            const isExist = this.listUsersFollow.find((v) => v.hrsCode === listUser[i].hrsCodeAssigns);

            if (!isExist) {
              listUser = listUser.filter((user) => user.hrsCodeAssigns !== listUser[i].hrsCodeAssigns);
            }
          }
        }
        this.listUser = [...this.listUser, ...listUser];

        if (!isPersional) {
          this.updateHrsCode();
        }

        this.onChangeWork();
        break;
      default:
        break;
    }
  }
}
