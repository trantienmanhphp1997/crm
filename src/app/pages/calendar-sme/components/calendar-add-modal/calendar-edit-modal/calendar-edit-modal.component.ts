import {Component, OnInit, HostBinding, Injector, ViewEncapsulation} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CalendarService } from '../../../services/calendar.service';
import {
  Format,
  FunctionCode,
  ListFrequencyCalendar,
  PriorityTaskOrCalendar,
  Scopes
} from 'src/app/core/utils/common-constants';
import {validateAllFormFields} from '../../../../../core/utils/function';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {RmModalComponent} from '../../../../rm/components';
import * as _ from 'lodash';
import {ChooseUserComponent} from '../../../../tasks/components';
import {finalize} from 'rxjs/operators';
import {RmApi} from '../../../../rm/apis';
import {CalendarAlertInitService} from '../../../../../core/services/calendar-alert-init.service';

@Component({
  selector: 'app-calendar-edit-modal',
  templateUrl: './calendar-edit-modal.component.html',
  styleUrls: ['./calendar-edit-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CalendarEditModalComponent extends BaseComponent implements OnInit {
  @HostBinding('class.app-create-calendar') classCreateCalendar = true;
  listUser = [];
  data: {};
  isUpdateAll = false;
  now = new Date();
  isDetailLead = false;
  cannotUpdateRepeat: boolean;
  start: string;
  end: string;
  listEventType = [];
  listUsersFollow = [];
  isLoading = false;
  listSuggest = [];
  isOriginalRecord = true;
  listData = [];
  listFrequency = ListFrequencyCalendar;
  isDetailEvent:boolean;
  isPersonalRecord:boolean;
  form = this.fb.group({
    id: '',
    idRoot: '',
    name: ['', CustomValidators.required],
    description: ['', [CustomValidators.required]],
    location: [''],
    repeat: [0],
    start: [''],
    end: [''],
    level: [],
    code: '',
    comment: '',
    status: '',
    result: '',
    hrsCodeAssigns:[[]],
    listUser: [[]],
    reason: ['',],
    leadCode: '',
    leadName: '',
    isUpdateTime: false,
    customerCode: '',
    customerName: '',
    scope: Scopes.VIEW,
    rsId: '',
    idEventAssign: ''
  });
  flagChecked = false ;
  isOutreachProcess: true;
  isWaitEvent = false;
  formSearch = this.fb.group({
    division: '',
    reportBy: 'rm',
    downloadReport: 'HTML',
    businessDate: [new Date(), CustomValidators.required],
  });
  optCreateDate = {
    format: Format.DateTimeUp,
    minDate: this.now,
  };
  disableList = []; // use for quick edit in add function

  constructor(injector: Injector,
              private modalActive: NgbActiveModal,
              private calendarService: CalendarService,
              private rmApi: RmApi,
              private modal: NgbActiveModal,
              private  calendarAlertInitService :CalendarAlertInitService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CALENDARSME}`);
  }

  ngOnInit() {
    this.disableFields();

    this.form.patchValue(this.data);
    this.form.get('rsId').setValue(this.objFunction.rsId);
    this.listUser = this.form.get('listUser').value;
    this.checkStatus();
    this.form.get('comment').value === undefined ? this.form.get('comment').setValue('') : this.form.get('comment').setValue(this.form.get('comment').value);
    this.listSuggest = this.listEventType.filter(i => i.name === this.form.get('name').value)[0].listSuggest;
    // nếu là xem chi tiết
    if (this.isDetailEvent) {
      this.cannotUpdateRepeat = this.form.get('repeat').value === 0 ? false : true;
      this.start = this.form.get('start').value;
      this.end = this.form.get('end').value;
      this.isLoading = true;
      this.rmApi.getAllRmInBranch().pipe(
        finalize(() => {
          this.isLoading = false;
        })
      ).subscribe((listFollow) => {
        this.listUsersFollow = listFollow;
      });
    }
  }

  confirmDialog(status: string) {
    if (this.form.valid) {
      if (_.isEmpty(this.listUser)) {
        this.listUser.push(
          {hrsCodeAssigns: this.currUser?.hrsCode,
            name:this.currUser?.code + ' - ' + this.currUser?.fullName});
      }
      this.updateHrsCode();
      this.form.get('rsId').setValue(this.objFunction.rsId);
      this.form.get('listUser').setValue(this.listUser);
      if (this.isDetailEvent && (!(this.form.get('start').value.toString() === this.start.toString())
        || !(this.form.get('end').value.toString() === this.end.toString())) ) {
        this.form.get('isUpdateTime').setValue(true);
      }
      this.modal.close(this.form);
    } else {
      validateAllFormFields(this.form);
    }
  }

  replyDialog(status: string) {
    if (status === 'accept') {
      this.form.get('status').setValue('NEW');
    } else if (status === 'reject') {
      this.form.get('status').setValue('REJECT');
      if (this.form.get('reason').value?.trim()?.length === 0) {
        this.form.get('reason').setErrors({cusRequired:true});
      }
    }
    if (this.form.valid) {
      const data = this.form.value;
      this.calendarService.replySme(data).subscribe(
        (item) => {
          if (item === 'SUCCESS') {
            this.messageService.success(this.notificationMessage.success);
            this.modalActive.close({ isDelete: true });
          }
        },
        () => {
          this.messageService.error(this.notificationMessage.error);
          this.isLoading = false;
        }
      );
    } else {
      validateAllFormFields(this.form);
    }
  }

  closeModal() {
    this.modalActive.close(false);
  }

  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }

  onChangeWork() {
    this.listSuggest = this.listEventType.filter(i => i.name === this.form.get('name').value)[0].listSuggest;
  }

  onChangeSuggest(i: number) {
    this.form.get('description').setValue(this.listSuggest[i]);
  }

  onChangeLevel() {
    if (!this.flagChecked && this.isOriginalRecord) {
      this.form.get('level').value === PriorityTaskOrCalendar.Medium ? this.form.get('level').setValue(PriorityTaskOrCalendar.High)
        : this.form.get('level').setValue(PriorityTaskOrCalendar.Medium)
    }
  }

  delete() {
    let data = {id: '', idRoot: ''};
    data = this.form.value;
    this.confirmService
      .confirm()
      .then((res) => {
        if (res) {
          if (this.isUpdateAll) {
            this.calendarService.deleteSme(data).subscribe(
              (item) => {
                if (item === 'SUCCESS') {
                  // update alert calendar
                  // this.calendarAlertInitService.getCalendarOfTheDay();

                  this.messageService.success(this.notificationMessage.success);
                  this.modalActive.close({ isDelete: true });
                }
              },
              () => {
                this.messageService.error(this.notificationMessage.error);
                this.isLoading = false;
              }
            );
          } else {
            this.calendarService.deleteSmeOneRecord(data).subscribe(
              (item) => {
                if (item === 'SUCCESS') {
                  // update alert calendar
                  // this.calendarAlertInitService.getCalendarOfTheDay();

                  this.messageService.success(this.notificationMessage.success);
                  this.modalActive.close({ isDelete: true });
                }
              },
              () => {
                this.messageService.error(this.notificationMessage.error);
                this.isLoading = false;
              }
            );
          }

        }
      })
      .catch(() => {});
  }
  onchangeStatus() {
    this.form.get('status').setValue(this.form.get('status').value === 'DONE' ? 'NEW' : 'DONE');
    this.checkStatus()
  }
  checkStatus() {
    this.flagChecked = (this.form.get('status').value === 'DONE' || this.form.get('status').value === 'WAIT') ? true : false
    this.isWaitEvent = this.form.get('status').value === 'WAIT' ? true : false
  }

  // dateLessThan(g: FormGroup) {
  //   return g.get('start').value >= g.get('end').value
  //     ? null : {errDate: true};
  // }

  isDisableRepeat() {
    if (this.form.get('start').value === '' || this.form.get('end').value === '') {
      this.form.get('repeat').setValue(0);
      return true;
    }
    else if (this.form.get('end').value !== null && this.form.get('end').value.getDate() !== this.form.get('start').value.getDate()) {
      this.form.get('repeat').setValue(0);
      return true;
    }
    else {
      return false;
    }
  }

  onDeleteUser(index: number) {
    _.remove(this.listUser, this.listUser[index]);
    this.updateHrsCode();
  }

  updateHrsCode() {
    this.form.get('hrsCodeAssigns').setValue(this.listUser.map((item) => item.hrsCodeAssigns));
  }

  search() {
    const chooseModal = this.modalService.open(ChooseUserComponent, { windowClass: 'choose-user-modal' });
    chooseModal.componentInstance.data  = this.listUsersFollow?.filter((item) => {
      return (
        this.listUser?.findIndex((i) => {
          return i.hrsCodeAssigns === item.hrsCode;
        }) === -1
      );
    });
    chooseModal.result
      .then((result) => {
        if (result) {
          this.listUser.push(
            {hrsCodeAssigns: result?.hrsCode,
              name:result?.code + ' - ' + result?.fullName,});
          this.updateHrsCode();
        }
      })
      .catch(() => {});
  }

  getStatusBtnUser(status: string): string {
    let statusClass = '';
    switch (status) {
      case 'NEW':
        statusClass = 'btn-user-active';
        break;
      case 'DONE':
        statusClass = 'btn-user-active';
        break;
      case 'REJECT':
        statusClass = 'btn-user-danger';
        break;
      case 'WAIT':
        statusClass = 'btn-user';
        break;
      default:
        statusClass = 'btn-user';
        break;
    }
    return statusClass;
  }

  getImgByStatus(status: string): string {
    let statusClass = '';
    switch (status) {
      case 'NEW':
        statusClass = 'la-check_active-custom';
        break;
      case 'DONE':
        statusClass = 'la-check_active-custom';
        break;
      case 'REJECT':
        statusClass = 'la-combined_shape-custom';
        break;
      default:
        statusClass = '';
        break;
    }
    return statusClass;
  }

  changeStartTime() {
    if (this.form.get('start').value <= new Date()) {
      this.form.get('start').setValue(new Date());
    }
    if (this.form.get('start').value >= this.form.get('end').value ) {
      this.form.get('end').setValue(this.form.get('start').value);
    }
  }

  disableFields(): void {
    this.disableList.forEach(item => {
      this.form.get(item).disable();
    });
  }
}
