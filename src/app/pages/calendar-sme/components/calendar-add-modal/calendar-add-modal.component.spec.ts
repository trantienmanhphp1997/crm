import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarAddModalComponent } from './calendar-add-modal.component';

describe('CalendarAddModalComponent', () => {
  let component: CalendarAddModalComponent;
  let fixture: ComponentFixture<CalendarAddModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarAddModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarAddModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
