import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ConfirmType } from 'src/app/core/utils/common-constants';

@Component({
  selector: 'confirm-calendar-dialog',
  templateUrl: './confirm-calendar-dialog.component.html',
  styleUrls: ['./confirm-calendar-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ConfirmCalendarDialogComponent implements OnInit {
  message: string | string[];
  type: number;
  confirmType = ConfirmType;
  title: string;
  isConfirm = true;
  isCopy: boolean;
  background = 'background-blue';
  listMessage: string[] = [];
  content = '';
  color = 'light-blue';
  constructor(private modal: NgbActiveModal) {}

  ngOnInit(): void {
  }

  close(event: number) {
    this.modal.close(event);
  }

  copy() {
    const el = document.createElement('textarea');
    el.value = document.getElementById('content').textContent;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    this.modal.close(false);
  }
}
