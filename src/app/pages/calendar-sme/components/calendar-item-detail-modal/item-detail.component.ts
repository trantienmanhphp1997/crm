import {Component, HostBinding, Injector, OnInit, ViewEncapsulation} from '@angular/core';
import {BaseComponent} from '../../../../core/components/base.component';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FunctionCode, ListFrequencyCalendar, Scopes} from '../../../../core/utils/common-constants';
import {CalendarService} from '../../services/calendar.service';
import {cleanDataForm} from '../../../../core/utils/function';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ItemDetailComponent extends BaseComponent implements OnInit {
  @HostBinding('class.app-item-detail-calendar') classItemDetailCalendar = true;
  isViewPersonal = false;
  isUpdateAll = false;
  id:{};
  job:any;
  data: {};
  param: {};
  constructor(injector: Injector,
              private modalActive: NgbActiveModal,
              private calendarService: CalendarService,
  ) {
    super(injector);
    this.objFunction = this.sessionService.getSessionData(`FUNCTION_${FunctionCode.CALENDARSME}`);
    this.isLoading = true;
  }
  formGroup = this.fb.group({
    id: [{value: '', disabled: true}],
    idRoot: '',
    code: '',
    start: new Date(),
    end: new Date(),
    description: [{value: '', disabled: true}],
    name: [{value: '', disabled: true}],
    location: [{value: '', disabled: true}],
    comment: [{value: '', disabled: false}],
    repeat: [{value: 0, disabled: true}],
    level: '',
    status: '',
    fullNameUser:  [{value: '', disabled: true}],
    result:  [{value: '', disabled: true}],
    rsId: '',
    scope: Scopes.VIEW,
    hrsCodeAssigns: []
  });
  listFrequency = ListFrequencyCalendar;
  ngOnInit(): void {
    this.formGroup.patchValue(this.job);
    this.formGroup.get('status').setValue(this.updateStatus( this.formGroup.get('status').value));
    this.formGroup.get('rsId').setValue(this.objFunction.rsId);
    this.isLoading = false;
  }
  updateStatus(status: string) {
    if (status !== 'DONE' && status !== 'WAIT') {
      status = 'NEW';
    }
    return status;
  }
  public onSubmit() {
    this.isLoading = true;
    this.data = cleanDataForm(this.formGroup);
    this.calendarService.updateSme(this.data).subscribe(
      (item) => {
        this.messageService.success(this.notificationMessage.success);
        this.job.comment = this.formGroup.get('comment').value;
        this.modalActive.close(this.job);
      },
      () => {
        this.messageService.error(this.notificationMessage.error);
        this.modalActive.close();
      }
    );
  }

  closeModal() {
    this.modalActive.close(false);
  }
}
