import {Component, OnInit, HostBinding, Injector, ViewEncapsulation} from '@angular/core';
import { BaseComponent } from 'src/app/core/components/base.component';
import { CustomValidators } from 'src/app/core/utils/custom-validations';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { validateAllFormFields } from 'src/app/core/utils/function';
import { CalendarService } from '../../services/calendar.service';
import { CategoryService } from '../../../system/services/category.service';
import {
  CommonCategory,
  Format,
  ListFrequencyCalendar,
  PriorityTaskOrCalendar
} from 'src/app/core/utils/common-constants';
import {ItemDetailComponent} from '../calendar-item-detail-modal/item-detail.component';

@Component({
  selector: 'app-calendar-list-event-modal',
  templateUrl: './calendar-list-event-modal.component.html',
  styleUrls: ['./calendar-list-event-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CalendarListEventModalComponent extends BaseComponent implements OnInit {
  @HostBinding('class.app-list-event-calendar') classCreateCalendar = true;
  isLoading = false;
  now = new Date();
  listData: any;
  listFrequency = ListFrequencyCalendar;
  optCreateDate = {
    format: Format.DateTimeUp,
    minDate: this.now,
  };

  constructor(injector: Injector,
              private modalActive: NgbActiveModal,
              private calendarService: CalendarService,
              private  categoryService :CategoryService
  ) {
    super(injector);
  }

  ngOnInit() {
  }
  getStatusClass(status: string): string {
    let statusClass = '';
    switch (status) {
      case 'NEW':
        statusClass = 'badge-primary';
        break;
      case 'DONE':
        statusClass = 'badge-success';
        break;
      case 'WAIT':
        statusClass = 'badge-blur';
        break;
      case 'REJECT':
        statusClass = 'badge-blur-deco';
        break;
      default:
        statusClass = 'badge-danger';
        break;
    }
    // if (this.isShowBtnStar()) {
    //   statusClass += ' badge-border';
    // }
    return statusClass;
  }
  closeModal() {
    this.modalActive.close(false);
  }

  onEdit(i: number) {
    const modal = this.modalService.open(ItemDetailComponent, {
      windowClass: 'item-detail-calendar-modal',
    });
    modal.componentInstance.job = this.listData[i];
    modal.result
      .then((res) => {
        if (res) {
         this.listData[i] = res;
        } else {
        }
      })
      .catch(() => {});
  }

  getLimitLength(str?: string, length = 20, noWhiteSpace = 10): string {
    // nếu string không có khoảng trắng thì sẽ lấy length
    if (!!str && str.indexOf(' ') === -1) {
      return str.length <= length
        ? str
        : str.slice(0, noWhiteSpace) + '...';
    }
    return !!str
      ? str.length <= length
        ? str
        : str.slice(0, length) + '...'
      : '';
  }

  public isShowBtnChat(comment: string): boolean {
    return (comment !== undefined && comment !== ''); // Custom lại loic show button chat
  }

}
