import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarLeftViewComponent } from './calendar-left-view.component';

describe('CalendarLeftViewComponent', () => {
  let component: CalendarLeftViewComponent;
  let fixture: ComponentFixture<CalendarLeftViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarLeftViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarLeftViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
