import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CalendarService {
  constructor(private http: HttpClient) {}

  getActivities(params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_activity}/activities`, { params });
  }

  getAllEventsForCBQL(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/events/findAll/cbql`,  params );
  }

  getEvents(params?): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/events/findAll`, params);
  }

  create(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/events`, data);
  }
  createSme(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/events/create/sme`, data);
  }
  update(data): Observable<any> {
    return this.http.put(`${environment.url_endpoint_activity}/events`, data);
  }

  delete(id): Observable<any> {
    return this.http.delete(`${environment.url_endpoint_activity}/events/${id}`);
  }

  deleteSme(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/events/delete/sme`, data,{responseType: 'text'});
  }

  deleteSmeOneRecord(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/events/delete/one/record/sme`, data,{responseType: 'text'});
  }

  getDetailEvents(id, params): Observable<any> {
    return this.http.get(`${environment.url_endpoint_activity}/events/findById/${id}`, { params });
  }

  getDetailEventsLoop(params): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/events/findByIdEventLoop`,  params );
  }
  updateSme(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/events/update/sme`, data,{ responseType: 'text' });
  }

  updateSmeOneRecord(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/events/update/one/record/sme`, data,{ responseType: 'text' });
  }

  replySme(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/events/reply/sme`, data,{ responseType: 'text' });
  }

  updateAlertCalendar(data): Observable<any> {
    return this.http.post(`${environment.url_endpoint_activity}/events/update/repeat/notification`, data,{ responseType: 'text' });
  }
}
