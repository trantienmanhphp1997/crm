#!/usr/bin/env groovy

static def fillEnv(BRANCH_NAME) {
	def env = "dev"
	switch (BRANCH_NAME) {
		case ~'.*develop.*|.*feature/build.*':
			env = "dev"
			break
		case ~'.*qa.*':
			env = "qa"
			break
		case ~'.*sit.*':
			env = "sit"
			break
		case ~'.*prod.*':
			env = "prod"
			break
	}

	return env
}

static GString fillImgTag(BRANCH_NAME) {
	def now = new Date().format("yyyyMMddHHmm", TimeZone.getTimeZone('UTC'))
	GString tag = "0.0.${now}-${BRANCH_NAME.tokenize('/').pop()}"
	switch (BRANCH_NAME) {
		case ~'.*develop.*|.*feature/build.*':
			tag = "0.0.${now}-${BRANCH_NAME.tokenize('/').pop()}"
			break
		case ~'.*qa.*':
			tag = "0.1.${now}-${BRANCH_NAME.tokenize('/').pop()}"
			break
		case ~'.*sit.*':
			tag = "0.2.${now}-${BRANCH_NAME.tokenize('/').pop()}"
			break
		case ~'.*prod.*':
			tag = "1.0.${now}-${BRANCH_NAME.tokenize('/').pop()}"
			break
	}

	return tag
}

pipeline {

	agent any

	environment {
		PROJECT = "crmcore"
		APP_ENV = fillEnv(env.GIT_BRANCH)
		SERVICE_NAME = "${env.GIT_URL.replaceFirst(/^.*\/([^\/]+?).git$/, '$1')}"
		NAMESPACE = "${PROJECT}-${APP_ENV}"
		IMG_TAG = fillImgTag(env.GIT_BRANCH)
		SERVICE_PORT = 80
		K8S_AUTH = "k8s_auth"
		REGISTRY_DOMAIN = "registry.evotek.vn"
		REGISTRY_AUTH = "registry_auth"
		CHART_DOMAIN = "nexus.evotek.vn"
		CHART_AUTH = "helm_chart_auth"
		CHART_REPO = "evotek_helm_repo"
		IMAGE_PULL_SECRET = "evotek-registry-pull-secret"
	}

	stages {

		stage('1. Check env') {
			steps {
        sh "java -version"
				sh "node -v"
				sh "npm -v"
        sh "docker -v"
        sh "helm version"
			}
		}

    stage('2. Npm install') {
      when { expression  {APP_ENV == 'dev'} }
      steps {
          sh "rm -rf node_modules/"
          sh "npm cache clean --force"
          sh "npm cache verify"
          sh "npm install --force"
        }
    }
		stage('3. Build application') {
			steps {
				sh "npm run build"
			}
		}

		stage('4. Build image') {
			steps {
				withCredentials([usernamePassword(credentialsId: "${REGISTRY_AUTH}", passwordVariable: 'password', usernameVariable: 'username')]) {
					configFileProvider([configFile(fileId: "${K8S_AUTH}", targetLocation: "admin.kubeconfig")]) {
						sh """
                                kubectl create secret docker-registry ${IMAGE_PULL_SECRET}  \
                                    --docker-username=${username}   \
                                    --docker-password=${password}   \
                                    --docker-server=${REGISTRY_DOMAIN}  \
                                    -n ${NAMESPACE} \
                                    --kubeconfig admin.kubeconfig | true
                        """
					}

					script {
						GString imageName = "${REGISTRY_DOMAIN}/${PROJECT}/${SERVICE_NAME}:${IMG_TAG}"
						sh "docker build -t ${imageName} ."
						sh "docker push ${imageName}"
						sh "docker rmi ${imageName}"
					}
				}
			}
		}

		stage('5. Build chart') {
			steps {
				withCredentials([usernamePassword(credentialsId: "${CHART_AUTH}", passwordVariable: 'password', usernameVariable: 'username')]) {
					sh """
                        helm repo add ${CHART_REPO} https://${CHART_DOMAIN}/repository/${CHART_REPO}    \
                                --username ${username}  \
                                --password ${password}  \
                            | true
                    """
					sh "helm create ${SERVICE_NAME}"
					sh "cp ./cicd/${APP_ENV}/values.yaml ./${SERVICE_NAME}"
					sh "cp ./cicd/deployment.yaml ./${SERVICE_NAME}/templates"
					sh "sed -i 's|DEFAULT_CHART|${SERVICE_NAME}|g' ./${SERVICE_NAME}/templates/deployment.yaml"
					sh """sed -i 's|tag: ""|tag: ${IMG_TAG}|g' ./${SERVICE_NAME}/values.yaml"""
					sh "helm lint ./${SERVICE_NAME}"
					sh "helm package ./${SERVICE_NAME} --version ${IMG_TAG}"
					sh """
                        curl -u ${username}:${password} https://${CHART_DOMAIN}/repository/${CHART_REPO}/   \
                            --upload-file ${SERVICE_NAME}-${IMG_TAG}.tgz -v
                    """
					sh "rm ./*.tgz"
				}
			}
		}

		stage('6. Deploy') {
			steps {
				withCredentials([usernamePassword(credentialsId: "${CHART_AUTH}", passwordVariable: 'password', usernameVariable: 'username')]) {
					configFileProvider([configFile(fileId: "${K8S_AUTH}", targetLocation: "admin.kubeconfig")]) {
						script {
							GString commonArgs = "-n ${NAMESPACE} --kubeconfig admin.kubeconfig"
							GString helmArgs = """${SERVICE_NAME} ${CHART_REPO}/${SERVICE_NAME} --version ${IMG_TAG}    \
                                        --set imagePullSecrets[0].name=${IMAGE_PULL_SECRET} \
                                        --set service.targetPort=${SERVICE_PORT}    \
                                        --username ${username}  \
                                        --password ${password}  \
                                        --wait"""

							sh "helm repo list"
							sh "helm repo update"
							sh "helm search repo ${CHART_REPO}"
							sh "helm history ${SERVICE_NAME} ${commonArgs} | true"
							sh "helm upgrade --install ${helmArgs} ${commonArgs}"
						}
					}
				}
			}
		}
	}
}
